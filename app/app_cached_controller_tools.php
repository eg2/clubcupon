<?php
/**
 * Conjunto de funciones y procedimientos estaticos utilizando por la clase
 * AppCachedController para realizar operaciones. Abstraidas en esta clase estatic permite
 * utilizarlos en diferentes lugares, mas que nada sin tener que cargar el framework y asi
 * poder optimizar el proceso de servir el cache.
 * Si se quiere implementar mayor rapidez se puede agregar este hack en el controlador principal, index.php,
 * del sitio para evitar cargar el framework y cargar los archivos cacheados si existen solo para usuarios no logueados.
 * <code>
 * // indicando la URL a buscar y el tiempo de vida en segundos del cache.
 * AppCachedControllerTools::dispatchCache($_SERVER['REQUEST_URI'],120);
 * </code>
 * Si se quiere se puede segmentar el cache para usuarios no autenticados en CakePHP con el siguiente codigo en vez del
 * anterior.
 * <code>
 * if (isset($_COOKIE['CAKEPHP'])):
 *   session_id($_COOKIE['CAKEPHP']);
 *   session_start();
 * endif;
 * if (!isset($_SESSION['Auth']['User'])):
 * 	AppCachedControllerTools::dispatchCache($_SERVER['REQUEST_URI'],120);
 * endif;
 * </code>
 */
class AppCachedControllerTools {
  /**
   * Extension para los archivos de cache, si se cambia se deben actualizar las reglas
   * de reescritura de apache
   * @var string
   */
  static $_cache_extension = '.cache.php';

  /**
   * Directorio donde se almacena el cache
   * @var unknown_type
   */
  static $_cache_path = 'cache/';

  /**
   * Genera el nombre de archivo para el archivo de cache desde la URL.
   * @param string $url
   * @return string
   */
  static function buildCacheFileName($url)
  {
    $path = md5($url).self::$_cache_extension;
    $path = self::$_cache_path.$path;
    return $path;
  }

	/**
   * Genera directorios recursivamente desde un path
   * @param string $path
   * @param int $mode
   */
  static function mkdirs($path, $mode = 0777)
  {
    if (is_dir($path))
    {
      return true;
    }
    return @mkdir($path, $mode, true);
  }

  /**
   * Elimina un archivo de cache
   * @param string $file
   */
  static function clearCacheFile($file)
  {
    @unlink($file);
  }

	/**
   * Valida que el cache de la presente accion exista y sea valido.
   * Si existe un cache pero esta vencido lo borra.
   * @param string $url del recurso cacheado
   * @param int $life_time segundos de vida del cache, si existe
   * @return boolean
   */
  static function hasValidCache($url,$life_time)
  {
    $file_name = self::buildCacheFileName($url);
    if (file_exists($file_name))
    {
      $last_change = time()-filemtime($file_name);
      if ($last_change < $life_time)
      {
        return true;
      } else {
        AppCachedControllerTools::clearCache($url);
        return false;
      }
    } else {
      return false;
    }
  }

	/**
   * Metodo para eliminar todo el cache generado
   * @param mixed $mixed puede ser un string o un array de paginas o nada y se borra todo el cache, es la URL
   * @return void
   */
  static function clearCache($mixed = null)
  {
    if ($mixed == null)
    {
      $del_cached_files = scandir(self::$_cache_path);
      $post_len = strlen(self::$_cache_extension);
      foreach($del_cached_files as $key => $file)
      {
        if (substr($file,-strlen(self::$_cache_extension))!=self::$_cache_extension)
        {
          unset($del_cached_files[$key]);
        } else {
          $del_cached_files[$key] = self::$_cache_path.$file;
        }
      }
    } else if (is_array($mixed))
    {
      foreach ($mixed as $url)
      {
        $del_cached_files = array($url => AppCachedControllerTools::buildCacheFileName($url));
      }
    } else if ($mixed != '')
    {
      $del_cached_files = array($mixed => AppCachedControllerTools::buildCacheFileName($mixed));
    }
    foreach ($del_cached_files as $key => $file)
    {
      AppCachedControllerTools::clearCacheFile($file);
    }
  }

  /**
   * Valida que la URL no exista en cache y si existe la devuelve.
   * @param string $url
   * @param int $life_time
   */
  static function dispatchCache($url, $life_time)
  {
    if (AppCachedControllerTools::hasValidCache($url,$life_time))
    {
      // mostramos el cache y evitamos ejecucion mas alla de este punto
      echo file_get_contents(AppCachedControllerTools::buildCacheFileName($url));
      die;
    }
  }

}