<?php

App::import('Model', 'shipping.ShippingAddress');

App::import('Model', 'shipping.ShippingAddressDeal');

class ShippingAddressDealsController extends AppController {
	
	
	var $name = 'ShippingAddressDeals';
	
	var $uses = array(
			'shipping.ShippingAddress',
			'shipping.ShippingAddressDeal',
			'api.ApiDeal',
			'api.ApiCompany',
				
	);
	
	function admin_associate() {
		$deal_id=null;
		if(isset($this->params['named']['deal_id'])){
			$deal_id=$this->params['named']['deal_id'];
		}else{
			if(isset($this->data['ShippingAddressDeal']['deal_id'])){
				$deal_id=$this->data['ShippingAddressDeal']['deal_id'];
			}
		}

		if(!isset($deal_id)){
			$this->Session->setFlash(__l('Debe elegir una Oferta para administrar sus Puntos de Retiro.') , 'default', null, 'error');
			$this->redirect(array("controller"=>"deals","action" => "admin_index"), true);
		}
	
		$this->layout = 'admin_bootstrap';
		$this->pageTitle = 'Puntos de Retiro';
		
	
		/* obtengo la OFERTA de la que quiero listar los Puntos de Retiro*/
		$currentDeal = $this->ApiDeal->findById($deal_id);
		/* obtengo la EMPRESA de la oferta*/
		$currentCompany = $this->ApiCompany->findByIdForApi($currentDeal['Deal']['company_id']);
		
		
		/* obtengo los Puntos de Retiro de la EMPRESA asociada con la Oferta*/
		$shippingCompany=$this->ShippingAddress->findByCompanyId($currentDeal['Deal']['company_id']);
		
		
		
		/* obtengo los Puntos de Retiro de CLUBCUPON */
		$shippingCc=$this->ShippingAddress->findByCompanySlug('club-cupon');
		//var_dump($shippingCc);die;
		
		/* obtengo los Puntos de Retiro de la OFERTA sin incluir las asociadas a Empresa CC - ClubCupon*/
		$shippingDeal=$this->ShippingAddressDeal->findAddressesByDealId($deal_id);
		
		
		/* obtengo los Puntos de Retiro de la OFERTA solo las asociadas a Empresa CC - ClubCupon*/
		$shippingCcDeal=$this->ShippingAddressDeal->findAddressesCcByDealId($deal_id);
		
		
		
		if (!empty($this->data)) {
			$deal_id=$this->data['ShippingAddressDeal']['deal_id'];
			
			/* elimino los Puntos de Retiro asociados a la Oferta*/
			$this->ShippingAddressDeal->delMany($deal_id);
			/* creo los nuevos Puntos de Retiro asociados a la oferta*/
			$this->ShippingAddressDeal->associateMany($deal_id,$this->data);
			
			/* elimino los Puntos de Retiro asociados a las Subofertas*/
			$this->deleteFromSubdeals($deal_id);
			/* creo los nuevos Puntos de Retiro asociados a la oferta*/
			$this->associateFromSubdeals($deal_id,$this->data);
				
			$this->Session->setFlash(__l('Los Puntos de Retiro fueron asociados con &eacute;xito.') , 'default', null, 'success');
			$this->redirect(array("controller" => "deals","action" => "admin_index"), true);
		}
		
		
		
		
		$this->set(compact('currentDeal','currentCompany','shippingCompany','shippingCc','shippingDeal','shippingCcDeal'));
	
	}
	private function deleteFromSubdeals($deal_id){
		
		$deal=array();
		$deal['Deal']['id']=$deal_id;
		$subDeals=$this->ApiDeal->findAllSubDealsForApi($deal);
		foreach ($subDeals as $subDeal) {
			//var_dump($subDeal['Deal']['id']);
			$this->ShippingAddressDeal->delMany($subDeal['Deal']['id']);
		}
	}
	private function associateFromSubdeals($deal_id,$data){
	
		$deal=array();
		$deal['Deal']['id']=$deal_id;
		$subDeals=$this->ApiDeal->findAllSubDealsForApi($deal);
		foreach ($subDeals as $subDeal) {
			//var_dump($subDeal['Deal']['id']);
			$this->ShippingAddressDeal->associateMany($subDeal['Deal']['id'],$data);
		}
	}
	
}