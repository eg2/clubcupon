<?php

App::import('Model', 'api.ApiCompany');
App::import('Model', 'accounting.AccountingType');
App::import('Behavior', 'accounting.AccountingCompany');

class AccountingCompaniesController extends AppController {

    var $name = 'AccountingCompanies';
    var $uses = array(
        'accounting.AccountingItem',
        'accounting.AccountingType',
        'api.ApiCompany'
    );

    private function initializeDependencies() {
        $this->ApiCompany->Behaviors->attach('AccountingCompany');
    }

    function beforeFilter() {
        $this->initializeDependencies();
        parent::beforeFilter();
    }

    function admin_index($calendar_id) {
        $this->pageTitle = 'Visibilidad de Empresas';
        $companies = $this->ApiCompany->findAllWithVisibleItemsByCalendarId($calendar_id);
        $this->set('companies', $companies);
    }

    function admin_update($company_id, $calendar_id, $visibility, $modality) {
        $modalityConditions = array();
        $result = false;
        if ($modality == 'Vendido') {
            $modalityConditions = AccountingType::liquidateBySold();
        } elseif ($modality == 'Redimido') {
            $modalityConditions = AccountingType::liquidateByRedeemed();
        }
        if (!empty($modalityConditions)) {
            if (in_array($visibility, array(0, 1, 2))) {
                $result = $this->AccountingItem->updateVisibilityByCompanyIdAndCalendarId($visibility, $company_id, $calendar_id, $modalityConditions);
            }
        }
        if ($result) {
            $this->Session->setFlash('Se actualizo con exito', 'default', null, 'success');
        } else {
            $this->Session->setFlash('No se pudo actualizar', 'default', null, 'error');
        }
        $this->redirect(array(
            'controller' => 'accounting_companies',
            'action' => 'index',
            $calendar_id
        ));
    }

}
