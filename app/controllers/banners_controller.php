<?php

class BannersController extends AppController {

    var $name = 'Banners';
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'Banner.order' => 'asc',
            'Banner.id' => 'desc',
        ),
    );
	var $uses = array('Banner','BannerType');

    function admin_index() {
        $this->pageTitle = 'Banners activos';
        $this->Banner->recursive = 0;
        $this->paginate = array ('order'=> array('start_date'=>'desc'));
        $this->set('banners', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            // __upload carga la imagen y retorna el nuevo nombre
            $this->data ['Banner']['tmp_image_name'] = $this->data ['Banner']['image']['tmp_name'];
            $this->data ['Banner']['image'] = $this->__upload($this->data ['Banner']['image']);
            // Guardamos el registro y notificamos
            if ($this->Banner->save($this->data)) {
                $this->Session->setFlash('Registro guardado', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('No se pudo guardar el registro', true);
            }
        } else {
            // default value
            $this->data ['Banner']['link'] = '#';
        }
        
        $banner_list = $this->Banner->BannerType->find('all', array(
                        'fields' => array('id', 'label', 'width', 'height'),
                        'recursive' => -1,
                    ));
        
        $this->set('banner_types', $this->__formatLabelsForBannerTypes($banner_list));
        
    }

    function admin_edit($id = null) {
        if (!empty($this->data)) {
			$this->Banner->id = $id;
            // Verifico si el usuario cargo una imagen
            if ($this->data ['Banner']['image']['name'] != '') {
                // __upload carga la imagen y retorna el nuevo nombre
                $this->data ['Banner']['tmp_image_name'] = $this->data ['Banner']['image']['tmp_name'];
                $fileName = $this->__upload($this->data ['Banner']['image']);
                $this->data ['Banner']['image'] = $fileName;
            } else {
                // prevenimos pisar con '' si no se selecciono una imagen...
                unset($this->data ['Banner']['image']);
            }
            // Guardamos el registro y notificamos
            if ($this->Banner->save($this->data)) {
                $this->Session->setFlash('Registro guardado', true);
                $this->redirect(array('action' => 'index'));
            }
        } else if ($id == null) {
			$this->Session->setFlash('El parametro ID no puede ser NULL para el metodo admin_edit');
        } else {
			$this->data = $this->Banner->read(null, $id);
        }
        
        $banner_list = $this->Banner->BannerType->find('all', array(
                        'fields' => array('id', 'label', 'width', 'height'),
                        'recursive' => -1,
                    ));
        
        $this->set('banner_types', $this->__formatLabelsForBannerTypes($banner_list));
                
    }

    function admin_delete($id = null) {
        //verificamos entradas indirectas
        if ($id == null) {
            die("No ID received");
        }
        $this->Banner->id = $id;
        $unknownId = Configure::read('Banner.unknown_id');
        // Verificamos si el banner existe
        if ($unknownId == $id) {
            $this->Session->setFlash('El banner Unknown no puede borrarse.');
            return $this->redirect(array('action' => 'index'));
        }
        try {
            // Borramos el registro y notificamos
            $this->Banner->delete($id);
            $this->Session->setFlash('Se borro correctamente el banner.');
        } catch (Exception $e) {
            $this->Session->setFlash('El banner no pudo borrarse. Error: ' . $e->getMessage());
        }
        $this->redirect(array('action' => 'index'));
    }

    function front() {
        $this->set('banners', $this->Banner->find('active_banners_for_current_city_row'));
    }
	
	function logout() {
		$today = date('Y-m-d H:i:s');
        $currentCity = Configure::read('Actual.city_id');
        $the_options = array(
            'conditions' => array(
                'status =' => '1',
                'OR' => array(
                    array('city_id = ' => ''),
                    array('city_id = ' => $currentCity),
                ),
                'AND' => array(
                    array('start_date <=' => $today),
                    array('end_date   >=' => $today),
                    array('banner_type_id =' => $this->BannerType->field('id', array('label' => 'Logout'))),
				)
			),
            'order' => 'Banner.order ASC',
        );
		
		$banners = $this->Banner->find('all', $the_options);
		$this->set('banners', $banners);
    }

    // INTERNALS -------------------------------------------
    // Carga de imagenes, opcionalmente pasamos un directorio
    function __upload($file_array, $targetDir = '/img/Banner/') {
        $file = new File($file_array ['tmp_name']);
        $file_base = pathinfo($file_array ['name']);
        if ($file_base ['extension'] != '') {
            $ext = strtolower($file_base ['extension']);
            $name = $this->data ['Banner']['name'];
            if ($ext != 'jpg' && $ext != 'jpeg' && $ext != 'gif' && $ext != 'png') {
                $this->Session->setFlash('Solo se pueden subir im&aacute;genes!', true);
                $this->render();
            } else {
                $data = $file->read();
                $file->close();
                $rndmSeed = $this->__str_rand(10);
                $cleanName = $this->__cleanChars($name);
                $full_path = $targetDir . "banner_" . $cleanName . "-" . $rndmSeed . "." . $ext;
                $file = new File(WWW_ROOT . $full_path, true);
                $file->write($data);
                $file->close();
                return $full_path;
            }
        }
    }

    // Reemplaza todos los caracteres especiales, ver correspondencia 1<->1
    function __cleanChars($string) {
        $string = str_replace(array(' ', '-'), array('_', '_'), $string);
        // revision extra para evitar acentos y caracteres bizarros...
        $string = str_replace(array('á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò', 'ù', 'ä', 'ë', 'ï', 'ö', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü'),
                              array('a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U'), $string);
        return $string;
    }

    function __str_rand($length = 8) {
        // Possible values
        $output = 'abcdefghijklmnopqrstuvwqyz0123456789';

        // Seed
        list ($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);

        // Generate
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $output [mt_rand(0, strlen($output) - 1)];
        }
        return $str;
    }
    
    /*
     * Toma los resultados de la búsqueda de los tipos de banners, y cambia el label
     * para que especifique la medida.
     * Devuelve el array formateado para un select
     * 
     */
    
    function __formatLabelsForBannerTypes($banner_list){
        $banner_types = array();
        
        foreach($banner_list as $banner){
            
            $id    = $banner['BannerType']['id'];
            $label = $banner['BannerType']['label'] . ' ( ancho: ' . $banner['BannerType']['width'] . 'px -- alto: ' . $banner['BannerType']['height'] . 'px )';
            
            $banner_types[$id] = $label;
            
        }
        
        return $banner_types;
    }

}

?>