<?php

class RedemptionsController extends AppController {

    public $name = "Redemptions";
    public $recursive = - 1;
    var $uses = array(
        'Redemption',
        'DealUser',
        'Deal',
        'Company'
    );
    var $components = array(
        'api.ApiLogger'
    );

    function redeem_coupon_by_posnetcode() {
        $posnetCode = $this->params['named']['posnet_code'];
        if (empty($posnet_code)) {
            
        }
        if (!$this->Auth->user('id')) {
            
        }
    }

    function redeemcoupon() {
        $missing_posnetcode = false;
        $deal_user_id = $_POST['deal_user_id'];
        $posnet_code = $_POST['posnet_code'];
        $only_posnet_code = ($this->params['form']['only_posnet_code'] == 1);
        if ($only_posnet_code) {
            $posnet_code = $this->params['form']['posnet_code'];
            if (empty($posnet_code)) {
                $result = array(
                    'status' => 0,
                    'message' => 'El código posnet está vacío'
                );
                $this->set('result', json_encode($result));
                return;
            }
            $redemption = $this->Redemption->findFirstByPosnetCode($posnet_code);
            if (is_null($redemption)) {
                $result = array(
                    'status' => 0,
                    'message' => 'El código posnet es incorrecto'
                );
                $this->set('result', json_encode($result));
                return;
            }
            $currentUser = $this->User->findById($this->Auth->user('id'));
            $this->Deal->Behaviors->detach('Excludable');
            $deal = $this->Deal->findById($redemption['DealUser']['deal_id']);
            $companyOfUser = $this->Company->findByUserId($currentUser['User']['id']);
            if ($companyOfUser['Company']['id'] != $deal['Deal']['company_id']) {
                $result = array(
                    'status' => 0,
                    'message' => 'El código posnet es incorrecto'
                );
                $this->set('result', json_encode($result));
                return;
            }
            if (!is_null($redemption['Redemption']['way'])) {
                $result = array(
                    'status' => 0,
                    'message' => 'El cupón ya está redimido'
                );
                $this->set('result', json_encode($result));
                return;
            }
            if ($this->Redemption->isCouponExpired($deal, $redemption)) {
                $result = array(
                    'status' => 0,
                    'message' => 'No es posible redimir un cupón vencido'
                );
                $this->set('result', json_encode($result));
                return;
            }
            if ($redemption['DealUser']['is_returned'] == 1) {
                $result = array(
                    'status' => 0,
                    'message' => 'No se puede redimir el cupón'
                );
                $this->set('result', json_encode($result));
                return;
            }
            $update_status = $this->Redemption->updateAsRedeemedByWeb($redemption['DealUser']['id'], $this->Auth->user('id'), $redemption['DealUser']['quantity'] * $redemption['DealUser']['discount_amount']);
            if ($update_status) {
                $result = array(
                    'status' => 1,
                    'message' => 'La redención fue exitosa'
                );
                $coupon = $this->DealUser->findCouponById($deal_user_id);
                $dealForEvent = $this->Deal->findDealFieldsForAccountingEventService($redemption['DealUser']['deal_id']);
                App::import('Component', 'AccountingEventService');
                $AccountingEventService = new AccountingEventServiceComponent();
                $AccountingEventService->registerAccountingEvent($dealForEvent);
            }
            $this->set('result', json_encode($result));
        } else {
            if ((empty($posnet_code) || empty($deal_user_id)) || empty($deal_discounted_price) && !$this->Auth->user('id')) {
                $missing_posnetcode = empty($posnet_code);
                if (!$missing_posnetcode) {
                    $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index'
                    ));
                }
            }
            $redemption = $this->Redemption->findByDealUserId($deal_user_id, 1);
            $this->Deal->Behaviors->detach('Excludable');
            $deal = $this->Deal->findById($redemption['DealUser']['deal_id']);
            if (!empty($redemption) && !$missing_posnetcode) {
                $discounted_price = (isset($_POST['posnet_price']) && $_POST['posnet_price'] != 0) ? $_POST['posnet_price'] : 0;
                $approved_code = $this->Redemption->isPosnetCodeCheckOk($redemption, $posnet_code);
                if ($approved_code) {
                    if ($redemption['DealUser']['is_returned'] == 1) {
                        $result = 'No se puede redimir el cupón';
                    } else if ($this->Redemption->isCouponExpired($deal, $redemption)) {
                        $result = 'No es posible redimir un cupón vencido';
                    } else if ($this->_isDiscontedPriceLessOrEqualThanDeal($deal_user_id, $discounted_price)) {
                        $this->Redemption->unbindModel(array(
                            'belongsTo' => array(
                                'DealUser'
                            )
                        ));
                        $update_status = $this->Redemption->updateAsRedeemedByWeb($deal_user_id, $this->Auth->user('id'), $discounted_price);
                        if ($update_status) {
                            $result = 'La redención fue exitosa';
                            $coupon = $this->DealUser->findCouponById($deal_user_id);
                            $dealForEvent = $this->Deal->findDealFieldsForAccountingEventService($coupon['DealUser']['deal_id']);
                            App::import('Component', 'AccountingEventService');
                            $AccountingEventService = new AccountingEventServiceComponent();
                            $AccountingEventService->registerAccountingEvent($dealForEvent);
                        } else {
                            $result = 'No se pudo actualizar la redención';
                        }
                    } else {
                        $result = 'El importe debe ser menor o igual al precio de la oferta';
                    }
                } else {
                    $result = 'El código ingresado es incorrecto';
                }
            } else {
                $result = 'No se encontró la redención';
                $result = $missing_posnetcode ? 'codigo posnet enviado no puede estar vacio' : $result;
                if ($posnet_code == "no_aplica") {
                    $update_status = $this->updateCuponWithNoRedemptionEntry($deal_user_id);
                    if ($update_status) {
                        $result = 'La redención fue exitosa';
                    } else {
                        $result = 'No se pudo actualizar la redención';
                    }
                }
            }
            $this->set('result', $result);
        }
    }

    function _isDiscontedPriceLessOrEqualThanDeal($deal_user_id, $discounted_price) {
        $deal = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.id' => $deal_user_id
            ),
            'contain' => array(
                'Deal' => array(
                    'fields' => array(
                        'Deal.discounted_price'
                    )
                )
            ),
            'fields' => array(
                'Deal.discounted_price'
            )
        ));
        return $discounted_price <= $deal['Deal']['discounted_price'];
    }

    public function updateCuponWithNoRedemptionEntry($deal_user_id) {
        App::import('Model', 'DealUser');
        $this->DealUser = new DealUser();
        return $this->DealUser->markAsUsedByCompany($deal_user_id);
    }

}
