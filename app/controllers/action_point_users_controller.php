<?php
class ActionPointUsersController extends AppController
  {
    var $name = 'ActionPointUsers';

    function index ()
      {
        $this->paginate = array (
            'conditions' => array ('user_id' => $this->Auth->user ('id')),
            'contain' => array (
                'ActionPoint' => array (
                    'Action' => array (
                        'fields' => array (
                            'Action.name',
                            'Action.description',
                        ),
                    ),
                    'fields' => array (
                        'ActionPoint.points',
                    )
                ),
            ),
        );
        $this->set ('actionPointUsers', $this->paginate ());
      } /*
        App::import ('Model', 'User');
        $this->User = new User ();
        App::import ('Model', 'Action');
        $this->Action = new Action ();
        App::import ('Component', 'Cron');
        $this->Cron = new CronComponent ();


        // var_dump ($this->User->add_points (1509, 2));
        // var_dump ($this->User->debit_points (1509, 16078, 5));
        // var_dump ($this->User->refund_points (16078));
        // var_dump ($this->User->get_current_points (1509));
        // var_dump ($this->Cron->points_expiring_soon ());

        die;
      }
    */
    function admin_add_points() {
      App::import ('Model', 'User');
      $this->User = new User ();

      if(!empty($this->data['ActionPointUser']['amount']) && !empty($this->data['ActionPointUser']['user_id'])) {
        $this->User->credit_points($this->data['ActionPointUser']['user_id'],$this->data['ActionPointUser']['amount']);

        $this->redirect (array('controller' => 'users', 'action' => 'index'));
      }
      $userId = $this->params['named']['user_id'];
      $user = $this->User->find('first', array(
                              'conditions' => array(
                                  'User.id = ' => $userId
                              ),
                              'recursive' => -1
                          ));
      if (empty($user)) {
        die('Invalid user_id in params');
      }
      $this->set('user',$user);
    }
  }
?>