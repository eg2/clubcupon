<?php
class UserProfilesController extends AppController
{
    var $name = 'UserProfiles';
    var $uses = array(
        'UserProfile',
        'Attachment',
        'EmailTemplate',
    );
    var $components = array(
        'Email'
    );
    var $helpers = array('Clubcupon');

    function beforeFilter()
    {
        $this->Security->disabledFields = array(
            'City.id',
            'State.id'
        );
        parent::beforeFilter();
    }
    function edit($user_id = null)
    {
        $this->pageTitle = __l('Edit Profile');
        $this->set('certificaPath', 'ClubCupon/cpanel/profile');


        if (!empty($this->data)) {
          // grabar
            if (empty($this->data['User']['id'])) {
                $this->data['User']['id'] = $this->Auth->user('id');
            }
            $user = $this->UserProfile->User->find('first', array(
                'conditions' => array(
                    'User.id' => $this->data['User']['id']
                ) ,
                'contain' => array(
                    'UserProfile' => array(
                        'fields' => array(
                            'UserProfile.id'
                        )
                    ) ,
                ) ,
                'recursive' => 0
            ));
            if (!empty($user)) {
                $this->data['UserProfile']['id'] = $user['UserProfile']['id'];
                $this->data['User']['email'] = $user['User']['email'];
            }
            $this->data['UserProfile']['user_id'] = $this->data['User']['id'];
            if($this->data['UserProfile']['neighbourhood_id']==0)
              $this->data['UserProfile']['neighbourhood_id']='';

            $this->UserProfile->set($this->data);
            $this->UserProfile->User->set($this->data);
            $this->UserProfile->State->set($this->data);
            $this->UserProfile->Region->set($this->data);
            $this->UserProfile->Neighbourhood->set($this->data);
            $this->UserProfile->City->set($this->data);

            if ($this->UserProfile->User->validates() &
                $this->UserProfile->validates() &
                $this->UserProfile->City->validates() &
                $this->UserProfile->Neighbourhood->validates() &
                $this->UserProfile->Region->validates() &
                $this->UserProfile->State->validates()
                ) {
                if(isset($this->data['UserProfile']['neighbourhood_id']) && $this->data['UserProfile']['neighbourhood_id']=='')
                  $this->data['UserProfile']['neighbourhood_id']=NULL;

                //$this->data['UserProfile']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->UserProfile->City->findOrSaveAndGetId($this->data['City']['name']);
                //$this->data['UserProfile']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->UserProfile->State->findOrSaveAndGetId($this->data['State']['name']);

                if ($this->UserProfile->save($this->data)) {
                    $this->UserProfile->User->save($this->data['User']);
                    if(isset($this->data['UserProfile']['language_id']) && $this->data['UserProfile']['language_id']!= $user['UserProfile']['languge_id'])
                      {
                        $this->UserProfile->User->UserLogin->updateUserLanguage();
                      }
                }
                $this->_saveQuestions($this->data['UserProfile']['user_id']);
                $this->Session->setFlash(__l('User Profile has been updated') , 'default', null, 'success');
                if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id')) AND $this->Auth->user('id') != $this->data['User']['id'] AND Configure::read('user.is_mail_to_user_for_profile_edit')) {
                    // Send mail to user to activate the account and send account details
                    $emailFindReplace = array(
                                              '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                                              '##USERNAME##' => $user['User']['username'],
                                              '##SITE_NAME##' => Configure::read('site.name') ,
                                              '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
                    );
                    $email = $this->EmailTemplate->selectTemplate('Admin User Edit');
                    $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                    $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                    $this->Email->to = $user['User']['email'];
                    $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                    $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                    $this->Email->send(strtr($email['email_content'], $emailFindReplace));
                }
            } else {
                
                if(!empty($this->UserProfile->validationErrors['dob'])){
                    $this->UserProfile->invalidate('dob','Debes ingresar una fecha completa'); 
                }
                
                $this->Session->setFlash(__l('User Profile could not be updated. Please, try again.') , 'default', null, 'error');
            }
            $user = $this->UserProfile->User->find('first', array(
                'conditions' => array(
                    'User.id' => $this->data['User']['id']
                ) ,
                'contain' => array(
                    'UserProfile' => array(
                        'fields' => array(
                            'UserProfile.id'
                        )
                    ) ,
                    'UserAnswer' => array(
                                'fields' => array(
                                  'UserAnswer.question_id',
                                  'UserAnswer.answer_id',
                                                  ),

                                ),
                ) ,
                'recursive' => 1
            ));
            if (!empty($user['User'])) {
                unset($user['UserProfile']);
                $this->data['User'] = array_merge($user['User'], $this->data['User']);
                $this->data['UserAnswer'] = $user['UserAnswer'];
            }
			//Setting ajax layout when submitting through iframe with jquery ajax form plugin
			if (!empty($this->params['form']['is_iframe_submit'])) {
				$this->layout = 'ajax';
			}
        } else {
          // popular campos (data vacio)
            if(empty($user_id)){
                $user_id = $this->Auth->user('id');
            }
            $this->set ('myPoints', $this->UserProfile->User->get_current_points ($user_id));

            $this->data = $this->UserProfile->User->find('first', array(
                'conditions' => array(
                    'User.id' => $user_id,
                    'User.user_type_id !=' => ConstUserTypes::Company
                ) ,
                'fields' => array(
                    'User.user_type_id',
                    'User.username',
                    'User.email',
                    'User.id',
                    'User.user_type_id',
                    'User.user_login_count',
                    'User.user_view_count',
                    'User.is_active',
                    'User.is_email_confirmed',
                ) ,
                'contain' => array(
                    'UserAnswer' => array(
                                'fields' => array(
                                  'UserAnswer.question_id',
                                  'UserAnswer.answer_id',
                                                  ),

                                ),
                    'UserProfile' => array(
                        'fields' => array(
                            'UserProfile.first_name',
                            'UserProfile.last_name',
                            'UserProfile.middle_name',
                            'UserProfile.gender',
                            'UserProfile.about_me',
                            // 'UserProfile.address_street',
                            // 'UserProfile.address_number',
                            'UserProfile.dni',
                            // 'UserProfile.i_love',
                            // 'UserProfile.other_sites',
                            'UserProfile.country_id',
                            'UserProfile.state_id',
                            'UserProfile.region_id',
                            'UserProfile.neighbourhood_id',
                            'UserProfile.neighbourhood',
                            'UserProfile.city_id',
                            'UserProfile.zip_code',
                            'UserProfile.cell_prefix',
                            'UserProfile.cell_number',
                            'UserProfile.cell_carrier',
                            'UserProfile.is_sms_enabled',
                            'UserProfile.dob',
                            'UserProfile.language_id',
                            'UserProfile.paypal_account',
                            'UserProfile.user_id'
                        ) ,
                        'City' => array(
                            'fields' => array(
                                'City.name'
                            )
                        ) ,
                        'State' => array(
                            'fields' => array(
                                'State.name'
                            )
                        )
                    )
                ) ,
                'recursive' => 2
            ));
            $this->data['UserProfile']['user_id'] = $user_id;
            if (!empty($this->data['UserProfile']['City'])) {
                $this->data['City']['name'] = $this->data['UserProfile']['City']['name'];
            }
            if (!empty($this->data['UserProfile']['State']['name'])) {
                $this->data['State']['name'] = $this->data['UserProfile']['State']['name'];
            }
            if(empty($this->data))
            {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['User']['username'];
        $title_for_layout = $this->pageTitle; //titulo
		$this->set('title_for_layout', $title_for_layout);
        $neighbourhoods = $this->UserProfile->Neighbourhood->find('list', array ('conditions' => array ('is_selectable' => true)));
        $countries = $this->UserProfile->Country->find('list', array ('conditions' => array ('is_selectable' => true)));

        $states=$this->UserProfile->State->find('list',array('conditions'=>array('country_id'=>$this->data['UserProfile']['country_id'], 'is_selectable' => true)));
        if($this->data['UserProfile']['state_id']!='')
          $cities=$this->UserProfile->City->find('list',array('conditions'=>array('state_id'=>$this->data['UserProfile']['state_id'], 'is_selectable' => true)));
        else
          $cities=array();
        if($this->data['UserProfile']['city_id']!='')
          $neighbourhoods=$this->UserProfile->Neighbourhood->find('list',array('conditions'=>array('city_id'=>$this->data['UserProfile']['city_id'], 'is_selectable' => true)));
        else
          $neighbourhoods=array();

        $neighbourhoods[0]='-- '.__l('Otro barrio').' --';
        $regions = $this->UserProfile->Neighbourhood->Region->find('list');
        if(empty($this->data['UserProfile']['gender'])){
            $this->data['UserProfile']['gender'] = '-- Seleccione una opción --';
        }
        $this->set(compact('countries','neighbourhoods','regions','states','cities','neighbourhoods'));
    }



    function _saveQuestions ($userId)
      {
        // busca en cada campo de 'UserProfile'
        foreach ($this->data ['UserProfile'] as $possibleInputName => $value)
          {
            // solo miramos los campos 'question_<num>'
            if (strpos ($possibleInputName, 'question_') === 0)
              {
                // buscamos <num> en 'question_<num>' y lo guardamos en questionId
                $tmp = explode ('_', $possibleInputName);
                $questionId = $tmp [1];

                // se inicializa el answerIds y isFree
                $answerIds = $value;
                $isFree = false;

                // setear los datos para cuando es free
                if ($value === '0')
                  {
                    $answerContent = $this->data ['UserProfile']['questionFree_' . $questionId];
                    $answerIds = $this->UserProfile->User->UserAnswer->Answer->findOrGetIdByContent ($answerContent, $questionId);
                    $isFree = true;
                  }

                // de una forma u otra, answerIds va a ser una array
                if (!is_array ($answerIds))
                  {
                    $answerIds = array ($answerIds);
                  }

                // para tenerla a mano
                $UserAnswer = $this->UserProfile->User->UserAnswer;

                $selfModel = $this->UserProfile;
                $dataSource = $selfModel->getDataSource ();
                $dataSource->begin ($selfModel);

                // borrar todas las asociaciones del usuario a esa pregunta y sus respuestas
                $res = $UserAnswer->deleteAll (array ('UserAnswer.user_id' => $userId, 'UserAnswer.question_id' => $questionId), false);

                // setear todas las asociaciones del usuario a esa pregunta y sus respuestas
                if ($value !== '')
                  {
                    foreach ($answerIds as $answerId)
                      {
                        $UserAnswer->create ();
                        $res = $res && $UserAnswer->set (array ('user_id' => $userId, 'question_id' => $questionId, 'answer_id' => $answerId));
                        $UserAnswer->save ();
                      }
                  }

                if ($res)
                  {
                    $dataSource->commit ($selfModel);
                  }
                else
                  {
                    $dataSource->rollback ($selfModel);
                  }
              }
          }
      }


    function admin_edit($id = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
        if (is_null($id) && empty($this->data)) {
            $this->cakeError('error404');
        }
        $this->setAction('edit', $id);
    }
    function admin_user_account($user_id = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
        if (is_null($user_id)) {
            $this->cakeError('error404');
        }
        $this->setAction('my_account', $user_id);
    }
    function my_account($user_id = null)
    {
        if (is_null($user_id)) {
            $this->cakeError('error404');
        }
        $this->set('user_id', $user_id);
    }

  function geoAjax ($dimension = 'state', $parent_id = 1)
    {
      $this->layout = 'ajax';
      $ret = array ();

      switch ($dimension)
        {
          case 'state':
            $ret = $this->UserProfile->State->find ('list',
                    array ('conditions' =>
                        array ('country_id' => $parent_id,
                               'is_selectable' => true,
                               'is_approved' => true),
                               'order' => 'name'));
            break;
          case 'city':
            $ret = $this->UserProfile->City->find ('list',
                    array ('conditions' =>
                        array ('state_id' => $parent_id,
                            'is_selectable' => true,
                            'is_approved' => true,
                            'is_group'=> 0),
                        'order' => 'name'));
            break;
          case 'neigh':
            $ret = $this->UserProfile->Neighbourhood->find ('list', array ('conditions' => array ('city_id' => $parent_id, 'is_selectable' => true), 'order' => 'name'));
            $ret [0] = '-- ' . __l ('Otro') . ' --';
            break;
        }

      $this->set ('geos', $ret);
    }
}
?>
