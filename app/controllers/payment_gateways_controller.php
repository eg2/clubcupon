<?php
class PaymentGatewaysController extends AppController
{
    var $name = 'PaymentGateways';
    function beforeFilter()
    {
        $this->Security->disabledFields = array(
            'PaymentGateway.makeActive',
            'PaymentGateway.makeInactive',
            'PaymentGateway.makeTest',
            'PaymentGateway.makeLive',
            'PaymentGateway.makeDelete'
        );
        // indicamos que por defecto utilice la conexion master a la DB
        AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }
    function index()
    {
        $this->pageTitle = __l('Payment Gateways');
        if (!empty($this->params['named']['type']) and $this->params['named']['type'] == 'menu') {
            $conditions['PaymentGateway.is_active'] = 1;
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'recursive' => 0
        );
        if (!empty($this->params['named']['current'])) {
            $this->set('current', $this->params['named']['current']);
        }
        $this->set('paymentGateways', $this->paginate());
        if (!empty($this->params['named']['type']) and $this->params['named']['type'] == 'menu') {
            $this->autoRender = false;
            $this->render('index_menu');
        }
    }
    function admin_index()
    {
        $this->pageTitle = __l('Payment Gateways');
        $this->paginate = array(
            'recursive' => 0
        );
        $this->set('paymentGateways', $this->paginate());
    }
    function admin_view($id = null)
    {
        $this->pageTitle = __l('Payment Gateway');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $paymentGateway = $this->PaymentGateway->find('first', array(
            'conditions' => array(
                'PaymentGateway.id = ' => $id
            ) ,
            'fields' => array(
                'PaymentGateway.id',
                'PaymentGateway.created',
                'PaymentGateway.modified',
                'PaymentGateway.name',
                'PaymentGateway.description',
                'PaymentGateway.email',
                'PaymentGateway.seller_id',
                'PaymentGateway.is_active',
                'PaymentGateway.is_test',
                'PaymentGateway.payment_trans_key',
                'PaymentGateway.payment_delimiter',
                'PaymentGateway.payment_trans_type',
                'PaymentGateway.protx_key',
            ) ,
            'recursive' => - 1,
        ));
        if (empty($paymentGateway)) {
            $this->cakeError('error404');
        }
        $this->pageTitle.= ' - ' . $paymentGateway['PaymentGateway']['name'];
        $this->set('paymentGateway', $paymentGateway);
    }
    function admin_add()
    {
        $this->pageTitle = __l('Add Payment Gateway');
        if (!empty($this->data)) {
            $this->PaymentGateway->create();
            if ($this->PaymentGateway->save($this->data)) {
                $this->Session->setFlash(__l('Payment Gateway has been added') , 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__l('Payment Gateway could not be added. Please, try again.') , 'default', null, 'error');
            }
        }
        $status_options = array(
            '' => __l('Select Status') ,
            0 => __l('Inactive') ,
            1 => __l('Active')
        );
        $this->set('status_options', $status_options);
    }
    function admin_edit($id = null)
    {
        $this->pageTitle = __l('Edit Payment Gateway');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->PaymentGateway->save($this->data)) {
                $this->Session->setFlash(__l('Payment Gateway has been updated') , 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__l('Payment Gateway could not be updated. Please, try again.') , 'default', null, 'error');
            }
        } else {
            $this->data = $this->PaymentGateway->read(null, $id);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $status_options = array(
            '' => __l('Select Status') ,
            0 => __l('Inactive') ,
            1 => __l('Active')
        );
        $this->set('status_options', $status_options);
        $this->pageTitle.= ' - ' . $this->data['PaymentGateway']['name'];
    }
    function admin_delete($id = null)
    {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->PaymentGateway->del($id)) {
            $this->Session->setFlash(__l('Payment Gateway deleted') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }
    function admin_move_to()
    {
        if (!empty($this->data['PaymentGateway']['Id'])) {
            foreach($this->data['PaymentGateway']['Id'] as $gateway_id => $is_checked) {
                if (!empty($is_checked['Check'])) {
                    if (!empty($this->data['PaymentGateway']['makeActive'])) {
                        $gateway['PaymentGateway']['id'] = $gateway_id;
                        $gateway['PaymentGateway']['is_active'] = 1;
                        $this->PaymentGateway->save($gateway, false);
                        $this->Session->setFlash(__l('Checked payment gateways has been changed to active') , 'default', null, 'success');
                    }
                    if (!empty($this->data['PaymentGateway']['makeInactive'])) {
                        $gateway['PaymentGateway']['id'] = $gateway_id;
                        $gateway['PaymentGateway']['is_active'] = 0;
                        $this->PaymentGateway->save($gateway, false);
                        $this->Session->setFlash(__l('Checked payment gateways has been changed to inactive') , 'default', null, 'success');
                    }
                    if (!empty($this->data['PaymentGateway']['makeTest'])) {
                        $gateway['PaymentGateway']['id'] = $gateway_id;
                        $gateway['PaymentGateway']['is_test'] = 1;
                        $this->PaymentGateway->save($gateway, false);
                        $this->Session->setFlash(__l('Checked payment gateways has been changed to inactive') , 'default', null, 'success');
                    }
                    if (!empty($this->data['PaymentGateway']['makeLive'])) {
                        $gateway['PaymentGateway']['id'] = $gateway_id;
                        $gateway['PaymentGateway']['is_test'] = 0;
                        $this->PaymentGateway->save($gateway, false);
                        $this->Session->setFlash(__l('Checked payment gateways has been changed to inactive') , 'default', null, 'success');
                    }
                    if (!empty($this->data['PaymentGateway']['makeDelete'])) {
                        $this->PaymentGateway->del($gateway_id);
                        $this->Session->setFlash(__l('Checked payment gateways has been deleted') , 'default', null, 'success');
                    }
                }
            }
        }
        $this->redirect(array(
            'controller' => 'payment_gateways',
            'action' => 'index'
        ));
    }
}
?>