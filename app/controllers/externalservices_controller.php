<?php

define('EXTERNAL_PATH_SERVICES', ROOT . DS . 'scripts/');
ini_set('include_path', EXTERNAL_PATH_SERVICES . PATH_SEPARATOR . ini_get('include_path'));
require_once 'ServiceFactory.php';

class OperationsServiceWithCakeLogger extends OperationsService {

    protected function log($msg) {
        return Debugger::log($msg, LOG_DEBUG);
    }

}

class ExternalservicesController extends AppCachedController {

    var $name = 'Externalservices';
    var $components = array(
        'Product.ProductInventoryService'
    );
    var $helpers = array();
    var $uses = array(
        'DealExternal',
        'Deal',
        'User',
    );

    function beforeFilter() {
        $this->Security->validatePost = false;
        $this->Security->disabledFields = array();
        parent::beforeFilter();
    }

    function admin_payments_acredit() {
        if (ConstUserTypes::isNotLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash('Usuario sin privilegios.');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
            die;
        }
        if (empty($this->params['named']['user_id'])) {
            die('Missing user_id in params');
        }
        if (empty($this->params['named']['deal_external_id'])) {
            die('Missing deal_external_id in params');
        }
        $userId = $this->params['named']['user_id'];
        $dealExternalId = $this->params['named']['deal_external_id'];
        $operationsService = ServiceFactory::getOpetarionsService('httpRequestCake');
        try {
            $operationsService->setHttpUser($this->Auth->user('id'));
            $operationsService->creditDealExternal($dealExternalId);
            $dealExternal = $this->DealExternal->findDealExternalById($dealExternalId);
            $this->ProductInventoryService->decrease($dealExternal['DealExternal']['deal_id'], $dealExternal['DealExternal']['quantity']);
        } catch (Exception $e) {
            $this->Session->setFlash('Error intentando acreditar pago: ' . $e->getMessage(), 'default', null, 'error');
            $this->redirect(array(
                'action' => 'admin_payments',
                'user_id' => $userId
            ));
        }
        $this->Session->setFlash('Se acreditó el pago correctamente', 'default', null, 'success');
        $this->redirect(array(
            'action' => 'admin_payments',
            'user_id' => $userId
        ));
    }

    function admin_payments_cancel() {
        if (ConstUserTypes::isNotLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash('Usuario sin privilegios.');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
            die;
        }
        if (empty($this->params['named']['user_id'])) {
            die('Missing user_id in params');
        }
        if (empty($this->params['named']['deal_external_id'])) {
            die('Missing deal_external_id in params');
        }
        $userId = $this->params['named']['user_id'];
        $dealExternalId = $this->params['named']['deal_external_id'];
        $operationsService = ServiceFactory::getOpetarionsService('httpRequestCake');
        try {
            $operationsService->setHttpUser($this->Auth->user('id'));
            $operationsService->cancelDealExternal($dealExternalId, null /* todos los cupones */);
        } catch (Exception $e) {
            $this->Session->setFlash('Error intentando cancelar pago: ' . $e->getMessage(), 'default', null, 'error');
            $this->redirect(array(
                'action' => 'admin_payments',
                'user_id' => $userId
            ));
        }
        $this->Session->setFlash('Se canceló el pago correctamente', 'default', null, 'success');
        $this->redirect(array(
            'action' => 'admin_payments',
            'user_id' => $userId
        ));
    }

    function admin_payments() {
        if (ConstUserTypes::isNotLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash('Usuario sin privilegios.');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
            die;
        }
        if (empty($this->params['named']['user_id'])) {
            $this->cakeError('Missing user_id in params');
        }
        $userId = $this->params['named']['user_id'];
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id = ' => $userId
            ),
            'recursive' => - 1
        ));
        if (empty($user)) {
            die('Invalid user_id in params');
        }
        $this->set('user', $user);
        $this->paginate = array(
            'conditions' => array(
                'DealExternal.user_id =' => $userId
            ),
            'limit' => 10,
        );
        $dealExternals = $this->paginate();
        $this->set('dealExternals', $dealExternals);
    }

    function _add_amount_to_wallet($userId, $amount) {
        die('Esta funcionalidad fue deshabilitada');
        if (ConstUserTypes::isNotLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash('Usuario sin privilegios.');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
            die;
        }
        $operationsService = ServiceFactory::getOpetarionsService('httpRequestCake');
        $operationsService->setHttpUser($this->Auth->user('id'));
        try {

            $operationsService->addFundsToWallet($userId, $amount);
        } catch (Exception $e) {
            $this->Session->setFlash('Error intentando agregar dinero a la Cuenta Club Cupón: ' . $e->getMessage(), 'default', null, 'error');
            $this->redirect(array(
                'action' => 'admin_add_amount_to_wallet',
                'user_id' => $userId
            ));
        }
        $this->Session->setFlash('Se actualizó el dinero correctamente', 'default', null, 'success');
        $this->redirect(array(
            'action' => 'admin_add_amount_to_wallet',
            'user_id' => $userId
        ));
    }

    function admin_add_amount_to_wallet() {
        die('Esta funcionalidad fue deshabilitada');
        if (!empty($this->data['Externalservices']['amount']) && !empty($this->data['Externalservices']['user_id'])) {
            return $this->_add_amount_to_wallet($this->data['Externalservices']['user_id'], $this->data['Externalservices']['amount']);
        }
        if (empty($this->params['named']['user_id'])) {
            die('Missing user_id in params');
        }
        $userId = $this->params['named']['user_id'];
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id = ' => $userId
            ),
            'recursive' => - 1
        ));
        if (empty($user)) {
            die('Invalid user_id in params');
        }
        $this->set('user', $user);
    }

}
