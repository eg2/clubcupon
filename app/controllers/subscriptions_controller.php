<?php

class SubscriptionsController extends AppController {

    var $name = 'Subscriptions';
    var $uses = array(
        'Subscription',
        'User'
    );

    function beforeFilter() {
        $this->Security->disabledFields = array();
        $this->Security->enabled = false;
        $this->Cookie->domain = 'clubcupon.com';
        parent::beforeFilter();
    }

    private function isValidCaptcha($captcha) {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';

        $img = new Securimage();
        return $img->check($captcha);
    }

    function _subscribeWelcomeMail($email, $city) {
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        App::import('Component', 'Email');
        $this->Email = & new EmailComponent();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $template = $this->EmailTemplate->selectTemplate('Subscription Welcome Mail');
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##SITE_NAME##' => Configure::read('site.name'),
            '##FROM_EMAIL##' => ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'],
            '##LEARN_HOW_LINK##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'whitelist'
                    ), true) . '?from=mailing',
            '##REFERRAL_AMOUNT##' => Configure::read('site.currency') . Configure::read('user.referral_amount'),
            '##REFER_FRIEND_LINK##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'refer_a_friend'
                    ), true) . '?from=mailing',
            '##FACEBOOK_LINK##' => ($city['City']['facebook_url']) ? $city['City']['facebook_url'] : Configure::read('facebook.site_facebook_url'),
            '##TWITTER_LINK##' => ($city['City']['twitter_url']) ? $city['City']['twitter_url'] : Configure::read('twitter.site_twitter_url'),
            '##RECENT_DEALS##' => Router::url(array(
                'controller' => 'deals',
                'action' => 'index',
                'admin' => false,
                'type' => 'recent'
                    ), true) . '?from=mailing',
            '##CONTACT_US_LINK##' => Router::url(array(
                'controller' => 'contacts',
                'action' => 'add',
                'admin' => false
                    ), true) . '?from=mailing',
            '##SITE_LOGO##' => Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'logo-email.png',
                'admin' => false
                    ), true),
            '##UNSUBSCRIBE_LINK##' => Router::url(array(
                'controller' => 'subscriptions',
                'action' => 'unsubscribe',
                $this->Subscription->getLastInsertId(),
                'admin' => false
                    ), true) . '?from=mailing',
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##SUBSCRIBER_EMAIL##' => $email,
            '##SUBSCRIBER_IP##' => $this->RequestHandler->getClientIP(),
            '##SUBSCRIPTION_DATE##' => date('d/m/Y'),
            '##SUBSCRIPTION_TIME##' => date('H:i:s')
        );
        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
        $this->Email->to = $email;
        $this->Email->subject = strtr($template['subject'], $emailFindReplace);
        $this->Email->content = strtr($template['email_content'], $emailFindReplace);
        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
        return $this->Email->send($this->Email->content);
    }

    function _redirectToLandingPre() {
        print_r('_redirectToLandingPre');
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'landingpre'
        ));
    }

    function _redirectToPopup() {
        return $this->redirect(array(
                    'controller' => 'firsts',
                    'action' => 'index'
        ));
    }

    function _redirectToDealsIndexFromSubscription($landing = '') {
        if (!empty($landing)) {
            return $this->redirect(array(
                        'controller' => 'deals',
                        'city' => $this->city_slug,
                        'action' => 'index?from=subscription&subscription_id=' . $this->Subscription->getLastInsertID() . '&landing=' . $landing
            ));
        } else {
            return $this->redirect(array(
                        'controller' => 'deals',
                        'city' => $this->city_slug,
                        'action' => 'index?from=subscription&subscription_id=' . $this->Subscription->getLastInsertID()
            ));
        }
    }

    function _redirectToDealsIndex($landing = '') {
        if (!empty($landing)) {
            return $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index?landing=' . $landing
            ));
        } else {
            return $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index'
            ));
        }
    }

    function redirectAjax($url) {
        echo '<script>';
        echo 'top.document.location.href="' . Router::url($url) . '";';
        echo '</script>';
        die;
    }

    function _redirectAjaxToDealsIndexFromSubscription() {
        return $this->redirectAjax(array(
                    'controller' => 'deals',
                    'action' => 'index?from=subscription',
                    'city' => $this->city_slug
        ));
    }

    function _facebook_subscribe(&$_DATA) {
        Debugger::log(sprintf('SUBSCRIPTIONS_CONTROLLER::_facebook_subscribe - INICIO'), LOG_DEBUG);
        $user = null;
        try {
            $fb_user = $this->facebook->api('/me?fields=id,email,first_name,middle_name,last_name,about');
            $this->data['Subscription']['email'] = $fb_user["email"];
        } catch (FacebookApiException $e) {
            Debugger::log($e, LOG_DEBUG);
            $user = null;
        }
    }

    private function verificaremail($email) {
        return ereg("^([a-zA-Z0-9._]+)@([a-zA-Z0-9.-]+).([a-zA-Z]{2,4})$", $email);
    }

    function add() {
        Debugger::log('SUBSCRIPTIONS_CONTROLLER::add() INICIO', LOG_DEBUG);
        if (!isset($_GET['required_facebook_subscribe'])) {
            Debugger::log('SUBSCRIPTIONS_CONTROLLER::no se está suscribiendo por facebook', LOG_DEBUG);
        } else {
            Debugger::log('SUBSCRIPTIONS_CONTROLLER::SI se está suscribiendo por facebook', LOG_DEBUG);
            if (!empty($_GET['subscription__city_id'])) {
                $this->data['Subscription']['city_id'] = $_GET['subscription__city_id'];
            }
            if (!empty($_GET['subscription__from'])) {
                $this->data['Subscription']['subscription__from'] = $_GET['subscription__from'];
            }
            if (!empty($_GET['subscription__utm_source'])) {
                $this->data['Subscription']['subscription__utm_source'] = $_GET['subscription__utm_source'];
            }
            if (!empty($_GET['subscription__landing'])) {
                $this->data['Subscription']['subscription__landing'] = $_GET['subscription__landing'];
            }
            $this->_facebook_subscribe($this->data);
        }
        if (!empty($_POST['subscription__from']))
            $this->data['Subscription']['from'] = $_POST['subscription__from'];
        if (!empty($_POST['subscription__email']))
            $this->data['Subscription']['email'] = $_POST['subscription__email'];
        if (!empty($_POST['subscription__city_id']))
            $this->data['Subscription']['city_id'] = $_POST['subscription__city_id'];
        if (!empty($_POST['subscription__campaign']))
            $this->data['Subscription']['campaign'] = $_POST['subscription__campaign'];
        if (isset($_POST['subscription__utm_source']))
            $this->data['Subscription']['utm_source'] = $_POST['subscription__utm_source'];
        if (isset($_POST['subscription__utm_mediun']))
            $this->data['Subscription']['utm_mediun'] = $_POST['subscription__utm_medium'];
        if (isset($_POST['subscription__utm_term']))
            $this->data['Subscription']['utm_term'] = $_POST['subscription__utm_term'];
        if (isset($_POST['subscription__utm_content']))
            $this->data['Subscription']['utm_content'] = $_POST['subscription__utm_content'];
        if (isset($_POST['subscription__utm_campaign']))
            $this->data['Subscription']['utm_campaign'] = $_POST['subscription__utm_campaign'];
        if (!empty($_POST['subscription__landing']))
            $this->data['Subscription']['landing'] = $_POST['subscription__landing'];
        $this->set('certificaPath', 'ClubCupon/subscription/add');
        AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Add Subscription');
        if (!empty($this->data)) {
            if (isset($_POST['data']['Subscription']['captcha'])) {
                $captchaValido = $this->isValidCaptcha($_POST['data']['Subscription']['captcha']);
                if (!$captchaValido) {
                    $this->Session->setFlash(Configure::read('web.message.subscriptionFailValidationCaptcha'), 'default', null, 'error');
                    if ($from == 'landing') {
                        return $this->_redirectToLandingPre();
                    } else if ($from === 'popup') {
                        return $this->_redirectToPopup();
                    } else {
                        return $this->_redirectToDealsIndex($this->data['Subscription']['landing']);
                    }
                }
            }
            if (!empty($_POST['external_post']) && $_POST['external_post'] == '1') {
                $_POST['Email'] = $_POST['data']['Subscription']['email'];
                $post_data = $_POST;
                $post_url = $_POST['external_post_url'];
                $this->__curlPostData($post_data, $post_url);
            }
            $this->data['Subscription']['subscription_ip'] = $this->RequestHandler->getClientIP();
            $from = isset($this->data['Subscription']['from']) ? $this->data['Subscription']['from'] : false;
            $this->data['Subscription']['email'] = trim($this->data['Subscription']['email']);
            list($email_user, $email_domain) = explode('@', $this->data['Subscription']['email']);
            if (in_array($email_domain, Configure::read('Email.blacklisted_domains'))) {
                $this->Session->setFlash(__l('Subscription could not be added. Please, try again.'), 'default', null, 'error');
                if ($from == 'landing') {
                    return $this->_redirectToLandingPre();
                } else if ($from === 'popup') {
                    return $this->_redirectToPopup();
                } else {
                    return $this->_redirectToDealsIndex($this->data['Subscription']['landing']);
                }
            }
            $subscription = $this->Subscription->findByEmailAndCityId($this->data['Subscription']['email'], $this->data['Subscription']['city_id']);
            $this->data['Subscription']['user_id'] = $this->Auth->user('id');
            $city = $this->City->findIsAprovedById($this->data['Subscription']['city_id']);
            $this->Cookie->write('city_slug', $city['City']['slug']);
            $wCity = $this->WebCity->findBySlug($city['City']['slug']);
            if ($wCity && !$wCity['WebCity']['is_group']) {
                $this->Cookie->write('geo_city_slug', $city['City']['slug']);
            }
            $this->city_slug = $city['City']['slug'];
            if (empty($subscription)) {
                $this->Subscription->create();
                if ($this->Subscription->isFake($this->data['Subscription']['email'])) {
                    $this->Session->setFlash("Te suscribiste con &eacute;xito a Club Cup&oacute;n.", 'default', null, 'success');
                    if ($from == 'popup') {
                        return $this->_redirectAjaxToDealsIndexFromSubscription();
                    }
                    return $this->_redirectToDealsIndexFromSubscription($this->data['Subscription']['landing']);
                } else {
                    $result = false;
                    if ($this->verificaremail($this->data['Subscription']['email'])) {
                        $result = $this->Subscription->save($this->data);
                    }
                    if ($result) {
                        $city = $this->Subscription->City->findById($this->data['Subscription']['city_id']);
                        $this->_subscribeWelcomeMail($this->data['Subscription']['email'], $city);
                        $this->Session->setFlash("Te suscribiste con &eacute;xito a Club Cup&oacute;n.", 'default', null, 'success');
                        $this->Cookie->write('CakeCookie[first_time_user]', 'no', true, '+5 months');
                        if ($from == 'popup') {
                            return $this->_redirectAjaxToDealsIndexFromSubscription();
                        }
                        return $this->_redirectToDealsIndexFromSubscription($this->data['Subscription']['landing']);
                    } else {
                        $this->Session->setFlash(__l('Subscription could not be added. Please, try again.'), 'default', null, 'error');
                        if ($from == 'landing') {
                            return $this->_redirectToLandingPre();
                        } else if ($from === 'popup') {
                            return $this->_redirectToPopup();
                        } else {
                            return $this->_redirectToDealsIndex($this->data['Subscription']['landing']);
                        }
                    }
                }
            } elseif (!$subscription['Subscription']['is_subscribed']) {
                $this->data['Subscription']['is_subscribed'] = 1;
                $this->data['Subscription']['id'] = $subscription['Subscription']['id'];
                $this->Subscription->save($this->data);
                $this->Session->setFlash(__l('Subscription has been added. Thanks to subscribe again.'), 'default', null, 'success');
                $this->Cookie->write('CakeCookie[first_time_user]', 'no', true, '+5 months');
                $this->Cookie->write('CakeCookie[fullscreenClose]', '1', true, '+5 months');
                if ($from == 'popup') {
                    return $this->_redirectAjaxToDealsIndexFromSubscription();
                }
                return $this->_redirectToDealsIndexFromSubscription($this->data['Subscription']['landing']);
            } else {
                $this->Cookie->write('first_time_user', 'no', true, '+5 months');
                $this->Cookie->write('CakeCookie[fullscreenClose]', '1', true, '+5 months');
                $this->Session->setFlash('Ya estás suscripto con ese mail', 'default', null, 'error');
                return $this->_redirectToDealsIndex($this->data['Subscription']['landing']);
            }
        }
        $slugToSearchFor = isset($this->params['named']['city']) ? $this->params['named']['city'] : Configure::read('Actual.city_slug');
        $city = $this->Subscription->City->findBySlug($slugToSearchFor);
        $this->data['Subscription']['city_id'] = $city['City']['id'];
        $cities = $this->Subscription->City->findListIsAproved();
        $this->set('campaign', isset($_GET['campaign']) ? $_GET['campaign'] : '');
        $this->set(compact('cities'));
        $this->set('pageTitle', __l('Deal of the Day'));
    }

    function unsubscribe($id = null) {
        $this->set('certificaPath', 'ClubCupon/subscription/unsubscribe');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $subscription = $this->Subscription->find('first', array(
            'conditions' => array(
                'Subscription.id' => $id
            ),
            'fields' => array(
                'Subscription.id',
                'Subscription.city_id'
            ),
            'recursive' => - 1
        ));
        $ciudad = $this->Subscription->City->findById($subscription['Subscription']['city_id']);
        $this->set('city_name', $ciudad['City']['name']);
        $this->set('city_slug', $ciudad['City']['slug']);
        if (!empty($this->data)) {
            if (empty($subscription)) {
                $this->Session->setFlash(__l('Please provide a subscribed email'), 'default', null, 'error');
            } else {
                $this->data['Subscription']['is_subscribed'] = 0;
                if ($this->Subscription->save($this->data)) {
                    $this->Session->setFlash('Cancelaste la suscripción a la lista de suscriptores', 'default', null, 'success');
                    $this->redirect(array(
                        'controller' => 'deals',
                        'city' => $this->city_slug,
                        'action' => 'index?from=subscription'
                    ));
                }
            }
        } else {
            $this->data['Subscription']['id'] = $id;
        }
    }

    function unsubscribe_all($id = null) {
        $this->set('certificaPath', 'ClubCupon/subscription/unsubscribe');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            $subscriptionEmail = $this->Subscription->find('list', array(
                'conditions' => array(
                    'Subscription.id' => $id
                ),
                'fields' => array(
                    'Subscription.email'
                ),
                'recursive' => - 1
            ));
            if ($this->Subscription->updateAll(array(
                        'Subscription.is_subscribed' => 0
                            ), array(
                        'Subscription.email' => $subscriptionEmail
                    ))) {
                $this->Session->setFlash('Has cancelado todas tus suscripciones', 'default', null, 'success');
                $this->redirect(array(
                    'controller' => 'deals',
                    'city' => $this->city_slug,
                    'action' => 'index?from=subscription'
                ));
            }
        } else {
            $this->data['Subscription']['id'] = $id;
        }
    }

    function find_total_available_balance_by_email() {
        $this->RequestHandler->respondAs('text/x-json');
        $this->autoRender = false;
        $data = array(
            'success' => 1
        );
        try {
            $data = array_merge($data, $_POST);
            $totalAvailableBalanceAmount = $this->_findTotalAvailableBalanceByEmail($_POST['email']);
            $data['amount'] = is_null($totalAvailableBalanceAmount[0]['total_amount']) ? 0 : $totalAvailableBalanceAmount[0]['total_amount'];
        } catch (Exception $e) {
            $data['success'] = 0;
            $data['error_message'] = $e->getMessage();
        }
        echo json_encode($data);
    }

    function _findTotalAvailableBalanceByEmail($email) {
        if (empty($email)) {
            throw new InvalidArgumentException("Email must not be empty");
        }
        return $this->Subscription->findTotalAvailableBalanceAmountByEmail($email);
    }

    function admin_index() {
        AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Subscriptions');
        $this->_redirectGET2Named(array(
            'q'
        ));
        $conditions = array();
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $conditions[] = sprintf('Subscription.city_id in (SELECT city_id from city_perms where city_perms.user_id = %d)', $this->Auth->user('id'));
        }
        if (!empty($this->data['Subscription']['city_id'])) {
            $conditions['Subscription.city_id'] = $this->data['Subscription']['city_id'];
        }
        $param_string = '';
        $param_string.=!empty($this->params['named']['type']) ? '/type:' . $this->params['named']['type'] : $param_string;
        if (!empty($this->data['Subscription']['type'])) {
            $this->params['named']['type'] = $this->data['Subscription']['type'];
        }
        if (empty($this->data['Subscription']['q']) && !empty($this->params['named']['q'])) {
            $this->data['Subscription']['q'] = $this->params['named']['q'];
        }
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'subscribed') {
            $this->data['Subscription']['type'] = $this->params['named']['type'];
            $conditions['Subscription.is_subscribed'] = 1;
            $this->pageTitle = __l('Subscribed Users');
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'unsubscribed') {
            $this->data['Subscription']['type'] = $this->params['named']['type'];
            $conditions['Subscription.is_subscribed'] = 0;
            $this->pageTitle = __l('Unsubscribed Users');
        }
        if (isset($this->data['Subscription']['q']) && !empty($this->data['Subscription']['q'])) {
            $this->params['named']['q'] = $this->data['Subscription']['q'];
            $this->pageTitle.= sprintf(__l(' - Search - %s'), $this->data['Subscription']['q']);
        }
        $this->Subscription->recursive = 0;
        $this->paginate = array(
            'conditions' => $conditions
        );
        if (!empty($this->data['Subscription']['q'])) {
            $this->paginate['search'] = $this->data['Subscription']['q'];
        }
        $this->set('subscriptions', $this->paginate());
        $subscribed_conditions = array(
            'Subscription.is_subscribed' => 1
        );
        $unsubscribed_conditions = array(
            'Subscription.is_subscribed' => 0
        );
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $unsubscribed_conditions[] = $subscribed_conditions[] = sprintf('Subscription.city_id in (SELECT city_id from city_perms where city_perms.user_id = %d)', $this->Auth->user('id'));
        }
        $this->set('subscribed', "");
        $this->set('unsubscribed', "");
        $this->set('pageTitle', $this->pageTitle);
        $moreActions = $this->Subscription->moreActions;
        $this->set(compact('moreActions'));
        $this->set('param_string', $param_string);
    }

    function admin_update() {
        AppModel::setDefaultDbConnection('master');
        $this->autoRender = false;
        if (!empty($this->data['Subscription'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $userIds = array();
            foreach ($this->data['Subscription'] as $subscription_id => $is_checked) {
                if ($is_checked['id']) {
                    $subscriptionIds[] = $subscription_id;
                }
            }
            if ($actionid && !empty($subscriptionIds)) {
                if ($actionid == ConstMoreAction::Delete) {
                    $this->Subscription->deleteAll(array(
                        'Subscription.id' => $subscriptionIds
                    ));
                    $this->Session->setFlash(__l('Checked subscriptions has been deleted'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::UnSubscripe) {
                    $this->Subscription->recursive = - 1;
                    $this->Subscription->updateAll(array(
                        'Subscription.is_subscribed' => 0
                            ), array(
                        'Subscription.id' => $subscriptionIds
                    ));
                    $this->Session->setFlash(__l('Checked subscriptions has been un subscribed'), 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function admin_delete($id = null) {
        AppModel::setDefaultDbConnection('master');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Subscription->del($id)) {
            $this->Session->setFlash(__l('Subscription deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function admin_export() {
        AppModel::setDefaultDbConnection('master');
        Configure::write('debug', 0);
        $conditions = array();
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $conditions[] = sprintf('Subscription.city_id in (SELECT city_id from city_perms where city_perms.user_id = %d)', $this->Auth->user('id'));
        }
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'subscribed') {
            $this->data['Subscription']['type'] = $this->params['named']['type'];
            $conditions['Subscription.is_subscribed'] = 1;
            $this->pageTitle = __l('Subscribed Users');
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'unsubscribed') {
            $this->data['Subscription']['type'] = $this->params['named']['type'];
            $conditions['Subscription.is_subscribed'] = 0;
            $this->pageTitle = __l('Unsubscribed Users');
        }
        if (isset($this->data['Subscription']['q']) && !empty($this->data['Subscription']['q'])) {
            $this->pageTitle.= sprintf(__l(' - Search - %s'), $this->data['Subscription']['q']);
        }
        $Subscribers = $this->Subscription->find('all', array(
            'conditions' => $conditions,
            'order' => array(
                'Subscription.id' => 'desc'
            ),
            'recursive' => 0
        ));
        if (!empty($Subscribers)) {
            foreach ($Subscribers as $Subscriber) {
                $data[]['Subscriber'] = array(
                    'SubscribedOn' => $Subscriber['Subscription']['created'],
                    'Email' => $Subscriber['Subscription']['email'],
                    'City' => $Subscriber['City']['name']
                );
            }
        }
        $this->set('data', $data);
    }

    function __curlPostData($data, $url) {
        $row_arr = array();
        $row_arr['Email'] = $data['Email'];
        $row_arr['Ciudad'] = $data['Ciudad'];
        $ch2 = curl_init();
        if (curl_error($ch2) != "") {
            echo "Error: $error\n";
        }
        curl_setopt($ch2, CURLOPT_URL, $url);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_USERAGENT, getenv("HTTP_USER_AGENT"));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, http_build_query($row_arr));
        curl_exec($ch2);
        curl_close($ch2);
    }

    function manage_subscriptions() {
        $excludedSlugsForSubscriptionsManagement = array(
            'mcdonalds'
        );
        $user_id = ($this->Auth->user('id'));
        if (empty($user_id)) {
            return $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'login'
            ));
        }
        $this->set('certificaPath', 'ClubCupon/cpanel/subscriptions');
        $active_cities = $this->City->findActiveCitiesForSubscription();
        $active_groups = $this->City->findActiveGroupsForSubscription();
        foreach ($active_groups as $idx => $city) {
            if (in_array($city['City']['slug'], $excludedSlugsForSubscriptionsManagement)) {
                unset($active_groups[$idx]);
            }
        }
        $subscribed_cities = $this->Subscription->findCitiesSubscribedByUserId($user_id);
        $user_data = $this->User->getUserData($user_id);
        $user_email = $user_data['User']['email'];
        if (!empty($this->data)) {
            $suscripciones_enviadas = $this->data['Subscription'];
            $suscripciones_activas_ordenadas = $this->formatSubscriptionsArrayForMatch($subscribed_cities, $user_email);
            $changes = false;
            foreach ($suscripciones_enviadas as $se_city_id => $se_status) {
                foreach ($suscripciones_activas_ordenadas as $subsc_id => $city_id) {
                    if ($se_status == 0 && $se_city_id == $city_id && !strpos($subsc_id, 'alt')) {
                        $this->Subscription->unsubscribeToCity($subsc_id);
                        $changes = true;
                    }
                    if ($se_city_id == $subsc_id && $se_status == 0) {
                        $id_limpio = str_replace("alt", "", $subsc_id);
                        $this->Subscription->unsubscribeToCity($id_limpio);
                        $changes = true;
                    }
                }
                if ($se_status == 1 && !strpos($se_city_id, 'alt')) {
                    $datos['user_id'] = $this->Auth->user('id');
                    $datos['city_id'] = $se_city_id;
                    $datos['is_subscribed'] = 1;
                    $datos['is_voluntary'] = 1;
                    $datos['email'] = $this->Auth->user('email');
                    $datos['subscription_ip'] = $this->RequestHandler->getClientIP();
                    $subscription_exists = $this->Subscription->findSubscriptionIdByUserIdAndUserEmailAndCityId($datos['user_id'], $datos['email'], $datos['city_id']);
                    if (!empty($subscription_exists) && $subscription_exists['Subscription']['is_subscribed'] == 0) {
                        $this->Subscription->subscribeToCity($subscription_exists['Subscription']['id']);
                        $changes = true;
                    } else if (empty($subscription_exists)) {
                        $this->Subscription->generateNewSubscriptionToCity($datos);
                        $changes = true;
                    }
                }
            }
            $subscribed_cities = $this->Subscription->findCitiesSubscribedByUserId($user_id);
            if ($changes) {
                $this->Session->setFlash('Has actualizado tus suscripciones', 'default', null, 'success');
            } else {
                $this->Session->setFlash('No se han registrado cambios en tus suscripciones', 'default', null, 'success');
            }
        }
        foreach ($active_cities as $idx => $city) {
            foreach ($subscribed_cities as $subsc_city) {
                if ($city['City']['id'] == $subsc_city['City']['id'] && $subsc_city['Subscription']['email'] == $user_email) {
                    $active_cities[$idx]['City']['checked'] = true;
                }
            }
        }
        foreach ($active_groups as $idx => $city) {
            foreach ($subscribed_cities as $subsc_city) {
                if ($city['City']['id'] == $subsc_city['City']['id'] && $subsc_city['Subscription']['email'] == $user_email) {
                    $active_groups[$idx]['City']['checked'] = true;
                }
            }
        }
        foreach ($subscribed_cities as $subsc_city) {
            if ($subsc_city['Subscription']['email'] != $user_email) {
                $subsc_city['City']['name'] = $subsc_city['City']['name'] . ' ( ' . $subsc_city['Subscription']['email'] . ' ) ';
                $subsc_city['City']['id'] = $subsc_city['Subscription']['id'] . 'alt';
                $subsc_city['City']['checked'] = true;
                if ($subsc_city['City']['is_group'] == 1) {
                    array_push($active_groups, $subsc_city);
                } else {
                    array_push($active_cities, $subsc_city);
                }
            }
        }
        $this->set('active_groups', $active_groups);
        $this->set('active_cities', $active_cities);
        $this->set('user_id', $user_id);
    }

    function formatSubscriptionsArrayForMatch($subscribed_cities, $user_email) {
        $suscripciones_activas_ordenadas = array();
        foreach ($subscribed_cities as $idx => $sCity) {
            $scValue = $sCity['Subscription']['city_id'];
            if ($sCity['Subscription']['email'] == $user_email) {
                $scKey = $sCity['Subscription']['id'];
            } else {
                $scKey = $sCity['Subscription']['id'] . 'alt';
            }
            $suscripciones_activas_ordenadas[$scKey] = $scValue;
        }
        return $suscripciones_activas_ordenadas;
    }

    public function facebook() {
        $this->layout = 'clean_theme_popup';
        try {
            if (!empty($_GET['city_id'])) {
                $dir_ip = $this->RequestHandler->getClientIP();
                $city_id = $_GET['city_id'];
                $email = $this->getEmailFromFacebook(Configure::read('facebook.fb_api_key'), Configure::read('facebook.fb_secrect_key'));
                $user = $this->User->findByEmail($email);
                $suscription = $this->Subscription->findByEmailAndCityId($email, $city_id);
                if (!empty($user))
                    $user_id = $user['User']['id'];
                if (empty($suscription)) {
                    $this->Subscription->suscribeEmail($email, $city_id, $user_id, $dir_ip);
                } else {
                    if (!$this->Subscription->isSuscriptionActive($suscription)) {
                        $this->Subscription->updateEmail($email, $city_id, $user_id, $dir_ip);
                    } else {
                        throw new Exception('Usuario Ya se encuentra suscrito');
                    }
                }
                $this->Session->setFlash('Has actualizado tus suscripciones', 'default', null, 'success');
            } else {
                $this->layout = 'clean_theme_popup';
            }
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'default', null, 'error');
        }
    }

    private function getEmailFromFacebook($appID, $appSecret) {
        App::import('Vendor', Configure::read('facebook_lib.path'));
        $this->facebook = new Facebook(array(
            'appId' => $appID,
            'secret' => $appSecret,
            'cookie' => true
        ));
        $fb_user = $this->facebook->api('/me?fields=id,email');
        return $fb_user['email'];
    }

}
