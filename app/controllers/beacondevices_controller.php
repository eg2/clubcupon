<?php

class BeacondevicesController extends AppController {

    var $name = 'Beacondevices';
    var $uses = array(
        'beacon.BeaconDevice'
    );
    
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'BeaconDevice.id' => 'desc'
        )
    );

    function admin_index() {
        $this->pageTitle = 'Beacons - dispositivos';
        $beacondevices = $this->paginate();
        $size = count($beacondevices);
        /*for ($a = 0; $a < $size; $a++) {
            $calendars[$a]['AccountingCalendar']['enabledActions'] = $this->_returnEnabledActionsByStatus($calendars[$a]['AccountingCalendar']);
        }*/
        $this->set('beacondevices', $beacondevices);
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->validateDeviceFields($this->data)) {
            	
            	$this->BeaconDevice->set('name',$this->data['Beacondevice']['name']);
            	$this->BeaconDevice->set('mac', $this->data['Beacondevice']['name']);
            	$this->BeaconDevice->set('uuid',$this->data['Beacondevice']['name']);
            	$this->BeaconDevice->set('major',$this->data['Beacondevice']['major']);
            	$this->BeaconDevice->set('minor',$this->data['Beacondevice']['minor']);
            	 
                $this->BeaconDevice->set('created_by', $this->Auth->user('id'));
          
                if ($this->BeaconDevice->save()) {
                    $this->Session->setFlash('Dispositivo guardado', true);
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash('No se pudo guardar el registro', true);
                }
            } else {
                $this->Session->setFlash('Hubo errores en la validacion', true);
            }
        }
    }

    function admin_edit($id) {
        if (!empty($this->data)) {
            $status = $this->AccountingCalendar->read('status', $id);
            if ($this->validateDeviceFields($this->data)) {
                $this->AccountingCalendar->id = $id;
                $this->AccountingCalendar->set($this->data);
                $this->AccountingCalendar->set('modified_by', $this->Auth->user('id'));
                $this->AccountingCalendar->set('until', $this->data['AccountingCalendar']['until'] . ' 23:59:59');
                if ($this->AccountingCalendar->save()) {
                    $this->Session->setFlash('Registro editado', true);
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash('No se pudo editar el registro', true);
                }
            } else {
                $this->Session->setFlash('Hubo errores en la validacion', true);
            }
        }
        $this->data = $this->AccountingCalendar->read(null, $id);
    }

    function admin_accept($id) {
        if (!empty($id)) {
            $calendarToSetAsAccepted = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->calendarIsApprovableOrRejectable($calendarToSetAsAccepted)) {
                if ($this->updateAsAccepted($calendarToSetAsAccepted)) {
                    $this->Session->setFlash('Calendario pasado a estado aceptado', true);
                } else {
                    $this->Session->setFlash('No se pudo aceptar el calendario', true);
                }
            } else {
                $this->Session->setFlash('No se puede aceptar el calendario', true);
            }
        } else {
            $this->Session->setFlash('No se ha seleccionado ningun Calendario', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_reject($id) {
        if (!empty($id)) {
            $calendarToSetAsRejected = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->calendarIsApprovableOrRejectable($calendarToSetAsRejected)) {
                if ($this->updateAsRejected($calendarToSetAsRejected)) {
                    $this->Session->setFlash('Calendario pasado a estado rechazado', true);
                } else {
                    $this->Session->setFlash('No se pudo rechazar el calendario', true);
                }
            } else {
                $this->Session->setFlash('No se puede rechazar el calendario', true);
            }
        } else {
            $this->Session->setFlash('No se ha seleccionado ningun Calendario', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_delete($id) {
        if (!empty($id)) {
            $calendarToSetAsDeleted = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->setCalendarAsDeleted($calendarToSetAsDeleted)) {
                $this->Session->setFlash('Calendario eliminado', true);
            } else {
                $this->Session->setFlash('No se pudo eliminar el calendario', true);
            }
        } else {
            $this->Session->setFlash('Debe seleccionarse un calendario a eliminar', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function updateAsAccepted($calendar) {
        $fields = array(
            'AccountingCalendar.status' => "'" . AccountingCalendar::STATUS_ACCEPTED . "'",
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function updateAsRejected($calendar) {
        $fields = array(
            'AccountingCalendar.status' => "'" . AccountingCalendar::STATUS_REJECTED . "'",
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function setCalendarAsDeleted($calendar) {
        $fields = array(
            'AccountingCalendar.deleted' => 1,
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function calendarIsApprovableOrRejectable($calendar) {
        return $calendar['AccountingCalendar']['status'] == AccountingCalendar::STATUS_PRE_ACCOUNTED;
    }

    function _returnEnabledActionsByStatus($calendar) {
        $enabledActions = array();
        if ($calendar['status'] == AccountingCalendar::STATUS_NEW) {
            $enabledActions = array(
                'edit',
                'delete'
            );
        }
        if ($calendar['status'] == AccountingCalendar::STATUS_PRE_ACCOUNTED) {
            $enabledActions = array(
                'accept'
            );
        }
        if ($calendar['status'] == AccountingCalendar::STATUS_ACCOUNTED) {
            $enabledActions = array(
                'edit'
            );
        }
        return $enabledActions;
    }

    function _isUntilDateAfterOrSameasSinceDate($calendar) {
        return strtotime($calendar['AccountingCalendar']['until']) >= strtotime($calendar['AccountingCalendar']['since']);
    }

    function _isExcecutionDateAfterNow($calendar) {
        return strtotime($calendar['AccountingCalendar']['execution']) > strtotime(date('Y-m-d H:i:s'));
    }

    function _isExcecutionDateAfterUntilDate($calendar) {
        return strtotime($calendar['AccountingCalendar']['execution']) > strtotime($calendar['AccountingCalendar']['until']);
    }

    function _areDatesOfCalendarNotEmpty($calendar) {
        return !empty($calendar['AccountingCalendar']['since']) && !empty($calendar['AccountingCalendar']['until']) && !empty($calendar['AccountingCalendar']['execution']);
    }

    function validateDeviceFields($calendar) {
        return true;
    }

}
