<?php

class UsersController extends AppController {

    var $name = 'Users';
    var $components = array(
        'Email',
        'now.Mail'
    );
    var $uses = array(
        'User',
        'UserLogin',
        'Company',
        'CityPerms',
        'Subscription',
        'EmailTemplate'
    );
    var $helpers = array(
        'Csv',
        'Gateway'
    );

    function beforeFilter() {
        $this->set('hideSubscription', true);
        $securityDisabled = array(
            'register',
            'login',
            'buy_prelogin',
            'reset'
        );
        if (in_array($this->action, $securityDisabled)) {
            $this->Security->validatePost = false;
        }
        $this->Security->disabledFields = array(
            'City.id',
            'City.name',
            'State.id',
            'State.name',
            'Company.name',
            'Company.phone',
            'Company.url',
            'Company.address1',
            'Company.address2',
            'Company.country_id',
            'Company.zip',
            'Company.latitude',
            'Company.longitude',
            'User.referer_name',
            'UserProfile.country_id',
            'UserProfile.state_id',
            'UserProfile.city_id',
            'User.geobyte_info',
            'User.maxmind_info',
            'User.referred_by_user_id',
            'User.referred_date',
            'User.type',
            'User.is_agree_terms_conditions',
            'User.is_agree_daily_subscriptions',
            'User.country_iso_code',
            'User.is_requested',
            'User.is_remember'
        );
        $this->User->UserLogin = new UserLogin;
        parent::beforeFilter();
    }

    function buy_prelogin($deal_id = null) {
        $this->autoRender = false;
        $email = $this->data['User']['email2check'];
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.email = ' => $email
            ),
            'fields' => array(
                'User.id'
            ),
            'recursive' => - 1
        ));
        $existe = true;
        if (!$user) {
            $existe = false;
        }
        $data = Array(
            "email" => $email,
            "existe" => $existe
        );
        echo json_encode($data);
    }

    function view($username = null) {
        $this->pageTitle = __l('User');
        if (is_null($username)) {
            $this->cakeError('error404');
        }
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.username = ' => $username
            ),
            'contain' => array(
                'UserProfile' => array(
                    'fields' => array(
                        'UserProfile.created',
                        'UserProfile.first_name',
                        'UserProfile.last_name',
                        'UserProfile.middle_name',
                        'UserProfile.about_me',
                        'UserProfile.dob',
                        'UserProfile.address',
                        'UserProfile.zip_code',
                        'UserProfile.paypal_account',
                        'UserProfile.gender'
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.name'
                        )
                    ),
                    'Language' => array(
                        'fields' => array(
                            'Language.id',
                            'Language.name'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.name'
                        )
                    ),
                    'Country' => array(
                        'fields' => array(
                            'Country.name'
                        )
                    )
                ),
                'UserAvatar' => array(
                    'fields' => array(
                        'UserAvatar.id',
                        'UserAvatar.dir',
                        'UserAvatar.filename',
                        'UserAvatar.width',
                        'UserAvatar.height'
                    )
                )
            ),
            'fields' => array(
                'User.id',
                'User.username',
                'User.email',
                'User.user_type_id',
                'User.created'
            ),
            'recursive' => 2
        ));
        $statistics = array();
        $statistics['referred_users'] = $this->User->find('count', array(
            'conditions' => array(
                'User.Referred_by_user_id' => $user['User']['id']
            ),
            'recursive' => - 1
        ));
        $statistics['deal_purchased'] = $this->User->DealUser->find('count', array(
            'conditions' => array(
                'DealUser.user_id' => $user['User']['id'],
                'DealUser.is_gift' => 0
            ),
            'recursive' => - 1
        ));
        $statistics['gift_sent'] = $this->User->GiftUser->find('count', array(
            'conditions' => array(
                'GiftUser.user_id' => $user['User']['id']
            ),
            'recursive' => - 1
        ));
        $statistics['gift_received'] = $this->User->GiftUser->find('count', array(
            'conditions' => array(
                'GiftUser.friend_mail' => $user['User']['email']
            ),
            'recursive' => - 1
        ));
        if (ConstUserFriendType::IsTwoWay) {
            $statistics['user_friends'] = $this->User->UserFriend->find('count', array(
                'conditions' => array(
                    'UserFriend.user_id' => $user['User']['id'],
                    'UserFriend.friend_status_id' => 2,
                    'UserFriend.is_requested' => array(
                        0,
                        1
                    )
                ),
                'recursive' => - 1
            ));
        } else {
            $statistics['user_friends'] = $this->User->UserFriend->find('count', array(
                'conditions' => array(
                    'UserFriend.user_id' => $user['User']['id'],
                    'UserFriend.friend_status_id' => 2,
                    'UserFriend.is_requested' => 0
                ),
                'recursive' => - 1
            ));
        }
        if (empty($user)) {
            $this->cakeError('error404');
        }
        $friend = $this->User->UserFriend->find('first', array(
            'conditions' => array(
                'UserFriend.user_id' => $this->Auth->user('id'),
                'UserFriend.friend_user_id' => $user['User']['id'],
                'UserFriend.friend_status_id' => ConstUserFriendStatus::Approved
            ),
            'recursive' => - 1
        ));
        $this->set('statistics', $statistics);
        $this->set('friend', $friend);
        $this->data['UserComment']['user_id'] = $user['User']['id'];
        $this->User->UserView->create();
        $this->data['UserView']['user_id'] = $user['User']['id'];
        $this->data['UserView']['viewing_user_id'] = $this->Auth->user('id');
        $this->data['UserView']['ip'] = $this->RequestHandler->getClientIP();
        $this->User->UserView->save($this->data);
        $this->pageTitle.= ' - ' . $username;
        $this->set('user', $user);
    }

    function registerok($now = false) {
        $this->set('certificaPath', 'ClubCupon/user/registerok');
        $this->set('now', $now);
    }

    function registernow() {
        $this->set('certificaPath', 'ClubCupon/user/registernow');
    }

    function registernowok() {
        $this->set('certificaPath', 'ClubCupon/user/registernowok');
    }

    function register($type = null) {
        $this->set('certificaPath', 'ClubCupon/user/register');
        $this->pageTitle = "Registro " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Registrarse en " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        $fbuser = $this->Session->read('fbuser');
        $fromPopup = isset($this->data['User']['from_popup']) && !empty($this->data['User']['from_popup']);
        if (!empty($fbuser['fb_user_id'])) {
            $this->data['User']['username'] = $fbuser['username'];
            $this->data['User']['email'] = '';
            $this->data['User']['fb_user_id'] = $fbuser['fb_user_id'];
            $this->Session->del('fbuser');
        }
        if (!empty($this->data)) {
            if (!empty($this->data['User']['type'])) {
                $type = $this->data['User']['type'];
            }
            if (!empty($this->data['City']['name'])) {
                $this->data['UserProfile']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->User->Company->City->findOrSaveAndGetId($this->data['City']['name']);
            }
            if (!empty($this->data['State']['name'])) {
                $this->data['UserProfile']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->User->Company->State->findOrSaveAndGetId($this->data['State']['name']);
            }
            if (!empty($this->data['User']['country_iso_code'])) {
                $this->data['UserProfile']['country_id'] = $this->User->UserProfile->Country->findCountryIdFromIso2($this->data['User']['country_iso_code']);
                if (empty($this->data['UserProfile']['country_id'])) {
                    unset($this->data['UserProfile']['country_id']);
                }
            }
            if ($this->data['User']['username'] == "%") {
                $this->data['User']['username'] = substr(preg_replace(array(
                            "/@/",
                            "/[^0-9A-Za-z]/"
                                        ), array(
                            "AT",
                            ""
                                        ), $this->data['User']['email']), 0, 15) . substr(uniqid(), -5);
                $this->data['UserProfile']['dni'] = '99999999';
            }
            $this->User->set($this->data);
            $this->User->UserProfile->set($this->data);
            $this->User->Company->set($this->data);
            if ($this->User->validates() & $this->User->UserProfile->validates() & $this->User->Company->validates() & $this->User->Company->City->validates() & $this->User->Company->State->validates()) {
                $this->User->create();
                if (!empty($this->data['User']['openid_url']) or ! empty($this->data['User']['fb_user_id'])) {
                    $this->data['User']['password'] = $this->Auth->password($this->data['User']['email'] . Configure::read('Security.salt'));
                    $this->data['User']['is_agree_terms_conditions'] = 1;
                    $this->data['User']['is_email_confirmed'] = 1;
                } else {
                    $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                    $this->data['User']['is_email_confirmed'] = 1;
                }
                $subscriptionTotalAmount = $this->Subscription->findTotalAvailableBalanceAmountByEmail($this->data['User']['email']);
                if (!is_null($subscriptionTotalAmount[0]['total_amount'])) {
                    $this->data['User']['available_balance_amount'] = $subscriptionTotalAmount[0]['total_amount'];
                }
                $this->data['User']['is_active'] = (Configure::read('user.is_admin_activate_after_register')) ? 0 : 1;
                $this->data['User']['user_type_id'] = ConstUserTypes::User;
                if ($this->Session->read('gift_user_id')) {
                    $this->data['User']['gift_user_id'] = $this->Session->read('gift_user_id');
                    $this->Session->del('gift_user_id');
                }
                $this->data['User']['signup_ip'] = $this->RequestHandler->getClientIP();
                if (!empty($type)) {
                    $this->data['User']['user_type_id'] = ConstUserTypes::Company;
                }
                if (isset($this->data['User']['wants_to_be_a_company']) && @$this->data['User']['wants_to_be_a_company'] == 1) {
                    $this->data['User']['user_type_id'] = ConstUserTypes::Company;
                }
                if (Configure::read('user.is_referral_system_enabled')) {
                    $referred_user_email = $this->data['User']['email'];
                    $referral_user_id = ClassRegistry::init('UserReferral')->getReferralUser($referred_user_email);
                    $this->data['User']['referred_by_user_id'] = $referral_user_id['UserReferral']['user_id'];
                    $this->data['User']['referred_date'] = date('Y-m-d H:i:s');
                }
                if ($this->User->save($this->data, false)) {
                    if (isset($this->data['User']['is_agree_daily_subscriptions']) && ($this->data['User']['is_agree_daily_subscriptions']) != '0') {
                        $this->_subscribeForRegister();
                    } else {
                        $this->_subscribeForRegister('0');
                    }
                    if (!empty($type)) {
                        if (!empty($this->data['City']['name'])) {
                            $this->data['Company']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->User->Company->City->findOrSaveAndGetId($this->data['City']['name']);
                            $this->data['UserProfile']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->User->Company->City->findOrSaveAndGetId($this->data['City']['name']);
                        }
                        if (!empty($this->data['State']['name'])) {
                            $this->data['Company']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->User->Company->State->findOrSaveAndGetId($this->data['State']['name']);
                            $this->data['UserProfile']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->User->Company->State->findOrSaveAndGetId($this->data['State']['name']);
                        }
                        $this->data['Company']['user_id'] = $this->User->getLastInsertId();
                        $this->data['Company']['is_online_account'] = 1;
                        $this->data['Company']['is_company_profile_enabled'] = 1;
                        $this->User->Company->create();
                        $this->User->Company->save($this->data['Company']);
                        $company_id = $this->User->Company->getLastInsertId();
                    }
                    $this->data['UserProfile']['user_id'] = $this->User->getLastInsertId();
                    $this->User->UserProfile->create();
                    $this->User->UserProfile->save($this->data);
                    $this->data['UserPermissionPreference']['user_id'] = $this->User->id;
                    $this->User->UserPermissionPreference->create();
                    $this->User->UserPermissionPreference->save($this->data);
                    if (Configure::read('user.is_admin_mail_after_register')) {
                        $emailFindReplace = array(
                            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                            '##USERNAME##' => $this->data['User']['username'],
                            '##SITE_NAME##' => Configure::read('site.name'),
                            '##SIGNUP_IP##' => $this->RequestHandler->getClientIP(),
                            '##EMAIL##' => $this->data['User']['email'],
                            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                        );
                        $email = $this->EmailTemplate->selectTemplate('New User Join');
                        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                        $this->Email->to = Configure::read('site.contact_email');
                        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                    }
                    $this->Auth->login($this->data);
                    $this->Session->setFlash(__l('You have successfully registered with our site.'), 'default', null, 'success');
                    if (!empty($this->data['User']['openid_url']) || !empty($this->data['User']['fb_user_id'])) {
                        if (Configure::read('user.is_welcome_mail_after_register')) {
                            $this->_sendWelcomeMail($this->User->id, $this->data['User']['email'], $this->data['User']['username']);
                        }
                        if ($this->Auth->login($this->data)) {
                            $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                            if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                return $this->redirect(array(
                                            'plugin' => 'now',
                                            'controller' => 'now_registers',
                                            'action' => 'my_company'
                                ));
                            } else {
                                if ($fromPopup) {
                                    $this->_redirectPopupAfterRegistration();
                                } else {
                                    $this->redirect(Router::url('/', true));
                                }
                            }
                        }
                    } else {
                        if (Configure::read('user.is_email_verification_for_register')) {
                            $this->Session->setFlash(__l('You have successfully registered with our site.'), 'default', null, 'success');
                            if ($this->data['User']['user_type_id'] == ConstUserTypes::Company) {
                                $this->_sendActivationMail($this->data['User']['email'], $this->User->id, $this->User->getActivateHash($this->User->id));
                            }
                        }
                    }
                    if (!Configure::read('user.is_email_verification_for_register') and ! Configure::read('user.is_admin_activate_after_register') and Configure::read('user.is_welcome_mail_after_register')) {
                        $this->_sendWelcomeMail($this->User->id, $this->data['User']['email'], $this->data['User']['username']);
                    }
                    if (!Configure::read('user.is_email_verification_for_register') and Configure::read('user.is_auto_login_after_register')) {
                        $this->Session->setFlash(__l('You have successfully registered with our site.'), 'default', null, 'success');
                        if ($this->Auth->login($this->data)) {
                            $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                            if ($this->RequestHandler->isAjax()) {
                                echo 'redirect*' . Router::url('/', true) . $this->data['User']['f'];
                                exit;
                            } else if (!empty($this->data['User']['f'])) {
                                return $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                            }
                            if ($this->data['User']['user_type_id'] == ConstUserTypes::Company) {
                                return $this->redirect(array(
                                            'plugin' => 'now',
                                            'controller' => 'now_registers',
                                            'action' => 'my_company'
                                ));
                            } else {
                                if ($fromPopup) {
                                    return $this->_redirectPopupAfterRegistration();
                                } else {
                                    return $this->redirect(Router::url('/', true));
                                }
                            }
                        }
                    }
                    if ($fromPopup) {
                        $this->_redirectPopupAfterRegistration();
                    }
                    if (@$this->data['User']['wants_to_be_a_company'] == 1) {
                        return $this->redirect(array(
                                    'controller' => 'users',
                                    'action' => 'registernowok'
                        ));
                    } else {
                        return $this->redirect(array(
                                    'controller' => 'users',
                                    'action' => 'registerok',
                                    0
                        ));
                    }
                }
            } else {
                if (empty($this->data['User']['openid_url'])) {
                    $this->Session->setFlash(__l('Your registration process is not completed. Please, try again.'), 'default', null, 'error');
                    if ($fromPopup) {
                        return $this->_redirectPopupAfterRegistration(true);
                    }
                    if (@$this->data['User']['wants_to_be_a_company'] == 1) {
                        echo $this->render('registernow');
                        die();
                    }
                } else {
                    $this->Session->setFlash(__l('OpenID verification is completed successfully. But you have to fill the following required fields to complete our registration process.'), 'default', null, 'error');
                }
            }
        }
        if (!empty($this->params['named']['f'])) {
            $this->data['User']['f'] = $this->params['named']['f'];
        }
        if (!empty($this->params['requested'])) {
            $this->data['User']['is_requested'] = 1;
        }
        unset($this->data['User']['passwd']);
        if ($this->Auth->user()) {
            if ($fromPopup) {
                return $this->_redirectPopupAfterRegistration();
            }
            $this->redirect(Router::url('/', true));
        }
        $countries = $this->User->UserProfile->Country->find('list');
        $this->set('type', $type);
        $this->set(compact('countries'));
        unset($this->data['User']['passwd']);
        unset($this->data['User']['confirm_password']);
        unset($this->data['User']['captcha']);
    }

    function _subscribeForRegister($is_subscribed = '1') {
        $this->data['Subscription']['user_id'] = $this->User->getLastInsertId();
        $this->data['Subscription']['city_id'] = Configure::read('Actual.city_id');
        $this->data['Subscription']['email'] = $this->data['User']['email'];
        $this->data['Subscription']['is_subscribed'] = $is_subscribed;
        $subscription = $this->Subscription->find('first', array(
            'conditions' => array(
                'Subscription.email' => $this->data['User']['email'],
                'Subscription.city_id' => Configure::read('Actual.city_id')
            ),
            'fields' => array(
                'Subscription.id',
                'Subscription.is_subscribed'
            ),
            'recursive' => - 1
        ));
        if (empty($subscription)) {
            $this->Subscription->create();
        } else {
            $this->data['Subscription']['is_subscribed'] = 1;
            $this->data['Subscription']['id'] = $subscription['Subscription']['id'];
        }
        if (!$this->Subscription->isFake($this->data['Subscription']['email'])) {
            $this->Subscription->save($this->data);
        }
    }

    function refer($referrer = null) {
        if (is_null($referrer)) {
            $this->cakeError('error404');
        }
        $cookie_value = $this->Cookie->read('referrer');
        if (!empty($referrer) && (empty($cookie_value) || (!empty($cookie_value) && $cookie_value != $referrer))) {
            $user_refername = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $referrer
                ),
                'recursive' => - 1
            ));
            if (empty($user_refername)) {
                $this->Session->setFlash(__l('Referrer username does not exist.'), 'default', null, 'error');
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'register'
                ));
            } else {
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'register'
                ));
            }
        } else {
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'register'
            ));
        }
    }

    function _openid() {
        $returnTo = Router::url(array(
                    'controller' => 'users',
                    'action' => $this->data['User']['redirect_page']
                        ), true);
        $siteURL = Router::url('/', true);
        if (!empty($this->data)) {
            try {
                $this->Openid->authenticate($this->data['User']['openid'], $returnTo, $siteURL, array(
                    'email',
                    'nickname'
                        ), array());
            } catch (InvalidArgumentException $e) {
                $this->Session->setFlash(__l('Invalid OpenID'), 'default', null, 'error');
            } catch (Exception $e) {
                $this->Session->setFlash(sprintf(__l('%s'), $e->getMessage()));
            }
        }
    }

    function _sendActivationMail($user_email, $user_id, $hash) {
        $array = array(
            'user_email' => $user_email,
            'user_id' => $user_id,
            'hash' => $hash,
            'params' => $this->params
        );
        return $this->Mail->sendActivationMail($array);
    }

    function _sendWelcomeMail($user_id, $user_email, $username) {
        
    }

    function activation($user_id = null, $hash = null) {
        $this->set('certificaPath', 'ClubCupon/user/activationAccount');
        $this->pageTitle = "Activación " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Activación en " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        if (is_null($user_id) or is_null($hash)) {
            $this->cakeError('error404');
        }
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'recursive' => - 1
        ));
        if (empty($user)) {
            $this->Session->setFlash('Solicitud de activaci&oacute;n es inv&acute;lida o ya fue utilizada. Prob&aacute; de nuevo.');
            $this->_homeRedirect();
        }
        if (!$this->User->isValidActivateHash($user_id, $hash)) {
            $hash = $this->User->getActivateHash($user_id);
            $this->Session->setFlash(__l('Invalid activation request'));
            $this->set('show_resend', 1);
            $resend_url = Router::url(array(
                        'controller' => 'users',
                        'action' => 'resend_activation',
                        $user_id,
                        $hash
                            ), true);
            $this->set('resend_url', $resend_url);
        } else {
            $this->data['User']['id'] = $user_id;
            $this->data['User']['is_email_confirmed'] = 1;
            $this->data['User']['is_active'] = (Configure::read('user.is_admin_activate_after_register')) ? 0 : 1;
            $this->User->save($this->data);
            if (!$this->data['User']['is_active']) {
                $this->Session->setFlash(__l('You have successfully activated your account. But you can login after admin activate your account.'), 'default', null, 'success');
                if (!empty($this->params['named']['city'])) {
                    $redirect_url['city'] = $this->params['named']['city'];
                } else {
                    $redirect_url = '/';
                }
                $this->redirect(Router::url($redirect_url, true));
            }
            if (Configure::read('user.is_welcome_mail_after_register')) {
                $this->_sendWelcomeMail($user['User']['id'], $user['User']['email'], $user['User']['username']);
            }
            if (Configure::read('user.is_auto_login_after_register')) {
                $this->Session->setFlash(__l('You have successfully activated and logged in to your account.'), 'default', null, 'success');
                $this->data['User']['email'] = $user['User']['email'];
                $this->data['User']['username'] = $user['User']['username'];
                $this->data['User']['password'] = $user['User']['password'];
                if ($this->Auth->login($this->data)) {
                    $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                    if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                        return $this->redirect(array(
                                    'plugin' => 'now',
                                    'controller' => 'now_registers',
                                    'action' => 'my_company'
                        ));
                    }
                    $city = !empty($this->params['named']['city']) ? $this->params['named']['city'] : '';
                    $this->redirect(Router::url(array(
                                'controller' => 'deals',
                                'action' => 'index',
                                'city' => $city
                                    ), true));
                }
            }
            $this->Session->setFlash(sprintf(__('You have successfully activated your account. Now you can login with your %s.'), Configure::read('user.using_to_login')), 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'login'
            ));
        }
    }

    function resend_activation($user_id = null, $hash = null) {
        if (is_null($user_id)) {
            $this->cakeError('error404');
        }
        $hash = $this->User->getActivateHash($user_id);
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'recursive' => - 1
        ));
        if ($this->_sendActivationMail($user['User']['email'], $user_id, $hash)) {
            if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
                $this->Session->setFlash(__l('Activation mail has been resent.'), 'default', null, 'success');
            } else {
                $this->Session->setFlash(__l('A Mail for activating your account has been sent.'), 'default', null, 'success');
            }
        } else {
            $this->Session->setFlash(__l('Try some time later as mail could not be dispatched due to some error in the server'), 'default', null, 'error');
        }
        if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
            $this->redirect(array(
                'controller' => (!empty($this->params['named']['type'])) ? 'companies' : 'users',
                'action' => 'index',
                'admin' => true
            ));
        } else {
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'login'
            ));
        }
    }

    function _redirectPopupAfterRegistration($error = false) {
        if ($error) {
            return $this->redirect(array(
                        'controller' => 'firsts',
                        'action' => 'register'
            ));
        }
        $this->Session->write('UserIdRecommend', $this->User->id);
        return $this->redirect(array(
                    'controller' => 'firsts',
                    'action' => 'recommend'
        ));
    }

    function _facebook_login($fromPopup = false) {
        Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - INICIO'), LOG_DEBUG);
        $user = null;
        try {
            $fb_user = $this->facebook->api('/me?fields=id,email,first_name,middle_name,last_name,about');
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'OR' => array(
                        'User.fb_user_id' => $fb_user["id"],
                        'User.email' => $fb_user["email"]
                    )
                ),
                'fields' => array(
                    'User.id',
                    'User.fb_user_id',
                    'User.password',
                    'User.username',
                    'User.email'
                )
            ));
        } catch (FacebookApiException $e) {
            Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - FacebookApiException'), LOG_DEBUG);
            $user = null;
        }
        $this->Auth->fields['username'] = 'username';
        if (empty($user)) {
            Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - $user is empty'), LOG_DEBUG);
            $this->User->create();
            $this->data['UserProfile']['first_name'] = $fb_user['first_name'];
            $this->data['UserProfile']['middle_name'] = isset($fb_user['middle_name']) ? $fb_user['middle_name'] : "";
            $this->data['UserProfile']['last_name'] = isset($fb_user['last_name']) ? $fb_user['last_name'] : "";
            $this->data['UserProfile']['about_me'] = isset($fb_user['about']) ? $fb_user['about'] : "";
            $this->data['User']['email'] = $fb_user['email'];
            if (strlen($fb_user['first_name']) > 2) {
                $this->data['User']['username'] = $this->User->checkUsernameAvailable(strtolower($fb_user['first_name']));
            }
            if (empty($this->data['User']['username'])) {
                $this->data['User']['username'] = $this->User->checkUsernameAvailable(strtolower($fb_user['first_name'] . $fb_user['last_name']));
            }
            $this->data['User']['username'] = str_replace(' ', '', $this->data['User']['username']);
            $this->data['User']['username'] = str_replace('.', '_', $this->data['User']['username']);
            if (strlen($this->data['User']['username']) <= 2) {
                $this->data['User']['username'] = !empty($fb_user['first_name']) ? str_replace(' ', '', strtolower($fb_user['first_name'])) : 'fbuser';
                $i = 1;
                $created_user_name = $this->data['User']['username'] . $i;
                while (!$this->User->checkUsernameAvailable($created_user_name)) {
                    $created_user_name = $this->data['User']['username'] . $i++;
                }
                $this->data['User']['username'] = $created_user_name;
            }
            $this->data['User']['fb_user_id'] = $fb_user['id'];
            $this->data['User']['password'] = $this->Auth->password($fb_user['id'] . Configure::read('Security.salt'));
            $this->data['User']['is_agree_terms_conditions'] = '1';
            $this->data['User']['is_email_confirmed'] = 1;
            $this->data['User']['is_active'] = 1;
            $this->data['User']['user_type_id'] = ConstUserTypes::User;
            $this->data['User']['signup_ip'] = $this->RequestHandler->getClientIP();
            if ($this->Session->read('gift_user_id')) {
                $this->data['User']['gift_user_id'] = $this->Session->read('gift_user_id');
                $this->Session->del('gift_user_id');
            }
            if (Configure::read('user.is_referral_system_enabled')) {
                $cookie_value = $this->Cookie->read('referrer');
                if (!empty($cookie_value)) {
                    $this->data['User']['referred_by_user_id'] = $cookie_value;
                    $this->data['User']['referred_date'] = date('Y-m-d H:i:s');
                }
            }
            $subscriptionTotalAmount = $this->Subscription->findTotalAvailableBalanceAmountByEmail($this->data['User']['email']);
            if (!is_null($subscriptionTotalAmount[0]['total_amount'])) {
                $this->data['User']['available_balance_amount'] = $subscriptionTotalAmount[0]['total_amount'];
            }
            Debugger::log('USERS_CONTROLLER::_facebook_login - User-save ::' . $this->data['User']['username'], LOG_DEBUG);
            $this->User->save($this->data, false);
            $this->Session->write('is_facebook_session', true);
            $this->data['UserProfile']['user_id'] = $this->User->id;
            $this->User->UserProfile->save($this->data);
            try {
                Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - obteniendo friends - INICIO'), LOG_DEBUG);
                $my_access_token = $this->facebook->getAccessToken();
                Debugger::log('USERS_CONTROLLER::_facebook_login - token ::' . $my_access_token, LOG_DEBUG);
                $friends = $this->facebook->api('/me/friends?fields=id,email,first_name&limit=1000', array(
                    'access_token' => $my_access_token
                ));
                $fb_friends = array();
                Debugger::log($friends['data'], LOG_DEBUG);
                foreach ($friends['data'] as $friend) {
                    Debugger::log('USERS_CONTROLLER::_facebook_login - friend :: ', LOG_DEBUG);
                    Debugger::log($friend, LOG_DEBUG);
                    $fb_friends['FbFriend']['fb_user_id'] = $friend['id'];
                    $fb_friends['FbFriend']['friend_user_id'] = $this->User->id;
                    $fb_friends['FbFriend']['username'] = $friend['first_name'];
                    $fb_friends['FbFriend']['email'] = $friend['email'];
                    $this->User->FbFriend->create();
                    $this->User->FbFriend->set($fb_friends);
                    $this->User->FbFriend->save($fb_friends);
                    Debugger::log('USERS_CONTROLLER::_facebook_login - friend salvado ', LOG_DEBUG);
                }
            } catch (FacebookApiException $e) {
                Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - obteniendo friends -FacebookApiException '), LOG_DEBUG);
            }
            if ($this->Auth->login($this->data)) {
                $this->Session->setFlash(__l('You have successfully registered with our site.'), 'default', null, 'success');
                if ($fromPopup) {
                    return $this->_redirectPopupAfterRegistration();
                } else {
                    return $this->redirect(Router::url('/', true));
                }
            }
        } else {
            Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - $user is NOT empty'), LOG_DEBUG);
            if (empty($user["User"]["fb_user_id"])) {
                $user["User"]["fb_user_id"] = $fb_user["id"];
                $this->User->save($user, false);
            }
            $this->data['User']['email'] = $user['User']['email'];
            $this->data['User']['username'] = $user['User']['username'];
            $this->data['User']['password'] = $user['User']['password'];
            Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - antes de $this-Auth>login'), LOG_DEBUG);
            if ($this->Auth->login($this->data)) {
                Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - antes de UserLogin-insertUserLogin'), LOG_DEBUG);
                $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - despues de UserLogin-insertUserLogin'), LOG_DEBUG);
                if ($redirectUrl = $this->Session->read('Auth.redirectUrl')) {
                    Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - $redirectUrl = Auth.redirectUrl '), LOG_DEBUG);
                    $this->Session->del('Auth.redirectUrl');
                    $this->redirect(Router::url('/', true) . $redirectUrl);
                } else {
                    Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - $redirectUrl =/= Auth.redirectUrl '), LOG_DEBUG);
                    if (isset($_GET['f']) && (strpos($_GET['f'], '/users/login') === false)) {
                        Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - redirect($_GET[f]) '), LOG_DEBUG);
                        $this->redirect($_GET['f']);
                    } else {
                        Debugger::log(sprintf('USERS_CONTROLLER::_facebook_login - redirect(Router) '), LOG_DEBUG);
                        $this->redirect(Router::url('/', true));
                    }
                }
            }
        }
    }

    function _facebook_ingreso() {
        $this->log('_facebook_ingreso->');
    }

    function prelogin($username = null) {
        $this->set('certificaPath', 'ClubCupon/user/login');
        if (!is_null($username)) {
            $this->set('username', $username);
        }
        isset($this->params['named']['qty']) ? $temp['User']['qty'] = $this->params['named']['qty'] : $temp['User']['qty'] = '';
        isset($this->params['named']['id']) ? $temp['User']['deal_id'] = $this->params['named']['id'] : $temp['User']['deal_id'] = '';
        if (isset($this->params['named']['id']) && isset($this->params['named']['id'])) {
            $temp['User']['thru_login'] = '1';
            $this->Session->write('fbuser_pymnt', $temp);
        }
        if (empty($this->data) and Configure::read('facebook.is_enabled_facebook_connect') && !$this->Auth->user() && $this->facebook->getUser()) {
            if (isset($_GET['required_facebook_login'])) {
                $this->_facebook_login();
            }
        }
        $this->pageTitle = "Login " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Login en " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        $title_for_layout = "Login " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->set('title_for_layout', $title_for_layout);
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'twitter') {
            App::import('Component', 'OauthConsumer');
            $this->OauthConsumer = & new OauthConsumerComponent();
            $requestToken = $this->OauthConsumer->getRequestToken('Twitter', 'http://twitter.com/oauth/request_token');
            $this->Session->write('requestToken', $requestToken);
            $this->redirect('http://twitter.com/oauth/authorize?oauth_token=' . $requestToken->key);
        }
        if (Configure::read('user.is_enable_openid') && !empty($this->data['User']['openid']) && $this->data['User']['openid'] != 'Click to Sign In' && $this->data['User']['openid'] != 'http://') {
            $this->Auth->logout();
            $this->data['User']['email'] = '';
            $this->data['User']['password'] = '';
            $this->data['User']['redirect_page'] = 'login';
            $this->_openid();
        }
        if (!empty($_GET['openid_identity']) && Configure::read('user.is_enable_openid')) {
            $returnTo = Router::url(array(
                        'controller' => 'users',
                        'action' => 'login'
                            ), true);
            $response = $this->Openid->getResponse($returnTo);
            if ($response->status == Auth_OpenID_SUCCESS) {
                if ($user = $this->User->UserOpenid->find('first', array(
                    'conditions' => array(
                        'UserOpenid.openid' => $response->identity_url
                    )
                        ))) {
                    $this->data['User']['email'] = $user['User']['email'];
                    $this->data['User']['username'] = $user['User']['username'];
                    $this->data['User']['password'] = $user['User']['password'];
                    if ($this->Auth->login($this->data)) {
                        $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                        if ($redirectUrl = $this->Session->read('Auth.redirectUrl')) {
                            $this->Session->del('Auth.redirectUrl');
                            $this->redirect(Router::url('/', true) . $redirectUrl);
                        } else {
                            if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                return $this->redirect(array(
                                            'plugin' => '',
                                            'controller' => 'users',
                                            'action' => 'change_password'
                                ));
                            } else {
                                $this->redirect(array(
                                    'controller' => 'deals',
                                    'action' => 'index'
                                ));
                            }
                        }
                    } else {
                        $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                        $this->redirect(array(
                            'controller' => 'users',
                            'action' => 'login'
                        ));
                    }
                } else {
                    $this->log('antes de $sregResponse');
                    $sregResponse = Auth_OpenID_SRegResponse::fromSuccessResponse($response);
                    $sreg = $sregResponse->contents();
                    $temp['username'] = isset($sreg['nickname']) ? $sreg['nickname'] : '';
                    $temp['email'] = isset($sreg['email']) ? $sreg['email'] : '';
                    $temp['openid_url'] = $response->identity_url;
                    $this->Session->write('openid', $temp);
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'register'
                    ));
                }
            } else {
                $this->Session->setFlash(__l('Authenticated failed or you may not have profile in your OpenID account'));
            }
        }
        if (empty($this->data['User']['openid']) || $this->data['User']['openid'] == 'Click to Sign In' || $this->data['User']['openid'] == 'http://') {
            if (!empty($this->data)) {
                $this->data['User'][Configure::read('user.using_to_login')] = trim($this->data['User'][Configure::read('user.using_to_login')]);
                unset($this->User->validate[Configure::read('user.using_to_login')]['rule3']);
                $this->User->set($this->data);
                if ($this->User->validates()) {
                    $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                    if ($this->Auth->login($this->data)) {
                        $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                        if ($this->Auth->user()) {
                            if (!empty($this->data['User']['is_remember']) and $this->data['User']['is_remember'] == 1) {
                                $this->Cookie->del('User');
                                $cookie = array();
                                $remember_hash = md5($this->data['User'][Configure::read('user.using_to_login')] . $this->data['User']['password'] . Configure::read('Security.salt'));
                                $cookie['cookie_hash'] = $remember_hash;
                                $this->Cookie->write('User', $cookie, true, $this->cookieTerm);
                                $this->User->updateAll(array(
                                    'User.cookie_hash' => '\'' . md5($remember_hash) . '\'',
                                    'User.cookie_time_modified' => '\'' . date('Y-m-d H:i:s') . '\''
                                        ), array(
                                    'User.id' => $this->Auth->user('id')
                                ));
                            } else {
                                $this->Cookie->del('User');
                            }
                            if ($this->RequestHandler->isAjax()) {
                                die;
                                if (!empty($this->data['User']['f'])) {
                                    echo 'redirect*' . Router::url('/', true) . $this->data['User']['f'];
                                } else {
                                    echo 'success';
                                }
                                exit;
                            } else if (!empty($this->data['User']['f'])) {
                                if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                    return $this->redirect(array(
                                                'plugin' => '',
                                                'controller' => 'users',
                                                'action' => 'change_password'
                                    ));
                                } else {
                                    $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                                }
                            } else if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
                                $this->redirect(array(
                                    'controller' => 'users',
                                    'action' => 'stats',
                                    'admin' => true
                                ));
                            } else if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
                                $this->redirect(array(
                                    'controller' => 'deals',
                                    'action' => 'index',
                                    'admin' => true
                                ));
                            } elseif ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
                                $this->redirect(Router::url('/', true));
                            } elseif ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                return $this->redirect(array(
                                            'plugin' => '',
                                            'controller' => 'users',
                                            'action' => 'change_password'
                                ));
                            } elseif ($this->Auth->user('user_type_id') == 7) {
                                $this->redirect(Router::url('/', true));
                            }
                        }
                    } else {
                        if (!empty($this->params['prefix']) && $this->params['prefix'] == 'admin') {
                            $this->Session->setFlash(sprintf(__l('Sorry, login failed.  Your %s or password are incorrect'), Configure::read('user.using_to_login')), 'default', null, 'error');
                        } else {
                            $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                        }
                    }
                } else {
                    $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                }
            } else {
                if (!empty($this->params['named']['f'])) {
                    $this->data['User']['f'] = $this->params['named']['f'];
                }
                if (!empty($this->params['requested'])) {
                    $this->data['User']['is_requested'] = 1;
                }
            }
        }
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        $this->data['User']['passwd'] = '';
    }

    function login($username = null) {
        Debugger::log(sprintf('USERS_CONTROLLER::login - INICIO'), LOG_DEBUG);
        $myCuponsUrl = Router::url(array(
                    'plugin' => '',
                    'controller' => 'deal_users',
                    'action' => 'index',
                    'admin' => false,
                    'type' => 'available'
                        ), true);
        $this->set('certificaPath', 'ClubCupon/user/login');
        if (!is_null($username)) {
            $this->set('username', $username);
        }
        isset($this->params['named']['qty']) ? $temp['User']['qty'] = $this->params['named']['qty'] : $temp['User']['qty'] = '';
        isset($this->params['named']['id']) ? $temp['User']['deal_id'] = $this->params['named']['id'] : $temp['User']['deal_id'] = '';
        if (isset($this->params['named']['id']) && isset($this->params['named']['id'])) {
            $temp['User']['thru_login'] = '1';
            $this->Session->write('fbuser_pymnt', $temp);
        }
        if (empty($this->data) and Configure::read('facebook.is_enabled_facebook_connect') && !$this->Auth->user() && $this->facebook->getUser()) {
            Debugger::log(sprintf('USERS_CONTROLLER::login - required_facebook_login VALOR'), LOG_DEBUG);
            if (isset($_GET['required_facebook_login'])) {
                $this->_facebook_login();
            }
            Debugger::log(sprintf('USERS_CONTROLLER::login - required_facebook_login SINVALOR'), LOG_DEBUG);
        }
        $this->pageTitle = "Login " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Login en " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        $title_for_layout = "Login " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->set('title_for_layout', $title_for_layout);
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'twitter') {
            App::import('Component', 'OauthConsumer');
            $this->OauthConsumer = & new OauthConsumerComponent();
            $requestToken = $this->OauthConsumer->getRequestToken('Twitter', 'http://twitter.com/oauth/request_token');
            $this->Session->write('requestToken', $requestToken);
            $this->redirect('http://twitter.com/oauth/authorize?oauth_token=' . $requestToken->key);
        }
        if (Configure::read('user.is_enable_openid') && !empty($this->data['User']['openid']) && $this->data['User']['openid'] != 'Click to Sign In' && $this->data['User']['openid'] != 'http://') {
            $this->Auth->logout();
            $this->data['User']['email'] = '';
            $this->data['User']['password'] = '';
            $this->data['User']['redirect_page'] = 'login';
            $this->_openid();
        }
        if (!empty($_GET['openid_identity']) && Configure::read('user.is_enable_openid')) {
            $returnTo = Router::url(array(
                        'controller' => 'users',
                        'action' => 'login'
                            ), true);
            $response = $this->Openid->getResponse($returnTo);
            if ($response->status == Auth_OpenID_SUCCESS) {
                if ($user = $this->User->UserOpenid->find('first', array(
                    'conditions' => array(
                        'UserOpenid.openid' => $response->identity_url
                    )
                        ))) {
                    $this->data['User']['email'] = $user['User']['email'];
                    $this->data['User']['username'] = $user['User']['username'];
                    $this->data['User']['password'] = $user['User']['password'];
                    $this->log('antes de Auth');
                    if ($this->Auth->login($this->data)) {
                        $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                        if ($redirectUrl = $this->Session->read('Auth.redirectUrl')) {
                            $this->Session->del('Auth.redirectUrl');
                            $this->log('del Auth.redirectUrl');
                            $this->redirect(Router::url('/', true) . $redirectUrl);
                        } else {
                            if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                $this->redirect(array(
                                    'controller' => 'now_deals',
                                    'action' => 'my_cupons',
                                    'plugin' => 'now',
                                    'admin' => false
                                ));
                            } else {
                                $this->redirect(array(
                                    'controller' => 'deals',
                                    'action' => 'index'
                                ));
                            }
                        }
                    } else {
                        $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                        $this->redirect(array(
                            'controller' => 'users',
                            'action' => 'login'
                        ));
                    }
                } else {
                    $this->log('antes de $sregResponse');
                    $sregResponse = Auth_OpenID_SRegResponse::fromSuccessResponse($response);
                    $sreg = $sregResponse->contents();
                    $temp['username'] = isset($sreg['nickname']) ? $sreg['nickname'] : '';
                    $temp['email'] = isset($sreg['email']) ? $sreg['email'] : '';
                    $temp['openid_url'] = $response->identity_url;
                    $this->Session->write('openid', $temp);
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'register'
                    ));
                }
            } else {
                $this->Session->setFlash(__l('Authenticated failed or you may not have profile in your OpenID account'));
            }
        }
        if (empty($this->data['User']['openid']) || $this->data['User']['openid'] == 'Click to Sign In' || $this->data['User']['openid'] == 'http://') {
            if (!empty($this->data)) {
                $this->data['User'][Configure::read('user.using_to_login')] = trim($this->data['User'][Configure::read('user.using_to_login')]);
                unset($this->User->validate[Configure::read('user.using_to_login')]['rule3']);
                $this->User->set($this->data);
                if ($this->User->validates()) {
                    $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                    if ($this->Auth->login($this->data)) {
                        $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                        if ($this->Auth->user()) {
                            if (!empty($this->data['User']['is_remember']) and $this->data['User']['is_remember'] == 1) {
                                $this->Cookie->del('User');
                                $cookie = array();
                                $remember_hash = md5($this->data['User'][Configure::read('user.using_to_login')] . $this->data['User']['password'] . Configure::read('Security.salt'));
                                $cookie['cookie_hash'] = $remember_hash;
                                $this->Cookie->write('User', $cookie, true, $this->cookieTerm);
                                $this->User->updateAll(array(
                                    'User.cookie_hash' => '\'' . md5($remember_hash) . '\'',
                                    'User.cookie_time_modified' => '\'' . date('Y-m-d H:i:s') . '\''
                                        ), array(
                                    'User.id' => $this->Auth->user('id')
                                ));
                            } else {
                                $this->Cookie->del('User');
                            }
                            if ($this->RequestHandler->isAjax()) {
                                die;
                                if (!empty($this->data['User']['f'])) {
                                    echo 'redirect*' . Router::url('/', true) . $this->data['User']['f'];
                                } else {
                                    echo 'success';
                                }
                                exit;
                            } else if (!empty($this->data['User']['f'])) {
                                $exception_array = array(
                                    'now/now_deals/my_deals'
                                );
                                if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                                    if (!in_array($this->data['User']['f'], $exception_array)) {
                                        $this->redirect(array(
                                            'controller' => 'deal_users',
                                            'action' => 'index_for_company',
                                            'plugin' => '',
                                            'admin' => false
                                        ));
                                    } else {
                                        $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                                    }
                                } else {
                                    if (!in_array($this->data['User']['f'], $exception_array)) {
                                        $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                                    } else {
                                        $this->redirect($myCuponsUrl);
                                    }
                                }
                            } else if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
                                $this->redirect(array(
                                    'controller' => 'users',
                                    'action' => 'stats',
                                    'admin' => true
                                ));
                            } else if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
                                $this->redirect(array(
                                    'controller' => 'deals',
                                    'action' => 'index',
                                    'admin' => true
                                ));
                            } elseif ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
                                $this->redirect($myCuponsUrl);
                            } elseif ($this->User->isCompany($this->Auth->user())) {
                                $company = $this->Company->findByUser($this->Auth->user());
                                if (Configure::read('now.is_enabled') && $this->Company->isNow($company)) {
                                    if ($this->Company->isNowWithBranches($company)) {
                                        return $this->_myCouponsNowRedirect();
                                    } elseif ($this->Company->isNowWithoutBranches($company)) {
                                        return $this->_addBranchRedirect();
                                    }
                                } else {
                                    return $this->_indexForCompanyRedirect();
                                }
                            } elseif ($this->User->isCompanyCandidate($this->Auth->user())) {
                                return $this->_myCompanyNowRedirect();
                            } elseif ($this->Auth->user('user_type_id') == 7) {
                                $this->redirect($myCuponsUrl);
                            }
                        }
                    } else {
                        if (!empty($this->params['prefix']) && $this->params['prefix'] == 'admin') {
                            $this->Session->setFlash(sprintf(__l('Sorry, login failed.  Your %s or password are incorrect'), Configure::read('user.using_to_login')), 'default', null, 'error');
                        } else {
                            $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                            if (!empty($this->data['User']['f'])) {
                                $this->Session->setFlash('No se pudo completar el logueo. Verifique los datos ingresados.', 'default', null, 'error');
                                if (isset($this->data['User']['email']) && !empty($this->data['User']['email'])) {
                                    $this->redirect(Router::url('/', true) . $this->data['User']['f'] . '/user_mail:' . $this->data['User']['email']);
                                } else {
                                    $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                                }
                            }
                        }
                    }
                } else {
                    $this->Session->setFlash($this->Auth->loginError, 'default', null, 'error');
                    if (!empty($this->data['User']['f'])) {
                        $this->Session->setFlash('No se pudo completar el logueo. Verifique los datos ingresados.', 'default', null, 'error');
                        if (isset($this->data['User']['email']) && !empty($this->data['User']['email'])) {
                            $this->redirect(Router::url('/', true) . $this->data['User']['f'] . '/user_mail:' . $this->data['User']['email']);
                        } else {
                            $this->redirect(Router::url('/', true) . $this->data['User']['f']);
                        }
                    }
                }
            } else {
                if (!empty($this->params['named']['f'])) {
                    $this->data['User']['f'] = $this->params['named']['f'];
                }
                if (!empty($this->params['requested'])) {
                    $this->data['User']['is_requested'] = 1;
                }
            }
        }
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        $this->data['User']['passwd'] = '';
    }

    function oauth_callback() {
        $this->autoRender = false;
        $requestToken = $this->Session->read('requestToken');
        $accessToken = $this->OauthConsumer->getAccessToken('Twitter', 'http://twitter.com/oauth/access_token', $requestToken);
        $this->Session->write('accessToken', $accessToken);
        $xml = $this->OauthConsumer->get('Twitter', $accessToken->key, $accessToken->secret, 'https://twitter.com/account/verify_credentials.xml');
        $this->data['User']['twitter_access_token'] = (isset($accessToken->key)) ? $accessToken->key : '';
        $this->data['User']['twitter_access_key'] = (isset($accessToken->secret)) ? $accessToken->secret : '';
        $twitter_city_id = $this->Session->read('twitter_city_id');
        if (!empty($twitter_city_id)) {
            $this->User->Company->City->updateAll(array(
                'City.twitter_access_token' => '\'' . $this->data['User']['twitter_access_token'] . '\'',
                'City.twitter_access_key' => '\'' . $this->data['User']['twitter_access_key'] . '\''
                    ), array(
                'City.id' => $twitter_city_id
            ));
            $this->Session->delete('twitter_city_id');
            $this->Session->setFlash(__l('City Twitter credentials are updated'), 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'cities',
                'admin' => true
            ));
        } else if ($this->Auth->user('id') && ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
            App::import('Model', 'Setting');
            $setting = new Setting;
            $setting->updateAll(array(
                'Setting.value' => '\'' . $this->data['User']['twitter_access_token'] . '\''
                    ), array(
                'Setting.name' => 'twitter.site_user_access_token'
            ));
            $setting->updateAll(array(
                'Setting.value' => '\'' . $this->data['User']['twitter_access_key'] . '\''
                    ), array(
                'Setting.name' => 'twitter.site_user_access_key'
            ));
            $this->Session->setFlash(__l('Your Twitter credentials are updated'), 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'settings',
                'admin' => true
            ));
        }
        App::import('Xml');
        $Xml = new Xml($xml);
        $data = $Xml->toArray();
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.twitter_user_id =' => $data['User']['id']
            ),
            'fields' => array(
                'User.id',
                'UserProfile.id',
                'User.user_type_id'
            ),
            'recursive' => 0
        ));
        if (empty($user)) {
            $this->data['User']['is_email_confirmed'] = 1;
            $this->data['User']['is_active'] = 1;
            $this->data['User']['is_agree_terms_conditions'] = '1';
            $this->data['User']['user_type_id'] = ConstUserTypes::User;
            $this->data['User']['signup_ip'] = $this->RequestHandler->getClientIP();
            $this->data['User']['pin'] = ($data['User']['id'] + Configure::read('user.pin_formula')) % 10000;
            $this->data['User']['twitter_user_id'] = $data['User']['id'];
            $created_user_name = $this->User->checkUsernameAvailable($data['User']['screen_name']);
            if (strlen($created_user_name) <= 2) {
                $this->data['User']['username'] = !empty($data['User']['screen_name']) ? $data['User']['screen_name'] : 'twuser';
                $i = 1;
                $created_user_name = $this->data['User']['username'] . $i;
                while (!$this->User->checkUsernameAvailable($created_user_name)) {
                    $created_user_name = $this->data['User']['username'] . $i++;
                }
            }
            $this->data['User']['username'] = $created_user_name;
        } else {
            $this->data['User']['id'] = $user['User']['id'];
        }
        unset($this->User->validate['username']['rule2']);
        unset($this->User->validate['username']['rule3']);
        $this->data['User']['password'] = $this->Auth->password($data['User']['id'] . Configure::read('Security.salt'));
        $this->data['User']['avatar_url'] = $data['User']['profile_image_url'];
        $this->data['User']['twitter_url'] = (isset($data['User']['url'])) ? $data['User']['url'] : '';
        $this->data['User']['description'] = (isset($data['User']['description'])) ? $data['User']['description'] : '';
        $this->data['User']['location'] = (isset($data['User']['location'])) ? $data['User']['location'] : '';
        if ($this->User->save($this->data)) {
            if ($this->Auth->login($this->data)) {
                $this->User->UserLogin->insertUserLogin($this->Auth->user('id'));
                $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
                ));
            }
        }
        if (!empty($this->data['User']['f'])) {
            $this->redirect(Router::url('/', true) . $this->data['User']['f']);
        }
        $this->redirect('/');
    }

    function logout($admin = 0) {
        if ($this->Session->read('is_facebook_session') == true) {
            $this->Session->delete('is_facebook_session');
        }
        App::import('Vendor', Configure::read('facebook_lib.path'));
        $GLOBALS['facebook_config']['debug'] = NULL;
        $this->facebook = new Facebook(array(
            'appId' => Configure::read('facebook.fb_api_key'),
            'secret' => Configure::read('facebook.fb_secrect_key'),
            'cookie' => true
        ));
        $this->Auth->logout();
        $this->Cookie->del('User');
        $this->Cookie->del('user_language');
        $this->Session->setFlash('Te desconectaste del sitio.', 'default', null, 'success');
        $this->Session->del('fbuser_pymnt');
        if ($admin) {
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'logout',
                'admin' => false
            ));
        }
        if (!empty($this->params['named']['city']))
            $this->set('city', $this->params['named']['city']);
    }

    function forgot_password() {
        $this->set('certificaPath', 'ClubCupon/user/forgotPassword');
        $this->pageTitle = "Olvidaste tu contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Olvidaste tu contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        if ($this->Auth->user('id')) {
            $this->redirect('/');
        }
        if (!empty($this->data)) {
            $this->User->set($this->data);
            unset($this->User->validate['email']['rule3']);
            if ($this->User->validates()) {
                $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.email =' => $this->data['User']['email'],
                        'User.is_active' => 1
                    ),
                    'fields' => array(
                        'User.id',
                        'User.email'
                    ),
                    'recursive' => - 1
                ));
                if (!empty($user['User']['email'])) {
                    $user = $this->User->find('first', array(
                        'conditions' => array(
                            'User.email' => $user['User']['email']
                        ),
                        'recursive' => - 1
                    ));
                    $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
                                'controller' => 'contacts',
                                'action' => 'add',
                                'admin' => false
                                    ), false) . '?from=mailing';
                    $emailFindReplace = array(
                        '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                        '##USERNAME##' => (isset($user['User']['username'])) ? $user['User']['username'] : '',
                        '##SITE_NAME##' => Configure::read('site.name'),
                        '##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
                        '##CONTACT_US##' => $contact_us_url,
                        '##RESET_URL##' => Router::url(array(
                            'controller' => 'users',
                            'action' => 'reset',
                            $user['User']['id'],
                            $this->User->getResetPasswordHash($user['User']['id'])
                                ), true) . '?from=mailing',
                        '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                    );
                    $email = $this->EmailTemplate->selectTemplate('Forgot Password');
                    $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                    $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                    $this->Email->to = $user['User']['email'];
                    $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                    $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                    $this->Email->send(strtr($email['email_content'], $emailFindReplace));
                    $this->Session->setFlash(__l('An email has been sent with a link where you can change your password'), 'default', null, 'success');
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'login'
                    ));
                } else {
                    $this->Session->setFlash(sprintf('No hay un usuario registrado con el correo %s  o bien el administrador ha desactivado su cuenta. Intentá de nuevo.', $this->data['User']['email']), 'default', null, 'error');
                }
            } else {
                $this->Session->setFlash(__l('Either the entered email address is not valid or your '), 'default', null, 'error');
            }
        }
    }

    function reset($user_id = null, $hash = null) {
        $this->set('certificaPath', 'ClubCupon/user/resetPassword');
        $this->pageTitle = "Recuperar password " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Recuperar password " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        if (!empty($this->data)) {
            if ($this->User->isValidResetPasswordHash($this->data['User']['user_id'], $this->data['User']['hash'])) {
                $this->User->set($this->data);
                if ($this->User->validates()) {
                    $this->User->updateAll(array(
                        'User.password' => '\'' . $this->Auth->password($this->data['User']['passwd']) . '\''
                            ), array(
                        'User.id' => $this->data['User']['user_id']
                    ));
                    $this->Session->setFlash(__l('Your password changed successfully, Please login now'), 'default', null, 'success');
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'login'
                    ));
                }
                $this->Session->setFlash(__l('Could not update your password, please enter password.'), 'default', null, 'error');
                $this->data['User']['passwd'] = '';
                $this->data['User']['confirm_password'] = '';
            } else {
                $this->Session->setFlash(__l('Invalid change password request'));
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'login'
                ));
            }
        } else {
            if (is_null($user_id) || is_null($hash)) {
                $this->cakeError('error404');
            }
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $user_id,
                    'User.is_active' => 1
                ),
                'recursive' => - 1
            ));
            if (empty($user)) {
                $this->Session->setFlash(__l('User cannot be found in server or admin deactivated your account, please register again'));
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'register'
                ));
            }
            if (!$this->User->isValidResetPasswordHash($user_id, $hash)) {
                $this->Session->setFlash(__l('Invalid change password request'));
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'login'
                ));
            }
            $this->data['User']['user_id'] = $user_id;
            $this->data['User']['hash'] = $hash;
        }
    }

    function change_password($user_id = null) {
        $this->set('certificaPath', 'ClubCupon/user/changePassword');
        $this->pageTitle = "Cambio de contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        Configure::write('meta.description', "Cambio de contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
        $this->log('titulo:' . $this->pageTitle);
        $title_for_layout = $this->pageTitle;
        $this->set('title_for_layout', $title_for_layout);
        if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
            $this->layout = 'now';
            $this->set('is_now_company', true);
        }
        if (($this->Auth->user('user_type_id') == ConstUserTypes::Company) || ($this->Auth->user('user_type_id') == ConstUserTypes::User)) {
            if ($this->Auth->User('id') != $user_id && !is_null($user_id)) {
                $this->cakeError('error404');
            }
            if ($this->Auth->user('is_openid_register')) {
                $this->cakeError('error404');
            }
        }
        if (!empty($this->data)) {
            $this->User->set($this->data);
            if ($this->User->validates()) {
                if ($this->User->updateAll(array(
                            'User.password' => '\'' . $this->Auth->password($this->data['User']['passwd']) . '\''
                                ), array(
                            'User.id' => $this->data['User']['user_id']
                        ))) {
                    if (ConstUserTypes::isNotLikeAdmin($this->Auth->user('user_type_id')) && Configure::read('user.is_logout_after_change_password')) {
                        $this->Auth->logout();
                        $this->Session->setFlash(__l('Your password changed successfully. Please login now'), 'default', null, 'success');
                        if ($this->RequestHandler->isAjax()) {
                            echo 'redirect*' . Router::url(array(
                                'controller' => 'users',
                                'action' => 'login'
                                    ), true);
                            exit;
                        } else {
                            $this->redirect(array(
                                'controller' => 'users',
                                'action' => 'login'
                            ));
                        }
                    } elseif (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id')) && $this->Auth->user('id') != $this->data['User']['user_id']) {
                        $user = $this->User->find('first', array(
                            'conditions' => array(
                                'User.id' => $this->data['User']['user_id']
                            ),
                            'fields' => array(
                                'User.username',
                                'User.email'
                            ),
                            'recursive' => - 1
                        ));
                        $emailFindReplace = array(
                            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                            '##PASSWORD##' => $this->data['User']['passwd'],
                            '##USERNAME##' => $user['User']['username'],
                            '##SITE_NAME##' => Configure::read('site.name'),
                            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                        );
                        $email = $this->EmailTemplate->selectTemplate('Admin Change Password');
                        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                        $this->Email->to = $user['User']['email'];
                        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
                    }
                    if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id')) && $this->Auth->user('id') != $this->data['User']['user_id']) {
                        $this->Session->setFlash(sprintf('%s su contraseña ha cambiado correctamente', $user['User']['username']), 'default', null, 'success');
                    } else {
                        $this->Session->setFlash(__l('Your password changed successfully'), 'default', null, 'success');
                    }
                } else {
                    $this->Session->setFlash(__l('Password could not be changed'), 'default', null, 'error');
                }
            } else {
                $this->Session->setFlash(__l('Password could not be changed'), 'default', null, 'error');
            }
            unset($this->data['User']['old_password']);
            unset($this->data['User']['passwd']);
            unset($this->data['User']['confirm_password']);
        } else {
            if (empty($user_id)) {
                $user_id = $this->Auth->user('id');
            }
        }
        if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
            $users = $this->User->find('list', array(
                'conditions' => array(
                    'User.fb_user_id =' => NULL,
                    'User.is_openid_register = ' => 0
                )
            ));
            foreach ($users as $key => $value) {
                if (empty($value))
                    unset($users[$key]);
            }
            $this->set(compact('users'));
        }
        $this->data['User']['user_id'] = (!empty($this->data['User']['user_id'])) ? $this->data['User']['user_id'] : $user_id;
        $login = $this->User->findById($this->Auth->user('id'));
        $this->set('login', $login);
        $this->set('company', $this->User->Company->find('all', array(
                    'conditions' => array(
                        'user_id' => $this->Auth->user('id')
                    ),
                    'recursive' => 0
        )));
        if ($this->User->isCompany($this->Auth->user())) {
            $this->set('numero_comercio', $login['Company']['id']);
        }
        $this->set('now_deal_count', 1);
    }

    function admin_index() {
        $this->pageTitle = __l('Users');
        $conditions = $count_conditions = array();
        $this->_redirectGET2Named(array(
            'company_type',
            'q'
        ));
        $this->pageTitle = __l('Users');
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $conditions[] = sprintf('User.id in (SELECT user_profiles.user_id from user_profiles where user_profiles.city_id IN (SELECT city_id from city_perms where city_perms.user_id = %d))', $this->Auth->user('id'));
            $conditions['NOT'] = array(
                'User.user_type_id' => array(
                    ConstUserTypes::Agency,
                    ConstUserTypes::Partner,
                    ConstUserTypes::Admin,
                    ConstUserTypes::SuperAdmin,
                    ConstUserTypes::Seller
                )
            );
        }
        if (!empty($this->data['User']['main_filter_id'])) {
            $this->params['named']['main_filter_id'] = $this->data['User']['main_filter_id'];
        }
        if (!empty($this->data['User']['filter_id'])) {
            $this->params['named']['filter_id'] = $this->data['User']['filter_id'];
        }
        if (!empty($this->data['User']['city_id'])) {
            $conditions[] = 'User.id in (SELECT user_id from user_profiles where city_id = ' . $this->data['User']['city_id'] . ')';
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'day') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 0;
            $this->pageTitle.= __l(' - Registered today');
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'week') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 7;
            $this->pageTitle.= __l(' - Registered in this week');
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'month') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 30;
            $this->pageTitle.= __l(' - Registered in this month');
        }
        if (isset($this->params['named']['stat'])) {
            $conditions['User.user_type_id !='] = ConstUserTypes::Company;
        }
        $param_string = "";
        $param_string.=!empty($this->params['named']['filter_id']) ? '/filter_id:' . $this->params['named']['filter_id'] : $param_string;
        $param_string.=!empty($this->params['named']['main_filter_id']) ? '/main_filter_id:' . $this->params['named']['main_filter_id'] : $param_string;
        if (!empty($this->params['named']['stat'])) {
            $param_string.=!empty($this->params['named']['stat']) ? '/stat:' . $this->params['named']['stat'] : $param_string;
        }
        if (!empty($this->params['named']['main_filter_id'])) {
            if ($this->params['named']['main_filter_id'] == ConstMoreAction::OpenID) {
                $conditions['User.is_openid_register'] = 1;
                $this->pageTitle.= __l(' - Registered through OpenID ');
            } else if ($this->params['named']['main_filter_id'] == ConstMoreAction::FaceBook) {
                $conditions['User.fb_user_id != '] = NULL;
                $this->pageTitle.= __l(' - Registered through Facebook ');
            } else if ($this->params['named']['main_filter_id'] == ConstUserTypes::User) {
                $conditions['User.user_type_id'] = ConstUserTypes::User;
                $conditions['User.fb_user_id'] = Null;
                $conditions['User.is_openid_register'] = 0;
            } else if (ConstUserTypes::isLikeAdmin($this->params['named']['main_filter_id'])) {
                $conditions['User.user_type_id'] = array(
                    ConstUserTypes::Admin,
                    ConstUserTypes::SuperAdmin,
                    ConstUserTypes::Agency,
                    ConstUserTypes::Partner
                );
                $this->pageTitle.= __l(' - Admin ');
            } else if ($this->params['named']['main_filter_id'] == 'gift_card') {
                $conditions['User.gift_user_id != '] = NULL;
                $this->pageTitle.= __l(' - Registered Via Gift Card');
            } else if ($this->params['named']['main_filter_id'] == 'all') {
                $conditions['User.user_type_id != '] = ConstUserTypes::Company;
                $this->pageTitle.= __l(' - All ');
            }
            $count_conditions = $conditions;
        } else {
            $count_conditions = $conditions;
        }
        if (!empty($this->params['named']['filter_id'])) {
            if ($this->params['named']['filter_id'] == ConstMoreAction::Active) {
                $conditions['User.is_active'] = 1;
                $this->pageTitle.= __l(' - Activos ');
            } else if ($this->params['named']['filter_id'] == ConstMoreAction::Inactive) {
                $conditions['User.is_active'] = 0;
                $this->pageTitle.= __l(' - Desactivados ');
            }
        }
        if (isset($this->data['User']['q']) && !empty($this->data['User']['q'])) {
            $this->pageTitle.= sprintf(__l(' - Search - %s'), $this->data['User']['q']);
            $param_string.= '/q:' . $this->data['User']['q'];
        }
        $this->User->recursive = 2;
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'Company',
                'RefferalUser',
                'GiftRecivedFromUser'
            )
        );
        if (isset($this->data['User']['q']) && !empty($this->data['User']['q'])) {
            $this->paginate['search'] = $this->data['User']['q'];
        }
        $this->set('param_string', $param_string);
        $this->set('users', $this->paginate());
        $this->set('pageTitle', $this->pageTitle);
        if (!empty($this->params['named']['main_filter_id']) && ConstUserTypes::isLikeAdmin($this->params['named']['main_filter_id'])) {
            $moreActions = $this->User->adminMoreActions;
        } else {
            $moreActions = $this->User->moreActions;
        }
        unset($moreActions[ConstMoreAction::Delete]);
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            unset($moreActions[ConstMoreAction::Active]);
            unset($moreActions[ConstMoreAction::Inactive]);
        }
        $this->set(compact('moreActions'));
        $this->set('active', "");
        $this->set('inactive', "");
    }

    function _getCityPermsFromData($data, $userId) {
        $selectedCities = array();
        foreach ($data as $city) {
            if ($city['city_id_selected']) {
                $selectedCities[] = array(
                    'city_id' => $city['city_id_selected'],
                    'user_id' => $userId
                );
            }
        }
        return $selectedCities;
    }

    function admin_add() {
        AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Add New User/Admin');
        if (!empty($this->data)) {
            $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
            $this->data['User']['is_agree_terms_conditions'] = '1';
            $this->data['User']['is_email_confirmed'] = 1;
            $this->data['User']['is_active'] = 1;
            $this->data['User']['signup_ip'] = $this->RequestHandler->getClientIP();
            $this->User->create();
            $this->User->UserProfile->set($this->data);
            if ($this->User->save($this->data) && $this->User->UserProfile->validates()) {
                $insertedUserId = $this->data['UserProfile']['user_id'] = $this->User->getLastInsertId();
                $this->User->UserProfile->create();
                $this->User->UserProfile->save($this->data);
                if (in_array($this->data['User']['user_type_id'], array(
                            ConstUserTypes::Agency,
                            ConstUserTypes::Partner
                        ))) {
                    $retPerms = $this->User->CityPerms->saveAll($this->_getCityPermsFromData($this->data['CityPerms'], $insertedUserId));
                }
                $emailFindReplace = array(
                    '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                    '##USERNAME##' => $this->data['User']['username'],
                    '##LOGINLABEL##' => ucfirst(Configure::read('user.using_to_login')),
                    '##USEDTOLOGIN##' => $this->data['User'][Configure::read('user.using_to_login')],
                    '##SITE_NAME##' => Configure::read('site.name'),
                    '##PASSWORD##' => $this->data['User']['passwd'],
                    '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                );
                $email = $this->EmailTemplate->selectTemplate('Admin User Add');
                $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                $this->Email->to = $this->data['User']['email'];
                $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                $this->Email->send(strtr($email['email_content'], $emailFindReplace));
                $this->Session->setFlash(__l('User has been added'), 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                unset($this->data['User']['passwd']);
                $this->Session->setFlash(__l('User could not be added. Please, try again.'), 'default', null, 'error');
            }
        }
        $userTypes = $this->User->UserType->find('list', array(
            'conditions' => array(
                'UserType.id !=' => ConstUserTypes::Company
            )
        ));
        $this->set(compact('userTypes'));
        if (!isset($this->data['User']['user_type_id'])) {
            $this->data['User']['user_type_id'] = ConstUserTypes::User;
        }
        $cities = $this->User->UserProfile->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            ),
            'recursive' => - 1
        ));
        $this->set(compact('cities'));
    }

    function admin_edit($id = null) {
        $this->pageTitle = __l('Edit User');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->User->save($this->data)) {
                if (ConstUserTypes::isPrivilegedUser($this->data['User']['user_type_id'])) {
                    $this->User->CityPerms->deleteAll(array(
                        'user_id' => $id
                            ), false);
                    $retPerms = $this->User->CityPerms->saveAll($this->_getCityPermsFromData($this->data['CityPerms'], $id));
                }
                $this->Session->setFlash(__l('Se actualizó la información del usuario'), 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__l('No se pudieron actualizar los datos del usuario.'), 'default', null, 'error');
            }
        } else {
            $this->data = $this->User->read(null, $id);
        }
        $citiesSelected = array_pluck('city_id', $this->data['CityPerms']);
        $this->pageTitle.= ' - ' . $this->data['User']['name'];
        $cities = $this->User->UserProfile->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            ),
            'recursive' => - 1
        ));
        $userTypes = $this->User->UserType->find('list', array(
            'conditions' => array(
                'UserType.id' => array(
                    ConstUserTypes::Agency,
                    ConstUserTypes::Admin,
                    ConstUserTypes::Partner
                )
            )
        ));
        $this->set(compact('cities', 'citiesSelected', 'userTypes'));
    }

    function admin_delete($id = null) {
        AppModel::setDefaultDbConnection('master');
        if (is_null($id) && $id == ConstUserTypes::Admin) {
            $this->cakeError('error404');
        }
        if ($this->User->del($id)) {
            $this->Session->setFlash(__l('User deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function admin_update() {
        AppModel::setDefaultDbConnection('master');
        $this->autoRender = false;
        if (!empty($this->data['User'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $userIds = array();
            foreach ($this->data['User'] as $user_id => $is_checked) {
                if ($is_checked['id']) {
                    $userIds[] = $user_id;
                }
            }
            if ($actionid && !empty($userIds)) {
                if ($actionid == ConstMoreAction::Inactive) {
                    $this->User->updateAll(array(
                        'User.is_active' => 0
                            ), array(
                        'User.id' => $userIds
                    ));
                    foreach ($userIds as $key => $user_id) {
                        $this->_sendAdminActionMail($user_id, 'Admin User Deactivate');
                    }
                    $this->Session->setFlash(__l('Checked users has been inactivated'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Active) {
                    $this->User->updateAll(array(
                        'User.is_active' => 1
                            ), array(
                        'User.id' => $userIds
                    ));
                    foreach ($userIds as $key => $user_id) {
                        $this->_sendAdminActionMail($user_id, 'Admin User Active');
                    }
                    $this->Session->setFlash(__l('Checked users has been activated'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Delete) {
                    foreach ($userIds as $key => $user_id) {
                        $this->_sendAdminActionMail($user_id, 'Admin User Delete');
                    }
                    $this->User->deleteAll(array(
                        'User.id' => $userIds
                    ));
                    $this->Session->setFlash(__l('Checked users has been deleted'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Export) {
                    $user_ids = implode(',', $userIds);
                    $hash = $this->User->getUserIdHash($user_ids);
                    $_SESSION['user_export'][$hash] = $userIds;
                    echo 'redirect*' . Router::url(array(
                        'controller' => 'users',
                        'action' => 'export',
                        'ext' => 'csv',
                        $hash,
                        'admin' => true
                            ), true);
                    exit;
                } else if ($actionid == ConstMoreAction::EnableCompanyProfile) {
                    $this->User->Company->updateAll(array(
                        'Company.is_company_profile_enabled' => 1
                            ), array(
                        'Company.user_id' => $userIds
                    ));
                    $this->Session->setFlash(__l('Checked companies profile has been enabled'), 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function _sendAdminActionMail($user_id, $email_template) {
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'fields' => array(
                'User.username',
                'User.email'
            ),
            'recursive' => - 1
        ));
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##USERNAME##' => $user['User']['username'],
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        $email = $this->EmailTemplate->selectTemplate($email_template);
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    function admin_stats() {
        AppModel::setDefaultDbConnection('master');
        $this->loadModel('Deal');
        $this->loadModel('Company');
        $this->loadModel('UserCashWithdrawal');
        $this->loadModel('Transaction');
        $this->pageTitle = __l('Site Stats');
        $periods = array(
            'day' => array(
                'display' => __l('Today'),
                'conditions' => array(
                    'created >= DATE_ADD(CURDATE(), INTERVAL - 0 DAY)',
                )
            ),
            'week' => array(
                'display' => __l('This week'),
                'conditions' => array(
                    'created >= DATE_ADD(CURDATE(), INTERVAL - 7 DAY)',
                )
            ),
            'month' => array(
                'display' => __l('This month'),
                'conditions' => array(
                    'created >= DATE_ADD(CURDATE(), INTERVAL - 30 DAY)',
                )
            ),
            'total' => array(
                'display' => __l('Total'),
                'conditions' => array()
            )
        );
        $models[] = array(
            'User' => array(
                'display' => __l('Users'),
                'conditions' => array(
                    'User.user_type_id != ' => 3
                ),
                'link' => array(
                    'controller' => 'users',
                    'action' => 'index'
                )
            )
        );
        $models[] = array(
            'Company' => array(
                'display' => __l('Companies'),
                'link' => array(
                    'controller' => 'companies',
                    'action' => 'index'
                )
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Open'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::Open
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::Open
                ),
                'alias' => 'DealOpen',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Draft'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::Draft
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::Draft
                ),
                'alias' => 'DealDraft',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Pending'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::PendingApproval
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::PendingApproval
                ),
                'alias' => 'DealPending',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Tipped'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::Tipped
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::Tipped
                ),
                'alias' => 'DealTipped',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Closed'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::Closed
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::Closed
                ),
                'alias' => 'DealClosed',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Paid To Company'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'filter_id' => ConstDealStatus::PaidToCompany
                ),
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::PaidToCompany
                ),
                'alias' => 'DealPaidToCompany',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('All'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index'
                ),
                'alias' => 'DealAll',
                'isSub' => 'Deal'
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Deals'),
                'link' => array(
                    'controller' => 'deals',
                    'action' => 'index'
                ),
                'colspan' => 7
            )
        );
        $models[] = array(
            'Transaction' => array(
                'display' => __l('Paid Referral Amount'),
                'link' => array(
                    'controller' => 'transactions',
                    'action' => 'index',
                    'type' => 11
                ),
                'conditions' => array(
                    'Transaction.transaction_type_id' => ConstTransactionTypes::ReferralAmountPaid
                ),
                'alias' => 'TransactionReferral',
                'isSub' => 'Transaction'
            )
        );
        $models[] = array(
            'Transaction' => array(
                'display' => __l('Paid Amount to Company'),
                'link' => array(
                    'controller' => 'transactions',
                    'action' => 'index',
                    'type' => 7
                ),
                'conditions' => array(
                    'Transaction.transaction_type_id' => ConstTransactionTypes::PaidDealAmountToCompany
                ),
                'alias' => 'TransactionPaidToCompany',
                'isSub' => 'Transaction'
            )
        );
        $models[] = array(
            'Transaction' => array(
                'display' => __l('Montos devueltos '),
                'link' => array(
                    'controller' => 'transactions',
                    'action' => 'index',
                    'type' => 13
                ),
                'conditions' => array(
                    'Transaction.transaction_type_id' => ConstTransactionTypes::AcceptCashWithdrawRequest
                ),
                'alias' => 'TransactionWithdrawAmount',
                'isSub' => 'Transaction'
            )
        );
        $models[] = array(
            'Transaction' => array(
                'display' => 'Depósitos en Cuenta Club Cupón',
                'link' => array(
                    'controller' => 'transactions',
                    'action' => 'index',
                    'type' => 1
                ),
                'conditions' => array(
                    'Transaction.transaction_type_id' => ConstTransactionTypes::AddedToWallet
                ),
                'alias' => 'TransactionAmountToWallet',
                'isSub' => 'Transaction'
            )
        );
        $models[] = array(
            'Transaction' => array(
                'display' => __l('Transactions'),
                'link' => array(
                    'controller' => 'Transaction',
                    'action' => 'index'
                ),
                'colspan' => 4
            )
        );
        $models[] = array(
            'UserCashWithdrawal' => array(
                'display' => __l('Pending Withdraw Request'),
                'link' => array(
                    'controller' => 'user_cash_withdrawals',
                    'action' => 'index',
                    'filter_id' => ConstWithdrawalStatus::Pending
                ),
                'conditions' => array(
                    'UserCashWithdrawal.withdrawal_status_id' => ConstWithdrawalStatus::Pending
                )
            )
        );
        $models[] = array(
            'Deal' => array(
                'display' => __l('Total Commission Amount') . ' (' . Configure::read('site.currency') . ')',
                'conditions' => array(
                    'Deal.deal_status_id' => array(
                        ConstDealStatus::PaidToCompany
                    )
                ),
                'alias' => 'DealCommssionAmount'
            )
        );
        foreach ($models as $unique_model) {
            foreach ($unique_model as $model => $fields) {
                foreach ($periods as $key => $period) {
                    $conditions = $period['conditions'];
                    if (!empty($fields['conditions'])) {
                        $conditions = array_merge($periods[$key]['conditions'], $fields['conditions']);
                    }
                    $aliasName = !empty($fields['alias']) ? $fields['alias'] : $model;
                    if ($model == 'Transaction') {
                        $TransTotAmount = $this->{$model}->find('first', array(
                            'conditions' => $conditions,
                            'fields' => array(
                                'SUM(Transaction.amount) as total_amount'
                            ),
                            'recursive' => - 1
                        ));
                        $this->set($aliasName . $key, $TransTotAmount['0']['total_amount']);
                    } else if ($model == 'Deal' && $aliasName == 'DealCommssionAmount') {
                        $TransTotAmount = $this->{$model}->find('first', array(
                            'conditions' => $conditions,
                            'fields' => array(
                                'SUM(Deal.total_commission_amount) as total_amount'
                            ),
                            'recursive' => - 1
                        ));
                        $this->set($aliasName . $key, $TransTotAmount['0']['total_amount']);
                    } else {
                        $this->set($aliasName . $key, $this->{$model}->find('count', array(
                                    'conditions' => $conditions,
                                    'recursive' => - 1
                        )));
                    }
                }
            }
        }
        $recentUsers = $this->User->find('all', array(
            'conditions' => array(
                'User.is_active' => 1,
                'User.user_type_id != ' => ConstUserTypes::Admin
            ),
            'fields' => array(
                'User.user_type_id',
                'User.username',
                'User.id'
            ),
            'recursive' => - 1,
            'limit' => 10,
            'order' => array(
                'User.id' => 'desc'
            )
        ));
        $loggedUsers = $this->User->find('all', array(
            'conditions' => array(
                'User.is_active' => 1,
                'User.user_type_id != ' => ConstUserTypes::Admin
            ),
            'fields' => array(
                'User.user_type_id',
                'User.username',
                'User.id'
            ),
            'recursive' => - 1,
            'limit' => 10,
            'order' => array(
                'User.last_logged_in_time' => 'desc'
            )
        ));
        $onlineUsers = $this->User->find('all', array(
            'conditions' => array(
                'User.is_active' => 1,
                'CkSession.user_id != ' => 0,
                'User.user_type_id != ' => ConstUserTypes::Admin
            ),
            'contain' => array(
                'CkSession' => array(
                    'fields' => array(
                        'CkSession.user_id'
                    )
                )
            ),
            'fields' => array(
                'DISTINCT User.username',
                'User.user_type_id',
                'User.id'
            ),
            'recursive' => 0,
            'limit' => 10,
            'order' => array(
                'User.last_logged_in_time' => 'desc'
            )
        ));
        $this->set('tmpCacheFileSize', bytes_to_higher(dskspace(TMP . 'cache')));
        $this->set('tmpLogsFileSize', bytes_to_higher(dskspace(TMP . 'logs')));
        $this->set(compact('loggedUsers', 'recentUsers', 'onlineUsers', 'periods', 'models'));
    }

    function admin_change_password($user_id = null) {
        if (!ConstUserTypes::isLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash(__l('No esta autorizado para realizar esta operacion'), 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'index'
            ));
        } else {
            AppModel::setDefaultDbConnection('master');
            $this->pageTitle = "Cambio de contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
            Configure::write('meta.description', "Cambio de contraseña " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.');
            $title_for_layout = $this->pageTitle;
            $this->set('title_for_layout', $this->pageTitle);
            if (!empty($this->data)) {
                $this->User->set($this->data);
                if ($this->User->validates()) {
                    if ($this->User->updateAll(array(
                                'User.password' => '\'' . $this->Auth->password($this->data['User']['passwd']) . '\''
                                    ), array(
                                'User.id' => $this->data['User']['user_id']
                            ))) {
                        $this->send_email_change_password($this->data['User']['user_id']);
                        $this->Session->setFlash(sprintf('La contraseña de %s ha sido cambiada exitosamente', $this->data['User']['username2']), 'default', null, 'success');
                        $this->redirect(array(
                            'controller' => 'users',
                            'action' => 'index'
                        ));
                    } else {
                        $this->Session->setFlash(__l('Password could not be changed'), 'default', null, 'error');
                    }
                } else {
                    $this->Session->setFlash(__l('No se pudo actualizar la contraseña. Verifique los datos ingresados'), 'default', null, 'error');
                }
                unset($this->data['User']['old_password']);
                unset($this->data['User']['passwd']);
                unset($this->data['User']['confirm_password']);
            } else {
                $this->data['User']['user_id'] = (!empty($this->data['User']['user_id'])) ? $this->data['User']['user_id'] : $user_id;
                $user = $this->User->find('first', array(
                    'recursive' => - 1,
                    'fields' => array(
                        'User.username',
                        'User.user_type_id'
                    ),
                    'conditions' => array(
                        'User.id' => $user_id
                    )
                ));
                $this->data['User']['username2'] = $user['User']['username'];
                $this->data['User']['passwd'] = '';
                if (ConstUserTypes::isLikeSuperAdmin($user['User']['user_type_id'])) {
                    $this->Session->setFlash(__l('No se puede modificar la contrase&ntilde;a de este tipo de usuario.'), 'default', null, 'error');
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'index'
                    ));
                }
            }
        }
    }

    private function send_email_change_password($user_id) {
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'fields' => array(
                'User.username',
                'User.email'
            ),
            'recursive' => - 1
        ));
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##PASSWORD##' => $this->data['User']['passwd'],
            '##USERNAME##' => $user['User']['username'],
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        $email = $this->EmailTemplate->selectTemplate('Admin Change Password');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    function admin_login() {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('login');
    }

    function admin_logout() {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('logout', 1);
    }

    function admin_export($hash = null) {
        AppModel::setDefaultDbConnection('master');
        Configure::write('debug', 0);
        $conditions = array();
        if (in_array($this->Auth->user('user_type_id'), array(
                    ConstUserTypes::Agency,
                    ConstUserTypes::Partner
                ))) {
            $conditions[] = sprintf('User.id in (SELECT user_profiles.user_id from user_profiles where user_profiles.city_id IN (SELECT city_id from city_perms where city_perms.user_id = %d))', $this->Auth->user('id'));
            $conditions['NOT'] = array(
                'User.user_type_id' => array(
                    ConstUserTypes::Agency,
                    ConstUserTypes::Partner,
                    ConstUserTypes::Admin
                )
            );
        }
        if (isset($this->params['named']['from_date']) || isset($this->params['named']['to_date'])) {
            $conditions['DATE(User.created) BETWEEN ? AND ? '] = array(
                $this->params['named']['from_date'],
                $this->params['named']['to_date']
            );
        }
        if (!empty($this->params['named']['main_filter_id'])) {
            if ($this->params['named']['main_filter_id'] == ConstMoreAction::OpenID) {
                $conditions['User.is_openid_register'] = 1;
                $this->pageTitle.= __l(' - Registered through OpenID ');
            } else if ($this->params['named']['main_filter_id'] == ConstMoreAction::FaceBook) {
                $conditions['User.fb_user_id != '] = NULL;
                $this->pageTitle.= __l(' - Registered through FaceBook ');
            } else if ($this->params['named']['main_filter_id'] == ConstUserTypes::User) {
                $conditions['User.user_type_id'] = ConstUserTypes::User;
                $conditions['User.fb_user_id = '] = NULL;
                $conditions['User.is_openid_register'] = 0;
            } else if (ConstUserTypes::isLikeAdmin($this->params['named']['main_filter_id'])) {
                $conditions['User.user_type_id'] = ConstUserTypes::Admin;
                $this->pageTitle.= __l(' - Admin ');
            } else if ($this->params['named']['main_filter_id'] == 'all') {
                $conditions['User.user_type_id != '] = ConstUserTypes::Company;
                $this->pageTitle.= __l(' - All ');
            }
            $count_conditions = $conditions;
        }
        if (!empty($this->params['named']['filter_id'])) {
            if ($this->params['named']['filter_id'] == ConstMoreAction::Active) {
                $conditions['User.is_active'] = 1;
                $this->pageTitle.= __l(' - Active ');
            } else if ($this->params['named']['filter_id'] == ConstMoreAction::Inactive) {
                $conditions['User.is_active'] = 0;
                $this->pageTitle.= __l(' - Inactive ');
            }
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'day') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 0;
            $this->pageTitle.= __l(' - Registered today');
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'week') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 7;
            $this->pageTitle.= __l(' - Registered in this week');
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'month') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(User.created) <= '] = 30;
            $this->pageTitle.= __l(' - Registered in this month');
        }
        if (!empty($hash) && isset($_SESSION['user_export'][$hash])) {
            $user_ids = implode(',', $_SESSION['user_export'][$hash]);
            if ($this->User->isValidUserIdHash($user_ids, $hash)) {
                $conditions['User.id'] = $_SESSION['user_export'][$hash];
            } else {
                $this->cakeError('error404');
            }
        }
        if (isset($this->params['named']['q']) && !empty($this->params['named']['q'])) {
            $conditions['User.username like'] = '%' . $this->params['named']['q'] . '%';
        }
        $users = $this->User->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'User.user_type_id',
                'User.username',
                'User.id',
                'User.email',
                'User.user_login_count',
                'User.user_openid_count'
            ),
            'recursive' => - 1
        ));
        if (!empty($users)) {
            foreach ($users as $user) {
                $data[]['User'] = array(
                    __l('Username') => $user['User']['username'],
                    __l('Email') => $user['User']['email'],
                    __l('Login count') => $user['User']['user_login_count']
                );
            }
        }
        $this->set('data', $data);
    }

    function resend_activemail($username = NUll, $status = NULL) {
        if (!empty($username) && !empty($status)) {
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.username' => $username
                )
            ));
            $this->_sendActivationMail($user['User']['email'], $user['User']['id'], $this->User->getActivateHash($user['User']['id']));
        }
        $this->set('username', $username);
    }

    function company_register() {
        $this->setAction('register', 'company');
    }

    function referred_users() {
        if (!empty($this->params['named']['user_id'])) {
            $conditions = array(
                'User.Referred_by_user_id' => $this->params['named']['user_id']
            );
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'UserAvatar' => array(
                    'fields' => array(
                        'UserAvatar.id',
                        'UserAvatar.filename',
                        'UserAvatar.dir'
                    )
                ),
                'DealUser' => array(
                    'fields' => array(
                        'DealUser.id'
                    )
                )
            )
        );
        $users = $this->paginate();
        foreach ($users as $user)
            $userlist[] = $user['User']['id'];
        $referred_users_deal_counts = $this->User->DealUser->find('all', array(
            'conditions' => array(
                'DealUser.user_id' => $userlist
            ),
            'fields' => array(
                'Count(DealUser.user_id) as deal_count',
                'DealUser.user_id'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        foreach ($referred_users_deal_counts as $referred_users_deal_count)
            $new_count[$referred_users_deal_count['DealUser']['user_id']] = $referred_users_deal_count['0']['deal_count'];
        foreach ($users as & $user)
            $user['User']['deal_count'] = ($new_count[$user['User']['id']]) ? $new_count[$user['User']['id']] : 0;
        $this->User->recursive = 2;
        $this->set('referredFriends', $users);
    }

    function whois($ip = null) {
        if (!empty($ip)) {
            $this->redirect(Configure::read('site.look_up_url') . $ip);
        }
    }

    function my_api($user_id = null) {
        if (is_null($user_id)) {
            $this->cakeError('error404');
        }
        $this->pageTitle = __l('My API');
        $api_key = $this->Auth->user('api_key');
        $api_token = $this->Auth->user('api_token');
        if (empty($api_key) && empty($api_token)) {
            $api_key = $this->_uuid();
            $api_token = substr(md5($this->Auth->user('username') . Configure::read('Security.salt')), 0, 15);
            $this->User->updateAll(array(
                'User.api_key' => '\'' . $api_key . '\'',
                'User.api_token' => '\'' . $api_token . '\''
                    ), array(
                'User.id' => $user_id
            ));
        }
        $this->set('api_key', $api_key);
        $this->set('api_token', $api_token);
    }

    function update_wallet_blocked($user_id) {
        if (!$user_id)
            $this->cakeError('error404');
        $user = $this->User->find('first', array(
            'fields' => 'User.wallet_blocked',
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        if (!$user)
            $this->cakeError('error404');
        $updateValue = $user['User']['wallet_blocked'] ? '0' : '1';
        $this->User->updateAll(array(
            'User.wallet_blocked' => $updateValue
                ), array(
            'User.id' => $user_id
        ));
        echo $updateValue;
        die;
    }

    function indexData() {
        @$index = $this->data['User']['username'] . ' ' . $this->data['User']['email'] . ' ' . $this->data['UserProfile']['first_name'] . ' ' . $this->data['UserProfile']['last_name'];
        return $index;
    }

    function admin_search() {
        $this->pageTitle = 'Resultados para: ' . $this->data['User']['q'];
        if (isset($this->data['Users']['q'])) {
            $ids = $this->User->search($this->data['User']['q'], array(
                'fields' => array(
                    'User.id'
                ),
                'conditions' => $conditions
            ));
            $users = $this->paginate('users_for_admin_index', array(
                'User.id' => Set::extract('/User/id', $ids)
            ));
            $this->set('users', $users);
            $this->set('parametro', $this->data['User']['q']);
        } else {
            $this->set('mensaje', 'No re recibieron parametros de busqueda');
        }
    }

    function admin_reindex() {
        $this->User->reindexAll(0);
        echo 'Index OK';
        die();
    }

    function my_stuff() {
        $this->set('certificaPath', 'ClubCupon/user/myStuff');
        if (!$this->User->isAllowed($this->Auth->user('user_type_id'))) {
            $this->cakeError('error404');
        }
        $this->pageTitle = 'Mis cosas';
    }

    function ocultar($user_id) {
        if (!$user_id)
            $this->cakeError('error404');
        $user = $this->User->find('first', array(
            'fields' => 'User.is_visible',
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        if (!$user)
            $this->cakeError('error404');
        $updateValue = $user['User']['is_visible'] ? '0' : '1';
        $this->User->updateAll(array(
            'User.is_visible' => $updateValue
                ), array(
            'User.id' => $user_id
        ));
        echo $updateValue;
        die;
    }

    function _myDealsNowRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_deals',
                    'action' => 'my_deals'
        ));
    }

    function _myCouponsNowRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_deals',
                    'action' => 'my_cupons'
        ));
    }

    function _addBranchRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_branches',
                    'action' => 'add'
        ));
    }

    function _myDealsWithoutNowRedirect($company = null) {
        return $this->_myDealsNowRedirect();
    }

    function _indexForCompanyRedirect() {
        return $this->_myCouponsNowRedirect();
    }

    function _myCompanyNowRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_registers',
                    'action' => 'my_company'
        ));
    }

    function _homeRedirect() {
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
        ));
    }

}
