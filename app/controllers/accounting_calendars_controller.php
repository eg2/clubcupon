<?php

class AccountingCalendarsController extends AppController {

    var $name = 'AccountingCalendars';
    var $uses = array(
        'accounting.AccountingCalendar'
    );
    var $helpers = array(
        'Time'
    );
    var $paginate = array(
        'fields' => array(
            'AccountingCalendar.*',
            "ifnull((select 'si' from accounting_items as AccountingItem  inner join accounting_errors as AccountingError on AccountingItem.id = AccountingError.accounting_item_id where AccountingItem.accounting_calendar_id = AccountingCalendar.id limit 1), 'no') as has_errors"
        ),
        'limit' => 20,
        'order' => array(
            'AccountingCalendar.id' => 'desc'
        )
    );

    function admin_index() {
        $this->pageTitle = 'Calendario de eventos';
        $calendars = $this->paginate();
        $size = count($calendars);
        for ($a = 0; $a < $size; $a++) {
            $calendars[$a]['AccountingCalendar']['enabledActions'] = $this->_returnEnabledActionsByStatus($calendars[$a]['AccountingCalendar']);
        }
        $this->set('calendars', $calendars);
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->validateCalendarFields($this->data)) {
                $this->AccountingCalendar->set($this->data);
                $this->AccountingCalendar->set('created_by', $this->Auth->user('id'));
                $this->AccountingCalendar->set('status', AccountingCalendar::STATUS_NEW);
                $this->AccountingCalendar->set('until', $this->data['AccountingCalendar']['until'] . ' 23:59:59');
                if ($this->AccountingCalendar->save()) {
                    $this->Session->setFlash('Registro guardado', true);
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash('No se pudo guardar el registro', true);
                }
            } else {
                $this->Session->setFlash('Las fechas ingresadas no responden al formato del calendario', true);
            }
        }
    }

    function admin_edit($id) {
        if (!empty($this->data)) {
            $status = $this->AccountingCalendar->read('status', $id);
            if ($this->validateCalendarFields($this->data) || $status['AccountingCalendar']['status'] == AccountingCalendar::STATUS_ACCOUNTED) {
                $this->AccountingCalendar->id = $id;
                $this->AccountingCalendar->set($this->data);
                $this->AccountingCalendar->set('modified_by', $this->Auth->user('id'));
                $this->AccountingCalendar->set('until', $this->data['AccountingCalendar']['until'] . ' 23:59:59');
                if ($this->AccountingCalendar->save()) {
                    $this->Session->setFlash('Registro editado', true);
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash('No se pudo editar el registro', true);
                }
            } else {
                $this->Session->setFlash('Las fechas ingresadas no responden al formato del calendario', true);
            }
        }
        $this->data = $this->AccountingCalendar->read(null, $id);
    }

    function admin_accept($id) {
        if (!empty($id)) {
            $calendarToSetAsAccepted = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->calendarIsApprovableOrRejectable($calendarToSetAsAccepted)) {
                if ($this->updateAsAccepted($calendarToSetAsAccepted)) {
                    $this->Session->setFlash('Calendario pasado a estado aceptado', true);
                } else {
                    $this->Session->setFlash('No se pudo aceptar el calendario', true);
                }
            } else {
                $this->Session->setFlash('No se puede aceptar el calendario', true);
            }
        } else {
            $this->Session->setFlash('No se ha seleccionado ningun Calendario', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_reject($id) {
        if (!empty($id)) {
            $calendarToSetAsRejected = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->calendarIsApprovableOrRejectable($calendarToSetAsRejected)) {
                if ($this->updateAsRejected($calendarToSetAsRejected)) {
                    $this->Session->setFlash('Calendario pasado a estado rechazado', true);
                } else {
                    $this->Session->setFlash('No se pudo rechazar el calendario', true);
                }
            } else {
                $this->Session->setFlash('No se puede rechazar el calendario', true);
            }
        } else {
            $this->Session->setFlash('No se ha seleccionado ningun Calendario', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_delete($id) {
        if (!empty($id)) {
            $calendarToSetAsDeleted = $this->AccountingCalendar->find('first', array(
                'conditions' => array(
                    'AccountingCalendar.id' => $id
                )
            ));
            if ($this->setCalendarAsDeleted($calendarToSetAsDeleted)) {
                $this->Session->setFlash('Calendario eliminado', true);
            } else {
                $this->Session->setFlash('No se pudo eliminar el calendario', true);
            }
        } else {
            $this->Session->setFlash('Debe seleccionarse un calendario a eliminar', true);
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function updateAsAccepted($calendar) {
        $fields = array(
            'AccountingCalendar.status' => "'" . AccountingCalendar::STATUS_ACCEPTED . "'",
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function updateAsRejected($calendar) {
        $fields = array(
            'AccountingCalendar.status' => "'" . AccountingCalendar::STATUS_REJECTED . "'",
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function setCalendarAsDeleted($calendar) {
        $fields = array(
            'AccountingCalendar.deleted' => 1,
            'AccountingCalendar.modified_by' => $this->Auth->user('id')
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->AccountingCalendar->updateAll($fields, $conditions);
    }

    function calendarIsApprovableOrRejectable($calendar) {
        return $calendar['AccountingCalendar']['status'] == AccountingCalendar::STATUS_PRE_ACCOUNTED;
    }

    function _returnEnabledActionsByStatus($calendar) {
        $enabledActions = array();
        if ($calendar['status'] == AccountingCalendar::STATUS_NEW) {
            $enabledActions = array(
                'edit',
                'delete'
            );
        }
        if ($calendar['status'] == AccountingCalendar::STATUS_PRE_ACCOUNTED) {
            $enabledActions = array(
                'accept'
            );
        }
        if ($calendar['status'] == AccountingCalendar::STATUS_ACCOUNTED) {
            $enabledActions = array(
                'edit'
            );
        }
        return $enabledActions;
    }

    function _isUntilDateAfterOrSameasSinceDate($calendar) {
        return strtotime($calendar['AccountingCalendar']['until']) >= strtotime($calendar['AccountingCalendar']['since']);
    }

    function _isExcecutionDateAfterNow($calendar) {
        return strtotime($calendar['AccountingCalendar']['execution']) > strtotime(date('Y-m-d H:i:s'));
    }

    function _isExcecutionDateAfterUntilDate($calendar) {
        return strtotime($calendar['AccountingCalendar']['execution']) > strtotime($calendar['AccountingCalendar']['until']);
    }

    function _areDatesOfCalendarNotEmpty($calendar) {
        return !empty($calendar['AccountingCalendar']['since']) && !empty($calendar['AccountingCalendar']['until']) && !empty($calendar['AccountingCalendar']['execution']);
    }

    function validateCalendarFields($calendar) {
        $error = 0;
        if (!$this->_areDatesOfCalendarNotEmpty($calendar)) {
            $this->AccountingCalendar->invalidate('since', 'Debes ingresar una fecha de inicio');
            $this->AccountingCalendar->invalidate('until', 'Debes ingresar una fecha de finalización');
            $this->AccountingCalendar->invalidate('execution', 'Debes ingresar una fecha de ejecución');
            return false;
        }
        if (!$this->_isUntilDateAfterOrSameasSinceDate($calendar)) {
            $this->AccountingCalendar->invalidate('until', 'La fecha de finalización debe ser mayor a la de inicio');
            $error++;
        }
        if (!$this->_isExcecutionDateAfterNow($calendar)) {
            $this->AccountingCalendar->invalidate('execution', 'La fecha de ejecución debe estar en el futuro');
            $error++;
        }
        if (!$this->_isExcecutionDateAfterUntilDate($calendar)) {
            $this->AccountingCalendar->invalidate('execution', 'La fecha de ejecución debe ser mayor a la de finalización');
            $error++;
        }
        return $error == 0;
    }

}
