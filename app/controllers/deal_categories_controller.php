<?php

class DealCategoriesController extends AppController {

  var $name = 'DealCategories';
  var $uses = array('DealCategory','Deal');

  function admin_index() {
    $deal_categories_all = $this->DealCategory->find('all',array('recursive' => -1));
    $deal_categories_list = $this->DealCategory->find('list',array('recursive' => -1));
    $deal_categories = array();
    foreach($deal_categories_all as $cat) {
      $cat = $cat['DealCategory'];
      $parentName = '[TOP]';
      if(!empty($deal_categories_list[$cat['parent_id']])) {
        $parentName = $deal_categories_list[$cat['parent_id']];
      }
      $deal_categories[$cat['id']] = array(
          'name' => $cat['name'],
          'id' => $cat['id'],
          'parentName' => $parentName,
      );
    }
    $this->set('deal_categories',$deal_categories);
  }

  function admin_add() {

    if (!empty($this->data)) {
      $this->DealCategory->save($this->data);
      $this->Session->setFlash('Se agregó una nueva categoría');
      $this->redirect(array('action' => 'index'));
    } else {
      $parents[0] = "[ Top ]";
      $deal_categories = $this->DealCategory->generatetreelist(null, null, null, " - ");
      if ($deal_categories) {
        foreach ($deal_categories as $key => $value)
          $parents[$key] = $value;
      }
      $this->set(compact('parents'));
    }
  }

  function admin_reorder() {
    debug($this->DealCategory->reorder());
    die;
  }

  function admin_edit($id=null) {
    if (!empty($this->data)) {
      if ($this->DealCategory->save($this->data) == false)
        $this->Session->setFlash('Error saving Category.');
      $this->redirect(array('action' => 'index'));
    } else {
      if ($id == null)
        die("No ID received");
      $this->data = $this->DealCategory->read(null, $id);
      $parents[0] = "[ Top ]";
      $deal_categories = $this->DealCategory->generatetreelist(null, null, null, " - ");
      if ($deal_categories)
        foreach ($deal_categories as $key => $value) {
          if($key != $id)
            $parents[$key] = $value;
        }
      $this->set(compact('parents'));
    }
  }

  function admin_delete($id=null) {
    if ($id == null)
      die("No ID received");
    $this->DealCategory->id = $id;
    $unknownId = Configure::read('DealCategory.unknown_id');

    if($unknownId == $id) {
      $this->Session->setFlash('La categoria Unknown no puede borrarse.');
      return $this->redirect(array('action' => 'index'));
    }
    try {
      $this->DealCategory->deleteCategoryUpdateDeal($id,$unknownId);
      $this->Session->setFlash('Se borro correctamente la categoría.');
    } catch(Exception $e) {
      $this->Session->setFlash('La categoría no pudo borrarse. Error: ' . $e->getMessage());
    }
    $this->redirect(array('action' => 'index'));
  }

}

