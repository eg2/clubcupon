<?php

class FibertelsController extends AppController {

    var $name = 'Fibertels';
    var $components = array('Cookie');
    
    const CLIENT_ID       = '49310de4ac8c4c1a84a7ed8f6cf4afe8';
    const CLIENT_SECRET   = 'f3c4189177b84bedb7bb1ce364c554d1';
    const SERVICIO        = 'Fibertel';
    const COOKIE_DURATION = '86400'; //1 dia de duracion
    
    function index()
    {
        
        //1-Recupero el deal_id (esta cookie se generó al presionar COMPRAR en un deal exclusivo FIBERTEL)
        //Si el usuario verifica OK, vamos a usar el id para confirmar la compra del mismo al redireccionar al metodo buy()
        if(isset($_COOKIE['fibertel_deal_id']))
        {
            $deal_id   = $_COOKIE['fibertel_deal_id'];
        }
        else
        {
            //No se genero la cookie o no existe localmente
            $this->_redirectOnFailedLogin();
        }
        
        
        //2-Recupero los datos obtenidos via URL (UserId & Code)
        //Estos datos vienen de FIBERTEL, luego que el usuario se logueara
        
        if(!empty($this->params['url']['UserId']) && !empty($this->params['url']['Code']))
        {
            //Tenemos los datos en la URL
            $user_id   = $this->params['url']['UserId'];
            $user_code = $this->params['url']['Code'];
            $alreadyCheckedIn = false; //pasa a true si tiene la cookie de verificacion
        }
        else
        {
            //Llegó por error, redireccionamos a la portada
            $this->_redirectOnFailedLogin();
        }
        
        //3-En el caso de que la verificacion haya sido exitosa...
        if(!empty($user_id) && !empty($user_code) )
        {
            //El login de FIBERTEL fue exitoso!, recupero el token
            $token = $this->_getFibertelUserToken($user_code);

            if(!empty($token))
            {
                //El token fue recuperado

                //Seteamos unas cookies para futuros accesos (y evitar los pasos previos...)
                setcookie('fibertelUser'   , $user_id . '---' . $token, time()+ self::COOKIE_DURATION);

                //Consultamos los datos necesarios para confirmar al usuario
                $user_personal_data = $this->_getUserDataByToken($token, $user_id);

                if(!empty($user_personal_data))
                {
                    //Se recuperaron los datos, verificamos que tenga contratado el servicio
                    $id_usuario_cc = $this->Auth->user('id');
                    
                    if($this->_parseAndCheckUserData($user_personal_data, $id_usuario_cc))
                    {
                        
                        //Todos los datos OK!, habilitamos la compra...
                        $this->_redirectToDealBuy($deal_id, true);
                    }
                    else
                    {
                        //No tiene contratado el servicio
                        $this->_redirectOnFailedLogin('Su usuario no parece tener contratado el servicio de FIBERTEL');
                    }

                }
                else
                {
                    //Falla, no se recuperaron los datos, o bien los datos estaban mal, o hubo un problema con la consulta
                    $this->_redirectOnFailedLogin();
                }
            }
            else
            {
                //Falla, no se recupero el TOKEN, redireccionamos
                $this->_redirectOnFailedLogin('No se pudieron verificar los datos de acceso. ');
            }
        }
        else
        {
            //Falla en el login, no se pudo acreditar contra FIBERTEL
            $this->_redirectOnFailedLogin('Fallo en la verificaci&oacute;n de datos de usuario para FIBERTEL. ');
        }
     
        
    
    }
    
    // INTERNALS //////////////////////////////////////////////////////////////////////////////////
    
    //Devuelve el token acorde a un user_code
    function _getFibertelUserToken($user_code)
    {
        $token_url = 'http://www.fibertel.com.ar/access-token.ashx?client_id=' . self::CLIENT_ID . '&client_secret=' . self::CLIENT_SECRET . '&code=' .  $user_code;

        //cURL
        $ch = curl_init ($token_url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt ($ch, CURLOPT_CAINFO, Configure::read ('CertificateBundlePath'));
        $response = curl_exec ($ch);
        curl_close ($ch);
        
        return $response;
    }

    //Devuelve un arreglo con los datos de usuario
    function _getUserDataByToken($token, $user_id)
    {
        $user_data_url = 'http://crm.fibertel.com.ar/webservice_2/personapi.aspx?access_token=' . $token . '&user_id=' . $user_id;
        
        //cURL
        $ch = curl_init ($user_data_url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt ($ch, CURLOPT_CAINFO, Configure::read ('CertificateBundlePath'));
        $response = curl_exec ($ch);
        curl_close ($ch);
        
        return $response;
    }
    
    
    //Recibe el array con todos los datos de usuario, los parsea y devuelve si estan verificados
    function _parseAndCheckUserData($string_datos, $id_usuario_cc)
    {
        
        $json_object = json_decode($string_datos, true);

        $brandsProducts       = $json_object['Person'][0]['BrandsProducts'];
        $fibertelUserId       = $json_object['Person'][0]['Id'];
        $fibertelUserJobTitle = $json_object['Person'][0]['JobTitle'];
        //
        $brandsProducts = strtolower($brandsProducts);
        $brandsProducts = explode(',', $brandsProducts);
        $fibertel_service_available = in_array(strtolower(self::SERVICIO), $brandsProducts) ? true : false;
        
        //Analizamos si se dan las dos condiciones, (Jobtitle y el servicio)
        $result = !empty($fibertelUserJobTitle) && $fibertel_service_available == true ? true : false;
        
        //Guardamos el userId de fibertel en el modelo USER (se sua luego para auditoria)
        $this->updateUserFibertetlId($id_usuario_cc, $fibertelUserId );
        
        return $result;
        
    }
    
    //Redirecciona en caso de que algo falle...
    function _redirectOnFailedLogin($warning_message = 'Falla en la verificacion de usuario de Fibertel, por favor, intentelo de nuevo')
    {
        //Fallo el login de FIBERTEL, redireccionamos
        $this->Session->setFlash($warning_message, 'default', null, 'error');
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'index',
            'admin' => false,
            ));
    }
    
    //Redirecciona a un deal especifico
    function  _redirectToDealBuy($deal_id, $verification = false)
    {
        
        $this->Cookie->write('fibertelUserLogged', 1, $encrypt = false, $expires = null);
        
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'buy_without_subdeal',
            $deal_id ,
            1,
            null, 
            null, 
            $verification,
            ));
    }
    
    //Analiza la existencia de la cookie de verificacion de usuario, y que los datos esten completos.
    function _checkuserCookies()
    {
        if (isset($_COOKIE['fibertelUser']))
        {
            
            $cookie_values = $_COOKIE['fibertelUser'];
            $datos = $this->parseUserCookie($cookie_values);
            
            if(!empty($datos[0]) && !empty($datos[1]))
            {
                //Los datos estan, este usuario ya paso por la verificacion antes...
                return true;
            }
            else
            {
                //La cookie existe, pero por alguna razon los datos estan incompletos...
                return false;
            }
        }
        else
        {
            //No existen las cookies...
            return false;
        }
    }
    
    //Devuelve un array con los datos de la cookie ordenados
    function parseUserCookie($cookieValue)
    {
        $datos = explode("---", $cookie_values);
        return $datos;
    }
    
    function updateUserFibertetlId($user_id, $fibertel_id)
    {
        
        App::import('Model', 'user');
        $this->User = & new User();
        $update_query = 'UPDATE `users` SET `fibertel_user_id` = \''.$fibertel_id.'\' WHERE `users`.`id` ='.$user_id;
        $this->User->query($update_query);
        
    }
    
}
