<?php
class RatesController extends AppController
  {
    var $name = 'Rates';

    function admin_index ()
      {
        // Setear el título de la página.
        $this->pageTitle = 'Razones de Conversi&oacute;n';

        // Buscar todas las razones y pasar la matriz "rates" a la view.
        $this->set ('rates', $this->Rate->find ('all', array ('recursive' => -1, 'order' => array ('Rate.created' => 'desc'))));
      }


    function admin_edit ()
      {
        // Indicamos que por defecto utilice la conexion master a la DB
        AppModel::setDefaultDbConnection ('master');

        // Setear el título
        $this->pageTitle = 'Editar Raz&oacute;n de Conversi&oacute;n';

        // Dependiendo de si es la primera llamada o la segunda (la que tiene datos)...
        if (!empty ($this->data))
          // Validar y guardar, si se falla, fallar
          if ($this->Rate->validates ())
            {
              $this->Rate->create (); unset ($this->data ['Rate']['id']);
              if ($this->Rate->save ($this->data))
                {
                  $this->Session->setFlash ('La Raz&oacute;n fue modificada', 'default', null, 'success');
                  $this->redirect (array ('contorller' => 'rates', 'action' => 'index', 'admin' => 'true'));
                }
              else  $this->Session->setFlash ('La Raz&oacute;n no se pudo modificar, por favor, int&eacute;ntelo nuevamente.', 'default', null, 'error');
            }
          else  $this->Session->setFlash ('La Raz&oacute;n no se pudo modificar, por favor, int&eacute;ntelo nuevamente.', 'default', null, 'error');
        else
          {
            // Buscar la Razón más reciente, fallar en caso de que no se encuentre
            $this->data = $this->Rate->find ('first', array ('order' => array ('Rate.created' => 'desc'), 'recursive' => -1));
            if (empty ($this->data))  $this->cakeError ('error404');
          }

        return;
      }
  }
?>