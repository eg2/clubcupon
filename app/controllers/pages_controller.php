<?php

if (Configure::read('app.rDebug.enable')) {
    App::import('Vendor', 'web.ref');
}
App::import('Component', 'web.WebViewDataCreator');
App::import('Vendor', 'web.commons');

class PagesController extends AppController {

    var $name = 'Pages';

    function beforeFilter() {
        $this->Security->disabledFields = array(
            'Page.id',
            'Page.Update',
            'Page.Add',
            'Page.content',
            'Page.description_meta_tag',
            'Page.slug',
            'Page.title'
        );
        parent::beforeFilter();
    }

    function admin_add() {
        AppModel::setDefaultDbConnection('master');
  
        if (!empty($this->data)) {
        	//$this->data['content']=$this->changeImgDomainContent($this->data['content']);
        	$this->data['Page']['content']=$this->changeImgDomainContent($this->data['Page']['content']);
            $this->Page->set($this->data);
            if ($this->Page->validates()) {
                $this->Page->save($this->data);
                $this->Session->setFlash('La pagina se guardo correctamente', 'default', null, 'success');
                $page_id = $this->Page->getLastInsertId();
                if ($this->data['Page']['Preview']) {
                    $page_slug = $this->Page->find('first', array(
                        'conditions' => array(
                            'Page.id' => $page_id
                        ),
                        'fields' => array(
                            'Page.slug'
                        ),
                        'recursive' => 1
                    ));
                    $this->redirect(array(
                        'controller' => 'pages',
                        'action' => 'view',
                        'type' => 'preview',
                        $page_slug['Page']['slug']
                    ));
                } else
                    $this->redirect(array(
                        'action' => 'index'
                    ));
            }
            else {
                $this->Session->setFlash('No se pudo guardar la pagina.', 'default', null, 'error');
            }
        }
        $templates = array();
        $template_files = glob(APP . 'views' . DS . 'pages' . DS . 'themes' . DS . '*.ctp');
        if (!empty($template_files)) {
            foreach ($template_files as $template_file) {
                $templates[basename($template_file)] = basename($template_file);
            }
        }
        $statusOptions = $this->Page->statusOptions;
        $this->set(compact('parentIdOptions', 'templates', 'statusOptions'));
    }

    function admin_edit($id = null) {
        AppModel::setDefaultDbConnection('master');
        if (!empty($this->data)) {
        	
        	$this->data['Page']['content']=$this->changeImgDomainContent($this->data['Page']['content']);
        	 
            $this->Page->set($this->data);
            if ($this->Page->validates()) {
                $this->Page->save($this->data);
                $this->Session->setFlash('La pagina se actualizo correctamente', 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash('No se pudo actualizar la pagina.', 'default', null, 'error');
            }
        } else {
            $this->data = $this->Page->read(null, $id);
            $pageFindReplace = array(
            		'##IMG_URL##' => STATIC_DOMAIN_FOR_CLUBCUPON,
            );
            $this->data['Page']['content'] = strtr($this->data['Page']['content'], $pageFindReplace);
        }
    }

    function admin_index($like) {
        $this->pageTitle = 'P&aacute;ginas';
        $this->Page->recursive = - 1;
        $title = $this->data['Page']['title'];
        $conditions = array(
            'order' => array(
                'Page.id' => 'DESC'
            )
        );
        if (!empty($title)) {
            $this->paginate = array(
                'order' => array(
                    'Page.id' => 'DESC'
                ),
                'conditions' => array(
                    "title LIKE" => "%$title%"
                )
            );
        } else {
            $this->paginate = array(
                $conditions
            );
        }
        $this->set('pages', $this->paginate());
    }

    function admin_delete($id = null, $cancelled = null) {
        AppModel::setDefaultDbConnection('master');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Page->del($id)) {
            $this->Session->setFlash('Se elimino la pagina', 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index',
                $cancelled
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function admin_view($slug = null) {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('view', $slug);
    }

    private function getNewImgDomain($img){
    	$pos_inicial=strrpos($img,"http://");
    	if($pos_inicial === false){
    		return $img;
    	}else{
    		$pos_final=strpos($img,"/",$pos_inicial+7);
    			
    		
    		$new_url=substr($img,0,$pos_inicial)."##IMG_URL##".substr($img,$pos_final);
    		
    		return $new_url;
    	}
    	
    }
    private function changeImgDomainContent($contenido){
    	$array = explode("<img", $contenido);
    	if(count($array)<2){
    		return $contenido;
    	}else{
    		$i=0;
    		foreach ($array as &$valor) {
    			echo $i.'<br>';
    			if($i>0){
    				$valor=$this->getNewImgDomain($valor);
    				//$this->getNewImgDomain($valor);
    			}
    			$i++;
    		}
    		$contenido2 = implode("<img", $array);
    		return $contenido2;
    	}
    	 
    }
    function view($slug = null) {
    	
        $this->Page->recursive = - 1;
        if (!empty($slug)) {
            $page = $this->Page->findBySlug($slug);
        } else {
            $page = $this->Page->find('first', array(
                'conditions' => array(
                    'Page.is_default' => 1
                )
            ));
            $slug = $page['Page']['slug'];
        }
        $this->set('certificaPath', 'ClubCupon/' . $slug);
        $title = Inflector::humanize($slug);
        $pageFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##SITE_URL##' => Router::url('/', true) . '?from=mailing',
            '##ABOUT_US_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'about',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##CONTACT_US_URL##' => Router::url(array(
                'controller' => 'contacts',
                'action' => 'add',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##FAQ_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'faq',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##SITE_CONTACT_PHONE##' => Configure::read('site.contact_phone'),
            '##SITE_CONTACT_EMAIL##' => Configure::read('site.contact_email'),
            'src="http://localhost' => 'src="',
            'src="http://clubcupon.com.ar' => 'src="',
            'src="http://www.clubcupon.com.ar' => 'src="',
        	/*'##IMG_URL##' => Router::url('/', true),*/
        	'##IMG_URL##' => STATIC_DOMAIN_FOR_CLUBCUPON,
        );
      
        if ($page && $page['Page']['draft'] != 0) {
            $page['Page']['title'] = strtr($page['Page']['title'], $pageFindReplace);
            $this->pageTitle = $page[$this->modelClass]['title'];
            $page['Page']['content'] = strtr($page['Page']['content'], $pageFindReplace);
            $this->set('page', $page);
            $this->set('title_for_layout', $page[$this->modelClass]['title']);
            $this->set('currentPageId', $page[$this->modelClass]['id']);
            $this->set('isPage', true);
            $this->set('slug', $page['Page']['slug']);
            if (in_array($title, array(
                        'Learn',
                        'Faq',
                        'Who',
                        'Terms',
                        'Policy',
                        'Protection',
                        'tocya',
                        'concurso'
                    ))) {
                $this->set('certificaPath', 'ClubCupon/page/' . $title);
            }
            $cSlug = $this->params['named']['city'];
            $myCity = new City();
            $city = $myCity->findBySlug($cSlug);
            if ($city && $city['City']['is_corporate'] == 1 && Configure::read('web.plugin.enable')) {
                $this->set('isBusinessUnit', $city['City']['is_business_unit']);
                $this->set('imageURL', $city['City']['image']);
                $this->set('citySlug', $city['City']['slug']);
                $usr = new stdClass();
                $usr->userEmail = $this->Auth->user('email');
                $this->set('user', $usr);
                $this->layout = 'exclusivePages';
            }
            if ($city && $city['City']['is_corporate'] == 0 && Configure::read('web.plugin.enable')) {
                $viewParams['citySlug'] = $city['City']['slug'];
                $viewParams['lastGeoCitySlug'] = $this->Cookie->read('geo_city_slug');
                $viewParams['userType'] = $this->Auth->user('user_type_id');
                $viewParams['userEmail'] = $this->Auth->user('email');
                $this->setViewType($title, $viewParams);
                $viewParams['url'] = $this->params['url']['url'];
                $viewParams['userPIN'] = $this->Cookie->read('PIN');
                $viewParams['userID'] = $this->Auth->user('id');
                $WebViewDataCreator = new WebViewDataCreatorComponent();
                $data = $WebViewDataCreator->createData($viewParams);
                if (empty($data)) {
                    $this->cakeError('error404');
                }
                $this->set('data', json_encode($data));
                $this->layout = 'clubcupon';
            }
        } else {
            $this->cakeError('error404');
        }
    }

    private function setViewType($title, &$viewParams) {
        switch ($title) {
            case 'Who':
                $viewParams['viewType'] = WebViewDataCreatorComponent::PAGE_WHO;
                break;

            case 'Terms':
                $viewParams['viewType'] = WebViewDataCreatorComponent::PAGE_TERMS;
                break;

            case 'Policy':
                $viewParams['viewType'] = WebViewDataCreatorComponent::PAGE_POLICY;
                break;

            case 'Protection':
                $viewParams['viewType'] = WebViewDataCreatorComponent::PAGE_PROTECTION;
                break;

            default:
                $viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
                break;
        }
    }

    function preview() {
        $this->layout = 'print';
        $title = (empty($_POST['data']['Page']['title'])) ? 'Ingresar T&iacute;tulo' : $_POST['data']['Page']['title'];
        $pageContent = (empty($_POST['data']['Page']['content'])) ? 'Ingresar Contenido' : $_POST['data']['Page']['content'];
        $pageFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##SITE_URL##' => Router::url('/', true) . '?from=mailing',
            '##ABOUT_US_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'about',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##CONTACT_US_URL##' => Router::url(array(
                'controller' => 'contacts',
                'action' => 'add',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##FAQ_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'faq',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##SITE_CONTACT_PHONE##' => Configure::read('site.contact_phone'),
            '##SITE_CONTACT_EMAIL##' => Configure::read('site.contact_email'),
            'src="http://localhost' => 'src="',
            'src="http://clubcupon.com.ar' => 'src="',
            'src="http://www.clubcupon.com.ar' => 'src="'
        );
        $title = strtr($title, $pageFindReplace);
        $pageContent = strtr($pageContent, $pageFindReplace);
        $this->set('pageContent', $pageContent);
        $this->set('pageTitle', $title);
    }

    function links() {
        $pages = $this->Page->find('active_disclaimer_pages');
        foreach ($pages as & $page) {
            $page['Page']['title'] = $this->replace_strings($page['Page']['title']);
        }
        $this->set('pages', $pages);
    }

    function replace_strings($text) {
        $pageFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##SITE_URL##' => Router::url('/', true) . '?from=mailing',
            '##ABOUT_US_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'about',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##CONTACT_US_URL##' => Router::url(array(
                'controller' => 'contacts',
                'action' => 'add',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##FAQ_URL##' => Router::url(array(
                'controller' => 'pages',
                'action' => 'view',
                'faq',
                'city' => $this->params['named']['city'],
                'admin' => false
                    ), true) . '?from=mailing',
            '##SITE_CONTACT_PHONE##' => Configure::read('site.contact_phone'),
            '##SITE_CONTACT_EMAIL##' => Configure::read('site.contact_email'),
            'src="http://localhost' => 'src="',
            'src="http://clubcupon.com.ar' => 'src="',
            'src="http://www.clubcupon.com.ar' => 'src="'
        );
        $text = strtr($text, $pageFindReplace);
        return $text;
    }

    function landingmobile() {
        $this->layout = 'clubcupon';
        $this->generateViewParams();
    }

    private function generateViewParams() {
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
        $this->viewParams['lastGeoCitySlug'] = $this->Cookie->read('geo_city_slug');
        $this->viewParams['userEmail'] = $this->Auth->user('email');
        $this->viewParams['userType'] = $this->Auth->user('user_type_id');
        $this->viewParams['userPIN'] = $this->Cookie->read('PIN');
        $this->viewParams['userID'] = $this->Auth->user('id');
        $this->viewParams['url'] = $this->params['url']['url'];
        $this->WebViewDataCreator = new WebViewDataCreatorComponent();
        $Data = $this->WebViewDataCreator->createData($this->viewParams);
        $this->set('data', json_encode($Data));
    }

}
