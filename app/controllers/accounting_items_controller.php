<?php

App::import('Component', 'accounting.AccountingCalendarService');
App::import('Model', 'accounting.AccountingCalendar');

class AccountingItemsController extends AppController {

    var $name = 'AccountingItems';
    var $uses = array(
        'accounting.AccountingItem',
        'DealUser'
    );

    function admin_search() {
        $calendar_id = $this->data['AccountingItem']['accounting_calendar_id'];
        $filter = $this->data['AccountingItem']['filter'];
        $q = trim($this->data['AccountingItem']['q']);
        if ($filter == 'model')
            $q = $this->data['AccountingItem']['filter_mode'];
        if (empty($calendar_id) && isset($this->params['named']['calendar_id']))
            $calendar_id = $this->params['named']['calendar_id'];
        if (empty($filter) && isset($this->params['named']['filter']))
            $filter = $this->params['named']['filter'];
        if (empty($q) && isset($this->params['named']['q']))
            $q = $this->params['named']['q'];
        if (isset($q) && !empty($q)) {
            $this->paginate = array(
                'conditions' => array(
                    'AccountingItem.accounting_calendar_id' => $calendar_id,
                    'AccountingItem.' . $filter . ' like' => $q
                ),
                'fields' => array(
                    'AccountingItem.id',
                    'AccountingItem.total_quantity',
                    'AccountingItem.total_amount',
                    'AccountingItem.deal_id',
                    'AccountingItem.model',
                    'AccountingItem.model_id',
                    'AccountingItem.accounting_calendar_id',
                    "ifnull((select AccountingError.description from accounting_errors as AccountingError where AccountingError.accounting_item_id = AccountingItem.id limit 1), 'no') as error"
                ),
                'order' => array(
                    'AccountingItem.id' => 'desc'
                ),
                'limit' => 20
            );
            $this->AccountingCalendar = new AccountingCalendar();
            $calendar = $this->AccountingCalendar->find('first', array(
                'recursive' => - 1,
                'fields' => array(
                    'AccountingCalendar.id',
                    'AccountingCalendar.since',
                    'AccountingCalendar.until',
                    'AccountingCalendar.execution',
                    'AccountingCalendar.status'
                ),
                'conditions' => array(
                    'AccountingCalendar.id' => $calendar_id
                )
            ));
            $items = $this->paginate();
            $this->set('calendar', $calendar);
            $this->set('items', $items);
            $this->set('param_search', 'filter:' . $filter . '/q:' . $q . '/calendar_id:' . $calendar_id);
            $this->render('admin_index');
        } else {
            $this->Session->setFlash('Debe ingresar alg&uacute;n valor para la b&uacute;squeda', 'default', null, 'error');
            if (empty($this->data['AccountingItem']['param_search'])) {
                $this->redirect(array(
                    'controller' => 'accounting_items',
                    'action' => 'index',
                    $calendar_id
                ));
            } else {
                $param_search = $this->data['AccountingItem']['param_search'];
                $pieces = explode('/', $param_search);
                $pieces0 = explode(':', $pieces[0]);
                $pieces1 = explode(':', $pieces[1]);
                $pieces2 = explode(':', $pieces[2]);
                $filter = $pieces0[1];
                $q = $pieces1[1];
                $calendar_id = $pieces2[1];
                $this->paginate = array(
                    'conditions' => array(
                        'AccountingItem.accounting_calendar_id' => $calendar_id,
                        'AccountingItem.' . $filter . ' like' => $q
                    ),
                    'fields' => array(
                        'AccountingItem.id',
                        'AccountingItem.total_quantity',
                        'AccountingItem.total_amount',
                        'AccountingItem.deal_id',
                        'AccountingItem.model',
                        'AccountingItem.model_id',
                        'AccountingItem.accounting_calendar_id',
                        "ifnull((select AccountingError.description from accounting_errors as AccountingError where AccountingError.accounting_item_id = AccountingItem.id limit 1), 'no') as error"
                    ),
                    'order' => array(
                        'AccountingItem.id' => 'desc'
                    ),
                    'limit' => 20
                );
                $this->AccountingCalendar = new AccountingCalendar();
                $calendar = $this->AccountingCalendar->find('first', array(
                    'recursive' => - 1,
                    'fields' => array(
                        'AccountingCalendar.id',
                        'AccountingCalendar.since',
                        'AccountingCalendar.until',
                        'AccountingCalendar.execution',
                        'AccountingCalendar.status'
                    ),
                    'conditions' => array(
                        'AccountingCalendar.id' => $calendar_id
                    )
                ));
                $items = $this->paginate();
                $this->set('calendar', $calendar);
                $this->set('items', $items);
                $this->set('param_search', 'filter:' . $filter . '/q:' . $q . '/calendar_id:' . $calendar_id);
                $this->render('admin_index');
            }
        }
    }

    function admin_index($calendar_id) {
        $this->pageTitle = 'Items';
        $this->paginate = array(
            'conditions' => array(
                'AccountingItem.accounting_calendar_id' => $calendar_id
            ),
            'fields' => array(
                'AccountingItem.id',
                'AccountingItem.total_quantity',
                'AccountingItem.total_amount',
                'AccountingItem.deal_id',
                'AccountingItem.model',
                'AccountingItem.model_id',
                'AccountingItem.accounting_calendar_id',
                "ifnull((select AccountingError.description from accounting_errors as AccountingError where AccountingError.accounting_item_id = AccountingItem.id limit 1), 'no') as error"
            ),
            'order' => array(
                'AccountingItem.id' => 'desc'
            ),
            'limit' => 20
        );
        $this->AccountingCalendar = new AccountingCalendar();
        $calendar = $this->AccountingCalendar->find('first', array(
            'recursive' => - 1,
            'fields' => array(
                'AccountingCalendar.id',
                'AccountingCalendar.since',
                'AccountingCalendar.until',
                'AccountingCalendar.execution',
                'AccountingCalendar.status'
            ),
            'conditions' => array(
                'AccountingCalendar.id' => $calendar_id
            )
        ));
        $items = $this->paginate();
        $this->set('calendar', $calendar);
        $this->set('items', $items);
    }

    function admin_exclude($item_id) {
        $this->AccountingCalendarService = new AccountingCalendarServiceComponent();
        $item = $this->AccountingItem->find('first', array(
            'recursive' => - 1,
            'fields' => array(
                'AccountingItem.id',
                'AccountingItem.accounting_calendar_id',
                'AccountingItem.deal_id'
            ),
            'conditions' => array(
                'AccountingItem.id' => $item_id
            )
        ));
        try {
            $this->AccountingCalendarService->excludeAccountingItem($item);
            $this->Session->setFlash('Se excluyo el item del calendario', 'default', null, 'success');
        } catch (Exception $exc) {
            $this->Session->setFlash('No se pudo excluir el item', 'default', null, 'error');
        }
        $this->redirect(array(
            'controller' => 'accounting_items',
            'action' => 'index',
            $item['AccountingItem']['accounting_calendar_id']
        ));
    }

    function admin_cancel($item_id, $calendar_id) {
        $this->autoRender = false;
        $this->AccountingCalendarService = new AccountingCalendarServiceComponent();
        try {
            $this->AccountingCalendarService->cancel($item_id);
            $this->Session->setFlash('Se cancelo con exito', 'default', null, 'success');
        } catch (Exception $exc) {
            $this->Session->setFlash('No se pudo cancelar', 'default', null, 'error');
        }
        $this->redirect(array(
            'controller' => 'accounting_items',
            'action' => 'index',
            $calendar_id
        ));
    }

}
