<?php
class TranslationsController extends AppController
{
    var $name = 'Translations';
    function beforeFilter()
    {
        $this->Security->disabledFields = array(
            'Translation.googleTranslate',
            'Translation.manualTranslate',
            'Translation.from_language',
            'Translation.makeUpdate',
            'Translation.makeSubmit'
        );
        // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }
    function admin_index()
    {
        $this->pageTitle = __l('Translations');
        if (!empty($this->params['named']['remove_language_id'])) {
            $this->Translation->deleteAll(array(
                'Translation.language_id' => $this->params['named']['remove_language_id']
            ));
            $lang_code = $this->Translation->Language->find('first', array(
                'conditions' => array(
                    'Language.id' => $this->params['named']['remove_language_id']
                ) ,
                'fields' => array(
                    'Language.iso2'
                ) ,
                'recursive' => -1
            ));
            Cache::delete($lang_code['Language']['iso2'] . '_translations');
            $this->Session->setFlash(__l('Translation deleted successfully') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $translations = $this->Translation->find('all', array(
            'fields' => array(
                'Language.name',
                'Translation.language_id',
                'Translation.key',
                'Translation.is_translated',
                'Translation.is_google_translate',
            ) ,
            'conditions' => array(
                'Translation.language_id !=' => 0
            ) ,
            'group' => 'Translation.language_id'
        ));
        $this->set('translations', $translations);
    }
    function admin_view($id = null)
    {
        $this->pageTitle = __l('Translation');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $translation = $this->Translation->find('first', array(
            'conditions' => array(
                'Translation.id = ' => $id
            ) ,
            'fields' => array(
                'Translation.id',
                'Translation.created',
                'Translation.modified',
                'Translation.language_id',
                'Translation.key',
                'Translation.lang_text',
                'Language.id',
                'Language.created',
                'Language.modified',
                'Language.name',
                'Language.iso2',
                'Language.iso3',
            ) ,
            'recursive' => 0,
        ));
        if (empty($translation)) {
            $this->cakeError('error404');
        }
        $this->pageTitle.= ' - ' . $translation['Translation']['id'];
        $this->set('translation', $translation);
    }
    function admin_add()
    {
        $this->pageTitle = __l('Add Translation');
        $translations = $this->Translation->find('all', array(
            'conditions' => array(
                'Language.iso2' => 'en'
            ) ,
            'recursive' => 0
        ));
        if (empty($translations)) {
            $this->Session->setFlash(__l('Default English variable is missing') , 'default', null, 'error');
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (!empty($this->data)) {
            if (!empty($this->data['Translation']['googleTranslate']) && !empty($this->data['Translation']['language_id'])) {
                $new_language = $this->Translation->Language->find('first', array(
                    'conditions' => array(
                        'Language.id' => $this->data['Translation']['language_id']
                    ) ,
                    'fields' => array(
                        'Language.iso2'
                    ) ,
                    'recursive' => -1
                ));
                for ($i = 0; $i < count($translations); $i+= 20) {
                    $key = '';
                    for ($j = $i; $j < $i+20; $j++) {
                        if (isset($translations[$j]['Translation']['key'])) {
                            $key.= 'q=' . urlencode($translations[$j]['Translation']['key']) . '&';
                        }
                    }
                    $var = json_decode(file_get_contents('http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&' . $key . 'langpair=en%7C' . $new_language['Language']['iso2']));
                    if ($var->responseStatus == 200) {
                        $j = $i;
                        foreach($var->responseData as $translated_arr) {
                            if ($translated_arr->responseStatus == 200) {
                                $this->data['Translation']['language_id'] = $this->data['Translation']['language_id'];
                                $this->data['Translation']['key'] = $translations[$j]['Translation']['key'];
                                $this->data['Translation']['lang_text'] = $translated_arr->responseData->translatedText;
                                $this->data['Translation']['is_translated'] = 1;
                                $this->data['Translation']['is_google_translate'] = 1;
                                $this->Translation->create();
                                $this->Translation->save($this->data);
                            }
                            $j++;
                        }
                    }
                }
            } elseif (!empty($this->data['Translation']['manualTranslate']) && !empty($this->data['Translation']['language_id'])) {
                foreach($translations as $translation) {
                    unset($translation['Translation']['id']);
                    $translation['Translation']['language_id'] = $this->data['Translation']['language_id'];
                    $translation['Translation']['lang_text'] = '';
                    $translation['Translation']['is_translated'] = 1;
                    $this->Translation->create();
                    $this->Translation->save($translation);
                }
            }
            $this->Session->setFlash(__l('Translation has been added') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'manage',
                'language_id' => $this->data['Translation']['language_id']
            ));
        }
        $existTranslations = $this->Translation->find('all', array(
            'fields' => array(
                'DISTINCT(Translation.language_id)',
                'Language.name'
            )
        ));
        $languages = $this->Translation->Language->find('list', array(
            'conditions' => array(
                'Language.is_active' => 1
            )
        ));
        $exists = array();
        if (!empty($existTranslations)) {
            foreach($existTranslations as $existTranslation) {
                $exists[] = $existTranslation['Translation']['language_id'];
                unset($languages[$existTranslation['Translation']['language_id']]);
            }
            $exists[] = array_search('English', $languages);
            unset($languages[array_search('English', $languages) ]);
        }
        $this->set(compact('languages'));
    }
    function admin_edit($id = null)
    {
        $this->pageTitle = __l('Edit Translation');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->Translation->save($this->data)) {
                $this->Session->setFlash(sprintf(__l('"%s" Translation has been updated') , $this->data['Translation']['id']) , 'default', null, 'success');
            } else {
                $this->Session->setFlash(sprintf(__l('"%s" Translation could not be updated. Please, try again.') , $this->data['Translation']['id']) , 'default', null, 'error');
            }
        } else {
            $this->data = $this->Translation->read(null, $id);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['Translation']['id'];
        $languages = $this->Translation->Language->find('list', array(
            'conditions' => array(
                'Language.is_active' => 1
            )
        ));
        $this->set(compact('languages'));
    }
    function admin_delete($id = null)
    {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Translation->del($id)) {
            $this->Session->setFlash(__l('Translation deleted') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }
    function admin_manage()
    {
        $this->pageTitle = __l('Edit Translations');
        if (isset($this->params['named']['language_id']) && !empty($this->params['named']['language_id'])) {
            $this->data['Translation']['language_id'] = $this->params['named']['language_id'];
        }
        if (!empty($this->data)) {
            if (!empty($this->data['Translation'])) {
                foreach($this->data['Translation'] as $key => $value) {
                    $this->Translation->id = $key;
                    $this->Translation->saveField('lang_text', $value['lang_text']);
                }
                if (!empty($this->data['Translation']['makeUpdate']) and $this->data['Translation']['makeUpdate']) {
                    $this->Session->setFlash(__l('Translation updated successfully') , 'default', null, 'success');
                }
            }
            $this->params['named']['language_id'] = $this->data['Translation']['language_id'];
            $lang_code = $this->Translation->Language->find('first', array(
                'conditions' => array(
                    'Language.id' => $this->params['named']['language_id']
                ) ,
                'fields' => array(
                    'Language.iso2'
                ) ,
                'recursive' => -1
            ));
            Cache::delete($lang_code['Language']['iso2'] . '_translations');
            $this->paginate = array(
                'conditions' => array(
                    'Translation.language_id = ' => $this->data['Translation']['language_id']
                ) ,
                'fields' => array(
                    'Translation.id',
                    'Translation.language_id',
                    'Translation.key',
                    'Translation.lang_text'
                ) ,
                'recursive' => -1,
            );
            $this->set('translations', $this->paginate());
        }
        $translations = $this->Translation->find('all', array(
            'fields' => array(
                'DISTINCT(Translation.language_id)',
                'Language.name'
            ) ,
            'conditions' => array(
                'Translation.language_id !=' => 0
            )
        ));
        foreach($translations as $translation) {
            $languages[$translation['Translation']['language_id']] = $translation['Language']['name'];
        }
        $this->set(compact('languages'));
    }
}
?>