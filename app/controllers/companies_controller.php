<?php

class CompaniesController extends AppController {

    var $name = 'Companies';
    var $components = array(
        'Email'
    );
    var $uses = array(
        'Company',
        'EmailTemplate',
        'Attachment',
        'CompanyCandidate'
    );

    function beforeFilter() {
        $securityDisabled = array(
            'admin_search'
        );
        if (in_array($this->action, $securityDisabled)) {
            $this->Security->validatePost = false;
        }
        $this->Security->disabledFields = array(
            'City',
            'FiscalCity',
            'FiscalState',
            'State',
            'Company.latitude',
            'Company.longitude',
            'UserAvatar.filename',
            'User.id',
            'Company.id',
            'Company.address1',
            'Company.address2',
            'Company.country_id',
            'Company.name',
            'Company.phone',
            'Company.url',
            'Company.zip',
            'Company.fiscal_name',
            'Company.fiscal_address',
            'Company.fiscal_phone',
            'Company.contact_phone',
            'Company.fiscal_fax',
            'Company.fiscal_bank',
            'Company.fiscal_bank_account',
            'Company.fiscal_bank_cbu',
            'Company.fiscal_cond_iva',
            'Company.fiscal_cuit',
            'Company.fiscal_iibb',
            'Company.percepcion_iibb_caba',
            'Company.declaracion_jurada_terceros',
            'Company.persona',
            'Company.cheque_noorden'
        );
        AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }

    function view($slug = null) {
        $this->pageTitle = __l('Company');
        if (is_null($slug)) {
            $this->cakeError('error404');
        }
        $conditions['Company.slug'] = $slug;
        $company = $this->Company->find('first', array(
            'conditions' => $conditions,
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.email',
                        'User.user_type_id',
                        'User.username',
                        'User.id',
                        'User.available_balance_amount',
                        'User.is_email_confirmed',
                        'User.is_active'
                    ),
                    'UserAvatar'
                ),
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug'
                    )
                ),
                'State' => array(
                    'fields' => array(
                        'State.id',
                        'State.name'
                    )
                ),
                'Country' => array(
                    'fields' => array(
                        'Country.id',
                        'Country.name',
                        'Country.slug'
                    )
                ),
                'Deal' => array(
                    'conditions' => array(
                        'Deal.deal_status_id' => array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                            ConstDealStatus::Closed,
                            ConstDealStatus::PaidToCompany
                        )
                    ),
                    'fields' => array(
                        'Deal.id',
                        'Deal.name',
                        'Deal.slug',
                        'Deal.description'
                    ),
                    'limit' => 5
                )
            ),
            'recursive' => 2
        ));
        if ($this->RequestHandler->prefers('kml')) {
            $this->set('company', $company);
        } else {
            $statistics = array();
            $statistics['referred_users'] = $this->Company->User->find('count', array(
                'conditions' => array(
                    'User.referred_by_user_id' => $company['Company']['user_id']
                )
            ));
            $statistics['deal_created'] = $this->Company->Deal->find('count', array(
                'conditions' => array(
                    'OR' => array(
                        'Deal.user_id' => $company['Company']['user_id'],
                        'Deal.company_id' => $company['Company']['id']
                    ),
                    'Deal.deal_status_id' => array(
                        ConstDealStatus::Open,
                        ConstDealStatus::Tipped,
                        ConstDealStatus::Closed,
                        ConstDealStatus::PaidToCompany
                    )
                )
            ));
            $statistics['deal_purchased'] = $this->Company->User->DealUser->find('count', array(
                'conditions' => array(
                    'DealUser.user_id' => $company['Company']['user_id'],
                    'DealUser.is_gift' => 0
                )
            ));
            $statistics['gift_sent'] = $this->Company->User->GiftUser->find('count', array(
                'conditions' => array(
                    'GiftUser.user_id' => $company['Company']['user_id']
                )
            ));
            $statistics['gift_received'] = $this->Company->User->GiftUser->find('count', array(
                'conditions' => array(
                    'GiftUser.friend_mail' => $company['Company']['user_id']
                )
            ));
            $statistics['user_friends'] = $this->Company->User->UserFriend->find('count', array(
                'conditions' => array(
                    'UserFriend.user_id' => $company['Company']['user_id'],
                    'UserFriend.friend_status_id' => 2,
                    'UserFriend.is_requested' => 0
                )
            ));
            if (empty($company)) {
                $this->cakeError('error404');
            }
            $this->set('statistics', $statistics);
            $this->pageTitle.= ' - ' . $company['Company']['name'];
            $this->set('company', $company);
        }
    }

    function edit($id = null) {
        $this->pageTitle = __l('Edit Company');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $this->Company->User->UserAvatar->Behaviors->attach('ImageUpload', Configure::read('avatar.file'));
        if (!empty($this->data)) {
            $user = $this->Company->User->find('first', array(
                'conditions' => array(
                    'User.id' => $this->data['User']['id']
                ),
                'contain' => array(
                    'UserAvatar' => array(
                        'fields' => array(
                            'UserAvatar.id',
                            'UserAvatar.filename',
                            'UserAvatar.dir',
                            'UserAvatar.width',
                            'UserAvatar.height'
                        )
                    )
                ),
                'recursive' => 0
            ));
            if (!empty($user['UserAvatar']['id'])) {
                $this->data['UserAvatar']['id'] = $user['UserAvatar']['id'];
            }
            if (!empty($this->data['UserAvatar']['filename']['name'])) {
                $this->data['UserAvatar']['filename']['type'] = get_mime($this->data['UserAvatar']['filename']['tmp_name']);
            }
            if (!empty($this->data['UserAvatar']['filename']['name']) || (!Configure::read('avatar.file.allowEmpty') && empty($this->data['UserAvatar']['id']))) {
                $this->Company->User->UserAvatar->set($this->data);
            }
            $ini_upload_error = 1;
            if (@$this->data['UserAvatar']['filename']['error'] == 1) {
                $ini_upload_error = 0;
            }
            $this->data['Company']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['City']['name']);
            $this->data['Company']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['State']['name']);
            $this->data['Company']['fiscal_city_id'] = !empty($this->data['FiscalCity']['id']) ? $this->data['FiscalCity']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['FiscalCity']['name'], 'FiscalCity');
            $this->data['Company']['fiscal_state_id'] = !empty($this->data['FiscalState']['id']) ? $this->data['FiscalState']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['FiscalState']['name'], 'FiscalState');
            $this->Company->set($this->data);
            if ($this->Company->validates() && $this->Company->User->UserAvatar->validates() && $ini_upload_error) {
                if ($this->Company->save($this->data, false)) {
                    if (!empty($this->data['UserAvatar']['filename']['name'])) {
                        $this->Attachment->create();
                        $this->data['UserAvatar']['class'] = 'UserAvatar';
                        $this->data['UserAvatar']['foreign_id'] = $this->data['User']['id'];
                        $this->Attachment->save($this->data['UserAvatar']);
                    }
                    $this->Session->setFlash(__l('Company has been updated'), 'default', null, 'success');
                } else {
                    $this->Session->setFlash(__l('Company could not be updated. Please, try again.'), 'default', null, 'error');
                }
                if ($this->Company->User->isAllowed($this->Auth->user('user_type_id'))) {
                    $this->redirect(array(
                        'controller' => 'users',
                        'action' => 'my_stuff'
                    ));
                }
            }
        } else {
            $this->data = $this->Company->find('first', array(
                'conditions' => array(
                    'Company.id = ' => $id
                ),
                'contain' => array(
                    'User' => array(
                        'UserAvatar' => array(
                            'fields' => array(
                                'UserAvatar.id',
                                'UserAvatar.dir',
                                'UserAvatar.filename',
                                'UserAvatar.width',
                                'UserAvatar.height'
                            )
                        ),
                        'UserProfile' => array(
                            'fields' => array(
                                'UserProfile.paypal_account'
                            )
                        ),
                        'fields' => array(
                            'User.user_type_id',
                            'User.username',
                            'User.id',
                            'User.available_balance_amount'
                        )
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.name'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.name'
                        )
                    ),
                    'FiscalCity' => array(
                        'fields' => array(
                            'FiscalCity.name'
                        )
                    ),
                    'FiscalState' => array(
                        'fields' => array(
                            'FiscalState.name'
                        )
                    )
                ),
                'recursive' => 2
            ));
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
            if (!empty($this->data['Company']['City'])) {
                $this->data['City']['name'] = $this->data['Company']['City']['name'];
            }
            if (!empty($this->data['Company']['State']['name'])) {
                $this->data['State']['name'] = $this->data['Company']['State']['name'];
            }
            if (!empty($this->data['Company']['FiscalCity'])) {
                $this->data['FiscalCity']['name'] = $this->data['Company']['FiscalCity']['name'];
            }
            if (!empty($this->data['Company']['FiscalState']['name'])) {
                $this->data['FiscalState']['name'] = $this->data['Company']['FiscalState']['name'];
            }
        }
        $this->pageTitle.= ' - ' . $this->data['Company']['name'];
        $cities = $this->Company->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            )
        ));
        $states = $this->Company->State->find('list');
        $countries = $this->Company->Country->find('list');
        $this->set(compact('cities', 'states', 'countries'));
    }

    function delete($id = null) {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Company->del($id)) {
            $this->Session->setFlash(__l('Company deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function admin_index() {
        $this->pageTitle = 'Empresas';
        $companyStatuses = array();
        $companyStatuses[2] = 'Todas';
        $companyStatuses[1] = 'Activas';
        $companyStatuses[0] = 'Inactivas';
        $conditions = isset($this->params['named']['tab']) ? array(
            'Company.is_online_account' => $this->params['named']['tab']
                ) : array();
        $companies_for_view = $this->paginate('companies_for_admin_index', $conditions);
        $selected = isset($this->params['named']['tab']) ? $this->params['named']['tab'] : 2;
        $moreActions = $this->Company->moreActions;
        if (!empty($this->params['named']['tab']) && $this->params['named']['tab'] == 0) {
            $moreActions[ConstMoreAction::DeductAmountFromWallet] = __l('Set As Paid');
        }
        $this->set('selected', $selected);
        $this->set('companyStatuses', $companyStatuses);
        $this->set('companies_for_view', $companies_for_view);
        $this->set('moreActions', $moreActions);
    }

    function admin_reindex() {
        $this->autoRender = false;
        $this->Company->reindexAll(-1);
        echo 'Index OK';
    }

    function admin_search() {
        $this->pageTitle = 'Resultados para: ' . @$this->data['Company']['q'];
        $companyStatuses = array();
        $companyStatuses[2] = 'Todas';
        $companyStatuses[1] = 'Activas';
        $companyStatuses[0] = 'Inactivas';
        $selected = ($this->data['Company']['selected'] == '0' | $this->data['Company']['selected'] == '1') ? $this->data['Company']['selected'] : 2;
        $this->setParamsSearch();
        if (isset($this->paramsQ) || isset($this->paramsId)) {
            $conditions = array();
            if (isset($selected) & $selected != 2) {
                $conditions = array(
                    'Company.is_online_account' => $this->data['Company']['selected']
                );
            }
            if ($this->isSearchById($this->data)) {
                $ids = $this->Company->findById($this->paramsId);
            } else {
                $ids = $this->Company->search($this->paramsQ, array(
                    'fields' => array(
                        'Company.id'
                    ),
                    'conditions' => $conditions
                ));
            }
            $this->paginate['limit'] = 10;
            $companies_for_view = $this->paginate('companies_for_admin_index', array(
                'Company.id' => Set::extract('/Company/id', $ids)
            ));
            $this->set('companies_for_view', $companies_for_view);
            $this->set('parametro', @$this->data['Company']['q']);
            $this->set('companyStatuses', $companyStatuses);
            $this->set('selected', $selected);
            $moreActions = $this->Company->moreActions;
            $this->set('moreActions', $moreActions);
            $this->render('admin_index');
        } else {
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    private function setParamsSearch() {
        if ((isset($this->data['Company']['q']) && !empty($this->data['Company']['q'])) || (isset($this->params['named']['q']) && !empty($this->params['named']['q']))) {
            $this->paramsQ = isset($this->data['Company']['q']) ? $this->data['Company']['q'] : $this->params['named']['q'];
            $this->paramsSearch = '/q:' . $this->paramsQ;
        }
        if ((isset($this->data['Company']['id']) && !empty($this->data['Company']['id'])) || (isset($this->params['named']['id']) && !empty($this->params['named']['id']))) {
            $this->paramsId = isset($this->data['Company']['id']) ? $this->data['Company']['id'] : $this->params['named']['id'];
            $this->paramsSearch.= '/id:' . $this->paramsId;
        }
        if (isset($this->paramsSearch)) {
            $this->set('paramsSearch', $this->paramsSearch);
        }
    }

    function isSearchById($data) {
        if (!empty($data['Company']['id'])) {
            return true;
        }
        return false;
    }

    function admin_add() {
        $this->pageTitle = 'A&ntilde;adir empresa';
        if (!empty($this->data)) {
            if (!empty($this->data['Company']['logo_image'])) {
                $this->data['Company']['tmp_image_name'] = $this->data['Company']['logo_image']['tmp_name'];
                $this->data['Company']['logo_image'] = $this->__upload($this->data['Company']['logo_image']);
            }
            if (!empty($this->data['City']['name'])) {
                $this->data['Company']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['City']['name']);
            }
            if (!empty($this->data['FiscalCity']['name'])) {
                $this->data['Company']['fiscal_city_id'] = !empty($this->data['FiscalCity']['id']) ? $this->data['FiscalCity']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['FiscalCity']['name'], 'FiscalCity');
            }
            if (!empty($this->data['State']['name'])) {
                $this->data['Company']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['State']['name']);
            }
            if (!empty($this->data['FiscalState']['name'])) {
                $this->data['Company']['fiscal_state_id'] = !empty($this->data['FiscalState']['id']) ? $this->data['FiscalState']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['FiscalState']['name'], 'FiscalState');
            }
            $this->data['Company']['zip'] = ($this->data['Company']['zip'] == '') ? '0' : $this->data['Company']['zip'];
            $this->data['Company']['email'] = $this->data['User']['email'];
            if (!ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
                $this->data['Company']['validated'] = 0;
                $mensaje = 'Una nueva empresa fue dada del alta. Su nombre es ' . $this->data['Company']['name'] . '<br />Por favor, ingrese al panel de control para confirmar los datos';
                $this->Email->from = Configure::read('company.email');
                $this->Email->replyTo = Configure::read('company.email');
                $this->Email->to = Configure::read('company.email');
                $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                $this->Email->subject = 'Se ha dado de alta una nueva empresa';
                $this->Email->send($mensaje);
            }
            $this->Company->create();
            $this->Company->set($this->data);
            $this->Company->User->set($this->data);
            $this->Company->State->set($this->data);
            $this->Company->City->set($this->data);
            if ($this->Company->User->validates() & $this->Company->validates() & $this->Company->City->validates() & $this->Company->State->validates()) {
                if (empty($this->data['Company']['user_id'])) {
                    $this->data['User']['user_type_id'] = ConstUserTypes::Company;
                    $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                    $this->data['User']['is_email_confirmed'] = '1';
                    $this->data['User']['is_active'] = '1';
                    if ($this->Company->User->save($this->data)) {
                        $user_id = $this->Company->User->getLastInsertId();
                        $this->data['Company']['user_id'] = $user_id;
                        $this->data['UserProfile']['user_id'] = $user_id;
                        $this->data['UserProfile']['city_id'] = $this->data['Company']['city_id'];
                        $this->data['UserProfile']['state_id'] = $this->data['Company']['state_id'];
                        $this->data['UserProfile']['zip_code'] = $this->data['Company']['zip'];
                        $this->Company->User->UserProfile->create();
                        $this->Company->User->UserProfile->save($this->data, array(
                            'validate' => false
                        ));
                    }
                }
                if ($this->Company->save($this->data)) {
                    $emailFindReplace = array(
                        '##USERNAME##' => $this->data['User']['username'],
                        '##LOGINLABEL##' => ucfirst(Configure::read('user.using_to_login')),
                        '##USEDTOLOGIN##' => $this->data['User'][Configure::read('user.using_to_login')],
                        '##SITE_NAME##' => Configure::read('site.name'),
                        '##PASSWORD##' => $this->data['User']['passwd'],
                        '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                        '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                    );
                    $email = $this->EmailTemplate->selectTemplate('Admin User Add');
                    $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
                    $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
                    $this->Email->to = $this->data['User']['email'];
                    $this->Email->subject = strtr($email['subject'], $emailFindReplace);
                    $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
                    $this->Email->send(strtr($email['email_content'], $emailFindReplace));
                    $this->Session->setFlash(__l('Company has been added'), 'default', null, 'success');
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__l('Company could not be added. Please, try again.'), 'default', null, 'error');
                }
            }
        }
        $sellers = $this->User->getActiveSellersForSelect();
        $countries = $this->Company->Country->find('list');
        $this->set(compact('countries', 'sellers'));
        unset($this->data['User']['passwd']);
        unset($this->data['User']['confirm_password']);
    }

    function admin_edit($id = null) {
        $this->pageTitle = __l('Edit Company');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $id = (!empty($this->data['Company']['id'])) ? $this->data['Company']['id'] : $id;
        $company = $this->Company->find('first', array(
            'conditions' => array(
                'Company.id' => $id
            ),
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.email',
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                )
            ),
            'fields' => array(
                'Company.id',
                'Company.now_eula_ok'
            ),
            'recursive' => 0
        ));
        if (!empty($this->data)) {
            if ($this->data['Company']['logo_image_file']['name'] != '') {
                $this->data['Company']['tmp_image_name'] = $this->data['Company']['logo_image_file']['tmp_name'];
                $fileName = $this->__upload($this->data['Company']['logo_image_file']);
                $this->data['Company']['logo_image'] = $fileName;
            } else {
                unset($this->Company->validate['logo_image']);
                unset($this->data['Company']['logo_image_file']);
            }
            if ($company['Company']['now_eula_ok'] == 1) {
                $this->Company->validate['deal_commission_percentage'] = array(
                    'rule1' => array(
                        'rule' => 'notempty',
                        'allowEmpty' => false,
                        'message' => __l('Required')
                    ),
                    'rule2' => array(
                        'rule' => 'numeric',
                        'allowEmpty' => true,
                        'message' => __l('Should be a number')
                    )
                );
            }
            $this->data['Company']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['City']['name']);
            $this->data['Company']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['State']['name']);
            $this->data['Company']['fiscal_city_id'] = !empty($this->data['FiscalCity']['id']) ? $this->data['FiscalCity']['id'] : $this->Company->City->findOrSaveAndGetId($this->data['FiscalCity']['name'], 'FiscalCity');
            $this->data['Company']['fiscal_state_id'] = !empty($this->data['FiscalState']['id']) ? $this->data['FiscalState']['id'] : $this->Company->State->findOrSaveAndGetId($this->data['FiscalState']['name'], 'FiscalState');
            $this->Company->set($this->data);
            $this->Company->State->set($this->data);
            $this->Company->City->set($this->data);
            $this->Company->User->set($this->data);
            if ($company['User']['email'] == $this->data['User']['email']) {
                unset($this->Company->User->validate['email']['rule3']);
            }
            if (!ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
                $this->data['Company']['validated'] = 0;
            }
            if ($this->Company->validates() && $this->Company->City->validates() && $this->Company->State->validates() && $this->Company->User->validates()) {
                if ($this->Company->save($this->data)) {
                    $company = $this->Company->find('first', array(
                        'fields' => array(
                            'Company.user_id'
                        ),
                        'recursive' => - 1
                    ));
                    $this->data['User']['is_email_confirmed'] = '1';
                    $this->data['User']['is_active'] = '1';
                    $this->data['User']['id'] = $company['Company']['user_id'];
                    $this->Company->User->save($this->data);
                    $this->Session->setFlash(__l('Company has been updated'), 'default', null, 'success');
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__l('Company could not be updated. Please, try again.'), 'default', null, 'error');
                }
            }
        } else {
            $this->data = $this->Company->find('first', array(
                'conditions' => array(
                    'Company.id ' => $id
                ),
                'contain' => array(
                    'User' => array(
                        'UserAvatar' => array(
                            'fields' => array(
                                'UserAvatar.id',
                                'UserAvatar.dir',
                                'UserAvatar.filename',
                                'UserAvatar.width',
                                'UserAvatar.height'
                            )
                        ),
                        'UserProfile' => array(
                            'fields' => array(
                                'UserProfile.first_name',
                                'UserProfile.last_name',
                                'UserProfile.middle_name',
                                'UserProfile.gender',
                                'UserProfile.about_me',
                                'UserProfile.country_id',
                                'UserProfile.state_id',
                                'UserProfile.city_id',
                                'UserProfile.zip_code',
                                'UserProfile.dob',
                                'UserProfile.language_id',
                                'UserProfile.paypal_account'
                            )
                        )
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.name'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.name'
                        )
                    ),
                    'FiscalCity' => array(
                        'fields' => array(
                            'FiscalCity.name'
                        )
                    ),
                    'FiscalState' => array(
                        'fields' => array(
                            'FiscalState.name'
                        )
                    )
                ),
                'recursive' => 2
            ));
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['Company']['name'];
        $logo_image = $this->data['Company']['logo_image'];
        $fiscalCities = $cities = $this->Company->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            )
        ));
        $fiscalStates = $states = $this->Company->State->find('list', array(
            'conditions' => array(
                'State.is_approved =' => 1
            ),
            'order' => array(
                'State.name' => 'asc'
            )
        ));
        $countries = $this->Company->Country->find('list');
        $sellers = $this->User->getActiveSellersForSelect();
        $this->set('logo_image', $logo_image);
        $this->set(compact('users', 'cities', 'fiscalCities', 'fiscalStates', 'states', 'countries', 'sellers'));
    }

    function admin_delete($id = null) {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $company = $this->Company->find('first', array(
            'conditions' => array(
                'Company.id' => $id
            ),
            'recursive' => - 1
        ));
        if (!empty($company['Company']['user_id']) && $this->Company->User->del($company['Company']['user_id'])) {
            $this->Session->setFlash(__l('Company deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function admin_update() {
        $this->autoRender = false;
        if (!empty($this->data['Company'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $companyIds = array();
            foreach ($this->data['Company'] as $company_id => $is_checked) {
                if ($is_checked['id']) {
                    $companyIds[] = $company_id;
                }
            }
            if ($actionid && !empty($companyIds)) {
                if ($actionid == ConstMoreAction::Active) {
                    foreach ($companyIds as $companyId) {
                        $get_company_user = $this->Company->find('first', array(
                            'conditions' => array(
                                'Company.id' => $companyId
                            ),
                            'recursive' => - 1
                        ));
                        $this->Company->User->updateAll(array(
                            'User.is_active' => 1
                                ), array(
                            'User.id' => $get_company_user['Company']['user_id']
                        ));
                    }
                    $this->Session->setFlash(__l('Las empresas seleccionadas fueron activadas.'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Inactive) {
                    foreach ($companyIds as $companyId) {
                        $get_company_user = $this->Company->find('first', array(
                            'conditions' => array(
                                'Company.id' => $companyId
                            ),
                            'recursive' => - 1
                        ));
                        $this->Company->User->updateAll(array(
                            'User.is_active' => 0
                                ), array(
                            'User.id' => $get_company_user['Company']['user_id']
                        ));
                    }
                    $this->Session->setFlash(__l('Las empresas seleccionadas han sido desactivadas'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::DeductAmountFromWallet) {
                    $this->Session->write('companies_list.data', $companyIds);
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function admin_deduct_amount() {
        $companies_list = $this->Session->read('companies_list.data');
        if (!empty($companies_list)) {
            $companies = $this->Company->find('all', array(
                'conditions' => array(
                    'Company.id' => $companies_list
                ),
                'contain' => array(
                    'User' => array(
                        'fields' => array(
                            'User.user_type_id',
                            'User.username',
                            'User.id',
                            'User.available_balance_amount'
                        )
                    )
                ),
                'recursive' => 0
            ));
            $this->set('companies', $companies);
        } else {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            foreach ($this->data['Company'] as $company_id => $company) {
                if (empty($company['amount'])) {
                    $this->Company->validationErrors[$company_id]['amount'] = __l('Required');
                }
            }
            if (empty($this->Company->validationErrors)) {
                $transactions = array();
                $transactions['Transaction']['user_id'] = $this->Auth->user('id');
                foreach ($this->data['Company'] as $company_id => $company) {
                    $transactions['Transaction']['foreign_id'] = $company['user_id'];
                    $transactions['Transaction']['class'] = 'SecondUser';
                    $transactions['Transaction']['amount'] = $company['amount'];
                    $transactions['Transaction']['description'] = $company['description'];
                    $this->Company->User->Transaction->log($transactions);
                    $this->Company->User->updateAll(array(
                        'User.available_balance_amount' => 'User.available_balance_amount -' . $company['amount']
                            ), array(
                        'User.id' => $company['user_id']
                    ));
                }
                $this->Session->del('companies_list');
                $this->Session->setFlash(__l('Amount deducted for the selected companies'), 'default', null, 'success');
                $this->redirect(Router::url(array(
                            'action' => 'index'
                                ), true));
            } else {
                $this->Session->setFlash(__l('Amount could not be deducted for the selected companies. Please, try again.'), 'default', null, 'error');
            }
        }
    }

    function admin_list_companies_data() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $companyID = $this->params['form']['compID'];
        $company_data = $this->Company->find('all', array(
            'conditions' => array(
                'Company.id' => $companyID
            ),
            'fields' => array(
                'Company.cheque_noorden',
                'Company.fiscal_bank_account'
            ),
            'recursive' => 0
        ));
        if ($company_data[0]['Company']['cheque_noorden'] && $company_data[0]['Company']['fiscal_bank_account'] != '') {
            return 1;
        } else {
            if ($company_data[0]['Company']['fiscal_bank_account'] != '') {
                return 2;
            }
            if ($company_data[0]['Company']['cheque_noorden']) {
                return 3;
            }
            return 0;
        }
    }

    function __upload($file_array, $targetDir = '/img/company/') {
        $file = new File($file_array['tmp_name']);
        $file_base = pathinfo($file_array['name']);
        if ($file_base['extension'] != '') {
            $ext = strtolower($file_base['extension']);
            $name = $this->data['Company']['name'];
            if ($ext != 'jpg' && $ext != 'jpeg' && $ext != 'gif' && $ext != 'png') {
                $this->Session->setFlash('S&oacute;lo se pueden subir im&aacute;genes!', true);
                $this->render();
            } else {
                $data = $file->read();
                $file->close();
                $rndmSeed = $this->__str_rand(10);
                $cleanName = $this->__cleanChars($name);
                $full_path = $targetDir . "company" . $cleanName . "-" . $rndmSeed . "." . $ext;
                $file = new File(WWW_ROOT . $full_path, true);
                $file->write($data);
                $file->close();
                return $full_path;
            }
        }
    }

    function __cleanChars($string) {
        $string = str_replace(array(
            ' ',
            '-'
                ), array(
            '_',
            '_'
                ), $string);
        $string = str_replace(array(
            'Ã¡',
            'Ã©',
            'Ã­',
            'Ã³',
            'Ãº',
            'Ã ',
            'Ã¨',
            'Ã¬',
            'Ã²',
            'Ã¹',
            'Ã¤',
            'Ã«',
            'Ã¯',
            'Ã¶',
            'Ã¼',
            'Ãƒï¿½',
            'Ãƒâ€°',
            'Ãƒï¿½',
            'Ãƒâ€œ',
            'ÃƒÅ¡',
            'Ãƒâ‚¬',
            'ÃƒË†',
            'ÃƒÅ’',
            'Ãƒâ€™',
            'Ãƒâ„¢',
            'Ãƒâ€ž',
            'Ãƒâ€¹',
            'Ãƒï¿½',
            'Ãƒâ€“',
            'ÃƒÅ“'
                ), array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U'
                ), $string);
        return $string;
    }

    function __str_rand($length = 8) {
        $output = 'abcdefghijklmnopqrstuvwqyz0123456789';
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str.= $output[mt_rand(0, strlen($output) - 1)];
        }
        return $str;
    }

}
