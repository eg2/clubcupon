<?php

class FirstsController extends AppController {

    var $name = 'Firsts';
    var $_flashErrors = array();
    var $components = array(
        'Email',
    );
    var $helpers = array(
        'Csv',
        'Gateway',
        'Javascript',
        'Html'
    );
    var $uses = array(
        'Deal',
        'City',
        'Subscription',
        'User',
        'EmailTemplate',
        'UserReferrals',
    );

    function beforeFilter() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->Security->disabledFields = array();
        $this->Security->validatePost = false;
        parent::beforeFilter();
    }

    function index() {
        if (isset ($_GET ['redirect'])) {
            $this->set ('redirect', Router::url(array('controller' => 'deals', 'action' => 'index'), true) . $this->params['named']['city'] . '?popup=no');
        }


        /*No seteamos certificaPth en el FirstsController::index para evitar el doble tageo*/
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $city = $this->City->find('first', array(
            'conditions' => array(
                'City.slug' => $this->params['named']['city']
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => -1
                ));
        $this->data['Subscription']['city_id'] = $city['City']['id'];

        $cities = $this->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            )
                ));
        $this->set('cities', $cities);
        //$this->layout = 'popup';
        $this->layout = 'clean_theme_popup';
    }

    function index_cordoba() {
        $this->index();
    }

    function promo() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
    }

    function beta() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
    }

    function register() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
    }

    function _checkOpenDeals() {
        $finderOptions = array('conditions' => array(
                'Deal.city_id' => Configure::read('Actual.city_id'),
                'Deal.is_side_deal' => 0,
                'Deal.deal_status_id' => array(ConstDealStatus::Open, ConstDealStatus::Tipped)
            ),
            'order' => array('Deal.start_date' => 'ASC'),
            'recursive' => -1);
        $deal = $this->Deal->find('first', $finderOptions);
        return $deal;
    }

    function noopendeal() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->set('certificaPath', 'ClubCupon/recommendNoDeal');
        $this->layout = 'clean_theme_popup';
    }

    function recommend($deal_id = null) {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->set('certificaPath', 'ClubCupon/recommendFriends');
        $this->layout = 'popup';

        $finderOptions = array('conditions' => array('Deal.id' => $deal_id,),'recursive' => -1,);
        $deal = $this->Deal->find('first', $finderOptions);

        if (!$deal) {
              $this->redirect(array('action' => 'noopendeal'));
        }

        App::import('vendor', 'OpenInviter', array('file' => 'OpenInviter/openinviter.php'));
        $inviter = new OpenInviter();
        $oiServices = $inviter->getPlugins();
        $inviter = $this->Session->read('userInviter');
        $this->set('oiServices', $oiServices);
        $this->set('inviter', $inviter);
    }

    function thanks() {
        $dealInfo = $this->_getDealInfo();
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
        $this->set('bityurl', $dealInfo['##BITLY_SHORT_URL##']);
        $this->set('deal_name', $dealInfo['##DEAL_NAME##'] );
        $this->set('deal_discount_percentage', $dealInfo['##DISCOUNT##']);
    }

    function confirm() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
    }

    function option1() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
        $this->data = $_POST;

        if ($this->Auth->user('id')) {
            $userId = $this->Auth->user('id');
        } else {
            $userId = $this->Session->read('UserIdRecommend');
        }


        if (!isset($this->data['emails']) || empty($this->data['emails'])) {
            $this->_addFlashError("Debes proporcionar un email");
        }

        if (!$this->_checkFlashErrors()) {
            return $this->redirect(array('controller' => 'firsts',
                        'action' => 'recommend'));
        }
        $emails = $this->data['emails'];
        $emailsFromUser = explode(',', $emails);
        $emails = array();
        foreach ($emailsFromUser as $email) {
            $email = trim($email);
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);
            if ($email === false) {
                $this->_addFlashError("Alguno de los emails ingresados no es inv&aacute;lido.");
            } else {
                $emails[$email] = $email;
            }
        }
        if (!$this->_checkFlashErrors()) {
            return $this->redirect(array('controller' => 'firsts',
                        'action' => 'recommend'));
        }

        //Guardamos los emails referidos
        ClassRegistry::init('UserReferral')->saveReferralsByUser($emails, $userId);

        $this->_sendMailsOption2($emails, $this->data ['name']);
        $this->Session->setFlash('Los mensajes fueron enviados correctamente', 'default', null, 'success');
        return $this->redirect(array('controller' => 'firsts',
                    'action' => 'thanks'));
    }

    function option2() {
        $this->pageTitle = "Recomenda " . Configure::read('site.name') . ' - Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
        $this->layout = 'popup';
        App::import('vendor', 'OpenInviter', array('file' => 'OpenInviter/openinviter.php'));
        $this->inviter = $inviter = new OpenInviter();
        $inviter->getPlugins();
        $this->data = $_POST;
        $this->_validateRecommendOption2();
        if (!$this->_checkFlashErrors()) {
            return $this->redirect(array('controller' => 'firsts',
                        'action' => 'recommend'));
        }
        if (!$inviter->login($this->data['email'], $this->data['password'])) {
            $internal = $inviter->getInternalError();
            $this->_addFlashError($internal ? $internal : "El login fall&oacute;. Por favor verific&aacute; tu usuario y contrase&ntilde;a y prob&aacute; de nuevo.");
        } elseif (false === $contacts = $inviter->getMyContacts()) {
            $this->_addFlashError("No se pudieron obtener los contactos. Intente nuevamente.");
        }
        if (!$this->_checkFlashErrors()) {
            return $this->redirect(array('controller' => 'firsts',
                        'action' => 'recommend'));
        }
        $this->set('oiSessionId', $inviter->plugin->getSessionID());
        $this->set('provider', $this->data['provider']);
        $this->set('contacts', $contacts);
    }

    function selcontacts() {
        $this->layout = 'popup';
        $this->data = $_POST;
        App::import('vendor', 'OpenInviter', array('file' => 'OpenInviter/openinviter.php'));
        $this->inviter = $inviter = new OpenInviter();
        $inviter->getPlugins();
        $inviter->startPlugin($this->data['provider']);
        $internal = $inviter->getInternalError();
        if ($internal)
            $this->_addFlashError($internal);
        if (!isset($this->data['contacts']) || !count($this->data['contacts'])) {
            $this->_addFlashError('Debes seleccionar al menos un contacto.');
        }
        if (!isset($this->data['oiSessionId']) || empty($this->data['oiSessionId'])) {
            $this->_addFlashError('Tu sesi&oacute;n caduc&oacute; por favor empieza de nuevo.');
        }
        $oiSessionId = $this->data['oiSessionId'];
        if (!$this->_checkFlashErrors()) {
            return $this->redirect(array('controller' => 'firsts',
                        'action' => 'recommend'));
        }
        $contacts = array();
        $sep = '||-||';
        foreach ($this->data['contacts'] as $contact) {
            list($email, $name) = explode($sep, $contact);
            $contacts[$email] = $name;
        }
        // $message=array('subject'=>$inviter->settings['message_subject'],'body'=>$inviter->settings['message_body'],'attachment'=>"\n\rAttached message: \n\r". 'TESTING ENVIO DE MSJ');
        // $sendMessage=$inviter->sendMessage($oiSessionId,$message,$contacts);
        // echo 'Message result: <br />';
        $sendMessage = $this->_sendMailsOption2($contacts);
        $this->Session->setFlash('Los mensajes fueron enviados correctamente', 'default', null, 'success');
        return $this->redirect(array('controller' => 'firsts',
                    'action' => 'thanks'));
    }

    function circa($deal_id) {
        // $circa = Configure::read ('CIRCA');

        if (!empty($this->data)) {
            // aca verificamos que el codigo este OK
            /* if (array_key_exists ($this->data ['CIRCA']['company'], $circa) &&
              in_array         ($this->data ['CIRCA']['code'],    $circa [$this->data ['CIRCA']['company']])) { */
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'buy',
                $this->data ['Deal']['deal_id'],
                1,
                $this->data ['CIRCA']['company'],
                $this->data ['CIRCA']['code'],
            )); /*
              } else {
              // aca se muestra un error
              $this->Session->setFlash('C&oacute;digo incorrecto','default',null,'error');
              $this->redirect (array (
              'controller' => 'deals',
              'action' => 'index',
              ));
              } */
        } else {
            $this->layout = 'popup';
            $this->set('deal_id', $deal_id);
            //  $this->set ('circa', $circa);
        }
    }

    function _validateRecommendOption2() {
        $inviter = $this->inviter;
        if (empty($this->data['email']))
            $this->_addFlashError("Ingres&aacute un email");
        if (empty($this->data['password']))
            $this->_addFlashError("Ingres&aacute un password");
        if (empty($this->data['provider']))
            $this->_addFlashError("No se selecciono proveedor.");
        $inviter->startPlugin($this->data['provider']);
        $internal = $inviter->getInternalError();
        if ($internal)
            $this->_addFlashError($internal);
    }

    function _addFlashError($msg) {
        array_push($this->_flashErrors, $msg);
    }

    function _checkFlashErrors() {
        $errs = $this->_flashErrors;
        if (count($errs)) {
            $errStr = implode('<br />', $errs);
            $this->Session->setFlash(utf8_encode($errStr), 'default', null, 'error');
            return false;
        }
        return true;
    }

    /*
     * @param $contacts is in de form of array($email1,$email2 ...)
     * @param $body
     */

    function _sendMailsOption1($contacts, $body) {
        if ($this->Auth->user('id')) {
            $userId = $this->Auth->user('id');
        } else {
            $userId = $this->Session->read('UserIdRecommend');
        }
        $user = $this->User->find('first', array('conditions' => array('User.id' => $userId), 'recursive' => -1 ));

        $dbo = $this->User->getDatasource();
        $logs = $dbo->_queriesLog;
        $firstName = $user['User']['username'];
        $referUrl = Router::url(array('controller' => 'users', 'action' => 'refer', $userId), true) . '?from=mailing';
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##USER_FIRST_NAME##' => $firstName,
            '##SITE_NAME##' => Configure::read('site.name'),
            '##REFER_URL##' => $referUrl,
            '##BODY##' => $body,
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
        );
        $email = $this->EmailTemplate->selectTemplate('Recommend Friend Option 1');
        // Send mail to users
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        $dealInfo = $this->_getDealInfo();
        $emailFindReplace = array_merge($dealInfo, $emailFindReplace);
        $replaces = strtr($email['email_content'], $emailFindReplace);
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        foreach ($contacts as $email) {
            $this->Email->to = $email;
            $this->Email->send($replaces);
        }
    }

    /*
     * @param $contacts is in de form of array($email => $name, ...)
     */

    function _sendMailsOption2($contacts, $from = '') {
        if ($this->Auth->user('id')) {
            $userId = $this->Auth->user('id');
        } else {
            $userId = $this->Session->read('UserIdRecommend');
        }
        $user = $this->User->find('first', array('conditions' => array('User.id' => $userId), 'recursive' => -1 ));
        $firstName = $from != '' ? $from : $user['User']['username'];
        $referUrl = Router::url(array('controller' => 'users', 'action' => 'refer', $userId), true) . '?from=mailing';
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##USER_FIRST_NAME##' => $firstName,
            '##SITE_NAME##' => Configure::read('site.name'),
            '##REFER_URL##' => $referUrl,
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
        );
        $email = $this->EmailTemplate->selectTemplate('Recommend Friend Option 2');
        // Send mail to users
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];

        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        $dealInfo = $this->_getDealInfo();
        $emailFindReplace = array_merge($dealInfo, $emailFindReplace);
        $replaces = strtr($email['email_content'], $emailFindReplace);
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        foreach ($contacts as $email => $name) {
            $this->Email->to = $email;
            $this->Email->send($replaces);
        }
    }

    function _getDealInfo() {
        $contains = array('Company' => array('fields' => array('Company.name',
                    'Company.id',
                    'Company.url',
                    'Company.zip',
                    'Company.address1',
                    'Company.address2',
                    'Company.city_id',
                    'Company.phone'),
                'City' => array('fields' => array('City.id',
                        'City.name',
                        'City.slug',)),
                'State' => array('fields' => array('State.id',
                        'State.name')),
                'Country' => array('fields' => array('Country.id',
                        'Country.name',
                        'Country.slug',))
            ),
            'Attachment' => array('fields' => array('Attachment.id',
                    'Attachment.dir',
                    'Attachment.filename',
                    'Attachment.width',
                    'Attachment.height')),
            'City' => array('Subscription' => array('fields' => array('Subscription.id',
                        'Subscription.user_id',
                        'Subscription.email',),
                    'conditions' => array('Subscription.is_subscribed' => 1)),
                'fields' => array('City.id',
                    'City.name',
                    'City.slug',)),);
        $defaultCity = $this->Deal->City->find('first', array(
            'conditions' => array(
                'City.slug' => Configure::read('Actual.city_slug')
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => -1
                ));
        $idCity = $defaultCity['City']['id'];
        $finderOptions = array('conditions' => array(
                'Deal.city_id' => $idCity,
                'Deal.is_side_deal' => 0,
                'Deal.deal_status_id' => array(ConstDealStatus::Open, ConstDealStatus::Tipped)
            ),
            'contain' => $contains,
            'order' => array('Deal.start_date' => 'ASC'),
            'recursive' => 2);
        $deal = $this->Deal->find('first', $finderOptions);
        $savings = $deal['Deal']['savings'];
        $buy_price = $deal['Deal']['discounted_price'];
        /* sending mail to all subscribers starts here */
        $address = $country = $state = $city = '';

        /* if (!empty ($deal ['Deal']['custom_company_address1'])) {
          $address .= $deal ['Deal']['custom_company_address1'];
          } else */ if (!empty($deal['Company']['address1'])) {
            $address .= $deal['Company']['address1'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_address2'])) {
          $address .= ', ' . $deal ['Deal']['custom_company_address2'];
          } else */ if (!empty($deal['Company']['address2'])) {
            $address .= ', ' . $deal['Company']['address2'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_city'])) {
          $address .= ', ' . $city = $deal ['Deal']['custom_company_city'];
          } else */ if (!empty($deal['Company']['City']['name'])) {
            $address .= ', ' . $city = $deal['Company']['City']['name'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_state'])) {
          $address .= ', ' . $state = $deal ['Deal']['custom_company_state'];
          } else */ if (!empty($deal['Company']['State']['name'])) {
            $address .= ', ' . $state = $deal['Company']['State']['name'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_country'])) {
          $address .= ', ' . $country = $deal ['Deal']['custom_company_country'];
          } else */ if (!empty($deal['Company']['Country']['name'])) {
            $address .= ', ' . $country = $deal['Company']['Country']['name'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_zip'])) {
          $address .= ', ' . $deal ['Deal']['custom_company_zip'];
          } else */ if (!empty($deal['Company']['zip'])) {
            $address .= ', ' . $deal['Company']['zip'];
        }

        /* if (!empty ($deal ['Deal']['custom_company_contact_phone'])) {
          $address .= ', ' . $deal ['Deal']['custom_company_contact_phone'];
          } else */ if (!empty($deal['Company']['phone'])) {
            $address .= ', ' . $deal['Company']['phone'];
        }

        $image_hash = 'small_big_thumb/Deal/' . $deal['Attachment']['id'] . '.' . md5(Configure::read('Security.salt') . 'Deal' . $deal['Attachment']['id'] . 'jpg' . 'small_big_thumb' . Configure::read('site.name')) . '.' . 'jpg';
        $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug'],
                    'admin' => false
                        ), false) . '?from=mailing';
        $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'contacts',
                    'action' => 'add',
                    'admin' => false
                        ), false) . '?from=mailing';
        $image_options = array(
            'dimension' => 'medium_big_thumb',
            'class' => '',
            'alt' => $deal['Deal']['name'],
            'title' => $deal['Deal']['name'],
            'type' => 'jpg'
        );
        $src = $this->Deal->getImageUrl('Deal', $deal['Attachment'], $image_options);

        $dealInfo = array(
            '##DEAL_NAME##' => $deal['Deal']['name'],
            // '##COMPANY_NAME##' => (!empty ($deal ['Deal']['custom_company_name'])) ? $deal ['Deal']['custom_company_name'] : $deal['Company']['name'],
            '##COMPANY_NAME##' => $deal['Company']['name'],
            '##COMPANY_ADDRESS##' => $address,
            '##COMPANY_WEBSITE##' => $deal['Company']['url'],
            '##CITY_NAME##' => $deal['City']['name'],
            '##ORIGINAL_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']),
            '##SAVINGS##' => Configure::read('site.currency') . removeZeroDecimals($savings),
            '##BUY_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($buy_price),
            '##DISCOUNT##' => $deal['Deal']['discount_percentage'] . '%',
            '##DESCRIPTION##' => $deal['Deal']['description'],
            '##COMPANY_SITE##' => $deal['Company']['url'],
            '##COUPON_CONDITION##' => $deal['Deal']['coupon_condition'],
            '##DEAL_URL##' => $deal_url,
            '##DEAL_LINK##' => $deal_url,
            '##CONTACT_US##' => $contact_us_url,
            '##DEAL_IMAGE##' => $src,
            '##BITLY_SHORT_URL##' => $deal['Deal']['bitly_short_url']
        );
        return $dealInfo;
    }

}
