<?php

if (Configure::read('app.rDebug.enable')) {
    App::import('Vendor', 'web.ref');
}
App::import('Model', 'web.WebSearchLinks');
App::import('Model', 'web.WebSearchLinksLogos');
App::import('Model', 'web.WebSearchLinksStyles');
App::import('Model', 'web.WebCity');

class LinksController extends AppController {

    private $actualCitySlug;
    private $actualTypeID;
    private $targetCitySlug;
    private $targetTypeID;
    private $WebSearchLinks;
    protected $WebCity;
    private $WebSearchLinksLogos;
    private $WebSearchLinksStyles;

    const NAME_DEFAULT_LOGO = 'Sin logo';
    const NAME_DEFAULT_STYLES = 'default';

    function __construct() {
        $this->WebSearchLinks = new WebSearchLinks();
        $this->WebCity = new WebCity();
        $this->WebSearchLinksLogos = new WebSearchLinksLogos();
        $this->WebSearchLinksStyles = new WebSearchLinksStyles();
        parent::__construct();
    }

    var $name = 'Links';
    var $uses = array();
    var $components = array(
    		'api.ApiLogger',
        'Cookie'
    );

    function beforeFilter() {
        parent::beforeFilter();
    }

    function admin_copy() {
        $this->runSteps();
    }

    function admin_index() {
    	$this->ApiLogger->debug(__METHOD__, null);
        $this->runSteps();
    }

    private function runSteps() {
    	$this->ApiLogger->debug(__METHOD__, null);
        $this->updateLinksFromForm();
        $this->setCityAndTypeFromFormData();
        $this->doSubAction();
        $this->setVarsForView();
        if (Configure::read('app.rDebug.enable')) {
            rdebug('dataObj', $this->data);
        }
    }

    private function setCityAndTypeFromFormData() {
        if (isset($_GET['citySlug'])) {
            $this->actualCitySlug = $_GET['citySlug'];
        }
        if (isset($_GET['linkType'])) {
            $this->actualTypeID = $_GET['linkType'];
        }
        if (isset($this->data['City']['slug'])) {
            $this->actualCitySlug = $this->data['City']['slug'];
        }
        if (isset($this->data['Type']['id'])) {
            $this->actualTypeID = $this->data['Type']['id'];
        }
        if (isset($this->data['CopyToCity']['slug'])) {
            $this->targetCitySlug = $this->data['CopyToCity']['slug'];
        }
        if (isset($this->data['CopyToType']['id'])) {
            $this->targetTypeID = $this->data['CopyToType']['id'];
        }
        if (empty($this->actualCitySlug)) {
            $this->actualCitySlug = Configure::read('web.default.citySlug');
        }
        if (empty($this->actualTypeID)) {
            $this->actualTypeID = ConstSearchLinksTypes::Destacado;
        }
        $this->ApiLogger->debug(__METHOD__, 'actualTypeID:'.$this->actualTypeID.'-actualCitySlug:'.$this->actualCitySlug);
    }

    private function addCatLink() {
        $city = $this->WebCity->findBySlug($this->actualCitySlug);
        $logo = $this->WebSearchLinksLogos->findByName(self::NAME_DEFAULT_LOGO);
        $style = $this->WebSearchLinksStyles->findByName(self::NAME_DEFAULT_STYLES);
        $data = array(
            'SearchLinks' => array(
            	'created' => date('Y-m-d H:i:s'),
            	'created_by' => $this->Auth->user ('id'),
            	'city_id' => $city['WebCity']['id'],
                'search_link_type_id' => $this->actualTypeID,
                'priority' => 0,
                'label' => ' ',
                'description' => ' ',
                'url' => ' ',
                'search_link_logo_right_id' => $logo['WebSearchLinksLogos']['id'],
                'search_link_logo_left_id' => $logo['WebSearchLinksLogos']['id'],
                'search_link_style_id' => $style['WebSearchLinksStyles']['id']
            )
        );
        
        
        
        $this->WebSearchLinks->create();
        $this->ApiLogger->debug(__METHOD__, $data);
        if(!$this->WebSearchLinks->save($data)){
        	$this->ApiLogger->debug(__METHOD__, 'Error en save');
        }else{
        	$this->ApiLogger->debug(__METHOD__, 'OK en save');
        }
    }

    private function updateLinksFromForm() {
    	$this->ApiLogger->debug(__METHOD__, $this->data['Links']);
        if (!empty($this->data['Links'])) {
            foreach ($this->data['Links'] as $link) {
                $logoRight = $this->WebSearchLinksLogos->findByName($link['search_link_logo_right_id']);
                $link['search_link_logo_right_id'] = $logoRight['WebSearchLinksLogos']['id'];
                $logoLeft = $this->WebSearchLinksLogos->findByName($link['search_link_logo_left_id']);
                $link['search_link_logo_left_id'] = $logoLeft['WebSearchLinksLogos']['id'];
                $style = $this->WebSearchLinksStyles->findByName($link['search_link_style_id']);
                $link['search_link_style_id'] = $style['WebSearchLinksStyles']['id'];
                if ($link['image']['name'] != '') {
                    $link['tmp_image_name'] = $link['image']['tmp_name'];
                    $link['image'] = $this->__upload($link['image'], $link['label']);
                } else {
                    unset($link['image']);
                }
                $this->WebSearchLinks->save($link);
            }
        }
    }

    private function doSubAction() {
        if (isset($_GET['subAction'])) {
            switch ($_GET['subAction']) {
                case 'delete':
                    $this->WebSearchLinks->delete($_GET['id']);
                    break;

                case 'add':
                    $this->addCatLink();
                    break;

                case 'copy':
                    $this->copyCatLinks();
                    break;

                default:
                    break;
            }
        }
    }

    private function deleteTargetLinks() {
        $city = $this->WebCity->findBySlug($this->targetCitySlug);
        $targetCityID = $city['WebCity']['id'];
        $targetTypeID = $this->targetTypeID;
        $this->WebSearchLinks->deleteAllByCityIdAndTypeId($targetCityID, $targetTypeID);
    }

    private function copyCatLinks() {
        if (!$this->isSourceEqualToTarget()) {
            $this->deleteTargetLinks();
            $actualLinks = $this->WebSearchLinks->findAllByCityIdAndTypeId($this->getActualCityID(), $this->actualTypeID, true);
            $this->duplicateCatLinks($actualLinks);
            $this->actualCitySlug = $this->targetCitySlug;
            $this->actualTypeID = $this->targetTypeID;
        }
    }

    private function isSourceEqualToTarget() {
        return ($this->actualCitySlug == $this->targetCitySlug && $this->actualTypeID == $this->targetTypeID);
    }

    private function duplicateCatLinks($actualLinks) {
        foreach ($actualLinks as $link) {
            $city = $this->WebCity->findBySlug($this->targetCitySlug);
            $data = array(
                'SearchLinks' => array(
                	'created' => date('Y-m-d H:i:s'),
                	'created_by' => $this->Auth->user ('id'),
                	'image' => $link['SearchLinks']['image'],
                    'city_id' => $city['WebCity']['id'],
                    'search_link_type_id' => $this->targetTypeID,
                    'priority' => $link['SearchLinks']['priority'],
                    'label' => $link['SearchLinks']['label'],
                    'description' => $link['SearchLinks']['description'],
                    'url' => $link['SearchLinks']['url']
                )
            );
            $this->WebSearchLinks->create();
            $this->WebSearchLinks->save($data);
        }
    }

    private function getCitiesOptions() {
        $conditions = array(
            'is_approved' => 1,
        );
        $cities = $this->WebCity->find('all', array(
            'conditions' => $conditions,
            'order' => 'order ASC'
        ));
        $citiesOptions = array();
        foreach ($cities as $cityActive) {
            $citiesOptions[$cityActive['WebCity']['slug']] = $cityActive['WebCity']['name'];
        }
        return $citiesOptions;
    }

    private function getActualCityID() {
        $city = $this->WebCity->findBySlug($this->actualCitySlug);
        return $city['WebCity']['id'];
    }

    private function setVarsForView() {
        $this->set('typeValue', $this->actualTypeID);
        $this->set('citiesOptions', $this->getCitiesOptions());
        $this->set('citySlugValue', $this->actualCitySlug);
        $this->set('cSlug', $this->Cookie->read('city_slug'));
        $links = $this->WebSearchLinks->findAllByCityIdAndTypeId($this->getActualCityID(), $this->actualTypeID, true);
        $linksLogos = $this->getSearchLinksLogos();
        $linksStyles = $this->getSearchLinksStyles();
        $this->data['Links'] = $links;
        $this->data['LinksLogos'] = $linksLogos;
        $this->data['LinksStyles'] = $linksStyles;
        $this->ApiLogger->debug(__METHOD__, $this->data['Links']);
        $this->ApiLogger->debug(__METHOD__, 'numero de elementos:'.sizeof($this->data['Links']));
    }

    public function getSearchLinksLogos() {
        $LinksLogos = array();
        $logos = $this->WebSearchLinksLogos->findAll();
        foreach ($logos as $logo) {
            $LinksLogos[] = $logo['WebSearchLinksLogos'];
        }
        return $LinksLogos;
    }

    public function getSearchLinksStyles() {
        $LinksStyles = array();
        $styles = $this->WebSearchLinksStyles->findAll();
        foreach ($styles as $style) {
            $LinksStyles[] = $style['WebSearchLinksStyles'];
        }
        return $LinksStyles;
    }

    function __upload($file_array, $link_name, $targetDir = '/img/link/') {
        $file = new File($file_array['tmp_name']);
        $file_base = pathinfo($file_array['name']);
        if ($file_base['extension'] != '') {
            $ext = strtolower($file_base['extension']);
            $name = $link_name;
            if ($ext != 'jpg' && $ext != 'jpeg' && $ext != 'gif' && $ext != 'png') {
                $this->Session->setFlash('S&oacute;lo se pueden subir im&aacute;genes!', true);
                $this->render();
            } else {
                $data = $file->read();
                $file->close();
                $rndmSeed = $this->__str_rand(10);
                $cleanName = $this->__cleanChars($name);
                $full_path = $targetDir . "link" . $cleanName . "-" . $rndmSeed . "." . $ext;
                $file = new File(WWW_ROOT . $full_path, true);
                $file->write($data);
                $file->close();
                return $full_path;
            }
        }
    }

    function __cleanChars($string) {
        $string = str_replace(array(
            ' ',
            '-'
                ), array(
            '_',
            '_'
                ), $string);
        $string = str_replace(array(
            'ÃƒÂ¡',
            'ÃƒÂ©',
            'ÃƒÂ­',
            'ÃƒÂ³',
            'ÃƒÂº',
            'ÃƒÂ ',
            'ÃƒÂ¨',
            'ÃƒÂ¬',
            'ÃƒÂ²',
            'ÃƒÂ¹',
            'ÃƒÂ¤',
            'ÃƒÂ«',
            'ÃƒÂ¯',
            'ÃƒÂ¶',
            'ÃƒÂ¼',
            'Ãƒï¿½',
            'Ãƒâ€°',
            'Ãƒï¿½',
            'Ãƒâ€œ',
            'ÃƒÅ¡',
            'Ãƒâ‚¬',
            'ÃƒË†',
            'ÃƒÅ’',
            'Ãƒâ€™',
            'Ãƒâ„¢',
            'Ãƒâ€ž',
            'Ãƒâ€¹',
            'Ãƒï¿½',
            'Ãƒâ€“',
            'ÃƒÅ“'
                ), array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U'
                ), $string);
        return $string;
    }

    function __str_rand($length = 8) {
        $output = 'abcdefghijklmnopqrstuvwqyz0123456789';
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str.= $output[mt_rand(0, strlen($output) - 1)];
        }
        return $str;
    }

}
