<?php
class ContactsController extends AppController
{
  var $name = 'Contacts';
  var $components = array(
                          'Email',
                          'RequestHandler'
                          );
  var $uses = array(
                    'Contact',
                    'EmailTemplate',
                    'UserProfile'
                    );

    function beforeFilter()
    {
      // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
      parent::beforeFilter();
    }


  function add()
  {
    if (!empty($this->data)) {
      $this->Contact->set($this->data);
      if ($this->Contact->validates()) {
        $ip = $this->RequestHandler->getClientIP();
        $this->data['Contact']['ip'] = $ip;
        $this->data['Contact']['user_id'] = $this->Auth->user('id');
        $emailFindReplace = array(
                                  '##SITE_NAME##' => Configure::read('site.name') ,
                                  '##FIRST_NAME##' => $this->data['Contact']['first_name'],
                                  '##LAST_NAME##' => !empty($this->data['Contact']['last_name']) ? ' ' . $this->data['Contact']['last_name'] : '',
                                  '##FROM_EMAIL##' => $this->data['Contact']['email'],
                                  '##FROM_URL##' => Router::url(array(
                                                                      'controller' => 'contacts',
                                                                      'action' => 'add'
                                                                      ) , true) . '?from=mailing',
                                  '##SITE_ADDR##' => gethostbyaddr($ip) ,
                                  '##IP##' => $ip,
                                  '##TELEPHONE##' => $this->data['Contact']['telephone'],
                                  '##MESSAGE##' => $this->data['Contact']['message'],
                                  '##SUBJECT##' => $this->data['Contact']['subject'],
                                  '##POST_DATE##' => date('F j, Y g:i:s A (l) T (\G\M\TP)') ,
                                  '##CONTACT_URL##' => Router::url('/' , true).'contactus' . '?from=mailing',
                                  '##SITE_URL##' => Router::url('/', true) . '?from=mailing',
                                  '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
                                  );
        // send to contact email
        $email = $this->EmailTemplate->selectTemplate('Contact Us');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->from = strtr($this->Email->from, $emailFindReplace);
        $this->Email->replyTo = strtr($this->Email->replyTo, $emailFindReplace);
        $this->Email->to = Configure::read('site.contact_email');
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
        // reply email
        $email = $this->EmailTemplate->selectTemplate('Contact Us Auto Reply');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $this->data['Contact']['email'];
        $this->Email->from = strtr($this->Email->from, $emailFindReplace);
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
        $this->set('success', 1);
      }
      else {
        $this->Session->setFlash(__l('Contact could not be added. Please, try again.') , 'default', null, 'error');
      }
      unset($this->data['Contact']['captcha']);
    }
    else {
      $SignedInUserDetail = $this->UserProfile->find('first', array(
                                                                    'conditions' => array(
                                                                                          'UserProfile.user_id' => $this->Auth->user('id')
                                                                                          ) ,
                                                                    'contain' => array(
                                                                                       'User' => array(
                                                                                                       'fields' => array(
                                                                                                                         'User.id' ,
                                                                                                                         'User.email'
                                                                                                                         )
                                                                                                       )
                                                                                       ) ,
                                                                    'fields' => array(
                                                                                      'UserProfile.first_name' ,
                                                                                      'UserProfile.last_name' ,
                                                                                      ) ,
                                                                    'recursive' => 0
                                                                    ));
      $this->data['Contact']['first_name'] = !empty($SignedInUserDetail['UserProfile']['first_name']) ? $SignedInUserDetail['UserProfile']['first_name'] : '';
      $this->data['Contact']['last_name'] = !empty($SignedInUserDetail['UserProfile']['last_name']) ? $SignedInUserDetail['UserProfile']['last_name'] : '';
      $this->data['Contact']['email'] = !empty($SignedInUserDetail['User']['email']) ? $SignedInUserDetail['User']['email'] : '';
    }
    $this->pageTitle = __l('Contact Us');
  }
}
?>