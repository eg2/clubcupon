<?php

class PaymentOptionsController extends AppController {

    const ACCOUNT_NORMAL = 'ACCOUNT_NORMAL';
    const ACCOUNT_TOURISM = 'ACCOUNT_TOURISM';
    const ACCOUNT_ALL = 'ACCOUNT_ALL';
    const STATUS_UPCOMING = 'STATUS_UPCOMING';
    const STATUS_OPEN_OR_TIPPED = 'STATUS_OPEN_OR_TIPPED';
    const STATUS_ALL = 'STATUS_ALL';
    const CITY_ALL = 'CITY_ALL';

    var $name = 'PaymentOptions';
    var $uses = array(
        'Deal',
        'PaymentOption',
        'PaymentOptionDeal',
        'City'
    );

    private function statuses() {
        return array(
            self::STATUS_UPCOMING => 'Proximas',
            self::STATUS_OPEN_OR_TIPPED => 'Abiertas o en marcha',
            self::STATUS_ALL => 'Todas'
        );
    }

    private function selectedDealStatusIdList() {
        $options = array(
            self::STATUS_UPCOMING => array(1),
            self::STATUS_OPEN_OR_TIPPED => array(2, 5),
            self::STATUS_ALL => array(1, 2, 5)
        );
        $idList = array();
        if (!empty($this->data['status'])) {
            $idList = $options[$this->data['status']];
        }
        return $idList;
    }

    private function accounts() {
        return array(
            self::ACCOUNT_NORMAL => 'Normal',
            self::ACCOUNT_TOURISM => 'Turismo',
            self::ACCOUNT_ALL => 'Todas'
        );
    }

    private function selectedAccountList() {
        $options = array(
            self::ACCOUNT_NORMAL => array(0),
            self::ACCOUNT_TOURISM => array(1),
            self::ACCOUNT_ALL => array(0, 1)
        );
        $accountList = array();
        if (!empty($this->data['account'])) {
            $accountList = $options[$this->data['account']];
        }
        return $accountList;
    }

    private function options() {
        return $this->PaymentOption->findAllForMultipleSelect();
    }

    private function selectedPaymentOptionIdList() {
        $selectedKeys = array();
        foreach ($this->data['PaymentOption'] as $key => $value) {
            if ($this->data['PaymentOption'][$key]['selected']) {
                $selectedKeys[] = $key;
            }
        }
        return $selectedKeys;
    }

    private function selectableCities() {
        $selectableCities[self::CITY_ALL] = 'Todas';
        $selectableCities += $this->City->findListIsAproved();
        return $selectableCities;
    }

    private function selectedCityId() {
        $selectedCityId = false;
        if ($this->data['city'] != self::CITY_ALL) {
            $selectedCityId = $this->data['city'];
        }
        return $selectedCityId;
    }

    function admin_assign() {
        if (isset($this->params['form']['cancel'])) {
            $this->redirect(array('controller' => 'deals', 'action' => 'admin_search'));
        }
        if ($this->RequestHandler->isPost()) {
            $dealStatusIdList = $this->selectedDealStatusIdList();
            $accountList = $this->selectedAccountList();
            $paymentOptionIdList = $this->selectedPaymentOptionIdList();
            $cityId = $this->selectedCityId();
            $message = 'Las opciones de pago seleccionadas se asignaron correctamente.';
            $messageKey = 'success';
            if (!empty($dealStatusIdList) && !empty($accountList) && !empty($paymentOptionIdList)) {
                $deleted = $this
                        ->PaymentOptionDeal
                        ->deleteByDealStatusIdListAccountListAndCityId(
                        $dealStatusIdList, $accountList, $cityId
                );
                $generated = $this
                        ->PaymentOptionDeal
                        ->generate(
                        $dealStatusIdList, $accountList, $paymentOptionIdList, $cityId
                );
                if (!$deleted) {
                    $message = 'No fue posible eliminar las opciones de pago previamente asignadas.';
                    $messageKey = 'error';
                }
                if (!$generated) {
                    $message = 'No fue posible asignar las opciones de pago seleccionadas.';
                    $messageKey = 'error';
                }
            } else {
                $message = 'No se seleccionaron opcines de pago.';
                $messageKey = 'flash';
            }
            $this->Session->setFlash(
                    $message, 'default', array(), $messageKey
            );
        }
        $this->set('accounts', $this->accounts());
        $this->set('statuses', $this->statuses());
        $this->set('options', $this->options());
        $this->set('selectableCities', $this->selectableCities());
    }

}
