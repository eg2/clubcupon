<?php
class GiftUsersController extends AppController
{
    var $name = 'GiftUsers';
    var $components = array(
        'Email',
    );
    var $uses = array(
        'GiftUser',
        'EmailTemplate'
    );
    function beforeFilter()
    {
        if (!$this->GiftUser->User->isAllowed($this->Auth->user('user_type_id'))) {
            $this->cakeError('error404');
        }
        $this->Security->disabledFields = array(
            'GiftUser.from',
            'GiftUser.submit',
			'GiftUser.id',
			'GiftUser.r',
			'GiftUser.more_action_id'
        );
          parent::beforeFilter();
    }
    function index()
    {
        if ((!$this->GiftUser->User->isAllowed($this->Auth->user('user_type_id')))) {
            $this->cakeError('error404');
        }
        $this->pageTitle = __l('My Gift Cards');
        $conditions = array();
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'sent') {
            $conditions['GiftUser.user_id'] = $this->Auth->user('id');
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'received') {
			 $conditions['or'] = array(
                'GiftUser.gifted_to_user_id = ' => $this->Auth->user('id') ,
                'GiftUser.friend_mail = ' => $this->Auth->user('email') ,
            );
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'all') {
            $conditions['or'] = array(
                'GiftUser.user_id = ' => $this->Auth->user('id') ,
                'GiftUser.friend_mail = ' => $this->Auth->user('email') ,
            );
        }
        $this->GiftUser->recursive = 1;
        $this->paginate = array(
            'conditions' => $conditions,
			'contain' => array(
				'User' => array(
					'UserAvatar' => array(
						'fields' => array(
							'UserAvatar.id',
							'UserAvatar.filename',
							'UserAvatar.dir',
							'UserAvatar.width',
							'UserAvatar.height'
						)
					)
				),
				'GiftedToUser' => array(
					'UserAvatar' => array(
						'fields' => array(
							'UserAvatar.id',
							'UserAvatar.filename',
							'UserAvatar.dir',
							'UserAvatar.width',
							'UserAvatar.height'
						)
					)
				)
			),
            'order' => array(
                'GiftUser.id' => 'desc'
            )
        );
        $this->set('giftUsers', $this->paginate());
        $this->set('sent', $this->GiftUser->find('count', array(
            'conditions' => array(
                'GiftUser.user_id = ' => $this->Auth->user('id')
            ) ,
            'recursive' => - 1
        )));
		 $count_conditions['or'] = array(
			'GiftUser.gifted_to_user_id = ' => $this->Auth->user('id') ,
			'GiftUser.friend_mail = ' => $this->Auth->user('email') ,
		);
        $this->set('received', $this->GiftUser->find('count', array(
            'conditions' => $count_conditions,
            'recursive' => - 1
        )));
        $this->log('titulo:'.$this->pageTitle);
        $title_for_layout = $this->pageTitle; //titulo
		$this->set('title_for_layout', $title_for_layout);
    }
    function view_gift_card($coupon_code = null)
    {
        $this->pageTitle = __l('Gift Card');
        if (is_null($coupon_code)) {
            $this->cakeError('error404');
        }
        $giftUser = $this->GiftUser->find('first', array(
            'conditions' => array(
                'GiftUser.coupon_code = ' => $coupon_code
            ) ,
            'contain' => array(
				'User' => array(
					'fields' => array(
						'User.user_type_id',
						'User.username',
						'User.id',
					)
				)
			) ,
            'recursive' => 0,
        ));
        if (empty($giftUser)) {
            $this->cakeError('error404');
        }
		$this->Session->write('gift_user_id', $giftUser['GiftUser']['user_id']);
        $this->set('giftUser', $giftUser);
    }
    function add()
    {
        $this->pageTitle = __l('Customize Your Gift Card');
        if (!$this->GiftUser->User->isAllowed($this->Auth->user('user_type_id'))) {
            $this->cakeError('error404');
        }
        $user_available_balance = $this->GiftUser->User->checkUserBalance($this->Auth->user('id'));
        if (!$user_available_balance) {
            $this->Session->setFlash(__l('You don\'t have sufficient balance to buy this gift') , 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'add_to_wallet'
            ));
        }
        if (!empty($this->data)) {
            $coupon_code = $this->_uuid();
            $this->data['GiftUser']['coupon_code'] = $coupon_code;
            $this->data['GiftUser']['is_redeemed'] = 0;
			$this->data['GiftUser']['from'] = (!empty($this->data['GiftUser']['from'])) ? $this->data['GiftUser']['from'] : $this->Auth->user('username');
            $this->GiftUser->create();
            if ($this->GiftUser->save($this->data)) {
                $giftusers_id = $this->GiftUser->getLastInsertId();
                $transaction['Transaction']['user_id'] = $this->Auth->user('id');
                $transaction['Transaction']['foreign_id'] = $giftusers_id;
                $transaction['Transaction']['class'] = 'GiftUser';
                $transaction['Transaction']['amount'] = $this->data['GiftUser']['amount'];
                $transaction['Transaction']['transaction_type_id'] = ConstTransactionTypes::GiftSent;
                $this->GiftUser->User->Transaction->log($transaction);
                $this->GiftUser->User->updateAll(array(
                    'User.available_balance_amount' => 'User.available_balance_amount -' . $this->data['GiftUser']['amount'],
                ) , array(
                    'User.id' => $this->Auth->user('id')
                ));

				$this->_send_gift_coupon_mail($this->data);
                $this->Session->setFlash(__l('Gift has been sent successfully.') , 'default', null, 'success');
                $this->redirect(array(
                    'controller' => 'users',
                    'action' => 'my_stuff#' . __l('My_Gift_Cards')
                ));
            } else {
                $this->Session->setFlash(__l('Gift could not be send. Please, try again.') , 'default', null, 'error');
            }
        } else {
            $user_available_balance = $this->GiftUser->User->checkUserBalance($this->Auth->user('id'));
            if ($user_available_balance <= 0) {
                $this->GiftUser->validationErrors['amount'] = __l('You don\'t have sufficient balance to buy this gift');
                $this->set('low_user_balance', $user_available_balance);
            }
            $this->data['GiftUser']['user_id'] = $this->Auth->user('id');
            $this->data['GiftUser']['from'] = $this->Auth->user('username');
            $this->data['GiftUser']['amount'] = '';
            $this->data['GiftUser']['friend_name'] = '';
            $this->data['GiftUser']['message'] = '';
        }
    }
	function _send_gift_coupon_mail($giftUser){
		 $emailFindReplace = array(
				'##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
                '##SITE_NAME##' => Configure::read('site.name') ,
                '##FRIEND_NAME##' => $giftUser['GiftUser']['friend_name'],
                '##FROM##' => $this->Auth->user('username') ,
                '##TO##' => $giftUser['GiftUser']['friend_name'],
                '##TO_MAIL##' => $giftUser['GiftUser']['friend_mail'],
                '##MESSAGE##' => $giftUser['GiftUser']['message'],
                '##GIFT_CODE##' => $giftUser['GiftUser']['coupon_code'],
                '##GIFT_AMOUNT##' => $giftUser['GiftUser']['amount'],
                '##CURRENCY_CODE##' => Configure::read('site.currency') ,
                '##BACKGROUND_IMAGE##' => Router::url(array(
                    'controller' => 'img',
                    'action' => 'gift-card.png',
                    'admin' => false
                ) , true) ,
                '##REDEEM_LINK##' => Router::url(array(
                    'controller' => 'gift_users',
                    'action' => 'view_gift_card',
                    $giftUser['GiftUser']['coupon_code'],
                    'admin' => false
                ) , true) . '?from=mailing' ,
                '##SITE_LOGO##' => Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'logo-email.png',
                    'admin' => false
                ) , true) ,
                '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            );
            $template = $this->EmailTemplate->selectTemplate('Gift Coupon');
			$this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
			$this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
            $this->Email->to = $giftUser['GiftUser']['friend_mail'];
            $this->Email->subject = strtr($template['subject'], $emailFindReplace);
            $this->Email->content = strtr($template['email_content'], $emailFindReplace);
			$this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
            $this->Email->send($this->Email->content);
	}

    function resend($gift_id = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        if (is_null($gift_id)) {
            $this->cakeError('error404');
        }
        $giftUser = $this->GiftUser->find('first', array(
            'conditions' => array(
                'GiftUser.id' => $gift_id
            ) ,
            'recursive' => - 1
        ));
        if (!empty($giftUser)) {
			$this->_send_gift_coupon_mail($giftUser);
            $this->Session->setFlash(sprintf(__l('Gift mail resended sucessfully.')) , 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'my_stuff',
                '#' . __l('My_Gift_Cards')
            ));
        } else {
            $this->cakeError('error404');
        }
    }
    function redeem($coupon_code = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        if (is_null($coupon_code)) {
            $this->cakeError('error404');
        }
        $giftUser = $this->GiftUser->find('first', array(
            'conditions' => array(
                'GiftUser.coupon_code' => $coupon_code,
            ) ,
            'recursive' => - 1
        ));
		if (empty($giftUser) || (Configure::read('GiftCard.verify_email_in_redeem')) && $this->Auth->user('email') != $giftUser['GiftUser']['friend_mail']) {
            $this->Session->setFlash(__l('Invalid gift coupon, Please enter the valid coupon code') , 'default', null, 'error');
        }else if($this->Auth->user('id') == $giftUser['GiftUser']['user_id'] &&  $this->Auth->user('email') != $giftUser['GiftUser']['friend_mail']){
            $this->Session->setFlash(__l('You cannot redeem your own gift coupon.') , 'default', null, 'error');
		}else if($giftUser['GiftUser']['is_redeemed']){
			$this->Session->setFlash(__l('This Gift Coupon is already redeemed, Please enter the valid coupon code') , 'default', null, 'error');
		}
		else
		{
            $transaction['Transaction']['user_id'] = $this->Auth->user('id');
            $transaction['Transaction']['foreign_id'] = $giftUser['GiftUser']['id'];
            $transaction['Transaction']['class'] = 'GiftUser';
            $transaction['Transaction']['amount'] = $giftUser['GiftUser']['amount'];
            $transaction['Transaction']['transaction_type_id'] = ConstTransactionTypes::GiftReceived;
            $this->GiftUser->User->Transaction->log($transaction);
            $this->GiftUser->updateAll(array(
                'GiftUser.is_redeemed' => 1,
                'GiftUser.gifted_to_user_id' => $this->Auth->user('id')
            ) , array(
                'GiftUser.id' => $giftUser['GiftUser']['id']
            ));
            $this->GiftUser->User->updateAll(array(
                'User.available_balance_amount' => 'User.available_balance_amount + ' . $giftUser['GiftUser']['amount'],
            ) , array(
                'User.id' => $this->Auth->user('id')
            ));
            $this->Session->setFlash(sprintf(__l('Gift redeemed sucessfully.')) , 'default', null, 'success');
        }
		$this->redirect(array(
				'controller' => 'users',
				'action' => 'my_stuff'. '?from=redeem' . '#' . __l('My_Gift_Cards')
		));
    }
    function redeem_gift()
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        if (!empty($this->data['GiftUser']['coupon_code'])) {
            if (isset($this->data['GiftUser']['submit'])) {
                echo 'redirect*' . Router::url(array(
                    'controller' => 'gift_users',
                    'action' => 'redeem',
                    $this->data['GiftUser']['coupon_code'],
                ) , true);
                exit;
            } else {
                if (!empty($this->data)) {
                    $this->redirect(array(
                        'controller' => 'gift_users',
                        'action' => 'redeem',
                        $this->data['GiftUser']['coupon_code'],
                        'admin' => false
                    ));
                }
            }
        } else {
            if (isset($this->data['GiftUser']['submit'])) {
                $this->GiftUser->validationErrors['coupon_code'] = __l('Required');
            }
        }
    }
    function admin_index()
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Gift Cards');
        $conditions = array();
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'redeemed') {
            $conditions['GiftUser.is_redeemed'] = 1;
            $this->pageTitle = __l('Redemmed Gift Cards');
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'new') {
            $conditions['GiftUser.is_redeemed'] = 0;
            $this->pageTitle = __l('New Gift Cards');
        }
        $this->GiftUser->recursive = 2;
        $this->paginate = array(
            'conditions' => $conditions,
			'contain' => array(
				'User' => array(
					'UserAvatar' => array(
						'fields' => array(
							'UserAvatar.id',
							'UserAvatar.filename',
							'UserAvatar.dir',
							'UserAvatar.width',
							'UserAvatar.height'
						)
					)
				),
				'GiftedToUser' => array(
					'UserAvatar' => array(
						'fields' => array(
							'UserAvatar.id',
							'UserAvatar.filename',
							'UserAvatar.dir',
							'UserAvatar.width',
							'UserAvatar.height'
						)
					)
				)
			),
            'order' => array(
                'GiftUser.id' => 'desc'
            ) ,
        );
        $this->set('giftUsers', $this->paginate());
        $this->set('redeemed', $this->GiftUser->find('count', array(
            'conditions' => array(
                'GiftUser.is_redeemed' => 1,
            ) ,
            'recursive' => - 1
        )));
        $this->set('new_gifts', $this->GiftUser->find('count', array(
            'conditions' => array(
                'GiftUser.is_redeemed' => 0,
            ) ,
            'recursive' => - 1
        )));
        $this->set('pageTitle', $this->pageTitle);
        $moreActions = $this->GiftUser->moreActions;
        $this->set(compact('moreActions'));
    }
    function admin_update()
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        $this->autoRender = false;
        if (!empty($this->data['GiftUser'])) {
            $r = $this->data['GiftUser']['r'];
            $actionid = $this->data['GiftUser']['more_action_id'];
            unset($this->data['GiftUser']['r']);
            unset($this->data['GiftUser']['more_action_id']);
            $userIds = array();
            foreach($this->data['GiftUser'] as $gift_id => $is_checked) {
                if ($is_checked['id']) {
                    $giftIds[] = $gift_id;
                }
            }
            if ($actionid && !empty($giftIds)) {
                if ($actionid == ConstMoreAction::Delete) {
                    $this->GiftUser->deleteAll(array(
                        'GiftUser.id' => $giftIds
                    ));
                    $this->Session->setFlash(__l('Checked gift cards has been deleted') , 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }
    function admin_delete($id = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->GiftUser->del($id)) {
            $this->Session->setFlash(__l('Gift deleted') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }
}
?>