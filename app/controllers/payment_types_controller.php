<?php
class PaymentTypesController extends AppController
{
   var $name = 'PaymentTypes';
   function beforeFilter()
   {
     // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
     parent::beforeFilter();
   }
   function admin_index()
    {
        $this->pageTitle = __l('Payment Types');
        $this->PaymentType->recursive = -1;
        $this->set('paymentTypes', $this->paginate());
    }
   function admin_edit($id = null)
    {
        $this->pageTitle = __l('Edit Payment Type');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->PaymentType->save($this->data)) {
                $this->Session->setFlash(__l('Payment Type has been updated') , 'default', null, 'success');
				$this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__l('Payment Type could not be updated. Please, try again.') , 'default', null, 'error');
            }
        } else {
            $this->data = $this->PaymentType->read(null, $id);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['PaymentType']['name'];
    }

}
?>