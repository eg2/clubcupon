<?php

class OffersController extends AppController {

    public $name = 'Offers';
    public $uses = null;
    public $components = array(
        'offer.OfferService',
    );

    private function redirectToDealEdit($deal) {
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'edit',
                    $deal['Deal']['id']
        ));
    }

    private function redirectToDealIndex() {
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
        ));
    }

    function admin_replicate() {
        $replica = $this->OfferService->replicate($this->params['named']['deal_id']);
        if (!empty($replica)) {
            $this->Session->setFlash('Replicación de oferta', 'default', null, 'success');
            return $this->redirectToDealEdit($replica);
        } else {
            $this->Session->setFlash('Imposible replicar oferta', 'default', null, 'error');
            return $this->redirectToDealIndex();
        }
    }
    
    function admin_resell() {
    	$replicaForResell = $this->OfferService->replicateForResell($this->params['named']['deal_id']);
    	if (!empty($replicaForResell)) {
    		$this->Session->setFlash('Oferta para Reventa', 'default', null, 'success');
    		return $this->redirectToDealEdit($replicaForResell);
    	} else {
    		$this->Session->setFlash('No se pudo crear la Oferta para Reventa', 'default', null, 'error');
    		return $this->redirectToDealIndex();
    	}
    }

}
