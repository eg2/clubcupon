<?php
App::import('Model', 'product.ProductCampaign');

class CampaignsController extends AppController {

    var $name = 'Campaigns';
    var $uses = array(
        'Product.ProductCampaign',
        'Product.ProductProduct',
        'Companies',
        'Deal'
    );
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'ProductCampaign.id' => 'desc',
        ),
        'contain' => array(
            'Company',
            'ProductProduct',
        ),
    );

    function __construct() {
    	
    	$this->ProductCampaign=&new ProductCampaign();
    	parent::__construct();
    }
    
    
    function admin_index() {
        $this->pageTitle = 'Listado de campa&ntilde;as';
        if (!empty($this->params['named']['campaign_name'])) {
            $findConditions['ProductCampaign.name LIKE'] = '%' . $this->params['named']['campaign_name'] . '%';
            $pagingLinksOptions['campaign_name'] = $this->params['named']['campaign_name'];
            $searchDefaultCampaignName = $this->params['named']['campaign_name'];
        }
        if (!empty($this->params['named']['company_id'])) {
            $findConditions['ProductCampaign.company_id'] = $this->params['named']['company_id'];
            $pagingLinksOptions['company_id'] = $this->params['named']['company_id'];
            $searchDefaultCompany = $this->params['named']['company_id'];
        }
        if (!empty($this->data['campaigns']['campaign_name'])) {
            $findConditions['ProductCampaign.name LIKE'] = '%' . $this->data['campaigns']['campaign_name'] . '%';
            $pagingLinksOptions['campaign_name'] = $this->data['campaigns']['campaign_name'];
            $searchDefaultCampaignName = $this->data['campaigns']['campaign_name'];
        }
        if (!empty($this->data['campaigns']['company_id'])) {
            $findConditions['ProductCampaign.company_id'] = $this->data['campaigns']['company_id'];
            $pagingLinksOptions['company_id'] = $this->data['campaigns']['company_id'];
            $searchDefaultCompany = $this->data['campaigns']['company_id'];
        }
        if (!empty($findConditions)) {
            $this->paginate['options'] = $pagingLinksOptions;
        }
        $this->paginate['conditions'] = $findConditions;
        $campaigns = $this->paginate();
        $campaignsList = $this->ProductCampaign->find('list');
        $companiesList = $this->Company->getOrderedCompaniesList();
        $this->set(compact('companiesList', 'campaigns', 'campaignsList', 'searchDefaultCompany', 'searchDefaultCampaignName'));
    }

    function admin_add() {
        $companiesList = $this->Company->getOrderedCompaniesList();
        if (!empty($this->data)) {
            $this->ProductCampaign->set($this->data['campaign']);
            $this->ProductCampaign->set('created_by', $this->Auth->user('id'));
            if ($this->ProductCampaign->save()) {
                $this->Session->setFlash('Campaña guardada', true);
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash('No se pudo guardar la campaña', true);
            }
        }
        $this->set(compact('companiesList'));
    }

    function admin_edit($id) {
        if (!empty($this->data)) {
            $this->ProductCampaign->id = $id;
            $this->ProductCampaign->set($this->data);
            $this->ProductCampaign->set('modified_by', $this->Auth->user('id'));
            if ($this->ProductCampaign->save()) {
                $this->Session->setFlash('Campaña editada', true);
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $validationErrors['ProductCampaign'] = $this->ProductCampaign->validationErrors;
                $this->Session->setFlash('No se pudo editar la campaña', true);
            }
        }
        $companiesList = $this->Company->getOrderedCompaniesList();
        $this->data = $this->ProductCampaign->read(null, $id);
        $this->set(compact('companiesList', 'validationErrors'));
    }

    function admin_delete($id) {
        if ($this->Deal->returnOpenUpcomingOrTippedCountByCampaignId($id) == 0) {
            try {
                // Borramos el registro, sus productos y notificamos
                $this->ProductCampaign->delete($id);
                $this->ProductProduct->deleteAllByCampaignId($id, $this->Auth->user('id'));
                $this->Session->setFlash('Se borro correctamente la campaña .');
            } catch (Exception $e) {
                $this->Session->setFlash('La campaña no pudo borrarse. Error: ' . $e->getMessage());
            }
        } else {
            $this->Session->setFlash('Esta campaña pertenece a una oferta Próxima, Abierta, o En Marcha, y no puede borrarse');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_search() {
        $this->render('admin_index');
    }

    function admin_all_by_company_id($id) {
        $this->layout = 'ajax';
        $campaigns = $this->ProductCampaign->findAllWithProductsByCompanyId($id);
        $this->set('campaigns', $campaigns);
    }

}
