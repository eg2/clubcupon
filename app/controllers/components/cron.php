<?php

class CronComponent extends Object {
  var $controller;


  function send_deals_mailing() {
    App::import('Core'      ,'Router');
    App::import('Model', 'Deal');
    App::import('Component', 'Email');
    App::import('Model','EmailTemplate');
    App::import('Model','Subscription');
    Debugger::log('send_deals_mailing begin',LOG_DEBUG);
    $this->Deal = &new Deal();
    $this->Subscription = &new Subscription();
    $this->EmailTemplate = &new EmailTemplate();
    $this->Email = &new EmailComponent();
    $this->Email->delivery =   Configure::read('site.email.delivery');
    $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
    require_once (LIBS . 'router.php');
    //send subscription mail
    $this->Deal->_sendSubscriptionMail();
  }
  
    function reportDealsWithExpiredCouponsToAccountingServiceComponent() {
    
      $this->Deal = &new Deal();
        
      $dealsWithExpiredCoupons = $this->Deal->find('all', array(
          'fields' => array(
              'Deal.id',
              'Deal.company_id',
          ) ,
          'conditions' => array(
              'coupon_expiry_date <= ' => date('Y-m-d H:i:s'),
              'is_pending_accounting_event'    => 0
          ) ,
          'recursive' => - 1
      ));
      
      
      if (!empty($dealsWithExpiredCoupons)) {
          
          App::import('Component', 'api.ApiLogger');
          App::import('Component', 'AccountingEventService');
          $AccountingEventService = new AccountingEventServiceComponent(); 
          
          foreach($dealsWithExpiredCoupons as $deal) {
              
            try {
                $AccountingEventService->registerAccountingEvent($deal);
             } catch(Exception $e) {
                 
                $this->ApiLogger->error(
                    "Hubo una falla al reportar el evento contable de cambio de estado de los cupones",
                    array('errorCode' => $e->getMessage())
                    );
             }
              
          }
      }
    }
  
  function update_deal() {

    App::import('Core','Router');
    App::import('Model', 'Deal');
    $this->Deal = &new Deal();
    $this->Deal->Behaviors->detach('Excludable');
    App::import('Model', 'Subscription');
    $this->Subscription = &new Subscription();
    App::import('Model', 'EmailTemplate');
    $this->EmailTemplate = &new EmailTemplate();
    App::import('Component', 'Email');
    $this->Email = &new EmailComponent();
    $this->Email->delivery =   Configure::read('site.email.delivery');
    $this->Email->smtpOptions = Configure::read('site.email.smtp_options');

	App::import('Model', 'Now.NowBranchesDeals');
	$this->NowBranchesDeals = &new NowBranchesDeals();
	App::import('Model', 'Now.NowDeal');
	$this->NowDeal = &new NowDeal();

    App::import('Model', 'UserCashWithdrawal');
    $this->UserCashWithdrawal = &new UserCashWithdrawal();
    $this->Deal->_processOpenStatus();
    require_once (LIBS . 'router.php');

    $this->reportDealsWithExpiredCouponsToAccountingServiceComponent();
    
	//"abrir" los nowdeals!
	$this->NowBranchesDeals->updateall(array(
	  'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Open,
	), array (
	  'NowDeal.start_date <= ' => date('Y-m-d H:i:s'),
	  'NowDeal.deal_status_id' => ConstDealStatus::Upcoming,
          'NowBranchesDeals.deleted' => 0,
	));

	//change status of upcoming to open
    $this->Deal->updateAll (array (
      'Deal.deal_status_id' => ConstDealStatus::Open,
    ), array (
      'Deal.start_date <= ' => date('Y-m-d H:i:s'),
      'Deal.deal_status_id' => ConstDealStatus::Upcoming,
     //   'NowBranchesDeals.deleted' => 0,
      // 'or' => array (
      //   'Deal.has_pins' => 0,
      //   'exists (select * from pins where deal_id = Deal.id and is_used = 0)',
      // ),
    ));
    //update failure deals
    $this->Deal->updateAll(array(
            'Deal.deal_status_id' => ConstDealStatus::Closed
            ) , array(
            'Deal.end_date <= ' => date('Y-m-d H:i:s') ,
            'Deal.deal_status_id' => ConstDealStatus::Open
    ));
    // Expired Tripped Deals To Closed With An Email To The Deal Owner With Deal User List
    $this->Deal->_closeDeals();



	//DELETE OLD DEALS!


        $this->NowBranchesDeals->updateall(array(
	  'NowBranchesDeals.deleted' => 1,
          'NowBranchesDeals.now_deal_status_id' =>ConstDealStatus::Closed,
	), array (
	  'NowBranchesDeals.ts_fin <= ' => date('Y-m-d H:i:s'),
	  'NowBranchesDeals.now_deal_status_id' => array(ConstDealStatus::Open,ConstDealStatus::Tipped,ConstDealStatus::Closed,ConstDealStatus::Canceled, ConstDealStatus::Upcoming),
	));


    // City wise deal count update
    $city_deal_counts = $this->Deal->find('all',array(
            'fields'=>array(
                    'Deal.city_id',
                    'count(Deal.id) as deal_count'
            ),
            'conditions'=>array(
                    'Deal.deal_status_id' =>array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped
                    )),
            'group'=>array(
                    'Deal.city_id'
            ),
            'recursive'=>-1
    ));
    $city_deal_counts_total = $this->Deal->find('all',array(
            'fields'=>array(
                    'Deal.city_id',
                    'count(Deal.id) as deal_count'
            ),
            'conditions'=>array(
                    'Deal.deal_status_id' =>array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                            ConstDealStatus::Closed,
                            ConstDealStatus::PaidToCompany,
                    )),
            'group'=>array(
                    'Deal.city_id'
            ),
            'recursive'=>-1
    ));
//    $this->Deal->City->updateAll(array(
//            'City.active_deal_count' => 0,
//            'City.deal_count' => 0
//            ),
//            array()
//    );

	if(count($city_deal_counts)){
		foreach($city_deal_counts as $city_deal_count) {
		  $this->Deal->City->updateAll(array(
				  'City.active_deal_count' => $city_deal_count['0']['deal_count']
				  ),
				  array(
				  'City.id'=>	$city_deal_count['Deal']['city_id']
				  )
		  );
		}
	}
    
    //Para todas las ciudades/grupos que NO tienen deals activos / proximos,
    $citiesIdsWithActiveDeals = array();
    foreach($city_deal_counts as $city_deal_count) {
        $citiesIdsWithActiveDeals[] = $city_deal_count['Deal']['city_id'];
    }
    
    if(count($citiesIdsWithActiveDeals)) {
        $this->Deal->City->updateAll(array(
                  'City.active_deal_count' => 0
                  ),
                  array('not' => array('City.id'=>$citiesIdsWithActiveDeals))
          );
    }
    
	if(count($city_deal_counts_total)){
		foreach($city_deal_counts_total as $deal_count) {
		   $this->Deal->City->updateAll(array(
				  'City.deal_count' => $deal_count['0']['deal_count']
				  ),
				  array(
				  'City.id'=>	$deal_count['Deal']['city_id']
				  )
		  );
		}
	}
  }

  function update_deal_by_date($yyyymmdd= false, $his = false) {
    App::import('Core','Router');
    App::import('Model', 'Deal');
    $this->Deal = &new Deal();
    $this->Deal->Behaviors->detach('Excludable');
    App::import('Model', 'Subscription');
    $this->Subscription = &new Subscription();
    App::import('Model', 'EmailTemplate');
    $this->EmailTemplate = &new EmailTemplate();
    App::import('Component', 'Email');
    $this->Email = &new EmailComponent();
    $this->Email->delivery =   Configure::read('site.email.delivery');
    $this->Email->smtpOptions = Configure::read('site.email.smtp_options');

    App::import('Model', 'Now.NowBranchesDeals');
    $this->NowBranchesDeals = &new NowBranchesDeals();
    App::import('Model', 'Now.NowDeal');
    $this->NowDeal = &new NowDeal();
    App::import('Model', 'UserCashWithdrawal');
    $this->UserCashWithdrawal = &new UserCashWithdrawal();

    /* si no recibe argumento usamos la fecha actual*/
    if(!$yyyymmdd) {
        $this->update_deal();
        return ;
    }

    list($yyyymmdd) = explode(" ", $yyyymmdd);

    $fecha  = date("Y-m-d", strtotime($yyyymmdd));
    $fecha_his  = date('Y-m-d H:i:s', strtotime(trim($yyyymmdd." ".$his)));

     if(!$this->checkDateTime($yyyymmdd." ".$his )){
         echo "ON ".__METHOD__. "invalid date for ".$yyyymmdd." ".$his."\r\n";
         Debugger::log("ON ".__METHOD__. "invalid date for ".$yyyymmdd." ".$his, LOG_DEBUG);
         return false;
      }


    $this->Deal->_processOpenStatus(null, $fecha_his);
     require_once (LIBS . 'router.php');

	//"abrir" los nowdeals!
	$this->NowBranchesDeals->updateall(array(
	  'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Open,
	), array (
	  'NowDeal.start_date <= ' => $fecha_his,
	  'NowDeal.deal_status_id' => ConstDealStatus::Upcoming,
	));

	//change status of upcoming to open
    $this->Deal->updateAll (array (
      'Deal.deal_status_id' => ConstDealStatus::Open,
    ), array (
      'Deal.start_date <= ' => $fecha_his,
      'Deal.deal_status_id' => ConstDealStatus::Upcoming,
      // 'or' => array (
      //   'Deal.has_pins' => 0,
      //   'exists (select * from pins where deal_id = Deal.id and is_used = 0)',
      // ),
    ));
    //update failure deals
    $this->Deal->updateAll(array(
            'Deal.deal_status_id' => ConstDealStatus::Closed
            ) , array(
            'Deal.end_date <= ' => $fecha_his,
            'Deal.deal_status_id' => ConstDealStatus::Open
    ));
    // Expired Tripped Deals To Closed With An Email To The Deal Owner With Deal User List
    $this->Deal->_closeDeals(array(), false, $fecha_his);
	//DELETE OLD DEALS!

            $this->NowBranchesDeals->updateall(array(
	  'NowBranchesDeals.deleted' => 1,
           'NowBranchesDeals.now_deal_status_id' =>ConstDealStatus::Closed,
	), array (
	  'NowBranchesDeals.ts_fin <= ' => $fecha_his,
	  'NowBranchesDeals.now_deal_status_id' => array(ConstDealStatus::Open,ConstDealStatus::Tipped,ConstDealStatus::Closed,ConstDealStatus::Canceled, ConstDealStatus::Upcoming),
	));


    // City wise deal count update
    $city_deal_counts = $this->Deal->find('all',array(
            'fields'=>array(
                    'Deal.city_id',
                    'count(Deal.id) as deal_count'
            ),
            'conditions'=>array(
                    'Deal.deal_status_id' =>array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped
                    )),
            'group'=>array(
                    'Deal.city_id'
            ),
            'recursive'=>-1
    ));
    $city_deal_counts_total = $this->Deal->find('all',array(
            'fields'=>array(
                    'Deal.city_id',
                    'count(Deal.id) as deal_count'
            ),
            'conditions'=>array(
                    'Deal.deal_status_id' =>array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                            ConstDealStatus::Closed,
                            ConstDealStatus::PaidToCompany,
                    )),
            'group'=>array(
                    'Deal.city_id'
            ),
            'recursive'=>-1
    ));
//    $this->Deal->City->updateAll(array(
//            'City.active_deal_count' => 0,
//            'City.deal_count' => 0
//            ),
//            array()
//    );

	if(count($city_deal_counts)){
		foreach($city_deal_counts as $city_deal_count) {
		  $this->Deal->City->updateAll(array(
				  'City.active_deal_count' => $city_deal_count['0']['deal_count']
				  ),
				  array(
				  'City.id'=>	$city_deal_count['Deal']['city_id']
				  )
		  );
		}
	}
	if(count($city_deal_counts_total)){
		foreach($city_deal_counts_total as $deal_count) {
		   $this->Deal->City->updateAll(array(
				  'City.deal_count' => $deal_count['0']['deal_count']
				  ),
				  array(
				  'City.id'=>	$deal_count['Deal']['city_id']
				  )
		  );
		}
	}
  }




  function processDealStatus ($type = '') {
    App::import('Core'      ,'Controller');
    App::import('Controller','Deals');
    App::import('Core'      ,'Router');
    App::import('Core'      ,'View');
    App::import('Component' ,'Email');
    App::import('Component' ,'Session');
    App::import('Model'     ,'Deal');
    App::import('Model'     ,'DealExternal');
    App::import('Model'     ,'EmailTemplate');

    $this->Email=new EmailComponent();
    $this->Email->Controller=new DealsController();
    $this->Email->Controller->Session=new SessionComponent();
    $view=new View($this->Email->Controller);
    $this->Email->Controller->Deal=$this->Deal=new Deal();
    $this->Email->Controller->EmailTemplate = new EmailTemplate();

    // !!! posible nudote
    $this->Email->Controller->Email=$this->Email;

    $this->Email->delivery   =Configure::read('site.email.delivery');
    $this->Email->smtpOptions=Configure::read('site.email.smtp_options');
    $this->Email->sendAs     ='html';
    // Este cronjob se encarga de actualizar el listado de pagos
    // pendientes contra el BAC.
    $this->Email->Controller->Deal->UpdateExternalDealStatus ($type);
  }

  function processSendCoupons(){
    App::import('Core'      ,'Controller');
    App::import('Controller','Deals');
    App::import('Core'      ,'Router');
    App::import('Core'      ,'View');
    App::import('Component' ,'Email');
    App::import('Component' ,'Session');
    App::import('Model'     ,'Deal');
    App::import('Model'     ,'DealExternal');
    App::import('Model'     ,'EmailTemplate');

    $this->Email=new EmailComponent();
    $this->Email->Controller=new DealsController();
    $this->Email->Controller->Session=new SessionComponent();
    $view=new View($this->Email->Controller);
    $this->Email->Controller->Deal=$this->Deal=new Deal();
    $this->Email->Controller->Deal->Behaviors->detach('Excludable');
    $this->Email->Controller->EmailTemplate=new EmailTemplate();
    // !!! posible nudote
    $this->Email->Controller->Email=$this->Email;

    $this->Email->delivery   =Configure::read('site.email.delivery');
    $this->Email->smtpOptions=Configure::read('site.email.smtp_options');
    $this->Email->sendAs     ='html';

    // http://jira.int.clarin.com/browse/CC-2214
    $selfModel = $this->Deal;
    $dataSource = $selfModel->getDataSource();
    App::import('Model', 'ProcessRun');
    $running_expiry_date = date("Y-m-d H:i:s", strtotime('-' . '4 hours', strtotime(date("Y-m-d H:i:s"))));
    $processRunName = 'processSendCoupons';
    $processRun = new ProcessRun();
    $processRunStatus = $processRun->getRunningByName($processRunName);
    if ($processRunStatus !== false && $processRunStatus['is_running'] && $processRunStatus['name'] != 'LOCK' && $processRunStatus['modified'] <= $running_expiry_date) {
        $dataSource->begin($selfModel);
        $processRun->stopRunning($processRunStatus['id']);
        $dataSource->commit($selfModel);
        $processRunStatus['is_running'] = false;
    }
    if ($processRunStatus['is_running']) {
        echo 'El proceso ya está corriendo';
        return false;
    } else {
        $dataSource->begin($selfModel);
        $processRunId = $processRun->startRunning($processRunName);
        $dataSource->commit($selfModel);
    }
    // http://jira.int.clarin.com/browse/CC-2214
    // Este cronjob se encarga de enviar los cupones para las ofertas cerradas
    // Los cupones seran marcados como `emailed`
    $this->Email->Controller->Deal->sendCreditedCupons();
    // http://jira.int.clarin.com/browse/CC-2214
    $dataSource->begin ($selfModel);
    $processRun->stopRunning ($processRunId);
    $dataSource->commit ($selfModel);
    // http://jira.int.clarin.com/browse/CC-2214
  }

  function processSendCouponsForCompany(){
    App::import('Core'      ,'Controller');
    App::import('Controller','Deals');
    App::import('Core'      ,'Router');
    App::import('Core'      ,'View');
    App::import('Component' ,'Email');
    App::import('Component' ,'Session');
    App::import('Model'     ,'Deal');
    App::import('Model'     ,'DealExternal');
    App::import('Model'     ,'EmailTemplate');

    $this->Email=new EmailComponent();
    $this->Email->Controller=new DealsController();
    $this->Email->Controller->Session=new SessionComponent();
    $view=new View($this->Email->Controller);
    $this->Email->Controller->Deal=$this->Deal=new Deal();
    $this->Email->Controller->Deal->Behaviors->detach('Excludable');
    $this->Email->Controller->EmailTemplate=new EmailTemplate();
    // !!! posible nudote
    $this->Email->Controller->Email=$this->Email;

    $this->Email->delivery   =Configure::read('site.email.delivery');
    $this->Email->smtpOptions=Configure::read('site.email.smtp_options');
    $this->Email->sendAs     ='html';
    // http://jira.int.clarin.com/browse/CC-2214
    $selfModel = $this->Deal;
    $dataSource = $selfModel->getDataSource();
    App::import('Model', 'ProcessRun');
    $running_expiry_date = date("Y-m-d H:i:s", strtotime('-' . '4 hours', strtotime(date("Y-m-d H:i:s"))));
    $processRunName = 'processSendCouponsForCOmpany';
    $processRun = new ProcessRun();
    $processRunStatus = $processRun->getRunningByName($processRunName);
    if ($processRunStatus !== false && $processRunStatus['is_running'] && $processRunStatus['name'] != 'LOCK' && $processRunStatus['modified'] <= $running_expiry_date) {
        $dataSource->begin($selfModel);
        $processRun->stopRunning($processRunStatus['id']);
        $dataSource->commit($selfModel);
        $processRunStatus['is_running'] = false;
    }
    if ($processRunStatus['is_running']) {
        return false;
    } else {
        $dataSource->begin($selfModel);
        $processRunId = $processRun->startRunning($processRunName);
        $dataSource->commit($selfModel);
    }
    // http://jira.int.clarin.com/browse/CC-2214
    // Este cronjob se encarga de mandar la lista de cupones acreditados el dia
    // anterior a la empresa.
    $this->Email->Controller->Deal->sendBuyersListCompany();
    // http://jira.int.clarin.com/browse/CC-2214
    $dataSource->begin ($selfModel);
    $processRun->stopRunning ($processRunId);
    $dataSource->commit ($selfModel);
    // http://jira.int.clarin.com/browse/CC-2214
  }

  /**
   * realiza el otorgamiento de puntos por antigüedad.
   *
   * Realiza el otorgamiento de puntos por antigüedad llamando a <code>ActionPointUser->process_seniority ()</code>.
   *
   * @access public
   * @return void
   */
  function points_seniority ()
    {
      // ActionPointUser no existe en un Cron, importarlo e instanciarlo
      App::import('Model', 'ActionPointUser');
      $this->ActionPointUser = new ActionPointUser ();

      // Llamar al método de proceso de status.
      $this->ActionPointUser->process_seniority ();
    }

  /**
   * procesa el vencimiento de puntos.
   *
   * Procesa el vencimiento de puntos llamando a <code>ActionPointUser->points_expiry ()</code>.
   *
   * @access public
   * @return void
   */
  function points_expiry ()
    {
      // ActionPointUser no existe en un Cron, importarlo e instanciarlo
      App::import('Model', 'ActionPointUser');
      $this->ActionPointUser = new ActionPointUser ();

      // Llamar al método de proceso de status.
      $this->ActionPointUser->process_expiry ();
    }


  /**
   * procesa el anuncio de los puntos proximos a vencer.
   *
   * Procesa el anuncio de los puntos proximos a vencer llamando a
   * <code>ActionPointUser->points_expiring_soon ()</code>.
   *
   * @access public
   * @return void
   */
  function points_expiring_soon ()
    {
      // ActionPointUser no existe en un Cron, importarlo e instanciarlo
      App::import('Model', 'ActionPointUser');
      $this->ActionPointUser = new ActionPointUser ();

      // Llamar al método de proceso de status.
      $this->ActionPointUser->process_expiring_soon ();
    }

   function send_payment_reminder_offline(){

       App::import('Model', 'DealExternal');

       $this->DealExternal = new DealExternal();

       /* 1er envio de mail recordatorio de pagos pendientes offline - 48hs*/
       $deal_externals = $this->DealExternal->pay_pending_by_days(0,2);

       if (count($deal_externals) > 0){

            foreach ($deal_externals as $key => $deal_external){

                $email = $deal_external['User']['email'];
                $username = $deal_external['User']['username'];
                $deal_name = $deal_external['Deal']['name'];
                $dealexternal_id = $deal_external['DealExternal']['id'];
				//$is_send_email= $deal_external['DealExternal']['is_send_email'];

                if ($this->send_reminder_payment_email($username, $email, $deal_name)) {

                    $this->DealExternal->id = $dealexternal_id;

                    $this->DealExternal->saveField('is_send_email', 1, false);

                    //CakeLog::write('debug', 'pago pendiente actualizado,id:'.$this->DealExternal->id,'/ is_send_email:'. $this->DealExternal->is_send_email);
                    Debugger::log (sprintf ('pago pendiente actualizado,id: %d / is_send_email: %d', $this->DealExternal->id,$this->DealExternal->is_send_email), LOG_DEBUG);

                }
            }

        }else{
             //CakeLog::write('debug', 'no hay pagos pendientes con antiguedad > de 2 dias con is_send_email = 0');
             Debugger::log (sprintf ('no hay pagos pendientes con antiguedad > de 2 dias con is_send_email = 0'), LOG_DEBUG);

        }
        /* 2do envio de mail recordatorio de pagos pendientes offline - 96hs*/
   	   $deal_externals = $this->DealExternal->pay_pending_by_days(1,4);

       if (count($deal_externals) > 0){

            foreach ($deal_externals as $key => $deal_external){

                $email = $deal_external['User']['email'];
                $username = $deal_external['User']['username'];
                $deal_name = $deal_external['Deal']['name'];
                $dealexternal_id = $deal_external['DealExternal']['id'];


                if ($this->send_reminder_payment_email($username, $email, $deal_name)) {

                    $this->DealExternal->id = $dealexternal_id;

                    $this->DealExternal->saveField('is_send_email', 2, false);
                    //CakeLog::write('debug', 'pago pendiente actualizado,id:'.$this->DealExternal->id,'/ is_send_email:'. $this->DealExternal->is_send_email);
                    Debugger::log (sprintf ('pago pendiente actualizado,id: %d / is_send_email: %d', $this->DealExternal->id,$this->DealExternal->is_send_email), LOG_DEBUG);
                }
            }

        }else{
               //CakeLog::write('debug', 'no hay pagos pendientes con antiguedad > de 4 dias con is_send_email = 1');
               Debugger::log (sprintf ('no hay pagos pendientes con antiguedad > de 4 dias con is_send_email = 1.'), LOG_DEBUG);

        }


     }

     function send_reminder_payment_email($username, $email, $deal_name) {

            App::import ('Model', 'EmailTemplate');
            App::import ('Component', 'Email');

            $this->Email=new EmailComponent();
            $this->EmailTemplate = new EmailTemplate();

            $emailFindReplace = array(
                '##USERNAME##' => $username,
                '##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
                '##DEAL_TITLE##' => $deal_name,
                '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . 'img/'
            );
            $template = $this->EmailTemplate->selectTemplate('Reminder offline payments');
            $this->Email->from = ($template ['from'] == '##FROM_EMAIL##' ) ? Configure::read ('EmailTemplate.from_email') : $template ['from'];
            $this->Email->to = $email;
            $this->Email->subject = strtr ($template ['subject'], $emailFindReplace);
            $this->Email->smtpOptions=Configure::read('site.email.smtp_options');
            $this->Email->content = strtr ($template ['email_content'], $emailFindReplace);
            $this->Email->sendAs = ($template ['is_html']) ? 'html' : 'text';

            return $this->Email->send($this->Email->content);
        }

     function send_expiration_reminder() {
            //Obtiene los datos de los cupones a expirar dentro de x dias, para enviarles mails de recordatorio
            App::import('Model', 'DealUser');
            $this->DealUser = &new DealUser();

            $now = time();
            $today = date('Y-m-d', $now);


            $start_date = date('Y-m-d', strtotime($today . '+ 15 day'));
            $end_date =  date('Y-m-d', strtotime($start_date . '+ 1 day'));

            if ($coupons = $this->DealUser->coupons_to_expire($start_date, $end_date)) {

                foreach ($coupons as $key => $coupon) {
                    $this->send_expiration_reminder_email($coupon);
                }
            }

    }

    function send_expiration_reminder_email($coupon){
            App::import ('Model', 'EmailTemplate');
            App::import ('Component', 'Email');


            $expire = date("d-m-Y",strtotime($coupon['Deal']['coupon_expiry_date']));

            $this->Email=new EmailComponent();
            $this->EmailTemplate = new EmailTemplate();
            $emailFindReplace = array(
                '##USERNAME##' => $coupon['User']['username'],
                '##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
                '##DEAL_TITLE##' => $coupon['Deal']['name'],
                '##PRICE##' => $coupon['Deal']['discounted_price'],
                '##EXPIRATION##' => $expire,
                '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . 'img/',
                '##URL##' => Configure::read ('static_domain_for_mails') . '/deal/' . $coupon['Deal']['slug'],
            	'##URL_LOGO_IMG##'=>'header_expired_coupon.jpg',
            	'##SITE_NAME##'=>'Club Cup&oacute;n',
            	'##SITE_URL##'=>'www.clubcupon.com.ar',
            	'##PIE_MAIL##'=>'Club Cup&oacute;n es un servicio de TECDIA S.A | <a href="http://soporte.clubcupon.com.ar" >Centro de Atenci&oacute;n al Cliente</a>',
            );
            if ($coupon['City']['is_business_unit'] == 1) {
            	$emailFindReplace['##URL_LOGO_IMG##']='header_expired_coupon_beneficios.jpg';
            	$emailFindReplace['##SITE_NAME##']= 'Nuestros Beneficios';
            	$emailFindReplace['##SITE_URL##'] = 'www.nuestrosbeneficios.com';
            	$emailFindReplace['##PIE_MAIL##'] = 'Ante cualquier consulta escribinos a <a href="mailto:soporte@nuestrosbeneficios.com" style="color:#2d6f95;text-decoration:none" target="_blank">soporte@nuestrosbeneficios.com</a>';
            	
            	$template['from'] ='soporte@nuestrosbeneficios.com';
            }
            
            
            $template = $this->EmailTemplate->selectTemplate('reminder expired coupons');
            $this->Email->from = ($template ['from'] == '##FROM_EMAIL##' ) ? Configure::read ('EmailTemplate.from_email') : $template ['from'];
            $this->Email->to = $coupon['User']['email'];
            $this->Email->subject = strtr ($template ['subject'], $emailFindReplace);
            $this->Email->smtpOptions=Configure::read('site.email.smtp_options');
            $this->Email->content = strtr ($template ['email_content'], $emailFindReplace);
            $this->Email->sendAs = ($template ['is_html']) ? 'html' : 'text';
            return $this->Email->send($this->Email->content);
        }


        public function update_expired_redemptions()
        {  
            /* Actualmente esto NO se utiliza en producción */
            App::import('Model', 'Redemption');
            $this->Redemption = &new Redemption();
            return $this->Redemption->updateExpiredNotRedeemedPosnetCodes();
        }

            function send_mail_no_buyers() {
    	CakeLog::write('debug', 'send_mail_no_buyers()');
        App::import('Model', 'Deal','DealExternal');
        $this->Deal = &new Deal();

        App::import('Model', 'User');
        $this->User = &new User();

        App::import('Model', 'EmailTemplate');
        App::import('Component', 'Email');
        $this->Email = new EmailComponent();
        $this->EmailTemplate = new EmailTemplate();
        $template = $this->EmailTemplate->selectTemplate('mail no buyers');

        $dias_atras = Configure::read('similar_deal.max_days');
        $dias_atras = date('Y-m-d H:i:s', strtotime(-1 * $dias_atras . ' day'));
        //$this->out('$dias_atras: ' . $dias_atras);
        $deals_activos = $this->Deal->query('select * from deals as Deal where (deal_status_id=2 or deal_status_id=5) and (id !=similar_deal_id or similar_deal_id is not null )  ');
        CakeLog::write('debug', 'size deals_activos : ' . count($deals_activos));
        if (!empty($deals_activos)) {
            foreach ($deals_activos as $deal) {
                $myQuery = 'SELECT distinct username,email,deals.name FROM
   users
   INNER JOIN deal_externals
     ON deal_externals.user_id = users.id
   INNER JOIN deals
     ON deal_externals.deal_id = deals.id WHERE
   deal_externals.external_status = "C" AND
   deals.deal_status_id = 6 AND
   deals.end_date >= "' . $dias_atras . '" AND
   deals.id =' . $deal['Deal']['similar_deal_id'] . ' AND
   NOT EXISTS
     (SELECT
        id
      FROM
        deal_externals e
      WHERE
        e.deal_id = deals.id AND
        users.id = e.user_id AND
        e.external_status IN ("P", "A"))';
                $users = $this->User->query($myQuery);
                CakeLog::write('debug', 'size users : ' . count($users));
                if (!empty($users)) {
                	$params_deal_url='?utm_source=ClubCupon&utm_medium=emailnc&utm_campaign=6902&popup=no';
                    foreach ($users as $user) {
                    	$no_buyer=$user['users'];
                    	$deal_external=$user['deals'];
                    	//var_dump($users);


                    	$emailFindReplace = array(
                            '##USERNAME##' => $no_buyer['username'],
                            //'##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
                            '##DEAL_TITLE##' => $deal_external['name'],
                            '##DEAL_URL##' => $deal['Deal']['bitly_short_url_subdomain'].$params_deal_url,
                            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . 'img/'
                        );
                        $this->Email->reset();
                        $this->Email->from = Configure::read('EmailTemplate.from_email');
                        $this->Email->to = $no_buyer['email'];
                        $this->Email->subject = 'Club Cupon te da otra oportunidad';
                        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
                        $this->Email->content = strtr($template['email_content'], $emailFindReplace);
                        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';

                        $this->Email->send($this->Email->content);

                    }
                }
            }
        }
    }

    function return_credit() {
        App::import('Model', 'Setting');
        AppModel::setDefaultDbConnection('master');

        $setting_model_obj = new Setting ();
        $settings = $setting_model_obj->getKeyValuePairs();
        Configure::write($settings);

        App::import('Model', 'Deal');
        App::import('Model', 'DealUser');
        App::import('Model', 'User');

        $this->Deal = &new Deal();
        $this->User = &new User();
        $this->DealUser = &new DealUser();
        $this->Deal->Behaviors->detach('Excludable');

        //$hours = Configure::read('deal.expiry_hours');
        $hours = Configure::read('deal.extended_hours_to_redeem_now');
        $expiry_date = date('Y-m-d H:i:s', strtotime("-$hours hours"));
        $this->Deal->recursive = -1;
        $deals = $this->Deal->find('all', array('conditions' => array('coupon_expiry_date < ' => $expiry_date, 'is_now' => 1)));

        foreach ($deals as $deal) {
            Debugger::log('return_credit - Deal - ' . $deal['Deal']['id'], LOG_DEBUG);
            $this->DealUser->recursive = - 1;
            $dealUsers = $this->DealUser->find('all', array('conditions' => array('deal_id' => $deal['Deal']['id'], 'is_used' => 0, 'is_returned' => 0)));
            foreach ($dealUsers as $cupon) {
                Debugger::log('return_credit - DealUsers - ' . $cupon['DealUser']['id'], LOG_DEBUG);
                $this->User->id = $cupon['DealUser']['user_id'];
                Debugger::log('return_credit - available_balance_amount - ' .$this->User->field('available_balance_amount') + ($cupon['DealUser']['discount_amount'] * $cupon['DealUser']['quantity']), LOG_DEBUG);
                $this->User->saveField('available_balance_amount', $this->User->field('available_balance_amount') + ($cupon['DealUser']['discount_amount'] * $cupon['DealUser']['quantity']));
                $this->DealUser->id = $cupon['DealUser']['id'];
                $this->DealUser->saveField('is_returned', 1);
            }
        }
    }

	function del_old_branch_deals(){
            $this->delete_branchs_deals();
	}

        function delete_branchs_deals() {
            Debugger::log('Se llamo a '.__METHOD__. '(No se deben borrar branches_deals)', LOG_DEBUG);
        }

	function reindex_all(){
		App::import ('Model', 'Setting');
		AppModel::setDefaultDbConnection ('master');
		$setting_model_obj = new Setting ();
        $settings = $setting_model_obj->getKeyValuePairs ();
        Configure::write($settings);

		$days = Configure::read('deal.reindex_days');
		$created = date('Y-m-d H:i:s', strtotime("-$days days"));

		App::import ('Model', 'SearchIndex');
		$this->index = new SearchIndex();
		$models = $this->index->find('all',array(
			'conditions'=>array('created >'=>$created),
		));

		if(!count($models)){
			return;
		}

		foreach($models as $model){
			$info = new $model['SearchIndex']['model'];
			$info->create();
			$info->id = $model['SearchIndex']['association_key'];
			$data = $info->indexData();
			if(!$data && method_exists($info,'getIndexData')){
				$data = $info->getIndexData();
			}
			if($data){
				$res = $this->index->save(
					array(
						'SearchIndex' => array(
							'id' => $model['SearchIndex']['id'],
							'model' => $info->name,
							'association_key' => $info->id,
							'data' => $data,
						)
					)
				);
			}
		}
		die();
	}




        function clone_now_deals($yyyymmdd= false)
        {
            App::import("Model", "now.NowDeal");
            App::import("Model", "now.NowScheduledDeals");
            App::import('Component', 'now.Schedule');

            $nowdeal = &new NowDeal();
            $ns_deals = new NowScheduledDeals();

            if(!$yyyymmdd) {
                $yyyymmdd = date("Y-m-d");
            }

            list($yyyymmdd) = explode(" ", $yyyymmdd);
            $fecha  = date("Y-m-d", strtotime($yyyymmdd));

            list($year, $month, $day ) = explode("-",$yyyymmdd);

            if(!checkdate($month, $day, $year) || ($fecha !=$yyyymmdd)){
                echo "ON ".__METHOD__. "invalid date for ".$yyyymmdd."\r\n";
                Debugger::log("ON ".__METHOD__. "invalid date for ".$yyyymmdd, LOG_DEBUG);
                return false;
            }

            $deals = $ns_deals->getScheduledNowDeals($fecha);
            Debugger::log("BEGIN CRON CLONE_NOW_DEALS***************** \r\n");
            Debugger::log("deals ".print_r($deals,1));
            foreach($deals as $scheduled_deal){

                Debugger::log("scheduled_deal ".print_r($scheduled_deal,1));
                $schedule = &new ScheduleComponent();

                $schedule->cloneNowDeal($scheduled_deal['NowScheduledDeals']['deal_id'], $fecha);
            }
            Debugger::log("END CRON CLONE_NOW_DEALS***************** \r\n");
            $nowdeal->reindexAll(0);



        }

        function checkDateTime($data) {
            return (date('Y-m-d H:i:s', strtotime($data)) == $data);
        }

}