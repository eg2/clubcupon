<?php
App::import('Component', 'Base');
App::import('Component', 'api.ApiBac');
App::import('Component', 'api.ApiLogger');
App::import('Model', 'GiftPin');
class GiftPinServiceComponent extends BaseComponent {

  const BAC_STATUS_AVAILABLE = 1; // El pin se encuntra listo para ser usado
  const BAC_STATUS_USED = 2; // El pin ha sido utilizado
  const BAC_STATUS_CANCELED = 3; // El pin se encuentra inhabilitado
  const BAC_STATUS_EXPIRED = 4; // El pin pertenece a un paquete que ha caducado
  const BAC_STATUS_DISABLED = 5; // El pin ha sido deshabilitado
  const BAC_STATUS_CANCELED_MANUALLY = 6; // El pin se anula manualmente

  private $bac_id_portal = null;

  public $name = 'GiftPinService';
  public $components = array(
    'api.ApiBac',
    'Base',
  );

  public function __construct() {
    $this->GiftPin = &new GiftPin();
    $this->bac_id_portal = Configure::read('BAC.id_portal');
    if (!isset($this->ApiBac)) {
      $this->ApiBac = &new ApiBacComponent();
    }
    parent::__construct();
  }

  public function isUsable($code) {
    if(strlen($code)> 10) {
      return false;
    }

    $bacPin = $this->obtenerBacPin($code);
    if($this->isAvaliableForBac($bacPin)
      && !$this->isBlocked($code)) {
      return $bacPin;
    } else {
      return false;
    }
  }

  private function isAvaliableForBac($bacPin) {
    if(!empty($bacPin)) {
      return $bacPin['idEstadoPin'] == self::BAC_STATUS_AVAILABLE;
    }
    return false;
  }

  private function isBlocked($code) {
    $giftPin = $this->GiftPin->findByCode($code);
    if(!empty($giftPin)) {
      return $this->GiftPin->isBlocked($giftPin);
    }
    return false;
  }

  public function saveBacPinInGiftPinByCode($code) {
    $giftPin = $this->GiftPin->findByCode($code);
    if(empty($giftPin)) {
      $bacPin = $this->obtenerBacPin($code);
      $giftPin = array('GiftPin' => array());
      $giftPin['GiftPin']['code'] = $code;
      $giftPin['GiftPin']['discount'] = $bacPin['monto'];
      $giftPin['GiftPin']['status'] = $bacPin['idEstadoPin'];
      $giftPin = $this->GiftPin->save($giftPin);
      $giftPin['GiftPin']['id'] = $this->GiftPin->id;
      Debugger::log(__METHOD__.__LINE__. print_r($giftPin,1));
    }
    return $giftPin;
  }

  public function block($code) {
    $giftPin = $this->GiftPin->findByCode($code);
    if(empty($giftPin) && (!empty($code))) {
      $giftPin = $this->saveBacPinInGiftPinByCode($code);
    }
  //  if($this->GiftPin->isBlocked($giftPin)) {
  //   throw new Exception("Gift Pin already blocked.");
  //  }
    if ($giftPin) {
      $giftPin = $this->GiftPin->block($giftPin);
    }
    return $giftPin;
  }

  public function unBlock($code) {
    if (!empty($code)){
      $giftPin = $this->GiftPin->findByCode($code);
    }
 
    try{
      return $this->GiftPin->unBlock($giftPin);
    } catch (Exception $e) { 
      $this->ApiLogger->error('Error al Desbloquear el pincode', 
              array('code' =>$code));
      return null;
    }
  }

  public function unBlockAndBurnGiftPin($code, $userId) {
      $this->unBlock($code);
      return $this->burnGiftPin($code, $userId);
  }

  public function burnGiftPin($code, $userId) {
      $giftPin = $this->saveBacPinInGiftPinByCode($code);
      $consumirPinParams = array(
        'idUsuarioPortal' => $userId,
        'idPortal' => $this->bac_id_portal,
        'nroPin' => $code,
        'retailer' => 'Compra ClubCupon',
      );
      try {
        $bacResponse = $this->ApiBac->consumirPin($consumirPinParams);
        if (is_string($bacResponse)) {
            Debugger::log(__CLASS__ . '::' . __METHOD__ .__LINE__.
        ' throws ' . $bacResponse, LOG_DEBUG);
     //     throw new Exception("$bacResponse");
        }
      } catch (Exception $e) {
          Debugger::log(__CLASS__ . '::' . __METHOD__ .__LINE__.
        ' throws ' . "No se pudo quemar el Pin en BAC ".$code, LOG_DEBUG);
        //throw new Exception ("No se pudo quemar el Pin en BAC $code");
      }
      $giftPin['GiftPin']['status'] = self::BAC_STATUS_USED;
      Debugger::log("QUEMANDO PIN START");
      Debugger::log(print_r($giftPin,1));
      Debugger::log("QUEMANDO PIN END");
      $this->GiftPin->save($giftPin);
      return $giftPin;
  }

  /**
   *  [codigo] => epqfxwnoz8
   *  [descripcion] => Código de descuento
   *  [idEstadoPaquete] => 1
   *  [idEstadoPin] => 1
   *  [idMoneda] => 1
   *  [idPaquete] => 10125
   *  [idPin] => 14091758
   *  [idProducto] => 102334
   *  [medida] => 0
   *  [monto] => 25
   */
   private function obtenerBacPin($code){
    $params = array(
        'idPortal' =>  $this->bac_id_portal,
        'codigoPin' => $code,
    );
    $bacPin = $this->ApiBac->obtenerPin($params);
    if (is_string($bacPin)) {
      throw new Exception("$bacPin");
    }
    return get_object_vars($bacPin);
  }

}
