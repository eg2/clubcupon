<?php

App::import('Core', 'Component');
App::import('Model', 'City');
App::import('Model', 'Deal');

class SecondaryDealsComponent extends Object{

    public $name = 'SecondaryDeals';

    
    var $components = array('Session');
    
    function __construct() {
        $this->Deal = new Deal();
        $this->City = new City();
        parent::__construct();
    }

    
    function secondaryDeals ($excludedCitySlugs) {
        
        $allowedCitiesAndGroups = $this->City->getAllowedCitiesAndGroupsForSecondaryDeals();
        
        $choosableCityAndGroupsSlugs = $this->filterCitySlugs($allowedCitiesAndGroups, $excludedCitySlugs);
        
        $selectedCityOrGroup = $this->selectRandomCityOrGroup($choosableCityAndGroupsSlugs);
        $selectedCityOrGroupSlug = reset($selectedCityOrGroup);
        $selectedCityOrGroupId = key($selectedCityOrGroup);
        
        $this->Session->write( 'lastCitySlug', $selectedCityOrGroupSlug );
        
        //recupero los deals
        return $this->Deal->findAllOpenOrTippedDealsByCityOrGroupId($selectedCityOrGroupId);
        
  }
    
    private function filterCitySlugs($allowedCitiesAndGroups, $excludedCitySlugs) {
        
        $filteredAllowedCitiesAndGroups = array_diff($allowedCitiesAndGroups, $excludedCitySlugs);
        
        return $filteredAllowedCitiesAndGroups;
    }

    private function selectRandomCityOrGroup($choosableCityAndGroupsSlugs) {
        $randomCityOrGroupIndex =  array_rand($choosableCityAndGroupsSlugs, 1);
        return array($randomCityOrGroupIndex => $choosableCityAndGroupsSlugs[$randomCityOrGroupIndex]);
    }



}

?>