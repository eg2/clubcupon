<?php

class ResendActivationMailsComponent extends Object {

    var $controller;

    function doIt() {
        App::import('Core', 'Router');
        App::import('Core', 'Controller');  // esta linea hace falta, de lo contrario no se pueden importar controllers
        App::import('Model', 'User');
        App::import('Controller', 'Users');
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');

        // construir "$this" como haga falta
        $this->UsersController = & new UsersController ();
        $this->UsersController->User = & new User ();
        $this->UsersController->Email = & new EmailComponent ();
        $this->UsersController->EmailTemplate = & new EmailTemplate ();

        // se busca una lista de usuarios sin activar
        $user_id__email = $this->UsersController->User->find('list', array(
                    'conditions' => array('User.is_email_confirmed' => false),
                    'fields' => array('User.email')
                ));

        // por cada uno...
        foreach ($user_id__email as $user_id => $email) {
            // intentamos mandar el mail de activacion
            $this->log('ResendActivationMails - procesando usuario con id = ' . $user_id . '...', LOG_DEBUG);

            if ($this->UsersController->_sendActivationMail($email, $user_id, $this->UsersController->User->getActivateHash($user_id))) {
                $this->log('ResendActivationMails - email despachado con exito a "' . $email . '".', LOG_DEBUG);
            } else {
                $this->log('ResendActivationMails - error al despachar email a "' . $email . '".', LOG_DEBUG);
            }
        } // foreach
    }

// doIt
}

// class ResendActivationMailsComponent
?>
