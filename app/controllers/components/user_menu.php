<?php

App::import('Model', 'User');
App::import('Model', 'Company');

class UserMenuComponent extends Object {

    public $name = 'UserMenu';

    function __construct() {
        $this->User = new User();
        $this->Company = new Company();
        parent::__construct();
    }

    function returnMenuOptionsForUser($userId) {
        $user = $this->User->getUserData($userId);
        $company = $this->Company->findByUserId($userId);
        $userBalanceStringClass = $user['User']['available_balance_amount'] > 0 ? 'str' : 'hidden';
        $companyIsNowDealsLink = array(
            'type' => 'link',
            'label' => 'Mis Ofertas',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_deals',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            ),
            'alias' => 'index'
        );
        $companyIsNowBranchesLink = array(
            'type' => 'link',
            'label' => 'Mis Sucursales',
            'plugin' => 'now',
            'controller' => 'now_branches',
            'action' => 'index',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $companyIsNowCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_cupons',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $companyIsNowProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Empresa',
            'plugin' => 'now',
            'controller' => 'now_registers',
            'action' => 'my_company',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $companyIsRegularProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Empresa',
            'plugin' => '',
            'controller' => 'users',
            'action' => 'change_password',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $companyIsRegularDealsLink = array(
            'type' => 'link',
            'label' => 'Mis Ofertas',
            'plugin' => '',
            'controller' => 'deals',
            'action' => 'company',
            'admin' => false,
            'parameters' => $company['Company']['slug'],
            'format' => array(
                'title' => '',
                'class' => '',
            ),
            'alias' => 'index'
        );
        $companyIsRegularCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_cupons',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $companyCampaign = array(
            'type' => 'link',
            'label' => 'Campaña',
            'plugin' => '',
            'controller' => 'dashboards',
            'action' => 'company_campaign',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            ),
        );
        $companyLiquidations = array(
            'type' => 'link',
            'label' => 'Liquidaciones',
            'plugin' => 'now',
            'controller' => 'now_liquidations',
            'action' => 'liquidations',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            ),
        );
        $userSubscriptionsLink = array(
            'type' => 'link',
            'label' => 'Suscripciones',
            'plugin' => '',
            'controller' => 'subscriptions',
            'action' => 'manage_subscriptions',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $userProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Perfil',
            'plugin' => '',
            'controller' => 'user_profiles',
            'action' => 'edit',
            'admin' => false,
            'parameters' => $user['User']['id'],
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $userChangePasswordLink = array(
            'type' => 'link',
            'label' => 'Cambio de contraseña',
            'plugin' => '',
            'controller' => 'users',
            'action' => 'change_password',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $userCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => '',
            'controller' => 'deal_users',
            'action' => 'index/type:available',
            'admin' => false,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $userAdminLink = array(
            'type' => 'link',
            'label' => 'Admin',
            'plugin' => '',
            'controller' => 'deals',
            'action' => 'index',
            'admin' => true,
            'parameters' => '',
            'format' => array(
                'title' => '',
                'class' => '',
            )
        );
        $userBalanceString = array(
            'type' => 'string',
            'string' => 'Ten&eacute;s $' . $user['User']['available_balance_amount'] . ' en tu cuenta.',
            'format' => array(
                'title' => '',
                'class' => $userBalanceStringClass,
            )
        );
        $userHelpLink = array(
            'type' => 'externalLink',
            'label' => 'Ayuda',
            'url' => 'http://soporte.clubcupon.com.ar/',
            'target' => '_blank',
            'format' => array(
                'title' => 'Centro de atenci&oacute;n al cliente',
                'class' => '',
            )
        );
        $companyHelpLink = array(
            'type' => 'externalLink',
            'label' => 'Ayuda',
            'url' => Configure::read('ayuda.comercios'),
            'target' => '_blank',
            'format' => array(
                'title' => 'Centro de atenci&oacute;n al comercio',
                'class' => '',
            )
        );
        $userIsCompanyLinks = array(
            $companyIsNowProfileLink,
            $companyIsNowDealsLink,
            $companyIsNowCuponsLink,
            $companyLiquidations,
            $companyHelpLink,
        );
        if ($this->User->isCompanyWithNow($user)) {
           // $userIsCompanyLinks[] = $companyIsNowBranchesLink;
        }
        $userIsRegularLinks = array(
            $userBalanceString,
            $userProfileLink,
            $userCuponsLink,
            $userSubscriptionsLink,
            $userHelpLink,
        );
        $userIsAdminLinks = array(
            $userBalanceString,
            $userProfileLink,
            $userCuponsLink,
            $userSubscriptionsLink,
            $userAdminLink,
            $userHelpLink,
        );
        $userIsAgencyLinks = array(
            $userSubscriptionsLink,
            $userAdminLink,
            $companyHelpLink,
        );
        $userIsSellerLinks = array(
            $userSubscriptionsLink,
            $companyHelpLink,
        );
        if ($this->User->isCompany($user)) {
            return $userIsCompanyLinks;
        }
        if ($this->User->isRegularUser($user)) {
            return $userIsRegularLinks;
        }
        if ($this->User->isAgencyUser($user) || $this->User->isPartnerUser($user)) {
            return $userIsAgencyLinks;
        }
        if ($this->User->isSellerUser($user)) {
            return $userIsSellerLinks;
        }
        if ($this->User->isAdminUser($user) || $this->User->isSuperAdminUser($user)) {
            return $userIsAdminLinks;
        }
    }

}
