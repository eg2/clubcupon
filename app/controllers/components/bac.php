
<?php

App::import('Component', 'api.ApiBac');

class BacComponent extends BaseComponent {

    private $sopa;
    private $ApiBac;
    private $logger;

    const BAC_TOURISM_PRODUCTID_IVA_FULL = 103033;
    const BAC_TOURISM_PRODUCTID_IVA_HALF = 103032;
    const BAC_TOURISM_PRODUCTID_EXEMPT = 103029;
    const BAC_TOURISM_PRODUCTID_RETAIL = 103034;

    private $CapitalFederal = null;
    private $Argentina = null;
    private $moneda_pesos = null;

    public function __construct() {
        if (!isset($this->ApiBac)) {
            $this->ApiBac = & new ApiBacComponent();
        }
        if (!isset($this->logger)) {
            $this->logger = ApiLogger::getInstance();
        }
        $this->moneda_pesos = Configure::read('BAC.pesos');
        $this->Argentina = Configure::read('BAC.Argentina');
        $this->CapitalFederal = Configure::read('BAC.CapitalFederal');
    }

    /**
     * iniciarPagoSeguroApi.
     *
     * @param Array con los parametros especificados por BAC $params
     * @return idPago, y status
     */
    public function iniciarPagoSeguroApi($params) {
        try {
            $idPago = $this->ApiBac->initializePaymente($params);
            $token['status'] = TRUE;
            $token['idPago'] = $idPago;
        } catch (Exception $e) {
            $token['status'] = FALSE;
            $this->logger->notice("error al iniciar el pago en Bac", array(
                'exception' => $e->getMessage(),
                'params' => $params
            ));
            // throw new Exception("No se pudo iniciar el pago en Bac");
        }
        return $token;
    }

    /**
     * iniciarPagoSeguro
     *
     * @param arreglo de parametros $params
     * @return Arreglo asociativo, ret = idPago y status= estado de la transacción
     */
    function iniciarPagoSeguro($params) {
        $params = (array) $params;
        try {
            $this->sopa = new SoapClient(Configure::read('BAC.wsdl.consultarPago'));
            $ret = $this->sopa->iniciarPagoSeguro($params);
            return array(
                'status' => TRUE,
                'ret' => $ret
            );
        } catch (Exception $e) {
            echo $e->getMessage();
            return array(
                'status' => FALSE,
                'ret' => $e
            );
        }
    }

    public function dump() {
        var_dump($this->sopa);
    }

    /**
     * obtenerPin
     *
     * @param alfanumérico $code
     * @return alfanumérico
     */
    public function obtenerPin($code) {
        try {
            $this->sopa = new SoapClient(Configure::read('BAC.wsdl.obtenerPin'));
            $params = new stdClass();
            $params->idPortal = Configure::read('BAC.id_portal');
            $params->codigoPin = $code;
            return array(
                'status' => TRUE,
                'ret' => $this->sopa->obtenerPin($parameter)
            );
        } catch (Exception $e) {
            return array(
                'status' => FALSE,
                'ret' => $e->getMessage()
            );
        }
    }

    /**
     * Obtiene el Portal Id para el Sitio
     * Actualmente se utiliza el mismo portal
     * Id para todas los tipos de ofertas.
     * @param unknown $isTourism (optional)
     * @return unknown
     */
    public function getPortalId($isTourism = 0) {
        $loger = ApiLogger::getInstance();
        $date = strtotime($dealStartDate);
        $portalId = Configure::read('BAC.id_portal');
        $loger->notice('Obteniendo el Id de Portal para la Compra', array(
            'isDealTourism' => $isTourism,
            'dealStartDate' => $dealStartDate,
            'idPortal' => $portalId
        ));
        return $portalId;
    }

    /**
     * Obtiene la descripción a enviar a BAC, con
     * el límite de caracteres necesarios
     * @param unknown $deal
     * @return unknown
     */
    protected function getDealDescription($deal) {
        return substr($deal['Deal']['name'], 0, 94);
    }

    /**
     * Obtiene el Id de producto para el resto de las Ofertas
     * se utiliza para acreditar en BAC
     * @param ProductId $isDealTourism (Ofertas de Turismo)
     * @param ProductId $isEndUser     (Se Factura al usuario Final)
     * @return ProductId
     */
    public function getProductId($isDealTourism, $isEndUser) {
        $loger = ApiLogger::getInstance();
        $loger->notice('Parametros recibidos en getProductId', array(
            'isDealTourism' => $isDealTourism,
            'isEndUser' => $isEndUser,
        ));
        if ($isEndUser) {
            $idProducto = Configure::read('BAC.id_producto_final');
        } else {
            $idProducto = Configure::read('BAC.id_producto');
        }
        $loger->notice('Obteniendo el Id de Producto para crédito de la Compra', array(
            'isDealTourism' => $isDealTourism,
            'dealStartDate' => $dealStartDate,
            'isEndUser' => $isEndUser,
            'idProducto' => $idProducto
        ));
        return $idProducto;
    }

    /**
     * Obtiene el Id de producto para el resto de las Ofertas
     * y se utiliza para debitar en BAC (descuentos al usuario, pago combinado)
     * @param Boolean $isDealTourism
     * @return ProductId
     */
    public function getProductDebitId($isDealTourism) {
        $loger = ApiLogger::getInstance();
        $loger->notice('Parametros recibidos getProductDebitId', array(
            'isDealTourism' => $isDealTourism,
        ));
        $idProductoDebito = Configure::read('BAC.id_producto_debito');
        $loger->notice('Obteniendo el Id de Producto para débito de la Compra', array(
            'isDealTourism' => $isDealTourism,
            'idProductoDebito' => $idProductoDebito
        ));
        return $idProductoDebito;
    }

    /**
     * Obtiene el Monto al detalle en caso de compras de
     * ofertas Turismos y Mayoristas
     * @param Deal $deal
     * @return Float
     */
    public function getRetailMount($deal) {
        $amountWholesaler = $deal['Deal']['amount_full_iva'] + $deal['Deal']['amount_half_iva'] + $deal['Deal']['amount_exempt'];
        $amountRetail = $deal['Deal']['discounted_price'] - $amountWholesaler;
        return $amountRetail;
    }

    /**
     * Obtiene el arreglo de productos necesarios
     * a enviar a Bac para iniciar el  pago Seguro.
     * @param Deal $deal
     * @param numeric $quantity
     * @param numeric $mount
     * @param numeric $moneda
     * @param boolean $isDebito   (optional)
     * @param numeric $idProducto
     * @return array
     */
    public function getProductsForBac($deal, $quantity, $mount, $moneda, $isDebito, $idProducto) {
        $prod = new stdClass();
        if ($isDebito) {
            $prod->idProducto = $this->getProductDebitId($deal['Deal']['is_tourism']);
            $monto = (float) $mount;
            $prod->monto = (-1) * (float) abs($monto);
            $prod->monto = (string) $prod->monto;
        } else {
            $prod->idProducto = $idProducto;
            $prod->monto = $mount;
        }
        $prod->cantidad = (int) $quantity;
        $prod->moneda = $moneda;
        return $prod;
    }
    public function getAmountRetail($deal) {
        $price         = $deal['Deal']['discounted_price'];
        $amount_exempt = $deal['Deal']['amount_exempt'];
        $amount_full_iva = $deal['Deal']['amount_full_iva'];
        $amount_half_iva = $deal['Deal']['amount_half_iva'];
        return ($price - ($amount_exempt+ $amount_full_iva + $amount_half_iva));
    }
    /**
     * Obtiene el Arreglo de Productos en el caso que la Oferta
     * sea de Turismo y Mayorista.
     * @param DealExternal $dealExternal
     * @param Deal $deal
     * @param boolean $isCombined
     * @return Array
     */
    public function getProductToTourism($dealExternal, $deal, $isCombined) {
        $isdebito = false;
        $quantity = $dealExternal['DealExternal']['quantity'];
        $moneda = $this->moneda_pesos;
        $amount_exempt = $deal['Deal']['amount_exempt'];
        $amount_full_iva = $deal['Deal']['amount_full_iva'];
        $amount_half_iva = $deal['Deal']['amount_half_iva'];
        $amount_retail = $this->getAmountRetail($deal);
        $items = array();
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_exempt, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_EXEMPT);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_full_iva, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_IVA_FULL);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_half_iva, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_IVA_HALF);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_retail, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_RETAIL);
        if ($isCombined) {
            $debito = $dealExternal['DealExternal']['internal_amount'];
            $isdebito = true;
            $idProduct = $this->getProductDebitId($deal['Deal']['is_tourism']);
            $items[] = $this->getProductsForBac($deal, $quantity = 1, $debito, $moneda, $isdebito, $idProduct);
        }
        return $items;
    }

    /**
     * Obtiene el arreglo de Productos para el Caso que un
     * usuario realice una compra combinada.
     * @param dealExternal $dealExternal
     * @param deal $deal
     * @return Array
     */
    public function getProductToPaymentCombined($dealExternal, $deal) {
        $quantity = $dealExternal['DealExternal']['quantity'];
        $mount = $deal['Deal']['discounted_price'];
        $moneda = $this->moneda_pesos;
        $debito = $dealExternal['DealExternal']['internal_amount'];
        $isTourism = $deal['Deal']['is_tourism'];
        $isEndUser = $deal['Deal']['is_end_user'];
        $isdebito = false;
        $idProduct = $this->getProductId($isTourism, $isEndUser);
        $prod = $this->getProductsForBac($deal, $quantity, $mount, $moneda, $isdebito, $idProduct);
        $isdebito = true;
        $idProduct = $this->getProductDebitId($deal['Deal']['is_tourism']);
        $prodDebito = $this->getProductsForBac($deal, $quantity = 1, $debito, $moneda, $isdebito, $idProduct);
        $productos = array(
            $prod,
            $prodDebito
        );
        return $productos;
    }

    /**
     * Obtiene el arreglo de Productos para el caso
     * de compra Directa, sin utilizar Cuenta Club cupon
     * @param dealExternal $dealExternal
     * @param deal $deal
     * @return Array
     */
    private function getProductToPaymentDirect($dealExternal, $deal) {
        $quantity = $dealExternal['DealExternal']['quantity'];
        $mount = $deal['Deal']['discounted_price'];
        $moneda = $this->moneda_pesos;
        $isTourism = $deal['Deal']['is_tourism'];
        $isEndUser = $deal['Deal']['is_end_user'];
        $isdebito = false;
        $idProduct = $this->getProductId($isTourism, $isEndUser);
        $prod = $this->getProductsForBac($deal, $quantity, $mount, $moneda, $isdebito, $idProduct);
        return array(
            $prod
        );
    }

    /**
     * Obtiene el Arreglo de Productos que sea acorde al tipo de compra
     *
     * @param dealExternal $dealExternal
     * @param deal $deal
     * @param isTourism $isTourism
     * @param isWholesaler $isWholesaler
     * @param isCombined $isCombined
     * @return Array
     */
    public function getParamsToProducts($dealExternal, $deal, $isTourism, $isWholesaler, $isCombined) {
        if ($isTourism && $isWholesaler) {
            $productos = $this->getProductToTourism($dealExternal, $deal, $isCombined);
        } elseif ($isCombined) {
            $productos = $this->getProductToPaymentCombined($dealExternal, $deal);
        } else {
            $productos = $this->getProductToPaymentDirect($dealExternal, $deal);
        }
        return $productos;
    }

    /**
     * Obtiene los parametros necesarios para iniciar la Compra segura
     * de un usuario
     * @param dealExternal $dealExternal
     * @param deal $deal
     * @param numeric $installments
     * @param numeric $gatewayId
     * @param boolean $pagoDirecto
     * @return Array
     */
    public function getParamsToBac($dealExternal, $deal, $installments, $gatewayId, $pagoDirecto) {
        $params = new stdClass();
        $instalments = $installments;
        $isTourism = $deal['Deal']['is_tourism'];
        $isWholesaler = $deal['Deal']['is_wholesaler'];
        $products = $this->getParamsToProducts($dealExternal, $deal, $isTourism, $isWholesaler, (!$pagoDirecto));
        $params->idUsuarioPortal = $dealExternal['DealExternal']['user_id'];
        $params->idPortal = $this->getPortalId($deal['Deal']['is_tourism']);
        $params->productos = $products;
        $params->idPagoPortal = $dealExternal['DealExternal']['id'];
        $params->idMedioPago = $dealExternal['DealExternal']['payment_type_id'];
        $params->idGateway = $gatewayId;
        $params->cantidadCuotas = $instalments;
        $params->descripcion = $this->getDealDescription($deal);
        $params->ipUsuario = RequestHandlerComponent::getClientIP();
        $loger = ApiLogger::getInstance();
        $loger->notice('getParamsToBac, Ip Usuario: ' . $params->ipUsuario);
        return $params;
    }

    /**
     * Obtiene el codigo de seguridad para realizar la llamada a
     * CrearPagoSeguro (POST form)
     * @param unknown $isDealTourism
     * @return unknown
     */
    public function getSecureCode($isDealTourism) {
        $loger = ApiLogger::getInstance();
        $secureCode = Configure::read('cc.bac.secure_code');
        $loger->notice('Obteniendo Código de Seguridad para la Compra', array(
            'isDealTourism' => $isDealTourism,
            'secureCode' => $secureCode
        ));
        return $secureCode;
    }

}