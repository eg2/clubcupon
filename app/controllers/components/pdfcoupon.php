<?php
/*
 * Archivo: app/controlers/components/pdfcoupon.php
 *
 * Usar como:  var $components = array ('Pdfcoupon');  (en Model)
 * o:          App::import ('Component', 'Pdfcoupon');
 *
 */

class PdfcouponComponent extends Object
  {
    function defaultSpec ()
      {
        return array
          (
            'width'  => 645,
            'height' => 955,
            //
            'padding_width' => 30,
            //
            'size_small'  => 34,
            'size_normal' => 42,
            'size_big'    => 48,
            //
            'header'         => array ('x' =>  30, 'y' =>  30, 'w' => 585, 'h' => 116),
            'code'           => array ('x' => 353, 'y' =>  55, 'w' => 260, 'h' =>  55),//'code'=> array ('x' => 353, 'y' =>  55, 'w' => 260, 'h' =>  55),//            'posnet'         => array ('x' => 353, 'y' =>  85, 'w' => 260, 'h' =>  25),
            'title'          => array ('x' =>  30, 'y' => 146, 'w' => 585, 'h' =>  88),
            'user_banner'    => array ('x' =>  30, 'y' => 253, 'w' => 184, 'h' =>  16),
            'user_data'      => array ('x' =>  30, 'y' => 269, 'w' => 356, 'h' =>  86),
            'company_banner' => array ('x' => 403, 'y' => 253, 'w' => 115, 'h' =>  16),
            'company_data'   => array ('x' => 403, 'y' => 269, 'w' => 212, 'h' =>  86),
            'expiry_banner'  => array ('x' =>  30, 'y' => 324, 'w' => 250, 'h' =>  16),
            'expiry_data'    => array ('x' =>  30, 'y' => 340, 'w' => 356, 'h' =>  29),
            'barcode'        => array ('x' => 403, 'y' => 390, 'w' => 212, 'h' =>  93),
            'details_banner' => array ('x' =>  30, 'y' => 390, 'w' => 129, 'h' =>  16),
            'details_data'   => array ('x' =>  30, 'y' => 406, 'w' => 356, 'h' => 230),
            'howto'          => array ('x' =>  30, 'y' => 633, 'w' => 585, 'h' => 287),
            'howto_link'     => array ('x' => 367, 'y' => 817, 'w' => 184, 'h' =>  15),
            'contact'        => array ('x' =>  30, 'y' => 920, 'w' => 585, 'h' => 144),
          );
      }

    function scaleSpec ($w, $h, & $spec = null)
      {
        if ($spec == null) $spec = $this->defaultSpec ();

        // Guardar el ancho original (va a ser la base para el escalado).
        $w_o = $spec ['width'];

        // Escalar alto y ancho dentro de los l�mites.
        $this->_scaleInBounds ($spec ['width'], $spec ['height'], $w, $h);
        $spec ['width' ] = $w;
        $spec ['height'] = $h;

        // Escalar el resto de los par�metros.
        $spec ['padding_width'      ] = $this->_scale ($spec ['padding_width'      ], $w_o, $w);
        $spec ['size_small'         ] = $this->_scale ($spec ['size_small'         ], $w_o, $w);
        $spec ['size_normal'        ] = $this->_scale ($spec ['size_normal'        ], $w_o, $w);
        $spec ['size_big'           ] = $this->_scale ($spec ['size_big'           ], $w_o, $w);
        $spec ['header'        ]['x'] = $this->_scale ($spec ['header'        ]['x'], $w_o, $w);
        $spec ['header'        ]['y'] = $this->_scale ($spec ['header'        ]['y'], $w_o, $w);
        $spec ['header'        ]['w'] = $this->_scale ($spec ['header'        ]['w'], $w_o, $w);
        $spec ['header'        ]['h'] = $this->_scale ($spec ['header'        ]['h'], $w_o, $w);
        $spec ['code'          ]['x'] = $this->_scale ($spec ['code'          ]['x'], $w_o, $w);
        $spec ['code'          ]['y'] = $this->_scale ($spec ['code'          ]['y'], $w_o, $w);
        $spec ['code'          ]['w'] = $this->_scale ($spec ['code'          ]['w'], $w_o, $w);
        $spec ['code'          ]['h'] = $this->_scale ($spec ['code'          ]['h'], $w_o, $w);
        $spec ['title'         ]['x'] = $this->_scale ($spec ['title'         ]['x'], $w_o, $w);
        $spec ['title'         ]['y'] = $this->_scale ($spec ['title'         ]['y'], $w_o, $w);
        $spec ['title'         ]['w'] = $this->_scale ($spec ['title'         ]['w'], $w_o, $w);
        $spec ['title'         ]['h'] = $this->_scale ($spec ['title'         ]['h'], $w_o, $w);
        $spec ['user_banner'   ]['x'] = $this->_scale ($spec ['user_banner'   ]['x'], $w_o, $w);
        $spec ['user_banner'   ]['y'] = $this->_scale ($spec ['user_banner'   ]['y'], $w_o, $w);
        $spec ['user_banner'   ]['w'] = $this->_scale ($spec ['user_banner'   ]['w'], $w_o, $w);
        $spec ['user_banner'   ]['h'] = $this->_scale ($spec ['user_banner'   ]['h'], $w_o, $w);
        $spec ['user_data'     ]['x'] = $this->_scale ($spec ['user_data'     ]['x'], $w_o, $w);
        $spec ['user_data'     ]['y'] = $this->_scale ($spec ['user_data'     ]['y'], $w_o, $w);
        $spec ['user_data'     ]['w'] = $this->_scale ($spec ['user_data'     ]['w'], $w_o, $w);
        $spec ['user_data'     ]['h'] = $this->_scale ($spec ['user_data'     ]['h'], $w_o, $w);
        $spec ['company_banner']['x'] = $this->_scale ($spec ['company_banner']['x'], $w_o, $w);
        $spec ['company_banner']['y'] = $this->_scale ($spec ['company_banner']['y'], $w_o, $w);
        $spec ['company_banner']['w'] = $this->_scale ($spec ['company_banner']['w'], $w_o, $w);
        $spec ['company_banner']['h'] = $this->_scale ($spec ['company_banner']['h'], $w_o, $w);
        $spec ['company_data'  ]['x'] = $this->_scale ($spec ['company_data'  ]['x'], $w_o, $w);
        $spec ['company_data'  ]['y'] = $this->_scale ($spec ['company_data'  ]['y'], $w_o, $w);
        $spec ['company_data'  ]['w'] = $this->_scale ($spec ['company_data'  ]['w'], $w_o, $w);
        $spec ['company_data'  ]['h'] = $this->_scale ($spec ['company_data'  ]['h'], $w_o, $w);
        $spec ['expiry_banner' ]['x'] = $this->_scale ($spec ['expiry_banner' ]['x'], $w_o, $w);
        $spec ['expiry_banner' ]['y'] = $this->_scale ($spec ['expiry_banner' ]['y'], $w_o, $w);
        $spec ['expiry_banner' ]['w'] = $this->_scale ($spec ['expiry_banner' ]['w'], $w_o, $w);
        $spec ['expiry_banner' ]['h'] = $this->_scale ($spec ['expiry_banner' ]['h'], $w_o, $w);
        $spec ['expiry_data'   ]['x'] = $this->_scale ($spec ['expiry_data'   ]['x'], $w_o, $w);
        $spec ['expiry_data'   ]['y'] = $this->_scale ($spec ['expiry_data'   ]['y'], $w_o, $w);
        $spec ['expiry_data'   ]['w'] = $this->_scale ($spec ['expiry_data'   ]['w'], $w_o, $w);
        $spec ['expiry_data'   ]['h'] = $this->_scale ($spec ['expiry_data'   ]['h'], $w_o, $w);
        $spec ['barcode'       ]['x'] = $this->_scale ($spec ['barcode'       ]['x'], $w_o, $w);
        $spec ['barcode'       ]['y'] = $this->_scale ($spec ['barcode'       ]['y'], $w_o, $w);
        $spec ['barcode'       ]['w'] = $this->_scale ($spec ['barcode'       ]['w'], $w_o, $w);
        $spec ['barcode'       ]['h'] = $this->_scale ($spec ['barcode'       ]['h'], $w_o, $w);
        $spec ['details_banner']['x'] = $this->_scale ($spec ['details_banner']['x'], $w_o, $w);
        $spec ['details_banner']['y'] = $this->_scale ($spec ['details_banner']['y'], $w_o, $w);
        $spec ['details_banner']['w'] = $this->_scale ($spec ['details_banner']['w'], $w_o, $w);
        $spec ['details_banner']['h'] = $this->_scale ($spec ['details_banner']['h'], $w_o, $w);
        $spec ['details_data'  ]['x'] = $this->_scale ($spec ['details_data'  ]['x'], $w_o, $w);
        $spec ['details_data'  ]['y'] = $this->_scale ($spec ['details_data'  ]['y'], $w_o, $w);
        $spec ['details_data'  ]['w'] = $this->_scale ($spec ['details_data'  ]['w'], $w_o, $w);
        $spec ['details_data'  ]['h'] = $this->_scale ($spec ['details_data'  ]['h'], $w_o, $w);
        $spec ['howto'         ]['x'] = $this->_scale ($spec ['howto'         ]['x'], $w_o, $w);
        $spec ['howto'         ]['y'] = $this->_scale ($spec ['howto'         ]['y'], $w_o, $w);
        $spec ['howto'         ]['w'] = $this->_scale ($spec ['howto'         ]['w'], $w_o, $w);
        $spec ['howto'         ]['h'] = $this->_scale ($spec ['howto'         ]['h'], $w_o, $w);
        $spec ['howto_link'    ]['x'] = $this->_scale ($spec ['howto_link'    ]['x'], $w_o, $w);
        $spec ['howto_link'    ]['y'] = $this->_scale ($spec ['howto_link'    ]['y'], $w_o, $w);
        $spec ['howto_link'    ]['w'] = $this->_scale ($spec ['howto_link'    ]['w'], $w_o, $w);
        $spec ['howto_link'    ]['h'] = $this->_scale ($spec ['howto_link'    ]['h'], $w_o, $w);
        $spec ['contact'       ]['x'] = $this->_scale ($spec ['contact'       ]['x'], $w_o, $w);
        $spec ['contact'       ]['y'] = $this->_scale ($spec ['contact'       ]['y'], $w_o, $w);
        $spec ['contact'       ]['w'] = $this->_scale ($spec ['contact'       ]['w'], $w_o, $w);
        $spec ['contact'       ]['h'] = $this->_scale ($spec ['contact'       ]['h'], $w_o, $w);

        return $spec;
      }

    function deal ($prop, $data, $name, $spec = null, $method = 'I')
      {
        return $this->_build (false, false, $prop, $data, $name, $spec, $method);
      }

    function gift ($prop, $data, $name, $spec = null, $method = 'I')
      {
        return $this->_build (true, false, $prop, $data, $name, $spec, $method);
      }

    function deal_tourism ($prop, $data, $name, $spec = null, $method = 'I')
      {
        return $this->_build (false, true, $prop, $data, $name, $spec, $method);
      }

    function gift_tourism ($prop, $data, $name, $spec = null, $method = 'I')
      {
        return $this->_build (true, true, $prop, $data, $name, $spec, $method);
      }


    function _scale ($dim_o, $base_o, $base_n)
      {
        return ($base_n / $base_o) * $dim_o;
      }

    function _ar ($w_o, $h_o, $w_n, $h_n)
      {
        if      ($w_n == null && $h_n == null)  return ($h_o / $w_o)                  ;
        if      ($w_n != null && $h_n == null)  return $this->_scale ($w_n, $w_o, $h_o);
        else if ($w_n == null && $h_n != null)  return $this->_scale ($h_n, $h_o, $w_o);
        else if ($w_n != null && $h_n != null)  return ($w_o / $h_o) == ($w_n * $h_n) ;
      }

    function _scaleInBounds ($w_o, $h_o, & $w_n, & $h_n)
      {
        $w_nn = $this->_ar ($w_o, $h_o, null, $h_n);
        $h_nn = $this->_ar ($w_o, $h_o, $w_n, null);

        if ($w_nn > $w_n)  $h_n = $h_nn;  else $w_n = $w_nn;
      }

    function _build ($isGift, $isTourism, $prop, $data, $name, $spec = null, $method = 'I')
      {
        if ($spec == null)  $spec = $this->defaultSpec ();
        if (!in_array ($method, array ('I', 'D', 'F', 'S', 'FI', 'FD', 'E')))  $method = 'I';

        // Configurar TCPDF.
        App::import ('Vendor', 'tcpdf/tcpdf');
        $pdf = new TCPDF ('P', 'mm', array ($spec ['width'], $spec ['height']), true, 'UTF-8', false);

        // Declarar propiedaddes.
        $pdf->SetCreator  ($prop ['creator' ]);
        $pdf->SetAuthor   ($prop ['author'  ]);
        $pdf->SetTitle    ($prop ['title'   ]);
        $pdf->SetSubject  ($prop ['subject' ]);
        $pdf->SetKeywords ($prop ['keywords']);

        // Eliminar headers, footers y m�rgenes.
        $pdf->setPrintHeader (false);
        $pdf->setPrintFooter (false);
        $pdf->SetMargins (0, 0, 0, true);
        $pdf->setHeaderMargin (0);
        $pdf->setFooterMargin (0);

        // Definir propiedades internas.
        $pdf->SetAutoPageBreak (false);
        $pdf->SetCellPadding (0);
        $pdf->setImageScale (1.0);
        $pdf->pixelsToUnits (1.0);
        $pdf->setCellHeightRatio (1.25);

        // -----------------------------------------------------------------------------------------
        // --  Cuerpo  -----------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------
        $pdf->AddPage ();
        // Definir la font por defecto
        $pdf->SetFont ('helvetica', '', $spec ['size_normal']);

        // Fondo gris y marco blanco
        $pdf->MultiCell ($spec ['width'], $spec ['height'], '', array ('LTRB' => array('width' => $spec ['padding_width'], 'color' => array (236, 236, 236))), 'C', false, 0, 0, 0);

        // Encabezado
        $pdf->Image ($data ['##ABSOLUTE_IMG_PATH##'] . ($isTourism ? 'coupon/header_t.gif' : 'coupon/header.gif'), $spec ['header']['x'], $spec ['header']['y'], $spec ['header']['w'], $spec ['header']['h']);

        // C�digo de cup�n y posnet
        $text = '<strong style="font-size:' . $spec ['size_small'] . 'pt;">'. $data ['##COUPON_CODE##'].'<br /></strong>'
              . '<strong style="font-size:' . $spec ['size_small'] . 'pt;"><br />Cod.Posnet: '. $data ['##POSNET_CODE##'].'</strong>';

        $pdf->MultiCell ($spec ['code']['w'], $spec ['code']['h'], $text, 0, 'C', false, 0, $spec ['code']['x'], $spec ['code']['y'], true, 0, true);

        // T�tulo de la campa�a (va a depender de si es un "gift" o no)
        $text = $isGift ?
                'El usuario: '
              . $data ['##USER_NAME##']
              . '<br />'
              . 'te envi&oacute;: '
              . $data ['##MESSAGE##']
              :
                '<br />'
              . 'Oferta: <strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">'
              . $data ['##DEAL_TITLE##']." - " .strip_tags($data ['##DESCRIPTIVE_TEXT##'])
              . '</strong><br /> '
              . ($isTourism ? 'Operador Responsable: Prosa Promotora Sol Argentina S.A. EVT Resol. Nº 0323/75, Legajo 1277. <br />' : ' ')
              . 'El monto de la compra ya fue abonado. La transacci&oacute;n se confirm&oacute; con &eacute;xito.'
            ;
        $pdf->MultiCell ($spec ['title']['w'], $spec ['title']['h'], $text, 0, 'L', false, 0, $spec ['title']['x'], $spec ['title']['y'], true, 0, true);

        // (de ac� en m�s la font es siempre "small")
        $pdf->SetFontSize ($spec ['size_small']);

        // Usuario (va a depender de si es un "gift" o no)
        $text = '<strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">TITULAR DEL CUP&Oacute;N</strong>';
        $pdf->MultiCell ($spec ['user_banner']['w'], $spec ['user_banner']['h'], $text, 0, 'L', false, 0, $spec ['user_banner']['x'], $spec ['user_banner']['y'], true, 0, true);
        $text = $isGift ?
                '<br />'
              . '<br />'
              . 'Usuario: <strong>'
              . $data ['##RECIPIENT_USER_NAME##']
              . '</strong><br />'
              . 'DNI: <strong>'
              . $data ['##GIFT_DNI##']
              . '</strong><br />'
              . 'DNI (registrado): <strong>'
              . $data ['##USER_DNI##']
              . '</strong><br /><br />'

              : // Me cago en el operador ternario. Larga vida al IF con llaves.

                '<br />'
              . '<br />'
              . 'Usuario: <strong>'
              . $data ['##USER_NAME##']
              . '</strong><br />'
              . 'DNI: <strong>'
              . $data ['##USER_DNI##']
              . '</strong>';
        if (!empty ($data ['##COUPON_PIN##']))  $text = $text . $data ['##COUPON_PIN##'];
        $pdf->MultiCell ($spec ['user_data']['w'], $spec ['user_data']['h'], $text, 0, 'L', false, 0, $spec ['user_data']['x'], $spec ['user_data']['y'], true, 0, true);

        // Campa�a
        $text = '<strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">CANJEAR EN</strong>';
        $pdf->MultiCell ($spec ['company_banner']['w'], $spec ['company_banner']['h'], $text, 0, 'L', false, 0, $spec ['company_banner']['x'], $spec ['company_banner']['y'], true, 0, true);
        $text = '<br />'
              . '<br />'
              . $data ['##COMPANY_NAME##']
              . '<br />'
              . $data ['##COMPANY_PHONE##']
              . '<br />'
              . $data ['##COMPANY_ADDRESS_1##']
              . '<br />'
              . $data ['##COMPANY_ADDRESS_2##']
              . '<br />'
              . $data ['##COMPANY_CITY##'];
        $pdf->MultiCell ($spec ['company_data']['w'], $spec ['company_data']['h'], $text, 0, 'L', false, 0, $spec ['company_data']['x'], $spec ['company_data']['y'], true, 0, true);

        // Vencimiento
        if ($data ['##IS_NOW##']) {
            $text = '<strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">VIGENCIA DEL CUP&Oacute;N</strong>';
        } else{
            $text = '<strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">VENCIMIENTO DEL CUP&Oacute;N</strong>';
        }

        $pdf->MultiCell ($spec ['expiry_banner']['w'], $spec ['expiry_banner']['h'], $text, 0, 'L', false, 0, $spec ['expiry_banner']['x'], $spec ['expiry_banner']['y'], true, 0, true);
        $text = '<br />'
              . '<br />'
              . $data ['##COUPON_EXPIRY_DATE##'];
        $pdf->MultiCell ($spec ['expiry_data']['w'], $spec ['expiry_data']['h'], $text, 0, 'L', false, 0, $spec ['expiry_data']['x'], $spec ['expiry_data']['y'], true, 0, true);

        // C�digo de barras
        $pdf->Image ($data ['##BARCODE##'], $spec ['barcode']['x'], $spec ['barcode']['y'], $spec ['barcode']['w'], $spec ['barcode']['h']);

        // Condiciones
        $text = '<strong style="color:#f25607;font-size:' . $spec ['size_big'] . 'pt;">CONDICIONES</strong>';
        $pdf->MultiCell ($spec ['details_banner']['w'], $spec ['details_banner']['h'], $text, 0, 'L', false, 0, $spec ['details_banner']['x'], $spec ['details_banner']['y'], true, 0, true);
        $text = '<br />'
              . '<br />'
              . $data ['##COUPON_CONDITION##'];
        $pdf->MultiCell ($spec ['details_data']['w'], $spec ['details_data']['h'], $text, 0, 'L', false, 0, $spec ['details_data']['x'], $spec ['details_data']['y'], true, 0, true);

        // "C�mo usar este cup�n"
        $pdf->Image ($data ['##ABSOLUTE_IMG_PATH##'] . 'coupon/004.gif', $spec ['howto']['x'], $spec ['howto']['y'], $spec ['howto']['w'], $spec ['howto']['h']);
        $pdf->Link ($spec ['howto_link']['x'], $spec ['howto_link']['y'], $spec ['howto_link']['w'], $spec ['howto_link']['h'], 'http://soporte.clubcupon.com.ar/');  // esto arregla el mapa roto de la template

        // Contacto
        $text = '<br />'
              . '&iexcl;Muchas Gracias por tu compra!  <strong>El equipo de Club Cup&oacute;n</strong>.  <a href="http://www.clubcupon.com/?from=mailing" target="_blank" style="color:#f25607;">www.clubcupon.com</a>';
        $pdf->MultiCell ($spec ['contact']['w'], $spec ['contact']['h'], $text, 0, 'L', false, 0, $spec ['contact']['x'], $spec ['contact']['y'], true, 0, true);

        // Generar salida
        return $pdf->Output ($name, $method);
      }
  }
