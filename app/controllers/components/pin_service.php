<?php

class PinServiceComponent extends Object {

    var $controller;

    function addPinsToDeal ($deal_id, $pins) {
      App::import ('Model', 'Pin');
      $this->Pin = & new Pin ();

      $data = array ();
      foreach ($pins as $pin) {
        $data [] = array ('deal_id' => $deal_id, 'code' => $pin);
      }

      return $this->Pin->saveAll ($data);
    }
    
    function addPinsToProduct ($product_id, $pins) {
    	App::import ('Model', 'Pin');
    	$this->Pin = & new Pin ();
    
    	$data = array ();
    	foreach ($pins as $pin) {
    		$data [] = array ('product_id' => $product_id, 'code' => $pin);
    	}
    
    	return $this->Pin->saveAll ($data);
    }
// class PinServiceComponent
}
?>
