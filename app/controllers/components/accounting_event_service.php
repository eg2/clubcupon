<?php

App::import('Core', 'Component');
App::import('Model', 'Company');
App::import('Model', 'Deal');

class AccountingEventServiceComponent extends Component{

    public $name = 'AccountingEventService';

    function __construct() {
        $this->Deal = new Deal();
        $this->Company = new Company();
        parent::__construct();
    }

    private function isPendingAccountingEventForDeal($deal) {
        return $deal['Deal']['is_pending_accounting_event'] == 1;
    }

    private function registerAccountingEventForDeal($deal) {
        $fields = array(
            'Deal.is_pending_accounting_event' => 1
        );
        $conditions = array(
            'Deal.id' => $deal['Deal']['id']
        );
        return $this->Deal->updateAll($fields, $conditions);
    }

    private function registerAccountingEventForCompany($company) {
        $fields = array(
            'Company.is_pending_accounting_event' => 1
        );
        $conditions = array(
            'Company.id' => $company['Company']['id']
        );
        $this->Company->updateAll($fields, $conditions);
    }

    function registerAccountingEvent($deal) {
        
        try {
            if (!$this->isPendingAccountingEventForDeal($deal)) {
                $this->registerAccountingEventForDeal($deal);
                $company = array(
                    'Company' => array(
                        'id' => $deal['Deal']['company_id']
                    )
                );
                $this->registerAccountingEventForCompany($company);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}

?>
