<?php
/*
 * Archivo: app/controlers/components/xemail.php
 *
 * Usar como:  var $components = array ('Xemail');  (en Model)
 * o:          App::import ('Component', 'Xemail');
 *
 */

class XemailComponent extends EmailComponent
  {
    /*
     * Lista de strings a attachear (se attachean así como están, con lo cual, han de estar ya
     *   codificados en base64 y chunkeados, además, han de tener su encabezado MIME seteado.
     *
     * Por ejemplo:
     *   $this->Xemail->stringAttachments [] = $MIMEHeader . chunk_split (base64_encode ($data));
     *
     */
    var $stringAttachments = array ();

    function doStringAttach ($str) { $this->stringAttachments [] = $str; }

    function send ($content = null, $template = null, $layout = null)
      {
        /* Si hay stringAttachments, levantar el centinela, si no, borrarlo.
         * Esto nos garantiza que en las l�neas:
         *   - http://api.cakephp.org/view_source/email-component/#line-374
         *   - http://api.cakephp.org/view_source/email-component/#line-444
         *   - http://api.cakephp.org/view_source/email-component/#line-477
         *   - http://api.cakephp.org/view_source/email-component/#line-582
         *   - http://api.cakephp.org/view_source/email-component/#line-609
         *   se entra en los "if"s si hay attachments por archivo o por strings.
         *
         * (Esto es para engañar a "parent::__createBoundary ()").
         *
         */
        if (!empty ($this->stringAttachments))        $this->attachments ['\0'] = '\0';
        else                                   unset ($this->attachments ['\0']);  // ¿Hace falta esto?

        // Llamar al padre para terminar el send.
        return parent::send ($content, $template, $layout);
      }

    function __attachFiles ()
      {
        // Agregar información "raw". Notar que si no hay stringAttachments esto es un NOP.
        foreach ($this->stringAttachments as $data)
          {
            // Notar que si había stringAttachments, "$this->__boundary" ya fue creado.
            $this->__message [] = '--' . $this->__boundary;
            $this->__message [] = $data;
            $this->__message [] = '';
          }

        /* Eliminar el centinela de attachments (esto evita que parent::_attachFiles () trate de
         *   attachear el archivo '\0'.
         *
         * (Esto es para "desengañar" a "parent::__attachFiles ()").
         *
         */
        unset ($this->attachments ['\0']);
        // Llamar al padre para terminar el attach.
        parent::__attachFiles ();
      }

	function __formatMessage ($message)
    {
      if (!empty ($this->attachments))
        $message = array_merge (
          array
            (
              '--' . $this->__boundary,
              'Content-Type: ' . (($this->sendAs === 'html') ? 'text/html' : 'text/plain') . '; charset=' . $this->charset,
              'Content-Transfer-Encoding: 7bit',
              ''
            ), $message);

      return $message;
    }

    function reset ()
      {
        // Resetear stringAttachments (de igual modo que attachments).
        $this->stringAttachments = array ();

        // Llamar al padre para terminar el reset.
        parent::reset ();
      }
  }
?>
