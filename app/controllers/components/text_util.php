<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class TextUtilComponent extends Object {
    
    function replaceAccents($s){
 
       $s =  mb_ereg_replace("[á|â|à]", 'a', $s);
       $s =  mb_ereg_replace("[é|ê|è]", 'e', $s);
       $s =  mb_ereg_replace("[í|î|ì]", 'i', $s);
       $s =  mb_ereg_replace("[ó|ô|ò]", 'o', $s);
       $s =  mb_ereg_replace("[ú|û|ù]", 'u', $s);
       $s =  mb_ereg_replace("[ñ]", 'n', $s);
       
       return $s;
    }
}
?>
