<?php

class ProductsController extends AppController {

    var $name = 'Products';
    var $components = array(
        'api.ApiLogger',
        'product.ProductInventoryService',
        'product.ProductService'
    );
    var $uses = array(
        'Product.ProductProduct',
        'Product.ProductCampaign',
        'Product.ProductInventory',
        'Companies',
        'Deal'
    );
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'ProductProduct.id' => 'desc'
        ),
        'contain' => array(
            'ProductCampaign'
        )
    );

    function admin_index($campaignId) {
        if (empty($campaignId)) {
            $this->Session->setFlash('Seleccionar una campaña para ver sus productos', true);
            $this->redirect(array(
                'controller' => 'Campaigns',
                'action' => 'index'
            ));
        }
        $this->pageTitle = 'Listado de productos por campa&ntilde;a';
        $campaign = $this->ProductCampaign->findById($campaignId);
        if (!empty($this->data['products']['product_name'])) {
            $conditions['ProductProduct.name LIKE'] = '%' . $this->data['products']['product_name'] . '%';
        }
        $conditions['ProductProduct.product_campaign_id'] = $campaignId;
        $this->paginate = array(
            'conditions' => $conditions,
            'fields' => array(
                'ProductProduct.*'
            ),
            'order' => array(
                'ProductProduct.id DESC'
            )
        );
        $companiesList = $this->Company->getOrderedCompaniesList();
        $companyname = $companiesList[$campaign['ProductCampaign']['company_id']];
        $campaignsList = $this->ProductCampaign->find('list');
        $searchDefaultCampaign = $campaign['ProductCampaign']['id'];
        $searchDefaultCompany = $campaign['ProductCampaign']['company_id'];
        $products = $this->paginate();
        $products = $this->ProductInventoryService->addAvailableStockForProducts($products);
        $this->set(compact('products', 'campaign', 'companiesList', 'campaignsList', 'searchDefaultCampaign', 'companyname', 'searchDefaultCompany', 'externalProducts'));
    }

    function admin_add($campaignId) {
        if (empty($campaignId)) {
            $this->Session->setFlash('Seleccionar una campaña para asignarle productos', true);
            $this->redirect(array(
                'controller' => 'Campaigns',
                'action' => 'index'
            ));
        }
        $externalProducts = $this->ProductInventory->findAllForSelectForm();
        if (!empty($this->data)) {
            if ($this->_isStockMoreOrEqualToZero($this->data['product'])) {
                try {
                    $this->ApiLogger->debug(__METHOD__, $this->data);
                    $optional['ProductProduct']['created_by'] = $this->Auth->user('id');
                    $optional['ProductProduct']['product_inventory_id'] = $this->data['ProductProduct']['product_inventory_id'];
                    $stock = $this->data['product']['stock'];
                    $decrementedUnits = $this->data['product']['decremented_units'];
                    $has_pins = $this->data['product']['has_pins'];
                    $campaignId = $this->data['product']['product_campaign_id'];
                    $optional['ProductProduct']['libertya_code'] = $this->data['ProductProduct']['libertya_code'];
                    $product = $this->ProductService->createProduct($this->data['product']['name'], $campaignId, $stock, $decrementedUnits, $has_pins, $optional);
                    $this->Session->setFlash('Producto guardado', true);
                    $this->set('product', $product);
                    $this->redirect(array(
                        'action' => 'index',
                        $this->data['product']['product_campaign_id']
                    ));
                } catch (Exception $e) {
                    $this->ApiLogger->error($e->getMessage(), $this->data);
                    $this->Session->setFlash('No se pudo guardar el producto', true);
                }
            } else {
                $this->ProductProduct->invalidate('stock', 'Debes ingresar un stock mayor o igual a cero');
            }
        }
        $this->set('externalProducts', $externalProducts);
        $this->data['product']['product_campaign_id'] = $campaignId;
    }

    function admin_edit() {
        $externalProducts = $this->ProductInventory->findAllForSelectForm();
        if (!empty($this->data)) {
            $this->ProductProduct->set($this->data);
            $this->ProductProduct->set('modified_by', $this->Auth->user('id'));
            $oldStock = $this->ProductProduct->findById($this->data['ProductProduct']['id']);
            if ($this->ProductProduct->save()) {
                $this->ApiLogger->debug(__METHOD__, $this->data);
                $action = __METHOD__ . ' - Edicion de Producto';
                $this->ApiLogger->debug(__METHOD__, $oldStock);
                $this->ProductInventoryService->trackingStockMovements($oldStock['ProductProduct']['id'], $action, $oldStock['ProductProduct']['stock'], $this->data['ProductProduct']['stock'], $this->Auth->user('id'));
                $this->Session->setFlash('El producto fue editado', 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index',
                    $this->data['ProductProduct']['product_campaign_id']
                ));
            } else {
                $validationErrors['ProductProduct'] = $this->ProductProduct->validationErrors;
                $this->Session->setFlash('No se pudo editar el producto', 'default', null, 'error');
            }
        }
        if (empty($this->params['named']['campaignId'])) {
            $this->Session->setFlash('Seleccionar una campaña', true);
            $this->redirect(array(
                'controller' => 'Campaigns',
                'action' => 'index'
            ));
        }
        if (empty($this->params['named']['productId'])) {
            $this->Session->setFlash('Seleccionar un producto a editar', true);
            $this->redirect(array(
                'controller' => 'Products',
                'action' => 'index',
                $this->params['named']['campaignId']
            ));
        }
        $this->data = $this->ProductProduct->read(null, $this->params['named']['productId']);
        $this->set('externalProducts', $externalProducts);
        $this->set('validationErrors', $validationErrors);
    }

    function admin_delete() {
        $productId = $this->params['named']['productId'];
        $campaignId = $this->params['named']['campaignId'];
        if ($this->Deal->returnOpenUpcomingOrTippedCountByProductId($productId) == 0) {
            try {
                $this->ProductProduct->delete($productId);
                $this->ApiLogger->debug(__METHOD__, $this->params);
                $this->Session->setFlash('Se borro correctamente el producto .');
            } catch (Exception $e) {
                $this->Session->setFlash('La producto no pudo borrarse. Error: ' . $e->getMessage());
            }
        } else {
            $this->Session->setFlash('Este producto pertenece a una oferta Próxima, Abierta, o En Marcha, y no puede borrarse');
        }
        $this->redirect(array(
            'action' => 'index',
            $campaignId
        ));
    }

    function admin_search() {
        $this->pageTitle = 'Listado de productos';
        if (!empty($this->data['products']['campaign_id'])) {
            $this->ApiLogger->debug('data::campaign_id', $this->data['products']['campaign_id']);
            $findConditions['ProductProduct.product_campaign_id'] = $this->data['products']['campaign_id'];
        }
        if (!empty($this->data['products']['product_name'])) {
            $findConditions['ProductProduct.name LIKE'] = '%' . $this->data['products']['product_name'] . '%';
        }
        if (!empty($this->params['named']['page'])) {
            if (empty($findConditions)) {
                if (!empty($this->params['named']['campaign_id'])) {
                    $this->ApiLogger->debug('params::campaign_id', $this->params['named']['campaign_id']);
                    $findConditions['ProductProduct.product_campaign_id'] = $this->params['named']['campaign_id'];
                }
                if (!empty($this->params['named']['product_name'])) {
                    $this->ApiLogger->debug('params::product_name', $this->params['named']['product_name']);
                    $findConditions['ProductProduct.name LIKE'] = '%' . $this->params['named']['product_name'] . '%';
                }
            }
        }
        $this->set('campaign_id', $findConditions['ProductProduct.product_campaign_id']);
        $this->set('product_name', str_replace('%', '', $findConditions['ProductProduct.name LIKE']));
        $this->ApiLogger->debug('findConditions', $findConditions);
        $this->ApiLogger->debug('campaign_id', $findConditions['ProductProduct.product_campaign_id']);
        $this->ApiLogger->debug('product_name', str_replace('%', '', $findConditions['ProductProduct.name LIKE']));
        $companiesList = $this->Company->getOrderedCompaniesList();
        $campaignsList = $this->ProductCampaign->find('list');
        $this->paginate['conditions'] = $findConditions;
        $products = $this->paginate();
        $this->set(compact('products', 'companiesList', 'campaignsList'));
    }

    function admin_all_by_campaign_id($id) {
        $this->layout = 'ajax';
        $this->set('products', $this->ProductProduct->findAllByCampaignId($id));
    }

    function admin_all_selectables_by_campaign_id($id) {
        $this->layout = 'ajax';
        $products = $this->ProductProduct->findAllSelectablesByCampaignId($id);
        foreach ($products as $i => $product) {
            $stock = $this->ProductInventoryService->availableQuantityByProduct($product);
            $products[$i]['ProductProduct']['name'] = $products[$i]['ProductProduct']['name'] . '::::::: Stock: ' . $stock;
        }
        $this->set('products', $products);
    }

    function _isStockMoreOrEqualToZero($product) {
        return $this->data['product']['stock'] >= 0;
    }

    function admin_add_pins($product_id = null) {
        $this->layout = 'admin_bootstrap';
        if (empty($product_id)) {
            if (!empty($this->data['product']['id'])) {
                $product_id = $this->data['product']['id'];
            } else {
                $this->cakeError('error404');
            }
        }
        $theProduct = $this->ProductProduct->find('first', array(
            'conditions' => array(
                'ProductProduct.id' => $product_id
            )
        ));
        if (isset($theProduct['ProductProduct']['has_pins']) && empty($theProduct['ProductProduct']['has_pins'])) {
            $this->Session->setFlash('El Producto no admite PINes', 'default', null, 'error');
            $this->redirect(array(
                'action' => 'index',
                $this->data['product']['product_campaign_id']
            ));
        }
        if (!empty($this->data)) {
            $pins = str_replace(array(
                "\r",
                "\f",
                "\t",
                "\v",
                "\b"
                    ), "\n", $this->data['product']['pins']);
            $pins = explode("\n", $pins);
            $pins = array_filter($pins);
            $pins = array_unique($pins);
            $pins = array_values($pins);
            App::import('Component', 'PinService');
            $this->PinService = & new PinServiceComponent();
            $this->PinService->addPinsToProduct($product_id, $pins);
            $this->Session->setFlash('Los PINes fueron agregados con éxito', 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index',
                $this->data['product']['product_campaign_id']
            ));
        } else {
            $this->set('product_name', $theProduct['ProductProduct']['name']);
            $this->set('has_pins', $theProduct['ProductProduct']['has_pins']);
            $this->set('product_id', $product_id);
            $this->set('num_pins_ok', $this->ProductProduct->ProductPin->find('count', array(
                        'conditions' => array(
                            'ProductPin.product_id' => $product_id,
                            'ProductPin.is_used' => false
                        ),
                        'recursive' => - 1
            )));
            $this->set('num_pins_ko', $this->ProductProduct->ProductPin->find('count', array(
                        'conditions' => array(
                            'ProductPin.product_id' => $product_id,
                            'ProductPin.is_used' => true
                        ),
                        'recursive' => - 1
            )));
        }
        $this->data['product']['product_campaign_id'] = $theProduct['ProductProduct']['product_campaign_id'];
    }

}
