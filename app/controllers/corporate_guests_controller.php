<?php

/**
 * @property Deal $Deal
 */
class CorporateGuestsController extends AppController {

    var $uses = array(
        'CorporateGuest',
        'Deal'
    );

    /**
     * Verifica si el pin corporate es valido.
     * @param string corporate_pin
     *
     * @throws InvalidArgumentException cuando el corporate_pin es vacio
     * @return boolean
     */
    function check_corporate_pin() {
        $this->RequestHandler->respondAs('text/x-json');
        $this->autoRender = false;
        $data = array('success' => 0, 'isValid' => 0);
        $data = array_merge($data, $_POST);
        try {
            if($this->CorporateGuest->isValidCorporatePin($_POST['corporate_pin'])) {
                $data['isValid'] = 1;
                $this->Cookie->write('is_corporate_guest', 1);
            } else {
                $data['isValid'] = 0;
                $this->Cookie->write('is_corporate_guest', 0);
            }
            $data['success'] = 1;
        } catch (Exception $e) {
            $data['success'] = 0;
            $data['errorMessage'] = $e->getMessage();
        }
        echo json_encode($data);
    }


    /**
     * Envia a la direccion de correo ingresa un mail para la alta como corporate.
     * @param string email
     *
     * @throws InvalidArgumentException cuando el email es vacio
     * @return boolean
     */
    function email_signin() {
       // $this->RequestHandler->respondAs('text/x-json');
        $this->autoRender = false;
        $data = array('success' => 0, 'isValid' => 0);
        $data = array_merge($data, $_POST);
        try {
            if($this->Deal->_sendExclusiveMailToUserOnPinRequest($_POST['email'])) {
                $data['isSent'] = 1;
            } else {
                $data['isSent'] = 0;
            }
            $data['success'] = 1;
        } catch (Exception $e) {
            $data['success'] = 0;
            $data['errorMessage'] = $e->getMessage();
        }
        echo json_encode($data);
    }

}
