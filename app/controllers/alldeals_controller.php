<?php

class AllDealsController extends AppController {

    var $name = 'Alldeals';
    var $uses = array(
        'Deal'
    );

    function show($city_id = null) {
        if (!empty($city_id)) {
            $ciudad = $this->Deal->City->findById($city_id);
            $selected_city_name = $ciudad['City']['name'];
            $titleforpage = 'Todas las ofertas en <span>' . $selected_city_name . '</span>';
        } else {
            $selected_city_name = null;
            $titleforpage = 'Todas las ofertas';
        }
        $dealsoftheday = $this->Deal->findAllOpenOrTippedAndNotCorporate($city_id);
        if (empty($dealsoftheday)) {
            if (isset($selected_city_name)) {
                $titleforpage = 'No encontramos ofertas en <span>' . $selected_city_name . '</span>';
            } else {
                $titleforpage = 'No encontramos ofertas';
            }
        }
        $this->set('titleforpage', $titleforpage);
        $this->set('selected_city_name', $selected_city_name);
        $this->set('dealsoftheday', $dealsoftheday);
        $this->set('dealsofthedaymethod', true);
        $this->set('escape_header', true);
    }

}