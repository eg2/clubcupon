<?php

class PassbooksController extends AppController {

    var $name = 'Passbooks';

	var $uses = array('DealUser');


    function generate_passbook_file($coupon_code=null)
    {

        App::import ('Vendor', 'passbook');
        $pass = new PKPass();

        App::import('Model', 'Redemption');
        $redemption = & new Redemption();

        if ($coupon_code == null) {
            $this->cakeError('error404');
        }
        
        $this->layout = 'blank';
        $this->autoRender = false ;
        
        $dealuser       = $this->DealUser->findByCouponCode($coupon_code);

        $data           = $redemption->findByDealUserId($dealuser['DealUser']['id']);
        $posnet_code    = $redemption->formatPosnetCode($data);
        $deal           = $this->DealUser->Deal->findDealById($dealuser['DealUser']['deal_id']);

        $pass->setCertificate           (Configure::read('passbook.p12Cert'));
        $pass->setCertificatePassword   (Configure::read('passbook.password'));
        $pass->setWWDRcertPath          (Configure::read('passbook.setWWDRcertPath'));

        // VER http://tracker.int.clarin.com/browse/CC-2519 para mas detalles

        $standardKeys                   = array(
                                            'description'           => 'Club Cupon Pass',
                                            'formatVersion'         => 1,
                                            'organizationName'      => 'CLARIN GLOBAL S.A.',
                                            'passTypeIdentifier'    => 'pass.com.cmd.clubcupon', //
                                            'serialNumber'          => $dealuser['DealUser']['id'],
                                            'teamIdentifier'        => '8SJ8VAAM5T'
                                            //
                                          );
        $associatedAppKeys              = array();
        $relevanceKeys                  = array();
        $webServiceKeys                 = array();


        //Referencia > http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

        $styleKeys                      = array(
                                            'coupon' => array(
                                                'primaryFields' => array(
                                                    array(
                                                        'key'   => 'deal',
                                                        'label' => 'Paga solo $'.$deal['Deal']['discounted_price'],
                                                        'value' => $deal['Deal']['name']
                                                    )
                                                ),
                                                'secondaryFields' => array(
                                                    array(
                                                        'key'   => 'date',
                                                        'label' => 'Canjeable hasta: ',
                                                        'value' => $deal['Deal']['coupon_expiry_date']
                                                    ),
                                                    array(
                                                        'key'   => 'company-address',
                                                        'label' => 'Direccion: ',
                                                        'value' => $deal['Deal']['custom_company_address1']
                                                    ),
                                                ),
                                                'backFields' => array(
                                                    array(
                                                        'key'   => 'deal',
                                                        'label' => 'Oferta',
                                                        'value' => $deal['Deal']['name']
                                                    ),
                                                    array(
                                                        'key'   => 'deal_description',
                                                        'label' => 'Detalles',
                                                        'value' => $deal['Deal']['subtitle']
                                                    ),
                                                    array(
                                                        'key'   => 'company-name',
                                                        'label' => 'Empresa',
                                                        'value' => $deal['Deal']['custom_company_name']
                                                    ),
                                                    array(
                                                        'key'   => 'company-address',
                                                        'label' => 'Direccion',
                                                        'value' => $deal['Deal']['custom_company_address1']
                                                    ),
                                                    array(
                                                        'key'   => 'coupon-code',
                                                        'label' => 'Codigo Posnet',
                                                        'value' => $posnet_code
                                                    ),
                                                    array(
                                                        'key'   => 'coupon-code',
                                                        'label' => 'Codigo Cupon',
                                                        'value' => $dealuser['DealUser']['coupon_code']
                                                    ),
                                                ),
                                                //'transitType' => 'PKTransitTypeAir' //para extra
                                            )
                                        );
        $visualAppearanceKeys           = array(
                                            'barcode'         => array(
                                                'format'          => 'PKBarcodeFormatQR',
                                                'message'         => $posnet_code,
                                                'altText'         => $posnet_code,
                                                'messageEncoding' => 'iso-8859-1'
                                            ),
                                        'backgroundColor' => 'rgb(245,137,54)',
                                        'logoText'        => 'Club Cupon'
                                        );

        //Listamos las imagenes a incluir en el passbook
        $passbookImages                 = array(
                                            Configure::read('passbook.imagesRoute').'icon.png',
                                            //Configure::read('passbook.imagesRoute').'imagen1.jpg',
                                        );

        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
                $standardKeys,
                $associatedAppKeys,
                $relevanceKeys,
                $styleKeys,
                $visualAppearanceKeys,
                $webServiceKeys
            );

        $pass->setJSON(json_encode($passData));

        $pass->addFile(ROOT.DS.'app'.DS.WEBROOT_DIR.DS.'img'.DS.'theme_clean'.DS.'passbook'.DS.'icon.png');
        $pass->addFile(ROOT.DS.'app'.DS.WEBROOT_DIR.DS.'img'.DS.'theme_clean'.DS.'passbook'.DS.'icon@2x.png');



        /*
        // Add files to the PKPass package
        foreach($passbookImages as $image)
        {
            $pass->addFile($image);
        }
        */


        //Create the passbook!
        return $pass->create(true);

    }







    // INTERNALS -------------------------------------------


    // Reemplaza todos los caracteres especiales, ver correspondencia 1<->1
    function __cleanChars($string) {
        $string = str_replace(array(' ', '-'), array('_', '_'), $string);
        // revision extra para evitar acentos y caracteres bizarros...
        $string = str_replace(array('á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò', 'ù', 'ä', 'ë', 'ï', 'ö', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü'),
                              array('a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U'), $string);
        return $string;
    }

    function __str_rand($length = 8) {
        // Possible values
        $output = 'abcdefghijklmnopqrstuvwqyz0123456789';

        // Seed
        list ($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);

        // Generate
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $output [mt_rand(0, strlen($output) - 1)];
        }
        return $str;
    }



}

?>