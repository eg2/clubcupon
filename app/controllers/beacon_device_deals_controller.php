<?php

class BeaconDeviceDealsController extends AppController {

    var $name = 'BeaconDeviceDeals';
    var $uses = array(
        'beacon.BeaconDeviceDeal',
        'beacon.BeaconDevice',
    	'api.ApiDeal',
    );
    
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'BeaconDeviceDeal.id' => 'desc'
        )
    );
    
	function admin_edit($id = null,$device_id = null) {
		
    	$id = (!empty($this->data['BeaconDeviceDeal']['id'])) ? $this->data['BeaconDeviceDeal']['id'] : $id;
    	
    	if (empty($this->data)) {
    	
    	}
    	//$this->data = $this->BeaconDeviceDeal->read(null, $id);
    	
    	$this->data = $this->BeaconDeviceDeal->find('first', array(
    			'conditions' => array(
    					'BeaconDeviceDeal.id' => $id
    			),
    			
    			'recursive' => 0
    	));
    	$date = new DateTime($this->data["BeaconDeviceDeal"]["since"]);
    	$new_date_format = $date->format('H:i:s');
    	$this->data["BeaconDeviceDeal"]["since"]=$new_date_format;
    	$date = new DateTime($this->data["BeaconDeviceDeal"]["until"]);
    	$new_date_format = $date->format('H:i:s');
    	$this->data["BeaconDeviceDeal"]["until"]=$new_date_format;
    	$mydeals=$this->BeaconDeviceDeal->find('all', array(
    			'recursive' => 0,
    			'fields' => array(
    					'ApiDeal.id',
    					'ApiDeal.name',
    					'BeaconDeviceDeal.id',
    					'BeaconDeviceDeal.title',
    					'DATE_FORMAT(BeaconDeviceDeal.since,"%H:%i:%s") as since',
    					'DATE_FORMAT(BeaconDeviceDeal.until,"%H:%i:%s") as until',
    					'BeaconDeviceDeal.beacon_device_id'
    			),
    			'contain' => array(
    					'ApiDeal'
    			),
    			'conditions' => array(
    					'BeaconDeviceDeal.beacon_device_id' => $device_id
    			),
    			'limit' => 20,
    			'order' => array(
    					'ApiDeal.id' => 'desc'
    			)
    	));
    	 
    	$thisBeaconDevice = $this->BeaconDevice->find('first', array(
    			'conditions' => array(
    					'BeaconDevice.id' => $device_id
    			),
        		'fields' => array(
        				'BeaconDevice.id',
        				'BeaconDevice.name'
        		),
        		'recursive' => 0
    	));
        $this->set('mydeals', $mydeals);
        $this->set('thisBeaconDevice', $thisBeaconDevice);
    	$this->set('beaconDeviceId', $device_id);
    	$this->render('admin_assign');
    }
  
    function admin_delete($id = null,$device_id = null ) {
    	$id = (!empty($this->data['BeaconDeviceDeal']['id'])) ? $this->data['BeaconDeviceDeal']['id'] : $id;
    	
    	if(isset($id) && !empty($id)){
    		try {
    			$this->BeaconDeviceDeal->delete ($id);
    			$this->Session->setFlash ('La oferta fue eliminada del Beacon', 'default', null, 'success');
    		} catch (Exception $e) {
    			$this->Session->setFlash('La oferta no pudo eliminarse del Beacon. Error: ' . $e->getMessage());
    		}
    	}
    	
    	$this->redirect(array(
            'action' => 'assign',
            $device_id
        ));
    }

    private function formatTimeRange(&$since,&$until){
    	$date = new DateTime($since);
    	$new_date_format = $date->format('H:i:s');
    	$since=$new_date_format;
    	$date = new DateTime($until);
    	$new_date_format = $date->format('H:i:s');
    	$until=$new_date_format;
    	
    } 
    
    function admin_assign($id = null) {
 		$this->pageTitle = 'Asignación de Ofertas a Beacons';
 		$id = (!empty($this->data['BeaconDeviceDeal']['beacon_device_id'])) ? $this->data['BeaconDeviceDeal']['beacon_device_id'] : $id;
 		
        if (!empty($this->data)) {
        	
        	    /*
        	
        	    $this->data['BeaconDeviceDeal']['since']="2015-01-01 ".$this->data['BeaconDeviceDeal']['since'];
        	    $this->data['BeaconDeviceDeal']['until']="2015-01-01 ".$this->data['BeaconDeviceDeal']['until'];
        	    */
        	
        	$this->formatTimeRange($this->data["BeaconDeviceDeal"]["since"],$this->data["BeaconDeviceDeal"]["until"]);
        	
        	 
        	    
        	    $this->data['BeaconDeviceDeal']['deal_id']=trim($this->data['BeaconDeviceDeal']['deal_id']);
        	    $isValidDealId=true;
        	    if(!isset($this->data['BeaconDeviceDeal']['id']) || empty($this->data['BeaconDeviceDeal']['id'])){//viene de una EDICION, no se valida el deal_id
        	    	$isValidDealId=$this->validateDealAssigned($id,$this->data['BeaconDeviceDeal']['deal_id'],$msg);
        	    }
        	    
        	  
        	    if(!$isValidDealId){
        	    	$this->Session->setFlash($msg, true);
        	    }else{
        	    
            	  $this->BeaconDeviceDeal->set($this->data);
            	  $this->BeaconDeviceDeal->set('created_by', $this->Auth->user('id'));
          
                  if ($this->BeaconDeviceDeal->save()) {
                    $this->Session->setFlash('Datos guardados', 'default', null, 'success');
                  
                    $this->redirect(array(
                        /*'controller' => 'beacon_device_deals',*/
                    		'action' => 'assign',$id
                    ));
                  } else {
                	
                    $this->Session->setFlash('No se pudo asignar la Oferta', true);
                  }
        	    }
            
        }
        $mydeals=$this->BeaconDeviceDeal->find('all', array(
        		'recursive' => 0,
        		'fields' => array(
        				'ApiDeal.id',
    					'ApiDeal.name',
    					'BeaconDeviceDeal.id',
    					'BeaconDeviceDeal.title',
    					'DATE_FORMAT(BeaconDeviceDeal.since,"%H:%i:%s") as since',
    					'DATE_FORMAT(BeaconDeviceDeal.until,"%H:%i:%s") as until',
    					'BeaconDeviceDeal.beacon_device_id'
        		),
        		'contain' => array(
        				'ApiDeal'
        		),
        		'conditions' => array(
        				'BeaconDeviceDeal.beacon_device_id' => $id
        		),
        		'limit' => 20,
        		'order' => array(
        				'ApiDeal.id' => 'desc'
        		)
        ));
        $thisBeaconDevice = $this->BeaconDevice->find('first', array(
    			'conditions' => array(
    					'BeaconDevice.id' => $id
    			),
        		'fields' => array(
        				'BeaconDevice.id',
        				'BeaconDevice.name'
        		),
        		'recursive' => 0
    	));
        $this->set('mydeals', $mydeals);
        $this->set('beaconDeviceId', $id);
        $this->set('thisBeaconDevice', $thisBeaconDevice);
    }
     private function validateDealAssigned($beacon_device_id,$deal_id,&$msg){
     	$deal=$this->ApiDeal->find('first', array(
     			'fields' => array(
     					'Deal.id',
     			),
     			'conditions' => array(
     					'Deal.id' => $deal_id,
     					'Deal.deal_status_id' => array(ConstDealStatus::Open,ConstDealStatus::Tipped),
     			),
     			'limit' => 1
     	));
     	if(empty($deal)){
     		$msg='La Oferta no existe, o no está habilitada para ser asignada';
     		return false;
     	}else{
     		$deviceDeal=$this->BeaconDeviceDeal->find('all', array(
     				'recursive' => 0,
     				'contain' => array(
     						'ApiDeal'
     				),
     				'fields' => array(
     						'ApiDeal.id',
     						'BeaconDeviceDeal.id',
     				),
     				'conditions' => array(
     						'BeaconDeviceDeal.beacon_device_id' => $beacon_device_id,
     						'ApiDeal.id' => $deal_id
     				),
     				'limit' => 1,
     		));
     		if(!empty($deviceDeal)){
     			$msg='La Oferta ya está asignada al Beacon';
     			return false;
     		}
     		
     	}
     	return true;
     	 	
     	
     }
    
    function admin_index($id = null) {
        $this->pageTitle = 'Asignación de Ofertas a Beacons';
        $id = (!empty($this->data['BeaconDeviceDeal']['id'])) ? $this->data['BeaconDeviceDeal']['id'] : $id;
        $deals = $this->ApiDeal->find('all', array(
        		'recursive' => - 1,
        		'fields' => array(
        				'Deal.id',
        				'Deal.name',
        				'Deal.subtitle',
             		),
        		'contain' => array(
        				'BeaconDeviceDeal'
        				),
        		'conditions' => array(
        				'Deal.id <>' => $id
        		),
        		'limit' => 10,
        		'order' => array(
        				'Deal.id' => 'desc'
        		)
        ));
        $mydeals=$this->BeaconDeviceDeal->find('all', array(
        		'recursive' => 0,
        		'fields' => array(
        				'ApiDeal.id',
        				'ApiDeal.name',
        				'ApiDeal.subtitle',
        				'BeaconDeviceDeal.id',
             		),
        		'contain' => array(
        				'ApiDeal'
        				),
        		'conditions' => array(
        				'BeaconDeviceDeal.beacon_device_id' => $id
        		),
        		'limit' => 20,
        		'order' => array(
        				'ApiDeal.id' => 'desc'
        		)
        ));
        /*
        $this->paginate=array(
                'conditions' => array(
                    'AccountingItem.accounting_calendar_id' => $calendar_id,
                    'AccountingItem.' . $filter . ' like' => $q
                ),
                'fields' => array(
                    'AccountingItem.id',
                    'AccountingItem.total_quantity',
                    'AccountingItem.total_amount',
                    'AccountingItem.deal_id',
                    'AccountingItem.model',
                    'AccountingItem.model_id',
                    'AccountingItem.accounting_calendar_id'
                ),
                'order' => array(
                    'AccountingItem.id' => 'desc'
                ),
                'limit' => 20
            );
        $deals = $this->paginate();
        */
       
        $this->set('deals', $deals);
        $this->set('mydeals', $mydeals);
    }

   

}
