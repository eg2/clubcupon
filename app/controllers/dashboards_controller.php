<?php

class DashboardsController extends AppController {

    var $name = 'Dashboards';
    var $components = array();
    var $uses = array(
        'AccountingCampaign',
        'AccountingCampaignItem',
        'AccountingCampaignItemExtra',
        'AccountingCampaignItemDetail',
        'AccountingMaxPaymentDateGrouped',
        'AccountingRedeemedGrouped',
        'AccountingDownpaymentPercentageGrouped',
        'AccountingCompanyIdGrouped',
        'AccountingDealIdGrouped',
        'AccountingGuaranteeFundGrouped',
        'Deal'
    );
    var $blank_layouts_exceptions = array(
        'deal_detail',
        'company_deals_by_campaign_code',
        'company_deals_by_max_payment_date',
        'company_deals_by_max_payment_date_and_mode',
    );

    var $paginate = array(
        'limit' => 20,
    );
    
    
    function beforeFilter() {
        $this->__validateIfControllerIsAccesibleByUser();
    }

    function beforeRender() {
        $this->__setNextLiquidation();
        $this->__setLayoutAccordingToView();
        parent::beforeRender();
    }

    function index() {
        $this->set('section', 'index');
    }

    function company_deals() {
        $this->set('section', 'company_deals');
    }

    function company_details() {
        $this->set('section', 'company_details');
    }

    function company_branches() {
        $this->set('section', 'company_branches');
    }

    function company_coupons() {
        $this->set('section', 'company_coupons');
    }

    function company_campaign() {
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        
        $conditionsForFindByCompanyId = array('company_id' => $company['Company']['id']);
        
        $campaigns = $this->paginate('all_by_company_id', $conditionsForFindByCompanyId);
        
        
        $this->set('dealsGroupedByCampaignCode', $campaigns);
        
        
        $isNowCompany = $this->Company->isNow($company);
        $this->set('company', $company);
        $this->set('isNowCompany', $isNowCompany);
        $this->set('section', 'company_campaign_liquidations');
    }

    function company_deals_by_campaign_code($campaignCode) {
        if (is_null($campaignCode)) {
            $this->__redirectOnWrongAccess();
        }
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $campaignItems = $this->AccountingCampaignItem->findAllByCompanyIdAndCampaignCode($company['Company']['id'], $campaignCode);
        $this->set('dealsByCampaignCode', $campaignItems);
    }
    
       
    function company_liquidations() {
        if ($this->params['named']['deal_id']) {
             $company = $this->Company->findByUserId($this->Auth->user('id'));
             $this->modelClass = 'AccountingDealIdGrouped';
             $cond['deal_company_id'] =  $company['Company']['id'];
             $cond['deal_id'] =  $this->params['named']['deal_id'];
             $this->paginate = array( 'conditions' => $cond, 'limit' => 10, 'order' =>'liquidate_total_amount DESC');
             $accountingEvents = $this->paginate();
             $this->set('citySlug',$this->params['named']['city']);
             $this->set('accountingEvents', $accountingEvents);
        } else {
            $company = $this->Company->findByUserId($this->Auth->user('id'));

            //hasta que modifiquemos el paginador para que soporte dos modelos..
            //$conditionsForFindByCompanyId = array('company_id' => $company['Company']['id']);
            //$accountingEventsByCalendarId = $this->paginate('all_by_company_id_grouped_by_calendar', $conditionsForFindByCompanyId);

            $conditionsForFindByCompanyId['conditions']['company_id'] =  $company['Company']['id'];
            $accountingEventsByMaxPaymentDate = $this->AccountingMaxPaymentDateGrouped->__findAllByCompanyIdGroupedByMaxPaymentDate($conditionsForFindByCompanyId);

            $this->set('dealsGroupedByMaxPaymentDate', $accountingEventsByMaxPaymentDate);

            $isNowCompany = $this->Company->isNow($company);
            $this->set('company', $company);
            $this->set('isNowCompany', $isNowCompany);
            $this->set('section', 'company_calendar_liquidations');
        }
    }

    function company_deals_by_max_payment_date($maxPaymentDate) {

        if (is_null($maxPaymentDate)) {
            $this->__redirectOnWrongAccess();
        }

        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $cond['conditions']['company_id'] =  $company['Company']['id'];
        $cond['conditions']['max_payment_date'] =  $maxPaymentDate;
        $cond['conditions']['pay_by_redeemed'] =  1;

        $accountingEventsByMaxPaymentDateRedeemed = $this->AccountingRedeemedGrouped->__findAllByCompanyIdGroupedByMaxPaymentDateRedeemed($cond);
        $cond['conditions']['pay_by_redeemed'] =  0;
        $accountingEventsByMaxPaymentDateNoRedeemed = $this->AccountingRedeemedGrouped->__findAllByCompanyIdGroupedByMaxPaymentDateRedeemed($cond);

        $accountingEventsByMaxPaymentDateGuaranteeFund = $this->AccountingGuaranteeFundGrouped->__findAllByCompanyIdGroupedByMaxPaymentDateGFund($cond);

        $this->set('redeemedByMaxDate', $accountingEventsByMaxPaymentDateRedeemed);
        $this->set('notRedeemedByMaxDate', $accountingEventsByMaxPaymentDateNoRedeemed);
        $this->set('guaranteeFundByMaxDate', $accountingEventsByMaxPaymentDateGuaranteeFund);
        $this->set('maxPaymentDate', $maxPaymentDate);
    }

    function company_deals_by_max_payment_date_and_mode($maxPaymentDate, $mode) {
        if (is_null($maxPaymentDate) || is_null($mode)) {
            $this->__redirectOnWrongAccess();
        }

        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $cond['conditions']['company_id'] =  $company['Company']['id'];
        $cond['conditions']['max_payment_date'] =  $maxPaymentDate;
        $accountingEvents = $this->AccountingDealIdGrouped->__findAllByCompanyIdGroupedByLiquidateIdAndMode($cond, $mode);
        
        if ($mode != 'Fondo') {
          $this->set('showOfferLink', true);
        } else {
          $this->set('showOfferLink', false);
        }

        $this->set('accountingEvents', $accountingEvents);
        $this->set('mode', $mode);
        $this->set('citySlug', $this->params['named']['city']);
    }
    
    function company_liquidation_details($liquidateId) {
        if (empty($liquidateId)) {
            $this->__redirectOnWrongAccess();
        }
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $campaignItemExtra = $this->AccountingCampaignItemExtra->findByCompanyIdAndLiquidateId($company['Company']['id'], $liquidateId);
        $campaignItemDetails = $this->AccountingCampaignItemDetail->findAllByAccountingItemId($campaignItemExtra['AccountingCampaignItemExtra']['bill_id']);
        $deal = $this->Deal->findById($campaignItemExtra['AccountingCampaignItemExtra']['deal_id']);
        if ($deal['Deal']['pay_by_redeemed']) {
          $this->set('showLiquidateGuaranteeFunds', false);
        } else {
          $this->set('showLiquidateGuaranteeFunds', true);
        }
        $isNowCompany = $this->Company->isNow($company);
        $branches = $this->Company->hasBranches($company);
        $this->set('dealAndAccountingItemDetails', $campaignItemExtra);
        $this->set('accountingItemsDetails', $campaignItemDetails);
        $this->set('company', $company);
        $this->set('isNowCompany', $isNowCompany);
        $this->set('branches', $branches);
    }

    function __setNextLiquidation() {
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $conditionsForFindByCompanyId['conditions']['company_id'] =  $company['Company']['id'];
        $nextLiq = $this->AccountingMaxPaymentDateGrouped->__findFirstByCompanyIdGroupedByMaxPaymentDate($conditionsForFindByCompanyId);
        $this->set('nextLiquidationDate', $nextLiq['AccountingMaxPaymentDateGrouped']['max_payment_date']);
        $this->set('nextLiquidationValue', $nextLiq['AccountingMaxPaymentDateGrouped']['liquidate_total_amount']);
    }

    function __setLayoutAccordingToView() {
        if (in_array($this->action, $this->blank_layouts_exceptions)) {
            $this->layout = 'bootstrap_blank';
        } else {
        	
        		$this->layout = 'bootstrap';
        
        	
        }
    }

    function __redirectOnWrongAccess() {
        $this->redirect(array(
            'controller' => 'dashboards',
            'action' => 'company_campaign_liquidations',
        ));
    }

    function __validateIfControllerIsAccesibleByUser() {
        if ($this->Auth->user('user_type_id') != ConstUserTypes::Company) {
            $this->cakeError('nowError404');
        }
    }
}
