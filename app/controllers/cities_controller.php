<?php

class CitiesController extends AppController {

    var $name = 'Cities';

    function beforeFilter() {
        AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }

    function index() {
        $this->paginate = array(
            'conditions' => array(
                'City.is_approved' => 1
            ),
            'fields' => array(
                'City.name',
                'City.slug',
                'City.active_deal_count'
            ),
            'order' => array(
                'City.name' => 'asc'
            ),
            'limit' => 200,
            'recursive' => - 1
        );
        $this->set('cities', $this->paginate());
    }

    function admin_index() {
        $this->_redirectGET2Named(array(
            'q'
        ));
        $this->disableCache();
        $this->pageTitle = __l('Cities');
        $conditions = array();
        $this->City->validate = array();
        if (!empty($this->params['named']['filter_id'])) {
            if ($this->params['named']['filter_id'] == ConstMoreAction::Active) {
                $this->pageTitle.= __l(' - Approved');
                $conditions[$this->modelClass . '.is_approved'] = 1;
            } else if ($this->params['named']['filter_id'] == ConstMoreAction::Inactive) {
                $this->pageTitle.= __l(' - Disapproved');
                $conditions[$this->modelClass . '.is_approved'] = 0;
            }
        }
        if (empty($this->data['City']['q']) && !empty($this->params['named']['q'])) {
            $this->data['City']['q'] = $this->params['named']['q'];
        }
        if (isset($this->data['City']['q']) && !empty($this->data['City']['q'])) {
            $this->params['named']['q'] = $this->data['City']['q'];
            $this->pageTitle.= sprintf(__l(' - Search - %s'), $this->data['City']['q']);
        }
        $this->City->recursive = 0;
        $this->paginate = array(
            'conditions' => $conditions,
            'fields' => array(
                'City.id',
                'City.name',
                'City.latitude',
                'City.longitude',
                'City.county',
                'City.code',
                'City.slug',
                'City.is_approved',
                'City.is_selectable',
                'City.is_group',
                'City.has_newsletter',
                'City.order',
                'City.is_corporate',
                'City.is_hidden',
                'City.is_searchable',
                'City.week_days',
                'State.name',
                'Country.name',
                'Language.name'
            ),
            'order' => array(
                'City.name' => 'asc'
            )
        );
        if (isset($this->data['City']['q'])) {
            $this->paginate['search'] = $this->data['City']['q'];
        }
        $this->set('cities', $this->paginate());
        $this->set('pending', $this->City->find('count', array(
                    'conditions' => array(
                        'City.is_approved = ' => 0
                    )
        )));
        $this->set('approved', $this->City->find('count', array(
                    'conditions' => array(
                        'City.is_approved = ' => 1
                    )
        )));
        $filters = $this->City->isFilterOptions;
        $moreActions = $this->City->moreActions;
        $this->set(compact('filters', 'moreActions'));
        $this->set('pageTitle', $this->pageTitle);
    }

    function admin_update_twitter($id = null) {
        $this->pageTitle = __l('Update Twitter');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        App::import('Component', 'OauthConsumer');
        $this->OauthConsumer = & new OauthConsumerComponent();
        $requestToken = $this->OauthConsumer->getRequestToken('Twitter', 'http://twitter.com/oauth/request_token');
        $this->Session->write('requestToken', $requestToken);
        $this->Session->write('twitter_city_id', $id);
        $this->redirect('http://twitter.com/oauth/authorize?oauth_token=' . $requestToken->key);
    }

    function admin_edit($id = null) {
        $this->pageTitle = 'Editar ciudad';
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $defaultCity = $this->City->find('first', array(
            'conditions' => array(
                'City.slug' => Configure::read('site.city')
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => - 1
        ));
        if (!empty($defaultCity) && $id == $defaultCity['City']['id']) {
            $this->set('id_default_city', true);
        }
        if (!empty($this->data)) {
            if ($this->data['City']['image']['size']) {
                $this->data['City']['image'] = $this->__upload($this->data['City']['image']);
                if (!$this->data['City']['image']) {
                    $this->City->invalidate('image', 'Solo se pueden subir imágenes de hasta 72px de alto');
                    $this->Session->setFlash('Solo se pueden subir im&aacute;genes de hasta 72px de alto', true);
                    $this->render();
                }
            } else {
                $this->data['City']['image'] = $this->data['City']['stored_image'];
            }
            if ($this->City->save($this->data)) {
                $this->Session->setFlash(__l('City has been updated'), 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__l('City could not be updated. Please, try again.'), 'default', null, 'error');
            }
        } else {
            $this->data = $this->City->find('first', array(
                'conditions' => array(
                    'City.id' => $id
                ),
                'recursive' => - 1
            ));
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
            $this->data['City']['stored_image'] = $this->data['City']['image'];
        }
        $this->pageTitle.= ' - ' . $this->data['City']['name'];
        $countries = $this->City->Country->find('list');
        $states = $this->City->State->find('list', array(
            'conditions' => array(
                'State.is_approved' => 1
            )
        ));
        App::import('Model', 'Translation');
        $this->Translation = new Translation();
        $languages = $this->Translation->get_languages();
        $this->set(compact('countries', 'states', 'languages'));
    }

    function admin_add() {
        $this->pageTitle = 'A&ntilde;adir ciudad';
        if (!empty($this->data)) {
            if ($this->data['City']['image']['size']) {
                $this->data['City']['image'] = $this->__upload($this->data['City']['image']);
                if (!$this->data['City']['image']) {
                    $this->City->invalidate('image', 'Solo se pueden subir imágenes, de 90px de ancho y 72px de alto');
                    $this->Session->setFlash('Solo se pueden subir im&aacute;genes, de 90px de ancho y 72px de alto', true);
                    $this->render();
                }
            } else {
                $this->data['City']['image'] = '';
            }
            $this->data['City']['is_approved'] = 1;
            $this->City->create();
            if ($this->City->save($this->data)) {
                $this->Session->setFlash('La ciudad ha sido creada', 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash('No se pudo crear la ciudad', 'default', null, 'error');
            }
        } else {
            $this->data['City']['is_approved'] = 1;
        }
        $countries = $this->City->Country->find('list');
        $states = $this->City->State->find('list', array(
            'conditions' => array(
                'State.is_approved =' => 1
            ),
            'order' => array(
                'State.name'
            )
        ));
        App::import('Model', 'Translation');
        $this->Translation = new Translation();
        $languages = $this->Translation->get_languages();
        $this->set(compact('countries', 'states', 'languages'));
    }

    function admin_update_status($id = null, $status = null) {
        if (is_null($id) || is_null($status)) {
            $this->cakeError('error404');
        }
        $this->data['City']['id'] = $id;
        if ($status == 'disapprove') {
            $this->data['City']['is_approved'] = 0;
        }
        if ($status == 'approve') {
            $this->data['City']['is_approved'] = 1;
        }
        $this->City->save($this->data);
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    function admin_update() {
        $this->autoRender = false;
        if (!empty($this->data['City'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['redirect_url']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $cityIds = array();
            foreach ($this->data['City'] as $city_id => $is_checked) {
                if ($is_checked['id']) {
                    $cityIds[] = $city_id;
                }
            }
            if ($actionid && !empty($cityIds)) {
                if ($actionid == ConstMoreAction::Inactive) {
                    $this->City->updateAll(array(
                        'City.is_approved' => 0
                            ), array(
                        'City.id' => $cityIds
                    ));
                    $this->Session->setFlash(__l('Selected cities has been inactivated'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Active) {
                    $this->City->updateAll(array(
                        'City.is_approved' => 1
                            ), array(
                        'City.id' => $cityIds
                    ));
                    $this->Session->setFlash(__l('Selected cities has been activated'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Delete) {
                    $defaultCity = $this->City->find('first', array(
                        'conditions' => array(
                            'City.slug' => Configure::read('site.city')
                        ),
                        'fields' => array(
                            'City.id'
                        ),
                        'recursive' => - 1
                    ));
                    $this->City->deleteAll(array(
                        'City.id' => $cityIds,
                        'City.slug !=' => Configure::read('site.city')
                    ));
                    $msg = __l('Selected cities has been deleted');
                    if (!empty($defaultCity) && in_array($defaultCity['City']['id'], $cityIds)) {
                        if (count($cityIds) == 1) {
                            $this->Session->setFlash(__l('You can not delete the default city. Please update default city from settings and try again.'), 'default', null, 'error');
                            $msg = '';
                        } else {
                            $msg.= ' ' . __l('except the default city. Please update default city from settings and try again.');
                        }
                    }
                    if (!empty($msg))
                        $this->Session->setFlash($msg, 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function admin_delete($id = null) {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $defaultCity = $this->City->find('first', array(
            'conditions' => array(
                'City.slug' => Configure::read('site.city')
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => - 1
        ));
        if (!empty($defaultCity) && $id == $defaultCity['City']['id']) {
            $this->Session->setFlash(__l('You can not delete the default city. Please update default city from settings and try again.'), 'default', null, 'error');
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->City->del($id)) {
            $this->Session->setFlash(__l('City deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function __upload($file_array, $targetDir = '/img/cities/') {
        $imageFile = new File($file_array['tmp_name']);
        $fileBase = pathinfo($file_array['name']);
        if ($this->_areImageDimensionsCorrect($imageFile) && $this->_isCorrectFileExtension($fileBase)) {
            $ext = strtolower($fileBase['extension']);
            $name = $this->data['City']['slug'];
            $data = $imageFile->read();
            $imageFile->close();
            $rndmSeed = $this->__str_rand(10);
            $cleanName = $this->__cleanChars($name);
            $full_path = $targetDir . "city_" . $cleanName . "-" . $rndmSeed . "." . $ext;
            $imageFile = new File(WWW_ROOT . $full_path, true);
            $imageFile->write($data);
            $imageFile->close();
            return $full_path;
        } else {
            return false;
        }
    }

    function _areImageDimensionsCorrect($imageFile) {
        $allowedImageDimensions = Configure::read('Site.city_logo');
        $allowedHeight = $allowedImageDimensions['height'];
        list($width, $height) = getimagesize($imageFile->path);
        return ($height <= $allowedHeight);
    }

    function _isCorrectFileExtension($fileBase) {
        $ext = strtolower($fileBase['extension']);
        return (!empty($ext) && in_array($ext, Configure::read('image.file.allowedExt')));
    }

    function __str_rand($length = 8) {
        $output = 'abcdefghijklmnopqrstuvwqyz0123456789';
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str.= $output[mt_rand(0, strlen($output) - 1)];
        }
        return $str;
    }

    function __cleanChars($string) {
        $string = str_replace(array(
            ' ',
            '-'
                ), array(
            '_',
            '_'
                ), $string);
        $string = str_replace(array(
            'á',
            'é',
            'í',
            'ó',
            'ú',
            'Ã ',
            'è',
            'ì',
            'ò',
            'ù',
            'ä',
            'ë',
            'ï',
            'ö',
            'ü',
            'Á',
            'Ã‰',
            'Í',
            'Ã“',
            'Ãš',
            'Ã€',
            'Ãˆ',
            'ÃŒ',
            'Ã’',
            'Ã™',
            'Ã„',
            'Ã‹',
            'Ï',
            'Ã–',
            'Ãœ'
                ), array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'a',
            'e',
            'i',
            'o',
            'u',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U',
            'A',
            'E',
            'I',
            'O',
            'U'
                ), $string);
        return $string;
    }

}
