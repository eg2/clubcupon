<?php
class ActionPointsController extends AppController
  {
    var $name = 'ActionPoints';

    /*
    function index ()
      {
        App::import ('Model', 'User');
        $this->User = new User ();
        App::import ('Model', 'Action');
        $this->Action = new Action ();
        App::import ('Component', 'Cron');
        $this->Cron = new CronComponent ();


        // var_dump ($this->User->add_points (1509, 2));
        // var_dump ($this->User->debit_points (1509, 16078, 5));
        // var_dump ($this->User->refund_points (16078));
        // var_dump ($this->User->get_current_points (1509));
        // var_dump ($this->Cron->points_expiring_soon ());

        die;
      }
    */


    function admin_index ()
      {
        // Action es necesario, aunque no sea un modelo asociado, importarlo e instanciarlo.
        App::import ('Model', 'Action');
        $this->Action = new Action ();

        // Setear el título de la página.
        $this->pageTitle = 'Acciones';

        // Buscar todas las acciones, y para cada una buscar el ActionPoint más nuevo.
        $as = $this->Action->find ('all', array ('conditions' => array ('NOT' => array ('Action.id' => array (ConstActionIds::DebitCredit))), 'recursive' => -1, 'order' => 'Action.name'));
        for ($i = 0; $i < count ($as); $i++)
          {
            $ap = $this->ActionPoint->find ('first', array ('recursive' => -1, 'conditions' => array ('ActionPoint.action_id' => $as [$i]['Action']['id']), 'order' => array ('ActionPoint.created' => 'desc')));
            $as [$i]['ActionPoint'] = $ap ['ActionPoint'];
          }

        // Pasar la matriz "actions" a la view.
        $this->set ('actions', $as);
      }


    function admin_edit ($id = null)
      {
        // Action es necesario, aunque no sea un modelo asociado, importarlo e instanciarlo.
        App::import ('Model', 'Action');
        $this->Action = new Action ();

        // Indicamos que por defecto utilice la conexion master a la DB
        AppModel::setDefaultDbConnection ('master');

        // Setear el título
        $this->pageTitle = 'Editar Acci&oacute;n';

        // Buscar ID y acción en cuestión, fallar en caso de no poderse
        if (is_null ($id))  $this->cakeError ('error404');
        $id = !empty ($id) ? $id : $this->data ['ActionPoint']['id'];
        $action = $this->ActionPoint->find ('first', array ('conditions' => array ('ActionPoint.id' => $id, 'NOT' => array ('ActionPoint.action_id' => array(ConstActionIds::DebitCredit))), 'recursive' => -1));
        if (empty ($action))  $this->cakeError ('error404');

        // Dependiendo de si es la primera llamada o la segunda (la que tiene datos)...
        if (!empty ($this->data))
          // Validar y guardar, si se falla, fallar
          if ($this->ActionPoint->validates ())
            {
              $this->ActionPoint->create (); unset ($this->data ['ActionPoint']['id']);
              if ($this->ActionPoint->save ($this->data))
                if ($this->Action->save ($this->data))
                  {
                    $this->Session->setFlash ('La Acci&oacute;n fue modificada', 'default', null, 'success');
                    $this->redirect (array ('controller' => 'action_points', 'action' => 'index', 'admin' => 'true'));
                  }
                else
                  {
                    $this->ActionPoint->delete ($this->ActionPoint->id);
                    $this->Session->setFlash ('La Acci&oacute;n no se pudo modificar, por favor, int&eacute;ntelo nuevamente.', 'default', null, 'error');
                  }
              else  $this->Session->setFlash ('La Acci&oacute;n no se pudo modificar, por favor, int&eacute;ntelo nuevamente.', 'default', null, 'error');
            }
          else  $this->Session->setFlash ('La Acci&oacute;n no se pudo modificar, por favor, int&eacute;ntelo nuevamente.', 'default', null, 'error');
        else
          {
            // Buscar la Action asociada a la ID obtenida y su ActionPoint más reciente, fallar en caso de que no se encuentre
            $this->data = $this->Action->find ('first', array ('conditions' => array ('Action.id' => $id), 'recursive' => -1));
            $ap = $this->ActionPoint->find ('first', array ('recursive' => -1, 'conditions' => array ('ActionPoint.action_id' => $this->data ['Action']['id'], 'NOT' => array('ActionPoint.action_id' => array(ConstActionIds::DebitCredit))), 'order' => array ('ActionPoint.created' => 'desc')));
            $this->data ['ActionPoint'] = $ap ['ActionPoint'];
            if (empty ($this->data))  $this->cakeError ('error404');
          }

        return;
      }
  }
?>