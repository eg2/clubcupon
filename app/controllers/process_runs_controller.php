<?php
class ProcessRunsController extends AppController
  {
    var $name = 'ProcessRuns';

    function admin_lock_crons () {
      $selfModel = $this->ProcessRun;
      $dataSource = $selfModel->getDataSource ();

      if (!$this->ProcessRun->isRunningByName ('LOCK')) {
        $dataSource->begin ($selfModel);
        $this->ProcessRun->startRunning ('LOCK');
        $dataSource->commit ($selfModel);
      }
      $this->redirect (array ('controller' => 'deals', 'action' => 'index', 'admin' => true));
    }

    function admin_unlock_crons () {
      $selfModel = $this->ProcessRun;
      $dataSource = $selfModel->getDataSource ();

      $processRunStatus = $this->ProcessRun->getRunningByName ('LOCK');
      if ($processRunStatus !== false && $processRunStatus ['is_running']) {
        $dataSource->begin ($selfModel);
        $this->ProcessRun->stopRunning ($processRunStatus ['id']);
        $dataSource->commit ($selfModel);
      }
      $this->redirect (array ('controller' => 'deals', 'action' => 'index', 'admin' => true));
    }
  }
?>