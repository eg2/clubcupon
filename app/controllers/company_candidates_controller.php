<?php
class CompanyCandidatesController extends AppController {

  var $name = 'CompanyCandidates';
  var $validatePost = false;

  var $components = array(
      'Email',
      'now.Mail',
  );

  var $uses = array(
      'CompanyCandidate',
      'Company',
	  'User',
	  'Country',
	  'State',
	  'City',
      'EmailTemplate',
  );

  function beforeFilter() {
    $this->Security->disabledFields = array(
        'City',
        'FiscalCity',
        'FiscalState',
        'State',
        'Company.latitude',
        'Company.longitude',
        'UserAvatar.filename',
        'User.id',
        'Company.id',
        'Company.address1',
        'Company.address2',
        'Company.country_id',
        'Company.name',
        'Company.phone',
        'Company.url',
        'Company.zip',
        'Company.fiscal_name',
        'Company.fiscal_address',
        'Company.fiscal_phone',
        'Company.contact_phone',
        'Company.fiscal_fax',
        'Company.fiscal_bank',
        'Company.fiscal_bank_account',
        'Company.fiscal_bank_cbu',
        'Company.fiscal_cond_iva',
        'Company.fiscal_cuit',
        'Company.fiscal_iibb',
        'Company.percepcion_iibb_caba',
        'Company.declaracion_jurada_terceros',
        'Company.persona',
        'Company.cheque_noorden'
    );
    // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');


	  $securityDisabled = array('admin_details');
		if (in_array($this->action, $securityDisabled)) {
			$this->Security->validatePost = false;
		}

    parent::beforeFilter();
  }

    function admin_reindex()
    {
        $this->CompanyCandidate->reindexAll(0);
        echo'Index OK';
        die();
    }

    function admin_search()
    {

    	$this->pageTitle = 'Resultados para: '. @$this->data['CompanyCandidate']['q'];

        if (isset($this->data['CompanyCandidate']['q']))
        {
            $conditions=array('state =' => $this->data['CompanyCandidate']['selected']);
            $ids = $this->CompanyCandidate->search($this->data['CompanyCandidate']['q'], array('fields'=> array('CompanyCandidate.id'), 'conditions' => $conditions));

            $newIds = array();
            if(isset($ids))
            {
                foreach ($ids as $i => $value)
                {
                    $newIds[]=$value[CompanyCandidate][id];
                }
            }

            $candidates =  $this->CompanyCandidate->findByIds($newIds);

            $this->set('selected', $this->data['CompanyCandidate']['selected']);
            $this->set('q',        $this->data['CompanyCandidate']['q']);
            $this->set('state',    $this->data['CompanyCandidate']['selected']);
            $this->set('company_candidates',$candidates);

        }

    }

    function admin_index()
    {
        $this->pageTitle = 'Empresas';
        $state = isset($this->params['named']['state'])?$this->params['named']['state']:1;
        $conditions=array('state =' => $state);

        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array(
                'CompanyCandidate.modified' => 'desc'
            ),
        );

        $this->set('selected',  $state);
        $this->set('state',     $state);
        $this->set('can_approve', $this->CompanyCandidate->canApproveIfState($state));
        $this->set('can_rejected', $this->CompanyCandidate->canRejectedIfState($state));
        $this->set('can_draft', $this->CompanyCandidate->canPassToDraftIfState($state));
        $this->set('can_pending', $this->CompanyCandidate->canPassToPendingIfState($state));
        $this->set('can_edited', $this->CompanyCandidate->canEditedIfState($state));
        $this->set('company_candidates', $this->paginate());

    }

    function admin_details($id){

      if(!empty($this->data)) {

        $ComCan = $this->CompanyCandidate->findById($id);
        $this->CompanyCandidate->set($this->data);
        $this->CompanyCandidate->id = $ComCan['CompanyCandidate']['id'];
        $this->data['CompanyCandidate']['id']= $id;

		$user_id = $ComCan['CompanyCandidate']['user_id'];
		$CompMediaDir = APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR;

		// FILE UPLOAD!!
		if(is_uploaded_file($this->data['CompanyCandidate']['extban']['tmp_name'])){
			$fdata = pathinfo($this->data['CompanyCandidate']['extban']['name']);
			if(in_array(strtoupper($fdata['extension']),ConstCandidate::allowedFiles())){
				foreach (glob($CompMediaDir.$user_id.DIRECTORY_SEPARATOR."extban_*") as $nombre_archivo){ unlink($nombre_archivo);}
				$fileData = fread(fopen($this->data['CompanyCandidate']['extban']['tmp_name'], "r"),$this->data['CompanyCandidate']['extban']['size']);
				file_put_contents($CompMediaDir.$user_id.DIRECTORY_SEPARATOR."extban_".$this->data['CompanyCandidate']['extban']['name'],$fileData);
			}else{
				$extban_e = true; //ERROR TIPO DE ARCHIVO NO PERMITIDO! IGNORADO, SI LO PIDEN USAR ESTA VARIABLE!
			}
		}

		if(is_uploaded_file($this->data['CompanyCandidate']['encfac']['tmp_name'])){
			$fdata = pathinfo($this->data['CompanyCandidate']['encfac']['name']);
			if(in_array(strtoupper($fdata['extension']),ConstCandidate::allowedFiles())){
				foreach (glob($CompMediaDir.$user_id.DIRECTORY_SEPARATOR."encfac_*") as $nombre_archivo){ unlink($nombre_archivo);}
				$fileData = fread(fopen($this->data['CompanyCandidate']['encfac']['tmp_name'], "r"),$this->data['CompanyCandidate']['encfac']['size']);
				file_put_contents($CompMediaDir.$user_id.DIRECTORY_SEPARATOR."encfac_".$this->data['CompanyCandidate']['encfac']['name'],$fileData);
			}else{
				$encfac_e = true; //ERROR TIPO DE ARCHIVO NO PERMITIDO! IGNORADO, SI LO PIDEN USAR ESTA VARIABLE!
			}
		}
		// END FILE UPLOAD!!

        if ($this->CompanyCandidate->validates()) {
          if($this->CompanyCandidate->save($this->data)){
			  if($_POST['SaveOption'] == "Rechazar"){
				$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "reject", "id"=>$id), true));
			  }elseif($_POST['SaveOption'] == "Borrador"){
				$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "reject", "id"=>$id), true));
			  }elseif($_POST['SaveOption'] == "Aprobar"){
				$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "aprove", "id"=>$id), true));
			  }elseif($_POST['SaveOption'] == "Guardar"){
				$this->Session->setFlash('Los cambios se guardaron correctamente!', 'default', null, 'success');
				$user = $this->User->find('first', array(
						'conditions' => array(
							'User.id' => $ComCan['CompanyCandidate']['user_id'],
						),
						'recursive' => -1
							));
					$array = array('user' => $user);
				$this->Mail->sendCandidateEditMail($array);
				$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "index"	), true));
			  }
		  }else{
            $this->Session->setFlash('Hubo un problema al guardar por favor revise el formulario e intente nuevamente!', 'default', null, 'error');
          }
        }
      }else{
        $this->data = $this->CompanyCandidate->findById($id);
      }

      $countries = $this->Country->find('list');
      $fiscal_states = $states = $this->State->find('list');
      $fiscal_cities = $cities = $this->City->find('list');
      $this->set(compact('countries', 'states', 'cities','fiscal_states', 'fiscal_cities'));

      $this->set('state', $this->data['CompanyCandidate']['state']);
      $this->set('can_draft', $this->CompanyCandidate->canPassToDraft($this->data));
      $this->set('can_approve', $this->CompanyCandidate->canApprove($this->data));
      $this->set('can_rejected', $this->CompanyCandidate->canRejected($this->data));
    }

    function admin_pending($id) {
        $this->data =  $this->CompanyCandidate->findById($id);
        $this->data['CompanyCandidate']['state'] = ConstCandidateStatuses::Pending;
        if($this->CompanyCandidate->save($this->data, false)){
          $this->Session->setFlash('La empresa fue pasada a pendiente!', 'default', null, 'success');
        } else {
          $this->Session->setFlash('Hubo un problema al interar pasar a pendiente la empresa.', 'default', null, 'error');
        }
        $this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "index"	), true));
    }

    function admin_reject($id, $toDraft=false){
      $toDraft = isset($this->params ['named']['toDraft'])  ? $this->params ['named']['toDraft']: $toDraft;
      if(!empty($this->data)) {
        $ComCan = $this->CompanyCandidate->findById($id);
        $this->CompanyCandidate->id = $ComCan['CompanyCandidate']['id'];
        if($this->params['form']['SaveOption'] == 'Borrador') {
          $state_id = ConstCandidateStatuses::Draft;
          $subject = 'Algunos de los datos de su compania nesecitan correccion y o actualizacion!';
        } elseif($this->params['form']['SaveOption'] == 'Rechazar') {
          $state_id = ConstCandidateStatuses::Reject;
          $subject = 'Su solicitud de alta ha sido rechazada!';
        } else {
          $this->Session->setFlash('Hubo un problema, por favor revise el formulario e intente nuevamente!', 'default', null, 'error');
          $this->render('admin_reject');
          return;
        }

        $this->data['CompanyCandidate']['state'] = $state_id;
        $this->Email->subject = $subject;
        if($this->CompanyCandidate->save($this->data, false)) {
          $this->Email->subject = $subject;
          $this->_sendAproveOrRejectedOrDraft($ComCan['CompanyCandidate']['user_id'],1,$this->data['CompanyCandidate']['message'], $state_id == ConstCandidateStatuses::Draft);
          $this->Session->setFlash('Mensaje enviado al Usuario!!', 'default', null, 'seccess');
          $this->data = $this->CompanyCandidate->findById($id);
          $this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "index"), true));
        }else{
          $this->Session->setFlash('Error al guardar!!', 'default', null, 'error');
        }

      }
      $this->data = $this->CompanyCandidate->findById($id);

      $this->set('can_draft', $toDraft);
      $this->set('can_rejected', !$toDraft);
    }

  function admin_aprove($id){
		//$this->data = $this->CompanyCandidate->findById($this->params['named']['candidate']);
		$this->data = $this->CompanyCandidate->findById($id);
		$this->data['Company'] = $this->data['CompanyCandidate'];
		$this->data['Company']['slug'] = Inflector::slug($this->data['Company']['name']);
		$this->data['Company']['id'] = false;
		$this->data['Company']['now_eula_ok'] = 1;
		$this->data['Company']['validated'] = 1;
                $this->data['Company']['has_posnet_device'] = 1;

        $this->data['Company']['deal_commission_percentage'] = Configure::read ('NowPlugin.deal.commission_percentage');
		$this->data['Company']['country_id'] = $this->Country->findCountryIdFromIso2('AR');
		$this->Company->set($this->data);
		if($this->Company->validates()){
			if($this->Company->save($this->data)){
        $company_id = $this->Company->getLastInsertId ();
        $this->CompanyCandidate->set($this->data);
        $this->CompanyCandidate->saveField('company_id', $company_id);
        $this->CompanyCandidate->saveField('state', ConstCandidateStatuses::Approved);
				$this->_sendAproveOrRejectedOrDraft($this->data['User']['id']);
				$this->Session->setFlash('Se aprob&oacute; la empresa !', 'default', null, 'success');
				//$this->CompanyCandidate->delete($this->params['named']['candidate'], false);
				$this->CompanyCandidate->delete($id, false);
			}else{
				$this->Session->setFlash('Error al Guardar!!', 'default', null, 'error');
			}
		}else{
			$this->Session->setFlash('Error al validar el Formulario!!', 'default', null, 'error');
			$this->set('errores',$this->Company->validationErrors);
			//$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "details", "id"=>$id), true));
			$this->render('admin_details');
			return;
		}
		$this->redirect(Router::url(array("controller" => "CompanyCandidates", "action" => "index"), true));
  }

  function admin_getfile(){
	$user = $this->params['named']['user'];
	$file = $this->params['named']['file'];
	$CompMediaDir = APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR;
	$dirfile = $CompMediaDir.$user.DIRECTORY_SEPARATOR.$file;

	if (file_exists($dirfile)) {
		header('Content-Description: File Transfer');
		header('Content-Type: binary/octet-stream');
		header('Content-Disposition: attachment; filename="'.$file.'"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($dirfile));
		ob_clean();
		flush();
		readfile($dirfile);
		exit;
	}else{
		die("No file found!");
	}
  }

  function admin_delfile(){
	$user = $this->params['named']['user'];
	$file = $this->params['named']['file'];
	$CompMediaDir = APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR;
	$dirfile = $CompMediaDir.$user.DIRECTORY_SEPARATOR.$file;

	if (file_exists($dirfile)) {
		unlink($dirfile);
		$this->Session->setFlash('Borrado Correctamente!', 'default', null, 'success');
	}else{
		$this->Session->setFlash('No existe el archivo!', 'default', null, 'error');
	}
	$this->redirect(array('action' => 'index'));
  }

  	  function _sendAproveOrRejectedOrDraft($user_id, $action = 0, $message = "", $is_draft=false) {


        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'recursive' => -1
                ));

        if($is_draft)
        {

            return $this->Mail->sendCandidateDraftMail(array(
                    'user' => $user,
                    'message' => $message,
                ));
        }
        else
        {

                    return $this->Mail->sendCandidateAproveOrRejectedMail(array(
                    'user' => $user,
                    'action' => $action,
                    'message' => $message,
                ));
        }



    }


}
?>
