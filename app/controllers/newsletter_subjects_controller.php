<?php

class NewsletterSubjectsController extends AppController {

    var $name = 'NewsletterSubjects';
    var $components = array();
    
    var $paginate = array(
        'limit' => 20,
        'conditions' => array( 'deleted' => 0 ),
        'order' => array('scheduled' => 'DESC'),
        );

    function admin_index() {
        
        $this->pageTitle = 'Newsletter subjects';
        $this->NewsletterSubject->recursive = 0;
        
        if(!empty($this->data['NewsletterSubject']['date'])){
            $conditionsForSearch['NewsletterSubject.scheduled'] = $this->data['NewsletterSubject']['date'];
        }
        
        if(!empty($this->data['NewsletterSubject']['city'])){
            $conditionsForSearch['NewsletterSubject.city_id'] = $this->data['NewsletterSubject']['city'];
        }
        $this->paginate = array(
            'conditions' => $conditionsForSearch,
        );
        
        
        $selectOptionsForCity = $this->NewsletterSubject->City->findApprovedCitiesAndApprovedGroupsList();
        $newsletterSubjects = $this->paginate();
        
        $this->set('selectOptionsForCity', $selectOptionsForCity);
        $this->set('newsletterSubjects', $newsletterSubjects);
        
    }
    
    function admin_add() {
        
        if (!empty($this->data)) {
            $this->data['NewsletterSubject']['created_by'] = $this->Auth->user('id');
            if ($this->NewsletterSubject->save($this->data)) {
                $this->Session->setFlash('Registro guardado', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('No se pudo guardar el registro', true);
            }
            
        }
        
        $selectOptionsForCity = $this->NewsletterSubject->City->findApprovedCitiesAndApprovedGroupsList();
        $this->set('selectOptionsForCity', $selectOptionsForCity);
        
    }
    function admin_edit($id = null) {
        
        if (!empty($this->data)) {
            
            $this->data['NewsletterSubject']['modified_by'] = $this->Auth->user('id');
            $this->NewsletterSubject->id = $id;
            if ($this->NewsletterSubject->save($this->data)) {
                $this->Session->setFlash('Registro actualizado', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('No se pudo actualizar el registro', true);
            }
        } else {
			$this->data = $this->NewsletterSubject->read(null, $id);
        }
        
        $selectOptionsForCity = $this->NewsletterSubject->City->findApprovedCitiesAndApprovedGroupsList();
        $this->set('selectOptionsForCity', $selectOptionsForCity);
        
    }
    
    function admin_delete($id = null) {
        if(!empty($id)){
            $this->autoRender = false;
            $this->NewsletterSubject->del($id);
            $this->Session->setFlash('Registro eliminado', true);
        } else {
            $this->Session->setFlash('No se ha especificado el resgistro a eliminar', true);
        }
        $this->redirect(array('action' => 'index'));
    }
    
    function admin_undelete($id = null) {
        if(!empty($id)){
            $this->autoRender = false;
            $this->NewsletterSubject->undelete($id);
            $this->Session->setFlash('Registro restaurado', true);
        } else {
            $this->Session->setFlash('No se ha especificado el resgistro a restaurar', true);
        }
        $this->redirect(array('action' => 'index'));
    }

}