<?php

App::import('Component', 'api.ApiSubscriptionsService');

class DealsController extends AppCachedController {

    var $name = 'Deals';
    var $components = array(
        'Email',
        'Paypal',
        'TextUtil',
        'GiftPinService',
        'api.ApiLogger',
        'Bac',
        'SecondaryDeals',
        'Product.ProductInventoryService',
        'offer.OfferService'
    );
    var $helpers = array(
        'Csv',
        'Gateway',
        'adminpagos',
        'BacFormat',
        'Tip',
        'Time'
    );
    var $uses = array(
        'Deal',
        'Company',
        'DealUser',
        'DealExternal',
        'DealCategory',
        'Subscription',
        'Attachment',
        'AttachmentMultiple',
        'EmailTemplate',
        'GoogleAdwordsConversion',
        'now.NowDeal',
        'now.NowBranch',
        'now.NowBranchesDeals',
        'now.NowScheduledDeals',
        'CorporateGuest',
        'UserProfile',
        'User',
        'PaymentPlanItem',
        'efg.EfgSegmentationProfile',
        'DealFlatCategory',
        'Product.ProductInventoryStrategy',
        'Product.ProductProduct',
        'DealTradeAgreement',
        'shipping.ShippingAddressDeal'
    );
    var $cache_views = array(
        'index',
        'view'
    );
    var $_bypass_cache_for_logged_users = true;
    var $_cache_lifetime = 120;
    public $logFileNamePrefix = 'api';

    function beforeFilter() {
        $this->Security->requireAuth('add', 'admin_add');
        if ($this->action == 'updateDealExternalPaymentType')
            $this->Security->validatePost = false;
        if (in_array($this->action, array(
                    'add',
                    'admin_add',
                    'admin_edit',
                    'admin_audit_now_deal',
                    'email_preview'
                )))
            $this->Security->enabled = false;
        $this->Security->disabledFields = array(
            'Attachment',
            'AttachmentMultiple',
            'Deal.calculator_min_limit',
            'Deal.calculator_discounted_price',
            'Deal.calculator_bonus_amount',
            'Deal.calculator_commission_percentage',
            'Deal.calculator_bonus_amount',
            'Deal.calculator_commission_percentage',
            'Deal.start_date',
            'Deal.end_date',
            'Deal.coupon_expiry_date',
            'Deal.user_id',
            'Deal.city_id',
            'Deal.cluster_id',
            'Deal.company_id',
            'Deal.bonus_amount',
            'Deal.commission_percentage',
            'Deal.total_commission_amount',
            'DealStatus.id',
            'DealStatus.name',
            'Deal.discount_amount',
            'Deal.discount_percentage',
            'Deal.discounted_price',
            'Deal.meta_description',
            'Deal.meta_keywords',
            'Deal.original_price',
            'Deal.savings',
            'Deal.id',
            'Deal.save_as_draft',
            'Deal.send_to_admin',
            'Deal.buy_max_quantity_per_user',
            'Deal.buy_min_quantity_per_user',
            'Deal.gift_email',
            'Deal.gift_from',
            'Deal.gift_to',
            'Deal.message',
            'Deal.quantity',
            'Deal.message',
            'Deal.deal_amount',
            'Deal.deal_id',
            'Deal.is_gift',
            'User.confirm_password',
            'User.email',
            'User.passwd',
            'User.username',
            'UserProfile.dni',
            'Deal.user_available_balance',
            'Deal.gift_to',
            'Deal.portal',
            'User.fwd',
            'Deal.cantidad_cuotas',
            'gift_pin_code',
            'gift_monto'
        );
        if (@$this->params['prefix'] == 'admin') {
            $this->Deal->Behaviors->disable('SmsExcludable');
        }
        $this->set('is_corporate_city', 0);
        $this->set('is_corporate_guest', 0);
        $corporateRestictedActions = array(
            'buy',
            'buy_without_subdeal'
        );
        if ($this->Deal->City->findIfIsHiddenCityBySlug($this->params['named']['city'])) {
            $this->set('is_hidden_city', 1);
        }
        if ($this->Deal->City->isCorporate($this->params['named']['city']) && $this->params['prefix'] != 'admin') {
            $this->set('is_corporate_city', 1);
            $corporateCity = $this->Deal->City->findBySlug($this->params['named']['city']);
            $this->set('corporateCity', $corporateCity);
            if (!$this->Cookie->read('is_corporate_guest') && ($this->params['url']['is_corporate_guest'] || $this->_isLogginUserCorporate())) {
                $this->Cookie->domain = Router::url($this->here, true);
                $this->Cookie->write('is_corporate_guest', 1);
            }
            $this->set('is_corporate_guest', $this->Cookie->read('is_corporate_guest'));
            if (!$this->Cookie->read('is_corporate_guest') && in_array($this->params['action'], $corporateRestictedActions)) {
                $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
                ));
            }
        }
        $excludedCitySlugs = array(
            'currentCitySlug' => $this->params['named']['city'],
            'lastCitySlug' => $this->Session->read('lastCitySlug')
        );
        $secondaryDeals = $this->SecondaryDeals->secondaryDeals($excludedCitySlugs);
        $this->set('secondaryDeals', $secondaryDeals);
        parent::beforeFilter();
    }

    function landingpre($slug = '', $kw = '') {
        $this->apiSubscription = new ApiSubscriptionsServiceComponent();
        $this->set('isCaptchaNecesary', $this->apiSubscription->isCaptchaNecessary($this->RequestHandler->getClientIP()));
        $this->set('certificaPath', 'ClubCupon/landing');
        $slug = strtolower($slug);
        switch ($slug) {
            case 'belleza':
            case 'gastronomia':
            case 'objetos':
            case 'entretenimiento':
                $this->set('kw', $kw);
                $this->layout = 'landing-full';
                $this->render($slug);
                break;

            case 'full':
                $this->layout = 'landing-full-full';
                $this->render($slug);
                break;

            case 'ofertas-en-belleza':
            case 'ofertas-en-entretenimiento':
            case 'ofertas-en-escapadas':
            case 'ofertas-en-fotografia':
            case 'ofertas-en-gastronomia':
            case 'ofertas-en-indumentaria':
            case 'ofertas-en-indumentaria-generica':
            case 'ofertas-en-regalos':
            case 'ofertas-en-salidas':
            case 'ofertas-en-servicios':
            case 'ofertas-en-turismo':
                $this->layout = 'landing-full-mailing';
                $this->render($slug);
                break;

            case 'vendedores':
                $this->layout = 'blank';
                $this->render($slug);
                break;

            default:
                $this->layout = 'pre';
        }
    }

    function index($city_slug = null) {
        $setRecentTitle = false;
        $this->set('is_deal_view', true);
        $this->set('certificaPath', 'ClubCupon/index');
        $limit = 20;
        if (!empty($city_slug)) {
            $this->Cookie->write('city_slug', $city_slug);
            $wCity = $this->WebCity->findBySlug($city_slug);
            if ($wCity && !$wCity['WebCity']['is_group']) {
                $this->Cookie->write('geo_city_slug', $city_slug);
            }
        }
        $not_conditions = array();
        $landing = isset($this->params['url']['landing']) ? $this->params['url']['landing'] : '';
        if (!empty($landing)) {
            switch ($landing) {
                case 'objetos':
                    return $this->redirect('/productos');
                case 'gastronomia':
                    return $this->redirect('/festival-gastronomico');
                case 'belleza':
                    return $this->redirect('/especial-belleza');
                case 'indumentaria':
                    return $this->redirect('/especial-indumentaria');
                case 'landingpre':
                case 'entretenimiento':
                default:
            }
        }
        if (empty($this->params['named']['company'])) {
            if (isset($this->params['named']['city'])) {
                $city_slug = $this->params['named']['city'];
            } else {
                $this->redirect($this->params['url']['url'], 302);
            }
            $city = $this->Deal->City->find('first', array(
                'conditions' => array(
                    'City.slug' => $city_slug,
                    'City.is_approved' => 1
                ),
                'fields' => array(
                    'City.name',
                    'City.id'
                ),
                'recursive' => - 1
            ));
            if (empty($city)) {
                $this->cakeError('error404');
            }
            $conditions['City.id'] = $city['City']['id'];
        }
        $order = array(
            'Deal.id' => 'desc'
        );
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'recent') {
            $limit = 10;
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Closed,
                ConstDealStatus::Canceled,
                ConstDealStatus::PaidToCompany
            );
            $conditions['Deal.is_side_deal'] = false;
            $not_conditions['Not']['Deal.id'] = array(
                '204',
                '226'
            );
            $setRecentTitle = true;
            $order = array(
                'Deal.end_date' => 'DESC'
            );
        } elseif (empty($this->params['named']['company'])) {
            if (Configure::read('deal.is_side_deal_enabled')) {
                $conditions['Deal.is_side_deal'] = 0;
            }
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Open,
                ConstDealStatus::Tipped
            );
            $this->pageTitle = Configure::read('site.name') . ' - ' . ucfirst($city['City']['name']) . ' Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
            $title_for_layout = Configure::read('site.name') . ' - ' . ucfirst($city['City']['name']) . ' Descuentos en Restaurantes, Cines, Spa, Gimnasios.';
            $this->set('title_for_layout', $title_for_layout);
            $order = array(
                'Deal.start_date' => 'desc',
                'Deal.deal_status_id' => 'desc'
            );
            $this->set('city_name', $city['City']['name']);
        }
        if (!empty($this->params['named']['company'])) {
            $this->set('homeForCompany', true);
            $company = $this->Deal->Company->find('first', array(
                'conditions' => array(
                    'Company.slug = ' => $this->params['named']['company']
                ),
                'fields' => array(
                    'Company.id',
                    'Company.name',
                    'Company.slug',
                    'Company.user_id'
                ),
                'recursive' => - 1
            ));
            if ((!$this->Auth->user('id')) || ($company['Company']['user_id'] != $this->Auth->user('id'))) {
                $this->cakeError('error404');
            }
            $conditions['Deal.company_id'] = $company['Company']['id'];
            $not_conditions['Not']['Deal.is_now'] = 1;
            if (!empty($this->params['named']['filter_id'])) {
                $conditions['Deal.deal_status_id'] = $this->params['named']['filter_id'];
            }
            $headings = __l('My Deals');
            $this->set('headings', $headings);
            $this->set('company_slug', $company['Company']['slug']);
        }
        if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
            $not_conditions['Not']['Deal.deal_status_id'] = array(
                ConstDealStatus::Upcoming
            );
        }
        if (empty($this->params['named']['company'])) {
            $conditions[] = 'Deal.parent_deal_id = Deal.id';
        }
        $this->paginate = array(
            'fields' => array(
                '*',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'A\') as `sold_coupons`'
            ),
            'conditions' => array(
                $conditions,
                $not_conditions
            ),
            'contain' => array(
                'User',
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                ),
                'Attachment',
                'AttachmentMultiple',
                'City',
                'DealStatus',
                'DealCategory'
            ),
            'order' => $order
        );
        if ($limit) {
            $this->paginate['limit'] = $limit;
        }
        $deals = $this->paginate();
        if (!empty($this->params['named']['company']) && !is_null($deals)) {
            foreach ($deals as & $deal) {
                if ($deal['Deal']['id'] == $deal['Deal']['parent_deal_id'] && !empty($deal['Deal']['descriptive_text'])) {
                    $deal['Deal']['name'] = $deal['Deal']['name'] . ' - ' . $deal['Deal']['descriptive_text'];
                }
            }
        }
        if (!is_null($deals)) {
            $count = 1;
            foreach ($deals as $k => $deal_buffer) {
                $isAccesibleForBuy = $this->checkIfDealIsAccesibleForBuy($deal_buffer['Deal']) || $this->Deal->hasSubdeals($deal_buffer['Deal']['id']);
                $deals[$k]['Deal']['isAccesibleForBuy'] = $isAccesibleForBuy;
                if ($isAccesibleForBuy) {
                    $deal_buy_link_and_type = $this->_generatePurchaseLink($deals[$k], $city);
                    $deals[$k]['Deal']['deal_buy_link'] = $deal_buy_link_and_type['link'];
                    $deals[$k]['Deal']['deal_promotion_type'] = $deal_buy_link_and_type['type'];
                }
                $has_subdeals = $this->Deal->hasSubdeals($deal_buffer['Deal']['id']);
                $deals[$k]['Deal']['has_subdeals'] = $has_subdeals;
                if (in_array($deal_buffer['Deal']['deal_status_id'], array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped
                        ))) {
                    $deals[$k]['Deal']['hasStock'] = $this->hasStock($deal_buffer['Deal']);
                }
                $count++;
            }
        }
        $this->set('deals', $deals);
        if ($setRecentTitle) {
            $this->pageTitle = 'Ofertas recientes en ' . Configure::read('site.name') . ' ';
            for ($i = 0; $i < 2; $i++) {
                if (!empty($deals[$i]['Deal']['name'])) {
                    $this->pageTitle .= $deals[$i]['Deal']['name'] . ' | ';
                }
            }
        }
        $this->set('side_deals', false);
        if (((empty($deals) && (!($this->Auth->user('id'))))) || ((empty($deals) && isset($company) && ($company['Company']['user_id'] != $this->Auth->user('id'))))) {
            $this->layout = 'landing-index';
        }
        if (isset($_GET['from']) && (in_array($_GET['from'], array(
                    'mailing',
                    'affiliate',
                    'subscription'
                )))) {
            $this->Cookie->write('CakeCookie[first_time_user]', 'no', true, '+5 months');
            $this->set('force_popup', false);
        } else {
            if (FIRST_TIME_USER && (!$this->Auth->user('id'))) {
                $fromSearchEngine = isset($_SERVER['HTTP_REFERER']) && ((strpos($_SERVER['HTTP_REFERER'], 'google') !== false) || (strpos($_SERVER['HTTP_REFERER'], 'bing') !== false) || (strpos($_SERVER['HTTP_REFERER'], 'yahoo') !== false));
            }
        }
        if (isset($_GET['popup']) && ($_GET['popup'] == 'si')) {
            $this->set('force_popup', true);
        }
        if (isset($_GET['popup']) && ($_GET['popup'] == 'no')) {
            $this->set('force_popup', false);
        }
        $this->set('from_subscription', isset($_GET['from']) && ($_GET['from'] == 'subscription'));
        if (empty($deals) && $this->Auth->user('user_type_id') == ConstUserTypes::Company && !$this->Deal->User->isAllowed($this->Auth->user('user_type_id')) && empty($this->params['named']['company'])) {
            $company = $this->Deal->Company->find('first', array(
                'conditions' => array(
                    'Company.user_id = ' => $this->Auth->user('id')
                ),
                'fields' => array(
                    'Company.slug'
                ),
                'recursive' => - 1
            ));
            $this->Session->setFlash(__l('No deals available in this city.'), 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index',
                'company' => $company['Company']['slug'],
                'admin' => false
            ));
        }
        if (!empty($this->params['named']['city'])) {
            $get_current_city = $this->params['named']['city'];
        } else {
            $get_current_city = Configure::read('site.city');
        }
        $this->set('get_current_city', $get_current_city);
        $hideSubscription = $this->isSubscriptionEnabledForCurrentCity($get_current_city);
        $this->set('hideSubscription', $hideSubscription);
        $whenSuscriptionIsHidden = $this->_whenSubscriptionIsNotEnabled($get_current_city);
        $this->set('whenSuscriptionIsHidden', $whenSuscriptionIsHidden);
        if (!empty($this->params['named']['company'])) {
            $dealStatuses = $this->Deal->DealStatus->find('list');
            $dealStatusesCount = array();
            foreach ($dealStatuses as $id => $dealStatus) {
                $dealStatusesCount[$id] = $this->Deal->find('count', array(
                    'conditions' => array(
                        'Deal.deal_status_id' => $id,
                        'Deal.company_id' => $company['Company']['id']
                    ),
                    'recursive' => - 1
                ));
            }
            $this->set('dealStatusesCount', $dealStatusesCount);
            $this->set('dealStatuses', $dealStatuses);
            $this->set('certificaPath', 'ClubCupon/Company/');
            if ($this->User->isCompanyWithNow($this->Auth->user())) {
                $company_is_now = true;
            } else {
                $company_is_now = false;
            }
            $this->set('company_is_now', $company_is_now);
            $this->layout = 'now';
            if ($this->User->isCompanyWithNow($this->Auth->user())) {
                $company = $this->Company->findByUserId($this->Auth->user('id'));
                $this->set('case', 'deals');
                $this->render('index_company_now_deals');
            } else {
                $this->render('index_company_deals');
            }
        } else if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'recent') {
            $this->set('certificaPath', 'ClubCupon/deal/recent');
            $this->render('index_recent_deals');
        }
    }

    function _dateGuard($date) {
        return ($date['month'] == '' || $date['day'] == '' || $date['year'] == '' || $date['hour'] == '' || $date['min'] == '' || $date['meridian'] == '') ? date('Y-m-d H:i:s') : $date;
    }

    function company_deals() {
        $conditions = array();
        if (!empty($this->params['named']['company_id'])) {
            $statusList = array(
                ConstDealStatus::Open,
                ConstDealStatus::Tipped,
                ConstDealStatus::Closed,
                ConstDealStatus::PaidToCompany
            );
            $companyUser = $this->Deal->Company->find('first', array(
                'conditions' => array(
                    'Company.id' => $this->params['named']['company_id'],
                    'Company.user_id' => $this->Auth->user('id')
                ),
                'fields' => array(
                    'Company.user_id'
                ),
                'recursive' => - 1
            ));
            if (!empty($companyUser) || ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
                $statusList[] = ConstDealStatus::Draft;
            }
            $conditions = array(
                'Deal.company_id' => $this->params['named']['company_id'],
                'Deal.deal_status_id' => $statusList
            );
        }
        $this->paginate = array(
            'fields' => array(
                '*',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'A\') as `sold_coupons`'
            ),
            'conditions' => $conditions,
            'contain' => array(
                'City' => array(
                    'fields' => array(
                        'City.id'
                    )
                ),
                'DealUser' => array(
                    'fields' => array(
                        'DealUser.discount_amount'
                    )
                )
            ),
            'contain' => array(
                'Attachment' => array(
                    'fields' => array(
                        'Attachment.id',
                        'Attachment.dir',
                        'Attachment.filename',
                        'Attachment.width',
                        'Attachment.height'
                    )
                ),
                'City',
                'DealStatus' => array(
                    'fields' => array(
                        'DealStatus.name'
                    )
                )
            ),
            'recursive' => 1
        );
        $this->set('company_deals', $this->paginate());
    }

    function coupons_export() {
        $conditions = array();
        if (!empty($this->params['named']['deal_id'])) {
            $conditions['DealUser.deal_id'] = $this->params['named']['deal_id'];
        }
        $this->Deal->DealUser->contain('UserProfile');
        $dealusers = $this->Deal->DealUser->find('all', array(
            'conditions' => $conditions,
            'recursive' => 1,
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                ),
                'Deal' => array(
                    'fields' => array(
                        'Deal.id',
                        'Deal.name',
                        'Deal.coupon_expiry_date',
                        'Deal.start_date'
                    )
                ),
                'Redemption',
                'UserProfile'
            ),
            'fields' => array(
                'UserProfile.dni',
                'UserProfile.first_name',
                'UserProfile.last_name',
                'DealUser.id',
                'DealUser.coupon_code',
                'DealUser.discount_amount',
                'DealUser.is_gift',
                'DealUser.is_used',
                'DealUser.gift_to',
                'DealUser.gift_dob',
                'DealUser.gift_dni',
                'Redemption.redeemed',
                'Redemption.way'
            )
        ));
        if (!empty($dealusers)) {
            foreach ($dealusers as $dealuser) {
                $fullName = '';
                if (!empty($dealuser['UserProfile']['last_name'])) {
                    $fullName = $dealuser['UserProfile']['last_name'];
                    if (!empty($dealuser['UserProfile']['first_name'])) {
                        $fullName . ', ' . $dealuser['UserProfile']['first_name'];
                    }
                }
                $data[]['Deal'] = array(
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'TÃ­tulo de la Oferta') => $dealuser['Deal']['name'],
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'Fecha de publicaciÃ³n') => $dealuser['Deal']['start_date'],
                    __l('Nombre de Usuario') => $dealuser['User']['username'],
                    __l('DNI') => $dealuser['UserProfile']['dni'],
                    __l('Monto Pagado ($)') => Configure::read('site.currency') . $dealuser['DealUser']['discount_amount'],
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'CupÃ³n') => $dealuser['DealUser']['coupon_code'],
                    __l('Es regalo') => $dealuser['DealUser']['is_gift'] ? 'SI' : 'NO',
                    __l('Nombre del Amigo') => $dealuser['DealUser']['gift_to'],
                    __l('Fec Nac Amigo') => $dealuser['DealUser']['gift_dob'] ? $dealuser['DealUser']['gift_dob'] : '-',
                    __l('DNI Amigo') => $dealuser['DealUser']['gift_dni'] ? $dealuser['DealUser']['gift_dni'] : '-',
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'Fecha de ExpiraciÃ³n') => date(Configure::read('site.datetime.format'), strtotime($dealuser['Deal']['coupon_expiry_date'])),
                    __l('Usado') => $dealuser['DealUser']['is_used'] ? 'SI' : 'NO',
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'Fecha de RedenciÃ³n') => (!isset($dealuser['Redemption']['redeemed']) || empty($dealuser['Redemption']['redeemed'])) ? '' : date(Configure::read('site.datetime.format'), strtotime($dealuser['Redemption']['redeemed'])),
                    iconv('UTF-8', 'ISO-8859-1//TRANSLIT', 'Modo de RedenciÃ³n') => $dealuser['Redemption']['way']
                );
                $deal_name = $dealuser['Deal']['name'];
            }
        }
        if (empty($this->params['named']['deal_id'])) {
            $deal_name = 'coupons';
        }
        $this->set('data', $data);
        $this->set('deal_name', $deal_name);
    }

    function admin_export() {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('coupons_export');
    }

    function view($slug = null, $count = null) {
        if (isset($_GET['from']) && (in_array($_GET['from'], array(
                    'mailing',
                    'affiliate',
                    'subscription'
                )))) {
            $this->set('force_popup', false);
            $this->Cookie->write('CakeCookie[first_time_user]', 'no', true, '+5 months');
        }
        if (isset($_GET['popup']) && ($_GET['popup'] == 'si')) {
            $this->set('force_popup', true);
        }
        if (isset($_GET['popup']) && ($_GET['popup'] == 'no')) {
            $this->set('force_popup', false);
        }
        $this->set('certificaPath', 'ClubCupon/deal/uniquePage');
        if (is_null($slug)) {
            $this->cakeError('error404');
        }
        if ($this->Auth->user('id')) {
            $user_available_balance = $this->Deal->User->checkUserBalance($this->Auth->user('id'));
            $user_wallet_blocked = $this->Deal->User->checkUserWalletBlocked($this->Auth->user('id'));
            $user_available_points = $this->Deal->User->get_current_points($this->Auth->user('id'));
        }
        if ($this->Auth->user('id')) {
            $this->set('logged_user_on_cc', true);
        }
        $deal = $this->Deal->findDealBySlug($slug);
        $has_subdeals = $this->Deal->hasSubdeals($deal['Deal']['id']);
        if (empty($deal) || $this->Deal->is_subdeal($deal)) {
            $this->cakeError('error404');
        }
        if ($this->params['named']['city'] != $deal['City']['slug']) {
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'view',
                'city' => $deal['City']['slug'],
                $this->params['pass'][0]
            ));
        }
        if (in_array($deal['City']['slug'], Configure::read('corporate.external_hidden_cities'))) {
            $this->redirectOnInvalidExternalHiddenCityBuyAttemp();
        }
        $og_data = array();
        $og_data['og:title'] = $deal['Deal']['name'];
        $og_data['og:type'] = 'product';
        $og_data['og:image'] = array(
            $this->_get_deal_image_url('medium_big_thumb', 'jpg', $deal['Attachment']['id'])
        );
        foreach ($deal['AttachmentMultiple'] as $attachment) {
            array_push($og_data['og:image'], $this->_get_deal_image_url('medium_big_thumb', 'jpg', $attachment['id']));
        }
        $og_data['og:url'] = Router::url(array(
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug']
                        ), true);
        $og_data['og:site_name'] = Configure::read('site.name');
        if (!empty($deal['City']['fb_api_key'])) {
            $og_data['fb:app_id'] = $deal['City']['fb_api_key'];
        } else {
            $og_data['fb:app_id'] = Configure::read('facebook.fb_api_key');
        }
        $og_data['og:description'] = $deal['Deal']['description'];
        if ($deal['Deal']['show_map']) {
            $og_data['og:latitude'] = $deal['Company']['latitude'];
            $og_data['og:longitude'] = $deal['Company']['longitude'];
            if (($deal['Company']['address1'] != '') && ($deal['Company']['address2'] != '')) {
                $og_data['og:street-address'] = $deal['Company']['address1'] . ' / ' . $deal['Company']['address2'];
            } else if ($deal['Company']['address1'] != '') {
                $og_data['og:street-address'] = $deal['Company']['address1'];
            } else if ($deal['Company']['address2'] != '') {
                $og_data['og:street-address'] = $deal['Company']['address2'];
            }
            $og_data['og:locality'] = $deal['Company']['City']['name'];
            $og_data['og:region'] = $deal['Company']['State']['name'];
            $og_data['og:postal-code'] = $deal['Company']['zip'];
            $og_data['og:country-name'] = $deal['Company']['Country']['name'];
            $og_data['og:email'] = $deal['Company']['User']['email'];
            $og_data['og:phone_number'] = $deal['Company']['phone'];
        }
        if (!empty($deal['Deal']['meta_keywords'])) {
            Configure::write('meta.keywords', $deal['Deal']['meta_keywords']);
        }
        if (!empty($deal['Deal']['meta_description'])) {
            Configure::write('meta.description', $deal['Deal']['meta_description']);
        }
        if (($this->Auth->user('user_type_id') == ConstUserTypes::User or ! $this->Auth->user('user_type_id')) && ($deal['Deal']['deal_status_id'] == ConstDealStatus::PendingApproval || $deal['Deal']['deal_status_id'] == ConstDealStatus::Upcoming || $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft || $this->Deal->is_subdeal($deal))) {
            $this->cakeError('error404');
        }
        if ($this->Auth->user('user_type_id') == ConstUserTypes::Company && ($deal['Deal']['deal_status_id'] == ConstDealStatus::PendingApproval || $deal['Deal']['deal_status_id'] == ConstDealStatus::Upcoming || $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft)) {
            $companyUser = $this->Deal->Company->find('first', array(
                'conditions' => array(
                    'Company.user_id' => $this->Auth->user('id')
                ),
                'fields' => array(
                    'Company.id'
                ),
                'recursive' => - 1
            ));
            if ($deal['Deal']['company_id'] != $companyUser['Company']['id'])
                $this->cakeError('error404');
        }
        $this->pageTitle = Configure::read('site.name') . ' - Oferta ' . $deal['Company']['name'] . ' ' . $deal['Deal']['name'];
        $title_for_layout = Configure::read('site.name') . ' - Oferta ' . $deal['Company']['name'] . ' ' . $deal['Deal']['name'];
        if (!$this->Deal->City->isCorporate($this->params['named']['city'])) {
            $this->Cookie->write('is_corporate_guest', 0);
            $this->set('is_corporate_city', 0);
        }
        if (!empty($this->params['named']['city'])) {
            $get_current_city = $this->params['named']['city'];
        } else {
            $get_current_city = $deal['City']['slug'];
        }
        $city_name = $deal['City']['name'];
        $hideSubscription = $this->isSubscriptionEnabledForCurrentCity($get_current_city);
        $whenSuscriptionIsHidden = $this->_whenSubscriptionIsNotEnabled($get_current_city);
        $hasStock = $this->hasStock($deal['Deal']);
        $this->set('hasStock', $hasStock);
        $isAccesibleForBuy = $this->checkIfDealIsAccesibleForBuy($deal['Deal']);
        $deal['Deal']['isAccesibleForBuy'] = $isAccesibleForBuy;
        if ($isAccesibleForBuy) {
            $deal_buy_link_and_type = $this->_generatePurchaseLink($deal, $get_current_city);
            $deal['Deal']['deal_buy_link'] = $deal_buy_link_and_type['link'];
            $deal['Deal']['deal_promotion_type'] = $deal_buy_link_and_type['type'];
        } else {
            if ($this->Auth->user('id')) {
                Debugger::log('La oferta no puede ser comprada.' . 'userId: ' . $this->Auth->user('id') . ' ip:' . $this->RequestHandler->getClientIP());
            }
            if (!empty($deal['Deal']['redirect_url']) && $deal['Deal']['redirect_url']) {
                $url = str_replace('https://', "", $deal['Deal']['redirect_url']);
                $url = str_replace('http://', "", $url);
                $url = 'http://' . $url;
                $qs = $this->getStringParams($_GET);
                $url = $url . '?' . $qs;
                $this->redirect($url);
            }
        }
        $this->set('has_subdeals', $has_subdeals);
        $this->set('get_current_city', $get_current_city);
        $this->set('is_deal_view', true);
        $this->set('count', $count);
        $this->set('deal', $deal);
        $this->set('from_page', 'deal_view');
        $this->set('og_data', $og_data);
        $this->set('title_for_layout', $title_for_layout);
        $this->set('city_name', $city_name);
        $this->set('hideSubscription', $hideSubscription);
        $this->set('whenSuscriptionIsHidden', $whenSuscriptionIsHidden);
    }

    private function getStringParams($params) {
        $str = '';
        foreach ($params as $key => $val) {
            if ($key != 'url') {
                $str = $str . ($key . '=' . urlencode($val) . '&');
            }
        }
        $str = substr_replace($str, "", -1);
        return $str;
    }

    private function hasStock($deal) {
        $deal_id = $deal['id'];
        $buy_max_quantity_per_user = $deal['buy_max_quantity_per_user'];
        $max_limit = $deal['max_limit'];
        $deal_user_count = $deal['deal_user_count'];
        $cupones_generados = $this->DealUser->findCuponsGeneratedByDeal($deal_id);
        if ($this->Auth->user('id')) {
            $user_id = $this->Auth->user('id');
        } else {
            if ($max_limit) {
                return (is_int($max_limit) && $max_limit <= $cupones_generados);
            }
            return true;
        }
        $total_bought_user = $this->DealExternal->getCountDealPurchasedFromUser($user_id, $deal_id);
        if (!$total_bought_user) {
            $total_bought_user = 0;
        }
        if (null == $max_limit || $max_limit == false) {
            if ($buy_max_quantity_per_user == null || $buy_max_quantity_per_user == false) {
                return true;
            }
            return $buy_max_quantity_per_user > $total_bought_user;
        } else {
            $global_stock = $max_limit > $deal_user_count;
            if ($buy_max_quantity_per_user == null || $buy_max_quantity_per_user == false) {
                return true;
            }
            $constrained_sotck = $buy_max_quantity_per_user > $total_bought_user;
            return $global_stock && $constrained_sotck;
        }
    }

    private function hasRemainingStock($deal) {
        $deal_id = $deal['id'];
        $max_limit = $deal['max_limit'];
        $nodisponibles = $this->DealExternal->findQuantityForCreditedOrPending($deal_id);
        if ($max_limit != null) {
            $max_limit = intval($max_limit);
            return $nodisponibles < $max_limit;
        } else {
            return true;
        }
    }

    function edit($id = null) {
        $isDraft = !empty($this->data['Deal']['save_as_draft']);
        $this->pageTitle = __l('Edit Deal');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->Deal->is_subdeal($this->data)) {
                $this->Deal->unbindModel(array(
                    'hasOne' => array(
                        'Attachment'
                    )
                ));
                $parent = $this->Deal->findById($this->data['Deal']['parent_deal_id']);
                $this->data['Deal'] = array_merge($parent['Deal'], $this->data['Deal']);
                $this->data['Deal']['name'] = $parent['Deal']['name'] . ' - ' . $this->data['Deal']['descriptive_text'];
            }
            $dataCompany = $this->Deal->Company->findById($this->data['Deal']['company_id']);
            $this->data['Deal']['is_end_user'] = $dataCompany['Company']['is_end_user'];
            unset($this->Deal->validate['start_date']['rule2']);
            if (!empty($this->data['Deal']['send_to_admin'])) {
                $this->data['Deal']['deal_status_id'] = ConstDealStatus::PendingApproval;
            }
            if (empty($this->data['Deal']['discount_percentage'])) {
                $this->data['Deal']['savings'] = $this->data['Deal']['discount_amount'];
            } else {
                $this->data['Deal']['savings'] = Precision::percentageOf($this->data['Deal']['original_price'], $this->data['Deal']['discount_percentage']);
            }
            $this->data['Deal']['discounted_price'] = Precision::sub($this->data['Deal']['original_price'], $this->data['Deal']['savings']);
            if ($isDraft || $this->Deal->validates()) {
                if ($isDraft) {
                    $this->data['Deal']['start_date'] = $this->_dateGuard($this->data['Deal']['start_date']);
                    $this->data['Deal']['end_date'] = $this->_dateGuard($this->data['Deal']['end_date']);
                    if (empty($this->data['Deal']['min_limit'])) {
                        $this->data['Deal']['min_limit'] = "0";
                    }
                    if (empty($this->data['Deal']['downpayment_percentage'])) {
                        $this->data['Deal']['downpayment_percentage'] = "0";
                    }
                }
                if ($this->Deal->save($this->data)) {
                    if (!empty($this->data['Attachment']['filename']['name'])) {
                        $this->data['Attachment']['filename']['type'] = get_mime($this->data['Attachment']['filename']['tmp_name']);
                    }
                    if (!empty($this->data['Attachment']['filename']['name']) || (!Configure::read('image.file.allowEmpty') && empty($this->data['Attachment']['id']))) {
                        $this->Deal->Attachment->set($this->data);
                    }
                    $this->Deal->set($this->data);
                    if (!empty($this->data['Attachment']['filename'])) {
                        $is_attachment_error = true;
                        if (!empty($this->data['Attachment']['filename']['name'])) {
                            $data['Attachment']['filename'] = $this->data['Attachment']['filename'];
                            $this->Attachment->Behaviors->attach('ImageUpload', Configure::read('image.file'));
                            $this->Attachment->set($data);
                            if ($this->Attachment->validates() && $is_attachment_error) {
                                $is_attachment_error = true;
                            } else {
                                $is_attachment_error = false;
                            }
                        }
                    }
                    $foreign_id = $this->data['Deal']['id'];
                    $attach = $this->Attachment->find('first', array(
                        'conditions' => array(
                            'Attachment.foreign_id = ' => $foreign_id,
                            'Attachment.class != ' => 'DealNewsLetter'
                        ),
                        'fields' => array(
                            'Attachment.id'
                        ),
                        'recursive' => - 1
                    ));
                    if (!empty($this->data['Attachment']['filename']['name'])) {
                        $this->data['Attachment']['filename'] = $this->data['Attachment']['filename'];
                        $this->data['Attachment']['class'] = $this->modelClass;
                        $this->data['Attachment']['description'] = 'Product Image';
                        $this->data['Attachment']['id'] = $attach['Attachment']['id'];
                        $this->data['Attachment']['foreign_id'] = $this->data['Deal']['id'];
                        $this->Attachment->Behaviors->attach('ImageUpload', Configure::read('product.file'));
                        $this->Attachment->set($this->data);
                        if ($this->Attachment->validates())
                            $this->Attachment->save($this->data['Attachment']);
                    }
                    $this->Session->setFlash(__l('Deal has been updated'), 'default', null, 'success');
                    $deal = $this->Deal->find('first', array(
                        'conditions' => array(
                            'Deal.id' => $this->data['Deal']['id']
                        ),
                        'contain' => array(
                            'City' => array(
                                'fields' => array(
                                    'City.id',
                                    'City.name',
                                    'City.slug'
                                )
                            ),
                            'Attachment',
                            'Company'
                        ),
                        'recursive' => 2
                    ));
                    $slug = $deal['Deal']['slug'];
                    $city_name = $deal['City']['name'];
                    $this->Session->setFlash(__l('Deal has been updated'), 'default', null, 'success');
                    if (ConstUserTypes::isLikeAdmin($this->Auth->user('user_type_id'))) {
                        $this->redirect(array(
                            'controller' => 'deals',
                            'action' => 'index'
                        ));
                    } else {
                        $this->redirect(array(
                            'controller' => 'deals',
                            'action' => 'company',
                            $deal['Company']['slug']
                        ));
                    }
                }
            } else {
                $this->Session->setFlash(__l('Deal could not be updated. Please, try again.'), 'default', null, 'error');
            }
            $attachment = $this->Attachment->find('first', array(
                'conditions' => array(
                    'Attachment.foreign_id = ' => $this->data['Deal']['id'],
                    'Attachment.class  <>' => 'DealNewsLetter'
                ),
                'recursive' => - 1
            ));
            $this->data['Attachment'] = $attachment['Attachment'];
            $attachment_newsletter = $this->Attachment->find('first', array(
                'conditions' => array(
                    'Attachment.foreign_id = ' => $this->data['Deal']['id'],
                    'Attachment.class = ' => 'DealNewsLetter'
                ),
                'recursive' => - 1
            ));
            $this->data['AttachmentNewsLetter'] = $attachment_newsletter['Attachment'];
            if (!empty($this->data['AttachmentNewsLetter']['filename']['name'])) {
                if ($attachment_newsletter['Attachment']['id']) {
                    $this->data['AttachmentNewsLetter']['id'] = $attachment_newsletter['Attachment']['id'];
                }
                $this->data['AttachmentNewsLetter']['foreign_id'] = $this->data['Deal']['id'];
                $this->data['AttachmentNewsLetter']['dir'] = 'DealNewsLetter/' . $this->data['Deal']['id'];
                $this->data['AttachmentNewsLetter']['class'] = 'DealNewsLetter';
                $this->Attachment->save($this->data['AttachmentNewsLetter']);
            }
        } else {
            $conditions = array();
            if (ConstUserTypes::isNotLikeAdmin($this->Auth->user('user_type_id'))) {
                $companyUser = $this->Deal->Company->find('first', array(
                    'conditions' => array(
                        'Company.user_id' => $this->Auth->user('id')
                    ),
                    'fields' => array(
                        'Company.id'
                    ),
                    'recursive' => - 1
                ));
                $conditions['Deal.company_id'] = $companyUser['Company']['id'];
            }
            $conditions['Deal.id'] = $id;
            $this->data = $this->Deal->find('first', array(
                'conditions' => array(
                    $conditions
                ),
                'recursive' => 1
            ));
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle .= ' - ' . $this->data['Deal']['name'];
        $discounts = array();
        for ($i = 1; $i <= 100; $i++) {
            $discounts[$i] = $i;
        }
        $cities = $this->Deal->City->find('list', array(
            'conditions' => array(
                'City.is_approved =' => 1
            ),
            'order' => array(
                'City.name' => 'asc'
            )
        ));
        $companies = $this->Deal->Company->find('list');
        asort($companies);
        $dealStatuses = $this->Deal->DealStatus->find('list');
        $this->set(compact('cities', 'dealStatuses', 'companies', 'discounts'));
        if ($this->Deal->is_subdeal($this->data)) {
            $this->set('parent_deal_id', $this->data['Deal']['parent_deal_id']);
        }
    }

    function add() {
        Debugger::log(sprintf('add - INICIO'), LOG_DEBUG);
        $paymentOptions = array();
        $this->set('paymentOptions', $paymentOptions);
        $this->set('errorPaymentOption', false);
        if (!empty($this->data['Deal']['is_shipping_address'])) {
            echo $this->data['Deal']['save_as_draft'] . '<br />';
            $this->data['Deal']['save_as_draft'] = $this->data['Deal']['is_shipping_address'];
            unset($this->Deal->validate['custom_company_address1']['rule1']);
        }
        $isDraft = !empty($this->data['Deal']['save_as_draft']);
        $this->pageTitle = __l('Add Deal');
        $this->Deal->Behaviors->attach('ImageUpload', Configure::read('image.file'));
        $sellers = $this->_getSellersName();
        $this->set('sellers_id', $sellers);
        $this->set('paymentPeriods', $this->_formatPaymentPeriodsArray());
        $productInventoryStrategyOptions = $this->ProductInventoryStrategy->find('list');
        $dealTradeAgreementOptions = $this->DealTradeAgreement->find('list');
        $this->set('dealTradeAgreementOptions', $dealTradeAgreementOptions);
        $dealTradeAgreementConditions = $this->DealTradeAgreement->generateArrayOfConditions();
        $this->set('dealTradeAgreementConditions', $dealTradeAgreementConditions);
        if (!empty($this->data)) {
            if ($this->data['Deal']['is_payment_term_exception']) {
                $this->data['Deal']['payment_term'] = $this->data['Deal']['custom_payment_term'];
            }
            if (is_null($this->data['Deal']['campaign_code']) || empty($this->data['Deal']['campaign_code'])) {
                if ($this->isReplicated() && !$this->isSaveAsDraftAction()) {
                    $this->Deal->invalidate('campaign_code', "No puede ser vacio");
                }
                $this->data['Deal']['campaign_code'] = $this->generateAlphaNumericSeedForCampaignCode();
                $this->data['Deal']['campaign_code_type'] = Deal::CAMPAIGN_CODE_TYPE_GENERATED;
            }
            $dealPriceDisplayConditions = $this->checkDealPriceDisplay($_POST['DisplayPrecio']);
            $this->data['Deal']['hide_price'] = $dealPriceDisplayConditions['hidePrice'];
            $this->data['Deal']['only_price'] = $dealPriceDisplayConditions['onlyPrice'];
            $deal_city_slug = $this->City->findById($this->data['Deal']['city_id']);
            $deal_city_slug = $deal_city_slug['City']['slug'];
            if (!empty($this->data['Deal']['parent_deal_id'])) {
                $this->set('parent_deal_id', $this->data['Deal']['parent_deal_id']);
                $this->Deal->unbindModel(array(
                    'hasOne' => array(
                        'Attachment'
                    )
                ));
                $parent_deal = $this->Deal->findById($this->data['Deal']['parent_deal_id']);
                $this->data['Deal'] = array_merge($parent_deal['Deal'], $this->data['Deal']);
                $this->data['Deal']['name'] = $parent_deal['Deal']['name'] . ' - ' . $this->data['Deal']['descriptive_text'];
                unset($this->data['Deal']['id']);
            }
            $dataCompany = $this->Deal->Company->findById($this->data['Deal']['company_id']);
            $paymentOptions = empty($_POST['paymentOptionValue']) ? array() : $_POST['paymentOptionValue'];
            if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
                $this->PaymentOption = & ClassRegistry::init('PaymentOption');
                $paymentOptions = array_keys($this->PaymentOption->find('list', array(
                            'conditions' => array(
                                'payment_setting_id' => default_payment_setting_id($deal_city_slug, $this->data['Deal']['is_tourism'])
                            )
                )));
            }
            $this->set('paymentOptions', $paymentOptions);
            $this->data['Deal']['paymentOptionValue'] = $paymentOptions;
            $this->data['Deal']['bonus_amount'] = (!empty($this->data['Deal']['bonus_amount'])) ? $this->data['Deal']['bonus_amount'] : 0;
            $this->data['Deal']['commission_percentage'] = (!empty($this->data['Deal']['commission_percentage'])) ? $this->data['Deal']['commission_percentage'] : 0;
            $this->data['Deal']['is_product'] = $this->data['Deal']['portal'] == 'is_product';
            if (!empty($this->data['AttachmentMultiple'])) {
                foreach ($this->data['AttachmentMultiple'] as $idx => $att) {
                    if (empty($att['filename']['name'])) {
                        unset($this->data['AttachmentMultiple'][$idx]);
                    } else {
                        $this->data['AttachmentMultiple'][$idx]['class'] = 'DealMultiple';
                        $this->data['AttachmentMultiple'][$idx]['filename']['type'] = get_mime($att['filename']['tmp_name']);
                    }
                }
            }
            Debugger::log(sprintf('add - [Attachment][filename][name]:' . $this->data['Attachment']['filename']['name']), LOG_DEBUG);
            if (!empty($this->data['Attachment']['filename']['name'])) {
                $this->data['Attachment']['filename']['type'] = get_mime($this->data['Attachment']['filename']['tmp_name']);
            }
            Debugger::log(sprintf('add - [Attachment][filename][type]:' . $this->data['Attachment']['filename']['type']), LOG_DEBUG);
            if (!empty($this->data['Attachment']['filename']['name']) || (!Configure::read('image.file.allowEmpty') && empty($this->data['Attachment']['id']))) {
                $this->data['Attachment']['class'] = 'Deal';
                $this->Deal->Attachment->set($this->data);
            }
            if ($this->_canCalcSegmentationProfile($this->data)) {
                $this->ApiLogger->debug('Se puede actualizar el segmentation profile de la oferta, calculando y asignando el profile que le corresponde.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id']
                ));
                $newSegmentationProfile = $this->_calcSegmentationProfileForDeal($this->data);
                $this->data['Deal']['segmentation_profile_id'] = $newSegmentationProfile['SegmentationProfile']['id'];
                $this->ApiLogger->debug('Segmentation profile asignado a la oferta.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id'],
                    'new_segmentation_profile' => $newSegmentationProfile
                ));
            } else {
                $this->ApiLogger->debug('No estan dadas las condiciones para actualizar el segmentation profile de la oferta.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id']
                ));
            }
            $this->Deal->set($this->data);
            $ini_upload_error = 1;
            if (empty($this->data['Deal']['parent_deal_id'])) {
                if ($this->data['Attachment']['filename']['error'] == 1) {
                    $ini_upload_error = 0;
                }
                if ($ini_upload_error == 0 || empty($this->data['Attachment']['filename']['name'])) {
                    $this->Deal->validationErrors['Attachment']['filename'] = __l('Required');
                }
                $pathInfo = pathinfo($this->data['Attachment']['filename']['name']);
                if (!$isDraft && !in_array(strtolower($pathInfo['extension']), Configure::read('image.file.allowedExt'))) {
                    $this->Deal->validationErrors['Attachment']['filename'] = __l('Upload a valid image file');
                }
            }
            $attachment_validation = $this->Deal->Attachment->validates();
            Debugger::log(sprintf('add - attachment_validation:' . ($attachment_validation > 0 ? 'ok' : 'falla')), LOG_DEBUG);
            debug($this->Deal->validationErrors);
            if (!empty($this->data['Deal']['parent_deal_id'])) {
                unset($this->Deal->validate['coupon_start_date']['rule1']);
                unset($this->Deal->validate['coupon_start_date']['rule2']);
                unset($this->Deal->validate['coupon_start_date']['rule3']);
                unset($this->Deal->validate['coupon_expiry_date']['rule1']);
            }
            if ($isDraft || $this->Deal->validates()) {
                if ($isDraft || !empty($this->data['Deal']['parent_deal_id']) || ($attachment_validation && $ini_upload_error)) {
                    $this->Deal->create();
                    if (!empty($this->data['Deal']['save_as_draft'])) {
                        $this->data['Deal']['deal_status_id'] = ConstDealStatus::Draft;
                    } elseif (ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
                        $this->data['Deal']['deal_status_id'] = ConstDealStatus::Upcoming;
                    } else {
                        $this->data['Deal']['deal_status_id'] = ConstDealStatus::PendingApproval;
                    }
                    if ($isDraft) {
                        $this->data['Deal']['start_date'] = $this->_dateGuard($this->data['Deal']['start_date']);
                        $this->data['Deal']['end_date'] = $this->_dateGuard($this->data['Deal']['end_date']);
                        if (empty($this->data['Deal']['min_limit'])) {
                            $this->data['Deal']['min_limit'] = "0";
                        }
                        if (empty($this->data['Deal']['downpayment_percentage'])) {
                            $this->data['Deal']['downpayment_percentage'] = "0";
                        }
                    }
                    if (!($this->Deal->save($this->data, array(
                                'validate' => !$isDraft
                            )))) {
                        $this->Session->setFlash(__l('Deal could not be added. Please, try again.'), 'default', null, 'error');
                    } else {
                        if (empty($this->data['Deal']['parent_deal_id'])) {
                            $this->Deal->updateAll(array(
                                'Deal.parent_deal_id' => $this->Deal->getLastInsertId()
                                    ), array(
                                'Deal.id' => $this->Deal->getLastInsertId()
                            ));
                        } else {
                            $this->ApiLogger->debug('inicio-replico en la suboferta los puntos de retiro del padre.', null);
                            $this->ShippingAddressDeal = new ShippingAddressDeal();
                            $shipping_address_ids = $this->ShippingAddressDeal->findAddressesByDealId($this->data['Deal']['parent_deal_id'], true);
                            if (!empty($shipping_address_ids)) {
                                $shippingAddressDealsReplica = array();
                                foreach ($shipping_address_ids as $shipping_address_id) {
                                    $replicaShippingAddressDeal = array();
                                    $replicaShippingAddressDeal['ShippingAddressDeal']['shipping_address_id'] = $shipping_address_id;
                                    $replicaShippingAddressDeal['ShippingAddressDeal']['deal_id'] = $this->Deal->getLastInsertId();
                                    $shippingAddressDealsReplica[] = $replicaShippingAddressDeal;
                                }
                                $this->ShippingAddressDeal->saveAll($shippingAddressDealsReplica);
                                if (!empty($this->data['Deal']['is_shipping_address'])) {
                                    if (ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
                                        $this->Deal->updateAll(array(
                                            'Deal.deal_status_id' => ConstDealStatus::Upcoming
                                                ), array(
                                            'Deal.id' => $this->Deal->getLastInsertId()
                                        ));
                                    } else {
                                        $this->Deal->updateAll(array(
                                            'Deal.deal_status_id' => ConstDealStatus::PendingApproval
                                                ), array(
                                            'Deal.id' => $this->Deal->getLastInsertId()
                                        ));
                                    }
                                }
                            }
                            $this->ApiLogger->debug('fin-replico en la suboferta los puntos de retiro del padre.', null);
                        }
                        if (empty($this->data['Deal']['similar_deal_id'])) {
                            $this->Deal->updateAll(array(
                                'Deal.similar_deal_id' => $this->Deal->getLastInsertId()
                                    ), array(
                                'Deal.id' => $this->Deal->getLastInsertId()
                            ));
                        }
                        if (!empty($this->data['Attachment']['filename']['name']) && $this->Deal->Attachment->validates() && $ini_upload_error) {
                            Debugger::log(sprintf('add - inicio - Deal - Attachment - create()'), LOG_DEBUG);
                            $c = $this->Deal->Attachment->create();
                            if (isset($c) && !empty($c)) {
                                Debugger::log(sprintf('add - fin - Deal - Attachment - create : OK'), LOG_DEBUG);
                            } else {
                                Debugger::log(sprintf('add - fin - Deal - Attachment - create : Falla'), LOG_DEBUG);
                            }
                            $lastId = $this->Deal->getLastInsertId();
                            $this->data['Attachment']['foreign_id'] = $lastId;
                            Debugger::log(sprintf('add - inicio - Deal - Attachment - save'), LOG_DEBUG);
                            $s = $this->Deal->Attachment->save($this->data['Attachment'], array(
                                'validate' => !$isDraft
                            ));
                            if (isset($s) && !empty($s)) {
                                Debugger::log(sprintf('*add - fin - Deal - Attachment - save : OK'), LOG_DEBUG);
                            } else {
                                Debugger::log(sprintf('*add - fin - Deal - Attachment - save : Falla'), LOG_DEBUG);
                            }
                            foreach ($this->data['AttachmentMultiple'] as $idx => $att) {
                                $this->data['AttachmentMultiple'][$idx]['foreign_id'] = $lastId;
                            }
                            if (!empty($this->data['AttachmentMultiple'])) {
                                $this->Deal->Attachment->saveAll($this->data['AttachmentMultiple']);
                            }
                        }
                        $logisticsAndStockErrors = array();
                        $message = __l('Deal has been added');
                        $messageType = 'success';
                        if ($this->isLogisticsAndStockValidable() && !$this->isSaveAsDraftAction()) {
                            $logisticsAndStockErrors = $this->OfferService->validateLogisticsAndStock($this->data['Deal']);
                        }
                        if (!empty($logisticsAndStockErrors)) {
                            $this->Deal->updateAll(array(
                                'Deal.deal_status_id' => ConstDealStatus::Draft
                                    ), array(
                                'Deal.id' => $this->Deal->getLastInsertId()
                            ));
                            $message = 'Guardada como borrador: la oferta es de precompra con logÃ­stica propia y no cumple con las restricciones necesarias.';
                            $messageType = 'error';
                        }
                        $deals = $this->Deal->find('first', array(
                            'conditions' => array(
                                'Deal.id' => $this->Deal->getLastInsertId()
                            ),
                            'contain' => array(
                                'City' => array(
                                    'fields' => array(
                                        'City.id',
                                        'City.name',
                                        'City.slug'
                                    )
                                ),
                                'Attachment',
                                'Company'
                            ),
                            'recursive' => 2
                        ));
                        $slug = $deals['Deal']['slug'];
                        $city_name = $deals['City']['name'];
                        $this->Deal->_updateDealBitlyURL($deals['Deal']['slug']);
                        $this->Session->setFlash($message, 'default', null, $messageType);
                        if (!empty($this->data['Deal']['parent_deal_id'])) {
                            Debugger::log(sprintf('add - [Deal][parent_deal_id]:' . $this->data['Deal']['parent_deal_id']), LOG_DEBUG);
                            $lastId = $this->Deal->getLastInsertId();
                            $attachment = $this->Deal->Attachment->find('first', array(
                                'conditions' => array(
                                    'class' => 'Deal',
                                    'foreign_id' => $this->data['Deal']['parent_deal_id']
                                )
                            ));
                            if (!is_dir('../media/Deal/' . $lastId)) {
                                mkdir('../media/Deal/' . $lastId);
                            }
                            copy('../media/Deal/' . $this->data['Deal']['parent_deal_id'] . '/' . $attachment['Attachment']['filename'], '../media/Deal/' . $lastId . '/' . $attachment['Attachment']['filename']);
                            $this->Deal->Attachment->Behaviors->detach('ImageUpload');
                            Debugger::log(sprintf('add - inicio - Deal - Attachment - create ()'), LOG_DEBUG);
                            $c = $this->Deal->Attachment->create();
                            if (isset($c) && !empty($c)) {
                                Debugger::log(sprintf('add - fin - Deal - Attachment - create : OK'), LOG_DEBUG);
                            } else {
                                Debugger::log(sprintf('add - fin - Deal - Attachment - create : Falla'), LOG_DEBUG);
                            }
                            Debugger::log(sprintf('add - inicio - Attachment - save'), LOG_DEBUG);
                            $s = $this->Deal->Attachment->save(array(
                                'Attachment' => array(
                                    'class' => 'Deal',
                                    'foreign_id' => $lastId,
                                    'filename' => $attachment['Attachment']['filename'],
                                    'dir' => 'Deal/' . $lastId,
                                    'mimetype' => $attachment['Attachment']['mimetype'],
                                    'filesize' => $attachment['Attachment']['filesize'],
                                    'height' => $attachment['Attachment']['height'],
                                    'width' => $attachment['Attachment']['width'],
                                    'thumb' => $attachment['Attachment']['thumb'],
                                    'description' => $attachment['Attachment']['description']
                                )
                                    ), false);
                            if (isset($s) && !empty($s)) {
                                Debugger::log(sprintf('add - fin - Attachment - save : OK'), LOG_DEBUG);
                            } else {
                                Debugger::log(sprintf('add - fin - Attachment - save : Falla'), LOG_DEBUG);
                            }
                        }
                        $attachment_newsletter_validation = true;
                        Debugger::log(sprintf('add - AttachmentNewsLetter[filename][name]:' . $this->data['AttachmentNewsLetter']['filename']['name']), LOG_DEBUG);
                        if (!empty($this->data['AttachmentNewsLetter']['filename']['name'])) {
                            Debugger::log(sprintf('add - AttachmentNewsLetter[filename][name] No Vacio'), LOG_DEBUG);
                            $lastId = $this->Deal->getLastInsertId();
                            $this->Attachment = new Attachment;
                            Debugger::log(sprintf('add - inicio - Attachment-create()'), LOG_DEBUG);
                            $c = $this->Attachment->create();
                            if (isset($c) && !empty($c)) {
                                Debugger::log(sprintf('add - fin - Attachment-create() : OK'), LOG_DEBUG);
                            } else {
                                Debugger::log(sprintf('add - fin - Attachment-create() : Falla'), LOG_DEBUG);
                            }
                            $this->data['AttachmentNewsLetter']['foreign_id'] = $lastId;
                            $dir = 'DealNewsLetter/' . $lastId;
                            $this->data['AttachmentNewsLetter']['dir'] = $dir;
                            $this->data['AttachmentNewsLetter']['class'] = 'DealNewsLetter';
                            $this->Attachment->set(array(
                                'Attachment' => array(
                                    'class' => 'DealNewsLetter',
                                    'dir' => $this->data['AttachmentNewsLetter']['dir'],
                                    'foreign_id' => $lastId,
                                    'filename' => $this->data['AttachmentNewsLetter']['filename']
                                )
                            ));
                            $attachment_newsletter_validation = true;
                            $attachment_newsletter_validation = $this->Attachment->validates();
                            Debugger::log(sprintf('add - attachment_newsletter_validation:' . (attachment_newsletter_validation > 0 ? 'ok' : 'falla')), LOG_DEBUG);
                            if ($attachment_newsletter_validation) {
                                $this->Attachment->save();
                            } else {
                                
                            }
                        }
                        Debugger::log(sprintf('add - attachment_newsletter_validation:' . ($attachment_newsletter_validation > 0 ? 'ok' : 'falla')), LOG_DEBUG);
                        if ($attachment_newsletter_validation) {
                            if (ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id')) && $attachment_newsletter_validation) {
                                Debugger::log(sprintf('add - FIN'), LOG_DEBUG);
                                $this->redirect(array(
                                    'action' => 'index'
                                ));
                            } else {
                                if (Configure::read('Deal.invite_after_deal_add')) {
                                    if ($this->Auth->user('user_type_id') == ConstUserTypes::Company && !Configure::read('user.is_company_actas_normal_user')) {
                                        $this->redirect(array(
                                            'action' => 'company',
                                            $deals['Company']['slug']
                                        ));
                                    } else {
                                        $this->redirect(array(
                                            'controller' => 'user_friends',
                                            'action' => 'deal_invite',
                                            'type' => 'deal',
                                            'deal' => $deals['Deal']['slug']
                                        ));
                                    }
                                } else {
                                    $this->redirect(array(
                                        'action' => 'company',
                                        $deals['Company']['slug']
                                    ));
                                }
                            }
                        }
                    }
                }
            } else {
                if (isset($this->Deal->validationErrors['paymentOptionValue']) && !empty($this->Deal->validationErrors['paymentOptionValue']))
                    $this->set('errorPaymentOption', $this->Deal->validationErrors['paymentOptionValue']);
                $this->Session->setFlash(__l('Deal could not be added. Please, try again.'), 'default', null, 'error');
            }
        }
        else {
            $this->data['Deal']['deferred_payment'] = '30';
            $this->data['Deal']['first_closure_percentage'] = '100';
            if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
                $this->cakeError('error404');
            } elseif ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                $company = $this->Deal->Company->find('first', array(
                    'conditions' => array(
                        'Company.user_id' => $this->Auth->user('id')
                    ),
                    'fields' => array(
                        'Company.id',
                        'Company.is_end_user'
                    ),
                    'recursive' => - 1
                ));
                if (empty($company)) {
                    $this->cakeError('error404');
                }
                $this->data['Deal']['company_id'] = $company['Company']['id'];
                $this->data['Deal']['is_end_user'] = !empty($company['Company']['is_end_user']);
            }
            if (!empty($this->params['named']['clone_deal_id'])) {
                $this->data = $this->Deal->find('first', array(
                    'conditions' => array(
                        'Deal.id' => $this->params['named']['clone_deal_id']
                    ),
                    'recursive' => - 1
                ));
                $this->data['Deal']['is_replicated'] = 1;
                if ($this->data['Deal']['payment_term'] == 0) {
                    $this->data['Deal']['payment_term'] = null;
                }
                if ($this->data['Deal']['payment_term'] > 0 && !in_array($this->data['Deal']['payment_term'], Configure::read('deal.paymentTerms'))) {
                    $this->data['Deal']['custom_payment_term'] = $this->data['Deal']['payment_term'];
                    $this->data['Deal']['is_payment_term_exception'] = 1;
                }
                $paymentOptionDeals = $this->Deal->PaymentOptionDeal->find('all', array(
                    'fields' => array(
                        'PaymentOptionDeal.payment_option_id',
                        'PaymentOptionDeal.payment_plan_id'
                    ),
                    'conditions' => array(
                        'deal_id' => $this->params['named']['clone_deal_id']
                    ),
                    'recursive' => 0
                ));
                list($paymentOptions, $paymentOptionPlan) = $this->getPaymentOptionsAndPlans($paymentOptionDeals);
                unset($this->data['Deal']['id']);
                unset($this->data['Deal']['is_pending_accounting_event']);
                unset($this->data['Deal']['is_subscription_mail_sent']);
                unset($this->data['Deal']['parent_deal_id']);
                if ($this->data['Deal']['campaign_code_type'] == Deal::CAMPAIGN_CODE_TYPE_GENERATED) {
                    $this->data['Deal']['campaign_code'] = null;
                }
            } else if (!empty($this->params['named']['parent_deal_id'])) {
                $this->data = $this->Deal->find('first', array(
                    'conditions' => array(
                        'Deal.id' => $this->params['named']['parent_deal_id']
                    ),
                    'recursive' => - 1
                ));
                unset($this->data['Deal']['campaign_code']);
                $paymentOptionDeals = $this->Deal->PaymentOptionDeal->find('all', array(
                    'fields' => array(
                        'PaymentOptionDeal.payment_option_id',
                        'PaymentOptionDeal.payment_plan_id'
                    ),
                    'conditions' => array(
                        'deal_id' => $this->params['named']['parent_deal_id']
                    ),
                    'recursive' => 0
                ));
                list($paymentOptions, $paymentOptionPlan) = $this->getPaymentOptionsAndPlans($paymentOptionDeals);
                if ($this->Deal->is_subdeal($this->data)) {
                    $this->Session->setFlash('Esta oferta no admite subofertas', 'default', null, 'error');
                    $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index',
                        'admin' => true
                    ));
                }
                $this->set('parent_deal_id', $this->params['named']['parent_deal_id']);
                unset($this->data['Deal']['is_pending_accounting_event']);
            } else {
                $paymentOptions = array();
            }
            $this->data['Deal']['portal'] = $this->data['Deal']['is_tourism'] == 1 ? 'is_tourism' : ($this->data['Deal']['is_product'] == 1 ? 'is_product' : 'is_normal');
            $this->set('paymentOptions', $paymentOptions);
            $this->set('paymentOptionPlans', $paymentOptionPlan);
            $this->data['Deal']['user_id'] = $this->Auth->user('id');
            $this->data['Deal']['buy_min_quantity_per_user'] = 1;
            $this->data['Deal']['minimal_amount_financial'] = isset($this->data['Deal']['minimal_amount_financial']) ? $this->data['Deal']['minimal_amount_financial'] : Configure::read('deal.global_minimal_amount_financial');
        }
        if (ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
            $companies = $this->Deal->Company->find('list');
            asort($companies);
            $companies_is_tourism = $this->Deal->Company->find('list', array(
                'fields' => array(
                    'Company.is_tourism'
                )
            ));
            $companies_is_end_user = $this->Deal->Company->find('list', array(
                'fields' => array(
                    'Company.is_end_user'
                )
            ));
            $this->set(compact('companies'));
            $this->set('companies_is_tourism', $companies_is_tourism);
            $this->set('companies_is_end_user', $companies_is_end_user);
        }
        $cities_conditions = array(
            'City.is_approved =' => 1
        );
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $cities_conditions[] = sprintf(' City.id IN (SELECT city_id from city_perms where city_perms.user_id = %d)', $this->Auth->user('id'));
        }
        $cities = $this->Deal->City->find('list', array(
            'conditions' => $cities_conditions,
            'order' => array(
                'City.name' => 'asc'
            )
        ));
        $creatingParentDeal = $this->_isParentDealCreation();
        $deal_categories = $this->DealCategory->generatetreelist(null, null, null, '&nbsp;&nbsp;&nbsp;');
        $clusters = $this->Deal->Cluster->find('list', array(
            'order' => array(
                'Cluster.name' => 'asc'
            )
        ));
        $this->set(compact('cities', 'clusters', 'deal_categories', 'creatingParentDeal', 'productInventoryStrategyOptions'));
        if (!empty($this->params['named']['clone_deal_id'])) {
            $this->set('city_id', $this->data['Deal']['city_id']);
        }
        Debugger::log(sprintf('add - FIN'), LOG_DEBUG);
    }

    private function isLogisticsAndStockValidable() {
        return $this->data['Deal']['deal_trade_agreement_id'] == 6;
    }

    private function isSaveAsDraftAction() {
        return !empty($this->data['Deal']['save_as_draft']);
    }

    private function isReplicated() {
        return !empty($this->data['Deal']['is_replicated']) && $this->data['Deal']['is_replicated'] == 1;
    }

    function invite_friends() {
        
    }

    function delete($id = null) {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Deal->del($id)) {
            $this->Session->setFlash(__l('Deal deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function update_show_sold_quantity($deal_id) {
        if (!$deal_id)
            $this->cakeError('error404');
        $deal = $this->Deal->find('first', array(
            'fields' => 'Deal.show_sold_quantity',
            'conditions' => array(
                'Deal.id' => $deal_id
            )
        ));
        if (!$deal)
            $this->cakeError('error404');
        $updateValue = $deal['Deal']['show_sold_quantity'] ? '0' : '1';
        $this->Deal->updateAll(array(
            'Deal.show_sold_quantity' => $updateValue
                ), array(
            'Deal.id' => $deal_id
        ));
        echo $updateValue;
        die;
    }

    function update_deal($deal_id = null) {
        AppModel::setDefaultDbConnection('master');
        if (is_null($deal_id)) {
            $this->cakeError('error404');
        }
        $this->Deal->updateAll(array(
            'Deal.deal_status_id' => ConstDealStatus::PendingApproval
                ), array(
            'Deal.parent_deal_id' => $deal_id
        ));
        $deal = $this->Deal->find('first', array(
            'conditions' => array(
                'Deal.id' => $deal_id
            ),
            'contain' => array(
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug'
                    )
                ),
                'Attachment',
                'Company'
            ),
            'recursive' => 2
        ));
        $slug = $deal['Deal']['slug'];
        $city_name = $deal['City']['name'];
        $this->Session->setFlash(__l('Deal has been updated'), 'default', null, 'success');
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'company',
            $deal['Company']['slug']
        ));
    }

    function admin_index() {
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'admin_search',
            'tab' => $this->selectedTab()
        ));
    }

    private function selectedTab() {
        $selectedTab = ConstDealStatus::Open;
        if (!empty($this->data['Deal']['selected'])) {
            $selectedTab = $this->data['Deal']['selected'];
        } elseif (!empty($this->params['named']['tab'])) {
            $selectedTab = $this->params['named']['tab'];
        }
        return $selectedTab;
    }

    private function defaultSearchConditions() {
        $today = date("Y-m-d");
        $conditions = array();
        $selectedTab = $this->selectedTab();
        if ($selectedTab != 99) {
            if ($selectedTab == 14) {
                $conditions = array_merge(array(
                    'Deal.coupon_expiry_date <=' => $today
                        ), $conditions);
            } elseif ($selectedTab == 15) {
                $conditions = array_merge(array(
                    'Deal.deal_status_id' => 8,
                    'Deal.end_date <=' => $today
                        ), $conditions);
            } else {
                $conditions = array_merge(array(
                    'Deal.deal_status_id' => $selectedTab
                        ), $conditions);
            }
        }
        return $conditions;
    }

    private function findSearchDealStatuses() {
        $conditions = array(
            'NOT' => array(
                'DealStatus.id' => array(
                    ConstDealStatus::Refunded,
                    ConstDealStatus::PaidToCompany,
                    ConstDealStatus::PendingApproval
                )
            )
        );
        $dealStatuses = $this->Deal->DealStatus->find('list', array(
            'conditions' => $conditions
        ));
        $dealStatuses[ConstDealStatus::Finalized] = 'Finalizada';
        $dealStatuses[ConstDealStatus::CicleEnded] = 'Fin de ciclo';
        $dealStatuses['99'] = 'Todas';
        return $dealStatuses;
    }

    private function findDealIdListByText($text) {
        $fields = array(
            'Deal.id'
        );
        return $this->Deal->search($text, array(
                    'fields' => $fields,
                    'conditions' => $this->defaultSearchConditions()
        ));
    }

    private function selectedValue($name, $default) {
        $selectedValue = $default;
        if (!empty($this->data['Deal'][$name])) {
            $selectedValue = $this->data['Deal'][$name];
        } elseif (!empty($this->params['named'][$name])) {
            $selectedValue = $this->params['named'][$name];
        }
        return $selectedValue;
    }

    private function isSearchByDealId() {
        $value = $this->selectedValue('qid', null);
        return !empty($value);
    }

    private function isSearchByCompanyId() {
        $value = $this->selectedValue('qcid', null);
        return !empty($value);
    }

    private function isSearchByText() {
        $value = $this->selectedValue('q', null);
        return !empty($value);
    }

    private function isSearchByDateRange() {
        $value = $this->selectedTab();
        return !in_array($value, array(1, 11)) && $this->selectedValue('date_range', '3') != 99;
    }

    private function paginateSearch($conditions) {
        $this->paginate = array(
            'fields' => array(
                '*',
                '(select username from users where users.id = Deal.seller_id) as seller_username',
                '(select first_name from user_profiles where user_profiles.user_id = Deal.seller_id) as seller_first_name',
                '(select last_name  from user_profiles where user_profiles.user_id = Deal.seller_id) as seller_last_name',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and deal_users.paid_date is not null and deal_users.deleted = 0) as `paid_coupons`',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and deal_users.paid_date is null and deal_users.deleted = 0) as `unpaid_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'A\') as `sold_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'P\' and ifnull(deal_externals.bac_substate,\'{null}\') not in (\'uninitialized\')) as `pending_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and ((deal_externals.external_status = \'A\') or (deal_externals.external_status = \'P\' and ifnull(deal_externals.bac_substate,\'{null}\') not in (\'uninitialized\')))) as `total_coupons`',
                '(select count(1) from anulled_coupons where anulled_coupons.deal_id = Deal.id ) as `anulled_coupons`',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and is_used = 1 ) as `used_coupons`',
                '(select sum(redemptions.discounted_price) from deal_users, redemptions where deal_users.deal_id = Deal.id and deal_users.id = redemptions.deal_user_id and is_used = 1 ) as `redeemed_amount`'
            ),
            'hint' => 'FORCE INDEX (PRIMARY)',
            'conditions' => $conditions,
            'order' => array(
                'Deal.id' => 'desc'
            ),
            'limit' => 20
        );
        return $this->paginate();
    }

    function admin_search() {
        $conditions = $this->defaultSearchConditions();
        $selectedTab = $this->selectedTab();
        if ($this->isSearchByCompanyId()) {
            $companyId = $this->selectedValue('qcid', null);
            $conditions = array_merge(array(
                'Deal.company_id' => $companyId
                    ), $conditions);
            $this->set('selectedQcid', $companyId);
        }
        if ($this->isSearchByDealId()) {
            $dealId = $this->selectedValue('qid', null);
            $conditions = array_merge(array(
                'Deal.id' => $dealId
                    ), $conditions);
            $this->set('selectedQid', $dealId);
        }
        if ($this->isSearchByText()) {
            $text = $this->selectedValue('q', null);
            $idList = $this->findDealIdListByText($text);
            if (!empty($idList)) {
                $conditions = array_merge(array(
                    'Deal.id' => Set::extract('/Deal/id', $idList)
                        ), $conditions);
            }
            $this->set('selectedQ', $text);
        }
        $dateRange = $this->selectedValue('date_range', '3');
        if ($this->isSearchByDateRange()) {
            if ($selectedTab == 99) {
                $conditions = array_merge(array(
                    'Deal.start_date >=' => date('Y-m-d', strtotime("-{$dateRange} month"))
                        ), $conditions);
            } else {
                $conditions = array_merge(array(
                    'Deal.start_date <=' => date('Y-m-d'),
                    'Deal.start_date >=' => date('Y-m-d', strtotime("-{$dateRange} month"))
                        ), $conditions);
            }
        }
        $this->set('dateRange', $dateRange);
        $deals = $this->paginateSearch($conditions);
        $this->set('deals', $deals);
        $dealStatuses = $this->findSearchDealStatuses();
        $moreActions = $this->Deal->moreActions($selectedTab);
        $this->set('dealStatuses', $dealStatuses);
        $this->set('selected', $selectedTab);
        $this->set('moreActions', $moreActions);
    }

    function admin_reindex() {
        die();
    }

    function deals_print() {
        $this->autoRender = false;
        if (!empty($this->params['named']['page_type']) && $this->params['named']['page_type'] == 'print') {
            $this->layout = 'print';
            if (!empty($this->params['named']['deal_id'])) {
                $conditions['Deal.id'] = $this->params['named']['deal_id'];
            }
            if (!empty($this->params['named']['filter_id']) && ($this->params['named']['filter_id'] != 'all')) {
                $conditions['Deal.deal_status_id'] = $this->params['named']['filter_id'];
            }
            $deals = $this->Deal->find('all', array(
                'conditions' => $conditions,
                'contain' => array(
                    'DealUser' => array(
                        'User' => array(
                            'UserProfile' => array(
                                'fields' => array(
                                    'UserProfile.dni',
                                    'UserProfile.first_name',
                                    'UserProfile.last_name'
                                )
                            ),
                            'fields' => array(
                                'User.id',
                                'User.username',
                                'User.email'
                            )
                        ),
                        'fields' => array(
                            'DealUser.id',
                            'DealUser.coupon_code',
                            'DealUser.discount_amount',
                            'DealUser.quantity',
                            'DealUser.is_gift'
                        )
                    )
                ),
                'fields' => array(
                    'Deal.id',
                    'Deal.name',
                    'Deal.deal_user_count',
                    'Deal.coupon_expiry_date'
                ),
                'recursive' => 3
            ));
            Configure::write('debug', 0);
            if (!empty($deals)) {
                foreach ($deals as $deal) {
                    foreach ($deal['DealUser'] as $dealusers) {
                        $data[]['Deal'] = array(
                            'dealname' => $deal['Deal']['name'],
                            'full_name' => $dealusers['User']['UserProfile']['last_name'] . ' ' . $dealusers['User']['UserProfile']['first_name'],
                            'dni' => $dealusers['User']['UserProfile']['dni'],
                            'is_gift' => $dealusers['is_gift'],
                            'gift_dni' => $dealusers['gift_dni'],
                            'gift_dob' => $dealusers['gift_dob'],
                            'username' => $dealusers['User']['username'],
                            'quantity' => $dealusers['quantity'],
                            'discount_amount' => $dealusers['discount_amount'],
                            'coupon_code' => $dealusers['coupon_code'],
                            'coupon_expiry_date' => $deal['Deal']['coupon_expiry_date']
                        );
                    }
                }
                $deal_list['deal_name'] = $deals['0']['Deal']['name'];
                $deal_list['deal_user_count'] = $deals['0']['Deal']['deal_user_count'];
                $this->set('deals', $data);
                $this->set('deal_list', $deal_list);
                $this->render('index_print_deal_users');
            }
        }
    }

    function admin_deals_print() {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('deals_print');
    }

    function admin_add() {
        AppModel::setDefaultDbConnection('master');
        $this->setAction('add');
    }

    function admin_listsimilares() {
        CakeLog::write('debug', 'listsimilares:company_id:' . $_POST["company_id"] . 'categgory_id:' . $_POST["company_id"]);
        $category_id = $_POST["category_id"];
        $company_id = $_POST["company_id"];
        $deal_id = $_POST["deal_id"];
        if ($category_id > 1) {
            CakeLog::write('debug', 'con categoria:');
            if ($deal_id == '') {
                $query = $this->Deal->find('all', array(
                    'fields' => array(
                        'id',
                        'name',
                        'start_date'
                    ),
                    'conditions' => array(
                        'deal_category_id' => $category_id,
                        'company_id' => $company_id
                    ),
                    'order' => 'start_date ASC'
                ));
            } else {
                $query = $this->Deal->find('all', array(
                    'fields' => array(
                        'id',
                        'name',
                        'start_date'
                    ),
                    'conditions' => array(
                        'deal_category_id' => $category_id,
                        'company_id' => $company_id,
                        'Deal.id !=' => $deal_id
                    ),
                    'order' => 'start_date ASC'
                ));
            }
        } else {
            CakeLog::write('debug', 'sin categroria');
            if ($deal_id == '') {
                $query = $this->Deal->find('all', array(
                    'fields' => array(
                        'id',
                        'name',
                        'start_date'
                    ),
                    'conditions' => array(
                        'company_id' => $company_id
                    ),
                    'order' => 'start_date ASC'
                ));
            } else {
                $query = $this->Deal->find('all', array(
                    'fields' => array(
                        'id',
                        'name',
                        'start_date'
                    ),
                    'conditions' => array(
                        'company_id' => $company_id,
                        'Deal.id !=' => $deal_id
                    ),
                    'order' => 'start_date ASC'
                ));
            }
        }
        $this->set('similares', $query);
        CakeLog::write('debug', 'tamanio:' . count($query));
        $this->layout = 'ajax';
    }

    function admin_edit($id = null) {
        $productInventoryStrategyOptions = $this->ProductInventoryStrategy->find('list');
        Debugger::log(sprintf('admin_edit - INICIO'), LOG_DEBUG);
        $paymentOptions = array();
        $this->set('paymentOptions', $paymentOptions);
        $this->set('errorPaymentOption', false);
        $isDraft = !empty($this->data['Deal']['save_as_draft']);
        AppModel::setDefaultDbConnection('master');
        $sellers = $this->_getSellersName();
        $this->set('sellers_id', $sellers);
        $dealTradeAgreementOptions = $this->DealTradeAgreement->find('list');
        $this->set('dealTradeAgreementOptions', $dealTradeAgreementOptions);
        $dealTradeAgreementConditions = $this->DealTradeAgreement->generateArrayOfConditions();
        $this->set('dealTradeAgreementConditions', $dealTradeAgreementConditions);
        $this->pageTitle = 'Editar oferta';
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $id = !empty($id) ? $id : $this->data['Deal']['id'];
        $this->ApiLogger->put('DealId', $id);
        $deal = $this->Deal->find('first', array(
            'conditions' => array(
                'Deal.id' => $id
            ),
            'recursive' => - 1
        ));
        if (empty($deal)) {
            $this->cakeError('error404');
        }
        if (!empty($deal['Deal']['deal_status_id']) && $deal['Deal']['deal_status_id'] == ConstDealStatus::Closed && !ConstUserTypes::isLikeSuperAdmin($this->Auth->user('user_type_id'))) {
            $this->cakeError('error404');
        }
        $attachment_newsletter = $this->Attachment->find('first', array(
            'conditions' => array(
                'Attachment.foreign_id = ' => $id,
                'Attachment.class = ' => 'DealNewsLetter'
            ),
            'recursive' => - 1
        ));
        $this->set('attachment_newsletter_id', $attachment_newsletter['Attachment']['id']);
        $this->set('paymentPeriods', $this->_formatPaymentPeriodsArray());
        if (!in_array($deal['Deal']['payment_term'], Configure::read('deal.paymentTerms'))) {
            $deal['Deal']['is_payment_term_exception'] = 1;
            $deal['Deal']['custom_payment_term'] = $deal['Deal']['payment_term'];
        }
        if (!empty($this->data)) {
            if (!empty($this->data['Deal']['is_shipping_address'])) {
                unset($this->Deal->validate['custom_company_address1']['rule1']);
            }
            if (is_null($this->data['Deal']['campaign_code']) || empty($this->data['Deal']['campaign_code'])) {
                $this->data['Deal']['campaign_code'] = $this->generateAlphaNumericSeedForCampaignCode();
                $this->data['Deal']['campaign_code_type'] = Deal::CAMPAIGN_CODE_TYPE_GENERATED;
            }
            $dealPriceDisplayConditions = $this->checkDealPriceDisplay($_POST['DisplayPrecio']);
            $this->data['Deal']['hide_price'] = $dealPriceDisplayConditions['hidePrice'];
            $this->data['Deal']['only_price'] = $dealPriceDisplayConditions['onlyPrice'];
            $this->data['Deal']['modified'] = date('Y-m-d h:i:s', time());
            $this->data['Deal'] = array_merge($deal['Deal'], $this->data['Deal']);
            $deal_city_slug = $this->City->findById($this->data['Deal']['city_id']);
            $deal_city_slug = $deal_city_slug['City']['slug'];
            if ($this->data['Deal']['is_payment_term_exception']) {
                $this->data['Deal']['payment_term'] = $this->data['Deal']['custom_payment_term'];
            }
            if ($this->Deal->is_subdeal($this->data)) {
                $this->Deal->unbindModel(array(
                    'hasOne' => array(
                        'Attachment'
                    )
                ));
                $parent = $this->Deal->findById($this->data['Deal']['parent_deal_id']);
                $this->data['Deal'] = array_merge($parent['Deal'], $this->data['Deal']);
                $this->data['Deal']['name'] = $parent['Deal']['name'] . ' - ' . $this->data['Deal']['descriptive_text'];
            }
            $dataCompany = $this->Deal->Company->findById($this->data['Deal']['company_id']);
            if (!empty($this->data['Deal']['send_to_admin']) && $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft) {
                $this->Deal->validate['start_date']['rule2'];
                $this->Deal->validate['coupon_start_date']['rule3'];
            } else {
                unset($this->Deal->validate['start_date']['rule2']);
                unset($this->Deal->validate['coupon_start_date']['rule3']);
            }
            $paymentOptions = empty($_POST['paymentOptionValue']) ? array() : $_POST['paymentOptionValue'];
            if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
                $this->PaymentOption = & ClassRegistry::init('PaymentOption');
                $paymentOptions = array_keys($this->PaymentOption->find('list', array(
                            'conditions' => array(
                                'payment_setting_id' => default_payment_setting_id($deal_city_slug, $this->data['Deal']['is_tourism'])
                            )
                )));
            }
            $this->set('paymentOptions', $paymentOptions);
            $this->data['Deal']['paymentOptionValue'] = $paymentOptions;
            $this->data['Deal']['is_product'] = $this->data['Deal']['portal'] == 'is_product';
            $currentSegmentationProfileId = $this->data['Deal']['segmentation_profile_id'];
            if ($this->_canCalcSegmentationProfile($this->data)) {
                $this->ApiLogger->debug('Se puede actualizar el segmentation profile de la oferta, calculando y asignando el profile que le corresponde.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id'],
                    'current_segmentation_profile_id' => $currentSegmentationProfileId
                ));
                $newSegmentationProfile = $this->_calcSegmentationProfileForDeal($this->data);
                $this->data['Deal']['segmentation_profile_id'] = $newSegmentationProfile['SegmentationProfile']['id'];
                $this->ApiLogger->debug('Segmentation profile asignado a la oferta.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id'],
                    'current_segmentation_profile_id' => $currentSegmentationProfileId,
                    'new_segmentation_profile' => $newSegmentationProfile
                ));
            } else {
                $this->ApiLogger->debug('No estan dadas las condiciones para actualizar el segmentation profile de la oferta.', array(
                    'gender' => $this->data['Deal']['gender'],
                    'deal_category_id' => $this->data['Deal']['deal_category_id'],
                    'current_segmentation_profile_id' => $currentSegmentationProfileId
                ));
            }
            $this->Deal->set($this->data);
            $logisticsAndStockErrors = array();
            if ($this->isLogisticsAndStockValidable() && !$this->isSaveAsDraftAction()) {
                $logisticsAndStockErrors = $this->OfferService->validateLogisticsAndStock($this->data['Deal']);
            }
            if ($isDraft || $this->Deal->validates()) {
                if ($isDraft) {
                    $this->data['Deal']['start_date'] = $this->_dateGuard($this->data['Deal']['start_date']);
                    $this->data['Deal']['end_date'] = $this->_dateGuard($this->data['Deal']['end_date']);
                    if (empty($this->data['Deal']['min_limit'])) {
                        $this->data['Deal']['min_limit'] = "0";
                    }
                    if (empty($this->data['Deal']['downpayment_percentage'])) {
                        $this->data['Deal']['downpayment_percentage'] = "0";
                    }
                }
                $continueProcess = empty($logisticsAndStockErrors);
                if (!empty($logisticsAndStockErrors)) {
                    $this->Session->setFlash('La oferta es de precompra con logÃ­stica propia y no cumple con las restricciones necesarias.', 'default', null, 'error');
                }
                if (!empty($this->data['Deal']['send_to_admin']) && !empty($this->data['Deal']['is_shipping_address'])) {
                    $this->ShippingAddressDeal = new ShippingAddressDeal();
                    if (!($this->ShippingAddressDeal->hasDealShippingAddressesByDealId($id))) {
                        $continueProcess = false;
                        $this->Session->setFlash('La oferta no se pudo actualizar: Debe asociarle Puntos de Retiro', 'default', null, 'error');
                    } else {
                        unset($this->Deal->validate['custom_company_address1']['rule1']);
                        if ($deal['Deal']['deal_status_id'] == ConstDealStatus::Draft) {
                            $this->data['Deal']['deal_status_id'] = ConstDealStatus::Upcoming;
                        }
                    }
                } elseif (!empty($this->data['Deal']['send_to_admin']) && empty($this->data['Deal']['is_shipping_address']) && $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft) {
                    $this->data['Deal']['deal_status_id'] = ConstDealStatus::Upcoming;
                }
                if ($continueProcess && $this->Deal->save($this->data, array(
                            'validate' => !$isDraft
                        ))) {
                    if (!empty($this->data['AttachmentMultiple'])) {
                        Debugger::log(sprintf('admin_edit - construyendo $this->data[AttachmentMultiple]'), LOG_DEBUG);
                        foreach ($this->data['AttachmentMultiple'] as $idx => $att) {
                            if (empty($att['filename']['name'])) {
                                unset($this->data['AttachmentMultiple'][$idx]);
                            } else {
                                $this->data['AttachmentMultiple'][$idx]['class'] = 'DealMultiple';
                                $this->data['AttachmentMultiple'][$idx]['filename']['type'] = get_mime($att['filename']['tmp_name']);
                                $this->data['AttachmentMultiple'][$idx]['foreign_id'] = $id;
                            }
                        }
                    }
                    if (!empty($this->data['Attachment']['filename']['name'])) {
                        $this->data['Attachment']['filename']['type'] = get_mime($this->data['Attachment']['filename']['tmp_name']);
                    }
                    if (!empty($this->data['Attachment']['filename']['name']) || (!Configure::read('image.file.allowEmpty') && empty($this->data['Attachment']['id']))) {
                        Debugger::log(sprintf('admin_edit - seteando datos $this->Deal->Attachment->set]'), LOG_DEBUG);
                        $this->Deal->Attachment->set($this->data);
                    }
                    $this->Deal->set($this->data);
                    if (!empty($this->data['Attachment']['filename'])) {
                        $is_attachment_error = true;
                        if (!empty($this->data['Attachment']['filename']['name'])) {
                            $data['Attachment']['filename'] = $this->data['Attachment']['filename'];
                            $this->Attachment->Behaviors->attach('ImageUpload', Configure::read('image.file'));
                            Debugger::log(sprintf('admin_edit - seteando $this->Attachment->set(data)'), LOG_DEBUG);
                            $this->Attachment->set($data);
                            if ($this->Attachment->validates() && $is_attachment_error) {
                                $is_attachment_error = true;
                            } else {
                                $is_attachment_error = false;
                            }
                        }
                    }
                    Debugger::log(sprintf('admin_edit -  $is_attachment_error:' . $is_attachment_error), LOG_DEBUG);
                    $foreign_id = $this->data['Deal']['id'];
                    $attach = $this->Attachment->findImageForDeal($foreign_id);
                    if (!empty($this->data['Attachment']['filename']['name'])) {
                        $this->data['Attachment']['filename'] = $this->data['Attachment']['filename'];
                        $this->data['Attachment']['class'] = $this->modelClass;
                        $this->data['Attachment']['description'] = 'Product Image';
                        $this->data['Attachment']['id'] = $attach['Attachment']['id'];
                        $this->data['Attachment']['foreign_id'] = $this->data['Deal']['id'];
                        $this->Attachment->Behaviors->attach('ImageUpload', Configure::read('product.file'));
                        $this->Attachment->set($this->data);
                        if ($this->Attachment->validates()) {
                            $this->Attachment->save($this->data['Attachment']);
                            foreach (glob('img/original/DealNewsLetter/' . $attach['Attachment']['id'] . '.*') as $filename) {
                                unlink($filename);
                            }
                        }
                    }
                    if (!empty($this->data['AttachmentMultipleRemove'])) {
                        foreach ($this->data['AttachmentMultipleRemove'] as $attId) {
                            $this->Attachment->delete(array(
                                'Attachment.id' => $attId,
                                'Attachment.class' => 'DealMultiple'
                            ));
                        }
                    }
                    if (!empty($this->data['AttachmentMultiple'])) {
                        $this->Deal->Attachment->saveAll($this->data['AttachmentMultiple']);
                    }
                    $attachment_newsletter = $this->Attachment->find('first', array(
                        'conditions' => array(
                            'Attachment.foreign_id = ' => $this->data['Deal']['id'],
                            'Attachment.class = ' => 'DealNewsLetter'
                        ),
                        'recursive' => - 1
                    ));
                    if (!empty($this->data['AttachmentNewsLetter']['filename']['name'])) {
                        $this->Attachment = new Attachment;
                        $this->Attachment->create();
                        if ($attachment_newsletter['Attachment']['id']) {
                            $this->data['AttachmentNewsLetter']['id'] = $attachment_newsletter['Attachment']['id'];
                        }
                        $this->data['AttachmentNewsLetter']['foreign_id'] = $this->data['Deal']['id'];
                        $this->data['AttachmentNewsLetter']['class'] = 'DealNewsLetter';
                        $this->data['AttachmentNewsLetter']['dir'] = 'DealNewsLetter/' . $this->data['Deal']['id'];
                        $this->Attachment->set(array(
                            'Attachment' => array(
                                'id' => $this->data['AttachmentNewsLetter']['id'],
                                'class' => 'DealNewsLetter',
                                'dir' => $this->data['AttachmentNewsLetter']['dir'],
                                'foreign_id' => $this->data['Deal']['id'],
                                'filename' => $this->data['AttachmentNewsLetter']['filename']
                            )
                        ));
                        if ($this->Attachment->validates()) {
                            $this->Attachment->save();
                            $this->Session->setFlash(__l('Deal has been updated'), 'default', null, 'success');
                            $this->redirect(array(
                                'controller' => 'deals',
                                'action' => 'index'
                            ));
                        } else {
                            $errornewsletter = $this->Attachment->invalidFields(array(
                                'Attachment'
                            ));
                            $this->Deal->validationErrors['AttachmentNewsletter']['filename'] = $errornewsletter;
                            $this->Deal->validationErrors['Attachment']['filename'] = '';
                            $this->Session->setFlash('La oferta no se pudo actualizar: Imagen del ', 'default', null, 'error');
                            $this->set('AttachmentNewsletterError', sprintf("<div class='error-message'>%s</div>", $errornewsletter['filename']));
                        }
                    }
                    $company_has_changed = ($this->data['Deal']['company_id'] != $deal['Deal']['company_id']);
                    $disableShippingAddress = ($deal['Deal']['is_shipping_address'] == 1) && ($this->data['Deal']['is_shipping_address'] == 0);
                    if ($company_has_changed || $disableShippingAddress) {
                        $this->ApiLogger->debug('CAMBIO EMPRESA.', null);
                        $this->ShippingAddressDeal = new ShippingAddressDeal();
                        if ($this->ShippingAddressDeal->hasDealShippingAddressesByDealId($id)) {
                            $this->ShippingAddressDeal->delMany($id);
                            $this->ApiLogger->debug('se borraron los puntos de retiro asociados.', null);
                            $this->ShippingAddressDeal->delManyFromSubdeals($id);
                            $this->ApiLogger->debug('se borraron los puntos de retiro asociados a sus Subofertas.', null);
                            if (!empty($this->data['Deal']['is_shipping_address'])) {
                                $this->ApiLogger->debug('tiene checkeado el Usa Puntos de Retiro.', null);
                                $conditions = array(
                                    'Deal.id' => $id
                                );
                                $this->Deal->updateAll(array(
                                    'Deal.deal_status_id' => ConstDealStatus::Draft
                                        ), $conditions);
                                $this->ApiLogger->debug('paso a Borrador, a la oferta y a sus Subofertas (si tuviera).', null);
                            } else {
                                $this->ApiLogger->debug('NO tiene checkeado el Usa Puntos de Retiro.', null);
                            }
                        } else {
                            $this->ApiLogger->debug('no tiene puntos de retiro asociados.', null);
                        }
                        if ($disableShippingAddress) {
                            $conditions = array();
                            $conditions['Deal.parent_deal_id'] = $id;
                            $this->ApiLogger->debug('DES-checkeo el Usa Puntos de Retiro.', null);
                            $this->Deal->unbindModel(array(
                                'hasOne' => array(
                                    'Attachment'
                                )
                            ));
                            $this->Deal->updateAll(array(
                                'Deal.is_shipping_address' => 0
                                    ), $conditions);
                            $this->ApiLogger->debug('ACTUALIZA IS_SHIPPING A 0.', null);
                        }
                    } else {
                        $this->ApiLogger->debug('NO CAMBIO EMPRESA.', null);
                    }
                    $this->Session->setFlash('Se actualizaron los datos de la oferta', 'default', null, 'success');
                    $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index'
                    ));
                }
            } else {
                if (isset($this->Deal->validationErrors['paymentOptionValue']) && !empty($this->Deal->validationErrors['paymentOptionValue']))
                    $this->set('errorPaymentOption', $this->Deal->validationErrors['paymentOptionValue']);
                $this->Session->setFlash(__l('Deal could not be added. Please, try again.'), 'default', null, 'error');
                if (empty($this->data['Deal']['max_limit']) && !empty($this->data['Deal']['has_pins'])) {
                    $this->Session->setFlash('La oferta no se pudo actualizar: Debes definir el Nro. mÃ¡ximo de compradores si la oferta tiene pines', 'default', null, 'error');
                    $this->set('errpins', 1);
                } else {
                    $this->Session->setFlash(__l('Deal could not be updated. Please, try again.'), 'default', null, 'error');
                    $this->set('errpins', 0);
                }
            }
            $attachment = $this->Attachment->findImageForDeal($this->data['Deal']['id']);
            $attachmentMultiple = $this->Attachment->findAdditionalImagesForDeal($this->data['Deal']['id']);
            $this->data['Attachment'] = $attachment['Attachment'];
            $this->data['AttachmentMultiple'] = $attachmentMultiple['AttachmentMultiple'];
            $this->data['Deal']['portal'] = $this->data['Deal']['is_tourism'] == 1 ? 'is_tourism' : $this->data['Deal']['is_product'] == 1 ? 'is_product' : 'is_normal';
        } else {
            $this->data = $this->Deal->find('first', array(
                'conditions' => array(
                    'Deal.id' => $id
                ),
                'contain' => array(
                    'Attachment'
                ),
                'recursive' => 0
            ));
            $this->data['Deal']['portal'] = $this->data['Deal']['is_tourism'] == 1 ? 'is_tourism' : ($this->data['Deal']['is_product'] == 1 ? 'is_product' : 'is_normal');
            $similar_deal_id = !empty($this->data['Deal']['similar_deal_id']) ? $this->data['Deal']['similar_deal_id'] : null;
            $textOfertaSimilar = 'Ninguna';
            if ($similar_deal_id == $this->data['Deal']['id'] || $similar_deal_id == NULL) {
                
            } else {
                $ofertaSimilar = $this->Deal->find('first', array(
                    'conditions' => array(
                        'Deal.id' => $similar_deal_id
                    )
                ));
                $textOfertaSimilar = '(' . $ofertaSimilar['Deal']['id'] . ')&nbsp;&nbsp;' . $ofertaSimilar['Deal']['name'];
            }
            $this->data['Deal']['portal'] = $this->data['Deal']['is_tourism'] == 1 ? 'is_tourism' : ($this->data['Deal']['is_product'] == 1 ? 'is_product' : 'is_normal');
            $this->set('textOfertaSimilar', $textOfertaSimilar);
            $paymentOptionDeals = $this->Deal->PaymentOptionDeal->find('all', array(
                'fields' => array(
                    'PaymentOptionDeal.payment_option_id',
                    'PaymentOptionDeal.payment_plan_id'
                ),
                'conditions' => array(
                    'deal_id' => $id
                ),
                'recursive' => 0
            ));
            list($paymentOptions, $paymentOptionPlan) = $this->getPaymentOptionsAndPlans($paymentOptionDeals);
            $this->set('paymentOptions', $paymentOptions);
            $this->set('paymentOptionPlans', $paymentOptionPlan);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle .= ' - ' . $this->data['Deal']['name'];
        $discounts = array();
        for ($i = 1; $i <= 100; $i++) {
            $discounts[$i] = $i;
        }
        $cities_conditions = array(
            'City.is_approved =' => 1
        );
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $cities_conditions[] = sprintf(' City.id IN (SELECT city_id from city_perms where city_perms.user_id = %d)', $this->Auth->user('id'));
        }
        $cities = $this->Deal->City->find('list', array(
            'conditions' => $cities_conditions,
            'order' => array(
                'City.name' => 'asc'
            )
        ));
        $deal_categories = $this->DealCategory->generatetreelist(null, null, null, '&nbsp;&nbsp;&nbsp;');
        $clusters = $this->Deal->Cluster->find('list', array(
            'order' => array(
                'Cluster.name' => 'asc'
            )
        ));
        $companies = $this->Deal->Company->find('list');
        asort($companies);
        $companies_is_tourism = $this->Deal->Company->find('list', array(
            'fields' => array(
                'Company.is_tourism'
            )
        ));
        $companies_is_end_user = $this->Deal->Company->find('list', array(
            'fields' => array(
                'Company.is_end_user'
            )
        ));
        $dealStatuses = $this->Deal->DealStatus->find('list');
        $this->set('deal', $deal);
        $this->set(compact('cities', 'dealStatuses', 'companies', 'discounts', 'clusters', 'deal_categories'));
        $this->set('companies_is_tourism', $companies_is_tourism);
        $this->set('companies_is_end_user', $companies_is_end_user);
        $this->set('productInventoryStrategyOptions', $productInventoryStrategyOptions);
        if ($this->Deal->is_subdeal($this->data)) {
            $this->set('parent_deal_id', $this->data['Deal']['parent_deal_id']);
            $this->set('is_subdeal', true);
        }
    }

    function getPaymentOptionsAndPlans($paymentOptionDeals) {
        $paymentOptions = array();
        $paymentOptionPlan = array();
        foreach ($paymentOptionDeals as $paymentOptionDeal) {
            $optionId = $paymentOptionDeal['PaymentOptionDeal']['payment_option_id'];
            $paymentOptions[] = $optionId;
            if (!empty($paymentOptionDeal['PaymentOptionDeal']['payment_plan_id'])) {
                $paymentOptionPlan[$optionId]['payment_plan_id'] = $paymentOptionDeal['PaymentOptionDeal']['payment_plan_id'];
            }
        }
        return array(
            $paymentOptions,
            $paymentOptionPlan
        );
    }

    function admin_delete($id = null) {
        Debugger::log("paso por " . __METHOD__, LOG_DEBUG);
    }

    function admin_update_billing_statement($redirect = true) {
        AppModel::setDefaultDbConnection('master');
        $this->autoRender = false;
        $sql = "UPDATE deals AS Deal SET                                                                 " . "  Deal.deal_user_count = (                                                               " . "    SELECT COUNT(1)                                                                      " . "    FROM deal_users AS DealUser                                                          " . "    WHERE DealUser.deal_id = Deal.id                                                     " . "  ),                                                                                     " . " Deal.total_purchased_amount = ROUND(                                                    " . "   Deal.discounted_price * Deal.deal_user_count                                          " . "  ),                                                                                     " . " Deal.total_commission_amount = ROUND(                                                   " . "   Deal.bonus_amount + (Deal.total_purchased_amount * (Deal.commission_percentage / 100))" . "  ),                                                                                     " . " Deal.modified = now()                                                                   " . "WHERE Deal.deal_status_id IN (                                                           " . "  6, 2, 5                                                                                " . ")                                                                                        " . "AND Deal.created >= DATE_ADD(CURDATE(), INTERVAL -180 DAY)                               ";
        $this->Deal->query($sql);
        $this->Session->setFlash(__l('Se actualizaron los montos necesarios para la liquidaciÃ³n.'), 'default', null, 'success');
        if ($redirect) {
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
        }
    }

    function admin_now_deals_management_single_deal_pay_to_company() {
        $nowDeal = array(
            $this->params['named']['now_deal_id']
        );
        $this->Deal->Behaviors->detach('Excludable');
        $this->autoRender = false;
        $r = 'now_deals_management/view:' . ConstDealStatus::Closed;
        $flash_message .= '';
        $flash_priority = '';
        if (empty($is_now_deal) && $this->_paidToCompanyNowDeals($nowDeal)) {
            $flash_message .= 'La liquidaciÃ³n se realizÃ³ con exito.';
            $flash_priority = 'success';
        } else {
            $flash_message .= 'Hubo un problema al enviar la liquidaciÃ³n.';
            $flash_priority = 'error';
        }
        $this->Session->setFlash(__l($flash_message), 'default', null, $flash_priority);
        $this->redirect($r);
    }

    function admin_update() {
        AppModel::setDefaultDbConnection('master');
        $this->autoRender = false;
        if (!empty($this->data['Deal'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $dealIds = array();
            foreach ($this->data['Deal'] as $deal_id => $is_checked) {
                if ($is_checked['id']) {
                    $dealIds[] = $deal_id;
                }
            }
            $dealIds = array_values($this->Deal->find('list', array(
                        'conditions' => array(
                            'OR' => array(
                                'parent_deal_id' => $dealIds,
                                'id' => $dealIds
                            )
                        ),
                        'fields' => array(
                            'id'
                        )
            )));
            if ($actionid && !empty($dealIds)) {
                if ($actionid == ConstDealStatus::Open) {
                    $dealsLeft = false;
                    foreach ($dealIds as $deal_id) {
                        $deal = $this->Deal->find('first', array(
                            'conditions' => array(
                                'Deal.id' => $deal_id
                            ),
                            'recursive' => - 1
                        ));
                        if (strtotime($deal['Deal']['end_date']) >= strtotime(date('Y-m-d H:i:s')) && strtotime($deal['Deal']['coupon_expiry_date']) >= strtotime(date('Y-m-d H:i:s'))) {
                            $this->Deal->updateAll(array(
                                'Deal.deal_status_id' => ConstDealStatus::Open,
                                'Deal.start_date' => '"' . date('Y-m-d H:i:s') . '"'
                                    ), array(
                                'Deal.id' => $deal_id
                            ));
                            $this->Deal->_processOpenStatus($deal_id);
                        } else {
                            $dealsLeft = true;
                        }
                    }
                    $msg = __l('Checked deals have been moved to open status. ');
                    if ($dealsLeft) {
                        $msg = 'Las ofertas seleccionadas se cambiaron al estado Abiertas. Algunas de las ofertas no fueron cambiadas de estado debido a que la fecha de cierre de vencimiento del cupon estÃ¡n en el pasado y/o se trataba de subofertas y no se esta actualizando la oferta padre.';
                    }
                    $this->Session->setFlash($msg, 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::Canceled) {
                    $openDealIds = $this->Deal->find('list', array(
                        'conditions' => array(
                            'Deal.id' => $dealIds,
                            'Deal.deal_status_id' => ConstDealStatus::Open
                        ),
                        'recursive' => - 1
                    ));
                    if (!empty($openDealIds)) {
                        $this->Deal->_refundDealAmount('update', array_keys($openDealIds));
                    }
                    $conditions['Deal.deal_status_id !='] = ConstDealStatus::Refunded;
                    $conditions['Deal.id'] = $dealIds;
                    $this->Deal->updateAll(array(
                        'Deal.deal_status_id' => ConstDealStatus::Canceled,
                        'Deal.deal_user_count' => 0,
                        'Deal.deal_tipped_time' => "'0000-00-00 00:00:00'",
                        'Deal.total_purchased_amount' => 0,
                        'Deal.total_commission_amount' => 0
                            ), $conditions);
                    $this->Session->setFlash(__l('Checked deals have been canceled'), 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::Rejected) {
                    $this->Deal->updateAll(array(
                        'Deal.deal_status_id' => ConstDealStatus::Rejected
                            ), array(
                        'Deal.id' => $dealIds
                    ));
                    $this->Session->setFlash(__l('Checked deals have been rejected'), 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::Refunded) {
                    $this->Deal->_refundDealAmount('admin_update', $dealIds);
                    $this->Session->setFlash(__l('Expired deals have been refunded'), 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::Closed) {
                    $this->Deal->_closeDeals($dealIds);
                    $this->Session->setFlash(__l('Checked deals have been closed'), 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::PaidToCompany) {
                    $flash_message = '';
                    $flash_priority = 'error';
                    $dealIdsGroupedByChannels = $this->_groupByChannel($dealIds);
                    $webDealIds = $dealIdsGroupedByChannels[Configure::read('publication_channel.web')];
                    $smsDealIds = $dealIdsGroupedByChannels[Configure::read('publication_channel.sms')];
                    if (!empty($webDealIds)) {
                        if ($this->_paidToCompanyWebDeals($webDealIds)) {
                            $flash_message .= 'La liquidaciÃ³n se realizÃ³ con exito.';
                            $flash_priority = 'success';
                        } else {
                            $flash_message .= 'Hubo un problema al enviar la liquidaciÃ³n.';
                            $flash_priority = 'error';
                        }
                    }
                    if (!empty($smsDealIds)) {
                        $flash_message .= '<br/>';
                        $flash_message .= 'Las siguientes ofertas no se procesaron, su medio de publicacion es SMS: ' . implode(', ', $dealIdsGroupedByChannels[Configure::read('publication_channel.sms')]);
                    }
                    $this->Session->setFlash(__l($flash_message), 'default', null, $flash_priority);
                } else if ($actionid == ConstDealStatus::Upcoming) {
                    $dealsLeft = false;
                    foreach ($dealIds as $deal_id) {
                        $deal = $this->Deal->find('first', array(
                            'conditions' => array(
                                'Deal.id' => $deal_id
                            ),
                            'fields' => array(
                                'Deal.end_date',
                                'Deal.coupon_expiry_date'
                            ),
                            'recursive' => - 1
                        ));
                        if (strtotime($deal['Deal']['end_date']) >= strtotime(date('Y-m-d H:i:s')) && strtotime($deal['Deal']['coupon_expiry_date']) >= strtotime(date('Y-m-d H:i:s'))) {
                            $this->Deal->updateAll(array(
                                'Deal.deal_status_id' => ConstDealStatus::Upcoming
                                    ), array(
                                'Deal.id' => $deal_id
                            ));
                        } else {
                            $dealsLeft = true;
                        }
                    }
                    $msg = __l('Checked deals have been moved to upcoming status. ');
                    if ($dealsLeft) {
                        $msg .= __l('Some of the deals are not upcoming due to the end date and coupon expiry date in past.');
                    }
                    $this->Session->setFlash($msg, 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::PendingApproval) {
                    $this->Deal->updateAll(array(
                        'Deal.deal_status_id' => ConstDealStatus::PendingApproval
                            ), array(
                        'Deal.id' => $dealIds
                    ));
                    $this->Session->setFlash(__l('Checked deals have been inactive'), 'default', null, 'success');
                } else if ($actionid == ConstDealStatus::TotallyPaid) {
                    $flash_message = '';
                    $flash_priority = 'error';
                    $dealIdsGroupedByChannels = $this->_groupByChannel($dealIds);
                    $webDealIds = $dealIdsGroupedByChannels[Configure::read('publication_channel.web')];
                    $smsDealIds = $dealIdsGroupedByChannels[Configure::read('publication_channel.sms')];
                    if (!empty($webDealIds)) {
                        $this->Deal->updateAll(array(
                            'Deal.deal_status_id' => ConstDealStatus::PaidToCompany
                                ), array(
                            'Deal.id' => $webDealIds
                        ));
                        $flash_message .= __l('Las ofertas seleccionadas se han marcado como pagadas');
                        $flash_priority = 'success';
                    }
                    if (!empty($smsDealIds)) {
                        $flash_message .= '<br/>';
                        $flash_message .= 'Las siguientes ofertas no se procesaron, su medio de publicacion es SMS: ' . implode(', ', $dealIdsGroupedByChannels[Configure::read('publication_channel.sms')]);
                    }
                    $this->Session->setFlash(__l($flash_message), 'default', null, $flash_priority);
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    public function _paidToCompanyNowDeals($dealIds) {
        return $this->_paidToCompanyDeals($dealIds, true);
    }

    public function _paidToCompanyWebDeals($dealIds) {
        return $this->_paidToCompanyDeals($dealIds);
    }

    public function _paidToCompanyDeals($dealIds, $is_now = false) {
        $dbo = $this->Deal->getDataSource();
        if ($is_now) {
            $subQuery = '(SELECT count(*) from deal_users WHERE deal_users.deal_id = `Deal`.id AND deal_users.is_used = 1)';
        } else {
            $subQuery = '(SELECT count(*) from deal_users WHERE deal_users.deal_id = `Deal`.id)';
        }
        $this->Deal->updateAll(array(
            'Deal.deal_user_count' => $subQuery,
            'Deal.modified' => '"' . date('Y-m-d H:i:s') . '"'
                ), array(
            'Deal.deal_status_id' => ConstDealStatus::Closed,
            'Deal.id' => $dealIds
        ));
        $this->Deal->updateAll(array(
            'Deal.total_purchased_amount' => '(Deal.discounted_price * Deal.deal_user_count)'
                ), array(
            'Deal.deal_status_id' => ConstDealStatus::Closed,
            'Deal.id' => $dealIds
        ));
        $this->Deal->updateAll(array(
            'Deal.total_commission_amount' => '(Deal.bonus_amount + ( Deal.total_purchased_amount * ( Deal.commission_percentage / 100 )))'
                ), array(
            'Deal.deal_status_id' => ConstDealStatus::Closed,
            'Deal.id' => $dealIds
        ));
        return $this->Deal->_payToCompany('admin_update', $dealIds, $is_now);
    }

    function _groupByChannel($dealIds) {
        $deals = $this->Deal->find('all', array(
            'conditions' => array(
                'Deal.id' => $dealIds
            ),
            'fields' => array(
                'Deal.id',
                'Deal.publication_channel_type_id'
            ),
            'recursive' => - 1
        ));
        $dealIdsGroupByChannel = array();
        foreach ($deals as $deal) {
            $dealIdsGroupByChannel[$deal['Deal']['publication_channel_type_id']][] = $deal['Deal']['id'];
        }
        return $dealIdsGroupByChannel;
    }

    function admin_update_status() {
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $this->Cron->update_deal();
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'index'
        ));
    }

    function cronjob_send_payment_reminder_offline() {
        $this->autoRender = false;
        echo "Inicializando cronjob send_payment_reminder_offline por APACHE\n";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->send_payment_reminder_offline();
        echo '<br />';
        echo "FINALIZADO cronjob por APACHE\n";
    }

    function cronjob_update_status() {
        $this->autoRender = false;
        echo "Inicializando cronjob UPDATE_STATUS por APACHE\n";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->update_deal();
        echo "FINALIZADO cronjob por APACHE\n";
        echo "Resultado del cronjob: \n";
    }

    function cronjob_send_deals_mailing() {
        $this->autoRender = false;
        echo "Inicializando cronjob cronjob_send_deals_mailing por APACHE\n";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->send_deals_mailing();
        echo "FINALIZADO cronjob por APACHE\n";
        echo "Resultado del cronjob: \n";
    }

    function cronjob_send_expiration_reminder() {
        $this->autoRender = false;
        echo "Inicializando cronjob cronjob_send_expiration_reminder por APACHE<br />";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->send_expiration_reminder();
        echo "FINALIZADO cronjob por APACHE<br />";
    }

    function cronjob_process_deal_status() {
        $this->autoRender = false;
        echo "Inicializando cronjob processDealStatus por APACHE\n";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->processDealStatus();
        echo "FINALIZADO cronjob por APACHE\n";
        echo "Resultado del cronjob: \n";
    }

    function cronjob_process_send_coupons() {
        $this->autoRender = false;
        echo "Inicializando cronjob processSendCoupons por APACHE<br />";
        AppModel::setDefaultDbConnection('master');
        App::import('Component', 'cron');
        $this->Cron = & new CronComponent();
        $ret = $this->Cron->processSendCoupons();
        echo "<br />FINALIZADO cronjob por APACHE<br />";
        echo "Resultado del cronjob: \n";
    }

    function buy_as_gift($deal_id = null, $quantity = 1, $circa_company = null, $circa_code = null) {
        $this->set('buyAsGift', true);
        $this->setAction('buy', $deal_id, $quantity, $circa_company, $circa_code);
    }

    function select_subdeal($deal_id = null, $quantity = 1, $circa_company = null, $circa_code = null) {
        $sub_deals = $this->Deal->find('all', array(
            'conditions' => array(
                'parent_deal_id' => $deal_id,
                'deal_status_id !=' => ConstDealStatus::Rejected
            ),
            'recursive' => - 1
        ));
        $parent = array();
        foreach ($sub_deals as $sub_deal) {
            if (!$this->Deal->is_subdeal($sub_deal)) {
                $parent = $this->Deal->find('first', array(
                    'conditions' => array(
                        'Deal.id' => $sub_deal['Deal']['id']
                    ),
                    'contain' => array(
                        'Attachment'
                    )
                ));
                break;
            }
        }
        $sub_deals_updated = array();
        foreach ($sub_deals as $sub_deal) {
            $dealIsAvailableForBuy = $this->checkIfDealIsAccesibleForBuy($sub_deal['Deal']);
            $sub_deal['Deal']['dealIsAvailableForBuy'] = $dealIsAvailableForBuy;
            array_push($sub_deals_updated, $sub_deal);
        }
        $this->set('sub_deals', $sub_deals_updated);
        $this->set('parent', $parent);
        $this->set('circa_company', $circa_company);
        $this->set('circa_code', $circa_code);
    }

    function buy_without_subdeal($deal_id = null, $quantity = 1, $circa_company = null, $circa_code = null, $is_fibertel_user_ok = false) {
        $this->ApiLogger->put('deal', $deal_id);
        $this->ApiLogger->notice('Iniciando compra de suboferta.', array(
            'quantity' => $quantity,
            'circa_company' => $circa_company,
            'is_fibertel_deal_user_ok' => $is_fibertel_user_ok
        ));
        try {
            $deal = $this->Deal->findById($deal_id);
            $buy_max_quantity_per_user = $deal['Deal']['buy_max_quantity_per_user'] != null ? intval($deal['Deal']['buy_max_quantity_per_user']) : 9999999999;
            $buy_max_quantity = intval($deal['Deal']['max_limit']);
            $user_id = $this->Auth->user('id');
            $deal_externals_count = null;
            $fail_message = 'Ya alcanzaste tu lÃ­mite mÃ¡ximo de compra para esta oferta. Pod&eacute;s seguir buscando otras ofertas';
            $aprobado = true;
            if ($user_id) {
                $deal_externals_count = $this->DealExternal->findQuantityGoupByUserIdDealId($user_id, $deal_id);
                $aprobado = $deal_externals_count < $buy_max_quantity_per_user;
                if ($deal['Deal']['max_limit'] != null) {
                    $buy_max_quantity = intval($deal['Deal']['max_limit']);
                    $aprobado = $deal_externals_count < $buy_max_quantity;
                    $fail_message = 'Ya alcanzaste el lÃ­mite mÃ¡ximo de compra para esta oferta.';
                }
            }
            if ($aprobado) {
                $this->ApiLogger->notice('Redireccionando a la accion de comprar.');
                if (isset($_GET['buyAsGift']) && $_GET['buyAsGift']) {
                    $this->set('buyAsGift', true);
                    $this->setAction('buy', $deal_id, $quantity, $circa_company, $circa_code, true, $is_fibertel_user_ok);
                } else {
                    $this->setAction('buy', $deal_id, $quantity, $circa_company, $circa_code, true, $is_fibertel_user_ok);
                }
            } else {
                $this->ApiLogger->notice('El usuario alcanzo el maximo de compras por usuario de la oferta.', array(
                    'max_limit' => $buy_max_quantity,
                    'deal_externals_count' => $deal_externals_count,
                    'buy_max_quantity_per_user' => $buy_max_quantity_per_user
                ));
                $this->ApiLogger->notice('Redireccionando a la home por validacion de cantidad maxima de compras con mensaje.', array(
                    'message' => $fail_message,
                    'severity' => 'error'
                ));
                $this->Session->setFlash($fail_message, 'default', null, 'error');
                $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
                ));
            }
        } catch (Exception $e) {
            $this->ApiLogger->error('Error al intentar comprar una suboferta.', array(
                'error' => $e->getMessage()
            ));
            $this->ApiLogger->notice('Redireccionando a la home por error.', array(
                'error' => $e->getMessage()
            ));
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
        }
    }

    private function hasUpdateBacUser($user, $deal_start_date) {
        if ($user['User']['bac_id'] == 0 && strtotime($deal_start_date) >= Configure::read('tec_dia.start_date')) {
            $ret = true;
        } else {
            $ret = false;
        }
        return $ret;
    }

    private function redirectOnInvalidExternalHiddenCityBuyAttemp() {
        $this->Session->setFlash('La oferta no esta disponible', true);
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'index',
            'city' => 'ciudad-de-buenos-aires'
        ));
    }

    function buy($deal_id = null, $quantity = 1, $circa_company = null, $circa_code = null, $subdeal_selected = false, $is_fibertel_user_ok = false) {
        $error = 2;
        $this->ApiLogger->put('deal', $deal_id);
        $this->ApiLogger->notice('Iniciando compra.', array(
            'quantity' => $quantity,
            'circa_company' => $circa_company,
            'subdeal_selected' => $subdeal_selected,
            'is_fibertel_deal_user_ok' => $is_fibertel_user_ok
        ));
        $this->set('PaymentTypesMP', Configure::read('PaymentTypesMP'));
        $this->disableCache();
        $quantity = isset($this->params['named']['quantity']) ? $this->params['named']['quantity'] : $quantity;
        $hideSubscription = $this->isSubscriptionEnabledForCurrentCity($this->params['named']['city']);
        $this->set('hideSubscription', $hideSubscription);
        $this->set('is_subdeal_selected', $subdeal_selected);
        $this->layout = 'dynamic-default';
        if ($this->Auth->user('id')) {
            $this->set('logged_user_on_cc', true);
        }
        if ($this->NowDeal->field('deal_status_id', array(
                    'NowDeal.id' => $deal_id
                ))) {
            $this->Deal->Behaviors->detach('Excludable');
        }
        $circa = Configure::read('CIRCA');
        $city_slug = $this->params['named']['city'];
        $this->set('certificaPath', 'ClubCupon/deal/buy');
        if ((!$this->Deal->User->isAllowed($this->Auth->user('user_type_id')))) {
            $this->ApiLogger->notice('El tipo de usuario no puede comprar. No se puede seguir con la compra.', array(
                'user_type_id' => $this->Auth->user('user_type_id')
            ));
            $this->ApiLogger->notice('Redireccionando a pagina de error 404 por validacion de tipo de usuario.');
            $this->cakeError('error404');
        }
        if ((is_null($deal_id) && empty($this->data['Deal']['deal_id']))) {
            $this->ApiLogger->notice('El parametro deal_id es vacio. No se puede seguir con la compra.');
            $this->ApiLogger->notice('Redireccionando a pagina de error 404 por validacion del parametro deal_id.');
            $this->cakeError('error404');
        }
        if (!empty($this->data['Deal']['circa_company'])) {
            $circa_company = $this->data['Deal']['circa_company'];
        }
        if (!empty($this->data['Deal']['circa_code'])) {
            $circa_code = $this->data['Deal']['circa_code'];
        }
        if (!empty($this->data['Deal']['deal_id'])) {
            $deal_id = $this->data['Deal']['deal_id'];
        }
        if (!empty($this->data['Deal']['quantity'])) {
            $quantity = $this->data['Deal']['quantity'];
        }
        if ($city_slug == 'circa' && (is_null($circa_company) || is_null($circa_code) || !array_key_exists($circa_company, $circa) || !in_array($circa_code, $circa[$circa_company]))) {
            $this->ApiLogger->notice('La compra es con CIRCA y los parametros relacionados con CIRCA no son validos. No se puede seguir con la compra.', array(
                'circa_company' => $circa_company,
                'circa_code' => $circa_code
            ));
            $this->ApiLogger->notice('Redireccionando a la home por validacion CIRCA con el mensaje.', array(
                'message' => 'Codigo incorrecto',
                'severity' => 'error'
            ));
            $this->Session->set('C&oacute;digo incorrecto', 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
        }
        $this->Deal->Behaviors->detach('Excludable');
        $deal = $this->Deal->find('first', array(
            'conditions' => array(
                'Deal.id' => $deal_id,
                'Deal.deal_status_id' => array(
                    ConstDealStatus::Open,
                    ConstDealStatus::Tipped
                )
            ),
            'recursive' => 0
        ));
        try {
            if ($this->Auth->user('id')) {
                $data = $this->Deal->User->find('first', array(
                    'conditions' => array(
                        'User.id' => $this->Auth->user('id')
                    ),
                    'recursive' => - 1
                ));
                if (($data && $data['User']['bac_user_id'] == 0) || $this->hasUpdateBacUser($data, $deal['Deal']['start_date'])) {
                    $this->Deal->User->createBacUser($this->Auth->user('id'), $deal['Deal']['is_tourism'], $deal['Deal']['start_date']);
                }
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $availableQuantity = $this->ProductInventoryService->availableQuantity($deal['Deal']['id']);
        Debugger::log('1) $$availableQuantity : ' . $availableQuantity);
        if ($this->Auth->user('id')) {
            $couponsBoughtByUser = $this->DealExternal->findQuantityGoupByUserIdDealId($this->Auth->user('id'), $deal['Deal']['id']);
            Debugger::log('$couponsBoughtByUser : ' . $couponsBoughtByUser);
            $availableQuantityByUser = $deal['Deal']['buy_max_quantity_per_user'] - $couponsBoughtByUser;
            $deal['Deal']['available_qty'] = min($availableQuantity, $availableQuantityByUser);
        } else {
            $deal['Deal']['available_qty'] = $availableQuantity;
        }
        Debugger::log('2) $$availableQuantity : ' . $deal['Deal']['available_qty']);
        if ($is_fibertel_user_ok == true && $deal['Deal']['is_fibertel_deal'] == 1 && $this->Cookie->read('fibertelUserLogged') != '1') {
            $fail_message = 'Esta oferta solo es accesible para usuarios de FIBERTEL, autenticados.';
            $this->ApiLogger->notice('Compra fibertel, el usuario no esta logueadon en FIBERTEL. No se puede seguir con la compra.', array(
                'fibertelUserLogged' => $this->Cookie->read('fibertelUserLogged')
            ));
            $this->ApiLogger->notice('Redireccionando a la home por validacion de logueado en FIBERTEL con mensaje.', array(
                'message' => $fail_message,
                'severity' => 'error'
            ));
            $this->Session->setFlash($fail_message, 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index',
                'admin' => false
            ));
        }
        if ($is_fibertel_user_ok == false && !empty($deal['Deal']['is_fibertel_deal']) && $this->Cookie->read('fibertelUserLogged') != '1') {
            
        }
        $user = $this->Deal->User->find('first', array(
            'conditions' => array(
                'User.id' => $this->Auth->user('id')
            ),
            'contain' => array(
                'UserProfile'
            ),
            'recursive' => 2
        ));
        if (empty($deal)) {
            $this->ApiLogger->notice('La oferta no existe o esta en un estado no permitido para la comprar.', array(
                'deal_id' => $deal_id
            ));
            $this->ApiLogger->notice('Redireccionando a pagina de error 404 por validacion de Deal.');
            $this->cakeError('error404');
        }
        $this->pageTitle = 'Comprar ' . $deal['Deal']['name'] . ' - ' . Configure::read('site.name');
        $this->log('titulo:' . $this->pageTitle);
        $title_for_layout = $this->pageTitle;
        $this->set('title_for_layout', $title_for_layout);
        $user_available_balance = 0;
        $user_available_points = 0;
        $user_wallet_blocked = null;
        if ($this->Auth->user('id')) {
            $user_available_balance = $this->Deal->User->checkUserBalance($this->Auth->user('id'));
            $user_wallet_blocked = $this->Deal->User->checkUserWalletBlocked($this->Auth->user('id'));
            $user_available_points = $this->Deal->User->get_current_points($this->Auth->user('id'));
        }
        if (in_array($deal['City']['slug'], Configure::read('corporate.external_hidden_cities')) && !strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST'])) {
            $deal_id = $_POST["deal_id"];
            $virginia_slims_code = $_POST["regid"];
            if (empty($deal_id) && empty($token)) {
                $this->ApiLogger->notice('Compra VirginiaSlims, el usuario ingreso la URL directamente, no viene desde el sitio de VirgniaSlims, redireccionamos.');
                $this->redirectOnInvalidExternalHiddenCityBuyAttemp();
            } else {
                $is_external_post_buy_ok = true;
            }
        }
        if (!empty($this->data) && !$this->data['User']['only_register']) {
            if (isset($this->data['Deal']['userprofile_dni']) && !empty($this->data['Deal']['userprofile_dni'])) {
                $this->Deal->User->UserProfile->id = $user['UserProfile']['id'];
                $this->Deal->User->UserProfile->saveField('dni', $this->data['Deal']['userprofile_dni']);
            }
            if (in_array($deal['City']['slug'], Configure::read('corporate.external_hidden_cities')) && $is_external_post_buy_ok) {
                $this->Deal->User->UserProfile->id = $user['UserProfile']['id'];
                $this->Deal->User->UserProfile->saveField('virginia_slims_code varchar', $this->Session->read('deal.externalToken'));
            }
            if (!empty($this->data['Deal']['is_fibertel_deal'])) {
                if ($fibertel_user_verification == false) {
                    $fail_message = 'TenÃ©s que estar acreditado como usuario de FIBERTEL para comprar esta oferta!. ';
                    $this->ApiLogger->notice('Compra fibertel, el usuario no esta acreditado como usuario FIBERTEL.');
                    $this->ApiLogger->notice('Redireccionando a la home por validacion de acreditado como usuario FIBERTEL con mensaje.', array(
                        'message' => $fail_message,
                        'severity' => 'error'
                    ));
                    $this->Session->set($fail_message, 'default', null, 'error');
                    $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'index'
                    ));
                }
            }
            $parsedPayment = explode('_', $this->data['Deal']['payment_type_id']);
            if (count($parsedPayment) !== 4) {
                $this->ApiLogger->notice('El parametro de con el medio de pago seleccionado no esta en el formato correcto "xxx_xxx_xxx_xxx". No se puede seguir con la compra.', array(
                    'payment_type_id' => $this->data['Deal']['payment_type_id']
                ));
                $this->ApiLogger->notice('Redireccionando a pagina de error 404 por validacion de formato del parametro con el medio de pago seleccionado.');
                $this->cakeError('error404');
            }
            list($id_gateway, $payment_type_id, $paymentOptionId, $bac_payment_type) = $parsedPayment;
            if (!isset($this->data['DealExternal']['used_points'])) {
                $this->data['DealExternal']['used_points'] = false;
            }
            if ($id_gateway == 5000 && $payment_type_id == 5001) {
                if ($this->Auth->user('id')) {
                    $total_deal_amount = Precision::mul($deal['Deal']['discounted_price'], $this->data['Deal']['quantity']);
                    list($needed_points, $needed_wallet) = $this->Deal->User->needed_points_and_money($this->Auth->user('id'), $total_deal_amount);
                    $this->Deal->User->updateAll(array(
                        'User.available_balance_amount' => 'User.available_balance_amount + ' . $needed_wallet
                            ), array(
                        'User.id' => $this->Auth->user('id')
                    ));
                    $this->data['Deal']['points'] = $needed_points;
                    $this->data['DealExternal']['used_points'] = true;
                }
                $payment_type_id = 5000;
            }
            $this->data['Deal']['payment_type_id'] = $payment_type_id;
            $this->data['Deal']['id_gateway'] = $id_gateway;
            $this->data['Deal']['bac_payment_type'] = $bac_payment_type;
            if (!$this->Auth->user('id')) {
                $this->Deal->User->set($this->data);
                $this->Deal->User->validates();
            } else {
                if (isset($this->data['Deal']['notifications']) && ($this->data['Deal']['notifications']) != '0') {
                    $this->_subscribeForBuyLoggedUser();
                }
            }
            if (empty($this->data['Deal']['user_id'])) {
                unset($this->Deal->validate['user_id']);
            }
            if (!$this->data['Deal']['is_gift']) {
                unset($this->data['Deal']['gift_to']);
                unset($this->data['Deal']['gift_email']);
                unset($this->data['Deal']['confirm_gift_email']);
                unset($this->data['Deal']['gift_dni']);
                unset($this->data['Deal']['gift_dob']);
                unset($this->data['Deal']['gift_message']);
                $giftData = array(
                    'is_gift' => false
                );
            } else {
                $giftData = array(
                    'is_gift' => true,
                    'to' => $this->data["Deal"]["gift_to"],
                    'email' => $this->data["Deal"]["gift_email"],
                    'message' => $this->data["Deal"]["gift_message"],
                    'dni' => $this->data["Deal"]["gift_dni"],
                    'dob' => ''
                );
                if (!empty($this->data["Deal"]["gift_dob"]['day'])) {
                    $giftData['dob'] = $this->data["Deal"]["gift_dob"]['year'] . '-' . $this->data["Deal"]["gift_dob"]['month'] . '-' . $this->data["Deal"]["gift_dob"]['day'];
                }
            }
            $this->Deal->set($this->data);
            $total_deal_amount = $this->data['Deal']['total_deal_amount'] = Precision::mul($deal['Deal']['discounted_price'], $this->data['Deal']['quantity']);
            $this->Deal->validates();
            $user_details_updated = true;
            if (!empty($user['User']['fb_user_id']) && empty($user['User']['email'])) {
                $this->data['User']['id'] = $this->Auth->user('id');
                $this->Deal->User->set($this->data['User']);
                if ($this->Deal->User->validates() && empty($this->Deal->User->validationErrors)) {
                    $this->Deal->User->save($this->data['User']);
                } else {
                    $user_details_updated = false;
                }
            }
            if (empty($this->Deal->validationErrors) && $user_details_updated) {
                try {
                    App::import('Component', 'AccountingEventService');
                    $AccountingEventService = new AccountingEventServiceComponent();
                    $AccountingEventService->registerAccountingEvent($deal);
                } catch (Exception $e) {
                    $this->ApiLogger->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                        'errorCode' => $e->getMessage()
                    ));
                }
                if (!$this->Auth->user('id') && isset($this->data['User'])) {
                    $this->_registerUser();
                }
                $this->data['DealExternal']['payment_option_id'] = $paymentOptionId;
                $giftAvailableAmount = 0;
                $bacPin = null;
                if (isset($this->data['gift_pin_code']) && !empty($this->data['gift_pin_code'])) {
                    $giftPinCode = $this->data['gift_pin_code'];
                    $bacPin = $this->GiftPinService->isUsable($giftPinCode);
                    if (!$bacPin) {
                        throw new Exception("The gift pin code is not valid or already used.");
                    }
                    $giftAvailableAmount = $bacPin['monto'];
                }
                if ($this->Auth->user('id') && $payment_type_id == ConstPaymentTypes::Wallet && ($user_available_balance + $giftAvailableAmount) >= $total_deal_amount && ((!$user_wallet_blocked && empty($giftPinCode)) || (!empty($giftPinCode)))) {
                    Debugger::log(sprintf('buy DealId: %d,quantity: %d - Compra por Wallet y tiene suficiente dinero para hacerlo.', $deal_id, $this->data['Deal']['quantity']), LOG_DEBUG);
                    $dealExternal = $this->Deal->externalPayment($this->data['Deal']['deal_id'], $quantity, $this->data['Deal']['user_id'], $payment_type_id, 'A', $this->data['DealExternal']['used_points'], $giftData, true);
                    $this->ProductInventoryService->decrease($this->data['Deal']['deal_id'], $this->data['Deal']['quantity']);
                    $walletUsedAmount = 0;
                    if (!empty($bacPin)) {
                        $giftUsedAmount = $this->calculateGiftUsedAmount($bacPin['monto'], $total_deal_amount);
                        $giftPin = $this->GiftPinService->burnGiftPin($giftPinCode, $dealExternal['DealExternal']['user_id']);
                        $this->data['DealExternal']['gift_pin_id'] = $giftPin['GiftPin']['id'];
                        $this->data['DealExternal']['gift_pin_code'] = $giftPinCode;
                        $this->data['DealExternal']['gifted_amount'] = $giftUsedAmount;
                        $this->DealExternal->save($this->data);
                        $this->Deal->addAmountToWallet($giftUsedAmount, $this->Auth->user('id'), 'add amount to wallet for gift pin code ' . $bacPin['monto']);
                        $walletUsedAmount = $total_deal_amount - $giftUsedAmount;
                    } else {
                        $walletUsedAmount = $total_deal_amount;
                    }
                    $this->data['DealExternal'] = $dealExternal['DealExternal'];
                    $this->data['DealExternal']['parcial_amount'] = 0;
                    $this->data['DealExternal']['final_amount'] = $total_deal_amount;
                    $this->data['DealExternal']['internal_amount'] = $walletUsedAmount;
                    $this->data['DealExternal']['payment_option_id'] = $paymentOptionId;
                    $this->data['DealExternal']['payment_plan_option_id'] = $this->data['Deal']['cantidad_cuotas'];
                    if ($this->data['Deal']['is_gift']) {
                        $this->data['Deal']['gift_data'] = $giftData;
                    }
                    $this->_buyDeal($this->data, false);
                    $this->Deal->DealExternal->save($this->data);
                    if ($this->_isFromAdwords()) {
                        $this->_trackBuyOfAdwords($this->data['DealExternal']['id']);
                    }
                    $this->set('certificaPath', 'ClubCupon/deal/buyOk');
                    $this->Deal->DealExternal->recursive = 1;
                    $this->Deal->recursive = 1;
                    $dealExternal = $this->Deal->DealExternal->read(NULL, $this->data['DealExternal']['id']);
                    $deal = $this->Deal->read(NULL, $dealExternal['DealExternal']['deal_id']);
                    $internalAmount = $dealExternal['DealExternal']['internal_amount'];
                    if ($deal['Deal']['is_now'] == 1) {
                        $this->Deal->Behaviors->detach('Excludable');
                        $attach = $this->Deal->Attachment->findImageForNowDeal($deal['Deal']['id']);
                        $deal['Attachment'] = $attach['Attachment'];
                    }
                    $this->set(compact('dealExternal', 'deal', 'internalAmount'));
                    $this->render("buyOk");
                } else {
                    Debugger::log(sprintf('buy DealId: %d,quantity: %d - Usuario no logueado o usuario logueado, pagando con bac o pagando con Wallet..', $deal_id, $this->data['Deal']['quantity']), LOG_DEBUG);
                    $this->process_user($deal);
                }
                $error = 0;
            } else {
                $this->ApiLogger->notice('La oferta o el usuario no son validos, no se los puede actualizar.', array(
                    'deal.validationErrors' => $this->Deal->validationErrors,
                    'user.validationErrors' => $this->Deal->User->validationErrors,
                    'deal.data' => $this->Deal->data['Deal'],
                    'user.data' => $this->Deal->User->data['User']
                ));
                $error = 1;
            }
            if (!$this->Auth->user('id')) {
                unset($this->data['User']['passwd']);
                unset($this->data['User']['confirm_password']);
            }
            $deal['Deal']['max_limit'] = empty($deal['Deal']['max_limit']) ? 99999999 : $deal['Deal']['max_limit'];
            $hastocknow = ($this->data['Deal']['quantity'] + $deal['Deal']['deal_user_count']) < $deal['Deal']['max_limit'];
            if ($this->Deal->isNow($deal) && $this->Auth->user('id') && !$hastocknow) {
                App::import('Model', 'now.NowBranchesDeals');
                $this->NowBranchesDeals = new NowBranchesDeals;
                $this->NowBranchesDeals->deletesoftByDealId($deal_id);
            }
        } else {
            if (isset($this->data['User']['only_register']) && $this->data['User']['only_register']) {
                $this->data['User']['confirm_email'] = $this->data['User']['email'];
                if ($this->_registerUser()) {
                    $this->redirect(Router::url('/', true) . $this->data['User']['fwd'] . '/' . $this->data['User']['quantity']);
                } else {
                    $this->set('has_register_errors', 1);
                }
            }
            if (isset($deal['Deal']['is_clarin365_deal']) && $deal['Deal']['is_clarin365_deal'] && !$this->_isValidClarin365CardNumber($_POST['clarin365CardNumber'])) {
                $this->Session->setFlash("No se pudo continuar con su compra por favor ingrese un Nro de Tarjeta Clarin 365 valido.");
                $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $quantity = (int) $quantity;
            if ($quantity == '' || $quantity == 0) {
                $quantity = 1;
            }
            $this->data['Deal']['is_gift'] = 0;
            $this->data['Deal']['quantity'] = $quantity;
            $this->data['Deal']['deal_amount'] = $deal['Deal']['discounted_price'];
            $this->data['Deal']['buy_min_quantity_per_user'] = $deal['Deal']['buy_min_quantity_per_user'];
            $this->data['Deal']['max_limit'] = $deal['Deal']['max_limit'];
            $this->data['Deal']['minimal_amount_financial'] = $deal['Deal']['minimal_amount_financial'];
            $this->data['Deal']['deal_id'] = $deal_id;
            $this->data['Deal']['total_deal_amount'] = Precision::mul($deal['Deal']['discounted_price'], $quantity);
            $this->Deal->validate = array_merge($this->Deal->validate, $this->Deal->validateCreditCard);
            $subDeals = $this->Deal->find('all', array(
                'conditions' => array(
                    'parent_deal_id' => $deal_id,
                    'id <>' => $deal_id,
                    'deal_status_id' => array(
                        ConstDealStatus::Open,
                        ConstDealStatus::Tipped
                    )
                ),
                'recursive' => - 1
            ));
            if ((count($subDeals) != 0) && !$subdeal_selected) {
                $this->setAction('select_subdeal', $deal_id, $quantity, $circa_company, $circa_code);
            }
            if ($this->Auth->user('id') && count($subDeals) == 0) {
                $deal_external_count = $this->DealExternal->findQuantityGoupByUserIdDealId($this->Auth->user('id'), $deal_id);
                $isEligibleForBuy = $deal_external_count < ($deal['Deal']['buy_max_quantity_per_user'] != null ? $deal['Deal']['buy_max_quantity_per_user'] : 9999999999);
                if (!$isEligibleForBuy) {
                    $this->Session->setFlash(sprintf(__l("You can't buy this deal. Your maximum allowed limit %s is over"), $deal['Deal']['buy_max_quantity_per_user']), 'default', null, 'error');
                    if ($this->Deal->isNow($deal)) {
                        $this->_redirectToNowDealDetail($deal);
                    } else {
                        $this->_redirectToDealView($deal);
                    }
                }
            }
            $walletPaymentOptions = $this->Deal->PaymentOptionDeal->find('all', array(
                'conditions' => array(
                    'PaymentOptionDeal.deal_id' => $deal_id,
                    'PaymentOption.payment_type_id' => 5000
                ),
                'contain' => array(
                    'PaymentOption'
                )
            ));
            $otherPaymentOptions = $this->Deal->PaymentOptionDeal->find('all', array(
                'conditions' => array(
                    'PaymentOptionDeal.deal_id' => $deal_id,
                    'not' => array(
                        'PaymentOption.payment_type_id' => array(
                            5000
                        )
                    )
                ),
                'contain' => array(
                    'PaymentOption'
                )
            ));
            if (count($otherPaymentOptions) == 0) {
                if (count($walletPaymentOptions) != 0) {
                    if ($this->Auth->user('id') && ($user_wallet_blocked || ($user_available_balance < ($deal['Deal']['discounted_price'] * $quantity)))) {
                        $this->Session->setFlash('No dispone de medios de pago para comprar esta oferta en este momento', 'default', null, 'error');
                        $this->redirect(array(
                            'controller' => 'deals',
                            'action' => 'index'
                        ));
                    }
                }
            }
        }
        $total_bought_user = $this->Deal->countUserDeals($deal_id, $this->Auth->user('id'));
        $this->set('total_bought_user', $total_bought_user);
        $this->data['Deal']['user_id'] = $this->Auth->user('id');
        $this->data['Deal']['circa_company'] = $circa_company;
        $this->data['Deal']['circa_code'] = $circa_code;
        $this->set('circa_company', $circa_company);
        $this->set('circa_code', $circa_code);
        $this->set('deal', $deal);
        $this->set('user', $user);
        $this->set('user_available_balance', $user_available_balance);
        $this->set('user_wallet_blocked', $user_wallet_blocked);
        $this->set('user_available_points', $user_available_points);
        $this->set('error', $error);
    }

    private function calculateGiftUsedAmount($giftAvailableAmount, $totalGrossAmount) {
        $giftUsedAmount = 0;
        if ($totalGrossAmount <= $giftAvailableAmount) {
            $giftUsedAmount = $totalGrossAmount;
        } else {
            $giftUsedAmount = $giftAvailableAmount;
        }
        return $giftUsedAmount;
    }

    function _isFromAdwords() {
        return !is_null($this->Session->read('utm_source'));
    }

    function _trackBuyOfAdwords($dealExternalId) {
        $this->_trackAdwords($dealExternalId, Configure::read('google_adwords.dealexternal_type'));
    }

    function _trackAdwords($id, $type_id) {
        $data = $this->_getAdwordFieldsInSession();
        $data['entity_id'] = $id;
        $data['entity_type_id'] = $type_id;
        return $this->GoogleAdwordsConversion->save($data);
    }

    function process_user($deal) {
        if (!empty($this->data)) {
            $total_deal_amount = Precision::mul($deal['Deal']['discounted_price'], $this->data['Deal']['quantity']);
            Debugger::log('$total_deal_amount:' . $total_deal_amount);
            $amount_needed = $total_deal_amount;
            $user_available_balance = 0;
            if ($this->Auth->user('id')) {
                $valid_user = true;
                $user_available_balance = $this->Deal->User->checkUserBalance($this->Auth->user('id'));
                App::import('Model', 'PaymentType');
                $this->PaymentType = new PaymentType();
                if ($this->data['Deal']['payment_type_id'] == ConstPaymentTypes::Wallet) {
                    $amount_needed = $total_deal_amount - $user_available_balance;
                } elseif ($deal['Deal']['is_discount_mp'] && $this->PaymentType->isPaymentTypeMP($this->data['Deal']['payment_type_id'])) {
                    $user_available_balance = $this->Deal->getDiscountMP($deal['Deal']['discounted_price'], $this->data['Deal']['quantity']);
                }
                $currentUserProfile = $this->Deal->User->UserProfile->find('first', array(
                    'conditions' => array(
                        'user_id' => $this->Auth->user('id')
                    ),
                    'recursive' => 1,
                    'fields' => array(
                        'UserProfile.id',
                        'UserProfile.dni',
                        'User.fb_user_id'
                    )
                ));
                $currentDni = $currentUserProfile['UserProfile']['dni'];
                $userProfileId = $currentUserProfile['UserProfile']['id'];
                if (empty($userProfileId)) {
                    $msg = "Hubo un error en el registro, por favor verifique los datos ingresados.";
                    $this->Session->setFlash($msg, 'default', null, 'error');
                }
                if (empty($currentDni) && empty($currentUserProfile['User']['fb_user_id'])) {
                    $userProfileDniData = array(
                        'UserProfile' => array(
                            'id' => $userProfileId,
                            'user_id' => $this->Auth->user('id'),
                            'dni' => $this->data['UserProfile']['dni']
                        )
                    );
                    $this->Deal->User->UserProfile->set($userProfileDniData);
                    if (!$this->Deal->User->UserProfile->validates()) {
                        return false;
                    }
                    $this->Deal->User->UserProfile->create();
                    $this->Deal->User->UserProfile->save($userProfileDniData);
                }
            } elseif (isset($this->data['User'])) {
                $this->_registerUser();
            }
            if (!isset($valid_user) || !$valid_user) {
                $msg = "Hubo un error en el registro, por favor verifique los datos ingresados.";
                $this->Session->setFlash($msg, 'default', null, 'error');
            } else {
                $this->pageTitle .= sprintf(__l('Buy %s Deal'), $deal['Deal']['name']);
                $payment_type_id = $this->data['Deal']['payment_type_id'];
                $id_gateway = $this->data['Deal']['id_gateway'];
                $bac_payment_type = $this->data['Deal']['bac_payment_type'];
                $user_id = $this->Auth->user('id');
                $deal_id = $this->data['Deal']['deal_id'];
                if ($user_id === NULL) {
                    $this->redirect(array(
                        'action' => 'buy',
                        $deal_id
                    ));
                }
                $quantity = $this->data['Deal']['quantity'];
                Debugger::log('BEGIN insertando en al tabla deal_externals para user_id: ' . $user_id, LOG_DEBUG);
                if ($this->data["Deal"]["is_gift"]) {
                    $giftData = array(
                        'is_gift' => true,
                        'to' => $this->data["Deal"]["gift_to"],
                        'email' => $this->data["Deal"]["gift_email"],
                        'message' => $this->data["Deal"]["gift_message"],
                        'dni' => $this->data["Deal"]["gift_dni"],
                        'dob' => $this->data["Deal"]["gift_dob"]
                    );
                } else {
                    $giftData = array(
                        'is_gift' => false
                    );
                }
                $external_status = 'P';
                $options = array(
                    'payment_option_id' => $this->data['DealExternal']['payment_option_id'],
                    'payment_plan_option_id' => $this->data['Deal']['cantidad_cuotas']
                );
                $dealExt = $this->Deal->externalPayment($deal_id, $quantity, $user_id, $payment_type_id, $external_status, $this->data['DealExternal']['used_points'], $giftData, false, $options);
                $cantidad_cuotas = $this->PaymentPlanItem->getNumInstallments($dealExt['DealExternal']['payment_plan_option_id']);
                $giftPinAmount = 0;
                if (empty($cantidad_cuotas)) {
                    $cantidad_cuotas = 1;
                }
                if (isset($this->data['gift_pin_code']) && !empty($this->data['gift_pin_code'])) {
                    $giftPinCode = $this->data['gift_pin_code'];
                    $bacPin = $this->GiftPinService->isUsable($giftPinCode);
                    if ($bacPin) {
                        $giftPin = $this->GiftPinService->block($giftPinCode);
                        $this->data['DealExternal']['gift_pin_id'] = $giftPin['GiftPin']['id'];
                        $this->data['DealExternal']['gift_pin_code'] = $giftPin['GiftPin']['code'];
                        $this->data['DealExternal']['gifted_amount'] = $this->calculateGiftUsedAmount($bacPin['monto'], Precision::mul($deal['Deal']['discounted_price'], $this->data['Deal']['quantity']));
                        $this->DealExternal->save($this->data);
                        $amount_needed = $amount_needed - $bacPin['monto'];
                        $total_deal_amount = $total_deal_amount - $bacPin['monto'];
                        $giftPinAmount = $bacPin['monto'];
                    }
                }
                $is_payment_mp = $this->PaymentType->isPaymentTypeMP($this->data['Deal']['payment_type_id']);
                if ($deal['Deal']['is_discount_mp'] && $is_payment_mp) {
                    $this->data['DealExternal']['discounted_amount'] = $this->Deal->getDiscountMP($amount_needed, 1);
                    $this->DealExternal->save($this->data, false);
                }
                if (!isset($dealExt['DealExternal']) || !isset($dealExt['DealExternal']['id']) || (int) $dealExt['DealExternal']['id'] == 0) {
                    $this->render('buy_canceled');
                    return;
                }
                if ($this->_isFromAdwords()) {
                    $this->_trackBuyOfAdwords($dealExt['DealExternal']['id']);
                }
                Debugger::log('END insertando en al tabla deal_externals REGISTRO CREADO: ' . debugEncode($dealExt), LOG_DEBUG);
                Debugger::log('BEGIN creando/actualizando usuario BAC', LOG_DEBUG);
                $this->Deal->User->createBacUser($this->Auth->user('id'), $deal['Deal']['is_tourism'], $deal['Deal']['start_date']);
                Debugger::log('END creando/actualizando usuario BAC', LOG_DEBUG);
                $deal = $this->Deal->findById($this->data['Deal']['deal_id']);
                $this->set('amount', $amount_needed);
                $this->set('deal', $deal);
                $this->set('dealdescription', $this->TextUtil->replaceAccents($deal['Deal']['name']));
                $this->set('payment_type_id', $payment_type_id);
                $this->set('bac_payment_type', $bac_payment_type);
                $this->set('id_gateway', $id_gateway);
                $this->set('user_id', $user_id);
                $this->set('deal_id', $deal_id);
                $this->set('quantity', $quantity);
                $this->set('dealExt', $dealExt);
                $this->set('cantidad_cuotas', $cantidad_cuotas);
                $this->set('total_deal_amount', $total_deal_amount);
                $this->set('user_available_balance', $user_available_balance);
                $this->set('is_discount_mp', $deal['Deal']['is_discount_mp']);
                $this->set('is_payment_mp', $is_payment_mp);
                if ($giftPinAmount > 0) {
                    $this->set('giftPinAmount', $giftPinAmount);
                }
                $priceCombinado = Precision::sub($total_deal_amount, $user_available_balance);
                if ($deal['Deal']['is_discount_mp'] && $this->PaymentType->isPaymentTypeMP($this->data['Deal']['payment_type_id'])) {
                    $total_discount_mp = $this->Deal->getDiscountMP($total_deal_amount, 1);
                    $this->set('total_discount_mp', $total_discount_mp);
                    Debugger::log('$total_discount_mp:' . $total_discount_mp);
                }
                if ($payment_type_id == ConstPaymentTypes::Wallet && !Precision::lessEqual($priceCombinado, '0')) {
                    $total_discount_mp_combined = $this->Deal->getDiscountMP($priceCombinado, 1);
                    $this->set('total_discount_mp_combined', $total_discount_mp_combined);
                }
                $this->render('do_payment_bac');
            }
        }
    }

    function _registerUser() {
        try {
            if (empty($this->data['User'])) {
                throw new InvalidArgumentException("data[User] should not be empty");
            }
            $this->Deal->User->create();
            $this->Deal->User->set($this->data['User']);
            $this->Deal->User->UserProfile->set($this->data['UserProfile']);
            $valid_user = false;
            $validateUser = $this->Deal->User->validates();
            $validateProfile = $this->Deal->User->UserProfile->validates();
            if ($validateUser && empty($this->Deal->User->validationErrors) && $validateProfile) {
                $this->data['User']['is_active'] = 1;
                $this->data['User']['is_email_confirmed'] = 1;
                $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                $this->data['User']['user_type_id'] = ConstUserTypes::User;
                $this->data['User']['signup_ip'] = $this->RequestHandler->getClientIP();
                $subscriptionTotalAmount = $this->Subscription->findTotalAvailableBalanceAmountByEmail($this->data['User']['email']);
                if (!is_null($subscriptionTotalAmount[0]['total_amount'])) {
                    $this->data['User']['available_balance_amount'] = $subscriptionTotalAmount[0]['total_amount'];
                }
                if ($this->Deal->User->save($this->data['User'], false)) {
                    $user_id = $this->Deal->User->getLastInsertId();
                    if ($this->Deal->User->save($this->data['User'], false)) {
                        $user_id = $this->Deal->User->getLastInsertId();
                        if (isset($this->data['Deal']['notifications']) && ($this->data['Deal']['notifications'] != '0')) {
                            $this->_subscribeForBuyRecentlyRegister();
                        }
                        $this->_sendWelcomeMail($user_id, $this->data['User']['email'], $this->data['User']['username']);
                        $this->data['UserProfile']['user_id'] = $user_id;
                        $this->Deal->User->UserProfile->create();
                        $this->Deal->User->UserProfile->save($this->data['UserProfile']);
                        $this->Auth->login($this->data['User']);
                        $this->data['Deal']['user_id'] = $user_id;
                        $valid_user = true;
                    }
                }
            }
        } catch (Exception $e) {
            $this->ApiLogger->error('Error al intentar registrar y loguear un usuario para la compra.', array(
                'error' => $e->getMessage()
            ));
            throw $e;
        }
        return $valid_user;
    }

    function _sendWelcomeMail($user_id, $user_email, $username) {
        
    }

    function debugadminmail() {
        $this->Deal->_sendAdminSubscriptionMail();
        die;
    }

    function admin_send_upcoming_deals() {
        $this->Deal->_sendAdminSubscriptionMail();
        $this->redirect(array(
            'controller' => 'deals',
            'action' => 'index'
        ));
    }

    function debugmail($id) {
        $deal = $this->Deal->find('first', array(
            'conditions' => array(
                'Deal.id' => $id
            ),
            'recursive' => 1
        ));
        if (isset($deal)) {
            $this->Deal->_sendCouponMail($deal);
        } else {
            echo 'de Deal ni la sombra';
            exit;
        }
    }

    function _buyDeal($data, $redirect = TRUE) {
        $this->Deal->Behaviors->detach('Excludable');
        AppModel::setDefaultDbConnection('master');
        $deal_id = $data['Deal']['deal_id'];
        $deal = $this->Deal->find('first', array(
            'conditions' => array(
                'Deal.id' => $data['Deal']['deal_id']
            ),
            'contain' => array(
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                ),
                'ProductProduct'
            ),
            'recursive' => 2
        ));
        if (empty($deal)) {
            Debugger::log(sprintf('_buyDeal No se logra levantar un deal para realizar una compra: Redirect `%d` -  Data: `%s` .', $redirect, debugEncode($data)), LOG_DEBUG);
            if ($redirect) {
                $this->cakeError('error404');
            } else {
                return FALSE;
            }
        }
        $selectedQuantity = $data['Deal']['quantity'];
        $total_deal_amount = Precision::mul($deal['Deal']['discounted_price'], $selectedQuantity);
        $deal_amount = $deal['Deal']['discounted_price'];
        $parcialAmount = ($data['DealExternal']['parcial_amount'] != NULL) ? $data['DealExternal']['parcial_amount'] : 0;
        $user = $this->Deal->User->find('first', array(
            'conditions' => array(
                'User.id' => $data['Deal']['user_id']
            ),
            'recursive' => - 1
        ));
        if (empty($user)) {
            Debugger::log('_buyDeal Usuario no encontrado: userid = ' . $data['Deal']['user_id'], LOG_DEBUG);
            return $this->returnFromBuyDeal($redirect, 'Hubo un problema al procesar tu compra.');
        }
        Debugger::log("_buyDeal Se procede a comprar {$selectedQuantity} cupones", LOG_DEBUG);
        try {
            App::import('Component', 'AccountingEventService');
            $AccountingEventService = new AccountingEventServiceComponent();
            $AccountingEventService->registerAccountingEvent($deal);
        } catch (Exception $e) {
            $this->ApiLogger->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                'errorCode' => $e->getMessage()
            ));
        }
        $arr_deal_users = array();
        $existingDealUsersQuantity = $this->Deal->DealUser->find('count', array(
            'conditions' => array(
                'DealUser.deal_external_id' => $data['DealExternal']['id']
            )
        ));
        $dealUserCreateQuantity = $selectedQuantity - $existingDealUsersQuantity;
        $deal_user = array();
        for ($i = 0; $i < $dealUserCreateQuantity; $i++) {
            if ($deal['Deal']['has_pins'] > 0 || (isset($deal['ProductProduct']) && $deal['ProductProduct']['has_pins'])) {
                if ($deal['Deal']['has_pins'] > 0) {
                    $pin = $this->Deal->Pin->next($deal['Deal']['id'], $deal['Deal']['has_pins']);
                } elseif (isset($deal['ProductProduct']) && $deal['ProductProduct']['has_pins']) {
                    $pin = $this->Deal->Pin->nextByProduct($deal['ProductProduct']['id'], $deal['ProductProduct']['has_pins']);
                }
                if (!empty($pin)) {
                    $deal_user['DealUser']['pin_code'] = $pin['code'];
                } else {
                    unset($deal_user['DealUser']['pin_code']);
                }
            }
            $deal_user['DealUser']['quantity'] = 1;
            $deal_user['DealUser']['deal_external_id'] = $data['DealExternal']['id'];
            $deal_user['DealUser']['deal_id'] = $data['Deal']['deal_id'];
            $deal_user['DealUser']['is_gift'] = $data['Deal']['is_gift'];
            $deal_user['DealUser']['user_id'] = $data['Deal']['user_id'];
            $deal_user['DealUser']['discount_amount'] = $deal_amount;
            $deal_user['DealUser']['payment_type_id'] = !empty($data['Deal']['payment_type_id']) ? $data['Deal']['payment_type_id'] : ConstPaymentTypes::Wallet;
            $coupon_code = $this->_uuid();
            $deal_user['DealUser']['coupon_code'] = $coupon_code;
            if ($data['Deal']['is_gift']) {
                $deal_user['DealUser']['gift_email'] = $data['Deal']['gift_data']['email'];
                $deal_user['DealUser']['message'] = $data['Deal']['gift_data']['message'];
                $deal_user['DealUser']['gift_to'] = $data['Deal']['gift_data']['to'];
                $deal_user['DealUser']['gift_dob'] = $data['Deal']['gift_data']['dob'];
                $deal_user['DealUser']['gift_dni'] = $data['Deal']['gift_data']['dni'];
                $deal_user['DealUser']['gift_from'] = $user['User']['username'];
            }
            $arr_deal_users[] = $deal_user;
            $this->Deal->DealUser->create();
            $this->Deal->DealUser->set($deal_user);
            $userHasMoney = (Precision::sub($user['User']['available_balance_amount'], $deal_amount)) >= 0;
            Debugger::log("_buyDeal userHasMoney - available_balance_amount: " . $user['User']['available_balance_amount'] . " deal_amount: " . $deal_amount, LOG_DEBUG);
            if ($userHasMoney && $this->Deal->DealUser->save($deal_user)) {
                Debugger::log("_buyDeal Se crea satisfactoriamente el DealUSer. Dato: " . str_replace('"', "`", debugEncode($deal_user)), LOG_DEBUG);
                $last_inserted_id = $this->Deal->DealUser->getLastInsertId();
                App::import('Model', 'Redemption');
                $redemption = new Redemption();
                $redemption->create($last_inserted_id, 0);
                if ($deal['Deal']['has_pins'] || (isset($deal['ProductProduct']) && $deal['ProductProduct']['has_pins'])) {
                    $this->Deal->Pin->updateAll(array(
                        'Pin.deal_user_id' => $last_inserted_id
                            ), array(
                        'Pin.id' => $pin['ids']
                    ));
                }
                $transaction['Transaction']['user_id'] = $deal_user['DealUser']['user_id'];
                $transaction['Transaction']['foreign_id'] = $last_inserted_id;
                $transaction['Transaction']['class'] = 'DealUser';
                $transaction['Transaction']['amount'] = $deal_amount;
                $transaction['Transaction']['transaction_type_id'] = (!empty($data['Deal']['is_gift'])) ? ConstTransactionTypes::DealGift : ConstTransactionTypes::BuyDeal;
                if (!$this->Deal->User->Transaction->log($transaction)) {
                    Debugger::log("_buyDeal no se puede setear el registro en Log transaction. Dato: " . debugEncode($transaction), LOG_DEBUG);
                    return $this->returnFromBuyDeal($redirect, 'Hubo un problema al procesar tu compra.');
                }
                $user['User']['available_balance_amount'] = Precision::sub($user['User']['available_balance_amount'], $deal_amount);
                if (!$this->Deal->User->updateAll(array(
                            'User.available_balance_amount' => 'User.available_balance_amount - ' . $deal_amount
                                ), array(
                            'User.id' => $deal_user['DealUser']['user_id']
                        ))) {
                    Debugger::log("_buyDeal no se puede actualizar el available balance amount. Deal_amount: " . $deal_amount . " user_id: " . $deal_user['DealUser']['user_id'], LOG_DEBUG);
                    return $this->returnFromBuyDeal($redirect, 'Hubo un problema al procesar tu compra.');
                }
                if (!empty($data['Deal']['is_fibertel_deal'])) {
                    $fibertel_user_id = $user['User']['fibertel_user_id '];
                    $fibertel_id = 772;
                    $token_url = 'http://crm.fibertel.com.ar/webservice_2/project.asmx?op=AddPerson&ProjectId=' . $fibertel_id . '&PersonId=' . $fibertel_user_id;
                    $ch = curl_init($token_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($ch, CURLOPT_CAINFO, Configure::read('CertificateBundlePath'));
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
                if (!$this->Deal->updateAll(array(
                            'Deal.deal_user_count' => 'Deal.deal_user_count + 1 '
                                ), array(
                            'Deal.id' => $deal_id
                        ))) {
                    Debugger::log("_buyDeal no se puede actualizar el deal_user_count para el deal_id: {$deal_id}", LOG_DEBUG);
                    return $this->returnFromBuyDeal($redirect, 'Hubo un problema al procesar tu compra.');
                }
                if ($deal['Deal']['deal_status_id'] == ConstDealStatus::Open) {
                    $db = $this->Deal->getDataSource();
                    $this->Deal->updateAll(array(
                        'Deal.deal_status_id' => ConstDealStatus::Tipped,
                        'Deal.deal_tipped_time' => '\'' . date('Y-m-d H:i:s') . '\''
                            ), array(
                        'Deal.deal_status_id' => ConstDealStatus::Open,
                        'Deal.deal_user_count >=' => $db->expression('Deal.min_limit'),
                        'Deal.id' => $deal_id
                    ));
                    App::import('Model', 'now.NowBranchesDeals');
                    $this->NowBranchesDeals = new NowBranchesDeals;
                    $this->NowBranchesDeals->updateAll(array(
                        'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Tipped
                            ), array(
                        'NowBranchesDeals.now_deal_id' => $deal_id
                    ));
                    if (!empty($deal['Deal']['max_limit']) && $deal['Deal']['deal_user_count'] >= $deal['Deal']['max_limit']) {
                        $this->Deal->_closeDeals(array(
                            $deal['Deal']['id']
                        ));
                        App::import('Model', 'now.NowBranchesDeals');
                        $this->NowBranchesDeals = new NowBranchesDeals;
                        $this->NowBranchesDeals->updateAll(array(
                            'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Closed
                                ), array(
                            'NowBranchesDeals.now_deal_id' => $deal['Deal']['id']
                        ));
                    }
                }
                $referred_by_user_id = $user['User']['referred_by_user_id'];
                if (Configure::read('user.is_referral_system_enabled') && !empty($referred_by_user_id)) {
                    Debugger::log("_buyDeal se le intenta pagar al referido: referral: {$referred_by_user_id}", LOG_DEBUG);
                    $data['User'] = $user['User'];
                    $this->_pay_to_referrer($data);
                }
                if (!$data['DealExternal']['used_points']) {
                    $retorno = $this->Deal->User->add_points($user['User']['id'], ConstActionIds::Buy);
                }
            } else {
                if ($userHasMoney) {
                    Debugger::log(sprintf('_buyDeal: No se pudo salvar el dealUser.'), LOG_DEBUG);
                } else {
                    Debugger::log(sprintf('_buyDeal: El usuario ya no posee plata para comprar esta oferta. Puede no ser un error - revisar transacciones'), LOG_DEBUG);
                }
                return $this->returnFromBuyDeal($redirect, 'You can\'t buy this deal.');
            }
        }
        $company_address = '';
        if (!empty($deal['Company']['address1'])) {
            $company_address .= $deal['Company']['address1'];
        }
        if (!empty($deal['Company']['address2'])) {
            $company_address .= $deal['Company']['address2'];
        }
        if (!empty($deal['Company']['City']['name'])) {
            $company_address .= $deal['Company']['City']['name'];
        }
        if (!empty($deal['Company']['State']['name'])) {
            $company_address .= $deal['Company']['State']['name'];
        }
        if (!empty($deal['Company']['Country']['name'])) {
            $company_address .= $deal['Company']['Country']['name'];
        }
        if (!empty($deal['Company']['zip'])) {
            $company_address .= $deal['Company']['zip'];
        }
        if (!empty($deal['Company']['phone'])) {
            $company_address .= $deal['Company']['phone'];
        }
        App::import('Model', 'PaymentType');
        $this->PaymentType = & new PaymentType();
        $paymentTypeData = $this->PaymentType->findById($data['DealExternal']['payment_type_id']);
        $company_address .= '.';
        if ($paymentTypeData['PaymentType']['name'] == 'TarjetaDeCreditoTurismo') {
            $paymentTypeData['PaymentType']['name'] = 'Tarjeta De Credito';
        }
        if ($paymentTypeData['PaymentType']['name'] == 'PagoOfflineTurismo ') {
            $paymentTypeData['PaymentType']['name'] = 'Pago Offline';
        }
        if ($paymentTypeData['PaymentType']['name'] == 'Tarjeta') {
            $paymentTypeData['PaymentType']['name'] = 'Tarjeta De Credito';
        }
        if ($deal['Deal']['is_discount_mp']) {
            $credit_used_html = ' Monto de descuento: ';
        } else {
            $credit_used_html = ' Cr&eacute;dito usado: ';
        }
        $externalPaidHtml = null;
        if ($paymentTypeData['PaymentType']['id'] != ConstPaymentTypes::Wallet) {
            $paymentTypeName = 'por ' . $paymentTypeData['PaymentType']['name'];
            $cuotas = '';
            if ($paymentTypeData['PaymentType']['name'] == 'NPS Turismo') {
                App::import('Model', 'PaymentPlanItem');
                $this->PaymentPlanItem = & new PaymentPlanItem();
                $paymentTypeName = 'con Tarjeta de Cr&eacute;dito';
                $numeroCuotas = $this->PaymentPlanItem->getNumInstallments($data['DealExternal']['payment_plan_option_id']);
                $valorCuota = $parcialAmount / $numeroCuotas;
                $valorCuota = round($valorCuota, 2);
                $cuotas = ' (en ' . $numeroCuotas . ' cuotas de ' . Configure::read('site.currency') . number_format($valorCuota, 2, ',', '.') . ')';
            }
            $externalPaidHtml = 'Precio pagado ' . $paymentTypeName . ': ' . ' <strong>' . Configure::read('site.currency') . number_format($parcialAmount, 2, ',', '.') . $cuotas . '</strong><br/>';
        } else {
            $externalPaidHtml = '';
        }
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##USERNAME##' => $user['User']['username'],
            '##DEAL_NAME##' => $deal['Deal']['name'],
            '##DEAL_TITLE##' => $deal['Deal']['name'],
            '##DEAL_AMOUNT##' => Configure::read('site.currency') . number_format($total_deal_amount, 2, ',', '.'),
            '##DEAL_EXTERNAL_PAID##' => Configure::read('site.currency') . number_format($parcialAmount, 2, ',', '.'),
            '##CREDIT_USED##' => Configure::read('site.currency') . number_format($total_deal_amount - $parcialAmount, 2, ',', '.'),
            '##CREDIT_USED_DESCRIPTION##' => $credit_used_html,
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##QUANTITY##' => $selectedQuantity,
            '##PURCHASE_ON##' => htmlentities(strftime("%d/%m/%Y")),
            '##DEAL_STATUS##' => ConstDealStatus::getFriendly($deal['Deal']['deal_status_id']),
            '##COMPANY_NAME##' => $deal['Company']['name'],
            '##COMPANY_ADDRESS##' => $company_address,
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##PAYMENT_TYPE##' => $paymentTypeData['PaymentType']['name'],
            '##EXTERNAL_PAID_HTML##' => $externalPaidHtml
        );
        if ($this->Deal->is_subdeal($deal) && !empty($deal['Deal']['descriptive_text'])) {
            $emailFindReplace['##DEAL_NAME##'] = $emailFindReplace['##DEAL_TITLE##'] = substr($deal['Deal']['name'], 0, -(strlen($deal['Deal']['descriptive_text']) + 3));
            $emailFindReplace['##DESCRIPTIVE_TEXT##'] = $deal['Deal']['descriptive_text'];
            $email_message = $this->EmailTemplate->selectTemplate('Subdeal Bought');
        } else {
            if ($deal['Deal']['is_now'] == 1) {
                $email_message = $this->EmailTemplate->selectTemplate('Deal Bought Ya');
            } else {
                $email_message = $this->EmailTemplate->selectTemplate('Deal Bought');
            }
        }
        $this->Session->setFlash(__l('You have bought a deal sucessfully.'), 'default', null, 'success');
        if ($redirect) {
            if (Configure::read('Deal.invite_after_deal_add')) {
                $this->redirect(array(
                    'controller' => 'user_friends',
                    'action' => 'deal_invite',
                    'type' => 'deal',
                    'deal' => $deal['Deal']['slug']
                ));
            } else {
                $this->redirect(array(
                    'action' => 'buyOk',
                    $deal_id,
                    $selectedQuantity,
                    base64_encode($selectedQuantity * $deal_amount)
                ));
            }
        } else
            return TRUE;
    }

    private function _doClarin365Get($cardNumber) {
        $url = Configure::read('clarin365.verification_url');
        $url .= "?q={\"nroCredencial\":\"{$cardNumber}\"}";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        if (!curl_errno($ch)) {
            curl_close($ch);
        } else {
            CakeLog::write('error', "365 validation - Error al verificar con clarin 365 un nro de tarjeta: {$cardNumber} el mensaje de error es: " . curl_error($ch));
            $result = "ERROR";
        }
        return $result;
    }

    function returnFromBuyDeal($redirect, $message) {
        if ($redirect) {
            $this->Session->setFlash(__l($message), 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index'
            ));
        } else {
            return FALSE;
        }
    }

    function _subscribeForBuyRecentlyRegister() {
        $this->data['Subscription']['user_id'] = $this->Deal->User->getLastInsertId();
        $this->data['Subscription']['city_id'] = Configure::read('Actual.city_id');
        $this->data['Subscription']['email'] = $this->data['User']['email'];
        $this->data['Subscription']['is_subscribed'] = '1';
        $subscription = $this->Subscription->find('first', array(
            'conditions' => array(
                'Subscription.email' => $this->data['User']['email'],
                'Subscription.city_id' => Configure::read('Actual.city_id')
            ),
            'fields' => array(
                'Subscription.id',
                'Subscription.is_subscribed'
            ),
            'recursive' => - 1
        ));
        if (empty($subscription)) {
            $this->Subscription->create();
        } else {
            $this->data['Subscription']['is_subscribed'] = 1;
            $this->data['Subscription']['id'] = $subscription['Subscription']['id'];
        }
        if (!$this->Subscription->isFake($this->data['Subscription']['email'])) {
            $this->Subscription->save($this->data);
        }
    }

    function _subscribeForBuyLoggedUser() {
        try {
            $this->data['Subscription']['user_id'] = $this->Auth->user('id');
            $this->data['Subscription']['city_id'] = Configure::read('Actual.city_id');
            $this->data['Subscription']['email'] = $this->Auth->user('email');
            $this->data['Subscription']['is_subscribed'] = '1';
            $subscription = $this->Subscription->find('first', array(
                'conditions' => array(
                    'Subscription.email' => $this->Auth->user('email'),
                    'Subscription.city_id' => Configure::read('Actual.city_id')
                ),
                'recursive' => - 1
            ));
            if (empty($subscription)) {
                $this->Subscription->create();
            } else {
                $this->data['Subscription']['is_subscribed'] = 1;
                $this->data['Subscription']['id'] = $subscription['Subscription']['id'];
            }
            $this->Subscription->save($this->data);
        } catch (Exception $e) {
            $this->ApiLogger->error('Error al intentar subscribir un usuario logueado.', array(
                'error' => $e->getMessage()
            ));
            throw $e;
        }
    }

    function _getDealUrl($deal_slug, $city_slug) {
        $dealUrl = STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $city_slug . '/deal/' . $deal_slug;
        return $dealUrl;
    }

    function _getSellersName() {
        $sellers_query = $this->Deal->query("SELECT users.id, username, first_name, last_name FROM user_profiles INNER JOIN users ON user_profiles.user_id = users.id AND user_type_id = 7 AND is_visible = 1;");
        $temp = array();
        foreach ($sellers_query as $seller) {
            $id = $seller['users']['id'];
            $username = $seller['users']['username'];
            $first_name = $seller['user_profiles']['first_name'];
            $last_name = $seller['user_profiles']['last_name'];
            if ($first_name) {
                $temp[$id] = $first_name . ' ' . $last_name;
            } else {
                $temp[$id] = $username;
            }
        }
        return $temp;
    }

    function _pay_to_referrer($data) {
        $dealUserCount = $this->Deal->DealUser->find('count', array(
            'conditions' => array(
                'DealUser.user_id' => $data['User']['id']
            ),
            'recursive' => - 1
        ));
        $today = strtotime($this->data['DealExternal']['created']);
        $registered_date = strtotime($data['User']['created']);
        $hours_diff = intval(($today - $registered_date) / 60 / 60);
        if (($dealUserCount == 1) && $hours_diff <= Configure::read('user.referral_deal_buy_time')) {
            $transaction['Transaction']['user_id'] = $data['User']['referred_by_user_id'];
            $transaction['Transaction']['foreign_id'] = ConstUserIds::Admin;
            $transaction['Transaction']['class'] = 'SecondUser';
            $transaction['Transaction']['amount'] = Configure::read('user.referral_amount');
            $transaction['Transaction']['transaction_type_id'] = ConstTransactionTypes::ReferralAmount;
            $this->Deal->User->Transaction->log($transaction);
            $transaction = array();
            $transaction['Transaction']['user_id'] = ConstUserIds::Admin;
            $transaction['Transaction']['foreign_id'] = $data['User']['referred_by_user_id'];
            $transaction['Transaction']['class'] = 'SecondUser';
            $transaction['Transaction']['amount'] = Configure::read('user.referral_amount');
            $transaction['Transaction']['transaction_type_id'] = ConstTransactionTypes::ReferralAmountPaid;
            $this->Deal->User->Transaction->log($transaction);
            $this->Deal->User->updateAll(array(
                'User.available_balance_amount' => 'User.available_balance_amount +' . Configure::read('user.referral_amount')
                    ), array(
                'User.id' => $data['User']['referred_by_user_id']
            ));
            $this->Deal->User->add_points($data['User']['referred_by_user_id'], ConstActionIds::Referral);
        }
    }

    function _sendMail($email_content_array, $template, $to, $sendAs = 'text') {
        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
        $this->Email->to = $to;
        $this->Email->subject = strtr($template['subject'], $email_content_array);
        $this->Email->content = strtr($template['email_content'], $email_content_array);
        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
        $this->Email->send($this->Email->content);
    }

    function payment_success() {
        $this->pageTitle = __l('Payment Success');
        $pay_pal_repsonse = $_POST;
        if (!empty($pay_pal_repsonse['auth_status'])) {
            $this->Session->setFlash(__l('Your payment has been successfully Finished. This Transaction we will updated in your Transactions after Deal has been tipped.'), 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'users',
                'action' => 'my_stuff',
                '#' . __l('My_Purchases')
            ));
        }
        $this->Session->setFlash(__l('Your payment has been successfully Finished, This Transaction we will updated in your Transactions page after receiving the Confirmation form PayPal,'), 'default', null, 'success');
        $this->redirect(array(
            'controller' => 'users',
            'action' => 'my_stuff',
            '#' . __l('My_Transactions')
        ));
    }

    function payment_cancel() {
        AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Payment Cancel');
        $this->Session->setFlash(__l('Transaction failure. Please try once again.'), 'default', null, 'error');
        $this->redirect(array(
            'controller' => 'users',
            'action' => 'my_stuff',
            '#' . __l('My_Transactions')
        ));
    }

    function barcode($barcode = null) {
        $this->autoRender = false;
        define('__TRACE_ENABLED__', false);
        define('__DEBUG_ENABLED__', false);
        include_once (APP . DS . 'vendors' . DS . 'barcode' . DS . 'barcode.php');

        include_once (APP . DS . 'vendors' . DS . 'barcode' . DS . Configure::read('barcode.symbology') . 'object.php');

        $output = "png";
        $width = Configure::read('barcode.width');
        $height = Configure::read('barcode.height');
        $xres = "2";
        $font = "5";
        $type = Configure::read('barcode.symbology');
        if (!empty($barcode)) {
            $style = BCS_ALIGN_CENTER;
            $style |= ($output == "png") ? BCS_IMAGE_PNG : 0;
            $style |= ($output == "jpeg") ? BCS_IMAGE_JPEG : 0;
            $style |= (isset($border) && $border == "on") ? BCS_BORDER : 0;
            $style |= (isset($drawtext) && $drawtext == "on") ? BCS_DRAW_TEXT : 0;
            $style |= (isset($stretchtext) && $stretchtext == "on") ? BCS_STRETCH_TEXT : 0;
            $style |= (isset($negative) && $negative == "on") ? BCS_REVERSE_COLOR : 0;
            switch ($type) {
                case "i25":
                    $obj = new I25Object($width, $height, $style, $barcode);
                    break;

                case "c39":
                    $obj = new C39Object($width, $height, $style, $barcode);
                    break;

                case "c128a":
                    $obj = new C128AObject($width, $height, $style, $barcode);
                    break;

                case "c128b":
                    $obj = new C128BObject($width, $height, $style, $barcode);
                    break;

                case "c128c":
                    $obj = new C128CObject($width, $height, $style, $barcode);
                    break;

                default:
                    $obj = false;
            }
            if ($obj) {
                if ($obj->DrawObject($xres)) {
                    $obj->SetFont($font);
                    $obj->DrawObject($xres);
                    $obj->FlushObject();
                    $obj->DestroyObject();
                    unset($obj);
                }
            }
        }
    }

    function buyOk($id) {
        die;
    }

    function buyPending($id) {
        $this->Deal->Behaviors->detach('Excludable');
        $this->set('certificaPath', 'ClubCupon/deal/buyPending');
        $this->DealExternal->recursive = - 1;
        $this->Deal->recursive = 1;
        if (!$this->Auth->user('id') || $id === NULL || ($dealExternal = $this->Deal->DealExternal->read(NULL, $id)) === FALSE) {
            $this->cakeError('error404');
        }
        if ($dealExternal['DealExternal']['user_id'] != $this->Auth->user('id')) {
            die('Pago inexistente para el usuario');
        }
        $this->Deal->recursive = - 1;
        $deal = $this->Deal->read(array(
            'Deal.id',
            'Deal.name',
            'Deal.discounted_price',
            'Deal.company_id',
            'Deal.is_discount_mp',
            'Deal.original_price',
            'Deal.city_id',
            'Deal.slug'
                ), $dealExternal['DealExternal']['deal_id']);
        $this->Company->recursive = - 1;
        $company = $this->Company->read(array(
            'Company.name'
                ), $deal['Deal']['company_id']);
        $deal['Company'] = $company['Company'];
        $attachment = $this->Attachment->find('first', array(
            'conditions' => array(
                'Attachment.foreign_id = ' => $deal['Deal']['id'],
                'class' => 'Deal'
            ),
            'recursive' => - 1
        ));
        $deal['Attachment'] = $attachment['Attachment'];
        $internalAmount = $dealExternal['DealExternal']['internal_amount'];
        App::import('Model', 'PaymentType');
        $this->PaymentType = new PaymentType();
        $is_payment_type_mp = $this->PaymentType->isPaymentTypeMP($dealExternal['DealExternal']['payment_type_id']);
        $this->set(compact('dealExternal', 'deal', 'internalAmount', 'dealExternal'));
        $this->set('is_payment_type_mp', $is_payment_type_mp);
        $city = $this->Deal->City->findById($deal['Deal']['city_id']);
        $this->set('dealUrl', $this->_getDealUrl($deal['Deal']['slug'], $city['City']['slug']));
        $this->set('city_slug', $city['City']['slug']);
    }

    function buyCanceled($id) {
        $this->Deal->Behaviors->detach('Excludable');
        $this->set('certificaPath', 'ClubCupon/deal/buyCanceled');
        $this->DealExternal->recursive = - 1;
        if (!$this->Auth->user('id') || $id === NULL || ($dealExternal = $this->Deal->DealExternal->read(NULL, $id)) === FALSE)
            $this->redirect('/');
        if ($dealExternal['DealExternal']['user_id'] != $this->Auth->user('id'))
            die('Pago inexistente para el usuario');
        App::import('Model', 'User');
        $this->User = new User();
        $this->User->id = $ext['DealExternal']['user_id'];
        $this->User->recursive = - 1;
        $this->User->saveField('wallet_blocked', false, $validate = false);
        $this->Deal->recursive = - 1;
        $deal = $this->Deal->read(NULL, $dealExternal['DealExternal']['deal_id']);
        $internalAmount = $dealExternal['DealExternal']['internal_amount'];
        $this->set(compact('dealExternal', 'deal', 'internalAmount'));
        $this->set('citySlug', $this->params['named']['city']);
    }

    function mp_transaction($estado, $deal_external_id) {
        AppModel::setDefaultDbConnection('master');
        $selfModel = $this->Deal;
        $dataSource = $selfModel->getDataSource();
        $dataSource->begin($selfModel);
        if ($estado == 'aceptado') {
            Debugger::log(sprintf('mp_transaction - Cambiando estado a PENDIENTE de verificaciÃ³n al deal_external de un pago declarado acreditado en callback: %d', $deal_external_id), LOG_DEBUG);
            $this->Deal->DealExternal->id = $deal_external_id;
            if (!$this->Deal->DealExternal->saveField('external_status', 'P')) {
                $dataSource->rollback($selfModel);
                Debugger::log(sprintf('mp_transaction - Error al intentar salvar el external_status PENDIENTE de operaciÃ³n acreditada. %d', $deal_external_id), LOG_DEBUG);
                $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
                return $this->redirect(array(
                            'controller' => 'deals',
                            'action' => 'index',
                            'admin' => false
                ));
            }
            $dataSource->commit($selfModel);
            return $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'buyPending',
                        $deal_external_id
            ));
        } else {
            $dataSource->rollback($selfModel);
            return $this->redirect(array(
                        'controller' => 'deals',
                        'action' => 'buyCanceled',
                        $deal_external_id
            ));
        }
    }

    function bac_transaction($estado = 'cancelado') {
        AppModel::setDefaultDbConnection('master');
        $this->Deal->Behaviors->detach('Excludable');
        $selfModel = $this->Deal;
        $dataSource = $selfModel->getDataSource();
        $dataSource->begin($selfModel);
        Debugger::log(sprintf('bac_transaction - Llega un usuario redirigido desde BAC con estado: %s y $_POST params: %s ', $estado, debugEncode($_POST)), LOG_DEBUG);
        if (!count($_POST)) {
            Debugger::log(sprintf('bac_transaction - No llegaron la cantidad de parametros necesarios estado: %s y $_POST params: %s ', $estado, debugEncode($_POST)), LOG_DEBUG);
            $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
            $dataSource->rollback($selfModel);
            return $this->_redirectToUnsecureHome();
        }
        $deal_external_id = $_POST['ID_PAGO_PORTAL'];
        $this->Deal->DealExternal->recursive = - 1;
        $ext = $this->Deal->DealExternal->read(NULL, $deal_external_id);
        if (!$ext) {
            Debugger::log(sprintf('bac_transaction - Se presento un usuario con un id_pago_portal no registrado como id en DealExternals estado: `%s` y $_POST params: `%s`', $estado, debugEncode($_POST)), LOG_DEBUG);
            $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
            $dataSource->rollback($selfModel);
            return $this->_redirectToUnsecureHome();
        }
        if ($ext['DealExternal']['buy_channel_type_id'] == BuyChannel::Mobile) {
            $token = $this->getTokenByDealExternal($ext);
            return $this->_redirecToMobile($ext['DealExternal']['deal_id'], $ext['DealExternal']['id'], $ext['DealExternal']['final_amount'], $token, $estado);
        }
        if ($ext['DealExternal']['external_status'] == 'A') {
            Debugger::log(sprintf('bac_transaction - Llega un pago ACREDITADO ya procesado al callback  por lo que se redirige al usuario hacia la pantalla de confirmacion: %s y $_POST params: %s ', $estado, debugEncode($_POST)), LOG_DEBUG);
            $dataSource->rollback($selfModel);
            return $this->_redirectToUnsecureDealsBuyOk($deal_external_id);
        }
        if ($estado == 'aceptado') {
            if (empty($_POST['ID_PAGO'])) {
                $subestado = 'MONEDERO';
            } else {
                $subestado = $_POST['ESTADO_DEL_PAGO'];
            }
            if ($subestado == 'MONEDERO') {
                Debugger::log(sprintf('bac_transaction - Usuario que pago todo por monedero, no se consulta a BAC. %d', $ext['DealExternal']['id']), LOG_DEBUG);
                $data = array(
                    'Deal' => array(
                        'deal_id' => $ext['DealExternal']['deal_id'],
                        'quantity' => $ext['DealExternal']['quantity'],
                        'is_gift' => $ext['DealExternal']['is_gift'],
                        'gift_data' => json_decode($ext['DealExternal']['gift_data'], true),
                        'payment_type_id' => $ext['DealExternal']['payment_type_id'],
                        'user_id' => $ext['DealExternal']['user_id']
                    ),
                    'DealExternal' => array(
                        'id' => $ext['DealExternal']['id'],
                        'final_amount' => NULL,
                        'used_points' => $ext['DealExternal']['used_points']
                    )
                );
                App::import('Model', 'User');
                $this->User = new User();
                $this->User->id = $ext['DealExternal']['user_id'];
                $this->User->saveField('wallet_blocked', false, $validate = false);
                if (!$this->_buyDeal($data, FALSE)) {
                    $dataSource->rollback($selfModel);
                    Debugger::log(sprintf('bac_transaction - No se logrÃ³ comprar al usuario que quiso pagar con monedero: %d y vino desde BAC', $ext['DealExternal']['id']), LOG_DEBUG);
                    $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
                    return $this->_redirectToUnsecureHome();
                }
                $this->Deal->DealExternal->id = $deal_external_id;
                if (!($this->Deal->DealExternal->saveField('external_status', 'A') && ($this->Deal->DealExternal->saveField('bac_substate', 'MONEDERO')))) {
                    $dataSource->rollback($selfModel);
                    Debugger::log(sprintf('bac_transaction - Error al intentar salvar el external_status. %d ACREDITADO de operacion por MONEDERO', $ext['DealExternal']['id']), LOG_DEBUG);
                    $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
                    return $this->_redirectToUnsecureHome();
                }
                $dataSource->commit($selfModel);
                return $this->_redirectToUnsecureDealsBuyOk($deal_external_id);
            } else if (in_array($subestado, array(
                        'ACREDITADO',
                        'ANULADO',
                        'ANULADO_MANUAL',
                        'CANCELADO',
                        'EXPIRADO',
                        'PENDIENTE'
                    ))) {
                Debugger::log(sprintf('bac_transaction - Cambiando estado a PENDIENTE de verificaciÃ³n al deal_external de un pago declarado acreditado en callback: %d', $ext['DealExternal']['id']), LOG_DEBUG);
                $this->Deal->DealExternal->id = $deal_external_id;
                if (!($this->Deal->DealExternal->saveField('external_status', 'P'))) {
                    $dataSource->rollback($selfModel);
                    Debugger::log(sprintf('bac_transaction - Error al intentar salvar el external_status PENDIENTE de operaciÃ³n acreditada. %d', $ext['DealExternal']['id']), LOG_DEBUG);
                    $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
                    return $this->_redirectToUnsecureHome();
                }
                $dataSource->commit($selfModel);
                return $this->_redirectToUnsecureDealsBuyPending($deal_external_id);
            }
        } else if ($estado == 'cancelado') {
            Debugger::log(sprintf('bac_transaction - Linea %s m estado %s, se redireccionara a redirectToUnsecureDealsBuyCanceled', __LINE__, $estado), LOG_DEBUG);
            $dataSource->rollback($selfModel);
            return $this->_redirectToUnsecureDealsBuyCanceled($deal_external_id);
        } else {
            $this->Deal->DealExternal->id = $deal_external_id;
            if (!($this->Deal->DealExternal->saveField('external_status', 'P'))) {
                $dataSource->rollback($selfModel);
                Debugger::log(sprintf('bac_transaction - Error al intentar salvar el external_status PENDIENTE. %d', $ext['DealExternal']['id']), LOG_DEBUG);
                $this->Session->setFlash(__l('No se pudo confirmar su compra por favor intente nuevamente realizar el pago.'), 'default', null, 'error');
                return $this->_redirectToUnsecureHome();
            }
            $dataSource->commit($selfModel);
            return $this->_redirectToUnsecureDealsBuyPending($deal_external_id);
        }
    }

    function updateDealExternalPaymentType($id, $payment_type_id, $user_available_amount) {
        $this->autoRender = false;
        $this->Deal->updateDealExternalPaymentType($id, $payment_type_id, $user_available_amount);
    }

    function _get_deal_image_url($dimension = 'medium_big_thumb', $type = 'jpg', $id = ConstAttachment::Deal) {
        $image_url = Router::url('/', true) . '/img/' . $dimension . '/Deal/' . $id . '.' . md5(Configure::read('Security.salt') . 'Deal' . $id . $type . $dimension . Configure::read('site.name')) . '.' . $type;
        return $image_url;
    }

    function email_preview() {
        Debugger::log(sprintf('email_preview - INICIO'), LOG_DEBUG);
        $this->layout = 'print';
        $dealUser = $_POST['data'];
        $output = $this->requestAction(array(
            'controller' => 'deals',
            'action' => 'coupon_view',
            $dealUser
                ), array(
            'pass'
        ));
        $this->set('email_content', $output);
    }

    function coupon_view($dealUser) {
        Debugger::log(sprintf('coupon_view - INICIO'), LOG_DEBUG);
        Debugger::log(sprintf('coupon_view - antes de Labels..'), LOG_DEBUG);
        $labels = array(
            '##VALOR_COUPON_TITLE##' => 'Valor',
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##DEAL_TITLE##' => $dealUser['Deal']['name'],
            '##DESCRIPTIVE_TEXT##' => (!empty($dealUser['Deal']['descriptive_text'])) ? ' (' . $dealUser['Deal']['descriptive_text'] . ')' : '',
            '##USER_DNI##' => (isset($dealUser['User']['UserProfile']['dni']) ? $dealUser['User']['UserProfile']['dni'] : ''),
            '##COUPON_PIN##' => empty($dealUser['DealUser']['pin_code']) ? '' : ('<br />PIN de tu cup&oacute;n: <strong>' . $dealUser['DealUser']['pin_code'] . '</strong>'),
            '##VALOR_COUPON##' => '$' . $dealUser['Deal']['discounted_price'],
            '##COUPON_CONDITION##' => ($dealUser['Deal']['is_now'] ? $dealUser['Deal']['description'] : $dealUser['Deal']['coupon_condition']),
            '##COUPON_CODE##' => $dealUser['DealUser']['coupon_code']
        );
        $labels['##COUPON_URL_LOGO_WEB##'] = 'coupon_web/002_a_logo.PNG';
        $labels['##COUPON_URL_IMG_FOOTER##'] = 'coupon_web/operadora.jpg';
        Debugger::log(sprintf('coupon_view - despues de Labels..'), LOG_DEBUG);
        Debugger::log(sprintf('coupon_view - NO isMail..'), LOG_DEBUG);
        if (!($dealUser['Deal']['id'] == $dealUser['Deal']['parent_deal_id'])) {
            $dealUser['Deal']['name'] = substr($dealUser['Deal']['name'], 0, -(strlen($dealUser['Deal']['descriptive_text']) + 3));
        }
        $labels['##USER_NAME##'] = $dealUser['User']['username'];
        if (isset($dealUser['DealUser']['created']) && !empty($dealUser['DealUser']['created'])) {
            $labels['##COUPON_PURCHASED_DATE##'] = htmlentities(strftime("%d/%m/%Y", strtotime($dealUser['DealUser']['created'])), ENT_QUOTES, 'UTF-8');
        }
        if (isset($dealUser['Redemption']) && ConstUserTypes::Company != $this->Auth->user('user_type_id') && isset($dealUser['Redemption'][0])) {
            $posnet_code = $dealUser['Redemption'][0]['posnet_code'];
            $labels['##POSNET_CODE_PREFIX##'] = substr($posnet_code, 0, 8);
            $labels['##POSNET_CODE_SUFFIX##'] = substr($posnet_code, 8, 10);
            $labels['##POSNET_CODE_TEXT##'] = 'Cod. Posnet <span style="font-weight:bold">' . $labels['##POSNET_CODE_PREFIX##'] . $labels['##POSNET_CODE_SUFFIX##'] . '</span>';
        } else {
            $labels['##POSNET_CODE_TEXT##'] = '';
        }
        if (!$dealUser['Deal']['is_now']) {
            $labels['##COUPON_EXPIRY_DATE##'] = 'V&aacute;lido desde el 1';
            $labels['##COUPON_URL_LOGO##'] = 'coupon/002_a_logo.PNG';
            $canjear_en = '';
            $cn = !empty($dealUser['Deal']['custom_company_name']) ? $dealUser['Deal']['custom_company_name'] : $dealUser['Company']['name'];
            if (!empty($cn))
                $canjear_en .= $cn . '<br />';
            $cp = !empty($dealUser['Deal']['custom_company_contact_phone']) ? $dealUser['Deal']['custom_company_contact_phone'] : '';
            if (!empty($cp) && !$cp == '')
                $canjear_en .= $cp . '<br />';
            $ca1 = !empty($dealUser['Deal']['custom_company_address1']) ? $dealUser['Deal']['custom_company_address1'] : $dealUser['Company']['address1'];
            if (!empty($ca1))
                $canjear_en .= $ca1 . '<br />';
            $ca2 = !empty($dealUser['Deal']['custom_company_address2']) ? $dealUser['Deal']['custom_company_address2'] : $dealUser['Company']['address2'];
            if (!empty($ca2))
                $canjear_en .= $ca2 . '<br />';
            $cc = !empty($dealUser['Deal']['custom_company_city']) ? $dealUser['Deal']['custom_company_city'] : $dealUser['Deal']['Company']['City']['name'];
            if (!empty($cc))
                $canjear_en .= $cc . '<br />';
            $labels['##CANJEAR_EN##'] = $canjear_en;
            if (isset($dealUser['Deal']['coupon_expiry_date'])) {
                $labels['##COUPON_EXPIRY_DATE##'] .= htmlentities(strftime("%d/%m/%Y", strtotime($dealUser['Deal']['coupon_start_date'])), ENT_QUOTES, 'UTF-8') . ' hasta el ';
                $labels['##COUPON_EXPIRY_DATE##'] .= htmlentities(strftime("%d/%m/%Y", strtotime($dealUser['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8');
            } else {
                $day = (empty($dealUser['Deal']['coupon_expiry_date']['day'])) ? '--' : $dealUser['Deal']['coupon_expiry_date']['day'];
                $month = (empty($dealUser['Deal']['coupon_expiry_date']['month'])) ? '--' : $dealUser['Deal']['coupon_expiry_date']['month'];
                $year = (empty($dealUser['Deal']['coupon_expiry_date']['year'])) ? '--' : $dealUser['Deal']['coupon_expiry_date']['year'];
                $couponStartDate = $day . "/" . $month . "/" . $year;
                $couponExpiryDate = $day . "/" . $month . "/" . $year;
                $labels['##COUPON_EXPIRY_DATE##'] .= $couponStartDate . ' hasta el ' . $couponExpiryDate;
            }
        } else {
            $labels['##COUPON_EXPIRY_DATE##'] = 'Pod&eacute;s usar este cup&oacute;n el ';
            $labels['##COUPON_EXPIRY_DATE##'] .= strftime("%d/%m/%Y", strtotime($dealUser['Deal']['coupon_expiry_date']));
            $labels['##COUPON_URL_LOGO##'] = 'coupon/header2.jpg';
            App::import('Model', 'now.NowBranchesDeals');
            $branch = new NowBranchesDeals;
            $canjear_en = $branch->getBranchAddressForCupons($dealUser['Deal']['id']);
            $labels['##CANJEAR_EN##'] = join("<br/>\r\n", $canjear_en);
            $this->NowBranchesDeals = new NowBranchesDeals;
            $resultado_NowBranchesDeals = $this->NowBranchesDeals->findByDealId($dealUser['Deal']['id']);
            if ($resultado_NowBranchesDeals) {
                $hora_min = $resultado_NowBranchesDeals['NowBranchesDeals']['ts_inicio'];
                $hora_max = $resultado_NowBranchesDeals['NowBranchesDeals']['ts_fin'];
                $hora_min = date('H:i', strtotime($hora_min));
                $hora_max = date('H:i', strtotime($hora_max));
                if (date('H', strtotime($hora_max)) < 1) {
                    $hora_max = '24:' . date('i', strtotime($hora_max));
                }
                $labels['##COUPON_EXPIRY_DATE##'] .= sprintf(" entre las %s y %s horas.", $hora_min, $hora_max);
            }
        }
        $labels['##PASSBOOK_SECTION##'] = '';
        $labels['##COUPON_MESSAGE##'] = <<<HTML
                &iexcl;Felicitaciones! &iexcl;Compraste la oferta 
                {$labels['##DEAL_TITLE##']}{$labels['##DESCRIPTIVE_TEXT##']}
                , ya pod&eacute;s disfrutarla!
                <br />
                <p style="color:#3e3e3e; font-family:sans-serif; font-size:15px; border-bottom:1px solid #CCCCCC; margin: 20px 0 20px; padding: 0 0 20px 0;">
                    El monto de la compra ya fue abonado. La transacci&oacute;n se confirm&oacute; con &eacute;xito.
                </p>
HTML;
        $labels['##COUPON_TITULAR##'] = 'Usuario: <strong>' . $labels['##USER_NAME##'] . '</strong><br />
                            DNI: <strong>' . $labels['##USER_DNI##'] . '</strong>' . $labels['##COUPON_PIN##'];
        $labels['##COUPON_USER##'] = $labels['##USER_NAME##'];
        $labels['##COUPON_DNI##'] = $labels['##USER_DNI##'] . $labels['##COUPON_PIN##'];
        $email_message = $this->EmailTemplate->selectTemplate('Deal Coupon Generic');
        $output = strtr($email_message['email_content'], $labels);
        Debugger::log(sprintf('coupon_view - FIN'), LOG_DEBUG);
        return $output;
    }

    function checkIfDealIsAccesibleForBuy($deal) {
        Debugger::log('checkIfDealIsAccesibleForBuy:INICIO');
        if (!$this->User->canBuyDeal($this->Auth->user())) {
            return false;
        } elseif ($this->Deal->isFinalizedOrClosed($deal)) {
            return false;
        } elseif ($this->Deal->hasSubdeals($deal['id'])) {
            return true;
        } else {
            $availableQuantity = $this->ProductInventoryService->availableQuantity($deal['id']);
            Debugger::log('$availableQuantity:' . $availableQuantity);
            if ($this->Auth->user('id')) {
                $couponsBoughtByUser = $this->DealExternal->findQuantityGoupByUserIdDealId($this->Auth->user('id'), $deal['id']);
                Debugger::log('$couponsBoughtByUser : ' . $couponsBoughtByUser);
                $buy_max_quantity_per_user = $deal['buy_max_quantity_per_user'] != null ? intval($deal['buy_max_quantity_per_user']) : 9999999999;
                $availableQuantityByUser = $buy_max_quantity_per_user - $couponsBoughtByUser;
                Debugger::log('$availableQuantityByUser : ' . $availableQuantityByUser);
                $availableQuantity = min($availableQuantity, $availableQuantityByUser);
            } else {
                Debugger::log('$availableQuantity:' . $availableQuantity);
            }
            $hasRemainingStock = $availableQuantity > 0;
            $isDealOpenOrTipped = $deal['deal_status_id'] == ConstDealStatus::Open || $deal['deal_status_id'] == ConstDealStatus::Tipped;
            $isAccesibleForBuy = $hasRemainingStock && $isDealOpenOrTipped;
            return $isAccesibleForBuy;
        }
    }

    function landingofertas() {
        $this->layout = 'landing-ofertas';
        $not_conditions = array();
        $conditions = array();
        $this->set('certificaPath', 'ClubCupon/landingofertas');
        $conditions['Deal.city_id'] = Configure::read('Actual.city_id');
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::Closed,
            ConstDealStatus::CicleEnded
        );
        $conditions[] = 'Deal.parent_deal_id = Deal.id';
        $not_conditions['Not']['Deal.deal_status_id'] = array(
            ConstDealStatus::PendingApproval,
            ConstDealStatus::Upcoming
        );
        $all_deals = $this->Deal->find('all', array(
            'conditions' => array(
                $conditions,
                $not_conditions
            ),
            'contain' => array(
                'Company',
                'City',
                'Attachment'
            ),
            'recursive' => 1,
            'limit' => 12,
            'order' => array(
                'Deal.deal_status_id' => 'ASC'
            )
        ));
        $this->set('all_deals', $all_deals);
        $this->set('escape_header', true);
    }

    function admin_now_deals_management() {
        if ($this->Deal->User->isAllowed($this->Auth->user('user_type_id'))) {
            $view = isset($this->params['named']['view']) ? $this->params['named']['view'] : ConstDealStatus::PendingApproval;
            $nowDeals = $this->NowDeal->findNowDealsByStatus($view);
            $this->set('selected', $view);
            $this->set('view', $view);
            $this->set('nowDeals', $nowDeals);
        } else {
            $this->Session->setFlash('Su usuario no tiene los permisos adecuados para acceder a esta vista', 'default', null, 'success');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'view',
                'plugin' => '',
                'admin' => false
            ));
        }
    }

    function admin_audit_now_deal($id = null) {
        if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
            $this->Session->setFlash('Su usuario no tiene permisos para ejecutar esta tarea', 'default', null, 'error');
            $this->redirect(array(
                'plugin' => '',
                'controller' => 'deals',
                'action' => 'index',
                'admin' => false
            ));
        }
        $this->pageTitle = 'Auditor&iacute;a de oferta de CC YA';
        $nowDeal = $this->NowDeal->findDealById($id, 1);
        $ns_deals = $this->NowScheduledDeals->findByDealId($id);
        $dias = array(
            'Monday' => "Lunes",
            'Tuesday' => "Martes",
            'Wednesday' => "Miercoles",
            'Thursday' => "Jueves",
            'Friday' => "Viernes",
            'Saturday' => "Sabado",
            'Sunday' => "Domingo"
        );
        $this->set('dias', $dias);
        $dias_selected = array_keys($dias);
        $deal_categories_root = $this->DealCategory->find('first', array(
            'conditions' => array(
                'name' => 'Now Root'
            ),
            'fields' => array(
                'id'
            ),
            'recursive' => - 1
        ));
        $deal_categories = $this->DealCategory->generatetreelist(array(
            'parent_id' => $deal_categories_root['DealCategory']['id']
                ), null, null);
        $this->set('deal_categories', $deal_categories);
        $company = $this->Company->findByUserId($nowDeal['NowDeal']['user_id']);
        $branches = $this->NowBranch->findByCompanyId($company['Company']['id']);
        $this->set('branches', $branches);
        if (is_null($id) || empty($nowDeal)) {
            $this->Session->setFlash('No existe la oferta a auditar', 'default', null, 'error');
            $this->redirect(array(
                'plugin' => '',
                'controller' => 'deals',
                'action' => 'now_deals_management',
                'admin' => true
            ));
        }
        if (!empty($this->data)) {
            $company = $this->Company->findByUserId($nowDeal['NowDeal']['user_id']);
            $this->data['Company'] = $company['Company'];
            if ($this->data['NowDeal']['scheduled_deals_end_date'] == "") {
                unset($this->data['NowDeal']['scheduled_deals_end_date']);
            }
            $this->data['NowDeal'] = array_merge($nowDeal['NowDeal'], $this->data['NowDeal']);
            $this->NowDeal->id = $id;
            $this->data['NowDeal']['private_note'] = serialize(array(
                'braches' => $this->data['NowDeal']['branches'],
                'times' => array(
                    'time_range_start' => $this->data['NowDeal']['time_range_start'],
                    'time_range_end' => @$this->data['NowDeal']['time_range_end']
                )
            ));
            if (strlen($this->data['NowDeal']['start_date'])) {
                $offset_start = sprintf("+%d hour", $this->data['NowDeal']['time_range_start']);
                $offset_end = sprintf("+%d hour", $this->data['NowDeal']['time_range_end']);
                $coupon_start_date = date("Y-m-d H:i:s", strtotime($offset_start, strtotime($this->data['NowDeal']['start_date'] . " 00:00:00")));
                $coupon_expiry_date = $this->data['NowDeal']['coupon_expiry_date'] = $this->data['NowDeal']['end_date'] = date("Y-m-d H:i:s", strtotime($offset_end, strtotime($this->data['NowDeal']['start_date'] . " 00:00:00")));
                $this->data['NowDeal']['start_date'] = $this->data['NowDeal']['start_date'] . " 00:00:00";
            }
            if ($this->NowDeal->save($this->data)) {
                $branches = $this->NowBranch->find('all', array(
                    'recursive' => - 1,
                    'conditions' => array(
                        'id' => $this->data['NowDeal']['branches']
                    )
                ));
                $this->NowDeal->set($this->data);
                if ($this->NowDeal->save($this->data)) {
                    $this->NowBranchesDeals->deleteAll(array(
                        'now_deal_id' => $this->NowDeal->id
                            ), false, false);
                    foreach ($branches as $branch) {
                        $this->NowBranchesDeals = new NowBranchesDeals;
                        $this->NowBranchesDeals->save(array(
                            'now_branch_id' => $branch['NowBranch']['id'],
                            'now_deal_id' => $this->NowDeal->id,
                            'created_by' => $this->Auth->User('id'),
                            'now_deal_status_id' => $this->data['NowDeal']['deal_status_id'],
                            'deal_category_id' => $this->data['NowDeal']['deal_category_id'],
                            'latitud' => $branch['NowBranch']['latitude'],
                            'longitud' => $branch['NowBranch']['longitude'],
                            'ts_inicio' => $coupon_start_date,
                            'ts_fin' => $coupon_expiry_date
                        ));
                    }
                }
                $this->createScheduledDeals($this->NowDeal->id);
                if ($this->NowDeal->id && !empty($this->data['Attachment']['filename']['name'])) {
                    $file_upload_error = true;
                    if ($this->data['Attachment']['filename']['error'] == 1) {
                        $file_upload_error = false;
                        $this->NowDeal->validationErrors['Attachment']['filename'] = __l('El archivo excede el maximo tamaÃ±o permitido');
                    }
                    if (empty($this->data['Attachment']['filename']['name'])) {
                        $file_upload_error = false;
                        $this->NowDeal->validationErrors['Attachment']['filename'] = __l('Required');
                    }
                    $pathInfo = pathinfo($this->data['Attachment']['filename']['name']);
                    if (!in_array(strtolower(@$pathInfo['extension']), Configure::read('image.file.allowedExt'))) {
                        $file_upload_error = false;
                        $this->NowDeal->validationErrors['Attachment']['filename'] = __l('Upload a valid image file');
                    }
                    if ($this->NowDeal->Attachment->validates() && $file_upload_error) {
                        $this->NowDeal->Attachment->deleteAll(array(
                            'foreign_id' => $this->NowDeal->id
                                ), false, false);
                        $this->NowDeal->Attachment->create();
                        $this->data['Attachment']['class'] = 'NowDeal';
                        $this->data['Attachment']['description'] = 'Product Image';
                        $this->data['Attachment']['foreign_id'] = $this->NowDeal->id;
                        if ($this->NowDeal->Attachment->save($this->data['Attachment'], false)) {
                            if (isset($this->data['Attachment']['old_id'])) {
                                $files = array_merge(glob('img/big_thumb/Deal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/medium_big_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/medium_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/micro_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/normal_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/site_logo_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/small_big_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'), glob('img/small_thumb/now.NowDeal/' . $this->data['Attachment']['old_id'] . '.*'));
                                foreach ($files as $filename) {
                                    unlink($filename);
                                }
                            }
                        }
                    }
                }
                $this->Session->setFlash('Registro guardado', true);
                $this->redirect(array(
                    'action' => 'now_deals_management'
                ));
            } else {
                $this->Session->setFlash('No se pudo guardar el registro', true);
            }
        } else {
            $this->data = $nowDeal;
            $branches_selected = array();
            if ($id != null) {
                $nowbranchesdeals = $this->NowBranchesDeals->findBranchesByDealId($id);
                foreach ($nowbranchesdeals as $bdeals) {
                    array_push($branches_selected, $bdeals['NowBranchesDeals']['now_branch_id']);
                }
            }
            $this->data['NowDeal']['branches'] = $branches_selected;
            if (!empty($this->data['NowDeal']['calculator_discounted_price']) && !empty($this->data['NowDeal']['calculator_min_limit']) && !empty($this->data['NowDeal']['calculator_commission_percentage']) && !empty($this->data['NowDeal']['calculator_bonus_amount'])) {
                $this->data['NowDeal']['calculator_total_purchased_amount'] = Precision::mul($this->data['NowDeal']['calculator_discounted_price'], $this->data['NowDeal']['calculator_min_limit']);
                $porcentageOfPurchasedAmount = Precision::percentageOf($this->data['NowDeal']['calculator_total_purchased_amount'], $this->data['NowDeal']['calculator_commission_percentage']);
                $this->data['NowDeal']['calculator_total_commission_amount'] = Precision::add($porcentageOfPurchasedAmount, $this->data['NowDeal']['calculator_bonus_amount']);
                $this->data['NowDeal']['calculator_net_profit'] = $this->data['NowDeal']['calculator_total_commission_amount'];
            }
            if ($ns_deals) {
                $this->data['NowDeal']['scheduled_deals_end_date'] = $ns_deals['NowScheduledDeals']['end_date'];
                $this->set("js_end_date", $ns_deals['NowScheduledDeals']['end_date']);
                if ($ns_deals['NowScheduledDeals']['Monday'] == 0) {
                    unset($dias_selected[0]);
                }
                if ($ns_deals['NowScheduledDeals']['Tuesday'] == 0) {
                    unset($dias_selected[1]);
                }
                if ($ns_deals['NowScheduledDeals']['Wednesday'] == 0) {
                    unset($dias_selected[2]);
                }
                if ($ns_deals['NowScheduledDeals']['Thursday'] == 0) {
                    unset($dias_selected[3]);
                }
                if ($ns_deals['NowScheduledDeals']['Friday'] == 0) {
                    unset($dias_selected[4]);
                }
                if ($ns_deals['NowScheduledDeals']['Saturday'] == 0) {
                    unset($dias_selected[5]);
                }
                if ($ns_deals['NowScheduledDeals']['Sunday'] == 0) {
                    unset($dias_selected[6]);
                }
            }
            $this->data['NowDeal']['scheduled_deals_is_disabled'] = $ns_deals['NowScheduledDeals']['is_disabled'];
            $clusters = $this->NowDeal->Cluster->find('list', array(
                'order' => array(
                    'Cluster.name' => 'asc'
                )
            ));
            $this->set('clusters', $clusters);
        }
        $this->set('dias_selected', $dias_selected);
        $this->data['NowDeal']['time_range_start'] = date("G", strtotime($nowDeal['NowBranchesDeals'][0]['ts_inicio']));
        $this->data['NowDeal']['time_range_end'] = date("G", strtotime($nowDeal['NowBranchesDeals'][0]['ts_fin']));
        if ($this->data['NowDeal']['time_range_end'] == 0) {
            $this->data['NowDeal']['time_range_end'] = 24;
        }
        $attachment = $this->Attachment->findImageForDeal($id);
        $this->data['Attachment'] = $attachment['Attachment'];
        list($this->data['NowDeal']['start_date']) = explode(" ", $this->data['NowDeal']['start_date']);
        $this->set('start_date', $this->data['NowDeal']['start_date']);
    }

    function check_clarin365_card_number() {
        $this->RequestHandler->respondAs('text/x-json');
        $this->autoRender = false;
        $data = array(
            'success' => 0,
            'isValid' => 0
        );
        $data = array_merge($data, $_POST);
        try {
            $data['isValid'] = $this->_isValidClarin365CardNumber($_POST['clarin365CardNumber']) ? 1 : 0;
            $data['success'] = 1;
        } catch (Exception $e) {
            $data['success'] = 0;
            $data['errorMessage'] = $e->getMessage();
        }
        echo json_encode($data);
    }

    function _isValidClarin365CardNumber($cardNumber) {
        return (substr($this->_doClarin365Get($cardNumber), 0, 2) == 'OK');
    }

    function _generatePurchaseLink($deal, $current_city) {
        if ($this->Deal->User->isAllowed($this->Auth->user('user_type_id')) && $deal['Deal']['deal_status_id'] != ConstDealStatus::Draft && $deal['Deal']['deal_status_id'] != ConstDealStatus::PendingApproval) {
            $deal_link_and_type = array();
            $is_logged_in = $this->Auth->user('id') ? 1 : 0;
            $is_fibertel_deal = isset($deal['Deal']['is_fibertel_deal']) && $deal['Deal']['is_fibertel_deal'] ? 1 : 0;
            $is_ticketportal_deal = isset($deal['Deal']['is_ticketportal_deal']) && $deal['Deal']['is_ticketportal_deal'] ? 1 : 0;
            $is_365_deal = isset($deal['Deal']['is_clarin365_deal']) && $deal['Deal']['is_clarin365_deal'] ? 1 : 0;
            if ($is_fibertel_deal && !$this->Deal->hasSubdeals($deal['Deal']['id'])) {
                $type = ConstDealTypes::Fibertel;
                if ($is_logged_in) {
                    $link = array(
                        'javascript:void(0)',
                        array(
                            'title' => $deal['Deal']['name']
                        )
                    );
                } else {
                    $url_redirection = '../../users/login/?f=deal/' . $deal['Deal']['slug'];
                    $link = array(
                        $url_redirection,
                        array(
                            'title' => $deal['Deal']['name'],
                            'target' => '_top'
                        )
                    );
                }
            } else if ($is_ticketportal_deal) {
                $type = ConstDealTypes::Ticketportal;
                $link = array(
                    '#dialog_ticketportal',
                    array(
                        'title' => $deal['Deal']['name'],
                        'id' => 'bt_comprar',
                        'class' => 'bt_comprar'
                    )
                );
            } else if ($is_365_deal && !Configure::read('orderPlugin.enabled')) {
                $type = ConstDealTypes::Clarin365;
                $link = array(
                    '#dialog_clarin365',
                    array(
                        'title' => $deal['Deal']['name'],
                        'id' => 'bt_comprar',
                        'class' => 'bt_comprar'
                    )
                );
            } else if ($current_city == 'circa') {
                $type = ConstDealTypes::Circa;
                $link = array(
                    'javascript:void(0)',
                    array(
                        'onclick' => 'modalPopupGeneric("' . Router::url(array(
                            'controller' => 'firsts',
                            'action' => 'circa',
                            $deal['Deal']['id']
                                ), false) . '", 810, 301);',
                        'escape' => false,
                        'title' => 'Comprar oferta'
                    )
                );
            } else {
                $type = ConstDealTypes::Regular;
                $link = array(
                    array(
                        'controller' => 'deals',
                        'action' => 'buy',
                        $deal['Deal']['id']
                    ),
                    array(
                        'title' => 'Comprar oferta',
                        'target' => '_top'
                    )
                );
            }
            $deal_link_and_type['type'] = $type;
            $deal_link_and_type['link'] = $link;
        }
        return $deal_link_and_type;
    }

    function _isLogginUserCorporate() {
        if ($this->Auth->user('id')) {
            $dni = $this->_getUserProfileDni($this->Auth->user('id'));
            return $this->CorporateGuest->isValidCorporatePin($dni);
        }
        return false;
    }

    function _getUserProfileDni($user_id = null) {
        if (empty($user_id)) {
            throw new InvalidArgumentException("Corporate pin must not be empty");
        }
        $res = $this->UserProfile->find('first', array(
            'conditions' => array(
                'UserProfile.user_id' => $this->Auth->user('id')
            ),
            'fields' => array(
                'UserProfile.dni'
            )
        ));
        return $res['UserProfile']['dni'];
    }

    function _redirectToUnsecureHome() {
        return $this->redirect(STATIC_DOMAIN_FOR_CLUBCUPON);
    }

    function _redirectToUnsecureDealsBuyPending($deal_external_id) {
        return $this->redirect(STATIC_DOMAIN_FOR_CLUBCUPON . '/order/order_processings/buyPending/' . $deal_external_id);
    }

    function _redirectToUnsecureDealsBuyOk($deal_external_id) {
        return $this->_redirectToUnsecureDealsBuyPending($deal_external_id);
    }

    function _redirectToUnsecureDealsBuyCanceled($deal_external_id) {
        return $this->redirect(STATIC_DOMAIN_FOR_CLUBCUPON . '/order/order_processings/buyCanceled/' . $deal_external_id);
    }

    function admin_searchnow() {
        $q = $this->data['Deal']['q'];
        $this->pageTitle = 'Resultados para: ' . @$this->data['Deal']['q'];
        if (isset($this->data['Deal']['q'])) {
            $view = $this->data['Deal']['selected'];
            $conditions = array(
                'NowDeal.deal_status_id' => $view
            );
            if ($this->data['Deal']['filtro'] == '1') {
                $ids = $this->NowDeal->search($this->data['Deal']['q'], array(
                    'fields' => array(
                        'NowDeal.id'
                    ),
                    'conditions' => $conditions
                ));
                $newIds = array();
                if (isset($ids)) {
                    foreach ($ids as $i => $value) {
                        $newIds[] = $value[NowDeal][id];
                    }
                }
                $nowDeals = $this->NowDeal->findNowDealsByIds($newIds);
            } else {
                $condIdEquals = array(
                    'NowDeal.id' => $q
                );
                $conditions = array_merge($condIdEquals, $conditions);
                $nowDeals = $this->NowDeal->findDealBy($conditions);
            }
            $this->set('selected', $view);
            $this->set('q', $q);
            $this->set('view', $view);
            $this->set('nowDeals', $nowDeals);
        }
    }

    function admin_reindexnow() {
        $this->NowDeal->reindexAll(0);
        echo 'Index OK';
        die();
    }

    function _redirectToNowDealDetail($deal) {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_deals',
                    'action' => 'detalles',
                    $deal['Deal']['id']
        ));
    }

    function _redirectToDealView($deal) {
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug']
        ));
    }

    private function createScheduledDeals($deal_id, $options = array()) {
        $this->NowScheduledDeals->deleteAll(array(
            'deal_id' => $deal_id
                ), false, false);
        if (!isset($this->data['NowDeal']['scheduled_deals_end_date'])) {
            return false;
        }
        $scheduled_deals = array();
        $this->fixInitialDate();
        $scheduled_deals['NowScheduledDeals']['deal_id'] = $deal_id;
        $scheduled_deals['NowScheduledDeals']['start_date'] = $this->data['NowDeal']['start_date'];
        $scheduled_deals['NowScheduledDeals']['end_date'] = $this->data['NowDeal']['scheduled_deals_end_date'];
        foreach ($this->data['NowDeal']['scheduled_deals'] as $dia) {
            $scheduled_deals['NowScheduledDeals'][$dia] = true;
        }
        $scheduled_deals['NowScheduledDeals']['is_disabled'] = $this->data['NowDeal']['scheduled_deals_is_disabled'];
        $this->NowScheduledDeals->create();
        $this->NowScheduledDeals->save($scheduled_deals);
    }

    private function fixInitialDate() {
        $s = date("Y-m-d", strtotime($this->data['NowDeal']['start_date']));
        $e = date("Y-m-d", strtotime($this->data['NowDeal']['scheduled_deals_end_date']));
        $dias = $this->data['NowDeal']['scheduled_deals'];
        $initial_date = false;
        $r = array();
        while ($s <= $e) {
            foreach ($dias as $dia) {
                $date_string = date("Y-m-d", strtotime($s . " first " . $dia . " this week"));
                if ($date_string >= date("Y-m-d", strtotime($s)) && $date_string <= $e) {
                    $r[] = $date_string;
                }
            }
            $s = date("Y-m-d", strtotime($s . " next week"));
            if (!empty($r)) {
                break;
            }
        }
        asort($r);
        $this->data['NowDeal']['start_date'] = !empty($r) ? current($r) : $this->data['NowDeal']['start_date'];
    }

    private function _redirecToMobile($dealId, $dealExternalId, $amount, $token, $estado) {
        $path = '/api/api_deals/buy?deal_id=' . $dealId . '&token=' . $token . '&deal_external_id=' . $dealExternalId . '&paystatus=' . $estado;
        return $this->redirect($path, true);
    }

    private function getTokenByDealExternal($dealExternal) {
        App::import('Model', 'User');
        $this->User = & new User();
        $user = $this->User->read(null, $dealExternal['DealExternal']['user_id']);
        return $user['User']['token'];
    }

    function check_gift_code($code) {
        $this->layout = false;
        try {
            $giftPin = $this->GiftPinService->isUsable($code);
        } catch (Exception $e) {
            $giftPin = false;
            $this->set('respuesta', array(
                'status' => FALSE,
                'ret' => __LINE__ . " : " . $e->getMessage()
            ));
        }
        if ($giftPin) {
            $this->set('respuesta', array(
                'status' => TRUE,
                'ret' => $giftPin
            ));
        } else {
            $this->set('respuesta', array(
                'status' => FALSE,
                'ret' => 'El cÃ³digo no existe o ya fue utilizado.'
            ));
        }
    }

    function checkDealPriceDisplay($priceDisplayStatus) {
        switch ($priceDisplayStatus) {
            case 'hidePrice':
                $hidePrice = 1;
                $onlyPrice = 0;
                break;

            case 'onlyPrice':
                $hidePrice = 0;
                $onlyPrice = 1;
                break;

            default:
                $hidePrice = 0;
                $onlyPrice = 0;
        }
        return array(
            'hidePrice' => $hidePrice,
            'onlyPrice' => $onlyPrice
        );
    }

    function isSubscriptionEnabledForCurrentCity($citySlug) {
        return $this->Deal->City->isUserSubscriptionEnabled($citySlug);
    }

    function _whenSubscriptionIsNotEnabled($citySlug) {
        $html = '&nbsp';
        if ($citySlug == 'mcdonalds') {
            return 'Tel. 0800-222-1035';
        }
        return $html;
    }

    function _formatPaymentPeriodsArray() {
        $paymentPeriods = Configure::read('deal.paymentTerms');
        $tmpArray = array();
        foreach ($paymentPeriods as $key => $value) {
            $tmpArray[$value] = $key;
        }
        return $tmpArray;
    }

    function generateAlphaNumericSeedForCampaignCode() {
        return substr(String::uuid(), -12);
    }

    function _isParentDealCreation() {
        return empty($this->params['named']['parent_deal_id']);
    }

    public function _canCalcSegmentationProfile($deal) {
        $canCalcSegmentation = isset($deal['Deal']['gender']) && !empty($deal['Deal']['gender']) && isset($deal['Deal']['deal_category_id']) && !empty($deal['Deal']['deal_category_id']);
        return $canCalcSegmentation;
    }

    public function _calcSegmentationProfileForDeal($deal) {
        $this->ApiLogger->debug(__METHOD__, array(
            'gender' => $deal['Deal']['gender'],
            'deal_category_id' => $deal['Deal']['deal_category_id']
        ));
        $dealFlatCategory = $this->DealFlatCategory->findById($deal['Deal']['deal_category_id']);
        $this->ApiLogger->trace('dealFlatCategory', $dealFlatCategory);
        $segmentationProfile = $this->EfgSegmentationProfile->findSingleMatchCategory($dealFlatCategory['DealFlatCategory']['l1_id'], array(
            'sexo' => $this->_convertGenderToProfileConstant($deal['Deal']['gender'])
        ));
        return $segmentationProfile;
    }

    public function _calcAndUpdateSegmentationProfile($deal) {
        $this->ApiLogger->debug(__METHOD__, $deal);
    }

    private function _convertGenderToProfileConstant($gender) {
        $this->ApiLogger->debug(__METHOD__, $gender);
        $segmentationSexCode;
        if ($gender == Deal::GENDER_MALE) {
            $segmentationSexCode = ConstSegmentationSex::MALE;
        } else if ($gender == Deal::GENDER_FEMALE) {
            $segmentationSexCode = ConstSegmentationSex::FEMALE;
        } else if ($gender == Deal::GENDER_UNISEX) {
            $segmentationSexCode = ConstSegmentationSex::UNDEFINED;
        } else {
            $segmentationSexCode = ConstSegmentationSex::UNDEFINED;
            $this->ApiLogger->error("El valor del campo genero para una oferta no es valido, se retorn el valor unisex por defecto.", array(
                'dealGenderParam' => $gender,
                'dealGenderValoresValidos' => configure::read('deal_gender.All'),
                'segmentationValorPorDefecto' => $segmentationSexCode
            ));
        }
        return $segmentationSexCode;
    }

}
