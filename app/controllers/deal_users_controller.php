<?php

class DealUsersController extends AppController {

    var $name = 'DealUsers';
    var $uses = array(
        'DealUser',
        'Redemption',
        'AccountingMaxPaymentDateGrouped',
        'Deal'
    );
    var $components = array(
        'Email',
        'api.ApiLogger'
    );

    function beforeFilter() {
        $this->DealUser->Behaviors->detach('Excludable');
        $this->Security->disabledFields = array(
            'DealUser.more_action_id',
            'DealUser.deal_name',
            'DealUser.coupon_code',
            'DealUser.r',
            'DealUser.id',
            'DealUser.filter_id',
            'DealUser.deal_id',
            'Deal.id'
        );
        AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }

    function beforeRender() {
        $this->__setNextLiquidation();
        parent::beforeRender();
    }

    function __setNextLiquidation() {
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $conditionsForFindByCompanyId['conditions']['company_id'] = $company['Company']['id'];
        $nextLiq = $this->AccountingMaxPaymentDateGrouped->__findFirstByCompanyIdGroupedByMaxPaymentDate($conditionsForFindByCompanyId);
        $this->set('nextLiquidationDate', $nextLiq['AccountingMaxPaymentDateGrouped']['max_payment_date']);
        $this->set('nextLiquidationValue', $nextLiq['AccountingMaxPaymentDateGrouped']['liquidate_total_amount']);
    }

    function index($deal_id = null) {
        Debugger::log(sprintf('DealUser->index - userId %d', $this->Auth->user('id')), LOG_DEBUG);
        $paymentsPending = $this->DealUser->User->getPaymentsPending($this->Auth->user('id'));
        $this->set('paymentsPending', $paymentsPending);
        $this->_redirectGET2Named(array(
            'coupon_code'
        ));
        $this->set('certificaPath', 'ClubCupon/cpanel/coupons');
        if (!empty($this->data['DealUser']['deal_id'])) {
            $this->params['named']['deal_id'] = $this->data['DealUser']['deal_id'];
        }
        $this->pageTitle = 'Mis Cupones';
        $filterConditions = $conditions = array();
        if (!empty($this->data['DealUser']['coupon_code'])) {
            $filterConditions['DealUser.coupon_code'] = $this->data['DealUser']['coupon_code'];
            $this->pageTitle.= ' - ' . $this->data['DealUser']['coupon_code'];
        }
        if ($this->Auth->user('user_type_id') == ConstUserTypes::Company && !empty($deal_id)) {
            $filterConditions['DealUser.deal_id'] = $deal_id;
            $this->pageTitle = '';
        } else if (isset($this->params['named']['deal_id'])) {
            $this->data['DealUser']['deal_id'] = $this->params['named']['deal_id'];
            $filterConditions['Deal.id'] = $this->params['named']['deal_id'];
        } elseif ($this->DealUser->User->isAllowed($this->Auth->user('user_type_id'))) {
            $filterConditions['DealUser.user_id'] = $this->Auth->user('id');
        } else {
            $this->cakeError('error404');
        }
        $filterConditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Closed,
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany,
            ConstDealStatus::PendingApproval
        );
        if (!empty($this->params['named']['type'])) {
            $selectedType = $this->params['named']['type'];
        }
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'available') {
            $conditions = $this->DealUser->conditionsForAvailable($filterConditions);
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'used') {
            $conditions = $this->DealUser->conditionsForUsed($filterConditions);
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'expired') {
            $conditions = $this->DealUser->conditionsForExpired($filterConditions);
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'open') {
            $conditions = $this->DealUser->conditionsForPending($filterConditions);
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'all') {
            $conditions = $this->DealUser->conditionsForAll($filterConditions);
            if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                $this->pageTitle = 'Mis Cupones';
            } else {
                $this->pageTitle = 'Mis Cupones';
            }
        }
        Debugger::log('DealUser->index - conditions antes' . debugEncode($conditions), LOG_DEBUG);
        $aditionalConditions['DealUser.user_id'] = $this->Auth->user('id');
        if ($this->Auth->user('id') == null) {
            $aditionalConditions['DealUser.user_id'] = 0;
        }
        $conditions = array_merge($conditions, $aditionalConditions);
        Debugger::log('DealUser->index - conditions despues' . debugEncode($conditions), LOG_DEBUG);
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                ),
                'Redemption' => array(
                    'fields' => array(
                        'posnet_code'
                    )
                ),
                'Deal' => array(
                    'Attachment',
                    'fields' => array(
                        'Deal.name',
                        'Deal.slug',
                        'Deal.discounted_price',
                        'Deal.coupon_expiry_date',
                        'Deal.deal_status_id',
                        'Deal.company_id',
                        'Deal.is_now'
                    )
                )
            ),
            'fields' => array(
                'DealUser.id',
                'DealUser.coupon_code',
                'DealUser.created',
                'DealUser.quantity',
                'DealUser.discount_amount',
                'DealUser.is_used',
                'DealUser.is_used_user',
                'DealUser.deal_id',
                'IF(
                    unix_timestamp(Deal.coupon_expiry_date)< unix_timestamp(now()) AND DealUser.emailed=1 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, vencido",
                    IF(
                        unix_timestamp(Deal.coupon_expiry_date)< unix_timestamp(now()), "Vencido",
                        (
                            IF(
                                DealUser.emailed=0 AND DealUser.is_used=0 AND DealUser.is_gift=0, "Pendiente",
                                (
                                    IF(
                                        DealUser.emailed=0 AND DealUser.is_used=0 AND DealUser.is_gift=1, "Regalo, disponible",
                                        IF(
                                            DealUser.emailed=0 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, usado",
                                            IF(
                                                DealUser.emailed=1 AND DealUser.is_used=0 AND DealUser.is_gift=1, "Regalo, enviado",
                                                IF(
                                                    DealUser.emailed=1 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, usado",
                                                    IF(
                                                        DealUser.emailed=1 AND DealUser.is_used=0 AND DealUser.is_gift=0 AND Deal.deal_status_id!=2, "Disponible", "Usado"
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                  )
                ) as coupon_status',
                'IF(Deal.is_variable_expiration=0, "Fijo","Variable") as is_variable_expiration',
                'IF(Deal.is_variable_expiration = 0,
                   IF(HOUR(Deal.coupon_expiry_date) + MINUTE(Deal.coupon_expiry_date) = 0, DATE(DATE_ADD(Deal.coupon_expiry_date, INTERVAL -1 DAY)), DATE(Deal.coupon_expiry_date)), 
                   DATE(DATE_ADD(GREATEST(Deal.start_date, Deal.coupon_start_date, DealUser.created), INTERVAL GREATEST(Deal.coupon_duration - 1, 0) DAY))
                ) AS real_expiration'
            ),
            'order' => array(
                'DealUser.created' => 'desc'
            )
        );
        if (!empty($deal_id) && ($this->Auth->user('user_type_id') != ConstUserTypes::User)) {
            $filterConditions['DealUser.deal_id'] = $deal_id;
        } else {
            $filterConditions['DealUser.user_id'] = $this->Auth->user('id');
        }
        $user = $this->DealUser->Deal->User->Company->findByUserId($this->Auth->User('id'));
        $this->set('user', $user);
        $this->set('dealUsers', $this->paginate());
        $this->set('deal_id', $deal_id);
        $title_for_layout = $this->pageTitle;
        $this->set('title_for_layout', $title_for_layout);
        $moreActions = $this->DealUser->moreActions;
        if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
            unset($moreActions[ConstMoreAction::Used], $moreActions[ConstMoreAction::NotUsed]);
        }
        $this->set(compact('moreActions'));
        $this->set('available', "");
        $this->set('used', "");
        $this->set('expired', "");
        $this->set('open', "");
        $this->set('all_deals', "");
        $this->set('user_type_id', $this->Auth->user('user_type_id'));
        $this->set('selected_type', $selectedType);
        if (!empty($this->params['named']['type']))
            $this->set('param_type', $this->params['named']['type']);
    }

    function index_for_company($deal_id = null) {
        $this->_redirectGET2Named(array(
            'coupon_code'
        ));
        if (!empty($this->data['DealUser']['deal_id'])) {
            $this->params['named']['deal_id'] = $this->data['DealUser']['deal_id'];
        }
        if (!empty($this->params['named']['deal_id'])) {
            $deal_id = $this->params['named']['deal_id'];
        }
        $this->pageTitle = __l('Deal Coupons');
        $filterConditions = $conditions = array();
        if (!empty($this->data['DealUser']['coupon_code'])) {
            $filterConditions['DealUser.coupon_code'] = $this->data['DealUser']['coupon_code'];
            $this->pageTitle.= ' - ' . $this->data['DealUser']['coupon_code'];
        }
        if (!empty($this->data['DealUser']['posnet_code'])) {
            $dealUserIdsByPosnetCode = $this->_findDealUserIdsByPosnetCode($this->data['DealUser']['posnet_code']);
            $filterConditions['DealUser.id'] = $dealUserIdsByPosnetCode;
            $this->pageTitle.= sprintf(__l(' - Search - %s'), $this->data['DealUser']['posnet_code']);
        }
        $this->set('user_type_id', $this->Auth->user('user_type_id'));
        if ($this->Auth->user('user_type_id') != ConstUserTypes::Company) {
            die('Error: No company or no deal_id');
            $this->cakeError('error404');
        }
        $user = $this->DealUser->Deal->User->Company->findByUserId($this->Auth->User('id'));
        $noFields = empty($this->params['named']['page']);
        if (!empty($deal_id) && $deal_id != 'all') {
            $filterConditions['DealUser.deal_id'] = $deal_id;
            $noFields = false;
        }
        $filterConditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Closed,
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany,
            ConstDealStatus::PendingApproval
        );
        $conditions = $this->DealUser->conditionsForAll($filterConditions);
        $this->pageTitle = sprintf(__l('My %s Coupons'), Configure::read('site.name'));
        if (!empty($this->data["DealUser"]['search_dni'])) {
            $conditions['UserProfile.dni like'] = '%' . $this->data["DealUser"]['search_dni'] . '%';
            $noFields = false;
        }
        if (!empty($this->data["DealUser"]['search_friend_dni'])) {
            $conditions['DealUser.gift_dni like'] = '%' . $this->data["DealUser"]['search_friend_dni'] . '%';
            $noFields = false;
        }
        if (!empty($this->data["DealUser"]['search_username'])) {
            $conditions['User.username like'] = '%' . $this->data["DealUser"]['search_username'] . '%';
            $noFields = false;
        }
        if (!empty($this->data["DealUser"]['search_coupon'])) {
            $conditions['DealUser.coupon_code like'] = '%' . $this->data["DealUser"]['search_coupon'] . '%';
            $noFields = false;
        }
        if (!empty($this->data["DealUser"]['search_coupon'])) {
            $conditions['DealUser.coupon_code like'] = '%' . $this->data["DealUser"]['search_coupon'] . '%';
            $noFields = false;
        }
        if (!empty($this->data['DealUser']['posnet_code'])) {
            $noFields = false;
        }
        $conditions['Deal.company_id'] = $user['Company']['id'];
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'UserProfile' => array(
                    'fields' => array(
                        'UserProfile.dni',
                        'UserProfile.first_name',
                        'UserProfile.last_name'
                    )
                ),
                'User' => array(
                    'fields' => array(
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                ),
                'Deal' => array(
                    'fields' => array(
                        'Deal.name',
                        'Deal.company_id',
                        'Deal.slug',
                        'Deal.coupon_expiry_date',
                        'Deal.deal_status_id',
                        'Deal.company_id',
                        'Deal.publication_channel_type_id',
                        'Deal.discounted_price'
                    )
                ),
                'Redemption' => array(
                    'fields' => array(
                        'Redemption.redeemed',
                        'Redemption.way'
                    )
                )
            ),
            'fields' => array(
                'DealUser.id',
                'DealUser.coupon_code',
                'DealUser.created',
                'DealUser.quantity',
                'DealUser.is_used',
                'DealUser.deal_id',
                'DealUser.is_gift',
                'DealUser.is_returned',
                'UserProfile.dni',
                'UserProfile.first_name',
                'UserProfile.last_name',
                'Deal.publication_channel_type_id',
                'Deal.discounted_price'
            ),
            'order' => array(
                'Deal.id' => 'desc'
            )
        );
        $this->set('user', $user);
        if (!empty($this->data) || !$noFields) {
            if ($noFields) {
                $this->Session->setFlash('Al menos un criterio de búsqueda debe ser definido', 'default', null, 'error');
            } else {
                $deals_users = $this->paginate();
                $this->set('dealUsers', $deals_users);
                if (!$deals_users) {
                    $this->data = array();
                    $this->Session->setFlash('No se han encontrado resultados', 'default', null, 'error');
                }
            }
        }
        $this->set('deal_id', $deal_id);
        $this->set('pageTitle', $this->pageTitle);
        $this->Deal->recursive = - 1;
        $this->set('deal', $this->DealUser->Deal->read(null, $deal_id));
        $moreActions = $this->DealUser->moreActions;
        unset($moreActions[ConstMoreAction::Delete]);
        $this->set('moreActions', $moreActions);
        $this->layout = 'now';
    }

    function gifted_deals($deal_id = null) {
        $this->_redirectGET2Named(array(
            'coupon_code'
        ));
        if (!empty($this->data['DealUser']['deal_id'])) {
            $this->params['named']['deal_id'] = $this->data['DealUser']['deal_id'];
        }
        $this->set('certificaPath', 'ClubCupon/cpanel/giftcoupons');
        $this->pageTitle = 'Mis Cupones de Regalo';
        $conditions = array();
        if (!empty($this->data['DealUser']['coupon_code'])) {
            $conditions['DealUser.coupon_code'] = $this->data['DealUser']['coupon_code'];
            $this->pageTitle.= ' - ' . $this->data['DealUser']['coupon_code'];
        }
        if ($this->Auth->user('user_type_id') == ConstUserTypes::Company && !empty($deal_id)) {
            $conditions['DealUser.deal_id'] = $deal_id;
            $conditions['DealUser.is_repaid'] = 0;
            $this->pageTitle = '';
        } else if (isset($this->params['named']['deal_id'])) {
            $this->data['DealUser']['deal_id'] = $this->params['named']['deal_id'];
            $conditions['Deal.id'] = $this->params['named']['deal_id'];
        } elseif ($this->DealUser->User->isAllowed($this->Auth->user('user_type_id'))) {
            $conditions['DealUser.user_id'] = $this->Auth->user('id');
        } else {
            $this->cakeError('error404');
        }
        $conditions['DealUser.is_gift'] = 1;
        if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'available') {
            $conditions['DealUser.is_used'] = 0;
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Closed,
                ConstDealStatus::Tipped,
                ConstDealStatus::PaidToCompany
            );
            $conditions['Deal.coupon_expiry_date >='] = date('Y-m-d H:i:s');
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'used') {
            $conditions['DealUser.is_used'] = 1;
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'open') {
            $conditions['Deal.deal_status_id'] = ConstDealStatus::Open;
        } elseif (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'all') {
            if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
                $this->pageTitle = 'Mis Cupones de Regalo';
            } else {
                $this->pageTitle = 'Mis Cupones de Regalo';
            }
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                ),
                'Deal' => array(
                    'Attachment',
                    'fields' => array(
                        'Deal.name',
                        'Deal.slug',
                        'Deal.coupon_expiry_date',
                        'Deal.deal_status_id',
                        'Deal.company_id'
                    )
                ),
                'Redemption' => array(
                    'fields' => array(
                        'Redemption.posnet_code'
                    )
                )
            ),
            'fields' => array(
                'DealUser.id',
                'DealUser.coupon_code',
                'DealUser.created',
                'DealUser.quantity',
                'DealUser.discount_amount',
                'DealUser.is_used',
                'DealUser.is_used_user',
                'DealUser.deal_id',
                'IF(
                    unix_timestamp(Deal.coupon_expiry_date)< unix_timestamp(now()) AND DealUser.emailed=1 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, vencido",
                    IF(
                        unix_timestamp(Deal.coupon_expiry_date)< unix_timestamp(now()), "Vencido",
                        (
                            IF(
                                DealUser.emailed=0 AND DealUser.is_used=0 AND DealUser.is_gift=0, "Pendiente",
                                (
                                    IF(
                                        DealUser.emailed=0 AND DealUser.is_used=0 AND DealUser.is_gift=1, "Regalo, disponible",
                                        IF(
                                            DealUser.emailed=0 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, usado",
                                            IF(
                                                DealUser.emailed=1 AND DealUser.is_used=0 AND DealUser.is_gift=1, "Regalo, enviado",
                                                IF(
                                                    DealUser.emailed=1 AND DealUser.is_used=1 AND DealUser.is_gift=1, "Regalo, usado",
                                                    IF(
                                                        DealUser.emailed=1 AND DealUser.is_used=0 AND DealUser.is_gift=0 AND Deal.deal_status_id!=2, "Disponible", "Usado"
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                  )
                ) as coupon_status',
                'IF(Deal.is_variable_expiration=0, "Fijo","Variable") as is_variable_expiration',
                'IF(Deal.is_variable_expiration = 0,
                   IF(HOUR(Deal.coupon_expiry_date) + MINUTE(Deal.coupon_expiry_date) = 0, DATE(DATE_ADD(Deal.coupon_expiry_date, INTERVAL -1 DAY)), DATE(Deal.coupon_expiry_date)), 
                   DATE(DATE_ADD(GREATEST(Deal.start_date, Deal.coupon_start_date, DealUser.created), INTERVAL GREATEST(Deal.coupon_duration - 1, 0) DAY))
                ) AS real_expiration'
            ),
            'order' => array(
                'Deal.slug' => 'desc'
            )
        );
        if (!empty($deal_id) && ($this->Auth->user('user_type_id') != ConstUserTypes::User)) {
            $conditions['DealUser.deal_id'] = $deal_id;
        } else {
            $conditions['DealUser.user_id'] = $this->Auth->user('id');
        }
        $user = $this->DealUser->Deal->User->Company->findByUserId($this->Auth->User('id'));
        $this->set('user', $user);
        $this->set('dealUsers', $this->paginate());
        $this->set('deal_id', $deal_id);
        $this->log('titulo:' . $this->pageTitle);
        $title_for_layout = $this->pageTitle;
        $this->set('title_for_layout', $title_for_layout);
        $moreActions = $this->DealUser->moreActions;
        if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
            unset($moreActions[ConstMoreAction::Used], $moreActions[ConstMoreAction::NotUsed]);
        }
        $this->set(compact('moreActions'));
        $conditionsSubQuery['`Deal`.`coupon_expiry_date` >='] = date('Y-m-d H:i:s');
        $conditionsSubQuery['`Deal`.`deal_status_id`'] = array(
            ConstDealStatus::Closed,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany
        );
        $dbo = $this->DealUser->getDataSource();
        $subQuery = $dbo->buildStatement(array(
            'fields' => array(
                '`Deal`.`id`'
            ),
            'table' => $dbo->fullTableName($this->DealUser->Deal),
            'alias' => 'Deal',
            'limit' => null,
            'offset' => null,
            'joins' => array(),
            'conditions' => $conditionsSubQuery,
            'order' => null,
            'group' => null
                ), $this->DealUser);
        $this->set('available', "");
        $this->set('used', "");
        $this->set('open', "");
        $this->set('gifted_deals', "");
        $this->set('all_deals', "");
        if (!empty($this->params['named']['type'])) {
            $selectedType = $this->params['named']['type'];
        }
        $this->set('selected_type', $selectedType);
    }

    function update() {
        $this->autoRender = false;
        if (!empty($this->data['DealUser'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $dealUserIds = array();
            foreach ($this->data['DealUser'] as $dealuser_id => $is_checked) {
                if ($is_checked['id']) {
                    $dealUserIds[] = $dealuser_id;
                }
            }
            if ($actionid && !empty($dealUserIds)) {
                if ($actionid == ConstMoreAction::Used) {
                    $this->DealUser->updateAll(array(
                        'DealUser.is_used' => 1
                            ), array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash(__l('Checked users status has been changed'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::NotUsed) {
                    $this->DealUser->updateAll(array(
                        'DealUser.is_used' => 0
                            ), array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash(__l('Checked users status has been changed'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Delete) {
                    $this->DealUser->deleteAll(array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash(__l('Se borraron los registros'), 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function _findViewDataByDealUserId($id) {
        $fields = array(
            'City.is_business_unit',
            'City.name',
            'Company.address1',
            'Company.address2',
            'Company.name',
            'Deal.coupon_condition',
            'Deal.coupon_duration',
            'Deal.coupon_expiry_date',
            'Deal.coupon_start_date',            
            'Deal.custom_company_address1',
            'Deal.custom_company_address2',
            'Deal.custom_company_city',
            'Deal.custom_company_contact_phone',
            'Deal.custom_company_name',
            'Deal.deal_category_id',
            'Deal.description',
            'Deal.descriptive_text',
            'Deal.discounted_price',
            'Deal.id',
            'Deal.is_now',
            'Deal.is_variable_expiration',
            'Deal.name',
            'Deal.parent_deal_id',
            'Deal.start_date',
            'DealExternal.shipping_address_id',
            'DealUser.coupon_code',
            'DealUser.created',
            'DealUser.gift_dni',
            'DealUser.pin_code',
            'DealUser.coupon_code',
            'DealUser.created',
            'DealUser.gift_dni',
            'DealUser.gift_to',
            'DealUser.id',
            'DealUser.is_gift',
            'DealUser.message',
            'User.id',
            'User.username',
            'UserProfile.dni'
        );
        $joins = array(
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'conditions' => array(
                    'Deal.id = DealUser.deal_id'
                )
            ),
            array(
                'table' => 'deal_externals',
                'alias' => 'DealExternal',
                'type' => 'inner',
                'conditions' => array(
                    'DealExternal.id = DealUser.deal_external_id'
                )
            ),
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'inner',
                'conditions' => array(
                    'Company.id = Deal.company_id'
                )
            ),
            array(
                'table' => 'cities',
                'alias' => 'City',
                'type' => 'inner',
                'conditions' => array(
                    'City.id = Deal.city_id'
                )
            ),
            array(
                'table' => 'users',
                'alias' => 'User',
                'type' => 'inner',
                'conditions' => array(
                    'User.id = DealUser.user_id'
                )
            ),
            array(
                'table' => 'user_profiles',
                'alias' => 'UserProfile',
                'type' => 'inner',
                'conditions' => array(
                    'UserProfile.user_id = User.id'
                )
            )
        );
        $conditions = array(
            'DealUser.id' => $id
        );
        return $this->DealUser->find('first', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'recursive' => -1
        ));
    }

    function view($id = null, $deal_id = null) {
        $data = $this->_findViewDataByDealUserId($id);
        if (empty($data)) {
            $this->cakeError('error404');
        }
        if ($this->params['named']['type'] == 'print') {
            $this->layout = 'print';
        }
        $emailFindReplace = array(
            '##DESCRIPTIVE_TEXT##' => (!empty($data['Deal']['descriptive_text'])) ? ' (' . $data['Deal']['descriptive_text'] . ')' : '',
            '##COUPON_PIN##' => empty($data['DealUser']['pin_code']) ? '' : ('<br />PIN de tu cup&oacute;n: <strong>' . $data['DealUser']['pin_code'] . '</strong>'),
            '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
            '##DEAL_TITLE##' => $data['Deal']['name'],
            '##DEAL_NAME##' => $data['Deal']['name'],
            '##SITE_NAME##' => Configure::read('site.name'),
            '##COUPON_CODE##' => $data['DealUser']['coupon_code'],
            '##USER_NAME##' => $data['User']['username'],
            '##COMPANY_NAME##' => !empty($data['Deal']['custom_company_name']) ? $data['Deal']['custom_company_name'] : $data['Company']['name'],
            '##COMPANY_ADDRESS_1##' => !empty($data['Deal']['custom_company_address1']) ? $data['Deal']['custom_company_address1'] : $data['Company']['address1'],
            '##COMPANY_ADDRESS_2##' => !empty($data['Deal']['custom_company_address2']) ? $data['Deal']['custom_company_address2'] : $data['Company']['address2'],
            '##COMPANY_PHONE##' => !empty($data['Deal']['custom_company_contact_phone']) ? $data['Deal']['custom_company_contact_phone'] : '',
            '##COMPANY_CITY##' => !empty($data['Deal']['custom_company_city']) ? $data['Deal']['custom_company_city'] : $data['City']['name'],
            '##COUPON_EXPIRY_DATE##' => htmlentities(strftime(($data['Deal']['is_now'] ? "%d/%m/%Y %H:%M" : "%d/%m/%Y"), strtotime($data['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8'),
            '##COUPON_PURCHASED_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($data['DealUser']['created'])), ENT_QUOTES, 'UTF-8'),
            '##COUPON_CONDITION##' => ($data['Deal']['is_now'] ? $data['Deal']['description'] : $data['Deal']['coupon_condition']),
            '##USER_DNI##' => isset($dniIndexedByUserId[$data['User']['id']]) ? $this->Deal->_formatLikeDNI($dniIndexedByUserId[$data['User']['id']]) : '',
            '##GIFT_DNI##' => !empty($data['DealUser']['gift_dni']) ? $this->Deal->_formatLikeDNI($data['DealUser']['gift_dni']) : '',
            '##SITE_LOGO##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'logo-email.png',
                'admin' => false
                    ), false),
            '##BARCODE##' => Configure::read('static_domain_for_mails') . '/img/barcode.jpeg',
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##ABSOLUTE_IMG_PATH_2##' => Configure::read('static_domain_for_mails_2') . '/img/email/',
            '##IS_NOW##' => $data['Deal']['is_now']
        );
        if ($this->Auth->user('user_type_id') == ConstUserTypes::Company) {
            $content_view = Deal::generateCouponContent($data, $emailFindReplace, true);
        } else {
            $content_view = Deal::generateCouponContent($data, $emailFindReplace);
        }
        echo $content_view;
    }

    function user_deals($user_id = null) {
        if (!empty($this->params['named']['user_id'])) {
            $conditions = array(
                'DealUser.user_id' => $this->params['named']['user_id']
            );
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'Deal' => array(
                    'Attachment' => array(
                        'fields' => array(
                            'Attachment.id',
                            'Attachment.dir',
                            'Attachment.filename',
                            'Attachment.width',
                            'Attachment.height'
                        )
                    ),
                    'fields' => array(
                        'Deal.name',
                        'Deal.slug'
                    )
                )
            )
        );
        $this->DealUser->recursive = 2;
        $user_deals = $this->paginate();
        $this->set('user_deals', $user_deals);
    }

    function _findDealUserIdsByPosnetCode($posnetCode) {
        $dealUserIds = array();
        $findResult = $this->Redemption->find('all', array(
            'conditions' => array(
                'Redemption.posnet_code' => $posnetCode
            ),
            'fields' => 'deal_user_id'
        ));
        foreach ($findResult as $item) {
            $dealUserIds[] = $item['Redemption']['deal_user_id'];
        }
        return $dealUserIds;
    }

    function admin_index() {
        $this->pageTitle = 'Todos los cupones';
        if (!empty($this->data['DealUser']['deal_name'])) {
            $this->params['named']['deal_name'] = $this->data['DealUser']['deal_name'];
        }
        if (!empty($this->data['DealUser']['coupon_code'])) {
            $this->params['named']['coupon_code'] = $this->data['DealUser']['coupon_code'];
        }
        if (!empty($this->data['DealUser']['posnet_code'])) {
            $this->params['named']['posnet_code'] = $this->data['DealUser']['posnet_code'];
        }
        if (!empty($this->data['DealUser']['filter_id'])) {
            $this->params['named']['filter_id'] = $this->data['DealUser']['filter_id'];
        }
        if (!empty($this->data['DealUser']['deal_id'])) {
            $this->params['named']['deal_id'] = $this->data['DealUser']['deal_id'];
        }
        if (!empty($this->data['DealUser']['dealuser_id'])) {
            $this->params['named']['dealuser_id'] = $this->data['DealUser']['dealuser_id'];
        }
        if (!empty($this->data['DealUser']['dealexternal_id'])) {
            $this->params['named']['dealexternal_id'] = $this->data['DealUser']['dealexternal_id'];
        }
        if (!empty($this->data['DealUser']['dealexternal_bac_id'])) {
            $this->params['named']['dealexternal_bac_id'] = $this->data['DealUser']['dealexternal_bac_id'];
        }
        $conditions = array();
        $conditions['DealUser.is_repaid'] = 0;
        $param_string = '';
        if (!empty($this->data['DealUser'])) {
            $param_string.=!empty($this->params['named']['filter_id']) ? '/filter_id:' . $this->params['named']['filter_id'] : '';
            $param_string.=!empty($this->params['named']['deal_name']) ? '/deal_name:' . $this->params['named']['deal_name'] : '';
            $param_string.=!empty($this->params['named']['coupon_code']) ? '/coupon_code:' . $this->params['named']['coupon_code'] : '';
            $param_string.=!empty($this->params['named']['deal_id']) ? '/deal_id:' . $this->params['named']['deal_id'] : '';
            $param_string.=!empty($this->params['named']['posnet_code']) ? '/posnet_code:' . $this->params['named']['posnet_code'] : '';
            $param_string.=!empty($this->params['named']['deal_user_id']) ? '/deal_user_id:' . $this->params['named']['deal_user_id'] : '';
        }
        if (isset($this->params['named']['dealexternal_id'])) {
            $this->data['DealExternal']['id'] = $this->params['named']['dealexternal_id'];
            $conditions['DealExternal.id'] = $this->params['named']['dealexternal_id'];
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['dealexternal_id']);
        }
        if (isset($this->params['named']['dealexternal_bac_id'])) {
            $this->data['DealExternal']['bac_id'] = $this->params['named']['dealexternal_bac_id'];
            $conditions['DealExternal.bac_id'] = $this->params['named']['dealexternal_bac_id'];
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['dealexternal_bac_id']);
        }
        if (isset($this->params['named']['dealuser_id'])) {
            $this->data['DealUser']['id'] = $this->params['named']['dealuser_id'];
            $conditions['DealUser.id'] = $this->params['named']['dealuser_id'];
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['dealuser_id']);
        }
        if (isset($this->params['named']['deal_name'])) {
            $this->data['DealUser']['deal_name'] = $this->params['named']['deal_name'];
            $deal_name = $this->params['named']['deal_name'];
            $conditions['Deal.name LIKE'] = '%' . $deal_name . '%';
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['deal_name']);
        }
        if (!empty($this->params['named']['coupon_code'])) {
            $this->data['DealUser']['coupon_code'] = $this->params['named']['coupon_code'];
            $conditions['DealUser.coupon_code'] = $this->params['named']['coupon_code'];
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['coupon_code']);
        }
        if (!empty($this->params['named']['posnet_code'])) {
            $this->data['DealUser']['posnet_code'] = $this->params['named']['posnet_code'];
            $dealUserIdsByPosnetCode = $this->_findDealUserIdsByPosnetCode($this->params['named']['posnet_code']);
            $conditions['DealUser.id'] = $dealUserIdsByPosnetCode;
            $this->pageTitle.= sprintf(' - Buscar - %s', $this->params['named']['posnet_code']);
        }
        if (isset($this->params['named']['deal_id'])) {
            $conditions['Deal.id'] = $this->params['named']['deal_id'];
            $this->data['DealUser']['deal_id'] = $this->params['named']['deal_id'];
        }
        if (isset($this->params['named']['deal_id'])) {
            $conditions['Deal.id'] = $this->params['named']['deal_id'];
            $this->data['DealUser']['deal_id'] = $this->params['named']['deal_id'];
        }
        if (!empty($this->params['named']['filter_id']) && $this->params['named']['filter_id'] == 'available') {
            $conditions['DealUser.is_used'] = 0;
            $conditions['DealUser.is_gift'] = 0;
            $conditions['Deal.coupon_expiry_date >='] = date('Y-m-d H:i:s');
        } elseif (!empty($this->params['named']['filter_id']) && $this->params['named']['filter_id'] == 'used') {
            $conditions['DealUser.is_used'] = 1;
        } elseif (!empty($this->params['named']['filter_id']) && $this->params['named']['filter_id'] == 'gifted_deals') {
            $conditions['DealUser.is_gift'] = 1;
        } elseif (!empty($this->params['named']['filter_id']) && $this->params['named']['filter_id'] == 'open') {
            $conditions['Deal.deal_status_id'] = ConstDealStatus::Open;
        } elseif (!empty($this->params['named']['filter_id']) && $this->params['named']['filter_id'] == 'refunded') {
            $conditions['Deal.deal_status_id'] = ConstDealStatus::Refunded;
            $conditions['DealUser.is_repaid'] = 1;
        } else {
            unset($conditions['DealUser.is_repaid']);
        }
        if (!empty($this->params['named']['filter_id'])) {
            $this->data['DealUser']['filter_id'] = $this->params['named']['filter_id'];
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'contain' => array(
                'User' => array(
                    'UserAvatar',
                    'fields' => array(
                        'User.user_type_id',
                        'User.username',
                        'User.id'
                    )
                ),
                'Deal' => array(
                    'fields' => array(
                        'Deal.name',
                        'Deal.slug',
                        'Deal.publication_channel_type_id',
                        'Deal.discounted_price'
                    ),
                    'Attachment' => array(
                        'fields' => array(
                            'Attachment.id',
                            'Attachment.dir',
                            'Attachment.filename',
                            'Attachment.mimetype',
                            'Attachment.filesize',
                            'Attachment.height',
                            'Attachment.width'
                        )
                    )
                ),
                'Redemption' => array(
                    'fields' => array(
                        'Redemption.posnet_code',
                        'Redemption.id',
                        'Redemption.redeemed',
                        'Redemption.way'
                    )
                ),
                'DealExternal' => array(
                    'fields' => array(
                        'DealExternal.id',
                        'DealExternal.bac_id'
                    )
                )
            ),
            'order' => array(
                'DealUser.id' => 'desc'
            )
        );
        $this->set('dealUsers', $this->paginate());
        $moreActions = $this->DealUser->moreActions;
        unset($moreActions[ConstMoreAction::Delete]);
        $this->set(compact('moreActions'));
        $this->set('param_string', $param_string);
        $this->set('pageTitle', $this->pageTitle);
    }

    function admin_update() {
        $this->autoRender = false;
        if (!empty($this->data['DealUser'])) {
            $r = $this->data[$this->modelClass]['r'];
            $actionid = $this->data[$this->modelClass]['more_action_id'];
            unset($this->data[$this->modelClass]['r']);
            unset($this->data[$this->modelClass]['more_action_id']);
            $dealUserIds = array();
            foreach ($this->data['DealUser'] as $dealuser_id => $is_checked) {
                if ($is_checked['id']) {
                    $dealUserIds[] = $dealuser_id;
                }
            }
            if ($actionid && !empty($dealUserIds)) {
                if ($actionid == ConstMoreAction::Used) {
                    $this->DealUser->updateAll(array(
                        'DealUser.is_used' => 1
                            ), array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Redemption->updateAsRedeemedByAdmin($dealUserIds, $this->Auth->user('id'));
                    $this->Session->setFlash(__l('Checked users status has been changed'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::NotUsed) {
                    $this->DealUser->updateAll(array(
                        'DealUser.is_used' => 0
                            ), array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash(__l('Checked users status has been changed'), 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::ResendEmail) {
                    $this->DealUser->updateAll(array(
                        'DealUser.emailed' => 0
                            ), array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash('Se dejan lo/s cupone/s listos para reenvio de email', 'default', null, 'success');
                } else if ($actionid == ConstMoreAction::Delete) {
                    $this->DealUser->deleteAll(array(
                        'DealUser.id' => $dealUserIds
                    ));
                    $this->Session->setFlash(__l('Deal User deleted'), 'default', null, 'success');
                }
            }
        }
        if (!$this->RequestHandler->isAjax()) {
            $this->redirect(Router::url('/', true) . $r);
        } else {
            $this->redirect($r);
        }
    }

    function admin_delete($id = null) {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->DealUser->del($id)) {
            $this->Session->setFlash(__l('Deal User deleted'), 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }

    function update_status($deal_user_id = null, $field = 'is_used') {
        $action = 1;
        $this->autoRender = false;
        if (!empty($deal_user_id)) {
            $DealUser = $this->DealUser->find('first', array(
                'conditions' => array(
                    'DealUser.id' => $deal_user_id
                ),
                'contain' => array(
                    'Deal'
                ),
                'recursive' => 2
            ));
        }
        $user = $this->DealUser->Deal->User->Company->findByUserId($this->Auth->User('id'));
        if ($DealUser['DealUser'][$field] == 1) {
            if (ConstUserTypes::isNotLikeAdmin($this->Auth->user('user_type_id'))) {
                return;
            }
            $status = 0;
            if (($DealUser['Deal']['company_id'] != $user['Company']['id']) || empty($user['Company']['id'])) {
                $action = 0;
            }
        } else {
            $status = 1;
        }
        if (!empty($action)) {
            $this->DealUser->updateAll(array(
                'DealUser.' . $field => $status
                    ), array(
                'DealUser.id' => $deal_user_id
            ));
        }
        echo __l($status);
    }

    function mark_used($deal_user_id) {
        $result = 0;
        if (!empty($deal_user_id)) {
            switch ($this->Auth->user('user_type_id')) {
                case ConstUserTypes::Company:
                    $result = $this->DealUser->markAsUsedByCompany($deal_user_id);
                    break;

                case ConstUserTypes::User:
                    $result = $this->DealUser->markAsUsedByUser($deal_user_id);
                    break;

                default:
                    $result = $this->DealUser->markAsUsedByUser($deal_user_id);
            }
        }
        echo $result;
    }

    public function send_cupon() {
        $this->RequestHandler->respondAs('text/x-json');
        $this->autoRender = false;
        $result = array(
            'success' => 0,
            'isValid' => 0
        );
        $result['debug']['params'] = $this->params;
        $dealUserId = $this->params['url']['deal_user_id'];
        $result['debug']['deal_user_id'] = $dealUserId;
        $hasSend = null;
        $isValidDealUserId = 1;
        $errorMessage = '';
        if (empty($dealUserId)) {
            $isValidDealUserId = 0;
            $errorMessage = 'deal_user_id no puede ser vacio';
        } else {
            $dealUser = $this->DealUser->find('first', array(
                'fields' => array(
                    'DealUser.id',
                    'DealUser.deal_id'
                ),
                'conditions' => array(
                    'DealUser.id' => $dealUserId
                ),
                'recursive' => - 1
            ));
            $result['debug']['deal_user'] = $dealUser;
            if (!$dealUser) {
                $isValidDealUserId = false;
                $errorMessage = 'deal_user_id no existe.';
            }
        }
        if ($isValidDealUserId) {
            $deal = array(
                'Deal' => array(
                    'id' => $dealUser['DealUser']['deal_id']
                )
            );
            $hasSend = $this->Deal->processTippedStatus($deal, $dealUser['DealUser']['id']);
            $errorMessage = 'No se pudo enviar el cupon';
        } else {
            $hasSend = false;
        }
        $result['success'] = ($hasSend ? 1 : 0);
        $result['error_message'] = $errorMessage;
        echo json_encode($result);
    }

}
