<?php
class EmailTemplatesController extends AppController
{
    var $name = 'EmailTemplates';
    function admin_index()
    {
      // indicamos que por defecto utilice la conexion master a la DB
    AppModel::setDefaultDbConnection('master');
        $this->pageTitle = __l('Email Templates');
        $this->EmailTemplate->recursive = -1;
		$this->paginate = array(
			'limit' => 50,
			'order' =>array(
				'EmailTemplate.name'=>'ASC'
			)
		);
        $this->set('emailTemplates', $this->paginate());
    }

    function viewall()
    {
        $this->layout = 'print';
        $this->pageTitle = __l('Email Templates');
        $this->EmailTemplate->recursive = -1;
		$this->paginate = array(
                                        'limit' => 1200,
                                        'order' =>array(
                                                        'EmailTemplate.name'=>'ASC'
                                                        )
                                        );
        //die(1);
        $this->set('emailTemplates', $this->paginate());
    }
    function see($id = null)
    {
     $this->disableCache();
        $this->pageTitle = __l('View Email Template');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        $this->data = $this->EmailTemplate->read(null, $id);
        if (empty($this->data)) {
                $this->cakeError('error404');

        }
        echo '<div class="js-responses">';
        echo $this->data['EmailTemplate']['email_content'];
        echo '</div>';
        die;
        $this->pageTitle.= ' - ' . $this->data['EmailTemplate']['name'];
        $this->set('email_variables', explode(',', $this->data['EmailTemplate']['email_variables']));
    }

    function admin_edit($id = null)
    {
      // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
     $this->disableCache();
        $this->pageTitle = __l('Edit Email Template');
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            if ($this->EmailTemplate->save($this->data)) {
                $this->Session->setFlash(__l('Email Template has been updated') , 'default', null, 'success');
            } else {
                $this->Session->setFlash(__l('Email Template could not be updated. Please, try again.') , 'default', null, 'error');
            }
            $emailTemplate = $this->EmailTemplate->find('first', array(
                'conditions' => array(
                    'EmailTemplate.id' => $this->data['EmailTemplate']['id']
                ) ,
                'fields' => array(
                    'EmailTemplate.name',
                    'EmailTemplate.email_variables',
					'EmailTemplate.description',
					'EmailTemplate.is_html'
                ) ,
                'recursive' => -1
            ));
            $this->data['EmailTemplate']['name'] = $emailTemplate['EmailTemplate']['name'];
            $this->data['EmailTemplate']['email_variables'] = $emailTemplate['EmailTemplate']['email_variables'];
            $this->data['EmailTemplate']['description'] = $emailTemplate['EmailTemplate']['description'];
			$this->set('emailTemplate',$emailTemplate);
        } else {
            $this->data = $this->EmailTemplate->read(null, $id);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['EmailTemplate']['name'];
        $this->set('email_variables', explode(',', $this->data['EmailTemplate']['email_variables']));
    }
}
?>