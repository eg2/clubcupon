<?php
class TransactionsController extends AppController
{
    var $name = 'Transactions';
    function beforeFilter()
    {
        $this->Security->disabledFields = array(
            'Transaction.from_date',
            'Transaction.user_id',
            'Transaction.to_date'
        );
        // indicamos que por defecto utilice la conexion master a la DB
      AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }
    function index()
    {
        $this->pageTitle = __l('Transactions');
        $conditions['Transaction.user_id'] = $this->Auth->user('id');
        $blocked_conditions['UserCashWithdrawal.user_id'] = $this->Auth->user('id');
        if (isset($this->data['Transaction']['from_date']) and isset($this->data['Transaction']['to_date'])) {
            $from_date = $this->data['Transaction']['from_date']['year'] . '-' . $this->data['Transaction']['from_date']['month'] . '-' . $this->data['Transaction']['from_date']['day'] . ' 00:00:00';
            $to_date = $this->data['Transaction']['to_date']['year'] . '-' . $this->data['Transaction']['to_date']['month'] . '-' . $this->data['Transaction']['to_date']['day'] . ' 23:59:59';
        }
        if (!empty($this->data)) {
             if($from_date < $to_date){
                    $blocked_conditions['UserCashWithdrawal.created >='] = $conditions['Transaction.created >='] = $from_date;
                    $blocked_conditions['UserCashWithdrawal.created <='] = $conditions['Transaction.created <='] = $to_date;
             }else{
                    $this->Transaction->validationErrors['to_date'] = __l('"From date" should be greater than "To date".');
                    $this->Session->setFlash(__l('From date should greater than To date. Please, try again.') , 'default', null, 'error');
            }
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'day') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <= '] = 0;
            $blocked_conditions['TO_DAYS(NOW()) - TO_DAYS(UserCashWithdrawal.created) <= '] = 0;
            $this->pageTitle.= __l(' - Amount Earned today');
            $this->data['Transaction']['from_date'] = array(
                'year' => date('Y', strtotime('today')) ,
                'month' => date('m', strtotime('today')) ,
                'day' => date('d', strtotime('today'))
            );
            $this->data['Transaction']['to_date'] = array(
                'year' => date('Y', strtotime('today')) ,
                'month' => date('m', strtotime('today')) ,
                'day' => date('d', strtotime('today'))
            );
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'week') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <= '] = 7;
            $blocked_conditions['TO_DAYS(NOW()) - TO_DAYS(UserCashWithdrawal.created) <= '] = 7;
            $this->pageTitle.= __l(' - Amount Earned in this week');
            $this->data['Transaction']['from_date'] = array(
                'year' => date('Y', strtotime('last week')) ,
                'month' => date('m', strtotime('last week')) ,
                'day' => date('d', strtotime('last week'))
            );
            $this->data['Transaction']['to_date'] = array(
                'year' => date('Y', strtotime('this week')) ,
                'month' => date('m', strtotime('this week')) ,
                'day' => date('d', strtotime('this week'))
            );
        }
        if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'month') {
            $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <= '] = 30;
            $blocked_conditions['TO_DAYS(NOW()) - TO_DAYS(UserCashWithdrawal.created) <= '] = 30;
            $this->pageTitle.= __l(' - Amount Earned in this month');
            $this->data['Transaction']['from_date'] = array(
                'year' => date('Y', (strtotime('this month', strtotime(date('m/01/y'))))) ,
                'month' => date('m', (strtotime('this month', strtotime(date('m/01/y'))))) ,
                'day' => date('d', (strtotime('this month', strtotime(date('m/01/y')))))
            );
            $this->data['Transaction']['to_date'] = array(
                'year' => date('Y', (strtotime('this month', strtotime(date('m/01/y'))))) ,
                'month' => date('m', (strtotime('this month', strtotime(date('m/01/y'))))) ,
                'day' => date('t', (strtotime('this month', strtotime(date('m/01/y')))))
            );
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array(
                'Transaction.id' => 'desc'
            ) ,
            'recursive' => 2
        );
        $transactions = $this->paginate();
        $this->set('transactions', $transactions);
        // To get commission percentage
        $credit = $this->Transaction->find('first', array(
            'conditions' => array(
                $conditions,
                'TransactionType.is_credit' => 1
            ) ,
            'fields' => array(
                'SUM(Transaction.amount) as total_amount'
            ) ,
            'group' => array(
                'Transaction.user_id'
            ) ,
            'recursive' => 0
        ));
        $this->set('total_credit_amount', !empty($credit[0]['total_amount']) ? $credit[0]['total_amount'] : 0);
        $debit = $this->Transaction->find('first', array(
            'conditions' => array(
                $conditions,
                'TransactionType.is_credit' => 0
            ) ,
            'fields' => array(
                'SUM(Transaction.amount) as total_amount'
            ) ,
            'group' => array(
                'Transaction.user_id'
            ) ,
            'recursive' => 0
        ));
        $this->set('total_debit_amount', !empty($debit[0]['total_amount']) ? $debit[0]['total_amount'] : 0);
        $blocked_amount = $this->Transaction->User->UserCashWithdrawal->find('first', array(
            'conditions' => $blocked_conditions,
            'fields' => array(
                'SUM(UserCashWithdrawal.amount) as total_amount'
            ) ,
            'group' => array(
                'UserCashWithdrawal.user_id'
            ) ,
            'recursive' => 0
        ));
        $this->set('blocked_amount', !empty($blocked_amount[0]['total_amount']) ? $blocked_amount[0]['total_amount'] : 0);
        if (empty($this->data)) {
            if (isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'week') {
                $this->data['Transaction']['from_date'] = array(
                    'year' => date('Y', strtotime("-7 days")) ,
                    'month' => date('m', strtotime("-7 days")) ,
                    'day' => date('d', strtotime("-7 days"))
                );
            } else if(isset($this->params['named']['stat']) && $this->params['named']['stat'] == 'month') {
                $this->data['Transaction']['from_date'] = array(
                    'year' => date('Y', strtotime('-30 days')) ,
                    'month' => date('m', strtotime('-30 days')) ,
                    'day' => date('d', strtotime('-30 days'))
                );
            } else {
                $this->data['Transaction']['from_date'] = array(
                    'year' => date('Y', strtotime('-90 days')) ,
                    'month' => date('m', strtotime('-90 days')) ,
                    'day' => date('d', strtotime('-90 days'))
                );
			}
			$this->data['Transaction']['to_date'] = array(
				'year' => date('Y', strtotime('today')) ,
				'month' => date('m', strtotime('today')) ,
				'day' => date('d', strtotime('today'))
			);
        }
    }
    
    
    
    function admin_index()
    {
        
        $this->pageTitle = 'Transacciones';
        
        $conditions = array();
        
        if (!empty($this->data['Transaction']['user_id']))
        {
            $this->params['named']['user_id']   = $this->data['Transaction']['user_id'];
        }
        
        if (!empty($this->data['Transaction']['from_date']['year']) && !empty($this->data['Transaction']['from_date']['month']) && !empty($this->data['Transaction']['from_date']['day']))
        {
            $this->params['named']['from_date'] = $this->data['Transaction']['from_date']['year'] . '-' . $this->data['Transaction']['from_date']['month'] . '-' . $this->data['Transaction']['from_date']['day'] . ' 00:00:00';
        }
        
        if (!empty($this->data['Transaction']['to_date']['year']) && !empty($this->data['Transaction']['to_date']['month']) && !empty($this->data['Transaction']['to_date']['day']))
        {
            $this->params['named']['to_date']   = $this->data['Transaction']['to_date']['year'] . '-' . $this->data['Transaction']['to_date']['month'] . '-' . $this->data['Transaction']['to_date']['day'] . ' 23:59:59';
        }
        
	    $param_string = "";
        
        $param_string.= !empty($this->params['named']['user_id']) ? '/user_id:' . $this->params['named']['user_id'] : $param_string;
        
        if (!empty($this->params['named']['user_id']))
        {
            $conditions['Transaction.user_id']      = $this->params['named']['user_id'];
            $this->data['Transaction']['user_id']   = $this->params['named']['user_id'];
        }
        
        
        if (!empty($this->params['named']['type']))
        {
            $conditions['Transaction.transaction_type_id'] = $this->params['named']['type'];
			$transaction_type = $this->Transaction->TransactionType->find('first', array(
					'conditions' => array(
						'TransactionType.id' => $this->params['named']['type']
					),
					'fields' => array(
						'TransactionType.name'
					),
					'recursive' => -1
				));
			$this->pageTitle .= ' - '.$transaction_type['TransactionType']['name'];
        }
        
        if (!empty($this->params['named']['stat']))
        {
            if ($this->params['named']['stat'] == 'day')
            {
                $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <='] = 0;
                $this->pageTitle .= __l(' - Today');
                $this->set('transaction_filter', __l('- Today'));
                $days = 0;
            }
            else if ($this->params['named']['stat'] == 'week')
            {
                $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <='] = 7;
                $this->pageTitle .= __l(' - This Week');
                $this->set('transaction_filter', __l('- This Week'));
                $days = 7;
            }
            else if ($this->params['named']['stat'] == 'month')
            {
                $conditions['TO_DAYS(NOW()) - TO_DAYS(Transaction.created) <='] = 30;
                $this->pageTitle .= __l(' - This Month');
                $this->set('transaction_filter', __l('- This Month'));
                $days = 30;
            }
            else
            {
                $this->pageTitle .= __l(' - Total');
                $this->set('transaction_filter', __l('- Total'));
            }
        }
        
        if (empty($this->data))
        {
            if (isset($days))
            {
                $this->data['Transaction']['from_date'] = array(
                    'year' => date('Y',     strtotime("-$days days")) ,
                    'month' => date('m',    strtotime("-$days days")) ,
                    'day' => date('d',      strtotime("-$days days"))
                );
            }
            else
            {
                $this->data['Transaction']['from_date'] = array(
                    'year' => date('Y',     strtotime('-90 days')) ,
                    'month' => date('m',    strtotime('-90 days')) ,
                    'day' => date('d',      strtotime('-90 days'))
                );
            }
            
            $this->data['Transaction']['to_date'] = array(
                'year' => date('Y',         strtotime('today')) ,
                'month' => date('m',        strtotime('today')) ,
                'day' => date('d',          strtotime('today'))
            );
        }
        
        if (!empty($this->params['named']['from_date']) && !empty($this->params['named']['to_date']))
        {
            if($this->params['named']['from_date'] < $this->params['named']['to_date'])
            {
                $conditions['Transaction.created >='] = $this->params['named']['from_date'];
                $conditions['Transaction.created <='] = $this->params['named']['to_date'];
            }
            else
            {
                $this->Session->setFlash('La fecha de finalizacion debe ser posterior a la de inicio.' , 'default', null, 'error');
            }
        }
        
        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array(
                'Transaction.id' => 'desc'
            ) ,
            'recursive' => 1
        );
        
        $users = $this->Transaction->User->find('list', array(
            'conditions' => array(
                'User.user_type_id !=' => ConstUserTypes::Admin,
                'User.user_type_id !=' => ConstUserTypes::SuperAdmin,
                'User.username !=' => ''
            ) ,
            'order' => array(
                'User.username' => 'asc'
            ),
            'recursive' => -1
        ));
        
        $this->set('users',         $users);
        $this->set('transactions',  $this->paginate());
		$this->set('param_string',  $param_string);
		$this->set('pageTitle',     $this->pageTitle);
        
    }
    
    function admin_delete($id = null)
    {
        if (is_null($id)) {
            $this->cakeError('error404');
        }
        if ($this->Transaction->del($id)) {
            $this->Session->setFlash(__l('Transaction deleted') , 'default', null, 'success');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->cakeError('error404');
        }
    }
	function admin_export() {
		Configure::write('debug', 0);
		$conditions = array();
        if (!empty($this->params['named']['user_id'])) {
            $conditions['Transaction.user_id'] = $this->params['named']['user_id'];
        }
		$transactions = $this->Transaction->find('all', array(
			'conditions' => $conditions ,
			'order' => array(
				'Transaction.id' => 'desc'
			) ,
			'recursive' => 2
		));
		if($transactions) {
			foreach($transactions as $transaction) {
				 if($transaction['TransactionType']['is_credit']) {
				 	$credit = $transaction['Transaction']['amount'];
					$debit = '--';
				 }
				 else {
				 	$credit = '--';
					$debit = $transaction['Transaction']['amount'];
				 }
				$data[]['Transaction'] = array(
					__l('Date') => $transaction['Transaction']['created'] ,
					__l('User') => $transaction['User']['username'] ,
					__l('Description') => $transaction['TransactionType']['name'] ,
					__l('Credit').' ('.Configure::read('site.currency').')' => $credit ,
					__l('Debit').' ('.Configure::read('site.currency').')' => $debit ,
					__l('Gateway Fees').' ('.Configure::read('site.currency').')' => $transaction['Transaction']['gateway_fees'] ,
				);
			}

		}
		$this->set('data', $data);
	}
}
?>
