<?php

class WidgetsController extends AppCachedController {

  var $name = 'Widgets';
  var $components = array(
  );
  var $uses = array(
      'Deal',
  );
  var $widgetSizes = array(
      'big' => array('w' => '420', 'h' => '420'),
      'medium' => array('w' => '320', 'h' => '320'),
      'small' => array('w' => '120', 'h' => '120')
  );
  var $helpers = array('Cache', 'Text');
  var $cacheAction = array(
    'widget/' => array('duration' => 21600),
  );
  
  function beforeFilter() {
    $this->Security->validatePost = false;
    $this->Security->disabledFields = array(
    );
    parent::beforeFilter();
  }

  function _getActiveDealForCity($city,$isSideDeal = 0) {

    $keyCache = '_widget_' . $city['City']['id'] . '_';
    if($isSideDeal)
      $keyCache .= 'side';
    Cache::set(array('duration' => '+5 minutes'));
    $deals = Cache::read($keyCache);
    if($deals !== false) {
      return $deals;
    }
    $order = array(
        'Deal.id' => 'desc'
    );

    $conditions['Deal.deal_status_id'] = array(
        ConstDealStatus::Open,
        ConstDealStatus::Tipped,
    );

    $conditions ['Deal.city_id'] = $city['City']['id'];
    $conditions ['Deal.is_side_deal'] = $isSideDeal;
    $order = array(
        'Deal.end_date' => 'DESC'
    );
    $finder = array(
        'conditions' => array(
            $conditions,
        ),
        'contain' => array(
            'Company' => array(
                'fields' => array(
                    'Company.name',
                    'Company.slug',
                    'Company.id',
                    'Company.user_id',
                    'Company.url',
                    'Company.zip',
                    'Company.address1',
                    'Company.address2',
                    'Company.city_id',
                    'Company.latitude',
                    'Company.longitude',
                    'Company.is_company_profile_enabled',
                    'Company.is_online_account'
                ),
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug',
                    )
                ),
                'State' => array(
                    'fields' => array(
                        'State.id',
                        'State.name'
                    )
                ),
                'Country' => array(
                    'fields' => array(
                        'Country.id',
                        'Country.name',
                        'Country.slug',
                    )
                )
            ),
            'Attachment' => array(
                'fields' => array(
                    'Attachment.id',
                    'Attachment.dir',
                    'Attachment.filename',
                    'Attachment.width',
                    'Attachment.height'
                )
            ),
            'City' => array(
                'fields' => array(
                    'City.id',
                    'City.name',
                    'City.slug',
                    'City.latitude',
                    'City.longitude',
                )
            ),
            'DealStatus' => array(
                'fields' => array(
                    'DealStatus.name',
                )
            ),
        ),
        'order' => $order,
        'recursive' => 3,
        'limit' => 1
    );
    $deals = $this->Deal->find('first',$finder);
    if($deals === false) {
      // no guardamos false, porque dps no podemos reconocer
      // el hit de cache.
      $deals = array();
    }
    Cache::set(array('duration' => '+5 minutes'));
    Cache::write($keyCache,$deals);
    return $deals;
  }

  function _wizzard($city_slug = null) {
    $cities = $this->Deal->City->find('list', array(
                'conditions' => array(
                  'is_approved' => 1
                ),
                'fields' => array(
                    'City.id',
                    'City.name',
                ),
                'recursive' => -1
            ));
    $this->set('cities',$cities);
    $this->set('widgetSizes',$this->widgetSizes);
  }

  function _defaultWidgetSizes($size_name = false) {
    if(isset ($this->widgetSizes[$size_name])) {
      return $this->widgetSizes[$size_name];
    }
    return $this->widgetSizes['big'];
  }

  function _widget() {
    $this->cacheAction = true;
    $city_id = empty($_GET['city_id']) ? ConstCities::CiudadDeBuenosAires : $_GET['city_id'];
    $sizeName = !empty($_GET['size']) && in_array($_GET['size'],array_keys($this->widgetSizes)) ? $_GET['size'] : 'big';
    $size = $this->_defaultWidgetSizes($sizeName);
    
    $city = $this->_findCityById($city_id);
    if(!$city) {
      die('invalid_city_id');
    }

    $conditions = array();
    $deal  = $this->_getActiveDealForCity($city);
    if(!$deal) {
      $deal  = $this->_getActiveDealForCity($city,true);
    }
    $this->set('deal',$deal);
    $this->set('city',$city);
    $this->set('size',$size);
    $this->autoRender = false;
    $this->layout = 'widget';
    $this->render('widget_' . $sizeName);
  }

  function clarin() {
    $city_slug = $this->params['named']['city'];
    $this->cacheAction = true;
    $city = $this->_findCityBySlug($city_slug);
    if(!$city) {
      die('invalid_city_slug');
    }
    $conditions = array();
    $deal  = $this->_getActiveDealForCity($city);
    if(!$deal) {
      $deal  = $this->_getActiveDealForCity($city,true);
    }
    $this->set('deal',$deal);
    $this->set('city',$city);
    $this->autoRender = false;
    $this->layout = 'widget-clarin';
    $this->render('widget_clarin');
  }
  
  //Segundo widget para clarin, identico, solo difiere el layout
  function clarin_2() {
    $city_slug = $this->params['named']['city'];
    $this->cacheAction = true;
    $city = $this->_findCityBySlug($city_slug);
    if(!$city) {
      die('invalid_city_slug');
    }
    $conditions = array();
    $deal  = $this->_getActiveDealForCity($city);
    if(!$deal) {
      $deal  = $this->_getActiveDealForCity($city,true);
    }
    $this->set('deal',$deal);
    $this->set('city',$city);
    $this->autoRender = false;
    $this->layout = 'widget-clarin_2';
    $this->render('widget_clarin_2');
  }
  

  function _findCityById($id) {
    $cacheKey = 'widget_city_by_id_' . $id;
    $city = Cache::read($cacheKey);
    if($city !== false) {
      return $city;
    }
    
    $city = $this->Deal->City->find('first', array(
                'conditions' => array(
                  'is_approved' => 1,
                  'id' => $id
                ),
                'fields' => array(
                    'City.id',
                    'City.name',
                    'City.slug'
                ),
                'recursive' => -1
            ));
    Cache::write($cacheKey,$city);
    return $city;
  }

  function _findCityBySlug($slug) {
    $cacheKey = 'widget_city_by_slug_' . $slug;
    $city = Cache::read($cacheKey);
    if($city !== false) {
      return $city;
    }

    $city = $this->Deal->City->find('first', array(
                'conditions' => array(
                  'is_approved' => 1,
                  'slug' => $slug
                ),
                'fields' => array(
                    'City.id',
                    'City.name',
                    'City.slug'
                ),
                'recursive' => -1
            ));
    Cache::write($cacheKey,$city);
    return $city;
  }

  function img() {
    $city_slug = $this->params['named']['city'];
    $this->cacheAction = true;
    $city = $this->_findCityBySlug($city_slug);
    if(!$city) {
      die('invalid_city_slug');
    }
    $conditions = array();
    $deal  = $this->_getActiveDealForCity($city);
    if(!$deal) {
      $deal  = $this->_getActiveDealForCity($city,true);
    }
    $image_hash = '/img/small_big_thumb/Deal/' . $deal['Attachment']['id'] . '.' . md5(Configure::read('Security.salt') . 'Deal' . $deal['Attachment']['id'] . 'jpg' . 'small_big_thumb' . Configure::read('site.name')) . '.' . 'jpg';
    header('Location: ' . $image_hash);
    die;
  }



}