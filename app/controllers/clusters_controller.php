<?php

App::import('Sanitize');


class ClustersController extends AppCachedController {
//
  var $name = 'Clusters';
  var $components = array(
  );
  var $helpers = array(
  );
  var $uses = array(
      'Deal',
      'ClusterSource',
      'ClusterExample',
      'Cluster',
  );

  function beforeFilter() {
    $this->Security->validatePost = false;
    $this->Security->disabledFields = array(
    );
    $this->clusterDefinition = Configure::read('cluster_example_definition');
    parent::beforeFilter();
  }

  function _retrieveSources() {
    foreach($this->clusterDefinition as $field => &$definition) {
      if(isset($definition['source']) && $definition['source'] == 'entity') {
        $entityName = $definition['sourceValue'];
        $entity =  &ClassRegistry::init ($entityName);
        if(!isset($definition['sourceConditions'])) {
          $definition['sourceConditions'] = array();
        }
        $definition['sourceValue'] = $entity->find('list',$definition['sourceConditions']);
      }
    }
  }

  function _fieldsOfCluster() {
    $fields = array();
    foreach($this->clusterDefinition as $field => &$definition) {
      if(!in_array($field, array('id','cluster_id'))) {
        $fields[] = $field;
      }
    }
    return $fields;
  }


  function admin_index() {
    $this->pageTitle = __l('Clusters');
    $clusters = $this->Cluster->find('all');
    $this->set('clusters', $clusters);
    $this->set('pageTitle', $this->pageTitle);
  }


  function admin_add() {
    $this->set('cluster',array());
    $this->_retrieveSources();
    $this->set('clusterDefinition',$this->clusterDefinition);
  }

  function admin_edit($id = null) {
    if (is_null($id)) {
      return $this->cakeError('error404');
    }
    $this->_retrieveSources();
    $this->set('clusterDefinition',$this->clusterDefinition);
    $this->set('cluster',array());


    $cluster = $this->Cluster->find('first',
            array('conditions' => array('id' => $id),
                  'recursive' => 2,
                ));
    if (!$cluster) {
      return $this->cakeError('error404');
    }
    $this->set('cluster',$cluster);
    $this->render('admin_add');
  }


  function admin_save() {
    $ret = array();
    if(empty($_POST['rules'])) {
      $this->_dieWithAjaxError('Missing param: rules');
    }
    if(empty($_POST['name'])) {
      $this->_dieWithAjaxError('Missing or empty param: name');
    }
    $rules = json_decode($_POST['rules'],true);
    if(empty($rules)) {
      $this->_dieWithAjaxError('Invalid value for rules param');
    }
    $idxOr = 0;
    $orClauses = array();
    $andClauses = array();




    if(!empty($_POST['cluster_id'])) {
      $clusterId = $_POST['cluster_id'];
      $cluster = $this->Cluster->find('first',array('conditions' => array('id' => $clusterId)));
      $cluster['Cluster']['name'] = $_POST['name'];
      $this->Cluster->save($cluster);
    } else {
      $cluster = array('Cluster' => array('name' => $_POST['name']));
      $this->Cluster->create();
      $this->Cluster->save($cluster);
      $clusterId = $this->Cluster->getLastInsertId();
    }

    $this->ClusterExample->deleteAll(array('ClusterExample.cluster_id' => $clusterId));

    foreach($rules as $orClause) {
      $finder = array_merge($this->_fieldsOfCluster(), $orClause);
      $data = array('ClusterExample' => $orClause);
      $data['ClusterExample']['cluster_id'] = $clusterId;
      $this->ClusterExample->create();
      $this->ClusterExample->save($data);
      $clusterExampleId = $this->ClusterExample->getLastInsertId();
    }


    $ret['cluster_id'] = $clusterId;
    $ret['url_redirect'] = Router::url(array('action' => 'index'));
    echo json_encode($ret);
    die;
  }


  function admin_preview() {
    $ret = array();
    if(empty($_POST['rules'])) {
      $this->_dieWithAjaxError('Missing param: rules');
    }
    $rules = json_decode($_POST['rules'],true);
    if(empty($rules)) {
      $this->_dieWithAjaxError('Invalid value for rules param');
    }

    $selectPart = 'SELECT * ';
    $selectPartCount = 'SELECT count(*) as quantity ';
    $rawQuery = ' from `cluster_sources`  WHERE ';

    $orClauses = array();

    foreach($rules as $rule) {
      $andClauses = array();
      foreach($rule as $field => $value) {
        $andClauses[] = '(  `'.$field.'` = \'' . Sanitize::escape($value) . '\' )';
      }
      $orClauses[] = "(". implode(" AND ",$andClauses) . ")";
    }

    $clausesStr = implode(" OR " , $orClauses);

    if(empty($clausesStr)) {
      $ret['result'] = array();
      $ret['count'] = 0;
      $ret['query'] = 'Empty data.';
    } else {
      $rawQuery .= $clausesStr;
      $rawQuery .= " limit 15; ";
      $ret['query'] = $selectPart . $rawQuery;
      $partialResult = $this->ClusterSource->query($selectPart . $rawQuery);
      $result = array();
      foreach($partialResult as $row) {
        $result[] = $row['cluster_sources'];
      }
      $ret['result'] = $result;
      $countResult = $this->ClusterSource->query($selectPartCount . $rawQuery);
      $countResult = $countResult[0][0]['quantity'];
      $ret['count'] = $countResult;
    }
    echo json_encode($ret);
    die;
  }

  function _dieWithAjaxError($msg) {
    $ret = array('error' => $msg);
    echo json_encode($ret);
    die;
  }


}
