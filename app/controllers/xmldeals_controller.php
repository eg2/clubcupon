<?php

class XmldealsController extends AppCachedController {

    var $name = 'Xmldeals';
    var $uses = array(
        'Deal'
    );

    function beforeFilter() {
        $this->Security->validatePost = false;
        $this->Security->disabledFields = array();
        return parent::beforeFilter();
    }

    function _findGoogleAdWordsCities() {
        return $city = $this->Deal->City->find('all', array(
            'conditions' => array(
                'is_approved' => 1,
                'is_corporate' => 0,
                'is_business_unit' => 0,
                'is_hidden' => 0,
            ),
            'recursive' => - 1
        ));
    }

    function csv() {
        $cities = $this->_findGoogleAdWordsCities();
        if (!empty($cities)) {
            foreach ($cities as & $city) {
                $city['deal_collection'] = $this->_getDealsForCity($city, $options);
                $city['deal_collection'] = $this->_formattedDeals($city['deal_collection']);
            }
        }

        $this->set('cities', $cities);
        $this->layout = false;
    }

    function dealandia() {
        $cities = $this->_findApprovedCities();
        if (!empty($cities)) {
            foreach ($cities as & $city) {
                $city['deal_collection'] = $this->_getDealsForCity($city);
            }
        }
        $this->set('cities', $cities);
        $this->layout = false;
    }

    function rss($city_slug = null, $options = null) {
        $cities = array();
        if (!empty($city_slug)) {
            $cities[] = $this->_findCityBySlugName($city_slug);
        } else {
            $cities = $this->_findApprovedCities();
        }
        if (!empty($cities)) {
            foreach ($cities as & $city) {
                $city['deal_collection'] = $this->_getDealsForCity($city, $options);
                $city['deal_collection'] = $this->_formattedDeals($city['deal_collection']);
            }
        }
        $this->set('cities', $cities);
        $this->layout = false;
    }

    function _formattedDeals($deals) {
        $counter = 0;
        foreach ($deals as $deal) {
            $deals[$counter]['Deal']['name'] = htmlspecialchars($deal['Deal']['name']);
            $deals[$counter]['Deal']['descriptive_text'] = htmlspecialchars($deal['Deal']['descriptive_text']);
            $counter++;
        }
        return $deals;
    }

    function descuentocity($city_slug = null, $options = null) {
        $cities = array();
        if (!empty($city_slug)) {
            $cities[] = $this->_findCityBySlugName($city_slug);
        } else {
            $cities = $this->_findApprovedCities();
        }
        if (!empty($cities)) {
            $dealCategoriesList = $this->Deal->DealCategory->find('list', array(
                'recursive' => - 1
            ));
            foreach ($cities as & $city) {
                $dealsForDescuentoCity = $this->_getDealsForCity($city, $options);
                $city['deal_collection'] = $this->_formattedDealsForDescuentoCity($dealsForDescuentoCity, $dealCategoriesList);
            }
        }
        $this->set('cities', $cities);
        $this->layout = false;
    }

    function sitemap() {
        $this->set('_this', $this);
    }

    function _getDealsForCity($city, $options = null) {
        if (empty($options) || !in_array($options, array(
                    'recent',
                    'groovinads'
                ))) {
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Open,
                ConstDealStatus::Tipped
            );
            $conditions[] = 'Deal.parent_deal_id = Deal.id';
            $order = array(
                'Deal.is_side_deal' => 'asc',
                'Deal.priority' => 'desc',
                'Deal.end_date' => 'desc',
                'Deal.id' => 'desc'
            );
        } else if ($options == 'recent') {
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Closed,
                ConstDealStatus::Canceled,
                ConstDealStatus::PaidToCompany
            );
            $conditions['Deal.is_side_deal'] = false;
            $order = array(
                'Deal.end_date' => 'desc'
            );
        } else if ($options == 'groovinads') {
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Open,
                ConstDealStatus::Tipped
            );
            $conditions[] = 'Deal.parent_deal_id = Deal.id';
            $conditions[] = 'Deal.discounted_price >= 200';
            $order = array(
                'Deal.is_side_deal' => 'asc',
                'Deal.priority' => 'desc',
                'Deal.end_date' => 'desc',
                'Deal.id' => 'desc'
            );
        }
        $conditions['Deal.city_id'] = $city['City']['id'];
        $finder = array(
            'conditions' => $conditions,
            'contain' => array(
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                ),
                'Attachment',
                'City',
                'DealCategory',
                'DealStatus',
                'DealFlatCategory'
            ),
            'order' => $order,
            'limit' => 1000
        );
        return $this->Deal->find('all', $finder);
    }

    function _findCityBySlugName($city_slug) {
        return $city = $this->Deal->City->find('first', array(
            'conditions' => array(
                'is_approved' => 1,
                'slug' => $city_slug
            ),
            'recursive' => - 1
        ));
    }

    function _findApprovedCities() {
        return $city = $this->Deal->City->find('all', array(
            'conditions' => array(
                'is_approved' => 1
            ),
            'recursive' => - 1
        ));
    }

    function _formattedDealsForDescuentoCity($dealsArray, $dealCategoriesList) {
        foreach ($dealsArray as $deal) {
            $singleFormattedDeal['dealTitle'] = htmlspecialchars($deal['Deal']['name']);
            $singleFormattedDeal['brand'] = !empty($deal['Deal']['custom_company_name']) ? $deal['Deal']['custom_company_name'] : $deal['Company']['name'];
            $singleFormattedDeal['brand_address'] = !empty($deal['Deal']['custom_company_address1']) ? $deal['Deal']['custom_company_address1'] : $deal['Company']['address1'];
            $singleFormattedDeal['companyLatitude'] = $deal['Company']['latitude'];
            $singleFormattedDeal['companyLongitude'] = $deal['Company']['longitude'];
            $singleFormattedDeal['companyAddress'] = $deal['Company']['address1'];
            $singleFormattedDeal['companyName'] = $deal['Company']['name'];
            $singleFormattedDeal['dealCategory'] = $dealCategoriesList[$deal['Deal']['deal_category_id']];
            $singleFormattedDeal['dealCity'] = $deal['City']['name'];
            $singleFormattedDeal['dealAttachment'] = $deal['Attachment'];
            $singleFormattedDeal['dealDiscountedPrice'] = Configure::read('site.currency') . $deal['Deal']['discounted_price'];
            $singleFormattedDeal['dealOriginalPrice'] = Configure::read('site.currency') . $deal['Deal']['original_price'];
            $singleFormattedDeal['dealLink'] = STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $deal['City']['slug'] . '/deal/' . $deal['Deal']['slug'];
            $singleFormattedDeal['dealDescription'] = '<p>
                                                                <a href = " ' . Router::url(array(
                        'controller' => 'deals',
                        'action' => 'view',
                        urlencode($deal['Deal']['slug'])
                            ), true) . '"  title = "' . $deal['Deal']['name'] . '">
                                                                    <img src = "' . $singleFormattedDeal['dealImgUrl'] . '"  alt = "' . $deal['Deal']['name'] . '" />
                                                                </a>
                                                           </p>' . $deal['Deal']['description'] . '<br />
                                                            Pag&aacute; ' . $deal['Deal']['discounted_price'];
            if ($deal['Deal']['only_price'] == 0) {
                $singleFormattedDeal['dealDescription '].= 'en vez de ' . $deal['Deal']['original_price'];
            }
            $singleFormattedDeal['dealDescription '].= '<div>' . $deal['Deal']['coupon_condition'] . '</div>
                                                                <div>' . $deal['Deal']['coupon_highlights'] . '</div>
                                                                <div><h3> ' . $deal['Company']['name'] . '</h3>
                                                                <p> ' . htmlentities($deal['Company']['address1'] . ' ' . $deal['Company']['address2'], ENT_QUOTES, 'utf-8') . '</p></div>';
            $singleFormattedDeal['dealStartDate'] = $deal['Deal']['start_date'];
            $singleFormattedDeal['dealEndDate'] = $deal['Deal']['end_date'];
            $formattedDealsArray[] = $singleFormattedDeal;
        }
        return $formattedDealsArray;
    }

}
