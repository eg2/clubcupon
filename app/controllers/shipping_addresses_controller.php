<?php
App::import('Model', 'shipping.ShippingAddress');
class ShippingAddressesController extends AppController {
	
	var $name = 'ShippingAddresses';

  var $components = array(
      'shipping.ShippingAddressService',
  );
	var $uses = array(
			'shipping.ShippingAddress',
			'api.ApiCountry',
			'api.ApiState',
			/*'City',*/
			'api.ApiCity',
			'api.ApiCompany',
			'api.ApiNeighbourhood',
			'shipping.ShippingAddressDeal',
      'shipping.ShippingAddressType'
			
	);
	function beforeFilter()
	{
		// indicamos que por defecto utilice la conexion master a la DB
		$this->layout = 'admin_bootstrap';
		AppModel::setDefaultDbConnection('master');
		parent::beforeFilter();
	}
	function admin_index($company_id) {
		
		if(!isset($company_id)){
			$this->Session->setFlash(__l('Debe elegir una Empresa para listar sus Puntos de Retiro.') , 'default', null, 'error');
			$this->redirect(array("controller"=>"companies","action" => "admin_index"), true);
		}
		
		
		
		$this->layout = 'admin_bootstrap';
		$this->pageTitle = 'Puntos de Retiro';
	
		/* obtengo la compañia de la que quiero listar los Puntos de Retiro*/
		$currentCompany = $this->ApiCompany->findById($company_id);
		
		$this->ShippingAddress->recursive = 1;
		$this->paginate = array(
				'conditions' => array(
						'ShippingAddress.deleted' => false,
						'ShippingAddress.company_id'=>$company_id,
				),
				'contain' => array(
						'City',
						'Neighbourhood',
				),
				'order' =>  $order = array(
                		'ShippingAddress.id' => 'DESC'
            			),
		);
		
		
		
		$this->set('shippingAddresses', $this->paginate());
		$this->set('currentCompany', $currentCompany);
		
	}
  private function isValidShippingAddressType($shippingAddress) {
    $hasCMDShippingAddress = 
      $this->ShippingAddressService->isShippingAddressManagedLogistic(
              $shippingAddress);
    if ($hasCMDShippingAddress) {
      $hasWarehouseId = $this->ShippingAddress->hasStorehouseId($shippingAddress);
      $isValid = ($hasCMDShippingAddress && $hasWarehouseId);
    } else {
      $isValid = true;
    }    
    return $isValid;
  }
	function admin_add($company_id)
	{
		$this->layout = 'admin_bootstrap';
		$this->pageTitle = __l('Agregar Punto de Retiro');
		$action='add';		
		
		if (!empty($this->data)) {
      if (!$this->isValidShippingAddressType($this->data)) {
        $this->Session->setFlash(__l('Punto de Retiro no fue a&ntilde;adido, Debe llenar un Id de Almacen') , 'default', null, 'error');
      } else {
        $company_id = $this->data['ShippingAddress']['company_id'];
        $this->ShippingAddress->create();
        $this->data = $this->ShippingAddressService->prepareData($this->data);
        if ($this->ShippingAddress->save($this->data)) {
          $this->Session->setFlash(__l('Punto de Retiro fue a&ntilde;adido') , 'default', null, 'success');

          $this->redirect(array("action" => "admin_index",
                    $this->data['ShippingAddress']['company_id']), true);

        } else {
          $this->Session->setFlash(__l('Punto de Retiro no fue a&ntilde;adido. Intentelo de nuevo.') , 'default', null, 'error');
        }
      }
		}
	
		$currentCompany = $this->ApiCompany->findById($company_id);
		$shippingAddressTypes = $this->ShippingAddressType->findAllForForm();
		$this->_loadCombosGeo($this->data,$countries,$states,$cities,$neighbourhoods);
		
		$this->set(compact('companies', 'countries', 'states', 'cities','neighbourhoods','currentCompany','action', 'shippingAddressTypes'));
	
	}
	
	function admin_delete($shipping_address_id,$company_id){
		$this->ShippingAddress->del($shipping_address_id);
		
		/*elimino las relaciones de PuntosdDeRetiro/Ofertas que tuvieran el PuntoDeRetiro eliminado*/
		$this->ShippingAddressDeal = new ShippingAddressDeal();
		$this->ShippingAddressDeal->delManyByShippingAddress($shipping_address_id);
		
		
		$shipping_address=$this->ShippingAddress->findById($shipping_address_id);
		$this->Session->setFlash(__l('Punto de Retiro eliminado.'), 'default', null, 'success');
		$this->redirect(array("action" => "index",
				$company_id), true);
	}
	
	function admin_update($shipping_address_id,$company_id){
		
		$this->layout = 'admin_bootstrap';
		$this->pageTitle = __l ( 'Actualizar Punto de Retiro' );
		$action='update';
		
		if (!empty ( $this->data )) {
      if (!$this->isValidShippingAddressType($this->data)) {
           $this->Session->setFlash(__l('Punto de Retiro no fue a&ntilde;adido, Debe llenar un Id de Almacen') , 'default', null, 'error');
      } else {
          $company_id = $this->data ['ShippingAddress'] ['company_id'];
          $this->data = $this->ShippingAddressService->prepareData($this->data);         
          $this->ShippingAddress->set ( $this->data );
          $this->ShippingAddress->set ( 'modified_by', $this->Auth->user ( 'id' ) );			
          if ($this->ShippingAddress->save ()) {
            $this->Session->setFlash ( 'Punto de Retiro actualizado', 'default', null, 'success' );
            $this->redirect ( array (
                'action' => 'index',
                $company_id 
            ) );
          } else {				
            $this->Session->setFlash ( 'No se pudo Actualizar. Verifique los datos e intentelo de nuevo', 'default', null, 'error' );
          }
      }
		} else {
			$this->data = $this->ShippingAddress->find ( 'first', array (
					'conditions' => array (
							'ShippingAddress.id' => $shipping_address_id 
					),
					'recursive' => 0 
			) );
      
		}
    $shippingAddressTypes = $this->ShippingAddressType->findAllForForm();
    $currentCompany = $this->Company->find('first', array(
        		'conditions' => array(
        				'Company.id =' => $company_id,
        		),
        ));
        $this->_loadCombosGeo($this->data,$countries,$states,$cities,$neighbourhoods);

		$this->set(compact('companies', 'countries', 'states', 'cities','neighbourhoods','currentCompany','action', 'shippingAddressTypes'));
    
		$this->render('admin_add');
        
	}
	
	function geoAjax ($dimension = 'state', $parent_id = 1){
		$this->autoRender = false;
		$ret = array ();
		switch ($dimension)
		{
			case 'state':
				$ret = $this->ApiState->findByCountryForApi ($parent_id);
				break;
			case 'city':
				$ret = $this->ApiCity->findByStateForApi ($parent_id);
				break;
			case 'neighbourhood':
				$ret = $this->ApiNeighbourhood->findByCityForApi ($parent_id);
				break;
		}
		$this->set ('geos', $ret);
		$this->render('geoAjax', 'ajax');
	}
	function _loadCombosGeo($data,&$countries,&$states,&$cities,&$neighbourhoods)
	{
		$countries = $this->ApiCountry->findAllForApi();
		 
		$states = $this->ApiState->findAllForApi();
			
		$cities = $this->ApiCity->findByStateForApi($data['ShippingAddress']['state_id']);
		if(!isset($cities)){
			$cities=array();
		}
		
		$neighbourhoods = $this->ApiNeighbourhood->findByCityForApi($data['ShippingAddress']['city_id']);
		if(!isset($neighbourhoods)){
			$neighbourhoods=array();
		}
	}
	
}