<?php

class BannerTypesController extends AppController {

    var $name = 'BannerTypes';

    function admin_index() {
        $this->BannerTypes->recursive = 0;
        $this->set('bannertypes', $this->paginate());
    }

    function admin_add() {

        if (!empty($this->data)) {

            if ($this->BannerType->save($this->data)) {
                $this->Session->setFlash(__('Registro guardado', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('No se pudo guardar el formato', true));
            }
        }
    }

    function admin_edit() {
        if (!empty($this->data)) {
            //Guardamos el registro y notificamos
            if ($this->BannerType->save($this->data)) {
                $this->Session->setFlash('Formato guardado', true);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('No se pudo guardar el formato', true));
            }
        }
    }

    function admin_delete($id=null) {
        //verificamos entradas indirectas
        if ($id == null)
            die("No ID received");
        $this->BannerTypes->id = $id;
        $unknownId = Configure::read('BannerTypes.unknown_id');

        //Verificamos si el formato existe
        if ($unknownId == $id) {
            $this->Session->setFlash('El formato Unknown no puede borrarse.');
            return $this->redirect(array('action' => 'index'));
        }
        try {
            //Borramos el formato y notificamos
            $this->BannerType->delete($id);
            $this->Session->setFlash('Se borro correctamente el formato.');
        } catch (Exception $e) {
            $this->Session->setFlash('El formato no pudo borrarse. Error: ' . $e->getMessage());
        }
        $this->redirect(array('action' => 'index'));
    }

}

?>