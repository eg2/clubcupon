<?php

class BeaconDevicesController extends AppController {

    var $name = 'BeaconDevices';
    var $uses = array(
        'beacon.BeaconDevice'
    );
    
    var $paginate = array(
        'limit' => 10,
        'order' => array(
            'BeaconDevice.id' => 'desc'
        ),
    	'conditions' => array(
    		'BeaconDevice.deleted' => 0
    	)
    );

    function admin_index() {
        $this->pageTitle = 'Beacons - dispositivos';
        $beacondevices = $this->paginate();
        $size = count($beacondevices);
        /*for ($a = 0; $a < $size; $a++) {
            $calendars[$a]['AccountingCalendar']['enabledActions'] = $this->_returnEnabledActionsByStatus($calendars[$a]['AccountingCalendar']);
        }*/
        $this->set('beacondevices', $beacondevices);
    }
    
    function admin_delete($id) {
    	if (!empty($id)) {
    		$beaconToSetAsDeleted = $this->BeaconDevice->find('first', array(
    				'conditions' => array(
    						'BeaconDevice.id' => $id
    				)
    		));
    		if ($this->setBeaconAsDeleted($beaconToSetAsDeleted)) {
    			$this->Session->setFlash('Beacon eliminado', 'default', null, 'success');
    		} else {
    			$this->Session->setFlash('No se pudo eliminar el Beacon', true);
    		}
    	} else {
    		$this->Session->setFlash('Debe seleccionarse un Beacon a eliminar', true);
    	}
    	$this->redirect(array(
    			'action' => 'index'
    	));
    }
    private function setBeaconAsDeleted($beacon) {
    	$fields = array(
    			'BeaconDevice.deleted' => 1,
    			'BeaconDevice.modified_by' => $this->Auth->user('id')
    	);
    	$conditions = array(
    			'BeaconDevice.id' => $beacon['BeaconDevice']['id']
    	);
    	return $this->BeaconDevice->updateAll($fields, $conditions);
    }

    function admin_add() {
        if (!empty($this->data)) {
            	$this->BeaconDevice->set($this->data);
            	$this->BeaconDevice->set('code', ((string)trim($this->data['BeaconDevice']['uuid'])).((string)trim($this->data['BeaconDevice']['major'])).((string)trim($this->data['BeaconDevice']['minor'])));
                $this->BeaconDevice->set('created_by', $this->Auth->user('id'));
          
                if ($this->BeaconDevice->save()) {
                    $this->Session->setFlash('Dispositivo guardado', 'default', null, 'success');
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                	//var_dump($this->BeaconDevice->validationErrors);
                    $this->Session->setFlash('No se pudo guardar los datos', true);
                }
            
        }
    }

    function admin_edit($id) {
        if (!empty($this->data)) {
        	     $this->BeaconDevice->set($this->data);
               //$this->BeaconDevice->set('code', ((string)trim($this->data['BeaconDevice']['uuid'])).((strin)trim($this->data['Beacondevice']['major'])).((string)trim($this->data['Beacondevice']['minor'])));
                $this->BeaconDevice->set('modified_by', $this->Auth->user('id'));
               
                
                if ($this->BeaconDevice->save()) {
                    $this->Session->setFlash('Registro editado', true);
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash('No se pudo editar el registro', true);
                }
            
        }
        $this->data = $this->BeaconDevice->read(null, $id);
        $this->render('admin_add');
    }


    function validateDeviceFields($data,&$message) {
    	
        return true;
    }

}
