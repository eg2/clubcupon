<?php

// DEVELOP DEBUGs ---------------------------------------------------------------------------
Configure::write('app.tracking.enable', true);
Configure::write('app.rDebug.enable', false);
Configure::write('app.replaceDealImages.enable', true); // Si no hay imÃ¡genes originales se reemplazan por una imagen
// que indica que no hay imagen original
define('NO_ORIGINAL_IMAGE', 'NO-IMAGE');

// END GLOBAL DEBUG -------------------------------------------------------------------------
class ConstUserTypes {

    const Admin = 1;
    const User = 2;
    const Company = 3;
    const Agency = 4;
    const Partner = 5;
    const SuperAdmin = 6;
    const Seller = 7;
    const Guest = 8;

    /*
     * Is super admin predicate
     */

    static function isLikeSuperAdmin($type) {
        return $type == self::SuperAdmin;
    }

    /*
     * Is not super admin predicate
     */

    static function isNotLikeSuperAdmin($type) {
        return !self::isLikeSuperAdmin($type);
    }

    /*
     * Is admin or equivalent
     */

    static function isLikeAdmin($type) {
        return $type == self::Admin || ($type == self::SuperAdmin);
    }

    /*
     * Is not admin or equivalent
     */

    static function isNotLikeAdmin($type) {
        return !self::isLikeAdmin($type);
    }

    /**
     * Admin or Partner user type predicate
     */
    static function isLikeAdminOrPartner($type) {
        return self::isLikeAdmin($type) || ($type == self::Partner);
    }

    /**
     * Admin or Partner user type predicate
     */
    static function isNotLikeAdminOrPartner($type) {
        return !self::isLikeAdmin($type) && ($type != self::Partner);
    }

    /**
     * Is not privileged user or admin
     */
    static function isNotPrivilegedUserOrAdmin($type) {
        return !self::isPrivilegedUserOrAdmin($type);
    }

    /*
     * Is privileged user or admin
     */

    static function isPrivilegedUserOrAdmin($type) {
        return self::isPrivilegedUser($type) || self::isLikeAdmin($type);
    }

    /**
     * Is not privileged user (Like partner or Agency)
     */
    static function isNotPrivilegedUser($type) {
        return !self::isPrivilegedUser($type);
    }

    /*
     * Is privileged user (Like partner or Agency)
     */

    static function isPrivilegedUser($type) {
        return ($type == self::Partner) || ($type == self::Agency);
    }

}

class ConstDealTypes {

    // Refieren a condiciones de alta de ofertas, y se utilizan en las vistas
    const Regular = 0;
    const Fibertel = 1;
    const Clarin365 = 2;
    const Ticketportal = 3;
    const Circa = 4;

}

class BuyChannel {

    const Mobile = 2;
    const Site = 1;

}

// very_short
Cache::config('very_short', array(
    'engine' => 'File',
    'duration' => '+1 minute',
    'probability' => 100,
    'path' => CACHE,
    'prefix' => 'cake_very_short'
        // [optional]  prefix every cache file with this string
));
Configure::write('payment.default_payment_setting_id', array(
    false => 8,
    true => 7
));
Configure::write('payment.default_payment_setting_ids', array(
    'cordoba' => array(
        false => 9,
        true => 7
    ),
    'productos' => array(
        false => 9,
        true => 7
    )
));

function default_payment_setting_id($city_slug, $is_tourism = false) {
    if (!in_array($city_slug, array_keys(Configure::read('payment.default_payment_setting_ids')))) {
        $ret = Configure::read('payment.default_payment_setting_id');
        return $ret[$is_tourism];
    } else {
        $ret = Configure::read('payment.default_payment_setting_ids');
        return $ret[$city_slug][$is_tourism];
    }
}

Configure::write('deal.additionalImages.limit', 4);
Configure::write('deal.NewsLetterImage.width', 609);
/**
 * La estructura es <fieldNameDb> => <ArrayDefinicion>
 * <ArrayDefinicion> => Es un array que soporta las claves:
 * <type> => date|select|freeInput - tipo de dato
 * <label> => String libre para nombre de columna, se puede omitir y se usara <fieldNameDb>
 * <show> => Boolean para ocultar se puede omitir y por defecto se toma true
 * <sourceValue> => <ArraySource>|String si es un string es el nombre de la entidad de la cual recuperar los datos.
 * <source> => String:entity | String:array Decide si el valor se toma desde una entidad o desde un literal de array
 * <sourceConditions> => <ArrayConditions> un array compatible con el mÃ©todo find del ORM de cake (por si se quieren filtrar inputs)
 */
Configure::write('cluster_example_definition', array(
    'id' => array(
        'type' => 'noAbm',
        'show' => false
    ),
    'dob' => array(
        'type' => 'date',
        'label' => 'Fecha de nacimiento'
    ),
    'gender' => array(
        'type' => 'select',
        'label' => 'GÃ©nero',
        'source' => 'array',
        'sourceValue' => array(
            'm' => 'Masculino',
            'f' => 'Femenino'
        )
    ),
    'localidad' => array(
        'type' => 'select',
        'label' => 'Localidad',
        'source' => 'entity',
        'sourceValue' => 'City'
    ),
    'is_active' => array(
        'type' => 'select',
        'label' => 'Activo',
        'source' => 'array',
        'sourceValue' => array(
            '1' => 'Si',
            '0' => 'No'
        )
    )
));
Configure::write('DealCategory.unknown_id', 1);
Configure::write('SMS.celco', array(
    1 => 'Personal',
    2 => 'Movistar',
    3 => 'Claro',
    4 => 'Nextel'
));
Configure::write('banner', array(
    'imageWidth' => '230',
    'imageHeight' => '100'
));
Configure::write('bannerNewsletter', array(
    'imageWidth' => '598',
    'imageHeight' => '100'
));
Configure::write('bannerLogout', array(
    'imageWidth' => '680',
    'imageHeight' => '239'
));
Configure::write('company.email', 'test@clubcupon.com.ar');
Configure::write('Email.blacklisted_domains', array(
    'sex.com'
));
Configure::write('Email.affiliates', array(
    'AdWords',
    'Soicos',
    'Intextual',
    'Mailtrack',
    'ThinkThanks',
    'ClickMagic',
    'vendedor',
    'latamdata'
));
// epsilon es una constante de tiempo "chica" que se remueve de los extremos de los intervalos de tiempo para evitar
// condiciones de competencia
Configure::write('ProcessDealStatus.epsilon', '30 seconds');
// array con la configuracin de los tipos de crones:
Configure::write('ProcessDealStatus.types', array(
    // los crones de consulta de pagos "nuevos":
    'new' => array(
        // edad minima para que llegue a BAC
        'minAge' => '2 minutes',
        // se consideran "nuevos" solo cuando tienen menos de 1 hora de vida
        'maxAge' => '1 hour',
        // y solo chequeamos los que estan en estado pendiente para evitar eliminar un pago que todavia no paso
        // por la plataforma de pagos (ie. un usuario hace click en "pagar", se dispara el cron y levanta un pago "W"
        // (como en BAC todavia no existe el pago, BAC devuelve una excepcion), se elimina el pago, el usuario pasa por
        // BAC, se genera el pago, pero a esta altura estamos desincronizados de BAC y todo se rompe.
        //
  // NOTA: esto solo puede suceder en el intervalo de tiempo entre que el usuario hace click en pagar (momento
        //   en el cual se general el DealExternal con estado "W") y llega a BAC (momento en el que en BAC se genera
        //   el pago/cargo y ya no retorna una excepcion).
        'statuses' => array(
            'P'
        )
    ),
    // los crones de consultas de pagos "medios":
    'med' => array(
        // se consideran "medios" cuando al menos tienen una hora de vida
        'minAge' => '1 hour',
        // pero solo si tienen menos de 24
        'maxAge' => '1 day',
        // se chquean todos los estados
        'statuses' => array(
            'P',
            'W'
        )
    ),
    // los crones de consulta de pados "viejos":
    'old' => array(
        // se consideran "viejos" cuando al menos tienen 24 horas de vida
        'minAge' => '1 day',
        // y no tienen edad minima (eventualmente se purgan cuando pasan de "Payments.pending_duration" o
        // "Payments.waiting_duration")
        'maxAge' => null,
        // se chequean todos los estados
        'statuses' => array(
            'P',
            'W'
        )
    )
));
// VersiÃ³n de assets para forzar recarga de CSSs y JSs
// ACTUALIZAR EN CADA RELEASE
Configure::write('theme.version_number', 'v12438/');
//
//
/* Nombre del Theme activo, dejar '' para default clasico Importante, la / para el directorio */
Configure::write('theme.theme_name', 'theme_clean/');
Configure::write('theme.asset_version', Configure::read('theme.theme_name') . Configure::read('theme.version_number'));
// Configure::write ('theme.asset_version', Configure::read('theme.theme_name') . '');
Configure::write('theme_dynamic.theme_name', 'theme_clean/dynamic/');
Configure::write('theme_dynamic.asset_version', Configure::read('theme_dynamic.theme_name') . Configure::read('theme.version_number'));
Configure::write('NowPlugin.theme.theme_name', 'theme_clean/');
Configure::write('NowPlugin.deal.is_side_deal_enabled', false);
Configure::write('NowPlugin.deal.min_limit', 1);
Configure::write('NowPlugin.deal.buy_min_quantity_per_user', 1);
Configure::write('NowPlugin.deal.commission_percentage', 30);
Configure::write('NowPlugin.deal.only_price', 0);
Configure::write('NowPlugin.deal.hide_price', 0);
Configure::write('NowPlugin.deal.enable_sheduled_deals', true);
Configure::write('NowPlugin.deal.coupon_duration', 1);
Configure::write('NowPlugin.paymentOptions', 'Por NPS');
Configure::write('NowPlugin.branches_latitude', '-34.6036');
Configure::write('NowPlugin.branches_longitude', '-58.3815');
Configure::write('NowPlugin.testing_scheduling', true);
Configure::write('NowPlugin.time_range', array(
    6 => 'MaÃ±ana (06hs-10hs)',
    10 => 'MediodÃ­a (10hs-14hs)',
    14 => 'Tarde (14hs-18hs)',
    18 => 'Noche (18hs-22hs)',
    22 => 'Trasnoche (22hs-06hs)'
));
Configure::write('NowPlugin.candidate.noalaorden', 0);
/* Offset for calendar minimun date */
/* If offset is 0 then today + 0 days are used (today)
 * If offset is 1 then today + 1 days are used (tomorrow)
 * If offset is 3 then today + 3 days are used (3 days from now)
 * If offset is -1 then today + (-1) days are used (yesterday)
 */
Configure::write('NowPlugin.calendar_minium_date', 0);
/* cantidad de credito que se bonifica por referente */
Configure::write('user.referral_amount', 10);
/* lapso en horas en el que se permite la bonificacion por referente */
Configure::write('user.referral_deal_buy_time', 72);
/**/
Configure::write('user.is_referral_system_enabled', true);
/* if set to true it will use external script to generate caf file  (script located at webroot/afiliados/posnet) */
Configure::write('Caf.use_external_export_script', false);
/* Posnet FIID, length:4 */
Configure::write('Caf.FIID', 'CLCU');
/* saves to redemtions_exports_history the exported file */
Configure::write('Caf.log_exports_history', true);
// En etapa de homologaciÃ³n deberÃ¡s utilizar HML3 y cuando lo generes para producciÃ³n deberÃ¡s poner PRO3
Configure::write('Caf.LogicalNetwork', 'PRO3');
Configure::write('Caf.bin', '62797603');
/* numero de dias de antiguedad mÃ¡xima de fecha-de-fin que puede tener una oferta-similar para el envio de mails a usuarios con pago cancelado */
Configure::write('similar_deal.max_days', 7);
/* url de verificacion para clarin 365 */
Configure::write('clarin365.verification_url', "http://old.365.com.ar/ws/api/verificacion.php");
Configure::write('google_adwords.fields', array(
    'utm_source',
    'utm_medium',
    'utm_term',
    'utm_content',
    'utm_campaign'
));
Configure::write('google_adwords.dealexternal_type', 1);
Configure::write('meta.description', 'Cupones de descuento hasta 60% en todas las categorÃ­as: viajes, spa, cines, restaurantes, belleza, electro y mucho mÃ¡s');
Configure::write('publication_channel.web', '1');
Configure::write('publication_channel.sms', '2');
Configure::write('publication_channel.ALL', array(
    '1' => 'Web',
    '2' => 'SMS'
));
// Configure::write('corporate.city_name', 'exclusive');
Configure::write('branchesdeals.expiry_days', 7);
// clubcupon NOW
Configure::write('now.asset_version', 'now/' . Configure::read('theme.version_number'));
Configure::write('now.is_enabled', 1);
Configure::write('now.is_menu_enabled', 1);
Configure::write('NowPlugin.search.pagelimit', 20);
Configure::write('navidad.is_menu_enabled', 0);
Configure::write('NowPlugin.home.categories', array(
    0 => array(
        'id' => 'todas',
        'name' => 'Todas las categorÃ­as'
    ),
    1 => array(
        'id' => 33,
        'name' => 'Entretenimiento'
    ),
    2 => array(
        'id' => 34,
        'name' => 'Salud y belleza'
    ),
    3 => array(
        'id' => 35,
        'name' => 'GastronomÃ­a'
    ),
    4 => array(
        'id' => 36,
        'name' => 'Otras'
    )
));

class NowTimeSpanOptions {

    static function vars() {
        return array(
            '0' => array(
                0,
                7,
                "Madrugada (0 a 7)"
            ),
            '7' => array(
                7,
                12,
                "MaÃ±ana (7 a 12)"
            ),
            '11' => array(
                11,
                16,
                "MediodÃ­a (11 a 16)"
            ),
            '15' => array(
                15,
                20,
                "Tarde (15 a 20)"
            ),
            '19' => array(
                19,
                24,
                "Noche (19 a 24)"
            )
        );
    }

    static function fix() {
        return array(
            'A' => 'Ahora',
            'H' => 'En cualquier horario'
                // 'M' => 'MaÃ±ana',
        );
    }

    static function horas() {
        return array(
            0 => '00:00',
            1 => '01:00',
            2 => '02:00',
            3 => '03:00',
            4 => '04:00',
            5 => '05:00',
            6 => '06:00',
            7 => '07:00',
            8 => '08:00',
            9 => '09:00',
            10 => '10:00',
            11 => '11:00',
            12 => '12:00',
            13 => '13:00',
            14 => '14:00',
            15 => '15:00',
            16 => '16:00',
            17 => '17:00',
            18 => '18:00',
            19 => '19:00',
            20 => '20:00',
            21 => '21:00',
            22 => '22:00',
            23 => '23:00',
            24 => '24:00'
        );
    }

}

class ConstFieldDataNames {

    // Refieren a condiciones de alta de ofertas, y se utilizan en las vistas
    const NowDealCatsTodas = 'Todas las categorias';
    const NowDealRootCat = 'Now Root';
    const NowDealCategoryDefault = 'Otra CategorÃ­a';

}

class ConstElementsEPlanning {

    const AD = 0;
    const AD_NORMAL = 1;
    const ADS_DEAL = 2;
    const AD_LOGOUT = 3;
    const AD_BACKGROUND = 4;

}

class ConstCandidate {

    const NowCandidateTipoPercep = 0;
    const NowCandidateTipoPers = 0;
    const NowCandidateDecJurTer = 0;

    function allowedFiles() {
        return array(
            'JPG',
            'JPEG',
            'GIF',
            'PNG',
            'BMP',
            'PDF',
            'DOC',
            'DOCX'
        );
    }

    function iconFiles() {
        return array(
            'PDF',
            'DOC',
            'DOCX'
        );
    }

}

class ConstNowRegisterSteps {

    const SignUp = 1;
    const ConfirmMail = 2;
    const MyCompany = 3;

}

class ConstCandidateStatuses {

    const Draft = 0;
    const Pending = 1;
    const Reject = 2;
    const Approved = 3;

}

Configure::write('corporate.city_name', array(
    'exclusive',
    'exclusivo-cmd',
    'mcdonalds',
    'zurich',
    'mapfre',
    'tarjeta-naranja',
    'beneficios-grupo',
    'la-argentina',
    'yagmour',
    'apex'
));
Configure::write('corporate_exceptions.city_name', array(
    'exclusive',
    'beneficios-grupo',
    'exclusivo-cmd'
));
Configure::write('corporate.external_hidden_cities', array(
    'virginiaslims'
));
Configure::write('secondaryDeals.excludedCitySlugs', array(
    'rutatlantica'
));
Configure::write('secondaryDeals.upperDealsLimit', 3);
Configure::write('secondaryDeals.lowerDealsLimit', 6);
Configure::write('ExclusiveEmailTemplate.subject', 'Bienvenido a Clubcupon Exclusive');
Configure::write('user.sms_id', 1);
/** EMAIL * */
Configure::write('EmailTemplate.catch_all_account', '@clubcupon.com.ar');
Configure::write('EmailTemplate.debug_email', false);
Configure::write('deal.expiry_hours', 96);
Configure::write('deal.reindex_days', 30);
Configure::write('deal.discount_mp', 0.10);
Configure::write('deal.limit_discount_mp', 100);
/* configuraciÃ³n para la bÃºsqueda por posiciÃ³n en API/deals - API. */
Configure::write('API.geo.delta', 0.1);
Configure::write('City.clubcuponya', 'ya');
/* Habilita LiteBox si es true */
Configure::write('isLiteBoxEnabled', false);
/**
 *  Metodo de pago para ofertas de tipo producto
 */
Configure::write('deal.paymentOptions', 'Por MercadoPago Productos');
/**
 *  Metodo de pago para ofertas de tipo turismo
 */
Configure::write('deal.paymentOptionsTurism', 'Por MercadoPago Turismo');
/**
 *  Metodo de pago que no son exlusivos, es decir son comunes entre metodos de
 * pago de producto , turismo y normal
 */
Configure::write('deal.not_exclusive_paymentOptions', array(
    'Por NPS'
        /* 'Por Monedero' */
));
Configure::write('passbook.enabled', true);
// PASSBOOK
// Al pasar al ambiente productivo revisar:
// passbook.standardKeys > organizationName //NO CAMBIAR! es siempre 'CLARIN GLOBAL S.A.'
// passbook.standardKeys > passTypeIdentifier
// passbook.standardKeys > serialNumber
// passbook.standardKeys > teamIdentifier
//
Configure::write('passbook.route', ROOT . DS . 'app' . DS . 'vendors' . DS . 'passbook' . DS . 'certificados' . DS);
Configure::write('passbook.imagesRoute', '../webroot/img/theme_clean/passbook/');
Configure::write('passbook.p12Cert', Configure::read('passbook.route') . 'Certificates.p12'); //p12
Configure::write('passbook.setWWDRcertPath', Configure::read('passbook.route') . 'WWDR.pem'); //PEM
Configure::write('passbook.password', 'cmdcmdcmd'); //password con el cual se genero el p12
Configure::write('passbook.hmac_secret', 'setec astronomy'); // secret para el hash
// Configure::write('static_domain_for_api', STATIC_DOMAIN_FOR_CLUBCUPON.'/');
Configure::write('ayuda.comercios', 'http://comercios.clubcupon.com.ar/');
Configure::write('deal.paymentOptionsTurism', array(
    'Por MercadoPago Turismo',
    'Por NPS Turismo'
));
Configure::write('deal.paymentOptions', array(
    'Por MercadoPago Productos',
    'Por NPS'
));
Configure::write('deal.paymentOptionForNormal', array(
    'Por NPS'
));
Configure::write('deal.paymentTerms', array(
    'LiquidaciÃ³n por vendidos  (30 dias)' => 30,
    'LiquidaciÃ³n por redimidos (7  dias)' => 7
));
// hash table states with
configure::write('BAC.id_provincia_id_state', array(
    '5429' => '1', //Capital Federal
    '5431' => '1', //Capital Federal
    '5448' => '1', //Capital Federal
    '5421' => '2', //Buenos Aires
    '5905' => '2', //Buenos Aires
    '6201' => '2', //Buenos Aires
    '5665' => '2', //Buenos Aires
    '5663' => '2', //Buenos Aires
    '5507' => '3', //Salta
    '5527' => '3', //Salta
    '5513' => '4', //Catamarca
    '5442' => '5', //Tucuman
    '5534' => '5', //Tucuman
    '5524' => '6', //Misiones
    '5519' => '7', //Formosa
    '5518' => '8', //Entre RÃ­os
    '5445' => '8', //Entre RÃ­os
    '5517' => '9', //Corrientes
    '5506' => '10', //San Luis
    '5529' => '10', //San Luis
    '5528' => '11', //SanJuan
    '5434' => '13', //Santa Fe
    '5531' => '13', //Santa Fe
    '5514' => '22', //Chaco
    '5515' => '19', //Chubut
    '5432' => '14', //Cordoba
    '5520' => '23', //Jujuy
    '5521' => '17', //La Pampa
    '5522' => '24', //La Rioja
    '5441' => '15', //Mendoza
    '5523' => '15', //Mendoza
    '5662' => '15', //Mendoza
    '5447' => '16', //Neuquen
    '5525' => '16', //Neuquen
    '5466' => '18', //RÃ­o negro
    '5526' => '18', //RÃ­o negro
    '5530' => '20', //Santa Cruz
    '5532' => '12', //Santiago De Este..
    '5533' => '21'
        // Tierra del Fuego
));
Configure::write('company.isCompanyPanelEnabled', true);
Configure::write('BAC.localidadComercial', 'Capital Federal');
Configure::write('BAC.gateway_efectivo', 200);
Configure::write('BAC.id_medio_de_pago_compensacion', 205);
Configure::write('deal.payment_term.now', 7);

class ConstSegmentationCategoryIds {

    const CATEGORY_PRODUCT = 7;
    const CATEGORY_BEAUTY = 10;
    const CATEGORY_GASTRONOMY = 2;
    const CATEGORY_TRAVEL = 4;
    const CATEGORY_ENTERTAINMENT = 5;
    const CATEGORY_PHOTOGRAPHY = 6;
    const CATEGORY_SERVICES = 12;
    const CATEGORY_COUNTRY_DAY = 14;
    const CATEGORY_SHOPPING = 15;
    const CATEGORY_PET = 19;
    const CATEGORY_NOW_ROOT = 21;
    const CATEGORY_OTHER = 32;

}

class ConstSegmentationSex {

    const UNDEFINED = 'DESCONOCIDO';
    const MALE = 'MASC';
    const FEMALE = 'FEM';

}

Configure::write('EfgUserProfileUpdate.days_for_profile_calculation', 180);
Configure::write('EfgUserProfileUpdate.default_profile_id', 3);
Configure::write('efg.filter_by_sender_engine.enable', 0);
Configure::write('efg.filter_by_sender_engine.sender_engine_code', 2);
Configure::write('deal_gender.ALL', array(
    'f' => 'Femenino',
    'm' => 'Masculino',
    'u' => 'Unisex'
));
Configure::write('Site.city_logo', array(
    'height' => 72
));
Configure::write('themeBrown.enabled', false);
Configure::write('orderPlugin.enabled', false);
Configure::write('themeBrown.enabled', false);

class DayOfWeek {

    // ISO-8601 numeric representation of the day
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;
    const SUNDAY = 7;

}

Configure::write('orderPlugin.enabled', true);
Configure::write('sellersPanel.openVelocity', 500);
// SEARCH PLUGIN ---------------------------------------------------------------
$GLOBALS['paramsDebug'] = false; // Debug - False para producciÃ³n.
$GLOBALS['trackingEnable'] = false; // True para producciÃ³n.
$GLOBALS['isLink'] = false; // Link Control
$GLOBALS['showFixedRFacets'] = false; // Fixed Range Facets
$GLOBALS['extraDealsFirstPage'] = 1; // Ofertas extras en la primer pÃ¡gina de resultados para equilibrar diseÃ±o de columnas (par/impar).
$GLOBALS['dealsQuantityPerPage'] = 21;
Configure::write('search.asset_version', 'search/' . Configure::read('theme.version_number'));
Configure::write('bootstrap.asset_version', 'bootstrap/' . Configure::read('theme.version_number'));
Configure::write('plugin-order.asset_version', 'plugin-order/' . Configure::read('theme.version_number'));
$GLOBALS['dummySearchElementUrlImage'] = '/img/search/img_cat.jpg';
$GLOBALS['dummySearchFirstElementUrlImage'] = '/img/search/img_destacado_cat.jpg';
// Messages
$GLOBALS['messages']['No Result'] = '<h1>Sin resultados para su bÃºsqueda.</h1><br />';
$GLOBALS['messages']['ShowResultsFor'] = ' Mostrando resultados para ';
$GLOBALS['messages']['ShowAllResults'] = ' Mostrando todas las ofertas';
$GLOBALS['messages']['Maybe1'] = "Buscaste '";
$GLOBALS['messages']['Maybe2'] = "', ,Â¿tal vez quisiste escribir ";
// Facets
$GLOBALS['extraFilters'] = array(
    'city_is_corporate:"false"',
    '-city_name:"Now Root"',
    '-city_name:"Now Root "'
);
// Facets
$GLOBALS['facetsParams'] = array(
    "fl" => array(
        'deal_name',
        'deal_description',
        'deal_category_path',
        'deal_original_price',
        'deal_discounted_price',
        'deal_end_date',
        'deal_id',
        'deal_slug',
        'deal_subtitle',
        'deal_discount_percentage',
        'deal_image_path',
        'deal_only_price',
        'deal_hide_price',
        'deal_is_purchasable',
        'deal_is_tourism',
        'deal_descriptive_text',
        'city_name',
        'city_is_group',
        'city_is_corporate',
        'company_name',
        'company_city_id'
    ),
    'facet' => 'true',
    'facet.mincount' => 1,
    'facet.field' => array(
        'city_name_no_group',
        'city_name_group',
        'deal_gender'
    ),
    'facet.pivot' => 'deal_category_path_l1,deal_category_path_l2,deal_category_path_l3',
    'facet.pivot.mincount' => 0
);
// Facets Hierarchical
$GLOBALS['HierFacetsCats'] = array(
    'deal_category_path_l1',
    'deal_category_path_l2',
    'deal_category_path_l3'
);
$GLOBALS['HierFacetsFieldNoLevel'] = 'deal_category_path_l';
// Facets Labels
$GLOBALS['facetsLabels'] = array(
    'city_name_no_group' => 'Ciudades',
    'city_name_group' => 'Especiales',
    'deal_gender' => 'GÃ©nero'
);
// Facets translations
$GLOBALS['facetsTranslations'] = array(
    'deal_gender' => array(
        'f' => 'Femenino',
        'm' => 'Masculino',
        'u' => 'Unisex'
    )
);
// Facets Range
$GLOBALS['fctsRParams'] = array(
    "facet.range" => "deal_discounted_price",
    "facet.range.start" => 0,
    "facet.range.end" => 50000,
    "facet.range.gap" => 500
);
$GLOBALS['facetsCustomRangeField'] = 'deal_discounted_price';
// BlackLists
$GLOBALS['CatBlackList'] = array(
    'Now Root',
    'Now Root ',
    ''
);
$GLOBALS['FctsBlackList'] = array(
    '_empty_',
    ''
);
// Search Order
$GLOBALS['searchOrderOptions'] = array(
    'Más relevantes' => '',
    'Menor precio' => 'deal_discounted_price asc',
    'Mayor precio' => 'deal_discounted_price desc',
    'Fecha de finalización' => 'deal_end_date asc'
);
Configure::write('search.specialCategoryIDWithEplanning', 5);
Configure::write('search.eplanningIds', array(
    'homeMiddle1x462' => 'Home/Middle1x462'
));
// END SEARCH PLUGIN -----------------------------------------------------------
// WEB PLUGIN ------------------------------------------------------------------
Configure::write('web.meta.description', 'Cupones de descuento hasta 60% en todas las categorÃ­as: viajes, spa, cines, restaurantes, belleza, electro y mucho mÃ¡s');
Configure::write('web.meta.keywords', 'Club CupÃ³n, descuentos, promociones, viajes, spa, cines, restaurantes, belleza, electro, promociÃ³n, cupÃ³n, Argentina');
Configure::write('web.meta.title', 'Descuentos hasta 60% en viajes, spa, cines, restaurantes, belleza, electro y mucho mÃ¡s');
Configure::write('web.plugin.enable', true);
Configure::write('web.tuTiempoLibre.enable', false);
Configure::write('web.pluginDebugImages.dummyListElementUrlImage', '/img/web/clubcupon/img_cupon.jpg');
Configure::write('web.pluginDebugImages.dummyFirstElementUrlImage', '/img/web/clubcupon/img_destacado.jpg');
Configure::write('web.pluginDebugImages.dummyDealUrlImage', '/img/web/clubcupon/img_ficha.jpg');
Configure::write('web.pluginDebugImages.dummyListMostViewedElementUrlImage', '/img/web/clubcupon/mas_visto_img.jpg');
Configure::write('web.pluginDebugImages.dummyUDNFirstUrlImage', '/img/web/exclusives/nuestrosBeneficios/destacado_2.jpg');
Configure::write('web.pluginDebugImages.dummyUDNListElementUrlImage', '/img/web/exclusives/nuestrosBeneficios/img_cupon.jpg');
Configure::write('web.pluginDebugImages.dummyUDNDealUrlImage', '/img/web/exclusives/nuestrosBeneficios/img_ficha.jpg');
Configure::write('web.pluginDebugImages.dummyUDNFirstUrlImageCC', '/img/web/exclusives/nuestrosBeneficios/destacado_2_cc.jpg');
Configure::write('web.pluginDebugImages.dummyUDNListElementUrlImageCC', '/img/web/exclusives/nuestrosBeneficios/img_cupon_cc.jpg');
Configure::write('web.pluginDebugImages.dummyUDNDealUrlImageCC', '/img/web/exclusives/nuestrosBeneficios/img_ficha_cc.jpg');
Configure::write('web.default.citySlug', 'ciudad-de-buenos-aires');
Configure::write('web.default.companySlug', 'all');
Configure::write('web.default.cityId', 42550);
Configure::write('web.rememberLogin.cookieTime', '+4 weeks');
Configure::write('web.firstUser.cookieTime', '+4 weeks');
Configure::write('web.cookie.time', 3600); // o '1 hour'
Configure::write('web.cookie.domain', 'clubcupon.com.ar');
Configure::write('web.cookie.key', 'h34gj46h546');
Configure::write('web.FbFriends.count', 200);
Configure::write('web.messages.PIN_Not_Found', array(
    'PIN Inexistente',
    'DNI Inexistente'
));
Configure::write('web.message.subscriptionFailValidationCaptcha', 'El cÃ³digo de captcha ingresado es errÃ³neo.');

class ConstSearchLinksTypes {

    const Destacado = 1;
    const Especial = 2;
    const Banner = 3;
    const Banner_mobile = 4;

}

// WEB Plugin subscription
Configure::write('web.message.subscriptionOK', 'Te suscribiste correctamente a Club CupÃ³n');
Configure::write('web.message.subscriptionMail', 'Te enviamos un email para confirmar la suscripciÃ³n');
Configure::write('web.message.subscriptionExist', 'Ya estÃ¡s suscripto');
Configure::write('web.message.subscriptionMailTemplate', 'Para confirmar tu suscripcion hacÃ© click');
Configure::write('web.messages.PIN_Not_Valid', array(
    'PIN InvÃ¡lido para la pÃ¡gina a la que quiere acceder',
    'DNI InvÃ¡lido para la pÃ¡gina a la que quiere acceder'
));
Configure::write('web.messages.logout', 'Cerraste tu sesiÃ³n de usuario exitosamente.');
Configure::write('web.recoverPassword.noUserMail', 'No hay un usuario registrado con el correo ');
Configure::write('web.maxAllDeals', 1000);
Configure::write('web.quantityDealsHomeList', 32);
Configure::write('web.quantityDealDetailList', 4);
Configure::write('web.titleClubCuponDealDetailSearchedList', 'MÃ¡s buscados');
Configure::write('web.quantityClubCuponDealDetailSearchedList', 5);
Configure::write('web.titleClubCuponDealDetailList', 'MÃ¡s ofertas');
Configure::write('web.quantityClubCuponDealDetailList', 8);
Configure::write('web.quantityClubCuponHomeListA1', 8);
Configure::write('web.quantityClubCuponHomeListA2', 8);
Configure::write('web.quantityClubCuponHomeListA3', 12);
Configure::write('web.quantityClubCuponHomeListB', 8);
Configure::write('web.firstLevelURLPath', '/web/');
Configure::write('web.extraDealURLPath', '/deal/');
Configure::write('web.asset_version', 'web/' . Configure::read('theme.version_number'));
Configure::write('web.clubcupon.asset_version', 'web/clubcupon/' . Configure::read('theme.version_number'));
Configure::write('web.now.asset_version', 'web/now/' . Configure::read('theme.version_number'));
Configure::write('libs.asset_version', 'libs/' . Configure::read('theme.version_number'));
Configure::write('theme_clean.asset_version', 'theme_clean/' . Configure::read('theme.version_number'));
Configure::write('web.maxQuantityHomeCategoryLinks', 10);
Configure::write('web.defaultSlotCategories', array(
    ConstSearchLinksTypes::Destacado => array(
        array(
            'label' => 'Viajes y Escapadas',
            'path' => '/turismo',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Ofertas Nacionales',
            'path' => '/ofertas-nacionales',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Ciudad de Buenos Aires',
            'path' => '/ciudad-de-buenos-aires',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'C&oacute;rdoba',
            'path' => '/cordoba',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'La Plata',
            'path' => '/la-plata',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Mendoza',
            'path' => '/mendoza',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Rosario',
            'path' => '/rosario',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Gran Bs. As.',
            'path' => '/gran-buenos-aires-1',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Productos',
            'path' => '/search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Productos &scV=-city--ciudad~de~buenos~aires-page--1-query--oooo',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Fotograf&iacute;a',
            'path' => '/search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Fotografia&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo',
            'logo' => null,
            'styleClass' => ''
        )
    ),
    ConstSearchLinksTypes::Especial => array(
        array(
            'label' => 'Viajes y Escapadas',
            'path' => '/turismo',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Ofertas Nacionales',
            'path' => '/ofertas-nacionales',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Buenos Aires',
            'path' => '/ciudad-de-buenos-aires',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'C&oacute;rdoba',
            'path' => '/cordoba',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'La Plata',
            'path' => '/la-plata',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Mendoza',
            'path' => '/mendoza',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Rosario',
            'path' => '/rosario',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Gran Bs. As.',
            'path' => '/gran-buenos-aires-1',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Productos',
            'path' => '/search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Productos &scV=-city--ciudad~de~buenos~aires-page--1-query--oooo',
            'logo' => null,
            'styleClass' => ''
        ),
        array(
            'label' => 'Fotograf&iacute;a',
            'path' => '/search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Fotografia&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo',
            'logo' => null,
            'styleClass' => ''
        )
    )
));
Configure::write('web.maxQuantitySpecialSearchLinks', 18);
// E-Planning
// ******* E-Planning Corporate Ids
// se Dejan como ejemplo en default id de ads de corporate y normal, reemplazar ids donde corresponda
Configure::write('web.eplanningIds', array(
    'default' => 'b50611b798a32dea',
    'defaultCorporate' => 'Corporate/Right2x231x179',
    'nuestros_convenios' => array(
        0 => 'NuestrosBeneficios/Convenios1',
        1 => 'NuestrosBeneficios/Convenios2',
        2 => 'NuestrosBeneficios/Convenios3',
        3 => 'NuestrosBeneficios/Convenios4',
        4 => 'NuestrosBeneficios/Convenios5',
        5 => 'NuestrosBeneficios/Convenios6',
        6 => 'NuestrosBeneficios/Convenios7',
        7 => 'NuestrosBeneficios/Convenios8',
        8 => 'NuestrosBeneficios/Convenios9',
        9 => 'NuestrosBeneficios/Convenios10',
        10 => 'NuestrosBeneficios/Convenios11',
        11 => 'NuestrosBeneficios/Convenios12',
        12 => 'NuestrosBeneficios/Convenios13',
        13 => 'NuestrosBeneficios/Convenios14',
        14 => 'NuestrosBeneficios/Convenios15',
        15 => 'NuestrosBeneficios/Convenios16',
        16 => 'NuestrosBeneficios/Convenios17',
        17 => 'NuestrosBeneficios/Convenios18',
        18 => 'NuestrosBeneficios/Convenios19',
        19 => 'NuestrosBeneficios/Convenios20',
        20 => 'NuestrosBeneficios/Convenios21',
        21 => 'NuestrosBeneficios/Convenios22',
        22 => 'NuestrosBeneficios/Convenios23',
        23 => 'NuestrosBeneficios/Convenios24',
        24 => 'NuestrosBeneficios/Convenios25',
        25 => 'NuestrosBeneficios/Convenios26',
        26 => 'NuestrosBeneficios/Convenios27',
        27 => 'NuestrosBeneficios/Convenios28',
        28 => 'NuestrosBeneficios/Convenios29',
        29 => 'NuestrosBeneficios/Convenios30'
    ),
    'tiempoLibre' => array(
        0 => 'NuestrosBeneficios/TiempoLibre1',
        1 => 'NuestrosBeneficios/TiempoLibre2',
        2 => 'NuestrosBeneficios/TiempoLibre3',
        3 => 'NuestrosBeneficios/TiempoLibre4',
        4 => 'NuestrosBeneficios/TiempoLibre5',
        5 => 'NuestrosBeneficios/TiempoLibre6',
        6 => 'NuestrosBeneficios/TiempoLibre7',
        7 => 'NuestrosBeneficios/TiempoLibre8',
        8 => 'NuestrosBeneficios/TiempoLibre9',
        9 => 'NuestrosBeneficios/TiempoLibre10',
        10 => 'NuestrosBeneficios/TiempoLibre11',
        11 => 'NuestrosBeneficios/TiempoLibre12',
        12 => 'NuestrosBeneficios/TiempoLibre13',
        13 => 'NuestrosBeneficios/TiempoLibre14',
        14 => 'NuestrosBeneficios/TiempoLibre15',
        15 => 'NuestrosBeneficios/TiempoLibre16',
        16 => 'NuestrosBeneficios/TiempoLibre17',
        17 => 'NuestrosBeneficios/TiempoLibre18',
        18 => 'NuestrosBeneficios/TiempoLibre19',
        19 => 'NuestrosBeneficios/TiempoLibre20',
        20 => 'NuestrosBeneficios/TiempoLibre21',
        21 => 'NuestrosBeneficios/TiempoLibre22',
        22 => 'NuestrosBeneficios/TiempoLibre23',
        23 => 'NuestrosBeneficios/TiempoLibre24',
        24 => 'NuestrosBeneficios/TiempoLibre25',
        25 => 'NuestrosBeneficios/TiempoLibre26',
        26 => 'NuestrosBeneficios/TiempoLibre27',
        27 => 'NuestrosBeneficios/TiempoLibre28',
        28 => 'NuestrosBeneficios/TiempoLibre29',
        29 => 'NuestrosBeneficios/TiempoLibre30'
    ),
    'dealAds' => array(
        0 => 'Ficha/Right',
        1 => 'Ficha/R1Ofertax218'
    ),
    'dealAdsCorp' => array(
        0 => 'NuestrosBeneficios/Right1',
        1 => 'NuestrosBeneficios/Right2',
        2 => 'NuestrosBeneficios/Right3',
        3 => 'NuestrosBeneficios/Right4'
    ),
    'dealTop' => 'a09dd7a82d90d544',
    'top1' => 'd622a76d2e218082',
    'bottom1' => 'b91172ba0d0dfd95',
    'backgroundCorporate' => 'Corporate/Skin',
    'backgroundNotCorporate' => 'Ficha/Skin',
    'especiales1' => 'Menu/R1Ofertax212',
    'especiales2' => 'Menu/R2Ofertax212',
    'homeTop940' => 'Home/Top_940',
    'homePromoTop2' => 'Home/PromoTop2',
    'homeMiddle1x462' => 'Home/Middle1x462',
    'homeMiddle2x462' => 'Home/Middle2x462',
    'homeMiddle3x728' => 'Home/Middle3x728',
    'menuR3Especialx200' => 'Menu/R3Especialx200'
));
Configure::write('web.specials.enableAds', true);
Configure::write('web.desplegableImg', '/img/web/clubcupon/banner_desplegable.jpg');
Configure::write('web.plugin.enableForceNewLayout', false);
Configure::write('web.newLayout.disable', array(
    '/pages/view',
    // NOW ------------------------
    'now/now_deals/index',
    'now/now_registers/landing',
    'now/now_registers/faq',
    // WEB ------------------------
    'web/web_users/registernow',
    'web/web_cities/home',
    'web/web_cities/todos',
    'web/web_deals/deal',
    // ORDER ----------------------
    'order/order_processings/buy',
    // ----------------------------
    'users/admin_stats',
    'search/search_querys/makesearch',
    'web/web_exclusive_logins/login'
));
Configure::write('web.plugin.messagePendingApproval', 'Su Usuario Se encuentra Pendiente a AprobaciÃ³n');
Configure::write('web.plugin.messagePendingReject', 'Su Empresa Fue Rechazada');
Configure::write('web.plugin.extraLinks', array(/* 0 => array(
          'url'=>'/ofertas-nacionales',
          'label'=>'Ofertas Nacionales'
          ),
          1 => array(
          'url'=>'/turismo',
          'label'=>'Turismo'
          )
         */
));
Configure::write('web.plugin.tabsMenuNo', 'NO');
Configure::write('web.plugin.tabsMenuTopPlaceHolderCitySlug', "##CITY_SLUG##");
Configure::write('web.plugin.tabsMenuMinPriority', 10);
// prioridades de 1 a 10 siendo 1 de mayor prioridad
Configure::write('web.plugin.tabsMenuTop', array(
    1 => array(
        'label' => 'OFERTAS DEL DIA',
        'alt' => 'Ofertas del dÃ­a',
        'path' => '/ciudad-de-buenos-aires',
        'pattern' => '/ciudad-de-buenos-aires*',
        'priority' => 2
    ),
    2 => array(
        'label' => 'TURISMO',
        'alt' => 'Turismo',
        'path' => '/turismo',
        'pattern' => '/turismo*',
        'priority' => 2
    ),
    /*    3 => array(
      'label' => 'MÃ³viles',
      'alt' => 'MÃ³viles',
      'path' => '/cel/landing',
      'pattern' => '/cel/landing*',
      'priority' => 2
      ), */
    3 => array(
        'label' => 'PROMOCIONES',
        'alt' => 'Promociones',
        'path' => '/##CITY_SLUG##/pages/promociones',
        'pattern' => '/*/pages/promociones',
        'priority' => 1
    )
));
Configure::write('web.topDesplegableItemsPerColumn', 6);
Configure::write('web.specialDesplegableItemsPerColumn', 6);
// END WEB PLUGIN ---------------------------------------------------------------
Configure::write('order.ThreatMetrix', FALSE); //FALSE
/* ------------ extender el plazo de redenciÃ³n a empresas (horas) ---- */
/* ------------ a partir de la fecha de finalizaciÃ³n de la oferta ---- */
Configure::write('deal.extended_hours_to_redeem_now', 168);
Configure::write('deal.extended_hours_to_redeem_imagena', 96);
Configure::write('NewsletterPowerMta.IgnoreGroupsInNormalProfile', false);
Configure::write('NewsletterPowerMta.IgnoreLowerProfile', false);
// plugin event
Configure::write('event.emtConversionEventsCode', array(
    'VIOMAIL' => 'OPENMAIL',
    'VISITA' => 'CLICKMAIL',
    'SUBSCRIBED' => 'SUBSCRIBED',
    'OPENMARKETING' => 'OPENMARKETING',
    'CONVERSION' => 'CONVERSION'
));
Configure::write('event.marketingSumary.FrequencyEventToExport', array(
    'A' => false,
    'B' => true,
    'C' => true,
    'E' => true,
    'D' => true,
    'NC' => true
));
Configure::write('event.marketingSumary.FileHeader', array(
    '[Email]',
    '[Segmento]',
    '[Subsciption_id]',
    '[Compro]',
    '[Fecha_ultima_compra]',
    '[Fecha_ultima_apertura]',
    '[Category_id]',
    '[Category_path]'
));
Configure::write('event.marketingSumary.FilePath', '/var/www/clubcupon/branches/d9430/app/tmp/planisys/');
Configure::write('event.marketingSumary.pageSize', 5000);
Configure::write('Log.Verbose', 0);
Configure::write('Log.EchoEnabled', 1);
Configure::write('shells.DotsProgressShell', 5000);
Configure::write('shells.DotsProgressShellMin', 300);
Configure::write('BAC.sendShippingInfo', 1);
Configure::write('libertya.delivery_mode_id', 18);
Configure::write('BAC.sendProductIdInventory', 1);
Configure::write('BAC.bac_origen_portal', 10);
Configure::write('CXENSE.is_widget_enable', 1);
Configure::write('CXENSE.quantity_of_items_in_widget', 5);
Configure::write('CXENSE.height_of_item_in_widget', 160);
Configure::write('CXENSE.height_title_items_in_widget', 40);
Configure::write('web.sello.enable', false);
Configure::write('web.bannermobileHtml5.enable', false);
Configure::write('clarin365.tracking_url', 'http://old.365.com.ar/ws/api/tracking.php');
Configure::write('descuentocity.is_tracking_enabled', true);
Configure::write('web.home.specialCitiesByCity', array(
    'ciudad-de-buenos-aires' => array(
        'lo-mejor-de-la-semana',
        'turismo',
        'festival-gastronomico',
        'especial-belleza',
        'productos',
        'ofertas-nacionales',
        'especial-salidas',
        'servicios',
        'especial-hogar',
        'especial-indumentaria',
        'gran-buenos-aires-1'
    ),
    'cordoba' => array(
        'turismo',
        'ofertas-nacionales',
        'festival-gastronomico'
    ),
    'gran-buenos-aires-1' => array(
        'lo-mejor-de-la-semana',
        'turismo',
        'festival-gastronomico',
        'especial-belleza',
        'productos',
        'ofertas-nacionales',
        'especial-salidas',
        'servicios',
        'especial-hogar',
        'especial-indumentaria',
        'ciudad-de-buenos-aires'
    ),
    'la-plata' => array(
        'turismo',
        'ofertas-nacionales',
        'festival-gastronomico'
    ),
    'mendoza' => array(
        'turismo',
        'ofertas-nacionales',
        'festival-gastronomico'
    ),
    'rosario' => array(
        'turismo',
        'ofertas-nacionales',
        'festival-gastronomico'
    )
));
Configure::write('web.reindex.start_deal_id', 25000);
Configure::write('web.reindex.max_rows_limit', 5000);
Configure::write('web.beacon.versions', array(
    array(
        'version' => 1000051,
        'isBlocker' => false
    )
));
Configure::write('coupon.global_message_until', '2016-08-18 00:00:00');
Configure::write('companies.show_no_redeemed', array(
    3319,
    1372,
    3310,
    3189,
    2124,
    3250,
    3226
));
Configure::write('web.ad_server', 'eplanning'); //eplanning, dsp