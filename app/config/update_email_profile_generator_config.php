<?php

$config['newsletter.page_size_for_commit'] = 500;
$config['newsletter.page_size'] = '50000';
$config['newsletter.range_frecuency_type'] = array(
    'A' => array(
        'frecuency' => array(
            'OPENMAIL' => array(
                0,
                60
            ),
            'CLICKMAIL' => array(
                0,
                60
            )
        ),
        'domain' => array(
            'hotmail' => array(
                'for_cities' => 'A_HOTMAIL',
                'for_groups' => 'A_HOTMAIL'
            ),
            'gmail' => array(
                'for_cities' => 'A_GMAIL',
                'for_groups' => 'A_GMAIL'
            )
        ),
        'default' => array(
            'for_cities' => 'A_OTROS',
            'for_groups' => 'A_OTROS'
        )
    ),
    'B' => array(
        'frecuency' => array(
            'OPENMAIL' => array(
                60,
                181
            ),
            'CLICKMAIL' => array(
                60,
                181
            ),
            'OPENMARKETING' => array(
                60,
                181
            )
        ),
        'domain' => array(
            'hotmail' => array(
                'for_cities' => 'B_HOTMAIL',
                'for_groups' => 'B_HOTMAIL'
            ),
            'gmail' => array(
                'for_cities' => 'B_GMAIL',
                'for_groups' => 'B_GMAIL'
            )
        ),
        'default' => array(
            'for_cities' => 'B_OTROS',
            'for_groups' => 'B_OTROS'
        )
    ),
    'C' => array(
        'frecuency' => array(
            'OPENMAIL' => array(
                181,
                360
            ),
            'CLICKMAIL' => array(
                181,
                360
            ),
            'OPENMARKETING' => array(
                181,
                360
            )
        ),
        'default' => array(
            'for_cities' => 'C',
            'for_groups' => 'C'
        )
    ),
    'G' => array(
        'frecuency' => array(
            'OPENMAIL' => array(
                360,
                0
            ),
            'CLICKMAIL' => array(
                360,
                0
            ),
            'OPENMARKETING' => array(
                360,
                0
            )
        ),
        'default' => array(
            'for_cities' => 'G',
            'for_groups' => 'G'
        )
    ),
    'D' => array(
        'frecuency' => array(
            'SUBSCRIBED' => array(
                10,
                31
            )
        ),
        'domain' => array(
            'hotmail' => array(
                'for_cities' => 'D_HOTMAIL',
                'for_groups' => 'D_HOTMAIL'
            ),
            'gmail' => array(
                'for_cities' => 'D_GMAIL',
                'for_groups' => 'D_GMAIL'
            )
        ),
        'default' => array(
            'for_cities' => 'D_OTROS',
            'for_groups' => 'D_OTROS'
        )
    ),
    'F' => array(
        'frecuency' => array(
            'SUBSCRIBED' => array(
                0,
                10
            )
        ),
        'domain' => array(
            'hotmail' => array(
                'for_cities' => 'D_HOTMAIL',
                'for_groups' => 'D_HOTMAIL'
            ),
            'gmail' => array(
                'for_cities' => 'D_GMAIL',
                'for_groups' => 'D_GMAIL'
            )
        ),
        'default' => array(
            'for_cities' => 'D_OTROS',
            'for_groups' => 'D_OTROS'
        )
    ),
    'E' => array(
        'frecuency' => array(
            'SUBSCRIBED' => array(
                31,
                0
            )
        ),
        'default' => array(
            'for_cities' => 'E',
            'for_groups' => 'E'
        )
    )
);
