<?php

$config['event.marketingSumary.FrequencyEventToExport'] = array(
    'G' => false,
    'A' => true,
    'B' => true,
    'E' => true,
    'D' => false,
    'F' => true,
    'C' => false,
    'NC' => false
);
$config['event.marketingSumary.FileHeader'] = array(
    '[Email]',
    '[Segmento]',
    '[Subsciption_id]',
    '[Compro]',
    '[Fecha_ultima_compra]',
    '[Fecha_ultima_apertura]',
    '[Category_id]',
    '[Category_path]'
);
$config['event.marketingSumary.FilePath'] = '/var/www/clubcupon/current/app/tmp/planisys/';
$config['event.marketingSumary.pageSize'] = 10000;
