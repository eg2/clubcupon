<?php

/*
 * Avoids auto esccape for the the debug file.
 */
function debugEncode ($arg) {
    $arg = json_encode (           $arg);
    $arg = str_replace ('"' , "`", $arg);
    $arg = str_replace ('\'', "`", $arg);

    return $arg;
}


function removeZeroDecimals ($priceStr, $decSep = '.') {
  $parts = explode ($decSep, $priceStr);

  if (count ($parts) === 2) {
    $integer  = $parts [0];
    $decimal  = $parts [1];
    if ($decimal === '00') {
      return $integer;
    }
  }

  return $priceStr;
}


function array_pluck ($key, $input) {
  if (is_array ($key) || !is_array ($input)) {
    return array ();
  }
  $array = array ();
  foreach ($input as $v) {
    if (array_key_exists ($key, $v)) {
      $array [] = $v [$key];
    }
  }

  return $array;
}


////////////////////////////////////////////////////////////////////////////////
// Cleanup de arrays
////////////////////////////////////////////////////////////////////////////////
function array_cleanup ($array) {
  if (!is_array ($array)) {
    return $array;
  }
  $array2 = array ();
  foreach ($array as $key => $value) {
    if (!empty ($value)) {
      $array2 [$key] = $value;
    }
  }

  return $array2;
}