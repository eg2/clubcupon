<?php

/* SVN FILE: $Id$ */
/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
include_once ('core_versioned.php');

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 * 	3: As in 2, but also with full controller dump.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
Configure::write('debug', 0);
/**
 * Application wide charset encoding
 */
Configure::write('App.encoding', 'UTF-8');
/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below:
 */
// Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Uncomment the define below to use CakePHP admin routes.
 *
 * The value of the define determines the name of the route
 * and its associated controller actions:
 *
 * 'admin' 		-> admin_index() and /admin/controller/index
 * 'superuser' -> superuser_index() and /superuser/controller/index
 */
Configure::write('Routing.admin', 'admin');
/**
 * Turn off all caching application-wide.
 *
 */
//	Configure::write('app_cached_controller.disable', true); // cache de controladores
//	Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * var $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting var $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
// Configure::write('Cache.check', true);

/**
 * Defines the default error type when using the log() function. Used for
 * differentiating error logging and debugging. Currently PHP supports LOG_DEBUG.
 */
define('LOG_ERROR', 0);
/**
 * The preferred session handling method. Valid values:
 *
 * 'php'	 		Uses settings defined in your php.ini.
 * 'cake'		Saves session files in CakePHP's /tmp directory.
 * 'database'	Uses CakePHP's database sessions.
 *
 * To define a custom session handler, save it at /app/config/<name>.php.
 * Set the value of 'Session.save' to <name> to utilize it in CakePHP.
 *
 * To use database sessions, execute the SQL file found at /app/config/sql/sessions.sql.
 *
 */
Configure::write('Session.save', 'php');
/**
 * The name of the table used to store CakePHP database sessions.
 *
 * 'Session.save' must be set to 'database' in order to utilize this constant.
 *
 * The table name set here should *not* include any table prefix defined elsewhere.
 */
// Configure::write('Session.table', 'cake_sessions');

/**
 * The DATABASE_CONFIG::$var to use for database session handling.
 *
 * 'Session.save' must be set to 'database' in order to utilize this constant.
 */
// Configure::write('Session.database', 'default');

/**
 * The name of CakePHP's session cookie.
 *
 * Note the guidelines for Session names states: "The session name references
 * the session id in cookies and URLs. It should contain only alphanumeric
 * characters."
 * @link http://php.net/session_name
 */
Configure::write('Session.cookie', 'CAKEPHP');
/**
 * Session time out time (in seconds).
 * Actual value depends on 'Security.level' setting.
 */
Configure::write('Session.timeout', '250');
/**
 * If set to false, sessions are not automatically started.
 */
Configure::write('Session.start', true);
/**
 * When set to false, HTTP_USER_AGENT will not be checked
 * in the session
 */
Configure::write('Session.checkAgent', true);
/**
 * The level of CakePHP security. The session timeout time defined
 * in 'Session.timeout' is multiplied according to the settings here.
 * Valid values:
 *
 * 'high'	Session timeout in 'Session.timeout' x 10
 * 'medium'	Session timeout in 'Session.timeout' x 100
 * 'low'		Session timeout in 'Session.timeout' x 300
 *
 * CakePHP session IDs are also regenerated between requests if
 * 'Security.level' is set to 'high'.
 */
Configure::write('Security.level', 'low');
/**
 * A random string used in security hashing methods.
 */
Configure::write('Security.salt', 'e9a5561e42c5ab47c6c81c14f06c0b8281cfc3ce');
/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a querystring parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps, when debug = 0, or set to 'force' to always enable
 * timestamping.
 */
// Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
// Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JavaScriptHelper::link().
 */
// Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The classname and database used in CakePHP's
 * access control lists.
 */
Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');
/**
 * If you are on PHP 5.3 uncomment this line and correct your server timezone
 * to fix the date & time related errors.
 */
// date_default_timezone_set('UTC');

/**
 *
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 */
Cache::config('default', array(
    'engine' => 'File', //[required]
    'duration' => 3600, //[optional]
    'probability' => 100, //[optional]
    'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
    'prefix' => 'cake_', //[optional]  prefix every cache file with this string
    'lock' => false, //[optional]  use file locking
    'serialize' => true
        // [optional]
));
/*
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Apc', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Xcache', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 * 		'user' => 'user', //user from xcache.admin.user settings
 *      'password' => 'password', //plaintext password (xcache.admin.pass)
 * 	));
 *
 *
 * Memcache (http://www.danga.com/memcached/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Memcache', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
 * 	));
 *
 */
Cache::config('default', array(
    'engine' => 'File'
));
Configure::write('BAC.wsdl.crearUsuario', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.modificarUsuario', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.consultarPago', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.liquidarEmpresa', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.altaModificacionClienteEmpresa', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
// Configure::write('BAC.crear_pago', 'http://test2.bac.cmd.com.ar/backend_web/pago/crearPago.htm');
Configure::write('BAC.crear_pago', 'http://test2.bac.cmd.com.ar/backend_web/pago/crear_pago.htm');
Configure::write('BAC.crearTx', 'http://test2.bac.cmd.com.ar/backend_web/pago/crearTx.htm');
Configure::write('BAC.wsdl.obtenerPin', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.consumirPin', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.id_portal', 18);
Configure::write('BAC.bac_tourism_id', 12);
Configure::write('BAC.id_producto', 18001);
Configure::write('BAC.id_producto_debito', 18002);
Configure::write('BAC.id_producto_comision_cmd', 18003);
Configure::write('BAC.bac_tourism_id_producto', 3001);
Configure::write('BAC.bac_tourism_id_producto_debito', 3002);
Configure::write('BAC.bac_tourism_id_producto_comision_cmd', 3003);
Configure::write('BAC.id_producto_final', 18000);
Configure::write('BAC.id_tipo_usuario_empresa', 3);
Configure::write('BAC.id_tipo_usuario_usuario', 1);
Configure::write('BAC.payment_id', 6);
Configure::write('BAC.Argentina', 1);
Configure::write('BAC.CapitalFederal', 1);
Configure::write('BAC.pesos', 1);
Configure::write('BAC.payments', array(
    'nps' => array(
        'id_gateway' => 2,
        'cards' => array(
            '20' => 'American Express',
            '25' => 'Visa',
            '22' => 'Mastercard',
            '23' => 'Cabal',
            '28' => 'Italcred',
            '24' => 'Naranja'
        )
    ),
    'dim' => array(
        'id_gateway' => 1,
        'cards' => array(
            '7' => 'En hasta 12 cuotas con Visa, Master, Amex, Naranja y otras tarjetas de credito',
            '8' => 'En 1 pago con Pago Facil, Rapipago y otros medios de pago'
        )
    ),
    'wallet' => array(
        'id_gateway' => 5000,
        'cards' => array(
            '5000' => 'Monedero',
            '5001' => 'Puntos'
        )
    )
));
Configure::write('MESES', array(
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
));
define('STATIC_DOMAIN_FOR_CLUBCUPON', 'http://desa.clubcupon.com.ar');
Configure::write('static_domain_for_mails', STATIC_DOMAIN_FOR_CLUBCUPON);
Configure::write('static_domain_for_mails_2', STATIC_DOMAIN_FOR_CLUBCUPON);
Configure::write('static_domain_for_api', STATIC_DOMAIN_FOR_CLUBCUPON . '/');
Configure::write('IVA', '1.21');
Configure::write('email_burst_limit', 10);
Configure::write('email_burst_sleep', 5000);
Configure::write('tcpdf_document_root', ROOT);
Configure::write('tcpdf_path_main', ROOT . DS . 'app' . DS . 'vendors' . DS . 'tcpdf' . DS);
Configure::write('tcpdf_path_url', STATIC_DOMAIN_FOR_CLUBCUPON . '/app/vendors/tcpdf');
Configure::write('PDF.doAttach', false);
Configure::write('PDF.deal.creator', 'ClubCupon');
Configure::write('PDF.deal.author', 'ClubCupon');
Configure::write('PDF.deal.title', 'Cupon');
Configure::write('PDF.deal.subject', 'ClubCupon');
Configure::write('PDF.deal.keywords', 'Cupon, ClubCupon');
Configure::write('PDF.gift.creator', 'ClubCupon');
Configure::write('PDF.gift.author', 'ClubCupon');
Configure::write('PDF.gift.title', 'Regalo');
Configure::write('PDF.gift.subject', 'ClubCupon');
Configure::write('PDF.gift.keywords', 'Cupon, ClubCupon, Regalo');
// Lista de direcciones que reciben una copia oculta del mail que se les envia a las empresas.
Configure::write('sendBuyersListCompany.bcc', array(
    'esapir@cmd.com.ar'
));
Configure::write('Campaign.source', 'ClubCupon');
Configure::write('Campaign.subscription_medium', 'email');
// http://www.php.net/manual/en/function.strtotime.php
Configure::write('Points.duration', '1 month'); // 1 mes
Configure::write('Points.expiry_threshold', '1 week'); // 1 semana
Configure::write('Points.unlimited_action_ids', array(
    1,
    3
));
Configure::write('Points.limit', 1000);
Configure::write('MP.acc', '19788488');
/* acc_id    */
Configure::write('MP.enc', 'I8jjVGC6fdAeXx2AlOG2%2BR7q1XA%3D');
/* enc       */
Configure::write('MP.snd', '2ZZOTTkdP5necI%2FUqHYBKIWeg0U%3D');
/* sonda_key */
Configure::write('MP.son.url', 'https://www.mercadopago.com/mlb/sonda');
/* url sonda */
Configure::write('MP.btn.url', 'https://www.mercadopago.com/mla/buybutton');
/* url boton */
Configure::write('MP.item.id', '0');
/* item_id */
Configure::write('MP.item.name', 'Cupon Oferta Diaria');
/* name    */
Configure::write('MP.url.cancel', STATIC_DOMAIN_FOR_CLUBCUPON . '/deals/mp_transaction/cancelado/');
/* url_cancel     */
Configure::write('MP.url.process', STATIC_DOMAIN_FOR_CLUBCUPON . '/deals/mp_transaction/aceptado/');
/* url_process    */
Configure::write('MP.url.succesfull', STATIC_DOMAIN_FOR_CLUBCUPON . '/deals/mp_transaction/aceptado/');
/* url_succesfull */
// http://www.php.net/manual/en/function.strtotime.php
Configure::write('Payments.pending_duration', '1 week'); //  1 semana
Configure::write('Payments.waiting_duration', '8 hours'); //  8 horas
Configure::write('Payments.running_duration', '4 hours'); // 4 horas
// sacado de: http://curl.haxx.se/ca/cacert.pem
Configure::write('CertificateBundlePath', ROOT . DS . 'app' . DS . 'webroot' . DS . 'cacert.pem');
Configure::write('CIRCA', array(
    'CIRCA INSTITUCIONAL' => array(
        'CIRCACORPORATIVO'
    ),
    'AGEA S.A.' => array(
        'CIRCAAGEA'
    ),
    'AGR S.A.' => array(
        'CIRCAAGR'
    ),
    'ARTEAR S.A.' => array(
        'CIRCAARTEAR'
    ),
    'ASOCIACION ORT' => array(
        'CIRCAORT'
    ),
    'CABLEVISION S.A.' => array(
        'CIRCACABLEVISION'
    ),
    'CMD' => array(
        'CIRCACMD'
    ),
    'GESTION COMPARTIDA' => array(
        'CIRCAGCGESTION'
    ),
    'GRUPO CAMPARI' => array(
        'CIRCACAMPARI'
    ),
    'GRUPO CASSARA' => array(
        'CIRCALPC'
    ),
    'GRUPO CLARIN' => array(
        'CIRCAGC'
    ),
    'GRUPO ESPASA S.A.' => array(
        'CIRCAESPASA'
    ),
    'IDEAS DEL SUR S.A.' => array(
        'CIRCAIDEAS'
    ),
    'IMPRIPOST' => array(
        'CIRCAIMPRIPOST'
    ),
    'POLKA' => array(
        'CIRCAPOLKA'
    ),
    'RADIO MITRE' => array(
        'CIRCAMITRE'
    ),
    'T&C SPORTS' => array(
        'CIRCATYC'
    ),
    'TINTA FRESCA S.A.' => array(
        'CIRCATINTA'
    ),
    'UNIR' => array(
        'CIRCAUNIR'
    ),
    'PATAGONIK' => array(
        'CIRCAPATAGONIK'
    ),
    'CANAL RURAL' => array(
        'CIRCACANALRURAL'
    ),
    'AUTOSPORTS' => array(
        'CIRCAAUTOSPORTS'
    ),
    'FICHERO' => __FILE__
));
/**
 * La cantidad de instancias de la aplicacion que estan activas en un determinado ambiente ambiente
 *
 * Ejemplo: en produccon hay 2 instancias activas, en test 1
 */
Configure::write('instance.instanceQuantity', 1);
/**
 * Es un numero entre 0 y instance.instanceQuantity -1, indica a que instancia corresponde este archivo
 * de configuracion. Se usa para poder correr procesos desde mas de una instancia.
 *
 */
Configure::write('instance.instanceNumber', 0);
/**
 * Determina cuï¿½l de las instancias es la principal, encargada marcar como enviados los mails de las ofertas.
 * 1 en caso de que se trate de la instancia principal, 0 en caso contrario.
 *
 */
Configure::write('instance.is_main', 1);
Configure::write('facebook_lib.path', 'facebook2/facebook_desa');
Configure::write('user.sms_id', 1);
Configure::write('ExclusiveEmailTemplate.subject', 'Bienvenido a Clubcupon Exclusive');
Configure::write('bac.static_content_url', 'http://test2.bac.cmd.com.ar');
// Configure::write('cc.bac.secure_code' ,'4e7b1240681fb37eac3cad05ae702949');
Configure::write('cctur.bac.secure_code', 'a4901dfa4dddc75f512fed9ccd318e11');
// Configure::write('BAC.crearPagoSeguro', 'http://test2.bac.cmd.com.ar/backend_web/pago/crearPagoSeguro.htm');
Configure::write('BAC.crearPagoSeguro', 'http://test2.bac.cmd.com.ar/backend_web/pago/crearPagoSeguro.htm');
Configure::write('passbook.absolutDomain', 'http://desa.clubcupon.com.ar/');
Configure::write('ApiModel.find.pagelimit', 80);
Configure::write('API.check_number_of_calls', 0);
Configure::write('API.maximum', 50);
Configure::write('API.time', '50 minutes');
Configure::write('API.user.token_expiry_time', '2 hours');
Configure::write('API.user.token_check_expirity', 1);
Configure::write('BAC.id_portal_old', 4);
Configure::write('BAC.id_producto_old', 2001);
Configure::write('BAC.id_producto_debito_old', 2002);
Configure::write('BAC.id_producto_comision_cmd_old', 2003);
Configure::write('BAC.bac_tourism_id_old', 12);
Configure::write('BAC.bac_tourism_id_producto_old', 3001);
Configure::write('BAC.bac_tourism_id_producto_debito_old', 3002);
Configure::write('BAC.bac_tourism_id_producto_comision_cmd_old', 3003);
Configure::write('tec_dia.start_date', strtotime('2013-02-01 00:00:00'));
Configure::write('cc.bac.secure_code', 'f7780932be8dad7d9ac41584646eaee4');
Configure::write('cc.bac.secure_code_old', '4e7b1240681fb37eac3cad05ae702949');
Configure::write('BAC.wsdl.crearCargos', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.condicion_venta_contado', 1);
Configure::write('BAC.id_tipo_documento_empresa', 80);
Configure::write('BAC.bac_tourism_product_id_full_iva', 103033);
Configure::write('BAC.bac_tourism_product_id_half_iva', 103032);
Configure::write('BAC.bac_tourism_product_id_exempt', 103029);
Configure::write('BAC.bac_tourism_product_id_retail', 103034);
Configure::write('BAC.bac_end_user_no_redemeed', 102732);
Configure::write('efg.filter_by_sender_engine.enable', 0);
Configure::write('efg.filter_by_sender_engine.sender_engine_code', 1);
Configure::write('BAC.wsdl.crearCargos', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.wsdl.crearPagos', 'http://test2.bac.cmd.com.ar/backend_ws/BacWS?wsdl');
Configure::write('BAC.gateway_efectivo', 200);
Configure::write('BAC.id_medio_de_pago_compensacion', 205);
Configure::write('Promotions.PaymentOptionIdWithPromotion', 3);
Configure::write('Promotions.WeekDayWithPromotion', array(
    DayOfWeek::WEDNESDAY,
    DayOfWeek::THURSDAY
));
// SEARCH PLUGIN ---------------------------------------------------------------
// Autocomplete
$GLOBALS['autocomplete'] = array(
    'SolrAsyncUrl' => 'http://trunk.clubcupon.int.cmd.com.ar:8080/solr/deals/select/',
    'SolrAsyncUrlBridge' => '/search/search_bridges/makesearch',
    'FacetField' => 'deal_name_autocomplete_terms',
    'Items' => 10
);

class ConstSolrSearch {

    // Php Solr Client
    const SLRPC_PATH = '/libs/SolrPhpClient/Apache/Solr/Service.php';
    const HOST = 'trunk.clubcupon.int.cmd.com.ar';
    const PORT = 8080;
    const PATH = '/solr/deals/';
    const IMPORT_START_PATH = "/solr/deals/dataimport?command=full-import";
    const IMPORT_STATUS_PATH = "/solr/deals/dataimport?command=status";
    // Query
    const LIMIT = 9;

}

// END SEARCH PLUGIN -----------------------------------------------------------
Configure::write('web.subscriptionMail.enable', false);
