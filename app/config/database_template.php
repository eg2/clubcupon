<?php

class DATABASE_CONFIG {

    const PREPRO = 'v307ucert.int.cmd.com.ar';
    const DESA = 'v241udesa.int.cmd.com.ar';
    const LOCAL = '127.0.0.1';
    const TRUNK = 'trunk_clubcupon';
    const MAINTENANCE = 'trunk_clubcupon';
    const BRANCH = 'branch_clubcupon';
    const DB_TEST = 'test_clubcupon';
    const USER_LOCAL = 'root';
    const PASSWORD_LOCAL = 'matilda123';
    const USER_DESA = 'trunk';
    const PASSWORD_DESA = 'trunk123';
    const USER_PREPRO = 'consulta';
    const PASSWORD_PREPRO = 'c0nsult4';
    /*    LOCAL CONFIG */
    //    const HOST = self::LOCAL;
    //    const USER = self::USER_LOCAL;
    //    const PASSWORD = self::PASSWORD_LOCAL;
    //    const DATABASE = self::TRUNK;
    /*    DESA CONFIG */
    //    const HOST = self::DESA;
    //    const USER = self::USER_DESA;
    //    const PASSWORD = self::PASSWORD_DESA;
    //    //const DATABASE = 'clubcupontrunk';
    //    const DATABASE = 'clubcupon';
    /*    PREPRO CONFIG */
    const HOST = self::PREPRO;
    const USER = self::USER_PREPRO;
    const PASSWORD = self::PASSWORD_PREPRO;
    // const DATABASE = 'clubcupontrunk';
    const DATABASE = 'clubcupon';

    // if there is no master/slave, set the values same to both
    var $slave = array(
        'driver' => 'mysql_ex', // for syntax highlighting and logging of sql. See /app/models/datasources/dbo/dbo_mysql_ex.php
        'persistent' => false,
        'host' => self::HOST, // slave host
        'login' => self::USER,
        'password' => self::PASSWORD,
        'database' => self::DATABASE,
        'encoding' => 'utf8'
    );
    var $master = array(
        'driver' => 'mysql_ex',
        'persistent' => false,
        'host' => self::HOST, // slave host
        'login' => self::USER,
        'password' => self::PASSWORD,
        'database' => self::DATABASE,
        'encoding' => 'utf8'
    );
    var $emt_config = array(
        'driver' => 'mysql_ex',
        'persistent' => false,
        'host' => self::HOST,
        'login' => self::USER,
        'password' => self::PASSWORD,
        'database' => 'emtdb',
        'prefix' => '',
    );

    // if there is no master/slave, set the values same to both
    //    var $slave = array(
    //        'driver' => 'mysql_ex', // for syntax highlighting and logging of sql. See /app/models/datasources/dbo/dbo_mysql_ex.php
    //        'persistent' => false,
    //        'host' => 'v241udesa.int.cmd.com.ar', // slave host
    //        'login' => 'trunk',
    //        'password' => 'trunk123',
    //        'database' => 'clubcupontrunk',
    //       // 'database' => 'clubcupon',
    // 'encoding' => 'utf8'
    //    );
    //    var $master = array(
    //        'driver' => 'mysql_ex',
    //        'persistent' => false,
    //        'host' => 'v241udesa.int.cmd.com.ar', // master host
    //        'login' => 'trunk',
    //        'password' => 'trunk123',
    //        'database' => 'clubcupontrunk',
    //        //'database' => 'clubcupon',
    // 'encoding' => 'utf8'
    //    );
    //
 //    var $test = array(
    //      'driver' => 'mysql_ex', // for syntax highlighting and logging of sql. See /app/models/datasources/dbo/dbo_mysql_ex.php
    //      'persistent' => false,
    //      'host' => self::LOCAL, // slave host
    //      'login' => self::USER_LOCAL,
    //      'password' => self::PASSWORD_LOCAL,
    //      'database' => self::DB_TEST,
    //      'encoding' => 'utf8'
    //    );
    //
 /*   var $test = array(
      'driver' => 'mysql_ex', // for syntax highlighting and logging of sql. See /app/models/datasources/dbo/dbo_mysql_ex.php
      'persistent' => false,
      'host' => 'localhost', // slave host
      'login' => 'root',
      'password' => '123456',
      // 'database' => 'clubcupontrunk',
      'database' => 'local_trunk',
      'encoding' => 'utf8'
      ); */
    function __construct() {
        $this->default = $this->slave;
    }

}
