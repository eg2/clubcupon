<?php

/**
 * Custom configurations
 */
// site actions that needs random attack protection...
$config['site']['_hashSecuredActions'] = array(
    'edit',
    'delete',
    'update',
    'refer',
    'unsubscribe',
    'barcode',
    'update_deal',
    'resend',
    'my_account',
    'view_gift_card'
);
$config['site']['domain'] = 'grouponpro';
$config['image']['file'] = array(
    'allowedMime' => array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    ),
    'allowedExt' => array(
        'jpg',
        'jpeg',
        'gif',
        'png'
    ),
    'allowedSize' => '5',
    'allowedSizeUnits' => 'MB',
    'allowEmpty' => false
);
$config['avatar']['file'] = array(
    'allowedMime' => array(
        'image/jpeg',
        'image/jpg',
        'image/gif',
        'image/png'
    ),
    'allowedExt' => array(
        'jpg',
        'jpeg',
        'gif',
        'png'
    ),
    'allowedSize' => '5',
    'allowedSizeUnits' => 'MB',
    'allowEmpty' => true
);

class ConstPaymentGateways {

    const PayPal = 1;
    const TwoCheckout = 2;
    const GlobalGateway = 3;
    const Decidir = 4;

}

class ConstRisk {

    const Low = 1;
    const Medium = 2;
    const High = 3;

    function getFriendly($key) {
        $translations = array(
            self::Low => 'Bajo',
            self::Medium => 'Medio',
            self::High => 'Alto',
        );
        return array_key_exists($key, $translations) ? $translations[$key] : 'N/A';
    }

}

class ConstGenders {

    const m = 'Masculino';
    const f = 'Femenino';

    function getGenders() {
        return array(
            'm' => self::m,
            'f' => self::f
        );
    }

}

class ConstPaymentStatus {

    const Success = 'Success';
    const Refund = 'Refund';
    const Cancel = 'Cancel';
    const Pending = 'Pending';

}

class ConstDealStatus {

    const Upcoming = 1;
    const Open = 2;
    const Canceled = 3;
    const Tipped = 5;
    const Closed = 6;
    const Refunded = 7;
    const PaidToCompany = 8;
    const PendingApproval = 9;
    const Rejected = 10;
    const Draft = 11;
    const Delete = 12; //Not available in table. only for coding purpose
    const TotallyPaid = 13; //Not available in table. only for coding purpose
    const Finalized = 14; //Not available in table. only for coding purpose
    const CicleEnded = 15; //Not available in table. only for coding purpose

    function getFriendly($key) {
        $translations = array(
            self::Upcoming => 'Próximas',
            self::Open => 'Abierta',
            self::Canceled => 'Cancelada',
            self::Tipped => 'En Marcha',
            self::Closed => 'Cerrada',
            self::Refunded => 'Reembolsada',
            self::PaidToCompany => 'Pagadas a la Empresa',
            self::PendingApproval => 'Pendiente de Aprobación',
            self::Rejected => 'Rechazada',
            self::Draft => 'Borrador',
            self::Delete => 'Eliminada',
            self::TotallyPaid => 'Totalmente Pagada',
            self::Finalized => 'Finalizada',
            self::CicleEnded => 'Fuera de Ciclo',
        );
        return $translations[$key];
    }

    function getPlain($key) {
        $translations = array(
            self::Upcoming => 'upcoming',
            self::Open => 'open',
            self::Canceled => 'canceled',
            self::Tipped => 'tipped',
            self::Closed => 'closed',
            self::Refunded => 'refunded',
            self::PaidToCompany => 'paid',
            self::PendingApproval => 'pending',
            self::Rejected => 'rejected',
            self::Draft => 'draft',
            self::Delete => 'deleted',
            self::TotallyPaid => 'totally_paid',
            self::Finalized => 'finalized',
            self::CicleEnded => 'ended',
        );
        return $translations[$key];
    }

}

class ConstUserIds {

    const Admin = 1;

}

class ConstAttachment {

    const UserAvatar = 1;
    const Deal = 0;

}

class ConstFriendRequestStatus {

    const Pending = 1;
    const Approved = 2;
    const Reject = 3;

}

class ConstMoreAction {

    const Inactive = 1;
    const Active = 2;
    const Delete = 3;
    const OpenID = 4;
    const Export = 5;
    const EnableCompanyProfile = 6;
    const Used = 7;
    const DisableCompanyProfile = 8;
    const Online = 9;
    const Offline = 10;
    const FaceBook = 11;
    const DeductAmountFromWallet = 12;
    const NotUsed = 13;
    const UnSubscripe = 14;
    const ResendEmail = 15;

}

class ConstUserFriendStatus {

    const Pending = 1;
    const Approved = 2;
    const Rejected = 3;

}

// setting for one way and two way friendships
class ConstUserFriendType {

    const IsTwoWay = true;

}

// Banned ips types
class ConstBannedTypes {

    const SingleIPOrHostName = 1;
    const IPRange = 2;
    const RefererBlock = 3;

}

// Banned ips durations
class ConstBannedDurations {

    const Permanent = 1;
    const Days = 2;
    const Weeks = 3;

}

class ConstReferralRule {

    const Referral = 1;
    const Referred = 2;
    const BuyedFirst = 3;
    const BuyedSecond = 4;

}

class ConstWithdrawalStatus {

    const Pending = 1;
    const Approved = 2;
    const Rejected = 3;
    const Failed = 4;
    const Success = 5;

}

class ConstMyGiftStatus {

    const Pending = 'Pending';
    const Success = 'Not Yet Redeemed';
    const ToCredit = 'Redeemed By Recipient';

}

class ConstTransactionTypes {

    const AddedToWallet = 1;
    const BuyDeal = 2;
    const DealGift = 3;
    const GiftSent = 4;
    const GiftReceived = 5;
    const ReferralAmount = 6;
    const PaidDealAmountToCompany = 7;
    const UserCashWithdrawalAmount = 8;
    const DealBoughtRefund = 9;
    const DealGiftRefund = 10;
    const ReferralAmountPaid = 11;
    const ReceivedDealPurchasedAmount = 12;
    const AcceptCashWithdrawRequest = 13;

}

// Setting for privacy setting
class ConstPrivacySetting {

    const EveryOne = 1;
    const Users = 2;
    const Friends = 3;
    const Nobody = 4;

}

class ConstCities {

    const CiudadDeBuenosAires = 42550;

}

class ConstPaymentTypes {

    const Wallet = 5000;

}

class ConstPointStatuses {

    const Available = 0;
    const Expired = 1;
    const Cancelled = 2;

}

class ConstActionIds {

    const DebitCredit = 1;
    const Seniority = 2;
    const Buy = 3;
    const Referral = 4;

}

$config['cdn']['images'] = null; // 'http://images.localhost/';
$config['cdn']['css'] = null; // 'http://static.localhost/';
date_default_timezone_set('America/Argentina/Buenos_Aires');
/*

  Configure::write('Config.language', 'spa');
 */
setlocale("LC_ALL", "es_AR.UTF-8");
setlocale('LC_NUMERIC', 'en_US');
/*
 * * to do move to settings table
 */
$config['sitemap']['models'] = array(
    'Deal',
);
$config['site']['is_admin_settings_enabled'] = true;
$config['site']['admin_demomode_updation_not_allowed_array'] = array(
    'email_templates/admin_edit',
    'settings/admin_edit',
    'countries/admin_edit',
    'cities/admin_delete',
    'countries/admin_delete',
);
$config['site']['email']['smtp_options'] = array(
    'port' => '465',
    'timeout' => '30',
    'host' => 'ssl://smtp.gmail.com',
    'username' => 'clubcupon@altodot.com',
    'password' => 'alto1234'
);
$config['site']['email']['delivery'] = 'smtp';
