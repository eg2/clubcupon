<?php

/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7820 $
 * @modifiedby    $LastChangedBy: renan.saddam $
 * @lastmodified  $Date: 2008-11-03 23:57:56 +0530 (Mon, 03 Nov 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
Router::parseExtensions('rss', 'csv', 'json', 'txt', 'pdf', 'kml', 'xml', 'mobile');
// REST support controllers
Router::mapResources(array(
    'deals'
));
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
$controllers = Cache::read('controllers_list', 'default');
if ($controllers === false) {
    $controllers = Configure::listObjects('controller');
    foreach ($controllers as & $value) {
        $value = Inflector::underscore($value);
    } //$controllers as &$value
    array_push($controllers, 'subscribe', 'company', 'company_candidates', 'deal', 'page', 'user', 'admin', 'deal_user', 'contactus', 'sitemap', 'robots', 'sitemap.xml', 'robots.txt', 'first', 'google9ce53ccf7b48e821.html', 'now.now_deals', 'order.order_processings');
    $controllers = implode('|', $controllers);
    Cache::write('controllers_list', $controllers);
} //$controllers === false
if (Configure::read('orderPlugin.enabled')) {
    Router::connect(':city/deals/buy/:deal_id/*', array(
        'plugin' => 'order',
        'controller' => 'order_processings',
        'action' => 'buy'
            ), array(
        'pass' => array(
            'deal_id'
        )
    ));
} //Configure::read('orderPlugin.enabled')
Router::connect('comercio/:id_compania', array(
    'plugin' => 'api',
    'controller' => 'api_deals',
    'action' => 'company_deals'
        ), array(
    'pass' => array(
        'id_compania'
    )
));
// Web Plugin 1 --------------------------------------
/* Router::connect(
  '/debmail/*',
  array('controller' => 'deals', 'action' => 'debugmail'),
  array()
  ); */
Router::connect('/Ofertas', array(
    'plugin' => 'web',
    'controller' => 'web_cities',
    'action' => 'home'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/nuestros-beneficios/*', array(
    'plugin' => 'web',
    'controller' => 'web_exclusive_logins',
    'action' => 'login',
    1
        ), array());
Router::connect('/web/login/*', array(
    'plugin' => 'web',
    'controller' => 'web_exclusive_logins',
    'action' => 'login'
        ), array());
Router::connect('/:city_slug/errors/error404/*', array(
    'plugin' => 'web',
    'controller' => 'web_errors',
    'action' => 'error404'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/users/login/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'login'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/admin/users/login/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'login'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/users/logout/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'logout'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/userLogout/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'logout'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/users/register/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'register'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/users/registernow', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'registernow'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/now/now_registers', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'registernow'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/users/forgot_password/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'forgotPassword'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('web/users/reset/:user_id/:hash/*', array(
    'plugin' => 'web',
    'controller' => 'web_users',
    'action' => 'reset'
        ), array(
    'user_id' => '[0-9\-]+',
    'hash' => '[a-zA-Z0-9\-]+'
));
Router::connect('/:city/pages/*', array(
    'controller' => 'pages',
    'action' => 'view'
        ), array());
Router::connect('/:city_slug/todos/*', array(
    'plugin' => 'web',
    'controller' => 'web_cities',
    'action' => 'todos'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/deal/:deal_Slug/*', array(
    'plugin' => 'web',
    'controller' => 'web_deals',
    'action' => 'deal'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+',
    'cupon_Id' => '[0-9\-]+'
));
Router::connect('/web/:city_slug/deal/:deal_Slug/*', array(
    'plugin' => 'web',
    'controller' => 'web_deals',
    'action' => 'deal'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+',
    'cupon_Id' => '[0-9\-]+'
));
// End Web Plugin --------------------------------------
// Search Plugin --------------------------------------
Router::connect('/search/:controller/:action/*', array(
    'plugin' => 'search'
        ), array());
// End Search Plugin --------------------------------------
// Promotion Plugin --------------------------------------
Router::connect('/promotion/:controller/:action/*', array(
    'plugin' => 'promotion'
        ), array());
Router::connect('/navidad/2014/', array(
    'plugin' => 'promotion',
    'controller' => 'promotions_christmas',
    'action' => 'year2014'
        ), array());
// End Promotion Plugin --------------------------------------
Router::connect('img_api/:size/*', array(
    'plugin' => 'api',
    'controller' => 'api_deals',
    'action' => 'img'
        ), array(
    'size' => '(?:[a-zA-Z0-9_]*)*'
));
Router::connect('api/img/:size/*', array(
    'plugin' => 'api',
    'controller' => 'api_deals',
    'action' => 'img'
        ), array(
    'size' => '(?:[a-zA-Z0-9_]*)*'
));
Router::connect('/api/:controller/:action/*', array(
    'plugin' => 'api',
    'controller' => 'api_deals',
    'action' => 'deals'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/order/:controller/:action/*', array(
    'plugin' => 'order',
    'controller' => 'order_processings',
    'action' => 'buy'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/integration/:controller/:action/*', array(
    'plugin' => 'integration',
    'controller' => 'integration_product_inventories',
    'action' => 'update_list_products'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/integration/:controller/:action/*', array(
    'plugin' => 'integration',
    'controller' => 'integration_product_inventories',
    'action' => 'save_or_update_product'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/product/:controller/:action/*', array(
    'plugin' => 'product',
    'controller' => 'product_inventory_tests',
    'action' => 'test'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/mobile/:controller/:action/*', array(
    'plugin' => 'mobile',
    'controller' => 'mobile_deals',
    'action' => 'deals'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/now/', array(
    'plugin' => 'now',
    'controller' => 'now_deals',
    'action' => 'index'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/now/:controller/:action/*', array(
    'plugin' => 'now',
    'controller' => 'now_deals',
    'action' => 'index'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/ya/', array(
    'plugin' => 'now',
    'controller' => 'now_deals',
    'action' => 'index'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/ya/:controller/:action/*', array(
    'plugin' => 'now',
    'controller' => 'now_deals',
    'action' => 'index'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/virginia_slims/:controller/:action/:id', array(
    'plugin' => '',
    'controller' => 'deals',
    'action' => 'buy'
));

Router::connect('/efg/:controller/:action/*', array(
    'plugin' => 'efg',
    'controller' => 'emails',
    'action' => 'newsletter'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));

Router::connect('/beacon/:controller/:action/*', array(
    'plugin' => 'beacon',
    'controller' => 'beacon_device_deals',
    
        ), array(
    
));

Router::connect('/payment/:controller/:action/*', array(
    'plugin' => 'payment',
    'controller' => 'payment_payments',
    'action' => 'updatePayment'
        ), array(
    'city' => '[a-zA-Z0-9\-]+'
));
Router::connect('/tags/:controller/:action/*', array(
    'plugin' => 'tags',
    'controller' => 'tags',
    'action' => 'index'
));
Router::connect('/', array(
    'controller' => 'deals',
    'action' => 'index'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/fibertel/:action', array(
    'controller' => 'fibertels'
        ), array());
Router::connect('/xmldeals/:action', array(
    'controller' => 'xmldeals'
        ), array());
Router::connect('/xmldeals/rss/:city_slug', array(
    'controller' => 'xmldeals'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/xmldeals/rss/:city_slug/recent', array(
    'controller' => 'xmldeals'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+',
    'options' => 'recent'
));
// Web Plugin 2 ------------------------------------------
Router::connect('/:city_slug/', array(
    'plugin' => 'web',
    'controller' => 'web_cities',
    'action' => 'home'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city_slug/showpopup/:ForcePopup', array(
    'plugin' => 'web',
    'controller' => 'web_cities',
    'action' => 'home'
));
// ----------------------------POPUP Web Plugin
Router::connect('/:city_slug/popup', array(
    'plugin' => 'web',
    'controller' => 'web_firsts',
    'action' => 'homepopup'
));
Router::connect('/:city_slug/ads', array(
    'plugin' => 'web',
    'controller' => 'web_firsts',
    'action' => 'ads'
));
Router::connect('/:city_slug/addsubscription/*', array(
    'plugin' => 'web',
    'controller' => 'web_subscriptions',
    'action' => 'addsubscriptions'
));
// ----------------------------
Router::connect('/:city_slug/deals/', array(
    'plugin' => 'web',
    'controller' => 'web_cities',
    'action' => 'home'
        ), array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
// End Web Plugin 2 --------------------------------------
Router::connect('/:city/users/twitter/login/', array(
    'controller' => 'users',
    'action' => 'login',
    'type' => 'twitter'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
// usado para los previews de las paginas...
Router::connect('/:city/pages/preview', array(
    'controller' => 'pages',
    'action' => 'preview'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/pages/*', array(
    'controller' => 'pages',
    'action' => 'view'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/pages/*', array(
    'controller' => 'pages',
    'action' => 'view'
));
Router::connect('/cel/landing', array(
    'controller' => 'pages',
    'action' => 'landingmobile'
));
Router::connect('/:city/company/user/register/*', array(
    'controller' => 'users',
    'action' => 'company_register'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/contactus/', array(
    'controller' => 'contacts',
    'action' => 'add'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/deals/recent', array(
    'controller' => 'deals',
    'action' => 'index',
    'type' => 'recent'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/deals/company/:company', array(
    'controller' => 'deals',
    'action' => 'index'
        ), array(
    'company' => '[a-zA-Z0-9\-]+',
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/city_suggestions/new', array(
    'controller' => 'city_suggestions',
    'action' => 'add'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/subscribe', array(
    'controller' => 'subscriptions',
    'action' => 'add'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/subscribeadd', array(
    'controller' => 'subscriptions',
    'action' => 'add'
        ), array());
Router::connect('/:city/' . Configure::read('Routing.admin'), array(
    'controller' => 'users',
    'action' => 'stats',
    'prefix' => Configure::read('Routing.admin'),
    'admin' => 1
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/robots', array(
    'controller' => 'devs',
    'action' => 'robots',
    'ext' => 'txt'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/sitemap', array(
    'controller' => 'xmldeals',
    'action' => 'sitemap',
    'ext' => 'xml'
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/img/:size/*', array(
    'controller' => 'images',
    'action' => 'view'
        ), array(
    'size' => '(?:[a-zA-Z0-9_]*)*'
));
Router::connect('/files/*', array(
    'controller' => 'images',
    'action' => 'view',
    'size' => 'original'
));
Router::connect('/img/*', array(
    'controller' => 'images',
    'action' => 'view',
    'size' => 'original'
));
Router::connect('/' . Configure::read('Routing.admin') . '/:controller/:action/*', array(
    'admin' => true
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/' . Configure::read('Routing.admin') . '/:controller/:action/*', array(
    'admin' => true
        ), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/:controller/:action/*', array(), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
