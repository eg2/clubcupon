<?php

$config['LoggerTrace']['enabled'] = 0;

$config['Efg']['LoggerTrace']['enabled'] = 0;

$config['Event']['LoggerTrace']['enabled'] = 1;

$config['EmailFilesGenerator']['Pmta']['forceSubscriptionQueryCities'] = false;

$config['EmailFilesGenerator']['Pmta']['forceSubscriptionQueryGroups'] = false;

$config['EmailFilesGenerator']['Pmta']['enableMinifyHtml'] = false;

$config['EmailFilesGenerator']['Pmta']['enableEmailMultiPart'] = false;

$config['Event']['EventImportEmtEventServiceComponent']['LoggerTrace']['enabled'] = 0;

// Cantidad de destinatarios a incluir en cada archivo para PowerMta

$config['EmailFilesGenerator']['Pmta']['PageSize'] = 5000;

// Nombre de la clave que identifica a un id de usuario dentro del archivo

$config['EmailFilesGenerator']['Pmta']['SubscriberKey'] = "Subscriber";

// Nombre de la clave que identifica a un codigo de segemnto dentro del archivo

$config['EmailFilesGenerator']['Pmta']['FrecuencyEventClassCodeKey'] = "SegmentCode";

// Nombre de la clave que identifica a un hash de usuario dentro del archivo de pmta

$config['EmailFilesGenerator']['Pmta']['SubscriberHashKey'] = "SubscriberHash";

// Caracteres especiales que se coloan al ppio del un tag a ser reemplazado en un archivo de Pmta

$config['EmailFilesGenerator']['Pmta']['BeginTag'] = "[";

// Caracteres especiales que se coloan al fin del un tag a ser reemplazado en un archivo de Pmta

$config['EmailFilesGenerator']['Pmta']['EndTag'] = "]";

// Direccion donde el proceso copia los archivos generados para powermta

//$config['EmailFilesGenerator']['Directory']['Output'] = "/var/www/clubcupon/current/app/tmp/efg/";
$config['EmailFilesGenerator']['Directory']['Output'] = "/var/www/trunk/app/tmp/efg/";
// Mail desde donde se enviaran los mails.

$config['EmailFilesGenerator']['From']['Mail'] = "info@clubcupon.com.ar";

// Valor de ClubCupon en EMT

$config['EmailFilesGenerator']['Emt']['Key'] = "cc";

// Url completa donde se encuentra la imagen fake de EMT para trackear aperturas de mails.

// Los tags deben seran reemplazados por los correspondientes valores de cada suscripto

// por lo que solo se debe modificar el DOMINIO donde se encuentra EMT

// $config['EmailFilesGenerator']['Emt']['FakeImageFullUrl'] = "http://emt.cmd.com.ar/hit?site=[site]&campaign=[campaign]&date=[date]&user=[user]&event=m";

$config['EmailFilesGenerator']['Emt']['FakeImageFullUrl'] = "http://emt.cmd.com.ar/hit?site=[site]&campaign=[campaign]&date=[date]&user=[user]&event=m&segment=[segment]";

// Dominio del mail usado para los rebotes

$config['EmailFilesGenerator']['Bounce']['Domain'] = "clubcupon.com.ar";

// Habilita la ejecucion del comando de sincronizacion Rsync ("1"->habilitado; "0"->Deshabilitado)

$config['EmailFilesGenerator']['Rsync']['Enabled'] = "0";

// Comando rsync a ejecutar una vez creado el archivo de mails. Modificar el comando todo lo que sea necesario

// salvo por la etiqueta [ORIGEN] que será reemplazada con la ruta del archivo actual.

$config['EmailFilesGenerator']['Rsync']['Command'] = "/usr/bin/rsync -avz [ORIGEN]  rsync://v162uprod.int.cmd.com.ar:/pickup/";

// Destinatarios del mail que se envia ante un fallo en la syncronizacion.

$config['EmailFilesGenerator']['Email']['To'] = "soporte@cmd.com.ar";

// Cuenta que envia los mails

$config['EmailFilesGenerator']['Email']['From'] = "soporte@cmd.com.ar";

// Activa o no el envio de mails por smtp, sino, dependerá de la configuracion del server

$config['EmailFilesGenerator']['Email']['IsSmtp'] = "1";

// Usuario de la cuenta SMTP

$config['EmailFilesGenerator']['Email']['SmtpAccount'] = "ndemarchi@cmd.com.ar";

// Password de la cuenta SMTP

$config['EmailFilesGenerator']['Email']['SmtpPassword'] = "";

// Host la cuenta SMTP

$config['EmailFilesGenerator']['Email']['SmtpHost'] = "127.0.0.1";

// Comando Rsync a ejecutar para copiar los archivos del directorio bad mails a la carpeta temporal

$config['EmailFilesGenerator']['BadMail']['Command'] = "/usr/bin/rsync --list-only rsync://v162uprod.int.cmd.com.ar:/badmail/ | grep txt";

//

$config['EmailFilesGenerator']['Pmta']['vmta'] = "vMTA-Transacc1";

//

$config['EmailFilesGenerator']['xack_off']['enabled'] = 1;





#impl6044

$config['EmailFilesGenerator']['Pmta']['vmta_for_no_buyer'] = "vMTA-Transacc1";

$config['EmailFilesGenerator']['Pmta']['vmta_for_buyer'] = "vMTA-Transacc1";



#impol6437

$config['EmailFilesGenerator']['LogEnable'] = 1;

$config['EmailFilesGenerator']['LogVerbose'] = 0;

$config['EmailFilesGenerator']['MinimumNumberOfPurchases'] = 1;

$config['EmailFilesGenerator']['ArrayHostForRequestImages'] = array(
                          'prepro0'=>'http://prepro.clubcupon.com.ar'
                );


