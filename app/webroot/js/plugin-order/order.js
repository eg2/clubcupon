$( document ).ready(function() {
    //disabled tab inactive 
    $(document).find('div.active').siblings().find('[name*=data]').attr('disabled', 'disabled');
    
    $(".message").click(function() {
        $(this).slideToggle(400);
    });
    $('#giftCboxSave').click(function() {
    	
        if(validateGiftForm()){
        	toogleIsGiftValue(1);
        	return true;
        }
        return false;
    });
    $('#cancelBuyGift').click(function(){
        toogleIsGiftValue(0);
    });
    $('.customGift').click(function(){
        tabActive = $(this).attr('href');
        tabToDisabled = $(tabActive).siblings();
        $(tabToDisabled).find('[name*=data]').attr('disabled', 'disabled');
        $(tabActive).find('[name*=data]').removeAttr('disabled');
    });
   
});
	function validateGiftForm(){
		
		var errors='';
		
		
		/* evaluo si se ha ingresado un nombre*/
		var to=$("input[name^='data[Deal][gift_options][to]']").val();
		if($.trim(to).length==0){
		errors+='<li>Debe ingresar el Nombre de su amigo.</li>';
		}
		
		/* evaluo que si se ha ingresado un dni este sea un numero entero*/
		var dni=$("input[name^='data[Deal][gift_options][dni]']").val();
		
		if($.trim(dni).length>0){
			if(!isInt(dni)){
				errors+='<li>Debe ingresar un Dni válido.</li>';	
			}
			
		}
		
	
		/* si el tab de "envio por mail" está activa evaluo que se haya ingresado un mail y que sea valido*/
		if($("#tab1").css('display')=='block'){
			var email=$("#DealEmail").val();
			if(!validateEmailFormat($.trim(email))){
				errors+='<li>Debe ingresar un mail válido.</li>';
			}else{
				/* verifico que los Emails ingresados coincidan*/
				var email2=$("#DealConfirmGiftEmail").val();
				if(!($.trim(email2)==$.trim(email))){
				errors+='<li>Los Mails ingresados deben ser iguales.</li>';
				}
			}
		}
	
		/* si el tab de "imprimir" está activa evaluo que se haya ingresado un mensaje*/
	
		if($("#tab2").css('display')=='block'){
			var message=$("#DealMessage2").val();
			if($.trim(message).length==0){
				errors+='<li>Debe ingresar el Mensaje del cupón.</li>';
			}
		}
	
		if(errors.length>0){
			$("#listOfErrors ul li").remove();//limpio errores pasados
		//	$("#listOfErrors ul").append('<li>lalalla</li><li>lelelele</li>');
			$("#listOfErrors ul").append(errors);
			$("#listOfErrors").css('display','block');
			return false;	
		}
	
		return true;
	
	}

	function isInt(value) {
	   return !isNaN(value) && parseInt(value) == value;
	}

	function validateEmailFormat(email){
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email)
	}

    function disablePaymentOptions(){
        $('.js-payment-type').each(function(i) {
            $(this).attr('disabled', 'disabled');
            $(this).removeAttr('checked');
        });
        hidePaymentOptionsAndTitlesForPinValidation();
    }
    
    function enablePaymentOptions(){
        $('.js-payment-type').each(function(i) {
            $(this).removeAttr('disabled');
        });
        showPaymentOptionsAndTitlesForPinValidation();
    }

    function hidePaymentOptionsAndTitlesForPinValidation(){
        togglePaymentOptionsAndTitles();
        $('.BUY_DEAL_DETAIL').css('margin-bottom', '0');
        $('.SELECT_PAYMENT_OPTION ').css('min-height', '80px');
        $('.SELECT_PAYMENT_OPTION .payment-group .bottom .subm').attr('value', 'Validar');
    }
    
    function showPaymentOptionsAndTitlesForPinValidation(){
        togglePaymentOptionsAndTitles();
        $('.BUY_DEAL_DETAIL').css('margin-bottom', '10px');
        $('.SELECT_PAYMENT_OPTION ').css('min-height', '200px');
        $('.SELECT_PAYMENT_OPTION .payment-group .bottom .subm').attr('value', 'Pagar');
    }

    function togglePaymentOptionsAndTitles() {
        $('.SELECT_PAYMENT_OPTION .navbar').toggle();
        $('.SELECT_PAYMENT_OPTION .payment-group h2').toggle();
        $('.SELECT_PAYMENT_OPTION .payment-group .payment-options').toggle();
        $('.SELECT_PAYMENT_OPTION .payment-group .bottom .pull-left').toggle();
    }
    function toogleIsGiftValue(isGift) {
        $('#DealIsGift').val(isGift);
    }