$(document).ready(function() {
    // Cuenta regresiva de una oferta
    $(function(){

        // contenedor destino
        var target = $('.horas-num');

        // Recuperamos la fecha de cierra
        var end_date = $(this).find('#deal_countdown_amout').html();

        // Formateamos a milisegundo
        ts = (new Date()).getTime() + end_date * 1000; // diferencia

        // iteramos actualizando el mensaje, e imprimimos en pantalla
        if ($('.horas-num').length > 0){
			$('.horas-num').countdown({
				timestamp	: ts,
				callback	: function(days, hours, minutes, seconds){
					var message = "";
					
					if (days > 0){
						message += '<span class="countDownFull">' + checkDateFormat(days) + 'd&nbsp;&nbsp;</span>';
						message += '<span class="countDownFull">' + checkDateFormat(hours) + 'h</span>';
					}else {
						message += '<span>' + checkDateFormat(hours) + ':</span>';
						message += '<span>' + checkDateFormat(minutes) + ':</span>';
						message += '<span>' + checkDateFormat(seconds) + '</span>';
						$('.clock').show();
					}
					target.html(message);
					
					$('.horas-num').show();
				}
			});
        }
    });
});
  
/*
 * Formatea la hora a doble dígito
 */
var checkDateFormat = function(stamp){
    if(stamp<10){
        tmp = stamp;
        stamp = '0' + tmp;
    }
    return stamp;
};

/*
 * Slider de ads
 */
function slider(container,posicion){
	 var pos = posicion;
	 
	 //cantidad de elementos visibles a la vez
	 var visibleElem = 4;		
	 
	 list = ( typeof list != 'undefined' && list instanceof Array ) ? list : [];
	 elem = ( typeof elem != 'undefined' && elem instanceof Array ) ? elem : [];
	 elemScnd = ( typeof elemScnd != 'undefined' && elemScnd instanceof Array ) ? elemScnd : [];
	 elemWidth = ( typeof elemWidth != 'undefined' && elemWidth instanceof Array ) ? elemWidth : [];
	 elemMarginL = ( typeof elemMarginL != 'undefined' && elemMarginL instanceof Array ) ? elemMarginL : [];
	 despl = ( typeof despl != 'undefined' && despl instanceof Array ) ? despl : [];
	 desplIndex = ( typeof desplIndex != 'undefined' && desplIndex instanceof Array ) ? desplIndex : [];
	 desplCantItems = ( typeof desplCantItems != 'undefined' && desplCantItems instanceof Array ) ? desplCantItems : [];
	 maxElem = ( typeof maxElem != 'undefined' && maxElem instanceof Array ) ? maxElem : [];
	 sliderIndex = ( typeof sliderIndex != 'undefined' && sliderIndex instanceof Array ) ? sliderIndex : [];
	 oContainer = ( typeof oContainer != 'undefined' && oContainer instanceof Array ) ? oContainer : [];
	 
	 oContainer[pos] = container;
	 
	 list[pos] = oContainer[pos].find('ul');
	 elem[pos] = list[pos].children('li');
	 elemScnd[pos] = list[pos].children('li:nth-child(2)');
	 elemWidth[pos] = parseInt(elemScnd[pos].css('width'));
	 elemMarginL[pos] = parseInt(elemScnd[pos].css('margin-left'));
	 maxElem[pos] = elem[pos].length;
	 despl[pos] = elemWidth[pos] + elemMarginL[pos];
	 desplIndex[pos] = maxElem[pos] - visibleElem;
	
	 if (desplIndex[pos] < 0){
		 oContainer[pos].find('.next').hide();
	 }
	 sliderIndex[pos] = 0;
	
	 oContainer[pos].find('.next').click(function(){
		var contenedor = $(this).parent().parent(); 
		var id = contenedor.attr('id');
		id = id.substring(id.length-1,id.length);
		
				
		if ((desplIndex[id] - sliderIndex[id]) > visibleElem){
			desplCantItems[id] = visibleElem;
		}else {
			desplCantItems[id] = desplIndex[id] - sliderIndex[id];
		}
		
		
		if (sliderIndex[id] >= desplIndex[id]){
//				$(this).toggle();
			$(this).parent().siblings('.content').children().animate({left: '0'});
			sliderIndex[id] = 0;
			
		}
		if (sliderIndex[id] < desplIndex[id]){
			$(this).parent().siblings('.content').children().animate({left: '-='+despl[id]*desplCantItems[id]});
			
			$(this).siblings('.prev').show();
			
			sliderIndex[id]+=desplCantItems[id];
		}
		
		console.log(desplIndex[id]);
		console.log(desplCantItems[id]);
		console.log(sliderIndex[id]);
	});
	
	 oContainer[pos].find('.prev').click(function(){
		var contenedor = $(this).parent().parent(); 
		var id = contenedor.attr('id');
		id = id.substring(id.length-1,id.length);

		if (sliderIndex[id] > visibleElem){
			desplCantItems[id] = visibleElem;
		}else {
			desplCantItems[id] = sliderIndex[id];
		}
		
		if (sliderIndex[id] <= 0){
//				$(this).toggle();
			$(this).parent().siblings('.content').children().animate({left: '-='+despl[id]*desplIndex[id]});	
			sliderIndex[id] = desplIndex[id];
		}
		if (sliderIndex[id] > 0){
			$(this).parent().siblings('.content').children().animate({left: '+='+despl[id]*desplCantItems[id]});
			
			$(this).siblings('.next').show();
			
			sliderIndex[id]-=desplCantItems[id];
		}			
		console.log(desplIndex[id]);
		console.log(desplCantItems[id]);
		console.log(sliderIndex[id]);		
	});	
}


/* popups legacy */
/* configuration registry */
var _configuration = {
  popupsDefault: {
    width:'800px',
    height:'600px',
    frase:'Cerrar'
  }
};

function returnSettingsForMobileAd() {

    var ua = navigator.userAgent;
    var blackberry1 = /blackberry/i.test(ua);
    var blackberry2 = /bb/i.test(ua);
    var blackberry3 = /playbook/i.test(ua);
    var blackberry = blackberry1 || blackberry2 || blackberry3;
   // var iphone = /iphone/i.test(ua);
    var android = /android/i.test(ua);
  //  var ipad = /ipad/i.test(ua);

    var settings = [];
    
    if(blackberry) {
        settings['name'] = 'blackberry';
        settings['url']  = 'http://appworld.blackberry.com/webstore/content/24574893/?lang=es&countrycode=AR';
    } else /*if(iphone) { 
        settings['name'] = 'iPhone';
        settings['url']  = 'https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8';
    } else*/ if(android) {
        settings['name'] = 'Android';
        settings['url']  = 'https://play.google.com/store/apps/details?id=com.cmd.clubcupon';
    } else /*if(ipad) {
        settings['name'] = 'iPad';
        settings['url']  = 'https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8';
    } else*/ {
        settings = false;
    }
    
    return settings;

}

function renderMobileAd(settingsForMobileAd) {
    
    var imgsDir = '/img/mobile/ad/'
    $('#mobileAd .logoIcon img').attr('src', imgsDir+settingsForMobileAd['name']+'.png')
    $('#mobileAd .text a').attr('href', settingsForMobileAd['url'])
    $('#mobileAd .text a').html(settingsForMobileAd['name'])
    $('#mobileAd .downloadLink a').attr('href', settingsForMobileAd['url'])
    $('#mobileAd').toggle();
    
}

var isMobile = {
	    	Android: function() {
	        	return navigator.userAgent.match(/Android/i);
	    	},
	    	BlackBerry: function() {
	        	return navigator.userAgent.match(/BlackBerry/i);
	    	},
	    	iOS: function() {
	        	return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    	},
	    	Opera: function() {
	        	return navigator.userAgent.match(/Opera Mini/i);
	    	},
	    	Windows: function() {
	        	return navigator.userAgent.match(/IEMobile/i);
	    	},
	    	any: function() {
	        	return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    	}
		};

var popupWelcome = function(url, width, height, onClose) {
	
 	if(!isMobile.any()){

	  if(typeof(width) == "undefined" || width == null) {
	    width = _configuration.popupsDefault.width;
	  }

	  if(typeof(height) == "undefined" || height == null) {
	    height = _configuration.popupsDefault.height;
	  }

	  var configColorbox = {
	    escKey: false,
	    overlayClose: false,
	    close: 'Ya estoy suscripto',
	    href: url,
	    iframe: true,
	    width: width,
	    height: height,
	    onClosed: function () {$.cookie('fullscreenClose', 1);}
	  };

	  $.fn.colorbox(configColorbox);
 	}
	return false;
};

$(document).ready(function() {
	// captcha reload function
	if ($('.js-captcha-reload').length > 0){
		$('.js-captcha-reload').livequery('click', function() {
			captcha_img_src = $(this).parents('.js-captcha-container').find('.captcha-img').attr('src');
		    captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
		    $(this).parents('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
		    if($('.js-captcha-input')) {
		    	$('.js-captcha-input').val('');
		    }
		    return false;
		});	
	}
	
	//pages sidebar_wide limit deal list
	var elem = $('#pages .bloque-promo');
	var elemCount = elem.length;
	var elemHeight =  elem.outerHeight(true);
	var contentHeight = $('.grid_12').height();
	var pagesHeight;

	for (i=1;i <= elemCount;i++){
		pagesHeight = elemHeight * i;
		
		if (pagesHeight > contentHeight) {
			$('#pages').css('visibility','visible');
			$('#pages').css('height',pagesHeight);
			break;
		}	
	}
	
});	

function fbFeed_deal(name, _href, percentage) {

	if (_href == null) {
		_href = 'http://www.clubcupon.com.ar';
	}

	var image_src = $('#deal_image').attr('src') + '';
	if (image_src[0] == "/") {
		image_src = image_src.substr(1);
	}
	var img = __cfg.path_absolute + image_src;

	var title = "Club Cupon";
	var _caption = "{*actor*} compartió la oferta \"" + name + "\". Aprovecha el " + percentage + "% de descuento ahora mismo.";

	FB.ui({
		method: 'stream.publish',
		attachment: {
			name: title,
			caption: _caption,
			href: _href,
			media:[{
				type:'image',
				src:img,
				href:_href
			}]
		},
		action_links: [{
		  text: 'Ver Cupón',
		  href: _href
		}],
		user_message_prompt: 'Comparte con tus amigos esta novedad'
	});
}

	