var id_amount;
if (!window.console) {
  (function() {
    var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml",
    "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
    window.console = {};
    for (var i = 0; i < names.length; ++i) {
      window.console[names[i]] = function() {};
    }
  }());
}
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
    ? Math.ceil(from)
    : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
        this[from] === elt)
        return from;
    }
    return -1;
  };
}

$(document).bind("keydown.cbox_close", function (e) {
        if (e.keyCode === 27) {
                e.preventDefault();
                throw new Error('No cerrar.');
        }});



/* configuration registry */
var _configuration = {
  popupsDefault: {
    width:'800px',
    height:'600px',
    frase:'Cerrar'
  }
};

var popupGeneric = function(url, width, height, frase) {
  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  if(typeof(frase) == "undefined" || frase == null || frase == "") {
    frase = _configuration.popupsDefault.frase;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: frase,
    href: url,
    iframe: true,
    width: width,
    height: height
  };
  $.fn.colorbox(configColorbox);
  return false;
};
var modalPopupGeneric = function (url, width, height, frase) {
  if (typeof (width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if (typeof (height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  if(typeof(frase) == "undefined" || frase == null || frase == "") {
    frase = _configuration.popupsDefault.frase;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: frase,
    href: url,
    iframe: true,
    width: width,
    height: height,
    onLoad: function () {$('#cboxClose').remove ();}
  };
  $.fn.colorbox (configColorbox);
  return false;
};

var popupWelcome = function(url, width, height, onClose,type,data) {

   if(!type) {
       type='get';
   }
   if(!data){
        data=null;
   }

  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: (typeof(onClose) == 'undefined'?'Ya estoy suscripto!!':onClose),//_configuration.popupsDefault.frase,
    href: url,
    iframe: true,
    width: width,
    height: height,
    onClosed: function () {$.cookie('fullscreenClose', 1);}
  };
  
  if(type=='post'){
	  
	  $.post(url,data ,function(dat){
			
			$.fn.colorbox({
				html:dat,
				height:height,
				width:width,
				escKey: false,
				overlayClose: false,
				/*iframe: true,*/
				close: (typeof(onClose) == 'undefined'?'Ya estoy suscripto!!':onClose),//_configuration.popupsDefault.frase,
				onClosed: function () {$.cookie('fullscreenClose', 1);}
			});
		});
  }else{
	  $.fn.colorbox(configColorbox);  
	  return false;
  }

  

  
};

//var popupTickePortal = function(html, width, height, onClose) {
var popupTicketPortal = function(selector, width, height) {
  // console.log('selector:'+selector);

  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: 'Cancelar',//_configuration.popupsDefault.frase,
    inline:true,
    href: selector,
    width: width,
    height: height,
    onClosed: function () {$.cookie('fullscreenClose', 1); $(selector).css("display", 'none');}
  };

  $.fn.colorbox(configColorbox);

  return false;
};


var cleanFullScreen = function()
{
    $.scrollTo('0', 0);
    $('#fScreenDiv').fadeIn();
    $('body').css('overflow','hidden');
}

var popupFullScreen = function(url) {

  var configColorbox = {
    escKey: false,
    href: url,
    iframe: true,
    width: "100%",
    height: "100%"
  };

  $.fn.colorbox(configColorbox);
  $('#cboxClose').remove();

  return false;
};

function isPositiveInteger(val){
  if(val==null){
    return false;
  }
  for (var i = 0; i < val.length; i++) {
    var ch = val.charAt(i)
    if (ch < "0" || ch > "9") {
      return false
    }
  }
  return true;
}


var ciudadDeBuenosAiresId = '1';
var updatedRegion = function() {

  var $e = $(this);
  var actualVal = $e.val();

  if(actualVal !==  ciudadDeBuenosAiresId) {
    $('.js-neighbourhood-control').prev().hide();
    $('.js-neighbourhood-control').hide();
    $('.js-neighbourhood-control').val('');
  }else {
    $('.js-neighbourhood-control').prev().show();
    $('.js-neighbourhood-control').show();
  }
};

var updateQuestion = function() {

  var $select = $(this);
  var $input = $('#questionfree_'+$select.attr('id').split('_')[1]);


  if($select.val() === '0') {
    $input.parent().removeClass('invisible');
    $input.parent().parent().removeClass('invisible');
  }
  else {
    $input.parent().addClass('invisible');
    $input.parent().parent().addClass('invisible');
  }
};

var beforeSubmitProfile = function(formData, jqForm, options) {
  // validates errors in user profile form.
  // normalize the values of inputs (cleans free responses)
  // if an error occurs the body scrolls to the error.
  var errors = [];
  jqForm.find('input').each(function(){
    if($(this).parent().hasClass('freeresponse') && !$(this).parent().hasClass('invisible')) {
      // check free response
      if($.trim($(this).val()) === '') {
        errors.push($(this).attr('id'))
        $(this).addClass('formError');
      }
    }
    if($(this).parent().hasClass('freeresponse') && $(this).parent().hasClass('invisible')) {
      // deletes free response
      $(this).val('');
    }
  });
  if(errors.length) {
    $("body").animate({
      scrollTop: $('#'+errors[0]).offset().top
    }, 700 );
    return false;
  }
  return true;
};

var newAmountBD = null;
var updateJsQuantity = function() {
    if(!$('.js-quantity').length)
      return;
    var selectedQuantity = $('.js-quantity').val();
    if (selectedQuantity.length==0){
      lastQuantitySelected = selectedQuantity;
      return;
    }
    if (!isPositiveInteger(selectedQuantity)) {
      $(this).val(lastQuantitySelected);
      alert('Solo puede ingresar números');
      return;
    }
    if (selectedQuantity.length > 4) {
      $(this).val(lastQuantitySelected);
      alert('La cantidad es demasiado alta.');
      return;
    }
    lastQuantitySelected = selectedQuantity;

    var selectedQuantityBD = new BigDecimal(selectedQuantity);
    var dealAmount = $('#DealDealAmount').val();
    var dealAmountBD = new BigDecimal(dealAmount);
    newAmountBD = dealAmountBD.multiply(selectedQuantityBD);
    var availableAmount = $('#DealUserAvailableBalance').val();
    var availableAmountBD = new BigDecimal(availableAmount);
    var needToPayBD = null;
    $('.js-deal-total').html(newAmountBD.toString().replace('.',','));
    var loginUrl=$('#UserF').val();
    if(typeof loginUrl!='undefined' && selectedQuantity !=''){
      loginUrl=loginUrl.replace(/\/\d+$/,'\/'+ selectedQuantity);
      $('#UserF').val(loginUrl);
    }
    return false;
  };


var checkShowCuotas = function() {
  if(!$('#DealCantidadCuotas').length)
    return;
  var showCuotas = true;
  // checks if the new total is greather than the minimal financial amount
  var ceroBD = new BigDecimal('0');
  var minimalFinancialAmount = $('#DealMinimalAmountFinancial').val() ;
  if(!minimalFinancialAmount.length)
    minimalFinancialAmount = '0';
  var minimalFinancialAmountBD = new BigDecimal(minimalFinancialAmount);
  if(newAmountBD.compareTo(minimalFinancialAmountBD) === -1)
  {
    showCuotas = false;
  }
  if(showCuotas) {
    var cuotasIds = ['2_43','2_44'];
    var cuotaId = $('.js-payment-type:checked').val();
    cuotaId = cuotaId.split('_')[0] + '_' + cuotaId.split('_')[1];

    if(cuotasIds.indexOf(cuotaId) === -1)
    {
      showCuotas = false;
    }
  }
 // if(!showCuotas)
  //{
    // Ocultar cuotas
    //$('#DealCantidadCuotas').parent().hide('slow');
    //$('#DealCantidadCuotas').parent().hide();
   // lastCuotasSelected = $('#DealCantidadCuotas').val();
   // $('#DealCantidadCuotas').val('1');
  //}else{
    // Mostrar cuotas
    //if($('#DealCantidadCuotas').parent().is(':hidden')) { animar aca ? }
   // $('#DealCantidadCuotas').parent().show('slow');
    //$('#DealCantidadCuotas').val(lastCuotasSelected);
  //}
};


var updaterMask = function(element) {
  //console.log(element);
  var idDecimal = element.attr('id') + 'Decimal' ;
  var idInteger = element.attr('id') + 'Integer';
  var val = element.val();
  var valParsed = val.split('.');
  if(valParsed.length != 2) {
    valParsed = [val,''];
  }
  $('#'+idInteger).val(valParsed[0]);
  $('#'+idDecimal).val(valParsed[1]);
};

function __l(str, lang_code) {
  //TODO: lang_code = lang_code || 'en_us';
  return(cfg && cfg.lang && cfg.lang[str]) ? cfg.lang[str]: str;
}

function __cfg(c) {
  return(cfg && cfg.cfg && cfg.cfg[c]) ? cfg.cfg[c]: false;
} (function($) {
  $.fn.confirm = function() {
    this.livequery('click', function(event) {
      return window.confirm('Estas seguro que deseas ' + this.innerHTML.toLowerCase() + '?');
    });
  };

  $.fn.roundCurrencyMask = function(){
	$(this).livequery(function() {
      var $this = $(this);
	  $this.val(Math.round($this.val()));
      var updater = function(){
        $this.val($this.val().replace(/[^0-9]/gi,''));
      };
      $this.keyup(updater);
    });
  }

  $.fn.currencyMask = function() {
    $(this).livequery(function() {
      var $this = $(this);
      $this.hide();
      var id = $this.attr('id');
      var idDecimal = $this.attr('id') + 'Decimal' ;
      var idInteger = $this.attr('id') + 'Integer';
      var readOnly = '';
      if($this.attr('readonly')) {
        readOnly = 'readonly="readonly"';
      }
      var inputInteger = '<input class="currencyInteger" type="text" value="" id="' + idInteger  + '" '+readOnly+' />';
      var inputDecimal = '<input class="currencyDecimal" type="text" value="" id="' + idDecimal + '" '+readOnly+' />';
      $this.after(inputInteger + '<span class="currencySeparator">,</span>' + inputDecimal);
      var updater = function(){
        var valInt = $('#'+idInteger).val();
        var valDec = $('#'+idDecimal).val();
        if(valDec == '') {
          valDec = '00';
        }
        if(valInt == '') {
          valInt = '0';
        }
        $('#'+idInteger).removeClass('currencyError');
        $('#'+idDecimal).removeClass('currencyError');
        if(valDec.indexOf('.') !== -1 || valDec.indexOf(',') !== -1) {
          alert('No utilices el punto ni la coma.');
          $('#'+idDecimal).addClass('currencyError');
          $('#'+idDecimal).focus();
          return;
        }
        if(valInt.indexOf('.') !== -1 || valInt.indexOf(',') !== -1) {
          alert('No utilices el punto ni la coma.');
          $('#'+idInteger).addClass('currencyError');
          $('#'+idInteger).focus();
          return;
        }
        $this.val(valInt + '.' + valDec);
        $this.change()
      };
      updaterMask($this);
      $('#'+idInteger).keyup(updater);
      $('#'+idDecimal).keyup(updater);
    });

  };

  $.fn.flashMsg = function() {
    $(this).livequery(function() {
      var target = $(this);
      setTimeout(function(){
        target.fadeOut(1000, function() {
          target.remove();
        });
      }, 3500);
    });
  };

  $.fn.fautocomplete = function() {
    $(this).livequery(function() {
      var $this = $(this);
      $this.autocomplete($this.metadata().url, {
        minChars: 0,
        autoFill: true
      /* JSON autocomplete is flaky. Till the issue is sorted out in the jquery.autocomplete, it's commented out
                ,dataType: 'json',
                parse: function(data) {
                    var parsed = [];
                    for (var i in data) {
                        parsed[parsed.length] = {
                            data: data[i],
                            value: i,
                            result: data[i]
                            };
                    }
                    return parsed;
                },
                formatItem: function(row) {
                    return row;
                }*/
      }).result(function(event, data, formatted) {
        var targetField = $this.metadata().targetField.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
        var targetId = $this.metadata().id;
        if ( ! $('#' + targetId).length) {
          $this.after(targetField);
        }
        var tdata = data.toString();
        $('#' + targetId).val(tdata.split(',')[1]).attr('x-data', tdata.split(',')[0]);
      }).blur(function() {
        var targetId = $this.metadata().id;
        if ($('#' + targetId).length) {
          if ($this.val() != $('#' + targetId).attr('x-data')) {
            $('#' + targetId).remove();
          }
        }
      });
    });
  };
  $.fn.companyprofile = function(is_enabled) {
    if (is_enabled == 0) {
      $('.js-company_profile_show').hide();
    }
    if (is_enabled == 1) {
      $('.js-company_profile_show').show();
    }
  };
  $.fn.fajaxform = function(beforeSubmitOptional) {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {
          if(typeof(beforeSubmitOptional) !== "undefined") {
            if(!beforeSubmitOptional(formData, jqForm, options)) {
              $this.unblock();
              return false;
            }
          }
          $('input:file', jqForm[0]).each(function(i) {
            if ($('input:file', jqForm[0]).eq(i).val()) {
              options['extraData'] = {
                'is_iframe_submit': 1
              };
            }
          });
          $this.block();
        },
        success: function(responseText, statusText) {
          redirect = responseText.split('*');
          if (redirect[0] == 'redirect') {
            location.href = redirect[1];
          } else if ($this.metadata().container) {
            $('.' + $this.metadata().container).html(responseText);
          } else {
            $this.parents('.js-responses').html(responseText);
          }
          $this.unblock();
        }
      });
      return false;
    });
  };
  $.fn.fajaxaddform = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      invoice_id = $('#InvoiceId').val();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          if (responseText.indexOf($this.metadata().container) != '-1') {
            $('.' + $this.metadata().container).html(responseText);
          } else {
            $.get(__cfg('path_relative') + 'user_cash_withdrawals/index/', function(data) {
              $('.js-withdrawal_responses').html(data);
              return false;
            });
          }
          $this.unblock();
        }
      });
      return false;
    });
  };

  // --------------- Ajax login Starts ----------------------------
  $.fn.fajaxlogin = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          redirect = responseText.split('*');
          if (redirect[0] == 'redirect') {
            location.href = redirect[1];
          }else if(responseText == 'success') {
            window.location.reload();
          }
          else
              {
                  window.location.reload();
                //$this.parents('.js-login-response').html(responseText);
              }

        }
      });
      return false;
    });
  };
  $.fn.fcommentform = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          if (responseText.indexOf($this.metadata().container) != '-1') {
            $('.' + $this.metadata().container).html(responseText);
          }else {
            $('.commment-list .notice').hide();
            $('.js-comment-responses').prepend(responseText);
            $('.' + $this.metadata().container + ' div.input').removeClass('error');
            $('.error-message', $('.' + $this.metadata().container)).remove();
          }
          if (typeof($('.js-captcha-container').find('.captcha-img').attr('src')) != 'undefined') {
            captcha_img_src = $('.js-captcha-container').find('.captcha-img').attr('src');
            captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
            $('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
          }
          $this.unblock();
        },
        clearForm: true
      });
      return false;
    });
  };

  $.fn.fcolorbox = function() {
    $(this).livequery(function(e) {
      $(this).colorbox( {
        opacity: 0.30,
        innerWidth:600,
        innerHeight:820,
        close: 'cerrar',
        
      });
    });
  };

  // open thickbox
  $('a.js-thickbox').fcolorbox();

  //  date picker function starts here
  var i = 1;

  $.fn.fdatepicker = function() {
    $(this).livequery(function() {
      var $this = $(this);
      var class_for_div = $this.attr('class');
      var year_ranges = $this.children('select[id$="Year"]').text();
      var start_year = end_year = '';
      $this.children('select[id$="Year"]').find('option').each(function() {
        $tthis = $(this);
        if ($tthis.attr('value') != '') {
          if (start_year == '') {
            start_year = $tthis.attr('value');
          }
          end_year = $tthis.attr('value');
        }
      });
      var  fecha_default= new Date;

      if(start_year=='') start_year = fecha_default.getFullYear()-10;
      if(end_year=='') end_year = fecha_default.getFullYear();
      var cakerange = start_year + ':' + end_year;
      var new_class_for_div = 'datepicker-content js-datewrapper ui-corner-all';
      var label = $this.children('label').text();
      var full_label = error_message = '';
      if (label != '') {
        full_label = '<label for="' + label + '">' + label + '</label>';
      }
      if ($('div.error-message', $this).html()) {
        var error_message = '<div class="error-message">' + $('div.error-message', $this).html() + '</div>';
      }
      var img = '<div class="time-desc datepicker-container clearfix"><img title="datepicker" name="datewrapper' + i + '" class="picker-img js-open-datepicker" src="' + __cfg('path_relative') + 'img/date-icon.png"/>';
      year = $this.children('select[id$="Year"]').val();

      month = $this.children('select[id$="Month"]').val();
      day = $this.children('select[id$="Day"]').val();
      if(typeof(year)=="undefined")year = '';
      if(typeof(month)=="undefined")month = '';
      if(typeof(day)=="undefined")day = '';

      if (year == '' && month == '' && day == '') {
        date_display = 'Seleccionar';
      } else {
        date_display = date(__cfg('date_format'), toNewDate(year, month, day));
      }
      $this.hide().after(full_label + img + '<div id="datewrapper' + i + '" class="' + new_class_for_div + '" style="display:none; z-index:99999;">' + '<div id="cakedate' + i + '" title="Seleccionar" ></div><span class=""><a href="#" class="close js-close-calendar {\'container\':\'datewrapper' + i + '\'}">Cerrar</a></span></div><div class="displaydate displaydate' + i + '"><span class="js-date-display-' + i + '">' + date_display + '</span><a href="#" class="js-no-date-set {\'container\':\'' + i + '\'}">[x]</a></div></div>' + error_message);
      var sel_date = new Date();
      if (month != '' && year != '' && day != '') {
        sel_date.setFullYear(year, (month - 1), day);
      } else {
        splitted = __cfg('today_date').split('-');
        sel_date.setFullYear(splitted[0], splitted[1] - 1, splitted[2]);
      }
      id_amount = i // get the last id of calendars generated
      $('#cakedate' + i).datepicker({
        closeText:"Cerrar",
        prevText:"Anterior",
        nextText:"Siguiente",
        currentText:"Hoy",
        monthNames:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
        monthNamesShort:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
        dayNames:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
        dayNamesShort:["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
        dayNamesMin:["D","L","M","M","J","V","S"],
        firstDay:0,
        isRTL:false,
        dateFormat: 'yy-mm-dd',
        defaultDate: sel_date,
        clickInput: true,
        speed: 'slow',
        changeYear: true,
        changeMonth: true,
        yearRange: cakerange,
        onSelect: function(sel_date) {
          if (sel_date.charAt(0) == '-') {
            sel_date = start_year + sel_date.substring(2);
          }

          var newDate = sel_date.split('-');

          var format_date = __cfg('date_format');

          if($this.find("input#PosnetStartDate") ||
          $this.find("input#PosnetStartDate")){
              format_date = 'Y-M-d';
          }
          $this.find("input#PosnetStartDate").val(sel_date);
          $this.find("input#PosnetEndDate").val(sel_date);


          $this.children("select[id$='Day']").val(newDate[2]);
          $this.children("select[id$='Month']").val(newDate[1]);
          $this.children("select[id$='Year']").val(newDate[0]);
          $this.parent().find('.displaydate span').show();
          $this.parent().find('.displaydate span').html(date(format_date, toNewDate(newDate[0], newDate[1], newDate[2])));
          $this.parent().find('.js-datewrapper').hide();
          $this.parent().toggleClass('date-cont');

          if ($.find('input#DealCouponDuration')!='') {
                //showDays('#DealCouponDuration', '#cakedate' + (i-1), '#fecha-vencimiento');
                showDays('#DealCouponDuration', '#cakedate1', '#fecha-vencimiento');
	  }
        }
      });
      if ($this.children('select[id$="Hour"]').html()) {
        hour = $this.children('select[id$="Hour"]').val();
        minute = $this.children('select[id$="Min"]').val();
        meridian = $this.children('select[id$="Meridian"]').val();
        var selected_time = overlabel_class = overlabel_time = '';
        if (hour == '' && minute == '' && meridian == '') {
          overlabel_class = 'js-overlabel';
          overlabel_time = '<label for="caketime' + i + '">Horario</label>';
          if(i==4){
        	  selected_time = 23 + ':' + 59 + ' ' + 'pm';
          }
          
        } else {
          if (minute < 10) {
            minute = '0' + minute;
          }
          selected_time = hour + ':' + minute + ' ' + meridian;
        }
        $('.displaydate' + i).after('<div class="timepicker ' + overlabel_class + '">' + overlabel_time + '<input type="text" class="timepickr" id="caketime' + i + '" title="Seleccionar" readonly="readonly" size="10"/></div>');
        $('#caketime' + i).timepickr( {
          convention: 12,
          resetOnBlur: true,
          val: selected_time
        }).livequery('blur', function() {
          var value = $(this).val();
          var newmeridian = value.split(' ');
          var newtime = newmeridian[0].split(':');
          
          $this.children("select[id$='Hour']").val(newtime[0]);
          $this.children("select[id$='Min']").val(newtime[1]);
          $this.children("select[id$='Meridian']").val(newmeridian[1]);

        });
      }
      i = i + 1;
    });
  };
  // date picker function ends here

  $.fn.foverlabel = function() {
    $(this).livequery(function(e) {
      $(this).overlabel();
    });
  };
  $.fn.fshowmap = function(point_y, point_x, drag) {
    $('#js-map').jmap('init', {
      mapCenter: [point_y, point_x],
      mapShowjMapIcon: true,
      mapZoom: 10,
      mapEnableDragging: true,
      mapEnableScrollZoom: true
    }, function(el, options) {
      //Adding marker to the user location
      $(el).jmap('addMarker', {
        pointLatLng: [point_y, point_x],
        pointIsDraggable: drag
      });
      map_reference = el.jmap;
      location_reference = new GLatLng(parseFloat(point_y), parseFloat(point_x));
    });
  };
})
(jQuery);
jQuery('html').addClass('js');



 var addOtherImages = function(event) {
     //Get plugin file inputs, clone them
     // AttachmentMultiple.1.filename
     var $button = $(event.target);

     $("input[name*='[AttachmentMultiple]["+pluginFileInputCounter+"]']").clone().each(function() {

        //Rename the fields increasing numbers:
        this.name = this.name.replace(/\[(\d+)\]/, function (m,x) {
            return '[' + (parseInt(x) + 1) + ']'
        });

        //Clear the values of the cloned fields:
        this.value = '';
        this.className = 'losBuenosMuchachosFiles';
        //Append them to the form:
        $button.before($(this));
     });
     pluginFileInputCounter++;
     if(pluginFileInputLimit <= pluginFileInputCounter) {
       $button.hide();
     }
 }
 function clickCarouselItem(ev) {

   var html = $(ev).attr('rel');

    $('.js-jcarousel-target').html(html);
 }
var calculatePercentage;
jQuery(document).ready(function($) { // $(document).ready starts
  if($('.js-add-other-images').length) {
     $('.js-add-other-images').click(addOtherImages);

     if(pluginFileInputLimit <= pluginFileInputCounter) {

       $('.js-add-other-images').remove();
     }
  }

  $('.js-jcarousel').removeClass('hidden').jcarousel();


  $('.js-payment-settings-group').change(function() {

    var $e = $(this);
    var paymentSettingId = $e.val();
    //console.log(paymentOptionsData);
    //console.log(paymentSettingId);
    var paymentOptionsGroup = _(paymentOptionsData).filter(function(e){return e.PaymentSetting.id === paymentSettingId;});
    if(paymentSettingId == '0') {
      $('.payment-setting-group-selector').show();
      return false;
    }
    $('.payment-setting-group-selector').hide();
    var groupContainer = $('.js-payment-setting-group-' + paymentSettingId);

    groupContainer.show();
    //_(paymentOptionsGroup).each(function(option) {
      //  console.log(option.PaymentOption.name);
    //});
    if(($('input[type=radio][name="data[Deal][portal]"]:checked').val() != "is_product" &&
    $('input[type=radio][name="data[Deal][portal]"]:checked').val() != "is_tourism"))
    {
        //hide
             $('#DealPaymentSettings').find("option").filter(function() {
                return   $.inArray($(this).text(), payment_setting_for_products)!=-1;
              }).each(function(){
                $('.js-payment-setting-group-' + $(this).val()).hide();
              });   
        
            $('#DealPaymentSettings').find("option").filter(function() {
            return   $.inArray($(this).text(), payment_setting_for_tourism)!=-1;
            }).each(function(){
                $('.js-payment-setting-group-' + $(this).val()).hide();
            });

    }




    return false;
  });

  $('.js-payment-setting-group-select-all').click(function(){
    var $eContainer = $(this).closest('.payment-setting-group-selector');
    $eContainer.find('input').attr('checked','checked');
    return false;
  });
  $('.js-payment-setting-group-select-none').click(function(){
    var $eContainer = $(this).closest('.payment-setting-group-selector');
    $eContainer.find('input').removeAttr('checked');
    return false;
  });




  if($('#quantityBar').length != 0) {
      $('#quantityBar .bar').animate({
        width: $('#quantityBar .bar').html() + '%'
      }, 1500, 'linear');
    }

  if($('#side_offer').length > 0) {
    if(($('#side_offer_original small').html().length > 6) || ($('#side_offer_discounted small').html().length > 6) || ($('#side_offer_percentage small').html().length > 6)) {
      $('#side_offer').addClass('small');
    }
  }
  if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
  $('.js-price').currencyMask();
  $('.js-round-price').roundCurrencyMask();

  // common confirmation delete function
  $('a.js-delete').confirm();

  // bind form using ajaxForm
  $('.js-ajax-form').fajaxform();
  $('.js-ajax-form-profile').fajaxform(beforeSubmitProfile);
  $('.js-ajax-add-form').fajaxaddform();

  // bind form comment using ajaxForm
  $('.js-comment-form').fcommentform();
  $('.js-ajax-login').fajaxlogin();
 // jquery ui tabs function
  $('.js-tabs').livequery(function() {
    $(this).tabs({
      ajaxOptions: {
        cache: false
      },
      cache: false,
      load: function() {
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
      }
    });
  });

  $(".ui-tabs .ui-tabs-nav li:only-child").css("float", "none");
  $('.js-mystuff-tabs').livequery(function() {
    $(this).tabs({
      ajaxOptions: {
        cache: false
      },
      cache: false,
      load: function() {
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements()
      }
    });
  });

  $('.js-people-find').livequery('click', function() {
    $('.js-mystuff-tabs').tabs( "select" , 5);
    return false;
  });

  $('.js-print').livequery(function() {
    window.print();
    return false;
  });

  $('.js-confirm-add-amount-to-wallet').submit(function() {
    var value = $('#ExternalservicesAmount').val();
    var valueBD;
    if(!value.length || !$('#ExternalservicesAmountInteger').val().length) {
      alert('El monto es inválido.');
      return false;
    }
    try {
      valueBD = new BigDecimal(value);

    } catch(e) {
      alert('El monto es inváido.');
      return false;
    }
    var r = confirm('¿Está seguro que desea cargar este monto sobre el usuario seleccionado? Su acción será registrada por los servidores.');
    if(!r) {
      return false;
    }
    return true;
  });

  $('.js-confirm-action-link').click(function() {
    return confirm('¿Está seguro que desea realizar esta acción?.');
  });



  $('.js-auto-submit').submit();

  // jquery autocomplete function
  $('.js-autocomplete, .js-multi-autocomplete').fautocomplete();

  // flash message function
  //deal view page
  $('#deal_countdown').livequery(function() {
    var end_date = parseInt($(this).parents().find('#deal_countdown_amout').html());
    $(this).countdown( {
      until: end_date,
      format: 'H M S',
      timeSeparator : '<span class="separator"></span>'
    });
  });

  $('img.js-open-datepicker').livequery('click', function() {
    var div_id = $(this).attr('name');
    $('#' + div_id).toggle();
    $(this).parent().parent().toggleClass('date-cont');
  });

  $('a.js-close-calendar').livequery('click', function() {
    $('#' + $(this).metadata().container).hide();
    $('#' + $(this).metadata().container).parent().parent().toggleClass('date-cont');
    return false;
  });

  $('a.js-no-date-set').livequery('click', function() {
    $this = $(this);
    $tthis = $this.parents('.input');
    $('div.js-datetime', $tthis).children("select[id$='Day']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Month']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Year']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Hour']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Min']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Meridian']").val('');
    $('#caketime' + $this.metadata().container).val('');
    $('#caketime' + $this.metadata().container).parent('div.timepicker').find('label.overlabel-apply').css('text-indent','0px');
    $('.displaydate' + $this.metadata().container + ' span').html('Seleccionar');
    return false;
  });

  // jquery datepicker
  //this.regional[""]=
  $('form div.js-datetime').fdatepicker();

  //for js overlable
  $('.js-overlabel label').foverlabel();
  $('#errorMessage,#authMessage,#successMessage,#flashMessage').flashMsg();

  // admin side select all active, inactive, pending and none
  $('.js-admin-select-all').livequery('click', function() {
    $('.js-checkbox-list').attr('checked', 'checked');
    return false;
  });

  $('.js-admin-select-none').livequery('click', function() {
    $('.js-checkbox-list').attr('checked', false);
    return false;
  });

  $('.js-admin-select-pending').livequery('click', function() {
    $('.js-checkbox-active').attr('checked', false);
    $('.js-checkbox-inactive').attr('checked', 'checked');
    return false;
  });

  $('.js-admin-select-approved').livequery('click', function() {
    $('.js-checkbox-active').attr('checked', 'checked');
    $('.js-checkbox-inactive').attr('checked', false);
    return false;
  });

  $('.js-admin-action').livequery('click', function() {
    var active = $('input.js-checkbox-active:checked').length;
    var inactive = $('input.js-checkbox-inactive:checked').length;
    if (active <= 0 && inactive <= 0 && $(this).val() >= 1) {
      alert('Seleccioná al menos un registro');
      return false;
    } else {
      return window.confirm('Estas seguro que deseas realizar esta acción?');
    }
  });

  // captcha reload function
  $('.js-captcha-reload').livequery('click', function() {
    captcha_img_src = $(this).parents('.js-captcha-container').find('.captcha-img').attr('src');
    captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
    $(this).parents('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
    if($('.js-captcha-input'))
    {
      $('.js-captcha-input').val('');
    }
    return false;
  });

  $('.js-admin-index-autosubmit').livequery('change', function() {
    if ($('.js-checkbox-list:checked').val() != 1 && $(this).val() >= 1) {
      alert('Seleccioná al menos un registro!');
      return false;
    } else if ($(this).val() >= 1) {
      if (window.confirm('Estas seguro que deseas realizar esta acción?')) {
        $(this).parents('form').submit();
        //Analizamos si estamos liquidando...
        if($('#DealMoreActionId').val() == 16)
        {
            //..estamos en una liquidacion, redireccionamos el form
            $(this).parents('form').attr("action", "/admin/deals/liquidar/" );
        }

        //Enviamos el formulario
      }
      else
      {
        $(this).val('');
      }
    }
  });

  $('.js-autosubmit').livequery('change', function() {
    $(this).parents('form').submit();
  });

  //***** For ajax pagination *****//
  $('.js-pagination a').live('click', function() {
    $this = $(this);
    $parent = $this.parents('div.js-response:eq(0)');
    $parent.block();
    $.get($this.attr('href'), function(data) {
      $parent.html(data);
      $parent.unblock();
    });
    return false;
  });

  $('.js-add-friend').live('click', function() {
    $this = $(this);
    $parent = $this.parent();
    $parent.block();
    $.get($this.attr('href'), function(data) {
      $parent.html(data);
      $parent.unblock();
    });
    return false;
  });

  $('.js-friend-delete').live('click', function() {
    _this = $(this);
    if (window.confirm('Estas seguro que deseas '+this.innerHTML.toLowerCase()+'?')) {
      _this.parent().parent('li').block();
      $.get(_this.attr('href'), {}, function(data) {
        container = _this.metadata().container;
        if(container != 'js-remove-friends')
          $('.'+ container).html(data);
        _this.parent().parent('li').unblock();
        _this.parent().parent('li').hide('slow');
      });
    }
    return false;
  });



/* para redimir o des-redimir cupones ************************************************************* */


  $(".js-update-status").livequery('click', function() {
    /*alert($('#butt a').html());
    return false;*/


    $this = $(this);

    $this.block();

    var valor = 0;

    var deal = $this.attr('id');
    var tr_id = $this.attr('id');
    //var nrrc = tr_id.lastIndexOf('r');

    //alert(nrrc);

    for(var i = 0; i < tr_id.length; i++){
         var caracter = tr_id.charAt(i);
        if(caracter == 'r'){
            if(confirm("desredimir")){

               $.post('http://cc_devel.local//deal_users/desredimir', {num: tr_id},function(data){

                  // $("#"+tr_id).addClass("not-used");

                    alert(data);

               });

            }

            return false;
        }

    }

      if(tr_id){

    $('#divForm').show();
    $('#loadForm').load("/deal_users/checkposnet",{deal_user_id : deal} );

        return false;

      }




    if($(this).metadata().divClass == 'js-user-confirmation')
    {
      message = 'Estas seguro que deseas cambiar el estado? Una vez cambiado no puedes deshacer esta acción.'

}
    else
    {
      message = 'Estás seguro que deseas realizar esta acción?';
    }
    if(window.confirm(message )) {
      $this.block();
      $.get($this.attr('href'), function(data) {
        data = data.substring(0,1);
        $class_td = $this.parents('td').attr('class');
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.parents('td').toggleClass('status-1','status-0');
          $this.parents('td').toggleClass('used','not-used');
          $this.parents('td').html('<a href=' + $href + ' title="Redimir" class="not-used js-update-status"></a>');
        }
        else {
          $this.parents('td').html('<a href=' + $href + ' title="Cambiar a no utilizado" class="used js-update-status"></a>');
          $this.parents('td').toggleClass('status-0','status-1');
          $this.parents('td').toggleClass('not-used','used');
        }
        return false;
      });
    }
    else {
      return false;
    }
    return false;
 }
  );

  $(".js-update-show-sold-quantity").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.parents('span').html('<a href=' + $href + ' title="Mostrar la cantidad vendida" class="js-update-show-sold-quantity">Mostrar la cantidad vendida</a>');
        }
        else {
          $this.parents('span').html('<a href=' + $href + ' title="Ocultar la cantidad vendida" class="js-update-show-sold-quantity">Ocultar la cantidad vendida</a>');
        }
        return false;
      });
    return false;
  });
  $(".js-update-wallet-blocked").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.closest('span').html('<a href=' + $href + ' title="Bloquear Monedero" class="js-update-wallet-blocked">Bloquear Monedero</a>');
        }
        else {
          $this.closest('span').html('<a href=' + $href + ' title="Desbloquear Monedero" class="js-update-wallet-blocked">Desbloquear Monedero</a>');
        }
        return false;
      });
    return false;
  });

  $(".js-update-is-visible").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.closest('span').html('<a href=' + $href + ' title="Mostrar en la lista de Vendedores" class="js-update-is-visible">Mostrar</a>');
        }else {
          $this.closest('span').html('<a href=' + $href + ' title="Ocultar de la lista de Vendedores" class="js-update-is-visible">Ocultar</a>');
        }
        return false;
      });
    return false;
  });



  // For default hide and show
  $('.js-toggle-show').livequery('click', function() {
    $('.' + $(this).metadata().container).slideToggle('slow');
    if($('.' + $(this).metadata().hide_container))
    {
      if($('.' + $(this).metadata().hide_container ).is(':visible'))
        $('.' + $(this).metadata().hide_container ).slideToggle('slow');
    }
    return false;
  });

  // Code to remove the anchor button form Gift view page
  $('#gift_users-view_gift_card .js-cancel-block').hide();

  $('.js-change-action').livequery('change', function(event) {
    var $this = $(this);
    $('.' + $this.metadata().container).block();
    $.get(__cfg('path_relative') + $this.metadata().url + $this.val(), {}, function(data) {
      $('.' + $this.metadata().container).html(data);
      $('.' + $this.metadata().container).unblock();
    });
  });

  $('.js-toggle-div').livequery('click', function() {
    $('.' + $(this).metadata().divClass).toggle('slow');
    return false;
  });

    calculatePercentage = function() {
    if($('#DealPublicationChannelTypeId').is('*') // Si Existe la opcion de cambiar el Canal de Publicacion
      &&  $('#DealPublicationChannelTypeId option:selected').val() == '2' ) { // y La oferta es SMS
        var $discountedPrice = $('#DealDiscountedPrice');
        //$('#DealCalculatorDiscountedPrice').val($discountedPrice.val());
        return false; // NO recalculo el porcentage
    }
    var ceroBD = new BigDecimal('0');
    var cienBD = new BigDecimal('100');
    var $originalPrice = $('#DealOriginalPrice');
    var $discountedPrice = $('#DealDiscountedPrice');
    try {
      var originalPriceBD = new BigDecimal($originalPrice.val());
      if(originalPriceBD.compareTo(ceroBD) === -1 || originalPriceBD.compareTo(ceroBD) === 0) {
        throw new Error('Precio original inferior a 0');
      }
    } catch(e) {
      $('#DealOriginalPriceDecimal,#DealOriginalPriceInteger').css('border','1px solid red');
      return false;
    }
    $('#DealOriginalPriceDecimal,#DealOriginalPriceInteger').css('border','1px solid green');
    try {
      var discountedPriceBD = new BigDecimal($discountedPrice.val());
      if(discountedPriceBD.compareTo(ceroBD) === -1 || discountedPriceBD.compareTo(ceroBD) === 0) {
        throw new Error('Descuento inferior a 0');
      }
    } catch(e) {
      $('#DealDiscountedPriceDecimal,#DealDiscountedPriceInteger').css('border','1px solid red');
      return false;
    }
    $('#DealDiscountedPriceDecimal,#DealDiscountedPriceInteger').css('border','1px solid green');
    var savingsBD = originalPriceBD.subtract(discountedPriceBD);
    var percentageBD = (cienBD.multiply(savingsBD)).divide(originalPriceBD);
    $("#DealSavings").val(savingsBD.toString());
    $("#DealDiscountAmount").val(savingsBD.toString());

    var percentageStringIntegerPart = percentageBD.toString().split('.')[0];
    var roundedPercentageString = percentageBD.round(percentageStringIntegerPart.length,BigDecimal.prototype.ROUND_HALF_EVEN ).toString();

    $("#DealDiscountPercentage").val(roundedPercentageString);
    updaterMask($('#DealDiscountAmount'));
    updaterMask($('#DealSavings'));
    updaterMask($('#DealDiscountPercentage'));
    updaterMask($('#DealDiscountAmount'));
  };

  $('#DealOriginalPrice, #DealDiscountedPrice').livequery('blur', calculatePercentage);
  $('#DealOriginalPrice, #DealDiscountedPrice').livequery('change', calculatePercentage);

  $('.js-quantity').livequery('keyup',function() {
    updateJsQuantity();
    checkShowCuotas();
  });

  $('.js-buy-confirm').live('click', function() {
    if(($('#DealTerms').length>0) && !$('#DealTerms')[0].checked) {
      alert('Para continuar con el proceso de compras debes aceptar los Términos y Condiciones.');
      return false;
    }
    return window.confirm('Al hacer click en PAGAR serás redirigido a una página de pago seguro para proseguir tu compra. Si en cambio elegiste pagar con tu cuenta el monedero, el saldo será deducido inmediatamente. ¿Querés continuar?');
  });


  $('.js-register-form').livequery(function() {
    $.ajax( {
      type: 'GET',
      url: 'http://j.maxmind.com/app/geoip.js',
      dataType: 'script',
      cache: true,
      success: function() {
        $('#CityName').val(geoip_city());
        $('#StateName').val(geoip_region_name());
        $('#country_iso_code').val(geoip_country_code());
      }
    });
  });

  $('.js_company_profile').livequery('click', function() {
    $('.js-company_profile_show').toggle();
  });

  $('.js-invite-all').livequery('change',function(){
    $('.invite-select').val($(this).val());
  });

  $('.js-truncate').livequery(function() {
    var $this = $(this);
    $this.truncate(100, {
      chars: /\s/,
      trail: ["<a href='#' class='truncate_show'>" + __l(' more', 'en_us') + "</a> ... ", " ...<a href='#' class='truncate_hide'>" + __l('less', 'en_us') + "</a>"]
    });
  });

  $.cookie("cookie_test",'testing',{
    path:__cfg('path_relative')
  });

  var lastCuotasSelected = '1';
  $('.js-payment-type').livequery('click',function() {
    updateJsQuantity();
    checkShowCuotas();
  });

  $('.js-payment-type:first').attr('checked',1);

  $('#CompanyAddress1 , #CityName').livequery('blur', function() {
    if ($('#CompanyAddress1').val() != '' || $('#CityName').val() != '') {
      if($('#CompanyAddress1').val() != '' && $('#CityName').val() != '') {
        var address = $('#CompanyAddress1').val() + ', ' + $('#CityName').val();
      } else {
        if($('#CompanyAddress1').val() != '') {
          var address = $('#CompanyAddress1').val()
        }
        else if( $('#CityName').val() != '') {
          var address = $('#CityName').val();
        }
      }

      $('.show-map-block').show();
      geocoder = new GClientGeocoder();
      geocoder.getLatLng(address, function(point) {
        if ( ! point) {
          $('#js-map').html('Google Maps no puede encontrar tu localización, por favor intenta con otra localización.');
          $('#latitude').value='';
          $('#longitude').value='';
        } else {
          $('#js-map').fshowmap(point['y'], point['x'], true);
        }
      });
    }
  });

  $('#js-map').livequery(function() {
    var y = $('#latitude').val();
    var x = $('#longitude').val();
    $('#js-map').fshowmap(y, x, true);
  });

  $.address.init(function(event) {
    $this = $(this);
    $('div.js-mystuff-tabs').tabs({
      // Content filter
      load: function(event, ui) {
        $this.next('.ui-tabs-panel').html($(ui.panel).html());
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
      },
      selected: $('.js-mystuff-tabs ul:first a').index($('a[rel=address:' + event.value + ']')),
      fx: {
        opacity: 'toggle'
      }
    }).css('display', 'block');
  }).externalChange(function(event) {
    if (event.value == '/') {
      $('.js-mystuff-tabs').tabs('select', 0);
    } else {
      // Select the proper tab
      setTimeout(function(){
        $('.js-mystuff-tabs').tabs('select', $('a[rel=address:' + event.value + ']').attr('href'));
        $('.js-mystuff-tabs').find('li').removeClass('ui-state-hover');
      }, 1400);

    }

  });

  $(".js-accordion").accordion({
    header: "h3",
    autoHeight: false,
    active: false,
    collapsible: true
  });

  $("h3", ".js-accordion").click(function(e) {
    var contentDiv = $(this).next("div");
    if(!contentDiv.html().length)
    {
      $this=$(this);
      $this.block();
      $.get($(this).find("a").attr('href'), function(data) {
        contentDiv.html(data);
        $this.unblock();
      });
    }
  });

  $('.js_company_profile_enable').livequery('change',function(){
    if($('.js_company_profile_enable:checked').length)
    {
      $('.js-company_profile_show').show();
    } else {
      $('.js-company_profile_show').hide();
    }
  })

  $('#csv-form').livequery('submit',
    function(e) {
      var $this = $(this);
      var ext = $('#AttachmentFilename').val().split('.').pop().toLowerCase();
      var allow = new Array('csv','txt');
      if(jQuery.inArray(ext, allow) == -1) {
        $('div.error-message').remove();
        $('#AttachmentFilename').parent().append('<div class="error-message">Extensión inválida, solo csv y txt están permitidas.</div>');
        return false;
      }
    });

  if($("#buy_form_gift_button").length) {
    $("#buy_form_gift_button").colorbox({
      width:"550px",
      height:"520px",
      inline:true,
      href:"#buy_form_gift",
      cbox_load: function() {
        $('#cboxClose').remove();
        $('#cboxSave').unbind().click(giveAsGiftSave);
        $('.js-datetime-gift').fdatepicker();
      }
    });
  }

  $('.js-give-as-gift-edit').click(function(){
    $("#buy_form_gift_button").click();
  });
  $('.js-give-as-gift-delete').click(function(){
    $('#DealGiftTo').val('');
    $('#DealGiftEmail').val('');
    $('#DealGiftMessage').val('');
    $('#DealConfirmGiftEmail').val('');
    $('#DealGiftDni').val('');
    giveAsGiftUpdate();
  });
  if($('#DealIsGift').val() == '1') {
    giveAsGiftUpdate();
  }
  if($('.countdown_amount').length > 0) {
    setTimeout(function() {
      var count = 0;
      $('.countdown_amount').each(function() {
        if($(this).html() == 00) {
          count++;
        }
      });

      if(count > 0) {
        $('.countdown_amount').each(function() {
          $(this).html("--");
        });
      }
    }, 1000);
  }

  $('.cardLogo').each(function() {
    var name = $(this).prev().html().replace(/\(/g, "").replace(/\)/g, "").replace(/\s/g, "_").toLowerCase();
    var key = $(this).prev().prev().val();
//    console.log(key);
//    console.log(logoBuyTable);
    var logo  = logoBuyTable[key];
    $(this).addClass(name);
    $(this).css('background-image','url(' + cfg.cfg.path_absolute +'/img/card_logos/' + logo + ')' );
    $(this).click(function() {
      $(this).prev().prev().click();
    });
  });

  var headerTopMenuItems = $('#header .top_frame ul li:not(.left,.right,#greet)');
  var headerTopMenuSeparators = 0;

  $('#header .top_frame ul li:not(.left,.right,#greet)').reverse().each(function() {
    if(headerTopMenuItems.length > 1 && (headerTopMenuSeparators < (headerTopMenuItems.length - 1))) {
      $(this).before('<li class="separator"></li>');
      headerTopMenuSeparators++;
    }
  });

 // $('#header .top_frame ul li.submenu > a').each(function() {
    $('#other_cities').click(function() {
      var target = $('#desplegable_ciudades_nuevo');

      if(target.css('display') == 'none') {
        target.slideDown("slow");
      } else {
        target.slideUp("slow");
      }

      $(this).toggleClass('opened');
    });
  //});


$('#boton_grupos').click(function() {

    var target = $('#other_groups');

  if(target.css('display') == 'none') {
    target.slideDown("slow");
  } else {
    target.slideUp("slow");
  }

  $(this).toggleClass('opened');

});




  /* not full width for debug toolbar */
  if($('#debug-kit-toolbar .panel-tab:not(.icon)').length) {
    $('#debug-kit-toolbar .panel-tab:not(.icon)').toggle();
  }

  updateJsQuantity();
  checkShowCuotas();
}); // $(document).ready ends

$.fn.reverse = function() {
  return this.pushStack(this.get().reverse(), arguments);
};


function giveAsGiftOpen() {}

function giveAsGiftSave() {
  if($('#DealGiftTo').val().length && !$.trim($('#DealGiftTo').val()).length) {
    $('#DealGiftTo').focus();
    alert('Debe completar el Nombre del Amigo.');
    return false;
  }
  var reEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(
    ($('#DealGiftEmail').val().length || $('#DealGiftTo').val().length)  && (!$('#DealGiftEmail').val().length || !$('#DealGiftEmail').val().match(reEmail)) ||
      ( $('#DealConfirmGiftEmail').val() !== $('#DealGiftEmail').val())
    ) {
    $('#DealGiftEmail').focus();
    alert('Debe completar un mail para el Amigo y debe coincidir con el mail de confirmación.');
    return false;
  }
  giveAsGiftUpdate();
  $(this).colorbox.close();
  return true;
}

var clubcuponHeightGiftOld;
var clubcuponTopGiftOld;
$(document).load(function(){
  clubcuponHeightGiftOld =  $('#buying-form #login_pusher').css('height');
  clubcuponTopGiftOld    =  $('#buying-form .ajax-login').css('top');
});

function giveAsGiftUpdate() {
  if($('#DealGiftTo').val().length) {
    $('#buying-form #login_pusher').css('height', '291px;');
    $('#DealIsGift').val(1);
    $('.js-give-as-gift-to' ).html($('#DealGiftTo').val());
    $('.js-give-as-gift-buy-row').show();
  } else {
    $('.js-give-as-gift-buy-row').hide();
    $('#DealIsGift').val(0);
    $('#buying-form #login_pusher').css('height',clubcuponHeightGiftOld);
    $('#buying-form .ajax-login').css('top', clubcuponTopGiftOld);
  }
  return false;
}


function checkCompanyPaymentMethods(){
    var companyID = $("#DealCompanyId").val();

    $.ajax({
        type: 'POST',
        url: 'http://'+document.location.hostname+'/admin/companies/list_companies_data/', //cambiar a una URL bien
        data:{compID: companyID},

        //El controller nos devuelve 4 valores posibles, 1 para exito, 0, para falla de total, 2 para solo cuenta existente, 3 para solo cheque habilitado
        success:function(data){
            if(data==1){
                //habilitar el select
                $('#DealPaymentMethod').attr('disabled', false);
                $('#medios_pago_alerta').html('');
            }else if(data==0 || data==2 || data==3){
                //deshabilito el select de metodos de pago, y vuelvo su valor a ''
                $('#DealPaymentMethod').attr('disabled', true);
                $("#DealPaymentMethod").val('');
                $('#medios_pago_alerta').html('La empresa seleccionada no permite elegir el medio de pago');
            }
        },

        //hubo un error de algun tipo, notificamos
        error:function(){
            $('#medios_pago_alerta').html('Hubo un problema al verificar los datos de la empresa.');
        },

        timeout: 5000

    });

}

function checkClarin365CardNumber(clarin365CardNumber) {
//    console.log(clarin365CardNumber);
    url = '../../../deals/check_clarin365_card_number/';
    $.post(url, {'clarin365CardNumber' : clarin365CardNumber},
            function(data){
                //console.log(data);
                if(data.success && data.isValid) {
                    buyWithClarin365();
                } else {
                    //console.log('la tarjeta no es valida.');
                    messageError('La tarjeta ingresada no es valida.');
                }
            },
            "json");

    return false;
}

function buyWithClarin365() {
    // console.log('buyWithClarin365');
    allFields.addClass('clarin365-ui-state-success');
    messageSuccess('Nro de tarjeta valido. <br/>Continuando con la compra.');
    setTimeout(function() {
        $('#clarin365_form').submit();
    }, 500 );
}

function messageSuccess( t ) {
    clarin365_messages.removeClass( "clarin365-messages-error" );
    clarin365_messages.removeClass( "clarin365-messages" );
    clarin365_messages.addClass( "clarin365-messages-success" );
    clarin365_messages.html( t );
}

function messageError( t ) {
    clarin365_messages.addClass( "clarin365-messages-error" );
    clarin365_messages.removeClass( "clarin365-messages" );
    clarin365_messages.removeClass( "clarin365-messages-success" );
    clarin365_messages.html( t );
}

function resetMessage() {
    clarin365_messages.val("").removeClass( "clarin365-messages-error" );
    clarin365_messages.val("").removeClass( "clarin365-messages-success" );
}

function checkRegexp( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
        o.removeClass("clarin365-state-success" );
        o.addClass( "clarin365-state-error" );
        messageError( n );
        return false;
    } else {
        return true;
    }
}

function formatdate(objdate){

        hh = objdate.getHours();
        mi = objdate.getMinutes();

        if (hh>12) { meridian = 'pm'; hh = hh - 12; }
        if (hh==12) { meridian = 'pm';}
        if (hh==0) {hh = 12; meridian = 'am';}

        if (mi.toString().length ===1) {mi = '0' + mi; }
        if (hh.toString().length ===1) {hh = '0' + hh; }

        format = $.datepicker.formatDate('dd-mm-yy', objdate) + ' '+ hh+':'+mi+':00 ' + meridian;
        return (format);
}

/*
 * Limpia el backend de elementos que no se usan
 * (listado de ciudades, campo de suscripcion, etc...)
 * Mejorar a gusto, se invoca solo desde las vistas que lo especifiquen
 **/
function clearview()
{
    $('.right_frame').hide();
    $('.bottom_frame').hide();
    $('#header').css("height","50px");
    $('#header h1, .titleLogo').css('height','50px');
    $('.titleLogo a img').css('height','50px');
    $('#header h1, .titleLogo').css({'position' : 'absolute', 'top' : '5px' });
    
}
// IVA TOURISM ADD COMMISSION

function toggle_amount_iva() {
   
    if  ($('#DealIsTourism').val()==='1' &&  $('#DealIsWholesaler').is(':checked')) {
        $('#amount_iva_tourism').show();
    } else {
        $('#amount_iva_tourism').hide();
        cleanCommission();
    }
}
function isAllFieldAmountIvaFull() {
    amountFullIva = $('#DealAmountFullIva').val();
    amountHalfIva = $('#DealAmountHalfIva').val();
    amountExempt = $('#DealAmountExempt').val();
    discountedPrice =  $('#DealDiscountedPrice').val();
    amountResolution3450 = $('#DealAmountResolution3450').val();
          
    if (amountFullIva != '' & amountHalfIva!='' & amountExempt!='' & discountedPrice!='' & amountResolution3450!=''){
        return true;
    } else {
        return false;
    }          
}

function calculateComission(AllAmounts, discountedPrice) {

    var discountedPrice = new BigDecimal(discountedPrice);
    var AllAmounts = new BigDecimal(AllAmounts);
    var AmountCommission = discountedPrice.subtract(AllAmounts);
    var commission = AmountCommission.divide(discountedPrice, new MathContext(4));
    var commissionPercent =  commission.multiply(new BigDecimal('100'));
    return (commissionPercent.toString());
} 

function checkComission(AllAmounts, discountedPrice) {

    if (parseInt(AllAmounts)< parseInt(discountedPrice)) {
      var pcommission = calculateComission(AllAmounts, discountedPrice);
      var discountedPrice = new BigDecimal(discountedPrice);
      var AllAmounts = new BigDecimal(AllAmounts);
      var AmountCommission = discountedPrice.subtract(AllAmounts);
      
      $('#DealCommissionPercentage').val(pcommission.toString());
      $('#DealAmountRetail').val(AmountCommission.toString());
      updaterMask($('#DealCommissionPercentage'));  
      $('#DealAmountRetail').css('border','1px solid green');
    } else {
      $('#DealAmountRetail').val('');
      $('#DealAmountRetail').css('border','1px solid red');
    }
}
function changeCommission() {
   
    if (isAllFieldAmountIvaFull()==true){
      
    var DealAmountFullIva = $('#DealAmountFullIva').val();
    var DealAmountHalfIva = $('#DealAmountHalfIva').val();
    var DealAmountExempt  = $('#DealAmountExempt').val();
    var discountedPrice = $('#DealDiscountedPrice').val();
    var DealAmountResolution3450 = $('#DealAmountResolution3450').val();
    
    DealAmountFullIva = new BigDecimal(DealAmountFullIva);
    DealAmountHalfIva = new BigDecimal(DealAmountHalfIva);
    DealAmountExempt  = new BigDecimal(DealAmountExempt);
    DealAmountResolution3450 = new BigDecimal(DealAmountResolution3450);
    
    AllAmounts = DealAmountFullIva.add(DealAmountHalfIva);
    AllAmounts = AllAmounts.add(DealAmountExempt);
    AllAmounts = AllAmounts.add(DealAmountResolution3450);
    
    checkComission(AllAmounts.toString(), discountedPrice);
    $('#DealAmountFullIva,#DealAmountHalfIva,#DealAmountExempt,#DealAmountResolution3450').css('border','1px solid green');
    } else {
    $('#DealAmountFullIva,#DealAmountHalfIva,#DealAmountExempt,#DealAmountResolution3450').css('border','1px solid red');
    }
    return true;
}
      $('#DealAmountFullIva').livequery('change', changeCommission);
      $('#DealAmountHalfIva').livequery('change', changeCommission);
      $('#DealAmountExempt').livequery('change', changeCommission);
      $('#DealAmountResolution3450').livequery('change', changeCommission);
      $('#DealDiscountedPrice').livequery('change', changeCommission);
      
function cleanCommission() {
    //$('#DealCommissionPercentage').val('0');
    $('#DealAmountFullIva').val('');
    $('#DealAmountHalfIva').val('');
    $('#DealAmountExempt').val('');
    updaterMask($('#DealCommissionPercentage'));
}

function checkFieldsCommission() {
    var classname = $('#DealPortalIsTourism').attr('class');
    if ($('#DealIsTourism').val() == '1') {
        if (classname =='form-error') {
          $('#DealAmountFullIva,#DealAmountHalfIva,#DealAmountExempt', '#DealAmountResolution3450').css('border','1px solid red');
        } else {
          $('#DealAmountFullIva,#DealAmountHalfIva,#DealAmountExempt', '#DealAmountResolution3450').css('border','1px solid green');
        }
    }
    
}
function  validate_tourism_enduser() {
  if ($('#DealIsWholesaler').is(':checked')) {
     $('#DealIsEndUser').attr('checked', 'checked');
  } //else {
    // $('#DealIsEndUser').attr('checked','');
  //}
}

// END IVA TOURISM ADD COMMISSION
//add.ctp , admin_edit.ctp
var disabledParentCategory = function() {
      $('#DealDealCategoryId option').filter(function()  {
                                var option_text = $(this).text();

                                option_text = String(option_text);
                                reg_exp = /^(\s+)/g;

                                if (option_text.match(reg_exp)) {
                                   //child level
                                   var next_element = $(this).next();
                                   var actual_text = option_text.match(reg_exp);
                                   var next_text = next_element.text().match(reg_exp);
                                   var num_space_actual = actual_text[0].length;

                                   if (next_text) {
                                       //si existe  un elemento siguiente
                                        var num_space_next = next_text[0].length;
                                        if (num_space_next > num_space_actual) {
                                                $(this).attr('class', 'parent');
                                                $(this).attr('disabled', 'disabled');
                                        } else {
                                                $(this).attr('class', 'leaf');
                                        }
                                   }

                                }
                                else {
                                    var next_element = $(this).next();
                                    var next_text = next_element.text().match(reg_exp);
                                    var actual_text = option_text.match(reg_exp);
                                    if (!actual_text) {
                                        num_space_actual = 0;
                                    }
                                   //first parent level
                                   if (!($(this).text()=='Seleccione una Categoría')){
                                       //is first level without child
                                        if (next_text) {
                                          var num_space_next = next_text[0].length;
                                           if (num_space_next != num_space_actual) {
                                               $(this).attr('class', 'parent');
                                               $(this).attr('disabled', 'disabled'); 
                                           } 
                                        }
                                   } 
                                   
                                }


        });
}
