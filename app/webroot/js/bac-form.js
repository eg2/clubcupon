  $(function () {
    switch ($('#ID_GATEWAY').val ())
      {
        case '1':$('#mp-form' ).submit ();break;
        case '2':$('#bac-form').submit ();break;
        case '5':$('#bac-form').submit ();break;
        case '11':$('#bac-form').submit ();break;
        case '23':$('#bac-form').submit ();break;
        case '24':$('#bac-form').submit ();break;
        case '30':$('#bac-form').submit ();break;
        default:
          // deja el primero elegido
          $('.js-payment-type:first').attr ('checked', 1);

          $('#payment-type').submit (function () {
            var loading = $('#BASE').val () + 'img/loading.gif';
            $('#submit').html ('<img src = "' + loading + '"  alt = "loading" />');
            var url = this.action;
            var deal_external_id = $('#DEAL_EXTERNAL_ID').val ();
            var paymentSelection = $('.js-payment-type:checked').val ();
            var id_gateway = paymentSelection.split ('_') [0];
            var payment_type_id = paymentSelection.split ('_') [1];
            var bac_payment_type = paymentSelection.split ('_') [3];

            var user_available_balance = $('#user_available_balance').val ();
            url += '/' + deal_external_id + '/' + payment_type_id + '/' + user_available_balance;
            $.get (url, function () {
              
              urlInit = $('#urlinitpay').val();
              //removing slug
              urlExpr = /city:((\w+-?\w+)*)$/;
              slug = urlInit.match(urlExpr);
              city_len = slug[0].length;
              //end removing slug
              to = $('#urlinitpay').val().length - (city_len + 1); //remove slug
              urlInit = urlInit.substring(0,to);
              
              $('#idMedioPago').val(payment_type_id);
              $('#idGateway').val(id_gateway);
              
              //serializar form init-pay
              dataInit = $("#initpay").serialize();
              
              //enviar form  init-pay y recibir respuesta
              $.post(
                  urlInit,
                  dataInit,
                  function (data) { 
                      $('#ID_GATEWAY').val(id_gateway);
                      $('#ID_PAGO').val(data.bac_info['idPago']);
                      $('#SECURE_HASH').val(data.bac_info['secureHash']);
                      if (!data) {
                         urlcancel = basecc +'/'+dealExternalId;
                         window.location.replace(urlcancel);
                      }
                      switch ($('#ID_GATEWAY').val ())
                      {
                        case '1':$('#mp-form' ).submit ();break;
                        case '2':$('#bac-form').submit ();break;
                        case '5':$('#bac-form').submit ();break;
                        case '11':$('#bac-form').submit ();break;
                        case '23':$('#bac-form').submit ();break;
                        case '24':$('#bac-form').submit ();break;
                        case '30':$('#bac-form').submit ();break;                      
                      }
            
                  },
                  'json'
              )
              .done(function(data) {
                $('#ID_PAGO').val(data.bac_info['idPago']);
                $('#SECURE_HASH').val(data.bac_info['secureHash']);
              })
              .fail(function(data){
                basecc = $('#basecc').val();
                dealExternalId = $('#idPagoPortal').val();
                urlcancel = basecc +'/'+dealExternalId;
                window.location.replace(urlcancel);
              });
              //settear los datos en bac-form si recibí respuesta
             });
              
            return false;
          });
          if (typeof (pagoDirecto) !== "undefined" && pagoDirecto)  $('#payment-type').submit();
      }
  });
