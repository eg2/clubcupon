var gaJsHost = (("https:" == document.location.protocol ) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

function sendDataToAnalytics(deal,dealExternal){
	try{
		console.log(":::::sendDataToAnalytics INICIO ::::::::");
		  var pageTracker = _gat._getTracker("UA-38701644-5");
		  pageTracker._trackPageview();
		  pageTracker._addTrans(
		      dealExternal.get('id'),            // order ID - required
		      "ClubCupon",                                                     // affiliation or store name
		      dealExternal.get('final_amount'), // total - required
		      "0.0",                                                           // tax
		      "0.0",                                                           // shipping
		      dealExternal.get('city_id'),                                      // city
		      "",                                                              // state or province
		      "Argentina"                                                      // country
		    );
		   // add item might be called for every item in the shopping cart
		   // where your ecommerce engine loops through each item in the cart and
		   // prints out _addItem for each 
		   pageTracker._addItem(
				   dealExternal.get('id'),       // order ID - necessary to associate item with transaction
				   deal.get('id'),                       // SKU/code - required
				   deal.get('title'),                       // product name
				   deal.get('category_name'),                                                         // category or variation
		      deal.get('price'),        // unit price - required
		      dealExternal.get('quantity')  // quantity - required
		   );
		   pageTracker._trackTrans();                                     //submits transaction to the Analytics servers
		   console.log(":::::sendDataToAnalytics FIN ::::::::");
	} catch(err) {console.log(":::::sendDataToAnalytics ERROR ::::::::");}
}
