(function() {
  this.JST || (this.JST = {});
  this.JST["backbone/templates/buy/payment_plan_items_list"] = function(obj) {
     obj || (obj = {});
     var __t, __p = '', __e = _.escape;
     with (obj) {
     __p += '<ul id="payment-plan-items" class="mdpago">\r\n</ul>';
     }
     return __p
  };
}).call(this);
