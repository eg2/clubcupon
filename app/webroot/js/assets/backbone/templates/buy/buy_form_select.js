(function() {
 this.JST || (this.JST = {});
 this.JST["backbone/templates/buy/buy_form_select"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="field col1 clearfix">  <label for="amount"><p>Cantidad:</p></label>  <select id="amount" name="amount">      ';
 if (this.dealExternal.get('qty_user_can_buy')==0) {;
__p += '      <option value="0"> 0 </option>      ';
} else { ;
__p += '        ';
 for (var i=1; i <= this.dealExternal.get('qty_user_can_buy'); i++){;
__p += '        <option value="' +
((__t = (i)) == null ? '' : __t) +
'"> ' +
((__t = (i)) == null ? '' : __t) +
' </option>        ';
};
__p += '      ';
};
__p += '  </select></div><div class="field col2 clearfix">  <label for="total_price"><p>Total: </p></label>  <input type="text" name="total_price" id="total_price"         value="' +
((__t = ( get('total_price') )) == null ? '' : __t) +
'" readonly="true" >  <label for="total_price" style="float:right"><p>$</p></label></div><div class="actions">    ';
 if (!this.dealExternal.get('qty_user_can_buy')==0) {;
__p += '    <p class="conexSeg">La informaci&oacute;n se enviara por <span>conexi&oacute;n segura</span></p>     <input type="submit" value="Comprar" class="btnNaranja" />    ';
 } else { ;
__p += '    <p class="conexSeg">Has superado el l&iacute;mite de compra por usuario </p>    ';
 };
__p += '</div>';

}
return __p
};

}).call(this);