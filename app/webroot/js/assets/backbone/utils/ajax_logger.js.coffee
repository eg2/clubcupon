class Buy.Utils.AjaxLogger extends Backbone.Router

  @TRACE = 99
  @DEBUG = 7
  @INFO = 6
  @NOTICE = 5
  @ERROR = 3

  @AJAX_LOG_URI = "/api/api_logs/save.json"
  @logHistory = []

  initialize: (options) ->
    console.debug "initalize #{JSON.stringify(options)}"
    @options = options
    if @options.ajaxLogUri?
      @ajaxLogUri = @options.ajaxLogUri
    else
      @ajaxLogUri = Buy.Utils.AjaxLogger.AJAX_LOG_URI
    if @options.defaultSeverity?
      @defaultSeverity = @options.defaultSeverity
    else
      @defaultSeverity = Buy.Utils.AjaxLogger.TRACE
    if options.name?
      @name = options.name
    else
      @name = 'default'
    if options.mdc?
      @mdc = options.mdc
    else
      @mdc = {}

  addMDC: (key, value) ->
    @mdc[key] = value

  setName: (name) ->
    @name = name

  log: (message, extraInfo, severity, name, mdc) ->
    name = @name unless name?
    mdc = @mdc unless mdc?
    severity = Buy.Utils.AjaxLogger.TRACE unless severity?
    logData =
      message: message
      extraInfo: extraInfo
      severity: severity
      name: name
      mdc: mdc
    console.debug "log(#{JSON.stringify(logData)})"
    Buy.Utils.AjaxLogger.logHistory.push = logData
    $.ajax
      url: @ajaxLogUri
      type: 'post'
      dataType: 'json'
      data: {log: logData}
      success: (data, textStatus, jqXHR) ->
        console.log "AjaxLogger::log {} Envio del log al server exitoso. %s"
      error: (jqXHR, textStatus, errorThrown) ->
        console.log(
          "AjaxLogger::log {} Error en el envio del log al server. %s",
          JSON.stringify({
            textStatus: textStatus
            errorThrown: errorThrown
          })
        )

  notice: (message, extraInfo, name, mdc) ->
    this.log(message, extraInfo, Buy.Utils.AjaxLogger.NOTICE , name, mdc)
  info: (message, extraInfo, name, mdc) ->
    this.log(message, extraInfo, Buy.Utils.AjaxLogger.INFO , name, mdc)
  debug: (message, extraInfo, name, mdc) ->
    this.log(message, extraInfo, Buy.Utils.AjaxLogger.DEBUG , name, mdc)
  error: (message, extraInfo, name, mdc) ->
    this.log(message, extraInfo, Buy.Utils.AjaxLogger.ERROR , name, mdc)

