Buy.Views.Buy ||= {}

class Buy.Views.Buy.DealDetailView extends Backbone.View
  template: JST["backbone/templates/buy/deal_detail"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @deal = options.deal # @deal.bind 'all', @render
    @render()

  render: =>
    $(@el).html(@template(deal: @deal))
