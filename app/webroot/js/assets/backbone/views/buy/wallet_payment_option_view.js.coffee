Buy.Views.Buy ||= {}

class Buy.Views.Buy.WalletPaymentOptionView extends Backbone.View
  template: JST["backbone/templates/buy/wallet_payment_option"]

  events: ->
    "click": "walletPaymentSelected"

  initialize: (options) ->
    @options = options
    @el = options.el
    @user = options.user
    @render()

  render: =>
    $(@el).html(@template(@user))
    @

  walletPaymentSelected: (e) ->
    e.preventDefault()
    e.stopPropagation()
    router.navigate('buyWithWallet', true)
