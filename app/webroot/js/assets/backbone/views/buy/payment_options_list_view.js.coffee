Buy.Views.Buy ||= {}

class Buy.Views.Buy.PaymentOptionsListView extends Backbone.View
  template: JST["backbone/templates/buy/payment_options_list"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @paymentOptions = options.paymentOptions
    @render()

  render: ->
    $(@el).html(@template(@))
    @renderPaymentOptions()

  renderPaymentOptions: ->
    @paymentOptions.each(@renderPaymentOption)

  renderPaymentOption: (paymentOption) ->
    view = new Buy.Views.Buy.PaymentOptionView
      paymentOption: paymentOption
    @$('ul#payment-options', @el).append(view.render().el)

