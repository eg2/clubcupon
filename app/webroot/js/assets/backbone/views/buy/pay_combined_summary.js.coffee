Buy.Views.Buy ||= {}

class Buy.Views.Buy.PayCombinedSummaryView extends Backbone.View
  template: JST["backbone/templates/buy/pay_combined_summary"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @user = options.user
    @dealExternal = options.dealExternal
    @render()

  render: =>
    $(@el).html @template(@)
    @
