Buy.Views.Buy ||= {}

class Buy.Views.Buy.BuyFormView extends Backbone.View
  template: JST["backbone/templates/buy/buy_form"]

  events: ->
    "change #amount": "changeAmount"
    "submit": "buy"

  initialize: (options) ->
    @options = options
    @el = options.el
    @dealExternal = options.dealExternal

    @dealExternal.bind "change:errors", () => @render()
    @dealExternal.bind "change:total_price", () =>
      $('#total_price', @el).val(@dealExternal.get 'total_price')
    @render()

  render: =>
    $(@el).html(@template(@dealExternal))
    @$('form').backboneLink(@dealExternal)
    @

  changeAmount: ->
    @newAmount = parseInt $('#amount', @el).val()
    if(@newAmount > 0)
      @dealExternal.set "amount", @newAmount
    @

  buy: (e) ->
    e.preventDefault()
    e.stopPropagation()
    router.navigate('selectPaymentMethod', true)
