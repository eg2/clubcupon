Buy.Views.Buy ||= {}

class Buy.Views.Buy.PaymentOptionView extends Backbone.View
  tagName: 'li'
  template: JST["backbone/templates/buy/payment_option"]

  events: ->
    "click": "paymentOptionSelected"

  initialize: (options) ->
    @options = options
    @paymentOption = options.paymentOption
    @

  render: ->
    $(@el).html @template @
    @

  paymentOptionSelected: (e) ->
    e.preventDefault()
    e.stopPropagation()

    if !@paymentOption.get("PaymentPlanItem")
        router.navigate("buyWithPaymentOption/#{@paymentOption.get('id')}", true)
    else
        console.log "::::: Mostrar Plan de Pagos :::::"
        router.navigate("selectPaymentPlanItem/#{@paymentOption.get('id')}",  true)
        
   
    
    

