Buy.Views.Buy ||= {}

class Buy.Views.Buy.SuccessBuyView extends Backbone.View
  template: JST["backbone/templates/buy/success_buy"]
  STATUS_SUCCESS: "success"
  STATUS_ERROR: "error"
  MESSAGE_SUCCESS: "Su compra fue realizada con exito!"
  MESSAGE_ERROR: "Error al comprar!"

  initialize: (options) ->
    @options = options
    @el = options.el
    @deal = options.deal
    if options.status == @STATUS_SUCCESS
      @message = @MESSAGE_SUCCESS
    else
      @message = @MESSAGE_ERROR
    @render()

  render: =>
    $(@el).html @template @

    new Buy.Views.Buy.DealDetailView
      deal: @deal
      el: "#deal-detail"
