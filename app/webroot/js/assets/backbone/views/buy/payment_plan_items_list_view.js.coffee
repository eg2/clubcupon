Buy.Views.Buy ||= {}

class Buy.Views.Buy.PaymentPlanItemsListView extends Backbone.View
  template: JST["backbone/templates/buy/payment_plan_items_list"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @paymentPlanItems = new Buy.Collections.PaymentPlanItemsCollection(
        JSON.parse(JSON.stringify(options.paymentPlanItem)))
    @render()

  render: ->
    console.log("::::::: View List Items ::::::")
    $(@el).html(@template(@))
    @renderPaymentPlanItems()
    

  renderPaymentPlanItems: ->
    @paymentPlanItems.each(@renderPaymentPlanItem)
   
  renderPaymentPlanItem: (paymentItem) ->
   view = new Buy.Views.Buy.PaymentPlanItemView
                  PaymentPlanItem: paymentItem
   @$('ul#payment-plan-items', @el).append(view.render().el)
  

