Buy.Views.Buy ||= {}

class Buy.Views.Buy.SelectPaymentPlanItemView extends Backbone.View
  template: JST["backbone/templates/buy/select_payment_plan_item"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @user = options.user
    @dealExternal = options.dealExternal
    @paymentOptions = options.paymentOptions
    @PaymentPlanItems = options.PaymentPlanItems
    @render()

  render: ->
    $(@el).html(@template(@))
    console.log ":::::: SelectPaymentPlanItemView ::::::"

    new Buy.Views.Buy.PaymentPlanItemsListView
      el: "#payment-plan-items"
      paymentPlanItem: @PaymentPlanItems