Buy.Views.Buy ||= {}

class Buy.Views.Buy.InitBuyView extends Backbone.View
  template: JST["backbone/templates/buy/init_buy"]

  #  events: #  '': ''

  initialize: (options) ->
    @options = options
    @deal = options.deal
    @dealExternal = options.dealExternal
    @user = options.user
    @el = options.el
    @render()

  render: ->
    $(@el).html(@template())
    new Buy.Views.Buy.DealDetailView
      deal: @deal
      el: "#deal-detail"
    
    if !@deal.get('buy_max_quantity_per_user')
        new Buy.Views.Buy.BuyFormView
            el: "#buy-form"
            dealExternal: @dealExternal
            deal: @deal
            user: @user
    else
        new Buy.Views.Buy.BuyFormSelectView
            el: "#buy-form"
            dealExternal: @dealExternal
            deal: @deal
            user: @user

