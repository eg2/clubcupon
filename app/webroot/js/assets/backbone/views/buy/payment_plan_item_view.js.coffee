Buy.Views.Buy ||= {}

class Buy.Views.Buy.PaymentPlanItemView extends Backbone.View
  tagName: 'li'
  template: JST["backbone/templates/buy/payment_plan_item"]

  events: ->
    "click": "paymentPlanItemSelected"

  initialize: (options) ->
    console.log (":::::: PaymentPlanItem View ::::::")
    @PaymentPlanItem = options.PaymentPlanItem
    @id = @PaymentPlanItem.id
    @name = @PaymentPlanItem.name
    @paymentOptionId = @PaymentPlanItem.PaymentOptionId

  render: ->
    $(@el).html @template @
    @

  paymentPlanItemSelected: (e) ->
    e.preventDefault()
    e.stopPropagation()
    console.log(':::::: paymentPlanItemSelected :::::')
    router.navigate("buyWithPaymentPlanItem/#{@PaymentPlanItem.get('id')}/paymentOptionId/#{@PaymentPlanItem.get('PaymentOptionId')}", true)
    
