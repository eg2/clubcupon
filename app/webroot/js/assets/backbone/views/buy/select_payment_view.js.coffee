Buy.Views.Buy ||= {}

class Buy.Views.Buy.SelectPaymentView extends Backbone.View
  template: JST["backbone/templates/buy/select_payment"]

  initialize: (options) ->
    @options = options
    @el = options.el
    @user = options.user
    @dealExternal = options.dealExternal
    @paymentOptions = options.paymentOptions
    @combined = options.combined
    @render()

  render: ->
    $(@el).html(@template(@))

    if @canUsesWallet()
      new Buy.Views.Buy.WalletPaymentOptionView
        el: "#wallet-payment-option"
        user: @user

    if @isCombined()
      new Buy.Views.Buy.PayCombinedSummaryView
        el: "#pay-combined-summary"
        user: @user
        dealExternal: @dealExternal

    new Buy.Views.Buy.PaymentOptionsListView
      el: "#payment-options"
      paymentOptions: @paymentOptions

  canUsesWallet: () -> @user.get("wallet") > 0 && !@isCombined()
  isCombined: () -> @combined
