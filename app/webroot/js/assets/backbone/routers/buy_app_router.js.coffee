class Buy.Routers.BuyAppRouter extends Backbone.Router
  api_buy_uri = "/api/api_deals/do_buy.json?"

  initialize: (options) ->
    @ajaxLogger = new Buy.Utils.AjaxLogger {name: "BuyAppRouter"}
    try
      console.log ":::::buyApp::initialize::BEGIN::::::"
      throw new Error("deal must not be empty") unless options.deal?
      throw new Error("user must not be empty") unless options.user?
      @options = options
      @el = options.el
      @deal =
        new Buy.Models.Deal JSON.parse JSON.stringify options.deal
      @user =
        new Buy.Models.User JSON.parse JSON.stringify options.user
      if options.dealExternal?
        @dealExternal = new Buy.Models.DealExternal JSON.parse JSON.stringify options.dealExternal.DealExternal
      else
        @dealExternal = new Buy.Models.DealExternal
          price: @deal.get "price"
          deal_id: @deal.get "id"
          user_id: @user.get "id"
      if options.walletPaymentOptionId?
        @walletPaymentOptionId = options.walletPaymentOptionId
        console.log(":::::Opcion de pago monedero activo:::::: #{@walletPaymentOptionId}");
      
      @paymentOptions = new Buy.Collections.PaymentOptionsCollection JSON.parse JSON.stringify options.paymentOptions
      @bacCrearPagoSeguroUri = options.bacCrearPagoSeguroUri
      @bacCrearTransactionUri = options.bacCrearTransactionUri
      @ajaxLogger = new Buy.Utils.AjaxLogger {name: "BuyAppRouter"}
      @ajaxLogger.addMDC('dealId', @deal.get('id'))
      @ajaxLogger.addMDC('userId', @user.get('id'))
      @ajaxLogger.addMDC('userEmail', @user.get('email'))
      console.log ":::::buyApp::initialize::END::::::"
    catch e
      @ajaxLogger.error(
        'Error al iniciar la pantalla de compra mobile',
          {
            exception:
              type: e.type
              arguments: e.arguments
              message: e.message
              stack: e.stack
          }
      )
      console.log "ERROR:\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
      console.log "\nSTACKTRACE:\n", e.stack
      window.router.navigate 'errorBuy', true


  routes:
    ""   : "initBuy"
    ".*" : "initBuy"
    "selectPaymentMethod" : "selectPaymentMethod"
    "selectPaymentMethod/combined/:combined" : "selectPaymentMethod"
    "buyWithWallet" : "buyWithWallet"
    "buyWithPaymentOption/:paymentOptionId" : "buyWithPaymentOption"
    "doBuy" : "doBuy"
    "successBuy" : "successBuy"
    "errorBuy" : "errorBuy"
    "doBacLitebox": "doBacLitebox"
    "doBacFormAutoSubmit": "doBacFormAutoSubmit"
    "selectPaymentPlanItem/:paymentOptionId" : "selectPaymentPlanItem"
    "buyWithPaymentPlanItem/:paymentPlanItemId/paymentOptionId/:paymentOptionId" : "buyWithPaymentPlanItem"

  notificationError: (message) ->
    @notification message, "notification_error"

  notificationProgress: (message) ->
    @notification message, "notification_loading"

  notificationSuccess: (message) ->
    @notification message, "notification_success"

  notification: (message, cssClass) ->
    "<div class=\"#{cssClass}\"><span>#{message}</span></div>"



  initBuy: ->
    $(@el).append @notificationProgress("Cargando...")
    console.log ":::::buyApp::initBuy::::::::"
    @ajaxLogger.setName "BuyAppRouter[initBuy]"
    @appView = new Buy.Views.Buy.InitBuyView
      el: @el
      deal: @deal
      dealExternal: @dealExternal
      user: @user

  selectPaymentMethod: (combined=0) ->
    $(@el).append @notificationProgress("Cargando Opciones de Pago...")
    console.log ":::::buyApp::selectPaymentMethod::::::::"
    @ajaxLogger.setName "BuyAppRouter[selectPaymentMethod]"
    @appView = new Buy.Views.Buy.SelectPaymentView
      el: @el
      user: @user
      dealExternal: @dealExternal
      paymentOptions: @paymentOptions
      combined: combined
    @appView.render()

  buyWithWallet: ->
    console.log ":::::buyApp::buyWithWallet::::::::"
    @ajaxLogger.setName "BuyAppRouter[buyWithWallet]"
    if @user.get("wallet") >= @dealExternal.get("total_price")
      @dealExternal.set 'payment_option_id', @walletPaymentOptionId
      @dealExternal.set 'is_wallet', 1
      @navigate 'doBuy', true
    else
      @dealExternal.set 'combined', 1
      @navigate 'selectPaymentMethod/combined/1', true

  buyWithPaymentOption: (paymentOptionId) ->
    console.log ":::::buyApp::buyWithPaymentOption::::::::"
    @ajaxLogger.setName "BuyAppRouter[buyWithPaymentOption]"
    console.log paymentOptionId
    @dealExternal.set 'payment_option_id', paymentOptionId
    @navigate 'doBuy', true

  buyWithPaymentPlanItem: (paymentPlanItemId, paymentOptionId) ->
    console.log ":::::buyApp::buyWithPaymentOption::::::::"
    @ajaxLogger.setName "BuyAppRouter[buyWithPaymentPlanItem]"
    @dealExternal.set 'payment_option_id', paymentOptionId
    @dealExternal.set 'payment_plan_option_id', paymentPlanItemId
    @navigate 'doBuy', true

  selectPaymentPlanItem: (paymentOptionId) ->
    console.log ":::::buyApp::selectPaymentPlanItem::::::::"
    console.log ":::::: Buscando Planes de Pagos ::::::"
    $(@el).append @notificationProgress("Cargando Planes de Pago...")    
    
    PaymentPlanItems = @getPaymentPlanItemsbyOptionId(paymentOptionId)
    @appView = new Buy.Views.Buy.SelectPaymentPlanItemView
      el: @el
      user: @user
      dealExternal: @dealExternal
      paymentOptions: @paymentOptions
      PaymentPlanItems: PaymentPlanItems
      
    @appView.render()
    
    
    
  getPaymentPlanItemsbyOptionId: (PaymentOptionId) ->
    
    for paymentOption in @options.paymentOptions
       if paymentOption.id is PaymentOptionId
           paymentPlanItems = paymentOption.PaymentPlanItem
           return paymentPlanItems;
    

  doBuy: ->
    $(@el).append @notificationProgress("Procesando Compra...")
    console.log ":::::buyApp::doBuy::::::::"
    @ajaxLogger.setName "BuyAppRouter[doBuy]"
    console.log jsDump.parse
      user: @user.attributes
      deal: @deal.attributes
      dealExternal: @dealExternal.attributes
    console.log ".......REST::BUY::SERVER......."
    data =
      token: @user.get 'token'
      deal_id: @deal.get 'id'
      is_combined: @dealExternal.get 'combined'
      payment_option_id: @dealExternal.get 'payment_option_id'
      quantity: @dealExternal.get 'amount'
      payment_plan_item_id: @dealExternal.get 'payment_plan_option_id'
    console.log data
    $.ajax
      url: api_buy_uri+"token=#{@user.get 'token' }"
      type: 'post'
      dataType: 'json'
      data: data
      success: (data, textStatus, jqXHR) ->
        try
          @ajaxLogger = window.router.ajaxLogger
          console.log ".......REST::BUY::SERVER...SUCCESS:"
          console.log jsDump.parse data
          console.log "deal external ::::::::::::::::::"
          console.log window.router.dealExternal.get 'is_wallet'
          unless data.dealExternal.DealExternal.id?
            @ajaxLogger.error(
              'No se a recibido el DealExternal desde la generacion de comprar mobile',
              {
                data: data
                textStatus: textStatus
              }
            )
            window.router.navigate 'errorBuy', true

          window.router.dealExternal = new Buy.Models.DealExternal JSON.parse JSON.stringify data.dealExternal.DealExternal
          @ajaxLogger.addMDC('dealExternalId', window.router.dealExternal.get('id'))
          console.log "dealOptionId #{window.router.dealExternal.get('payment_option_id')}; walletOptionId: #{}"
          if window.router.dealExternal.get('payment_option_id') == window.router.walletPaymentOptionId
            @ajaxLogger.log("Compra con Monedero Exitosa")
            window.router.navigate 'successBuy', true
          else
            window.router.dealExternal.bacInfo = data.bac_info
       #     if data.use_litebox == true
       #       window.router.navigate 'doBacLitebox', true
       #     else
            window.router.navigate 'doBacFormAutoSubmit', true
        catch e
          @ajaxLogger.error(
            'Error al intentar generar la compra mobile',
            {
              exception:
                type: e.type
                arguments: e.arguments
                message: e.message
                stack: e.stack
            }
          )
          console.log "ERROR:\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
          console.log "\nSTACKTRACE:\n", e.stack
          window.router.navigate 'errorBuy', true

  successBuy: ->
    console.log ":::::buyApp::successBuy::::::::"
    @appView = new Buy.Views.Buy.SuccessBuyView
      el: @el
      deal: @deal
      status: "success"
    @appView.render()

  errorBuy: ->
    console.log ":::::buyApp::errorBuy::::::::"
    @appView = new Buy.Views.Buy.SuccessBuyView
      el: @el
      deal: @deal
      status: "error"
    @appView.render()

  doBacLitebox: ->
    @ajaxLogger.setName "BuyAppRouter[doBacLitebox]"
    try
      $(@el).append @notificationProgress("Cargando Mercado Pago...")
      console.log ":::::buyApp::doBacLitebox:::::::"
      data =
        ID_GATEWAY: @dealExternal.bacInfo.id_gateway
        ID_PAGO: @dealExternal.bacInfo.token
        ID_PORTAL: @dealExternal.bacInfo.id_portal
        SECURE_HASH: @dealExternal.bacInfo.secure_hash
      @ajaxLogger.info("Compra con LiteBox, llamando al LiteBox de Pago",
        {
          data: data
          bacUri: @bacCrearPagoSeguroUri
        })
      crearPagoLiteBox data, @bacCrearPagoSeguroUri
    catch e
      @ajaxLogger.error(
        'Error al intentar abrir el litebox de pago mobile',
        {
          exception:
            type: e.type
            arguments: e.arguments
            message: e.message
            stack: e.stack
        }
      )
      console.log "ERROR:\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
      console.log "\nSTACKTRACE:\n", e.stack
      window.router.navigate 'errorBuy', true


  doBacFormAutoSubmit: ->
    @ajaxLogger.setName "BuyAppRouter[doBacLitebox]"
    console.log ":::::buyApp::doBacFormAutoSubmit:::::"
    @ajaxLogger.info("Compra con Form AutoSubmit, submitenado formulario de Pago",
      {
        dealExternal: @dealExternal.bacInfo
        bacUri: @bacCrearTransactionUri
      })

    $(@el).append @notificationProgress("Redireccionando a Mercado Pago...")
    bacForm = "
      <form action=\"#{@bacCrearPagoSeguroUri}\" method=\"post\" id=\"bac-form\">
          <input type = \"hidden\" name = \"SECURE_HASH\" id=\"SECURE_HASH\" value = \"#{@dealExternal.bacInfo.secure_hash}\"/>
          <input type = \"hidden\" name = \"ID_GATEWAY\"    id=\"ID_GATEWAY\"    value = \"#{@dealExternal.bacInfo.id_gateway}\" />
          <input type = \"hidden\" name = \"ID_PAGO\"     id=\"ID_PAGO\"         value = \"#{@dealExternal.bacInfo.token}\" />
          <input type = \"hidden\" name = \"ID_PORTAL\"                          value = \"#{@dealExternal.bacInfo.id_portal}\" />
          <noscript>
              <input type=\"submit\" value=\"Enviar\" />
          </noscript>
      </form>
    "
    console.log bacForm
    $(@el).append bacForm
    $('#bac-form').submit()



window.callbackMP = (json) ->
  @ajaxLogger.setName "BuyAppRouter[callBackMP]"
  try
    switch json.collection_status
      when "approved"
        console.log '::::: MP :::: PAGO::ACREDITADO ::::'
        @ajaxLogger.notice(
          'El pago esta acreditado para el Litebox de pago mobile.',
          json
        )
        window.router.navigate 'successBuy', true
      when "pending"
        console.log '::::: MP :::: PAGO::PENDIENTE ::::'
        @ajaxLogger.notice(
          'El pago esta pendinte para el Litebox de pago mobile.',
          json
        )
        window.router.navigate 'successBuy', true
      when "in_progress"
        console.log '::::: MP :::: PAGO::ENPROGRESO ::::'
        @ajaxLogger.notice(
          'El pago esta en progreso para el Litebox de pago mobile.',
          json
        )
        window.router.navigate 'successBuy', true
      when "rejected"
        console.log '::::: MP :::: PAGO::RECHAZADO ::::'
        @ajaxLogger.notice(
          'El pago fue rechazado por el Litebox de pago mobile.',
          json
        )
        window.router.navigate 'errorBuy', true
      else
        console.log '::::: MP :::: PAGO::NOPROCESADO ::::'
        console.log "status #{json.collection_status}"
        @ajaxLogger.error(
          'Error se recibio una respuesta no esperada del litebox de pago mobile',
          json
        )
        window.router.navigate 'errorBuy', true
  catch e
    console.log "ERROR:\nType: #{e.type}\nArgs: #{e.arguments}\nMessage: #{e.message}"
    console.log "\nSTACKTRACE:\n", e.stack
    @ajaxLogger.error(
      'Error recibido del litebox de pago mobile',
          {
            exception:
              type: e.type
              arguments: e.arguments
              message: e.message
              stack: e.stack
          }
    )
    window.router.navigate 'errorBuy', true

