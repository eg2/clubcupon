class Buy.Models.PaymentPlanItem extends Backbone.Model
  paramRoot: 'payment_plan_item'

  defaults:
    name: null
    id: null

  initialize: (options) ->
      console.log(':::::Instanció Modelo PaymentPlanItem:::::')
      @options = options
      @name = @options.name
      @id = @options.id
      @paymentOptionID = @options.PaymentOptionId
      @ 

class Buy.Collections.PaymentPlanItemsCollection extends Backbone.Collection
  model: Buy.Models.PaymentPlanItem
  url: '/payment_plan_items'
