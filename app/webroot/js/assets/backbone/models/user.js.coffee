class Buy.Models.User extends Backbone.Model
  paramRoot: 'user'

  defaults:
    id: null
    email: null
    token: null
    wallet: null

class Buy.Collections.UsersCollection extends Backbone.Collection
  model: Buy.Models.User
  url: '/users'
