class Buy.Models.PaymentOption extends Backbone.Model
  paramRoot: 'payment_option'

  defaults:
    title: null
    id: null
    PaymentPlanItems: null

  initialize: (options={}) ->
      throw new Error("id must not be empty") unless options.id?
      @PaymentPlanItems = @.get("PaymentPlanItem")
      console.log(@PaymentPlanItems)
      if @PaymentPlanItems
          @createPlanItems()
      @

  createPlanItems: ()->
      @createPlanItem(paymentPlanItem) for paymentPlanItem in @PaymentPlanItems
      @
  createPlanItem: (paymentPlanItem) ->
      new Buy.Models.PaymentPlanItem paymentPlanItem
      @

class Buy.Collections.PaymentOptionsCollection extends Backbone.Collection
  model: Buy.Models.PaymentOption
  url: '/payment_options'
