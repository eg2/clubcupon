// Generated by CoffeeScript 1.6.3
(function() {
  var _ref, _ref1,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Buy.Models.Deal = (function(_super) {
    __extends(Deal, _super);

    function Deal() {
      _ref = Deal.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Deal.prototype.paramRoot = 'deal';

    Deal.prototype.defaults = {
      id: null,
      title: null,
      description: null,
      price: null
    };

    Deal.prototype.getImg = function() {
      console.log("image url:::::::::::::::");
      return "/api/img/iponorm_large_thumb/Deal/" + (this.get('id')) + ".jpg";
    };

    return Deal;

  })(Backbone.Model);

  Buy.Collections.DealsCollection = (function(_super) {
    __extends(DealsCollection, _super);

    function DealsCollection() {
      _ref1 = DealsCollection.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    DealsCollection.prototype.model = Buy.Models.Deal;

    DealsCollection.prototype.url = '/deals';

    return DealsCollection;

  })(Backbone.Collection);

}).call(this);
