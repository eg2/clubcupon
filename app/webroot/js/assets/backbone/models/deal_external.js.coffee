class Buy.Models.DealExternal extends Backbone.Model
  paramRoot: 'deal_external'

  defaults: ->
    id: null
    deal_id: null
    user_id: null
    amount: 1
    combined: 0
    payment_option_id: null
    price: null
    total_price: null


  initialize: (options={}) ->
    unless options.id?
      throw new Error("price must be greater than 0") unless options.price > 0
      throw new Error("deal_id must not be empty") unless options.deal_id?
      throw new Error("user_id must not be empty") unless options.user_id?

    if (options.price? && options.amount?)
      @set total_price: options.price * options.amount

    @bind 'change:price', @calculateTotal
    @bind 'change:amount', @calculateTotal

  calculateTotal: ->
    @set 'total_price', @get('price') * @get('amount')


class Buy.Collections.DealExternalsCollection extends Backbone.Collection
  model: Buy.Models.DealExternal
  url: '/deal_externals'
