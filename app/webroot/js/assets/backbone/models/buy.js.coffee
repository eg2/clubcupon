class Buy.Models.Buy extends Backbone.Model
  paramRoot: 'buy'

  defaults:
    id: null
    deal_id: null
    user_id: null
    amount: null
    payment_option_id: null
    price: null
    total_price: null

class Buy.Collections.BuysCollection extends Backbone.Collection
  model: Buy.Models.Buy
  url: '/buys'
