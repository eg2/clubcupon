function doFacebookLogin() {
  FB.login(function(response) {
    if (response.session) {
      if (response.perms) {
        var goTo = '/users/login';
        goTo += '?required_facebook_login=true&f='+ window.location.href;
        window.location.href = goTo;
      } else {
        alert("Debes aceptar los permisos extendidos para poder acceder al sitio. Intentalo nuevamente.");
      }
    }
  }, {
    perms:'email'
  });
}

$(document).ready(function(){
  var fb_api_key = __cfg('api_key');
  if (typeof(FB) != 'undefined' && __cfg('api_key') != false) {
    FB.init({
      appId: __cfg('api_key'),
      status: false,
      cookie: true,
      xfbml: false
    });
  }
});


function fbFeed_deal(name, _href, percentage) {
  if (_href == null) {
    _href = 'http://www.clubcupon.com.ar';
  }
  var image_src = $('#deal_image').attr('src') + '';
  if(image_src[0] == "/") {
    image_src = image_src.substr(1);
  }
  var img = __cfg('path_absolute') + image_src;
  var title = "Club Cupon - Todos los días una alegría";
  var _caption = "{*actor*} compartió la oferta \"" + name + "\". Aprovecha el " + percentage + "% de descuento ahora mismo.";

  FB.ui(
  {
    method: 'stream.publish',
    attachment: {
      name: title,
      caption: _caption,
      href: _href,
      media:[{
        type:'image',
        src:img,
        href:_href
      }]
    },
    action_links: [
    {
      text: 'Ver Cupón',
      href: _href
    }
    ]
    ,
    user_message_prompt: 'Comparte con tus amigos esta novedad'
  }
  );
}