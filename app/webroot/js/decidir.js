/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function decidirIsNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
}


function decidirCheckAmountAndSubmit(montoDisplayId,montoRealId) {
      montoDisplay = $('#'+montoDisplayId).val();
      if(montoDisplay.indexOf(",") != -1) {
        alert('Utilice el punto como separador decimal.');
        return false;
      }
      if((montoDisplay.split(".").length > 2) || (montoDisplay.split(".").length == 2 && (montoDisplay.split(".")[1].length > 2) )) {
        alert('Utilice 2 lugares decimales para introducir el monto.');
        return false;
      }
      var montoReal = $('#'+montoRealId);
      montoReal.val(Math.round(parseFloat($('#'+montoDisplayId).val()) * 100));
      if($('#'+montoRealId).val() <= 0) {
        alert('Debes especificar un monto mayor a cero.');
        return false;
      }
      if(!decidirIsNumeric($('#'+montoDisplayId).val())) {
        alert('El monto introducido no es válido.');
        return false;
      }
      return true;
}






