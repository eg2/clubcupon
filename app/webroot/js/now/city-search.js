$(document).ready(function(){
    
        //deshabilitamos la tecla ENTER
        $(window).keypress(function (event){ 
            if (event.which == 13)
            { 
                return false;
            } 
        });
    
        //LLevamos a cero el form.
        disableCitySearchSubmitButton();
    
	$('input.text1').unbind('keyup');
	$('input.text1').keyup(function(e){
		clearTimeout(window.to);
		window.to = setTimeout("seekAddress('"+$(e.target).attr('value')+"');",500);
	});
        
        //Opciones desplegadas
	$("#city-ops a").live('click',function(e){
                enableCitySearchSubmitButton();
    console.log($(e));
    console.log($(e.target).text());
    console.log('title: ' + $(e.target).attr('title'));
    console.log('value: ' + $(e.target).text());
    console.log('rel: ' + $(e.target).attr('rel'));
		$("input.text1").attr('value',$(e.target).text());
		$("input.text1").attr('rel',$(e.target).text());
		$("input#NowDealLocation").attr('value',$(e.target).text());
		$("input#NowDealLocationId").attr('value',$(e.target).attr('rel'));
		$("input#NowDealLocationId").attr('rel',$(e.target).attr('rel'));
		$("#NowDealLatlng").attr('value',$(e.target).attr('rel'));
		$("#city-ops").slideUp();
		//$("#catoverlay").fadeOut();
	});
	
	
	$("a.now-category").unbind('click');
	$("a.now-category").click(function(e){
		e.preventDefault();
		$("input#NowDealCategory").attr('value',$(e.target).attr('rel'));
		$("input#NowDealCategory").attr('rel',$(e.target).attr('name'));
                
                if($(this).attr('id') == 'cat-todas')
                {
                    disableCitySearchSubmitButton();
                    
                    $("#city-ops").slideUp();
                }
                
                
		$("form#NowDealIndexForm").submit();
	});
        
        
        $('#NowDealLocation').focus(function(){
          disableCitySearchSubmitButton();
        });
	
	//$("#catoverlay").fadeTo(400,0.7); // NO SE USA MAS!!
	
});

function disableCitySearchSubmitButton()
{
    $('#NowDealLocation').val('');
    $("input.text1").attr('value','');
    $("input.text1").attr('rel','Barrio/Ciudad, Localidad');
    $("#NowDealLatlng").attr('value','');
    $('#submitCitySearch').attr('disabled', 'disabled');
    $('#submitCitySearch').attr('style', 'background:#fff; border:grey; color:#ccc;');
}
function enableCitySearchSubmitButton()
{
    $('#submitCitySearch').removeAttr("disabled"); 
    $('#submitCitySearch').attr('style', 'background: url("/img/now/btn-bg4.png") repeat-x scroll 0 0 #00A2CF; border: 1px solid #017CA6;');
}


var deals = new Array();
var lastsearch = "";
var norte = -33.26859;
var sur = -40.84775;
var este = -56.68250;
var oeste = -63.70517;

function seekAddress(addr){
    console.log("seekAddress... [ addr:" + addr + "]");
	  if (addr.length && lastsearch != addr) {
		  lastsearch = addr;
		  $('#city-ops').slideUp(function(){
        console.log("#city-ops >> slideUp...");
        fn_success = function(result) {
        console.log("localidades posibles recibidas...");

        neighbourhoodsLength = result.neighbourhoods.length;
        console.log(neighbourhoodsLength);
					$("#city-ops").html("");
					if (result.neighbourhoods != null && result.neighbourhoods.length > 0) {
            i = 0;
            console.log(result);
            console.log(result.neighbourhoods);
            for (var i = 0; i < neighbourhoodsLength; i++) {
							neighbourhood_el = $("<a href='#' rel='' title=''></a>");
              neighbourhood = result.neighbourhoods[i];
              console.log(i);
              console.log(neighbourhood.Neighbourhood.name);
              neighbourhood_name = neighbourhood.Neighbourhood.name;
							if(neighbourhood_name.length > 35) {
                neighbourhood_name = neighbourhood_name.substring(0,35) + '...';
              };
              $(neighbourhood_el).html(neighbourhood_name) ; //+ ' - ' + result.neighbourhoods[i].City.name);
              $(neighbourhood_el).attr(
                'title',
                neighbourhood.name
              );
              $(neighbourhood_el).attr(
                'rel',
                neighbourhood.Neighbourhood.id
              );
              //result.neighbourhoods[i].Neighbourhood.name // + ' - ' + result.neighbourhoods[i].City.name
              $("#city-ops").append(neighbourhood_el).append("<br>");
            }

						if($("#city-ops").html().length){
              console.log("se generaron localidades posibles para seleccionar, abriendo el selector...");
							$("#city-ops").slideDown();
              $("#NowDealLocationLoading").css("display", "none");
						} else {
              console.log("no se generaron localidades posibles para seleccionar...");
              $("#NowDealLocationLoading").css("display", "none");
            }

          } else {
					  console.log("No existen localidades para esa direccion");
            $("#NowDealLocationLoading").css("display", "none");
          }
        };
        url = cfg.cfg.path_absolute + 'api/api_neighbourhoods/neighbourhoods/phase:'+addr+'.json';
        console.log("url...."+url);
        params = {url: url, dataType: 'json', success: fn_success};
        console.log("consultando localidades posible: " + addr);
        $("#NowDealLocationLoading").css("display", "block");
        $.ajax(params);
		  });
    } else {
      console.log("la dirrecion ingresada es vacia o igual a la ultima...");
    }
}

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
      'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
      'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
      'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
      'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});
    
    while (length--) {
      method = methods[length];
    
      // Only stub undefined methods.
      if (!console[method]) {
        console[method] = noop;
      }
    }
}());

//https://maps.googleapis.com/maps/api/staticmap?path=color:0xFFFF00FF|fillcolor:0xFFFF0033|weight:2|-34.55,-58.50|-34.60,-58.50|-34.60,-58.45|-34.55,-58.45|-34.55,-58.50&size=512x512&zoom=13&sensor=false	
	
