/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/* rectangulo de la Ciudad  de Buenos Aires. */
var markersarr = new Array();
var terminocap=false;
var terminobs=false;
var norte = -34.345704;
var sur =  -35.090698;
var este =  -56.68250;
var oeste = -63.70517;
var argbounds;

function moveMap(addr){
    map.setZoom(8);
    markersarr = cleanoverlays(markersarr);
    $('#msgmap').remove();
    var geocoder = new google.maps.Geocoder();
    argbounds = new google.maps.LatLngBounds(new google.maps.LatLng(sur,oeste), new google.maps.LatLng(norte, este));
                                        
    if (geocoder) {
        terminocap=false;
        terminobs=false;
        geocoder.geocode({'address': addr + ",  Ciudad Autónoma de Buenos Aires, AR"}, function (resultscap, statuscap){
            console.log('resultscap.length: ' + resultscap.length);
            
            if (statuscap == google.maps.GeocoderStatus.OK && resultscap.length > 0) {
                setPositionMap(resultscap, argbounds, map, myOptions,'input#latitude','input#longitude');
                terminocap = true;
            }
        });
        
        geocoder.geocode({'address': addr + ",  Buenos Aires, AR"}, function (resultsbsas, status){
            console.log('resultsbsas.length: ' + resultsbsas.length);
            if (status == google.maps.GeocoderStatus.OK && resultsbsas.length > 0) {
                setPositionMap(resultsbsas, argbounds, map, myOptions,'input#latitude','input#longitude'); 
                terminobs = true;
            }
            else {
                alert('Error en obtener dirección, intente de nuevo. ');
            }
        });
        
        zoomear();
       
    }
    
}

function setPositionMap(results, argbounds, objmap, objoptions, inputlat, inputlog, arrmarkers) {
     console.log('detro de setposition map: '+ results.length);
     //console.log('0' + results[0].geometry.location );
     //console.log('1' + results[1].geometry.location );
     for(rs in results)  {
         if (results[rs].geometry){ //this for ie8
            if(argbounds.contains(results[rs].geometry.location)) {
              console.log('0' +  results[0].geometry.location);
              console.log('rs' + results[rs].geometry.location);
              var marker = createmarker(objmap, objoptions, results[rs].geometry.location);
              
              google.maps.event.addListener(marker,'click', function (ev) {
                                        objmap.setCenter(new google.maps.LatLng(ev.latLng.lat(),ev.latLng.lng()));
                                        $(inputlat).attr('value',ev.latLng.lat());
					$(inputlog).attr('value',ev.latLng.lng());
                                        map.setZoom(16);
					marker.setVisible(true);
					marker.setAnimation(google.maps.Animation.b);
                                        marker.setPosition(ev.latLng);
                                        ev.stop();
                                        });
              markersarr.push(marker);
             
              
            }
        }
     }
}

function createmarker(objMap, objMapOptions, objLocation){
   
    var marker = new google.maps.Marker({
                            position: objMapOptions.center,
                            map: objMap,
                            visible: false,
                            title: 'Aqui estoy!'});
                        
    marker.setPosition(objLocation);
    marker.setVisible(true);
    marker.setAnimation(google.maps.Animation.b);
    
    return marker;
}

function cleanoverlays(markersArray){
    
    if (markersArray.length > 0){
        for (i in markersArray) {
             console.log(markersArray[i]);
             if (i < markersArray.length) { //this for ie8
                markersArray[i].setMap(null);
             } //this for ie8
        }
    }
    return new Array();
}

 function zoomear(){
        if(terminocap && terminobs){

        var distance;
        if (markersarr.length === 2) {
            // verificando que las direcciones no sean las mismas
            var ptoA = new google.maps.LatLng(markersarr[0].position.lat(),markersarr[0].position.lng());
            var ptoB = new google.maps.LatLng(markersarr[1].position.lat(),markersarr[1].position.lng());
            distance = google.maps.geometry.spherical.computeDistanceBetween(ptoA, ptoB);
        }

        if((markersarr.length === 2 && distance === 0 ) || markersarr.length===1){
            // consiguió una sola direción, ambas búsquedas apunta a la misma direccion
            google.maps.event.addListenerOnce(map, 'idle', function() {
                        google.maps.event.trigger(map,'click',{latLng:new google.maps.LatLng(markersarr[0].position.lat(),markersarr[0].position.lng())});
            });
            map.setCenter(new google.maps.LatLng(markersarr[0].position.lat(),markersarr[0].position.lng()));
            map.setZoom(16);
            //map.panTo(new google.maps.LatLng(markersarr[0].position.lat(),markersarr[0].position.lng()));


        }
        else {
            // consiguió mas de una dirección, ambas búsquedas.
            $('.mapa').prepend('<p style="color: black" id="msgmap"> <b> Elegí tu sucursal de todos estos puntos: </b> </p> <br/>');
            map.setCenter(new google.maps.LatLng(-34.612, -58.450));
            map.setZoom(9);
        }
        }
        else{
            window.setTimeout("zoomear();", 250);
        }

}

function ajustarPunteroMapa(){
        if(($('#NowBranchStreet').attr('value') != $('#NowBranchStreet').attr('title')) && $.isNumeric($('#NowBranchNumber').attr('value'))){
            moveMap($('#NowBranchStreet').attr('value') + " " + $('#NowBranchNumber').attr('value'));
        }
}


