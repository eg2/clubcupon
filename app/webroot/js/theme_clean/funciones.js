$(document).ready(function() {


    //Cuenta regresiva de una oferta
    $(function(){

        //contenedor destino
        var target = $('.horas-num');

        //Recuperamos la fecha de cierra
        var end_date = $(this).find('#deal_countdown_amout').html();

        //Formateamos a milisegundo
        ts = (new Date()).getTime() + end_date * 1000; //diferencia

        //iteramos actualizando el mensaje, e imprimimos en pantalla
	$('.horas-num').countdown({
		timestamp	: ts,
		callback	: function(days, hours, minutes, seconds){
			var message = "";
			//smessage += checkDateFormat(days);
			message += checkDateFormat((days*24)+hours) + '<span>:</span>';
			message += checkDateFormat(minutes) + '<span>:</span>';
			message += checkDateFormat(seconds);
			target.html(message);
		}
	});
    });



         //$('.tiempo-cupon ul li').removeClass('hidden')

        $('.tiempo-cupon').slideDown('slow',function() {
            heightPlus       = $('.datos-empresa').height();
            buySideBarHeight = $('#buySideBar').height();
            recomendarHeight = $('.recomendar').height();
            //$('#buySideBar').height(heightPlus + buySideBarHeight+recomendarHeight).$('.datos-empresa').show();

            $('#buySideBar').animate({
                            height: heightPlus + buySideBarHeight+recomendarHeight
                            }, 500 , function(){
                                $('.datos-empresa').show();
                            });
                    }
                );


    /*
     * Formatea la hora a doble dígito
     */
    var checkDateFormat = function(stamp){
        if(stamp<10){
            tmp = stamp;
            stamp = '0' + tmp;
        }
        return stamp;
    }

    //Animacion container para desplegable
        //Apertura del desplegable de ciudades
        $(".city").click(function() {
            $(".desplegable").slideToggle(400);
        });

    //Cierre del desplegable de ciudades
        $(".btn-cerrar").click(function() {
            $(".desplegable").slideToggle(400);
        });

    //Cierre de alertas
        $("#successMessage").click(function() {
            $(this).slideToggle(400);
        });

        $("#errorMessage").click(function() {
            $(this).slideToggle(400);
        });

        $("#flashMessage").click(function() {
            $(this).slideToggle(400);
        });

    //Animacion container para ingresa-tucorreo
        $('#header a.text').click(function() {;
            $(".ingresa-tucorreo").slideToggle(50);
        });

        $('.ingresa-tucorreo span').click(function() {;
            $(".ingresa-tucorreo").slideToggle(50);
        });

//***********************************************************
//Mas info
//***********************************************************
	var altura      = "100%"
	var alturaB     = "100%"
	var alturaC     = "100%"


        var altura_bloque_home      = 770;
        var altura_nombre_oferta    = $('.h1-format').  outerHeight();
        var altura_subtitulo_oferta = $('.subtitle').   outerHeight();
        var altura_imagen_oferta    = $('#deal_image'). outerHeight();
        var altura_bloque_tabs      = $('.bloque-tabs').outerHeight();
        var alt_cerrado = altura_bloque_home- altura_nombre_oferta - altura_subtitulo_oferta - altura_imagen_oferta - altura_bloque_tabs-79;

	function heightfijo(){



		$('#conten').animate({
		opacity: 1,
		height: '100%'
		}, 1, function() {
		});

		altura=$('#conten').height()+"px";

                if($('#conten'). height()< alt_cerrado) {$('#conten'). parent().find('.vermascont'). hide();}
                if($('#contenB').height()< alt_cerrado) {$('#contenB').parent().find('.vermascont2').hide();}
                if($('#contenC').height()< alt_cerrado) {$('#contenC').parent().find('.vermascont3').hide();}

		$('#conten').animate({
		opacity: 1,
		height: alt_cerrado + 'px'
		}, 1, function() {
		document.getElementById('grupocontenidos').style.position='static';
		document.getElementById('grupocontenidos').style.visibility='visible';
		});

                //para calcular el segundo Tab
		$('#contenB').animate({
		opacity: 1,
		height: '100%'
		}, 1, function() {
		});


		alturaB=$('#contenB').height()+"px";

		$('#contenB').animate({
		opacity: 1,
		height: alt_cerrado + 'px'
		}, 1, function() {
		document.getElementById('grupocontenidosB').style.position='static';
		document.getElementById('grupocontenidosB').style.visibility='visible';

		});

                //para calcular el tercer Tab
		$('#contenC').animate({
		opacity: 1,
		height: '100%'
		}, 1, function() {
		});


		alturaC=$('#contenC').height()+"px";

		$('#contenC').animate({
		opacity: 1,
		height: alt_cerrado + 'px'
		}, 1, function() {
		document.getElementById('grupocontenidosC').style.position='static';
		document.getElementById('grupocontenidosC').style.visibility='visible';

		});
	}

//ejecutar funcion para calcular Height
	heightfijo()

        //mostrar primer Tab
	$(".vermascont").click(function() {
        var obj = document.getElementById('conten');
		if(obj.style.height == altura){
			contenedor = document.getElementById('verm');
			contenedor.innerHTML = 'Ver Más';
			$('#conten').animate({
			opacity: 1,
			height: alt_cerrado + 'px'
			}, 250, function() {

			});
		}else{
			contenedor = document.getElementById('verm');
			contenedor.innerHTML = 'Ver Menos';
			$('#conten').animate({
			opacity: 1,
			height: altura
			}, 250, function() {

			});
			}
	});

	//mostrar Segundo Tab
	$(".vermascont2").click(function() {
       var obj = document.getElementById('contenB');
		if(obj.style.height == alturaB){
			contenedor = document.getElementById('verm2');
			contenedor.innerHTML = 'Ver M&aacute;s';
			$('#contenB').animate({
			opacity: 1,
			height: alt_cerrado + 'px'
			}, 250, function() {

			});
		}else{
			contenedor = document.getElementById('verm2');
			contenedor.innerHTML = 'Ver Menos';
			$('#contenB').animate({
			opacity: 1,
			height: alturaB
			}, 250, function() {

			});
			}

	});

        //mostrar tercer Tab
	$(".vermascont3").click(function() {
        var obj = document.getElementById('contenC');
		if(obj.style.height == alturaC){
			contenedor = document.getElementById('verm3');
			contenedor.innerHTML = 'Ver M&aacute;s';
			$('#contenC').animate({
			opacity: 1,
			height: alt_cerrado + 'px'
			}, 250, function() {

			});
		}else{
			contenedor = document.getElementById('verm3');
			contenedor.innerHTML = 'Ver Menos';
			$('#contenC').animate({
			opacity: 1,
			height: alturaC
			}, 250, function() {

			});
			}
	});

	//clase select al tab
	$("#tabA").click(function() {
            cleanClasses();
            $('#tabA').addClass("select");
            $('#tabB').addClass("noselecttab");
            $('#tabC').addClass("noselecttab");
	});

	$("#tabB").click(function() {
            cleanClasses();
            $('#tabA').addClass("noselecttab");
            $('#tabB').addClass("select");
            $('#tabC').addClass("noselecttab");
	});

	$("#tabC").click(function() {
            cleanClasses();
            $('#tabA').addClass("noselecttab");
            $('#tabB').addClass("noselecttab");
            $('#tabC').addClass("select");
	});

function cleanClasses(){
    
    $('#tabA').removeClass("noselecttab");
    $('#tabB').removeClass("noselecttab");
    $('#tabC').removeClass("noselecttab");
    $('#tabA').removeClass("select");
    $('#tabB').removeClass("select");
    $('#tabC').removeClass("select");
    
}


//***********************************************************
// FIN Mas info
//***********************************************************


// TABS
	ele = new Array();

	$('ul.menu-oferta').each(function(){
		$(this).find('li a').each(function(){
			ele.push(this.getAttribute("href",2))
		});
	});

//Funcion para ocultar todos
	function ocultar(){
			for(a=0;a<ele.length;a++){
			$(ele[a]).hide();
		}
	}

//mostrar el primero cuando carga la pagina
	ocultar();
	$("#tab-1").show();

//mostrar seleccionado...
    $('ul.menu-oferta li a').click(function(e){
	ocultar();
	var posicionlink = e.target.getAttribute("href",2);
	$(posicionlink).show();
	return false;
    });
    //Animacion DIV Modal
 	$(".imagen-js").mouseenter(function() {;
		$(".div-modal", this).show();
	});

	$(".div-modal").mouseleave(function() {;
		$(this).hide();
		$(".div-modal").hide();
	});
    //Animacion DIV Modal Mini
 	$(".bloque-promo .imagen-js").mouseenter(function() {;
		$(".div-modal", this).show();
	});

	$(".bloque-promo .imagen-js").mouseleave(function() {;
		$(".bloque-promo .div-modal").hide();
	});

    //Animacion DIV Modal Mini landing
 	$(".bloque-promo .imagen-js").mouseenter(function() {;
		$(".div-modal", this).show();
	});

	$(".bloque-promo .imagen-js").mouseleave(function() {;
		$(".bloque-promo .div-modal").hide();
	});

    //Funcion UL desplegable Cuenta LOGUEADO
 	$("ul.mi-cuenta li:first-child a").click(function() {;
		$("ul.mi-cuenta").hide();
	});

    //Funcion UL desplegable Cuenta LOGUEADO
        $("ul.mi-cuenta a.menu-js-off").click(function() {;
            $("ul.mi-cuenta").hide();
        });

        $(".ingresa a.menu-js").click(function() {;
            $("ul.mi-cuenta").slideToggle(50);
        });



    /**************************************************************************/
    /* EVENTOS */
    /**************************************************************************/

    $('#cboxboton_cierre').click(function(){
        $.colorbox.close()
    });

    // captcha reload function
  $('.js-captcha-reload').livequery('click', function() {
    captcha_img_src = $(this).parents('.js-captcha-container').find('.captcha-img').attr('src');
    captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
    $(this).parents('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
    if($('.js-captcha-input'))
    {
      $('.js-captcha-input').val('');
    }
    return false;
  });

    $('.js-quantity').livequery('keyup',function() {
        updateJsQuantity();
        checkShowCuotas();
    });

	  $('.js-give-as-gift-edit').click(function(){
		$("#buy_form_gift_button").click();
	  });
	  $('.js-give-as-gift-delete').click(function(){
		$('#DealGiftTo').val('');
		$('#DealGiftDni').val('');
		$('#DealGiftEmail').val('');
		$('#DealGiftMessage').val('');
		$('#DealConfirmGiftEmail').val('');
		$('#DealGiftDni').val('');
		giveAsGiftUpdate();
	  });


	if($("#buy_form_gift_button").length) {
		$("#buy_form_gift_button").colorbox({
		  width:"550px",
		  height:"520px",
		  inline:true,
		  href:"#buy_form_gift",
		  cbox_load: function() {
			$('#cboxClose').remove();
			$('#cboxSave').unbind().click(giveAsGiftSave);
			if(!$(".picker-img").length){
				$('.js-datetime-gift').fdatepicker();
			}
		  },
                   onComplete:function() {
                    //El boton esta oculto, lo muestro solo en este caso
                    $('#cboxboton_cierre').show();
                  }
		});
	}

$('img.js-open-datepicker').livequery('click', function() {
    var div_id = $(this).attr('name');
    $('#' + div_id).toggle();
    $(this).parent().parent().toggleClass('date-cont');
  });

  $('a.js-close-calendar').livequery('click', function() {
    $('#' + $(this).metadata().container).hide();
    $('#' + $(this).metadata().container).parent().parent().toggleClass('date-cont');
    return false;
  });

  $('a.js-no-date-set').livequery('click', function() {
    $this = $(this);
    $tthis = $this.parents('.input');
    $('div.js-datetime', $tthis).children("select[id$='Day']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Month']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Year']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Hour']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Min']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Meridian']").val('');
    $('#caketime' + $this.metadata().container).val('');
    $('#caketime' + $this.metadata().container).parent('div.timepicker').find('label.overlabel-apply').css('text-indent','0px');
    $('.displaydate' + $this.metadata().container + ' span').html('Seleccionar');
    return false;
  });

  /**** plugin facebook ****/
  var fb_api_key = __cfg('api_key');
  var pa=__cfg('path_absolute');
  if (typeof(FB) != 'undefined' && __cfg('api_key') != false) {
    FB.init({
      appId: __cfg('api_key'),
      channelUrl  : pa+'channel.html',
      status: false,
      cookie: true,
      xfbml: false
    });
  }
  $("a.view-icon.js-thickbox").colorbox({
	  close: 'Cerrar',
	  title: '&nbsp',
	  onComplete:function() {
	  			  	//$('#cboxClose').show();
   	  }
  });
  /****                  ****/

  $("img.tipimg").mouseenter(function() {;
            $("#reg-form .tip").show();
	});

	$("img.tipimg").mouseleave(function() {;
		$("#reg-form .tip").hide();
	});

});//fin doc ready

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


/* configuration registry */
var _configuration = {
  popupsDefault: {
    width:'800px',
    height:'600px',
    frase:'Cerrar'
  }
};

//  date picker function starts here
  var i = 1;
  $.fn.fdatepicker = function() {
    $(this).livequery(function() {
      var $this = $(this);
      var class_for_div = $this.attr('class');
      var year_ranges = $this.children('select[id$="Year"]').text();

      var start_year = end_year = '';
      $this.children('select[id$="Year"]').find('option').each(function() {
        $tthis = $(this);
        if ($tthis.attr('value') != '') {
          if (start_year == '') {
            start_year = $tthis.attr('value');
          }
          end_year = $tthis.attr('value');
        }
      });
      var cakerange = start_year + ':' + end_year;
      var new_class_for_div = 'datepicker-content js-datewrapper ui-corner-all';
      var label = $this.children('label').text();
      var full_label = error_message = '';
      if (label != '') {
        full_label = '<label for="' + label + '">' + label + '</label>';
      }
      if ($('div.error-message', $this).html()) {
        var error_message = '<div class="error-message">' + $('div.error-message', $this).html() + '</div>';
      }
      var img = '<div class="time-desc datepicker-container"><img title="datepicker" name="datewrapper' + i + '" class="picker-img js-open-datepicker" src="' + __cfg('path_relative') + 'img/date-icon.png"/>';
      year = $this.children('select[id$="Year"]').val();

      month = $this.children('select[id$="Month"]').val();
      day = $this.children('select[id$="Day"]').val();
      if (year == '' && month == '' && day == '') {
        date_display = 'Seleccionar';
      } else {
        date_display = date(__cfg('date_format'), toNewDate(year, month, day));
      }
      $this.hide().after(full_label + img + '<div id="datewrapper' + i + '" class="' + new_class_for_div + '" style="display:none; z-index:99999;">' + '<div id="cakedate' + i + '" title="Seleccionar" ></div><span class=""><a href="#" class="close js-close-calendar {\'container\':\'datewrapper' + i + '\'}">Cerrar</a></span></div><div class="displaydate displaydate' + i + '"><span class="js-date-display-' + i + '">' + date_display + '</span><a href="#" class="js-no-date-set {\'container\':\'' + i + '\'}">[x]</a></div></div>' + error_message);
      var sel_date = new Date();
      if (month != '' && year != '' && day != '') {
        sel_date.setFullYear(year, (month - 1), day);
      } else {
        splitted = __cfg('today_date').split('-');
        sel_date.setFullYear(splitted[0], splitted[1] - 1, splitted[2]);
      }
	  //console.log(i);
      $('#cakedate' + i).datepicker({
        closeText:"Cerrar",
        prevText:"Anterior",
        nextText:"Siguiente",
        currentText:"Hoy",
        monthNames:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
        monthNamesShort:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
        dayNames:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
        dayNamesShort:["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
        dayNamesMin:["D","L","M","M","J","V","S"],
        firstDay:0,
        isRTL:false,
        dateFormat: 'yy-mm-dd',
        defaultDate: sel_date,
        clickInput: true,
        speed: 'slow',
        changeYear: true,
        changeMonth: true,
        yearRange: cakerange,
        onSelect: function(sel_date) {
          if (sel_date.charAt(0) == '-') {
            sel_date = start_year + sel_date.substring(2);
          }
          var newDate = sel_date.split('-');
          $this.children("select[id$='Day']").val(newDate[2]);
          $this.children("select[id$='Month']").val(newDate[1]);
          $this.children("select[id$='Year']").val(newDate[0]);
          $this.parent().find('.displaydate span').show();
          $this.parent().find('.displaydate span').html(date(__cfg('date_format'), toNewDate(newDate[0], newDate[1], newDate[2])));
          $this.parent().find('.js-datewrapper').hide();
          $this.parent().toggleClass('date-cont');
        }
      });
      if ($this.children('select[id$="Hour"]').html()) {
        hour = $this.children('select[id$="Hour"]').val();
        minute = $this.children('select[id$="Min"]').val();
        meridian = $this.children('select[id$="Meridian"]').val();
        var selected_time = overlabel_class = overlabel_time = '';
        if (hour == '' && minute == '' && meridian == '') {
          overlabel_class = 'js-overlabel';
          overlabel_time = '<label for="caketime' + i + '">Horario</label>';
        } else {
          if (minute < 10) {
            minute = '0' + minute;
          }
          selected_time = hour + ':' + minute + ' ' + meridian;
        }
        $('.displaydate' + i).after('<div class="timepicker ' + overlabel_class + '">' + overlabel_time + '<input type="text" class="timepickr" id="caketime' + i + '" title="Seleccionar" readonly="readonly" size="10"/></div>');
        $('#caketime' + i).timepickr( {
          convention: 12,
          resetOnBlur: true,
          val: selected_time
        }).livequery('blur', function() {
          var value = $(this).val();
          var newmeridian = value.split(' ');
          var newtime = newmeridian[0].split(':');
          $this.children("select[id$='Hour']").val(newtime[0]);
          $this.children("select[id$='Min']").val(newtime[1]);
          $this.children("select[id$='Meridian']").val(newmeridian[1]);
        });
      }
      i = i + 1;
    });
  };
  
  
  function returnSettingsForMobileAd() {

        var ua = navigator.userAgent;
        var blackberry1 = /blackberry/i.test(ua);
        var blackberry2 = /bb/i.test(ua);
        var blackberry3 = /playbook/i.test(ua);
        var blackberry = blackberry1 || blackberry2 || blackberry3;
        var iphone = /iphone/i.test(ua);
        var android = /android/i.test(ua);
        var ipad = /ipad/i.test(ua);
    
        var settings = [];
        
        if(blackberry) {
            settings['name'] = 'blackberry';
            settings['url']  = 'http://appworld.blackberry.com/webstore/content/24574893/?lang=es&countrycode=AR';
        } else if(iphone) { 
            settings['name'] = 'iPhone';
            settings['url']  = 'https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8';
        } else if(android) {
            settings['name'] = 'Android';
            settings['url']  = 'https://play.google.com/store/apps/details?id=com.cmd.clubcupon';
        } else if(ipad) {
            settings['name'] = 'iPad';
            settings['url']  = 'https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8';
        } else {
            settings = false;
        }
        
        return settings;
    
    }

    function renderMobileAd(settingsForMobileAd) {
        
        var imgsDir = 'img/mobile/ad/'
        $('#mobileAd .logoIcon img').attr('src', imgsDir+settingsForMobileAd['name']+'.png')
        $('#mobileAd .text a').attr('href', settingsForMobileAd['url'])
        $('#mobileAd .text a').html(settingsForMobileAd['name'])
        $('#mobileAd .downloadLink a').attr('href', settingsForMobileAd['url'])
        $('#mobileAd').toggle();
        
    }


  
  
  // date picker function ends here
var popupWelcome = function(url, width, height, onClose) {

 if(!returnSettingsForMobileAd()){

  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: 'Ya estoy suscripto',
    href: url,
    iframe: true,
    width: width,
    height: height,
    onClosed: function () {$.cookie('fullscreenClose', 1);}
  };

  $.fn.colorbox(configColorbox);

 }
  return false;
};


var popupGeneric = function(url, width, height, frase) {

  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  if(typeof(frase) == "undefined" || frase == null || frase == "") {
    frase = _configuration.popupsDefault.frase;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: frase,
    href: url,
    iframe: true,
    width: width,
    height: height
  };
  $.fn.colorbox(configColorbox);
  return false;
};



////
var newAmountBD = null;

var updateJsQuantity = function() {
  //console.log('entro aca updateJsQuantity');
  //$('#vista4 div.row.compra-blq.back-white').parent().show();
  //$('.wallet-bonification').hide();
  $('li div input[value^="5000"]').first().parent().parent().siblings().show();
  if(!$('.js-quantity').length)
    return;
  var selectedQuantity = $.trim($('.js-quantity').val());
  var minQuantity = $.trim($('.min-quantity').html());

  if (selectedQuantity.length==0){
    lastQuantitySelected = selectedQuantity;
    return;
  }

  if (!isPositiveInteger(selectedQuantity)) {
	
    alert('Solo puede ingresar números');
    if(!lastQuantitySelected==''){
    	$('.js-quantity').val(lastQuantitySelected);	
    }else{
    	if(!minQuantity==''){
    		$('.js-quantity').val(minQuantity);	
    	}else{
    		$('.js-quantity').val(1);
    	}
    }
    return;
  }else{
	  //alert($('.min-quantity').html());
	  if(!minQuantity==''){
		  if(selectedQuantity<minQuantity){
			  alert('La Cantidad debe ser mayor o igual al minimo permitido');
			  $('.js-quantity').val(minQuantity);  
			  return;
		  }
		  
	  }else{
		  if (selectedQuantity==0) {
			  alert('La Cantidad no puede ser cero');
			  if(!lastQuantitySelected==''){
			    	$('.js-quantity').val(lastQuantitySelected);	
			  }else{
			    	if(!minQuantity==''){
			    		$('.js-quantity').val(minQuantity);	
			    	}else{
			    		$('.js-quantity').val(1);
			    	}
			  }
			  return;
		  } 
	  }
	  
  }

  if (selectedQuantity.length > 4) {
    $(this).val(lastQuantitySelected);
    alert('La cantidad es demasiado alta.');
    return;
  }
  
  

  lastQuantitySelected = selectedQuantity;

  var selectedQuantityBD = new BigDecimal(selectedQuantity);

  var dealAmount = $('#DealDealAmount').val();

  var dealAmountBD = new BigDecimal(dealAmount);
  var gift_amount = $('#gift_monto').val();
  var credito;
  if(gift_amount!="" && parseInt(gift_amount,10)>0){
    var giftAmountBD = new BigDecimal(gift_amount);
    //newAmountBD = dealAmountBD.multiply(selectedQuantityBD).subtract(giftAmountBD)
    
    credito = dealAmountBD.multiply(selectedQuantityBD).subtract(giftAmountBD);
    
    if (credito <= 0 && ($("#DealQuantity").parent().find("div.error-message").length == 0)) {
      walletBonificationAmount = credito * -1;
      walletBonificationAmount = 0;
      //$('#wallet-bonification-amount').html('$'+walletBonificationAmount);
      //$('.wallet-bonification').show();
      //newAmountBD = 0;
    }
  } else {
    newAmountBD = dealAmountBD.multiply(selectedQuantityBD);
  }
  /* dejar solo clubcupon si esta como metodo de pago*/
  //alert('newAmountBD:'+newAmountBD);

  if(credito <= 0){
    
      if($('li input[value^="5000"]').first().length>0){
            
            $('li div input[value^="5000"]').first().parent().siblings().hide();
            $('li div input[value^="5000"]').first().attr("checked", "checked");
      } else {
         
           // no tiene monedero pero el codigo gift pin tiene un valor  mas alto que la oferta.
           $('li div input[value^="5000"]').first().parent().siblings().show();
           lastPayment = $('li div input[name*=payment_type_id').last();
           payment_monedero = '<li><div><input type="radio" value="5000_5000_57_5000" class="js-payment-type cardLogo" id="DealPaymentTypeId50005000575000" name="data[Deal][payment_type_id]"><img src="/img/card_logos/sin_logo.gif">Cuenta Club Cupon</div></li>';
           lastPayment.parent().parent().parent().append(payment_monedero);
           $('li div input[value^="5000"]').parent().parent().parent().show();
         
           /* var qty_local  = $('.js-quantity').val();
           var deal_amount_local = $('#DealDealAmount').val();
           var gift_amount_local = $('#gift_monto').val();
           qty_local = parseInt(qty_local, 10);
           deal_amount_local = parseInt(deal_amount_local, 10);
           gift_amount_local = parseInt(gift_amount_local, 10);
           while(((deal_amount_local*qty_local)-gift_amount_local) <= 0){

               qty_local++;
           }
           $('li input.js-payment-type').first().attr("checked", "checked");

          $('li input.js-payment-type').parent().hide();
          $('#vista4 div.row.compra-blq.back-white').parent().hide();*/
          //$('.wallet-bonification').hide();
          //$(".gift_pin_code_error_msg").html("Necesitas al menos comprar "+qty_local+" unidades para poder utilizar el código de descuento ");
         
        $(".gift_pin_code_error_msg").show();
       }

  } else {
      //$('.wallet-bonification').hide();
 
      if($('li input[value^="5000"]').first().length>0){
        if ($('#dealHasWallet').val()=='0') {
            $('li div input[value^="5000"]').first().parent().siblings().show();
            $('li div input[value^="5000"]').first().parent().parent().hide();
        }
      } 
      $('li div input:radio').first().attr("checked", "checked");
      
      $('li div input[name*=payment_type_id]').first().attr("checked", "checked");
      $('li input.js-payment-type').parent().show();
      $('#vista4 div.row.compra-blq.back-white').parent().show();
  }




  var availableAmount = $('#DealUserAvailableBalance').val();

  var availableAmountBD = new BigDecimal(availableAmount);

  var needToPayBD = null;
  if(credito)
  {
    if  (credito <= 0 ) {
         $('li div input[value^="5000"]').parent().parent().siblings().hide();
         $('li div input[value^="5000"]').first().attr("checked", "checked");
         
         if($('li input[value^="5000"]').first().length>0){
            
            $('li div input[value^="5000"]').parent().siblings().hide();
            $('li div input[value^="5000"]').parent().parent().show();
            $('li div input[value^="5000"]').attr("checked", "checked");
         } else {
           
           $('li div input[value^="5000"]').first().parent().siblings().show();
           lastPayment = $('li div input[name*=payment_type_id').last();
           payment_monedero = '<li><div><input type="radio" value="5000_5000_57_5000" class="js-payment-type cardLogo" id="DealPaymentTypeId50005000575000" name="data[Deal][payment_type_id]"><img src="/img/card_logos/sin_logo.gif">Cuenta Club Cupon</div></li>';
           lastPayment.parent().parent().append(payment_monedero);
           $('li div input[value^="5000"]').parent().parent().parent().show();
           
         }
  
    } else {
      $('li div input[name*=payment_type_id]').first().attr("checked", "checked");
      $('#vista4 div.row.compra-blq.back-white').parent().show();
      $(".gift_pin_code_error_msg").hide();
    }
    $('.js-deal-total').html(credito.toString().replace('.00',''));
  }
  else
  {
    $('.js-deal-total').html(newAmountBD.toString().replace('.00',''));
    credito = 0;
  }
    
  var loginUrl=$('#UserF').val();
 
  if(typeof loginUrl!='undefined' && selectedQuantity !=''){
   
    loginUrl=loginUrl.replace(/\/(\d+)\/?(\d*)(.*)$/, "\/$1\/"+selectedQuantity+"$3");
    $('#UserF').val(loginUrl);
    $('#UserQuantity').val(selectedQuantity);
    // $('#UserFwd').val(loginUrl);
    //$('#UserAddFormRegister').attr("action","/"+$('#UserFwd').val());
  }

  return false;

};


var checkShowCuotas = function(){
    if(!$('#DealCantidadCuotas').length)
    return;
    var showCuotas = true;
    if(!newAmountBD)
        {
            var selectedQuantity = $('.js-quantity').val();
            lastQuantitySelected = selectedQuantity;
            var selectedQuantityBD = new BigDecimal(selectedQuantity);
            var dealAmount = $('#DealDealAmount').val();
            var dealAmountBD = new BigDecimal(dealAmount);
            newAmountBD = dealAmountBD.multiply(selectedQuantityBD);
        }

    // checks if the new total is greather than the minimal financial amount
    var ceroBD = new BigDecimal('0');
    var minimalFinancialAmount = $('#DealMinimalAmountFinancial').val() ;
    if(!minimalFinancialAmount.length)
        minimalFinancialAmount = '0';
    var minimalFinancialAmountBD = new BigDecimal(minimalFinancialAmount);
    if(newAmountBD.compareTo(minimalFinancialAmountBD) === -1){
        showCuotas = false;
        }

    if(showCuotas){
        var cuotasIds = ['2_43','2_44'];
        var cuotaId = $('.js-payment-type:checked').val();
        if(typeof cuotaId == "undefined"){
			showCuotas = false;
		}else{
			cuotaId = cuotaId.split('_')[0] + '_' + cuotaId.split('_')[1];
			//console.log('cuota id',cuotaId);
			if(cuotasIds.indexOf(cuotaId) === -1){
				showCuotas = false;
			}
		}
	}
if(!showCuotas)
{
// Ocultar cuotas
//$('#DealCantidadCuotas').parent().hide('slow');
$('#DealCantidadCuotas').parent().hide();
lastCuotasSelected = $('#DealCantidadCuotas').val();
$('#DealCantidadCuotas').val('1');
}else{
// Mostrar cuotas
//if($('#DealCantidadCuotas').parent().is(':hidden')) { animar aca ? }
$('#DealCantidadCuotas').parent().show('slow');
$('#DealCantidadCuotas').val(lastCuotasSelected);
}
};





$.fn.fcommentform = function() {
    $(this).livequery('submit', function(e) {
        var $this = $(this);
        $this.block();
        $this.ajaxSubmit( {
            beforeSubmit: function(formData, jqForm, options) {},
            success: function(responseText, statusText) {
                if (responseText.indexOf($this.metadata().container) != '-1') {
                $('.' + $this.metadata().container).html(responseText);
                }else {
                    $('.commment-list .notice').hide();
                    $('.js-comment-responses').prepend(responseText);
                    $('.' + $this.metadata().container + ' div.input').removeClass('error');
                    $('.error-message', $('.' + $this.metadata().container)).remove();
                }
                if (typeof($('.js-captcha-container').find('.captcha-img').attr('src')) != 'undefined') {
                    captcha_img_src = $('.js-captcha-container').find('.captcha-img').attr('src');
                    captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
                    $('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
                }
                $this.unblock();
            },
            clearForm: true
        });
        return false;
    });
};



/**************************************************************************/
/* GENERICAS */
/**************************************************************************/

function isPositiveInteger(val){

  if(val==null){
    return false;
  }

  for (var i = 0; i < val.length; i++) {
    var ch = val.charAt(i)
    if (ch < "0" || ch > "9") {
      return false
    }
  }
  return true;
}

/* la usa facebook*/
function __cfg(c) {
	  return(cfg && cfg.cfg && cfg.cfg[c]) ? cfg.cfg[c]: false;
}

var clubcuponHeightGiftOld;
var clubcuponTopGiftOld;
$(document).load(function(){
  clubcuponHeightGiftOld =  $('#buying-form #login_pusher').css('height');
  clubcuponTopGiftOld    =  $('#buying-form .ajax-login').css('top');
});

function giveAsGiftSave() {
  if($('#DealGiftTo').val().length && !$.trim($('#DealGiftTo').val()).length) {
    $('#DealGiftTo').focus();
    alert('Debe completar el Nombre del Amigo.');
    return false;
  }
  var reEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(
    ($('#DealGiftEmail').val().length || $('#DealGiftTo').val().length)  && (!$('#DealGiftEmail').val().length || !$('#DealGiftEmail').val().match(reEmail)) ||
      ( $('#DealConfirmGiftEmail').val() !== $('#DealGiftEmail').val())
    ) {
    $('#DealGiftEmail').focus();
    alert('Debe completar un mail para el Amigo y debe coincidir con el mail de confirmación.');
    return false;
  }
  giveAsGiftUpdate();
  $(this).colorbox.close();
  return true;
}

function giveAsGiftUpdate() {
  if($('#DealGiftTo').val().length) {
    $('#buying-form #login_pusher').css('height', '291px;');
    $('#DealIsGift').val(1);
    $('.js-give-as-gift-to' ).html($('#DealGiftTo').val());
    $('.js-give-as-gift-buy-row').show();
  } else {
    $('.js-give-as-gift-buy-row').hide();
    $('#DealIsGift').val(0);
    $('#buying-form #login_pusher').css('height',clubcuponHeightGiftOld);
    $('#buying-form .ajax-login').css('top', clubcuponTopGiftOld);
  }
  return false;
}

$('.js-register-form').livequery(function() {
    $.ajax( {
        type: 'GET',
    url: 'http://j.maxmind.com/app/geoip.js',
    dataType: 'script',
    cache: true,
    success: function() {
        $('#CityName').val(geoip_city());
        $('#StateName').val(geoip_region_name());
        $('#country_iso_code').val(geoip_country_code());
    }
    });
});

function checkClarin365CardNumber(clarin365CardNumber) {
    url = '../../../deals/check_clarin365_card_number/';
    $.post(url, {'clarin365CardNumber' : clarin365CardNumber},
            function(data){
                if(data.success && data.isValid) {
                    buyWithClarin365();
                } else {
                    messageError('La tarjeta ingresada no es valida.');
                }
            },
            "json");

    return false;
}

function buyWithClarin365() {
    allFields.addClass('clarin365-ui-state-success');
    messageSuccess('Nro de tarjeta valido. <br/>Continuando con la compra.');
    setTimeout(function() {
        $('#clarin365_form').submit();
    }, 500 );
}

function messageSuccess( t ) {
    clarin365_messages.removeClass( "clarin365-messages-error" );
    clarin365_messages.removeClass( "clarin365-messages" );
    clarin365_messages.addClass( "clarin365-messages-success" );
    clarin365_messages.html( t );
}

function messageError( t ) {
    clarin365_messages.addClass( "clarin365-messages-error" );
    clarin365_messages.removeClass( "clarin365-messages" );
    clarin365_messages.removeClass( "clarin365-messages-success" );
    clarin365_messages.html( t );
}

function resetMessageClarin365() {
    clarin365_messages.val("").removeClass( "clarin365-messages-error" );
    clarin365_messages.val("").removeClass( "clarin365-messages-success" );
}

function checkUrlClarin365( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
        o.removeClass("clarin365-state-success" );
        o.addClass( "clarin365-state-error" );
        messageError( n );
        return false;
    } else {
        return true;
    }
}

var updateQuestion = function() {
  var $select = $(this);
  var $input = $('#questionfree_'+$select.attr('id').split('_')[1]);

  if($select.val() === '0') {
    $input.parent().removeClass('invisible');
    $input.parent().parent().removeClass('invisible');
  }
  else {
    $input.parent().addClass('invisible');
    $input.parent().parent().addClass('invisible');
  }
};


function updateUserAddFormRegisterQty()
{
    var  where = $("#UserFwd").val();
    where +="/quantity:" +$("#DealQuantity").val();
     $("#UserFwd").val(where);
}

function checkGiftCode()
{

  var discount_code =  $("#discount_code[name=discount_code]").val();
  $(".gift_pin_code_error_msg").hide();
  $.ajax( {
    url: '/deals/check_gift_code/'+discount_code,
    dataType: 'json',
    beforeSend: function(){
        $("#pincode_result_img").attr('src', '/img/loading.gif');
        $("#gift_monto").val("");
    },
    success:function(respuesta){
      if(respuesta.status != true){
          $("#pincode_result_img").attr('src', '/img/icon-error-24x24-pincode.png');
        $(".gift_pin_code_error_msg").html(respuesta.ret);
        $("#gift_pin_code").val("");
        $("#gift_monto").val("");
        $(".gift_pin_code_error_msg").show();
      } else {
          $("#pincode_result_img").attr('src', '/img/icon-success-24x24-pincode.png');
          $(".gift_pin_code_error_msg").html("");
        $("#gift_pin_code").val(discount_code);
        $("#gift_monto").val(respuesta.ret.monto);
      }
      updateJsQuantity();
    }
  }
  );
}


function fbFeed_deal(name, _href, percentage) {

 if (_href == null) {
    _href = 'http://www.clubcupon.com.ar';
  }
  var image_src = $('#deal_image').attr('src') + '';
  if(image_src[0] == "/") {
    image_src = image_src.substr(1);
  }
  var img = __cfg('path_absolute') + image_src;
  var title = "Club Cupon - Todos los días una alegría";
  var _caption = "{*actor*} compartió la oferta \"" + name + "\". Aprovecha el " + percentage + "% de descuento ahora mismo.";

  FB.ui(
  {
    method: 'stream.publish',
    attachment: {
      name: title,
      caption: _caption,
      href: _href,
      media:[{
        type:'image',
        src:img,
        href:_href
      }]
    },
    action_links: [
    {
      text: 'Ver Cupón',
      href: _href
    }
    ]
    ,
    user_message_prompt: 'Comparte con tus amigos esta novedad'
  }
  );
}