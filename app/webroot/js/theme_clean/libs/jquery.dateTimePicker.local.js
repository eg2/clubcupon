/* 
 * Localizacion de textos para el datePicker y timePicker
 */

$(function()  {
    $.timepicker.regional['es'] = {
                            currentText: 'Ahora',
                            closeText: 'Done',
                            amNames: ['AM', 'A'],
                            pmNames: ['PM', 'P'],
                            timeFormat: 'HH:mm',
                            timeSuffix: '',
                            timeOnlyTitle: 'Seleccionar una hora',
                            timeText: 'Horario',
                            hourText: 'Horas',
                            minuteText: 'Minutos',
                            secondText: 'Segundos',
                            millisecText: 'Milisegundos',
                            timezoneText: 'Zona horaria',
                            isRTL: false
                        }
    $.datepicker.regional['es'] = {
            closeText:          'Confirmar',
            prevText:           'Anterior',
            nextText:           'Siguiente',
            currentText:        'Mes actual',
            monthNames:         ['Enero',   'Febrero',  'Marzo',    'Abril',        'Mayo',     'Junio',    'Julio',    'Agosto',   'Septiembre',   'Octubre',  'Noviembre',    'Diciembre'],
            monthNamesShort:    ['En',      'Feb',      'Mar',      'Abr',          'May',      'Jun',      'Jul',      'Ago',      'Sep',          'Oct',      'Nov',          'Dic'],
            dayNames:           ['Domingo', 'Lunes',    'Martes',   'Miercoles',    'Jueves',   'Viernes',  'Sabado'],
            dayNamesShort:      ['Dom',     'Lun',      'Mar',      'Mie',          'Jue',      'Vie',      'Sab'],
            dayNamesMin:        ['D','L','M','X','J','V','S'],
            weekHeader:         '#',
            dateFormat:         'dd/mm/yy',
            firstDay:           0,
            isRTL:              false,
            showMonthAfterYear: false,
            yearSuffix:         ''
    };

    $.datepicker.setDefaults($.datepicker.regional['es']);
    $.timepicker.setDefaults($.timepicker.regional['es']);
});