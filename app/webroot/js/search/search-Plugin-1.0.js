 $(document).ready(function () {

    if ($.browser.msie) {
    
    	$('input[placeholder]').each(function () {
            var obj = $(this);
            if (obj.attr('placeholder') != '') {
                if ($.trim(obj.val()) == '' && obj.attr('type') != 'password') {
                    obj.val(obj.attr('placeholder'));
                    obj.addClass('placeholderIE');
                }
            }
        });

    	$("#q").click(function () {
            var obj = $(this);
            if (obj.val() == obj.data('holder')) {
            	obj.val('');
            	obj.removeClass('placeholderIE');
            }
        });

    	$("#q").blur(function () {
            var obj = $(this);
            if ($.trim(obj.val()) == '') {
                obj.val(obj.data('holder'));
                obj.addClass('placeholderIE');
            }
        });
    }
	 
});
 
function isNumber(n) {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
}

function successCback(data, response, directToSolr) {
    if (directToSolr) {
        var arr = data.facet_counts.facet_fields[autocompFField];
    } else {
        var arr = data;
    }
    var length = arr.length;
    var resp = new Array();
    var ct = 0;
    for (var i = 0; i < length; i++) {
        if (!isNumber(arr[i]) && ct < autocompItems) {
            resp.push(arr[i]);
            ct++;
        }
    }
    response(resp);
}

function initAutocomplete() {
	
    $(".qSearch").autocomplete({
        source: function (request, response) {
            if ($.browser.msie && window.XDomainRequest) {
                var urlQIE = autocompSolrAsyncUrlBridge;
                var dataQ = "term=" + request.term;
                $.ajax({
                    type: "post",
                    url: urlQIE,
                    data: dataQ,
                    dataType: "json",
                    success: function (data) {
                        successCback(data, response, false);
                    }
                });
            } else {
                var dataQ = {
                    'q': '*.*',
                    'start': 0,
                    'rows': 0,
                    'indent': 'on',
                    'facet': 'true',
                    'facet.field': autocompFField,
                    'facet.prefix': request.term,
                    'fq': extraFilter,
                    'wt': 'json'
                };
                var urlQ = autocompSolrAsyncUrl;
                $.ajax({
                    url: urlQ,
                    dataType: "jsonp",
                    jsonp: 'json.wrf',
                    data: dataQ,
                    success: function (data) {
                        successCback(data, response, true);
                    }
                });
            }
        },
        minLength: 2,
        select: function (event) {
            setTimeout(
                function () {
                    $("#searchFormRight").submit();
                },
                200
            );
        }
    });
    $("#searchFormRight").submit(function () {
        var value = $.trim($('#q').val());
        if (value === '') {
            return false;
        }
    });
    $(function () {
        $('#q').data('holder', $('#q').attr('placeholder'));
        $('#q').focusin(function () {
            $(this).attr('placeholder', '');
        });
        $('#q').focusout(function () {
            $(this).attr('placeholder', $(this).data('holder'));
        });
    });
}