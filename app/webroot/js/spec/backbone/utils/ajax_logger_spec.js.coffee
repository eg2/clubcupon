describe "Utils.AjaxLogger", ->
  ajaxLogger = {}

  beforeEach ->
    console.log "before:each"

  it "should be exist", ->
    expect(Buy.Utils.AjaxLogger).toBeDefined()

  describe "#initialize", ->
    it "sets the options", ->
      options =
        name: "Matilda"
        logUri: "buy.ctp"
      ajaxLogger = new Buy.Utils.AjaxLogger options
      expect(ajaxLogger.options).toEqual(options)

#  describe "#defaults", ->
#    it "has a default done value of false", ->
#      expect(item.defaults().done).toEqual(false)
#
#    it "sets the order attribute based on Todo.Items.nextOrder()", ->
#      sinon.stub(Todo.Items, "nextOrder", ->
#        55
#      )
#      expect(item.defaults().order).toEqual(55)


