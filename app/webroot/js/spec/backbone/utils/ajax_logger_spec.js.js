// Generated by CoffeeScript 1.4.0

describe("Utils.AjaxLogger", function() {
  var ajaxLogger;
  ajaxLogger = {};
  beforeEach(function() {
    return console.log("before:each");
  });
  it("should be exist", function() {
    return expect(Buy.Utils.AjaxLogger).toBeDefined();
  });
  return describe("#initialize", function() {
    return it("sets the options", function() {
      var options;
      options = {
        name: "Matilda",
        logUri: "buy.ctp"
      };
      ajaxLogger = new Buy.Utils.AjaxLogger(options);
      return expect(ajaxLogger.options).toEqual(options);
    });
  });
});
