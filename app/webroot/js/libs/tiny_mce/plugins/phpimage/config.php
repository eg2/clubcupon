<?php

$_cur_dir = getcwd(); //if ($_cur_dir == FALSE) { $_cur_dir = dirname($_SERVER['SCRIPT_FILENAME']); }
$_cur_dir = $_cur_dir.'/../../../../..'; // minus the amout of directorys back to root directory from current run script e.g. /js/tinymce/plugins/phpimage

$language						= 'es_ES';
//Directorio de subida
$server_image_directory		= $_cur_dir.'/img/pages';  //e.g. '/home/user/public_html/uploads'; 
// URL del directorio de imagenes
$url_image_directory			= '/img/pages'; 
// cambiar espacios por underscores, para evitar el (%20) de las urls
$handle->file_safe_name 	= true;
# renombrar si ya existe
$handle->file_auto_rename 	= true;
// image_resize determines is an image will be resized (default: false)
$handle->image_resize		= false;
// image_ratio if true, resize image conserving the original sizes ratio, using image_x AND image_y as max sizes if true (default: false)
$handle->image_ratio			= true;
// image_ratio_x if true, resize image, calculating image_x from image_y and conserving the original sizes ratio (default: false)
$handle->image_y				= 800;
// image_ratio_y if true, resize image, calculating image_y from image_x and conserving the original sizes ratio (default: false)
$handle->image_x				= 800;
// file_safe_name formats the filename (spaces changed to _) (default: true)
$handle->file_safe_name 	= true;
?>