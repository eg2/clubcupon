<?php
// +------------------------------------------------------------------------+
// | class.upload.xx_XX.php                                                 |
// +------------------------------------------------------------------------+
// | Copyright (c) xxxxxx 200x. All rights reserved.                        |
// | Version       0.25                                                     |
// | Last modified xx/xx/200x                                               |
// | Email         xxx@xxx.xxx                                              |
// | Web           http://www.xxxx.xxx                                      |
// +------------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify   |
// | it under the terms of the GNU General Public License version 2 as      |
// | published by the Free Software Foundation.                             |
// |                                                                        |
// | This program is distributed in the hope that it will be useful,        |
// | but WITHOUT ANY WARRANTY; without even the implied warranty of         |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
// | GNU General Public License for more details.                           |
// |                                                                        |
// | You should have received a copy of the GNU General Public License      |
// | along with this program; if not, write to the                          |
// |   Free Software Foundation, Inc., 59 Temple Place, Suite 330,          |
// |   Boston, MA 02111-1307 USA                                            |
// |                                                                        |
// | Please give credit on sites that use class.upload and submit changes   |
// | of the script so other people can use them as well.                    |
// | This script is free to use, don't abuse.                               |
// +------------------------------------------------------------------------+

/**
 * Class upload xxxxxx translation
 *
 * @version   0.28
 * @author    xxxxxxxx (xxx@xxx.xxx)
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @copyright xxxxxxxx
 * @package   cmf
 * @subpackage external
 */

    $translation = array();
    $translation['file_error']                  = 'Error en el archivo, reintentar.';
    $translation['local_file_missing']          = 'El archivo local no existe.';
    $translation['local_file_not_readable']     = 'El archivo local no se puede leer.';
    $translation['uploaded_too_big_ini']        = 'Error en la carga (el archivo excede el tamaño maximo especificado en el php.ini).';
    $translation['uploaded_too_big_html']       = 'Error en la carga (el archivo excede el tamaño maximo especificado).';
    $translation['uploaded_partial']            = 'Error en la carga (el archivo se cargo parcialmente).';
    $translation['uploaded_missing']            = 'Error en la carga (no se cargo ningun archivo).';
    $translation['uploaded_no_tmp_dir']         = 'Error en la carga (carpeta temporal faltante).';
    $translation['uploaded_cant_write']         = 'Error en la carga (no se puede escribir en el disco).';
    $translation['uploaded_err_extension']      = 'Error en la carga (problema en la extension).';
    $translation['uploaded_unknown']            = 'Error en la carga (error desconocido).';
    $translation['try_again']                   = 'Error en la carga. Reintentar.';
    $translation['file_too_big']                = 'El archivo es demasiado grande.';
    $translation['no_mime']                     = 'No se pudo detectar el tipo de MIME.';
    $translation['incorrect_file']              = 'Tipo de archivo incorrecto.';
    $translation['image_too_wide']              = 'Imagen demasiado alta.';
    $translation['image_too_narrow']            = 'Imagen demasiado angosta.';
    $translation['image_too_high']              = 'Imagen demasiado grande.';
    $translation['image_too_short']             = 'Imagen demasiado pequeña.';
    $translation['ratio_too_high']              = 'Proporcion de imagen excedida (demasiado ancha).';
    $translation['ratio_too_low']               = 'Proporcion de imagen reducida (demasiado alta).';
    $translation['too_many_pixels']             = 'La imagen tiene demasiados pixeles.';
    $translation['not_enough_pixels']           = 'La imagen tiene pocos píxeles.';
    $translation['file_not_uploaded']           = 'Selecciona una imagen antes de presionar UPLOAD.';
    $translation['already_exists']              = '%s ya existe, selecciona otro nombre.';
    $translation['temp_file_missing']           = 'El temp de origen no es correcto, no se puede continuar el proceso.';
    $translation['source_missing']              = 'El archivo de origen es erroneo, no se puede continuar el proceso.';
    $translation['destination_dir']             = 'No se pudoc crear el directorio de destino, no se puede continuar el proceso.';
    $translation['destination_dir_missing']     = 'El directorio de destino no existe, no se puede continuar el proceso.';
    $translation['destination_path_not_dir']    = 'El directorio de destino no es correcto, no se puede continuar el proceso.';
    $translation['destination_dir_write']       = 'No se puede hacer escribible el directorio de destino, no se puede continuar el proceso.';
    $translation['destination_path_write']      = 'El directorio de destino no es escribible, no se puede continuar el proceso.';
    $translation['temp_file']                   = 'No se pudo crear el atchivo temporal, no se puede continuar el proceso.';
    $translation['source_not_readable']         = 'No se puede leer el archivo de origen, no se puede continuar el proceso.';
    $translation['no_create_support']           = 'No create from %s support.';
    $translation['create_error']                = 'Error al crear la imagen %s desde el origen.';
    $translation['source_invalid']              = 'No se puede leer el archivo de origen, es una imagen?.';
    $translation['gd_missing']                  = 'La libreria GD parece no estar instalada.';
    $translation['watermark_no_create_support'] = 'No hay soporte para marca de agua desde %s, no se puede leer la marca de agua.';
    $translation['watermark_create_error']      = 'No hay soporte de lectura para %s, no se puede leer la marca de agua.';
    $translation['watermark_invalid']           = 'Formato de imagend esconocido, no se puede leer la marca de agua.';
    $translation['file_create']                 = 'No hay soporte de creacion para %s.';
    $translation['no_conversion_type']          = 'No se definio el tipo de conversion.';
    $translation['copy_failed']                 = 'Error al copiar el archivo al servidor. copy() falló.';
    $translation['reading_failed']              = 'Error al leer el archivo.';

?>