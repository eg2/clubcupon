var futura_light = {src: '/swf/sifr/futura_light.swf'};
var futura_bold = {src: '/swf/sifr/futura_bold.swf'};

sIFR.activate(futura_light, futura_bold);
sIFR.domains = ['localhost', 'desaclubcupon.dev.altodot.xstrat.us'];

function replace_sIFR_elements() {
  // main titles
  sIFR.replace(futura_light, {
    wmode: 'transparent',
    selector: '.titleInsideContent,h2.title, #wrap.admin h2',
    css: '.sIFR-root {color:#F25607; text-transform:uppercase; font-size:30px;}'
  });

  sIFR.replace(futura_light, {
    wmode: 'transparent',
    selector: 'h2.titleBig',
    css: '.sIFR-root {color:#F25607; text-transform:uppercase; font-size:40px;}'
  });
  sIFR.replace(futura_light, {
    wmode: 'transparent',
    selector: 'h1.titleBig',
    css: '.sIFR-root {color:#F25607; text-transform:uppercase; font-size:40px;}'
  });


  sIFR.replace(futura_light, {
    wmode: 'transparent',
    selector: 'h1#_right_title',
    css: '.sIFR-root {color:#F25607; text-transform:uppercase; font-size:63px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#header .right_frame h2',
    css: '.sIFR-root {color:#ffffff; font-weight:bold; font-size:17px;} .sIFR-root strong {color:#F25607; font-size:22px}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '.blackTitleBig',
    css: '.sIFR-root {color:#000000; font-weight:bold; font-size:22px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: 'h4.blackTitleSmall',
    css: '.sIFR-root {color:#000000; font-weight:bold; font-size:14px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: 'h4.blackTitleSmallCentered',
    css: '.sIFR-root {color:#000000; font-weight:bold; font-size:14px; text-align:center;}'
  });

  // #deal_price
  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: 'h2#deal_price',
    css: '.sIFR-root {color:#000000; text-align:center; text-transform:uppercase; font-size:39px; font-weight:bold;}'
  });

  // #deal_data
  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#deal_data .prices .first strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:15px; font-weight:bold; vertical-align:middle;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#deal_data .prices .middle strong',
    css: '.sIFR-root {color:#F25607; text-align:center; text-transform:uppercase; font-size:20px; font-weight:bold; text-indent:5px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#deal_data .prices .last strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:18px; font-weight:bold; vertical-align:middle; text-indent:10px;}'
  });

  // sidebar
  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer.small .prices .first strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:10px; font-weight:bold; vertical-align:middle;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer.small .prices .middle strong',
    css: '.sIFR-root {color:#ffffff; text-align:center; text-transform:uppercase; font-size:11px; font-weight:bold; text-indent:5px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer.small .prices .last strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:10px; font-weight:bold; vertical-align:middle; text-indent:10px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer .prices .first strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:12px; font-weight:bold; vertical-align:middle;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer .prices .middle strong',
    css: '.sIFR-root {color:#ffffff; text-align:center; text-transform:uppercase; font-size:14px; font-weight:bold; text-indent:5px;}'
  });

  sIFR.replace(futura_bold, {
    wmode: 'transparent',
    selector: '#side_offer .prices .last strong',
    css: '.sIFR-root {color:#5F5F5F; text-align:center; text-transform:uppercase; font-size:12px; font-weight:bold; vertical-align:middle; text-indent:10px;}'
  });
}