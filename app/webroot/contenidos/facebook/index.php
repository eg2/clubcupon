<?php



require_once '../../../config/database.php';

// Conexion a la base de datos
$db = new DATABASE_CONFIG();
$link = mysql_connect($db->default['host'], $db->default['login'], $db->default['password']);
if (!$link) {
    die('No pudo conectarse: ' . mysql_error());
}
mysql_select_db($db->default['database']); 

// Funcion que arma la URL de una oferta
function getImageUrl($deal,$name,$attach) {
	$model = "Deal";
    $options = array(
		'dimension' => 'small_big_thumb',
		'class' => '',
		'alt' => $name,
		'title' => $name,
		'type' => 'jpg'
     );
	$security_salt = "e9a5561e42c5ab47c6c81c14f06c0b8281cfc3ce";
	$site_name = "ClubCupon"; 
	$static_domain = "http://www.clubcupon.com.ar";
	$image_hash = $options['dimension'] . '/' . $model . '/' . 
				  $attach. '.' . 
				  md5($security_salt. $model. $attach . $options['type']. $options['dimension']. $site_name) . 
				  '.' . $options['type'];
 			  
	return $static_domain . '/img/' . $image_hash;
}

function getDealUrl($dealId,$slug,$startDate) {
	return "http://www.clubcupon.com.ar/ciudad-de-buenos-aires/deal/".$slug."?popup=no&utm_source=facebook&utm_medium=fanPageApp&utm_campaign=".$dealId."&utm_content=".$startDate ;
}

// Query que trae las ofertas
$query = sprintf(" SELECT d.id, d.name, d.subtitle, d.slug, d.original_price, d.discounted_price, d.discount_percentage, date(d.start_date) as start_date, a.id as attach
				   FROM deals d, attachments a
				   WHERE date(d.start_date) = date(now()) 
				   AND d.id = a.foreign_id
				   AND a.class = 'Deal'
				   AND d.city_id = 42550 
				   AND d.id = d.parent_deal_id 
				   ORDER BY d.is_side_deal, d.priority "
				);
$result = mysql_query($query);

// Comprobar resultado del query
if (!$result) {
    $message  = 'Invalid query: ' . mysql_error() . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Club Cup&oacute;n</title>
<meta name="description" content="Club Cup&oacute;n" />
<meta name="keywords" content="Club Cup&oacute;n" />

<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/global.css" />

</head>
<body>
    <div class="contentListFacebook">    
        <div class="columnaDerecha">
            <div class="headerListado">
            <a href="#"><img src="img/header_App.jpg" alt="" title="" /></a>
            </div>                    
			<?php
			// Ciclo que imprime las ofertas
			while ($row = mysql_fetch_assoc($result)) {
			?>
		            <div class="bloqueOfertas"><!--empieza oferta-->
		            	<div class="imgContent">
		                	<a href="<? echo getDealUrl($row['id'],$row['slug'],$row['start_date']) ; ?>" target="_blank">
									<img width="180px" height="120px" src="<? echo getImageUrl($row['id'],$row['name'],$row['attach']); ?>" 
										 alt="<? echo htmlentities($row['name']); ?>" title="<? echo htmlentities($row['name']); ?>" /></a>
		                </div> 
		                <div class="compraContent">
		                	<div class="beneficio">
		                    	<p>Beneficio</p>
		                        <p><? echo round($row['discount_percentage']); ?>%</p>
		                    </div>
		                    <div class="precio">
		                    	<p>Precio</p>
		                        <p>$<? echo round($row['discounted_price']); ?></p>
		                    </div>
		                    <a class="botonComprar" href="<? echo getDealUrl($row['id'],$row['slug'],$row['start_date']) ; ?>" target="_blank">&iexcl;Comprar!</a>
		                </div> 
		                <div class="detalleContent">
		                	<p><span>$<? echo round($row['discounted_price']); ?> en vez de $<? echo round($row['original_price']); ?></span> por <? echo htmlentities($row['name']);?></p>  
		                    <p class="textoDetalle"><? echo htmlentities($row['subtitle']);?>...</p>                  
		                </div>             
		            </div><!--termina oferta-->  
			<?
			}
			?>
        </div>
	</div> <!-- CIERRE MAIN-->
</body>
</html>
<?
// Libero los recursos asociados con el resultset
// y cierro la conexion.
mysql_free_result($result);
mysql_close($link);
?>