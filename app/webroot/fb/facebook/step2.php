<?php
	if (!empty($_POST['apikey']) && !empty($_POST['apisecret']) && !empty($_POST['onetimelogintoken'])) {
		include('facebook/facebook.php');
		$facebookAdmin = new FacebookRestClient($_POST['apikey'], $_POST['apisecret']);
		$result = $facebookAdmin->call_method('facebook.auth.getSession',array('auth_token' => $_POST['onetimelogintoken'], 'generate_session_secret' => true));
		echo '<pre>';
		print_r($result);
		echo '</pre>';
		exit;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Create Facebook Session Token</title>
</head>
<body>
	<h2>Create Facebook Session Token</h2>
	<form action="step2.php" method="post">
		<table>
			<tr>
				<td><label for="apikey">Facebook API Key</label></td>
				<td><input type="text" name="apikey" id="apikey" size="40" /></td>
			</tr>
			<tr>
				<td><label for="apisecret">Facebook Secret Key</label></td>
				<td><input type="text" name="apisecret" id="apisecret" size="40" /></td>
			</tr>
			<tr>
				<td><label for="onetimelogintoken">Facebook One Time Login Token</label></td>
				<td><input type="text" name="onetimelogintoken" id="onetimelogintoken" size="40" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="submitForm" value="Submit" /></td>
			</tr>
		</table>
	</form>
</body>
</html>