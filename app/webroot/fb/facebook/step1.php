<?php
	if (!empty($_POST['apikey'])) {
		$redirect = 'https://login.facebook.com/code_gen.php?api_key=' . $_POST['apikey'] . '&v=1.0';
		header('location:' . $redirect);
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Create One Time Login Token</title>
</head>
<body>
	<h2>Create One Time Login Token</h2>
	<form action="step1.php" method="post">
		<table>
			<tr>
				<td><label for="apikey">Facebook API Key</label></td>
				<td><input type="text" name="apikey" id="apikey" size="40" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="submitForm" value="Submit" /></td>
			</tr>
		</table>
	</form>
</body>
</html>