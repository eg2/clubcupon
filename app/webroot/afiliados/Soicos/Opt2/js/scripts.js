$(document).ready(function(){							   
	setTimeout(function(){
		$.colorbox({
		    href: 'popup.php',
		    transition: "none",
		    overlayClose: false,
		    escKey: false,
		    onComplete: function() {
		        $("#emailaddress").focus(function() {
		            if(!$(this).data('default')) $(this).data('default', $(this).val());
		            if($(this).val() == $(this).data('default'))
		                $(this).val('');
	                $(this).css('background-color', '#FFFFFF');
		        }).blur(function() {
		            if($.trim($(this).val()).length==0) $(this).val($(this).data('default'));
		            if(!checkEmail())
		            {
		                $(this).css('background-color', '#FEF1EC');
		                $(this).attr('title', 'Por favor ingrese una dirección de correo válida.');
		            }
		        });

		        $("#suscribeForm").submit(function() {
                    if(checkEmail() && $("#city_id").val()!=0)
                    {
                        $.ajax({
                            url: $(this).attr('action'),
                            method: 'post',
                            complete: function() {
                                $.colorbox({
                                    href: 'gracias.php',
                                    transition: "none",
		                            overlayClose: false,
		                            escKey: false,
		                            onComplete: function() {
                                        new Image().src = 'http://soicos.com/conv.php?pid=213';
		                            }
                                });
                            }
                        });
                    }
                    return false;
                });
		    }
	    });
	}, 1000);

	function checkEmail()
	{
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test($('#emailaddress').val())) {
            return false;
        }
        return true;
    }
});
