<?php
$time = explode(' ', microtime());
$time = $time[1] + $time[0]; // return array
$begintime = $time; //define begin time
define('Caf_FIID', 'CLCU');
/* saves to redemtions_exports_history the exported file */
define('Caf_log_exports_history', true);
// En etapa de homologación deberás utilizar HML3 y cuando lo generes para producción deberás poner PRO3
define('Caf_LogicalNetwork', 'PRO3');
define('Caf_bin', '62797603');
session_write_close();
session_id($_COOKIE['CAKEPHPX']);
session_start();

 require dirname(__FILE__)."/../../../config/database.php";


 function isAdmin(){

     $has_admin_credential = false;
     if(isset($_SESSION['Auth']))
     {
         if(isset($_SESSION['Auth']['User']))
         {
             if(isset($_SESSION['Auth']['User']['user_type_id'])){
                 $has_admin_credential = in_array($_SESSION['Auth']['User']['user_type_id'],
                         array(6));
             }
         }
     }
     return $has_admin_credential;
 }

 function reformatPrice($amount, $rounding) {
        if (!in_array(strtoupper($rounding), array(
                    'ROUND_CEIL',
                    'ROUND_FLOOR',
                    'REMOVE_DECIMAL_POINT'
                ))) {
            throw new InvalidArgumentException("parameter \$rounding must be any of the following 'ROUND_CEIL','ROUND_FLOOR', 'REMOVE_DECIMAL_POINT'");
        }
        if ($rounding == 'ROUND_CEIL') {
            $price = ceil($amount);
        }
        if ($rounding == 'ROUND_FLOOR') {
            $price = floor($amount);
        }
        if ($rounding == 'REMOVE_DECIMAL_POINT') {
            $price = join("", explode('.', $amount));
        }
        return str_pad($price, 12, "0", STR_PAD_LEFT);
    }

     function cleanUsernameString($username) {
        $username = preg_replace("/(á|Á|ä|Ä|À|à)/", "A", $username);
        $username = preg_replace("/(é|É|ë|Ë|È|è)/", "E", $username);
        $username = preg_replace("/(í|Í|ï|Ï|Ì|ì)/", "I", $username);
        $username = preg_replace("/(ó|Ó|ö|Ö|Ò|ò)/", "O", $username);
        $username = preg_replace("/(ú|Ú|ü|Ü|Ù|ù)/", "U", $username);
        $username = preg_replace("/(ñ|Ñ)/", "N", $username);
        $username = preg_replace("/(Ç|ç|Ḉ|ḉ)/", "C", $username);
        $username = strtoupper($username);
        return $username;
    }



 function _writeFileHeader($date) {
        // son todos los campos que exige el archivo NEG
        $year = $date['year'];
        $year_yy = $year - 2000;
        $month = $date['month'];
        $day = $date['day'];
        $hour = $date['hour'];
        $min = $date['min'];
        $sec = $date['sec'];
        $milsec = $date['u'];
        $date = $year . $month . $day;
        $file_header = $cant_file = '000000001'; //contador de registros
        $file_header.= $record_type = 'FH'; //tipo de registro
        $file_header.= $refresh_type = '0'; //tipo refresh 0 = Full, 1 = parcial
        $file_header.= $code_file_refresh = 'CF'; //cod archvo a refrescar CF = CAF
        $file_header.= $refresh_group = Caf_FIID; //Internamente lo llamamos FIID, el valor te lo pasaremos al momento de homologar…
        $file_header.= $tape_date = $year . $month . $day; //fecha del refresh
        $file_header.= $tape_time = $hour . $min; //hora de refresh
        $file_header.= $log_network = Caf_LogicalNetwork; //En etapa de homologación deberás utilizar HML3 y cuando lo generes para producción deberás poner PRO3 //red logica donde se procesa el refresh
        $file_header.= $release_number = '60'; //nro de release de base 24
        $file_header.= $partition_number = '  '; //Como es Full entiendo que debería ser blanco, en el document está aclarado.
        /*
         * Last Extract Date for POS   : Como siempre sera FULL deben poner la misma
         * fecha que en Tape Date, sino iría la fecha de Tape Date de la última vez que hicieron el full
         */
        /*
         * En los campos de Last Extract date for ATM y Last Extract date for POS debe ir la misma fecha.
         * En los campos de Impacting start date for ATM e Impacting start date for POS debe ir la misma fecha.
         * En los campos de Impacting start time for ATM e Impacting start time for POS debe ir la misma hora.
         *
         */
        $file_header.= $last_extract_date_atm = str_pad($year_yy . $month . $day, 6, " "); //Fecha del ultimo extract full realizado
        $file_header.= $impacting_start_date_atm = str_pad($year . $month . $day, 8, " "); //Fecha del último registro extractado.
        $file_header.= $impacting_start_time_atm = str_pad($hour . $min . $sec . $milsec, 12, "0"); //Hora del último registro extractado
        $file_header.= $last_extract_date_pos = str_pad($year_yy . $month . $day, 6, " ");
        $file_header.= $impacting_start_date_pos = str_pad($year . $month . $day, 8, " ");
        $file_header.= $impacting_start_time_pos = str_pad($hour . $min . $sec . $milsec, 12, "0");
        /*
         * -Last Extract Date for TLR : deben de dejar 9
         * - Impacting Start Date for TLR : deben de dejar 9
         * - Impacting Start Time for TLR : se completan 9.
         */
        $file_header.= $last_extract_date_tlr = str_pad("", 6, "9"); //Fecha del ultimo extract full realizado
        $file_header.= $impacting_start_date_tlr = str_pad("", 8, "9"); //Fecha del último registro extractado
        $file_header.= $impacting_start_time_tlr = str_pad("", 12, "9"); //Hora del último registro extractado
        $file_header.= $impacted_type = '1'; //Indica si se tiene que impactar los registros refrescados
        $file_header.= $caf_expnt = '0'; //para el cálculo de los montos en el CAF. 0 = Se utiliza el monto informado en el CAF
        $file_header.= $preauthorization_support = '0'; //Flag indicando si el Host soporta Preautorización
        $file_header.= $user_field = '     '; //campo no utilizado
        /*
         * - last extract date for TB : deben de dejar 9.
         * - Impacting Start Date for TB : deben de dejar 9.
         * - Impacting Start Time for TB : se completan 9.
         */
        $file_header.= $last_extract_date_tb = str_pad("", 6, "9");
        ; //Fecha del ultimo extract full realizado
        $file_header.= $impacting_start_date_tb = str_pad("", 8, "9"); //Fecha del último registro extractado
        $file_header.= $impacting_start_time_tb = str_pad("", 12, "9"); //Hora del último registro extractado
        $file_header = str_pad($file_header, 1298, " ", STR_PAD_RIGHT);
        return $file_header . "\r\n";
    }

     function _writeBlockHeader() {
        $block_header = '000000002BH';
        $block_header = str_pad($block_header, 1298, " ", STR_PAD_RIGHT);
        return $block_header . "\r\n";
    }


function _writeBlockTrailer($records) {
        $number_record = str_pad($records, 9, "0", STR_PAD_LEFT);
        $record_count = $number_record + 3;
        $block_trailer = str_pad($record_count, 9, "0", STR_PAD_LEFT);
        ; //Sumatoria de monto para refresh de PBF
        $block_trailer.= $record_type = 'BT'; //Tipo de Registro
        $block_trailer.= $amount = str_pad("", 18, "0");
        ; //Sumatoria de monto para refresh de PBF
        $block_trailer.= $number_record; //Cantidad de registros sin Header y Trailer.
        $block_trailer = str_pad($block_trailer, 1298, " ", STR_PAD_RIGHT);
        return $block_trailer . "\r\n";
    }

     function _writeFileTrailer($records) {
        $number_record = str_pad($records, 9, "0", STR_PAD_LEFT);
        $record_count = $number_record + 4;
        $file_trailer = str_pad($record_count, 9, "0", STR_PAD_LEFT); //Contador de Registro
        $file_trailer.= $record_type = 'FT'; //Tipo de Registro
        $file_trailer.= $number_record; //Cantidad de registros sin Header y Trailer
        $file_trailer.= $next_file = '0'; //indica si existe otro archivo a procesar
        $file_trailer.= $user_filler = '   '; //campo no utilizado
        $file_trailer = str_pad($file_trailer, 1298, " ", STR_PAD_RIGHT);
        return $file_trailer . "\r\n";
    }


 function get_posnets_code($options = array()) {
 $dbconfig = new DATABASE_CONFIG;

$db  = new mysqli($dbconfig->master['host'], $dbconfig->master['login'], $dbconfig->master['password'], $dbconfig->master['database']);

         $results = $db->query("SELECT *
        FROM (
        SELECT
            Redemption.posnet_code as columna_de_ordenamiento,
            Redemption.posnet_code as 'Redemption.posnet_code',
            DealUser.id as 'DealUser.id',
            DealUser.created as'DealUser.created',
            Deal.id as 'Deal.id',
            Deal.start_date as 'Deal.start_date',
            Deal.coupon_duration as 'Deal.coupon_duration',
            Deal.discounted_price as 'Deal.discounted_price',
            User.username  as 'User.username'
        FROM redemptions Redemption INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
        INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
        INNER JOIN users User ON User.id = DealUser.user_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
        where Redemption.posnet_code  > 627976030000000000
        AND Redemption.way is null
        and Redemption.reported_expired = 0
        and Redemption.reported_used = 0
        and Redemption.expired = 0
        and Company.has_posnet_device = 1
        UNION ALL
        SELECT
            Preredemption.posnet_code as columna_de_ordenamiento,
            Preredemption.posnet_code as 'Redemption.posnet_code',
            'null' as 'DealUser.id',
            Deal.created as'DealUser.created',
            Deal.id as 'Deal.id',
            Deal.start_date as 'Deal.start_date',
            Deal.coupon_duration as 'Deal.coupon_duration',
            Deal.discounted_price as 'Deal.discounted_price',
            'CONSUMIDOR FINAL'  as 'User.username'

        FROM preredemptions Preredemption
        INNER JOIN deals Deal ON Deal.id = Preredemption.deals_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
            WHERE Preredemption.is_assigned = 0
            and Company.has_posnet_device = 1

        ) AS Redemption

        ORDER BY columna_de_ordenamiento asc
    ");




     if(empty($results))
     {
         return false;
     }


     $posnet = array();

     while($row = $results->fetch_assoc())
     {
        $posnet[] = array(
            'Redemption'  => array(
                            'posnet_code' => $row['Redemption.posnet_code']
                            ),
            'DealUser' => array('created' =>$row['DealUser.created']),
            'Deal' =>array(
                        'id' => $row['Deal.id'],
                        'start_date' => $row['Deal.start_date'],
                        'coupon_duration' => $row['Deal.coupon_duration'],
                        'discounted_price' => $row['Deal.discounted_price']
                    ),
            'User' => array('username' => $row['User.username']
              )


        );

     }

    return $posnet;

    }

function updateSent($codes, $status)
{
    $dbconfig = new DATABASE_CONFIG;
    $db  = new mysqli($dbconfig->master['host'], $dbconfig->master['login'], $dbconfig->master['password'], $dbconfig->master['database']);

   $results = $db->query(sprintf('UPDATE TABLE redemptions set sent="%s" , reported_new=1 where  posnet_code in (%s) ', date('Y-m-d H:i:s.u'), join(",", $codes)));

}
 function _writeCafRecord($status, $handle) {

        //$r = & new Redemption();
        $results = get_posnets_code();
        if (!$results) {
            return false;
        }
        $i = 0;
        $record_count = 2;
        $coupons = array();
        $coupons_to_update = array();
        foreach ($results as $coupon) {
            $vigencia = date("ym", strtotime($coupon['DealUser']['created']));
            $vencimiento = date("ym", strtotime($coupon['Deal']['start_date'] . " +" . $coupon['Deal']['coupon_duration'] . " day"));
            $vencimiento_iso_8601 = date("Ymd", strtotime($coupon['Deal']['start_date'] . " +" . $coupon['Deal']['coupon_duration'] . " day"));
            $deal_id = $coupon['Deal']['id'];
            $username = $coupon['User']['username'];
            $username = cleanUsernameString($username);
            $username = str_pad($username, 20, " ", STR_PAD_RIGHT);
            $deal_discounted_price = $coupon['Deal']['discounted_price'];
            $deal_discounted_price = reformatPrice($deal_discounted_price, 'ROUND_CEIL');
            // $this->Redemption->updatedSent($coupon['Redemption']['posnet_code'], $status);
            $row = array();
            $posnet_code = str_pad($coupon['Redemption']['posnet_code'], 19, " ", STR_PAD_RIGHT);
            $row[] = $length = "0616";
            $record_count+= 1;
            $row[] = str_pad($record_count, 9, 0, STR_PAD_LEFT);
            $row[] = $pan = $posnet_code;
            switch ($status) {
                case 'used':
                case 'expired':
                    $record_type = "D"; //baja
                    break;

                default:
                    $record_type = "A"; //alta
                    break;
            }
            $row[] = $member_number = "000";
            $row[] = $record_type;
            $row[] = $card_type = "C ";
            $row[] = $fiid = Caf_FIID;
            $row[] = $card_state = "1";
            $row[] = $pin_offset = str_pad("", 16, " ");
            $row[] = $pin_offset2 = str_pad("", 16, " ");
            $row[] = $ttl_withdrawal_limit = str_pad("", 12, "0");
            $row[] = $offline_withdrawal_limit = str_pad("", 12, "0");
            $row[] = $ttl_cash_advance_limit = str_pad("", 12, "0");
            $row[] = $offline_cash_advance_limit = str_pad("", 12, "0");
            $row[] = $ttl_pf_limit = str_pad("", 12, "0");
            $row[] = $offline_pf_limit = str_pad("", 12, "0");
            /* o sea el valor que contenga ese cupon. */
            $row[] = $aggregate_limit = $deal_discounted_price; //str_pad("", 12, "0", STR_PAD_LEFT);
            $row[] = $aggregate_limit_offline = $deal_discounted_price; //str_pad("", 12, "0", STR_PAD_LEFT);
            $row[] = $user_filler = str_pad("", 96, "0");
            /*
             * - First Used Date : deben dejar el espacio VACIO.
             * - Last Reset Date : deben dejar el espacio VACIO.
             */
            $row[] = $first_used_date = str_pad("", 6, " "); //"YYMMDD";
            $row[] = $last_reset_date = str_pad("", 6, " "); // "YYMMDD";
            $row[] = $expiration_date = $vencimiento; //"0000";//"YYMM";
            $row[] = $effective_date = $vigencia; // "0000";//"YYMM";
            $row[] = $user_filler = str_pad("", 1, " ");
            $row[] = $secondary_card_expiration_date = "0000";
            $row[] = $secondary_card_effective_date = "0000";
            $row[] = $secondary_card_state = " ";
            $row[] = $user_filler = str_pad("", 33, " ");
            $row[] = $credit_class = "01";
            $row[] = $user_filler = str_pad("", 150, " ");
            $datos_discrecionales = "";
            $service_code = 101; //Campo de 3 dígitos de verificación opcional.Verificación obligatoria para CABAL
            $row[] = $cmstrack1 = str_pad("%B" . trim($pan) . "^" . $username . "^" . $expiration_date . $service_code . $datos_discrecionales . "?", 82, " ", STR_PAD_RIGHT);
            $row[] = $cmstrack2 = str_pad(";" . trim($pan) . "=" . $expiration_date . $service_code . $datos_discrecionales . "?", 40, " ", STR_PAD_RIGHT);
            $row[] = $cmsfechavig = $vencimiento_iso_8601; // "00000000";//"AAAAMMDD";
            $row[] = Caf_FIID;
            $base_segment = join("", $row);
            $row = array();
            $row[] = $length = "0356";
            $row[] = $segment_data = str_pad("0000", 12, " ", STR_PAD_RIGHT);
            $row[] = $ttl_purchase_limit = $deal_discounted_price; // str_pad("", 12, "0");
            $row[] = $offline_purchase_limit = $deal_discounted_price; //str_pad("", 12, "0");
            $row[] = $ttl_cash_advance_limit = str_pad("", 12, "0");
            $row[] = $offline_cash_advance_limit = str_pad("", 12, "0");
            $row[] = $ttl_withdrawal_limit = str_pad("", 12, "0");
            $row[] = $offline_withdrawal_limit = str_pad("", 12, "0");
            $row[] = $ttl_cc_limit = str_pad("", 12, "0");
            $row[] = $offline_cc_limit = str_pad("", 12, "0");
            $row[] = $ttl_pf_limit = str_pad("", 12, "0");
            $row[] = $offline_pf_limit = str_pad("", 12, "0");
            $row[] = $ttl_pc_limit = str_pad("", 12, "0");
            $row[] = $offline_pc_limit = str_pad("", 12, "0");
            $row[] = $user_filler = str_pad("", 144, "0");
            $row[] = $uselimit = str_pad("1", 4, "0", STR_PAD_LEFT);
            $row[] = $ttl_refund_credit_limit = str_pad("1", 12, "0", STR_PAD_LEFT);
            $row[] = $offline_refund_credit_limit = str_pad("1", 12, "0", STR_PAD_LEFT);
            $row[] = $reason_code = " ";
            $row[] = $last_used = "000000"; //"YYMMDD";
            $row[] = $user_filler = " ";
            $row[] = $issuer_profile = str_pad("", 16, " ");
            $pos_segment = join("", $row);
            $row = array();
            $row[] = $length = "0040";
            $row[] = $account_count = "01";
            for ($n = 0; $n < intval($account_count); $n++) {
                /* Tipo de cuenta "31" */
                $row[] = $account_type = "31";
                /* Accounts. Number: Numero de cuenta "627976..." (el mismo dato que el campo PAN: Numero de tarjeta) */
                $row[] = $pan;
                /* Stat.Estado de la cuenta. "1" */
                $row[] = $account_state = "1";
                /*  Accounts. Descr: Descripción de la cuenta. "          " (diez espacios en blanco) */
                $row[] = $account_desc = str_pad("", 10, " ");
                /* Accounts. Corp: Campo no utilizado. "0" */
                $row[] = $account_corp = "0";
                /* Accounts. Qual: Flag para calificación de cuentas. "0" */
                $row[] = $account_qual = "0";
            }
            $account_segment = join("", $row);
            fwrite($handle, str_pad($base_segment . $pos_segment . $account_segment, 1298, " ", STR_PAD_RIGHT) . "\r\n");
            $i++;
            $coupons_to_update[] = $coupon['Redemption']['posnet_code'];
        }
        if (!empty($coupons_to_update)) {
            updateSent($coupons_to_update, $status);
        }
        return $i;
    }

if(!isAdmin()){
    header('HTTP/1.1 403 Forbidden');
    echo "<h1>Not authorized</h1>";
    echo "<h2>try to log in as admin</h2>";
    die;

}






        $max_execution_time = ini_get('max_execution_time');
        if (isset($_GET['clearlog'])) {
            if(is_dir(dirname(__FILE__)."/tmp/")){
                try {
                    unlink(dirname(__FILE__)."/tmp/process.log");
                } catch(Exception $e) {
                //
                }
            }

        }


        if (isset($_GET['file'])) {

             if(!is_dir(dirname(__FILE__)."/tmp/")){
                 mkdir (dirname(__FILE__)."/tmp/");
             }
            ini_set('max_execution_time', 0);


            $temp_file = tempnam("/tmp", "caf_export_" . date("YmdHisu") . "_");
            $handle = fopen($temp_file, "a+");


            list($date['year'], $date['month'], $date['day'], $date['hour'], $date['min'], $date['sec'], $date['u']) = explode("-", date("Y-m-d-H-i-s-u"));
            fwrite($handle, _writeFileHeader($date));
            fwrite($handle, _writeBlockHeader());
            $the_status = array(
                'new'
            );
            $caf_array = array();
            foreach ($the_status as $status) {
                $cant_records = _writeCafRecord($status, $handle);
            }
            fwrite($handle, _writeBlockTrailer($cant_records));
            fwrite($handle, _writeFileTrailer($cant_records));
            $file_name = "caf_export_" . date("YmdHisu");
            $res = false;
            fclose($handle);
            $zip_filename = "caf_export";
            if (in_array("ZipArchive", get_declared_classes())) {
                $zip = new ZipArchive;
                $zip_filename.= ".zip";
                $res = $zip->open( dirname(__FILE__)."/tmp/" . $zip_filename, ZipArchive::OVERWRITE);
                $filemime = "application/x-zip";
                if ($res === TRUE) {
                    $zip->addFile($temp_file, "caf_export.txt");
                    $zip->close();
                }
            } else {
                $zip_filename.= ".bz2";
                $bz = bzopen( dirname(__FILE__)."/tmp/" .  $zip_filename, "w");
                $res = $bz !== false;
                $filemime = "application/x-bz2";
                bzwrite($bz, file_get_contents($temp_file));
                bzclose($bz);
            }

            unlink($temp_file);
            $time = explode(" ", microtime());
            $time = $time[1] + $time[0];
            $endtime = $time; //define end time
            $totaltime = ($endtime - $begintime); //decrease to get total time
            $fp = fopen(dirname(__FILE__)."/tmp/process.log", "a+");
            fwrite($fp,date("Y-m-d H:i:s").": ".__FILE__." duration= ". $totaltime." seconds\r\n");
            fclose($fp);

            // desactivo el layout o sino imprime html en el archivo NEG generado
            ini_set('max_execution_time', $max_execution_time);
            header("Content-Type: ".$filemime);
                header("Content-Disposition: attachment; filename=".$zip_filename);
                echo file_get_contents(dirname(__FILE__)."/tmp/".$zip_filename);
                unlink(dirname(__FILE__)."/tmp/".$zip_filename);

        }
        if(isset($_GET['showsession']))
        {
            print_r($_SESSION);
        }


