<?php
/**
 * Motor de Cache para CakePHP.
 * Implementacion de un controlador que agrega funcionamiento de cache antes de ejecutar la vista
 * a un archivo plano en disco para ser servido directamente por el webserver sin necesidad de cargar o ejecutar
 * el framework.
 *
 * El cache puede ser deshabilitado de forma global agregando en el core.php la siguiente linea.
 * <code>
 * 	Configure::write('app_cached_controller.disable',true);
 * </code>
 * Para utilizar el cache se debe modificar el controlador a cachear para que extienda esta clase y no el controlador
 * generico del proyecto.
 * <code>
 * class myController extends AppCachedController {
 * </code>
 *
 * El motor de cache busca antes de ejecutar la accion si la misma esta cacheada, si la encuentra y esta
 * habilitada muestra ese resultado y detiene la ejecucion del framework para evitar mayor carga. Si no la encuentra la ejecuta
 * y al terminar si corresponde guarda el resultado de la vista en disco para utilizar luego.
 * El cache se puede implementar para todas las acciones de un controlador o solo para algunas acciones.
 * Para todas las acciones se modifica el valor del atributo de clase "AppCachedController::$_cache_view" a true y listo.
 * Para algunas acciones se indica cuales en un array en el atributo de clase "AppCachedController::$cache_views".
 * Si se quiere obviar el cache para usuarios con session de Cake indicarlo en el atributo de clase "
 * "AppCachedController::$_bypass_cache_for_logged_users".
 *
 * Para un servir un cache mas eficiente ver @see AppCachedControllerTools
 *
 */
include_once (dirname(__FILE__).'/app_cached_controller_tools.php');

class AppCachedController extends AppController
{
  /**
   * Indica si se debe generar la vista o buscarla en el cache
   * Se puede indicar en general o en cada accion del controlador que implemente esta clase
   * si se quiere cachear la vista o no. Cambiando el valor del atributo en la clase se puede implementar cache para
   * todas las vistas.
   * <code>
   * 	 $this->_cache_view = true;
   * </code>
   * @var boolean
   */
  protected $_cache_view = false;

  /**
   *
   * Es un array que indica todas las acciones que se van a cachear.
   * Indicando en este array en vez de dentro de cada accion se habilita a saltear toda la ejecucion del
   * controlador ya que se busca en este array primero que nada en la ejecucion.
   * @var array
   */
  protected $cache_views = array();

  /**
   * Indica si se debe mostrar el cache o no a los usuarios con una sesion valida.
   * <code>
   * 	 $this->_bypass_cache_for_logged_users = true;
   * </code>
   * @var boolean
   */
  protected $_bypass_cache_for_logged_users = false;

  /**
   * Indica el tiempo de vida del cache, expresado en segundos.
   * @var int
   */
  protected $_cache_lifetime = 3600;

	/**
   * (non-PHPdoc)
   * @see Controller::afterFilter()
   */
  function afterFilter()
  {
    if ($this->mustSaveCache())
    {
      $this->buildViewCache();
    }
    parent::afterFilter();
  }

  /**
   * Ejecuta la comprobacion de cache y detiene la ejecucion con el cache
   * encontrado, si encuentra uno.
   * Este metodo puede llamarse en cualquier momento de una accion para detener
   * la ejecucion y escapar con la version cacheada del sitio.
   * @return void
   */
  public function checkCache()
  {
    // evitamos la comprobacion de cache si se lo pidio para usuarios logueados o  si estamos debugueando
    if (($this->Auth->user('id') && $this->_bypass_cache_for_logged_users == true) || (Configure::read('debug')>0) ||
        !isset($_COOKIE['CakeCookie']['first_time_user']))
    {
      return null;
    }
    // procesa o sirve el cache si existe
    AppCachedControllerTools::dispatchCache($_SERVER['REQUEST_URI'],$this->_cache_lifetime);
  }

  /**
   * (non-PHPdoc)
   * @see AppController::beforeFilter()
   */
  function beforeFilter()
  {
    $this->checkCache();
    parent::beforeFilter();
  }

  /**
   * Indica si es necesario recalcular el cache de la vista.
   * Se lo puede reescribir para validar de otra manera cuando es necesario recalcularlo.
   * @return boolean
   */
  function mustSaveCache()
  {
    if (Configure::read('debug')<1 && ($this->_cache_view || in_array($this->action, $this->cache_views)) && Configure::read('app_cached_controller.disable') != true && ($this->_bypass_cache_for_logged_users == true && $this->Auth->user('id')==null))
    {
      return true;
    } else {
      return false;
    }
  }

	/**
   * Ejecuta el motor de cache sobre un controlador luego de que este se haya ejecutado
   * @return void
   */
  private function buildViewCache()
  {
    AppCachedControllerTools::mkdirs(dirname($this->getCacheFileName()));
//    $result = "<!-- Cached view for: ".$_SERVER['REQUEST_URI']." -->\n";
    $result = $this->output;
    if ($this->_bypass_cache_for_logged_users == true)
    {
      // no se lo necesita con el hack del index
      // $result = $this->addSessionValidationCode($result);
    }
    if (!file_put_contents($this->getCacheFileName(), $result))
    {
      trigger_error('Error al generar el cache');
    }
  }

  /**
   * Genera un codigo PHP para anteponer a la vista generada que forza validacion del usuario
   * y si el usuario esta logueado evita la version cacheada de la pagina. Se usa solo en el caso
   * de cacheo de vista completo.
   * @param string $output resultado de la vista computado
   * @return string
   */
  private function addSessionValidationCode($output)
  {
    $cookie_name = Configure::read('Session.cookie');
    $root_path = WWW_ROOT.'index.php';
    $pre_code = <<<CODE
<?php
if (isset(\$_COOKIE['${cookie_name}'])):
  session_id(\$_COOKIE['${cookie_name}']);
  session_start();
endif;
if (isset(\$_SESSION['Auth']['User'])):
	header('Location: /index.php?url='.\$_SERVER['REQUEST_URI']);
	die;
else:
?>
CODE;
    $post_code = <<<CODE
<?php endif; ?>
CODE;
    return $pre_code.$output.$post_code;
  }

  /**
   * Genera el nombre del archivo de cache
   * @return string
   */
  private function getCacheFileName()
  {
    return AppCachedControllerTools::buildCacheFileName($_SERVER['REQUEST_URI']);
  }

  /**
   * Metodo para eliminar todo el cache generado
   * @param mixed $mixed puede ser un string o un array de paginas o nada y se borra todo el cache, es la URL
   * @return void
   */
  static function clearCache($mixed = null)
  {
    AppCachedControllerTools::clearCache($mixed);
  }

}