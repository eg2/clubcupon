<?php

class AccountingCompanyIdGrouped extends AppModel {

    public $name = 'AccountingCompanyIdGrouped';
    public $alias = 'AccountingCompanyIdGrouped';
    public $useTable = 'accounting_company_id_grouped';
    public $recursive = - 1;

    function __findAllByCompanyIdGroupedByLiquidateIdAndMode($conditions, $mode) {
        
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
            'max_payment_date' => $conditions['conditions']['max_payment_date'],
        );
        switch ($mode) {
            case 'Redimido':
                $conditionsForFind['pay_by_redeemed'] = 1;
                break;
            case 'Vendido':
                $conditionsForFind['pay_by_redeemed'] = 0;
                break;
/*            case 'Fondo':
                break;*/
            default: break;
        }
        $order = array(
            'liquidate_total_amount DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }

}