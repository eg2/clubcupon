<?php
class Rate extends AppModel
  {
    var $name         = 'Rate';
    var $displayField = 'created';


    function __construct ($id = false, $table = null, $ds = null)  { parent::__construct ($id, $table, $ds); }


    /**
     * devuelve la razón de conversión a la fecha <var>$date</var>.
     *
     * Devuelve la razón de conversión a la fecha <var>$date</var> buscando en <code>`rates`</code>
     * los registros tales que la fecha de creación sea a lo sumo la fecha pasada, tras esa query,
     * toma el campo <code>rate</code> del modelo <code>Rate</code>.
     *
     * @access public
     * @param datetime $date Fecha a la cual se desea saber la razón de conversión.
     * @return float|false
     */
    function get_rate_for ($date)
      {
        // Buscar la razón de conversión más nueva a la fecha, si no existe, fallar.
        $r = $this->find ('first', array ('conditions' => array ('created <= ' => $date), 'order' => 'created DESC'));
        if (empty ($r))  return false;

        return (float) $r ['Rate']['rate'];
      }

    /**
     * devuelve la razón de conversión al día de la fecha.
     *
     * Devuelve la razón de conversión al día de la fecha buscando en <code>`rates`</code> los
     * registros tales que la fecha de creación sea a lo sumo la fecha actual, tras esa query,
     * toma el campo <code>rate</code> del modelo <code>Rate</code>.
     *
     * equivalente a <code>$this->get_rate_for (date ('Y-m-d H:i:s'))</code>.
     *
     * @access public
     * @return float|false
     */
    function get_current_rate ()  { return $this->get_rate_for (date ('Y-m-d H:i:s')); }

    /**
     * devuelve el equivalente en dinero de los puntos <var>$points</var> con la razón actual.
     *
     * Devuelve el equivalente en dinero de los puntos <var>$points</var> con la razón actual
     * haciendo <code>$points * $this->get_current_rate ()</code>.
     *
     * @access public
     * @param int $points Cantidad de puntos a convertir.
     * @return float|false
     */
    function valuate ($points)
      {
        // Buscar la razón de conversión actual, si no existe, fallar.
        $r = $this->get_current_rate ();
        if ($r === false)  return false;

        return (float) $points * $r;
      }
  }
