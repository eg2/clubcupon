<?php
class Question extends AppModel
{
    var $name = 'Question';
    var $displayField = 'content';

    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'Answer' => array(
            'className' => 'Answer',
            'foreignKey' => 'question_id',
            'fields' => array('Answer.content','Answer.id')
        )
    );

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }
}
