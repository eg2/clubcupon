<?php

class DealCategory extends AppModel {
	var $name = 'DealCategory';
	var $actsAs = array('Tree');
  var $hasMany = array('Deal');

  function deleteCategoryUpdateDeal($id,$unknownId) {
    $dataSource = $this->getDataSource();
    $dataSource->begin($this);
    try {

      $ret = $this->Deal->updateAll(
          array('Deal.deal_category_id' => $unknownId),
          array('Deal.deal_category_id' => $id)
      );
      if(!$ret) {
        throw new Exception('Error al intentar actualizar a la categoría Unknown.');
      }
      if($this->removeFromTree($id, true) == false) {
        throw new Exception('Error al intentar borrar la categoría.');
      }
      $dataSource->commit($this);
    } catch(Exception $e) {
      $dataSource->rollback($this);
      throw new Exception($e->getMessage());
    }
    return true;
  }
  
  function findByName($name) {
  	return $this->find('first', array(
  			'conditions' => array(
  					'DealCategory.name' => $name
  			),
  			'recursive' => -1
  	));
  }

}

