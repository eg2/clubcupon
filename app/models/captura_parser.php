<?php

class CapturaParser
{

    private $captura_records;
    private $file_header;
    private $file_trailer;


    public function __construct($captura_file_path, $options = array()) {
        if (!file_exists($captura_file_path)) {
            throw new Exception(sprintf("couldn't read file %s", $captura_file_path));
        }

        $this->populateClassMembers($captura_file_path);
    }

    private function populateClassMembers($file) {
        $fp = fopen($file, 'r');
        $data = array();
        while ($line = fscanf($fp, "%[ -~]")) {
            /* if(strlen($line[0])!=1298)
              {
              throw new Exception("Invalid line length");
              } */
            $str = rtrim($line[0]);
            if(!empty($str))
            $data[] = $str;
        }
        fclose($fp);
        unset($line);


        $this->file_header = array_shift($data);
        $this->file_trailer = array_pop($data);
        $this->captura_records = $this->capturaFactory($data);
    }


    public function getCapturaRecords() {
        return $this->captura_records;
    }

    public function getHeaderEmisor() {
        return substr($this->file_header, 0, 8);
    }

    public function getHeaderFiller() {
        return substr($this->file_header, 08, 12);
    }

    public function getHeaderCodMov() {
        return substr($this->file_header, 20, 3);
    }

    public function getHeaderFiller2() {
        return substr($this->file_header, 23, 5);
    }

    public function getHeaderTipoReg() {
        return substr($this->file_header, 28, 3);
    }

    public function getHeaderFiller3() {
        return substr($this->file_header, 31, 16);
    }

    public function getHeaderFecha() {
        return substr($this->file_header, 47, 6);
    }

    public function getHeaderFiller4() {
        return substr($this->file_header, 53, 40);
    }

    public function getHeaderCodMon() {
        return substr($this->file_header, 93, 3);
    }

    public function getHeaderFiller5() {
        return substr($this->file_header, 96, 72);
    }

    public function getTrailerEmisor() {
        return substr($this->file_trailer, 0, 8);
    }

    public function getTrailerFiller() {
        return substr($this->file_trailer, 8, 12);
    }

    public function getTrailerCodMov() {
        return substr($this->file_trailer, 20, 3);
    }

    public function getTrailerFiller2() {
        return substr($this->file_trailer, 23, 5);
    }

    public function getTrailerTipoReg() {
        return substr($this->file_trailer, 28, 3);
    }

    public function getTrailerFiller3() {
        return substr($this->file_trailer, 31, 24);
    }

    public function getTrailerImportTot() {
        $parte_entera = substr($this->file_trailer, 55, 13);
        $parte_decimal = substr($this->file_trailer, 68, 2);
        return $parte_entera.".".$parte_decimal;
    }

    public function getTrailerImportSDesc() {
        $parte_entera = substr($this->file_trailer, 70, 13);
        $parte_decimal = substr($this->file_trailer, 83, 2);
        return $parte_entera.".".$parte_decimal;
    }

    public function getTrailerCantidad() {
        return substr($this->file_trailer, 85, 8);
    }

    public function getTrailerCodMon() {
        return substr($this->file_trailer, 93, 3);
    }

    public function getTrailerFiller4() {
        return substr($this->file_trailer, 96, 72);
    }



     private function capturaFactory($data) {

        App::import('Model', 'CapturaRecord');


        $file_header = array(
            'Emisor' => $this->getHeaderEmisor(),
            'CodMov' => $this->getHeaderCodMov(),
            'TipoReg' => $this->getHeaderTipoReg(),
            'Fecha' => $this->getHeaderFecha(),
            'CodMon' => $this->getHeaderCodMon(),
        );

        $file_trailer = array(
            'Emisor' => $this->getTrailerEmisor(),
            'CodMov' => $this->getTrailerCodMov(),
            'TipoReg' => $this->getTrailerTipoReg(),
            'ImportTot' => $this->getTrailerImportTot(),
            'ImportSDesc' => $this->getTrailerImportSDesc(),
            'Cantidad' => $this->getTrailerCantidad(),
            'CodMon' => $this->getTrailerCodMon(),
        );
        $caps = array(
            'FILE_HEADER' => $file_header,
            'FILE_TRAILER' => $file_trailer,
            'RECORDS' => false,

        );
        foreach ($data as $captura_record) {
            $cr = new CapturaRecord($captura_record);
            $registro = array(
                'Comercio' => $cr->getComercio(),
                'CarRes' => $cr->getCarRes(),
                'Cupon' => $cr->getCupon(),
                'TipoReg' => $cr->getTipoReg(),
                'Tarjeta' => $cr->getTarjeta(),
                'Fecha' => $cr->getFecha(),
                'Cuotas' => $cr->getCuotas(),
                'ImportTot' => $cr->getImporteTotal(),
                'ImportSDesc' => $cr->getImporteSdesc(),
                'CodAut' => $cr->getCodAut(),
                'CodMon' => $cr->getCodMon(),
                'Hora' => $cr->getHora(),
                'Pin' => $cr->getPin(),
                'Track2' => $cr->getTrack2(),
                'CuponOrigDev' => $cr->getCuponOrigDev(),
                'FechaOrigDev' => $cr->getFechaOrigDev(),
                'IndConting' => $cr->getIndConting(),
                'Anulacion' => $cr->getAnulacion(),
                'CodRespuesta' => $cr->getCodRespuesta(),
                'PlanCuotas' => $cr->getPlanCuotas(),
                'AcctTyp' => $cr->getAcctType(),
                'AcctSource' => $cr->getAcctSource(),
                'TermTyp' => $cr->getTermTyp(),
                'Private' => $cr->getPrivate(),

            );



            $records[] = $registro;
        }
        $caps['RECORDS'] = isset($records) ? $records : false;
        return $caps;
    }

}