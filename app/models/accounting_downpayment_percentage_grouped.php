<?php

class AccountingDownpaymentPercentageGrouped extends AppModel {

    public $name = 'AccountingDownpaymentPercentageGrouped';
    public $alias = 'AccountingDownpaymentPercentageGrouped';
    public $useTable = 'accounting_downpayment_percentage_grouped';
    public $recursive = - 1;

    function __findAllByCompanyIdGroupedByMaxPaymentDateDPPercentage($conditions) {
        
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
            'max_payment_date' => $conditions['conditions']['max_payment_date'],
        );
        $order = array(
            'max_payment_date DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }

}
