<?php
class Neighbourhood extends AppModel
{

  public $name = 'Neighbourhood';
  public $alias = 'Neighbourhood';
  public $useTable = 'neighbourhoods';
  public $recursive = -1;

  var $belongsTo = array('Region','Neighbourhood');

}
