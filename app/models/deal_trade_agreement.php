<?php

class DealTradeAgreement extends AppModel {

    const ID_TRADICIONAL      = 1;
    const ID_PRE_COMPRA       = 2;
    const ID_PAGO_PROVEEDORES = 3;
    const ID_ANTICIPO         = 4;
    const ID_CANJE            = 5;
    
    
    public $name = 'DealTradeAgreement';
    public $alias = 'DealTradeAgreement';
    public $useTable = 'deal_trade_agreement';
    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        $conditions = array(
            'DealTradeAgreement.id' => $id,
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }
    function generateArrayOfConditions(){
    	$order = array(
    			'DealTradeAgreement.id ASC'
    	);
    	return $this->find('all', array(
    			'fields'=> array('DealTradeAgreement.id','DealTradeAgreement.is_end_user_default','DealTradeAgreement.is_end_user_enabled'),
    			'order' => $order
    	));
    }

}