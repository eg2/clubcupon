<?php
class CorporateGuest extends AppModel {

   var $name = 'CorporateGuest';

   var $validate = array(
       'pin' => array('required' => true)
   );

   function isValidCorporatePin($corporate_pin = null) {

       if(empty($corporate_pin)) {
           return false;
            //throw new InvalidArgumentException("Corporate pin must not be empty");
       }

       $count = $this->find('count', array('conditions' => array('pin' => $corporate_pin)));
       return ($count >= 1);
   }
}
