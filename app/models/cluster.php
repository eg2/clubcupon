<?php

App::import('Sanitize');

class Cluster extends AppModel
{
    var $name = 'Cluster';
    var $actsAs = array(
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'ClusterExample',
    );

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

    function getQueryFromClusterExamples($clusterExamples) {
      if(empty($clusterExamples)) {
        return false;
      }
      $orConditions = array();
      $selectPart = 'SELECT id FROM cluster_sources WHERE ';
      foreach($clusterExamples as $example) {
        $andConditions = $example['ClusterExample'];
        unset($andConditions['id']);
        unset($andConditions['cluster_id']);
        $andConditionsParsed = array();
        foreach($andConditions as $andField => $andValue) {
          if($andValue)
            $andConditionsParsed[] = '(  `'.$andField.'` = \'' . Sanitize::escape($andValue) . '\' )';
        }
        $orConditions[] = "(". implode(" AND ",$andConditionsParsed) . ")";
      }
      $query = $selectPart . implode(" OR ", $orConditions);
      return $query;
    }
}
