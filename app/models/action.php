<?php
class Action extends AppModel
  {
    var $name         = 'Action';
    var $displayField = 'name';

    var $hasMany = array
      (
        'ActionPoint' => array
          (
            'className'  => 'ActionPoint',
            'foreignKey' => 'action_id',
            'dependent'  => true,
          ),
      );


    function __construct ($id = false, $table = null, $ds = null)  { parent::__construct ($id, $table, $ds); }


    /**
     * devuelve los puntos que otorga la acción <var>$action_id</var> a la fecha <var>$date</var>.
     *
     * Devuelve los puntos que otorga la acción <var>$action_id</var> a la fecha <var>$date</var>
     * buscando en <code>`actions`</code> los registros que tengan la <code>`action_id`</code>
     * pasada y tales que la fecha de creación en <code>`action_points`</code> asociada sea a lo
     * sumo la fecha pasada, tras esa query, toma el primer elemento del modelo
     * <code>ActionPoint</code> resultante (como estos están ordenados de forma <i>decreciente</i>
     * por fecha de creación, el resultado será el puntaje asociado a la fecha dada).
     *
     * @access public
     * @param int $action_id ID de la acción cuyos puntos a la fecha <var>$date</var> se desean.
     * @param datetime $date Fecha a la cual se desea saber el puntaje.
     * @return int|false
     */
    function get_points_for ($action_id, $date)
      {
        // Buscar el ActionPoint más nuevo, si no existe, fallar.
        $ap = $this->ActionPoint->find ('first', array ('conditions' => array ('action_id' => $action_id, 'ActionPoint.created <= ' => $date), 'order' => 'ActionPoint.created DESC'));
        if (empty ($ap))  return false;

        return (int) $ap ['ActionPoint']['points'];
      }

    /**
     * devuelve los puntos que otorga la acción <var>$action_id</var> al día de la fecha.
     *
     * Devuelve los puntos que otorga la acción <var>$action_id</var> al día de la fecha buscando en
     * <code>`actions`</code> los registros que tengan la <code>`action_id`</code> pasada y tales
     * que la fecha en <code>`action_points`</code> asociada sea a lo sumo la fecha actual, tras esa
     * query, toma el primer elemento del modelo <code>ActionPoint</code> resultante (como estos
     * están ordenados de forma <i>decreciente</i> por fecha de creación, el resultado será el
     * puntaje asociado a la fecha).
     *
     * equivalente a <code>$this->get_points_for ($action_id, date ('Y-m-d H:i:s'))</code>.
     *
     * @access public
     * @param int $action_id ID de la acción cuyos puntos a la fecha se desean.
     * @return int|false
     */
    function get_current_points ($action_id)  { return $this->get_points_for ($action_id, date ('Y-m-d H:i:s')); }
  }
