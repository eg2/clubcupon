<?php
class ClusterExample extends AppModel
{
    var $name = 'ClusterExample';
    var $actsAs = array(
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
    );
    var $belongsTo = array(
        'Cluster'
    );

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }
}