<?php

class Pin extends AppModel {

    var $name = 'Pin';
    var $displayField = 'name';
    var $actsAs = array
        (
        'WhoDidIt' => array()
    );

    var $belongsTo = array
        (
        'Deal' => array
            (
            'className' => 'Deal',
            'foreignKey' => 'deal_id',
        ),
       'DealUser' => array (
           'className'  => 'DealUser',
           'foreignKey' => 'deal_user_id',
       ),
        	/*	'ProductProduct' => array
        		(
        				'className' => 'product.ProductProduct',
        				'foreignKey' => 'product_product_id',
        		),
        		*/ 
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    /**
     * devuelve el id y el código del próximo PIN libre para la oferta <var>$deal_id</var>.
     *
     * Devuelve el id y el código del próximo PIN libre para la oferta <var>$deal_id</var>
     * buscando los campos <code>`id`</code> y <code>`code`</code> de la tabla <code>`pins`</code>
     * de los registros que tengan el <code>`deal_id`</code> igual a <var>$deal_id</var> y tengan
     * <code>`is_used`</code> en falso.
     *
     * @access public
     * @param int $deal_id ID de la oferta cuyo próximo PIN se desea.
     * @return array(int,string)|array(false,false)
     */
    function next ($deal_id, $qty = 1) {
      $pins = $this->find ('all', array (
          'conditions' => array (
              'Pin.deal_id' => $deal_id,
              'Pin.is_used' => false,
          ),
          'fields' => array (
            'Pin.id',
            'Pin.code',
          ),
          'order' => array (
              'Pin.created'
          ),
          'limit' => $qty,
      ));

      if (count ($pins) == $qty) {
        $pin_ids = array ();
        $codes = '';
        foreach ($pins as $pin) {
          $pin_ids [] = $pin ['Pin']['id'];
          $codes = $pin ['Pin']['code'] . "\n" . $codes;
        }
        $codes = substr ($codes, 0, -1);
        $this->updateAll (array ('Pin.is_used' => true), array ('Pin.id' => $pin_ids));
        return array ('ids' => $pin_ids, 'code' => $codes);
      }

      return array ();
    }
    
    /**
     * devuelve el próximo PIN libre para el producto <var>$product_id</var>.
     */
    function nextByProduct ($product_id, $qty = 1) {
    	$pins = $this->find ('all', array (
    			'conditions' => array (
    					'Pin.product_id' => $product_id,
    					'Pin.is_used' => false,
    			),
    			'fields' => array (
    					'Pin.id',
    					'Pin.code',
    			),
    			'order' => array (
    					'Pin.created'
    			),
    			'limit' => $qty,
    	));
    
    	if (count ($pins) == $qty) {
    		$pin_ids = array ();
    		$codes = '';
    		foreach ($pins as $pin) {
    			$pin_ids [] = $pin ['Pin']['id'];
    			$codes = $pin ['Pin']['code'] . "\n" . $codes;
    		}
    		$codes = substr ($codes, 0, -1);
    		$this->updateAll (array ('Pin.is_used' => true), array ('Pin.id' => $pin_ids));
    		return array ('ids' => $pin_ids, 'code' => $codes);
    	}
    
    	return array ();
    }
    

    // hacerlo mas atomico
    function countPinsByDeal($deal_id){
    $result = $this->find('count', 
                        array('conditions' => array('Pin.deal_id'=>$deal_id)));
    return (!empty($result)?$result:0);
    }
}
