<?php

class Subscription extends AppModel {

    var $name = 'Subscription';
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'city_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'email' => array(
                'rule2' => array(
                    'rule' => 'email',
                    'allowEmpty' => false,
                    'message' => __l('Please enter valid email address')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'utm_source' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_medium' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_term' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_content' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_campaign' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            )
        );
        $this->moreActions = array(
            ConstMoreAction::Delete => __l('Delete'),
            ConstMoreAction::UnSubscripe => __l('Unsubscribe')
        );
    }

    function findByEmailAndCityId($email, $cityId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Subscription.email' => $email,
                        'Subscription.city_id' => $cityId
                    ),
                    'recursive' => - 1
        ));
    }

    function findTotalAvailableBalanceAmountByEmail($email) {
        return $this->find('first', array(
                    'fields' => array(
                        'MAX(Subscription.available_balance_amount) as total_amount'
                    ),
                    'conditions' => array(
                        'Subscription.email' => $email
                    ),
                    'recursive' => - 1
        ));
    }

    function findCitiesSubscribedByUserId($user_id) {
        if (empty($user_id)) {
            return false;
        }
        $conditions = array();
        $conditions['Subscription.user_id'] = $user_id;
        $conditions['Subscription.is_subscribed'] = 1;
        $subscribed_cities = $this->find('all', array(
            'conditions' => array(
                $conditions
            ),
            'contain' => array(
                'City'
            )
        ));
        return $subscribed_cities;
    }

    function findEmailInFakePatterns($email) {
        return $this->find('first', array(
                    'contain' => array(
                        'FakePatterns' => array(
                            'fields' => array(
                                'pattern'
                            )
                        )
                    ),
                    'conditions' => array(
                        'FakePatterns.pattern' => $email
                    ),
                    'recursive' => - 1
        ));
    }

    function findSubscriptionIdByUserIdAndUserEmailAndCityId($user_id, $user_email, $cityId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Subscription.user_id' => $user_id,
                        'Subscription.email' => $user_email,
                        'Subscription.city_id' => $cityId
                    ),
                    'recursive' => - 1
        ));
    }

    function generateNewSubscriptionToCity($subscription_data) {
        $this->create();
        $this->save($subscription_data);
    }

    function subscribeToCity($subscription_id) {
        $this->updateSuscriptionStatus($subscription_id, 1);
    }

    function unsubscribeToCity($subscription_id) {
        $this->updateSuscriptionStatus($subscription_id, 0);
    }

    function updateSuscriptionStatus($subscription_id, $status) {
        $this->updateAll(array(
            "Subscription.is_subscribed" => $status,
            "Subscription.modified" => '\'' . date("Y-m-d H:i:s") . '\''
                ), array(
            "Subscription.id" => $subscription_id
        ));
    }

    function isSuscriptionActive($suscription) {
        return $suscription['Subscription']['is_subscribed'];
    }

    function suscribeEmail($email, $city, $user_id = null, $dir_ip = null) {
        $row['Subscription']['email'] = $email;
        $row['Subscription']['city_id'] = $city;
        $row['Subscription']['is_suscribe'] = 1;
        if (!is_null($user_id)) {
            $row['Subscription']['user_id'] = $user_id;
        } else {
            $row['Subscription']['user_id'] = null;
        }
        if (!is_null($dir_ip)) {
            $row['Subscription']['suscription_ip'] = $dir_ip;
        }
        if ($this->save($row) === false) {
            throw new Exception('Hubo un problema al guardar.');
        }
        return true;
    }

    function updateEmail($email, $city, $user_id = null, $dir_ip = null) {
        $fields = array(
            'Subscription.is_subscribed' => 1
        );
        $conditions = array(
            'Subscription.email' => $email,
            'Subscription.city_id' => $city
        );
        return $this->updateAll($fields, $conditions);
    }

    function isFake($email) {
        App::import('Model', 'FakePatterns');
        $this->FakePatterns = & new FakePatterns();
        if (!($cachedFakePatterns = Cache::read('cached_fake_patterns'))) {
            $cachedFakePatterns = $this->FakePatterns->find('all');
            Cache::write('cached_fake_patterns', $cachedFakePatterns);
        }
        $isFake = false;
        $array_cadena = explode("@", $email);
        foreach ($cachedFakePatterns as $key => $value) {
            $pattern = $value['FakePatterns']['pattern'];
            $array_pattern = explode("@", $pattern);
            $coincideUser = $this->matchPattern($array_cadena[0], $array_pattern[0]);
            $coincideHost = $this->matchPattern($array_cadena[1], $array_pattern[1]);
            $isFake = $coincideUser && $coincideHost;
            if ($isFake) {
                break;
            }
        }
        return $isFake;
    }

    function matchPattern($cadena, $fraccion) {
        $array = explode("%", $fraccion);
        $numero_elementos = count($array);
        if ($numero_elementos == 2) {
            if (!empty($array[0])) {
                if (!empty($array[1])) {
                    if ($this->stringBeginsWith($cadena, $array[0]) && $this->stringEndsWith($cadena, $array[1])) {
                        return true;
                    }
                } else {
                    if ($this->stringBeginsWith($cadena, $array[0])) {
                        return true;
                    }
                }
            } else {
                if (!empty($array[1])) {
                    if ($this->stringEndsWith($cadena, $array[1])) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else {
            if ($cadena == $fraccion) {
                return true;
            }
        }
        return false;
    }

    function stringBeginsWith($haystack, $beginning, $caseInsensitivity = false) {
        if ($caseInsensitivity)
            return strncasecmp($haystack, $beginning, strlen($beginning)) == 0;
        else
            return strncmp($haystack, $beginning, strlen($beginning)) == 0;
    }

    function stringEndsWith($haystack, $ending, $caseInsensitivity = false) {
        if ($caseInsensitivity)
            return strcasecmp(substr($haystack, strlen($haystack) - strlen($ending)), $haystack) == 0;
        else
            return strpos($haystack, $ending, strlen($haystack) - strlen($ending)) !== false;
    }

}
