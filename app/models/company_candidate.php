<?php
class CompanyCandidate extends AppModel
{
    /*
	var $name = 'CompanyCandidate';
    var $displayField = 'name';
	*/
	
	var $actsAs = array (
      'Searchable',
	  'SoftDeletable' => array('find' => true),
	 );
	var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'FiscalCity' => array(
            'className' => 'City',
            'foreignKey' => 'fiscal_city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'FiscalState' => array(
            'className' => 'State',
            'foreignKey' => 'fiscal_state_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        )
    );
	
	function asertMinLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) >= $limit;
    }

    function asertMaxLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) <= $limit;
    }

    function asertLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) == $limit;
    }

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array(
                'rule3' => array(
                    'rule' => array('asertMaxLength', 64),
                    'message' => 'El nombre debe tener un máximo de 64 caracteres.'
                ),
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_name' => array(
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_address' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_city_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_state_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_cuit' => array(
                'rule5' => array(
                    'rule' => '_validarcuit',
                    'message' => 'El CUIT no es válido.'
                ),
                'rule4' => array(
                    'rule' => array('comparison', '<=', 39000000000),
                    'message' => 'El CUIT debe ser inferior a 39.000.000.000.'
                ),
                'rule3' => array(
                    'rule' => array('asertLength', 11),
                    'message' => 'El CUIT debe contener 11 números.'
                ),
                'rule2' => array(
                  'rule' => 'numeric',
                  'allowEmpty' => false,
                  'message' => 'Ténes que ingresar sólo números'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_iibb' => array(
               'rule3' => array(
                    'rule' => array('asertLength', 12),
                    'message' => 'El IIBB debe contener 12 números.'
                ),
                'rule2' => array(
                  'rule' => 'numeric',
                  'allowEmpty' => false,
                  'message' => 'Ténes que ingresar sólo números'
                ),
               /* 'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )*/
            ) ,
            'percepcion_iibb_caba' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Seleccione el tipo de percepción'
            ) ,
            'fiscal_cond_iva' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_phone' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Ténes que ingresar sólo números, sin espacios, ni guiones'
            ) ,
            'fiscal_fax' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'Ténes que ingresar sólo números, sin espacios, ni guiones'
            ) ,
            'fiscal_bank_account' => array(
                'rule1' => array (
                                'rule' => 'notempty',
                                'allowEmpty' => false,
                                'message' => 'Requerido',
                                'required' => true),
                'rule2' => array (
                             'rule' => 'numeric',
                             'allowEmpty' => false,
                             'message' => 'Ténes que ingresar sólo números, sin espacios, ni guiones'
                ),
            ),
            'contact_phone' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Ténes que ingresar sólo números, sin espacios, ni guiones'
            ) ,

            'fiscal_bank_cbu' => array(
                'rule1' => array(
					'rule' => 'numeric',
					'allowEmpty' => false,
					'message' => 'Ténes que ingresar sólo números'
				),
				'rule2' => array(
					'rule' => array('asertLength', 22),
                    'message' => 'El CBU debe contener 22 números.'
				)
                // cbu
            ) ,

            'persona' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique el tipo de persona'
            ) ,
            'declaracion_jurada_terceros' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si es una declaración jurada de pago a terceros'
            ) ,
            'cheque_noorden' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si el pago por cheque es o no a la orden'
            ) ,
            'slug' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'email' => array(
                'rule' => 'email',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'url' => array(
                'rule2' => array(
                    'rule' => array(
                        'url'
                    ) ,
                    'message' => 'Ténes que ser una URL válida, comenzando con http://',
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => array(
                        'custom',
                        '/^http[s]*:\/\//'
                    ) ,
                    'message' => 'Ténes que ser una URL válida, comenzando con http://' ,
                    'allowEmpty' => true
                )
            ),
			'fiscal_bank' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
	  'address1' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
	  'zip'  => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
        );
        $this->moreActions = array(
            ConstMoreAction::Active => __l('Activate'),
            ConstMoreAction::Inactive => __l('Deactivate'),
        );
    }
	function findByIds($ids){
    	
    	$conditions = array('CompanyCandidate.id' => $ids);
    	 

        $candidates = $this->find('all', array(

            
            'conditions' => $conditions,
            'recursive' => 0,

        ));

        return $candidates;
    }
	function indexData() {
      return !empty($this->data['CompanyCandidate']['name'])?$this->data['CompanyCandidate']['name']:false;
  	}
	function getIndexData(){
		return $this->field('name');
	}
	function validFiles(){
		$extban = glob(APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR.$this->data['CompanyCandidate']['user_id'].DIRECTORY_SEPARATOR."extban_*");
		$encfac = glob(APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR.$this->data['CompanyCandidate']['user_id'].DIRECTORY_SEPARATOR."encfac_*");
		return (!empty($extban) && !empty($encfac));
	}
	
	function _validarcuit($S) {
    /*  determina si el dígito verificador es correcto
        Retorna true si es correcto y false si es incorrecto */
        $S = $this->data['CompanyCandidate']['fiscal_cuit'];
        $v2 = (substr($S,0,1) * 5 +
            substr($S,1,1)  * 4 +
            substr($S,2,1)  * 3 +
            substr($S,3,1)  * 2 +
            substr($S,4,1)  * 7 +
            substr($S,5,1)  * 6 +
            substr($S,6,1)  * 5 +
            substr($S,7,1)  * 4 +
            substr($S,8,1)  * 3 +
            substr($S,9,1) * 2) % 11;
        $v3 = 11 - $v2;
        switch ($v3) {
        case 11 : $v3 = 0; break;
        Case 10 : $v3 = 9; break;
        }     
        return substr($S,10,1) == $v3;
    }

  var $can_approve         = array(ConstCandidateStatuses::Pending);
  var $can_pass_to_draft   = array(ConstCandidateStatuses::Pending);
  var $can_pass_to_pending = array(ConstCandidateStatuses::Reject);
  var $can_rejected        = array(ConstCandidateStatuses::Pending);
  var $can_edited          = array(ConstCandidateStatuses::Pending);

  public function canEditedIfState($state=null) {
    return $this->_canDo($this->can_edited, $state);
  }
  public function canRejectedIfState($state=null) {
    return $this->_canDo($this->can_rejected, $state);
  }
  public function canApproveIfState($state=null) {
    return $this->_canDo($this->can_approve, $state);
  }
  public function canPassToDraftIfState($state=null) {
    return $this->_canDo($this->can_pass_to_draft, $state);
  }
  public function canPassToPendingIfState($state=null) {
    return $this->_canDo($this->can_pass_to_pending, $state);
  }

  public function canEdited($candidate) {
    return $this->_canDo($this->can_edited, $candidate['CompanyCandidate']['state']);
  }
  public function canApprove($candidate) {
    return $this->_canDo($this->can_approve, $candidate['CompanyCandidate']['state']);
  }
  public function canRejected($candidate) {
    return $this->_canDo($this->can_rejected, $candidate['CompanyCandidate']['state']);
  }
  public function canPassToDraft($candidate) {
    return $this->_canDo($this->can_pass_to_draft, $candidate['CompanyCandidate']['state']);
  }
  public function canPassToPending($candidate) {
    return $this->_canDo($this->can_pass_to_pending, $candidate['CompanyCandidate']['state']);
  }


  function _canDo($allowed_states, $state=null) {
    if(is_null($allowed_states)){
      throw new Exception('Actions should not be empty');
    }
    if(is_null($state)){
      return false;
    }
    return in_array($state, $allowed_states);
  }


  function findByUserId($userId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'CompanyCandidate.user_id' => $userId
                    ),
                    'recursive' => -1
                ));
    }

  function findByUser($data) {
    return $this->findByUserId($data['User']['id']);
  }

}

