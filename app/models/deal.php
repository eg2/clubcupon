<?php

App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));
App::import('Component', 'GiftPinService');
App::import('Component', 'product.ProductInventoryService');

class Deal extends AppModel {

    const CAMPAIGN_CODE_TYPE_GENERATED = 'G';
    const CAMPAIGN_CODE_TYPE_EXTERNAL = 'E';
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';
    const GENDER_UNISEX = 'u';

    var $name = 'Deal';
    var $displayField = 'name';
    var $actsAs = array(
        'Excludable',
        'SmsExcludable' => array(
            'field' => 'publication_channel_type_id',
            'excludable_value' => '2',
            'find' => true
        ),
        'Searchable',
        'Sluggable' => array(
            'label' => array(
                'name'
            ),
            'overwrite' => false
        ),
        'WhoDidIt' => array(),
        'Logable' => array(
            'userModel' => 'User',
            'userKey' => 'user_id',
            'change' => 'full',
            'description_ids' => TRUE,
            'noLogUserTypes' => array(
                ConstUserTypes::User,
                ConstUserTypes::Company,
                ConstUserTypes::Agency,
                ConstUserTypes::Partner
            )
        )
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealStatus' => array(
            'className' => 'DealStatus',
            'foreignKey' => 'deal_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Cluster' => array(
            'className' => 'Cluster',
            'foreignKey' => 'cluster_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealFlatCategory' => array(
            'className' => 'api.ApiDealFlatCategory',
            'foreignKey' => 'deal_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealCategory' => array(
            'className' => 'DealCategory',
            'foreignKey' => 'deal_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'ProductProduct' => array(
            'className' => 'ProductProduct',
            'foreignKey' => 'product_product_id',
            'conditions' => '',
            'dependent' => true
        )
    );
    var $hasOne = array(
        'Attachment' => array(
            'className' => 'Attachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'Attachment.class' => array(
                    'Deal',
                    'NowDeal'
                )
            ),
            'dependent' => true
        )
    );
    var $hasMany = array(
        'PaymentOptionDeal' => array(
            'className' => 'PaymentOptionDeal',
            'foreignKey' => 'deal_id'
        ),
        'DealUser' => array(
            'className' => 'DealUser',
            'foreignKey' => 'deal_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Pin' => array(
            'className' => 'Pin',
            'foreignKey' => 'deal_id',
            'dependent' => true
        ),
        'AttachmentMultiple' => array(
            'className' => 'Attachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'AttachmentMultiple.class =' => 'DealMultiple'
            ),
            'dependent' => true
        ),
        'AnulledCoupon' => array(
            'className' => 'AnulledCoupon',
            'foreignKey' => 'deal_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'DealExternal' => array(
            'className' => 'DealExternal',
            'foreignKey' => 'deal_id',
            'dependent' => true
        )
    );

    function indexData() {
        $indexData = $this->data['Deal']['name'];
        $fields = array(
            'Deal.id',
            'Deal.name',
            'Company.name',
            'City.name',
            'User.username'
        );
        $conditions = array(
            'Deal.id' => $this->id
        );
        $joins = array(
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'inner',
                'conditions' => array(
                    'Company.id = Deal.company_id'
                )
            ),
            array(
                'table' => 'cities',
                'alias' => 'City',
                'type' => 'inner',
                'conditions' => array(
                    'City.id = Deal.city_id'
                )
            ),
            array(
                'table' => 'users',
                'alias' => 'User',
                'type' => 'left',
                'conditions' => array(
                    'User.id = Deal.created_by'
                )
            )
        );
        $data = $this->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'joins' => $joins,
            'recursive' => - 1
        ));
        if (!empty($data)) {
            $indexData = $data['Deal']['name'] . ' ' . $data['Company']['name'] . ' ' . $data['User']['username'] . ' ' . $data['City']['name'];
        }
        return $indexData;
    }

    function beforeSave() {
        $this->data['Deal']['seller_id'] = $this->Company->getSellerId($this->data['Deal']['company_id']);
        if ($this->data['Deal']['is_now']) {
            $this->data['Deal']['pay_by_redeemed'] = 1;
        }
        App::import('Component', 'api.ApiDealService');
        $apiDealService = & new ApiDealServiceComponent();
        $this->data['Deal']['coupon_expiry_date'] = $apiDealService->couponLastExpirationDate($this->data);
        return true;
    }

    public function afterSave($created) {
        if (!$created) {
            if (!$this->is_subdeal($this->data)) {
                $subdeals = $this->find('all', array(
                    'conditions' => array(
                        'Deal.parent_deal_id' => $this->data['Deal']['id'],
                        'Deal.id <>' => $this->data['Deal']['id'],
                        'NOT' => array(
                            'Deal.deal_status_id' => array(
                                3,
                                10
                            )
                        )
                    ),
                    'contain' => array(
                        'Attachment'
                    )
                ));
                if (!empty($subdeals)) {
                    $attachment = $this->Attachment->find('first', array(
                        'conditions' => array(
                            'class' => 'Deal',
                            'foreign_id' => $this->data['Deal']['id']
                        )
                    ));
                    $this->Attachment->Behaviors->detach('ImageUpload');
                    $parent = $this->data['Deal'];
                    unset($parent['descriptive_text']);
                    unset($parent['min_limit']);
                    unset($parent['max_limit']);
                    unset($parent['buy_min_quantity_per_user']);
                    unset($parent['buy_max_quantity_per_user']);
                    unset($parent['original_price']);
                    unset($parent['discounted_price']);
                    unset($parent['discount_percentage']);
                    unset($parent['discount_amount']);
                    unset($parent['savings']);
                    unset($parent['minimal_amount_financial']);
                    unset($parent['has_pins']);
                    unset($parent['private_note']);
                    $this->unbindModel(array(
                        'hasOne' => array(
                            'Attachment'
                        )
                    ));
                    $data = $this->data;
                    foreach ($subdeals as $subdeal) {
                        $subdeal['Deal'] = array_merge($parent, array_filter($subdeal['Deal']));
                        $subdeal['Deal']['descriptive_text'] = empty($subdeal['Deal']['descriptive_text']) ? "" : $subdeal['Deal']['descriptive_text'];
                        $subdeal['Deal']['company_id'] = $parent['company_id'];
                        $subdeal['Deal']['name'] = $parent['name'] . ' - ' . $subdeal['Deal']['descriptive_text'];
                        $subdeal['Deal']['city_id'] = $parent['city_id'];
                        $subdeal['Deal']['end_date'] = $parent['end_date'];
                        $subdeal['Deal']['agreed_percentage'] = $parent['agreed_percentage'];
                        $subdeal['Deal']['is_product'] = $parent['is_product'];
                        $subdeal['Deal']['payment_term'] = $parent['payment_term'];
                        $subdeal['Deal']['is_tourism'] = $parent['is_tourism'];
                        $subdeal['Deal']['is_discount_mp'] = $parent['is_discount_mp'];
                        $subdeal['Deal']['deal_trade_agreement_id'] = $parent['deal_trade_agreement_id'];
                        $subdeal['Deal']['is_end_user'] = $parent['is_end_user'];
                        $subdeal['Deal']['start_date'] = $parent['start_date'];
                        $subdeal['Deal']['coupon_start_date'] = $parent['coupon_start_date'];
                        $subdeal['Deal']['coupon_expiry_date'] = $parent['coupon_expiry_date'];
                        $subdeal['Deal']['is_variable_expiration'] = $parent['is_variable_expiration'];
                        $subdeal['Deal']['coupon_duration'] = $parent['coupon_duration'];
                        $subdeal['Deal']['priority'] = $parent['priority'];
                        $subdeal['Deal']['deal_status_id'] = $parent['deal_status_id'];
                        $this->save($subdeal, array(
                            'validate' => false,
                            'callbacks' => false
                        ));
                    }
                    $this->data = $data;
                    $this->bindModel(array(
                        'hasOne' => array(
                            'Attachment'
                        )
                    ));
                }
            }
        }
        if (!isset($this->data['Deal']['paymentOptionValue'])) {
            AppCachedControllerTools::clearCache();
            return parent::afterSave($created);
        }
        $options = $this->data['Deal']['paymentOptionValue'];
        $id = $this->id;
        if (!$created) {
            $id = $this->data['Deal']['id'];
            $this->PaymentOptionDeal->deleteAll(array(
                'PaymentOptionDeal.deal_id' => $id
                    ), false);
        }
        foreach ($options as $idOption => $values) {
            if (isset($values['PaymentOptionId'])) {
                $this->PaymentOptionDeal->create();
                $data = array(
                    'PaymentOptionDeal' => array()
                );
                $data['PaymentOptionDeal']['deal_id'] = $id;
                $data['PaymentOptionDeal']['payment_option_id'] = $idOption;
            }
            if (isset($values['PaymentPlanId']) & isset($values['PaymentOptionId'])) {
                $data['PaymentOptionDeal']['payment_plan_id'] = $values['PaymentPlanId'];
            }
            if (isset($values['PaymentOptionId'])) {
                $this->PaymentOptionDeal->save($data);
            }
            Debugger::log(sprintf("idOption %s, deal %s", $idOption, $id), LOG_DEBUG);
        }
        if (!$created) {
            App::import('Component', 'api.ApiDealService');
            $apiDealService = & new ApiDealServiceComponent();
            Debugger::log(sprintf("Deal::afterSave()::propagatePaymentOptions( %s )", $this->data['Deal']['id']), LOG_DEBUG);
            $apiDealService->propagatePaymentOptions($this->data['Deal']['id']);
        }
        AppCachedControllerTools::clearCache();
        return parent::afterSave($created);
    }

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->GiftPinService = & new GiftPinServiceComponent();
        $this->consultarPagoBacUrl = Configure::read('BAC.wsdl.consultarPago');
        $this->processDealStatusTypes = Configure::read('ProcessDealStatus.types');
        $this->processDealStatusEpsilon = Configure::read('ProcessDealStatus.epsilon');
        $this->runningExpiryDate = date("Y-m-d H:i:s", strtotime('-' . Configure::read('Payments.running_duration'), strtotime(date("Y-m-d H:i:s"))));
        $this->pendingExpiryDate = date("Y-m-d H:i:s", strtotime('-' . Configure::read('Payments.pending_duration'), strtotime(date("Y-m-d H:i:s"))));
        $this->validate = array(
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'agreed_percentage' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe ser un valor numerico, entre 0 y 100'
                ),
                'rule2' => array(
                    'rule' => array(
                        'range', -1,
                        101
                    ),
                    'allowEmpty' => false,
                    'message' => 'Debe ser un valor entero, entre 0 y 100'
                ),
                'rule3' => array(
                    'rule' => '_numberIsInteger',
                    'allowEmpty' => false,
                    'message' => 'Debe ser un valor entero'
                )
            ),
            'requisition_number' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe ser un valor numerico'
                ),
                'rule2' => array(
                    'rule' => '_numberIsInteger',
                    'allowEmpty' => false,
                    'message' => 'Debe ser un valor entero'
                ),
                'rule3' => array(
                    'rule' => array(
                        '_validateMinLengthRequisitionNumber'
                    ),
                    'message' => __l('Debe tener un mínimo de 3 dígitos')
                )
            ),
            'name' => array(
                'rule2' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                ),
                'rule1' => array(
                    'rule' => array(
                        '_checkEmailFriendly'
                    ),
                    'message' => __l('Incompatible con las convenciones de mail: incluye TO, BCC, CC, CCO u otros')
                )
            ),
            'sales_forecast' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => 'Ingresar sólo números'
                )
            ),
            'paymentOptionValue' => array(
                'rule1' => array(
                    'rule' => array(
                        '_checkPaymentOptions'
                    ),
                    'message' => __l('Debe elegir al menos un medio de pago para la oferta')
                )
            ),
            'product_inventory_strategy_id' => array(
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Debe elegir una estrategia de inventario de producto'
                )
            ),
            'product_campaign_id' => array(
                'rule1' => array(
                    'rule' => '_checkifCampaignIsSelectedBySelectedInventoryStrategy',
                    'message' => 'La estrategia de inventario seleccionada, requiere que selecciones una campañía'
                )
            ),
            'product_product_id' => array(
                'rule1' => array(
                    'rule' => '_checkifProductIsSelectedBySelectedInventoryStrategy',
                    'message' => 'La estrategia de inventario seleccionada, requiere que selecciones un producto'
                )
            ),
            'custom_subject' => array(
                'rule' => array(
                    '_checkEmailFriendly'
                ),
                'message' => 'Incompatible con las convenciones de mail: incluye TO, BCC, CC, CCO u otros'
            ),
            'publication_channel_type_id' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'original_price' => array(
                'rule2' => array(
                    'rule' => array(
                        '_validateOriginalPrice'
                    ),
                    'message' => __l('Should be greater than 0')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_validatePrices'
                    ),
                    'message' => 'Precio original debe ser mayor que el Precio con descuento.'
                )
            ),
            'discounted_price' => array(
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'min_limit' => array(
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_checkMinLimitOnUpdate',
                        'on' => 'update'
                    ),
                    'message' => 'La cantidad mínima no puede ser superior a la ya fijada.'
                )
            ),
            'max_limit' => array(
                'rule3' => array(
                    'rule' => array(
                        '_checkMaxLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum limit should be greater than or equal to minimum limit')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Should be a number')
                )
            ),
            'buy_min_quantity_per_user' => array(
                'rule3' => array(
                    'rule' => array(
                        '_compareDealAndBuyMinLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Minimum buy limit should be less than or equal to deal maximum limit')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                )
            ),
            'buy_max_quantity_per_user' => array(
                'rule4' => array(
                    'rule' => array(
                        '_checkMaxQuantityLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum limit should be greater than or equal to minimum limit')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_compareDealAndBuyMaxLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum buy limit should be less than or equal to deal maximum limit')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Required')
                )
            ),
            'city_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'company_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'custom_payment_term' => array(
                'rule1' => array(
                    'rule' => '_isPaymentPeriodOverriden',
                    'message' => 'Ingresar la cantidad de dias para el plazo de pago a medida'
                ),
                'rule2' => array(
                    'rule' => 'numeric',
                    'message' => 'Ingresar un numero de días',
                    'allowEmpty' => true
                ),
                'positive' => array(
                    'rule' => '_isPaymentTermMoreThanOne',
                    'message' => 'Debe ser un valor positivo mayor que 1'
                ),
                'integer' => array(
                    'rule' => '_isPaymentTermInteger',
                    'message' => 'Debe ser un valor entero'
                )
            ),
            'deal_status_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'quantity' => array(
                'rule5' => array(
                    'rule' => array(
                        '_isEligibleMaximumQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Quantity is more than the maximum quantity.')
                ),
                'rule4' => array(
                    'rule' => array(
                        '_isEligibleQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('You can\'t buy this quantity.')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_isEligibleMinimumQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Quantity is less than the minimum quantity.')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                )
            ),
            'gift_email' => array(
                'rule2' => array(
                    'rule' => 'email',
                    'allowEmpty' => false,
                    'message' => __l('Must be a valid email')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'confirm_gift_email' => array(
                'rule1' => array(
                    'rule' => array(
                        'email',
                        'confirm_email'
                    ),
                    'message' => __l('El email y el email de confirmación deben coincidir, por favor intentá nuevamente')
                )
            ),
            'gift_to' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'gift_dni' => array(
                'rule4' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Required')
                )
            ),
            'deal_category_id' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe seleccionar una categoría'
                )
            ),
            'coupon_condition' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'start_date' => array(
                'rule2' => array(
                    'rule' => '_isValidStartDate',
                    'message' => __l('Start date should be greater than today'),
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'end_date' => array(
                'rule2' => array(
                    'rule' => '_isValidEndDate',
                    'message' => __l('End date should be greater than start date'),
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'coupon_start_date' => array(
                'rule1' => array(
                    'rule' => '_isValidCouponStartDate',
                    'message' => 'La fecha de inicio de Canje debe ser mayor o igual a la fecha de publicación de la Oferta',
                    'allowEmpty' => false
                ),
                'rule3' => array(
                    'rule' => '_isNotTodayCouponStartDate',
                    'message' => 'La fecha de inicio de canje del cupón debe ser mayor a la fecha de hoy',
                    'allowEmpty' => false
                ),
                'rule2' => array(
                    'rule' => '_isCouponStartDateNotNull',
                    'message' => 'Requerido'
                )
            ),
            'coupon_expiry_date' => array(
                'rule1' => array(
                    'rule' => '_isValidCouponExpiryDate',
                    'message' => 'La fecha de vencimiento del cupón debe ser mayor que la del inicio de canje',
                    'allowEmpty' => false
                ),
                'rule4' => array(
                    'rule' => '_isValidCouponExpiryDateByEndDate',
                    'message' => 'La fecha de vencimiento del cupón debe ser mayor que la del fin de la Oferta',
                    'allowEmpty' => false
                ),
                'rule3' => array(
                    'rule' => '_isNotTodayCouponExpiryDate',
                    'message' => 'La fecha de vencimiento del cupón debe ser mayor a la fecha de hoy',
                    'allowEmpty' => false
                ),
                'rule2' => array(
                    'rule' => '_isCouponExpiryDateNotNull',
                    'message' => 'Requerido'
                )
            ),
            'coupon_duration' => array(
                'rule2' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => 'Ingresar por cuantos días es válido el cupón desde su compra'
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Should be a number')
                )
            ),
            'discount_amount' => array(
                'rule3' => array(
                    'rule' => array(
                        '_checkTourismTaxAmount',
                        'discount_amount'
                    ),
                    'message' => __l('El monto discriminado para mayoristas de turismo no puede ser igual a cero.')
                ),
                'rule2' => array(
                    'rule' => array(
                        '_checkDiscountAmount',
                        'original_price',
                        'discount_amount'
                    ),
                    'message' => __l('Discount amouont should be less than original amount.')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'commission_percentage' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Should be a number')
                )
            ),
            'downpayment_percentage' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe tener un valor numerico'
                )
            ),
            'ticketportal_deal_pin' => array(
                'rule' => array(
                    '_validateTicketPortalDealPin'
                ),
                'message' => __l('Debe tener como valor el pin de promocion')
            ),
            'ticketportal_deal_url' => array(
                'rule' => array(
                    '_validateTicketPortalDealUrl'
                ),
                'message' => __l('Debe tener como valor una url valida incluyendo http://')
            ),
            'custom_company_name' => array(
                'rule1' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'custom_company_address1' => array(
                'rule1' => array(
                    'rule' => array(
                        '_checkCustomCompanyAddress1'
                    ),
                    'required' => false,
                    'allowEmpty' => false,
                    'message' => 'Campo Requerido'
                )
            ),
            'portal' => array(
                'rule1' => array(
                    'rule' => array(
                        '_checkConceptWholesaler'
                    ),
                    'message' => 'Debe llenar los conceptos por mayorista'
                ),
                'rule2' => array(
                    'rule' => array(
                        '_validConceptWholesaler'
                    ),
                    'message' => 'El precio de descuento debe ser mayor a los conceptos de Mayorista'
                )
            ),
            'is_wholesaler' => array(
                'rule1' => array(
                    'rule' => array(
                        '_validateTourismAndEndUser'
                    ),
                    'message' => 'Las ofertas Mayoristas deben ser ofertas deTurismos y facturadas al usuario Final',
                    'allowEmpty' => true
                )
            ),
            'campaign_code_type' => array(
                'rule' => array(
                    '_checkCampaignCodeTypeExternalIsNumeric'
                ),
                'message' => 'El codigo de campania debe ser numerico.'
            )
        );
        $this->validateCreditCard = array(
            'firstName' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'lastName' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'creditCardNumber' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Should be numeric')
            ),
            'cvv2Number' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Should be numeric')
            ),
            'zip' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'address' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'city' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'state' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'country' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            )
        );
        $this->filters = array(
            ConstDealStatus::PendingApproval => __l('Pending Approval'),
            ConstDealStatus::Upcoming => __l('Upcoming'),
            ConstDealStatus::Open => __l('Open'),
            ConstDealStatus::Closed => __l('Closed')
        );
    }

    function _validConceptWholesaler() {
        if ($this->data[$this->name]['portal'] === 'is_tourism' && $this->data[$this->name]['is_wholesaler']) {
            $allAmount = ($this->data[$this->name]['amount_exempt'] + $this->data[$this->name]['amount_full_iva'] + $this->data[$this->name]['amount_half_iva']);
            $price = $this->data[$this->name]['discounted_price'];
            if ($price <= $allAmount) {
                return false;
            }
        }
        return true;
    }

    function _validateTourismAndEndUser() {
        if (!empty($this->data[$this->name]['is_wholesaler'])) {
            if ($this->data[$this->name]['portal'] === 'is_tourism' && $this->data[$this->name]['is_end_user']) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    function _checkConceptWholesaler() {
        if ($this->data[$this->name]['portal'] === 'is_tourism' && $this->data[$this->name]['is_wholesaler']) {
            if (empty($this->data[$this->name]['amount_exempt']) || empty($this->data[$this->name]['amount_full_iva']) || empty($this->data[$this->name]['amount_half_iva']) || empty($this->data[$this->name]['amount_resolution_3450'])) {
                return false;
            }
        }
        return true;
    }

    function _checkCommissionAndBonus() {
        $commission = (int) ($this->data[$this->name]['commission_percentage']);
        $bonus_amnt = (int) ($this->data[$this->name]['bonus_amount']);
        return !(($commission == 0) && ($bonus_amnt == 0));
    }

    function _checkEmailFriendly($check) {
        $this->Email = & new EmailComponent();
        foreach ($check as $value)
            if ($this->Email->__strip($value) != $value)
                return false;
        return true;
    }

    function _checkPaymentOptions($check) {
        return is_array($check['paymentOptionValue']) && count($check['paymentOptionValue']) > 0;
    }

    function _checkMinLimitOnUpdate($check) {
        $tmp = $this->find('first', array(
            'conditions' => array(
                'id' => $this->id,
                'deal_status_id' => array(
                    ConstDealStatus::Open,
                    ConstDealStatus::Tipped
                )
            ),
            'fields' => array(
                'min_limit'
            ),
            'recursive' => - 1
        ));
        if (!$tmp)
            return true;
        $min_limit = $tmp['Deal']['min_limit'];
        return $min_limit >= $check['min_limit'];
    }

    function _checkTourismTaxAmount() {
        $this->getApiLoggerInstance()->trace('checking tourism tax amount', array(
            'portal' => $this->data[$this->name]['portal'],
            'is_wholesaler' => $this->data[$this->name]['is_wholesaler']
        ));
        if ($this->data[$this->name]['portal'] === 'is_tourism' && $this->data[$this->name]['is_wholesaler']) {
            $discriminatedAmountForTourismWholesalers = $this->data[$this->name]['discount_amount'] - ($this->data[$this->name]['amount_exempt'] + $this->data[$this->name]['amount_full_iva'] + $this->data[$this->name]['amount_half_iva'] + $this->data[$this->name]['amount_resolution_3450']);
            $this->getApiLoggerInstance()->trace('DiscriminatedAmountForTourismWholesalers Arguments', array(
                'discriminatedAmountForTourismWholesalers' => $discriminatedAmountForTourismWholesalers,
                'discount_amount' => $this->data[$this->name]['discount_amount'],
                'amount_exempt' => $this->data[$this->name]['amount_exempt'],
                'amount_full_iva' => $this->data[$this->name]['amount_full_iva'],
                'amount_half_iva' => $this->data[$this->name]['amount_half_iva'],
                'amount_resolution_3450' => $this->data[$this->name]['amount_resolution_3450']
            ));
            if ($discriminatedAmountForTourismWholesalers == 0) {
                return false;
            }
        }
        return true;
    }

    function _checkDiscountAmount() {
        if ($this->data[$this->name]['publication_channel_type_id'] == Configure::read('publication_channel.web') && $this->data[$this->name]['discount_amount'] > $this->data[$this->name]['original_price']) {
            return false;
        }
        return true;
    }

    function _isValidExpiryDate() {
        if (strtotime($this->data[$this->name]['coupon_expiry_date']) > strtotime($this->data[$this->name]['end_date'])) {
            return true;
        }
        return false;
    }

    function _isValidStartDate() {
        $result = false;
        if ($this->data[$this->name]['parent_deal_id'] != $this->data[$this->name]['id']) {
            $result = true;
        } else {
            if ((strtotime($this->data[$this->name]['start_date']) > strtotime(date('Y-m-d H:i:s')))) {
                $result = true;
            }
        }
        return $result;
    }

    function _isValidEndDate() {
        if (!empty($this->data[$this->name]['end_date']) && !empty($this->data[$this->name]['start_date']) && strtotime($this->data[$this->name]['end_date']) > strtotime($this->data[$this->name]['start_date'])) {
            return true;
        }
        return false;
    }

    function _isValidCouponStartDate() {
        $result = false;
        if ((strtotime($this->data[$this->name]['coupon_start_date']) >= strtotime($this->data[$this->name]['start_date']))) {
            $result = true;
        }
        return $result;
    }

    function _isCouponStartDateNotNull() {
        return !is_null($this->data[$this->name]['coupon_start_date']);
    }

    function _isNotTodayCouponStartDate() {
        $result = false;
        if ((strtotime($this->data[$this->name]['coupon_start_date']) > strtotime(date('Y-m-d H:i:s')))) {
            $result = true;
        }
        return $result;
    }

    function _isValidCouponExpiryDate() {
        if (!empty($this->data[$this->name]['coupon_expiry_date']) && !empty($this->data[$this->name]['coupon_start_date']) && strtotime($this->data[$this->name]['coupon_expiry_date']) > strtotime($this->data[$this->name]['coupon_start_date'])) {
            return true;
        }
        return false;
    }

    function _isValidCouponExpiryDateByEndDate() {
        if (!empty($this->data[$this->name]['coupon_expiry_date']) && !empty($this->data[$this->name]['end_date']) && strtotime($this->data[$this->name]['coupon_expiry_date']) > strtotime($this->data[$this->name]['end_date'])) {
            return true;
        }
        return false;
    }

    function _isNotTodayCouponExpiryDate() {
        $result = false;
        if ((strtotime($this->data[$this->name]['coupon_expiry_date']) > strtotime(date('Y-m-d H:i:s')))) {
            $result = true;
        }
        return $result;
    }

    function _isCouponExpiryDateNotNull() {
        return !is_null($this->data[$this->name]['coupon_expiry_date']);
    }

    function _numberIsInteger() {
        return floor($this->data[$this->name]['agreed_percentage']) == $this->data[$this->name]['agreed_percentage'];
    }

    function _isPaymentPeriodOverriden() {
        if ($this->data[$this->name]['is_payment_term_exception']) {
            return !empty($this->data[$this->name]['custom_payment_term']);
        }
        return true;
    }

    function _isPaymentTermMoreThanOne() {
        return $this->data[$this->name]['payment_term'] >= 1;
    }

    function _isPaymentTermInteger() {
        return floor($this->data[$this->name]['payment_term']) == $this->data[$this->name]['payment_term'];
    }

    function isEligibleForBuy($deal_id, $user_id, $buy_max_quantity_per_user) {
        $deals_count = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.deal_id' => $deal_id,
                'DealUser.user_id' => $user_id
            ),
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        if (empty($buy_max_quantity_per_user) || empty($deals_count) || $deals_count[0]['total_count'] < $buy_max_quantity_per_user) {
            return true;
        }
        return false;
    }

    function _isEligibleMaximumQuantity() {
        $deals_count = $this->_countUserBoughtDeals();
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.buy_max_quantity_per_user'
            ),
            'recursive' => - 1
        ));
        $newTotal = (!empty($deals_count[0]['total_count']) ? $deals_count[0]['total_count'] : 0) + $this->data[$this->name]['quantity'];
        if (empty($deal['Deal']['buy_max_quantity_per_user']) || $newTotal <= $deal['Deal']['buy_max_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _isEligibleMinimumQuantity() {
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.buy_min_quantity_per_user',
                'Deal.deal_user_count',
                'Deal.max_limit'
            ),
            'recursive' => - 1
        ));
        if ($deal['Deal']['buy_min_quantity_per_user'] > 1) {
            $deals_count = $this->_countUserBoughtDeals();
            $boughtTotal = (!empty($deals_count[0]['total_count']) ? $deals_count[0]['total_count'] : 0) + $this->data[$this->name]['quantity'];
            if ($boughtTotal >= $deal['Deal']['buy_min_quantity_per_user'] || (!empty($deal['Deal']['max_limit']) && ($deal['Deal']['max_limit'] - $deal['Deal']['deal_user_count']) < $deal['Deal']['buy_min_quantity_per_user'])) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    function _countUserBoughtDeals() {
        $deals_count = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.deal_id' => $this->data[$this->name]['deal_id'],
                'DealUser.user_id' => $this->data[$this->name]['user_id']
            ),
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        return $deals_count;
    }

    function _isEligibleQuantity() {
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.deal_user_count',
                'Deal.max_limit'
            ),
            'recursive' => - 1
        ));
        $inventoryService = ProductInventoryServiceComponent::instance();
        $dealAvailableQuantity = $inventoryService->availableQuantity($this->data[$this->name]['deal_id']);
        return $this->data[$this->name]['quantity'] <= $dealAvailableQuantity;
    }

    function _compareDealAndBuyMinLimt() {
        if (empty($this->data[$this->name]['max_limit']) || $this->data[$this->name]['max_limit'] >= $this->data[$this->name]['buy_min_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _compareDealAndBuyMaxLimt() {
        if (empty($this->data[$this->name]['max_limit']) || $this->data[$this->name]['max_limit'] >= $this->data[$this->name]['buy_max_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _checkMaxLimt() {
        if ($this->data[$this->name]['max_limit'] >= $this->data[$this->name]['min_limit']) {
            return true;
        }
        return false;
    }

    function _checkMaxQuantityLimt() {
        if ($this->data[$this->name]['buy_max_quantity_per_user'] >= $this->data[$this->name]['buy_min_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function processDealStatus($deal_id, $last_inserted_id) {
        App::import('Core', 'Router');
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $deal_id
            ),
            'recursive' => - 1
        ));
        if (!empty($deal)) {
            $this->processTippedStatus($deal, $last_inserted_id);
        } else {
            Debugger::log(sprintf('processDealStatus - No se encontro Deal asociada al cupon. Error Grave. DealId: %d con %DealUser %d', $deal_id, $last_inserted_id), LOG_DEBUG);
        }
    }

    function _formatLikeDNI($dni) {
        $result = '';
        do
            $result = (substr($dni, -3)) . '.' . $result; while (($dni = substr($dni, 0, -3)) != '');
        return substr($result, 0, -1);
    }

    function processTippedStatus($deal_details, $last_inserted_id) {
        $dealUserConditions = array();
        $dealUserConditions['DealUser.id'] = $last_inserted_id;
        Debugger::log(sprintf('processTippedStatus - Se va a intentar enviar un cupon para Deal Details: %s, Deal Id %d ', debugEncode($deal_details), $last_inserted_id), LOG_DEBUG);
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $deal_details['Deal']['id']
            ),
            'contain' => array(
                'DealUser' => array(
                    'User' => array(
                        'UserProfile'
                    ),
                    'DealExternal',
                    'conditions' => $dealUserConditions
                ),
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                ),
                'Attachment',
                'City',
                'ProductProduct',
                'DealCategory'
            ),
            'recursive' => 3
        ));
        if (!$this->_sendCouponMail($deal)) {
            Debugger::log(sprintf('processTippedStatus - No se pudo enviar el cupon para DealUser Id %d, no se va a marcar el email enviado. ', $last_inserted_id), LOG_DEBUG);
            return false;
        }
        if (!$this->DealUser->updateAll(array(
                    'DealUser.emailed' => 1
                        ), array(
                    'DealUser.id' => $last_inserted_id
                ))) {
            Debugger::log(sprintf('processTippedStatus - No se pudo actualizar el Cupon como Emailed =1 para DealUser Id %d ', $last_inserted_id), LOG_DEBUG);
            return false;
        }
        Debugger::log(sprintf('processTippedStatus - DealUser Id %d fue enviado el cupon y se marco como enviado.', $last_inserted_id), LOG_DEBUG);
        return true;
    }

    function sendBuyersListCompany() {
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        App::import('Component', 'Email');
        $this->Email = & new EmailComponent();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $deal_ids = $this->find('list', array(
            'conditions' => array(
                'deal_status_id' => array(
                    ConstDealStatus::Closed,
                    ConstDealStatus::Tipped
                )
            ),
            'fields' => array(
                'id'
            ),
            'recursive' => - 1
        ));
        $deal_ids = is_null($deal_ids) ? array() : array_values($deal_ids);
        $deal_ids_users = $this->DealUser->find('list', array(
            'conditions' => array(
                'deal_id' => $deal_ids,
                'emailed' => 1,
                'email_to_company' => 0
            ),
            'fields' => array(
                'deal_id'
            ),
            'recursive' => - 1
        ));
        $deal_ids_users = is_null($deal_ids_users) ? array() : array_values($deal_ids_users);
        $deal_ids_anulled = $this->AnulledCoupon->find('list', array(
            'conditions' => array(
                'deal_id' => $deal_ids,
                'anulled_email_sent' => 0
            ),
            'fields' => array(
                'deal_id'
            ),
            'recursive' => - 1
        ));
        $deal_ids_anulled = is_null($deal_ids_anulled) ? array() : array_values($deal_ids_anulled);
        $deal_ids = array_values(array_unique(array_merge($deal_ids_users, $deal_ids_anulled)));
        $deals = $this->find('all', array(
            'conditions' => array(
                'Deal.id' => $deal_ids
            ),
            'contain' => array(
                'DealUser' => array(
                    'conditions' => array(
                        'DealUser.emailed' => 1
                    ),
                    'User' => array(
                        'UserProfile' => array(
                            'fields' => array(
                                'UserProfile.dni'
                            )
                        ),
                        'fields' => array(
                            'User.username',
                            'User.email'
                        )
                    )
                ),
                'AnulledCoupon' => array(
                    'User' => array(
                        'UserProfile' => array(
                            'fields' => array(
                                'UserProfile.dni'
                            )
                        ),
                        'fields' => array(
                            'User.username',
                            'User.email'
                        )
                    )
                ),
                'Company' => array(
                    'User' => array(
                        'fields' => array(
                            'User.username',
                            'User.email'
                        )
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.id',
                            'City.name',
                            'City.slug'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.id',
                            'State.name'
                        )
                    ),
                    'Country' => array(
                        'fields' => array(
                            'Country.id',
                            'Country.name',
                            'Country.slug'
                        )
                    )
                )
            )
        ));
        foreach ($deals as $deal) {
            $deal_users = $deal['DealUser'];
            $deal_users_ids = array();
            $deal_users_list = '';
            $companyUser = $this->Company->User->find('first', array(
                'conditions' => array(
                    'User.id' => $deal['Company']['user_id']
                ),
                'fields' => array(
                    'User.username',
                    'User.id',
                    'User.email'
                ),
                'recursive' => - 1
            ));
            if (!empty($deal_users)) {
                $deal_users_list.= '<table width = "100%"  cellpadding = "5"  cellspacing = "1"  bgcolor = "#CCCCCC"  border = "0"  style = "color: #666; font-size: 11px;">';
                $deal_users_list.= '  <tr>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . 'Usuario' . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . 'Nombre amigo' . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . 'DNI' . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . 'DNI amigo' . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . 'Codigo de cupon' . '</th>' . '<th bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . __l('Quantity') . '</th>' . '  </tr>';
                foreach ($deal_users as $deal_user) {
                    $deal_users_ids[] = $deal_user["id"];
                    $deal_users_list.= '  <tr>' . '<td bgcolor = "#FFFFFF"  align = "left">' . $deal_user['User']['username'] . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . (empty($deal_user['gift_to']) ? '-' : $deal_user['gift_to']) . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . (isset($deal_user['User']['UserProfile']['dni']) ? $deal_user['User']['UserProfile']['dni'] : '') . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . (empty($deal_user['gift_dni']) ? '-' : $deal_user['gift_dni']) . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . $deal_user['coupon_code'] . '</td>' . '<td bgcolor = "#FFFFFF"  align = "center">' . $deal_user['quantity'] . '</td>' . '  </tr>';
                }
                $deal_users_list.= '</table>';
            }
            $anulled_coupons = $deal['AnulledCoupon'];
            $anulled_coupons_ids = array();
            $anulled_coupons_list = '';
            if (!empty($anulled_coupons)) {
                $anulled_coupons_list.= '<table width = "100%"  cellpadding = "5"  cellspacing = "1"  bgcolor = "#CCCCCC"  border = "0"  style = "color: #666; font-size: 11px;">';
                $anulled_coupons_list.= '  <tr>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . __l('Username') . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . __l('DNI') . '</th>' . '<th align = "left"  bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . __l('Coupon code') . '</th>' . '<th bgcolor = "#fbdddf"  style = "color: #981860; font-size: 14px;">' . __l('Quantity') . '</th>' . '  </tr>';
                foreach ($anulled_coupons as $anulled_coupon) {
                    $anulled_coupons_ids[] = $anulled_coupon["id"];
                    $anulled_coupons_list.= '  <tr>' . '<td bgcolor = "#FFFFFF"  align = "left">' . $anulled_coupon['User']['username'] . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . (isset($anulled_coupon['User']['UserProfile']['dni']) ? $anulled_coupon['User']['UserProfile']['dni'] : '') . '</td>' . '<td bgcolor = "#FFFFFF"  align = "left">' . $anulled_coupon['coupon_code'] . '</td>' . '<td bgcolor = "#FFFFFF"  align = "center">' . $anulled_coupon['quantity'] . '</td>' . '  </tr>';
                }
                $anulled_coupons_list.= '</table>';
            }
            $emailFindReplace = array(
                '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
                '##SITE_NAME##' => Configure::read('site.name'),
                '##SITE_LINK##' => Configure::read('static_domain_for_mails') . '/',
                '##USERNAME##' => $companyUser['User']['username'],
                '##DEAL_NAME##' => $deal['Deal']['name'],
                '##DEAL_URL##' => Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug'],
                    'admin' => false
                        ), true),
                '##COUPON_EXPIRY_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8'),
                '##COUPON_CONDITION##' => !empty($deal['Deal']['coupon_condition']) ? $deal['Deal']['coupon_condition'] : 'N/A',
                '##REDEMPTION_PLACE##' => $deal['Company']['address1'] . '<br />' . $deal['Company']['address2'] . '<br />' . $deal['Company']['City']['name'] . '<br />' . $deal['Company']['State']['name'] . '<br />' . $deal['Company']['Country']['name'] . '<br />' . $deal['Company']['zip'],
                '##TABLE##' => ($deal_users_list == '' ? '<em>No hay cupones v&aacute;lidos a la fecha.</em>' : $deal_users_list),
                '##ANULLED##' => ($anulled_coupons_list == '' ? '<em>No hay cupones anulados a la fecha.</em>' : $anulled_coupons_list)
            );
            $template = $this->EmailTemplate->selectTemplate('Deal Coupon Buyers List');
            $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
            $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
            $this->Email->to = $companyUser['User']['email'];
            $this->Email->subject = strtr($template['subject'], $emailFindReplace);
            $this->Email->content = strtr($template['email_content'], $emailFindReplace);
            $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
            Debugger::log('sendBuyersListCompany - Se va a enviar la siguiente lista de cupones por correo. Lista de DealUsers ids ' . debugEncode($deal_users_ids) . ', lista de AnulledCoupons ids' . debugEncode($anulled_coupons_ids) . '.', LOG_DEBUG);
            $isEmailToCompanyOk = $this->Email->send($this->Email->content);
            $isEmailToBCCsOk = true;
            foreach (Configure::read('sendBuyersListCompany.bcc') as $bcc) {
                $this->Email->to = $bcc;
                $isEmailToBCCsOk = $this->Email->send($this->Email->content) && $isEmailToBCCsOk;
            }
            if (!($isEmailToCompanyOk && $isEmailToBCCsOk)) {
                Debugger::log('sendBuyersListCompany - No se pudo enviar a la empresa la lista de cupones acreditados el dia anterior. No se marcan los registros como enviados.  Lista de DealUsers ids ' . debugEncode($deal_users_ids) . '.', LOG_DEBUG);
            } else {
                Debugger::log('sendBuyersListCompany - Se actualiza el campo email_to_company en los DealUsers que acabamos de enviar a la empresa.', LOG_DEBUG);
                if (!$this->DealUser->updateAllAsEmailedToCompany($deal_users_ids)) {
                    Debugger::log('sendBuyersListCompany - No se pudo actualizar el campo email_to_company en DealUsers. Lista de DealUsers ids ' . debugEncode($deal_users_ids) . '.', LOG_DEBUG);
                }
                if (!$this->AnulledCoupon->updateAll(array(
                            'AnulledCoupon.anulled_email_sent' => 1
                                ), array(
                            'AnulledCoupon.id' => $anulled_coupons_ids
                        ))) {
                    Debugger::log('sendBuyersListCompany - No se pudo actualizar el campo anulled_email_sent en AnulledCoupon. Lista de AnulledCoupons ids ' . debugEncode($anulled_coupons_ids) . '.', LOG_DEBUG);
                }
            }
        }
    }

    function findDealForSendCupon($dealId, $dealUserId) {
        $dealUserConditions = array();
        $dealUserConditions['DealUser.id'] = $dealUserId;
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $dealId
            ),
            'contain' => array(
                'DealUser' => array(
                    'User' => array(
                        'UserProfile'
                    ),
                    'DealExternal',
                    'conditions' => $dealUserConditions
                ),
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                ),
                'Attachment',
                'City',
                'ProductProduct',
                'DealCategory'
            ),
            'recursive' => 3
        ));
        return $deal;
    }

    function mapViewData($deal, $dealUser) {
        return array(
            'City' => array(
                'is_business_unit' => $deal['City']['is_business_unit'],
                'name' => $deal['City']['name']
            ),
            'Company' => array(
                'address1' => $deal['Company']['address1'],
                'address2' => $deal['Company']['address2'],
                'name' => $deal['Company']['name']
            ),
            'Deal' => array(
                'coupon_condition' => $deal['Deal']['coupon_condition'],
                'coupon_duration' => $deal['Deal']['coupon_duration'],
                'coupon_expiry_date' => $deal['Deal']['coupon_expiry_date'],
                'coupon_start_date' => $deal['Deal']['coupon_start_date'],
                'custom_company_address1' => $deal['Deal']['custom_company_address1'],
                'custom_company_address2' => $deal['Deal']['custom_company_address2'],
                'custom_company_city' => $deal['Deal']['custom_company_city'],
                'custom_company_contact_phone' => $deal['Deal']['custom_company_contact_phone'],
                'custom_company_name' => $deal['Deal']['custom_company_name'],
                'deal_category_id' => $deal['Deal']['deal_category_id'],
                'description' => $deal['Deal']['description'],
                'descriptive_text' => $deal['Deal']['descriptive_text'],
                'discounted_price' => $deal['Deal']['discounted_price'],
                'id' => $deal['Deal']['id'],
                'is_now' => $deal['Deal']['is_now'],
                'is_variable_expiration' => $deal['Deal']['is_variable_expiration'],
                'name' => $deal['Deal']['name'],
                'parent_deal_id' => $deal['Deal']['parent_deal_id'],
                'start_date' => $deal['Deal']['start_date']
            ),
            'DealExternal' => array(
                'shipping_address_id' => $dealUser['DealExternal']['shipping_address_id']
            ),
            'DealUser' => array(
                'coupon_code' => $dealUser['coupon_code'],
                'created' => $dealUser['created'],
                'gift_dni' => $dealUser['gift_dni'],
                'pin_code' => $dealUser['pin_code'],
                'coupon_code' => $dealUser['coupon_code'],
                'created' => $dealUser['created'],
                'gift_dni' => $dealUser['gift_dni'],
                'gift_to' => $dealUser['gift_to'],
                'id' => $dealUser['id'],
                'is_gift' => $dealUser['is_gift'],
                'message' => $dealUser['message']
            ),
            'User' => array(
                'id' => $dealUser['User']['id'],
                'username' => $dealUser['User']['username']
            ),
            'UserProfile' => array(
                'dni' => $dealUser['User']['UserProfile']['dni']
            )
        );
    }

    function _sendCouponMail($deal) {
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        App::import('Component', 'Xemail');
        App::import('Component', 'Pdfcoupon');
        $this->Pdfcoupon = & new PdfcouponComponent();
        $this->Xemail = & new XemailComponent();
        $this->Xemail->delivery = Configure::read('site.email.delivery');
        $this->Xemail->smtpOptions = Configure::read('site.email.smtp_options');
        App::import('Component', 'api.ApiDealService');
        $apiDealService = & new ApiDealServiceComponent();
        $atLeastOneCouponNotSended = false;
        $buyers_name = array();
        $emailFindReplace = array();
        $props = array();
        $doAttach = Configure::read('PDF.doAttach');
        $userIds = array_pluck('user_id', $deal['DealUser']);
        $dniIndexedByUserId = $this->User->UserProfile->find('list', array(
            'conditions' => array(
                'user_id' => $userIds
            ),
            'fields' => array(
                'user_id',
                'dni'
            ),
            'recursive' => 0
        ));
        if (!empty($deal['DealUser'])) {
            foreach ($deal['DealUser'] as $deal_user) {
                echo __METHOD__ . ' ' . __LINE__ . ' Procesando dealUser: ' . $deal_user['id'];
                $oldConditionPins = $deal['Deal']['has_pins'] && empty($deal_user['pin_code']);
                $newConditionPins = isset($deal['ProductProduct']) && $deal['ProductProduct']['has_pins'] && empty($deal_user['pin_code']);
                if ($oldConditionPins || $newConditionPins) {
                    if ($oldConditionPins) {
                        $pin = $this->Pin->next($deal['Deal']['id'], $deal['Deal']['has_pins']);
                    } elseif ($newConditionPins) {
                        $pin = $this->Pin->nextByProduct($deal['ProductProduct']['id'], $deal['ProductProduct']['has_pins']);
                    }
                    if (empty($pin)) {
                        $atLeastOneCouponNotSended = true;
                        continue;
                    }
                    $this->DealUser->updateAll(array(
                        'pin_code' => '\'' . $pin['code'] . '\''
                            ), array(
                        'DealUser.id' => $deal_user['id']
                    ));
                    $deal_user['pin_code'] = $pin['code'];
                }
                $barcode = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'deals',
                            'action' => 'barcode',
                            $deal_user['id'],
                            'admin' => false
                                ), true);
                App::import('Model', 'Redemption');
                $redemption = & new Redemption();
                $data = $redemption->findByDealUserId($deal_user['id']);
                $posnet_code = $redemption->formatPosnetCode($data);
                $descriptive_text = $deal['Deal']['name'];
                if ($deal['Deal']['id'] == $deal['Deal']['parent_deal_id']) {
                    $descriptive_text.= " " . $deal['Deal']['descriptive_text'];
                }
                $expiry_date = $apiDealService->couponExpirationDate($deal, $deal_user);
                $coupon_expiry_date = htmlentities(strftime(($deal['Deal']['is_now'] ? "%d/%m/%Y %H:%M" : "%d/%m/%Y"), strtotime($expiry_date)), ENT_QUOTES, 'UTF-8');
                if (!$deal_user['is_gift']) {
                    $buyers_name[$deal_user['coupon_code']] = $deal_user['User']['username'];
                    $props = array(
                        'creator' => Configure::read('PDF.deal.creator'),
                        'author' => Configure::read('PDF.deal.author'),
                        'title' => Configure::read('PDF.deal.title'),
                        'subject' => Configure::read('PDF.deal.subject'),
                        'keywords' => Configure::read('PDF.deal.keywords')
                    );
                    $emailFindReplace = array(
                        '##DESCRIPTIVE_TEXT##' => (!empty($deal['Deal']['descriptive_text'])) ? ' (' . $deal['Deal']['descriptive_text'] . ')' : '',
                        '##COUPON_PIN##' => empty($deal_user['pin_code']) ? '' : ('<br />PIN de tu cup&oacute;n: <strong>' . $deal_user['pin_code'] . '</strong>'),
                        '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
                        '##DEAL_TITLE##' => $deal['Deal']['name'],
                        '##DEAL_NAME##' => $deal['Deal']['name'],
                        '##SITE_NAME##' => Configure::read('site.name'),
                        '##COUPON_CODE##' => $deal_user['coupon_code'],
                        '##POSNET_CODE##' => $posnet_code,
                        '##USER_NAME##' => $deal_user['User']['username'],
                        '##COMPANY_NAME##' => !empty($deal['Deal']['custom_company_name']) ? $deal['Deal']['custom_company_name'] : $deal['Company']['name'],
                        '##COMPANY_ADDRESS_1##' => !empty($deal['Deal']['custom_company_address1']) ? $deal['Deal']['custom_company_address1'] : $deal['Company']['address1'],
                        '##COMPANY_ADDRESS_2##' => !empty($deal['Deal']['custom_company_address2']) ? $deal['Deal']['custom_company_address2'] : $deal['Company']['address2'],
                        '##COMPANY_PHONE##' => !empty($deal['Deal']['custom_company_contact_phone']) ? $deal['Deal']['custom_company_contact_phone'] : '',
                        '##COMPANY_CITY##' => !empty($deal['Deal']['custom_company_city']) ? $deal['Deal']['custom_company_city'] : '',
                        '##COUPON_EXPIRY_DATE##' => $coupon_expiry_date,
                        '##COUPON_PURCHASED_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal_user['created'])), ENT_QUOTES, 'UTF-8'),
                        '##COUPON_CONDITION##' => ($deal['Deal']['is_now'] ? $deal['Deal']['description'] : $deal['Deal']['coupon_condition']),
                        '##USER_DNI##' => isset($dniIndexedByUserId[$deal_user['User']['id']]) ? $this->_formatLikeDNI($dniIndexedByUserId[$deal_user['User']['id']]) : '',
                        '##GIFT_DNI##' => !empty($deal_user['gift_dni']) ? $this->_formatLikeDNI($deal_user['gift_dni']) : '',
                        '##SITE_LOGO##' => Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'logo-email.png',
                            'admin' => false
                                ), false),
                        '##BARCODE##' => Configure::read('static_domain_for_mails') . '/img/barcode.jpeg',
                        '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
                        '##ABSOLUTE_IMG_PATH_2##' => Configure::read('static_domain_for_mails_2') . '/img/email/',
                        '##IS_NOW##' => $deal['Deal']['is_now']
                    );
                    $template = $this->EmailTemplate->selectTemplate('Deal Coupon Generic');
                    if ($deal['City']['is_business_unit'] == 1) {
                        $this->Xemail->from = 'tucupon@nuestrosbeneficios.com';
                    } else {
                        $this->Xemail->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
                    }
                    $this->Xemail->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
                    $this->Xemail->to = $deal_user['User']['email'];
                    if ($deal['City']['is_business_unit'] == 1) {
                        $site = 'Nuestros Beneficios';
                    } else {
                        $site = 'ClubCupon';
                    }
                    if (!empty($deal['Deal']['descriptive_text'])) {
                        $adDesc = ' (' . $deal['Deal']['descriptive_text'] . ')';
                    } else {
                        $adDesc = '';
                    }
                    $template['subject'] = 'Este es tu cupón de compra de ' . $site . ': ##DEAL_NAME##' . $adDesc . '.';
                    $this->Xemail->subject = strtr($template['subject'], $emailFindReplace);
                    if ($deal['Deal']['is_now']) {
                        $this->Xemail->subject.= ' Ya!';
                    }
                    Debugger::log(sprintf('_sendCouponMail - antes de generateContentCoupon'), LOG_DEBUG);
                    $dataForCouponContent = $this->mapViewData($deal, $deal_user);
                    $output = Deal::generateCouponContent($dataForCouponContent, $emailFindReplace);
                    Debugger::log(sprintf('_sendCouponMail - despues de generateContentCoupon'), LOG_DEBUG);
                    $this->Xemail->content = $output;
                    $this->Xemail->sendAs = ($template['is_html'] == 1) ? 'html' : 'text';
                    if ($doAttach) {
                        $emailFindReplace['##BARCODE##'] = $emailFindReplace['##ABSOLUTE_IMG_PATH_2##'] . 'coupon_web/barcode.gif';
                        if ($deal['Deal']['is_tourism'] && strtotime($deal['Deal']['start_date']) < Configure::read('tec_dia.start_date')) {
                            $this->Xemail->stringAttachments[0] = $this->Pdfcoupon->deal_tourism($props, $emailFindReplace, 'cupon.pdf', $this->Pdfcoupon->scaleSpec(200, 287), 'E');
                        } else {
                            $this->Xemail->stringAttachments[0] = $this->Pdfcoupon->deal($props, $emailFindReplace, 'cupon.pdf', $this->Pdfcoupon->scaleSpec(200, 287), 'E');
                        }
                    } else {
                        unset($this->Xemail->stringAttachments[0]);
                    }
                    Debugger::log(sprintf('_sendCouponMail - antes de send'), LOG_DEBUG);
                    if (!$this->Xemail->send($this->Xemail->content)) {
                        $atLeastOneCouponNotSended = true;
                        Debugger::log(sprintf('_sendCouponMail - ERROR: No se pudo enviar un cupon el DealUser es: %s. Log del smtp: %s', debugEncode($deal_user), debugEncode($this->Xemail->smtpError)), LOG_DEBUG);
                    }
                    Debugger::log(sprintf('_sendCouponMail - despues de send'), LOG_DEBUG);
                } else {
                    $buyers_name[$deal_user['coupon_code']] = $deal_user['User']['username'];
                    $props = array(
                        'creator' => Configure::read('PDF.gift.creator'),
                        'author' => Configure::read('PDF.gift.author'),
                        'title' => Configure::read('PDF.gift.title'),
                        'subject' => Configure::read('PDF.gift.subject'),
                        'keywords' => Configure::read('PDF.gift.keywords')
                    );
                    $emailFindReplace = array(
                        '##COUPON_PIN##' => empty($deal_user['pin_code']) ? '' : ('<br />PIN de tu cup&oacute;n: <strong>' . $deal_user['pin_code'] . '</strong>'),
                        '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
                        '##MESSAGE##' => $deal_user['message'],
                        '##DEAL_TITLE##' => $descriptive_text,
                        '##DEAL_NAME##' => $descriptive_text,
                        '##SITE_NAME##' => Configure::read('site.name'),
                        '##DATE_PURCHASE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal_user['created'])), ENT_QUOTES, 'UTF-8'),
                        '##COUPON_CODE##' => $deal_user['coupon_code'],
                        '##POSNET_CODE##' => $posnet_code,
                        '##USER_NAME##' => $deal_user['User']['username'],
                        '##RECIPIENT_USER_NAME##' => $deal_user['gift_to'],
                        '##COMPANY_NAME##' => !empty($deal['Deal']['custom_company_name']) ? $deal['Deal']['custom_company_name'] : $deal['Company']['name'],
                        '##COMPANY_ADDRESS_1##' => !empty($deal['Deal']['custom_company_address1']) ? $deal['Deal']['custom_company_address1'] : $deal['Company']['address1'],
                        '##COMPANY_ADDRESS_2##' => !empty($deal['Deal']['custom_company_address2']) ? $deal['Deal']['custom_company_address2'] : $deal['Company']['address2'],
                        '##COMPANY_PHONE##' => !empty($deal['Deal']['custom_company_contact_phone']) ? $deal['Deal']['custom_company_contact_phone'] : '',
                        '##COMPANY_CITY##' => !empty($deal['Deal']['custom_company_city']) ? $deal['Deal']['custom_company_city'] : '',
                        '##COUPON_EXPIRY_DATE##' => 'Pod&eacute;s usar este cup&oacute;n el ' . htmlentities(strftime(($deal['Deal']['is_now'] ? "%d/%m/%Y" : "%d/%m/%Y"), strtotime(($deal['Deal']['is_now'] ? $deal['Deal']['deal_tipped_time'] : $deal['Deal']['coupon_expiry_date']))), ENT_QUOTES, 'UTF-8'),
                        '##COUPON_PURCHASED_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal_user['created'])), ENT_QUOTES, 'UTF-8'),
                        '##COUPON_CONDITION##' => ($deal['Deal']['is_now'] ? $deal['Deal']['description'] : $deal['Deal']['coupon_condition']),
                        '##USER_DNI##' => $dniIndexedByUserId[$deal_user['User']['id']],
                        '##GIFT_DNI##' => !empty($deal_user['gift_dni']) ? $this->_formatLikeDNI($deal_user['gift_dni']) : '',
                        '##SITE_LOGO##' => Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'logo-email.png',
                            'admin' => false
                                ), false),
                        '##BARCODE##' => $barcode,
                        '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
                        '##ABSOLUTE_IMG_PATH_2##' => Configure::read('static_domain_for_mails_2') . '/img/email/'
                    );
                    $template = $this->EmailTemplate->selectTemplate('Deal Coupon Generic');
                    if ($deal['City']['is_business_unit'] == 1) {
                        $this->Xemail->from = 'tucupon@nuestrosbeneficios.com';
                    } else {
                        $this->Xemail->from = 'tucupon@clubcupon.com.ar';
                    }
                    $this->Xemail->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
                    $this->Xemail->to = $deal_user['gift_email'];
                    $template['subject'] = 'Cupon de regalo de ##USER_NAME##';
                    $this->Xemail->subject = strtr($template['subject'], $emailFindReplace);
                    Debugger::log(sprintf('_sendCouponMail - antes de generateContentCoupon GIFT'), LOG_DEBUG);
                    $dataForCouponContent = $this->mapViewData($deal, $deal_user);
                    $output = Deal::generateCouponContent($dataForCouponContent, $emailFindReplace);
                    Debugger::log(sprintf('_sendCouponMail - despues de generateContentCoupon GIFT'), LOG_DEBUG);
                    $this->Xemail->content = $output;
                    $this->Xemail->sendAs = ($template['is_html'] == 1) ? 'html' : 'text';
                    if ($doAttach) {
                        $emailFindReplace['##BARCODE##'] = $emailFindReplace['##ABSOLUTE_IMG_PATH_2##'] . 'coupon_web/barcode.gif';
                        if ($deal['Deal']['is_tourism'] && strtotime($deal['Deal']['start_date']) < Configure::read('tec_dia.start_date')) {
                            $this->Xemail->stringAttachments[0] = $this->Pdfcoupon->gift_tourism($props, $emailFindReplace, 'cupon.pdf', $this->Pdfcoupon->scaleSpec(200, 287), 'E');
                        } else {
                            $this->Xemail->stringAttachments[0] = $this->Pdfcoupon->gift($props, $emailFindReplace, 'cupon.pdf', $this->Pdfcoupon->scaleSpec(200, 287), 'E');
                        }
                    } else {
                        unset($this->Xemail->stringAttachments[0]);
                    }
                    if (!$this->Xemail->send($this->Xemail->content)) {
                        $atLeastOneCouponNotSended = true;
                        Debugger::log(sprintf('_sendCouponMail - ERROR: No se pudo enviar un cupon el DealUser es: %s. Log del smtp: %s', debugEncode($deal_user), debugEncode($this->Xemail->smtpError)), LOG_DEBUG);
                    }
                }
                $this->DealUser->id = $deal_user['id'];
            }
        }
        return !$atLeastOneCouponNotSended;
    }

    public static function isCategoryNow($deal_category_id) {
        return false;
    }

    public static function generateCouponContent($data, $labels, $fromCompany = false) {
        App::import('Component', 'api.ApiDealService');
        $apiDealService = & new ApiDealServiceComponent();
        if ($data['City']['is_business_unit'] == 1) {
            $labels['##COUPON_URL_LOGO_WEB##'] = 'coupon_web/logo.png';
            $labels['##SUPPORT_HREF##'] = 'mailto:soporte@nuestrosbeneficios.com';
            $labels['##SUPPORT_LABEL##'] = 'soporte@nuestrosbeneficios.com';
            $labels['##SUPPORT_TEXT##'] = 'mandanos un mail a';
            $labels['##PAGE_TITLE##'] = 'Nuestros Beneficios';
        } else {
            $labels['##COUPON_URL_LOGO_WEB##'] = 'coupon_web/002_a_logo.PNG';
            $labels['##SUPPORT_HREF##'] = 'http://soporte.clubcupon.com.ar';
            $labels['##SUPPORT_LABEL##'] = 'http://soporte.clubcupon.com.ar';
            $labels['##SUPPORT_TEXT##'] = 'ingres&aacute; en este link';
            $labels['##PAGE_TITLE##'] = 'Club Cup&oacute;n';
        }
        $labels['##COUPON_URL_IMG_FOOTER##'] = 'coupon_web/operadora.jpg';
        $labels['##VALOR_COUPON_TITLE##'] = 'Valor';
        $labels['##VALOR_COUPON##'] = '$' . sprintf('%d', $data['Deal']['discounted_price']);
        $labels['##COUPON_CONDITION##'] = ($data['Deal']['is_now'] ? $data['Deal']['description'] : $data['Deal']['coupon_condition']);
        if ($data['Deal']['is_now'] || Deal::isCategoryNow($data['Deal']['deal_category_id'])) {
            $labels['##COUPON_HOW_TO_USE##'] = '<strong>1</strong> Llev&aacute; tu Smartphone con este correo o imprimilo<br />';
            $labels['##COUPON_HOW_TO_USE##'].= '<strong>2</strong> Si lo imprim&iacute;s prestale atenci&oacute;n a la calidad del cup&oacute;n<br />';
            $labels['##COUPON_HOW_TO_USE##'].= '<strong>3</strong> Present&aacute; tu Smartphone o cup&oacute;n y tu DNI en el comercio<br />';
            $labels['##COUPON_HOW_TO_USE##'].= '<strong>4</strong> &iexcl;A disfrutar!';
            $labels['##COUPON_URL_LOGO_WEB##'] = 'coupon_web/002_b_logo.png';
            $labels['##COUPON_EXPIRY_DATE##'] = 'Pod&eacute;s usar este cup&oacute;n s&oacute;lo hoy ';
        } else {
            $labels['##COUPON_HOW_TO_USE##'] = '<strong>1</strong> Imprim&iacute; este correo<br />
                                <strong>2</strong> Prestale atenci&oacute;n a la calidad del cup&oacute;n<br />
                                <strong>3</strong> Present&aacute; el cup&oacute;n y tu DNI en el comercio<br />
                                <strong>4</strong> &iexcl;A disfrutar!';
            $labels['##COUPON_EXPIRY_DATE##'] = 'V&aacute;lido desde el ';
            $coupon_start_date = $apiDealService->couponStartDate($data, $data);
            $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_start_date)), ENT_QUOTES, 'UTF-8') . '<br />Hasta el ';
            $coupon_expiry_date = $apiDealService->couponExpirationDate($data, $data);
            $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_expiry_date)), ENT_QUOTES, 'UTF-8');
        }
        $labels['##USER_NAME##'] = $data['User']['username'];
        $labels['##USER_DNI##'] = (isset($data['UserProfile']['dni']) ? $data['UserProfile']['dni'] : '');
        $labels['##COUPON_PURCHASED_DATE##'] = htmlentities(strftime("%d/%m/%Y", strtotime($data['DealUser']['created'])), ENT_QUOTES, 'UTF-8');
        App::import('Model', 'Redemption');
        $redemption = & new Redemption();
        $redemptionData = $redemption->findByDealUserId($data['DealUser']['id']);
        $posnet_code = $redemption->formatPosnetCode($redemptionData);
        $labels['##COUPON_CODE##'] = $data['DealUser']['coupon_code'];
        if (!$fromCompany) {
            $labels['##POSNET_CODE##'] = $posnet_code;
            $labels['##POSNET_CODE_PREFIX##'] = substr($posnet_code, 0, 8);
            $labels['##POSNET_CODE_SUFFIX##'] = substr($posnet_code, 8, 10);
            $labels['##POSNET_CODE_TEXT##'] = 'Cod. Posnet ' . $labels['##POSNET_CODE_PREFIX##'] . '<span style="font-weight:bold">' . $labels['##POSNET_CODE_SUFFIX##'] . '</span>';
        } else {
            $labels['##POSNET_CODE_TEXT##'] = '';
        }
        if ($data['DealUser']['is_gift']) {
            $descriptive_text = $data['Deal']['name'];
            if ($data['Deal']['id'] == $data['Deal']['parent_deal_id']) {
                $descriptive_text.= " " . $data['Deal']['descriptive_text'];
            }
            $labels['##DEAL_TITLE##'] = $descriptive_text;
            $labels['##DEAL_NAME##'] = $descriptive_text;
            if ($data['Deal']['is_now']) {
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($data['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8');
            } else {
                $labels['##COUPON_EXPIRY_DATE##'] = 'V&aacute;lido desde el ';
                $coupon_start_date = $apiDealService->couponStartDate($data, $data);
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_start_date)), ENT_QUOTES, 'UTF-8') . '<br />Hasta el ';
                $coupon_expiry_date = $apiDealService->couponExpirationDate($data, $data);
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_expiry_date)), ENT_QUOTES, 'UTF-8');
            }
            $labels['##VALOR_COUPON_TITLE##'] = '';
            $labels['##VALOR_COUPON##'] = '';
            $labels['##MESSAGE##'] = $data['DealUser']['message'];
            $labels['##COUPON_MESSAGE##'] = '<strong>Hola,</strong>
                      &iexcl;Tu amigo <strong>' . $labels['##USER_NAME##'] . '</strong> te envi&oacute; un regalo sorpresa!<br />';
            $labels['##COUPON_MESSAGE##'].= $labels['##DEAL_NAME##'] . '<br />';
            $labels['##COUPON_MESSAGE##'].= '<p style="font-size:12px; border-bottom:1px solid #CCCCCC; margin: 20px 0 20px; padding: 0 0 20px 0;">' . $labels['##MESSAGE##'] . '</p>';
            $labels['##RECIPIENT_USER_NAME##'] = $data['DealUser']['gift_to'];
            $labels['##GIFT_DNI##'] = !empty($data['DealUser']['gift_dni']) ? $data['DealUser']['gift_dni'] : '';
            $labels['##COUPON_TITULAR##'] = '<strong>' . $labels['##RECIPIENT_USER_NAME##'] . '</strong><br />
                            DNI: <strong>' . $labels['##GIFT_DNI##'] . '</strong><br />
                            DNI (comprador): <strong>' . $labels['##USER_DNI##'] . '</strong>' . $labels['##COUPON_PIN##'];
            $labels['##COUPON_USER##'] = $labels['##RECIPIENT_USER_NAME##'];
            $labels['##COUPON_DNI##'] = $labels['##GIFT_DNI##'];
            $labels['##COUPON_DNI##'].= '<br /><span style="font-weight: lighter">DNI (comprador):</span> ' . $labels['##USER_DNI##'] . $labels['##COUPON_PIN##'];
        } else {
            if ($data['Deal']['is_now']) {
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($data['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8');
            } else {
                $labels['##COUPON_EXPIRY_DATE##'] = 'V&aacute;lido desde el ';
                $coupon_start_date = $apiDealService->couponStartDate($data, $data);
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_start_date)), ENT_QUOTES, 'UTF-8') . '<br />Hasta el ';
                $coupon_expiry_date = $apiDealService->couponExpirationDate($data, $data);
                $labels['##COUPON_EXPIRY_DATE##'].= htmlentities(strftime("%d/%m/%Y", strtotime($coupon_expiry_date)), ENT_QUOTES, 'UTF-8');
            }
            $labels['##COUPON_TITULAR##'] = 'Usuario: <strong>' . $labels['##USER_NAME##'] . '</strong><br />
                            DNI: <strong>' . $labels['##USER_DNI##'] . '</strong>' . $labels['##COUPON_PIN##'];
            $labels['##COUPON_USER##'] = $labels['##USER_NAME##'];
            $labels['##COUPON_DNI##'] = $labels['##USER_DNI##'] . $labels['##COUPON_PIN##'];
            $bar = "stackoverflowpro";
            $labels['##COUPON_MESSAGE##'] = <<<HTML
    
                    &iexcl;Felicitaciones! &iexcl;Compraste la oferta
                    {$labels['##DEAL_TITLE##']}{$labels['##DESCRIPTIVE_TEXT##']}!
    
                    <br />
                    <p style="color:#3e3e3e; font-size:15px; border-bottom:1px solid #CCCCCC; margin: 20px 0 20px; padding: 0 0 20px 0;">
                        El monto de la compra ya fue abonado. La transacci&oacute;n se confirm&oacute; con &eacute;xito.
                    </p>
HTML;
        }
        if (!$data['Deal']['is_now']) {
            $labels['##COUPON_URL_LOGO##'] = 'coupon/002_a_logo.PNG';
            $canjear_en = '';
            if (isset($data['DealExternal']['shipping_address_id'])) {
                App::import('Model', 'shipping.ShippingAddress');
                $shippingAddress = & new ShippingAddress();
                $canjear_en = $shippingAddress->getAddressToShowInCoupon($data['DealExternal']['shipping_address_id']);
            } else {
                $cn = !empty($data['Deal']['custom_company_name']) ? $data['Deal']['custom_company_name'] : $data['Company']['name'];
                if (!empty($cn))
                    $canjear_en.= $cn . '<br />';
                $cp = !empty($data['Deal']['custom_company_contact_phone']) ? $data['Deal']['custom_company_contact_phone'] : '';
                if (!empty($cp) && !$cp == '')
                    $canjear_en.= $cp . '<br />';
                $ca1 = !empty($data['Deal']['custom_company_address1']) ? $data['Deal']['custom_company_address1'] : $data['Company']['address1'];
                if (!empty($ca1))
                    $canjear_en.= $ca1 . '<br />';
                $ca2 = !empty($data['Deal']['custom_company_address2']) ? $data['Deal']['custom_company_address2'] : $data['Company']['address2'];
                if (!empty($ca2))
                    $canjear_en.= $ca2 . '<br />';
                $cc = !empty($data['Deal']['custom_company_city']) ? $data['Deal']['custom_company_city'] : '';
                if (!empty($cc))
                    $canjear_en.= $cc . '<br />';
            }
            $labels['##CANJEAR_EN##'] = $canjear_en;
        }
        else {
            $labels['##COUPON_URL_LOGO##'] = 'coupon/header2.jpg';
            App::import('Model', 'now.NowBranchesDeals');
            App::import('Model', 'now.NowBranch');
            $nowBranchesDeals = new NowBranchesDeals;
            $resultado_NowBranchesDeals = $nowBranchesDeals->findByDealId($data['Deal']['id']);
            if ($resultado_NowBranchesDeals) {
                $hora_min = $resultado_NowBranchesDeals['NowBranchesDeals']['ts_inicio'];
                $hora_max = $resultado_NowBranchesDeals['NowBranchesDeals']['ts_fin'];
                $hora_min = date('H:i', strtotime($hora_min));
                $hora_max = date('H:i', strtotime($hora_max));
                if (date('H', strtotime($hora_max)) < 1) {
                    $hora_max = '24:' . date('i', strtotime($hora_max));
                }
                $labels['##COUPON_EXPIRY_DATE##'].= sprintf(" entre las %s y %s horas.", $hora_min, $hora_max);
            }
            $branch = new NowBranchesDeals;
            $canjear_en = $branch->getBranchAddressForCupons($data['Deal']['id']);
            $labels['##CANJEAR_EN##'] = join("<br/>\r\n", $canjear_en);
        }
        if (Configure::read('passbook.enabled')) {
            $labels['##ABSOLUTE_PASSBOOK_URL##'] = Configure::read('passbook.absolutDomain') . '/passbooks/generate_passbook_file/' . $data['DealUser']['coupon_code'];
            $labels['##ABSOLUTE_PASSBOOK_IMG_URL##'] = Configure::read('passbook.absolutDomain') . 'img/email/coupon/Add_to_Passbook_ES.png';
            $labels['##PASSBOOK_SECTION##'] = '<a href="' . $labels['##ABSOLUTE_PASSBOOK_URL##'] . '" target="_blank" style="color:#333333;">
                            <img src="' . $labels['##ABSOLUTE_PASSBOOK_IMG_URL##'] . '" width="120" height="40" style="display:block;">
                            <br />
                            Para agregar este cup&oacute;n a tu Passbook, abr&iacute; este link desde tu iPhone o iPod touch.
                        </a>';
            $labels['##PASSBOOK_SECTION##'] = <<<HTML
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:arial,helvetica,sans-serif!important;">
                        <tr>
                            <td>
                                <a style="color:#333333; width:22%;" target="_blank" href="{$labels['##ABSOLUTE_PASSBOOK_URL##']}">
                                    <img src="{$labels['##ABSOLUTE_PASSBOOK_IMG_URL##']}" alt="#" width="120" height="40" style="display:block;">
                                </a>
                            </td>
                            <td style="font-size:11px; width:78%;">
                                Para agregar este cup&oacute;n a tu Passbook,
                                <br />
                                abr&iacute;
                                <a href="{$labels['##ABSOLUTE_PASSBOOK_URL##']}" style="color:#00597c; text-decoration:none;">
                                    este link
                                </a>
                                desde tu iPhone o iPod touch.
                            </td>
                        </tr>
                    </table>
HTML;
        }
        if (date("Y-m-d H:i:s") < Configure::read('coupon.global_message_until')) {
            $labels['##COUPON_GLOBAL_MESSAGE##'] = <<<EOD
		        <tr>
                          <td> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #cccccc; border-bottom:1px solid #cccccc; margin-top:20px;" style="border-right:1px solid #cccccc; margin-right:10%;" style="font-family:arial,helvetica,sans-serif!important;">
                              <tr>
                                <td valign="top" style="margin:0; height:110px;">
                                  <h2 style="color:#e96c0e; margin:12px 0;">C&oacute;digo de promoci&oacute;n en Electropuntonet.com</h2>
                                  <p style="font-size:12px;margin:0;">Obten&eacute; $200 de descuento en <a href="http://www.electropuntonet.com/"><span style="font-weight:bold">www.electropuntonet.com</span></a> ingresando el c&oacute;digo <span style="font-weight:bold">CC2016</span> cuando pag&aacute;s. V&aacute;lido hasta el 20/08/2016.</p>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
EOD;
        } else {
            $labels['##COUPON_GLOBAL_MESSAGE##'] = '';
        }
        App::import('Model', 'EmailTemplate');
        $emailTemplate = & new EmailTemplate();
        $email_message = $emailTemplate->selectTemplate('Deal Coupon Generic');
        $output = strtr($email_message['email_content'], $labels);
        return $output;
    }

    function _refundDealAmount($type = '', $dealIds = array()) {
        return false;
        $dealUserConditions = array(
            'DealUser.is_paid' => 1,
            'DealUser.is_repaid' => 0
        );
        if (!empty($dealIds)) {
            $conditions['Deal.id'] = $dealIds;
        } elseif (!empty($type) && $type == 'cron') {
            $conditions['Deal.deal_status_id'] = array(
                ConstDealStatus::Canceled
            );
        }
        $deals = $this->find('all', array(
            'conditions' => $conditions,
            'contain' => array(
                'DealUser' => array(
                    'User' => array(
                        'fields' => array(
                            'User.username',
                            'User.email'
                        )
                    ),
                    'PaypalDocaptureLog' => array(
                        'fields' => array(
                            'PaypalDocaptureLog.authorizationid',
                            'PaypalDocaptureLog.dodirectpayment_amt',
                            'PaypalDocaptureLog.id',
                            'PaypalDocaptureLog.currencycode'
                        )
                    ),
                    'PaypalTransactionLog' => array(
                        'fields' => array(
                            'PaypalTransactionLog.authorization_auth_exp',
                            'PaypalTransactionLog.authorization_auth_id',
                            'PaypalTransactionLog.authorization_auth_amount',
                            'PaypalTransactionLog.authorization_auth_status'
                        )
                    ),
                    'conditions' => $dealUserConditions
                ),
                'Company' => array(
                    'fields' => array(
                        'Company.name',
                        'Company.id',
                        'Company.url',
                        'Company.zip',
                        'Company.address1',
                        'Company.address2',
                        'Company.city_id'
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.id',
                            'City.name',
                            'City.slug'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.id',
                            'State.name'
                        )
                    ),
                    'Country' => array(
                        'fields' => array(
                            'Country.id',
                            'Country.name',
                            'Country.slug'
                        )
                    )
                ),
                'Attachment' => array(
                    'fields' => array(
                        'Attachment.id',
                        'Attachment.dir',
                        'Attachment.filename',
                        'Attachment.width',
                        'Attachment.height'
                    )
                ),
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug'
                    )
                )
            ),
            'recursive' => 2
        ));
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        App::import('Component', 'Email');
        $this->Email = & new EmailComponent();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        if (!empty($deals)) {
            $dealIds = array();
            App::import('Component', 'Paypal');
            $this->Paypal = & new PaypalComponent();
            $payment_gateway = $this->User->Transaction->PaymentGateway->find('first', array(
                'conditions' => array(
                    'PaymentGateway.id' => ConstPaymentGateways::PayPal
                ),
                'fields' => array(
                    'PaymentGateway.is_test',
                    'PaymentGateway.email'
                ),
                'recursive' => - 1
            ));
            $sender_info = array(
                'API_UserName' => Configure::read('paypal.directpay_API_UserName'),
                'API_Password' => Configure::read('paypal.directpay_API_Password'),
                'API_Signature' => Configure::read('paypal.directpay_API_Signature'),
                'is_testmode' => $payment_gateway['PaymentGateway']['is_test']
            );
            foreach ($deals as $deal) {
                if (!empty($deal['DealUser'])) {
                    $dealUserIds = array();
                    foreach ($deal['DealUser'] as $deal_user) {
                        if ((Configure::read('site.is_credit_card_payment_enabled') || Configure::read('site.is_paypal_auth_payment_enabled')) && $deal_user['payment_type_id'] != ConstPaymentTypes::Wallet) {
                            $payment_response = array();
                            if ($deal_user['payment_type_id'] == ConstPaymentTypes::CreditCard) {
                                $post_info['authorization_id'] = $deal_user['PaypalDocaptureLog']['authorizationid'];
                            } else if ($deal_user['payment_type_id'] == ConstPaymentTypes::PayPalAuth) {
                                $post_info['authorization_id'] = $deal_user['PaypalTransactionLog']['authorization_auth_id'];
                            }
                            $post_info['note'] = __l('Deal Payment refund');
                            $payment_response = $this->Paypal->doVoid($post_info, $sender_info);
                            if (!empty($payment_response)) {
                                if ($deal_user['payment_type_id'] == ConstPaymentTypes::CreditCard) {
                                    $data_paypal_docapture_log['PaypalDocaptureLog']['id'] = $deal_user['PaypalDocaptureLog']['id'];
                                    foreach ($payment_response as $key => $value) {
                                        $data_paypal_docapture_log['PaypalDocaptureLog']['dovoid_' . strtolower($key)] = $value;
                                    }
                                    $data_paypal_docapture_log['PaypalDocaptureLog']['dovoid_response'] = serialize($payment_response);
                                    $this->DealUser->PaypalDocaptureLog->save($data_paypal_docapture_log);
                                } else if ($deal_user['payment_type_id'] == ConstPaymentTypes::PayPalAuth) {
                                    $data_paypal_capture_log['PaypalTransactionLog']['id'] = $deal_user['PaypalTransactionLog']['id'];
                                    foreach ($payment_response as $key => $value) {
                                        $data_paypal_capture_log['PaypalTransactionLog']['void_' . strtolower($key)] = $value;
                                    }
                                    $data_paypal_capture_log['PaypalTransactionLog']['void_data'] = serialize($payment_response);
                                    $this->DealUser->PaypalTransactionLog->save($data_paypal_capture_log);
                                }
                            }
                        } else {
                            $transaction['Transaction']['user_id'] = $deal_user['user_id'];
                            $transaction['Transaction']['foreign_id'] = $deal_user['id'];
                            $transaction['Transaction']['class'] = 'DealUser';
                            $transaction['Transaction']['amount'] = $deal_user['discount_amount'];
                            $transaction['Transaction']['transaction_type_id'] = (!empty($deal_user['is_gift'])) ? ConstTransactionTypes::DealGiftRefund : ConstTransactionTypes::DealBoughtRefund;
                            $this->DealUser->User->Transaction->log($transaction);
                            $this->DealUser->User->updateAll(array(
                                'User.available_balance_amount' => 'User.available_balance_amount +' . $deal_user['discount_amount']
                                    ), array(
                                'User.id' => $deal_user['user_id']
                            ));
                        }
                        $address = '';
                        if (!empty($deal['Deal']['custom_company_address1'])) {
                            $address.= $deal['Deal']['custom_company_address1'];
                        } else if (!empty($deal['Company']['address1'])) {
                            $address.= $deal['Company']['address1'];
                        }
                        if (!empty($deal['Deal']['custom_company_address2'])) {
                            $address.= ', ' . $deal['Deal']['custom_company_address2'];
                        } else if (!empty($deal['Company']['address2'])) {
                            $address.= ', ' . $deal['Company']['address2'];
                        }
                        if (!empty($deal['Deal']['custom_company_city'])) {
                            $address.= ', ' . $city = $deal['Deal']['custom_company_city'];
                        } else if (!empty($deal['Company']['City']['name'])) {
                            $address.= ', ' . $city = '';
                        } else {
                            $city = '';
                        }
                        if (!empty($deal['Deal']['custom_company_state'])) {
                            $address.= ', ' . $state = $deal['Deal']['custom_company_state'];
                        } else if (!empty($deal['Company']['State']['name'])) {
                            $address.= ', ' . $state = $deal['Company']['State']['name'];
                        } else {
                            $state = '';
                        }
                        if (!empty($deal['Deal']['custom_company_country'])) {
                            $address.= ', ' . $country = $deal['Deal']['custom_company_country'];
                        } else if (!empty($deal['Company']['Country']['name'])) {
                            $address.= ', ' . $country = $deal['Company']['Country']['name'];
                        } else {
                            $country = '';
                        }
                        if (!empty($deal['Deal']['custom_company_zip'])) {
                            $address.= ', ' . $deal['Deal']['custom_company_zip'];
                        } else if (!empty($deal['Company']['zip'])) {
                            $address.= ', ' . $deal['Company']['zip'];
                        }
                        if (!empty($deal['Deal']['custom_company_contact_phone'])) {
                            $address.= ', ' . $deal['Deal']['custom_company_contact_phone'];
                        } else if (!empty($deal['Company']['phone'])) {
                            $address.= ', ' . $deal['Company']['phone'];
                        }
                        $emailFindReplace = array(
                            '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
                            '##USER_NAME##' => $deal_user['User']['username'],
                            '##SITE_NAME##' => Configure::read('site.name'),
                            '##DEAL_NAME##' => $deal['Deal']['name'],
                            '##COMPANY_NAME##' => !empty($deal['Deal']['custom_company_name']) ? $deal['Deal']['custom_company_name'] : $deal['Company']['name'],
                            '##COMPANY_ADDRESS##' => $address,
                            '##CITY_NAME##' => $deal['City']['name'],
                            '##ORIGINAL_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']),
                            '##SAVINGS##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['savings']),
                            '##BUY_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']),
                            '##DISCOUNT##' => $deal['Deal']['discount_percentage'] . ' %',
                            '##COMPANY_SITE##' => $deal['Company']['url'],
                            '##DEAL_URL##' => Configure::read('static_domain_for_mails') . Router::url(array(
                                'controller' => 'deals',
                                'action' => 'view',
                                $deal['Deal']['slug'],
                                'admin' => false
                                    ), false),
                            '##DEAL_LINK##' => Configure::read('static_domain_for_mails') . Router::url(array(
                                'controller' => 'deals',
                                'action' => 'view',
                                $deal['Deal']['slug'],
                                'admin' => false
                                    ), false),
                            '##IMG_URL##' => Configure::read('static_domain_for_mails') . Router::url(array(
                                'controller' => 'img',
                                'action' => $image_hash
                                    ), false),
                            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
                        );
                        $template = $this->EmailTemplate->selectTemplate('Deal Amount Refunded');
                        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
                        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
                        $this->Email->to = $deal_user['User']['email'];
                        $this->Email->subject = strtr($template['subject'], $emailFindReplace);
                        $this->Email->content = strtr($template['email_content'], $emailFindReplace);
                        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
                        $this->Email->send($this->Email->content);
                        $dealUserIds[] = $deal_user['id'];
                    }
                    if (!empty($dealUserIds)) {
                        $this->DealUser->updateAll(array(
                            'DealUser.is_repaid' => 1
                                ), array(
                            'DealUser.id' => $dealUserIds
                        ));
                    }
                }
                $refundedDealIds[] = $deal['Deal']['id'];
            }
            if (!empty($refundedDealIds)) {
                $this->updateAll(array(
                    'Deal.deal_status_id' => ConstDealStatus::Refunded
                        ), array(
                    'Deal.id' => $refundedDealIds
                ));
            }
        }
    }

    function _payToCompany($pay_type = '', $dealIds = array(), $is_now = false) {
        $conditions = array();
        $paidDealIds = array();
        if (!empty($dealIds)) {
            $conditions['Deal.id'] = $dealIds;
        } elseif ($pay_type == 'cron') {
            $conditions['Deal.deal_status_id'] = ConstDealStatus::Closed;
        }
        if ($is_now) {
            $DealUsersCountFilterClause = '(select count(*) from deal_users where deal_id = Deal.id and paid_date is null and deal_users.is_used = 1 AND deal_users.deleted = 0) as unpaid_count';
            $dealUsersFilter = array(
                'DealUser.is_used' => 1
            );
        } else {
            $DealUsersCountFilterClause = '(select count(*) from deal_users where deal_users.deal_id = Deal.id AND deal_users.paid_date is null AND deal_users.deleted = 0) as unpaid_count';
            $dealUsersFilter = '';
        }
        $deals = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'Deal.id',
                'Deal.discounted_price',
                'Deal.company_id',
                'Deal.is_end_user',
                'Deal.bonus_amount',
                'Deal.commission_percentage',
                'Deal.is_tourism',
                'Deal.deal_user_count',
                $DealUsersCountFilterClause
            ),
            'contain' => array(
                'Company' => array(
                    'fields' => array(
                        'Company.id',
                        'Company.user_id'
                    )
                )
            ),
            'recursive' => 0
        ));
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                $totalPurchasedAmount = Precision::mul($deal['Deal']['discounted_price'], $deal['0']['unpaid_count']);
                $user_id = $deal['Company']['user_id'];
                $monto = Precision::add($deal['Deal']['bonus_amount'], Precision::mul($totalPurchasedAmount, Precision::div($deal['Deal']['commission_percentage'], '100')));
                $monto = Precision::mul($monto, Configure::read('IVA'));
                $precioUnitario = Precision::mul($deal['Deal']['discounted_price'], Precision::div($deal['Deal']['commission_percentage'], '100'));
                $precioUnitario = Precision::mul($precioUnitario, Configure::read('IVA'));
                $ret = ($deal['0']['unpaid_count'] == 0) || $this->bacLiquidarEmpresa($deal['Deal']['company_id'], $deal['0']['unpaid_count'], $monto, $deal['Deal']['commission_percentage'], $precioUnitario, $deal['Deal']['id'], $deal['Deal']['is_tourism'], $deal['Deal']['is_end_user']);
                if ($ret) {
                    $paidDealIds[] = $deal['Deal']['id'];
                }
            }
            $this->updateAll(array(
                'Deal.modified' => '"' . date('Y-m-d H:i:s') . '"'
                    ), array(
                'Deal.id' => $paidDealIds
            ));
            $this->DealUser->updateAll(array(
                'DealUser.paid_date' => '"' . date('Y-m-d H:i:s') . '"'
                    ), array(
                'DealUser.deal_id' => $paidDealIds,
                'DealUser.paid_date is null',
                $dealUsersFilter
            ));
            return count($paidDealIds) === count($deals);
        }
        return false;
    }

    function _buildGATracking($name, &$medium = null, &$content = null, &$source = null, &$term = null) {
        if (is_null($source))
            $source = Configure::read('Campaign.source');
        if (is_null($medium))
            $medium = Configure::read('Campaign.subscription_medium');
        return 'utm_source' . '=' . $source . '&' . 'utm_medium' . '=' . $medium . (is_null($term) ? '' : '&' . 'utm_term' . '=' . $term) . (is_null($content) ? '' : '&' . 'utm_content' . '=' . $content) . '&' . 'utm_campaign' . '=' . $name;
    }

    function _getSubscriptionsForMail($registerUsersQuery, $cityId, $limit, $page) {
        $conditions = array(
            'city_id' => $cityId,
            'is_subscribed' => 1
        );
        if ($registerUsersQuery) {
            
        } else {
            
        }
        $q = Configure::read('instance.instanceQuantity');
        $n = Configure::read('instance.instanceNumber');
        if ($q > 1) {
            $conditions[] = ' MOD(Subscription.id,' . $q . ') = ' . $n;
        }
        return $this->City->Subscription->find('all', array(
                    'conditions' => $conditions,
                    'order' => array(
                        'Subscription.is_voluntary DESC'
                    ),
                    'fields' => array(
                        'Subscription.id',
                        'Subscription.email'
                    ),
                    'limit' => $limit,
                    'page' => $page,
                    'recursive' => - 1
        ));
    }

    function _sendSubscriptionMailForMultipleDealsOLD($deals, $registerUsersQuery = false) {
        App::import('Model', 'Banner');
        $banner = new Banner();
        $banner_ad_data = $banner->__getNewsletterBanner($deals[0]['City']['id']);
        $vars['BANNER_IMG_PATH'] = $banner_ad_data['Banner']['image'];
        $vars['BANNER_IMG_LINK'] = $banner_ad_data['Banner']['link'];
        $vars['URL_TRACKER'] = $banner_ad_data['Banner']['tracker_url'];
        $this->Email->from = 'ClubCupon - ' . $deals[0]['City']['name'] . ' ' . chr(60) . '%EMAIL_FROM%' . chr(62);
        $city_id = $deals[0]['City']['id'];
        $city_name = $deals[0]['City']['name'];
        $template = $this->EmailTemplate->selectTemplate('Deal of the day');
        $replyto = !empty($deals[0]['City']['email_from']) ? $deals[0]['City']['email_from'] : Configure::read('EmailTemplate.reply_to_email');
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? $replyto : $template['reply_to'];
        $limit = Configure::read('email_burst_limit');
        $sleeptime = Configure::read('email_burst_sleep');
        $page = 1;
        $subscriptions = $this->_getSubscriptionsForMail($registerUsersQuery, $deals[0]['City']['id'], $limit, $page);
        $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/';
        $vars['DISCOUNT_PERCENTAGE'] = $deals[0]['Deal']['discount_percentage'];
        $vars['CITY_NAME'] = $deals[0]['City']['name'];
        $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
        $vars['SITE_NAME'] = Configure::read('site.name');
        $vars['TWITTER_URL'] = empty($deals[0]['City']['twitter_url']) ? Configure::read('twitter.site_twitter_url') : $deals[0]['City']['twitter_url'];
        $vars['FACEBOOK_URL'] = empty($deals[0]['City']['facebook_url']) ? Configure::read('facebook.site_facebook_url') : $deals[0]['City']['facebook_url'];
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        while (!empty($subscriptions)) {
            Debugger::log('_sendSubscriptionMail Enviando pagina ' . $page, LOG_DEBUG);
            foreach ($subscriptions as $subscriber) {
                $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'contacts',
                            'action' => 'add',
                            'admin' => false
                                ), false) . '?from=mailing';
                $vars['UNSUBSCRIBE_LINK'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'subscriptions',
                            'action' => 'unsubscribe',
                            $subscriber['Subscription']['id'],
                            'admin' => false
                                ), false);
                $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
                $vars['SITE_NAME'] = Configure::read('site.name');
                $vars['FACEBOOK_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'facebook.png',
                            'admin' => false
                                ), false);
                $vars['TWITTER_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'twitter.png',
                            'admin' => false
                                ), false);
                $vars['CONTACT_US'] = $contact_us_url;
                $vars['BACKGROUND'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'background.png',
                            'admin' => false
                                ), false);
                $vars['BTN_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'btn.png',
                            'admin' => false
                                ), false);
                $vars['READMORE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'btn.png',
                            'admin' => false
                                ), false);
                $vars['EMAIL-COMMENT-BG'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'email-comment-bg.png',
                            'admin' => false
                                ), false);
                $vars['SITE_LOGO'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'img',
                            'action' => 'theme-image',
                            'logo-email.png',
                            'admin' => false
                                ), false);
                $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/';
                foreach ($deals as & $deal) {
                    $address = $country = $state = $city = '';
                    if (!empty($deal['Company']['address1'])) {
                        $address.= $deal['Company']['address1'];
                    }
                    if (!empty($deal['Company']['address2'])) {
                        $address.= ', ' . $deal['Company']['address2'];
                    }
                    if (!empty($deal['Company']['City']['name'])) {
                        $address.= ', ' . $city = $deal['Company']['City']['name'];
                    }
                    if (!empty($deal['Company']['State']['name'])) {
                        $address.= ', ' . $state = $deal['Company']['State']['name'];
                    }
                    if (!empty($deal['Company']['Country']['name'])) {
                        $address.= ', ' . $country = $deal['Company']['Country']['name'];
                    }
                    if (!empty($deal['Company']['zip'])) {
                        $address.= ', ' . $deal['Company']['zip'];
                    }
                    if (!empty($deal['Company']['phone'])) {
                        $address.= ', ' . $deal['Company']['phone'];
                    }
                    $image_hash = 'small_big_thumb/Deal/' . $deal['Attachment']['id'] . '.' . md5(Configure::read('Security.salt') . 'Deal' . $deal['Attachment']['id'] . 'jpg' . 'small_big_thumb' . Configure::read('site.name')) . '.' . 'jpg';
                    $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                                'controller' => 'deals',
                                'action' => 'view',
                                $deal['Deal']['slug'],
                                'admin' => false
                                    ), false) . '?' . $this->_buildGATracking($deal['Deal']['id'], $medium, $subscriber['Subscription']['User']['username'], $source) . '&popup=no';
                    $image_options = array(
                        'dimension' => 'medium_big_thumb',
                        'class' => '',
                        'alt' => $deal['Deal']['name'],
                        'title' => $deal['Deal']['name'],
                        'type' => 'jpg'
                    );
                    $src = $this->getImageUrl('Deal', $deal['Attachment'], $image_options);
                    $deal['COMMENT'] = $deal['Deal']['comment'];
                    $deal['DEAL_IMAGE'] = $src;
                    $deal['DEAL_NAME'] = $deal['Deal']['name'];
                    $deal['COMPANY_NAME'] = $deal['Company']['name'];
                    $deal['COMPANY_ADDRESS'] = $address;
                    $deal['COMPANY_WEBSITE'] = $deal['Company']['url'];
                    $deal['CITY_NAME'] = $deal['City']['name'];
                    $deal['ORIGINAL_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']);
                    $deal['SAVINGS'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['savings']);
                    $deal['BUY_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']);
                    $deal['DISCOUNT'] = $deal['Deal']['discount_percentage'] . '%';
                    $deal['DESCRIPTION'] = $deal['Deal']['coupon_highlights'];
                    $deal['SUBTITLE'] = $deal['Deal']['subtitle'];
                    $deal['COMPANY_SITE'] = $deal['Company']['url'];
                    $deal['COUPON_CONDITION'] = $deal['Deal']['coupon_condition'];
                    $deal['HIDE_PRICE'] = $deal['Deal']['hide_price'];
                    $deal['ONLY_PRICE'] = $deal['Deal']['only_price'];
                    $deal['DEAL_URL'] = $deal_url;
                    $deal['DEAL_LINK'] = $deal_url;
                    $deal['DEAL_IMAGE'] = $src;
                    $deal['RSS_FEED'] = $rss_feed_url_for_city;
                    $deal['FACEBOOK_URL'] = 'http://www.facebook.com/share.php?u=' . preg_replace('/\//', '', Router::url('/', true), 1) . 'deal/' . $deal['Deal']['slug'];
                    $deal['TWITTER_URL'] = 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $deal['Deal']['bitly_short_url_prefix'];
                    $deal['DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['start_date'])), ENT_QUOTES, 'UTF-8');
                    $deal['END_DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['end_date'])), ENT_QUOTES, 'UTF-8');
                    if (!$deal['Deal']['is_side_deal'])
                        $vars['main_deal'] = $deal;
                }
                $vars['deals'] = $deals;
                $this->Email->to = $subscriber['Subscription']['email'];
                $this->Email->from = 'ClubCupon - ' . $city_name . ' ' . chr(60) . '%EMAIL_FROM%' . chr(62);
                $this->Email->return = $city_id . "+" . date("Ymd") . "+cc+";
                $this->Email->return.= str_replace("@", "=", $subscriber['Subscription']['email']);
                $this->Email->return.= Configure::read('EmailTemplate.catch_all_account');
                Debugger::log("this->Email->return =>" . $this->Email->return, LOG_DEBUG);
                if (Configure::read('EmailTemplate.debug_email')) {
                    $this->Email->from = str_replace('%EMAIL_FROM%', $this->Email->return, $this->Email->from);
                } else {
                    $this->Email->from = str_replace('%EMAIL_FROM%', Configure::read('EmailTemplate.from_email'), $this->Email->from);
                }
                $attachment_newsletter = $this->Attachment->find('first', array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $vars['main_deal']['Deal']['id'],
                        'Attachment.class = ' => 'DealNewsLetter'
                    ),
                    'recursive' => - 1
                ));
                $vars['main_deal']['newsletterimage'] = !empty($attachment_newsletter);
                if ($vars['main_deal']['newsletterimage']) {
                    $src = $this->getImageUrl('DealNewsLetter', array(
                        'id' => $attachment_newsletter['Attachment']['id']
                            ), array(
                        'dimension' => 'original'
                    ));
                    $vars['main_deal']['DEAL_IMAGE'] = $src;
                }
                if (empty($vars['main_deal']['Deal']['custom_subject'])) {
                    if (count($deals) == 1) {
                        $this->Email->subject = $vars['main_deal']['DEAL_NAME'];
                    } else if (count($deals) == 2) {
                        $this->Email->subject = $vars['main_deal']['DEAL_NAME'] . ' Y 1 Super Oferta mas.';
                    } else {
                        $this->Email->subject = $vars['main_deal']['DEAL_NAME'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas mas.';
                    }
                } else {
                    $this->Email->subject = $vars['main_deal']['Deal']['custom_subject'];
                }
                $this->Email->content = $this->EmailTemplate->getContentDynamicTemplate('Deal Of the day multiple', $vars);
                $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
                $strLog = '(USUARIO NO REGISTRADO)';
                if ($registerUsersQuery) {
                    $strLog = '(USUARIO POR CLUSTER)';
                }
                if ($this->Email->send($this->Email->content)) {
                    Debugger::log('_sendSubscriptionMailForMultipleDeals Email enviado a:' . $subscriber['Subscription']['email'] . ' ' . $strLog, LOG_DEBUG);
                } else {
                    Debugger::log('_sendSubscriptionMailForMultipleDeals Email no pudo ser enviado a:' . $subscriber['Subscription']['email'] . ' ' . $strLog, LOG_DEBUG);
                }
            }
            usleep($sleeptime);
            $page++;
            $subscriptions = $this->_getSubscriptionsForMail($registerUsersQuery, $deals[0]['City']['id'], $limit, $page);
        }
    }

    function _sendSubscriptionMailForMultipleDealsNEW($deals, $registerUsersQuery = false) {
        Debugger::log('_sendSubscriptionMailForMultipleDeals inicio', LOG_DEBUG);
        App::import('Model', 'Banner');
        $banner = new Banner();
        $banner_ad_data = $banner->__getNewsletterBanner($deals[0]['City']['id']);
        $vars['BANNER_IMG_PATH'] = $banner_ad_data['Banner']['image'];
        $vars['BANNER_IMG_LINK'] = $banner_ad_data['Banner']['link'];
        $vars['URL_TRACKER'] = $banner_ad_data['Banner']['tracker_url'];
        $email_from = !empty($deals[0]['City']['email_from']) ? $deals[0]['City']['email_from'] : Configure::read('EmailTemplate.from_email');
        $email_from = 'ClubCupon - ' . $deals[0]['City']['name'] . ' ' . chr(60) . $email_from . chr(62);
        $template = $this->EmailTemplate->selectTemplate('Deal of the day');
        $city_name = $deals[0]['City']['name'];
        $city_id = $deals[0]['City']['id'];
        $replyto = !empty($deals[0]['City']['email_from']) ? $deals[0]['City']['email_from'] : Configure::read('EmailTemplate.reply_to_email');
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? $replyto : $template['reply_to'];
        $limit = Configure::read('email_burst_limit');
        $sleeptime = Configure::read('email_burst_sleep');
        $page = 1;
        $subscriptions = $this->_getSubscriptionsForMail($registerUsersQuery, $deals[0]['City']['id'], $limit, $page);
        $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/';
        $vars['DISCOUNT_PERCENTAGE'] = $deals[0]['Deal']['discount_percentage'];
        $vars['CITY_NAME'] = $deals[0]['City']['name'];
        $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
        $vars['SITE_NAME'] = Configure::read('site.name');
        $vars['TWITTER_URL'] = empty($deals[0]['City']['twitter_url']) ? Configure::read('twitter.site_twitter_url') : $deals[0]['City']['twitter_url'];
        $vars['FACEBOOK_URL'] = empty($deals[0]['City']['facebook_url']) ? Configure::read('facebook.site_facebook_url') : $deals[0]['City']['facebook_url'];
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'contacts',
                    'action' => 'add',
                    'admin' => false
                        ), false) . '?from=mailing';
        $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
        $vars['SITE_NAME'] = Configure::read('site.name');
        $vars['FACEBOOK_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'facebook.png',
                    'admin' => false
                        ), false);
        $vars['TWITTER_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'twitter.png',
                    'admin' => false
                        ), false);
        $vars['CONTACT_US'] = $contact_us_url;
        $vars['BACKGROUND'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'background.png',
                    'admin' => false
                        ), false);
        $vars['BTN_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'btn.png',
                    'admin' => false
                        ), false);
        $vars['READMORE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'btn.png',
                    'admin' => false
                        ), false);
        $vars['EMAIL-COMMENT-BG'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'email-comment-bg.png',
                    'admin' => false
                        ), false);
        $vars['SITE_LOGO'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'logo-email.png',
                    'admin' => false
                        ), false);
        $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/';
        foreach ($deals as & $deal) {
            $address = $country = $state = $city = '';
            if (!empty($deal['Company']['address1'])) {
                $address.= $deal['Company']['address1'];
            }
            if (!empty($deal['Company']['address2'])) {
                $address.= ', ' . $deal['Company']['address2'];
            }
            if (!empty($deal['Company']['City']['name'])) {
                $address.= ', ' . $city = $deal['Company']['City']['name'];
            }
            if (!empty($deal['Company']['State']['name'])) {
                $address.= ', ' . $state = $deal['Company']['State']['name'];
            }
            if (!empty($deal['Company']['Country']['name'])) {
                $address.= ', ' . $country = $deal['Company']['Country']['name'];
            }
            if (!empty($deal['Company']['zip'])) {
                $address.= ', ' . $deal['Company']['zip'];
            }
            if (!empty($deal['Company']['phone'])) {
                $address.= ', ' . $deal['Company']['phone'];
            }
            $image_hash = 'small_big_thumb/Deal/' . $deal['Attachment']['id'] . '.' . md5(Configure::read('Security.salt') . 'Deal' . $deal['Attachment']['id'] . 'jpg' . 'small_big_thumb' . Configure::read('site.name')) . '.' . 'jpg';
            $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                        'controller' => 'deals',
                        'action' => 'view',
                        $deal['Deal']['slug'],
                        'admin' => false
                            ), false) . '?' . $this->_buildGATracking($deal['Deal']['id'], $medium) . '&popup=no';
            $image_options = array(
                'dimension' => 'medium_big_thumb',
                'class' => '',
                'alt' => $deal['Deal']['name'],
                'title' => $deal['Deal']['name'],
                'type' => 'jpg'
            );
            $src = $this->getImageUrl('Deal', $deal['Attachment'], $image_options);
            $deal['COMMENT'] = $deal['Deal']['comment'];
            $deal['DEAL_IMAGE'] = $src;
            $deal['DEAL_NAME'] = $deal['Deal']['name'];
            $deal['COMPANY_NAME'] = $deal['Company']['name'];
            $deal['COMPANY_ADDRESS'] = $address;
            $deal['COMPANY_WEBSITE'] = $deal['Company']['url'];
            $deal['CITY_NAME'] = $deal['City']['name'];
            $deal['ORIGINAL_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']);
            $deal['SAVINGS'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['savings']);
            $deal['BUY_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']);
            $deal['DISCOUNT'] = $deal['Deal']['discount_percentage'] . '%';
            $deal['DESCRIPTION'] = $deal['Deal']['coupon_highlights'];
            $deal['SUBTITLE'] = $deal['Deal']['subtitle'];
            $deal['COMPANY_SITE'] = $deal['Company']['url'];
            $deal['COUPON_CONDITION'] = $deal['Deal']['coupon_condition'];
            $deal['HIDE_PRICE'] = $deal['Deal']['hide_price'];
            $deal['ONLY_PRICE'] = $deal['Deal']['only_price'];
            $deal['DEAL_URL'] = $deal_url;
            $deal['DEAL_LINK'] = $deal_url;
            $deal['DEAL_IMAGE'] = $src;
            $deal['RSS_FEED'] = $rss_feed_url_for_city;
            $deal['FACEBOOK_URL'] = 'http://www.facebook.com/share.php?u=' . preg_replace('/\//', '', Router::url('/', true), 1) . 'deal/' . $deal['Deal']['slug'];
            $deal['TWITTER_URL'] = 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $deal['Deal']['bitly_short_url_prefix'];
            $deal['DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['start_date'])), ENT_QUOTES, 'UTF-8');
            $deal['END_DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['end_date'])), ENT_QUOTES, 'UTF-8');
            if (!$deal['Deal']['is_side_deal']) {
                $vars['main_deal'] = $deal;
            }
        }
        $attachment_newsletter = $this->Attachment->find('first', array(
            'conditions' => array(
                'Attachment.foreign_id = ' => $vars['main_deal']['Deal']['id'],
                'Attachment.class = ' => 'DealNewsLetter'
            ),
            'recursive' => - 1
        ));
        $vars['main_deal']['newsletterimage'] = !empty($attachment_newsletter);
        if ($vars['main_deal']['newsletterimage']) {
            $src = $this->getImageUrl('DealNewsLetter', array(
                'id' => $attachment_newsletter['Attachment']['id']
                    ), array(
                'dimension' => 'original'
            ));
            $vars['main_deal']['DEAL_IMAGE'] = $src;
        }
        $vars['deals'] = $deals;
        if (empty($vars['main_deal']['Deal']['custom_subject'])) {
            if (count($deals) == 1) {
                $subject = $vars['main_deal']['DEAL_NAME'];
            } else if (count($deals) == 2) {
                $subject = $vars['main_deal']['DEAL_NAME'] . ' Y 1 Super Oferta mas.';
            } else {
                $subject = $vars['main_deal']['DEAL_NAME'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas mas.';
            }
        } else {
            $subject = $vars['main_deal']['Deal']['custom_subject'];
        }
        $vars['UNSUBSCRIBE_LINK'] = '_UNSUBSCRIBE_HERE_';
        $content = $this->EmailTemplate->getContentDynamicTemplate('Deal Of the day multiple', $vars);
        while (!empty($subscriptions)) {
            Debugger::log('_sendSubscriptionMail Enviando pagina ' . $page, LOG_DEBUG);
            foreach ($subscriptions as $subscriber) {
                $unsubscribe_link = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'subscriptions',
                            'action' => 'unsubscribe',
                            $subscriber['Subscription']['id'],
                            'admin' => false
                                ), false);
                $this->Email->from = 'ClubCupon - ' . $city_name . ' ' . chr(60) . '%EMAIL_FROM%' . chr(62);
                $this->Email->return = $city_id . "+" . date("Ymd") . "+cc+";
                $this->Email->return.= str_replace("@", "=", $subscriber['Subscription']['email']);
                $this->Email->return.= Configure::read('EmailTemplate.catch_all_account');
                $this->Email->to = $subscriber['Subscription']['email'];
                Debugger::log('this->Email->return =>' . $this->Email->return, LOG_DEBUG);
                if (Configure::read('EmailTemplate.debug_email')) {
                    $this->Email->from = str_replace('%EMAIL_FROM%', $this->Email->return, $this->Email->from);
                } else {
                    $this->Email->from = str_replace('%EMAIL_FROM%', Configure::read('EmailTemplate.from_email'), $this->Email->from);
                }
                $this->Email->subject = $subject;
                $contentForSubscriber = str_replace('_UNSUBSCRIBE_HERE_', $unsubscribe_link, $content);
                $this->Email->content = $contentForSubscriber;
                $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
                $strLog = '(USUARIO NO REGISTRADO)';
                if ($registerUsersQuery) {
                    $strLog = '(USUARIO POR CLUSTER)';
                }
                Debugger::log(__LINE__ . ' this->Email->return =>' . $this->Email->return, LOG_DEBUG);
                if ($this->Email->send($this->Email->content)) {
                    Debugger::log('_sendSubscriptionMailForMultipleDeals Email enviado a:' . $subscriber['Subscription']['email'] . ' ' . $strLog, LOG_DEBUG);
                } else {
                    Debugger::log('_sendSubscriptionMailForMultipleDeals Email no pudo ser enviado a:' . $subscriber['Subscription']['email'] . ' ' . $strLog, LOG_DEBUG);
                }
            }
            usleep($sleeptime);
            $page++;
            $subscriptions = $this->_getSubscriptionsForMail($registerUsersQuery, $deals[0]['City']['id'], $limit, $page);
            Debugger::log('_sendSubscriptionMailForMultipleDeals fin', LOG_DEBUG);
        }
    }

    function _sendSubscriptionMailForMultipleDeals($deals, $registerUsersQuery = false) {
        if (true) {
            $this->_sendSubscriptionMailForMultipleDealsNEW($deals, $registerUsersQuery);
        } else {
            $this->_sendSubscriptionMailForMultipleDealsOLD($deals, $registerUsersQuery);
        }
    }

    function _sendSubscriptionMailForOneDeal($deal) {
        if ($deal['Deal']['hide_price'] == 0) {
            $template = $this->EmailTemplate->selectTemplate('Deal of the day');
        } else {
            $template = $this->EmailTemplate->selectTemplate('Deal of the day hide price');
        }
        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
        $savings = $deal['Deal']['savings'];
        $buy_price = $deal['Deal']['discounted_price'];
        $address = $country = $state = $city = '';
        if (!empty($deal['Company']['address1'])) {
            $address.= $deal['Company']['address1'];
        }
        if (!empty($deal['Company']['address2'])) {
            $address.= ', ' . $deal['Company']['address2'];
        }
        if (!empty($deal['Company']['City']['name'])) {
            $address.= ', ' . $city = $deal['Company']['City']['name'];
        }
        if (!empty($deal['Company']['State']['name'])) {
            $address.= ', ' . $state = $deal['Company']['State']['name'];
        }
        if (!empty($deal['Company']['Country']['name'])) {
            $address.= ', ' . $country = $deal['Company']['Country']['name'];
        }
        if (!empty($deal['Company']['zip'])) {
            $address.= ', ' . $deal['Company']['zip'];
        }
        if (!empty($deal['Company']['phone'])) {
            $address.= ', ' . $deal['Company']['phone'];
        }
        $image_hash = 'small_big_thumb/Deal/' . $deal['Attachment']['id'] . '.' . md5(Configure::read('Security.salt') . 'Deal' . $deal['Attachment']['id'] . 'jpg' . 'small_big_thumb' . Configure::read('site.name')) . '.' . 'jpg';
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'contacts',
                    'action' => 'add',
                    'admin' => false
                        ), false);
        $image_options = array(
            'dimension' => 'medium_big_thumb',
            'class' => '',
            'alt' => $deal['Deal']['name'],
            'title' => $deal['Deal']['name'],
            'type' => 'jpg'
        );
        $src = $this->getImageUrl('Deal', $deal['Attachment'], $image_options);
        $emailFindReplace = array(
            '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
            '##SITE_NAME##' => Configure::read('site.name'),
            '##DEAL_NAME##' => $deal['Deal']['name'],
            '##COMPANY_NAME##' => $deal['Company']['name'],
            '##COMPANY_ADDRESS##' => $address,
            '##COMPANY_WEBSITE##' => $deal['Company']['url'],
            '##CITY_NAME##' => $deal['City']['name'],
            '##ORIGINAL_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']),
            '##SAVINGS##' => Configure::read('site.currency') . removeZeroDecimals($savings),
            '##BUY_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($buy_price),
            '##DISCOUNT##' => $deal['Deal']['discount_percentage'] . '%',
            '##DESCRIPTION##' => $deal['Deal']['coupon_highlights'],
            '##COMPANY_SITE##' => $deal['Company']['url'],
            '##COUPON_CONDITION##' => $deal['Deal']['coupon_condition'],
            '##DEAL_IMAGE##' => $src,
            '##FACEBOOK_URL##' => 'http://www.facebook.com/share.php?u=' . preg_replace('/\//', '', Router::url('/', true), 1) . 'deal/' . $deal['Deal']['slug'],
            '##TWITTER_URL##' => 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $deal['Deal']['bitly_short_url_prefix'],
            '##DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['start_date'])), ENT_QUOTES, 'UTF-8'),
            '##END_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['end_date'])), ENT_QUOTES, 'UTF-8'),
            '##FACEBOOK_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'facebook.png',
                'admin' => false
                    ), false),
            '##TWITTER_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'twitter.png',
                'admin' => false
                    ), false),
            '##COMMENT##' => $deal['Deal']['comment'],
            '##CONTACT_US##' => $contact_us_url,
            '##BACKGROUND##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'background.png',
                'admin' => false
                    ), false),
            '##DEAL_IMAGE##' => $src,
            '##BTN_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'btn.png',
                'admin' => false
                    ), false),
            '##READMORE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'btn.png',
                'admin' => false
                    ), false),
            'EMAIL-COMMENT-BG' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'email-comment-bg.png',
                'admin' => false
                    ), false),
            '##SITE_LOGO##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'logo-email.png',
                'admin' => false
                    ), false),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        $limit = Configure::read('email_burst_limit');
        $sleeptime = Configure::read('email_burst_sleep');
        $page = 1;
        $subscriptions = $this->City->Subscription->find('all', array(
            'conditions' => array(
                'city_id' => $deal['City']['id'],
                'is_subscribed' => 1
            ),
            'fields' => array(
                'Subscription.id',
                'Subscription.email'
            ),
            'limit' => $limit,
            'page' => $page,
            'recursive' => - 1
        ));
        while (!empty($subscriptions)) {
            Debugger::log('_sendSubscriptionMail Enviando pagina ' . $page, LOG_DEBUG);
            foreach ($subscriptions as $subscriber) {
                $emailFindReplace['##UNSUBSCRIBE_LINK##'] = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'subscriptions',
                            'action' => 'unsubscribe',
                            $subscriber['Subscription']['id'],
                            'admin' => false
                                ), false);
                $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                            'controller' => 'deals',
                            'action' => 'view',
                            $deal['Deal']['slug'],
                            'admin' => false
                                ), false) . '?' . $this->_buildGATracking($deal['Deal']['id'], $medium, $subscriber['Subscription']['User']['username'], $source) . '&popup=no';
                $emailFindReplace['##DEAL_URL##'] = $deal_url;
                $emailFindReplace['##DEAL_LINK##'] = $deal_url;
                $this->Email->to = $subscriber['Subscription']['email'];
                $this->Email->subject = empty($deal['Deal']['custom_subject']) ? $deal['Deal']['name'] : $deal['Deal']['custom_subject'];
                $this->Email->content = strtr($template['email_content'], $emailFindReplace);
                $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
                if ($this->Email->send($this->Email->content)) {
                    Debugger::log('_sendSubscriptionMailForOneDeal Email enviado a:' . $subscriber['Subscription']['email'], LOG_DEBUG);
                } else {
                    Debugger::log('_sendSubscriptionMailForOneDeal Email no pudo ser enviado a:' . $subscriber['Subscription']['email'], LOG_DEBUG);
                }
            }
            usleep($sleeptime);
            $page++;
            $subscriptions = $this->City->Subscription->find('all', array(
                'conditions' => array(
                    'city_id' => $deal['City']['id'],
                    'is_subscribed' => 1
                ),
                'fields' => array(
                    'Subscription.id',
                    'Subscription.email'
                ),
                'limit' => $limit,
                'page' => $page,
                'recursive' => - 1
            ));
        }
    }

    function _sendSubscriptionMailForCity($city_id) {
        App::import('Model', 'Subscription');
        $this->Subscription = & new Subscription();
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        App::import('Component', 'Email');
        $this->Email = & new EmailComponent();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $deals = $this->find('all', array(
            'order' => array(
                'Deal.priority' => 'DESC',
                'Deal.deal_status_id' => 'ASC',
                'Deal.start_date' => 'ASC'
            ),
            'conditions' => array(
                'Deal.parent_deal_id = Deal.id',
                'Deal.deal_status_id' => array(
                    ConstDealStatus::Open,
                    ConstDealStatus::Tipped
                ),
                'Deal.is_subscription_mail_sent' => 0,
                'Deal.city_id' => $city_id,
                'Deal.publication_channel_type_id' => 1
            ),
            'contain' => array(
                'Attachment',
                'City',
                'Company' => array(
                    'City',
                    'State',
                    'Country'
                )
            )
        ));
        Debugger::log('_sendSubscriptionMailForCity SE ENCONTRARON ' . count($deals) . ' para city_id: ' . $city_id, LOG_DEBUG);
        if (!empty($deals)) {
            $hasMainDeal = false;
            $mainDeal = false;
            $dealIds = array();
            foreach ($deals as $deal) {
                $dealIds[] = $deal['Deal']['id'];
                if (!$deal['Deal']['is_side_deal']) {
                    $mainDeal = $deal;
                    $hasMainDeal = true;
                }
            }
            if ($hasMainDeal) {
                Debugger::log('_sendSubscriptionMailForCity Envio del MAIL CON MULTIPLES OFERTAS. para city_id: ' . $city_id, LOG_DEBUG);
                $clusterExamples = $this->Cluster->ClusterExample->find('all', array(
                    'recursive' => - 1,
                    'conditions' => array(
                        'ClusterExample.cluster_id' => $mainDeal['Deal']['cluster_id']
                    )
                ));
                $registerUsersQuery = false;
                $this->_sendSubscriptionMailForMultipleDeals($deals);
                if ($registerUsersQuery) {
                    $this->_sendSubscriptionMailForMultipleDeals($deals, $registerUsersQuery);
                }
            } else {
                Debugger::log('_sendSubscriptionMailForCity No se encontraron ofertas principales (pero si ofertas laterales) para city_id: ' . $city_id, LOG_DEBUG);
                $dealIds = array();
            }
            $is_main = Configure::read('instance.is_main');
            if (!empty($dealIds) && $is_main == 1) {
                $this->updateAll(array(
                    'Deal.is_subscription_mail_sent' => 1
                        ), array(
                    'Deal.id' => $dealIds
                ));
            }
        }
    }

    function _sendAdminSubscriptionMail() {
        $cities = $this->City->find('all', array(
            'conditions' => array(
                'City.is_approved' => 1,
                'City.has_newsletter' => true
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => - 1
        ));
        if (!$cities)
            return;
        foreach ($cities as $city)
            $this->_sendAdminSubscriptionMailForCity($city['City']['id']);
    }

    function _sendAdminSubscriptionMailForCity($city_id) {
        App::import('Model', 'Subscription');
        App::import('Model', 'EmailTemplate');
        App::import('Component', 'Email');
        $this->Subscription = & new Subscription();
        $this->EmailTemplate = & new EmailTemplate();
        $this->Email = & new EmailComponent();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $deals = $this->find('all', array(
            'conditions' => array(
                'Deal.city_id' => $city_id,
                'Deal.deal_status_id' => ConstDealStatus::Upcoming,
                'Deal.parent_deal_id = Deal.id',
                'Deal.is_subscription_mail_sent' => 0,
                'DATE(start_date) = DATE(DATE_ADD(NOW(),INTERVAL 1 DAY))'
            ),
            'contain' => array(
                'Company' => array(
                    'fields' => array(
                        'Company.name',
                        'Company.id',
                        'Company.url',
                        'Company.zip',
                        'Company.address1',
                        'Company.address2',
                        'Company.city_id',
                        'Company.phone'
                    ),
                    'City' => array(
                        'fields' => array(
                            'City.id',
                            'City.name',
                            'City.slug'
                        )
                    ),
                    'State' => array(
                        'fields' => array(
                            'State.id',
                            'State.name'
                        )
                    ),
                    'Country' => array(
                        'fields' => array(
                            'Country.id',
                            'Country.name',
                            'Country.slug'
                        )
                    )
                ),
                'Attachment' => array(
                    'fields' => array(
                        'Attachment.id',
                        'Attachment.dir',
                        'Attachment.filename',
                        'Attachment.width',
                        'Attachment.height'
                    )
                ),
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug'
                    )
                )
            )
        ));
        if (!empty($deals))
            $this->_sendAdminSubscriptionMailForMultipleDeals($deals);
    }

    function _sendAdminSubscriptionMailForOneDeal($deal) {
        App::import('Component', 'Auth');
        $this->Auth = new AuthComponent();
        $template = $this->EmailTemplate->selectTemplate('Deal of the day');
        $address = $country = $state = $city = '';
        if (!empty($deal['Company']['address1'])) {
            $address.= $deal['Company']['address1'];
        }
        if (!empty($deal['Company']['address2'])) {
            $address.= ', ' . $deal['Company']['address2'];
        }
        if (!empty($deal['Company']['City']['name'])) {
            $address.= ', ' . $city = $deal['Company']['City']['name'];
        }
        if (!empty($deal['Company']['State']['name'])) {
            $address.= ', ' . $state = $deal['Company']['State']['name'];
        }
        if (!empty($deal['Company']['Country']['name'])) {
            $address.= ', ' . $country = $deal['Company']['Country']['name'];
        }
        if (!empty($deal['Company']['zip'])) {
            $address.= ', ' . $deal['Company']['zip'];
        }
        if (!empty($deal['Company']['phone'])) {
            $address.= ', ' . $deal['Company']['phone'];
        }
        $image_options = array(
            'dimension' => 'medium_big_thumb',
            'class' => '',
            'alt' => $deal['Deal']['name'],
            'title' => $deal['Deal']['name'],
            'type' => 'jpg'
        );
        $src = $this->getImageUrl('Deal', $deal['Attachment'], $image_options);
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug'],
                    'admin' => false
                        ), false) . '?' . $this->_buildGATracking($deal['Deal']['id'], $medium, $_SESSION['Auth']['User']['username'], $source) . '&popup=no';
        $emailFindReplace = array(
            '##SITE_LINK##' => Configure::read('static_domain_for_mails'),
            '##SITE_NAME##' => Configure::read('site.name'),
            '##COMPANY_ADDRESS##' => $address,
            '##COMPANY_NAME##' => $deal['Company']['name'],
            '##COMPANY_WEBSITE##' => $deal['Company']['url'],
            '##COMPANY_SITE##' => $deal['Company']['url'],
            '##CITY_NAME##' => $deal['City']['name'],
            '##ORIGINAL_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']),
            '##SAVINGS##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['savings']),
            '##BUY_PRICE##' => Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']),
            '##DISCOUNT##' => $deal['Deal']['discount_percentage'] . '%',
            '##DESCRIPTION##' => $deal['Deal']['coupon_highlights'],
            '##COUPON_CONDITION##' => $deal['Deal']['coupon_condition'],
            '##COMMENT##' => $deal['Deal']['comment'],
            '##DEAL_URL##' => $deal_url,
            '##DEAL_LINK##' => $deal_url,
            '##CONTACT_US##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'contacts',
                'action' => 'add',
                'admin' => false
                    ), false),
            '##FACEBOOK_URL##' => 'http://www.facebook.com/share.php?u=' . preg_replace('/\//', '', Router::url('/', true), 1) . 'deal/' . $deal['Deal']['slug'],
            '##TWITTER_URL##' => 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $deal['Deal']['bitly_short_url_prefix'],
            '##DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['start_date'])), ENT_QUOTES, 'UTF-8'),
            '##END_DATE##' => htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['end_date'])), ENT_QUOTES, 'UTF-8'),
            '##FACEBOOK_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'facebook.png',
                'admin' => false
                    ), false),
            '##TWITTER_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'twitter.png',
                'admin' => false
                    ), false),
            '##BACKGROUND##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'background.png',
                'admin' => false
                    ), false),
            '##BTN_IMAGE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'btn.png',
                'admin' => false
                    ), false),
            '##READMORE##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'btn.png',
                'admin' => false
                    ), false),
            'EMAIL-COMMENT-BG' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'email-comment-bg.png',
                'admin' => false
                    ), false),
            '##SITE_LOGO##' => Configure::read('static_domain_for_mails') . Router::url(array(
                'controller' => 'img',
                'action' => 'theme-image',
                'logo-email.png',
                'admin' => false
                    ), false),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##DEAL_IMAGE##' => $src,
            '##UNSUBSCRIBE_LINK##' => '',
            '##DEAL_NAME##' => $deal['Deal']['name']
        );
        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
        $this->Email->subject = strtr($template['subject'], $emailFindReplace);
        $this->Email->content = strtr($template['email_content'], $emailFindReplace);
        $this->Email->to = $_SESSION['Auth']['User']['email'];
        $this->Email->send($this->Email->content);
    }

    function _sendAdminSubscriptionMailForMultipleDeals($deals) {
        App::import('Component', 'Auth');
        $this->Auth = new AuthComponent();
        $template = $this->EmailTemplate->selectTemplate('Deal of the day');
        $BannerClass = ClassRegistry::init('Banner');
        $banner_ad_data = $BannerClass->__getNewsletterBanner($deals[0]['City']['id']);
        $vars['BANNER_IMG_PATH'] = $banner_ad_data['image'];
        $vars['BANNER_IMG_LINK'] = $banner_ad_data['link'];
        $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
        $vars['SITE_NAME'] = Configure::read('site.name');
        $vars['CITY_NAME'] = $deals[0]['City']['name'];
        $vars['CONTACT_US'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'contacts',
                    'action' => 'add',
                    'admin' => false
                        ), false) . '?from=mailing';
        $vars['FACEBOOK_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'facebook.png',
                    'admin' => false
                        ), false);
        $vars['TWITTER_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'twitter.png',
                    'admin' => false
                        ), false);
        $vars['BACKGROUND'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'background.png',
                    'admin' => false
                        ), false);
        $vars['BTN_IMAGE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'btn.png',
                    'admin' => false
                        ), false);
        $vars['READMORE'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'btn.png',
                    'admin' => false
                        ), false);
        $vars['EMAIL-COMMENT-BG'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'email-comment-bg.png',
                    'admin' => false
                        ), false);
        $vars['SITE_LOGO'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'img',
                    'action' => 'theme-image',
                    'logo-email.png',
                    'admin' => false
                        ), false);
        $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/';
        $vars['UNSUBSCRIBE_LINK'] = '';
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        $i = 0;
        $vars['deals'] = $deals;
        foreach ($deals as & $deal) {
            $address = $country = $state = $city = '';
            if (!empty($deal['Company']['address1'])) {
                $address.= $deal['Company']['address1'];
            }
            if (!empty($deal['Company']['address2'])) {
                $address.= ', ' . $deal['Company']['address2'];
            }
            if (!empty($deal['Company']['City']['name'])) {
                $address.= ', ' . $city = $deal['Company']['City']['name'];
            }
            if (!empty($deal['Company']['State']['name'])) {
                $address.= ', ' . $state = $deal['Company']['State']['name'];
            }
            if (!empty($deal['Company']['Country']['name'])) {
                $address.= ', ' . $country = $deal['Company']['Country']['name'];
            }
            if (!empty($deal['Company']['zip'])) {
                $address.= ', ' . $deal['Company']['zip'];
            }
            if (!empty($deal['Company']['phone'])) {
                $address.= ', ' . $deal['Company']['phone'];
            }
            $deal_url = Configure::read('static_domain_for_mails') . Router::url(array(
                        'controller' => 'deals',
                        'action' => 'view',
                        $deal['Deal']['slug'],
                        'admin' => false
                            ), false) . '?' . $this->_buildGATracking($deal['Deal']['id'], $medium, $_SESSION['Auth']['User']['username'], $source) . 'popup=no';
            $image_options = array(
                'dimension' => 'medium_big_thumb',
                'class' => '',
                'alt' => $deal['Deal']['name'],
                'title' => $deal['Deal']['name'],
                'type' => 'jpg'
            );
            $src = $this->getImageUrl('Deal', $deal['Attachment'], $image_options);
            $deal['COMPANY_ADDRESS'] = $address;
            $deal['COMPANY_NAME'] = $deal['Company']['name'];
            $deal['COMPANY_WEBSITE'] = $deal['Company']['url'];
            $deal['COMPANY_SITE'] = $deal['Company']['url'];
            $deal['CITY_NAME'] = $deal['City']['name'];
            $deal['ORIGINAL_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']);
            $deal['SAVINGS'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['savings']);
            $deal['BUY_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']);
            $deal['DISCOUNT'] = $deal['Deal']['discount_percentage'] . '%';
            $deal['DESCRIPTION'] = $deal['Deal']['coupon_highlights'];
            $deal['COUPON_CONDITION'] = $deal['Deal']['coupon_condition'];
            $deal['DEAL_NAME'] = $deal['Deal']['name'];
            $deal['COMMENT'] = $deal['Deal']['comment'];
            $deal['DEAL_URL'] = $deal_url;
            $deal['DEAL_LINK'] = $deal_url;
            $deal['FACEBOOK_URL'] = 'http://www.facebook.com/share.php?u=' . preg_replace('/\//', '', Router::url('/', true), 1) . 'deal/' . $deal['Deal']['slug'];
            $deal['TWITTER_URL'] = 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $deal['Deal']['bitly_short_url_prefix'];
            $deal['DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['start_date'])), ENT_QUOTES, 'UTF-8');
            $deal['END_DATE'] = htmlentities(strftime("%d/%m/%Y", strtotime($deal['Deal']['end_date'])), ENT_QUOTES, 'UTF-8');
            $deal['DEAL_IMAGE'] = $src;
            if (!$deal['Deal']['is_side_deal'])
                $vars['main_deal'] = $deal;
            $vars['deals'][$i] = $deal;
            $i++;
        }
        $this->Email->from = ($template['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $template['from'];
        $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
        $this->Email->sendAs = ($template['is_html']) ? 'html' : 'text';
        if (empty($vars['main_deal']['Deal']['custom_subject'])) {
            if (count($deals) == 1) {
                $this->Email->subject = $vars['main_deal']['DEAL_NAME'];
            } else if (count($deals) == 2) {
                $this->Email->subject = $vars['main_deal']['DEAL_NAME'] . ' Y 1 Super Oferta mas.';
            } else {
                $this->Email->subject = $vars['main_deal']['DEAL_NAME'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas mas.';
            }
        } else {
            $this->Email->subject = $vars['main_deal']['Deal']['custom_subject'];
        }
        $this->Email->content = $this->EmailTemplate->getContentDynamicTemplate('Deal Of the day multiple', $vars);
        $this->Email->to = $_SESSION['Auth']['User']['email'];
        $this->Email->send($this->Email->content);
    }

    function _sendSubscriptionMail() {
        $cities = $this->City->find('all', array(
            'order' => array(
                'City.order' => 'DESC'
            ),
            'conditions' => array(
                'City.is_approved' => 1,
                'City.has_newsletter' => true
            ),
            'fields' => array(
                'City.id'
            ),
            'recursive' => - 1
        ));
        if (empty($cities))
            return;
        foreach ($cities as $city) {
            $this->_sendSubscriptionMailForCity($city['City']['id']);
        }
    }

    function _closeDeals($dealIds = array(), $closed = FALSE, $fecha = false) {
        $when = date('Y-m-d H:i:s');
        if ($fecha != false) {
            $when = date('Y-m-d H:i:s', strtotime($fecha));
        }
        $this->Behaviors->detach('Excludable');
        $conditions = array();
        if (empty($dealIds)) {
            $deals = $this->find('list', array(
                'conditions' => array(
                    'Deal.deal_status_id' => $closed ? ConstDealStatus::Closed : ConstDealStatus::Tipped,
                    'Deal.end_date <= ' => $when
                ),
                'fields' => array(
                    'Deal.id',
                    'Deal.id'
                )
            ));
            if (!empty($deals)) {
                $dealIds = array_keys($deals);
            }
        }
        if (!empty($dealIds)) {
            $conditions['Deal.id'] = $dealIds;
            $this->updateAll(array(
                'Deal.total_purchased_amount' => '(Deal.discounted_price * Deal.deal_user_count)',
                'Deal.total_commission_amount' => '(Deal.bonus_amount + ( Deal.total_purchased_amount * ( Deal.commission_percentage / 100 )))',
                'Deal.end_date' => '"' . $when . '"',
                'Deal.deal_status_id' => ConstDealStatus::Closed
                    ), $conditions);
        }
        App::import('Model', 'now.NowBranchesDeals');
        $this->NowBranchesDeals = new NowBranchesDeals;
        $this->NowBranchesDeals->updateAll(array(
            'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Closed
                ), array(
            'NowBranchesDeals.now_deal_id' => $dealIds
        ));
    }

    function _processOpenStatus($deal_id = null, $fecha = false) {
        App::import('Component', 'OauthConsumer');
        $this->OauthConsumer = & new OauthConsumerComponent();
        $conditions = array();
        if (is_null($deal_id)) {
            $conditions['Deal.deal_status_id'] = ConstDealStatus::Upcoming;
            $conditions['Deal.start_date <='] = date('Y-m-d H:i:s');
            if ($fecha != false) {
                $conditions['Deal.start_date <='] = $fecha;
            }
        } else {
            $conditions['Deal.id'] = $deal_id;
        }
        $deals = $this->find('all', array(
            'conditions' => array(
                $conditions
            ),
            'contain' => array(
                'City' => array(
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug',
                        'City.fb_api_key',
                        'City.fb_secret_key',
                        'City.fb_session_key',
                        'City.twitter_username',
                        'City.twitter_password',
                        'City.twitter_access_token',
                        'City.twitter_access_key',
                        'City.twitter_url',
                        'City.facebook_url'
                    )
                ),
                'Attachment'
            ),
            'recursive' => 1
        ));
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if (Configure::read('twitter.enable_twitter_post_open_deal') or Configure::read('facebook.enable_facebook_post_open_deal')) {
                    $twitter_access_token = (!empty($deal['City']['twitter_access_token'])) ? $deal['City']['twitter_access_token'] : Configure::read('twitter.site_user_access_token');
                    $twitter_access_key = (!empty($deal['City']['twitter_access_key'])) ? $deal['City']['twitter_access_key'] : Configure::read('twitter.site_user_access_key');
                    if (!empty($deal['City']['fb_api_key'])) {
                        $fb_api_key = $deal['City']['fb_api_key'];
                    } else {
                        $fb_api_key = Configure::read('facebook.fb_api_key');
                    }
                    if (!empty($deal['City']['fb_session_key'])) {
                        $fb_session_key = $deal['City']['fb_session_key'];
                    } else {
                        $fb_session_key = Configure::read('facebook.fb_session_key');
                    }
                    if (!empty($deal['City']['fb_secret_key'])) {
                        $fb_secret_key = $deal['City']['fb_secret_key'];
                    } else {
                        $fb_secret_key = Configure::read('facebook.fb_secrect_key');
                    }
                    $slug = $deal['Deal']['slug'];
                    $city_name = $deal['City']['name'];
                    if (empty($deal['Deal']['bitly_short_url_subdomain']) or empty($deal['Deal']['bitly_short_url_prefix'])) {
                        $this->_updateDealBitlyURL($slug);
                        $url = Configure::read('static_domain_for_mails') . '/deal/' . $slug;
                    } else {
                        if (Configure::read('site.city_url') == 'prefix')
                            $url = $deal['Deal']['bitly_short_url_prefix'];
                        else
                            $url = $deal['Deal']['bitly_short_url_subdomain'];
                    }
                    $message = strtr(Configure::read('twitter.new_deal_message'), array(
                        '##URL##' => $url,
                        '##DEAL_NAME##' => $deal['Deal']['name'],
                        '##SLUGED_SITE_NAME##' => Inflector::slug(strtolower(Configure::read('site.name')))
                    ));
                    if (Configure::read('twitter.enable_twitter_post_open_deal')) {
                        $xml = $this->OauthConsumer->post('Twitter', $twitter_access_token, $twitter_access_key, 'https://twitter.com/statuses/update.xml', array(
                            'status' => $message
                        ));
                    }
                }
            }
        }
    }

    function _updateDealBitlyURL($deal_slug, $city = '') {
        $bitly_short_url_prefix = Configure::read('static_domain_for_mails') . '/deal/' . $deal_slug;
        $bitly_short_url_prefix = $this->getBitlyUrl($bitly_short_url_prefix);
        $this->updateAll(array(
            'Deal.bitly_short_url_subdomain' => '\'' . $bitly_short_url_prefix . '\'',
            'Deal.bitly_short_url_prefix' => '\'' . $bitly_short_url_prefix . '\''
                ), array(
            'Deal.slug' => $deal_slug
        ));
    }

    function getBitlyUrl($url = null) {
        if (is_null($url)) {
            return false;
        }
        $ret = file_get_contents('http://api.bit.ly/shorten?version=2.0.1&longUrl=' . $url . '&login=' . Configure::read('bitly.username') . '&apiKey=' . Configure::read('bitly.api_key'));
        $result = json_decode($ret, true);
        if ($result['errorCode'] == '0' && $result['statusCode'] == 'OK') {
            return $result['results'][$url]['shortUrl'];
        } else {
            return $url;
        }
    }

    function getImageUrl($model, $attachment, $options) {
        $default_options = array(
            'dimension' => 'big_thumb',
            'class' => '',
            'alt' => 'alt',
            'title' => 'title',
            'type' => 'jpg'
        );
        $options = array_merge($default_options, $options);
        $image_hash = $options['dimension'] . '/' . $model . '/' . $attachment['id'] . '.' . md5(Configure::read('Security.salt') . $model . $attachment['id'] . $options['type'] . $options['dimension'] . Configure::read('site.name')) . '.' . $options['type'];
        return Router::url('/', true) . '/img/' . $image_hash;
    }

    function externalPayment($deal_id, $quantity, $user_id, $payment_type_id, $external_status, $used_points, $giftData, $inhibit_block = false, $options = array(), $buy_ip = null) {
        try {
            $gift_data = json_encode($giftData);
            $is_gift = $giftData['is_gift'] ? '1' : '0';
            $created = $updated = date('Y-m-d H:i:s');
            if (is_null($buy_ip)) {
                $buy_ip = RequestHandlerComponent::getClientIP();
            }
            $this->DealExternal->recursive = - 1;
            if (isset($options['payment_option_id'])) {
                $payment_option_id = $options['payment_option_id'];
                $this->DealExternal->set('payment_option_id', $payment_option_id);
            }
            if (isset($options['payment_plan_option_id'])) {
                $payment_plan_option_id = $options['payment_plan_option_id'];
                $this->DealExternal->set('payment_plan_option_id', $payment_plan_option_id);
            }
            if ($payment_type_id == ConstPaymentTypes::Wallet && !$inhibit_block) {
                $this->User->id = $user_id;
                $this->User->saveField('wallet_blocked', 1, $validate = false);
            }
            $this->unbindAll();
            if (!$this->updateAll(array(
                        'deal_external_count' => 'deal_external_count + ' . $quantity
                            ), array(
                        'Deal.id' => $deal_id
                    ))) {
                Debugger::log("externalPayment no se puede actualizar el deal_external_count para el deal_id: {$deal_id}", LOG_DEBUG);
            }
            $this->bindModel(array(
                'belongsTo' => array(
                    'User' => array(
                        'className' => 'User',
                        'foreignKey' => 'user_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => ''
                    ),
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'counterCache' => true
                    ),
                    'Company' => array(
                        'className' => 'Company',
                        'foreignKey' => 'company_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'counterCache' => true
                    ),
                    'DealStatus' => array(
                        'className' => 'DealStatus',
                        'foreignKey' => 'deal_status_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'counterCache' => true
                    ),
                    'Cluster' => array(
                        'className' => 'Cluster',
                        'foreignKey' => 'cluster_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'counterCache' => true
                    ),
                    'DealCategory' => array(
                        'className' => 'DealCategory',
                        'foreignKey' => 'deal_category_id',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'counterCache' => true
                    )
                ),
                'hasOne' => array(
                    'Attachment' => array(
                        'className' => 'Attachment',
                        'foreignKey' => 'foreign_id',
                        'conditions' => array(
                            'Attachment.class =' => 'Deal'
                        ),
                        'dependent' => true
                    )
                ),
                'hasMany' => array(
                    'PaymentOptionDeal' => array(
                        'className' => 'PaymentOptionDeal',
                        'foreignKey' => 'deal_id'
                    ),
                    'DealUser' => array(
                        'className' => 'DealUser',
                        'foreignKey' => 'deal_id',
                        'dependent' => true,
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'limit' => '',
                        'offset' => '',
                        'exclusive' => '',
                        'finderQuery' => '',
                        'counterQuery' => ''
                    ),
                    'Pin' => array(
                        'className' => 'Pin',
                        'foreignKey' => 'deal_id',
                        'dependent' => true
                    ),
                    'AttachmentMultiple' => array(
                        'className' => 'Attachment',
                        'foreignKey' => 'foreign_id',
                        'conditions' => array(
                            'AttachmentMultiple.class =' => 'DealMultiple'
                        ),
                        'dependent' => true
                    ),
                    'AnulledCoupon' => array(
                        'className' => 'AnulledCoupon',
                        'foreignKey' => 'deal_id',
                        'dependent' => true,
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'limit' => '',
                        'offset' => '',
                        'exclusive' => '',
                        'finderQuery' => '',
                        'counterQuery' => ''
                    ),
                    'DealExternal' => array(
                        'className' => 'DealExternal',
                        'foreignKey' => 'deal_id',
                        'dependent' => true
                    )
                )
            ));
            $ret = $this->DealExternal->save(compact('deal_id', 'quantity', 'user_id', 'payment_type_id', 'external_status', 'used_points', 'created', 'updated', 'is_gift', 'gift_data', 'buy_ip', 'payment_option_id'));
            $id = $this->DealExternal->getInsertID();
            if (isset($this->data['Deal']['points'])) {
                $this->User->debit_points($user_id, $id, $this->data['Deal']['points']);
            }
            $ret['DealExternal']['id'] = $id;
        } catch (Exception $e) {
            $this->getApiLoggerInstance()->error('Error al intentar registrar la compra.', array(
                'error' => $e->getMessage()
            ));
            throw $e;
        }
        return $ret;
    }

    function isBacOk($user_id, $deal_external_id, $portal = 0) {
        try {
            $dealExternal = $this->DealExternal->read(null, $deal_external_id);
            $deal = $this->read(null, $dealExternal['DealExternal']['deal_id']);
            $idPortal = $this->getPortalId($portal, $deal['Deal']['start_date']);
            $params = array(
                'idPortal' => $idPortal,
                'idUsuarioPortal' => $user_id,
                'idPagoPortal' => $deal_external_id
            );
            $this->ApiLogger->trace('consultando pago en bac', array(
                'parametros' => $params,
                'uri' => $this->consultarPagoBacUrl
            ));
            $sopa = new SoapClient($this->consultarPagoBacUrl);
            $ret = array(
                'status' => TRUE,
                'ret' => $sopa->consultarPago($params)
            );
        } catch (Exception $e) {
            $this->ApiLogger->trace('error al consultar pago en bac', array(
                'parametros' => $params,
                'uri' => $this->consultarPagoBacUrl,
                'exception' => $e
            ));
            $ret = array(
                'status' => FALSE,
                'ret' => $e
            );
        }
        $this->ApiLogger->trace('la consulta del pago en bac fue realizada', array(
            'parametros' => $params,
            'uri' => $this->consultarPagoBacUrl,
            'result' => $ret
        ));
        return $ret;
    }

    function getGateway($payment_type_id) {
        App::import('Model', 'PaymentType');
        $paymentType = new PaymentType();
        $gateway_id = $paymentType->findById($payment_type_id);
        return $gateway_id['PaymentType']['gateway_id'];
    }

    function makeBacSurrogate($de, $ag) {
        $bac_surrogate = new StdClass();
        $bac_surrogate->montoFinal = $bac_surrogate->montoParcial = (float) $ag;
        $bac_surrogate->interes = 0;
        $bac_surrogate->estado = 'ACREDITADO';
        return array(
            'status' => TRUE,
            'ret' => $bac_surrogate
        );
    }

    function isPaymentOk($user_id, $deal_external_id, $payment_type_id, $portal = 0) {
        $gateway_id = $this->getGateway($payment_type_id);
        $r = $this->isBacOk($user_id, $deal_external_id, $portal);
        return $r;
    }

    function isMpOk($user_id, $deal_external_id, $portal = 0) {
        $POSTopts = array(
            'acc_id' => Configure::read('MP.acc'),
            'sonda_key' => Configure::read('MP.snd'),
            'seller_op_id' => $deal_external_id
        );
        $POSTstrs = array();
        foreach ($POSTopts as $key => $value)
            $POSTstrs[] = $key . '=' . urlencode($value);
        $ch = curl_init(Configure::read('MP.son.url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CAINFO, Configure::read('CertificateBundlePath'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $POSTstrs));
        $output = curl_exec($ch);
        Debugger::log('isMpOk - Consultando a sonda:', LOG_DEBUG);
        Debugger::log('..url: ' . Configure::read('MP.son.url'), LOG_DEBUG);
        Debugger::log('..dealExternal id: ' . $deal_external_id, LOG_DEBUG);
        if (curl_error($ch) != '')
            Debugger::log('..error: ' . curl_error($ch), LOG_DEBUG);
        else
            Debugger::log('..output:\n' . $output, LOG_DEBUG);
        if (!$output)
            $ret = array(
                'status' => FALSE,
                'ret' => new Exception(curl_error($ch))
            );
        else {
            try {
                $xml = new SimpleXMLElement($output);
                $bac_surrogate = new StdClass();
                $bac_surrogate->montoFinal = $bac_surrogate->montoParcial = (float) $xml->operation->price;
                $bac_surrogate->interes = 0;
                switch ((string) $xml->operation->status) {
                    case 'A':
                        $bac_surrogate->estado = 'ACREDITADO';
                        break;

                    case 'C':
                        $bac_surrogate->estado = 'CANCELADO';
                        break;

                    default:
                        $bac_surrogate->estado = 'PENDIENTE';
                        break;
                }
                $ret = array(
                    'status' => TRUE,
                    'ret' => $bac_surrogate
                );
            } catch (Exception $e) {
                $ret = array(
                    'status' => FALSE,
                    'ret' => new Exception($e->getMessage())
                );
            }
        }
        curl_close($ch);
        return $ret;
    }

    function sendCreditedCupons() {
        Debugger::log(sprintf('sendCreditedCupons - Se van a levantar los Deals y se van a enviar los cupones no enviados de estas ofertas.'), LOG_DEBUG);
        $this->DealUser->recursive = 0;
        $dealUsers = $this->DealUser->find('all', array(
            'fields' => 'DealUser.id,DealUser.deal_id',
            'conditions' => array(
                'DealUser.emailed' => 0
            )
        ));
        if (isset($dealUsers) && !empty($dealUsers)) {
            foreach ($dealUsers as $du) {
                Debugger::log(sprintf('sendCreditedCupons - Deal:%u', $du['DealUser']['deal_id']), LOG_DEBUG);
                Debugger::log(sprintf('sendCreditedCupons - DealUser:%u', $du['DealUser']['id']), LOG_DEBUG);
                $this->processDealStatus($du['DealUser']['deal_id'], $du['DealUser']['id']);
            }
        }
    }

    function findDealExternalsForProcessDealStatus($processType) {
        $conditions = array(
            'DealExternal.external_status' => array(
                'P'
            )
        );
        if (array_key_exists($processType, $this->processDealStatusTypes)) {
            if (array_key_exists('statuses', $this->processDealStatusTypes[$processType]) && !empty($this->processDealStatusTypes[$processType]['statuses'])) {
                $conditions['DealExternal.external_status'] = $this->processDealStatusTypes[$processType]['statuses'];
            }
            if (array_key_exists('minAge', $this->processDealStatusTypes[$processType]) && !empty($this->processDealStatusTypes[$processType]['minAge'])) {
                $conditions['DealExternal.created <='] = date("Y-m-d H:i:s", strtotime('+' . $this->processDealStatusEpsilon, strtotime('-' . $this->processDealStatusTypes[$processType]['minAge'], strtotime(date("Y-m-d H:i:s")))));
            }
            if (array_key_exists('maxAge', $this->processDealStatusTypes[$processType]) && !empty($this->processDealStatusTypes[$processType]['maxAge'])) {
                $conditions['DealExternal.created >='] = date("Y-m-d H:i:s", strtotime('-' . $this->processDealStatusEpsilon, strtotime('-' . $this->processDealStatusTypes[$processType]['maxAge'], strtotime(date("Y-m-d H:i:s")))));
            }
        }
        return $this->DealExternal->find('all', array(
                    'conditions' => $conditions,
                    'contain' => array(
                        'User',
                        'Deal'
                    )
        ));
    }

    function cancelExpiryDealExternals($pendingExpiryDate) {
        $expiredDealExternals = $this->DealExternal->find('list', array(
            'fields' => array(
                'DealExternal.id',
                'DealExternal.user_id'
            ),
            'conditions' => array(
                'DealExternal.created <=' => $pendingExpiryDate,
                'DealExternal.external_status' => array(
                    'P'
                ),
                'not' => array(
                    'DealExternal.internal_amount' => 0.00
                )
            ),
            'recursive' => - 1
        ));
        if (empty($expiredDealExternals)) {
            $this->ApiLogger->notice("No se han encontrado Compras (DealExternals) expiradas.", array(
                'ExpiredDealExternals' => $expiredDealExternals,
                'ExpiryDate' => $pendingExpiryDate
            ));
            return false;
        }
        $userIdsToUnBlockWallet = array_unique(array_values($expiredDealExternals));
        $hasUnblockWallets = $this->User->updateAll(array(
            'User.wallet_blocked' => 0
                ), array(
            'User.id' => $userIdsToUnBlockWallet
        ));
        $this->ApiLogger->notice("Se han desbloqueados los monederos de los usuarios cuyas compras han expirado.", array(
            'expiryDate' => $pendingExpiryDate,
            'userIds updated' => $this->User->getAffectedRows(),
            'hasUnblockWallets' => $hasUnblockWallets,
            'userIds selected' => $userIdsToUnBlockWallet
        ));
        return $this->DealExternal->updateAll(array(
                    'DealExternal.external_status' => '\'C\'',
                    'DealExternal.cancellation_reason' => '\'Pending expiry\'',
                    'DealExternal.updated' => '\'' . date('Y-m-d H:i:s.u') . '\''
                        ), array(
                    'DealExternal.created <=' => $pendingExpiryDate,
                    'DealExternal.external_status' => array(
                        'P'
                    )
        ));
    }

    private function extractColumn($list, $modelName, $columnName) {
        $result = array();
        if (!empty($list)) {
            foreach ($list as $item) {
                $result[] = $item[$modelName][$columnName];
            }
        }
        return $result;
    }

    private function initializeDealController() {
        $dealsController = new DealsController();
        $dealsController->EmailTemplate = new EmailTemplate();
        $dealsController->Email = new EmailComponent();
        $dealsController->Email->delivery = Configure::read('site.email.delivery');
        $dealsController->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $dealsController->Deal = $this;
        $dealsController->Session = new SessionComponent();
        return $dealsController;
    }

    function stockCanBeDecreased($status) {
        return in_array($status, array(
            'ACREDITADO',
            'PENDIENTE'
        ));
    }

    function stockDecrease($dealId, $quantity, $userId) {
        $inventoryService = ProductInventoryServiceComponent::instance();
        $inventoryService->decrease($dealId, $quantity, $userId);
    }

    function UpdateExternalDealStatus($processType = '') {
        $this->ApiLogger = new ApiLogger(get_class($this) . '::UpdateExternalDealStatus', 'process_deal_status');
        $this->ApiLogger->notice('Iniciando Proceso, inicializando componentes', array(
            'processType' => $processType
        ));
        $this->Behaviors->detach('Excludable');
        App::import('Model', 'ProcessRun');
        App::import('Model', 'User');
        $processRun = new ProcessRun();
        $this->User = new User();
        $selfModel = $this;
        $dataSource = $selfModel->getDataSource();
        $dataSource->fullDebug = true;
        $this->DealExternal->recursive = 0;
        $dealsController = $this->initializeDealController();
        $view = new View($dealsController);
        $this->ApiLogger->notice('Componentes inicializados', array(
            'processType' => $processType
        ));
        $processRunName = 'UpdateExternalDealStatus';
        if ($processType != '') {
            $processRunName.= '_' . $processType;
        }
        $processRunStatus = $processRun->getRunningByName($processRunName);
        $this->ApiLogger->notice('Verificando estado de la ultima ejecuccion del proceso');
        if ($processRunStatus !== false && $processRunStatus['is_running'] && $processRunStatus['name'] != 'LOCK' && $processRunStatus['modified'] <= $this->runningExpiryDate) {
            $this->ApiLogger->notice('La ultima ejecuccion del proceso aun se encuentra running y lockeada, se cancela su ejecucion.', $processRunStatus);
            $dataSource->begin($selfModel);
            $processRun->stopRunning($processRunStatus['id']);
            $this->ApiLogger->notice('COMMIT de los cambios.');
            $dataSource->commit($selfModel);
            $processRunStatus['is_running'] = false;
            $this->ApiLogger->notice('La ultima ejecuccion del proceso fue cancelada.', $processRunStatus);
        }
        if ($processRunStatus['is_running']) {
            $this->ApiLogger->notice('La ultima ejecuccion del proceso aun esta corriendo normalmente.', $processRunStatus);
            return false;
        } else {
            $this->ApiLogger->notice('La ultima ejecuccion del proceso esta finalizada, se genera una nueva ejecuccion del proceso.');
            $dataSource->begin($selfModel);
            $processRunId = $processRun->startRunning($processRunName);
            $this->ApiLogger->put('pid', $processRunId);
            $this->ApiLogger->notice('Nueva Ejecucion del proceso generada, COMMIT de los cambios.', $processRunId);
            $dataSource->commit($selfModel);
        }
        $hasCancelled = $this->cancelExpiryDealExternals($this->pendingExpiryDate);
        if ($hasCancelled) {
            $this->ApiLogger->notice("Se cancelaron Compras expiradas.", array(
                'HasCancelled' => $hasCancelled,
                'ExpiryDate' => $this->pendingExpiryDate,
                'Compras (dealExternalIds)' => $this->DealExternal->getAffectedRows(),
                'CantidadDeComprasCanceladasPorExpiracion' => count($this->DealExternal->getAffectedRows())
            ));
        }
        $dealExternals = $this->findDealExternalsForProcessDealStatus($processType);
        if (empty($dealExternals)) {
            $this->ApiLogger->info('No se encontraron Compras (DealExternals) pendientes a procesar.');
        }
        $this->ApiLogger->info('Compras (DealExternals) a procesar.', array(
            'Compras (DealExternalIds)' => $this->extractColumn($dealExternals, 'DealExternal', 'id'),
            'CantidadDeComprasAProcesar' => count($dealExternals)
        ));
        $i = 0;
        $errorCount = 0;
        if (!empty($dealExternals)) {
            foreach ($dealExternals as $ext) {
                $i = $i + 1;
                $this->ApiLogger->put('#', $i);
                $this->ApiLogger->put('dealExternalId', $ext['DealExternal']['id']);
                $this->ApiLogger->notice('Procesando la Compra ' . $ext['DealExternal']['id'], $ext['DealExternal']);
                $dataSource->begin($selfModel);
                $ext['Deal']['deal_id'] = $ext['DealExternal']['deal_id'];
                $ext['Deal']['quantity'] = $ext['DealExternal']['quantity'];
                $ext['Deal']['is_gift'] = $ext['DealExternal']['is_gift'];
                $ext['Deal']['gift_data'] = json_decode($ext['DealExternal']['gift_data'], true);
                $ext['Deal']['payment_type_id'] = $ext['DealExternal']['payment_type_id'];
                $ext['Deal']['user_id'] = $ext['DealExternal']['user_id'];
                $deal = array(
                    'Deal' => $ext['Deal']
                );
                $isOk = $this->isPaymentOk($ext['DealExternal']['user_id'], $ext['DealExternal']['id'], $ext['DealExternal']['payment_type_id'], $deal['Deal']['is_tourism']);
                if (!$isOk['status']) {
                    $this->ApiLogger->error('BAC respondio con una excepcion.', $isOk['ret']->getMessage());
                    $errorCount = $errorCount + 1;
                    if ((stripos($isOk['ret']->getMessage(), 'TransaccionService.usuarioInexistente') !== false) || (stripos($isOk['ret']->getMessage(), 'TransaccionService.portalClienteInexistente') !== false) || (stripos($isOk['ret']->getMessage(), 'TransaccionService.pagoInexistente') !== false)) {
                        $this->ApiLogger->notice('Cancelando Deal External.');
                        $ext['DealExternal']['external_status'] = 'C';
                        $ext['DealExternal']['cancellation_reason'] = $isOk['ret']->getMessage();
                        $ext['DealExternal']['updated'] = date('Y-m-d H:i:s.u');
                        if (!$this->DealExternal->save($ext['DealExternal'])) {
                            $this->ApiLogger->error('No se pudo cancelar el Deal External. ROLLBAC de cambios');
                            $errorCount = $errorCount + 1;
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('Deal External fue cancelado exitosamente.');
                        if ($ext['DealExternal']['internal_amount']) {
                            $this->ApiLogger->notice('La compra fue realizado con monedero, desbloqueando monedero del usuario.', $ext['DealExternal']);
                            if (!$this->User->updateAll(array(
                                        'User.wallet_blocked' => 0
                                            ), array(
                                        'User.id' => $ext['DealExternal']['user_id']
                                    ))) {
                                $this->ApiLogger->error('No se pudo desbloquear el monedero. ROLLBAC de los cambios.');
                                $errorCount = $errorCount + 1;
                                $dataSource->rollback($selfModel);
                            }
                            $this->ApiLogger->notice('Monedero del usuario desbloqueado.');
                        }
                        $this->ApiLogger->notice('COMMIT de los cambios.');
                        $dataSource->commit($selfModel);
                    } else {
                        $this->ApiLogger->error('Bac responde con una Exception no conocida por no clubcupon.', $isOk['ret']->getMessage());
                        $errorCount = $errorCount + 1;
                        $this->ApiLogger->notice('ROLLBAC de los cambios.');
                        $dataSource->rollback($selfModel);
                    }
                    continue;
                }
                $status = $isOk['ret']->estado;
                if ($isOk['status'] && empty($ext['DealExternal']['bac_id'])) {
                    $this->ApiLogger->notice('Actualizando el campo bac_id del deal_external', array(
                        'dealExternal' => $ext,
                        'bacResponse' => $isOk
                    ));
                    $this->DealExternal->id = $ext['DealExternal']['id'];
                    $ext['DealExternal']['bac_id'] = $isOk['ret']->idPago;
                    if ($this->DealExternal->saveField('bac_id', $ext['DealExternal']['bac_id'])) {
                        if ($this->stockCanBeDecreased($status)) {
                            $this->stockDecrease($ext['DealExternal']['deal_id'], $ext['DealExternal']['quantity'], $ext['DealExternal']['user_id']);
                        }
                        $this->ApiLogger->notice('Campo bac_id del deal_external actualizado exitosamente', array(
                            'dealExternal' => $ext['DealExternal'],
                            'bacResponse' => $isOk
                        ));
                    } else {
                        $errorCount = $errorCount + 1;
                        $this->ApiLogger->error('Error al intentar actualiza el campo bac_id del deal_external', array(
                            'dealExternal' => $ext['DealExternal'],
                            'bacResponse' => $isOk
                        ));
                    }
                }
                if (isset($isOk['ret']->estadoDescripcionGateway)) {
                    $ext['DealExternal']['bac_substate'] = substr($isOk['ret']->estadoDescripcionGateway, 0, 63);
                    $this->ApiLogger->notice('Actualizando el campo bac_substate del deal_external', array(
                        'dealExternal' => $ext['DealExternal'],
                        'bacResponse' => $isOk
                    ));
                    if ($this->DealExternal->saveField('bac_substate', $ext['DealExternal']['bac_substate'])) {
                        $this->ApiLogger->notice('Campo bac_substate del deal_external actualizado exitosamente', $ext['DealExternal']);
                    } else {
                        $errorCount = $errorCount + 1;
                        $this->ApiLogger->error('Error al intentar actualiza el campo bac_substate del deal_external', $ext['DealExternal']);
                    }
                }
                if (in_array($status, array(
                            'CANCELADO',
                            'ACREDITADO',
                            'PENDIENTE',
                            'ANULADO',
                            'EXPIRADO',
                            'ANULADO_INICIADO'
                        ))) {
                    $ext['DealExternal']['bac_id'] = $isOk['ret']->idPago;
                    if ($status != 'PENDIENTE') {
                        $this->ApiLogger->notice('El pago se encuentra en un estado distinto de PENDIENTE, forzando el desbloqueo del monedero del usuario.', array(
                            'userId' => $ext['DealExternal']['user_id'],
                            'status' => $status
                        ));
                        $this->User->id = $ext['DealExternal']['user_id'];
                        $this->User->saveField('wallet_blocked', 0, $validate = false);
                    }
                    if ($status == 'CANCELADO' || $status == 'ANULADO' || $status == 'EXPIRADO' || $status == 'ANULADO_INICIADO') {
                        $this->ApiLogger->notice('El pago se encuentra en un estado cancelatorio, cancelando la compra (deal_external).', array(
                            'status' => $status
                        ));
                        $ext['DealExternal']['external_status'] = 'C';
                        $ext['DealExternal']['updated'] = date('Y-m-d H:i:s');
                        if (!$this->DealExternal->save($ext['DealExternal'])) {
                            $this->ApiLogger->error('No se pudo cancelar la compra (deal_external). ROLLBAC de cambios');
                            $errorCount = $errorCount + 1;
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('Compra (deal_external) cancelada exitosamente.');
                        if ($this->hasGiftPin($ext)) {
                            $this->unBlockGiftPin($ext);
                        }
                    } else if ($status == 'ACREDITADO') {
                        $this->ApiLogger->notice('El pago se encuentra ACREDITADO, depositando saldo al monedero del usuario.', array(
                            'userId' => $ext['DealExternal']['user_id'],
                            'montoParcial' => $isOk['ret']->montoParcial
                        ));
                        $ret = $this->addAmountToWalletFromBac($isOk['ret'], $ext, $ext['DealExternal']['user_id'], 'add amount to wallet from cron job');
                        $this->addAmountToWalletIfDealHasDiscount($ext);
                        if ($this->hasGiftPin($ext)) {
                            $giftPinCode = $ext['DealExternal']['gift_pin_code'];
                            $this->addAmountToWalletIfHasGiftPin($ext['DealExternal']);
                            $this->GiftPinService->burnGiftPin($giftPinCode, $ext['DealExternal']['user_id']);
                            $this->ApiLogger->notice('Compra tiene Pin de Descuento: ' . $ext['DealExternal']['gift_pin_code'] . ', depositá saldo al monedero del usuario.');
                        }
                        if (!$ret) {
                            $errorCount = $errorCount + 1;
                            $this->ApiLogger->error('No se pudo depositar los montos al monedero del usuario. ROLLBAC de cambios.', array(
                                'userId' => $user_id
                            ));
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('Pasando a estado Acreditado la compra (deal_external).', $ext['DealExternal']);
                        $ext['DealExternal']['external_status'] = 'A';
                        $ext['DealExternal']['updated'] = date('Y-m-d H:i:s');
                        if (!$this->DealExternal->save($ext['DealExternal'])) {
                            $errorCount = $errorCount + 1;
                            $this->ApiLogger->error('No se pudo pasar a estado Acreditado la compra (deal_external). ROLLBAC de cambios.', $ext['DealExternal']);
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('Se paso a la compra (deal_external) a estado Acreditado exitosamente.', $ext['DealExternal']);
                        $this->ApiLogger->notice('Complentando la Compra, generando Cupones....');
                        $ret = $dealsController->_buyDeal($ext, FALSE);
                        if (!$ret) {
                            $errorCount = $errorCount + 1;
                            $this->ApiLogger->error('Error al intentar Complentar la Compra (_buyDeal). ROLLBAC de cambios.', $ext['DealExternal']);
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('La Compra se Completo exitosamente, se generaron los cupones correspondientes.', $ext['DealExternal']);
                    } else if ($status == 'PENDIENTE') {
                        $this->ApiLogger->notice('El pago se encuentra PENDIENTE, pasando a Pendiente a la compra (deal_external).', array(
                            'dealExternal' => $ext['DealExternal'],
                            'status' => $status
                        ));
                        $ext['DealExternal']['external_status'] = 'P';
                        $ext['DealExternal']['updated'] = date('Y-m-d H:i:s');
                        if (!$this->DealExternal->save($ext['DealExternal'])) {
                            $errorCount = $errorCount + 1;
                            $this->ApiLogger->error('No se pudo pasar a estado Pendiente la Compra. ROLLBAC de cambios.', $ext['DealExternal']);
                            $dataSource->rollback($selfModel);
                            continue;
                        }
                        $this->ApiLogger->notice('Se paso a la Compra a estado Pendiente exitosamente.', $ext['DealExternal']);
                    }
                }
                $this->ApiLogger->notice('La Compra ' . $ext['DealExternal']['id'] . ' procesada exitosamente. COMMIT de los Cambios', $ext['DealExternal']);
                $dataSource->commit($selfModel);
                $this->ApiLogger->delete('#');
                $this->ApiLogger->delete('dealExternalId');
            }
        }
        $this->ApiLogger->info('Compras (DealExternals) procesadas en su totalidad.', array(
            'Compras (DealExternalIds)' => $this->extractColumn($dealExternals, 'DealExternal', 'id'),
            'CantidadDeComprasProcesadas' => count($dealExternals),
            'CantidadDeErrores' => $errorCount
        ));
        $dataSource->begin($selfModel);
        $processRun->stopRunning($processRunId);
        $dataSource->commit($selfModel);
        $this->ApiLogger->info('Proceso apagado.', array(
            "ProcessRunId" => $processRunId
        ));
    }

    function hasGiftPin($ext) {
        return !empty($ext['DealExternal']['gift_pin_code']);
    }

    function addAmountToWalletFromBac($bacRet, &$ext, $userId, $description = false) {
        $ext['DealExternal']['parcial_amount'] = $bacRet->montoParcial;
        $ext['DealExternal']['final_amount'] = $ext['DealExternal']['parcial_amount'] + $ext['DealExternal']['internal_amount'] + $ext['DealExternal']['discounted_amount'] + $ext['DealExternal']['gifted_amount'];
        $ret = $this->DealExternal->save(array(
            'id' => $ext['DealExternal']['id'],
            'parcial_amount' => $ext['DealExternal']['partial_amount'],
            'final_amount' => $ext['DealExternal']['final_amount']
        ));
        if (!$ret) {
            Debugger::log(sprintf('addAmountToWalletFromBac - Error no se pudo actualizar el parcial_amount y el final_amount. %s', debugEncode($bacRet)), LOG_DEBUG);
            return false;
        }
        Debugger::log(sprintf('addAmountToWalletFromBac - Actualizados el parcial_amount y el final_amount. %s', debugEncode($bacRet)), LOG_DEBUG);
        return $this->addAmountToWallet($bacRet->montoFinal - $bacRet->interes, $userId, $description);
    }

    function addAmountToWallet($amount, $userId, $description = false) {
        $data = array(
            'user_id' => $userId,
            'foreign_id' => ConstUserIds::Admin,
            'class' => 'SecondUser',
            'amount' => $amount,
            'description' => $description ? $description : 'added amount to wallet ' . __METHOD__,
            'gateway_fees' => 0,
            'transaction_type_id' => ConstTransactionTypes::AddedToWallet
        );
        $transaction_id = $this->User->Transaction->log($data);
        return $this->User->updateAll(array(
                    'User.available_balance_amount' => 'User.available_balance_amount + ' . $amount
                        ), array(
                    'User.id' => $userId
        ));
    }

    function updateDealExternalPaymentType($id, $payment_type_id, $user_available_amount) {
        $this->DealExternal->recursive = 0;
        $this->DealExternal->id = $id;
        $ext = $this->DealExternal->read(NULL, $id);
        if ($ext === FALSE) {
            return false;
        }
        $ext['DealExternal']['payment_type_id'] = $payment_type_id;
        $ext['DealExternal']['updated'] = date('Y-m-d H:i:s');
        $ext['DealExternal']['internal_amount'] = $user_available_amount;
        if (!$this->DealExternal->save($ext['DealExternal'])) {
            return false;
        }
        return true;
    }

    function bacLiquidarEmpresa($empresa_id, $cantidad, $monto, $porcentajeComision, $unitario, $deal_id, $portal = 0) {
        $sopa = new SoapClient(Configure::read('BAC.wsdl.liquidarEmpresa'));
        $this->Company->recursive = - 1;
        $company = $this->Company->read(NULL, $empresa_id);
        if ($company['Company']['is_end_user'])
            return true;
        $this->Company->User->recursive = - 1;
        $user = $this->Company->User->read(NULL, $company['Company']['user_id']);
        $deal = $this->read(null, $deal_id);
        $ret = $this->Company->createBacUserCompany($company, $user, $portal, $deal['Deal']['start_date']);
        $idPortal = $this->getPortalId($portal, $deal['Deal']['start_date']);
        $idProductComision = $this->getProductIdComision($portal, $deal['Deal']['start_date']);
        if (!$ret) {
            Debugger::log('bacLiquidarEmpresa Fallo al intentar crear/modificar usuario en BAC para Empresa. No se sigue adelante con la liquidación de empresa_id: ' . debugEncode($empresa_id), LOG_DEBUG);
            return FALSE;
        }
        $params = array(
            'idPortal' => $idPortal,
            'idUsuarioPortal' => $company['Company']['user_id'],
            'idProducto' => $idProductComision,
            'cantidad' => $cantidad,
            'montoTotal' => $monto,
            'porcentajeComision' => $porcentajeComision,
            'precioUnitario' => $unitario,
            'idMoneda' => Configure::read('BAC.pesos'),
            'idComprobantePortal' => $deal_id
        );
        Debugger::log('bacLiquidarEmpresa Enviando params a WS: ' . debugEncode($params), LOG_DEBUG);
        $fecha = date('Y-m-d H:i:s');
        try {
            $ret = $sopa->liquidarEmpresa($params);
        } catch (Exception $e) {
            $ret = FALSE;
            Debugger::log('ERROR bacLiquidarEmpresa @ ' . $fecha . ' Exception: ' . $e->getMessage(), LOG_DEBUG);
        }
        Debugger::log('END bacLiquidarEmpresa @ ' . $fecha, LOG_DEBUG);
        return $ret;
    }

    function is_subdeal($data = null) {
        if (empty($data)) {
            $data = $this->data;
        }
        $is_subdeal = isset($data['Deal']['id']) && isset($data['Deal']['parent_deal_id']) && ($data['Deal']['parent_deal_id'] != $data['Deal']['id']);
        return $is_subdeal;
    }

    function moreActions($key) {
        $moreActionsArray = array(
            ConstDealStatus::Upcoming => array(
                ConstDealStatus::Open => 'Abierto',
                ConstDealStatus::Canceled => 'Cancelada',
                ConstDealStatus::Rejected => 'Rechazada'
            ),
            ConstDealStatus::Tipped => array(
                ConstDealStatus::Closed => 'Cerrada'
            ),
            ConstDealStatus::Canceled => array(
                ConstDealStatus::Open => 'Abierto'
            ),
            ConstDealStatus::PendingApproval => array(
                ConstDealStatus::Upcoming => 'Proxima',
                ConstDealStatus::Rejected => 'Rechazada'
            ),
            ConstDealStatus::Rejected => array(
                ConstDealStatus::Upcoming => 'Proxima',
                ConstDealStatus::Open => 'Abierta',
                ConstDealStatus::Canceled => 'Cancelada'
            ),
            ConstDealStatus::Draft => array(
                ConstDealStatus::Canceled => 'Cancelada',
                ConstDealStatus::Rejected => 'Rechazada'
            ),
            ConstDealStatus::Closed => array(
                ConstDealStatus::TotallyPaid => 'Totalmente Pagada'
            ),
            ConstDealStatus::Open => array(
                ConstDealStatus::Closed => 'Cerrada'
            )
        );
        if (array_key_exists($key, $moreActionsArray)) {
            return $moreActionsArray[$key];
        } else {
            return array();
        }
    }

    function __findDealsForAdminIndex($options = array()) {
        $the_options = array(
            'fields' => array(
                '*',
                '(select username from users where users.id = Deal.seller_id) as seller_username',
                '(select first_name from user_profiles where user_profiles.user_id = Deal.seller_id) as seller_first_name',
                '(select last_name  from user_profiles where user_profiles.user_id = Deal.seller_id) as seller_last_name',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and deal_users.paid_date is not null and deal_users.deleted = 0) as `paid_coupons`',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and deal_users.paid_date is null and deal_users.deleted = 0) as `unpaid_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'A\') as `sold_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and deal_externals.external_status = \'P\' and ifnull(deal_externals.bac_substate,\'{null}\') not in (\'uninitialized\')) as `pending_coupons`',
                '(select sum(deal_externals.quantity) from deal_externals where deal_externals.deal_id = Deal.id and ((deal_externals.external_status = \'A\') or (deal_externals.external_status = \'P\' and ifnull(deal_externals.bac_substate,\'{null}\') not in (\'uninitialized\')))) as `total_coupons`',
                '(select count(1) from anulled_coupons where anulled_coupons.deal_id = Deal.id ) as `anulled_coupons`',
                '(select count(1) from deal_users where deal_users.deal_id = Deal.id and is_used = 1 ) as `used_coupons`',
                '(select sum(redemptions.discounted_price) from deal_users, redemptions where deal_users.deal_id = Deal.id and deal_users.id = redemptions.deal_user_id and is_used = 1 ) as `redeemed_amount`'
            ),
            'contain' => array(
                'User' => array(
                    'UserAvatar'
                ),
                'City',
                'DealStatus',
                'DealUser',
                'Company' => array(
                    'City',
                    'State'
                ),
                'Attachment'
            ),
            'order' => array(
                'Deal.id' => 'desc'
            )
        );
        return $this->find('all', array_merge($the_options, $options));
    }

    function __findDealsForAdminIndexCount($options = array()) {
        $the_options = array();
        return $this->find('count', array_merge($the_options, $options));
    }

    function __findFinalizedDealsForAdminIndex($options = array()) {
        $today = date("Y-m-d");
        $the_options = array(
            'conditions' => array(
                'Deal.deal_status_id' => ConstDealStatus::PaidToCompany,
                'Deal.coupon_expiry_date <=' => $today
            )
        );
        return $this->__findDealsForAdminIndex(array_merge($the_options, $options));
    }

    function __findFinalizedDealsForAdminIndexCount($options = array()) {
        $today = date("Y-m-d");
        $options = array(
            array(
                'Deal.coupon_expiry_date <=' => $today
            )
        );
        return $this->__findDealsForAdminIndexCount(ConstDealStatus::PaidToCompany, $options);
    }

    function __findEndedDealsForAdminIndex($options = array()) {
        $today = date("Y-m-d");
        $options = array(
            'Deal.end_date <=' => $today
        );
        return $this->__findDealsForAdminIndex('*', $options);
    }

    function __findEndedDealsForAdminIndexCount($options = array()) {
        return $this->__findDealsForAdminIndexCount(ConstDealStatus::Open, $options);
    }

    function countUserDeals($deal_id, $user_id) {
        $deals_count = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.deal_id' => $deal_id,
                'DealUser.user_id' => $user_id
            ),
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        return $deals_count['0']['total_count'];
    }

    function findAllOpenOrTippedDealsByCityOrGroupId($cityOrGroupId) {
        $not_conditions = array();
        $conditions = array();
        $conditions['Deal.city_id'] = $cityOrGroupId;
        $conditions['Deal.is_tourism'] = false;
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Open,
            ConstDealStatus::Tipped
        );
        $conditions[] = 'Deal.parent_deal_id = Deal.id';
        return $this->find('all', array(
                    'conditions' => array(
                        $conditions
                    ),
                    'contain' => array(
                        'Company',
                        'City',
                        'Attachment'
                    ),
                    'recursive' => 1,
                    'limit' => Configure::read('secondaryDeals.lowerDealsLimit'),
                    'order' => array(
                        'Deal.priority' => 'DESC',
                        'Deal.deal_status_id' => 'ASC',
                        'Deal.start_date' => 'ASC',
                        'Deal.id' => 'ASC'
                    )
        ));
    }

    public function findDealBySlug($slug) {
        $deal = $this->find('first', array(
            'fields' => array(
                '*',
                '`deal_external_count` as `total_coupons`'
            ),
            'conditions' => array(
                'Deal.slug = ' => $slug
            ),
            'contain' => array(
                'User',
                'Company' => array(
                    'City',
                    'State',
                    'Country',
                    'User'
                ),
                'Attachment',
                'AttachmentMultiple',
                'City'
            )
        ));
        return $deal;
    }

    public function findDealById($id) {
        $deal = $this->find('first', array(
            'fields' => array(
                '*',
                '`deal_external_count` as `total_coupons`'
            ),
            'conditions' => array(
                'Deal.id = ' => $id
            ),
            'recursive' => - 1
        ));
        return $deal;
    }

    public function findDealFieldsForAccountingEventService($id) {
        $deal = $this->find('first', array(
            'fields' => array(
                'Deal.id',
                'Deal.company_id'
            ),
            'conditions' => array(
                'Deal.id = ' => $id
            ),
            'recursive' => - 1
        ));
        return $deal;
    }

    function hasSubdeals($dealId = null) {
        if ($dealId == null || $dealId == false) {
            Debugger::log("dealId is null or false on " . __METHOD__, LOG_DEBUG);
        }
        $deals_count = $this->find('count', array(
            'conditions' => array(
                'Deal.parent_deal_id = ' => $dealId
            )
        ));
        return $deals_count > 1 ? true : false;
    }

    function findById($deal_id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Deal.id' => $deal_id
                    ),
                    'recursive' => - 1
        ));
    }

    function _validateTicketPortalDealUrl() {
        if ($this->data[$this->name]['is_ticketportal_deal'] == 1 && (empty($this->data[$this->name]['ticketportal_deal_url']) || !$this->_isValidURL($this->data[$this->name]['ticketportal_deal_url']))) {
            return false;
        }
        return true;
    }

    function _validateTicketPortalDealPin() {
        if ($this->data[$this->name]['is_ticketportal_deal'] == 1 && empty($this->data[$this->name]['ticketportal_deal_pin'])) {
            return false;
        }
        return true;
    }

    function _validatePrices() {
        return ($this->data[$this->name]['publication_channel_type_id'] == Configure::read('publication_channel.sms') || ($this->data[$this->name]['original_price'] >= $this->data[$this->name]['discounted_price']));
    }

    function _validateOriginalPrice() {
        if ($this->data[$this->name]['publication_channel_type_id'] == Configure::read('publication_channel.web') && (empty($this->data[$this->name]['original_price']) || $this->data[$this->name]['original_price'] <= 0)) {
            return false;
        }
        return true;
    }

    function _validateMinLengthRequisitionNumber() {
        $isValid = true;
        if ($this->data[$this->name]['deal_trade_agreement_id'] == 2) {
            $isValid = $this->data[$this->name]['requisition_number'] >= 100;
        }
        return $isValid;
    }

    function _validateSmsDeal() {
        if ($this->data[$this->name][''] == 'WEB') {
            
        }
    }

    function _isValidURL($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

    function isFinalizedOrClosed($deal) {
        return $deal['deal_status_id'] == ConstDealStatus::Finalized || $deal['deal_status_id'] == ConstDealStatus::Closed;
    }

    function usesStockControlStrategy($deal) {
        return $deal['product_inventory_strategy_id'] == 2;
    }

    function returnOpenUpcomingOrTippedCountByProductId($id) {
        $conditions = array(
            'Deal.product_product_id' => $id,
            'Deal.product_inventory_strategy_id' => 2,
            'Deal.deal_status_id' => array(
                ConstDealStatus::Open,
                ConstDealStatus::Upcoming,
                ConstDealStatus::Tipped
            )
        );
        return $this->find('count', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function returnOpenUpcomingOrTippedCountByCampaignId($id) {
        $conditions = array(
            'Deal.product_campaign_id' => $id,
            'Deal.product_inventory_strategy_id' => 2,
            'Deal.deal_status_id' => array(
                ConstDealStatus::Open,
                ConstDealStatus::Upcoming,
                ConstDealStatus::Tipped
            )
        );
        return $this->find('count', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function _isEqualPinsAndStock($deal_id) {
        $result = true;
        App::import('Model', 'Pin');
        $this->Pin = & new Pin();
        $stock = !empty($this->data['Deal']['max_limit']) ? $this->data['Deal']['max_limit'] : time();
        if ($this->Pin->countPinsByDeal($deal_id) < $stock && $stock != 0) {
            $result = false;
        }
        return $result;
    }

    function _checkHasPins() {
        $result = true;
        if (!empty($this->data['Deal']['has_pins'])) {
            if ((!empty($this->data['Deal']['parent_deal_id'])) && empty($this->data['Deal']['save_as_draft']) && (!empty($this->data['Deal']['id']))) {
                return $this->_isEqualPinsAndStock($this->data['Deal']['id']);
            }
        }
        return $result;
    }

    function _checkCustomCompanyAddress1() {
        if ($this->data['Deal']['is_shipping_address'] == 1) {
            return true;
        } elseif ($this->data['Deal']['is_shipping_address'] == 0 && empty($this->data['Deal']['custom_company_address1'])) {
            return false;
        }
        return true;
    }

    function _checkifCampaignIsSelectedBySelectedInventoryStrategy() {
        if ($this->data['Deal']['product_inventory_strategy_id'] == 2) {
            return $this->data['Deal']['product_campaign_id'] != '';
        }
        if ($this->data['Deal']['product_inventory_strategy_id'] == 1) {
            return true;
        }
    }

    function _checkifProductIsSelectedBySelectedInventoryStrategy() {
        if ($this->data['Deal']['product_inventory_strategy_id'] == 2) {
            return $this->data['Deal']['product_product_id'] != '';
        }
        if ($this->data['Deal']['product_inventory_strategy_id'] == 1) {
            return true;
        }
    }

    function _sendExclusiveMailToUserOnPinRequest($email) {
        App::import('Component', 'Email');
        $this->Email = new EmailComponent();
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate();
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $vars = array();
        $vars['SITE_LINK'] = Configure::read('static_domain_for_mails');
        $vars['SITE_NAME'] = Configure::read('site.name');
        $vars['CONTACT_US'] = Configure::read('static_domain_for_mails') . Router::url(array(
                    'controller' => 'contacts',
                    'action' => 'add',
                    'admin' => false
                        ), false);
        $vars['ABSOLUTE_IMG_PATH'] = Configure::read('static_domain_for_mails') . '/img/email/exclusive/';
        $vars['USER_EMAIL'] = $email;
        $vars['USER_IP'] = RequestHandlerComponent::getClientIP();
        $vars['DATE'] = date("d - m - y");
        $vars['TIME'] = date('h : i : s A');
        $vars['UNSUBSCRIBE_LINK'] = '#';
        (string) $cuerpo_del_mail = $this->EmailTemplate->getContentDynamicTemplate('Exclusive Email Template', $vars);
        $this->Email->to = $email;
        $this->Email->from = Configure::read('EmailTemplate.from_email');
        $this->Email->replyTo = Configure::read('EmailTemplate.reply_to_email');
        $this->Email->subject = Configure::read('ExclusiveEmailTemplate.subject');
        $this->Email->sendAs = 'html';
        if ($this->Email->send($cuerpo_del_mail)) {
            Debugger::log('_sendExclusiveMailToUserOnPinRequest Email enviado a:' . $email . ' ' . LOG_DEBUG);
            return true;
        } else {
            Debugger::log('_sendExclusiveMailToUserOnPinRequests Email no pudo ser enviado a:' . $email . ' ' . LOG_DEBUG);
            return false;
        }
    }

    public function isNow($deal) {
        return $deal['Deal']['is_now'];
    }

    function findAllOpenOrTippedAndNotCorporate($city_id) {
        $not_conditions = array();
        $conditions = array(
            'City.is_approved' => 1,
            'City.is_hidden' => 0,
            'City.has_newsletter' => 1,
            'City.deal_count >=' => 1
        );
        if (!empty($city_id)) {
            $conditions['Deal.city_id'] = $city_id;
        }
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Open,
            ConstDealStatus::Tipped
        );
        $conditions[] = 'Deal.parent_deal_id = Deal.id';
        $not_conditions['Not']['Deal.deal_status_id'] = array(
            ConstDealStatus::PendingApproval,
            ConstDealStatus::Upcoming,
            ConstDealStatus::Closed,
            ConstDealStatus::CicleEnded
        );
        foreach (Configure::read('corporate.city_name') as $key => $corporate_city_slug) {
            $corporate_city_ids[$key] = $this->City->findBySlug($corporate_city_slug);
        }
        $not_conditions['Not']['Deal.city_id'] = $corporate_city_ids;
        $deals = $this->find('all', array(
            'conditions' => array(
                $conditions,
                $not_conditions
            ),
            'contain' => array(
                'Company',
                'City',
                'Attachment'
            ),
            'recursive' => 1,
            'order' => array(
                'Deal.deal_status_id' => 'ASC'
            )
        ));
        return $deals;
    }

    function getDiscountMP($discount_price, $quantity) {
        $total = Precision::mul($discount_price, $quantity);
        $discount = Precision::mul($total, ((float) Configure::read('deal.discount_mp')));
        if ($discount > ((float) Configure::read('deal.limit_discount_mp'))) {
            $discount = Configure::read('deal.limit_discount_mp');
        }
        return $discount;
    }

    private function unBlockGiftPin($dealExternal) {
        Debugger::log(__METHOD__ . ':: Desbloqueando gift_pin_code', LOG_DEBUG);
        return $this->GiftPinService->unBlock($dealExternal['gift_pin_code']);
    }

    private function addAmountToWalletIfHasGiftPin($dealExternal) {
        $this->ApiLogger->notice('Depositando monto del codigo de pin en Monedero');
        $giftPinCode = $dealExternal['gift_pin_code'];
        $this->GiftPinService->unblock($giftPinCode);
        if ($giftPinCode) {
            $bacPin = $this->GiftPinService->isUsable($giftPinCode);
        }
        if (!$bacPin) {
            $this->ApiLogger->error('No es usable, pincode: ' . $dealExternal['gift_pin_code'], $dealExternal);
        } else {
            $giftAvailableAmount = $bacPin['monto'];
            $this->addAmountToWallet($giftAvailableAmount, $dealExternal['user_id'], 'add amount to wallet from discount');
        }
    }

    function addAmountToWalletIfDealHasDiscount($dealExternal) {
        $discount = $this->findById($dealExternal['DealExternal']['deal_id']);
        if ($discount['Deal']['is_discount_mp'] == 1) {
            $deal = $this->findById($dealExternal['DealExternal']['deal_id']);
            $totalToApplyDiscount = ($deal['Deal']['discounted_price'] * $dealExternal['DealExternal']['quantity']) - $dealExternal['DealExternal']['gifted_amount'] - $dealExternal['DealExternal']['internal_amount'];
            $discountedAmount = $this->getDiscountMP($totalToApplyDiscount, 1);
            $this->addAmountToWallet($discountedAmount, $dealExternal['DealExternal']['user_id'], 'add amount to wallet from discount');
        }
    }

    public function _checkCampaignCodeTypeExternalIsNumeric() {
        if ($this->data[$this->name]['campaign_code_type'] == Deal::CAMPAIGN_CODE_TYPE_EXTERNAL && !is_numeric($this->data[$this->name]['campaign_code'])) {
            return false;
        }
        return true;
    }

    public function isdefeated($deal_id) {
        $this->Behaviors->detach('Excludable');
        $deal = $this->findById($deal_id);
        $today = time();
        $defeated = strtotime('+1 day', strtotime($deal['Deal']['coupon_expiry_date']));
        if ($today >= $defeated) {
            return true;
        } else {
            return false;
        }
    }

    public function isTourism($deal) {
        return $deal['Deal']['is_tourism'];
    }

    public function isWholesaler($deal) {
        return $deal['Deal']['is_wholesaler'];
    }

    public static function isForResell($deal_id, $deal_parent_deal_id, $deal_trade_agreement_id, $deal_coupon_expiry_date, $deal_status_id) {
        $result = false;
        if ($deal_trade_agreement_id == DealTradeAgreement::ID_PRE_COMPRA || $deal_trade_agreement_id == DealTradeAgreement::ID_PAGO_PROVEEDORES) {
            if (strtotime($deal_coupon_expiry_date) <= time()) {
                if ($deal_status_id == ConstDealStatus::Closed) {
                    if (!($deal_id <> $deal_parent_deal_id)) {
                        $result = true;
                    }
                }
            }
        }
        return $result;
    }

    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $parameters = compact('conditions');
        $this->recursive = $recursive;
        $count = $this->find('count', array_merge($parameters, $extra));
        if (isset($extra['group'])) {
            $count = $this->getAffectedRows();
        }
        return $count;
    }

    function asertMinLength($check, $limit) {
        $value = array_values($check);
        return strlen($value[0]) >= $limit;
    }

}
