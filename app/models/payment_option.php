<?php

class PaymentOption extends AppModel {

    var $name = 'PaymentOption';
    var $belongsTo = array(
        'PaymentMethod' => array(
            'className' => 'PaymentMethod',
            'foreignKey' => 'payment_method_id'
        ),
        'PaymentSetting' => array(
            'className' => 'PaymentSetting',
            'foreignKey' => 'payment_setting_id'
        ),
        'PaymentType' => array(
            'className' => 'PaymentType',
            'foreignKey' => 'payment_type_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findAllForMultipleSelect() {
        $subselect = " (select                                                "
                . "   PaymentPlan.name                                        "
                . " from                                                      "
                . "   payment_option_plans as PaymentOptionPlan               "
                . " 	 inner join payment_plans as PaymentPlan              "
                . "     on PaymentPlan.id = PaymentOptionPlan.payment_plan_id "
                . " where                                                     "
                . "   PaymentOptionPlan.payment_option_id = PaymentOption.id  "
                . " order by                                                  "
                . "   PaymentOptionPlan.`order`,                              "
                . "   PaymentOptionPlan.id                                    "
                . " limit 1) as plan_name                                     ";
        $fields = array(
            'PaymentOption.id',
            'PaymentSetting.account',
            'PaymentSetting.gateway',
            'PaymentOption.name',
            $subselect
        );
        $joins = array(
            array(
                'table' => 'payment_settings',
                'alias' => 'PaymentSetting',
                'type' => 'inner',
                'conditions' => array(
                    'PaymentSetting.id = PaymentOption.payment_setting_id'
                )
            )
        );
        $conditions = array(
            'PaymentSetting.account' => array(
                'Normal',
                'Turismo'
            ),
            'PaymentSetting.gateway' => array(
                'MercadoPago',
                'NPS'
            ),
            'NOT' => array(
                'PaymentOption.payment_type_id' => array(
                    5000
                )
            )
        );
        $order = array(
            'PaymentSetting.account',
            'PaymentSetting.gateway',
            'PaymentOption.id'
        );
        return $this->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'order' => $order,
                    'recursive' => -1
        ));
    }

}
