<?php
class CityPerms extends AppModel {

	var $name = 'CityPerms';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
			),
			'City' => array('className' => 'City',
								'foreignKey' => 'city_id',
                                          ),
	);


	function __construct($id = false, $table = null, $ds = null) {
          parent::__construct($id, $table, $ds);
	}
}
