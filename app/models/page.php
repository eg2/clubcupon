<?php
class Page extends AppModel
{
    var $name = 'Page';
    var $displayField = 'title';
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'El campo Titulo es obligatorio'
            ) ,
			'slug' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'El campo URL es obligatorio'
            ) ,
            'content' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'El campo de contenido es obligatorio'
            )
        );
        $this->statusOptions = array(
            '0' => 'Published',
            '1' => 'Draft'
        );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Custom Finds
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function __findActiveDisclaimerPages($options = array())
    {
        $the_options = array(
            'conditions' => array(
                'is_disclaimer =' => '1',
                ),
            //'order' => 'order ASC',
        );
        return $this->find('all', array_merge($the_options, $options));
    }
}

