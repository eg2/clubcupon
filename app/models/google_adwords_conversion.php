<?php 
class GoogleAdwordsConversion extends AppModel {

   var $name = 'GoogleAdwordsConversion';

   var $validate = array(
       'utm_source' => array('required' => true),
       'utm_medium' => array('required' => true),
       'utm_campaign' => array('required' => true),
       'entity_type_id' => array('required' => true),
       'entity_id' => array('required' => true)
   );
}
