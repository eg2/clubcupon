<?php
class ExcludableBehavior extends ModelBehavior {

    var $__settings = array();
    var $model = null;

    function setup(&$model, $settings = array()) {
        $default = array('field' => 'is_now', 'excludable_value' => '1', 'find' => true);
        if (!isset($this->__settings[$model->alias])) {
            $this->__settings[$model->alias] = $default;
        }
        $this->__settings[$model->alias] = 
            array_merge($this->__settings[$model->alias], ife(is_array($settings), $settings, array()));
    }


    /**
     * Run before a model is about to be find, used only fetch for not now records.
     *
     * @param object $model Model about to be now.
     * @param array $queryData Data used to execute this query, i.e. conditions, order, etc.
     * @return mixed Set to false to abort find operation, or return an array with data used to execute query
     * @access public
     */
    function beforeFind(&$model, $queryData) {
        if ($this->__settings[$model->alias]['find'] && $model->hasField($this->__settings[$model->alias]['field'])) {
            $Db =& ConnectionManager::getDataSource($model->useDbConfig);
            $include = false;

            if (!empty($queryData['conditions']) && is_string($queryData['conditions'])) {
                $include = true;

                $fields = array(
                    $Db->name($model->alias) . '.' . $Db->name($this->__settings[$model->alias]['field']),
                    $Db->name($this->__settings[$model->alias]['field']),
                    $model->alias . '.' . $this->__settings[$model->alias]['field'],
                    $this->__settings[$model->alias]['field']
                );

                foreach($fields as $field) {
                    if (preg_match('/^' . preg_quote($field) . '[\s=!]+/i', $queryData['conditions']) || preg_match('/\\x20+' . preg_quote($field) . '[\s=!]+/i', $queryData['conditions'])) {
                        $include = false;
                        break;
                    }
                }
            } else if (empty($queryData['conditions']) || (!array_key_exists($this->__settings[$model->alias]['field'], $queryData['conditions']) && !array_key_exists($model->alias . '.' . $this->__settings[$model->alias]['field'], $queryData['conditions']))) {
                $include = true;
            }

            if ($include) {
                if (empty($queryData['conditions'])) {
                    $queryData['conditions'] = array();
                }

                if (is_string($queryData['conditions'])) {
                    $queryData['conditions'] = $Db->name($model->alias) . '.' . $Db->name($this->__settings[$model->alias]['field']) . '!= ' . $this->__settings[$model->alias]['excludable_value'] . ' AND ' . $queryData['conditions'];
                } else {
                    $queryData['conditions'][$model->alias . '.' . $this->__settings[$model->alias]['field'] . ' !='] = $this->__settings[$model->alias]['excludable_value'];
                }
            }
        }

        return $queryData;
    }



}

?>
