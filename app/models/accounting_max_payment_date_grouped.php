<?php

class AccountingMaxPaymentDateGrouped extends AppModel {

    public $name = 'AccountingMaxPaymentDateGrouped';
    public $alias = 'AccountingMaxPaymentDateGrouped';
    public $useTable = 'accounting_max_payment_date_grouped';
    public $recursive = - 1;

    function __findFirstByCompanyIdGroupedByMaxPaymentDate($conditions) {
        return $this->__findByCondAndType($conditions, 'first');
    }

    function __findAllByCompanyIdGroupedByMaxPaymentDate($conditions) {
        return $this->__findByCondAndType($conditions, 'all');
    }

    private function __findByCondAndType($conditions, $type) {
        $conditionsForFind = array('deal_company_id' => $conditions['conditions']['company_id']);
        $order = array('max_payment_date DESC');
        return $this->find($type, array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }
}
