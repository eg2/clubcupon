<?php
class UserAnswer extends AppModel {

	var $name = 'UserAnswer';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
			),
			'Answer' => array('className' => 'Answer',
								'foreignKey' => 'answer_id',
                                          ),
			'Question' => array('className' => 'Question',
								'foreignKey' => 'question_id',
			)

	);


	function __construct($id = false, $table = null, $ds = null) {
          parent::__construct($id, $table, $ds);
	}
}
