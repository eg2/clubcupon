<?php

    class NewsletterSubject extends AppModel
    {
        var $name = 'NewsletterSubject';
        
        var $actsAs = array (
            'SoftDeletable',
        );
        
        var $belongsTo = array(
            'City',
        );
        
        var $validate = array(
        'city_id' => array(
            
            'Vacio' => array(
                'rule' => 'notempty',
                'message' => 'Especificar una ciudad',
                'allowEmpty' => false),
            
            'Fecha' => array(
                'rule' => '_cityAlreadyHasASubjectOnSelectedDate',
                'message' => 'La ciudad seleccionada ya posee un Asunto en la fecha especificada')
            ),
        'scheduled' => array(
            'rule' => 'notempty',
            'message' => 'Especificar una fecha',
            'allowEmpty' => false,
            ),
        'male_text' => array(
            'rule' => 'notempty',
            'message' => 'Ingresar un subject masculino',
            'allowEmpty' => false,
            ),
        'female_text' => array(
            'rule' => 'notempty',
            'message' => 'Ingresar un subject femenino',
            'allowEmpty' => false,
            ),
        'unisex_text' => array(
            'rule' => 'notempty',
            'message' => 'Ingresar un subject unisex',
            'allowEmpty' => false,
            ),
        );
        
        function __construct() {
            parent::__construct();
        }
        
        function _cityAlreadyHasASubjectOnSelectedDate(){
            
            $cityId = $this->data[$this->name]['city_id'];
            $newsletterSubjectId = $this->data[$this->name]['id'];
            $scheduled = $this->data[$this->name]['scheduled'];
            
            $the_options = array(
            'conditions' => array(
                'city_id =' => $cityId,
                'scheduled =' => $scheduled,
                'not' => array (
                    'NewsletterSubject.id' => $newsletterSubjectId,
                  ),
                ),
            );
            
            $results =  $this->find('first', $the_options);
            return empty ( $results );
            
        }
        
        
    }


