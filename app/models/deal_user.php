<?php

class DealUser extends AppModel {

    var $name = 'DealUser';
    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Deal' => array(
            'className' => 'Deal',
            'foreignKey' => 'deal_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PaymentType' => array(
            'className' => 'PaymentType',
            'foreignKey' => 'payment_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DealExternal' => array ( 
            'className'  => 'DealExternal',
            'foreignKey' => 'deal_external_id',
        ),
       
    );
    var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => false,
            'dependent' => true,
            'conditions' => array('DealUser.user_id = UserProfile.user_id'),
            'fields' => '',
            'order' => ''
        ),
        'PaypalDocaptureLog' => array(
            'className' => 'PaypalDocaptureLog',
            'foreignKey' => 'deal_user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PaypalTransactionLog' => array(
            'className' => 'PaypalTransactionLog',
            'foreignKey' => 'deal_user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Redemption' => array ( 
            'className'  => 'Redemption',
            'foreignKey' => 'deal_user_id',
        ),
    );

    var $hasMany = array (
        'Pin' => array (
            'className'  => 'Pin',
            'foreignKey' => 'deal_user_id',
        ),
        'Redemption' => array (
            'className'  => 'Redemption',
            'foreignKey' => 'deal_user_id',
        ),
        
    );
    var $actsAs = array('SoftDeletable' => array('find' => true));
    
    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'deal_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'quantity' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            )
        );
        $this->moreActions = array(
            ConstMoreAction::Used => __l('Used'),
            ConstMoreAction::NotUsed => __l('Not Used'),
            ConstMoreAction::ResendEmail => 'Reenviar Email',
            // ConstMoreAction::Delete => __l('Delete')
        );
    }

    function conditionsForAll($aditionalConditions) {
        //$conditions['DealUser.is_gift'] = 0;
        $conditions['Deal.deal_status_id'] = array(ConstDealStatus::Closed,
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany,
            ConstDealStatus::PendingApproval);
        $conditions = array_merge($conditions, $aditionalConditions);
        return $conditions;
    }

    function conditionsForPending($aditionalConditions) {
        $conditions['Deal.deal_status_id'] = array(ConstDealStatus::Closed,
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany);
        $conditions['DealUser.emailed'] = 0;
        $conditions['DealUser.is_used'] = 0;
        $conditions['DealUser.is_gift'] = 0;
        $conditions['DATE(Deal.coupon_expiry_date) >='] = date('Y-m-d');
        $conditions = array_merge($conditions, $aditionalConditions);
        return $conditions;
    }

    function conditionsForExpired($aditionalConditions) {
        $conditions['Deal.deal_status_id'] = array(ConstDealStatus::Closed,
            ConstDealStatus::Open,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany);
        $conditions['DealUser.is_gift'] = 0;
        $conditions['DATE(Deal.coupon_expiry_date) <'] = date('Y-m-d');
        $conditions = array_merge($conditions, $aditionalConditions);
        return $conditions;
    }

    function conditionsForUsed($aditionalConditions) {
        $conditions = array();
        $conditions['DealUser.is_used'] = 1;
        $conditions['DealUser.is_gift'] = 0;
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Closed,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany
        );
        $conditions['DATE(Deal.coupon_expiry_date) >='] = date('Y-m-d');
        $conditions = array_merge($conditions, $aditionalConditions);
        return $conditions;
    }

    function conditionsForAvailable($aditionalConditions) {
        $conditions = array();
        $conditions['DealUser.emailed'] = 1;
        $conditions['DealUser.is_used'] = 0;
        $conditions['DealUser.is_gift'] = 0;
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Closed,
            ConstDealStatus::Tipped,
            ConstDealStatus::PaidToCompany
        );
        $conditions['DATE(Deal.coupon_expiry_date) >='] = date('Y-m-d');
        $conditions = array_merge($conditions, $aditionalConditions);
        return $conditions;
    }
      function coupons_to_expire($start_date, $end_date) {
        $coupons = $this->find('all', array(
                    'conditions' => array('date(Deal.coupon_expiry_date) = date(?)' => array($start_date),
                        'DealUser.is_used' => 0),
                    'recursive' => 1,
                    'fields' => array('Deal.coupon_expiry_date',
                        'User.email',
                        'User.username',
                        'Deal.name',
                        'Deal.discounted_price',
                        'Deal.slug',
                    	'City.is_business_unit'
                    ),
        			'contain' => array(
        				'City',
	        		),
                        )
        );
        return $coupons;
    }

////////////////////////////////////////////////////////////////////////////////
//REFACTOR
////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Retrieves  quantity of cupons generated for a
     * deal
     *
     * @param int $deal_id
     * @return int
     */
    public function findCuponsGeneratedByDeal($deal_id) {
        return $this->find('count', array('conditions' =>
                    array('DealUser.deal_id ' => $deal_id)
                        )
        );
    }
    
    /**
     * Retrieves  a Coupon
     * dealuser
     *
     * @param int $dealUser_id
     * @return array
     */
    public function findCouponById($dealUser_id) {
        return $this->find(
                'first', array(
                    'conditions' =>array('DealUser.id ' => $dealUser_id),
                    'recursive' => -1,
                    )
        );
    }

    public function findByCouponCode($coupon_code) {
        return $this->find(
                'first', array(
                    'conditions' =>array('DealUser.coupon_code ' => $coupon_code),
                    'recursive' => -1,
                    )
        );
    }    
    
    public function markAsUsedByCompany($deal_user_id) {
        return $this->updateAll(array('DealUser.is_used' => 1), array('DealUser.id' => $deal_user_id));
    }

    public function markAsUsedByUser($deal_user_id) {
        return $this->updateAll(array('DealUser.is_used_user' => 1), array('DealUser.id' => $deal_user_id));
    }

    public function updateAllAsEmailedToCompany($deal_users_ids) {
        $saved = false;
        if (!empty($deal_users_ids)) {
            $list = implode(',', $deal_users_ids);
            $query = " UPDATE `deal_users` AS `DealUser` SET `DealUser`.`email_to_company` = 1 WHERE `DealUser`.`id` IN ({$list}) ";
            $saved = $this->query($query);
        }
        return $saved;
    }	    
    
    public function findAllCouponsByDealId($deal_id) {
        return $this->find(
                'all', array(
                    'conditions' =>array('DealUser.deal_id ' => $deal_id),
                    'contain' => array(
                        'Redemption'
                    ),
                    'recursive' => 0,
                    )
        );
        
    }
    
	public function updateAsNotBilled($deal_user_id){
		
    	$fields = array(
            'DealUser.is_billed' => 0,
        );
        $conditions = array(
            'DealUser.id' => $deal_user_id
        );
        
        return $this->updateAll($fields, $conditions);
    }
    
	public function updateCouponsAsNotBilled($deal_user_ids){
		
		$saved = false;
		
		if (!empty($deal_user_ids)) {
			
			$list = implode(',', $deal_user_ids);
			
			
			
			$query = " UPDATE `deal_users` AS `DealUser` SET `DealUser`.`is_billed` = 0 WHERE `DealUser`.`id` IN ({$list}) ";
		
			$saved = $this->query($query);
		}
		return $saved;
		
    }
    
}