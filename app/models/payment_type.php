<?php

class PaymentType extends AppModel {

    var $name = 'PaymentType';
    var $displayField = 'name';
    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'DealUser' => array(
            'className' => 'DealUser',
            'foreignKey' => 'payment_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            )
        );
    }

    function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'PaymentType.id' => $id
                    ),
                    'recursive' => -1
                ));
    }

    function isPaymentTypeMP($id) {
        $data = $this->findById($id);
        if($data['PaymentType']['name'] === "Cuenta Club Cupon"){
            return true;
        }
        return in_array($data['PaymentType']['gateway_id'], array(11, 5, 23, 24));
    }
}

