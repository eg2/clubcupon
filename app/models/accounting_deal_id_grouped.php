<?php

class AccountingDealIdGrouped extends AppModel {

    public $name = 'AccountingDealIdGrouped';
    public $alias = 'AccountingDealIdGrouped';
    public $useTable = 'accounting_deal_id_grouped';
    public $recursive = - 1;

    function __findAllByCompanyIdGroupedByLiquidateIdAndMode($conditions, $mode) {
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
            'max_payment_date' => $conditions['conditions']['max_payment_date'],
        );
        $joins = array();

        switch ($mode) {
            case 'Redimido':
                $conditionsForFind['pay_by_redeemed'] = 1;
                break;
            case 'Vendido':
                $conditionsForFind['pay_by_redeemed'] = 0;
                break;
            case 'Fondo':
    //            $conditionsForFind['liquidate_guarantee_fund_percentage >='] = 0;
                $conditionsForFind['pay_by_redeemed !='] = 1;
                $conditionsForFind['has_expired ='] = 1;
//                $joins =  array(
//                  array(
//                    'table' => 'accounting_items',
//                    'alias' => 'AccountingItem',
//                    'type' => 'INNER',
//                    'conditions' => array(
//                      'AccountingDealIdGrouped.liquidate_id = AccountingItem.id'
//                    )
//                  ));
                break;
            default: break;
        }

        $order = array(
            'liquidate_total_amount DESC'
        );

        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'joins' => $joins,
                    'page' => $conditions['page'],
        ));
    }

    function __findAllByCompanyIdGroupByLiquidate($conditions, $order, $limit, $page) {
      
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'limit' => $limit,
                    'page' => $page,
        ));
    }

    function __findAccountingEventsByDealIdAndCompanyId($dealId, $companyId) {
        $cond['deal_company_id'] =  $companyId;
        $cond['deal_id'] =  $dealId;
        $order = array( 'liquidate_total_amount DESC'); 
        //$limit = 10;  $page = 1;
        $accountingEvents = $this->__findAllByCompanyIdGroupByLiquidate($cond, $order, $limit, $page);
        return $accountingEvents;
    }
}
