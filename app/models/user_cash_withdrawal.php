<?php
class UserCashWithdrawal extends AppModel
{
    var $name = 'UserCashWithdrawal';
    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'WithdrawalStatus' => array(
            'className' => 'WithdrawalStatus',
            'foreignKey' => 'withdrawal_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true,
            'counterScope' => ''
        ),
    );
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ) ,
            'withdrawal_status_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ) ,
            'amount' => array(
                'rule2' => array(
                    'rule' => 'numeric',
                    'message' => __l('Should be numeric')
                ) ,
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            )
        );
        $this->moreActions = array(
            ConstWithdrawalStatus::Pending => __l('Pending') ,
            ConstWithdrawalStatus::Approved => __l('Approve (Pay to user)') ,
            ConstWithdrawalStatus::Rejected => __l('Rejected') ,
        );
    }
    function _checkAmount($amount)
    {
        $user_available_balance = $this->User->checkUserBalance($this->data[$this->name]['user_id']);
        if ($user_available_balance < $amount) {
            $this->validationErrors['amount'] = 'La cantidad es mayor que el monto de la Cuenta Club Cupón';
        }
        if($this->data[$this->name]['user_type_id'] == ConstUserTypes::User){
			if (($amount < Configure::read('user.minimum_withdraw_amount')) || ($amount > Configure::read('user.maximum_withdraw_amount') )){
				$this->validationErrors['amount'] = sprintf(__l('Given amount should lies from  %s%s to %s%s') , Configure::read('site.currency'),Configure::read('user.minimum_withdraw_amount') , Configure::read('site.currency') , Configure::read('user.maximum_withdraw_amount')  );
			}
		}else if($this->data[$this->name]['user_type_id'] == ConstUserTypes::Company){
			if (($amount < Configure::read('company.minimum_withdraw_amount')) || ($amount > Configure::read('company.maximum_withdraw_amount') )){
				$this->validationErrors['amount'] = sprintf(__l('Given amount should lies from  %s%s to %s%s') , Configure::read('company.minimum_withdraw_amount') , Configure::read('site.currency') , Configure::read('company.maximum_withdraw_amount') , Configure::read('site.currency'));
			}
		}
        return false;
    }
    function _transferAmount($user_type_id = '', $userCashWithdrawalsIds = array())
    {
      // Esta funcionalidad todavía no fue solicitada por el cliente.
      // Se deshabilita.
      return false;

        App::import('Model', 'PaypalTransactionLog');
        $this->PaypalTransactionLog = &new PaypalTransactionLog();
        App::import('Component', 'Paypal');
        $this->Paypal = &new PaypalComponent();
        $flash_message = '';
        $conditions['UserCashWithdrawal.withdrawal_status_id'] = ConstWithdrawalStatus::Pending;
        if (!empty($userCashWithdrawalsIds)) {
            $conditions['UserCashWithdrawal.id'] = $userCashWithdrawalsIds;
        } elseif ($user_type_id == ConstUserTypes::User) {
            $conditions['User.user_type_id'] = ConstUserTypes::User;
        } elseif ($user_type_id == ConstUserTypes::Company) {
            $conditions['User.user_type_id'] = ConstUserTypes::Company;
        }
        $userCashWithdrawals = $this->find('all', array(
            'conditions' => $conditions,
            'contain' => array(
                'User' => array(
                    'UserProfile' => array(
                        'fields' => array(
                            'UserProfile.paypal_account'
                        )
                    ) ,
                    'fields' => array(
                        'User.username',
                        'User.available_balance_amount',
                        'User.blocked_amount'
                    )
                ) ,
            ) ,
            'recursive' => 2
        ));
        if (!empty($userCashWithdrawals)) {
            $paymentGateway = $this->User->Transaction->PaymentGateway->find('first', array(
                'conditions' => array(
                    'PaymentGateway.id ' => ConstPaymentGateways::PayPal
                ) ,
                'recursive' => -1
            ));
            foreach($userCashWithdrawals as $userCashWithdrawal) {
                if (!empty($userCashWithdrawal) && !empty($userCashWithdrawal['User']['UserProfile']['paypal_account']) && $userCashWithdrawal['User']['blocked_amount'] >= $userCashWithdrawal['UserCashWithdrawal']['amount']) {
                    $this->data['PaypalTransactionLog']['user_id'] = ConstUserIds::Admin;
                    $this->data['PaypalTransactionLog']['currency_type'] = Configure::read('paypal.currency_code');
                    $this->data['PaypalTransactionLog']['is_mass_pay'] = 1;
                    $this->data['PaypalTransactionLog']['mass_pay_status'] = 'PENDING';
                    $this->data['PaypalTransactionLog']['payer_email'] = $paymentGateway['PaymentGateway']['email'];
                    $this->data['PaypalTransactionLog']['receiver_email'] = $userCashWithdrawal['User']['UserProfile']['paypal_account'];
                    $this->data['PaypalTransactionLog']['currency_type'] = Configure::read('paypal.currency_code');
                    $this->data['PaypalTransactionLog']['user_cash_withdrawal_id'] = $userCashWithdrawal['UserCashWithdrawal']['id'];
                    $this->PaypalTransactionLog->save($this->data, false);
                    $paypal_transaction_list[] = $paypal_transaction_id = $this->PaypalTransactionLog->getLastInsertId();
                    $userCashWithdrawal_list[] = $userCashWithdrawal['UserCashWithdrawal']['id'];
                    $reciever_info[] = array(
                        'receiverEmail' => $userCashWithdrawal['User']['UserProfile']['paypal_account'],
                        'amount' => $userCashWithdrawal['UserCashWithdrawal']['amount'],
                        'uniqueID' => $userCashWithdrawal['UserCashWithdrawal']['id'],
                        'transaction_log' => $paypal_transaction_id,
                        'note' => 'Redeem'
                    );
                }
            }
            if (!empty($userCashWithdrawal_list)) {
                $sender_info = array(
                    'API_UserName' => Configure::read('paypal.masspay_API_UserName') ,
                    'API_Password' => Configure::read('paypal.masspay_API_Password') ,
                    'API_Signature' => Configure::read('paypal.masspay_API_Signature')
                );
                $notify_url = Router::url(array(
                    'controller' => 'user_cash_withdrawals',
                    'action' => 'process_masspay_ipn',
                    'admin' => false
                ) , true);
                $paypal_response = $this->Paypal->massPay($sender_info, $reciever_info, $notify_url, 'Your Payment Has been Sent', $paymentGateway['PaymentGateway']['is_test'],Configure::read('paypal.currency_code'));
                if (!empty($paypal_response)) {
                    $this->PaypalTransactionLog->updateAll(array(
                        'PaypalTransactionLog.masspay_response' => '\'' . serialize($paypal_response) . '\''
                    ) , array(
                        'PaypalTransactionLog.id' => $paypal_transaction_list
                    ));
                    $this->updateAll(array(
                        'UserCashWithdrawal.withdrawal_status_id' => ConstWithdrawalStatus::Approved
                    ) , array(
                        'UserCashWithdrawal.id' => $userCashWithdrawal_list
                    ));
                    $flash_message = $paypal_response['ACK'];
                }
            } else {
                $flash_message = 'LowBalance';
            }
        } else {
            $flash_message = 'Failure';
        }
        return $flash_message;
    }
}
