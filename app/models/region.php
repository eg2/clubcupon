<?php
class Region extends AppModel
{
    var $name = 'Region';
    var $displayField = 'name';
    var $belongsTo = 'UserProfile';

    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = 'Neighbourhood';

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }
}
