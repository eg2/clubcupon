<?php

class User extends AppModel {

    var $name = 'User';
    var $displayField = 'username';
    var $actsAs = array(
        'Sluggable' => array(
            'label' => array(
                'username'
            )
        ),
        'Logable' => array(
            'userModel' => 'User',
            'userKey' => 'user_id',
            'change' => 'full',
            'description_ids' => TRUE,
            'noLogUserTypes' => array(
                ConstUserTypes::User,
                ConstUserTypes::Company,
                ConstUserTypes::Agency,
                ConstUserTypes::Partner
            )
        )
    );
    var $belongsTo = array(
        'UserType' => array(
            'className' => 'UserType',
            'foreignKey' => 'user_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'RefferalUser' => array(
            'className' => 'User',
            'foreignKey' => 'referred_by_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'GiftRecivedFromUser' => array(
            'className' => 'User',
            'foreignKey' => 'gift_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'ActionPointUser',
        'CityPerms' => array(
            'className' => 'CityPerms',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserAnswer' => array(
            'className' => 'UserAnswer',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserView' => array(
            'className' => 'UserView',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserComment' => array(
            'className' => 'UserComment',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserOpenid' => array(
            'className' => 'UserOpenid',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserFriend' => array(
            'className' => 'UserFriend',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'FbFriend' => array(
            'className' => 'FbFriend',
            'foreignKey' => 'friend_user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'BlockedUser' => array(
            'className' => 'BlockedUser',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'DealUser' => array(
            'className' => 'DealUser',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'GiftUser' => array(
            'className' => 'GiftUser',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserCashWithdrawal' => array(
            'className' => 'UserCashWithdrawal',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CitySuggestion' => array(
            'className' => 'CitySuggestion',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'DealExternal' => array(
            'className' => 'DealExternal',
            'foreignKey' => 'user_id'
        )
    );
    var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'UserPermissionPreference' => array(
            'className' => 'UserPermissionPreference',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'UserAvatar' => array(
            'className' => 'UserAvatar',
            'foreignKey' => 'foreign_id',
            'dependent' => true,
            'conditions' => array(
                'UserAvatar.class' => 'UserAvatar'
            ),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CkSession' => array(
            'className' => 'CkSession',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CompanyCandidate' => array(
            'className' => 'CompanyCandidate',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function afterSave($created) {
        if ($created) {
            ClassRegistry::init('UserReferral')->updateRegisteredReferredUser($this->data['User']['email']);
        }
    }

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'user_id' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'message' => __l('Required')
                )
            ),
            'username' => array(
                'rule6' => array(
                    'rule' => array(
                        'minLength',
                        '3'
                    ),
                    'message' => 'Debe ser mínimo de 3 caracteres'
                ),
                'rule5' => array(
                    'rule' => array(
                        'maxLength',
                        '20'
                    ),
                    'message' => 'Must be maximum of 20 characters'
                ),
                'rule4' => array(
                    'rule' => 'alphaNumeric',
                    'message' => 'Sólo letras o números, sin espacios u otros signos'
                ),
                'rule3' => array(
                    'rule' => 'isUnique',
                    'message' => 'El nombre de usuario ya existe'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Ingresá un nombre de usuario'
                )
            ),
            'email' => array(
                'rule3' => array(
                    'rule' => 'isUnique',
                    'message' => 'La dirección de mail ya existe'
                ),
                'rule2' => array(
                    'rule' => 'email',
                    'message' => 'Debe ser un mail válido'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Ingresá un mail'
                )
            ),
            'passwd' => array(
                'rule2' => array(
                    'rule' => array(
                        'minLength',
                        6
                    ),
                    'message' => 'Debe tener al menos 6 caracteres'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Ingresá una contraseña'
                )
            ),
            'old_password' => array(
                'rule3' => array(
                    'rule' => array(
                        '_checkOldPassword',
                        'old_password'
                    ),
                    'message' => __l('Your old password is incorrect, please try again')
                ),
                'rule2' => array(
                    'rule' => array(
                        'minLength',
                        6
                    ),
                    'message' => 'Debe tener al menos 6 caracteres'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Ingresá tu antigua contraseña'
                )
            ),
            'confirm_password' => array(
                'rule3' => array(
                    'rule' => array(
                        '_checkPassword',
                        'passwd',
                        'confirm_password'
                    ),
                    'message' => 'Las contraseñas no coinciden, por favor, volvé a escribirlas'
                ),
                'rule2' => array(
                    'rule' => array(
                        'minLength',
                        6
                    ),
                    'message' => 'Debe tener al menos 6 caracteres'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Reingresá tu contraseña'
                )
            ),
            'confirm_email' => array(
                'rule2' => array(
                    'rule' => array(
                        '_checkEmail',
                        'email',
                        'confirm_email'
                    ),
                    'message' => 'El email y el email de confirmación deben coincidir, por favor intentá nuevamente'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'ingresá¡ la confirmación del mail.'
                )
            ),
            'captcha' => array(
                'rule2' => array(
                    'rule' => '_isValidCaptcha',
                    'message' => 'El cÃƒÂ³digo de captcha ingresado es errÃƒÂ³neo'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Ingresá el código de la imagen'
                )
            ),
            'is_agree_terms_conditions' => array(
                'rule' => array(
                    'equalTo',
                    '1'
                ),
                'message' => 'Debes aceptar los términos y condiciones.'
            ),
            'message' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'subject' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'city' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            )
        );
        $this->isFilterOptions = array(
            ConstMoreAction::Inactive => __l('Inactive'),
            ConstMoreAction::Active => __l('Active')
        );
        $this->moreActions = array(
            ConstMoreAction::Inactive => __l('Inactive'),
            ConstMoreAction::Active => __l('Active'),
            ConstMoreAction::Delete => __l('Delete'),
            ConstMoreAction::Export => __l('Export')
        );
        $this->adminMoreActions = array(
            ConstMoreAction::Active => __l('Active'),
            ConstMoreAction::Export => __l('Export')
        );
        $this->bulkMailOptions = array(
            1 => __l('All Users'),
            2 => __l('Inactive Users'),
            3 => __l('Active Users')
        );
    }

    function _checkEmail($field1 = array(), $field2 = null, $field3 = null) {
        if ($this->data[$this->name][$field2] == $this->data[$this->name][$field3]) {
            return true;
        }
        return false;
    }

    function _checkPassword($field1 = array(), $field2 = null, $field3 = null) {
        if ($this->data[$this->name][$field2] == $this->data[$this->name][$field3]) {
            return true;
        }
        return false;
    }

    function _checkOldPassword($field1 = array(), $field2 = null) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $_SESSION['Auth']['User']['id']
            ),
            'recursive' => - 1
        ));
        if (AuthComponent::password($this->data[$this->name][$field2]) == $user['User']['password']) {
            return true;
        }
        return false;
    }

    function getResetPasswordHash($user_id = null) {
        return md5($user_id . '-' . date('y-m-d') . Configure::read('Security.salt'));
    }

    function isValidResetPasswordHash($user_id = null, $hash = null) {
        return (md5($user_id . '-' . date('y-m-d') . Configure::read('Security.salt')) == $hash);
    }

    function getActivateHash($user_id = null) {
        return md5($user_id . '-' . Configure::read('Security.salt'));
    }

    function isValidActivateHash($user_id = null, $hash = null) {
        return (md5($user_id . '-' . Configure::read('Security.salt')) == $hash);
    }

    function getUserIdHash($user_ids = null) {
        return md5($user_ids . Configure::read('Security.salt'));
    }

    function isValidUserIdHash($user_ids = null, $hash = null) {
        return (md5($user_ids . Configure::read('Security.salt')) == $hash);
    }

    function isAllowed($user_type = null) {
        if ($user_type != ConstUserTypes::Company || ($user_type == ConstUserTypes::Company && Configure::read('user.is_company_actas_normal_user'))) {
            return true;
        }
        return false;
    }

    function checkUserBalance($user_id = null) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'fields' => array(
                'User.available_balance_amount'
            ),
            'recursive' => - 1
        ));
        if ($user['User']['available_balance_amount']) {
            return $user['User']['available_balance_amount'];
        }
        return false;
    }

    function checkUserWalletBlocked($user_id = null) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'fields' => array(
                'User.wallet_blocked'
            ),
            'recursive' => - 1
        ));
        if ($user['User']['wallet_blocked'])
            return $user['User']['wallet_blocked'];
        return false;
    }

    function getPaymentsPending($userId) {
        return $this->DealExternal->find('count', array(
                    'conditions' => array(
                        'DealExternal.user_id' => $userId,
                        'DealExternal.external_status' => 'P'
                    )
        ));
    }

    function _checkamount($amount) {
        if (!empty($amount) && !is_numeric($amount)) {
            $this->validationErrors['amount'] = __l('Amount should be Numeric');
        }
        if (empty($amount)) {
            $this->validationErrors['amount'] = __l('Required');
        }
        if (!empty($amount) && $amount < Configure::read('wallet.min_wallet_amount')) {
            $this->validationErrors['amount'] = __l('Amount should be greater than minimum amount');
        }
        if (Configure::read('wallet.max_wallet_amount') && !empty($amount) && $amount > Configure::read('wallet.max_wallet_amount')) {
            $this->validationErrors['amount'] = sprintf(__l('Given amount should lies from  %s%s to %s%s'), Configure::read('wallet.min_wallet_amount'), Configure::read('site.currency'), Configure::read('wallet.max_wallet_amount'), Configure::read('site.currency'));
        }
        return false;
    }

    function checkUsernameAvailable($username) {
        $user = $this->find('count', array(
            'conditions' => array(
                'User.username' => $username
            ),
            'recursive' => - 1
        ));
        if (!empty($user)) {
            return false;
        }
        return $username;
    }

    function createBacUser($user = null, $portal = 0, $dealStartDate = null) {
        if ($user === null) {
            return false;
        }
        if (empty($dealStartDate)) {
            $dealStartDate = time();
        }
        if (!is_array($user)) {
            $this->recursive = - 1;
            $user = $this->read(null, $user);
        }
        $user_profile = $this->UserProfile->find('first', array(
            'conditions' => array(
                'UserProfile.user_id' => $user['User']['id']
            ),
            'recursive' => - 1
        ));
        if (is_null($user['User']['username'])) {
            $loginUserNameForBAC = $user['User']['id'];
        } else {
            $loginUserNameForBAC = $user['User']['username'];
        }
        $idPortal = $this->getPortalId($portal, $dealStartDate);
        if (@file_get_contents(Configure::read('BAC.wsdl.altaModificacionClienteEmpresa'))) {
            $sopa = new SoapClient(Configure::read('BAC.wsdl.altaModificacionClienteEmpresa'));
        }
        $params = array(
            'idPortal' => $idPortal,
            'idUsuarioPortal' => $user['User']['id'],
            'login' => $loginUserNameForBAC,
            'email' => $user['User']['email'],
            'idPais' => Configure::read('BAC.Argentina'),
            'idProvincia' => Configure::read('BAC.CapitalFederal'),
            'idTipoUsuario' => Configure::read('BAC.id_tipo_usuario_usuario'),
            'nombre' => $user_profile['UserProfile']['first_name'],
            'apellido' => $user_profile['UserProfile']['last_name']
        );
        if (strtotime($dealStartDate) < Configure::read('tec_dia.start_date')) {
            if ($portal == 0) {
                if (!empty($user['User']['bac_user_id'])) {
                    $params['idUsuario'] = $user['User']['bac_user_id'];
                }
            } else {
                if (!empty($user['User']['bac_tourism_id'])) {
                    $params['idUsuario'] = $user['User']['bac_tourism_id'];
                }
            }
        } else {
            if ($user['User']['bac_id'] == 0) {
                $params['idUsuario'] = null;
            } else {
                $params['idUsuario'] = $user['User']['bac_id'];
            }
        }
        try {
            Debugger::log(sprintf('intentando crear usuario en BAC enviando params: %s', debugEncode($params)), LOG_DEBUG);
            if (!isset($sopa))
                throw new Exception("WS de BAC unreachable");
            $bac_user_id = $sopa->altaModificacionClienteEmpresa($params);
        } catch (Exception $e) {
            $bac_user_id = 0;
            Debugger::log('error en el WS de BAC: ' . $e->getMessage() . ' id: ' . $bac_user_id, LOG_DEBUG);
        }
        Debugger::log('creado usuario en BAC si entre el intentando... y esto no hay otro mensaje', LOG_DEBUG);
        $this->id = $user['User']['id'];
        if ($bac_user_id != null) {
            if (strtotime($dealStartDate) < Configure::read('tec_dia.start_date')) {
                $this->saveField($portal == 0 ? 'bac_user_id' : 'bac_tourism_id', $bac_user_id);
            } else {
                $this->saveField('bac_id', $bac_user_id);
            }
        }
        return $bac_user_id;
    }

    function get_points_for($user_id, $date) {
        $p = $this->query('select SUM(points) as total from action_point_users where user_id = ' . $user_id . ' AND created <= \'' . $date . '\'  AND status = ' . ConstPointStatuses::Available . '');
        return (int) $p[0][0]['total'];
    }

    function get_current_points($user_id) {
        return $this->get_points_for($user_id, date('Y-m-d H:i:s'));
    }

    function get_limited_points_for($user_id, $date, $action_ids) {
        return 0;
        if (!is_array($action_ids))
            $p = $this->query('select SUM(points) as total from action_point_users where user_id = ' . $user_id . ' AND created <= \'' . $date . '\'  AND status = ' . ConstPointStatuses::Available . '');
        else
            $p = $this->query('select SUM(points) as total from action_point_users where user_id = ' . $user_id . ' AND action_id not in (' . implode(', ', $action_ids) . ') AND created <= \'' . $date . '\'  AND status = ' . ConstPointStatuses::Available . '');
        return (int) $p[0][0]['total'];
    }

    function get_limited_current_points($user_id, $action_ids) {
        return $this->get_limited_points_for($user_id, date('Y-m-d H:i:s'), $action_ids);
    }

    function add_points($user_id, $action_id) {
        App::import('Model', 'ActionPoint');
        $this->ActionPoint = new ActionPoint();
        App::import('Model', 'Action');
        $this->Action = new Action();
        if ($action_id == ConstActionIds::DebitCredit)
            return false;
        $a = $this->Action->find('first', array(
            'recursive' => - 1,
            'conditions' => array(
                'id' => $action_id
            )
        ));
        if (empty($a))
            return false;
        $ap = $this->ActionPoint->find('first', array(
            'recursive' => - 1,
            'conditions' => array(
                'action_id' => $action_id
            ),
            'order' => 'ActionPoint.created DESC'
        ));
        if (empty($ap))
            return false;
        $u = $this->find('first', array(
            'recursive' => - 1,
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        if (empty($u))
            return false;
        if (empty($a['Action']['is_active']))
            return true;
        if ($this->get_limited_current_points($user_id, Configure::read('Points.unlimited_action_ids')) >= Configure::read('Points.limit'))
            return true;
        $u['User']['available_points']+= $ap['ActionPoint']['points'];
        $apu = array(
            'ActionPointUser' => array(
                'action_point_id' => $ap['ActionPoint']['id'],
                'user_id' => $user_id,
                'deal_external_id' => null,
                'points' => $ap['ActionPoint']['points'],
                'status' => ConstPointStatuses::Available
            )
        );
        $this->ActionPointUser->create();
        if (!$this->ActionPointUser->save($apu))
            return false;
        $this->id = (int) $u['User']['id'];
        if (!$this->saveField('available_points', $u['User']['available_points'])) {
            $this->ActionPointUser->delete($this->ActionPointUser->id);
            return false;
        }
        return true;
    }

    function credit_points($user_id, $amount) {
        $u = $this->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        if (empty($u))
            return false;
        $u['User']['available_points']+= $amount;
        $apu = array(
            'ActionPointUser' => array(
                'action_point_id' => ConstActionIds::DebitCredit,
                'user_id' => $user_id,
                'deal_external_id' => null,
                'points' => $amount,
                'status' => ConstPointStatuses::Available
            )
        );
        $this->ActionPointUser->create();
        if (!$this->ActionPointUser->save($apu))
            return false;
        $this->id = (int) $u['User']['id'];
        if (!$this->saveField('available_points', $u['User']['available_points'])) {
            $this->ActionPointUser->delete($this->ActionPointUser->id);
            return false;
        }
        return true;
    }

    function debit_points($user_id, $deal_external_id, $amount) {
        App::import('Model', 'DealExternal');
        $this->DealExternal = new DealExternal();
        $d = $this->DealExternal->find('count', array(
            'conditions' => array(
                'DealExternal.id' => $deal_external_id,
                'DealExternal.user_id' => $user_id,
                'external_status' => array(
                    'A',
                    'P',
                    'W'
                )
            )
        ));
        if ($d == 0)
            return false;
        $u = $this->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        if (empty($u))
            return false;
        $u['User']['available_points']-= $amount;
        $apu = array(
            'ActionPointUser' => array(
                'action_point_id' => ConstActionIds::DebitCredit,
                'user_id' => $user_id,
                'deal_external_id' => $deal_external_id,
                'points' => - $amount,
                'status' => ConstPointStatuses::Available
            )
        );
        $this->ActionPointUser->create();
        if (!$this->ActionPointUser->save($apu))
            return false;
        $this->id = (int) $u['User']['id'];
        if (!$this->saveField('available_points', $u['User']['available_points'])) {
            $this->ActionPointUser->delete($this->ActionPointUser->id);
            return false;
        }
        return true;
    }

    function refund_points($deal_external_id) {
        App::import('Model', 'DealExternal');
        $this->DealExternal = new DealExternal();
        $apu = $this->ActionPointUser->find('first', array(
            'conditions' => array(
                'deal_external_id' => $deal_external_id,
                'status' => ConstPointStatuses::Available
            )
        ));
        if (empty($apu))
            return false;
        $d = $this->DealExternal->find('count', array(
            'conditions' => array(
                'DealExternal.id' => $apu['ActionPointUser']['deal_external_id'],
                'external_status' => 'C'
            )
        ));
        if ($d == 0)
            return false;
        $u = $this->find('first', array(
            'conditions' => array(
                'User.id' => $apu['ActionPointUser']['user_id']
            )
        ));
        if (empty($u))
            return false;
        $u['User']['available_points']+= $apu['ActionPointUser']['points'];
        $this->ActionPointUser->id = (int) $apu['ActionPointUser']['id'];
        if (!$this->ActionPointUser->saveField('status', ConstPointStatuses::Cancelled))
            return false;
        $this->id = (int) $u['User']['id'];
        if (!$this->saveField('available_points', $u['User']['available_points'])) {
            $this->ActionPointUser->id = (int) $apu['ActionPointUser']['id'];
            $this->ActionPointUser->saveField('status', ConstPointStatuses::Available);
            return false;
        }
        return true;
    }

    function needed_points_and_money($user_id, $amount) {
        App::import('Model', 'Rate');
        $this->Rate = new Rate();
        $needed = ceil($amount / $this->Rate->get_current_rate());
        $u = $this->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            )
        ));
        $actual = $u['User']['available_points'];
        $final = min($needed, $actual);
        return array(
            (int) $final,
            $this->Rate->valuate($final)
        );
    }

    function isCompany($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Company && $this->Company->findByUser($data);
    }

    function isCompanyWithoutNow($data) {
        return $this->isCompany($data) && !$this->isCompanyWithNow($data);
    }

    function isCompanyWithNow($data) {
        return $this->isCompany($data) && $this->Company->isNow($this->Company->findByUser($data));
    }

    function isCompanyCandidate($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Company && ($this->CompanyCandidate->findByUser($data) || !$this->Company->findByUser($data));
    }

    function isRegularUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::User;
    }

    function isAgencyUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Agency;
    }

    function isPartnerUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Partner;
    }

    function isSellerUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Seller;
    }

    function isAdminUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::Admin;
    }

    function isSuperAdminUser($data) {
        return $data['User']['user_type_id'] == ConstUserTypes::SuperAdmin;
    }

    function getUserData($id) {
        return $this->findById($id);
    }

    function findById($id) {
        $conditions = array(
            'User.id' => $id
        );
        $joins = array(
            array(
                'table' => 'user_profiles',
                'alias' => 'UserProfile',
                'type' => 'left',
                'conditions' => array(
                    'UserProfile.user_id = User.id'
                )
            ),
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'left',
                'conditions' => array(
                    'Company.user_id = User.id'
                )
            )            
        );
        return $this->find('first', array(
                    'fields' => 'User.*, UserProfile.*, Company.*',
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'recursive' => - 1
        ));
    }

    function getActiveSellersForSelect() {
        $sellersProfiles = $this->getActiveSellersFullProfile();
        foreach ($sellersProfiles as $sellerProfile) {
            $id = $sellerProfile['User']['id'];
            $username = $sellerProfile['User']['username'];
            $first_name = $sellerProfile['UserProfile']['first_name'];
            $last_name = $sellerProfile['UserProfile']['last_name'];
            $temp[$id] = !empty($first_name) ? $first_name . ' ' . $last_name : $username;
        }
        return $temp;
    }

    function getActiveSellersFullProfile() {
        $conditions['User.user_type_id'] = ConstUserTypes::Seller;
        $conditions['User.is_visible'] = '1';
        return $this->find('all', array(
                    'conditions' => array(
                        $conditions
                    ),
                    'contain' => array(
                        'UserProfile'
                    ),
                    'recursive' => 0,
                    'order' => array(
                        'User.id' => 'ASC'
                    )
        ));
    }

    function canBuyDeal($user) {
        if (empty($user)) {
            $canBuyDeal = true;
        } else {
            $canBuyDeal = ($user['User']['user_type_id'] != NULL && in_array($user['User']['user_type_id'], array(
                        ConstUserTypes::Admin,
                        ConstUserTypes::User,
                        ConstUserTypes::SuperAdmin
            )));
        }
        return $canBuyDeal;
    }

    function findActiveUserById($id) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $id,
                'User.is_active' => 1
            ),
            'recursive' => - 1
        ));
        return $user;
    }

    function findByEmail($email) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.email =' => $email,
                'User.is_active' => 1
            ),
            'fields' => array(
                'User.id',
                'User.email'
            ),
            'recursive' => - 1
        ));
        return $user;
    }

}
