<?php
class FbFriend extends AppModel
{
    var $name = 'FbFriend';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'friend_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        
    );
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            
        );
        
    }
    
    function saveAllFriends($data) {
    	if (count($data) > 0 && !empty($data[0])) {
    		$value_array = array();
    		$table_name = $this->table;
    		$model_name = $this->name;
    		$fields = array_keys($data[0][$model_name]);
    		foreach ($data as $key => $value)
    			$value_array[] = "('" . implode('\',\'', $value[$model_name]) . "')";
    		$sql = "INSERT INTO " . $table_name . " (" . implode(', ', $fields) . ") VALUES " . implode(',', $value_array);
    		$this->query($sql);
    		return true;
    	}
    	return false;
    }
   
}
