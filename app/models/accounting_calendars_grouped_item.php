<?php

class AccountingCalendarsGroupedItem extends AppModel {

    public $name = 'AccountingCalendarsGroupedItem';
    public $alias = 'AccountingCalendarsGroupedItem';
    public $useTable = 'accounting_calendars_grouped_items';
    public $recursive = - 1;

    function findAllByCompanyIdAndCalendarCode($companyId, $calendarCode) {
        $conditions = array(
            'deal_company_id' => $companyId,
            'liquidate_accounting_calendar_id' => $calendarCode
        );
        $order = array(
            'liquidate_id DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

}