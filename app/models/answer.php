<?php
class Answer extends AppModel
{
    var $name = 'Answer';
    var $displayField = 'content';

    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array('Question' => array(
                                               'fields'  => array('Answer.content','Answer.id','Answer.creator')
                                               ));

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }


    function findOrGetIdByContent($content,$questionId)
    {
      $findExist = $this->find('first', array(
                                              'conditions' => array(
                                                                    'content' => $content,
                                                                    'question_id' => $questionId
                                                                    ) ,
                                              'fields' => array(
                                                                'id'
                                                                ) ,
                                              'recursive' => -1
                                              ));
      if (!empty($findExist)) {
        return $findExist[$this->name]['id'];
      } else {
        $data = array();
        $data['creator'] = 'user';
        $data['content'] = $content;
        $data['question_id'] = $questionId;
        $this->create();
        $this->set($data);
        $this->save($data);
        return $this->getLastInsertId();;
      }
    }
}
