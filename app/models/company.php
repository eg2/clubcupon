<?php
class Company extends AppModel
{
    var $name = 'Company';
    var $displayField = 'name';
    var $actsAs = array(
        'WhoDidIt' => array(),
        'Searchable',
        'Sluggable' => array(
            'label' => array(
                'name'
            )
        ) ,
    );
    //$validate set in __construct for multi-language support
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'FiscalCity' => array(
            'className' => 'City',
            'foreignKey' => 'fiscal_city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'FiscalState' => array(
            'className' => 'State',
            'foreignKey' => 'fiscal_state_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ) ,
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        )
    );
    var $hasMany = array(
        'Deal' => array(
            'className' => 'Deal',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'NowDeal' => array(
            'className' => 'Now.NowDeal',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => array('NowDeal.is_now'=>1),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'NowBranch' => array(
            'className' => 'Now.NowBranch',
            'foreignKey' => 'company_id',
            'dependent' => false,
        )
    );

    function asertMinLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) >= $limit;
    }

    function asertMaxLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) <= $limit;
    }

    function asertLength($check, $limit){
      $value = array_values($check);
      return strlen($value[0]) == $limit;
    }

    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array(
                'rule3' => array(
                    'rule' => array('asertMaxLength', 64),
                    'message' => 'El nombre debe tener un máximo de 64 caracteres.'
                ),
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_name' => array(
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_address' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_city_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_state_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_cuit' => array(
                'rule5' => array(
                    'rule' => '_validarcuit',
                    'message' => 'El CUIT no es válido.'
                ),
                'rule4' => array(
                    'rule' => array('comparison', '<=', 39000000000),
                    'message' => 'El CUIT debe ser inferior a 39.000.000.000.'
                ),
                'rule3' => array(
                    'rule' => array('asertLength', 11),
                    'message' => 'El CUIT debe contener 11 números.'
                ),
                'rule2' => array(
                  'rule' => 'numeric',
                  'allowEmpty' => false,
                  'message' => 'Debe ingresar sólo números'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'fiscal_iibb' => array(
               'rule3' => array(
                    'rule' => array('asertMaxLength', 12),
                    'message' => 'El IIBB debe contener hasta 12 números.'
                ),
                'rule2' => array(
                  'rule' => 'numeric',
                  'allowEmpty' => false,
                  'message' => 'Debe ingresar sólo números'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                )
            ) ,
            'percepcion_iibb_caba' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Seleccione el tipo de percepción'
            ) ,
            'fiscal_cond_iva' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'fiscal_phone' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Debe ingresar sólo números, sin espacios, ni guiones'
            ) ,
            'contact_phone' => array(
                'rule' => 'alphaNumeric',
                'allowEmpty' => true,
                'message' => __l ('Debe ser un telefono valido')
            ) ,

            'fiscal_bank_cbu' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'Debe ingresar sólo números'
            ) ,

            'persona' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique el tipo de persona'
            ) ,
            'declaracion_jurada_terceros' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si es una declaración jurada de pago a terceros'
            ) ,
            'cheque_noorden' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si el pago por cheque es o no a la orden'
            ) ,
            'fiscal_bank_account' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'Debe ingresar sólo números'
            ) ,
            'slug' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'email' => array(
                'rule' => 'email',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ) ,
            'logo_image' => array(
                'rule' => '_isValidSize',
                'message' => 'Revise que haya cargado una imagen, y que tenga 158 px de ancho / 59 px de alto',
                'allowEmpty' => true,
            ),

            'url' => array(
                'rule1' => array(
                    'rule' => array(
                        'custom',
                        '/^http[s]*:\/\//'
                    ) ,
                    'message' => 'Debe ser una URL válida, comenzando con http://' ,
                    'allowEmpty' => true
                )
            ),
            'seller_id' => array (
                'rule1' => array (
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => 'Debe asignarse un/a vendedor/a a esta empresa'
                )
            ),
            /*'deal_commission_percentage' => array (
            'rule1' => array (
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l ('Required')
                ),
                'rule2' => array (
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l ('Should be a number')
                ),
        )*/
        );
        $this->moreActions = array(
            ConstMoreAction::Active => __l('Activate'),
            ConstMoreAction::Inactive => __l('Deactivate'),
        );
    }

    function createBacUserCompany($company, $user, $portal = 0, $dealStartDate = null) {
        if (empty($user)) {
            Debugger::log('Error la empresa no tiene entidad usuaria relacionada.', LOG_DEBUG);
            return FALSE;
        }
        if (empty($company)) {
            Debugger::log('createBacUserCompany Error se llamo a la funcion sin un argumento valido.', LOG_DEBUG);
            return FALSE;
        }
        $email = $user['User']['email'];
        $idUsuario = $user['User']['id'];
        $condicionesIva = array(
            'monotributo' => 5,
            'exento' => 7,
            'ri' => 1,
        );
        $sopa = new SoapClient(Configure::read('BAC.wsdl.altaModificacionClienteEmpresa'));
        $idPortal = $this->getPortalId($portal, $dealStartDate);
        $params = array(
            'idPortal' => $idPortal,
            'idUsuarioPortal' => $idUsuario,
            'login' => $company['Company']['name'],
            'email' => $email,
            'idPais' => Configure::read('BAC.Argentina'),
            'idProvincia' => Configure::read('BAC.CapitalFederal'),
            'idTipoUsuario' => Configure::read('BAC.id_tipo_usuario_empresa'),
            'razonSocial' => $company['Company']['fiscal_name'],
            'domicilioComercial' => $company['Company']['fiscal_address'],
            'localidadComercial' => $company['Company']['fiscal_state_id'],
            'codigoPostalComercial' => $company['Company']['zip'],
            'telefonoComercial' => $company['Company']['fiscal_phone'],
            'idCondicionIva' => $condicionesIva[$company['Company']['fiscal_cond_iva']],
            'idTipoDocumento' => 80, // @todo pasar a configuración CUIT
            'nroDocumento' => $company['Company']['fiscal_cuit'],
            'nroInscriptoIIBB' => $company['Company']['fiscal_iibb']
        );
        $date = strtotime($dealStartDate);
        if ($date < Configure::read('tec_dia.start_date')) {
            if ($portal == 0) {
                if ($company['Company']['bac_user_id'] != 0) {
                    $params['idUsuario'] = $company['Company']['bac_user_id'];
                }
            } else if ($portal == 1) {
                if ($company['Company']['bac_tourism_id'] != 0) {
                    $params['idUsuario'] = $company['Company']['bac_tourism_id'];
                }
            }
        } else {
            if ($company['Company']['bac_id'] != 0) {
                $params['idUsuario'] = $company['Company']['bac_id'];
            }
        }
        Debugger::log('createBacUserCompany PARAMS a enviar para company: ' . debugEncode($params), LOG_DEBUG);
        try {
            $bac_user_id = $sopa->altaModificacionClienteEmpresa($params);
        } catch (Exception $e) {
            Debugger::log('createBacUserCompany Exception intentando crear/modificar usuario: `' . $e->getMessage() . '` para comapany: ' . debugEncode($params), LOG_DEBUG);
            return false;
        }
        $this->id = $company['Company']['id'];
        if ($bac_user_id != null) {
            if ($date < Configure::read('tec_dia.start_date')) {
                $this->saveField($portal == 0 ? 'bac_user_id' : 'bac_tourism_id', $bac_user_id);
            } else {
                $this->saveField('bac_id', $bac_user_id);
            }
        }
        return $bac_user_id;
    }

    //Devuelve la ciudad de origen para un usuario especifico
  function checkUserCompany ($user_id = null) {
    $ciudad = $this->find ('first', array ('conditions' => array ('Company.user_id' => $user_id), 'recursive' => -1));
    return $ciudad;
  }

////////////////////////////////////////////////////////////////////////////////
  //REFACTOR
  ////////////////////////////////////////////////////////////////////////////////
  function findByUserId($userId) {
    return $this->find('first', array(
      'conditions' => array(
        'Company.user_id' => $userId
      ),
      'recursive' => -1
    ));
  }

  function findByUser($data) {
    if(isset($data['Company'])) { // si ya esta no la busco.
      return $data;
    }
    return $this->findByUserId($data['User']['id']);
  }

  function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Company.id' => $id
                    ),
                    'recursive' => -1
                ));
    }

    function _isValidSize($data, $required = false) {

        if ($this->data ['Company']['tmp_image_name'] != '') {

            list ($width, $height) = getimagesize($this->data ['Company']['tmp_image_name']);

            //medidas de referencia
            $sizes = array();
            $sizes['maxWidth']  = 158;
            $sizes['maxHeight'] = 59;

            //Verificamos si estamos dentro de las medidas
            return ($width == $sizes['maxWidth'] && $height == $sizes['maxHeight']);

        } else {
            return false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Custom Finds
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //Generico para todos los custom finds
    function __findCompaniesForAdminIndex ($options = array ())
    {
        //Array extra para las condiciones que se aplican a todas las búsquedas
        $this->recursive = 0;
        $this->unbindModel(
                array('hasMany' => array('Deal', 'NowDeal', 'NowBranch'))
        );
        $the_options = array();
        return $this->find('all', array_merge($the_options, $options));
    }

    function _validarcuit($S) {
    /*  determina si el dígito verificador es correcto
        Retorna true si es correcto y false si es incorrecto */
        $S = $this->data ['Company']['fiscal_cuit'];
        $v2 = (substr($S,0,1) * 5 +
            substr($S,1,1)  * 4 +
            substr($S,2,1)  * 3 +
            substr($S,3,1)  * 2 +
            substr($S,4,1)  * 7 +
            substr($S,5,1)  * 6 +
            substr($S,6,1)  * 5 +
            substr($S,7,1)  * 4 +
            substr($S,8,1)  * 3 +
            substr($S,9,1) * 2) % 11;
        $v3 = 11 - $v2;
        switch ($v3) {
        case 11 : $v3 = 0; break;
        Case 10 : $v3 = 9; break;
        }
        return substr($S,10,1) == $v3;
    }
    /* En este metodo especificamos que campos de los deals requerimos sean indexados. */
    function indexData() {
      return !empty($this->data['Company']['name'])?$this->data['Company']['name']:false;
    }

	function getIndexData(){
		return $this->field('name');
	}

    function isNow($data) {
      return $data['Company']['now_eula_ok'];
    }

    function isNowWithBranches($data) {
      return $this->isNow($data)
        && $this->hasBranches($data);
    }

    function isNowWithoutBranches($data) {
      return $this->isNow($data)
        && !$this->hasBranches($data);
    }

    function hasBranches($data) {
      return $this->NowBranch->findByCompanyId($data['Company']['id']);
    }
    
    function getSellerId($id) {
        $company = $this->findById($id);
        return $company['Company']['seller_id'];
    }
    
    function getOrderedCompaniesList() {
        $companiesList = $this->find('list');
        asort($companiesList);
        return $companiesList;
    }
    

}

