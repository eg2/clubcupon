<?php

    class UserReferral extends AppModel
    {
        var $name = 'UserReferral';

        /*
         * @param $contacts is in de form of array($email1,$email2 ...)
         */
        function saveReferralsByUser($contacts, $userId)
        {

            AppModel::setDefaultDbConnection('master');

            $referred_contacts = array();
            foreach ($contacts as $email)
            {
                array_push($referred_contacts, array('user_id'=>$userId, 'ref_email'=> $email));
            }

            $this->saveAll($referred_contacts);

        }


        //Devuelve el userId de un usuario que haya referenciado a otro, cuando este se registra
        function getReferralUser($email)
        {
            $user_referral_row = $this->find( 'first', array( 'conditions' => array( 'UserReferral.ref_email' => $email, 'UserReferral.registered <>' => '0' ) ) );
            return $user_referral_row;
        }

        //Marcamos como REGISTRADO un usuario referido, cuando este se ha dado de alta en el sitio. (ver User::afterSave)
        function updateRegisteredReferredUser($email)
        {
            $this->updateAll( array("UserReferral.registered" => '1'), array( 'UserReferral.ref_email' => $email ) );
        }



    }


