<?php

class AccountingRedeemedGrouped extends AppModel {

    public $name = 'AccountingRedeemedGrouped';
    public $alias = 'AccountingRedeemedGrouped';
    public $useTable = 'accounting_redeemed_grouped';
    public $recursive = - 1;

    function __findAllByCompanyIdGroupedByMaxPaymentDateRedeemed($conditions) {
        
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
            'max_payment_date' => $conditions['conditions']['max_payment_date'],
            'pay_by_redeemed' => $conditions['conditions']['pay_by_redeemed'],
        );
        $order = array(
            'max_payment_date DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }

}
