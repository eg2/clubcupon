<?php

class Attachment extends AppModel {

    var $name = 'Attachment';
    var $actsAs = array(
        'ImageUpload' => array(
            'allowedMime' => '*',
            'allowedExt' => '*'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function afterSave($created) {
        if (!$created && isset($this->data['Deal'])) {
            App::import('Model', 'Deal');
            $this->Deal = & new Deal();
            if (!$this->Deal->is_subdeal($this->data)) {
                $parent = $this->data['Deal'];
                $subdeals = $this->Deal->find('all', array(
                    'conditions' => array(
                        'Deal.parent_deal_id' => $this->data['Deal']['id'],
                        'Deal.id <>' => $this->data['Deal']['id']
                    ),
                    'contain' => array(
                        'Attachment'
                    )
                ));
                $newAttachment = $this->replaceAttachment($this->data);
                $this->data = $newAttachment;
                foreach ($subdeals as $subdeal) {
                    $hasReplaceAttachment = false;
                    if (isset($subdeal['Attachment']['id']) AND ! is_null($subdeal['Attachment']['id'])) {
                        $hasReplaceAttachment = true;
                    }
                    if (!is_dir('../media/Deal/' . $subdeal['Deal']['id'])) {
                        mkdir('../media/Deal/' . $subdeal['Deal']['id']);
                    }
                    copy('../media/Deal/' . $parent['id'] . '/' . $this->data['Attachment']['filename'], '../media/Deal/' . $subdeal['Deal']['id'] . '/' . $this->data['Attachment']['filename']);
                    $this->save(array(
                        'Attachment' => array(
                            'id' => $subdeal['Attachment']['id'],
                            'class' => 'Deal',
                            'foreign_id' => $subdeal['Deal']['id'],
                            'filename' => $this->data['Attachment']['filename'],
                            'dir' => 'Deal/' . $subdeal['Deal']['id'],
                            'mimetype' => $this->data['Attachment']['mimetype'],
                            'filesize' => $this->data['Attachment']['filesize'],
                            'height' => $this->data['Attachment']['height'],
                            'width' => $this->data['Attachment']['width'],
                            'description' => $this->data['Attachment']['description']
                        )
                            ), array(
                        'validate' => false,
                        'callbacks' => false
                    ));
                    foreach (glob('img/medium_thumb/Deal/' . $subdeal['Attachment']['id'] . '.*.jpg') as $filename) {
                        unlink($filename);
                    }
                    if ($hasReplaceAttachment) {
                        $subdealAttachment = $this->findById($subdeal['Attachment']['id']);
                        $idsToDelete[] = $subdeal['Attachment']['id'];
                        $newSubdealAttachment = $this->replaceAttachment($subdealAttachment);
                    }
                }
                $this->Behaviors->disable('ImageUpload');
                foreach ($idsToDelete as $id) {
                    $this->delete($id);
                }
                $this->Behaviors->enable('ImageUpload');
            }
        }
    }

    public function replaceAttachment($attachment) {
        $saveOk = $this->save(array(
            'Attachment' => array(
                'id' => null,
                'class' => 'Deal',
                'foreign_id' => $attachment['Attachment']['foreign_id'],
                'filename' => $attachment['Attachment']['filename'],
                'dir' => 'Deal/' . $attachment['Attachment']['foreign_id'],
                'mimetype' => $attachment['Attachment']['mimetype'],
                'filesize' => $attachment['Attachment']['filesize'],
                'height' => $attachment['Attachment']['height'],
                'width' => $attachment['Attachment']['width'],
                'description' => $attachment['Attachment']['description']
            )
                ), array(
            'validate' => false,
            'callbacks' => false
        ));
        if ($saveOk) {
            $newAttachment = $this->findById($this->id);
            $this->Behaviors->disable('ImageUpload');
            $this->delete($attachment['Attachment']['id']);
            $this->Behaviors->enable('ImageUpload');
            return $newAttachment;
        } else {
            return $attachment;
        }
    }

    function findImageForDeal($mixed) {
        $dealId = null;
        if (is_array($mixed) && isset($mixed['Deal']))
            $dealId = $mixed['Deal']['id'];
        if (is_array($mixed) && isset($mixed['id']))
            $dealId = $mixed['id'];
        if (!is_array($mixed))
            $dealId = $mixed;
        return $this->find('first', array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $dealId,
                        'Attachment.class = ' => 'Deal'
                    ),
                    'recursive' => - 1
        ));
    }

    function findImageForNowDeal($mixed) {
        $dealId = null;
        if (is_array($mixed) && isset($mixed['NowDeal']))
            $dealId = $mixed['NowDeal']['id'];
        if (is_array($mixed) && isset($mixed['id']))
            $dealId = $mixed['id'];
        if (!is_array($mixed))
            $dealId = $mixed;
        return $this->find('first', array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $dealId,
                        'Attachment.class = ' => 'NowDeal'
                    ),
                    'recursive' => - 1
        ));
    }

    function findAdditionalImagesForDeal($mixed) {
        $dealId = null;
        if (is_array($mixed) && isset($mixed['Deal']))
            $dealId = $mixed['Deal']['id'];
        if (is_array($mixed) && isset($mixed['id']))
            $dealId = $mixed['id'];
        if (!is_array($mixed))
            $dealId = $mixed;
        return $this->find('all', array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $dealId,
                        '`Attachment.class = ' => 'DealMultiple'
                    ),
                    'recursive' => - 1
        ));
    }

}
