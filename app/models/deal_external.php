<?php

class DealExternal extends AppModel {

    var $name = 'DealExternal';
    var $actsAs = array(
        'WhoDidIt' => array()
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        ),
        'Deal' => array(
            'className' => 'Deal',
            'foreignKey' => 'deal_id',
        ),
        'PaymentType' => array(
            'className' => 'PaymentType',
            'foreignKey' => 'payment_type_id',
        ),
        'PaymentOption' => array(
            'className' => 'PaymentOption',
            'foreignKey' => 'payment_option_id',
        ),
    );

    function pay_pending_by_days($num_send_mail, $days) {
        // $fechaReferencia = date('Y-m-d H:i:s', strtotime('-'.$days.' days'));//dias con hora y minuto
        $fechaReferencia = date('Y-m-d', strtotime('-' . $days . ' days')); //solo dias
        $DealExternal = $this->find('all', array(
            'conditions' => array(
                'DealExternal.is_send_email' => $num_send_mail,
                'PaymentOption.is_offline' => 1,
                'DealExternal.external_status' => 'P',
                // 'DealExternal.created <=' => $fechaReferencia),
                'DATE_FORMAT(DealExternal.created, "%Y-%m-%d") =' => $fechaReferencia
            ),
            'recursive' => 1,
            'fields' => array(
                'DealExternal.id',
                'DealExternal.is_send_email',
                'DealExternal.user_id',
                'DealExternal.deal_id',
                'PaymentOption.is_offline',
                'User.email',
                'User.username',
                'Deal.name'
            ), //array de nombres de campos
        ));
        // $this->log($DealExternal);
        return $DealExternal;
    }

    function pay_pending() {
        $DealExternal = $this->find('all', array(
            'conditions' => array(
                'DealExternal.is_send_email' => array(
                    0,
                    1
                ),
                'PaymentOption.is_offline' => 1
            ), //array de condiciones
            'recursive' => 1,
            'fields' => array(
                'DealExternal.id',
                'DealExternal.is_send_email',
                'DealExternal.user_id',
                'DealExternal.deal_id',
                'PaymentOption.is_offline',
                'User.email',
                'User.username',
                'Deal.name'
            ), //array de nombres de campos
        ));
        // $this->log($DealExternal);
        return $DealExternal;
    }

    public function getCountOfCouponsReturnedByDealNotNow($deal_id) {
        $fields = array(
          'SUM(DealExternal.quantity) as quantity_returned'
        );

        $conditions = array(
          'EXISTS ( select 1 from deal_users DealUser where DealExternal.id = DealUser.deal_external_id )',
          'DealExternal.deal_id' => $deal_id,
          'DealExternal.external_status = "C"',
        );
        
        $countOfCouponsReturned = $this->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'recursive' => - 1
        ));
        return $countOfCouponsReturned['0']['quantity_returned'];
    }

    /**
     * Retrieves the amount of times a user payed a particular deal
     *
     * @param int $user_id
     * @param int $deal_id
     * @return int
     */
    public function getCountDealPurchasedFromUser($user_id, $deal_id) {
        // select count(*) from deal_externals where deal_id = ? and user_id = ? tiene que ser menor o igual que buy_max_quantity_per_user
        $purchased_deals_qty = $this->find('count', array(
            'conditions' => array(
                'DealExternal.deal_id ' => $deal_id,
                'DealExternal.user_id ' => $user_id
            )
        ));
        return $purchased_deals_qty;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // REFACTOR
    // //////////////////////////////////////////////////////////////////////////////
    function findByUserIdAndDealId($user_id, $deal_id) {
        $quantity = $this->find('all', array(
            'fields' => array(
                'SUM(DealExternal.quantity) as quantity'
            ),
            'conditions' => array(
                'DealExternal.user_id' => $user_id,
                'DealExternal.deal_id' => $deal_id,
                'DealExternal.external_status' => array(
                    'P',
                    'A'
                ),
                'not' => array(
                    'DealExternal.bac_id' => null
                )
            ),
            'recursive' => - 1
        ));
        $cantidad = $quantity[0][0]['quantity'];
        return $cantidad != null || !empty($cantidad) ? $cantidad : 0;
    }

    function findQuantityGoupByUserIdDealId($user_id, $deal_id) {
        $quantity = 0;
        $dealExternal = $this->find('first', array(
            'fields' => array(
                'SUM(DealExternal.quantity) as quantity'
            ),
            'conditions' => array(
                'DealExternal.user_id' => $user_id,
                'DealExternal.deal_id' => $deal_id,
                'DealExternal.external_status' => array(
                    'P',
                    'A'
                ),
                'or' => array(
                    'DealExternal.payment_type_id' => 5000,
                    'not' => array(
                        'DealExternal.bac_id' => null
                    )
                )
            ),
            'recursive' => - 1
        ));
        if (!empty($dealExternal)) {
            $quantity = $dealExternal[0]['quantity'];
        }
        return $quantity;
    }

    function findByDealId($deal_id) {
        $quantity = $this->find('first', array(
            'fields' => array(
                'SUM(quantity) as quantity'
            ),
            'conditions' => array(
                'deal_id' => $deal_id,
                'AND' => array(
                    'OR' => array(
                        'external_status' => 'A',
                        'AND' => array(
                            'external_status' => 'P',
                            array(
                                'NOT' => array(
                                    'bac_id' => null
                                )
                            )
                        )
                    )
                )
            ),
            'recursive' => - 1
        ));
        $cantidad = $quantity[0]['quantity'];
        return $cantidad != null || !empty($cantidad) ? $cantidad : 0;
    }

    function checkIfAvalilableStock($deal) {
        $stock = $this->findByDealId($deal['id']);
        $max_limit = $deal['max_limit'];
        if ($max_limit != null) {
            $stock = intval($stock); //JIC
            $max_limit = intval($max_limit); //JIC
            return $stock < $max_limit;
        } else {
            // No hay limites
            return true;
        }
    }

    function findQuantityForCreditedOrPending($deal_id) {
        $stock = $this->find('first', array(
            'fields' => array(
                'SUM(quantity) as acreditados'
            ),
            'conditions' => array(
                'deal_id' => $deal_id,
                'AND' => array(
                    'OR' => array(
                        'external_status' => 'A',
                        'AND' => array(
                            'external_status' => 'P',
                            array(
                                'NOT' => array(
                                    'bac_id' => null
                                )
                            )
                        )
                    )
                )
            ),
            'recursive' => - 1
        ));
        return $stock[0]['acreditados'];
    }

    function findDealExternalById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

}
