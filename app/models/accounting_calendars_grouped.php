<?php

class AccountingCalendarsGrouped extends AppModel {

    public $name = 'AccountingCalendarsGrouped';
    public $alias = 'AccountingCalendarsGrouped';
    public $useTable = 'accounting_calendars_grouped';
    public $recursive = - 1;

    function __findAllByCompanyIdGroupedByCalendar($conditions) {
        
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
        );
        $order = array(
            'liquidate_accounting_calendar_id DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }

}