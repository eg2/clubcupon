<?php

class PaymentOptionPlan extends AppModel {

    var $name = 'PaymentOptionPlan';
    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );
    var $recursive = -1;
    var $belongsTo = array(
        'PaymentOption' => array(
            'className' => 'PaymentOption',
            'foreignKey' => 'payment_option_id',
        ),
        'PaymentPlan' => array(
            'className' => 'PaymentPlan',
            'foreignKey' => 'payment_plan_id',
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
    }

    function findAllByPaymentOptionId($paymentOptionId) {
        $conditions = array(
            'PaymentOptionPlan.payment_option_id' => $paymentOptionId
        );
        $order = array(
            'PaymentOptionPlan.order'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

}
