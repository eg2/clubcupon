<?php

App::import('Model', 'PosNetLayout');

class CafParser extends PosNetLayout
{

    private $caf_records;

    public function __construct($caf_file_path, $options = array()) {
        if (!file_exists($caf_file_path)) {
            throw new Exception(sprintf("couldn't read file %s", $caf_file_path));
        }

        $this->populateClassMembers($caf_file_path);
    }

    /**
     * Reads file and populates class properties
     * @param type $file
     */
    private function populateClassMembers($file) {
        $fp = fopen($file, 'r');
        $data = array();
        while ($line = fscanf($fp, "%[ -~]")) {
            /* if(strlen($line[0])!=1298)
              {
              throw new Exception("Invalid line length");
              } */
            $data[] = $line[0];
        }
        fclose($fp);
        unset($line);

        $this->file_header = array_shift($data);

        $this->block_header = array_shift($data);
        $this->file_trailer = array_pop($data);
        $this->block_trailer = array_pop($data);
        $this->caf_records = $this->cafFactory($data);
    }

    /**
     *  Returns CAF FILE as an associative array
     *
     * @return array
     */
    public function getCafRecords() {
        return $this->caf_records;
    }

    /**
     * Factory method
     *
     */
    private function cafFactory($data) {

        App::import('Model', 'CafRecord');


        $file_header = array(
                'RecordCount' => $this->getFHRecordCount(),
                'RecordType'  => $this->getFHRecordType(),
                'RefreshType' => $this->getFHRefreshType(),
                'Application' => $this->getFHApplication(),
                'RefreshGroup' => $this->getFHRefreshGroup(),
                'TapeDate' => $this->getFHTapeDate(),
                'TapeTime' => $this->getFHTapeTime(),
                'LogicalNetwork' => $this->getFHLogicalNetwork(),
                'ReleaseNumber' =>$this->getFHReleaseNumber(),
                'PartitionNumber' =>$this->getFHPartitionNumber(),
                'LastExtractDateATM'=>$this->getFHLastExtractDateATM(),
                'ImpactingStartDateATM'=> $this->getFHImpactingStartDateATM(),
                'ImpactingStartTimeATM'=> $this->getFHImpactingStartTimeATM(),
                'LastExtractDatePOS'=>$this->getFHLastExtractDatePOS(),
                'ImpactingStartDatePOS'=> $this->getFHImpactingStartDatePOS(),
                'ImpactingStartTimePOS'=> $this->getFHImpactingStartTimePOS(),
                'LastExtractDateTLR'=>$this->getFHLastExtractDateTLR(),
                'ImpactingStartDateTLR'=> $this->getFHImpactingStartDateTLR(),
                'ImpactingStartTimeTLR'=> $this->getFHImpactingStartTimeTLR(),
                'CAF-EXPNT' => $this->getFHCafExpnt(),
                'PreAuthSupport' => $this->getFHPreAuthSupport(),
                'UserField' => $this->getFHUserField(),
                'LastExtractDateTB'=>$this->getFHLastExtractDateTB(),
                'ImpactingStartDateTB'=> $this->getFHImpactingStartDateTB(),
                'ImpactingStartTimeTB'=> $this->getFHImpactingStartTimeTB(),



            );
            $block_header = array(
                'RecordCount' => $this->getBHRecordCount(),
                'RecordType' => $this->getBHRecordType(),
                'CardIssuer' => $this->getBHCardIssuer(),
                'UserField' => $this->getBHUserField(),
            );
            $block_trailer = array(
                'RecordCount' => $this->getBTRecordCount(),
                'RecordType' => $this->getBTRecordType(),
                'Amount' => $this->getBTAmount(),
                'NumberOfRecord' => $this->getBTNumberOfRecord(),
            );
            $file_trailer = array(
                'RecordCount' => $this->getFTRecordCount(),
                'RecordType' => $this->getFTRecordType(),
                'NumberOfRecord' => $this->getFTNumberOfRecord(),
                'NextFile' => $this->getFTNextFile(),
                'UserFiller' => $this->getFTUserFiller(),
            );
        $cafs = array(
            'FILE_HEADER' => $file_header,
            'BLOCK_HEADER' =>$block_header,
            'BLOCK_TRAILER' =>$block_trailer,
            'FILE_TRAILER' => $file_trailer,
            'RECORDS' => false,

        );
        foreach ($data as $caf_record) {
            $cr = new CafRecord($caf_record);
            $base = array(
                'Lenght' => $cr->getBASELength(),
                'RecordCount' => $cr->getBASERecordCount(),
                'PAN' => $cr->getBASEPAN(),
                'MemberNumber' => $cr->getBASEMemberNumber(),
                'RecordType' => $cr->getBASERecordType(),
                'CardType' => $cr->getBASECardType(),
                'FIID' => $cr->getBASEFIID(),
                'CardState' => $cr->getBASECardState(),
                'PinOffset' => $cr->getBASEPinOffset(),
                'PinOffset2' => $cr->getBASEPinOffset2(),
                'TTLWithdrawalLimit' => $cr->getBASETTLWithdrawalLimit(),
                'OfflineWithdrawalLimit' => $cr->getBASEOfflineWithdrawalLimit(),
                'TTLCashAdvanceLimit' => $cr->getBASETTLCashAdvanceLimit(),
                'OfflineCashAdvanceLimit' => $cr->getBASEOfflineCashAdvanceLimit(),
                'TTLPFLimit' => $cr->getBASETTLPFLimit(),
                'OfflinePFLimit' => $cr->getBASEOfflinePFLimit(),
                'AggregateLimit' => $cr->getBASEAggregateLimit(),
                'AggregateLimitOffline' => $cr->getBASEAggregateLimitOffline(),
                'UserFiller' => $cr->getBASEUserFiller(),
                'FirstUsedDate' => $cr->getBASEFirstUsedDate(),
                'LastResetDate' => $cr->getBASELastResetDate(),
                'ExpirationDate' => $cr->getBASEExpirationDate(),
                'EffectiveDate' => $cr->getBASEEffectiveDate(),
                'UserFiller2' => $cr->getBASEUserFiller2(),
                'SecondaryCardExpirationDate' => $cr->getBASESecondaryCardExpirationDate(),
                'SecondaryCardEffectiveDate' => $cr->getBASESecondaryCardEffectiveDate(),
                'SecondaryCardState' => $cr->getBASESecondaryCardState(),
                'UserFiller3' => $cr->getBASEUserFiller3(),
                'CreditClass' => $cr->getBASECreditClass(),
                'UserFiller4' => $cr->getBASEUserFiller4(),
                'CSMTrack1' => $cr->getBASECSMTrack1(),
                'CSMTrack2' => $cr->getBASECSMTrack2(),
                'CSMFechaVig' => $cr->getBASECSMFechaVig(),
                'FIID2' => $cr->getBASEFIID2(),
            );

            $pos = array(
                'Lenght' => $cr->getPOSLength(),
                'SegmentData' => $cr->getPOSSegmentData(),
                'TTLPurchaseLimit' => $cr->getPOSTTLPurchaseLimit(),
                'OfflinePurchaseLimit' => $cr->getPOSOfflinePurchaseLimit(),
                'TTLCashAdvanceLimit' => $cr->getPOSTTLCashAdvanceLimit(),
                'OfflineCashAdvanceLimit' => $cr->getPOSOfflineCashAdvanceLimit(),
                'TTLWithdrawalLimit' => $cr->getPOSTTLWithdrawalLimit(),
                'OfflineWithdrawalLimit' => $cr->getPOSOfflineWithdrawalLimit(),
                'TTLCCLimit' => $cr->getPOSTTLCCLimit(),
                'OfflineCCLimit' => $cr->getPOSOfflineCCLimit(),
                'TTLPFLimit' => $cr->getPOSTTLPFLimit(),
                'OfflinePFLimit' => $cr->getPOSOfflinePFLimit(),
                'TTLPCLimit' => $cr->getPOSTTLPCLimit(),
                'OfflinePCLimit' => $cr->getPOSOfflinePCLimit(),
                'UserFiller' => $cr->getPOSUserFiller(),
                'UseLimit' => $cr->getPOSUseLimit(),
                'TTLRefundCreditLimit' => $cr->getPOSTTLRefundCreditLimit(),
                'OfflineRefundCreditLimit' => $cr->getPOSOfflineRefundCreditLimit(),
                'ReasonCode' => $cr->getPOSReasonCode(),
                'LastUsed' => $cr->getPOSLastUsed(),
                'UserFiller2' => $cr->getPOSUserFiller2(),
                'IssuerProfile' => $cr->getPOSIssuerProfile(),
                'Accounts' => $cr->getAccounts(),
            );


            $records[] = array(

                'BASE' => $base,
                'POS' => $pos,
            );
        }
        $cafs['RECORDS'] = isset($records) ? $records : false;
        return $cafs;
    }

}