<?php
class PaymentPlan extends AppModel
{
    var $name = 'PaymentPlan';

    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );
    var $recursive = -1;
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(			 

        );
    }
}
