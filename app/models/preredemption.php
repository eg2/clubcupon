<?php

class Preredemption extends AppModel {

    const POSNET_CODE_IVR_PREFIX = "62797603";

    public $name = 'Preredemption';
    public $alias = 'Preredemption';


    var $actsAs = array('SoftDeletable' => array('find' => true));


    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds); // ¿ que es esto , que funcion realiza ?
    }


    function generatePosnetCode($preffix=null, $codeSize=18, $exception_array) {
       /* if(strlen($preffix) >= $codeSize) {
            Debugger::log(__METHOD__.' El tamano del prefijo es mayor o igual que el tamano del codigo a generar. Dato: $preffix:'.$preffix.', $codeSize:'.$codeSize);
            return $preffix;
        }*/

        $randSize = $codeSize - strlen($preffix);

         $posnet_code = $preffix . sprintf('%0'.$randSize.'d', mt_rand(1,pow(10, $randSize)-1));
         while (in_array($posnet_code, $exception_array) && $x < 50){
             $posnet_code = $preffix . sprintf('%0'.$randSize.'d', mt_rand(1,pow(10, $randSize)-1));
             $x++;
         }
         if(in_array($posnet_code, $exception_array)) {
             Debugger::log(__METHOD__.' El codigo posnet generado pudo aparecer en el array exception_array');
         }
        App::import("Model", "Redemption");
        $redemption = new Redemption;
        $res = $redemption->find('first', array(
                    'fields' => array('id'),
                    'conditions' => array(
                        'posnet_code' => $posnet_code,
                    ),
                    'recursive' => -1
                ));

        if(empty($res)) {

            $res = $this->find('first', array(
                    'fields' => array('id'),
                    'conditions' => array(
                        'posnet_code' => $posnet_code,
                    ),
                    'recursive' => -1
                ));

            if(empty($res)) {

                $this->getApiLoggerInstance()->log('El codigo posnet disponible.', array(
                  'preffix' => $preffix,
                  'codeSize' => $codeSize,
                  'posnet_code' => $posnet_code,
                      ), LOG_INFO, __FUNCTION__);
                return  $posnet_code;
            }

            $this->getApiLoggerInstance()->log('El codigo posnet existe en preredemption, se intentara generar uno nuevo.', array(
                'preffix' => $preffix,
                'codeSize' => $codeSize,
                'posnet_code' => $posnet_code,
                'predemption' => $res,
              ), LOG_INFO, __FUNCTION__);
            return $this->generatePosnetCode($preffix, $codeSize, $exception_array);
        }

        $this->getApiLoggerInstance()->log('El codigo posnet existe en preredemption, se intentara generar uno nuevo.', array(
            'preffix' => $preffix,
            'codeSize' => $codeSize,
            'posnet_code' => $posnet_code,
            'predemption' => $res,
                ), LOG_INFO, __FUNCTION__);
        return $this->generatePosnetCode($preffix, $codeSize, $exception_array);
    }

    /**
     * Crea tanta cantidad de registros en la tabla preredemptions, como
     *
     * @param int $deal_id
     * @param int $max_limit  valor de deal.max_limit
     * @return type
     *
     * @throws Exception when parameters are not natural numbers
     */
    function createPreRedemptions($deal_id, $max_limit ) {

        if(!$this->_isNaturalNumber($deal_id))
        {
            throw new Exception("deal_id debe ser un numero mayor a 0");
        }
        if(!$this->_isNaturalNumber($max_limit))
        {
            throw new Exception("max_limit debe ser un numero mayor a 0");
        }
        $preredemption_data = array();

        for ( $n = 0; $n  < $max_limit; $n++)
        {
          /*
         * Parece que en algun momento Configure::read('Caf.bin') vuelve vacio
         * usamos un literal
         */
            array_push($preredemption_data,array(
                   'Preredemption' => array(
                    'deals_id' => $deal_id,
                    'posnet_code' => $this->generatePosnetCode(self::POSNET_CODE_IVR_PREFIX)
                )));
        }

        $saved =  $this->saveAll($preredemption_data);
        return $saved;
    }

    /*
     * returns true on positive integers
     *
     * @param int $number positive integer number
     *
     */
    private function _isNaturalNumber($number)
    {
        $i = (int) $number;
        return (is_integer($i) && $i == $number && $i > 0);
    }


    /**
     *
     */
    public function getUnassignedPreredemptionSlot($deal_id)
    {

        $res = $this->find('first', array(
                    'fields' => array('id', 'posnet_code'),
                    'conditions' => array(
                        'deals_id' => $deal_id,
                        'is_assigned' =>0,
                    ),
                    'recursive' => -1
                ));

        return $res;
    }


    public function assignPreredemptionSlot($preredemption_id)
    {
        return  $this->save(array(
                'Preredemption' => array(
                    'id' => $preredemption_id,
                    'is_assigned' => true,
                )
            ));
    }

}
