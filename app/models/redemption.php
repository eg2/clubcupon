<?php

class Redemption extends AppModel {

    const POSNET_CODE_IVR_PREFIX = "62797603";

    public $name = 'Redemption';
    public $belongsTo = array(
        'DealUser' => array(
            'className' => 'DealUser',
            'foreignKey' => 'deal_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasOne = array(
        'Deal' => array(
            'className' => 'Deal',
            'foreignKey' => false,
            'dependent' => true,
            'fields' => array(
                'id',
                'start_date',
                'coupon_duration'
            ),
            'conditions' => array(
                'Deal.id = DealUser.deal_id'
            ),
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => false,
            'conditions' => array(
                'User.id = DealUser.user_id'
            ),
            'fields' => '',
            'order' => ''
        )
    );
    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function formatPosnetCode($redemption) {
        $posnet_code = $redemption['Redemption']['posnet_code'];
        return $posnet_code;
    }

    function isPosnetCodeCheckOk($redemption, $check) {
        return substr($redemption['Redemption']['posnet_code'], -4) == $check;
    }

    function findByDealUserId($deal_user_id, $recursive = - 1) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Redemption.deal_user_id' => $deal_user_id
                    ),
                    'contain' => array(
                        'DealUser'
                    ),
                    'recursive' => $recursive
        ));
    }

    function updateExpiredNotRedeemedPosnetCodes() {
        $this->unbindModel(array(
            'belongsTo' => array(
                'DealUser'
            )
        ));
        $hoy = date("Y-m-d H:i:s");
        $rows = $this->find('all', array(
            'conditions' => array(
                'Redemption.expired' => 0,
                'Redemption.way' => null,
            ),
            'fields' => array(
                "Redemption.deal_user_id",
                "DealUser.deal_id",
                "Deal.coupon_expiry_date"
            ),
            'joins' => array(
                array(
                    'table' => 'deal_users',
                    'alias' => 'DealUser',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'DealUser.id = Redemption.deal_user_id',
                    )
                ),
                array(
                    'table' => 'deals',
                    'alias' => 'Deal',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Deal.id = DealUser.deal_id',
                        'Deal.is_now' => 0,
                        'Deal.coupon_expiry_date <' => $hoy,
                    )
                )
            ),
            'limit' => 10000,
            'recursive' => - 1
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        foreach ($rows as $k => $row) {
            $this->updateAll(array(
                'Redemption.expired' => 1
                    ), array(
                'Redemption.deal_user_id' => $row['Redemption']['deal_user_id']
            ));
        }
        $hoursToExtend = Configure::read('deal.extended_hours_to_redeem_now');
        $todayNow = date('Y-m-d H:i:s', strtotime("-$hoursToExtend hours"));
        $rowsNow = $this->find('all', array(
            'conditions' => array(
                'Redemption.expired' => 0,
                'Redemption.way' => null,
            ),
            'fields' => array(
                "Redemption.deal_user_id",
                "DealUser.deal_id",
                "Deal.coupon_expiry_date"
            ),
            'joins' => array(
                array(
                    'table' => 'deal_users',
                    'alias' => 'DealUser',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'DealUser.id = Redemption.deal_user_id',
                    )
                ),
                array(
                    'table' => 'deals',
                    'alias' => 'Deal',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Deal.id = DealUser.deal_id',
                        'Deal.is_now' => 1,
                        'Deal.coupon_expiry_date <' => $todayNow,
                    )
                )
            ),
            'limit' => 10000,
            'recursive' => - 1
        ));
        foreach ($rowsNow as $k => $row) {
            $this->updateAll(array(
                'Redemption.expired' => 1
                    ), array(
                'Redemption.deal_user_id' => $row['Redemption']['deal_user_id']
            ));
        }
        return;
    }

    function generatePosnetCode($preffix = null, $codeSize = 18) {
        if (strlen($preffix) >= $codeSize) {
            $this->getApiLoggerInstance()->log("El tamano del prefijo es mayor o igual que el tamano del codigo a generar.", array(
                'preffix' => $preffix,
                'codeSize' => $codeSize
                    ), LOG_INFO, __FUNCTION__);
            return $preffix;
        }
        $randSize = $codeSize - strlen($preffix);
        $posnet_code = $preffix . sprintf('%0' . $randSize . 'd', mt_rand(1, pow(10, $randSize) - 1));
        App::import("Model", "Preredemption");
        $preredemption = new Preredemption;
        $res = $preredemption->find('first', array(
            'fields' => array(
                'id'
            ),
            'conditions' => array(
                'posnet_code' => $posnet_code,
            ),
            'recursive' => - 1
        ));
        if (empty($res)) {
            $this->getApiLoggerInstance()->log('El codigo posnet disponible.', array(
                'preffix' => $preffix,
                'codeSize' => $codeSize,
                'posnet_code' => $posnet_code,
                    ), LOG_INFO, __FUNCTION__);
            return $posnet_code;
        }
        $this->getApiLoggerInstance()->log('El codigo posnet existe en preredemption, se intentara generar uno nuevo.', array(
            'preffix' => $preffix,
            'codeSize' => $codeSize,
            'posnet_code' => $posnet_code,
            'predemption' => $res,
                ), LOG_INFO, __FUNCTION__);
        return $this->generatePosnetCode($preffix, $codeSize);
    }

    function create($deal_user_id, $created_by) {
        $saved = false;
        $i = 1;
        while (!$saved && $i <= 50) {
            $saved = $this->save(array(
                'Redemption' => array(
                    'deal_user_id' => $deal_user_id,
                    'created_by' => $created_by,
                    'posnet_code' => $this->fetchPosnetCode($deal_user_id)
                )
            ));
            $i++;
        }
        return $saved;
    }

    function fetchPosnetCode($deal_user_id) {
        App::import("Model", 'DealUser');
        App::import("Model", 'Deal');
        $posnet_code = $this->generatePosnetCode(self::POSNET_CODE_IVR_PREFIX);
        $deal_user = new DealUser;
        $deal = new Deal;
        $deal->Behaviors->detach('Excludable');
        $res = $deal_user->find('first', array(
            'fields' => array(
                'deal_id'
            ),
            'conditions' => array(
                'id' => $deal_user_id,
            ),
            'recursive' => - 1
        ));
        if ($res) {
            $resultado = $deal->findById($res['DealUser']['deal_id']);
            if ($resultado) {
                if ($resultado['Deal']['is_now'] == 1) {
                    App::import("Model", 'Preredemption');
                    $preredemption = new Preredemption;
                    $preredemption_res = $preredemption->getUnassignedPreredemptionSlot($res['DealUser']['deal_id']);
                    if ($preredemption_res) {
                        $posnet_code = $preredemption_res['Preredemption']['posnet_code'];
                        $preredemption->assignPreredemptionSlot($preredemption_res['Preredemption']['id']);
                    } else {
                        Debugger::log("Deal.is_now es 1, pero no se encontro preredemtions", LOG_DEBUG);
                    }
                }
            }
        }
        return $posnet_code;
    }

    function updateAsRedeemedByAdmin($dealUserIds, $modifiedBy) {
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        $this->unbindModel(array(
            'belongsTo' => array(
                'DealUser'
            )
        ));
        $this->updateAll(array(
            'Redemption.modified_by' => $modifiedBy,
            'Redemption.way' => "'admin'",
            'Redemption.redeemed' => '\'' . date('Y-m-d H:i:s.u') . '\''
                ), array(
            'Redemption.deal_user_id' => $dealUserIds
        ));
    }

    function updateAsRedeemedByWeb($deal_user_id, $modified_by, $importe_total) {
        $date = date('Y-m-d H:i:s');
        App::import('Model', 'DealUser');
        $this->DealUser = new DealUser();
        $this->DealUser->markAsUsedByCompany($deal_user_id);
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        return $this->updateAll(array(
                    'Redemption.modified_by' => "'$modified_by'",
                    'Redemption.way' => "'web'",
                    'Redemption.discounted_price' => $importe_total,
                    'Redemption.redeemed' => "'$date'"
                        ), array(
                    'Redemption.deal_user_id' => "$deal_user_id"
        ));
    }

    function updateAsRedeemedByPosnet($posnet_code, $modified_by, $redeemed, $importe_total) {
        $posnet_code = $posnet_code;
        $dealuser = $this->find('first', array(
            'fields' => array(
                'Redemption.deal_user_id'
            ),
            'conditions' => array(
                'Redemption.posnet_code' => $posnet_code
            ),
            'recursive' => - 1,
        ));
        Debugger::log(__METHOD__ . __LINE__ . ' = last_id_print' . print_r($dealuser, 1), LOG_DEBUG);
        if ($dealuser) {
            App::import('Model', 'DealUser');
            $this->DealUser = new DealUser();
            $this->DealUser->markAsUsedByCompany($dealuser['Redemption']['deal_user_id']);
            try {
                $fullDealUserRecordset = $this->DealUser->findCouponById($dealuser['Redemption']['deal_user_id']);
                $dealForEvent = $this->Deal->findDealFieldsForAccountingEventService($fullDealUserRecordset['DealUser']['deal_id']);
                App::import('Component', 'AccountingEventService');
                $AccountingEventService = new AccountingEventServiceComponent();
                $AccountingEventService->registerAccountingEvent($dealForEvent);
            } catch (Exception $e) {
                $this->getApiLoggerInstance()->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                    'errorCode' => $e->getMessage()
                ));
            }
        }
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        $return = $this->updateAll(array(
            'Redemption.modified_by' => $modified_by,
            'Redemption.modified' => '\'' . date('Y-m-d H:i:s.u') . '\'',
            'Redemption.way' => "'posnet'",
            'Redemption.discounted_price' => $importe_total,
            'Redemption.redeemed' => '\'' . $redeemed . '\'',
                ), array(
            'Redemption.posnet_code' => $posnet_code
        ));
        return $return;
    }

    function updateAsRedeemedByIvr($posnet_code, $modified_by = null) {
        $posnet_code = $posnet_code;
        $dealuser = $this->find('first', array(
            'fields' => array(
                'Redemption.id',
                'Redemption.deal_user_id',
                'DealUser.discount_amount'
            ),
            'conditions' => array(
                'Redemption.posnet_code' => "62797603" . $posnet_code
            ),
            'recursive' => 1,
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        $ahora = date('Y-m-d H:i:s');
        return $this->updateAll(array(
                    'Redemption.modified_by' => "'$modified_by'",
                    'Redemption.way' => "'ivr'",
                    'Redemption.discounted_price' => $dealuser['DealUser']['discount_amount'],
                    'Redemption.redeemed' => "'$ahora'"
                        ), array(
                    'Redemption.id' => $dealuser['Redemption']['id'],
        ));
    }

    public function get_posnets_code($options = array(
    )) {
        $results = $this->query("SELECT *
        FROM (
        SELECT
            Redemption.posnet_code as columna_de_ordenamiento,
            Redemption.posnet_code as 'Redemption.posnet_code',
            DealUser.id as 'DealUser.id',
            DealUser.created as'DealUser.created',
            Deal.id as 'Deal.id',
            Deal.start_date as 'Deal.start_date',
            Deal.coupon_duration as 'Deal.coupon_duration',
            Deal.discounted_price as 'Deal.discounted_price',
            User.username  as 'User.username'
        FROM redemptions Redemption INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
        INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
        INNER JOIN users User ON User.id = DealUser.user_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
        where Redemption.posnet_code  > 627976030000000000
        AND Redemption.way is null
        and Redemption.reported_expired = 0
        and Redemption.reported_used = 0
        and Redemption.expired = 0
        and Company.has_posnet_device = 1
        AND DATE(Deal.coupon_expiry_date) >= DATE(NOW())
        UNION ALL
        SELECT
            Preredemption.posnet_code as columna_de_ordenamiento,
            Preredemption.posnet_code as 'Redemption.posnet_code',
            'null' as 'DealUser.id',
            Deal.created as'DealUser.created',
            Deal.id as 'Deal.id',
            Deal.start_date as 'Deal.start_date',
            Deal.coupon_duration as 'Deal.coupon_duration',
            Deal.discounted_price as 'Deal.discounted_price',
            'CONSUMIDOR FINAL'  as 'User.username'
        FROM preredemptions Preredemption
        INNER JOIN deals Deal ON Deal.id = Preredemption.deals_id
            WHERE Preredemption.is_assigned = 0
            AND DATE(Deal.coupon_expiry_date) >= DATE(NOW())
        ) AS Redemption
        ORDER BY columna_de_ordenamiento asc
    ");
        if (empty($results)) {
            return false;
        }
        $posnet = array();
        foreach ($results as $posnet_item) {
            $posnet[] = array(
                'Redemption' => array(
                    'posnet_code' => $posnet_item['Redemption']['Redemption.posnet_code']
                ),
                'DealUser' => array(
                    'created' => $posnet_item['Redemption']['DealUser.created']
                ),
                'Deal' => array(
                    'id' => $posnet_item['Redemption']['Deal.id'],
                    'start_date' => $posnet_item['Redemption']['Deal.start_date'],
                    'coupon_duration' => $posnet_item['Redemption']['Deal.coupon_duration'],
                    'discounted_price' => $posnet_item['Redemption']['Deal.discounted_price']
                ),
                'User' => array(
                    'username' => $posnet_item['Redemption']['User.username']
                )
            );
        }
        return $posnet;
    }

    public function is_valid_posnet_code_by_company_id($posnet_code, $company_id) {
        if (strlen($posnet_code) != 10) {
            Debugger::log(__METHOD__ . "posnet_code " . $posnet_code . " no mide 10 caracteres ");
            return false;
        }
        $posnet_code = self::POSNET_CODE_IVR_PREFIX . $posnet_code;
        $query = "
        SELECT
            Redemption.posnet_code as columna_de_ordenamiento,
            Redemption.posnet_code as 'Redemption.posnet_code',
            Redemption.way as 'Redemption.way',
            Redemption.expired as 'Redemption.expired',
            DealUser.id as 'DealUser.id',
            DealUser.created as'DealUser.created',
            Deal.id as 'Deal.id',
            Deal.start_date as 'Deal.start_date',
            Deal.coupon_duration as 'Deal.coupon_duration',
            Deal.discounted_price as 'Deal.discounted_price',
            User.username  as 'User.username'
        FROM redemptions Redemption INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
        INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
        INNER JOIN users User ON User.id = DealUser.user_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
        where Redemption.posnet_code  > 627976030000000000
        and Redemption.reported_expired = 0
        and Redemption.reported_used = 0
        and Redemption.posnet_code ='" . $posnet_code . "'
        AND Company.id ='" . $company_id . "'
        ORDER BY columna_de_ordenamiento asc
    ";
        $results = $this->query($query);
        if (empty($results)) {
            return false;
        }
        $posnet = array();
        foreach ($results as $posnet_item) {
            $posnet[] = array(
                'Redemption' => array(
                    'posnet_code' => $posnet_item['Redemption']['Redemption.posnet_code'],
                    'way' => $posnet_item['Redemption']['Redemption.way'],
                    'expired' => $posnet_item['Redemption']['Redemption.expired'],
                ),
                'DealUser' => array(
                    'created' => $posnet_item['DealUser']['DealUser.created'],
                    'id' => $posnet_item['DealUser']['DealUser.id']
                ),
                'Deal' => array(
                    'id' => $posnet_item['Deal']['Deal.id'],
                    'start_date' => $posnet_item['Deal']['Deal.start_date'],
                    'coupon_duration' => $posnet_item['Deal']['Deal.coupon_duration'],
                    'discounted_price' => $posnet_item['Deal']['Deal.discounted_price']
                ),
                'User' => array(
                    'username' => $posnet_item['User']['User.username']
                )
            );
        }
        return $posnet;
    }

    public function updatedSent($posnet_code, $status = false) {
        $updates = array(
            'Redemption.sent' => '\'' . date('Y-m-d H:i:s.u') . '\''
        );
        switch ($status) {
            case 'new':
                $updates['Redemption.reported_new'] = true;
                break;

            case 'used':
                $updates['Redemption.reported_used'] = true;
                break;

            case 'expired':
                $updates['Redemption.reported_expired'] = true;
                break;

            default:
                break;
        }
        $this->unbindModel(array(
            'hasOne' => array(
                'Deal'
            )
        ));
        $this->unbindModel(array(
            'hasOne' => array(
                'User'
            )
        ));
        return $this->updateAll($updates, array(
                    'Redemption.posnet_code' => $posnet_code
        ));
    }

    public function findByPosnetCode($posnetCode) {
        $du = $this->find('all', array(
            'conditions' => array(
                'Redemption.posnet_code ' => $posnetCode
            )
        ));
        return $du;
    }

    public function findFirstByPosnetCode($posnetCode) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Redemption.posnet_code ' => $posnetCode
                    )
        ));
    }

    public function getCompanyNumbers($page, $pagesize) {
        $query = "
        SELECT  distinct Company.id
        FROM redemptions Redemption
        INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
        INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
        INNER JOIN users User ON User.id = DealUser.user_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
        where Redemption.posnet_code  > 627976030000000000
        AND Redemption.way is null
        and Redemption.reported_expired = 0
        and Redemption.reported_used = 0
        and Redemption.expired = 0
        ORDER BY Company.id  asc limit " . $page . "," . $pagesize;
        $results = $this->query($query);
        if (empty($results)) {
            return false;
        }
        return $results;
    }

    public function getPosnetCodes($page, $pagesize) {
        $query = "
        SELECT  Company.id, Redemption.posnet_code
        FROM redemptions Redemption
        INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
        INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
        INNER JOIN users User ON User.id = DealUser.user_id
        INNER JOIN companies Company ON Company.id = Deal.company_id
        where Redemption.posnet_code  > 627976030000000000
        AND Redemption.way is null
        and Redemption.reported_expired = 0
        and Redemption.reported_used = 0
        and Redemption.expired = 0
        ORDER BY Company.id  asc limit " . $page . "," . $pagesize;
        $results = $this->query($query);
        if (empty($results)) {
            return false;
        }
        return $results;
    }

    public function isCouponExpired($deal, $dealUser) {
        App::import('Component', 'api.ApiDealService');
        $apiDealService = & new ApiDealServiceComponent();
        $now = date('Y-m-d H:i:s');
        if ($deal['Deal']['is_now']) {
            $extendedHours = Configure::read('deal.extended_hours_to_redeem_now');
        }
        if ($deal['Deal']['company_id'] == 18) {
            $extendedHours = Configure::read('deal.extended_hours_to_redeem_imagena');
        }
        $couponExpiryDate = $apiDealService->couponExpirationDate($deal, $dealUser);
        if (is_null($deal)) {
            $this->getApiLoggerInstance()->log('No se encontro una oferta asociada al codigo posnet.', array(
                'couponExpiryDate' => $deal['Deal']['coupon_expiry_date'],
                'dealId' => $deal['Deal']['id'],
                'extendedHours' => $extendedHours,
                    ), LOG_INFO, __FUNCTION__);
            return true;
        }
        if ($deal['Deal']['is_now'] || $deal['Deal']['company_id'] == 18) {
            $couponExpiryDate = date('Y-m-d H:i:s', strtotime($deal['Deal']['coupon_expiry_date'] . '+' . $extendedHours . 'hours'));
        }
        $isExpired = strtotime($couponExpiryDate) < strtotime($now);
        if ($isExpired) {
            $this->getApiLoggerInstance()->log('Evaluando la vigencia del cupon. El cupon esta Expirado.', array(
                'coupon_expiry_date' => $deal['Deal']['coupon_expiry_date'],
                'nowDate' => $now,
                'couponExpiryDate' => $couponExpiryDate,
                'is_now_deal' => $deal['Deal']['is_now']
            ));
        }
        return $isExpired;
    }

}
