<?php

/**
 * Parses the File Header,  Block Header
 * Block Trailer and File Trailer segments
 */
abstract class PosNetLayout
{


    private $file_header;
    private $block_header;
    private $file_trailer;
    private $block_trailer;
    const RefreshFull = 0;
    const RefreshPartial = 1;
    const NotImpactRecord = 0;
    const ImpactRecord = 1;


    /**
     * Contador de Registro del FILE HEADER
     *
     * @return string
     */
    public function getFHRecordCount() {
        return substr($this->file_header, 0, 9);
    }

    /**
     * Tipo de Registro  del FILE HEADER
     *
     * @return string
     */
    public function getFHRecordType() {
        return substr($this->file_header, 9, 2);
    }
    /**
     * Tipo de Refresh del FILE HEADER
     *
     * @return int Tipo de refresh
     */
    public function getFHRefreshType() {
        $r_type= substr($this->file_header, 11, 1);
        $r_type = (int)$r_type;
        return ($r_type) ? self::RefreshPartial: self::RefreshFull;

    }

    /**
     * Código de archivo a refrescar
     *
     * @return string
     */
    public function getFHApplication() {
        return substr($this->file_header, 12, 2);
    }

    /**
     * Grupo de Refresh
     *
     * @return string
     */
    public function getFHRefreshGroup() {
        return substr($this->file_header, 14, 4);
    }

    /**
     *  Fecha del Refresh
     *
     * @return string  YYYYMMDD
     */
    public function getFHTapeDate() {
        return substr($this->file_header, 18, 8);
    }

    /**
     *  Hora del Refresh
     *
     * @return string  HHmm
     */
    public function getFHTapeTime() {
        return substr($this->file_header, 26, 4);
    }

    /**
     * Red Lógica donde se procesa el Refresh
     *
     * @return string
     */
    public function getFHLogicalNetwork() {
        return substr($this->file_header, 30, 4);
    }

    /**
     * Número de Release de Base 24
     *
     * @return string
     */
    public function getFHReleaseNumber() {
        return substr($this->file_header, 34, 2);
    }

    /**
     * Número de Partición a ser refrescada
     *
     * @return string
     */
    public function getFHPartitionNumber() {
        return substr($this->file_header, 36, 2);
    }

    /**
     * ATM Fecha del ultimo extract full realizado
     *
     * @return string YYMMDD
     */
    public function getFHLastExtractDateATM() {
        return substr($this->file_header, 38, 6);
    }

    /**
     * ATM Fecha del último registro extractado.
     *
     * @return string  YYYYMMDD
     */
    public function getFHImpactingStartDateATM() {
        return substr($this->file_header, 44, 8);
    }

    /**
     * ATM Hora del último registro extractado.
     *
     * @return string HHMMSSMMMMMM
     */
    public function getFHImpactingStartTimeATM() {
        return substr($this->file_header, 52, 12);
    }

    /**
     * POS Fecha del ultimo extract full realizado
     *
     * @return string YYMMDD
     */
    public function getFHLastExtractDatePOS() {
        return substr($this->file_header, 64, 6);
    }

    /**
     * POS Fecha del último registro extractado.
     *
     * @return string  YYYYMMDD
     */
    public function getFHImpactingStartDatePOS() {
        return substr($this->file_header, 70, 8);
    }

    /**
     * POS Hora del último registro extractado.
     *
     * @return string HHMMSSMMMMMM
     */
    public function getFHImpactingStartTimePOS() {
        return substr($this->file_header, 78, 12);
    }

    /**
     * TLR Fecha del ultimo extract full realizado
     *
     * @return string YYMMDD
     */
    public function getFHLastExtractDateTLR() {
        return substr($this->file_header, 90, 6);
    }

    /**
     * TLR Fecha del último registro extractado.
     *
     * @return string  YYYYMMDD
     */
    public function getFHImpactingStartDateTLR() {
        return substr($this->file_header, 96, 8);
    }

    /**
     * TLR Hora del último registro extractado.
     *
     * @return string HHMMSSMMMMMM
     */
    public function getFHImpactingStartTimeTLR() {
        return substr($this->file_header, 104, 12);
    }

    /**
     * Indica si se tiene que impactar los registros refrescados
     *
     * @return int
     */
    public function getFHImpactedType() {
        $impact_type =  substr($this->file_header, 116, 1);
        $impact_type = (int)$impact_type;
        return ($impact_type) ? self::ImpactRecord:self::NotImpactRecord;
    }

    /**
     * Valor para el cálculo de los montos en el CAF
     *
     * @return string
     */
    public function getFHCafExpnt() {
        return substr($this->file_header, 117, 1);
    }

    /**
     * Flag indicando si el Host soporta Preautorización.
     *
     * @return string
     */
    public function getFHPreAuthSupport() {
        return substr($this->file_header, 118, 1);
    }

    /**
     * Campo no utilizado
     *
     * @return string
     */
    public function getFHUserField() {
        return substr($this->file_header, 119, 5);
    }

    /**
     * TB : Fecha del ultimo extract full realizado
     *
     * @return string YYMMDD
     */
    public function getFHLastExtractDateTB() {
        return substr($this->file_header, 124, 6);
    }

    /**
     * TB:  Fecha del último registro extractado.
     * @return string YYYYMMDD
     */
    public function getFHImpactingStartDateTB() {
        return substr($this->file_header, 130, 8);
    }

    /**
     * TB: Hora del último registro extractado
     *
     * @return string HHMMSSMMMMMM
     */
    public function getFHImpactingStartTimeTB() {
        return substr($this->file_header, 138, 12);
    }


    /**
     * Contador de Registro
     *
     * @return string
     */
    public function getBHRecordCount() {
        return substr($this->block_header, 0, 9);
    }

    /**
     * Tipo de Registro
     *
     * @return string "BH"
     */
    public function getBHRecordType() {
        return substr($this->block_header, 9, 2);
    }

    /**
     * FIID de la Institución a refrescar. Solo se usa en refresh de PBF
     *
     * @return string
     */
    public function getBHCardIssuer() {
        return substr($this->block_header, 11, 4);
    }

    /**
     * Campo no utilizado
     *
     * @return string
     */
    public function getBHUserField() {
        return substr($this->block_header, 15, 29);
    }

    /**
     * Contador de Registro
     *
     * @return string
     */
    public function getBTRecordCount() {
        return substr($this->block_trailer, 0, 9);
    }

    /**
     * Tipo de Registro
     *
     * @return string "BT"
     */
    public function getBTRecordType() {
        return substr($this->block_trailer, 9, 2);
    }
    /**
     * Sumatoria de monto para refresh de PBF.
     *
     * @return string
     */
    public function getBTAmount() {
        return substr($this->block_trailer, 11, 18);
    }

    /**
     * Cantidad de registros sin Header y Trailer.
     *
     * @return string
     */
    public function getBTNumberOfRecord() {
        return substr($this->block_trailer, 29, 9);
    }

    /**
     * Contador de Registro
     *
     * @return string
     */
    public function getFTRecordCount() {
        return substr($this->file_trailer, 0, 9);
    }

    /**
     * Tipo de Registro
     *
     * @return string "FT"
     */
    public function getFTRecordType() {
        return substr($this->file_trailer, 9, 2);
    }

    /**
     * Cantidad de registros sin Header y Trailer.
     *
     * @return string
     */
    public function getFTNumberOfRecord() {
        return substr($this->file_trailer, 11, 9);
    }

    /**
     *  Flag que indica si existe otro archivo o cinta a procesar o es el último archivo o cinta.
     *
     * @return boolean
     */
    public function getFTNextFile() {
        $next_file = substr($this->file_trailer, 20, 1);
        return (boolean) $next_file;
    }

    /**
     * Campo no utilizado.
     *
     * @return string
     */
    public function getFTUserFiller() {
        return substr($this->file_trailer, 21, 3);
    }

}