<?php

/**
 * Description of Process
 *
 * @author msaladino
 */
class ProcessRun extends AppModel {

    var $name = 'ProcessRun';

    function getRunningById($processRunId) {
        $conditions = array('id' => $processRunId);
        $result = $this->find('first', array('conditions' => $conditions));
        return empty ($result) ? false : $result['ProcessRun'];
    }

    function getRunningByName($name) {
        $conditions = array('name' => array ($name, 'LOCK'));
        $order = array('id DESC');
        $result = $this->find('first', array('conditions' => $conditions, 'order' => $order));
        return empty ($result) ? false : $result['ProcessRun'];
    }

    function isRunningById($processRunId) {
        $conditions = array('id' => $processRunId);
        $result = $this->find('first', array('conditions' => $conditions));
        return !empty ($result) && $result['ProcessRun']['is_running'];
    }

    function isRunningByName($name) {
        $conditions = array('name' => array ($name, 'LOCK'));
        $order = array('id DESC');
        $result = $this->find('first', array('conditions' => $conditions, 'order' => $order));
        return !empty ($result) && $result['ProcessRun']['is_running'];
    }

    function startRunning($name) {
        $data = array('name' => $name, 'is_running' => 1);
        $this->save($data);
        return $this->getLastInsertID();
    }

    function stopRunning($processRunId) {
        $this->updateAll(array('is_running' => 0, 'modified' => '\'' . date('Y-m-d H:i:s.u') . '\''), array('id' => $processRunId));
    }

}
