<?php
//TODO:  add comments to Class methods
class CafRecord
{


    private $record;

    public function __construct($caf_records) {
        $this->record = $caf_records;
    }

    public function getBASELength() {
        return substr($this->record, 0, 4);
    }

    public function getBASERecordCount() {
        return substr($this->record, 4, 9);
    }

    public function getBASEPAN() {
        return substr($this->record, 13, 19);
    }

    public function getBASEMemberNumber() {
        return substr($this->record, 32, 3);
    }

    public function getBASERecordType() {
        return substr($this->record, 35, 1);
    }

    public function getBASECardType() {
        return substr($this->record, 36, 2);
    }

    public function getBASEFIID() {
        return substr($this->record, 38, 4);
    }

    public function getBASECardState() {
        return substr($this->record, 42, 1);
    }

    public function getBASEPinOffset() {
        return substr($this->record, 43, 16);
    }

    public function getBASEPinOffset2() {
        return substr($this->record, 59, 16);
    }

    public function getBASETTLWithdrawalLimit() {
        return substr($this->record, 75, 12);
    }

    public function getBASEOfflineWithdrawalLimit() {
        return substr($this->record, 87, 12);
    }

    public function getBASETTLCashAdvanceLimit() {
        return substr($this->record, 99, 12);
    }

    public function getBASEOfflineCashAdvanceLimit() {
        return substr($this->record, 111, 12);
    }

    public function getBASETTLPFLimit() {
        return substr($this->record, 123, 12);
    }

    public function getBASEOfflinePFLimit() {
        return substr($this->record, 135, 12);
    }

    public function getBASEAggregateLimit() {
        return substr($this->record, 147, 12);
    }

    public function getBASEAggregateLimitOffline() {
        return substr($this->record, 159, 12);
    }

    public function getBASEUserFiller() {
        return substr($this->record, 171, 96);
    }

    public function getBASEFirstUsedDate() {
        return substr($this->record, 267, 6);
    }

    public function getBASELastResetDate() {
        return substr($this->record, 273, 6);
    }

    public function getBASEExpirationDate() {
        return substr($this->record, 279, 4);
    }

    public function getBASEEffectiveDate() {
        return substr($this->record, 283, 4);
    }

    public function getBASEUserFiller2() {
        return substr($this->record, 287, 1);
    }

    public function getBASESecondaryCardExpirationDate() {
        return substr($this->record, 288, 4);
    }

    public function getBASESecondaryCardEffectiveDate() {
        return substr($this->record, 292, 4);
    }

    public function getBASESecondaryCardState() {
        return substr($this->record, 296, 1);
    }

    public function getBASEUserFiller3() {
        return substr($this->record, 297, 33);
    }

    public function getBASECreditClass() {
        return substr($this->record, 330, 2);
    }

    public function getBASEUserFiller4() {
        return substr($this->record, 332, 150);
    }

    public function getBASECSMTrack1() {
        return substr($this->record, 482, 82);
    }

    public function getBASECSMTrack2() {
        return substr($this->record, 564, 40);
    }

    public function getBASECSMFechaVig() {
        return substr($this->record, 604, 8);
    }

    public function getBASEFIID2() {
        return substr($this->record, 612, 4);
    }

    public function getSegment() {
        $length = substr($this->record, 616, 4);
        if ($length != '0356') {
            throw new Exception("Not Supported. POS Segment detected");
        }
    }

    public function getPOSLength() {
        return substr($this->record, 616, 4);
    }

    public function getPOSSegmentData() {
        return substr($this->record, 620, 12);
    }

    public function getPOSTTLPurchaseLimit() {
        return substr($this->record, 632, 12);
    }

    public function getPOSOfflinePurchaseLimit() {
        return substr($this->record, 644, 12);
    }

    public function getPOSTTLCashAdvanceLimit() {
        return substr($this->record, 656, 12);
    }

    public function getPOSOfflineCashAdvanceLimit() {
        return substr($this->record, 668, 12);
    }

    public function getPOSTTLWithdrawalLimit() {
        return substr($this->record, 680, 12);
    }

    public function getPOSOfflineWithdrawalLimit() {
        return substr($this->record, 692, 12);
    }

    public function getPOSTTLCCLimit() {
        return substr($this->record, 704, 12);
    }

    public function getPOSOfflineCCLimit() {
        return substr($this->record, 716, 12);
    }

    public function getPOSTTLPFLimit() {
        return substr($this->record, 728, 12);
    }

    public function getPOSOfflinePFLimit() {
        return substr($this->record, 740, 12);
    }

    public function getPOSTTLPCLimit() {
        return substr($this->record, 752, 12);
    }

    public function getPOSOfflinePCLimit() {
        return substr($this->record, 764, 12);
    }

    public function getPOSUserFiller() {
        return substr($this->record, 776, 144);
    }

    public function getPOSUseLimit() {
        return substr($this->record, 920, 4);
    }

    public function getPOSTTLRefundCreditLimit() {
        return substr($this->record, 924, 12);
    }

    public function getPOSOfflineRefundCreditLimit() {
        return substr($this->record, 936, 12);
    }

    public function getPOSReasonCode() {
        return substr($this->record, 948, 1);
    }

    public function getPOSLastUsed() {
        return substr($this->record, 949, 6);
    }

    public function getPOSUserFiller2() {
        return substr($this->record, 955, 1);
    }

    public function getPOSIssuerProfile() {
        return substr($this->record, 956, 16);
    }

    public function getAccounts() {
        $accounts = array(
            'AccountsLength' => substr($this->record, 972, 4),
            'AccountsCount' => (int) substr($this->record, 976, 2),
            'Accounts' => array(),
        );

        if ($accounts['AccountsCount'] > 0) {
            for ($i = 0; $i < $accounts['AccountsCount']; $i++) {
                $accounts['Accounts'][] = array(
                    'AccountType' => substr($this->record, 978 + ($i * 34), 2),
                    'AccountNumber' => substr($this->record, 980 + ($i * 34), 19),
                    'AccountStatus' => substr($this->record, 999 + ($i * 34), 1),
                    'AccountDesc' => substr($this->record, 1000 + ($i * 34), 10),
                    'AccountCorp' => substr($this->record, 1010 + ($i * 34), 1),
                    'AccountQual' => substr($this->record, 1011 + ($i * 34), 1),
                );
            }
        }
        return $accounts;
    }

}