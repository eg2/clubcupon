<?php
class UserProfile extends AppModel
{
    var $name = 'UserProfile';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'Language' => array(
            'className' => 'Language',
            'foreignKey' => 'language_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'Neighbourhood' => array(
            'className' => 'Neighbourhood',
            'foreignKey' => 'neighbourhood_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
        'Region' => array(
            'className' => 'Region',
            'foreignKey' => 'region_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) ,
    );
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'dob' => array (
              'rule1' => array (
                'rule' => 'notempty',
                'allowEmpty' => false,
               	'message' => __l('Required')
              ),
              'rule2' => array (
                'rule' => 'date',
                'message' => __l('Must be a valid date'),
                'allowEmpty' => true
              ),
              'rule3' => array (
                'rule' => array (
                  '_checkCurrentDate'
                ) ,
                'message' => __l('DOB should be lesser than current date')
              )
            ),
            'first_name' => array (
              'rule1' => array (
                'rule' => 'notempty',
                'message' => __l ('Required')
              ),
              'rule5' => array (
                'rule' => array (
                  'maxLength',
                  '100'
                ),
                'message' => __l ('Debe tener un máximo de 100 caracteres.')
              ),
              'rule4' => array (
                'rule' => array ('custom', '/^[a-z0-9 ]*$/i'),
                'message' => __l ('Must be a valid character')
              ),
            ),
            'last_name' => array(
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                ),
                'rule5' => array(
                                    'rule' => array(
                                        'maxLength',
                                        '100'
                                    ) ,
                                    'message' => __l('Debe tener un máximo de 100 caracteres.')
                                ) ,
                'rule4' => array(
                    'rule' => array ('custom', '/^[a-z0-9 ]*$/i'),
                    'message' => __l('Must be a valid character')
                ) ,

            ),
            'dni' => array(
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                ),
                 'rule4' => array(
                    'rule' => 'numeric',
                    'message' => 'Ingresá sólo números.'
                ) ,
            ),
            /* 'address_number'=>array(
              'rule1' => array (
                'rule' => 'notempty',
                'message' => __l ('Required')
              ),
              'rule4' => array(
                'rule' => 'numeric',
                'message' => __l ('Should be a number')
              ),
            ), */
            /* 'address_street'=>array(
              'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
              'message'=>__l('La calle debe ser letras y números.')
            ), */
            'zip_code'=>array(
               'rule3' => array(
                        'rule' => array(
                            '_checkZipCode'
                        ) ,
                        'message' => 'El código postal debe poseer letras y números. O sólo números.'
                    )
            ),
            'cell_carrier' => array (
                'rule3' => array (
                    'rule' => array(
                        '_checkCell_carrier'
                    ),
                    // 'rule' => array ('inList', array_keys (Configure::read ('SMS.celco'))),
                    'message' => 'Seleccione un proveedor',
                    'allowEmpty' => true
                )
            ),
            'cell_prefix' => array (
                'rule1' => array (
                    'rule' => array (
                        '_checkCell_numbers'
                    ),
                    'message' => 'Ambos campos deben ser un numero',
                    'allowEmpty' => true
                ),
                'rule3' => array (
                    'rule' => array (
                        '_checkCell_lengths'
                    ),
                    'message' => 'El prefijo y el número deben tener 10 dígitos.',
                    'allowEmpty' => true
                )
            ),
            'cell_number' => array (
                'rule1' => array (
                    'rule' => array (
                        '_checkCell_numbers'
                    ),
                    'message' => 'Ambos campos deben ser un numero',
                    'allowEmpty' => true
                ),
                'rule3' => array (
                    'rule' => array (
                        '_checkCell_lengths'
                    ),
                    'message' => 'El prefijo y el número deben tener 10 dígitos.',
                    'allowEmpty' => true
                )
            ),
            'is_sms_enabled' => array (
                'rule2' => array (
                    'rule' => array (
                        '_checkCell_existence'
                    ),
                    'message' => 'SI desea recibir ofertas por SMSs debe proveer un numero válido',
                    'allowEmpty' => true
                ),
            ),
            'country_id' => array (
              'rule1' => array (
                'rule' => 'notempty',
                'message' => __l ('Required')
              )
            ),
            'state_id' => array (
              'rule1' => array (
                'rule' => 'notempty',
                'message' => __l ('Required')
              )
            ),
            /*
              'city_id' => array (
                'rule1' => array (
                  'rule' => 'notempty',
                  'message' => __l ('Required')
                )
              ),
             */

      'paypal_account' => array(
				'rule' => 'email',
				'message' => __l('Must be a valid email'),
                'allowEmpty' => true
			)
        );
    }

    function _checkCell_lengths ()
    {
      @$cell_prefix = trim ($this->data [$this->name]['cell_prefix']);
      @$cell_number = trim ($this->data [$this->name]['cell_number']);

      $cell = ($cell_number == '' ? '' : $cell_prefix . $cell_number);

      return (($cell == '') || (strlen ($cell) == 10));
    }

    function _checkCell_numbers ()
    {
      @$cell_prefix = trim ($this->data [$this->name]['cell_prefix']);
      @$cell_number = trim ($this->data [$this->name]['cell_number']);

      $cell = $cell_number == '' ? '' : $cell_prefix . $cell_number;

      return (($cell == '') || ctype_digit ($cell));
    }

    function _checkCell_existence ()
    {
      @$cell_prefix    = trim ($this->data [$this->name]['cell_prefix'   ]);
      @$cell_number    = trim ($this->data [$this->name]['cell_number'   ]);
      @$is_sms_enabled =       $this->data [$this->name]['is_sms_enabled'] ;

      $cell = $cell_number == '' ? '' : $cell_prefix . $cell_number;

      return !($is_sms_enabled && ($cell == ''));
    }

    function _checkCell_carrier ()
    {
      $cell_number   = trim ($this->data [$this->name]['cell_number' ]);
      $cell_carrier  =       $this->data [$this->name]['cell_carrier'] ;
      $cell_carrires = array_keys (Configure::read ('SMS.celco'));

      return (($cell_number == '') || (in_array ($cell_carrier, $cell_carrires)));
    }

    function _checkZipCode()
    {
      $zip_code = $this->data[$this->name]['zip_code'];
      $pattern = "/^[A-Za-z0-9]*[0-9][A-Za-z0-9]*$/";
      return preg_match($pattern,$zip_code) === 1;
    }
    function _checkCurrentDate()
    {
        if (strtotime($this->data[$this->name]['dob']) <= strtotime(date('Y-m-d'))) return true;
        return false;
    }

    //Devuelve la ciudad de origen para un usuario especifico
    function checkUserCity ($user_id = null) {
        $ciudad = $this->find ('first', array ('conditions' => array ('UserProfile.user_id' => $user_id), 'recursive' => -1));
        return $ciudad;
    }
}

