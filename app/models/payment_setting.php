<?php

class PaymentSetting extends AppModel {

    var $name = 'PaymentSetting';
    var $displayField = 'name';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

}
