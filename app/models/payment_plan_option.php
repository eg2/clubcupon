<?php
class PaymentPlanOption extends AppModel
{
    var $name = 'PaymentPlanOption';

    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );
    var $recursive = -1;
    var $belongsTo = array(
      'PaymentPlan' => array(
        'className' => 'PaymentPlan',
        'foreignKey' => 'payment_plan_id',
      )
    );


    //The Associations below have been created with all possible keys, those that are not needed can be removed
    function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->validate = array(			 

        );
    }
    
    function getNumInstallments($payment_plan_option_id) {
    $num_quota = 
      $this->read('payment_installments',
              $payment_plan_option_id);
    return $num_quota['PaymentPlanOption']['payment_installments'];
    }
}
