<?php

class AccountingCampaignItem extends AppModel {

    public $name = 'AccountingCampaignItem';
    public $alias = 'AccountingCampaignItem';
    public $useTable = 'accounting_campaign_items';
    public $recursive = - 1;

    function findAllByCompanyIdAndCampaignCode($companyId, $campaignCode) {
        $conditions = array(
            'deal_company_id' => $companyId,
            'deal_campaign_code' => $campaignCode
        );
        $order = array(
            'liquidate_id DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

}