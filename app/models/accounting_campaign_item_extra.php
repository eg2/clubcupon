<?php

class AccountingCampaignItemExtra extends AppModel {

    public $name = 'AccountingCampaignItemExtra';
    public $alias = 'AccountingCampaignItemExtra';
    public $useTable = 'accounting_campaign_item_extras';
    public $recursive = - 1;

    function findByCompanyIdAndLiquidateId($companyId, $liquidateId) {
        $conditions = array(
            'deal_company_id' => $companyId,
            'liquidate_id ' => $liquidateId
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

}