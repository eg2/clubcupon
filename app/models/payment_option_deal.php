<?php

class PaymentOptionDeal extends AppModel {

    var $name = 'PaymentOptionDeal';
    var $belongsTo = array(
        'PaymentOption' => array(
            'className' => 'PaymentOption',
            'foreignKey' => 'payment_option_id'
        ),
        'PaymentPlan' => array(
            'classname' => 'PaymentPlan',
            'foreignKey' => 'payment_plan_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    private function deleteSql($dealStatusIdList, $accountList, $cityId) {
        $idListCsv = implode(',', $dealStatusIdList);
        $accountListCsv = implode(',', $accountList);
        $cityCondition = $cityId ? " and city_id = {$cityId} " : "";
        $subselect = " select id from deals where deal_status_id in ({$idListCsv}) and is_tourism in ({$accountListCsv}) {$cityCondition}";
        $delete = " delete from payment_option_deals where deal_id in ({$subselect}) ";
        return $delete;
    }

    function deleteByDealStatusIdListAccountListAndCityId($dealStatusIdList, $accountList, $cityId) {
        $deleted = false;
        if (!empty($dealStatusIdList) && !empty($accountList)) {
            $sql = $this->deleteSql($dealStatusIdList, $accountList, $cityId);
            $this->query($sql);
            $deleted = $this->getAffectedRows();
        }
        return $deleted;
    }

    private function generateSql($dealStatusIdList, $accountList, $paymentOptionIdList, $cityId) {
        $idListCsv = implode(',', $dealStatusIdList);
        $accountListCsv = implode(',', $accountList);
        $paymentOptionIdListCsv = implode(',', $paymentOptionIdList);
        $cityCondition = $cityId ? " and Deal.city_id = {$cityId} " : "";
        $generate = " insert into payment_option_deals (                          "
                . " 	deal_id,                                                  "
                . " 	payment_option_id,                                        "
                . " 	payment_plan_id                                           "
                . " )                                                             "
                . " select                                                        "
                . "    Deal.id as deal_id,                                        "
                . "    PaymentOption.id as payment_option_id,                     "
                . "    (select                                                    "
                . "        payment_plan_id                                        "
                . "    from                                                       "
                . "        payment_option_plans as PaymentOptionPlan              "
                . "    where                                                      "
                . "        PaymentOptionPlan.payment_option_id = PaymentOption.id "
                . "    order by                                                   "
                . "        PaymentOptionPlan.`order`,                             "
                . "        PaymentOptionPlan.id                                   "
                . "    limit 	                                                  "
                . "        1) as payment_plan_id                                  "
                . " from                                                          "
                . "    deals as Deal,                                             "
                . "    payment_options as PaymentOption                           "
                . " where Deal.deal_status_id in ({$idListCsv})                   "
                . "    and Deal.is_tourism in ({$accountListCsv})                 "
                . "    and PaymentOption.id in ({$paymentOptionIdListCsv})        "
                . "    {$cityCondition}                                           ";
        return $generate;
    }

    function generate($dealStatusIdList, $accountList, $paymentOptionIdList, $cityId) {
        $generated = false;
        if (!empty($dealStatusIdList) && !empty($accountList) && !empty($paymentOptionIdList)) {
            $sql = $this->generateSql(
                    $dealStatusIdList, $accountList, $paymentOptionIdList, $cityId
            );
            $this->query($sql);
            $generated = $this->getAffectedRows();
        }
        return $generated;
    }

}
