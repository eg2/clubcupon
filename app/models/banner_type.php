<?php

class BannerType extends AppModel {

    var $name = 'BannerType';
    var $hasMany = 'Banner';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'label' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'width' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'height' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
                ));
    }
    
    //Devuelve el ando y alto correspondientes a un tipo de banner
    function retrieveBannerSizes($bannerType){
        
        $sizes = array();
        $sizes ['maxWidth']  = $this->field('width', array('id'=>$bannerType));
        $sizes ['maxHeight'] = $this->field('height',array('id'=>$bannerType));
        
        return $sizes;
        
    }

}