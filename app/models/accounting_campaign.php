<?php

class AccountingCampaign extends AppModel {

    public $name = 'AccountingCampaign';
    public $alias = 'AccountingCampaign';
    public $useTable = 'accounting_campaigns';
    public $recursive = - 1;

    function __findAllByCompanyId($conditions) {
        return $this->findAllByCompanyId($conditions);
    }
    
    function findAllByCompanyId($conditions) {
        
        $conditionsForFind = array(
            'deal_company_id' => $conditions['conditions']['company_id'],
        );
        $order = array(
            'deal_campaign_code DESC'
        );
        return $this->find('all', array(
                    'conditions' => $conditionsForFind,
                    'order' => $order,
                    'limit' => $conditions['limit'],
                    'page' => $conditions['page'],
        ));
    }

}