<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7847 $
 * @modifiedby    $LastChangedBy: renan.saddam $
 * @lastmodified  $Date: 2008-11-08 08:24:07 +0530 (Sat, 08 Nov 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */

class AppModel extends Model {

    // indica la base de datos por defecto a utilizar para cualquier operacion.
    static $db_dafault_name = 'master';
    var $actsAs = array(
        'Containable',
        'RowObject',
    );

    /**
     * metodo para indicar en tiempo de ejecucion la conexion por defecto para todos los modelos
     * @param string $conn_name nombre de la conexion a utilizar
     */
    static function setDefaultDbConnection($conn_name) {

    }

    /**
     * Indica el nombre de la conexion a utilizar
     * @param string $conn_name nombre de la conexion a utilizar
     */
    function setDbConnection($conn_name) {
        if ($conn_name != '') {
            $this->useDbConfig = $conn_name;
            return true;
        } else {
            trigger_error('No se indicó un nombre de conexión a utilizar');
        }
    }

    function getIdHash($ids = null) {
        return md5($ids . Configure::read('Security.salt'));
    }

    function isValidIdHash($ids = null, $hash = null) {
        return (md5($ids . Configure::read('Security.salt')) == $hash);
    }

    function findOrSaveAndGetId($name, $d_name = null) {
        $d_name = (empty($d_name)) ? $this->name : $d_name;
        $findExist = $this->find('first', array('conditions' => array('name' => $name), 'fields' => array('id'), 'recursive' => -1));

        if (empty($findExist)) {
            $this->data [$d_name]['name'] = $name;
            $this->save($this->data [$d_name]);

            return $this->getLastInsertId();
        }

        return $findExist [$this->name]['id'];
    }

    function _isValidCaptcha() {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';
        $img = new Securimage();
        return $img->check($this->data[$this->name]['captcha']);
    }

    function _checkForPrivacy($type, $username, $logged_in_user, $is_boolean=false) {
        $is_show = true;
        App::import('Model', 'UserPermissionPreference');
        $privacy_model_obj = new UserPermissionPreference();
        $privacy = $privacy_model_obj->getUserPrivacySettings($username);
        if ($privacy['UserPermissionPreference'][$type] == ConstPrivacySetting::Users and !$logged_in_user) {
            $is_show = false;
        } else if ($privacy['UserPermissionPreference'][$type] == ConstPrivacySetting::Nobody) {
            $is_show = false;
        } else if ($privacy['UserPermissionPreference'][$type] == ConstPrivacySetting::Friends) {
            // To write user friends lists in config
            App::import('Model', 'UserFriend');
            $user_friend_obj = new UserFriend();
            $is_show = $user_friend_obj->checkIsFriend($logged_in_user, $username);
        } else if ($is_boolean) {
            $is_show = $privacy['UserPermissionPreference'][$type];
        }
        return $is_show;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Find Dispatcher
    ////////////////////////////////////////////////////////////////////////////
    function find($type, $options = array()) {
        $options = array_cleanup($options);

        $method = null;
        $do_count = false;

        if (is_string($type)) {
            if (((strtolower($type) == 'count') && isset($options ['type']) && is_string($options ['type'])) ||
                    (isset($options ['type']) && is_string($options ['type']) && (strtolower($options ['type']) == 'count'))) {
                if (strtolower($type) == 'count') {
                    $type = $options ['type'];
                }
                unset($options ['type']);
                $method = sprintf('__find%s', Inflector::camelize($type));

                if (method_exists($this, $method . 'Count')) {
                    $method .= 'Count';
                } else if (method_exists($this, $method)) {
                    $do_count = true;
                } else {
                    $method = null;
                }
            } else {
                if (substr ($type, -4) == '_row') {
                  $method = sprintf('__find%s', Inflector::camelize(substr ($type, 0, -4)));
                } else {
                  $method = sprintf('__find%s', Inflector::camelize(        $type        ));
                }

                if (!method_exists($this, $method)) {
                    $method = null;
                }
            }
        }

        if (isset($method)) {
            $this->findQueryType = $type;
            @$this->findQueryNesting++;
            $result = $this->{$method}($options);
        } else {
            $result = parent::find($type, $options);
        }

        if ($do_count) {
            $result = count($result);
        }
        return $result;
    }

  function unbindAll ($params = array ()) {
    foreach ($this->__associations as $ass) {
      if (!empty ($this->{$ass})) {
        $this->__backAssociation [$ass] = $this->{$ass};
        if (isset ($params[$ass])) {
          foreach ($this->{$ass} as $model => $detail) {
            if (!in_array ($model, $params [$ass])) {
              $this->__backAssociation = array_merge ($this->__backAssociation, $this->{$ass});
              unset ($this->{$ass} [$model]);
            }
          }
        } else {
          $this->__backAssociation = array_merge ($this->__backAssociation, $this->{$ass});
          $this->{$ass} = array ();
        }
      }
    }
    return true;
  }
  public function getPortalId($portal, $dealStartDate){
    
    if (empty($dealStartDate)) {
      $dealStartDate = time();
      $date = strtotime($dealStartDate);
    } else {
      $date = strtotime($dealStartDate);
    }
    if ($portal == 0 && $date < Configure::read('tec_dia.start_date')){
      $portalId = Configure::read ('BAC.id_portal_old');
    }
    elseif ($date < Configure::read('tec_dia.start_date')) {
      $portalId = Configure::read('BAC.bac_tourism_id_old');
    }
    elseif ($date >= Configure::read('tec_dia.start_date')){
      $portalId = Configure::read('BAC.id_portal'); 
    }
    return $portalId;
  }
  public function getProductIdComision($portal, $dealStartDate) {
    if (empty($dealStartDate)) {
      $dealStartDate = time();
      $date = strtotime($dealStartDate);
    } else {
      $date = strtotime($dealStartDate);
    }
    if ($portal == 0 && $date < Configure::read('tec_dia.start_date')) {
	$productId = Configure::read('BAC.id_producto_comision_cmd_old');
    } elseif ($date < Configure::read('tec_dia.start_date')) {
        $productId = Configure::read('BAC.bac_tourism_id_producto_comision_cmd_old');
    } elseif ($date >=Configure::read('tec_dia.start_date')) {
	$productId = Configure::read('BAC.id_producto_comision_cmd');
    }
    return $productId;
  }

  function getApiLoggerInstance() {
    App::import('Component', 'api.ApiBase');
    return ApiLogger::getInstance();
  }
  
}