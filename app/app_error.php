<?php

class AppError extends ErrorHandler {

  function error404($params) {
    $Dispatcher = new Dispatcher();
    if(!isset($params['url'])){
      $params['url'] = '';
    }
    $Dispatcher->dispatch('/ciudad-de-buenos-aires/errors/error404/', array('broken-url' => '/' . $params['url']));
  }

  function nowError404($params) {
    $Dispatcher = new Dispatcher();
    if(!isset($params['url'])){
      $params['url'] = '';
    }
    $Dispatcher->dispatch('/now/now_errors/error404/', array('broken-url' => '/' . $params['url']));
  }

}

