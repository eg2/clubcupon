<?php

/**
 * Description of Precision
 *
 * @author chulo
 */
class Precision {

    static $decimalPrecision = 2;

    static function equal($first, $second) {
        if (function_exists('bccomp')) {
            return bccomp($first, $second, self::$decimalPrecision) === 0;
        } else {
            return ($first == $second);
        }
    }

    static function greater($first, $second) {
        if (function_exists('bccomp')) {
            return bccomp($first, $second, self::$decimalPrecision) === 1;
        } else {
            return ($first > $second);
        }
    }

    static function greaterEqual($first, $second) {
        return self::greater($first, $second) || self::equal($first, $second);
    }

    static function less($first, $second) {
        if (function_exists('bccomp')) {
            return bccomp($first, $second, self::$decimalPrecision) === -1;
        } else {
            return ($first < $second);
        }
    }

    static function lessEqual($first, $second) {
        return self::less($first, $second) || self::equal($first, $second);
    }

    static function mul($first, $second) {
        if (function_exists('bcmul')) {
            return bcmul($first, $second, self::$decimalPrecision);
        }
        return $first * $second;
    }

    static function div($first, $second) {
        if (function_exists('bcdiv')) {
            return bcdiv($first, $second, self::$decimalPrecision);
        }
        return $first / $second;
    }

    static function add($first, $second) {
        if (function_exists('bcadd')) {
            return bcadd($first, $second, self::$decimalPrecision);
        }
        return $first + $second;
    }

    static function sub($first, $second) {
        if (function_exists('bcsub')) {
            return bcsub($first, $second, self::$decimalPrecision);
        }
        return $first - $second;
    }

    static function percentageOf($amount, $percentage, $is100 = true) {
        if ($is100) {
            $percentage = self::div($percentage, '100');
        }
        return self::mul($amount, $percentage);
    }

}

?>
