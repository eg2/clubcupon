<?php
//============================================================+
// File name   : tcpdf_config.php
// Begin       : 2004-06-11
// Last Update : 2010-12-16
//
// Description : Configuration file for TCPDF.
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Configuration file for TCPDF.
 * @author Nicola Asuni
 * @package com.tecnick.tcpdf
 * @version 4.9.005
 * @since 2004-10-27
 */

// If you define the constant K_TCPDF_EXTERNAL_CONFIG, the following settings will be ignored.

if (!defined ('K_TCPDF_EXTERNAL_CONFIG'))
  {
    $_SERVER ['DOCUMENT_ROOT'] = Configure::read ('tcpdf_document_root');  // DOCUMENT_ROOT fix for IIS Webserver
    define ('K_PATH_MAIN',       Configure::read ('tcpdf_path_main'   ));  // Installation path (/var/www/tcpdf/)
    define ('K_PATH_URL',        Configure::read ('tcpdf_path_url'    ));  // URL path to tcpdf installation folder (http://localhost/tcpdf/)

    define ('K_PATH_FONTS', K_PATH_MAIN . 'fonts/');  // path for PDF fonts (use K_PATH_MAIN . 'fonts/old/' for old non-UTF8 fonts)
    define ('K_PATH_CACHE', K_PATH_MAIN . 'cache/');  // cache directory for temporary files (full path)

    define ('K_CELL_HEIGHT_RATIO', 1.25);  // height of cell repect font height

    // solo se usan en el header
    // define ('K_PATH_IMAGES',    K_PATH_MAIN   . 'images/'   );  // images directory
    // define ('K_BLANK_IMAGE',    K_PATH_IMAGES . '_blank.png');  // blank image
  }

//============================================================+
// END OF FILE
//============================================================+
