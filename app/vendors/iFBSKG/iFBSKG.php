<?php

/*
 *    IIIIII  FFFFFFF  BBBBB   SSSSSSS  KK   KK  GGGGGGG
 *      II    FF       BB  BB  SS       KK KK    GG
 *      II    FFFFFF   BBBBB   SSSSSSS  KKKK     GG  GGG
 *      II    FF       BB  BB       SS  KK KK    GG    G
 *    IIIIII  FF       BBBBB   SSSSSSS  KK   KK  GGGGGGG
 *
 *    iFBSKG: infinite facebook session key generator
 * -----------------------------------------------------------------------------
 * version: 1.0b
 * date-created: 2010-11-11 10:03PM GMT-3
 * date-modified: 2010-11-11 10:03PM GMT-3
 *
 * 1) login with your app's administrator facebook account.
 * 2) login to your app, go here:
 *      www.facebook.com/login.php?YOUR_API_KEY
 * 3) this should redirect to your canvas page / connect url
 * 4) get a ONE-TIME-CODE from here:
 *      http://www.facebook.com/code_gen.php?v=1.0&api_key=YOUR_API_KEY
 * 5) fill the config data for this script checking your app's config page
 * 6) execute this script
*/

require_once 'facebook/facebook.php';

// config data
$apiKey = "d37446b39a11a89463407c683d099ae9";
$appSecret = "9b1bec9a3b378b4758ffff1ade6f5a23";
$oneTimeCode = "93QHP0";

// script
$facebook = new Facebook($apiKey, $appSecret);
$infiniteKeyArray = $facebook->api_client->auth_getSession($oneTimeCode);
print_r($infiniteKeyArray);