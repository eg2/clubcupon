<?php
require_once 'Table.php';
require_once 'Color2.php';
require_once 'ApiStrUtils.php';

/**
 * Api Logger handling for Cake.
 *
 * @package       Api.Utility
 */
class ApiLog {

    const LOG_TRACE = 99;

    private $prefixFileName = null;

    function __construct($prefixFileName = null) {
        $this->prefixFileName = $prefixFileName;
    }

    private $severityLevels = array(
        LOG_WARNING => 'warning',
        LOG_NOTICE => 'notice',
        LOG_INFO => 'info',
        LOG_DEBUG => 'debug',
        ApiLog::LOG_TRACE => 'trace',
        LOG_ERR => 'error',
        LOG_ERROR => 'error'
    );
    private $fileNameSuffixBySeverityLevels = array(
        LOG_WARNING => 'debug',
        LOG_NOTICE => 'debug',
        LOG_INFO => 'debug',
        LOG_DEBUG => 'debug',
        ApiLog::LOG_TRACE => 'debug',
        LOG_ERR => 'error',
        LOG_ERROR => 'error'
    );

    private function getFileName($type, $msg) {
        $fileName = LOGS;
        if (!empty($this->prefixFileName)) {
            $fileName.= $this->prefixFileName . "_";
        }
        if (isset($this->fileNameSuffixBySeverityLevels[$type])) {
            $fileName.= $this->fileNameSuffixBySeverityLevels[$type];
        } else {
            $fileName.= $this->severityLevels[$type];
        }
        $fileName.= ".log";
        return $fileName;
    }

    public function write($type, $msg) {
        // TODO verificar nivel de log
        if ($type == 'error' || $type == 'warning') {
            $filename = LOGS . 'error.log';
        } elseif (in_array($type, $this->severityLevels)) {
            $filename = LOGS . 'debug.log';
        } else {
            $filename = LOGS . $type . '.log';
        }
        $output = date('Y-m-d H:i:s') . ' ' . ucfirst($this->severityLevels[$type]) . ': ' . $msg . "\n";
        $log = new File($this->getFileName($type, $msg), true);
        if ($log->writable()) {
            return $log->append($output);
        }
    }

}

class ApiLoggerMDC {

    private $mdc = null;

    function __construct() {
        $this->reset();
    }

    public function reset() {
        $this->mdc = array();
    }

    public function delete($key) {
        unset($this->mdc[$key]);
    }

    public function put($key, $val) {
        $this->mdc[$key] = $val;
    }

    public function putAll($items) {
        foreach ($items as $key => $val) {
            $this->put($key, $val);
        }
    }

    public function get($key) {
        return $this->mdc[$key];
    }

    public function isEmpty() {
        return count($this->mdc) == 0;
    }

    public function toArray() {
        return $this->mdc;
    }

}

class ApiLogger {

    const PREFIX_API = 'api';

    protected static $instance = null;
    private $name = null;
    private $prefixFileName = null;
    private $apiMDC = null;
    private $apiLog = null;


    public function loadForcePrefixFileName($name) {
      $forcePrefixFileName = Configure::read($name.'.Log.PrefixFileName');
      if (is_null($forcePrefixFileName)) {
        $forcePrefixFileName = Configure::read('Log.PrefixFileName');
      }
      return $forcePrefixFileName;
    }

    public function loadConfigTraceEnabled($name, $prefixFileName) {
      $isTraceEnabled = Configure::read($name.'.Log.Verbose');
      if (is_null($isTraceEnabled)) {
        $isTraceEnabled = Configure::read($prefixFileName.'Log.Verbose');
      }
      if (is_null($isTraceEnabled)) {
        $isTraceEnabled = Configure::read('Log.Verbose');
      }
      return $isTraceEnabled;
    }

    function __construct($name = null, $prefixFileName = null, $isTraceEnabled=null) {
        if (!is_null($name)) {
            $this->name = $name;
        }
        if (is_null($prefixFileName)) {
            $this->prefixFileName = self::PREFIX_API;
        }else{
        	$this->prefixFileName = $prefixFileName;
        }

        $this->echoEnabled = Configure::read('Log.EchoEnabled');

        $this->isTraceEnabled = $isTraceEnabled;
        $this->apiMDC = new ApiLoggerMDC();
        $this->apiLog = new ApiLog($this->prefixFileName);
        $this->ConsoleColor = new Console_Color2();
    }

    public function echoConsoleWithLog($message, $data = array(), $newLine = true, $putTime = false) {
      $this->echoConsole($message . ' [' . json_encode($data) . ']', $newLine, $putTime);
      $this->notice($message, $data);
    }

    public function echoConsole($message,$newLine = true, $putTime= false) {
      $message = $this->color($message);
      if ($putTime) {
        $message .= " (" . date('Y-m-d H:i:s') . ")";
      }
      if($this->echoEnabled) {
      	if($newLine){
      		echo $message .  "\n";
      	}else{
      		echo $message;
      	}
      }
    }

    public function echoConsoleWithTime($message){
      return $this->echoConsole($message, true, true);
    }

    public function log($message, $data = null, $level = ApiLog::LOG_TRACE, $name = null, $mdcArr = null) {
      $forcePrefixFileName = $this->loadForcePrefixFileName($this->name);
      if(!is_null($forcePrefixFileName) && ($this->prefixFileName != $forcePrefixFileName)) {
        $this->apiLog = new ApiLog($forcePrefixFileName);
        $this->prefixFileName = $forcePrefixFileName;
      }

      $logText = "";
      if (!is_null($name)) {
        $logText.= $name;
      } elseif (!empty($this->name)) {
        $logText.= $this->name;
      }
      if (is_null($level)) {
        $level = ApiLog::LOG_TRACE;
      }
      if (!is_null($mdcArr)) {
        $logText.= json_encode($mdcArr);
      } elseif (!$this->apiMDC->isEmpty()) {
        $logText.= json_encode($this->apiMDC->toArray());
      }
      if (!empty($message)) {
        $logText.= ': ' . $message;
      }
      if (count($data) > 0) {
        $logText.= ' ' . json_encode($data);
      }
      if (empty($logText)) {
        $this->warning("logeando un message vacio.");
        return false;
      }
      return $this->apiLog->write($level, $logText);
    }

    public function notice($message, $data = null, $name = null, $mdc = null) {
        $this->log($message, $data, LOG_NOTICE, $name, $mdc);
    }

    public function info($message, $data = null, $name = null, $mdc = null) {
        $this->log($message, $data, LOG_INFO, $name, $mdc);
    }

    public function error($message, $data = null, $name = null, $mdc = null) {
        $this->log('[ERROR] '.$message, $data, LOG_WARNING, $name, $mdc);
        $this->log($message, $data, LOG_ERROR, $name, $mdc);
    }

    public function warning($message, $data = null, $name = null, $mdc = null) {
        $this->log($message, $data, LOG_WARNING, $name, $mdc);
    }

    public function debug($message, $data = null, $name = null, $mdc = null) {
        $this->log($message, $data, LOG_DEBUG, $name, $mdc);
    }

    public function trace($message, $data = null, $name = null, $mdc = null) {
      $actualIsTraceEnabled = null;
      $actualIsTraceEnabled = $this->loadConfigTraceEnabled($this->name, $this->prefixFileName);
      if (is_null($actualIsTraceEnabled) && !is_null($this->isTraceEnabled)) {
        $actualIsTraceEnabled = $this->isTraceEnabled;
      }

      if (!is_null($actualIsTraceEnabled) && $actualIsTraceEnabled) {
        $this->log($message, $data, ApiLog::LOG_TRACE, $name, $mdc);
      }
    }

    public function iterateItems($items=null, $alias=null, $fields=null, $showField=false) {
      if(is_null($fields) && !is_array($fields)) {
        $fields = array($fields);
      }
      if(is_null($fields)) {
        $fields = array('id');
      }
      $compactItems;
      foreach ($items as $item) {
        $valueFields = array();
        foreach ($fields as $field) {
          if($showField) {
            $valueFields[$field] = $item[$alias][$field];
          } else {
            $valueFields[] = $item[$alias][$field];
          }
        }
        $compactItems[] = $valueFields;
      }
      return $compactItems;
    }


    public function reset() {
        $this->apiMDC->reset();
    }

    public function delete($key) {
        $this->apiMDC->delete($key);
        $this->apiMDC->put($key, '');
    }

    public function get($key) {
        return $this->apiMDC->get($key);
    }

    public function put($key, $val) {
        $this->apiMDC->put($key, $val);
    }

    public function putAll($items) {
        foreach ($items as $key => $val) {
            $this->put($key, $val);
        }
    }

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new ApiLogger('default', self::PREFIX_API);
        }
        return self::$instance;
    }

    public function printTable($headers, $values) {
      $table = new Console_Table(CONSOLE_TABLE_ALIGN_LEFT, CONSOLE_TABLE_BORDER_ASCII, 1, null, true);

      $headersWithColors = array();
      foreach ($headers as $header) {
        $headersWithColors = $this->ConsoleColor->convert("%W$header%n");
      }

      foreach ($values as $key => $value) {
        $table->addRow(array(
          $this->ConsoleColor->convert("%W$key%n"),
          $this->ConsoleColor->convert("%b$value%n"),
          $this->ConsoleColor->convert("%R$value%n"),
        ));
      }

      echo $table->getTable();
    }

    public function printTablesForEntityLists($entitiesByType, $firstColor="%W", $sencodColor="%b") {
      $isTraceEnabled = Configure::read($this->name.'.Log.Verbose');

      foreach ($entitiesByType as $type => $entities) {
      foreach ($entities as $entityCode => $entity) {
        if ($isTraceEnabled) {
          $this->printTableForEntity($entity, $entityCode, $type);
        } else {
          $this->echoConsole($this->color("%W$entityCode%b<$type%n>"));
        }
      }
      }
    }

    public function color($text) {
        return $this->ConsoleColor->convert($text);
    }

    public function echoConsoleLine($char='_', $size=50) {
       $this->echoConsole(str_repeat($char, $size));
    }

    public function printTableForEntity($values, $entityCode=null, $type=null, $firstColor = '%W', $secondColor = '%b') {
      if (!is_null($entityCode)) {
        $this->echoConsoleLine(':');
        $this->echoConsole($this->color("$firstColor$entityCode%n$secondColor<$type%n>"));
        $this->echoConsoleLine(':');
      }
      $table = new Console_Table(CONSOLE_TABLE_ALIGN_LEFT, CONSOLE_TABLE_BORDER_ASCII, 1, null, true);
      $table->setHeaders(array(
        $this->ConsoleColor->convert("$firstColor"."field%n"),
        $this->ConsoleColor->convert("$firstColor"."value%n"),
   //     $this->ConsoleColor->convert("%Wespected%n")
      ));
      foreach ($values as $key => $value) {
        $table->addRow(array(
          $this->ConsoleColor->convert("$firstColor$key%n"),
          $this->ConsoleColor->convert("$secondColor$value%n"),
    //      $this->ConsoleColor->convert("%R$value%n"),
        ));
      }

      echo $table->getTable();
    }

    public function printTableForEntityWithExpected($values, $entityCode=null, $type=null, $firstColor = '%W', $secondColor = '%b') {
      if (!is_null($entityCode)) {
        $this->echoConsoleLine(':');
        $this->echoConsole($this->color("$firstColor$entityCode%n$secondColor$type%n>"));
        $this->echoConsoleLine(':');
      }
      $table = new Console_Table(CONSOLE_TABLE_ALIGN_LEFT, CONSOLE_TABLE_BORDER_ASCII, 1, null, true);
      $table->setHeaders(array(
        $this->ConsoleColor->convert("$firstColor"."field%n"),
        $this->ConsoleColor->convert("$firstColor"."value%n"),
        $this->ConsoleColor->convert("$firstColor"."expected%n"),
   //     $this->ConsoleColor->convert("%Wespected%n")
      ));
      foreach ($values as $key => $value) {
        if ($value['real'] == $value['expected']) {
          $expectedColor = '%g';
        } else {
          $expectedColor = '%R';
        };



        if ($this->startWith($key, '_')) {
        	$realValueWithColor = '%y'. $value['real'];
        	$expectedWithColor ='%y'. $value['expected'];
        	$table->addRow(array(
        	$this->ConsoleColor->convert("%y$key%n"),
        	$this->ConsoleColor->convert("%y$realValueWithColor%n"),
        	$this->ConsoleColor->convert("%y$expectedWithColor%n"),
        			));
        } else {
        	$realValueWithColor = '%b'. $value['real'];
        	$expectedWithColor = $expectedColor . $value['expected'];
        $table->addRow(array( 
          $this->ConsoleColor->convert("$firstColor$key%n"),
          $this->ConsoleColor->convert("$secondColor$realValueWithColor%n"),
          $this->ConsoleColor->convert("$secondColor$expectedWithColor%n"),
        ));
        }
      }

      echo $table->getTable();
    }

    public function startWith($haystack, $needle) {
    	return $needle === "" || strpos($haystack, $needle) === 0;
    }
    


}

class ApiXml {

    /**
     * Initialize SimpleXMLElement or DOMDocument from a given XML string, file path, URL or array.
     *
     * ### Usage:
     *
     * Building XML from a string:
     *
     * `$xml = Xml::build('<example>text</example>');`
     *
     * Building XML from string (output DOMDocument):
     *
     * `$xml = Xml::build('<example>text</example>', array('return' => 'domdocument'));`
     *
     * Building XML from a file path:
     *
     * `$xml = Xml::build('/path/to/an/xml/file.xml');`
     *
     * Building from a remote URL:
     *
     * `$xml = Xml::build('http://example.com/example.xml');`
     *
     * Building from an array:
     *
     * {{{
     *  $value = array(
     *  'tags' => array(
     *  'tag' => array(
     *  array(
     *  'id' => '1',
     *  'name' => 'defect'
     *  ),
     *  array(
     *  'id' => '2',
     *  'name' => 'enhancement'
     * )
     *  )
     *  )
     *  );
     * $xml = Xml::build($value);
     * }}}
     *
     * When building XML from an array ensure that there is only one top level element.
     *
     * ### Options
     *
     * - `return` Can be 'simplexml' to return object of SimpleXMLElement or 'domdocument' to return DOMDocument.
     * - `loadEntities` Defaults to false.  Set to true to enable loading of `<!ENTITY` definitions.  This
     *   is disabled by default for security reasons.
     * - If using array as input, you can pass `options` from Xml::fromArray.
     *
     * @param string|array $input XML string, a path to a file, an URL or an array
     * @param array $options The options to use
     * @return SimpleXMLElement|DOMDocument SimpleXMLElement or DOMDocument
     * @throws XmlException
     */
    public static function build($input, $options = array(
    )) {
        if (!is_array($options)) {
            $options = array(
                'return' => (string) $options
            );
        }
        $defaults = array(
            'return' => 'simplexml',
            'loadEntities' => false,
        );
        $options = array_merge($defaults, $options);
        if (is_array($input) || is_object($input)) {
            return self::fromArray((array) $input, $options);
        } elseif (strpos($input, '<') !== false) {
            return self::_loadXml($input, $options);
        } elseif (file_exists($input) || strpos($input, 'http://') === 0 || strpos($input, 'https://') === 0) {
            $input = file_get_contents($input);
            return self::_loadXml($input, $options);
        } elseif (!is_string($input)) {
            throw new XmlException(__d('cake_dev', 'Invalid input.'));
        }
        throw new XmlException(__d('cake_dev', 'XML cannot be read.'));
    }

    /**
     * Parse the input data and create either a SimpleXmlElement object or a DOMDocument.
     *
     * @param string $input The input to load.
     * @param array  $options The options to use. See Xml::build()
     * @return SimpleXmlElement|DOMDocument.
     */
    protected static function _loadXml($input, $options) {
        $hasDisable = function_exists('libxml_disable_entity_loader');
        $internalErrors = libxml_use_internal_errors(true);
        if ($hasDisable && !$options['loadEntities']) {
            libxml_disable_entity_loader(true);
        }
        if ($options['return'] === 'simplexml' || $options['return'] === 'simplexmlelement') {
            $xml = new SimpleXMLElement($input, LIBXML_NOCDATA);
        } else {
            $xml = new DOMDocument();
            $xml->loadXML($input);
        }
        if ($hasDisable && !$options['loadEntities']) {
            libxml_disable_entity_loader(false);
        }
        libxml_use_internal_errors($internalErrors);
        return $xml;
    }

    /**
     * Transform an array into a SimpleXMLElement
     *
     * ### Options
     *
     * - `format` If create childs ('tags') or attributes ('attribute').
     * - `version` Version of XML document. Default is 1.0.
     * - `encoding` Encoding of XML document. If null remove from XML header. Default is the some of application.
     * - `return` If return object of SimpleXMLElement ('simplexml') or DOMDocument ('domdocument'). Default is SimpleXMLElement.
     *
     * Using the following data:
     *
     * {{{
     * $value = array(
     *    'root' => array(
     *        'tag' => array(
     *            'id' => 1,
     *            'value' => 'defect',
     *            '@' => 'description'
     *         )
     *     )
     * );
     * }}}
     *
     * Calling `Xml::fromArray($value, 'tags');`  Will generate:
     *
     * `<root><tag><id>1</id><value>defect</value>description</tag></root>`
     *
     * And calling `Xml::fromArray($value, 'attribute');` Will generate:
     *
     * `<root><tag id="1" value="defect">description</tag></root>`
     *
     * @param array $input Array with data
     * @param array $options The options to use
     * @return SimpleXMLElement|DOMDocument SimpleXMLElement or DOMDocument
     * @throws XmlException
     */
    public static function fromArray($input, $options = array(
    )) {
        if (!is_array($input) || count($input) !== 1) {
            throw new XmlException(__d('cake_dev', 'Invalid input.'));
        }
        $key = key($input);
        if (is_integer($key)) {
            throw new XmlException(__d('cake_dev', 'The key of input must be alphanumeric'));
        }
        if (!is_array($options)) {
            $options = array(
                'format' => (string) $options
            );
        }
        $defaults = array(
            'format' => 'tags',
            'version' => '1.0',
            'encoding' => Configure::read('App.encoding'),
            'return' => 'simplexml'
        );
        $options = array_merge($defaults, $options);
        $dom = new DOMDocument($options['version'], $options['encoding']);
        self::_fromArray($dom, $dom, $input, $options['format']);
        $options['return'] = strtolower($options['return']);
        if ($options['return'] === 'simplexml' || $options['return'] === 'simplexmlelement') {
            return new SimpleXMLElement($dom->saveXML());
        }
        return $dom;
    }

    /**
     * Recursive method to create childs from array
     *
     * @param DOMDocument $dom Handler to DOMDocument
     * @param DOMElement $node Handler to DOMElement (child)
     * @param array $data Array of data to append to the $node.
     * @param string $format Either 'attribute' or 'tags'.  This determines where nested keys go.
     * @return void
     * @throws XmlException
     */
    protected static function _fromArray($dom, $node, &$data, $format) {
        if (empty($data) || !is_array($data)) {
            return;
        }
        foreach ($data as $key => $value) {
            if (is_string($key)) {
                if (!is_array($value)) {
                    if (is_bool($value)) {
                        $value = (int) $value;
                    } elseif ($value === null) {
                        $value = '';
                    }
                    $isNamespace = strpos($key, 'xmlns:');
                    if ($isNamespace !== false) {
                        $node->setAttributeNS('http://www.w3.org/2000/xmlns/', $key, $value);
                        continue;
                    }
                    if ($key[0] !== '@' && $format === 'tags') {
                        $child = null;
                        if (!is_numeric($value)) {
                            // Escape special characters
                            // http://www.w3.org/TR/REC-xml/#syntax
                            // https://bugs.php.net/bug.php?id=36795
                            $child = $dom->createElement($key, '');
                            $child->appendChild(new DOMText($value));
                        } else {
                            $child = $dom->createElement($key, $value);
                        }
                        $node->appendChild($child);
                    } else {
                        if ($key[0] === '@') {
                            $key = substr($key, 1);
                        }
                        $attribute = $dom->createAttribute($key);
                        $attribute->appendChild($dom->createTextNode($value));
                        $node->appendChild($attribute);
                    }
                } else {
                    if ($key[0] === '@') {
                        throw new XmlException(__d('cake_dev', 'Invalid array'));
                    }
                    if (is_numeric(implode('', array_keys($value)))) { // List
                        foreach ($value as $item) {
                            $itemData = compact('dom', 'node', 'key', 'format');
                            $itemData['value'] = $item;
                            self::_createChild($itemData);
                        }
                    } else { // Struct
                        self::_createChild(compact('dom', 'node', 'key', 'value', 'format'));
                    }
                }
            } else {
                throw new XmlException(__d('cake_dev', 'Invalid array'));
            }
        }
    }

    /**
     * Helper to _fromArray(). It will create childs of arrays
     *
     * @param array $data Array with informations to create childs
     * @return void
     */
    protected static function _createChild($data) {
        extract($data);
        $childNS = $childValue = null;
        if (is_array($value)) {
            if (isset($value['@'])) {
                $childValue = (string) $value['@'];
                unset($value['@']);
            }
            if (isset($value['xmlns:'])) {
                $childNS = $value['xmlns:'];
                unset($value['xmlns:']);
            }
        } elseif (!empty($value) || $value === 0) {
            $childValue = (string) $value;
        }
        if ($childValue) {
            $child = $dom->createElement($key, $childValue);
        } else {
            $child = $dom->createElement($key);
        }
        if ($childNS) {
            $child->setAttribute('xmlns', $childNS);
        }
        self::_fromArray($dom, $child, $value, $format);
        $node->appendChild($child);
    }

    /**
     * Returns this XML structure as a array.
     *
     * @param SimpleXMLElement|DOMDocument|DOMNode $obj SimpleXMLElement, DOMDocument or DOMNode instance
     * @return array Array representation of the XML structure.
     * @throws XmlException
     */
    public static function toArray($obj) {
        if ($obj instanceof DOMNode) {
            $obj = simplexml_import_dom($obj);
        }
        if (!($obj instanceof SimpleXMLElement)) {
            throw new XmlException(__d('cake_dev', 'The input is not instance of SimpleXMLElement, DOMDocument or DOMNode.'));
        }
        $result = array();
        $namespaces = array_merge(array(
            '' => ''
                ), $obj->getNamespaces(true));
        self::_toArray($obj, $result, '', array_keys($namespaces));
        return $result;
    }

    /**
     * Recursive method to toArray
     *
     * @param SimpleXMLElement $xml SimpleXMLElement object
     * @param array $parentData Parent array with data
     * @param string $ns Namespace of current child
     * @param array $namespaces List of namespaces in XML
     * @return void
     */
    protected static function _toArray($xml, &$parentData, $ns, $namespaces) {
        $data = array();
        foreach ($namespaces as $namespace) {
            foreach ($xml->attributes($namespace, true) as $key => $value) {
                if (!empty($namespace)) {
                    $key = $namespace . ':' . $key;
                }
                $data['@' . $key] = (string) $value;
            }
            foreach ($xml->children($namespace, true) as $child) {
                self::_toArray($child, $data, $namespace, $namespaces);
            }
        }
        $asString = trim((string) $xml);
        if (empty($data)) {
            $data = $asString;
        } elseif (!empty($asString)) {
            $data['@'] = $asString;
        }
        if (!empty($ns)) {
            $ns.= ':';
        }
        $name = $ns . $xml->getName();
        if (isset($parentData[$name])) {
            if (!is_array($parentData[$name]) || !isset($parentData[$name][0])) {
                $parentData[$name] = array(
                    $parentData[$name]
                );
            }
            $parentData[$name][] = $data;
        } else {
            $parentData[$name] = $data;
        }
    }

}
