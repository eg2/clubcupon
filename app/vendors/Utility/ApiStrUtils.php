<?php

/**
 * Api Str Utils.
 *
 * @package       Api.Utility
 */
class ApiStrUtils {

    public static function isEndWithDot($phase) {
        return self::isEndWith($phase, '.');
    }

    public static function isEndWith($phase, $chart) {
        return $phase[strlen($phase) - 1] == $chart;
    }

    /**
     * Translates a camel case string into a string with underscores (e.g. firstName -&gt; first_name)
     * @param    string   $str    String in camel case format
     * @return    string            $str Translated into underscore format
     *
     */
    public static function from_camel_case($str) {
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');
        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    /**
     * Translates a string with underscores into camel case (e.g. first_name -&gt; firstName)
     * @param    string   $str                     String in underscore format
     * @param    bool     $capitalise_first_char   If true, capitalise the first char in $str
     * @return   string                              $str translated into camel caps
     *
     */
    public static function to_camel_case($str, $capitalise_first_char = false) {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $str);
    }

    public static function stringBeginsWith($haystack, $beginning, $caseInsensitivity = false) {
        if ($caseInsensitivity)
            return strncasecmp($haystack, $beginning, strlen($beginning)) == 0;
        else
            return strncmp($haystack, $beginning, strlen($beginning)) == 0;
    }

    public static function stringEndsWith($haystack, $ending, $caseInsensitivity = false) {
        if ($caseInsensitivity)
            return strcasecmp(substr($haystack, strlen($haystack) - strlen($ending)), $haystack) == 0;
        else
            return strpos($haystack, $ending, strlen($haystack) - strlen($ending)) !== false;
    }

    public static function cleanChars($string) {
        // revision extra para evitar acentos y caracteres bizarros...
        return str_replace(array('á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò', 'ù', 'ä', 'ë', 'ï', 'ö', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü', 'ñ', 'Ñ', "'", '"'), array('a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'n', 'N', '', ''), $string);
    }

}
