<?php
error_reporting(E_ALL&~E_DEPRECATED);
ini_set('error_reporting', E_ALL&~E_DEPRECATED);
/**
 *
 * @copyright 2009
 */
class CronShell extends Shell {
    public $tasks = array('GenerateSmsCoupons');

    function main()
    {
        // site settings are set in config
        App::import ('Model', 'Setting');
        App::import ('vendor', 'Precision', array ('file' =>'Precision/Precision.php'));
        // indicamos que por defecto utilice la conexion master a la DB
        AppModel::setDefaultDbConnection ('master');

        $setting_model_obj = new Setting ();
        $settings = $setting_model_obj->getKeyValuePairs ();
        Configure::write ($settings);
        App::import ('Component', 'Cron');
        $this->Cron = & new CronComponent ();
        $option    = !empty ($this->args [0]) ? $this->args [0] : '';
        $suboption = !empty ($this->args [1]) ? $this->args [1] : '';
        $suboption2 = !empty ($this->args [2]) ? $this->args [2] : '';

        // Comenzar output buffering
        //ob_start ();
        $content = Configure::read ('debug') != 0;

        switch ($option)
          {
            case 'process_send_coupons':
                $this->Cron->processSendCoupons();
                break;
            case 'update_expired_redemptions':
                $this->Cron->update_expired_redemptions();
                break;
            case 'process_send_coupons_for_company': $this->Cron->processSendCouponsForCompany   (          ); break;
            case 'send_deals_mailing'  : $this->Cron->send_deals_mailing   (          ); break;
            case 'update_deal'         : $this->Cron->update_deal          (          ); break;
            case 'process_deal_status' : $this->Cron->processDealStatus    ($suboption); break;
            case 'points_seniority'    : $this->Cron->points_seniority     (          ); break;
            case 'points_expiry'       : $this->Cron->points_expiry        (          ); break;
            case 'points_expiring_soon': $this->Cron->points_expiring_soon (          ); break;
            case 'send_payment_reminder_offline': $this->Cron->send_payment_reminder_offline (          ); break;
            case 'send_expiration_reminder': $this->Cron->send_expiration_reminder (          ); break;
            case 'send_mail_no_buyers': $this->Cron->send_mail_no_buyers(); break;
            case 'return_credit': $this->Cron->return_credit(); break;
            case 'del_old_branch_deals': $this->Cron->del_old_branch_deals(); break;
            case 'delete_branchs_deals': $this->out('delete_branchs_deals no se debe borrar',1); break;
            case 'reindex_all': $this->Cron->reindex_all(); break;
            case 'clone_now_deals': $this->Cron->clone_now_deals($suboption); break;
            case 'update_deal_by_date':

                if (date("Y-m-d H:i:s", strtotime($suboption." ".$suboption2))!==$suboption." ".$suboption2)
                {
                    $this->out(date("Y-m-d H:i:s", strtotime($suboption." ".$suboption2)));
                    $this->out("'".trim($suboption." ".$suboption2)."' no es una fecha valida.  Omitiendo...");

                    $this->out("Ejemplo de uso:". $option." ".date("Y-m-d H:i:s"));
                    break;
                }
                $this->Cron->update_deal_by_date($suboption,$suboption2);
                break;


            default:
              echo "`update_deal` Cambia de estado las ofertas.\n";
              echo "`process_send_coupons` Envia los cupones y la lista de cupones enviados al comercio.\n";
              echo "`update_expired_redemptions` actualiza el campo redemptions.expired a aquellos registros que no fueron redimidos cuyo Deal.coupon_expiry_date correspondiente ha caducado.\n";
              echo "`send_deals_mailing` Envia el mailing diario para las ofertas activas.\n";
              echo "`process_deal_status` Revisa la lista de pendientes y actualiza pagos.\n";
              echo "`points_seniority` Asigna puntos por antigüedad.\n";
              echo "`points_expiry` Hace vencer los puntos.\n";
              echo "`points_expiring_soon` Manda mails con puntos próximos a vencer.\n";
              echo "`send_payment_reminder_offline` Manda mails de recordatorio de pago a quienes pagaron con medios offline.\n";
              echo "`send_expiration_reminder` Envia mails de recordatorio de expiracion de cupones\n";
              echo "`return_credit` devuelve el credito de los cupones no usados!\n";
              echo "`del_old_branch_deals` borra los branches deals creados antiormente a la cantidad de dias configurada en core.php!\n";
              echo "`delete_branchs_deals` elimina los registros en la tabla branches_deals que tengan campo deleted=1\n";
              echo "`reindex_all` actualiza los registros de search_index\n";
              echo "`clone_now_deals` clona los deals correspondientes al dia de la fecha actual o a la que se paso como argumento si cumple las condiciones adecuadas\n";
              echo "`update_deal_by_date [YYYY-mm-dd H:i:s]` Cambia de estado las ofertas segun la fecha (opcional) .\n";

              $option = '';
              break;
          } // switch

        // Finalizar output buffering y obtener el contenido en $content
     //   $content = ob_get_contents ();
        //ob_end_clean ();

        if ($option != '') {
          if ($suboption != '') {
            $option = $option . ' (' . $suboption . ') ';
          } else {
            $option = $option . ' ';
          }
        } else {
          echo $content;
        }
        if ($content)  $this->log ('Cron job ' . $option . 'prints: ' . $content,      LOG_DEBUG);
        else           $this->log ('Cron job ' . $option . 'ended without any issue.', LOG_DEBUG);
    }
}

?>
