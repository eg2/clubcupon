<?php

App::import('Component', 'api.ApiDealService');
App::import('Component', 'api.ApiRedemptionService');
App::import('Model', 'Redemption');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealUser');

class RedemptionTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->dealUserId = 1880952;//1887866;
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiDeal = new ApiDeal();
        $this->Redemption = new Redemption();
        $this->ApiRedemptionService = new ApiRedemptionServiceComponent();
        $this->ApiDealService = new ApiDealServiceComponent();
    }

    function setUp() {
        echo __METHOD__ . "\n";
    }

    function tearDown() {
        echo __METHOD__ . "\n";
    }

    function show() {
        $dealUser = $this->ApiDealUser->findById($this->dealUserId);
        $deal = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
        $apiRedemptionServiceIsCouponExpired = $this->ApiRedemptionService->isCouponExpired($dealUser);
        $couponExpiryDate = $this->ApiDealService->couponExpirationDate($deal, $dealUser);
        $redemptionByDealUserId = $this->Redemption->findByDealUserId($dealUser['DealUser']['id']);
        $redemptionByPosnetCode = $this->Redemption->findFirstByPosnetCode($redemptionByDealUserId['Redemption']['posnet_code']);
        $redemptionIsCouponExpired = $this->Redemption->isCouponExpired($deal, $redemptionByPosnetCode);
        echo "\n";
        echo "Deal\n";
        echo '  ' . 'id                    : ' . $deal['Deal']['id'] . "\n";
        echo '  ' . 'start_date            : ' . $deal['Deal']['start_date'] . "\n";
        echo '  ' . 'end_date              : ' . $deal['Deal']['end_date'] . "\n";
        echo '  ' . 'coupon_start_date     : ' . $deal['Deal']['coupon_start_date'] . "\n";
        echo '  ' . 'coupon_expiry_date    : ' . $deal['Deal']['coupon_expiry_date'] . "\n";
        echo '  ' . 'coupon_duration       : ' . $deal['Deal']['coupon_duration'] . "\n";
        echo '  ' . 'is_variable_expiration: ' . $deal['Deal']['is_variable_expiration'] . "\n";
        echo "\n";
        echo "DealUser\n";
        echo '  ' . 'created: ' . $dealUser['DealUser']['created'] . "\n";
        echo '  ' . 'is_used: ' . $dealUser['DealUser']['is_used'] . "\n";
        echo "\n";
        echo "isCouponExpired\n";
        echo '  ' . 'couponExpiryDate             : ' . $couponExpiryDate . "\n";
        echo '  ' . 'isCouponExpired (Service)    : ' . json_encode($apiRedemptionServiceIsCouponExpired) . "\n";
        echo '  ' . 'isCouponExpired (Redemption) : ' . json_encode($redemptionIsCouponExpired) . "\n";
        echo "\n";
    }

    function main() {
        $this->setUp();
        $this->show();
        $this->tearDown();
    }

}
