<?php

App::import('Core', 'Router');
App::import('Core', 'Controller');
App::import('Core', 'HttpSocket');

class UnsubscriberShell extends Shell {

    var $uses = array(
        'Subscription'
    );

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('unsuscriber');
    }

    function main() {
        echo "\n" . date("Y-m-d H:i:s") . " - EMPIEZA el proceso de desuscripcion.";
        try {
            echo "\n" . date("Y-m-d H:i:s") . " - Entrando al try A\n ";
            $HttpSocket = new HttpSocket();
            $idapp = Configure::read("Unsuscriber.id_app");
            $plazo = Configure::read("Unsuscriber.plazo_en_dias");
            $cantidadPorConsulta = Configure::read("Unsuscriber.registros_por_consulta");
            $urlDeployEMT = Configure::read("Unsuscriber.url_deploy_emt");
            $diaADesuscribirHasta = strtotime("today");
            $diaADesuscribirDesde = mktime(0, 0, 0, date("m", $diaADesuscribirHasta), date("d", $diaADesuscribirHasta) - $plazo, date("Y", $diaADesuscribirHasta));
            $desde = date("Ymd", $diaADesuscribirDesde);
            $hasta = date("Ymd", $diaADesuscribirHasta);
            $proximaPaginaIndex = 1;
            $quedanRegistros = true;
            echo "\n" . date("Y-m-d H:i:s") . " - Variables levantadas. idapp:$idapp | desde : $desde | hasta : $hasta";
            $myData = null;
            while ($quedanRegistros) {
                echo "\n" . date("Y-m-d H:i:s") . " - Haciendo el get a emt";
                $HttpSocket->get($urlDeployEMT, "idapp=$idapp&desde=$desde&hasta=$hasta&cantidad=$cantidadPorConsulta&pagina=$proximaPaginaIndex");
                $myData = json_decode($HttpSocket->response['body']);
                if (isset($myData)) {
                    $emails = null;
                    foreach ($myData as $email) {
                        $emails[] = $email;
                    }
                    echo "\n" . date("Y-m-d H:i:s") . " - Lista de mails recibidos: \n";
                    $this->Subscription->unbindModel(array(
                        'belongsTo' => array(
                            'User'
                        )
                    ));
                    $this->Subscription->unbindModel(array(
                        'belongsTo' => array(
                            'City'
                        )
                    ));
                    foreach ($emails as $email_to_unsubscribe) {
                        echo "Desuscribiendo el mail $email_to_unsubscribe\n";
                        if (!$this->Subscription->updateAll(array(
                                    'Subscription.is_subscribed' => "0",
                                    'Subscription.utm_campaign' => '"cake_unsubscriber_bounce"',
                                    'Subscription.modified' => '"' . date('Y-m-d H:i:s') . '"'
                                        ), array(
                                    "Subscription.email" => $email_to_unsubscribe,
                                    "Subscription.is_subscribed" => "1"
                                ))) {
                            echo "ERROR: No se pudo desuscribir el email $email_to_unsubscribe\n";
                            print_r(error_get_last());
                            trigger_error("El update de las subscription no se pudo ejecutar");
                        }
                    }
                }
                if (sizeof($myData) != $cantidadPorConsulta) {
                    $quedanRegistros = false;
                }
                $proximaPaginaIndex++;
            }
        } catch (Exception $e) {
            trigger_error("Se produjo un error \n" . $e->getMessage());
            echo "Se produjo un error \n" . $e->getMessage();
        }
        if ($proximaPaginaIndex > 2) {
            $totalRegistrosProcesados = $cantidadPorConsulta * ($proximaPaginaIndex - 2) + sizeof($myData);
        } else {
            $totalRegistrosProcesados = sizeof($myData);
        }
        echo "\n" . date("Y-m-d H:i:s") . " - TERMINA el proceso de desuscripcion. Se procesaron $totalRegistrosProcesados\n";
    }

}
