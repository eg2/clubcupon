<?php
class GenerateSmsCouponsTask extends Shell {
    public $uses = array('Deal','DealExternal');

    public function execute() {
        if( !isset($this->params['q_coupons']) ) {
            $this->err(
                '[DEAL_SMS_GENERATE] El parametro q_coupons (cantidad de cupones) es obligatorio.');
            return false;
        }
        $q_cupons = $this->params['q_coupons'];

        if( !isset($this->params['deal_id']) ) {
            $this->err('[DEAL_SMS_GENERATE] El parametro dead_id (Id de la Oferta) es obligatorio.');
            return false;
        }
        $deal_id = $this->params['deal_id'];


        $this->Deal->Behaviors->disable('SmsExcludable');
        $this->_generateCuponsByDeal($deal_id, $q_cupons);
	$this->Deal->Behaviors->enable('SmsExcludable');

        $this->out("[DEAL_SMS_GENERATE] OK");
    }

    public function _generateCuponsByDeal($deal_id, $q_cupons=1) {
        $this->out("[DEAL_SMS_GENERATE] Generando ".$q_cupons." cupones para la Oferta id ".$deal_id.".");
        $deal = $this->Deal->findById($deal_id); 
        $user_id         = Configure::read('user.sms_id');
        $payment_type_id = "5000";
        $external_status = "A";
        $used_points     = false;
        $giftData        = '{"is_gift" => false}';
        $inhibit_block   = true;
        $options         = array();
        $ext = $this->Deal->externalPayment(
            $deal_id         , 
            $q_cupons       , 
            $user_id        , 
            $payment_type_id, 
            $external_status, 
            $used_points    , 
            $giftData       , 
            $inhibit_block  , 
            $options        ,
            1
        );

        App::import('Core', 'Controller');   
        App::import('Core', 'Component');    
        App::import('Core', 'Router');       
        App::import('Core', 'Component');    
        App::import('Controller', 'Deals');  
        App::import('Component', 'Session'); 
        App::import('Core'      ,'Router');
        App::import('Model', 'Deal');
        App::import('Component', 'Email');
        App::import('Model','EmailTemplate');
        App::import('Model','Subscription');
        $dealsController = new DealsController ();
        $dealsController->EmailTemplate = new EmailTemplate ();
        $dealsController->Email = new EmailComponent ();
        $dealsController->Email->delivery = Configure::read ('site.email.delivery');
        $dealsController->Email->smtpOptions = Configure::read ('site.email.smtp_options');
        $dealsController->Deal = $this->Deal;
        $dealsController->Session = new SessionComponent ();
        $view = new View ($dealsController);

        $ext ['Deal']['deal_id'] = $ext ['DealExternal']['deal_id'];
        $ext ['Deal']['quantity'] = $q_cupons;
        $ext ['Deal']['is_gift'] = $ext ['DealExternal']['is_gift'];
        $ext ['Deal']['gift_data'] = json_decode ($ext ['DealExternal']['gift_data'], true);
        $ext ['Deal']['payment_type_id'] = $ext ['DealExternal']['payment_type_id'];
        $ext ['Deal']['user_id'] = $user_id = $ext ['DealExternal']['user_id'];
        $ext ['DealExternal']['parcial_amount'] = 0;
        $ext ['DealExternal']['final_amount'] = $deal ['Deal']['discounted_price'];
        $ext ['DealExternal']['final_amount'] = $deal ['Deal']['discounted_price'];
	
        $ret = $dealsController->_buyDeal ($ext, FALSE);

        $this->out("[DEAL_SMS_GENERATE] ".$q_cupons." cupones generados para la Oferta id ".$deal_id.".");
    }
}
