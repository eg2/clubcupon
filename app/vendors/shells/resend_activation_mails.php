<?php

error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('error_reporting', E_ALL & ~E_DEPRECATED);

class ResendActivationMailsShell extends Shell {

    function main() {
        // no se por que se hace esto, pero esta en todos lados de todas formas :P
        // site settings are set in config
        App::import('Model', 'Setting');
        // indicamos que por defecto utilice la conexion master a la DB
        AppModel::setDefaultDbConnection('master');

        $setting_model_obj = new Setting ();
        $settings = $setting_model_obj->getKeyValuePairs();
        Configure::write($settings);


        App::import('Component', 'ResendActivationMails');
        $this->ResendActivationMails = & new ResendActivationMailsComponent ();
        $this->ResendActivationMails->doIt();
    }

// main
}

// class
?>
