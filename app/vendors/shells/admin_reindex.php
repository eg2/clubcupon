<?php

App::import('Model', 'ConnectionManager');

class AdminReindexShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->dataSource = ConnectionManager::getDataSource('master');
    }

    function rowCount($id) {
        $sql = "select                                                                  " .
                "      count(Deal.id) as rowCount                                       " .
                "from deals as Deal                                                     " .
                "	inner join companies as Company on Company.id = Deal.company_id " .
                "	left join users as User on User.id = Deal.created_by            " .
                "	inner join cities as City on City.id = Deal.city_id             " .
                "where                                                                  " .
                "	Deal.id >= {$id}                                                ";
        $rowCount = $this->dataSource->query($sql);
        return $rowCount [0][0]['rowCount'];
    }

    function insert($start, $pageSize, $id) {
        $sql = "insert into search_index (association_key, model, `data`, created, modified)            " .
                "select                                                                                 " .
                "       Deal.id as association_key,                                                     " .
                "       'Deal' as model,                                                                " .
                "       concat(Deal.name, ' ',Company.name, ' ', ifnull(User.username,''), ' ',City.name) as `data`," .
                "       now() as created,                                                               " .
                "       now() as modified                                                               " .
                "from deals as Deal                                                                     " .
                "	inner join companies as Company on Company.id = Deal.company_id                 " .
                "	left join users as User on User.id = Deal.created_by                            " .
                "	inner join cities as City on City.id = Deal.city_id                             " .
                "where                                                                                  " .
                "	Deal.id >= {$id}                                                                " .
                "order by                                                                               " .
                "	Deal.id	                                                                        " .
                "limit {$start}, {$pageSize}                                                            ";
        $this->dataSource->rawQuery($sql);
    }

    function delete() {
        $this->dataSource->rawQuery("delete from search_index where model = 'Deal'");
    }

    function main() {
        $id = Configure::read('web.reindex.start_deal_id');
        $pageSize = Configure::read('web.reindex.max_rows_limit');
        $rowCount = $this->rowCount($id);
        $pages = ceil($rowCount / $pageSize);
        $page = 0;
        echo "start\n";
        echo "delete\n";
        $this->delete();
        echo "insert\n";
        echo "  id      : " . $id . "\n";
        echo "  pageSize: " . $pageSize . "\n";
        echo "  rowCount: " . $rowCount . "\n";
        echo "  pages   : " . $pages . "\n";
        while ($page < $pages) {
            echo "    page    : " . $page . "\n";
            $start = $page * $pageSize;
            $this->insert($start, $pageSize, $id);
            $page++;
        }
        echo "end\n";
    }

}
