<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h1 class="h1-format">Transacciones</h1>
        
            <!-- Contenidos -->

            <?php if(empty($this->params['named']['stat']) && !isset($this->data['Transaction']['tab_check'])): ?>

                <div class="menu-submenu">
                    <ul class="bloque-menu clearfix">
                        <li><?php echo $html->link('Hoy',         array('controller'=> 'transactions', 'action'=>'index','stat' => 'day'),   array('escape'=>false, 'title' => 'Transacciones del d&iacute;a')); ?></li>
                        <li><?php echo $html->link('Esta semana', array('controller'=> 'transactions', 'action'=>'index','stat' => 'week'),  array('escape'=>false, 'title' => 'Transacciones de la semana'));   ?></li>
                        <li><?php echo $html->link('Este mes',    array('controller'=> 'transactions', 'action'=>'index','stat' => 'month'), array('escape'=>false, 'title' => 'Transacciones del mes'));        ?></li>
                        <li><?php echo $html->link('Todas',       array('controller'=> 'transactions', 'action'=>'index','stat' => 'all'),   array('escape'=>false, 'title' => 'Todas las transacciones'));      ?></li>
                    </ul>
                </div>
            
            <?php else: ?>
            
    <?php
        echo $form->create('Transaction', array('action' => 'index' ,'class' => 'normal js-ajax-form'));
     ?>
        <div class="clearfix date-time-block">
            <div class="input date-time clearfix">
                <div class="js-datetime">
                    <?php echo $form->input('from_date', array('label' => __l('From '), 'type' => 'date', 'minYear' => date('Y')-10, 'maxYear' => date('Y'), 'div' => false, 'empty' => __l('Please Select'))); ?>
                </div>
            </div>
            <div class="input date-time end-date-time-block clearfix">
                <div class="js-datetime">
                    <?php echo $form->input('to_date', array('label' => __l('To '),  'type' => 'date', 'minYear' => date('Y')-10, 'maxYear' => date('Y'), 'div' => false, 'empty' => __l('Please Select'))); ?>
                </div>
            </div>
        </div>
      <?php
        echo $form->input('tab_check', array('type' => 'hidden','value' => 'tab_check'));
        echo $form->end(__l(' '));
    ?>
    <?php echo $this->element('paging_counter');?>
    <table>
        <tr>
            <th><?php echo $paginator->sort('FECHA',       'created');?></th>
            <th><?php echo $paginator->sort('DESCRIPCION', 'transaction_type_id');?></th>
            <th><?php echo $paginator->sort('CREDITO',     'amount').' ('.Configure::read('site.currency').')';?></th>
            <th><?php echo $paginator->sort('DEBITO',      'amount').' ('.Configure::read('site.currency').')';?></th>
        </tr>
    <?php
		if (!empty($transactions)):
			$i = 0;
			$j = 1;
			foreach ($transactions as $transaction):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
	?>
        <tr<?php echo $class;?>>
            <td>
            <?php echo htmlentities(ucfirst(strftime("%A %d de %b, %Y", strtotime($transaction['Transaction']['created'])))); ?>
            </td>
            <td>
				<?php echo $html->transactionDescription($transaction);?>
            </td>
            <td>
                <?php
                    if($transaction['TransactionType']['is_credit']):
                        echo $html->cCurrency($transaction['Transaction']['amount']);
                    else:
                        echo '--';
                    endif;
                 ?>
            </td>
            <td>
                <?php
                    if($transaction['TransactionType']['is_credit']):
                        echo '--';
                    else:
                        echo $html->cCurrency($transaction['Transaction']['amount']);
                    endif;
                 ?>
            </td>
        </tr>
    <?php
        $j++;
        endforeach;
    ?>
    <?php
    else:
    ?>
        <tr>
            <td colspan="11" class="notice"><?php echo __l('No Transactions available');?></td>
        </tr>
    <?php
    endif;
    ?>
    </table>
    <table>
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Credit');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($total_credit_amount);?></td>
        </tr>
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Debit');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($total_debit_amount);?></td>
        </tr>
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Withdraw Request');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($blocked_amount);?></td>
        </tr>
<!--        <tr class="total-block">
            <td colspan="4" class="dr"><?php echo __l('Transaction Summary (Cr - Db - Withdraw Request)');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($total_credit_amount - ($total_debit_amount + $blocked_amount));?></td>
        </tr>-->
        <tr class="total-block">
            <td colspan="4" class="dr"><?php echo __l('Account Balance');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($user_available_balance);?></td>
        </tr>
    </table>
    <?php
    if (!empty($transactions)) {
        ?>
            <div class="paging-links-wrapper">
                <?php echo $this->element('paging_links'); ?>
            </div>
        <?php
    }
    ?>
   
<?php endif; ?>

            
                       <!-- / Contenidos -->

        </div>
    </div>
</div>