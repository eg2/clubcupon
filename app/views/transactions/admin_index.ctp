<?php 
$debit_total_amt   = 0;
$credit_total_amt  = 0;
$gateway_total_fee = 0;
$credit            = 1;
$debit             = 1;
if(!empty($this->params['named']['type']) && ($this->params['named']['type'] == ConstTransactionTypes :: AddedToWallet) && !empty($this->params['named']['stat'])) {
    $debit = 0;
}
if(!empty($this->params['named']['type']) && ($this->params['named']['type'] == ConstTransactionTypes::ReferralAmountPaid || $this->params['named']['type'] == ConstTransactionTypes::AcceptCashWithdrawRequest || $this->params['named']['type'] == ConstTransactionTypes::PaidDealAmountToCompany) && !empty($this->params['named']['stat'])) {
    $credit = 0;
	
}
?>

<div class="transactions index ">
	
		<?php echo $form->create('Transaction' , array('action' => 'admin_index', 'type' => 'post', 'class' => 'normal search-form clearfix')); ?>
		<?php echo $form->input('User.username');?>
		<?php //echo $form->autocomplete('User.username', array('label' => __l('User'), 'acFieldKey' => 'Transaction.user_id', 'acFields' => array('User.username'), 'acSearchFieldNames' => array('User.username'), 'maxlength' => '255'));?>
        
        <div class="clearfix date-time-block">
            <div class="input date-time clearfix">
                <div class="js-datetime">
                    <?php echo $form->input('from_date', array('label' => __l('From '), 'type' => 'date', 'minYear' => date('Y')-10, 'maxYear' => date('Y'), 'div' => false, 'empty' => __l('Please Select'))); ?>
                </div>
            </div>
            <div class="input date-time end-date-time-block clearfix">
                <div class="js-datetime">
                    <?php echo $form->input('to_date', array('label' => __l('To '),  'type' => 'date', 'minYear' => date('Y')-10, 'maxYear' => date('Y'), 'div' => false, 'empty' => __l('Please Select'))); ?>
                </div>
            </div>
        </div>  

		<?php echo $form->end('Buscar'); ?>
	

	<div>
	<div class="add-block1">
	<?php if(!empty($transactions)) {?>
	<?php echo $html->link('CSV', array('controller' => 'transactions', 'action' => 'export'.$param_string, 'ext' => 'csv', 'admin' => true), array('class' => 'export', 'title' => 'CSV', 'escape' => false)); ?>
	<?php } ?>
	</div>
	<?php echo $this->element('paging_counter');?>
    <table class="list">
        <tr>
           <th><?php echo $paginator->sort('Fecha', 'Transaction.created');?></th>
            <?php if(empty($this->params['named']['user_id'])):	 ?>
                <th><?php echo $paginator->sort('Usuario', 'User.username');?></th>
            <?php endif; ?>
            <th><?php echo $paginator->sort('Descripcion','TransactionType.name');?></th>
            <?php if(!empty($credit)){ ?>
                <th><?php echo $paginator->sort('Credito', 'Transaction.amount').' ('.Configure::read('site.currency').')';?></th>
            <?php } ?>
            <?php if(!empty($debit)){?>
                <th><?php echo $paginator->sort('Debito', 'Transaction.amount').' ('.Configure::read('site.currency').')';?></th>
            <?php } ?>
            <th><?php echo $paginator->sort('Comisión Medios de Pago ($)', 'Transaction.gateway_fees');?></th>
        </tr>
    <?php
    if (!empty($transactions)):
        foreach ($transactions as $transaction):
    ?>
        <tr>
                <td><?php echo $html->cDateTime($transaction['Transaction']['created']);?></td>
				<?php if(empty($this->params['named']['user_id'])):	 ?>
                    <td class="dl">
                    <?php echo $html->getUserLink($transaction['User']);?></td>
	            <?php endif; ?>
                <td class="dl">
					<?php echo $html->transactionDescription($transaction);?>
                </td>
                <?php if(!empty($credit)) {?>
                    <td class="dr">
                        <?php
                            if($transaction['TransactionType']['is_credit']):
                                echo $html->cCurrency($transaction['Transaction']['amount']);
								$credit_total_amt = $credit_total_amt + $transaction['Transaction']['amount']; 

                            else:
                                echo '--';
                            endif;
                         ?>
                    </td>
                <?php } ?>
                <?php if(!empty($debit)) {?>
                    <td class="dr">
                        <?php
                            if($transaction['TransactionType']['is_credit']):
                                echo '--';
                            else:
                                echo $html->cCurrency($transaction['Transaction']['amount']);
							    $debit_total_amt = $debit_total_amt + $transaction['Transaction']['amount'];
                            endif;
                         ?>
                    </td>
                <?php } ?>
                <td class="dr">
					<?php echo $html->cFloat($transaction['Transaction']['gateway_fees']);
						 $gateway_total_fee = $gateway_total_fee + $transaction['Transaction']['gateway_fees']; ?>
			    </td>
            </tr>
    <?php
        endforeach; ?>

		<tr class="total-block">
            <td colspan="<?php echo (!empty($this->params['named']['user_id'])) ? 2 : 3;?>" class="dr"><?php echo __l('Total');?></td>
             <?php if(!empty($credit)) {?>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($credit_total_amt);?></td>
			 <?php } if(!empty($debit)) {?>
			<td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($debit_total_amt);?></td> 
			<?php } ?>
			<td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($gateway_total_fee);?></td>
        </tr>
   <?php else:
    ?>
        <tr>
            <td colspan="11" class="notice"><?php echo __l('No Transactions available');?></td>
        </tr>
    <?php
    endif;
    ?>
    </table>
	   <?php if(!empty($this->params['named']['user_id'])): ?>
       <table class="list">
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Credit');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($credit_total_amt);?></td>
        </tr>
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Debit');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($debit_total_amt);?></td>
        </tr>
        <tr>
            <td colspan="4" class="dr"><?php echo __l('Withdraw Request');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($transaction['User']['blocked_amount']);?></td>
        </tr>
        <tr class="total-block">
            <td colspan="4" class="dr"><?php echo $transaction['User']['username'].' - '.__l('Account Balance');?></td>
            <td class="dr"><?php echo Configure::read('site.currency') . $html->cCurrency($transaction['User']['available_balance_amount']);?></td>
        </tr>
    </table>
	 <?php endif; ?>
    <?php
    if (!empty($transactions)) {
        ?>
        <style>
            .paging{text-align:center;}
        </style>
            <div style="clear:both; height:50px; padding-top:50px;">
                <?php echo $this->element('paging_links'); ?>
            </div>
        <?php
    }
    ?>
</div>
</div>
