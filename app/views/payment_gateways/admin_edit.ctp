<h2><?php echo __l('Edit Payment Gateways');?></h2>
<div >
  <?php echo $form->create('PaymentGateway', array('class' => 'normal'));?>
  <fieldset>
  <?php
		echo $form->input('id');
		echo $form->input('name',array('label' => __l('Name')));
		echo $form->input('description',array('label' => __l('Description')));		
		echo $form->input('is_test',array('label' => __l('Test Mode')));		
		echo $form->input('email',array('label' => __l('Email')));
  ?>
  </fieldset>
  <?php echo $form->end(__l('Update'));?>
</div>
