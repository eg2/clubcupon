
<h2><?php echo __l('Add Payment Gateways');?></h2>
<div id="breadcrumb">
  <?php $html->addCrumb('Home', array('controller' => 'products','action' => 'home')); ?>
  <?php $html->addCrumb('Settings', array('controller' => 'settings','action' => 'index')); ?>
  <?php $html->addCrumb('Payment Gateways', array('controller' => 'payment_gateways','action' => 'index')); ?>
  <?php $html->addCrumb(__l('Add')); ?>
  <?php echo $html->getCrumbs(' &raquo; '); ?> </div>
<div>
  <?php echo $form->create('PaymentGateway', array('class' => 'normal'));?>
  <fieldset>
  <?php
		echo $form->input('name',array('label' => __l('Name')));
		echo $form->input('description',array('label' => __l('Description')));
		echo $form->input('is_active',array('label' => __l('Status:'), 'type' => 'select', 'options' => $status_options));
		echo $form->input('is_test',array('label' => __l('Test Mode')));
		echo $form->input('gateway_fees',array('label' => __l('Gateway Fees')));
		echo $form->input('payment_trans_key',array('label' => __l('Payment Trans Key'),'after' => __l('You can enter any secret code to be used in transaction like 2checkout secret code', __l('Transaction Key'))));
		echo $form->input('payment_trans_type',array('label' => __l('Payment Trans Type')));
		echo $form->input('payment_delimiter',array('label' => __l('Payment Delimiter')));
		echo $form->input('protx_key',array('label' => __l('Protx Key')));
		echo $form->input('email',array('label' => __l('Email')));
		echo $form->input('seller_id',array('label' => __l('Seller')));
		
	?>
  </fieldset>
  <?php echo $form->end(__l('Add'));?> </div>
