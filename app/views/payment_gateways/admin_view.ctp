<div id="breadcrumb">
<?php $html->addCrumb(__l('Payment Gateways'), array('controller' => 'payment_gateways','action' => 'index')); ?>
<?php $html->addCrumb($paymentGateway['PaymentGateway']['name']); ?>
  <?php echo $html->getCrumbs(' &raquo; ',__l('Home')); ?> </div>
<div class="paymentGateways view">
  <h2><?php echo __l('Payment Gateway');?></h2>
  <dl class="list">
    <?php $i = 0; $class = ' class="altrow"';?>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Id');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cInt($paymentGateway['PaymentGateway']['id']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Created');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cDateTime($paymentGateway['PaymentGateway']['created']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Modified');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cDateTime($paymentGateway['PaymentGateway']['modified']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Name');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['name']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Description');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['description']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Email');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['gateway_fees']);?></dd>
	 <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Gateways Fees');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['email']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Seller Id');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['seller_id']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Payment Trans Key');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['payment_trans_key']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Payment Delimiter');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['payment_delimiter']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Payment Trans Type');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['payment_trans_type']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Protx Key');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cText($paymentGateway['PaymentGateway']['protx_key']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Is Active');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cBool($paymentGateway['PaymentGateway']['is_active']);?></dd>
    <dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __l('Is Test');?></dt>
    <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cBool($paymentGateway['PaymentGateway']['is_test']);?></dd>
  </dl>
</div>
