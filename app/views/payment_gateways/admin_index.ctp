
<h2><?php echo __l('Payment Gateways');?></h2>
<div class="home-page-block">
  <div ><?php echo $this->element('paging_counter');?></div>
  <table class="list">
    <tr>     
      <th class="left"><?php echo $paginator->sort(__l('Gateway'),'name');?></th>
      <th><?php echo $paginator->sort(__l('Account'),'email');?></th>    
      <th><?php echo $paginator->sort(__l('Mode'),'is_test');?></th>
    </tr>
    <?php
if (!empty($paymentGateways)):

$i = 0;
foreach ($paymentGateways as $paymentGateway):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	if($paymentGateway['PaymentGateway']['is_active']):
		$status_class = 'js-checkbox-active';
	else:
		$status_class = 'js-checkbox-inactive';
	endif;
	
?>
    <tr<?php echo $class;?>>	   
      <td class="left">
	    <div class="actions-block">
			<div class="actions round-5-left">
				  <?php echo $html->link(__l('Edit'), array('action' => 'edit', $paymentGateway['PaymentGateway']['id']), array('class' => 'edit js-edit', 'title' => __l('Edit')));?>
			</div>
			</div>			
	  <?php echo $html->cText($paymentGateway['PaymentGateway']['name']);?></td>
      <td class="left"><?php echo $html->cText($paymentGateway['PaymentGateway']['email']);?></td>      
      <td><?php echo ($paymentGateway['PaymentGateway']['is_test']  ? __l('Test') : __l('Real'));?></td>
    </tr>
    <?php
    endforeach;
else:
?>
    <tr>
      <td colspan="12" class="notice"><?php echo __l('No payment gateways available');?></td>
    </tr>
    <?php
endif;
?>
  </table>
  <?php
if (!empty($paymentGateways)) {
?>
<?php
    echo $this->element('paging_links');

	?>

<?php
	
}
?>
</div>