<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">


        <?php if(isset($success)) { ?>

            <div class="success-msg">
                Gracias, recibimos tu mensaje y nos contactaremos los antes posible.
            </div>

        <?php }else{ ?>

            <!-- Contenidos -->


        <h2>Contactanos</h2>
        <?php
            echo $form->create('Contact',    array('class' => 'normal js-contact-form'));
            echo $form->input ('first_name', array('label' => 'Nombre'));
            echo $form->input ('last_name',  array('label' => 'Apellido'));
            echo $form->input ('email',      array('label' => 'Mail'));
            echo $form->input ('telephone',  array('label' => 'Teléfono'));
            echo $form->input ('subject',    array('label' => 'Tema'));
            echo $form->input ('message',    array('label' => 'Mensaje','type' => 'textarea'));
        ?>
        <div class="captcha-block clearfix js-captcha-container">
            <div class="captcha-left">
                <?php echo $html->image(Router::url(array('controller' => 'contacts', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('alt' => __l('[Image: CAPTCHA image. You will need to recognize the text in it; audible CAPTCHA available too.]'), 'title' => __l('CAPTCHA image'), 'class' => 'captcha-img'));?>
            </div>
            <div class="captcha-right">
                <?php
                    echo $html->link('Actualizar CAPTCHA', '#', array('class'      => 'js-captcha-reload captcha-reload', 'title' => 'Actualizar CAPTCHA'));
                    echo $html->link('Versión audio',           array('controller' => 'contacts', 'action' => 'captcha_play'), array('class' => 'captcha-audio', 'title' => 'Versión audio', 'rel' => 'nofollow'));
                ?>
            </div>
        </div>
    <?php
            echo $form->input('captcha', array('label' => 'Código de Seguridad', 'class' => 'js-captcha-input'));
            echo $form->end('Enviar');
        }
    ?>

                    <!-- / Contenidos -->

        </div>
    </div>
</div>
