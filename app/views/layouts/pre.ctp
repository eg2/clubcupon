<?php $title_for_layout = isset($title_for_layout) ? $title_for_layout : ''; ?>
<?php //echo $this->element('head', array('title_for_layout' => $title_for_layout));
echo $this->element(Configure::read('theme.theme_name') . 'pre/head'); 
?>
<body>

  <?php
  	if (Configure::read('app.tracking.enable')) {
  		echo $this->element('trackingGoogle');
  		echo $this->element('trackingWeb');
  	}
   
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
  ?>
  <div id="header_pre">
    <?php echo $html->image('logo.png'); ?>
  </div>
  <div id="wrap" class="landingpre">
    <?php echo $content_for_layout; ?>
    <div id="footer_push"></div>
  </div>
  <div id="footer_pre">
    Copyright &copy; <?php echo date("Y"); ?> Derechos Reservados. - <?php echo $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms'), array('escape' => false, 'target' => '_blank')); ?> - <?php echo $html->link('Pol&iacute;ticas de Privacidad', array('controller' => 'pages', 'action' => 'policy'), array('escape' => false, 'target' => '_blank')); ?>
  </div>
  <?php echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>
</body>
</html>