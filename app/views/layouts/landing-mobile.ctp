<?php 
    header('Access-Control-Allow-Origin: *');
    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element(Configure::read('theme.theme_name') . 'head');
    
?>
<link rel="stylesheet" href="/css/mobile/landing/reset.css">
<link rel="stylesheet" href="/css/mobile/landing/galeria.css">
<link rel="stylesheet" href="/css/mobile/landing/nivo-slider.css">
<link rel="stylesheet" href="/css/mobile/landing/default.css">
<!--script type="text/javascript" src="/js/mobile/jquery-1.9.0.min.js"--><!--/script-->
<script type="text/javascript" src="/js/mobile/jquery.tools.min.js"></script>
<script type="text/javascript" src="/js/mobile/jquery.nivo.slider.js"></script>
    <body>
        <?php
        
            echo $this->element('tracking-dataexpand');
            
            //Flash alerts
            if ($session->check('Message.error'))  {$session->flash('error');}
            if ($session->check('Message.success')){$session->flash('success');}
            if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
            echo $this->element(Configure::read('theme.theme_name') . 'header');
            echo $content_for_layout;
            echo $this->element(Configure::read('theme.theme_name') . 'footer');
            
            //Tracker
            echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false ));
            
        ?>
    </body>
</html>
