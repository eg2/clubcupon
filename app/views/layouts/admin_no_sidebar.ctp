
<?php 

    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element(Configure::read('theme.theme_name') . 'head');
    
?>
    
    <body>
        <?php
        
            //Flash alerts
            if ($session->check('Message.error'))  {$session->flash('error');}
            if ($session->check('Message.success')){$session->flash('success');}
            if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
            echo $this->element(Configure::read('theme.theme_name') . 'header', array('is_admin' => true));
            echo $content_for_layout;
            //echo $this->element(Configure::read('theme.theme_name') . 'footer');
            
            //Tracker
            echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false ));
            
        ?>
    </body>
</html>
