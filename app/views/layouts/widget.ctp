<?php $title_for_layout = isset($title_for_layout) ? $title_for_layout : ''; ?>
<?php
$title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
$browser = $_SERVER['HTTP_USER_AGENT'];
$browserIsLinuxHost = (strpos($browser, 'Linux') !== false);
$browserIsIE7 = (strpos($browser, 'MSIE 7.0') !== false);
$browserIsIE8 = (strpos($browser, 'MSIE 8.0') !== false);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head>
    <?php echo $html->charset(), "\n"; ?>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>
      <?php echo Configure::read('site.name'); ?> |
      <?php echo $html->cText($title_for_layout, false); ?>
    </title>
    <?php
      echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

      $html->css('_reset', null, null, false);
      $html->css('_useful', null, null, false);
      $html->css('plugins/jquery.autocomplete', null, null, false);
      $html->css('plugins/jquery-ui-1.7.1.custom', null, null, false);
      $html->css('plugins/ui.timepickr', null, null, false);
      $html->css('plugins/colorbox', null, null, false);
      if($browserIsIE7) {
        $html->css('ie7', null, null, false);
      }
      if($browserIsIE7 || $browserIsIE8) {
        $html->css('ie7and8', null, null, false);
      }
      if (isset($javascript)) {
        $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => false));
        $javascript->link('libs/jquery', false);
        $javascript->link('facebook', false);
        $javascript->link('libs/jquery.form', false);
        $javascript->link('libs/jquery.blockUI', false);
        $javascript->link('libs/jquery.livequery', false);
        $javascript->link('libs/jquery.metadata', false);
        $javascript->link('libs/jquery.autocomplete', false);
        $javascript->link('libs/jquery-ui-1.7.2.custom.min', false);
        $javascript->link('libs/jquery.countdown', false);
        $javascript->link('libs/jquery.timepickr', false);
        $javascript->link('libs/jquery.overlabel', false);
        $javascript->link('libs/jquery.colorbox', false);
        $javascript->link('libs/date.format', false);
        $javascript->link('libs/jquery.cookie', false);
        $javascript->link('libs/jquery.truncate-2.3', false);
        $javascript->link('libs/jquery.jmap', false);
        $javascript->link('libs/jquery.address-1.2.1', false);
        $javascript->link('libs/facebook-sdk', false);
        $javascript->link('common.js?v=5', false);
      }

      echo $html->meta('description', $meta_for_layout['description']), "\n";
      ?>
      <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />
      <?php echo $asset->scripts_for_layout();  ?>
      <?php if(!$browserIsLinuxHost): ?>
          <script src="/js/libs/sifr/sifr.js" type="text/javascript"></script>
          <script src="/js/libs/sifr/sifr-config.js" type="text/javascript"></script>
      <?php endif; ?>
      <?php echo $html->meta('favicon.png', '/favicon.png', array('type' => 'icon')); ?>
    <link rel="image_src" href="../img/logo-facebook.jpg" />
  </head>
<body>
  <?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
  ?>


    <?php echo $content_for_layout; ?>
  <?php echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>
</body>
</html>