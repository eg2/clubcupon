<?php $title_for_layout = isset($title_for_layout) ? $title_for_layout : ''; ?>
<?php echo $this->element('head', array('title_for_layout' => $title_for_layout)); ?>
<body style="background:none transparent !important;">
  <div class="popup">
    <?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
    ?>
    <?php echo $content_for_layout; ?>
    <?php echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>
    <?php echo $cakeDebug ?>
  </div>
</body>
</html>
