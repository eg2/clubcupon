<?php 

    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element(Configure::read('theme.theme_name') . 'head');
    
?>

    <body>
        <?php
        
            echo $this->element('tracking-dataexpand');

            //layout
            echo $this->element(Configure::read('theme.theme_name') . 'header');
            echo $content_for_layout;
            echo $this->element(Configure::read('theme.theme_name') . 'footer');

        ?>
    </body>
</html>