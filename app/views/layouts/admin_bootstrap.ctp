<?php echo $this->element('admin_bootstrap/head', array('title_for_layout' => $title_for_layout)); ?>
<body>
<?php
	if (Configure::read('app.tracking.enable')) {
		echo $this->element('trackingGoogle');
		echo $this->element('trackingWeb');
	}
?>
  <?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
  ?>
  <?php echo $this->element('header', array('is_admin' => true)); ?>
  <div id="wrap" class="admin">
    <?php
    echo '<div class="admin-sideone box">';
    echo $this->element('admin-sidebar', array('cache' => array('time' => Configure::read('site.element_cache'))));
    echo '</div>';
    echo '<div class="admin-sidetwo box">';
    echo $content_for_layout;
    echo '</div>';
    ?>
    <div id="footer_push"></div>
  </div>
  <div class="clear"></div>
</body>
</html>
