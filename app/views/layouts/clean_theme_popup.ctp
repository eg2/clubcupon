<?php

    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $head_parameters = array ('css_layout' => 'popup'); //para determinar que hoja de estilos levantamos
    echo $this->element(Configure::read('theme.theme_name') . 'head', $head_parameters);

    if ($session->check('Message.error')):
        $session->flash('error');
    endif;
    
    if ($session->check('Message.success')):
        $session->flash('success');
    endif;
    
    if ($session->check('Message.flash')):
        $session->flash();
    endif;
    
    
?>






<?php
    echo $content_for_layout;
    echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false ));
    echo $cakeDebug;
?>
    
</body>
</html>

