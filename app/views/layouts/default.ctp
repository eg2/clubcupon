<?php 
    header('Access-Control-Allow-Origin: *');
    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element(Configure::read('theme.theme_name') . 'head');
    
?>
    
    <body>
        <?php
        if (Configure::read('app.tracking.enable')) {
            echo $this->element('tracking-dataexpand');
       		echo $this->element('trackingGoogle');
       		echo $this->element('trackingWeb');
        }
            //Flash alerts
            if ($session->check('Message.error'))  {$session->flash('error');}
            if ($session->check('Message.success')){$session->flash('success');}
            if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
            echo $this->element(Configure::read('theme.theme_name') . 'header');
            echo $content_for_layout;
            echo $this->element(Configure::read('theme.theme_name') . 'footer');
            echo $this->element('clubcupon/tagCxense');
            echo $this->element('clubcupon/tagEplanningProduct5');
            //Tracker
            echo '<div style="display:none">';
            echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false ));
            echo '</div>';
            
        ?>
        
    </body>
</html>
