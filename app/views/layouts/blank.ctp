<html>
    <head>
        <title>Alta por vendedores</title>
        <style type="text/css">
            
            body{
                font-family: Arial;
                background-color:#eee;}
            
        </style>
    </head>
    <body>
   <?php
	if (Configure::read('app.tracking.enable')) {
  		echo $this->element('trackingGoogle');
  		echo $this->element('trackingWeb');
  	}
  	?> 
        <div>
            <?php echo $content_for_layout; ?>
        </div>
    </body>
</html>
