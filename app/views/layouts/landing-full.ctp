<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml">
  <head>
    <?php echo $html->charset (), "\n"; ?>
    <?php echo $html->meta ('keywords', $meta_for_layout ['keywords']), "\n"; ?>
    <?php echo $html->meta ('description', $meta_for_layout ['description']), "\n"; ?>
    <?php echo $html->meta ('favicon.png', '/favicon.png', array ('type' => 'icon')); ?>
    <title>
      <?php echo Configure::read ('site.name'); ?> |
      <?php echo $html->cText ($title_for_layout, false); ?>
    </title>
    <?php echo $html->css ('_reset'); ?>
    <?php echo $html->css ('landing-full'); ?>
    <?php echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>
  </head>
  <body bgcolor = "#CCCCCC">
    <?php 
        echo $this->element('tracking-dataexpand');
        echo $content_for_layout;
    ?>
  </body>
</html>