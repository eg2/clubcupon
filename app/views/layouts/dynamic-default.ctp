<?php 

    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element(Configure::read('theme_dynamic.theme_name') . 'head');
    
?>
    
    <body>
        <?php
            echo $this->element('tracking-dataexpand');
            
            //Flash alerts
            if ($session->check('Message.error'))  {$session->flash('error');}
            if ($session->check('Message.success')){$session->flash('success');}
            if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
            echo $this->element(Configure::read('theme_dynamic.theme_name') . 'header');
            echo $content_for_layout;
            echo $this->element(Configure::read('theme_dynamic.theme_name') . 'footer');
            
            //Tracker
            echo '<div style="display:none">';
            echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false ));
            echo '</div>';
            
        ?>
    </body>
</html>