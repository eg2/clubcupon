<?php $title_for_layout = isset($title_for_layout) ? $title_for_layout : ''; ?>
<?php echo $this->element('head', array('title_for_layout' => $title_for_layout)); ?>
  <body bgcolor = "#FFFFFF"  style = "margin: 0px;">
    <?php echo $this->element('tracking-dataexpand'); ?>      
    <?php echo $this->element('tracking',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>      
    <?php echo $content_for_layout; ?>
  </body>  
</html>