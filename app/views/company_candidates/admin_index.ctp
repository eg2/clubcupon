<script>
    $(document).ready(function() {
      $('#b_search').click(function() {
         largo = $('#CompanyCandidateQ').val().length ;
         if(largo < 4){
             alert('El criterio de b\xFAsqueda debe contener al menos 4 caracteres');
             $('#CompanyCandidateQ').focus();
             return false;
         }
       });
    });
</script>

<div id="contents">
    <div class="tabs">
        <ul>
            <li>
                <?php
                    $class =  $state == ConstCandidateStatuses::Pending ? 'active' : '';
                    echo $html->link("Pendientes", array('controller' => 'company_candidates', 'action' => 'index', 'state' => ConstCandidateStatuses::Pending), array('class' => $class));
                ?>
            </li>
            <li>
                <?php
                    $class =  $state == ConstCandidateStatuses::Reject ? 'active' : '';
                    echo $html->link("Rechazados", array('controller' => 'company_candidates', 'action' => 'index', 'state' => ConstCandidateStatuses::Reject), array('class' => $class));
               ?>
            </li>
            <li>
                <?php
                    $class =  $state == ConstCandidateStatuses::Draft ? 'active' : '';
                    echo $html->link("Borradores",     array('controller' => 'company_candidates', 'action' => 'index', 'state' => ConstCandidateStatuses::Draft), array('class' => $class));
                ?>
            </li>
            <!-- Buscador -->
            <li>
                <div class="buscador">
                    <?php

                        echo $form->create("CompanyCandidate",  array('action' => 'search'));
                        echo $form->input ('q',                 array('label'  => 'Buscar Empresa'));
                        echo $form->input ('selected',          array('type'   => 'hidden', 'value'=> $selected));
                        $submit_options    = array(
                        'id'    => 'b_search',
                        'label' => 'Buscar',
                        );
                        echo $form->end($submit_options);
                    ?>
                </div>
            </li>
        </ul>
    </div>
</div>

<h2>Empresas Candidatas</h2>

<?php if(count($company_candidates)){ ?>

    <div class="paginador_empresas">
        <?php echo $this->element('paging_links'); ?>
    </div>

    <table class = "list">
        <tr>
            <th>Nombre</th>
            <th>Fiscal</th>
            <th>Email</th>
            <th>Cuit</th>
            <th>Tel&eacute;fono</th>
            <th>Ultima Actualizaci&oacute;n</th>
            <th>Acciones</th>
        </tr>
        
        <?php foreach ($company_candidates as $candidate){ ?>
            <tr>
                <td><?php  echo $candidate['CompanyCandidate']['name'];         ?></td>
                <td><?php  echo $candidate['CompanyCandidate']['fiscal_name'];  ?></td>
                <td><?php  echo $candidate['User']['email'];                    ?></td>
                <td><?php  echo $candidate['CompanyCandidate']['fiscal_cuit'];  ?></td>
                <td><?php  echo $candidate['CompanyCandidate']['contact_phone'];?></td>
                <td><?php  echo $candidate['CompanyCandidate']['modified'];     ?></td>
                <td>
                    <?php
                        //Botones de acciones
                        if($can_edited) {
                        echo $html->link("Editar", array('controller' => 'company_candidates', 'action' => 'details', 'id'=>$candidate['CompanyCandidate']['id']), array ('class' => 'link-button-generic')); 
                        }
                        if($can_approve) {
                          echo $html->link("Aprobar",  array('controller' => 'company_candidates', 'action' => 'aprove',  'id'=>$candidate['CompanyCandidate']['id']), array ('class' => 'link-button-generic'));
                        }
                        if($can_rejected) {
                          echo $html->link('Rechazar', array('controller' => 'company_candidates', 'action' => 'reject',  'id'=>$candidate['CompanyCandidate']['id']), array ('class' => 'link-button-generic'));
                        }
                        if($can_draft) {
                          echo $html->link('Pasar a borrador', array('controller' => 'company_candidates', 'action' => 'reject',  'id'=>$candidate['CompanyCandidate']['id'], 'toDraft'=>'1'), array ('class' => 'link-button-generic'));
                        }
                        if($can_pending) {
                          echo $html->link('Pasar a pendiente', array('controller' => 'company_candidates', 'action' => 'pending',  'id'=>$candidate['CompanyCandidate']['id']), array ('class' => 'link-button-generic'));
                        }
                    ?>
                </td>
            </tr>
        <?php
        }        
    }
    else
    {
        echo '<tr><td>No hay empresas</td></tr>';
    }
?>
            

</table>

<style>
    .add {padding-left:25px; margin:10px 10px 0 0; display:block; float:right;}
    .paginador_empresas {margin:15px 0;}
    .paginador_empresas .paging{text-align:center;}
    .buscador .input {margin:5px 8px 0 5px; width:300px;}
    .buscador .submit {margin: 5px 0 0; width: 70px; float:right;}
    #b_search { padding: 5px 10px;  margin: 0; height: 25px;}
    .buscador label {margin:5px 8px 0 0; min-width:50px; padding:0 5px; color:#777; font-weight: bold;}
</style>
