<div class="companies form clearfix">
	<div id="content" class="box">
		<div id="register">
			<div class="deal-side1">
				<div class="content-r clearfix">
					<?php echo $form->create('CompanyCandidate', array('url' => array('action' => 'reject')));?>
					<?php echo $form->input('id', array('type' => 'hidden')); ?>
					<?php echo $form->input('message', array('type' => 'textarea')); ?>
					<div class="submit-block">
            <div class="submit">
              <?php if($can_draft) { ?>
              <input type="submit" name="SaveOption" value="Borrador" class="form-submit">
              <?php  } ?>
              <?php if($can_rejected) { ?>
              <input type="submit" name="SaveOption" value="Rechazar" class="form-submit">
              <?php  } ?>
            </div>
					</div>
					<?php echo $form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
