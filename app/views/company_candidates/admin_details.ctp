<div class="companies form clearfix">
	<?php if(isset($errores)){?>
	<div id="content" class="box">
		<div id="register">
			<div class="deal-side1">
				<div class="content-r clearfix">
					<h2>Errores</h2>
					<ul><?php
						foreach($errores as $key=>$error){
							echo "<li><i>$error</i> (campo:<u>$key</u>)</li>";
						}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div id="content" class="box">
		<div id="register">
			<div class="deal-side1">
				<div class="content-r clearfix">
					<h2>Archivos</h2>
					<?php
						$CompMediaDir = APP.'media'.DIRECTORY_SEPARATOR.'companies'.DIRECTORY_SEPARATOR;
						foreach (glob($CompMediaDir.$this->data['CompanyCandidate']['user_id'].DIRECTORY_SEPARATOR."*") as $nombre_archivo){
							$path_parts = pathinfo($nombre_archivo);
							echo $path_parts['basename'] ." &raquo; ";
							echo $html->link($html->image('icon-transaction.png'),
							array(
								'controller' => 'CompanyCandidates',
								'action' => 'getfile',
								'user' => $this->data['CompanyCandidate']['user_id'],
								'file' => $path_parts['basename']),
							array(
								'escape' => false)
							);
							echo " ";
							echo $html->link($html->image('icon-delete.png'),
							array(
								'controller' => 'CompanyCandidates',
								'action' => 'delfile',
								'user' => $this->data['CompanyCandidate']['user_id'],
								'file' => $path_parts['basename']),
							array(
								'escape' => false),
              '¿Estás seguro de querer eliminar el archivo de forma permanente?'
							);
							echo "<br>";
						}
					?>
				</div>
			</div>
		</div>
	</div>



<div id="content" class="box">
  <div id="register">
    <div class="deal-side1">
      <div class="content-r clearfix">
	   <?php echo $form->create('CompanyCandidate', array('url' => array('action' => 'details'), 'type' => 'file'));?>
       <?php echo $this->element('company_candidate_fields'); ?>

      <div class="submit-block">
			<input type="submit" name="SaveOption" value="Guardar" class="form-submit" />
			<?php if($can_rejected){?>
			<input type="submit" name="SaveOption" value="Rechazar" class="form-submit" />
			<?php }?>
			<?php if($can_approve){?>
			<input type="submit" name="SaveOption" value="Aprobar" class="form-submit" />
			<?php }?>
        </div>

        <?php echo $form->end(); ?>
</div>
</div>
</div>
</div>
</div>
