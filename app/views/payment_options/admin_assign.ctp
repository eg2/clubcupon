<h2>Administración global de opciones de pago</h2>
<?php
$formOptions = array(
    'type' => 'post',
    'action' => 'admin_assign',
    'class' => 'normal'
);
echo $form->create(false, $formOptions);
?>
<fieldset>
    <?php
    $accountOptions = array(
        'label' => 'Tipo de oferta',
        'type' => 'select',
        'options' => $accounts
    );
    echo $form->input('account', $accountOptions);

    $statusOptions = array(
        'label' => 'Estado de la oferta',
        'type' => 'select',
        'default' => 'CITY_ALL',
        'options' => $statuses
    );
    echo $form->input('status', $statusOptions);

    $citiesOptions = array(
        'label' => 'Ciudad',
        'type' => 'select',
        'options' => $selectableCities
    );
    echo $form->input('city', $citiesOptions);
    ?>
</fieldset>

<table class="list" border="1" cellspacing="20" cellpadding="20">
    <thead>
        <tr>
            <th>Id</th>
            <th>Cuenta</th>
            <th>Gateway</th>
            <th>Nombre</th>
            <th>Plan</th>
            <th>Selección</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($options as $option) {
            echo "<tr>";
            echo "<td>{$option['PaymentOption']['id']}</td>";
            echo "<td>{$option['PaymentSetting']['account']}</td>";
            echo "<td>{$option['PaymentSetting']['gateway']}</td>";
            echo "<td>{$option['PaymentOption']['name']}</td>";
            echo "<td>{$option[0]['plan_name']}</td>";
            $checkboxOptions = array(
                'type' => 'checkbox',
                'div' => false,
                'label' => false,
                'name' => "data[PaymentOption][{$option['PaymentOption']['id']}][selected]"
            );
            $checkboxId = "PaymentOption.{$option['PaymentOption']['id']}.selected";
            echo "<td>{$form->input($checkboxId, $checkboxOptions)}</td>";
            echo "</tr>";
        }
        ?>
    </tbody>
</table>
<?php
echo $form->submit('Aceptar', array('name' => 'ok'));
echo $form->submit('Cancelar', array('name' => 'cancel'));
echo $form->end();
?> 