<div class="userComments form js-ajax-form-container">
    <div class="userComments-add-block js-corner round-5 starcomments">
        <?php echo $form->create('UserComment', array('class' => "normal comment-form clearfix js-comment-form {container:'js-ajax-form-container',responsecontainer:'js-responses'}"));?>
        	<fieldset>
        	<?php
        		echo $form->input('user_id', array('type' => 'hidden'));
        		echo $form->input('comment', array('type' => 'textarea','label' => __l('Comment')));
        	?>
        	</fieldset>
        <?php echo $form->end(__l('Add'));?>
    </div>
</div>