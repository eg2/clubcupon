<div class="userComments form">
<?php echo $form->create('UserComment', array('class' => 'normal'));?>
	<fieldset>
 		<h2><?php echo __l('Add User Comment');?></h2>
	<?php
		echo $form->input('user_id',array('label' => __l('User')));
		echo $form->input('posted_user_id',array('label' => __l('Posted User')));
		echo $form->input('comment',array('label' => __l('Comment')));
	?>
	</fieldset>
<?php echo $form->end(__l('Add'));?>
</div>
