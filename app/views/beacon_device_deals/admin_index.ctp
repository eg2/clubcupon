<?php
    //echo $html->css(Configure::read('theme.asset_version') . '/components/datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery-ui');
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.8.3');
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery-ui');
    
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery.timepicker.min');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery.timepicker');
    
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/bootstrap-datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'bootstrap-datepicker');
    
?>
<script>
    $(document).ready(function () {
        clearview();
        $('#basicExample').timepicker({
            'minTime': '12:00am',
            'maxTime': '24:00pm',
            'showDuration': true
        });
    });
</script>
<style>
form .info {
padding-bottom: 10px !important;
width:100% !important;
}
    .top{
         float: left;
    	height: 70px;
    	width: 100%;
        background:#f0f0f0;
    }
    .top .title1{
        font-weight: bold;
        font-size: 23px;
        padding: 35px 0 0 5px;
        display: block;
        color:#444;
        float:left;
    }
    .left-block, .right-block{min-height:380px}
    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
    .left-block .title2{
        font-weight:bold;
        font-size:18px;
        padding-bottom: 4px;
        display: block;
        color:#f79621;
    }
    .left-block p{
        font-style: italic;
        font-size:12px;
        text-align:right;
    }
    .legend p{
        font-style: italic;
        font-size:12px;
    }
    .legend{
        padding: 8px 0 0 15px;
        float:left;
    }
    .right-block{
        padding:3px;
        float:right;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block{
        padding:3px;
        float:left;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block table{
        width:100%;

    }
    .right-block table{
        width:924px;

    }
    thead td, .orangeHeader
    {
        padding:        5px!important;
        min-height:     25px;
        min-width:    100px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }
    tr td{padding:5px;}
    .deleteIcon, .editIcon, .approveIcon, .rejectIcon, .detailsIcon .updateVisibility{
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0px;
        width:80px;
        display:block;
        float:left;
    }
    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    .detailsIcon{
        background-color: #7F7F7F;
    }
    .updateVisibility{
        background-color: #e7c3c3;
    }
    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
        float: right;
    }
    #pagingLinks{}
    #pagingLinks li{
        float:left;
        padding: 5px;
    }
    .pagLinkArrow{
        font-weight: bold;
    }
    .tblActions{
    }
    .tblActions td{
        margin:0px;
        padding:0px;
        padding-bottom:2px;
    }
     .error-message{
        float:left;
        width:100%;
        margin-bottom:15px;
    }
</style>

<div class="top">
    <span class="title1">Asignar Oferta</span>
</div>
<?php echo $form->create('BeaconDeviceDeal',array ('action'    => $actionForm,         'id'    => 'beaconDeviceDealForm')); ?>
<div class="left-block">
    <span class="title2">Dispositivo</span>
    <p>
       <span class="timeTitle">id de la oferta:</span>
       <?php echo $form->input   ('deal_id', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
       <span class="info">ID de la oferta que se le asignar&aacute; al Beacon </span>
       <p>
         Los negocios pueden tener una franja horaria de atenci&oacute;n.<br>
         Si no se modifica &eacute;sta se considerar&aacute; de 24hs.<br>
       </p>
       <span class="timeTitle">hora desde:</span>
       <?php echo $form->input   ('since', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false)); ?>
       <span class="info">Hora de inicio de atenci&oacute;n del negocio</span>
      
       <span class="timeTitle">hora hasta:</span>
        <?php echo $form->input   ('until', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false)); ?>
       <span class="info">Hora de fin de atenci&oacute;n del negocio</span>
     </p>

    <?php
          echo $form->submit('Guardar',array ('div'       => 'submitButton'));
    ?>
</div>
<?php echo $form->end(); ?>
<div class="center-block">
    <table>
        <thead>
        <td>ID</td>
        <td>NOMBRE</td>
        <td>SUBTITULO</td>
        <td>Acciones</td>
        </thead>

        <?php
        foreach ($deals as $deal) {
            echo '<tr>';
            echo '<td>'. $deal["Deal"]["id"].'</td>';
			echo '<td>'. $deal["Deal"]["name"].'</td>'; 
        	echo '<td>'. $deal["Deal"]["subtitle"].'</td>'; 
        	echo '<td><table class="tblActions">';
            
                
        	echo '<tr><td>';                
              
			echo '</td></tr>';
			echo '</table></td>';
				
        	echo '</tr>';
        }
        ?>

    </table>
    
</div>


<div class="top">
    <span class="title1">Ofertas asignadas</span>
</div>

<div class="center-block">
    <table>
        <thead>
        <td>ID</td>
        <td>NOMBRE</td>
        <td>SUBTITULO</td>
        <td>Acciones</td>
        </thead>

        <?php
        
        foreach ($mydeals as $deal) {
            echo '<tr>';
            echo '<td>'. $deal["ApiDeal"]["id"].'</td>';
			echo '<td>'. $deal["ApiDeal"]["name"].'</td>'; 
        	echo '<td>'. $deal["ApiDeal"]["subtitle"].'</td>'; 
        	echo '<td><table class="tblActions">';
            
                
        	echo '<tr><td>';                
                echo $html->link("Editar", array(
					'controller' => 'beacon_device_deals',
                    'action' => 'edit',
                    $deal['BeaconDeviceDeal']['id']
                        ), array(
                    'class' => 'editIcon',
                    'style' => 'float:left;width:200px;'
                ));
			echo '</td></tr>';
			echo '</table></td>';
        	echo '</tr>';
        }
        ?>

    </table>
    
</div>