<?php
    //echo $html->css(Configure::read('theme.asset_version') . '/components/datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery-ui');
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.8.3');
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery-ui');
    
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery.timepicker.min');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery.timepicker');
    
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/bootstrap-datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'bootstrap-datepicker');
    
?>
<script>
    $(document).ready(function () {
        clearview();
        $('#BeaconDeviceDealSince').timepicker({
            'minTime': '12:00am',
            'maxTime': '23:59pm',
            'timeFormat': 'H:i:s',
            'showDuration': false,
            'scrollDefault': '12:00am',
              
        });
        $('#BeaconDeviceDealUntil').timepicker({
            'minTime': '12:00am',
            'maxTime': '23:59pm',
            'timeFormat': 'H:i:s',
            'showDuration': false,
            'scrollDefault': '23:30pm',
            
        });
    });
</script>
<style>

.required {
    background: url(/img/required_label_bg.png) no-repeat center right;
}

form .info {
padding-bottom: 10px !important;
width:100% !important;
}
    
    .top{
        height:110px;
        background:#f0f0f0;
        background: url(/img/web/clubcupon/new/images/hicon-beacon.png) no-repeat scroll center center transparent;
        border-bottom: 1px solid #eee;
    }
    .top .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
            
        }
    
    .left-block, .right-block{
    	/*height:320px*/
    }
    .left-block{
        padding:15px;
        float:left;
        width:275px;
       /* background:#fafafa;*/
    }
        .left-block .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }
        .left-block p{
            font-style: italic;
            font-size:12px;
        }
    
    .right-block{
        padding:3px 15px;
        float:right;
        width:910px;
        background:#fff;
        position: relative;
    }
    
    .calendarTop{width:212px!important;}
    h3{width:100%!important;}
    
    input{float:none;width: 80px;}
    table{margin-top:15px;}
    .formatDateInput{background:none; border:none; color:#f79621; font-weight:bold; width:325px; padding-left:0;}
    .timeTitle{
        font-weight: bold;
        font-size: 14px;
        font-style: italic;
        margin-top:15px;
        display:block;
        }
        
    #wrapper-since, #wrapper-until, #wrapper-execution, .submitButton{
        position:absolute;
    }
    .ui-datepicker-inline{
        min-height: 255px;
    }
    
    #wrapper-since{
        left:0;
        width:211px;
    }
    #wrapper-until{
        left:238px;
        width:211px;
    }
    #wrapper-execution{
        left:476px;
        width:448px;
    }
        #wrapper-execution .calendarTop{ width:436px!important;}
        
    .submitButton{
       /* top:312px;*/
        left:370px;
        bottom:20px;
    }
    .submitButton input{
        background: orange;
        font-weight:bold;
        border:none;
        color:#fff;
        height:35px;
        width:170px;
    }
    
    .ui-datepicker-current {visibility: hidden!important;}
    
    .error-message{
        float:left;
        width:100%;
        margin-bottom:15px;
    }
    
    /* */
    
    .center-block{
        padding:3px;
        float:left;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block table{
        width:100%;

    }
    thead td, .orangeHeader
    {
        padding:        5px!important;
        min-height:     25px;
        min-width:    100px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }
    tr td{padding:5px;}
    .deleteIcon, .editIcon, .approveIcon, .rejectIcon, .detailsIcon .updateVisibility{
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0px;
        width:80px;
        display:block;
        float:left;
    }
    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    .detailsIcon{
        background-color: #7F7F7F;
    }
    .updateVisibility{
        background-color: #e7c3c3;
    }
    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
        float: right;
    }
    #pagingLinks{}
    #pagingLinks li{
        float:left;
        padding: 5px;
    }
    .pagLinkArrow{
        font-weight: bold;
    }
    .tblActions{
    }
    .tblActions td{
        margin:0px;
        padding:0px;
        padding-bottom:2px;
    }
    
    .top{
         float: left;
    	height: 70px;
    	width: 100%;
        background:#f0f0f0;
    }
    .top .title1{
        font-weight: bold;
        font-size: 23px;
        padding: 35px 0 0 5px;
        display: block;
        color:#444;
        float:left;
    }
    
    .submitButton{
        position:relative;
    }
    
    .titleDealId{
    	width:120px;
    }
    .titleTitle{
    width:180px;
    }
    .titleSince{
    width:100px;
    }
    .titleUntil{
    width:100px;
    }
    .titleAddress{
    width:100px;
    }
    
    
    
</style>
<?php 



$isNew=true;
if(isset($this->data ["BeaconDeviceDeal"] ["id"]) && !empty($this->data ["BeaconDeviceDeal"] ["id"])){
	$isNew=false;
}


?>
<div class="top">
    <span class="title1"><span style="color:#f79621">
    <?php
    if(!$isNew){
      echo 'EDICIÓN - ';
    }else{
	  echo 'NUEVO - ';
	} 
    ?>
    </span>
    Asignar Oferta a Beacon [<?php echo $thisBeaconDevice['BeaconDevice']['name']?>] 
    </span>
</div>
<?php echo $form->create('BeaconDeviceDeal',array ('action'    => 'assign',         'id'    => 'beaconDeviceDealForm')); ?>
<?php echo $form->input('beacon_device_id', array ('type'   => 'hidden','value' => $beaconDeviceId));?>
<?php echo $form->input('id', array ('type'   => 'hidden'));?>
<div class="left-block">
    <p class="required" style="color:red;width:100px;padding-left:400px">
    Campos obligatorios
    </p>
    <p>
       <span class="timeTitle required titleDealId">id de la oferta:</span>
       <?php
       if(!$isNew){
		  echo $form->input   ('deal_id', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'readonly' => true,'div' => false));
       }else{
		  echo $form->input   ('deal_id', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false));
	   } 
       ?>
       <span class="info">ID de la oferta que se le asignar&aacute; al Beacon <br></span>
      
       <span class="timeTitle required titleTitle">texto para notificaci&oacute;n:</span>
       <?php
         echo $form->input   ('title', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false, 'style' => 'width:500px','maxlength'=>72));
       ?>
       <span class="info">texto que aparecer&aacute; en el celular cuando detecte el beacon. Max:72 caracteres. <br></span>
      
       
       
       <span class="timeTitle required titleSince">hora desde:</span>
       <?php
       		if($isNew){
				echo $form->input   ('since', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false,'value' => '00:00:00'));
			}else{
				echo $form->input   ('since', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false));
			} 
       		 
       ?>
       <span class="info">Hora de inicio de atenci&oacute;n del negocio</span>
       <span class="timeTitle required titleUntil">hora hasta:</span>
       <?php
       		if($isNew){
       	 		echo $form->input   ('until', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false,'value' => '23:59:59'));
       	 	}else{
				echo $form->input   ('until', array ('type'   => 'text', 'class' => 'time', 'label' => false, 'div' => false,));
			} 
       	?>
       <span class="info">Hora de fin de atenci&oacute;n del negocio</span>
       
        <span class="timeTitle titleAddress">Ubicaci&oacute;n:</span>
       <?php
         echo $form->input   ('address', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false, 'style' => 'width:500px','maxlength'=>100));
	   
       ?>
       <span class="info">Direcci&oacute;n en donde se instalar&aacute; el Beacon<br></span>
      
     </p>

    <?php
          echo $form->submit('Guardar',array ('div'       => 'submitButton'));
    ?>
</div>
<?php echo $form->end(); ?>



<div class="top">
    <span class="title1">Ofertas asignadas</span>
</div>

<div class="center-block">
    <table>
        <thead>
        <td>ID</td>
        <td>NOTIFICACION</td>
        <td>OFERTA</td>
        <td>HORA INICIO</td>
        <td>HORA FIN</td>
        <td>Acciones</td>
        </thead>

        <?php
        
        foreach ($mydeals as $deal) {

            echo '<tr>';
            echo '<td>'. $deal["ApiDeal"]["id"].'</td>';
			echo '<td>'. $deal["BeaconDeviceDeal"]["title"].'</td>'; 
        	echo '<td>'. $deal["ApiDeal"]["name"].'</td>'; 
        	echo '<td>'. $deal[0]["since"].'</td>'; 
        	echo '<td>'. $deal[0]["until"].'</td>'; 
        	echo '<td><table class="tblActions">';
            echo '<tr><td>';                
                echo $html->link("Editar", array(
					'controller' => 'beacon_device_deals',
                    'action' => 'edit',
                    $deal['BeaconDeviceDeal']['id'],
					$deal['BeaconDeviceDeal']['beacon_device_id']
                        ), array(
                    'class' => 'link-button-generic',
                    'style' => 'float:left;'
                ));
			echo '</td></tr>';
			echo '<tr><td>';
			echo $html->link("Eliminar", array(
					'controller' => 'beacon_device_deals',
					'action' => 'delete',
					$deal['BeaconDeviceDeal']['id'],
					$deal['BeaconDeviceDeal']['beacon_device_id']
			), array(
					'class'=>'js-confirm-action-link link-button-generic',
					'style' => 'float:left;'
			));
			echo '</td></tr>';
			echo '</table></td>';
        	echo '</tr>';
        }
        if(empty($mydeals)){
			echo '<tr>';
			echo '<td colspan="4">El beacon no tiene Ofertas asignadas</td>';
			echo '</tr>';
		}
        ?>

    </table>
    
</div>