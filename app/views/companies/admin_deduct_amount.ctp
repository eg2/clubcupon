<div class="transactions form">
<?php echo $form->create('Company', array('action' => 'deduct_amount','class' => 'normal'));?>
	<fieldset>
 		<h2><?php echo __l('Deduct Amount');?></h2>
	<?php
		foreach($companies as  $company)
		{
			?>
			<h3><?php echo $html->cText($company['Company']['name']); ?></h3>
			<?php echo __l('Available Balance: ').Configure::read('site.currency').$html->cCurrency($company['User']['available_balance_amount']);?>
            <?php if($company['User']['available_balance_amount'] > 0): ?>
				<?php echo $form->input('Company.'.$company['Company']['id'].'.amount', array('after' => Configure::read('site.currency'), 'label' => __l('Amount')));?>
                <?php echo $form->input('Company.'.$company['Company']['id'].'.user_id', array('type' => 'hidden', 'value' => $company['Company']['user_id']));?>
                <?php echo $form->input('Company.'.$company['Company']['id'].'.description', array('type' => 'textarea', 'label' => __l('Description')));?>
             <?php endif; ?>
            <?php
		}
	?>
	</fieldset>
<?php echo $form->end(__l('Update'));?>
</div>
