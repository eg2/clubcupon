<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <Placemark>
      <name><?php echo $company['Company']['name']; ?></name>
      <description>
        <![CDATA[
          <h1><?php echo $company['Company']['name']; ?></h1>
          <address>
          	<?php
                $address = (!empty($company['Company']['address1'])) ? $company['Company']['address1'] : '';
                $address.= (!empty($company['Company']['address2'])) ? ', ' . $company['Company']['address2'] : '';
                $address.= (!empty($company['Company']['City']['name'])) ? ', ' . $company['Company']['City']['name'] : '';
                $address.= (!empty($company['Company']['State']['name'])) ? ', ' . $company['Company']['State']['name'] : '';
                $address.= (!empty($company['Company']['Country']['name'])) ? ', ' . $company['Company']['Country']['name'] : '';
                $address.= (!empty($company['Company']['zip'])) ? ', ' . $company['Company']['zip'] : '';
                $address.= (!empty($company['Company']['phone'])) ? ', ' . $company['Company']['phone'] : '';
				echo $address;
			?>
          </address>
          <p>
               <img title="testingcomp" alt="[Image: <?php echo $company['Company']['name']; ?>]" class="" src="<?php echo Router::url('/',true).$html->getImageUrl('UserAvatar', $company['User']['UserAvatar'], array('dimension' => 'medium_thumb', 'alt' => sprintf(__l('[Image: %s]'), $html->cText($company['User']['username'], false)), 'title' => $html->cText($company['Company']['name'], true)));?>"/>
			  <?php echo $html->truncate($company['Company']['company_profile'],20); ?>
		  </p>
          <?php if(!empty($company['Deal'])): ?>
              <dl>
                  <?php foreach($company['Deal'] as $deal): ?>
                      <dt>
						<a href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['slug']),true);?>" title = "<?php echo $deal['name'];?>">
							<?php echo  $deal['name'];?>
						</a>
                      <dd><?php echo $html->truncate($deal['description'],50); ?></dd>
                  <?php endforeach; ?>
              </dl>
          <?php endif; ?>
        ]]>
      </description>
      <Point>
          <coordinates><?php echo $company['Company']['longitude']; ?>,<?php echo $company['Company']['latitude']; ?></coordinates>
      </Point>
    </Placemark>
  </Document>
</kml>
