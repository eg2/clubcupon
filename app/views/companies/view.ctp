<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">

            <!-- Contenidos -->




    <h2><?php echo $html->cText($company['Company']['name']);?></h2>
    <?php if (Configure::read('company.is_show_company_statistics')) { ?>
        <dl class="list statistics-list clearfix">
        <dt>Oferta</dt>
        <dd>(<?php echo $html->cInt($statistics['deal_created']);?>)</dd>
        <?php
            if (Configure::read('user.is_company_actas_normal_user')){
                if (Configure::read('company.is_show_referred_friends') && Configure::read('user.is_referral_system_enabled')) {
        ?>
                    <dt>Usuarios Referidos</dt>
                    <dd>(<?php echo $html->cInt($statistics['referred_users']);?>)</dd>
                <?php
                }
                if (Configure::read('company.is_show_friend') && Configure::read('friend.is_enabled')) { ?>
                    <dt>Amigos</dt>
                    <dd>(<?php echo $html->cInt($statistics['user_friends']);?>)</dd>
                <?php } ?>
                <dt>Ofertas Compradas</dt>
                <dd>(<?php echo $html->cInt($statistics['deal_purchased']);?>)</dd>
                <dt>Regalos enviados</dt>
                <dd>(<?php echo $html->cInt($statistics['gift_sent']);?>)</dd>
                <dt>Regalo recibido</dt>
                <dd>(<?php echo $html->cInt($statistics['gift_received']);?>)</dd>
        <?php } ?>
        </dl>
    <?php } ?>
    <div class="viewpage-content">
        <div class="user-avatar user-view-image">
            <?php echo $html->showImage('UserAvatar', $company['User']['UserAvatar'], array('dimension' => 'big_thumb', 'alt' => sprintf(__l('[Foto: %s]'), $html->cText($company['User']['username'], false)), 'title' => $html->cText($company['User']['username'], false)));?>
        </div>
        <div class="user-view-content">
            <dl class="list">
            <dt>Direcci&oacute;n</dt>
                <dd>
                    <address>
                        <?php
                            echo $html->cText($company['Company']['address1']);
                            echo $html->cText($company['City']['name']) . ',';
                            echo $html->cText($company['State']['name']);
                            echo $html->cText($company['Country']['name']);
                            echo $html->cText($company['Company']['zip']);
                        ?>
                    </address>
                </dd>
                <?php if(!empty($company['Company']['url'])){ ?>
                    <dt>URL</dt>
                    <dd><a href="<?php echo $company['Company']['url'];?>" title="<?php echo $html->cText($company['Company']['url'],false);?>" target="_blank"><?php echo $html->cText($company['Company']['url'],false);?></a></dd>
                <?php }
                if(!empty($company['Company']['phone'])){ ?>
                    <dt>Tel&eacute;fono</dt>
                    <dd><?php echo $html->cText($company['Company']['phone']);?></dd>
                <?php } ?>
            </dl>
        </div>
        <?php
            if(!empty($company['Company']['company_profile'])) {
                echo '<p class="width100 floatLeft">' . $html->cHtml($company['Company']['company_profile']) . '</p>';
            }

            if(!empty($company['Company']['latitude']) and !empty($company['Company']['longitude'] )) {
                echo '<h4 class="subtitle">'. __l('Locate Us on Google Maps').'</h4>';
                echo '<a href="http://maps.google.com/maps?q=' . Router::url(array('controller' => 'companies', 'action' => 'view',$company['Company']['slug'],'ext' => 'kml'),true).'" title="'. $company['Company']['name'].'" target="_blank">';
                echo '<img border="0" alt="[Image:' . $company['Company']['name'] . 'Static Google Map]" src="http://maps.google.com/maps/api/staticmap?center='.$company['Company']['latitude'].','.$company['Company']['longitude'].'&amp;markers=color:blue|label:A|'.$company['Company']['latitude'].','.$company['Company']['longitude'].'&amp;zoom='.Configure::read('GoogleMap.static_map_zoom_level').'&amp;size=320x320&amp;sensor=false"/>';
                echo '</a>';
            }
        ?>
    </div>
        <div class="menu-submenu">
            <ul class="bloque-menu clearfix">
                    <?php
                        if (Configure::read('company.is_show_deal_owned')) {
                            echo '<li>' . $html->link('Ofertas', array('controller' => 'deals', 'action' => 'company_deals', 'company_id' =>  $company['Company']['id']),array('title' => 'Ofertas')) . '</li>';
                        }
                        if (Configure::read('user.is_company_actas_normal_user')) {

                            if (Configure::read('company.is_show_deal_purchased')) {
                                echo '<li>' . $html->link('Ofertas Compradas', array('controller' => 'deal_users', 'action' => 'user_deals', 'user_id' =>$company['Company']['user_id']),array('title' => 'Ofertas Compradas')) .'</li>';
                            }
                            if (Configure::read('company.is_show_friend') && Configure::read('friend.is_enabled') && $auth->user() ) {
                                echo '<li>' . $html->link('Amigos', array('controller' => 'user_friends', 'action' => 'myfriends', 'user_id' =>$company['Company']['user_id']),array('title' => 'Amigos')) . '</li>';
                            }
                            if (Configure::read('company.is_show_referred_friends') && Configure::read('user.is_referral_system_enabled')) {
                                echo '<li>' . $html->link('Usuarios Referidos', array('controller' => 'users', 'action' => 'referred_users', 'user_id' =>$company['Company']['user_id']),array('title' => 'Usuarios Referidos')) . '</li>';
                            }

                        }
                    ?>
            </ul>
        </div>

</div>

            <!-- / Contenidos -->

        </div>
    </div>
</div>