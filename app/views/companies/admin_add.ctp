<div class="companies form clearfix">
	<h2>A&ntilde;adir empresa</h2>
	<?php
		echo $form->create('Company', array('class' => 'normal', 'enctype' => 'multipart/form-data'));
	?>

        <?php

        if (ConstUserTypes::isLikeSuperAdmin ($auth->user ('user_type_id')))
        {

        ?>
        <fieldset class="form-block">

              <legend>Validaci&oacute;n</legend>
              <?php
                echo $form->input('validated'                  ,array('label' => 'Datos validados: ','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Si', '0' => 'No')));
              ?>

        </fieldset>
        <?php
        }
        ?>

	<fieldset class="form-block">

            <legend>Cuenta</legend>
            <?php
                echo $form->input('name'                       ,array ('label' => 'Nombre de fantasía'));
                echo $form->input('phone'                      ,array ('label' => 'Teléfono'));
                echo $form->input('contact_phone'              ,array ('label' => 'Teléfono de contacto', 'info' => 'Ingresar solo n&uacute;meros. Este telefono será publicado como dato de contacto en el cupón.'));
                echo $form->input('url'                        ,array ('label' => 'URL', 'info' => __l('eg. http://www.example.com')));
                echo $form->input('User.username'              ,array ('info'  => 'Mínimo 3 caracteres, Máximo 20. Sin espacios y caracteres especiales.','label' => 'Nickname'));
                echo $form->input('User.email'                 ,array ('label' => 'Email'));
                echo $form->input('User.passwd'                ,array ('label' => 'Contrase&ntilde;a'));
                echo $form->input('User.confirm_password'      ,array ('type'  => 'password', 'label' => 'Confirmar contrase&ntilde;a'));
                echo $form->input('is_online_account'          ,array ('type'  => 'hidden','value' => '1'));
                echo $form->input('bac_portal_id'              ,array ('label' => 'Portal Id', 'info' => 'Ingresar Identificador del portal asociado a la empresa en BAC'));
                echo $form->input('sap_code'                   ,array ('label' => 'Nro. Proveedor SAP', 'maxlength' => '10'));
                echo $form->input('seller_id'                  ,array ('label' => 'Vendedor','type' => 'select', 'options' => $sellers, 'empty'=> '== Seleccionar =='));
                echo $form->input('logo_image'                 ,array ('label' => 'Logo', 'type' => 'file'));
                echo '<div class="notice">La imagen debe tener 158 px de ancho / 59 px de alto</div>';
                echo '<div style="clear:both"></div>';
                echo $form->input('is_tourism'                 ,array ('label' => 'Es compañía de turismo'));
                echo $form->input('is_end_user'                ,array ('label' => 'Se factura al usuario final'));
                echo $form->input('has_posnet_device'          ,array ('label' => 'Tiene terminal posnet'));
                echo $form->input('is_sales_report_enabled'          ,array ('label' => 'Se le envía reporte de ventas'));
            ?>
	</fieldset>

	<fieldset class="form-block">
            <legend>Direcci&oacute;n</legend>
            <?php
                echo $form->input('address1'                   ,array('label' => 'Dirección'));
                echo $form->input('country_id'                 ,array('label' => 'Pa&iacute;s'));
                echo $form->autocomplete('State.name'          ,array('label' => 'Provincia', 'acFieldKey' => 'State.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
                echo $form->autocomplete('City.name'           ,array('label' => 'Localidad', 'acFieldKey' => 'City.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
                echo $form->input('zip'                        ,array('label' => 'C&oacute;digo Postal'));
            ?>
	</fieldset>
	<fieldset class="form-block">
            <legend>Facturación y Liquidaci&oacute;n</legend>
            <?php
                echo $form->input('is_pending_accounting_event', array('label' => 'Tiene eventos contables pendientes', 'default' => 0, 'type' => 'checkbox'));
            ?>
	</fieldset>

	<fieldset class="form-block">
            <legend><?php echo __l('Locate yourself on google maps'); ?></legend>
            <script src="http://maps.googleapis.com/maps/api/js?&sensor=false" type="text/javascript"></script>
		<div id="show-map" style="height: 260px; width: 630px;">

		</div>
		<?php
			echo $form->input('latitude' ,array('type' => 'hidden', 'id'=>'latitude'));
			echo $form->input('longitude',array('type' => 'hidden', 'id'=>'longitude'));
		?>
		<script type="text/javascript">
		var myOptions = {
          center: new google.maps.LatLng(-34.612, -58.450),
		  zoom: 8,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("show-map"),myOptions);
		var marker = new google.maps.Marker({
				position: myOptions.center,
				map: map,
				visible: false,
				animation: google.maps.Animation.b,
				title: 'Aqui estoy!'
			});

		google.maps.event.addListener(map, 'click', function(ev) {
			$('input#latitude').attr('value',ev.latLng.lat());
			$('input#longitude').attr('value',ev.latLng.lng());
			marker.setPosition(ev.latLng);
			marker.setVisible(true);
			marker.setAnimation(google.maps.Animation.b);
		  });

		</script>
	</fieldset>

    <fieldset class="form-block">
              <legend><?php echo __l('Datos Fiscales'); ?></legend>
              <?php
                echo $form->input('fiscal_name'                 ,array('label' => 'Razon Social'));
                echo $form->input('fiscal_address'              ,array('label' => 'Domicilio Comercial'));
                echo $form->autocomplete('FiscalCity.name'      ,array('label' => 'Localidad', 'acFieldKey' => 'FiscalCity.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
                echo $form->autocomplete('FiscalState.name'     ,array('label' => 'Provincia', 'acFieldKey' => 'FiscalState.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
                echo $form->input('fiscal_phone'                ,array('label' => 'Teléfono'));
                echo $form->input('fiscal_fax'                  ,array('label' => 'Fax'));
                echo $form->input('fiscal_cuit'                 ,array('label' => 'CUIT'));
                echo $form->input('fiscal_iibb'                 ,array('label' => 'Nro Insc. IIBB'));
                echo $form->input('percepcion_iibb_caba'        ,array('label' => 'Percepción IIBB CABA','type' => 'select',    'options' => array('' => 'Seleccionar','1' => 'Exento', '0' => 'No Exento')));
                echo $form->input('fiscal_cond_iva'             ,array('label' => 'Condición frente al IVA','type' => 'select', 'options' => array('monotributo' => 'Monotributo', 'exento' => 'Exento', 'ri' => 'Responsable Inscripto')));
                echo $form->input('fiscal_bank'                 ,array('label' => 'Banco'));
                echo $form->input('fiscal_bank_account'         ,array('label' => 'Nro de Cuenta'));
                echo $form->input('fiscal_bank_cbu'             ,array('label' => 'CBU'));
                echo $form->input('persona'                     ,array('label' => 'persona','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Titular', '0' => 'No titular')));
                echo $form->input('declaracion_jurada_terceros' ,array('label' => 'Declaración jurada de pago a terceros','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Si', '0' => 'No')));
              ?>
	</fieldset>
    
    <fieldset class="form-block">
        <?php
        echo '<legend>Datos de pago por cheque:</legend>';
        echo $form->input('cheque_noorden'              ,array('label' => 'No a la orden ','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Si', '0' => 'No')));
        ?>
	</fieldset>

        <div class="submit-block">
            <?php echo $form->submit('Crear empresa'); ?>
        </div>

        <?php echo $form->end(); ?>
        <style>
            .notice{
                background: #CCC;
                clear: both;
                color: #666666;
                font-size: 14px;
                font-weight: bold;
                padding: 5px;
                margin:5px 144px;
            }

        </style>
</div>

<script>
    $(document).ready(function() {
        clearview();
    });
</script>