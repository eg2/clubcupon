<script>
    $(document).ready(function() {
      $('#b_search').click(function() {
         largo = $('#CompanyQ').val().length ;
         largoid = $('#CompanyId').val().length ;

         if((largo < 4 ) && largoid == 0){
             alert('El criterio de b\xFAsqueda debe contener al menos 4 caracteres');
             $('#CompanyQ').focus();
             return false;
         }
       });
       $('#CompanyQ').focus(function() {
           $('#CompanyId').val('');
       });
       $('#CompanyId').focus(function() {
           $('#CompanyQ').val('');
       }); 
    });
</script>
<style>
<!--
 #pagingLinks{}
    
        #pagingLinks li{
            float:left;
            padding: 5px;
        }
    
        .pagLinkArrow{
            font-weight: bold;
        }
-->
</style>
<!-- <h2><?php echo $pageTitle;?></h2> -->
<div id="contents">
    <div class="tabs">
        <ul>
            <?php
            
                foreach ($companyStatuses as $id => $companyStatus)
                {
                    echo '<li><a ' ;
                    if($id == $selected){ echo 'class="active" ';}
                    echo 'href="/admin/companies/tab:'. $id .'" >' . $companyStatus . '</a></li>';
                }
                
            ?>
            <!-- Buscador -->
            <li>
                <div class="buscador">
                    <?php

                        echo $form->create("Company",  array('action' => 'search'));
                        echo $form->input ('q',        array('label'  => 'Buscar Empresa'));
                        echo $form->input ('id', array('label'=>'Buscar por Id:', 'type'=> 'text'));
                        echo $form->submit("Buscar", array('id'=> 'b_search'));
                        if($selected != 2){
                            echo $form->input ('selected', array('type'   => 'hidden', 'value' => $selected));
                        }
                    ?>
                </div>
            </li>
        </ul>
    </div>
</div>


<div>
    
<?php
    //Tenemos empresas para mostrar?
    if (!empty($companies_for_view)){
        

        echo $html->link('A&ntilde;adir empresa', array('controller' => 'companies', 'action' => 'add'), array('class' => 'add','title'=>'A&ntilde;adir empresa','escape' => false));        
        
        echo $form->create('Company' , array('class' => 'normal js-ajax-form {"container" : "js-search-responses"}','action' => 'update'));
        echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url']));
        echo $this->element('paging_counter');
        
        //Iniciamos la tabla de empresas para mostrar
        echo '<table class="list" >';
        
        //Encabezado de tabla
        echo '<tr>';
        
            echo '<th>Seleccionar</th>';

            echo '<th> ID </th>';
            
            echo '<th>' . $paginator->sort('Nombre', 'Company.name') . '</th>';

            echo '<th>Direcci&oacute;n</th>';

            echo '<th>' . $paginator->sort('Email', 'User.email') . '</th>';

            echo '<th>' . $paginator->sort('Usuario', 'User.username') . '</th>';

            echo '<th>' . $paginator->sort('URL', 'Company.url') . '</th>';

            echo '<th>Percepci&oacute;n IIBB CABA</th>';

            echo '<th>Banco</th>';

            echo '<th>Nro. cuenta</th>';

            echo '<th>CBU</th>';

            echo '<th>Persona</th>';

            echo '<th>Pago a terceros</th>';

            echo '<th>Cheque no a la orden</th>';

            echo '<th>Acept&oacute; T&C de CCYA</th>';
            
            echo '<th>' . $paginator->sort('Saldo disponible', 'User.available_balance_amount').' ('.Configure::read('site.currency').')</th>';

        echo '</tr>';
            
        //Iteramos mostrando los registros
        foreach ($companies_for_view as $company){
            
            $status_class = $company['Company']['is_company_profile_enabled'] ? 'js-checkbox-active' : 'js-checkbox-inactive';
            echo '<tr class="' . $status_class .'">';
            
                //Checkbox - hover acciones
                echo '<td>';
                
                    //Hover de acciones
                    echo '<div class="actions-block">';
                        echo '<div class="actions round-5-left">';
                            echo '<span>' . $html->link('Editar', array('action' => 'edit', $company['Company']['id']), array('class' => 'edit js-edit', 'title' => 'Editar')) . '</span>';

                            if(isset($this->params['named']['tab']) && $this->params['named']['tab'] != ConstMoreAction::Offline || !empty($this->params['named']['q'])) {
                                if(Configure::read('user.is_email_verification_for_register') and (!$company['User']['is_active'] or !$company['User']['is_email_confirmed'])){
                                    echo $html->link('Reenviar Activaci&oacute;n', array('controller' => 'users', 'action'=>'resend_activation', $company['User']['id'],'type' => 'company', 'admin' => false),array('escape'=>false, 'title' => 'Reenviar Activaci&oacute;n','class' =>'recent-activation'));
                                }
                            }
                            echo '<span>' . $html->link('Cambiar la contrase&ntilde;a', array('controller' => 'users', 'action'=>'admin_change_password', $company['User']['id']), array('title' => 'Cambiar la contrase&ntilde;a','class' => 'password','escape' => false)) . '</span>';
                            echo '<span>' . $html->link('Puntos de Retiro', array('controller' => 'shipping_addresses', 'action'=>'admin_index', $company['Company']['id']), array('title' => 'Puntos de Retiro','class' => 'homepage','escape' => false)) . '</span>';
                        echo '</div>';
                    echo '</div>';
                    
                    //Checkbox
                    echo $form->input('Company.'.$company['Company']['id'].'.id', array('type' => 'checkbox', 'id' => "admin_checkbox_".$company['Company']['id'], 'label' => false, 'class' => $status_class.' js-checkbox-list'));
                
                echo '</td>';
                
                //ID empresa
                echo '<td>';
                echo $company['Company']['id'];
                echo '</td>';
                
            
                //Nombre empresa
                echo '<td>';
                    if(!empty($company['Company']['logo_image'])){
                        echo $html->image ($company['Company']['logo_image'], array ('alt' => $company['Company']['name'], 'width' => '158', 'height' => '59')) ;
                        echo '<br />';
                    }
                    echo '<strong>';
                        if($company['Company']['is_online_account'] and $company['Company']['is_company_profile_enabled']){
                                echo $html->link($company['Company']['name'], array('controller' => 'companies', 'action'=>'view', $company['Company']['slug'], 'admin' => false), array('title' => sprintf(__l('%s'),$company['Company']['name'])));
                        }else{
                                echo $company['Company']['name'];
                        }
                    echo '</strong>';
                echo '</td>';
      
                //Direccion
                echo '<td>';
            
                    if (!empty($company['Company']['address1'])) { echo $html->cText($company['Company']['address1']); }
                    echo '<br />';
                    if (!empty($company['Company']['address2'])) { echo $html->cText($company['Company']['address2']); }
                    echo '<br />';
                    if (!empty($company['City']['name']))        { echo $html->cText($company['City']['name']); }
                    echo '<br />';
                    if (!empty($company['State']['name']))       { echo $html->cText($company['State']['name']); }
                    echo '<br />';
                    if (!empty($company['Country']['name']))     { echo $html->cText($company['Country']['name']); }
                    echo '<br />';
                    if (!empty($company['Company']['zip']))      { echo $html->cText($company['Company']['zip']); }
                    echo '<br />';
                    if (!empty($company['Company']['phone']))    { echo $html->cText($company['Company']['phone']); }
                   
                echo '</td>';
                //Email
                echo '<td>' . $company['User']['email'] . '</td>';
        
                //Usuario
                echo '<td>' . $company['User']['username'] . '</td>';
        
                //URL
                echo '<td>';
                    //echo '<a href="' . $company['Company']['url'] . '" target="_blank">' . $html->cText($company['Company']['url']) . '</a>';
                echo '</td>';
        
                //IIBB
                echo '<td>';
                    echo $company['Company']['percepcion_iibb_caba'] ? 'Exento' : 'No exento';
                echo '</td>';
                
                //Banco
                echo '<td>' . $company['Company']['fiscal_bank'] . '</td>';
                
                //Nro. Cuenta
                echo '<td>' . $company['Company']['fiscal_bank_account'] . '</td>';
                
                //CBU
                echo '<td>' . $company['Company']['fiscal_bank_cbu'] . '</td>';
                
                //Persona
                echo '<td>';
                    echo $company['Company']['persona'] ? 'Titular' : 'No titular';
                echo '</td>';
                
                //pago a terceros
                echo '<td>';
                    $icon =  $company['Company']['declaracion_jurada_terceros'] ? 'Banner/icon-published.png' : 'Banner/icon-unpublished.png'; 
                    echo $html->image($icon);
                echo '</td>';
                
                //Cheque no a la orden
                echo '<td>';
                    switch ($company['Company']['cheque_noorden'])
                    {
                        case NULL:
                            $texto_cheque = 'Desconocido';
                            break;
                        case 1:
                            $texto_cheque = 'S&iacute;';
                            break;
                        case 0:
                            $texto_cheque = 'No';
                    }

                    echo $texto_cheque;
                echo '</td>';
                
                //Acepto los términos y condiciones de CCYA
                $is_now_eula_ok = $company['Company']['now_eula_ok'] ? 'Banner/icon-published.png' : 'Banner/icon-unpublished.png'; 
                echo '<td>' . $html->image($is_now_eula_ok) . '</td>';
                
                //Saldo disponible
                echo '<td>' . $html->cCurrency($company['User']['available_balance_amount']) . '</td>';
            
            echo '</tr>';
            
             
            
            
        }
        //Cierra la tabla de empresas
        echo '</table>';
        
        //Acciones
        echo '<div class="admin-select-block">';
        
            echo'<div class="admin-checkbox-button">';
                echo $form->input('more_action_id', array('class' => 'js-admin-index-autosubmit', 'label' => false, 'empty' => 'Mas acciones'));
            echo '</div>';
            
            echo'<div style="clear:both; margin:15px 0; width:500px;">';
                echo '<strong>Seleccionar:</strong>';
                echo $html->link('Todos',         '#', array('class' => 'js-admin-select-all',      'title' => 'Todos'));
                echo ' - ';
                echo $html->link('Ninguno',       '#', array('class' => 'js-admin-select-none',     'title' => 'Ninguno'));
                echo ' - ';
                echo $html->link('Deshabilitado', '#', array('class' => 'js-admin-select-pending',  'title' => 'Deshabilitados'));
                echo ' - ';
                echo $html->link('Habilitado',    '#', array('class' => 'js-admin-select-approved', 'title' => 'Habilitados'));
            echo '</div>';

        echo '</div>';
        
        //Links de paginacion
        echo '<div class="paginador_empresas" style="clear:both">';
            echo $this->element('paging_links_companies');
        echo '</div>';
        
        //Cierre de form por los checkboxes
        echo'<div class="hide">';
        echo $form->submit('Submit');
        echo '</div>';
        
    }else{
        
        echo 'No se encontraron empresas';
        
    }

    
?>    
    <style>
        .add {padding-left:25px; margin:10px 10px 0 0; display:block; float:right;}
        .paginador_empresas {margin:15px 0;}
        .paginador_empresas .paging{text-align:center;}
        .buscador .input {margin:5px 8px 0 5px; width:300px;}
        .buscador .submit {margin: 5px 0 0; width: 70px; float:right;}
        #b_search { padding: 5px 10px;  margin: 0; height: 25px;}
        .buscador label {margin:5px 8px 0 0; min-width:50px; padding:0 5px; color:#777; font-weight: bold;}
        .checkbox {
          background: none;
          margin: 5px 0 0 10px;
          /* line-height: 10px; */
        }
        .checkbox input {
          margin: 6px 10px 0 0;
        }
        .checkbox label {
          min-width: 40px;
        }
    </style>    
    

</div>
