<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            <!-- Contenidos -->

<?php
    $disable_cuit = isset ($this->data ['Company']) && (
        (isset ($this->data ['Company']['bac_user_id']) && ((int) $this->data ['Company']['bac_user_id'] != 0)) ||
        (isset ($this->data ['Company']['bac_tourism_id']) && ((int) $this->data ['Company']['bac_tourism_id'] != 0))
        );
    
    if ($auth->user('user_type_id') == ConstUserTypes::Company){
        if (empty($this->params['isAjax']) or !$this->params['isAjax']){
            echo '<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=' . Configure::read('GoogleMap.api_key') . '" type="text/javascript"></script>-->';
            echo $this->element('js_tiny_mce_setting', array('cache' => Configure::read('tiny_mce_cache_time.listing_element_cache_duration'))); 
            echo '<div id="content_full" class="box">'; //cierra al pie...
        }
    } 
?>
    <div class="companies form js-responses  companies">
        <div class="js-tabs">
            <ul class="clearfix">
                <?php
                if ($auth->user('user_type_id') != ConstUserTypes::Company){ 
                    echo '<li>' . $html->link('Mi perfil', '#my-profile') . '</li>';
                }
                ?>
                <li><?php echo $html->link('Cambiar contrase&ntilde;a', array('controller' => 'users', 'action' => 'change_password'), array('escape'=>false, 'title' => 'Cambiar contrase&ntilde;a')); ?></li>
            </ul>
          
            <?php if ($auth->user('user_type_id') != ConstUserTypes::Company){ ?>
                <div id='my-profile' class="clearfix">
                    <?php echo $form->create('Company', array('class' => 'normal js-ajax-form', 'enctype' => 'multipart/form-data')); ?>
                    <div>
                        <h2 class="title">Editar empresa</h2>
                    </div>
                    <fieldset class="form-block">
                        <legend>Cuenta</legend>
                        <?php
                            echo $form->input('id');
                            echo $form->input('name',         array('label' => 'Nombre de fantasía'));
                            echo $form->input('phone',        array('label' => 'Teléfono'));
                            echo $form->input('contact_phone',array('label' => 'Teléfono de contacto', 'info' => 'Este telefono será publicado como dato de contacto en el cupón.'));
                            echo $form->input('url',          array('label' => 'URL', 'info' => 'por ejemplo. http://www.example.com'));
                            echo $form->input('has_pins',     array('label' => 'Cantidad de pines por cupón'));
                            echo $form->input('is_tourism',   array('label' => 'Es compa&ntilde;&iacute;a de turismo'));
                            echo $form->input('is_end_user',  array('label' => 'Se factura al usuario final'));
                        ?>
                    </fieldset>
                    
                    <fieldset class="form-block">
                        <legend>Direcci&oacute;n</legend>
                        <?php
                            echo $form->input       ('address1',   array('label' => 'Dirección'));
                            echo $form->input       ('country_id', array('label' => 'País'));
                            echo $form->autocomplete('State.name', array('label' => 'Provincia', 'acFieldKey' => 'State.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
                            echo $form->autocomplete('City.name',  array('label' => 'Ciudad',    'acFieldKey' => 'City.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
                            echo $form->input       ('zip',        array('label' => 'Código postal'));
                        ?>
                    </fieldset>
                    
                    <fieldset class="form-block">
                        <legend>Datos Fiscales</legend>
                        <?php
                            echo $form->input       ('fiscal_name',        array('label' => 'Razon Social'));
                            echo $form->input       ('fiscal_address',     array('label' => 'Domicilio Comercial'));
                            echo $form->autocomplete('FiscalCity.name',    array('label' => 'Localidad', 'acFieldKey' => 'FiscalCity.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
                            echo $form->autocomplete('FiscalState.name',   array('label' => 'Provincia', 'acFieldKey' => 'FiscalState.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
                            echo $form->input       ('fiscal_phone',       array('label' => 'Teléfono'));
                            echo $form->input       ('fiscal_fax',         array('label' => 'Fax'));
                            echo $form->input       ('fiscal_cuit',        array('label' => 'CUIT', 'disabled' => $disable_cuit));
                            echo $form->input       ('fiscal_cuit',        array('label' => 'CUIT'));
                            echo $form->input       ('fiscal_iibb',        array('label' => 'Nro Insc. IIBB'));
                            echo $form->input       ('fiscal_cond_iva',    array('label' => 'Condición frente al IVA','type' => 'select', 'options' => array('monotributo' => 'Monotributo', 'exento' => 'Exento', 'ri' => 'Responsable Inscripto')));
                            echo $form->input       ('fiscal_bank',        array('label' => 'Banco'));
                            echo $form->input       ('fiscal_bank_account',array('label' => 'Nro de Cuenta'));
                            echo $form->input       ('fiscal_bank_cbu',    array('label' => 'CBU'));
                        ?>
                    </fieldset>
                    
                    <fieldset class="form-block">
                        <legend>Logo</legend>
                        <div class="company-profile-image">
                            <?php echo $html->getUserAvatarLink($this->data['User'], 'normal_thumb'); ?>
                        </div>
                        <?php
                          echo $form->input('UserAvatar.filename', array('type' => 'file', 'size' => '33', 'label' => 'Cargar Logotipo', 'class' => 'browse-field'));
                          echo $form->input('User.id', array('type' => 'hidden'));
                        ?>
                    </fieldset>

                    <fieldset class="form-block">
                        <?php
                          echo $form->input('latitude',  array('type' => 'hidden', 'id' => 'latitude'));
                          echo $form->input('longitude', array('type' => 'hidden', 'id' => 'longitude'));
                        ?>
                        <legend>Buscate en Google Maps</legend>
                            <div class="show-map">
                                <div id="js-map">Por favor, actualiz&aacute; la informaci&oacute;n de direcciones para generar mapa de localizaci&oacute;n.</div>
                            </div>
                    </fieldset>
                    
                    <?php echo $form->end('Actualizar'); ?>
                </div>
        <?php } ?>
    </div>
</div>
        
<?php if (empty($this->params['isAjax']) or !$this->params['isAjax']){ echo '</div>'; }?> 


            <!-- / Contenidos -->

        </div>
    </div>
</div>

