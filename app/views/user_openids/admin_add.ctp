<div class="userOpenids form">
<?php echo $form->create('UserOpenid', array('class' => 'normal'));?>
	<fieldset>
 		<h2><?php echo __l('Add User Openid');?></h2>
	<?php
		echo $form->input('user_id',array('label' => __l('User')));
		echo $form->input('openid',array('label' => __l('OpenID')));
		echo $form->input('verify',array('type' => 'checkbox','label' => __l('Verify')));
	?>
	</fieldset>
<?php echo $form->end(__l('Add'));?>
</div>
