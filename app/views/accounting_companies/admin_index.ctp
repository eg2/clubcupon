<script>

</script>
<style>

    .top{
        height:55px;
        background:#f0f0f0;
        padding:5px;
    }
    .top .title1{
        font-weight: bold;
        font-size: 23px;
        padding: 35px 0 0 5px;
        display: block;
        color:#444;
        float:left;
        width: 100%;
    }

    .left-block, .right-block{min-height:380px}

    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
    .left-block .title2{
        font-weight:bold;
        font-size:18px;
        padding-bottom: 4px;
        display: block;
        color:#f79621;
    }
    .left-block p{
        font-style: italic;
        font-size:12px;
        text-align:right;
    }
    .legend p{
        font-style: italic;
        font-size:12px;
    }
    .legend{
        padding: 8px 0 0 15px;
        float:left;
    }

    .right-block{
        padding:3px;
        float:right;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block{
        padding:3px;
        float:left;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block table{
        width:924px;

    }
    .right-block table{
        width:924px;

    }
    .listaVacia{
        font-color:#CAC9C7;
        font-style:italic
    }


    thead td, .orangeHeader
    {
        padding:        5px!important;
        min-height:     25px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }

    tr td{padding:5px;}


    .deleteIcon, .editIcon, .approveIcon, .rejectIcon, .detailsIcon {
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0 3px;
        width:80px;
        display:block;
        float:left;
    }

    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    .detailsIcon{
        background-color: #7F7F7F;
    }

    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
        float: right;
    }

    #pagingLinks{}

    #pagingLinks li{
        float:left;
        padding: 5px;
    }

    .pagLinkArrow{
        font-weight: bold;
    }

    .input{
        /*padding-top:10px;*/
        margin-top:0px;
    } 
    .submit{
        width:100px;
    }
    .submit input{
        margin-top:0px;
    }
    .submit{
        margin-top:0px;
    }

    .izq label{
        min-width: 0px;
    }
    .input, .text{
        width:230px;
    }
    .legend{
        padding-top:0px;
    }
    .legend p {
        font-size: 11px;
        padding-left:210px;
    }
    .top .title1 {
        padding-top:5px;
    }
</style>

<div class="top">
    <span class="title1">Estado de los pagos</span>
</div>
<div>
    <?php
    echo $this->element("flash_message");
    ?>
</div>
<div class="center-block">
    <table>
        <thead>
        <td>Id Empresa</td>
        <td>Nombre</td>
        <td>Modalidad</td>
        <td>Estado del pago</td>
        <td>Acciones</td>
        </thead>
        <?php
        foreach ($companies as $company) {
            echo '<tr>';
            echo '<td>' . $company['Company']['id'] . '</td>';
            echo '<td>' . $company['Company']['name'] . '</td>';
            echo '<td>' . $company[0]['deal_modality'] . '</td>';
            echo '<td>';
            switch ($company[0]['is_visible']) {
                case 0:
                    echo 'Pendiente';
                    break;
                case 1:
                    echo 'Pagado';
                    break;
                case 2:
                    echo 'Compensado';
                    break;
                default:
                    break;
            }
            echo '</td>';
            echo '<td>';
            if ($company[0]['is_visible'] == 0) {
                echo $html->link("Cambiar a Pagado", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    1,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'editIcon',
                    'style' => 'float:left'
                ));
                echo $html->link("Cambiar a Compensado", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    2,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'approveIcon',
                    'style' => 'float:left'
                ));
            }
            if ($company[0]['is_visible'] == 1) {
                echo $html->link("Cambiar a Pendiente", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    0,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'deleteIcon',
                    'style' => 'float:left'
                ));
                echo $html->link("Cambiar a Compensado", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    2,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'approveIcon',
                    'style' => 'float:left'
                ));
            }
            if ($company[0]['is_visible'] == 2) {
                echo $html->link("Cambiar a Pendiente", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    0,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'deleteIcon',
                    'style' => 'float:left'
                ));
                echo $html->link("Cambiar a Pagado", array(
                    'controller' => 'accounting_companies',
                    'action' => 'update',
                    $company['Company']['id'],
                    $company['AccountingItem']['accounting_calendar_id'],
                    1,
                    $company[0]['deal_modality']
                        ), array(
                    'class' => 'editIcon',
                    'style' => 'float:left'
                ));
            }
            echo '</td>';
            echo '</tr>';
        }
        ?>
        </tr>
    </table>
</div>
