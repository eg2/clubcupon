<style>
    .required label{
        width:190px;
        display:block;
        height:35px;
    }
    .required input{
        float:right;
        margin-top:-30px;
    }
    
    form{
        width:350px;
    }
    
    
</style>

<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix">
            
            <h1 class="h1-format">Restablecer tu contrase&ntilde;a</h1>


            <!-- Contenidos -->

              <?php
                echo $form->create ('User',             array('action' => 'reset' ,'class' => 'normal'));
                echo $form->input  ('user_id',          array('type' => 'hidden'));
                echo $form->input  ('hash',             array('type' => 'hidden'));
                echo $form->input  ('passwd',           array('type' => 'password','label' => 'Introducir una nueva contrase&ntilde;a' ,'id' => 'password'));
                echo $form->input  ('confirm_password', array('type' => 'password','label' => 'Confirmar contrase&ntilde;a'));
                echo $form->submit ('Cambiar contrase&ntilde;a',    array('id'     => 'bt_enviar', 'class'=>'btn-verde', 'escape'=>false));
                echo $form->end    ();
            ?>

            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>