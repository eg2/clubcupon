<?php

    if(empty($this->data['User']['f'])) { 
            //usar esto para escapar el formato y que se vea bien esto en el login tradicional...
            echo '<div id="main" class="container_16 main_frt">';
        }

   /*
    * Hack: Cuando es una petición AJAX y no estamos dentro del form
    * de compra, quiere decir que el usuario intentó realizar un acción
    * AJAX pero ya había perdido la sesión. Lo que hacemos aquí es verificar
    * que no se esté dentro del for de compra por JS y lo redireccionamos
    * al login.
    *
    */

    if(!empty($this->data['User']['is_requested']))
    {
        echo '<script type="text/javascript">' . "\n";
        echo 'if(!$("#buying-form").length){' . "\n";
        echo '  top.location.href = "/users/login/";' . "\n";
        echo '}' . "\n";
        echo '</script>';
    }

    echo $form->create('User', array('action' => 'login'));
    
    

    if (!empty($this->data['User']['is_requested'])){
        echo $form->input('is_requested', array('type' => 'hidden'));
    }
    
    //Facebook
    echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection');
    
    
?>

<script type="text/javascript">
$(document).ready(function() {
    
        $usuario_box  = $("#UserEmail");
		
        $usuario_box.focus(function() {
            if( $usuario_box.attr("value") == "Mail" ) {
                $usuario_box.attr("value", "");
            }
        });
		
        $usuario_box.blur(function() {
            if( $usuario_box.attr("value") == "" ) {
                $usuario_box.attr("value", "Mail");
            }
        });
        
		$pass_box  = $("#UserPasswd");
		
        $pass_box.focus(function() {
            if( $pass_box.attr("value") == "*#$%&#" ) {
                $pass_box.attr("value", "");
            }
        });
		
        $pass_box.blur(function() {
            if( $pass_box.attr("value") == "" ) {
                $pass_box.attr("value", "*#$%&#");
            }
        });
        
       <?php if(empty($this->data['User']['f'])) { ?>
        
            $(function(){
                document.body.style.overflow = 'hidden';
                positionFooter(); 
                function positionFooter(){
                    $("#footer").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#footer").height()-$("#header").height()+6)+"px"})	
                }
                //Capturamos los eventos de ventana
                $(window)
                    .scroll(positionFooter)
                    .resize(positionFooter)
            });

       <?php } ?>
    
    });
</script>

   <!-- Contenidos -->
<div class="grid_16 fmt-bg">

    <div class="grid_11 alpha cuadro-registrado fmt-cajas clearfix">
        <h2>&iquest;Estas Registrado en Club Cup&oacute;n?</h2>
        <div class="grid_4">
            <span>*</span>
			<?php if(isset($this->params['named']['disableCheck'])){?>
			<?php echo $form->input(Configure::read('user.using_to_login'), array('label'=>false,'value' => 'Mail','error' => false)); ?>
			<?php }else{?>
            <?php echo $form->input(Configure::read('user.using_to_login'), array('label'=>false,'value' => 'Mail')); ?>
			<?php }?>
        </div>

        <div class="grid_4">
            <span>*</span>
            <?php
                //Campo password
                echo $form->input('passwd', array('label' => false, 'value' => '*#$%&#'));
                
                //link olvidaste cotnraseña
                echo $html->link('&iquest;Olvidaste tu contrase&ntilde;a?', array('controller' => 'users', 'action' => 'forgot_password', 'admin' => false), array('escape'=>false, 'title' => '&iquest;Olvidaste tu contrase&ntilde;a?', 'class'=>'olv-pass'));
                if (!isset($this->data['User']['is_requested'])) {
                    echo $html->link('Registrarse', array('controller' => 'users', 'action' => 'register'), array('title' => __l('Signup'), 'class' => 'separated'));
                } else {
                    echo $html->link('Registrarse', '#register_anchor', array('title' => __l('Signup'), 'class' => 'separated'));
                }
            ?>
            
        </div>

        <div class="grid_3 alpha omega">
            <?php 
                //echo $form->submit('Ingresar', array('id' => 'bt_login', 'class'=>'btn-ingresar'));
                echo $form->submit('Ingresar', array('class'=>'btn-ingresar'));
            ?>
            
        </div>
    </div>

    <div class="grid_5 alpha omega cuadro-fb fmt-cajas">
        <p>&iquest;Ya ten&eacute;s una cuenta en Facebook?</p>
        <p>Usala para registrarte en Club Cup&oacute;n</p>
        <?php echo $html->link($html->image('/img/theme_clean/fb-btn.jpg'), '#', array('rel'=> 'nofollow','title' => 'fconnect', 'id' => 'facebookLoginButton'), null, false); ?>
    </div>		
</div>

    <?php

        $f = (!empty($_GET['f'])) ? $_GET['f'] : ((!empty($this->data['User']['f'])) ? $this->data['User']['f'] : (($this->params['controller'] != 'users' && ($this->params['action'] != 'login' && $this->params['action'] != 'admin_login')) ? $this->params['url']['url'] : ''));

        if (!empty($f)){ echo $form->input('f', array('type' => 'hidden', 'value' => $f)); }

        echo $form->end();
        /*
         * Esto quedo trunco, ver que paso, ya que el link de registrarse, no figura en la maqueta
        if (!isset($this->data['User']['is_requested'])){
            echo $html->link('Registrarse', array('controller' => 'users', 'action' => 'register'), array('escape'=>true, 'title' => 'Inscripci&oacute;n', 'class' => 'separated'));
        }else{
            echo $html->link('Registrarse', '#register_anchor', array('title' => 'Inscripci&oacute;n', 'class' => 'separated'));
        }
         * 
         * Mismo el recordarme, ver de ponerlo con el control de  if(empty($this->data['User']['f'])) { ....
        //echo $form->input('User.is_remember', array('type' => 'checkbox', 'label' => 'Recordarme en este equipo.'));
         * 
         */

    ?>
    <!-- / Contenidos -->
            
    <?php
        if(empty($this->data['User']['f'])) { 
            //usar esto para escapar el formato y que se vea bien esto en el login tradicional...
            echo '</div>';
        }
    ?>