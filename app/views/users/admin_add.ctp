<div class="users form">
	<h2> <?php echo __l('Add User');?></h2>
<?php echo $form->create('User', array('class' => 'normal'));?>
	<fieldset>
 	
	<?php
    echo $form->input('user_type_id',array('label' => __l('User Type')));
		echo $form->input('email',array('label' => __l('Email')));
		echo $form->input('username',array('label' => __l('Username')));
		echo $form->input('passwd', array('label' => __l('Password')));
	?>
	</fieldset>
  <br />
  <fieldset id="cityPermsContainer">
    <strong>El usuario se crear&aacute; con permisos sobre las siguientes ciudades: </strong>
    <?php $i=0; ?>
    <?php foreach($cities as $cityId => $cityName):  ?>
        <?php echo $form->input('CityPerms.'.$i++.'.city_id_selected',array('value' => $cityId,'type'=>'checkbox','label'=>$cityName)); ?>
    <?php endforeach;  ?>
  </fieldset>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#UserUserTypeId').change(function(){
        if($(this).val() == '<?php echo ConstUserTypes::Agency ?>' || $(this).val() == '<?php echo ConstUserTypes::Partner ?>') {
          $('#cityPermsContainer').removeClass('hidden').show();
        } else {
          $('#cityPermsContainer').hide();
        }
      });
      $('#UserUserTypeId').change();
    })
  </script>

<?php echo $form->end(__l('Add'));?>
</div>