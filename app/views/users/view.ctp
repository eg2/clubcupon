<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            
            <h2><?php echo ucfirst($html->cText($user['User']['username'], false)); ?></h2>
            
            <!-- Contenidos -->
            
	
                <?php
                if($auth->user('username')!=$user['User']['username'] && Configure::read('friend.is_enabled') && ConstUserTypes::isNotLikeAdmin($user['User']['user_type_id']) ){
                    if (!empty($friend)){
                        if ($friend['UserFriend']['friend_status_id'] == ConstUserFriendStatus::Pending){
                            $is_requested = ($friend['UserFriend']['is_requested']) ? 'sent' : 'received';
                            echo $html->link('El pedido de Amistad está pendiente.', array('controller' => 'user_friends', 'action' => 'remove', $user['User']['username'], $is_requested), array('class' => 'user-pending js-friend', 'escape'=>false, 'title' => 'Hac&eacute;clic para eliminar de tu lista de amigos'));
                        }else{
                            $is_requested = ($friend['UserFriend']['is_requested']) ? 'sent' : 'received';
                            echo $html->link('Eliminar Amigo', array('controller' => 'user_friends', 'action' => 'remove', $user['User']['username'], $is_requested), array('class' => 'js-delete remove-user delete js-add-friend', 'title' => 'Hac&eacute; clic para eliminar de tu lista de amigos', 'escape'=>false));
                        }
                    }else{
                        if($html->isAllowed($auth->user('user_type_id'))){
                            echo $html->link('A&ntilde;adir como amigo', array('controller' => 'user_friends', 'action' => 'add', $user['User']['username']), array('escape'=>false, 'class' => 'add add-friend js-add-friend', 'title' => 'A&ntilde;adir como amigo'));
                        }
                    }
                }
                
                if (Configure::read('user.is_show_user_statistics')){ ?>
		
                    <dl>
                        
                        <?php if (Configure::read('user.is_show_referred_friends') && Configure::read('user.is_referral_system_enabled')) {?>
                            <dt>Usuarios Referidos</dt>
                            <dd>(<?php echo $html->cInt($statistics['referred_users']);?>)</dd>
                        <?php } ?>
                            
                        <?php if (Configure::read('user.is_show_friend') && Configure::read('friend.is_enabled')) {?>
                            <dt>Amigos</dt>
                            <dd>(<?php echo $html->cInt($statistics['user_friends']);?>)</dd>
                        <?php } ?>
                            
                        <?php if (Configure::read('user.is_show_deal_purchased')) {?>
                            <dt>Ofertas Compradas</dt>
                            <dd>(<?php echo $html->cInt($statistics['deal_purchased']);?>)</dd>
                        <?php } ?>
                            
                        <dt>Regalos enviados</dt>
                        <dd>(<?php echo $html->cInt($statistics['gift_sent']);?>)</dd>
                        <dt>Regalos recibidos</dt>
                        <dd>(<?php echo $html->cInt($statistics['gift_received']);?>)</dd>
                    
                    </dl>
	
            <?php } ?>
            
            <div class="user-avatar user-view-image">
                <?php echo $html->showImage('UserAvatar', $user['UserAvatar'], array('dimension' => 'big_thumb', 'alt' => sprintf(__l('[Image: %s]'), $html->cText($user['User']['username'], false)), 'title' => $html->cText($user['User']['username'], false)));?>
            </div>
            <div class="user-view-content">
                <?php if(!empty($user['UserProfile']['about_me'])){ ?>
                    <div class="about-content round-5">
                        <?php if(!empty($user['UserProfile']['about_me'])): ?>
                            <h3>Sobre mi</h3>
                            <p><?php echo nl2br($html->cText($user['UserProfile']['about_me']));?></p>
                        <?php endif; ?>
                    </div>
                <?php } ?>
		<dl class="list company-list clearfix round-5">
                    <?php if($user['UserProfile']['created'] != '0000-00-00 00:00:00'){ ?>
			<dt>Miembro desde</dt>
			<dd><?php echo $html->cDate($user['User']['created']);?></dd>
		<?php } ?>
        <?php
			if ($html->checkForPrivacy('Profile', 'is_show_name', $auth->user('id'), $user['User']['id']) || ConstUserTypes::isLikeAdmin($auth->user('user_type_id')) ):
				$name = array();
				if(!empty($user['UserProfile']['first_name'])):
					$name[]= $user['UserProfile']['first_name'];
				endif;
				if(!empty($user['UserProfile']['middle_name'])):
					$name[]= $html->cText($user['UserProfile']['middle_name']);
				endif;
				if(!empty($user['UserProfile']['last_name'])):
					$name[]= $html->cText($user['UserProfile']['last_name']);
				endif;
				if($name):
				?>
				<dt><?php echo __l('Name');?></dt>
					<dd>
					<?php echo implode(' ',$name);?>

					</dd>

				<?php
				endif;
			endif;
		?>
		<?php
			if ($html->checkForPrivacy('Profile', 'is_show_gender', $auth->user('id'), $user['User']['id']) || ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))):
				if (!empty($user['UserProfile']['Gender'])):
		?>
					<dt><?php echo __l('Gender');?></dt>
						<dd><?php echo $html->cText(ConstGenders::$user['UserProfile']['Gender']);?></dd>
		<?php
				endif;
			endif;
		?>
		<?php
			if ($html->checkForPrivacy('Profile', 'is_show_address', $auth->user('id'), $user['User']['id']) || ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))):
				if(!empty($user['UserProfile']['address'])):
		?>
					<dt><?php echo __l('Address');?></dt>
						<dd>
                            <?php if(!empty($user['UserProfile']['address'])) { ?>
                                <p><?php echo $html->cText($user['UserProfile']['address']);?></p>
                            <?php } ?>
                            <?php if(!empty($user['UserProfile']['City']['name'])) { ?>
                                <p><?php echo $html->cText($user['UserProfile']['City']['name']);?></p>
                            <?php } ?>
                            <?php if(!empty($user['UserProfile']['State']['name'])) { ?>
                                <p><?php echo $html->cText($user['UserProfile']['State']['name']);?></p>
                            <?php } ?>
                              <?php if(!empty($user['UserProfile']['Country']['name'])) { ?>
                                <p><?php echo $html->cText($user['UserProfile']['Country']['name']);?></p>
                            <?php } ?>
                             <?php if(!empty($user['UserProfile']['zip_code'])) { ?>
                                <p><?php echo $html->cText($user['UserProfile']['zip_code']);?></p>
                            <?php } ?>
                        </dd>
		<?php
				endif;
			endif;
		?>
		<?php
			if ($auth->user('username')== $user['User']['username'] || ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))):
				if (!empty($user['UserProfile']['paypal_account'])):
		?>
					<dt><?php echo __l('Paypal Account');?></dt>
						<dd><?php echo $html->cText($user['UserProfile']['paypal_account']);?></dd>
		<?php
				endif;
			endif;
		?>
		<?php
			if (!empty($user['UserProfile']['language_id'])):
		?>
				<dt><?php echo __l('Language');?></dt>
					<dd><?php echo $html->cText($user['UserProfile']['Language']['name']);?></dd>
		<?php
			endif;
		?>
	</dl>
	

		</div>
	<div class="js-tabs user-view-tabs">
        <ul class="clearfix">
			<?php if (Configure::read('Profile-is_allow_comment_add') && $html->isAllowed($auth->user('user_type_id')) && ($html->checkForPrivacy('Profile', 'is_allow_comment_add', $auth->user('id'), $user['User']['id']) || ConstUserTypes::isLikeAdmin($auth->user('user_type_id')))): ?>
				<li><?php echo $html->link(__l('Comments'), '#tabs-1');?></li>
			<?php endif; ?>
            <?php if($auth->user('id')): ?>
				<?php if (Configure::read('user.is_show_deal_purchased')): ?>
                    <li><?php echo $html->link(__l('Deal Purchased'), array('controller' => 'deal_users', 'action' => 'user_deals', 'user_id' =>  $user['User']['id']),array('title' => __l('Deals Purchased'))); ?></li>
                <?php endif; ?>
                <?php if (Configure::read('user.is_show_friend') && Configure::read('friend.is_enabled')): ?>
                    <li><?php echo $html->link(__l('Friends'), array('controller' => 'user_friends', 'action' => 'myfriends', 'user_id' =>  $user['User']['id'], 'status' => ConstFriendRequestStatus::Approved),array('title' => __l('Friends'))); ?></li>
                <?php endif; ?>
                <?php if (Configure::read('user.is_show_referred_friends') && Configure::read('user.is_referral_system_enabled')): ?>
                    <li><?php echo $html->link(__l('Referred Users'), array('controller' => 'users', 'action' => 'referred_users', 'user_id' =>  $user['User']['id']),array('title' => __l('Referred Users'))); ?></li>
                <?php endif; ?>
              <?php endif; ?>
        </ul>
		<?php if (Configure::read('Profile-is_allow_comment_add') && $html->isAllowed($auth->user('user_type_id')) && $html->checkForPrivacy('Profile', 'is_allow_comment_add', $auth->user('id'), $user['User']['id']) || ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
			<div id='tabs-1'>
				<div class="main-content-block js-corner round-5">
					<div class="js-responses">
						<?php echo $this->element('user_comments-index', array('username' => $user['User']['username'], 'cache' => Configure::read('user_comments.listing_element_cache_duration'), 'key' => $user['User']['username']));?>
					</div>
				</div>
                <?php if($auth->user('id')): ?>
                    <div class="main-content-block js-corner round-5">
                        <h2><?php echo __l('Add Your comments'); ?></h2>
                        <?php echo $this->element('../user_comments/add');?>
                    </div>
                <?php endif; ?>
			</div>
		<?php endif; ?>
    </div>

            <!-- / Contenidos -->

        </div>
    </div>
</div>