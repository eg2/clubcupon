<?php if(empty($this->params['isAjax']) && empty($this->params['named']['stat'])): ?>
	<div class="js-tabs">
        <ul class="clearfix">
            <li><?php echo $html->link('Usuarios', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => ConstUserTypes::User),array('title' => 'Usuarios'));?></li>
            <?php if(Configure::read('user.is_enable_openid')): ?>
                <li><?php echo $html->link('Usuarios OpenID', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => ConstMoreAction::OpenID),array('title' => 'Usuarios OpenID'));?></li>
			<?php endif; ?>
			<?php if(Configure::read('facebook.is_enabled_facebook_connect')): ?>
                <li><?php echo $html->link('Usuarios de Facebook', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => ConstMoreAction::FaceBook),array('title' => 'Usuarios de Facebook' ));?></li>
			<?php endif; ?>
            <li><?php echo $html->link('Registrado a trav&eacute;s de tarjeta de regalo', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => 'gift_card'),array('escape'=>false, 'title' => 'Registrado a trav&eacute;s de tarjeta de regalo'));?></li>
            <li><?php echo $html->link('Administraci&oacute;n', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => ConstUserTypes::Admin),array('escape'=>false, 'title' => 'Administraci&oacute;n'));?></li>
            <li><?php echo $html->link('Todos', array('controller' => 'users', 'action' => 'index', 'main_filter_id' => 'all'),array('title' => 'Todos'));?></li>
       </ul>
    </div>
<?php else: ?>
		<div class="js-response">
        <?php if(!empty($this->params['named']['main_filter_id']) && empty($this->params['named']['filter_id']) && empty($this->data)): ?>
           <div class="users index js-responses">
             <div class="js-tabs">
                <ul class="clearfix">
                    <li><?php echo $html->link('Usuarios activos', array('controller' => 'users', 'action' => 'index', 'filter_id' => ConstMoreAction::Active,'main_filter_id' => $this->params['named']['main_filter_id']),array('title' => sprintf(__l('Active ').$pageTitle.'(%s)',$active)));?></li>
                    <li><?php echo $html->link('Usuarios inactivos', array('controller' => 'users', 'action' => 'index', 'filter_id' => ConstMoreAction::Inactive, 'main_filter_id' => $this->params['named']['main_filter_id']),array('title' => sprintf(__l('Inactive ').$pageTitle.'(%s)',$inactive))); ?></li>
                    <li><?php echo $html->link('Todos',array('controller'=> 'users', 'action'=>'index', 'filter_id' => 'all','main_filter_id' => $this->params['named']['main_filter_id']),array('title' => sprintf(__l('All ').$pageTitle.'(%s)',$active + $inactive))); ?></li>
                </ul>
             </div>
        <?php else: ?>
         	<div class="js-search-responses">
            <h2><?php echo $pageTitle; ?></h2>
        	<?php if(empty($this->params['named']['from_more_actions'])): ?>
                <?php echo $form->create('User', array('type' => 'post', 'class' => 'normal search-form clearfix js-ajax-form {"container" : "js-search-responses"}', 'action'=>'index')); ?>

                            <?php echo $form->input('q', array('label' => 'Palabra clave')); ?>
                            <?php echo $form->input('main_filter_id', array('type' => 'hidden', 'value' => !empty($this->params['named']['main_filter_id'])? $this->params['named']['main_filter_id']:'')); ?>
                            <?php echo $form->input('filter_id', array('type' => 'hidden', 'value' => !empty($this->params['named']['filter_id'])?$this->params['named']['filter_id']:'')); ?>
                   <div class="input text">
                   <?php
                      $city_opts = array ();
                      foreach ($clubcupon->activeCitiesWithDeals () as $acwd)
                          $city_opts [$acwd ['City']['id']] = $acwd ['City']['name'];
                      echo $form->label ('city_id', 'Ciudad');
                      echo $form->select ('city_id', $city_opts);
                   ?>
                   </div>
                            <?php echo $form->input('tab_check', array('type' => 'hidden', 'value' => '1')); ?>

                            <?php echo $form->submit('Buscar',array('name' => "data['User']['search']"));?>

                <?php echo $form->end(); ?>
                   <div class="clearfix add-block1">

	        	<?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id')) && empty($this->params['named']['from_more_actions'])): ?>

                    <?php echo $html->link('A&ntilde;adir', array('controller' => 'users', 'action' => 'add'), array('escape'=>false,'class' => 'add','title'=>'A&ntilde;adir')); ?>

              <?php endif; ?>
              	<?php if(empty($this->params['named']['from_more_actions'])): ?>

                    <?php
                        echo $html->link('CSV', array('controller' => 'users', 'action' => 'export'.$param_string, 'ext' => 'csv', 'admin' => true), array('title' => 'CSV', 'class' => 'export'));
                    ?>

	            <?php endif; ?>
	              </div>
            <?php endif; ?>
                <?php echo $form->create('User' , array('class' => 'normal js-ajax-form {"container" : "js-moreaction-responses"}','action' => 'update'));  ?>
                <?php echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url']."/"."from_more_actions:1")); ?>
	        	<?php if(empty($this->params['named']['from_more_actions'])): ?>
	                <?php echo $this->element('paging_counter'); ?>
	            <?php endif; ?>
                <div class="overflow-block">
             	<div class="js-moreaction-responses">
                <table class="list">
                    <tr>
                        <th>Seleccionar</th>
                        <th><?php echo $paginator->sort('Email', 'User.email'); ?></th>
                        <th><?php echo $paginator->sort('Usuario', 'User.username'); ?></th>
                        <?php if($this->params['named']['main_filter_id'] == 'gift_card'): ?>
                            <th><?php echo $paginator->sort('Regalo recibido de', 'GiftRecivedFromUser.username'); ?></th>
                        <?php endif; ?>
                        <th>Usuario Referidos</th>
                        <?php if($this->params['named']['filter_id'] == 'all') { ?>
                        <th><?php echo $paginator->sort('Activo', 'User.is_active'); ?></th>
                        <?php } ?>
                        <th><?php echo $paginator->sort('Email Confirmado', 'User.is_email_confirmed'); ?></th>
                        <th><?php echo $paginator->sort('Cantidad de Logins', 'User.user_login_count'); ?></th>
                        <th><?php echo $paginator->sort('IP de Registro', 'User.signup_ip'); ?></th>
                        <th><?php echo $paginator->sort('Creado el', 'User.created'); ?></th>
                        <th><?php echo $paginator->sort('Saldo disponible', 'User.available_balance_amount').' ('.Configure::read('site.currency').')'; ?></th>
                        <th><?php echo $paginator->sort('Puntos', 'User.available_points'); ?></th>
                    </tr>
                <?php
                if (!empty($users)):
                $i = 0;
                foreach ($users as $user):
                    $class = null;
                    if ($i++ % 2 == 0):
                        $class = ' class="altrow"';
                    endif;
                    if($user['User']['is_active']):
                        $status_class = 'js-checkbox-active';
                    else:
                        $status_class = 'js-checkbox-inactive';
                    endif;
                    $online_class = 'offline';
                    if (!empty($user['CkSession']['user_id'])) {
                        $online_class = 'online';
                    }
                ?>
                    <tr<?php echo $class;?>>
                        <td>
                          <?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
                                  <div class="actions-block">
                                      <div class="actions round-5-left">
                                  <?php if(ConstUserTypes::isPrivilegedUserOrAdmin($user['User']['user_type_id'])){ ?>
                                      <span><?php echo $html->link('Editar', array( 'action'=>'edit', $user['User']['id']), array('class' => 'edit js-edit', 'title' => 'Editar'));?></span>
                                  <?php } ?>
                                  <?php if(ConstUserTypes::isNotLikeAdmin($user['User']['user_type_id']) && 1 == 3){ ?>
                                      <span><?php echo $html->link('Borrar', array('action'=>'delete', $user['User']['id']), array('class' => 'delete js-delete', 'title' => 'Borrar'));?></span>
                                  <?php } ?>

                                     <?php if(Configure::read('user.is_email_verification_for_register') and (!$user['User']['is_active'] or !$user['User']['is_email_confirmed'])):
                                              echo $html->link('Reenviar activaci&oacute;n', array('controller' => 'users', 'action'=>'resend_activation', $user['User']['id'], 'admin' => false),array('escape'=>false, 'title' => 'Reenviar activaci&oacute;n','class' =>'recent-activation'));
                                        endif;
                                  ?>
                                  <?php if(ConstUserTypes::isLikeSuperAdmin($auth->user('user_type_id')) && !ConstUserTypes::isLikeSuperAdmin($user['User']['user_type_id'])){?>
                                      <span><?php echo $html->link('Cambiar contrase&ntilde;a', array('controller' => 'users', 'action'=>'admin_change_password', $user['User']['id']), array('escape'=>false, 'title' => 'Cambiar contrase&ntilde;a','class' => 'password'));
                                    
                                      ?></span>
                                 <?php  }?>
                                  <span><?php echo $html->link('Transacciones', array('controller' => 'transactions', 'action'=>'admin_index','user_id' => $user['User']['id']), array('title' => 'Transacciones','class' => 'transaction'));?></span>
                                  <?php if(ConstUserTypes::isLikeSuperAdmin($auth->user('user_type_id'))): ?>
                                    <span><?php echo $html->link('Pagos', array('controller' => 'externalservices', 'action'=>'payments','user_id' => $user['User']['id']), array('title' => 'Pagos','class' => 'payments'));?></span>
                                    
                                    <span><?php echo $html->link('Agregar puntos', array('controller' => 'action_point_users', 'action'=>'add_points','user_id' => $user['User']['id']), array('title' => 'Pagos','class' => 'add_points'));?></span>
									<?php if ($user['User']['is_visible']): ?>
                                    <span><?php echo $html->link ('Ocultar en la lista de vendedores', array ('controller' => 'users', 'action' => 'ocultar', $user ['User']['id'], 'admin' => false), array('class' => 'js-update-is-visible', 'title' => 'Ocultar en la lista de Vendedores')); ?></span>
                                    <?php else: ?>
									<span><?php echo $html->link ('Mostrar en la lista de vendedores', array ('controller' => 'users', 'action' => 'ocultar', $user ['User']['id'], 'admin' => false), array('class' => 'js-update-is-visible', 'title' => 'Ocultar o Mostrar en la lista de Vendedores')); ?></span>
									<?php endif; ?>
								  <?php endif; ?>

                                  <?php if ($user['User']['wallet_blocked']): ?>
                                      <span><?php echo $html->link ('Desbloquear Cuenta Club Cup&oacute;n', array ('controller' => 'users', 'action' => 'update_wallet_blocked', $user ['User']['id'], 'admin' => false), array('escape'=>false, 'class' => 'js-update-wallet-blocked', 'title' => 'Desbloquear  Cuenta Club Cup&oacute;n')); ?></span>
                                  <?php else: ?>
                                      <span><?php echo $html->link ('Bloquear Cuenta Club Cup&oacute;n', array ('controller' => 'users', 'action' => 'update_wallet_blocked', $user ['User']['id'], 'admin' => false), array('escape'=>false, 'class' => 'js-update-wallet-blocked', 'title' => 'Bloquear  Cuenta Club Cup&oacute;n')); ?></span>
                                  <?php endif; ?>
                                      </div>
                                  </div>
                          <?php endif; ?>
                        <?php echo $form->input('User.'.$user['User']['id'].'.id', array('type' => 'checkbox', 'id' => "admin_checkbox_".$user['User']['id'], 'label' => false, 'class' => $status_class.' js-checkbox-list')); ?>
                        </td>
                        <td class="dl"><?php echo $html->cText($user['User']['email']);?></td>
                        <td class="dl">
                            <?php
                            echo $user['User']['username'];
                            if(ConstUserTypes::isPrivilegedUserOrAdmin($user['User']['user_type_id'])){
                              echo ' (' . $html->getUserTypeFriendly($user['User']['user_type_id']) . ')';
                            }
                            ?>
                        </td>
                        <?php if($this->params['named']['main_filter_id'] == 'gift_card'): ?>
                            <td class="dl">
                                <?php echo $user['GiftRecivedFromUser']['username']; ?>
                            </td>
                        <?php endif; ?>
                        <td>
                          - <?php echo $user['RefferalUser']['username']; ?>
                        </td>
                        <?php if($this->params['named']['filter_id'] == 'all') { ?>
                            <td><?php echo ($user['User']['is_active']) ? 'Activo' : 'Inactivo'; ?></td>
                        <?php } ?>
                        <td><?php echo ($user['User']['is_email_confirmed']) ? 'S&iacute;' : 'No'; ?></td>
                        <td><?php echo $html->link($html->cInt($user['User']['user_login_count'], false), array('controller' => 'user_logins', 'action' => 'index', 'username' => $user['User']['username']));?></td>
                        <td>
                        <?php if(!empty($user['User']['signup_ip'])): ?>
                            <?php echo $html->cText($user['User']['signup_ip']).' ['.gethostbyaddr($user['User']['signup_ip']).']' . '('. $html->link('whois', array('controller' => 'users', 'action' => 'whois', $user['User']['signup_ip'], 'admin' => false), array('target' => '_blank', 'title' => 'whois', 'escape' => false)) .')';?>
                        <?php else: echo 'N / A'; endif; ?>
                        </td>
                        <td>
                          <?php if($user['User']['created'] == '0000-00-00 00:00:00'){
                                echo '-';
                            }else{
                                echo $html->cDateTimeHighlight($user['User']['created']);
                            }
                          ?>
                        </td>
                        <td class = "dr">
                        <?php
                          if ($user['User']['wallet_blocked']) {
                            echo '<span class = "monedero red">';
                          } else {
                            echo '<span class = "monedero">';
                          }
                          echo $html->cCurrency($user['User']['available_balance_amount']);
                          echo '</span>';
                        ?>
                        </td>
                        <td class = "dr">
                        <?php
                          if ($user['User']['wallet_blocked']) {
                            echo '<span class = "monedero red">';
                          } else {
                            echo '<span class = "monedero">';
                          }
                          echo $html->cCurrency($user['User']['available_points']);
                          echo '</span>';
                        ?>
                        </td>
                    </tr>
                <?php
                    endforeach;
                else:
                ?>
                    <tr>
                        <td colspan="17" class="notice">No hay usuarios disponibles</td>
                    </tr>
                <?php
                endif;
                ?>
                </table>
                </div>
                </div>
                <?php
                if (!empty($users) && empty($this->params['named']['from_more_actions'])):
                ?>
                    <div class="admin-select-block">
                    <div>
                        Seleccionar:
                        <?php echo $html->link('Todos', '#', array('class' => 'js-admin-select-all', 'title' => 'Todos')); ?>
                        <?php echo $html->link('Ninguno', '#', array('class' => 'js-admin-select-none', 'title' => 'Ninguno')); ?>
                        <?php if($this->params['named']['filter_id'] == 'all') { ?>
                            <?php echo $html->link('Inactivos', '#', array('class' => 'js-admin-select-pending', 'title' => 'Inactivos')); ?>
                            <?php echo $html->link('Activos',   '#', array('class' => 'js-admin-select-approved', 'title' => 'Activos'));  ?>
                        <?php } ?>
                    </div>
                        <div class="admin-checkbox-button"><?php echo $form->input('more_action_id', array('class' => 'js-admin-index-autosubmit', 'label' => false, 'empty' => '-- Más acciones --')); ?></div>
                        </div>
                    <div class="js-pagination">
                        <?php echo $this->element('paging_links'); ?>
                    </div>

                    <div class="hide">
                        <?php echo $form->submit('Submit'); ?>
                    </div>
                <?php
                endif;
                echo $form->end();
                ?>
             </div>
	    <?php if(!empty($this->params['named']['main_filter_id']) && empty($this->params['named']['filter_id']) && empty($this->data)): ?>
            </div>
        <?php endif; ?>

    <?php endif; ?>
	 </div>
<?php endif; ?>