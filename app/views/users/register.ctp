<?php /*
<div style="display:none">
	<div id="re-ccya-pop-up" class="contenedor-popup-recib">
		<form>
		<div class="bloque-ccya-pop2">
			<!--a href="">CERRAR <span>X</span></a-->
			<img src="/img/now/logoYApop.png">
			<p class="title">Bienvenido a <span>Club Cup&oacute;n Ya!</span></p>
			<p>Para utilizar este nuevo servicio debe continuar con los siguientes pasos:</p>

				<textarea name="Texto">Program Participation. Participation in the Program is subject to Google’s prior approval and Your continued compliance with the Program Policies ("Program Policies"), located at https://www.google.com/adsense/policies, and/or such other URL as Google may provide from time to time. Google reserves the right to refuse participation to any applicant or participant at any time in its sole discretion. By enrolling in the Program, You represent that You are at least 18 years of age and agree that Google may serve (a) third party and/or Google provided advertisements and/or other content (such third party provided advertisements, Google provided advertisements and other content, collectively, "Ads"), provided, however, that if Google serves non-compensated content, You will have the ability to opt out of receiving such content as part of the Program, (b) related Google queries and/or Ad search box(es) (collectively, “Links”), (c) Google Web and/or Site search results (collectively, "Search Results"), and/or (d) Google referral Ads (“Referral Buttons”), each in connection with the Web site(s), media player(s), video content and/or mobile content that You designate, or such other properties expressly authorized in writing by Google (including by electronic mail) (such other properties, “Other Properties”), and the Atom, RSS, or other feeds distributed through such Web site(s) , media player(s), video content, mobile content and/or Other Properties (each such Web site, media player, video content, mobile content, Other Property or feed, a "Property"). For the avoidance of doubt, any reference in this Agreement or the Program Policies to an individual “Web page”, “Web site”, “Web site page” or the like that is part of the Property will also mean feeds and media players distributed through such Web site. Multiple accounts held by the same individual or entity are subject to immediate termination
		Program Participation. Participation in the Program is subject to Google’s prior approval and Your continued compliance with the Program Policies ("Program Policies"), located at https://www.google.com/adsense/policies, and/or such other URL as Google may provide from time to time. Google reserves the right to refuse participation to any applicant or participant at any time in its sole discretion. By enrolling in the Program, You represent that You are at least 18 years of age and agree that Google may serve (a) third party and/or Google provided advertisements and/or other content (such third party provided advertisements, Google provided advertisements and other content, collectively, "Ads"), provided, however, that if Google serves non-compensated content, You will have the ability to opt out of receiving such content as part of the Program, (b) related Google queries and/or Ad search box(es) (collectively, “Links”), (c) Google Web and/or Site search results (collectively, "Search Results"), and/or (d) Google referral Ads (“Referral Buttons”), each in connection with the Web site(s), media player(s), video content and/or mobile content that You designate, or such other properties expressly authorized in writing by Google (including by electronic mail) (such other properties, “Other Properties”), and the Atom, RSS, or other feeds distributed through such Web site(s) , media player(s), video content, mobile content and/or Other Properties (each such Web site, media player, video content, mobile content, Other Property or feed, a "Property"). For the avoidance of doubt, any reference in this Agreement or the Program Policies to an individual “Web page”, “Web site”, “Web site page” or the like that is part of the Property will also mean feeds and media players distributed through such Web site. Multiple accounts held by the same individual or entity are subject to immediate termination
				</textarea>
		</div>
		<!--div class="conte-check">
		<input type="checkbox" /><label>He leido y acepto los t&eacute;rminos y condiciones del servicio</label>
		<input class="continuar btn-celeste" type="submit" value="Continuar" />
		</div-->
		</form>
	</div>
</div>
*/ 
echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection');
?>
<style>
  .botonIzq .submit {
    float: left;
  }
  #bt_fb {
    margin-left: 10px;
  }
   
 *.unselectable {
   -khtml-user-drag: none;
   -o-user-select:none;
   -moz-user-select:none;
   -khtml-user-select:none;
   -webkit-user-select:none !important;
   -ms-user-select:none; 
 }
 
 .editUserEmailText {
   alignment-adjust:baseline;
   text-align: right;
   padding-right: 69px;
 }
</style>
<script> 
$(document).ready(function(){
  
   $('input.disablecopypaste').bind('copy paste', function (e) {
       e.preventDefault();
    });
});
</script>
<div class="container_16 main_frt" id="main">
	<div class="grid_12" id="reg-form">
		<div class="bloque-sesion">
			<div class="boxRegistro">
				<div class="contentRegistro">
					<div class="boxIngresa">

            <h1>&iexcl;Registrate!</h1>
			<p class="recib-ofertas">Y recib&iacute; las mejores ofertas de Club Cup&oacute;n!</p>
			<form action="" method="post">

            <!-- Contenidos -->

                <?php


                    //Considerando la cantidad de excepciones, pasamos todo a php, sin cortes a los bloques...

                    $action = (!empty($type) ? 'company_register' : 'register');

                    $formClass = !empty($this->data['User']['is_requested']) ? 'js-ajax-login' : '';


                        //Empieza el bloque de la izquierda, formulario de registro
                        echo $form->create('User', array('action' => 'register', 'class' => 'normal js-register-form '.$formClass));
						echo '<ul>';
                        if (!empty($type)){  echo '<legend>Cuenta</legend>';}

                        if (!empty($this->data['User']['openid_url'])){
                            echo $form->input('openid_url',         array('type' => 'hidden', 'value' => $this->data['User']['openid_url']));
                        }
                        //echo '<div class="grid_5">';
                        echo '<li>';
						$nicknamefield = $form->input('username',               array('info'  => '', 'label' => 'Nickname'));
						$tip = '<img alt="" src="/img/theme_clean/tip.png" class="tipimg"> <div class="tip" style="display: none;">Debe comenzar con una letra. Debe ser como m&iacute;nimo de 3 caracteres y maximo de 20 caracteres. No utilizar caracteres especiales ni espacios<span class="spr-bubble"></span></div>';
						echo preg_replace("/\<\/div\>$/","$tip</div>",$nicknamefield);
                        echo '</li><li>';
						echo $form->input('email',                  array('label' => 'Email'));
                        //echo '<div class="editUserEmailText" ><a id="editUserEmail"  align="right"> Editar </a></div>';
                        echo '</li><li>';
            echo $form->input('confirm_email',                  array('label' => 'Confirmación de Email', 'class'=>'disablecopypaste'));
                        echo '</li><li>';
						echo $form->input('UserProfile.dni',        array('label' => 'DNI'));
                        echo $form->input('referred_by_user_id',    array('type'  => 'hidden',));
						echo '</li>';
                        //echo '</div>';

                        if (empty($this->data['User']['openid_url']) && empty($this->data['User']['fb_user_id'])){
                            echo '<li>';
							echo $form->input('passwd',             array('label' => 'Contraseña'));
							echo '</li><li>';
                            echo $form->input('confirm_password',   array('type' => 'password', 'label' => 'Confirmar contraseña'));
							echo $form->input('type',               array('type' => 'hidden', 'value' => $type));
							echo '</li><li>';
                        }

                        if (!empty($type)){
                            echo $form->input('Company.name',       array('label' => 'Nombre de empresa'));
                            echo $form->input('Company.phone',      array('label' => 'Teléfono'));
                            echo $form->input('Company.url',        array('label' => 'URL', 'help' => 'por ejemplo. http://www.example.com'));
                        }

                        if (!empty($this->data['User']['is_requested'])){
                            echo $form->input('is_requested',       array('type' => 'hidden'));
                        }

                        if (!empty($this->data['User']['f'])){
                            echo $form->input('f',                  array('type' => 'hidden'));
                        }

                        if (!empty($type)){ echo'<legend>Direcci&oacute;n</legend>';  }

                        if (!empty($type)) {
                            echo $form->input('Company.address1',   array('label' => 'Dirección1'));
                            echo $form->input('Company.address2',   array('label' => 'Dirección1'));
                            echo $form->input('Company.country_id', array('empty' => 'Seleccioná', 'label' => 'Pais'));
                            echo $form->autocomplete('State.name',  array('label' => 'Provincia', 'acFieldKey' => 'State.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
                            echo $form->autocomplete('City.name',   array('label' => 'Ciudad',    'acFieldKey' => 'City.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
                            echo $form->input('Company.zip',        array('label' => 'Código postal'));
                        } else {
                            echo $form->input('country_iso_code',   array('type' => 'hidden', 'id' => 'country_iso_code'));
                            echo $form->input('State.name',         array('type' => 'hidden'));
                            echo $form->input('City.name',          array('type' => 'hidden'));
                        }

                        if (!empty($refer)) {
                            if (isset($_GET['refer']) && ($_GET['refer'] != '')) { $refer = $_GET['refer']; }
                            echo $form->input('referer_name',       array('value' => $refer, 'label' => 'Código de Referencia'));
                        } else {
                            echo $form->input('referer_name',       array('type' => 'hidden'));
                        }

                        if (!empty($type)){

                            echo '<legend>Buscate en Google Maps</legend>';
                            echo $form->input('Company.latitude',   array('type' => 'hidden', 'id' => 'latitude'));
                            echo $form->input('Company.longitude',  array('type' => 'hidden', 'id' => 'longitude'));
                            echo '<div class="show-map" style="">';
                            echo '<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=' .  Configure::read('GoogleMap.api_key') . '"   type="text/javascript"></script>';
                            echo '<div id="js-map"></div>';
                            echo '</div>';
                        }

                        if (empty($this->data['User']['openid_url'])){
                            if (empty($this->data['User']['fb_user_id']) && empty($type) && Configure::read('user.is_enable_openid')) {
                                echo '<span> o </span>';
                                echo $form->input('openid',         array('id' => 'openid_identifier', 'class' => 'bg-openid-input', 'label' => __l('OpenID')));
                            }

                            if (!empty($this->data['User']['fb_user_id'])){
                                echo $form->input('fb_user_id',     array('type' => 'hidden', 'value' => $this->data['User']['fb_user_id']));
                            }
							echo '<li>';
                            echo'<div class="captcha-block js-captcha-container">';
                            echo $html->image(Router::url(           array('controller' => 'users', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('title' => 'CAPTCHA', 'class' => 'floatLeft captcha-img'));
                            echo '<div class="captcha-right">';
                            echo $html->link($html->image('captcha_reload.png'), '#', array('class' => 'js-captcha-reload captcha-reload', 'title' => 'Actualizar CAPTCHA', 'escape' => false));
                            echo '<br />';
                            echo $html->link($html->image('captcha_audio.png'), array('controller' => 'users', 'action' => 'captcha_play'), array('title' => 'Version Audio', 'rel' => 'nofollow', 'escape' => false));
                            echo '</div>';
                            echo '</div>';
							echo '</li><li>';
                            echo $form->input('captcha', array('label' => 'Código de Seguridad', 'class' => 'js-captcha-input'));
							echo '</li>';
                            echo '<div class="terms">';
                            echo $form->input('is_agree_terms_conditions',array('type' => 'checkbox', 'checked' => 'checked', 'label' => sprintf('He le&iacute;do y acepto los %s del servicio', $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false),array( 'target' => '_blank', 'escape' => false))),'class'=>'chk'));
                            echo $form->input('is_agree_daily_subscriptions',array('type' => 'checkbox', 'checked' => 'checked', 'label' => 'S&iacute;, quiero recibir emails con las ofertas diarias de Club Cup&oacute;n','class'=>'chk'));
                            if(Configure::read('now.is_enabled')){
								//echo $form->input('wants_to_be_a_company',array('type' => 'checkbox', 'checked' => '', 'label' => 'Registrarme como Empresa (<a href="#re-ccya-pop-up" class="ver-tyc">Ver Terminos y Condiciones</a>)','class'=>'chk'));
								echo $form->input('wants_to_be_a_company',array('type' => 'hidden', 'value' => '0'));
							}
                            echo '</div>';
                            echo'<div class="clear"></div>'; //asi evitamos que los floats del 960 nos volteen el boton de submit
                        }
						echo '<li class="botonIzq">';
						echo $form->submit('Registrarse', array('id' => 'bt_enviar','class'=>'buttonRegistro','style'=>'height:auto;width:auto;'));
						echo '<div class="grid_5 alpha omega cuadro-fb fmt-cajas" id ="bt_fb">';
            echo ' <p>&iquest;Ya ten&eacute;s una cuenta en Facebook?</p><p>Usala para registrarte en Club Cup&oacute;n</p>'; 
            echo $html->link($html->image('/img/theme_clean/fb-btn.jpg'), '#', array('rel'=> 'nofollow','title' => 'fconnect', 'id' => 'facebookLoginButton'), null, false);
            echo '</div>';
            echo '</li>';
					echo '</ul>';
                    echo $form->end();
                ?>

                <!-- / Contenidos -->
            </div>
					</div>
				</div>
			</div>
		</div>
	<div class="grid_4">
		<div class="bloque-derecha">
			<div class="sobreClubCupon">
				<div class="contentBox">
					<h3>Sobre Club Cup&oacute;n</h3>
                    <p>
                        En <strong>CLUB CUP&Oacute;N</strong> te ofrecemos
                        un descuento nuevo todos los
                        d&iacute;as para que puedas disfrutar
                        de las mejores propuestas a
                        precios incre&iacute;bles.
                    </p>
                    <p>
                        Si ten&eacute;s ganas de salir a comer,
                        tener un d&iacute;a de relax o de
                        shopping est&aacute;s en el sitio
                        indicado.
                    </p>
                    <span>Ofertas Incre&iacute;bles</span>
                    <p>
                        Cada d&iacute;a te ofreceremos una
                        oferta a un precio incre&iacute;ble en
                        entretenimiento, restaurantes,
                        indumentaria, belleza, deportes
                        y mucho m&aacute;s &iexcl;Aprovech&aacute; ahora
                        mismo los descuentos que
                        estabas esperando haciendo
                        click en <strong>"&iexcl;Comprar!"</strong>
                    </p>
                    <span>Compr&aacute;</span>
                    <p>
                        Hac&eacute; click en el bot&oacute;n
                        "&iexcl;Comprar!" y complet&aacute; los datos
                        del medio de pago que vas a
                        usar. Una vez que el pago este
                        confirmado, te enviaremos el
                        cup&oacute;n a tu mail
                        para que puedas disfrutar del
                        beneficio.
                    </p>
                    <span>Disfrut&aacute;</span>
                    <p>
                        S&oacute;lo resta que imprimas el
                        cup&oacute;n que te enviamos por
                        mail, lo lleves
                        al comercio correspondiente
                        y disfrutes del beneficio.&iexcl;En
                        tu cup&oacute;n encontrar&aacute;s toda la
                        informaci&oacute;n necesaria para
                        que puedas hacerlo!
                    </p>
				</div>
			</div>
		</div>
    </div>
</div>
<script type = "text/javascript">

  $(document).ready (function () {
	$('.ver-tyc').colorbox({inline:true});

      function verifyCreditForSubscription(email) {
          url = '../../../subscriptions/find_total_available_balance_by_email/';
          $.post(url, {email : email},
              function(data){
                  console.log(data);
                  if(data.success && data.amount > 0) {
                      showAvailableCreditBySubscription();
                  } else {
                      hideAvailableCreditBySubscription();
                  }
              },
                  "json");

          return false;
      }

      function showAvailableCreditBySubscription() {
          // TODO
      }

      function hideAvailableCreditBySubscription() {
          // TODO
      }

      $('#UserEmail').change(function() {
         // verifyCreditForSubscription($(this).val());
      });

  });



</script>