<div class="users stats">
  <h2>Estad&iacute;sticas del Sitio</h2>
  <table class="list" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <th rowspan="2"><?php echo __l('Period'); ?></th>
      <?php
      foreach ($models as $unique_model) {
        foreach ($unique_model as $model => $fields) {
          if (!isset($fields['isSub'])) {
            $element = isset($fields['colspan']) ? 'colspan ="' . $fields['colspan'] . '"' : 'rowspan="2"';

            
            if ($model == 'Transaction') {
              echo '<th ' . $element . '>' . $fields['display'] . ' (' . Configure::read('site.currency') . ')' . '</th>';
            } else {
              echo '<th ' . $element . '>' . $fields['display'] . '</th>';
            }
          }
        }
      }
      ?>
    </tr>
    <tr>
      <?php
      foreach ($models as $unique_model) {
        foreach ($unique_model as $model => $fields) {
          if (isset($fields['isSub'])) {
            echo '<th>' . $fields['display'] . '</th>';
          }
        }
      }
      ?>
    </tr>
    <?php
      foreach ($periods as $key => $period) {
        echo '<tr>';
        echo '<td>' . $period['display'] . '</td>';

        foreach ($models as $unique_model) {
          foreach ($unique_model as $model => $fields) {
            $aliasName = isset($fields['alias']) ? $fields['alias'] : $model;
            if (!empty($fields['link'])):
              $fields['link']['stat'] = $key;
              if (!isset($fields['colspan'])) :
                echo '<td>';

                if ($model == 'Transaction') {
                  echo $html->link($html->cCurrency(${$aliasName . $key}), $fields['link'], array('escape' => false, 'title' => __l('Click to View Details')));
                } else if ($model == 'Deal' && $fields['alias'] == 'DealCommssionAmount') {
                  echo $html->cCurrency(${$aliasName . $key});
                } else {
                  echo $html->link($html->cInt(${$aliasName . $key}), $fields['link'], array('escape' => false, 'title' => __l('Click to View Details')));
                }
                echo '</td>';
              endif;
            elseif (empty($fields['link'])):
    ?>
              <td><?php echo $html->cCurrency(${$aliasName . $key}); ?></td>
<?php else: ?>
                <td><?php echo $html->link(${$model . $key}, array('controller' => Inflector::tableize($model), 'action' => 'index', 'stat' => $key), array('title' => __l('Click to view details'))); ?></td>
    <?php
                endif;
              }
            }
            echo '</tr>';
          }
    ?>
        </table>
        <div class="clearfix record-block width100 floatLeft">
          <h3><?php echo __l('Recently registered users'); ?></h3>
          <div class="width100 floatLeft">
          <?php
              if (!empty($recentUsers)) {
                $users = '';
                foreach ($recentUsers as $user) {
                  $users .= sprintf('%s, ', $html->getUserLink($user['User']));
                }
                echo $users;
              } else {
                echo '<p class="notice">' . __l('Recently no users registered') . '</p>';
              }
          ?>
        </div>
      </div>

      <div class="clearfix record-block width100 floatLeft">
        <h3><?php echo __l('Online users'); ?></h3>
        <div class="width100 floatLeft">
        <?php
            if (!empty($onlineUsers)) {
              $users = '';
              foreach ($onlineUsers as $user) {
                $users .= sprintf('%s, ', $html->getUserLink($user['User']));
              }
              echo $users;
            } else {
              echo '<p class="notice">' . __l('Recently no users online') . '</p>';
            }
        ?>
        </div>
      </div>

      <div class="clearfix record-block width100 floatLeft">
        <h3><?php echo __l('Memory Status'); ?></h3>
        <dl class="list">
          <dt class="altrow"><?php echo __l('Used Cache Memory'); ?></dt>
          <dd class="altrow"><?php echo $tmpCacheFileSize; ?></dd>
          <dt><?php echo __l('Used Log Memory'); ?></dt>
          <dd><?php echo $tmpLogsFileSize; ?></dd>
        </dl>
      </div>
</div>