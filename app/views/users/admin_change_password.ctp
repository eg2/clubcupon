
<div>
	<div class="contenido-form clearfix" style="position:relative;">
		<div class="encabezado">
		    <h2 class="h2mod"><?php echo $this->data['User']['username2'];?></h2>
            <h2 class="h2mod">Cambio de contrase&ntilde;a</h2>
           
            <!--p class="subt-passw">Complet&aacute; todos los campos para proceder</p-->
        </div>
		<div id="form_adm_change_pass" class="form-fmt clearfix">
            <div class="grid_7 prefix_3" style="padding-bottom:20px;float:left">
                <?php
                    echo $form->create('User', array('action' => 'admin_change_password', 'class' => ''));
                    
                    echo $form->input('user_id', array('type' => 'hidden'));
                    echo $form->input('username2', array('type' => 'hidden'));
                
                    echo $form->input('passwd', array('type' => 'password', 'label' => 'Introducir una nueva contraseña', 'id' => 'new-password'));
                    echo $form->input('confirm_password', array('type' => 'password', 'label' => 'Confirmar Contraseña'));
                   
                    echo '<div style="padding-left:20px">';
                    echo $form->submit('Cambiar Contraseña', array('id' => 'btCambiarContrasenia', 'class' => 'btn-azul-d2 mt302'));
                    echo '</div>';
                    echo $form->end();
                ?>
            </div>
        </div>
     </div>
 </div>