<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix">
            
            <h1 class="h1-format">&iquest;Olvidaste tu contrase&ntilde;a?</h1>


            <!-- Contenidos -->
            
            <p>
                Ingres&aacute; tu email y te enviaremos instrucciones para restablecer tu contrase&ntilde;a.<br /><br />
            </p>

              <?php
                echo $form->create('User', array('action' => 'forgot_password', 'class' => 'normal'));
                echo $form->input('email', array('type'   => 'text','label' => 'Mail'));
                echo $form->submit('Recuperar contraseña',    array('id'     => 'bt_enviar', 'class'=>'btn-verde'));
                echo $form->end();
              ?>

            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>
