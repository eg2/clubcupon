<div class="users form">
	<h2> <?php echo __l('Editar permisos sobre ciudades');?></h2>
<?php echo $form->create('User', array('class' => 'normal','action'=>'edit'));?>
<?php echo $form->input('id'); ?>
	<fieldset>
 	
	<?php
    echo $form->input('user_type_id',array('label' => __l('User Type')));
		echo $form->input('email',array('label' => __l('Email'),'readonly'=>true));
		echo $form->input('username',array('label' => __l('Username'),'readonly'=>true));
	?>
	</fieldset>

  <fieldset>
    <?php $i=0; ?>
    <?php foreach($cities as $cityId => $cityName):  ?>
        <?php $checked = in_array($cityId,$citiesSelected); ?>
        <?php echo $form->input('CityPerms.'.$i++.'.city_id_selected',array('value' => $cityId,'type'=>'checkbox','label'=>$cityName, 'checked'=> $checked)); ?>
    <?php endforeach;  ?>
  </fieldset>


<?php echo $form->end(__l('Save'));?>
</div>