<?php

    if ($auth->user('user_type_id') == ConstUserTypes::Company)
    {
        echo '<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=' . Configure::read('GoogleMap.api_key') . '" type="text/javascript"></script>';
        echo $this->element('js_tiny_mce_setting', array('cache' => Configure::read('tiny_mce_cache_time.listing_element_cache_duration')));
    }

?>

<div id="main" class="container_16 box">

    <div id="my-account" class="grid_16  bloque-home clearfix">




        <div class="title-h2 grid_4 alpha" style="height:20px">MI CUENTA</div>
        <div class="grid_12 omega">
                <ul id="my_stuff_tabs">

                    <?php
                        if($auth->user('user_type_id') == ConstUserTypes::Company)
                        {
                            $user = $html->getCompany($auth->user('id'));
                            echo '<li class="my_stuff_li">' . $html->link('Mis Datos',              array('controller' => 'companies',     'action' => 'edit',$user['Company']['id']),  array('title' => 'Mis Datos' )) . '</li>';
                        }
                        else
                        {
                            echo '<li  class="my_stuff_li">' . $html->link('Mis Datos',              array('controller' => 'user_profiles', 'action' => 'my_account',$auth->user('id')), array('title' => 'Mis Datos')) . '</li>' ;
                        }

                        if($auth->sessionValid() && $html->isAllowed($auth->user('user_type_id')))
                        {
                            echo '<li  class="my_stuff_li">' . $html->link('Mis Cupones',            array('controller' => 'deal_users',    'action' => 'index'), array('title' => 'Mis Cupones')) . '</li>';
                            echo '<li  class="my_stuff_li">' . $html->link('Mis Cupones de Regalo',  array('controller' => 'deal_users',    'action' => 'gifted_deals','admin' => false), array('title' => 'Mis Cupones de Regalo')) . '</li>';

                            if(Configure::read('friend.is_enabled'))
                            {
                                echo '<li  class="my_stuff_li">' . $html->link('Mis amigos',         array('controller' => 'user_friends',  'action' => 'lst',   'admin' => false), array('title' => 'Mis amigos')) . '</li>';
                                echo '<li  class="my_stuff_li">' . $html->link('Importar amigos',    array('controller' => 'user_friends',  'action' => 'import','admin' => false), array('title' => 'Importar amigos')) . '</li>';
                            }

                        }
                    ?>

                </ul>

            </div>



    </div>

</div>
