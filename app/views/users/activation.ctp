<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h1>Activ&aacute; tu cuenta</h1>
            
            <!-- Contenidos -->

            <?php
                if(!empty($show_resend)):
                    echo sprintf(__l('A&uacute;n no ha activaste tu cuenta. Por favor, activala. Si no recibiste el correo de activaci&oacute;n, %s para volver a enviarlo.'), $html->link('click here', $resend_url));
                endif;
            ?>

            <!-- / Contenidos -->

        </div>
    </div>
</div>