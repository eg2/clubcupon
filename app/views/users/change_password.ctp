<?php

if($is_now_company || $this->params['admin']){
?>
<style>
    .required label{
        width:180px;
        display:block;
        color:#333333;
    }
    
    .error-message{width:195px;
        display:block;}
    
    input{
        float:right; 
      
    }
    .required{
        height:30px;
    }
    #UserChangePasswordForm{
        width:360px!important;
    }
    .chgPass input{
      height:20px;
      float:left; 
    }
    .chgPass label{
      float:left; 
    }
    .submit input{
      height:35px;
    }
    .chgPass .submit{
      float:right;
    }
    .chgPass .input{
      width:370px;
    }

</style>
<?php
    echo $this->element('dashboard/company_dashboard_menu_tabs');
?>
<div class="grid_16 paso3addofertas adds-suc" style="font: 12px arial;">
	<div class="contenido-form clearfix" style="position:relative;">
		<div id="form-overlay" style="display:none; position: absolute; width: 100%; height: 100%; background: none repeat scroll 0% 0% black; opacity: 0.21; left: 0px; top: 0px; border-radius: 5px 5px 5px 5px;"></div>
		
		
		
		
        <div class="encabezado">
            <h2 class="h2mod">Cambio de contrase&ntilde;a</h2>
            <p class="subt-passw">Complet&aacute; todos los campos para proceder</p>
        </div>
		<div class="form-fmt clearfix">
            <div class="grid_7 prefix_3" style="padding-left: 10px;">
                <?php

                    echo $form->create('User', array('action' => 'change_password', 'class' => 'chgPass'));

                    echo $form->input('user_id', array('type' => 'hidden', 'readonly' => 'readonly'));
                    echo $form->input('old_password', array('type' => 'password', 'label' => 'Contraseña anterior', 'id' => 'old-password'));
                    echo '<div style="clear:both; margin-bottom:15px;">&nbsp;</div>';
                    echo $form->input('passwd', array('type' => 'password', 'label' => 'Introducir una nueva contraseña', 'id' => 'new-password'));
                    echo '<div style="clear:both; margin-bottom:15px;">&nbsp;</div>';
                    echo $form->input('confirm_password', array('type' => 'password', 'label' => 'Confirmar Contraseña'));
                    
                    echo $form->submit('Cambiar Contraseña', array('id' => 'btCambiarContrasenia', 'class' => 'btn-azul-d mt30'));

                    echo $form->end();

                ?>

            </div>
        </div>
        
        <?php if(isset($numero_comercio)){?>
	    <!-- Numero de Comercio-->
	    	<div class="encabezado">
     			<h2 class="h2mod" style="border-bottom: 1px solid #DDDDDD;">N&uacute;mero de Comercio</h2>
     		</div>
     		<div class="form-fmt clearfix">
     			<div class="grid_7 prefix_3">
     			<div class="input"><br/>
     			   <label for="old-password"></label>
     			   <b style="font: bold 16px arial;"><?php echo $login['Company']['id']?></b>
     			</div>
     			</div>
            </div>
     	<?php }?>
    </div>
</div>


<?php }else{ ?>

<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix changePassword">

            <?php

                echo $this->element(Configure::read('theme.theme_name') . 'my_stuff');


            ?>


            <div class="grid_7">
            <!-- Contenidos -->
                <h1>Cambio de contrase&ntilde;a</h1>
                <p>Complete todos los campos para proceder<br /><br /></p>

            <?php


                echo $form->create('User', array('action' => 'change_password', 'class' => ''));
                echo $form->input('user_id', array('type' => 'hidden', 'readonly' => 'readonly'));
                echo $form->input('old_password', array('type' => 'password', 'label' => 'Contraseña anterior', 'id' => 'old-password'));
                

                echo $form->input ('passwd', array('type' => 'password', 'label' => 'Introducir una nueva contraseña', 'id' => 'new-password'));
                echo $form->input ('confirm_password', array('type' => 'password', 'label' => 'Confirmar Contraseña'));
                echo $form->submit('Cambiar Contraseña', array('id' => 'btCambiarContrasenia', 'class' => 'btn-azul-d mt30'));

                echo $form->end();

            ?>
			</div>
				<?php if(Configure::read('now.is_enabled')){?>
					<?php if(!isset($login['Company']['id'])){ ?>
					<div class="grid_8" id="ccnow" style="display:none">
						<div class="bloque-ccya clearfix">
							<img src="/img/now/logoYApop.png">
							<p class="title">Bienvenido a <span>Club Cup&oacute;n YA!</span></p>
							<?php if(!isset($login['CompanyCandidate']['id'])){ ?>
							<p>Completa tus datos para ser parte de Club Cup&oacute;n YA.</p>
							<input type="button" value="Completar mis Datos" class="btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_registers','action'=>'my_company'));?>'">
							<?php }else{ ?>
							<p>Tus datos estan pendientes de aprobaci&oacute;n.
							Revis&aacute; tus datos. Una vez aprobados no podras editarlos.</p>
							<input type="button" value="Modificar mis Datos" class="btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_registers','action'=>'my_company'));?>'">
							<?php }?>
						</div>
					</div>
					<?php }elseif(!$login['Company']['now_eula_ok']){ ?>
					<div class="grid_8"id="ccnow" style="display:none">
						<div class="bloque-ccya clearfix">
							<img src="/img/now/logoYApop.png">
							<p class="title">Bienvenido a <span>Club Cup&oacute;n YA!</span></p>
							<p>Para utilizar este nuevo servicio deb&eacute;s aceptar los <a href="#re-ccya-pop-up" class="eula_check ver-tyc">T&eacute;rminos y Condiciones del servicio</a></p>
						<?php echo $form->create('Company',array('url'=>array('plugin'=>'now','controller' => 'now_registers', 'action' => 'eula'))); ?>
						<?php echo $form->input('now_eula_ok',array('type'=>'checkbox','label'=>'Acepto los T&eacute;rminos y Condiciones','div'=>'eula_check')); ?>
						<div style="clear:both;"></div>
						<?php echo $form->submit('Continuar', array('class'=>'btn-verde')); ?>
						<?php echo $form->end(); ?>
						</div>
					</div>
					<?php }else{ ?>
					<div class="grid_8" id="ccnow" style="display:none">
						<div class="bloque-ccya clearfix">
						<img src="/img/now/logoYApop.png">
						<p class="title">Bienvenido a <span>Club Cup&oacute;n Ya!</span></p>
						<p>Para comenzar a utilizar este servicio debes cargar la informaci&oacute;n de la sucursal</p>
						<h2>Sucursales:</h2>
						<input type="button" value="Agregar Sucursal" class="deals_btns btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_branches','action'=>'add'));?>'">
						<?php if(count($company[0]['NowBranch'])){ ?>
						<input type="button" value="Editar Sucursales" class="deals_btns btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_branches'));?>'">
						<?php }?>
						<div style="clear:both;"></div>
							<?php if(count($company[0]['NowBranch'])){ ?>
							<p>Y luego las ofertas</p>
							<h2>Ofertas:</h2>
							<input type="button" value="Agregar Oferta" class="deals_btns btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_deals','action'=>'add'));?>'">
							<?php if(count($company[0]['NowDeal'])){ ?>
							<input type="button" value="Ver Ofertas Ya" class="deals_btns btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'now_deals','action'=>'my_deals'));?>'">
							<?php } ?>
							<?php if(count($company[0]['Deal'])){ ?>
							<input type="button" value="Ver Ofertas" class="deals_btns btn-celeste formato-btn-mod" onclick="window.location = '<?php echo Router::url(array('plugin'=>'','controller'=>'deals','action'=>'company',$company[0]['Company']['slug']));?>'">
							<?php } ?>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
				<?php } ?>
			<!-- / Contenidos -->
        </div>
    </div>
</div>

<script type = "text/javascript">
	$(document).ready (function () {
		$('.ver-tyc').colorbox({
			inline:true,
			onLoad:function(){
				$('div#cboxClose').html('Continuar');
			}
		});
	});
</script>
</div>
<?php } ?>