<div id="main" class="container_16 main_frt">
	<div id="reg-ok" class="grid_12">
		<div class="bloque-sesion">
		<?php if(@$now){?>
		<div class="contenedor-popup-recib">
			<div class="bloque-ccya-popup1">
				<!--a href="">Cerrar <span>X</span></a-->
				<img src="/img/now/logoYApop.png">
				<p class="title-pop">Recibimos tus datos correctamente</p>
				<p>Un mail de confirmacion fu&eacute; enviado a tu casilla de correo.</p>
			</div>
		</div>
		<?php }else{?>
		<div class="boxRegistro">
			<div class="contentRegistro">
				<div class="boxIngresa">
					<p class="reg-exito">&iexcl;Registro realizado con Exito!</p>
                    <a class="buttonRegistro comprar" onclick="window.location='<?php echo Router::url(array('controller'=>'deals', 'city'=>'ciudad-de-buenos-aires'));?>'">Mir&aacute; las ofertas de hoy</a>
				</div>
			</div>
		</div>
		<?php }?>
		</div>
    </div>
	<div class="grid_4">
		<div class="bloque-derecha">
			<div class="sobreClubCupon">
				<div class="contentBox">
					<h3>Sobre Club Cup&oacute;n</h3>
					<p>En <strong>CLUB CUP&Oacute;N</strong> te ofrecemos
					un descuento nuevo todos los
					d&iacute;as para que puedas disfrutar
					de las mejores propuestas a
					precios incre&iacute;bles.</p>
					<p>Si ten&eacute;s ganas de salir a comer,
					tener un d&iacute;a de relax o de
					shopping est&aacute;s en el sitio
					indicado.</p>
					<span>Ofertas Incre&iacute;bles</span>
					<p>Cada d&iacute;a te ofreceremos una
					oferta a un precio incre&iacute;ble en
					entretenimiento, restaurantes,
					indumentaria, belleza, deportes
					y mucho m&aacute;s &iexcl;Aprovech&aacute; ahora
					mismo los descuentos que
					estabas esperando haciendo
					click en <strong>"&iexcl;Comprar!"</strong></p>
					<span>Compr&aacute;</span>
					<p>Hac&eacute; click en el bot&oacute;n
					"Comprar!" y complet&aacute; los datos
					del medio de pago que vas a
					usar. Una vez que el pago este
					confirmado, te enviaremos el
					cup&oacute;n a tu mail
					para que puedas disfrutar del
					beneficio.</p>
					<span>Disfrut&aacute;</span>
					<p>S&oacute;lo resta que imprimas el
					cupón que te enviamos por
					mail, lo lleves
					al comercio correspondiente
					y disfrutes del beneficio.¡En
					tu cup&oacute;n encontrar&aacute;s toda la
					informaci&oacute;n necesaria para
					que puedas hacerlo!</p>
				</div>
			</div>
		</div>
    </div>
</div>