<div id="main" class="container_16 main_frt">
<div class="grid_12">
            <div class="bloque-sesion">            	
                <div class="LogOut">
                	<p>Has Finalizado tu sesi&oacute;n</p>
                    <span>
						<?php echo $html->link('Ver ofertas del d&iacute;a', array('controller' => 'deals', 'action' => 'index', 'city' => $city), array('escape'=>false)); ?>
						|
						<?php echo $html->link('Volver a ingresar', array('controller' => 'users', 'action' => 'login')); ?>
					</span>
                	
					<?php echo($this->requestAction(array('controller' => 'banners', 'action' => 'logout'),array('return')));?>
					
					<div class="followLog">
                    <p>Encontranos en la Web</p>
                    <ul>
                    	<li style="width:125px"><p>Hacete fan</p><a href="http://www.facebook.com/clubcuponcba" class="faceLogOut"><span>facebook</span></a></li>
                        <li style="width:115px"><p>Seguinos</p><a href="http://twitter.com/ClubCuponCBA" class="twitterLogOut"><span>Twitter</span></a></li>
                    </ul>
                    </div>
                </div>                
            </div>
        </div>
        <div class="grid_4">
        	<div class="bloque-derecha">
            	<div class="sobreClubCupon">
                	<div class="contentBox">
                    	<h3>Sobre Club Cup&oacute;n</h3>
                        <p>En <strong>CLUB CUP&Oacute;N</strong> te ofrecemos
                        un descuento nuevo todos los 
                        d&iacute;as para que puedas disfrutar 
                        de las mejores propuestas a 
                        precios incre&iacute;bles.</p>
                        <p>Si ten&eacute;s ganas de salir a comer, 
                        tener un d&iacute;a de relax o de 
                        shopping est&aacute;s en el sitio 
                        indicado.</p>
                        <span>Ofertas Incre&iacute;bles</span>
                        <p>Cada d&iacute;a te ofreceremos una 
                        oferta a un precio incre&iacute;ble en 
                        entretenimiento, restaurantes, 
                        indumentaria, belleza, deportes 
                        y mucho m&aacute;s &iexcl;Aprovech&aacute; ahora 
                        mismo los descuentos que 
                        estabas esperando haciendo 
                        click en <strong>"&iexcl;Comprar!"</strong></p>
                        <span>Compr&aacute;</span>
                        <p>Hac&eacute; click en el bot&oacute;n 
                        "Comprar!" y complet&aacute; los datos 
                        del medio de pago que vas a 
                        usar. Una vez que el pago este 
                        confirmado, te enviaremos el 
                        cup&oacute;n a tu correo&oacute;electrónico 
                        para que puedas disfrutar del 
                        beneficio.</p>
                        <span>Disfrut&aacute;</span>
                        <p>S&oacute;lo resta que imprimas el 
                        cup&oacute;n que te enviamos por 
                        mail, lo lleves 
                        al comercio correspondiente 
                        y disfrutes del beneficio.&iexcl;En 
                        tu cup&oacute;n encontrar&aacute;s toda la
                        informaci&oacute;n necesaria para 
                        que puedas hacerlo!</p>
                    </div>
                </div>
            </div>
        </div>  






<?php /*
    <div class="grid_16">
        <div class="bloque-home clearfix">
            
            <!-- Contenidos -->

            <script type="text/javascript">
                var logoutFromFB = function() {
                    document.location.href = '<?php echo $logoutFBUrl ?>';
                };
                $(document).ready(function() {
                    if($.browser.msie || (typeof(FB._session) == "undefined")) {
                        logoutFromFB();
                    } else {
                        FB.logout(function(response) {
                            logoutFromFB();
                        });
                    }
                });
            </script>

            <p>
                Te has desconectado de Club Cup&oacute;n...
                <br />
                <?php echo $html->link('Ver todas las ofertas en productos > ', array('controller' => 'deals', 'action' => 'landingofertas'), array('title' => 'Pod&eacute;s ver todas las ofertas en productos > ', 'escape' => false)); ?>
            </p>

            <!-- / Contenidos -->

        </div>
    </div>
*/ ?>
</div>
