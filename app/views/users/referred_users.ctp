<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            
            <!-- Contenidos -->

            <?php if (!empty($referredFriends)){ ?>
                <ol>

                    <?php foreach ($referredFriends as $referredFriend){ ?>
                        <li>
                            <?php

                                $user_details = array(
                                    'username'     => $referredFriend['User']['username'],
                                    'user_type_id' => $referredFriend['User']['user_type_id'],
                                    'id'           => $referredFriend['User']['id'],
                                    'UserAvatar'   => $referredFriend['UserAvatar']
                                );

                                echo $html->getUserAvatarLink($user_details,'medium_thumb', false); 

                            ?>

                            <p><?php echo $html->getUserLink($referredFriend['User']);?></p>

                            <dl>
                                <dt>Miembro desde</dt>
                                <dd><?php echo $html->cDate($referredFriend['User']['created']);?></dd>
                                <?php if($referredFriend['User']['deal_count']) { ?>
                                    <dt>Total de compra:</dt>
                                    <dd><?php echo $html->cInt($referredFriend['User']['deal_count']);?></dd>
                                <?php } ?>
                            </dl>

                        </li>
                    <?php } ?>
                </ol>
            <?php }else{ ?>
            <p>No hay Usuarios Referidos disponibles</p>
            <?php } ?>

            <!-- / Contenidos -->

        </div>
    </div>
</div>
