<h2><?php echo "Pagos del usuario &ldquo;" . $user ['User']['username'] . "&rdquo;  (" . $user ['User']['id'] . ")" ?></h2>
<div class = "users form">
  <table class = "list">
      <tr>
          <td colspan="14">
              <style>
                  .paging{
                      text-align:center;
                      margin:15px 0;
                  }
              </style>
              <?php echo $this->element('paging_links'); ?>
          </td>
      </tr>
    <tr>
      <th>Id</th>
      <th>Id BAC</th>
      <th>Oferta</th>
      <th>Forma de Pago</th>
      <th>Cantidad</th>
      <th>Monto Interno</th>
      <th>Monto Final</th>
      <th>Monto Parcial</th>
      <th>Estado</th>
      <th>&iquest;Es Regalo?</th>
      <th>&iquest;Es Turismo?</th>
      <th>Acciones</th>
      <th>Creaci&oacute;n</th>
      <th>Modificaci&oacute;n</th>
    </tr>
    <?php foreach ($dealExternals as $ext) { ?>
      <tr>
        <td><?php echo $ext ['DealExternal']['id']; ?></td>
        <td><?php echo ($ext ['DealExternal']['bac_id'] == '') ? '--' : $ext ['DealExternal']['bac_id']; ?></td>
        <td><?php echo $ext ['Deal']['name']; ?></td>
        <td><?php echo $ext ['PaymentType']['name']; ?></td>
        <td><?php echo $ext ['DealExternal']['quantity']; ?></td>
        <td><?php echo $ext ['DealExternal']['internal_amount']; ?></td>
        <td><?php echo $ext ['DealExternal']['final_amount']; ?></td>
        <td><?php echo $ext ['DealExternal']['parcial_amount']; ?></td>
        <td><?php echo $ext ['DealExternal']['external_status']; ?></td>
        <td><?php echo ($ext ['DealExternal']['is_gift'] == '0') ? 'No' : 'Si' ; ?></td>
        <td><?php echo ($ext ['Deal']['is_tourism'] == '0') ? 'No' : 'Si' ; ?></td>
        <td>
          <?php echo $html->link (__l('Acreditar pago manualmente'),
                  array (
                    'controller' => 'externalservices',
                    'action' => 'payments_acredit',
                    'user_id' => $ext ['DealExternal']['user_id'],
                    'deal_external_id' => $ext ['DealExternal']['id']),
                  array (
                    'title' => __l ('Acreditar el pago manualmente'),
                    'class' => 'js-confirm-action-link link-button-generic')); ?>
          <?php echo $html->link (__l('Cancelar pago manualmente'),
                  array (
                    'controller' => 'externalservices',
                    'action' => 'payments_cancel',
                    'user_id' => $ext ['DealExternal']['user_id'],
                    'deal_external_id' => $ext ['DealExternal']['id']),
                  array (
                    'title' => __l ('Cancelar pago manualmente'),
                    'class' => 'js-confirm-action-link link-button-generic')); ?>
        </td>
        <td><?php echo $ext ['DealExternal']['created']; ?></td>
        <td><?php echo $ext ['DealExternal']['updated']; ?></td>
      </tr>
    <?php } ?>
      <tr>
          <td colspan="14">
              <style>
                  .paging{
                      text-align:center;
                      margin:15px 0;
                  }
              </style>
              <?php echo $this->element('paging_links'); ?>
          </td>
      </tr>
  </table>
  <?php if (empty($dealExternals)) { ?>
    <h3>El usuario no tiene pagos asociados.</h3>
  <?php } ?>
</div>