<div class="externalservices form">
	<h2>Agregar monto a la Cuenta Club Cup&oacute;n</h2>
  <?php echo $form->create('Externalservices', array('class' => 'normal','action'=>'admin_add_amount_to_wallet', 'class' => 'js-confirm-add-amount-to-wallet'));?>
  <?php echo $form->input('user_id',array('value' => $user['User']['id'], 'type' => 'hidden')); ?>
	<fieldset>
    <p>Agregar monto a la Cuenta Club Cup&oacute;n del usuario: "<strong><?php echo htmlentities($user['User']['username'],ENT_QUOTES,"UTF-8"); ?></strong>".</p>
    <br />
    <div>
      <p>Monto actual del usuario: <strong><?php echo $user['User']['available_balance_amount']; ?></strong></p>
    </div>
    <div>
      <?php echo $form->input('amount',array('label' => __l('Cantidad a cargar'), 'class' => 'js-price', 'value' => '0.00')); ?>
    </div>
	</fieldset>


	<div class="submit-block clearfix">
    <div class="floatLeft">
        <?php echo $form->submit(__l('Aceptar'));?>
    </div>
		<div class=" floatLeft">
			<?php echo $html->link(__l('Volver a usuarios'), array('controller' => 'users', 'action' => 'index'), array('class' => 'cancel-button', 'title' => __l('Cancel'), 'escape' => false));?>
		</div>
	</div>
	<?php echo $form->end(); ?>

</div>