<div id="content_full" class="box">

    <?php echo $this->element('my_stuff'); ?>

    <div class="userPermissionPreferences form js-responses">
            <div class='js-permission-responses'>
                <?php
                    echo $form->create('UserPermissionPreference', array('class' => 'normal js-ajax-form {"container" : "js-permission-responses"}'));
                    echo $form->input('User.id', array('type' => 'hidden'));
                    echo $form->input('User.username', array('type' => 'hidden'));
                
                    foreach($userPreferenceCategories as $userPreferenceCategory)
                    {
                ?>
                        <fieldset class="form-block">
                            <legend>Configuraci&oacute;n de privacidad</legend>

                            <?php
                                foreach ($this->data['UserPermissionPreference'] as $key => $val)
                                {
                                    $field = explode('-', $key);
                                    if ($field[0] == $userPreferenceCategory['UserPreferenceCategory']['name'])
                                    {
                                        if ($field[1] != 'is_show_captcha')
                                        {
                                            echo $form->input($key, array('type' => 'select', 'label' => Inflector::humanize(str_replace('is_','',$field[1])) , 'options' => $privacyTypes));
                                        }
                                        else
                                        {
                                            echo $form->input($key, array('type' => 'select','label' => Inflector::humanize(str_replace('is_','',$field[1])), 'options' => array('1' => 'Si', '0' => 'No')));
                                        }
                                    }
                                }
                            ?>
                        </fieldset>
                <?php
                    }
                    echo $form->end('Actualizar');
                ?>
            </div>
    </div>

</div>