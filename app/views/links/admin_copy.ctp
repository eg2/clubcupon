<div class="slotsCopy">
    <?php
    $url = '/' . $cSlug . '/admin/links?subAction=copy';
    echo '<h2>Copiar</h2>';
    echo $form->create('Copy', array(
        'url' => $url,
        'onsubmit' => 'return confirm("Â¿EstÃ¡ seguro que desea copiar estos links a la ciudad de destino? \nSe reemplazarÃ¡n los links actuales de dicha ciudad.");'
    ));
    echo '<h3>desde:</h3>';
    echo $form->input('City.slug', array(
        'type' => 'select',
        'label' => 'Ciudad',
        'options' => $citiesOptions,
        'value' => $citySlugValue
    ));
    echo $form->input('Type.id', array(
        'type' => 'select',
        'label' => 'Tipo',
        'options' => array(
            ConstSearchLinksTypes::Destacado => 'Destacados',
            ConstSearchLinksTypes::Especial => 'Especiales',
            ConstSearchLinksTypes::Banner => 'Banner',
        	ConstSearchLinksTypes::Banner_mobile => 'Banners Mobile'
        ),
        'value' => $typeValue
    ));
    echo '<h3>hacia:</h3>';
    echo $form->input('CopyToCity.slug', array(
        'type' => 'select',
        'label' => 'Ciudad',
        'options' => $citiesOptions,
        'value' => Configure::read('web.default.citySlug')
    ));
    echo $form->input('CopyToType.id', array(
        'type' => 'select',
        'label' => 'Tipo',
        'options' => array(
            ConstSearchLinksTypes::Destacado => 'Destacados',
            ConstSearchLinksTypes::Especial => 'Especiales',
            ConstSearchLinksTypes::Banner => 'Banner',
        	ConstSearchLinksTypes::Banner_mobile => 'Banners Mobile'
        ),
        'value' => ConstSearchLinksTypes::Destacado
    ));
    echo $form->end('Copiar');
    ?>
</div>
