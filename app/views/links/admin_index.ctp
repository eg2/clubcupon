<style>
    .file label {
        /*-webkit-appearance: button;
        -moz-appearance: button;*/
        appearance: button;
        line-height: 16px;
        padding: .2em .4em;
        margin: .2em;
        float: none;
        border:none;
        min-width:unset;
    }
    .file input {
        display:none;
    }
    .file{
        display: inline-block;
        background: #fff;
        border: 1px solid #9d9d9d;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        background: rgb(255,255,255); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, #9d9d9d 300%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(300%,rgba(145,189,214,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 300%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 300%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 300%); /* IE10+ */
        background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,#9d9d9d 300%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#9d9d9d',GradientType=0 ); /* IE6-9 */
        height: 22px;
        padding-top: 4px;
        text-align: center;
        vertical-align: middle;
        width: 100%;
    }
    .file:hover{
        background: rgb(255,255,255); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, #9d9d9d 200%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(200%,rgba(145,189,214,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 200%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 200%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%,#9d9d9d 200%); /* IE10+ */
        background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,#9d9d9d 200%); /* W3C */

    }

</style>
<div class="slots-index">
    <?php
    $url = '/' . $cSlug . '/admin/links';
    echo '<h2>Slots:</h2>';
    echo '<div class="formWrapper">';
    echo $form->create('City', array(
        'url' => $url
    ));
    echo $form->input('City.slug', array(
        'type' => 'select',
        'label' => 'Ciudad',
        'options' => $citiesOptions,
        'value' => $citySlugValue,
        'onchange' => 'this.form.submit()'
    ));
    echo $form->hidden('Type.id', array(
        'value' => $typeValue
    ));
    echo $form->end();
    echo $form->create('Type', array(
        'url' => $url
    ));
    echo $form->input('Type.id', array(
        'type' => 'select',
        'label' => 'Tipo',
        'options' => array(
            ConstSearchLinksTypes::Destacado => 'Destacados',
            ConstSearchLinksTypes::Especial => 'Especiales',
            ConstSearchLinksTypes::Banner => 'Banners',
            ConstSearchLinksTypes::Banner_mobile => 'Banners Mobile'
        ),
        'value' => $typeValue,
        'onchange' => 'this.form.submit()'
    ));
    echo $form->hidden('City.slug', array(
        'value' => $citySlugValue
    ));
    echo $form->end();
    echo '<div class="input text">';
    echo $html->link('Agregar un Link', array(
        'controller' => 'links',
        'action' => 'index',
        '?' => array(
            'subAction' => 'add',
            'citySlug' => $citySlugValue,
            'linkType' => $typeValue
        )
    ));
    echo '</div>';
    echo '</div>';
    echo $form->create('Links', array(
        'action' => 'index',
        'enctype' => 'multipart/form-data'
    ));
    echo $form->hidden('Type.id', array(
        'value' => $typeValue
    ));
    echo $form->hidden('City.slug', array(
        'value' => $citySlugValue
    ));
    echo '<table>';
    echo '<thead>';
    echo '<tr>';
    echo '<td>URL</td>';
    echo '<td>Nombre</td>';
    echo '<td>Descripcion</td>';
    echo '<td>Orden</td>';
    echo '<td>Logo Izquierda</td>';
    echo '<td>Logo Derecha</td>';
    echo '<td>Style</td>';
    echo '<td colspan="2"></td>';
    echo '</tr>';
    echo '</thead><tbody>';

    foreach ($this->data['Links'] as $key => $value) {
        $logoOptions = array();
        $logoSelected = array();
        foreach ($this->data['LinksLogos'] as $logo) {
            if ($logo['id'] == $value['SearchLinks']['search_link_logo_right_id']) {
                $logoSelected[] = array(
                    $logo['name'] => $logo['name']
                );
            } else {
                $aux = array();
                $aux = array(
                    $logo['name'] => $logo['name']
                );
                $logoOptions = array_merge($logoOptions, $aux);
            }
        }

        $logoOptions = array_merge($logoSelected, $logoOptions);
        $logoOptionsLeft = array();
        $logoSelectedLeft = array();
        foreach ($this->data['LinksLogos'] as $logo) {
            if ($logo['id'] == $value['SearchLinks']['search_link_logo_left_id']) {
                $logoSelectedLeft[] = array(
                    $logo['name'] => $logo['name']
                );
            } else {
                $aux = array();
                $aux = array(
                    $logo['name'] => $logo['name']
                );
                $logoOptionsLeft = array_merge($logoOptionsLeft, $aux);
            }
        }
        $logoOptionsLeft = array_merge($logoSelectedLeft, $logoOptionsLeft);
        $stylesOptions = array();
        $styleSelected = array();
        foreach ($this->data['LinksStyles'] as $style) {
            if ($style['id'] == $value['SearchLinks']['search_link_style_id']) {
                $styleSelected[] = array(
                    $style['name'] => $style['name']
                );
            } else {
                $aux = array();
                $aux = array(
                    $style['name'] => $style['name']
                );
                $stylesOptions = array_merge($stylesOptions, $aux);
            }
        }
        $stylesOptions = array_merge($styleSelected, $stylesOptions);
        echo $form->hidden('Links.' . $key . '.id', array(
            'value' => $value['SearchLinks']['id']
        ));
        echo '<tr>';
        echo '<td>';
        echo $form->input('Links.' . $key . '.url', array(
            'label' => '',
            'size' => 20,
            'value' => $value['SearchLinks']['url']
        ));
        echo '</td><td><div style="padding-top:23px;">';
        echo $form->input('Links.' . $key . '.label', array(
            'label' => false,
        	'div' => false,
        	'size' => 10,
            'value' => $value['SearchLinks']['label']
        ));
         echo '</div></td><td>';
        echo $form->input('Links.' . $key . '.description', array(
            'label' => '',
            'value' => $value['SearchLinks']['description']
        ));        
        echo '</td><td><div style="padding-top:23px;">';
        echo $form->input('Links.' . $key . '.priority', array(
            'label' => false,
			'div' => false,
            'size' => 1,
            'value' => $value['SearchLinks']['priority']
        ));
        echo '</div></td><td>';
        echo $form->input('Links.' . $key . '.search_link_logo_left_id', array(
            'label' => '',
            'size' => 1,
            'options' => $logoOptionsLeft
        ));
        echo '</td><td>';
        echo $form->input('Links.' . $key . '.search_link_logo_right_id', array(
            'label' => '',
            'size' => 1,
            'options' => $logoOptions
        ));
        echo '</td><td>';
        echo $form->input('Links.' . $key . '.search_link_style_id', array(
            'label' => '',
            'size' => 1,
            'options' => $stylesOptions
        ));
        echo '</td><td width="350" style="padding-top:40px">';
        echo $html->image($value['SearchLinks']['image'], array(
            'alt' => $value['SearchLinks']['alt'],
            'width' => '125'
        ));
        echo $form->input('Links.' . $key . '.image', array(
            'label' => 'Seleccione imagen',
            'type' => 'file',
            'message' => ''
        ));
        echo '<span class="info">Tama&ntilde;o 215x275 p&iacute;eles. S&oacute;lo im&aacute;genes en formato jpg, jpeg, gif, png o bmp</span>';
        echo '</td><td style="width: 150px; vertical-align: bottom;">';
        echo $html->link('Borrar', array(
            'controller' => 'links',
            'action' => 'index',
            '?' => array(
                'subAction' => 'delete',
                'id' => $value['SearchLinks']['id'],
                'citySlug' => $citySlugValue,
                'linkType' => $typeValue
            )
                ), array(
            'class' => 'deleteIcon'
                ), "Â¿EstÃ¡ seguro que desea eliminar este link?");
        echo '</td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '<hr>';
    echo $form->end('Guardar');
    ?>
</div>

