<div class="states form">
    <div>
        <div>
            <h2><?php echo __l('Edit State - ').$html->cText($this->data['State']['name']); ?></h2>
        </div>
        <div>
            <?php echo $form->create('State',  array('class' => 'normal','action'=>'edit'));?>
            <?php
                echo $form->input('id');
                echo $form->input('country_id',array('label' => __l('Country'),'empty'=>__l('Please Select')));
                echo $form->input('name',array('label' => __l('Name')));
                echo $form->input('code',array('label' => __l('Code')));
                echo $form->input('adm1code',array('label' => __l('Admlcode')));
                echo $form->input('is_approved', array('label' => __l('Approved?')));
            		echo $form->input('is_selectable',array('label' => __l('Seleccionable en el perfil de usuario')));
            ?>
            <?php echo $form->end(__l('Update'));?>
        </div>
    </div>
</div>

