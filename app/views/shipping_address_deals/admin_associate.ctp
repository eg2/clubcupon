
<?php 

?>
<style>
label, select, button, input[type="button"], input[type="reset"], input[type="submit"], input[type="radio"], input[type="checkbox"] {
    cursor: auto;
}
.control-group{
    float: left !important;
    width: 100% !important;
   /*border:1px solid red !important;*/
}
.checkbox {
   /*border:1px solid green !important;*/
    width: 50% !important;
    /*width: auto !important;*/
}
.checkbox label {
    padding-top: 0px !important;
}

.control-label{
    width: 100% !important;
    font-weight: bolder;
}
.help-block {
   /*border:1px solid;*/
   display: inline;
   padding-left:20px
   
}
span.required {
    color: #FF0000;
    font-weight: bold;
}

.selectAddressAll, .selectAddressccAll {
  color: #c46535;
}
.deselectAddressAll, .deselectAddressccAll{
  color: #c46535;
} 
</style>
<?php $isEmpresaCC=($currentCompany['Company']['name']=='Club Cupón');
?>
<div>
    <div>
        <div class="alert alert-info">
            <h2><?php echo __l('Puntos de Retiro'); ?> </h2>
            <div >  
                <strong>Oferta:&nbsp;</strong>&nbsp;<?php echo  $currentDeal['Deal']['name']?><br>
                <strong>Empresa:&nbsp;</strong>&nbsp;<?php echo  $currentCompany['Company']['name']?><br>
                <strong>Ciudad:&nbsp;</strong>&nbsp;<?php echo  $currentCompany['City']['name']?><br>
            </div> 
        </div>
      
        <div style = "margin-left:10px">
         <?php echo $form->create('ShippingAddressDeal',  array('action'=>'associate'));?>
         <?php  
 				 
				 echo $form->hidden('deal_id',array('value' => $currentDeal['Deal']['id']	));
 
				 if(!$isEmpresaCC){
				 	echo $form->input('shipping_address_id',
							array ('options' =>$shippingCompany ,
								'class' => 'form-control',
                           		'type'=>'select',
                           		'multiple'=>'checkbox',
                           		'legend' => false,
                           		'div' => 'control-group',
	                           	'selected' =>$shippingDeal,
    	                        'label' => array('text' => 'Puntos de Retiro de la Empresa','class' => 'control-label') 
							)
					);
				 } 
				?>
        <br/>
        <?php	if(!empty($shippingCompany) && !$isEmpresaCC){ ?>
        &nbsp; <a class="selectAddressAll"> Seleccionar todos </a> | <a class="deselectAddressAll"> Ninguno </a>
        <?php } ?>
        
        <br/>
        <br/>
     
        <?php	if(!isset($shippingCompany) || empty($shippingCompany)){ ?>
				<div class="controls">
				    &nbsp;
				    <?php 
				    echo $html->link('No hay Puntos de Retiro asociados. Haga click aquí para crearlos.', array('controller' => 'shipping_addresses', 'action' => 'admin_add', $currentCompany['Company']['id']), array('class'=>'text-error','target'=>'_blank','escape'=>false, 'title' => 'Crear Puntos de Retiro'));
				    ?>
			  </div>
			  </div><!-- bloque Ptos de Retiro por Empresa -->
				<?php 
				}
				
					echo $form->input('shipping_addresscc_id',
							array ('options' =>$shippingCc,
								'class' => 'form-control',
								'type'=>'select',
								'multiple'=>'checkbox',
								'legend' => false,
								'div' => 'control-group',
								'selected' =>$shippingCcDeal,
								'label' => array('text' => 'Puntos de Retiro de ClubCupon','class' => 'control-label')
							)
				);
			 ?>  
       
       <div class="control-group">
           <div>
               <?php	if((!empty($shippingCc))){ ?>
               &nbsp; <a class="selectAddressccAll"> Seleccionar todos </a> | <a class="deselectAddressccAll"> Ninguno </a>
               <?php } ?>
           </div>
           
           <br/>
           <div class="controls">       
					     <input type="button" id="btn_add_shipping_address" class="btn btn-large" value="Cancelar" onclick="window.location='<?php echo Router::url(array('controller'=>'deals','action'=>'admin_index'));?>'">            	
				   </div>
           <div class="controls" style="float:left;width:200px">       
                &nbsp;
           </div>
           <div class="controls">
              <?php  echo $form->end(array('label' => __l('Save'), 'class' => 'btn btn-primary btn-large', 'div' => false));?>
           </div>
       </div>    
			</div>
     </div>
</div>
            
<script type='text/javascript'>
     $(document).ready(function() {
        function  deSelectAllCheckbox(name) {
             $("input[name*='"+ name +"']").removeAttr('checked');
        }
        function  selectAllCheckbox(name) {
             $("input[name*='"+ name +"']").attr('checked','checked');
        }

        $('.selectAddressAll').click(function() {
          var name = 'shipping_address_id';
          selectAllCheckbox(name);
        });
        
        $('.deselectAddressAll').click(function() {
          var name = 'shipping_address_id';
          deSelectAllCheckbox(name);
        });

        $('.selectAddressccAll').click(function() {
          var name = 'shipping_addresscc_id';
          selectAllCheckbox(name);
        });
       
        $('.deselectAddressccAll').click(function() {
          var name = 'shipping_addresscc_id';
          deSelectAllCheckbox(name);
        });
    }); 
</script>