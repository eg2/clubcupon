<div class = "form">
<?php echo $form->create ('Rate', array ('class' => 'normal', 'action' => 'edit', 'admin' => 'true', 'enctype' => 'multipart/form-data')); ?>
  <h2><?php echo 'Editar Raz&oacute;n de Conversi&oacute;n'; ?></h2>
	<?php
		echo $form->input ('id', array ('type' => 'hidden'));
		echo $form->input ('rate', array ('label' => 'Raz&oacute;n'));
	?>
  <div class = "submit-block">
    <?php
      echo $form->submit (__l ('Update'));
      echo $html->link ('<input type = "button"  value = "' . __l ('Cancel') . '"  class = "cancel-button" />', array ('controller' => 'rates', 'action' => 'index', 'admin' => true), array ('escape' => false));
    ?>
  </div>
  <?php echo $form->end (); ?>
</div>