<div class = "index js-response">
  <h2><?php echo 'Razones de Conversi&oacute;n';?></h2>
    <div class = "clearfix add-block1">
      <?php echo $html->link ('Cambiar', array ('controller' => 'rates', 'action' => 'edit', 'admin' => 'true'), array ('class' => 'edit', 'title' => 'Cambiar')); ?>
    </div>
  <?php echo $form->create ('Rate' , array ('class' => 'normal', 'action' => 'update')); ?>
  <table class = "list">
    <tr>				
      <th class = "dl"><div class = "js-pagination">Raz&oacute;n</div></th>
      <th><div class = "js-pagination"><?php echo 'Fecha'; ?></div></th>
    </tr>
    <?php
      if (!empty ($rates)):
        $i = 0;
        foreach ($rates as $rate):
          $class = null;
          if ($i++ % 2 == 0):
            $class = ' class = "altrow"';
          endif;
    ?>
    <tr<?php echo $class; ?>>
      <td class = "dl"><?php echo $html->cText ($rate ['Rate']['rate']); ?></td>
      <td><?php echo $html->cText ($rate ['Rate']['created']); ?></td>
    </tr>
    <?php
        endforeach;
			else:
    ?>
    <tr>
      <td colspan = "7"  class = "notice"><?php echo 'No hay razones disponibles'; ?></td>
    </tr>
    <?php
			endif;
    ?>
  </table>
  <?php if (!empty ($rates)): ?>
  <div class = "hide">
    <?php echo $form->submit ('Submit');  ?>
  </div>
  <?php endif; ?>
  <?php echo $form->end (); ?>
</div>