<div class="translations index">
<h2><?php echo __l('Translations');?></h2>
<div class="add-block1">
	<?php echo $html->link(__l('Make New Translation'), array('controller' => 'translations', 'action' => 'add'), array('class' => 'add', 'title'=>__l('Make New Translation'))); ?>
</div>
<div class = "notice">
	<?php echo __l('To make new translation default translated language English should be available.');?>
</div>
<h3><?php echo __l('Available Translations');?></h3>
<table class="list">
    <tr>
		<th><?php echo __l('Language');?></th>
        <th><?php echo __l('Google Translate');?></th>
    </tr>
<?php
if (!empty($translations)):

$i = 0;
foreach ($translations as $translation):
	$class = null;
	if ($i++ % 2 == 0):
		$class = ' class="altrow"';
    endif;
?>
	<tr<?php echo $class;?>>
		<td>
			<div class="actions-block">
            <div class="actions round-5-left">
                <span><?php echo $html->link(__l('Manage'), array('action' => 'manage', 'language_id' => $translation['Translation']['language_id']), array('class' => 'edit js-edit', 'title' => __l('Manage')));?></span>
                <span><?php echo $html->link(__l('Delete'), array('action' => 'index', 'remove_language_id' => $translation['Translation']['language_id']), array('class' => 'delete js-delete', 'title' => __l('Delete Translation')));?></span>
           </div>
        </div>
        	<?php echo $html->cText($translation['Language']['name']);?></td>
		<td><?php echo $html->cBool($translation['Translation']['is_google_translate']);?></td>
	</tr>
<?php
    endforeach;
else:
?>
	<tr>
		<td colspan="7" class="notice"><?php echo __l('No Translations available');?></td>
	</tr>
<?php
endif;
?>
</table>
</div>