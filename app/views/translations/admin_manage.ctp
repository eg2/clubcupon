<?php if(!empty($translations)): ?>
<h2><?php echo __l(sprintf('Edit Translations - %s', $languages[$this->data['Translation']['language_id']])); ?></h2>
<?php else: ?>
<h2><?php echo __l('Edit Translations'); ?></h2>
<?php endif; ?>
<div class="translations form">
<?php echo $form->create('Translation', array('action' => 'manage', 'class' => 'normal')); ?>
	<fieldset>
	<?php
		echo $form->input('language_id',array('label' => __l('Language')));
		echo $form->submit(__l('Submit'), array('name' => 'data[Translation][makeSubmit]'));
		if(!empty($translations)):
			echo $this->element('paging_counter');
			foreach ($translations as $translation):
				echo $form->input('Translation.'.$translation['Translation']['id'].'.lang_text', array('label' => __l($translation['Translation']['key']), 'value' => __l($translation['Translation']['lang_text'])));
			endforeach;
			echo $form->submit(__l('Update'), array('name' => 'data[Translation][makeUpdate]'));
			echo $this->element('paging_links');
		else:
	?>

	<?php
		endif;
	?>
	</fieldset>
	<?php echo $form->end(); ?>
</div>