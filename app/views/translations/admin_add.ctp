<div class="translations form">
<h2><?php echo __l('Add New Translation');?></h2>
<?php echo $form->create('Translation', array('class' => 'normal'));?>
	<fieldset>
 		<legend><?php echo $html->link(__l('Translations'), array('action' => 'index'));?> &raquo; <?php echo __l('Add New Translation');?></legend>
	<?php
		echo $form->input('from_language', array('label' => __l('From language'),'value' => __l('English'), 'disabled' => true));
		echo $form->input('language_id', array('label' => __l('To Language')));
		echo $form->submit('Manual Translate', array('name' => 'data[Translation][manualTranslate]'));
		echo $form->submit('Google Translate', array('name' => 'data[Translation][googleTranslate]'));
	?>
	</fieldset>
<?php echo $form->end();?>
</div>
