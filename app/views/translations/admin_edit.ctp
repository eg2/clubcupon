<div class="translations form">
<?php echo $form->create('Translation', array('class' => 'normal'));?>
	<fieldset>
 		<legend><?php echo $html->link(__l('Translations'), array('action' => 'index'),array('title' => __l('Translations')));?> &raquo; <?php echo __l('Edit Translation');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('language_id');
		echo $form->input('key');
		echo $form->input('lang_text');
	?>
	</fieldset>
<?php echo $form->end(__l('Update'));?>
</div>
