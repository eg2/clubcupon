<?php
    echo $html->css(Configure::read('theme.asset_version') . '/components/datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery-ui');
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.8.3');
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery-ui');
?>
<script>
    
    $(document).ready(function() {
        clearview();
        $( "#NewsletterSubjectScheduled" ).datepicker({
            numberOfMonths:1,
            showButtonPanel: false,
            showWeek: true,
            dateFormat: 'yy-mm-dd',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: '#'
        });
    });
    
</script>
<style>
    .ui-widget-header .ui-icon {
        background-image: url("../../../../../img/js-ui-personalized/ui-icons_gray_256x240.png");
    }
</style>
<h2>Editar Asunto de newsletter</h2>

<?php
    echo $form->create ('NewsletterSubject', array ('class' => 'normal', 'action'=>'edit'));
    echo $form->input  ('id');
    echo $form->input  ('city_id',           array ('label' => 'Ciudad: ','type' => 'select', 'options' => $selectOptionsForCity, 'empty'=> 'Elegir una Ciudad'));
    echo $form->input  ('male_text',         array ('label' => 'Asunto masculino'));
    echo $form->input  ('female_text',       array ('label' => 'Asunto femenino'));
    echo $form->input  ('unisex_text',       array ('label' => 'Asunto unisex'));
    echo $form->input  ('scheduled',         array ('label' => 'Fecha', 'type'   => 'text', 'class' => 'formatDateInput', 'readonly'));
    echo $form->end    ('Editar Asunto');
?>