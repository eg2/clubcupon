<script>
    $(document).ready(function() {
        clearview();
        $( "#NewsletterSubjectDate" ).datepicker({
            numberOfMonths:1,
            showButtonPanel: false,
            showWeek: true,
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<style>
    .ui-widget-header .ui-icon {
        background-image: url("../../img/js-ui-personalized/ui-icons_ffffff_256x240.png");
    }
    .pagLinks {
        clear:both;
        margin:20px;
        width:50%;
    }
    
    
    #searchSubject {float:right; width:660px; margin-bottom:15px;}
        #searchSubject label, #searchSubject input{ float:none;}
        #searchSubject .field{ width:150px;}
        #searchSubject .button{ width:100px; padding-top: 13px;}
            #searchSubject .button .submit{ margin-top: 10px;}
        #searchSubject .actions{ width:130px; padding-top:17px;}
            #searchSubject .actions .add-block1{ width:100%; margin:0; padding: 15px 0 0 0;}
        
    table {
        width:100%;
    }
    thead td {
        padding:        5px!important;
        min-height:     25px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }
    
    thead td a{color:#fff;}
    tr td{padding:5px;}
    
    
    
    .deleteIcon, .editIcon, .approveIcon, .rejectIcon{
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0 3px;
        width:80px;
        display:block;
        float:left;
    }
    
    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    
    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
    }
    .noResults{
        text-align:center;
        font-weight:bold;
        color:orange;
        font-size:20px;
    }

</style>
<div class = "dealCategories">
  <h2>Asuntos del Newsletter</h2>
  
  <!-- Search Form -->
  <?php echo $form->create("NewsletterSubject",array('action' => 'index')); ?>
    <table id="searchSubject">
        <tr>
            <td class="field">
                <?php echo $form->input  ('date', array ('label' => 'Fecha', 'type' => 'text',   'class' => 'formatDateInput', 'readonly')); ?>
            </td>
            <td class="field">
                <?php echo $form->input  ('city', array ('label' => 'Ciudad',   'type' => 'select', 'options' => $selectOptionsForCity, 'empty'=> 'Elegir una Ciudad')); ?>
            </td>
            <td class="button">
                <?php echo $form->end(array( 'id' => 'buscar', 'label' => 'Buscar!' )); ?>
            </td>
            <td class="actions">
                <div class = "clearfix add-block1">
                    <?php echo $html->link ("Crear Asunto", array ('controller' => 'NewsletterSubjects', 'action' => 'add'), array ('class' => 'add', 'title' => 'Crear Subject')); ?>
                </div>
            </td>
        </tr>
    </table>
  <!-- / Search Form -->
    
  <div class="pagLinks">
    <?php echo $this->element('paging_links'); ?>
  </div>
  <table>
    <thead>
      <td><?php echo $paginator->sort('Ciudad',             'City.name'); ?></td>
      <td><?php echo $paginator->sort('Fecha',              'NewsletterSubject.scheduled'); ?></td>
      <td><?php echo $paginator->sort('Asunto masculino',   'NewsletterSubject.male_text'); ?></td>
      <td><?php echo $paginator->sort('Asunto femenino',    'NewsletterSubject.female_text'); ?></td>
      <td><?php echo $paginator->sort('Asunto unisex',      'NewsletterSubject.unisex_text'); ?></td>
      <td>&nbsp;</td>
    </thead>
    <?php

    if(!is_null($newsletterSubjects)){
      foreach ($newsletterSubjects as $newsletterSubject) {
        echo '<tr>';
            echo '<td>' . $newsletterSubject['City']['name'] .'</td>';
            echo '<td>' . $time->format('d-m-Y', $newsletterSubject['NewsletterSubject']['scheduled'] ) .'</td>';
            echo '<td>' . $newsletterSubject['NewsletterSubject']['male_text'] .'</td>';
            echo '<td>' . $newsletterSubject['NewsletterSubject']['female_text'] .'</td>';
            echo '<td>' . $newsletterSubject['NewsletterSubject']['unisex_text'] .'</td>';
            echo '<td>';
            echo $html->link ("Editar",     array ('action' => 'edit',   $newsletterSubject['NewsletterSubject']['id']), array ('class' => 'editIcon'));
            echo $html->link ("Borrar",     array ('action' => 'delete', $newsletterSubject['NewsletterSubject']['id']), array ('class' => 'deleteIcon'),   '¿Seguro querés borrar este calendario?');
            echo '</td>';
        echo '</tr>';
      }
    } else {
        echo '<tr><td colspan="6" class="noResults"> Sin resultados </td></tr>';
    }
      
    ?>
  </table>
  
  <div class="pagLinks">
    <?php echo $this->element('paging_links'); ?>
  </div>
  
</div>