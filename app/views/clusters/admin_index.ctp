<div>
  <h2>Clusters</h2>
  <a href="<?php echo Router::url(array('controller'=>'clusters','action' => 'add'))?>">Agregar nuevo Cluster</a>
</div>

<table class="list">
    <tr>
        <th><div >Nombre</div></th>
        <th><div ></div></th>
    </tr>
<?php
if (!empty($clusters)):
$i = 0;
foreach ($clusters as $cluster):
    $class = null;
    if ($i++ % 2 == 0):
        $class = ' class="altrow"';
    endif;
?>
    <tr<?php echo $class;?>>
        <td class="dl"><?php echo $html->cText($cluster['Cluster']['name']);?></td>
        <td class="dl">
          <a href="<?php echo Router::url(array('controller'=>'clusters','action' => 'edit', 'id' => $cluster['Cluster']['id']))?>">Editar</a>
        </td>
    </tr>
<?php
    endforeach;
else:
?>
    <tr>
       <td colspan="17" class="notice"><?php echo __l('No hay clusters cargados');?></td>
    </tr>
<?php
endif;
?>
</table>
