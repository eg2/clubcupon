<script type="text/javascript" src="/js/clusters.js"></script>
<div class="tableDefinitionContainer">
      <script type="text/javascript" >
      var clustersDefinition = <?php echo json_encode($clusterDefinition); ?>;
      var clustersDefinitionKeys = <?php echo json_encode(array_keys($clusterDefinition)); ?>;
      </script>
  <?php if($cluster): ?>
    <input type="hidden" class="js-cluster-id" value="<?php echo $cluster['Cluster']['id']; ?>" />
    <script type="text/javascript" >
      var cluster = <?php echo json_encode($cluster); ?>;
    </script>
  <?php endif; ?>
  <h2>Agregar cluster</h2>
  <label>Nombre del Cluster:</label><input type="text" name="name" value="<?php if($cluster) echo  htmlentities($cluster['Cluster']['name']); ?>" />
  <div class="rules-container">
  </div>
  <div class="clear"></div>
  <div class="buttonActions">
    <button class="js-add-new-example">Agregar nueva ejemplo</button>
    <button class="js-save-cluster">Guardar</button>
    <button class="js-run-preview-rules">Ejecutar preview</button>
  </div>
  <div class="js-preview-result-rules">
    <table>
    </table>
  </div>
</div>




<div class="js-template-rule" style="display:none;">
  <div class="rule">
    <table>
      <tr>
        <?php
        foreach($clusterDefinition as $field => $definition) {
          if(!isset($definition['show']) || $definition['show'] === true) {
            $label = $field;
            if(!empty($definition['label'])) {
              $label = $definition['label'];
            }
            echo '<th>'.$label.'</th>';
          }
        }
        ?>
        <th></th>
      </tr>
      <tr>


        <?php
        foreach($clusterDefinition as $field => $definition) {
          if(!isset($definition['show']) || $definition['show'] === true) {
            echo '<td>';
            if($definition['type'] == 'select') {
              echo '<select name="' . $field . '">';
              echo '<option value="0" selected="selected">-- Seleccionar campo --</option>';
              foreach($definition['sourceValue'] as $key => $value) {
                echo '<option value="'.$key.'">'.$value.'</option>';
              }
              echo '</select>';
            }
            if($definition['type'] == 'date') {
              echo '<input type="text" name="' . $field . '" class="js-date-picker">';
            }
            if($definition['type'] == 'freeInput') {
              echo '<input type="text" name="' . $field . '" class="freeInput">';
            }

            echo '</td>';
          } // end show
        }
        ?>
        <td>
        </td>

        <td>
          <button class="js-delete-rule">Eliminar Regla</button>
        </td>
      </tr>

    </table>

  </div>
  
</div>
