<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="grid_12">
            <div class="bloque-home clearfix">
                <!--
                <h1 class="h1-format"><a href="">Titulo H1</a></h1>
                <h2><a href="">Titulo H2</a></h2>
                -->
                <div class="grid_12 omega alpha ancho">
                    
                <?php echo $form->create('BlockedUser', array('class' => 'normal'));?>
                <fieldset>
                    <legend><?php echo $html->link('Usuarios bloqueados', array('action' => 'index'));?> &raquo; Agregar usuario bloqueado</legend>
                    <?php
                        echo $form->input('user_id',array('label' => __l('User')));
                        echo $form->input('blocked_user_id',array('label' => 'Usuarios bloqueados'));
                    ?>
                </fieldset>
                    
                <?php echo $form->end('A&ntilde;adir');?>
                
                </div>
            </div>
        </div>
    </div>
</div>