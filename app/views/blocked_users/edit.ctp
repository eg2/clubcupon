<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="grid_12">
            <div class="bloque-home clearfix">
                
                <!-- Contenidos -->
                <?php echo $form->create('BlockedUser', array('class' => 'normal'));?>
                        <fieldset>
                            <legend><?php echo $html->link('Usuarios bloqueados', array('action' => 'index'));?> &raquo; Editar usuario bloqueado</legend>
                            <?php
                                echo $form->input('id');
                                echo $form->input('user_id',array('label' => 'Usuario'));
                                echo $form->input('blocked_user_id',array('label' => 'Usuarios bloqueados'));
                            ?>
                        </fieldset>
                <?php echo $form->end('Actualizar');?>                
                <!-- / Contenidos -->
                
                </div>
            </div>
        </div>
    </div>
</div>



