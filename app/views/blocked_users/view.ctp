<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="grid_12">
            <div class="bloque-home clearfix">
                
                <!-- Contenidos -->
                <h2>Usuarios bloqueados</h2>
                <dl class="list"><?php $i = 0; $class = ' class="altrow"';?>
                        <dt<?php if ($i % 2 == 0) echo $class;?>>ID</dt>
                                <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cInt($blockedUser['BlockedUser']['id']);?></dd>
                        <dt<?php if ($i % 2 == 0) echo $class;?>>Creado</dt>
                                <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cDateTime($blockedUser['BlockedUser']['created']);?></dd>
                        <dt<?php if ($i % 2 == 0) echo $class;?>>Modificado</dt>
                                <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->cDateTime($blockedUser['BlockedUser']['modified']);?></dd>
                        <dt<?php if ($i % 2 == 0) echo $class;?>>Usuario</dt>
                                <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->getUserLink($blockedUser['User']);?></dd>
                        <dt<?php if ($i % 2 == 0) echo $class;?>>Bloqueado</dt>
                                <dd<?php if ($i++ % 2 == 0) echo $class;?>><?php echo $html->getUserLink($blockedUser['Blocked']);?></dd>
                </dl>>                
                <!-- / Contenidos -->
                
                </div>
            </div>
        </div>
    </div>
</div>


