<div class="userFriends index">
<ol class="friends clearfix " start="<?php echo $paginator->counter(array('format' => '%start%')); ?>">
<?php
if (!empty($userFriends)) {
foreach ($userFriends as $userFriend) {
?>
	<li id="friend-<?php echo $userFriend['UserFriend']['id']; ?>" class="friend">
    	<?php echo $html->getUserAvatarLink($userFriend['FriendUser'], 'medium_thumb');?>          
		<?php echo $html->link($userFriend['FriendUser']['username'], array('controller' => 'users', 'action' => 'view', $userFriend['FriendUser']['username']), array('title' => $userFriend['FriendUser']['username']));?>
	</li>
<?php
    } ?>
</ol>
 <?php  }

else {
?>
<div>
		<p class="notice"><?php echo __l('No friends available'); ?></p>
</div>

<?php
 	}
?>

<?php
if (!empty($userFriends) and $total_friends > 12) {
    echo $this->element('paging_links');
}
?>
</div>