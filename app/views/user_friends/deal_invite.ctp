<div class="invite_friends">
	<div class="js-tabs">
		<ul class="clearfix">			
			<li><?php echo $html->link(__l('Invite to Your Friends'), array(
				'controller' => 'user_friends',
				'action' => 'myfriends',
				'type' => 'deal',
				'deal' => $deal_slug
			)); ?></li>
			<li><?php echo $html->link(__l('Import Friends'), array(
				'controller' => 'user_friends',
				'action' => 'import',
				'type' => 'deal',
				'deal' => $deal_slug
			)); ?></li>
			<li><?php echo $html->link(__l('Share Via Social Media'), '#js-share-via-facebook'); ?></li>
		</ul>
	<div id="js-share-via-facebook">
		<div class="clearfix">
			<ul class="share-list">  
			  <li><?php echo $html->link('Mail it', 'mailto:?body='.sprintf(__l('Check out %ss daily deal for coolest stuff in your city. '),Configure::read('site.name')).'-'.Router::url(array('controller' => 'users', 'action' => 'refer',  $auth->user('id')), true).'&subject='.__l('I think you should get ').Configure::read('site.name'), array('class' => 'quick', 'target' => 'blank'));?></li>
			  <li><?php echo $html->link('Share it on Facebook', 'http://www.facebook.com/share.php?u='.Router::url(array('controller' => 'users', 'action' => 'refer',  $auth->user('id')), true), array('class' => 'face','target' => 'blank'));?></li>
			  <li><?php echo $html->link('Tweet it', 'http://www.twitter.com/home?status='.Router::url(array('controller' => 'users', 'action' => 'refer',  $auth->user('id')), true), array('class' => 'tweet','target' => 'blank'));?></li>
			</ul>
		</div>
	</div>

	</div>
	<div>
		<?php echo $html->link(__l('Skip'),array('controller' => 'deals', 'action' => 'view', $deal_slug), array('class' => 'face','target' => 'blank', 'title' => __l('Skip')));?></li>
	</div>
</div>
