<?php

    $linksAppend = '?utm_source=Clarin&utm_medium=Caja&utm_term=BN-300x250&utm_campaign=Oferta-del-Dia-'.date('ymd');
    if(empty($deal))
    {
        echo '<h1>No hay ofertas para ' . $html->cText($city['City']['name']) . '</h1>';
        die;
    }
    
    $image = '<img src="/images/cmd/currentDeal.jpg?v='.  $deal['Deal']['id'].'"  alt="Oferta del Día"  style="border: none; width:150px;height:95px" width="185" height="125">';
    
?>

                <div class="box-gray modules cmd-cc">
                    <div class="content">
                        <div class="hd">
                            <h3 class="ce2"><em>Oferta del dia</em></h3>
                        </div>
                        <div class="bd ">
                            <div class="cc-info">
                                <div class="l">
                                    <a target="_blank" href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),true) . $linksAppend; ?>" >
                                        <?php echo $image; ?>
                                    </a>
                                </div>
                                <h2>
                                    <a href="http://www.clubcupon.com.ar/<?php echo $linksAppend; ?>" target="_blank">
                                        <img src="/images/cmd/clarin-widget-logocc_2.jpg" border="0"  width="77" height="30"  />
                                    </a>
                                </h2>
                                <h5>
                                    <a target="_blank" href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),true) . $linksAppend; ?>" >
                                        <?php echo $text->truncate($deal['Deal']['name'], 96, '...', false);  ?>
                                    </a>
                                </h5>
                            </div>
                            <ul class="cc-discount">
                                <li class="firts">
                                    Original
                                    <span>$ <?php echo $deal['Deal']['original_price']; ?></span>
                                </li>
                                <li>
                                    Club Cupon
                                    <span>$ <?php echo $deal['Deal']['discounted_price']; ?></span>
                                </li>
                                <li class="descuento">
                                    Descuento
                                    <span><?php echo $deal['Deal']['discount_percentage']; ?>%</span>
                                </li>
                            </ul>
                            <a target="_blank" class="cc-buy" href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'buy', $deal['Deal']['id']),true) ?>" >
                                Comprar
                            </a>
                        </div>
                    </div>
                </div>
