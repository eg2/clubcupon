<div id="content" class="box">
  <div class="js-widget-options">
    <h2 class="title">1. Configur&aacute;s</h2>
    <?php echo $form->input('city_id', array('label' => __l('Ciudad de la oferta:'), 'options' => $cities, 'class' => 'js-widget-city')); ?>
    <?php echo $form->input('widget_sizes', array('label' => __l('Ciudad de la oferta:'), 'options' => array_keys($widgetSizes), 'class' => 'js-widget-size')); ?>
  </div>


  <div class="preview">
    <h2 class="title">2. Preview</h2>
    <div id="js-widget-preview-container"><iframe name="widget-preview" id="widget-preview" src="" ></iframe></div>
  </div>

  <div class="your-code">
    <h2 class="title">3. Copia el c&oacute;digo</h2>
    <p>Copia y peg&aacute; el siguiente c&oacute;digo dentro de tu p&aacute;gina o blog:</p>
    <textarea rows="12" cols="80" class="js-widget-code"></textarea>
  </div>

</div>

<script type="text/javascript">
  var widgetBaseUrl = "<?php echo Router::url(array('controller' => 'widgets','action' => 'widget') , true)  ?>";
  var widgetSizes = <?php echo json_encode($widgetSizes);  ?>;
  var widgetSizesValues = <?php echo json_encode(array_keys($widgetSizes));  ?>;
  function reloadUrlIframe() {
    var cityId = $('#city_id').val();
    var widgetSize = widgetSizesValues[$('#widget_sizes').val()];
    console.log('widgetSize',widgetSize);
    console.log('widgetSizes',widgetSizes);
    var url = widgetBaseUrl + '?city_id=' + cityId + '&size=' + widgetSize;
    $('#widget-preview').attr('src',url);

    $('#widget-preview').attr('width',widgetSizes[widgetSize].w);
    $('#widget-preview').attr('height',widgetSizes[widgetSize].h);
    var html = $('#js-widget-preview-container').html();
    $('.js-widget-code').val(html);
  }
  $(document).ready(function(){
    $('#city_id').change(reloadUrlIframe);
    $('#widget_sizes').change(reloadUrlIframe);
    reloadUrlIframe();
  });
</script>
