<?php
  // $linksAppend = '?utm_source=RI&utm_medium=BN-300x250&utm_campaign=Oferta-del-dia-'.date('ymd');
  $linksAppend = '?utm_source=Clarin&utm_medium=Caja&utm_term=BN-300x250&utm_campaign=Oferta-del-Dia-'.date('ymd');
  if(empty($deal)): ?>
<h1>No hay ofertas para <?php echo  $html->cText($city['City']['name']);?></h1>
<?php die; ?>
<?php endif; ?>
<?php $image = '<img src="/images/cmd/currentDeal.jpg?v='.  $deal['Deal']['id'].'"  alt="Oferta del Día"  style="border: none; width:150px;height:95px" width="185" height="125">' ?>
<div style="width:300px; height:250px; border:1px solid #666; margin:0 auto; font-family:Arial; font-size:12px; background: none repeat scroll 0 0 white;">
<div style="width:300px; height:15px; background-color:#bd1320;font-size:15px;"></div>
<div style="width:300px; height:8px; background-color:#e0ddda; font-size:8px;"></div>
<div style="width:300px; height:5px; background-color:#f5f2ef; font-size:5px;"></div>
<div style="width:180px; height:35px; margin-top:8px; margin-left:8px; font-family:Georgia, 'Times New Roman', Times, serif; color:#bd1320; font-size:24px; float:left"><b>Oferta del Día</b></div>
<div style="float:left; width:100px"><a href="http://www.clubcupon.com.ar/<?php echo $linksAppend; ?>" target="_blank"><img src="/images/cmd/clarin-widget-logocc.jpg" border="0" style="margin-top:5px;" /></a></div>
<div style="width:283px; height:90px;clear:both; padding:8px; margin-bottom:22px; padding-top:8px;">
<div style="width:150px; height:95px; float:left; border:1px solid #666;">
    <a target="_blank" href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),true) . $linksAppend; ?>" >
      <?php echo $image; ?>
    </a>
   </div>
<div style="overflow: hidden;width:115px; height:89px; float:left; margin-left:8px; font-family:Arial; font-size:12px;">
  <a title="<?php echo $deal['Deal']['name']; ?>" target="_blank" style="text-decoration:none; color:#6b6c71;" href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),true) . $linksAppend; ?>">
    <?php echo $text->truncate($deal['Deal']['name'], 96, '...', false);  ?>
  </a>
</div>
</div>
<div style="border:1px solid #a8a8a8; width:280px; height:34px; padding-top: 4px; background:#eeeeee; clear:both; margin:0 auto 10px 10px; font-size:11px; color:#6b6c71;" align="center">
<div style="float:left; width:69px;">ORIGINAL <br /><span style="font-size: 14px"><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['original_price'], 'span'); ?></span></div>
<div style="float:left; width:69px;">CLUB CUPÓN <br /><b style="font-size: 14px"><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price'], 'span'); ?></b></div>
<div style="float:left; width:69px;">DESCUENTO<br /><span style="font-size: 14px;font-weight:bold;color:#FF6600;"><?php echo $deal['Deal']['discount_percentage']; ?>%</span></div>

<div style="float:right;">
  <a href="<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),true) . $linksAppend; ?>" style="text-decoration:none; color:#6b6c71;" target="_blank" title="<?php echo $deal['Deal']['name']; ?>">
    <img border="0" src="/images/cmd/clarin-widget-btn.png" border="0" style="margin-right:5px;" />
  </a>

</div>
</div>
</div>
