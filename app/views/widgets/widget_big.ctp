  <?php if(empty($deal)): ?>
<h1>No hay ofertas para <?php echo  $html->cText($city['City']['name']);?></h1>
<?php die; ?>
<?php endif; ?>
<div style="border: 1px solid #000">
<pre>
    Viendo widget de: <?php echo ($size['w']); ?>x<?php echo ($size['h']); ?>
  </pre>
  <h1><?php echo $html->cText($city['City']['name']); ?></h1>
  <?php $image = $html->showImage('Deal', $deal['Attachment'], array('id' => 'deal_image', 'class' => 'bigImage', 'style' => 'padding-top:10px;', 'dimension' => 'normal_thumb', 'alt' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false), 'title' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false))); ?>
<div id="content" class="box">
    <h3 id="right_title">OFERTA DEL D&Iacute;A
        <span id="deal_title">
                <?php echo $html->link($deal['Deal']['name'], array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), array('target' => '_blank','title' => sprintf(__l('%s'), $deal['Deal']['name']))); ?>
        </span>
    </h3>
    <?php echo $html->link($image, array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), array('target' => '_blank','title' => sprintf(__l('%s'), $deal['Deal']['name'])), null, false); ?>
    <div class="left">
        <div id="deal_tag">
            <h2 id="deal_price"><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price'], 'small'); ?></h2>
            <h3><?php echo $html->link('Comprar', array('controller' => 'deals', 'action' => 'buy', $deal['Deal']['id']), array('title' => 'Comprar',  'target' => '_blank')); ?></h3>
            <h4 class="blackTitleSmallCentered">ESTA OFERTA TERMINA EN:</h4>
        <div id="deal_countdown">&nbsp;</div>
        <span id="deal_countdown_amout" class="hidden"><?php echo intval(strtotime($deal['Deal']['end_date']) - time()); ?></span>
        <h4>LA OFERTA ESTÁ EN MARCHA</h4>
    </div>
    <?php if ($deal['Deal']['show_sold_quantity']) { ?>
    <?php if ($deal['Deal']['min_limit'] != 0) { ?>
        <div id="quantityBar">
            <div class="bar"><?php echo ($deal['Deal']['deal_external_count'] * 100) / $deal['Deal']['min_limit']; ?></div>
            <div class="overlay"></div>
        </div>
    <?php } ?>

    <?php if($deal['Deal']['deal_external_count'] == 1) { ?>
      <div class="quantityShow"><h4>COMPR&Oacute; <span><?php echo $deal['Deal']['deal_external_count']; ?></span> PERSONA</h4></div>
    <?php } elseif ($deal['Deal']['deal_external_count'] != 0) { ?>
      <div class="quantityShow"><h4>COMPRARON <span><?php echo $deal['Deal']['deal_external_count']; ?></span> PERSONAS</h4></div>
    <?php } ?>
    <?php } ?>
    <div id="company_data">
        <h2><?php
            /* if (!empty ($deal ['Deal']['custom_company_name'])) {
            echo $html->cText($deal['Deal']['custom_company_name']);
            } else */ {
            echo $html->cText($deal['Company']['name']);
            }
            ?></h2>
        <p>
            <a href="<?php echo $deal['Company']['url']; ?>" target="_blank"><?php echo $deal['Company']['url']; ?></a>
            <br /><br />
            <?php
            /* if (!empty ($deal ['Deal']['custom_company_address1'])) {
            echo $html->cText ($deal ['Deal']['custom_company_address1']) . ', ';
            } else */ if (!empty ($deal ['Company']['address1'])) {
            echo $html->cText ($deal ['Company']['address1']) . ', ';
            }

            /* if (!empty ($deal ['Deal']['custom_company_city'])) {
            echo $html->cText ($deal ['Deal']['custom_company_city']) . ', ';
            } else */ if (!empty ($deal ['Company']['City']['name'])) {
            echo $html->cText ($deal ['Company']['City']['name']) . ', ';
            }

            /* if (!empty ($deal ['Deal']['custom_company_state'])) {
            echo $html->cText ($deal ['Deal']['custom_company_state']) . ', ';
            } else */ if (!empty ($deal ['Company']['State']['name'])) {
            echo $html->cText ($deal['Company']['State']['name']) . ', ';
            }

            /* if (!empty ($deal ['Deal']['custom_company_zip'])) {
            echo $html->cText ($deal ['Deal']['custom_company_zip']);
            } else */ if (!empty ($deal ['Company']['zip'])) {
            echo $html->cText ($deal ['Company']['zip']);
            }
            ?>
        </p>
    </div>
</div>




</div>
</div>