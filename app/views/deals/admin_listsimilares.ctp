<style type="text/css">
	#feedback { font-size: 1.4em; }
	#lista .ui-selecting { background: #FECA40; }
	#lista .ui-selected { background: #F39814; color: white; }
	#lista { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	#lista li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; overflow-x:hidden}
</style>
<script type="text/javascript">
	$(function() {
		<?php if(count($similares)>0):?>
		$("#lista").selectable({
		      selected: function(event, ui) {
		            $(ui.selected).siblings().removeClass("ui-selected");
		            var idlist =  $(ui.selected).attr('id') ;//id del "li" seleccionado, en nuestro caso el id=id_deal
		            $('#id-selected').val(idlist);//asigno a un div oculto el valor del id de la oferta
		            $('#name-selected').html( $(ui.selected).html());//asigno a un div oculto el valor del nombre de la oferta
		            //$('#textOfertaSimilar').html(  $(ui.selected).html()); funciona pero mejor asignarle el valor cuando hace clic en ACEPTAR
		      }
		});
		<?php endif;?>
					
	});
</script>
<div id="id-selected" style="display:none"></div>
<div id="name-selected" style="display:none"></div>

<ol id="lista">
<?php if(count($similares)==0):?>
<li class="ui-widget-content">
No se encontraron ofertas similares.
</li>
<?php else: ?>
<?php foreach ($similares as $oferta): ?>
	<?php 
		 $numMaxCarac=60;
		 $txtOferta=$oferta['Deal']['name']; 
		 $txtFechaComienzo=date("d-m-Y", strtotime($oferta['Deal']['start_date']));
		 /*if(strlen($txtOferta)>$numMaxCarac) $txtOfertaFormateado=substr($txtOferta,0,$numMaxCarac).' (...) ';
		 else $txtOfertaFormateado=$txtOferta;*/
	?>
	<li class="ui-widget-content" id="<?php echo $oferta['Deal']['id']; ?>" >
		 <?php echo '('.$oferta['Deal']['id'].')&nbsp;&nbsp;'.$txtOferta;?>
	</li>
<?php endforeach; ?>	
<?php endif;?>
</ol>