<div class="js-response">
<h2><?php echo __l('Recent Deals');?> </h2>
<?php echo $this->element('paging_counter'); ?>
    <ol class="list">
    <?php if(!empty($deals)): ?>
      <?php foreach($deals as $deal): ?>
        <li class="clearfix">
        	<div class="deal-l">
            	<p><?php echo $html->cInt($deal['Deal']['deal_user_count']); ?> <span><?php echo __l('Bought'); ?></span></p>
            </div>
        	<div class="deal-r">
              <p><?php echo $html->link($html->truncate($deal['Deal']['name'],80), array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']),array('title'=>$deal['Deal']['name']));?></p>
              <dl class="price-list">
                <dt><?php echo __l('Price'); ?>-<?php echo Configure::read('site.currency')?></dt>
                <dd><?php echo $html->cCurrency($deal['Deal']['discounted_price']); ?></dd>
                <dt><?php echo __l('Value'); ?>-<?php echo Configure::read('site.currency')?></dt>
                <dd><?php echo $html->cCurrency($deal['Deal']['original_price']); ?></dd>
                <dt><?php echo __l('Save'); ?>-<?php echo Configure::read('site.currency')?></dt>
                <dd><?php echo $html->cCurrency($deal['Deal']['discount_amount']); ?></dd>
              </dl>
            </div>
        </li>
      <?php endforeach; ?>
    <?php else: ?>
        <li><p class="notice"><?php echo __l('No Deals available');?></p></li>
    <?php endif; ?>
    </ol>
</div>