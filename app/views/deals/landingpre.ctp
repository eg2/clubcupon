
<!-- facebook -->
        <script src="//connect.facebook.net/en_US/all.js"></script>
<?php
    if(Configure::read('themeBrown.enabled')){ 
        echo $this->element(Configure::read('theme.theme_name') . 'themeBrown');
    }
    echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection');
?>


<style>
    body {
    font: 12px Arial, Helvetica, sans;
    color: #000;
    background: #fff url(/img/body_bg.png) repeat-x top;
    margin: 0;
    }

#header_pre {
    height: 70px!important;
    text-align: center;
    padding: 20px 0 50px;
    }

#wrap.landingpre {
    min-height: inherit!important;
    }

#wrap {
    width: 980px;
    float: left;
    position: relative;
    left: 50%;
    min-height: 100%;
    height: auto!important;
    margin: 0 0 0 -490px;
    }

#content_full.box {
    width: 938px;
    }

#content_full {
    width: 980px;
    float: left;
    }

.box {
    background: #fff;
    border: 1px solid #c4c4c4;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
    margin: 10px 0 0;
    padding: 20px;
    }

#footer_pre {
    width: 100%;
    float: left;
    height: 60px;
    bottom: 0;
    left: 0;
    line-height: 60px;
    text-align: center;
    background: #d9d9d9;
    font-size: 11px;
    }

#footer_pre a {
    color: #f25607;
    }
    
a, a:hover {
    text-decoration: none;
    }
    
.preBox .txt {
    width: 100%;
    float: left;
    text-align: center;
    margin: 20px 0;
    }
    
.preBox .subs_box {
    width: 635px;
    height: 73px;
    float: left;
    display: inline;
    background: url(/img/pre/box_bg.png) no-repeat;
    margin: 20px 0 20px 44px;
    padding: 50px 0 0 215px;
    }

    form {
        width: 100%;
        float: left;
        }
    
        .preBox .subs_box input {
            width: 190px;
            float: left;
            color: #3c3227;
            font-size: 14px;
            }
    
        .preBox .subs_box select.orange {
            width: 190px;
            color: #f25607;
            font-size: 12px;
            font-weight: 700;
            margin: 0 0 0 95px;
            }    
    
        .preBox .subs_box input.submit {
            width: 113px;
            height: 40px;
            border: 0;
            background: url(/img/pre/btn.png) no-repeat top;
            cursor: pointer;
            margin: -7px 0 0 15px;
            }
    
.preBox .imgs {
    width: 850px;
    float: left;
    display: inline;
    margin: 8px 0 20px 44px;
    }

.preBox .imgs img {
  margin:0 23px 0 0;
}

.noMargin, .margin0 {
    margin: 0 !important;
}

img, input, td, th {
    vertical-align: middle;
}

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
    border: 0 none;
    color: #333333;
    font-size: 100%;
    font-weight: inherit;
    margin: 0;
    outline: 0 none;
    padding: 0;
    vertical-align: baseline;
}

form {
    float: left;
    margin-top: -10px;margin-top: 0px\0/;
    width: 100%;
}

input, select, textarea {
    background: none repeat scroll 0 0 #EEEEEE;
    border: 1px solid #9D9D9D;
    border-radius: 5px;
    float: left;
    padding: 3px 0 3px 5px;
    
}
select{
	background-position-x:0%;
	background-position-y:0%
}

#SubscriptionEmail{margin-top:5px;}
#SubscriptionCityId{margin-top:0px;}
</style>
<div id="content_full" class="box preBox">
    <div class="txt">
        <?php echo $html->image('pre/txt.png'); ?>
    </div>
    <div class="subs_box">
        <?php echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform')); ?>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td><?php echo $form->input ('email', array ('value' => 'Ingres&aacute; tu mail', 'label'=>false, 'escape'=>false, 'div'=>false, 'onclick' => "this.value == 'Ingres&aacute; tu mail' ? this.value = '' : '';")); ?></td>
                <td>&nbsp;</td>
                <td>
                    <?php
                        $options = array();
                        foreach ($clubcupon->activeCitiesWithDealsPre() as $city) {
                            $options[$city ['WebCity']['id']] = $city ['WebCity']['name'];
                        }
                        echo $form->select('city_id', $options,Configure::read ('Actual.city_id'), array('class'=>'orange', 'div'=>false)) ;
                        echo $form->hidden('from', array('value' => 'landing'));
                        echo $form->hidden('utm_source', array('value' => 'AdWords'));
                        echo $form->hidden('landing', array('value' => 'landingpre'));
                    ?>
                </td>
                <td><?php echo $form->submit(' ', array('class'=>'submit', 'div'=>false)); ?></td>
            </tr>
            <?php if($isCaptchaNecesary){ ?>
            <tr><td colspan="5"><?php echo $this->element('clubcupon/captcha', array('landingpre'=>true)); ?></td></tr>
            <?php } ?>
            <tr>
            <td style="padding:7px 0 0 100px;font:bold 13px Arial, Helvetica, sans;vertical-align: middle;" colspan="5">
			<strong>&#191;Ten&eacute;s cuenta en Facebook? usala para suscribirte en ClubCup&oacute;n
			 <?php 
        echo $html->link($html->image('/img/theme_clean/fb-btn.jpg'), '#', array('rel'=> 'nofollow','title' => 'fconnect', 'id' => 'facebookSubscribeButton'), null, false);
        ?> 
			</strong>
			</td>
			</tr>
        </table>
        <?php echo $form->end(); ?>
    </div>
    
    
        
    <div class="imgs">
        <?php echo $html->image('pre/img1.png'); ?>
        <?php echo $html->image('pre/img2.png'); ?>
        <?php echo $html->image('pre/img3.png'); ?>
        <?php echo $html->image('pre/img4.png', array('class' => 'noMargin')); ?>
    </div>
    <div class="regis" style="width: 100%;">
		<div class="regis" style="clear: both; float: right">
			<a href="http://www.clubcupon.com.ar/?from=affiliate">Ya estoy suscripto</a>
		</div>
    </div>    
    
    
</div>
