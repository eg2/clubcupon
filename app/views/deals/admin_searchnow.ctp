<!-- override de plantilla -->
<style>
    #footer_push{height:0!important;margin:0!important;}
    h3{font-size:24px!important; color:rgb(242, 86, 7)!important; padding:15px 0 0 15px!important;}
    .active a{font-weight:bold!important; color:orange!important;}
    .izq {/*border:1px solid red;*/float:left;width: 150px;}
    .dere {/*border:1px solid blue;*/float:left;margin-top: 12px;}
    .radios {/*border:1px solid green;*/float:left;margin-top: 39px;}
</style>
<script>
$(document).ready(function() {
  $('#buscar').click(function() {
	 $('#DealQ').val($.trim($('#DealQ').val()));
     largo = $('#DealQ').val().length ;
     if ($("input[name='data[Deal][filtro]']:checked").val() == '1') {
       if(largo < 4){
         alert('El criterio de busqueda por [Titulo/Empresa] debe contener al menos 4 caracteres');
         $('#DealQ').focus();
         return false;
       }
     }else{
    	 if(largo < 1){
             alert('El criterio de busqueda por [id] debe contener al menos 1 caracter');
             $('#DealQ').focus();
             return false;
         }
     }
   });
});
</script>
<div id="contents">
    <div class="tabs">
        <ul>
            <li <?php $class = $view == ConstDealStatus::PendingApproval ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Pendientes', array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::PendingApproval)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Rejected ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Rechazados', array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Rejected)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Draft ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Borradores', array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Draft)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Upcoming ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Proximas',   array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Upcoming)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Open ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Abiertas', array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Open)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Tipped ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('En Marcha',   array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Tipped)); ?>
            </li>
            <li <?php $class = $view == ConstDealStatus::Closed ? 'active' : '';?> class="<?php echo $class; ?>">
                <?php echo $html->link('Cerradas',   array('controller' => 'deals', 'action' => 'now_deals_management', 'view'=>ConstDealStatus::Closed)); ?>
            </li>
        </ul>
    </div>

    <h3>Ofertas YA</h3>
    <?php
            echo '<div class="der ancho">';
            //En ajuste...
            echo $form->create("Deal",array('action' => 'searchnow'));
            echo '<div  class="izq">';
            echo $form->input('q'    , array('label' => 'Buscar Oferta'                     ));
            echo $form->input('selected', array('type'  => 'hidden',        'value'=> $selected));
            echo '</div>';
            echo '<div class="radios">';
            $options=array('1'=>'Titulo / Empresa','2'=>'Id');
            $attributes=array('legend'=>false,'default'=>'1');
            echo $form->radio('filtro',$options,$attributes);
            echo '</div>';
            echo '<div class="dere">';

            $submit_options    = array(
                'id'    => 'buscar',
                'label' => 'Buscar',
                );
            echo $form->end($submit_options);

            echo '<br />';
            echo '</div>';
            echo '</div>';
      ?>
    <?php if(count($nowDeals)){ ?>
        <table class='list'>
            <tr>
                <th> Id             </th>
                <th> Nombre         </th>
                <th> Creado el      </th>
                <th> Modificado el  </th>
                <th> Creado por     </th>
                <th> Modificado por </th>
                <th> Empresa        </th>
                <th> Ciudad         </th>
                <th> Acciones       </th>
            </tr>

            <?php foreach ($nowDeals as $nowdeal){ ?>
            <tr>
                <td> <?php echo $nowdeal['NowDeal']['id']?>             </td>
                <td> <?php echo $nowdeal['NowDeal']['name']?>           </td>
                <td> <?php echo $nowdeal['NowDeal']['created']?>        </td>
                <td> <?php echo $nowdeal['NowDeal']['modified']?>       </td>
                <td> <?php echo $nowdeal['NowDeal']['created_by']?>     </td>
                <td> <?php echo $nowdeal['NowDeal']['modified_by']?>    </td>
                <td> <?php echo $nowdeal['Company']['name']?>           </td>
                <td> <?php echo $nowdeal['City']['name']?>              </td>
                <td width="390">
                    <?php
						if(!in_array($nowdeal['NowDeal']['deal_status_id'],array(ConstDealStatus::Draft))){
							echo $html->link('Editar',           array ('plugin' => '',    'controller' => 'deals',     'action' => 'audit_now_deal', 'admin' => true,  'id' => $nowdeal['NowDeal']['id']), array('class'=>'link-button-generic'));
						}
						if(!in_array($nowdeal['NowDeal']['deal_status_id'],array(ConstDealStatus::Open,ConstDealStatus::Tipped,ConstDealStatus::Upcoming,ConstDealStatus::Draft, ConstDealStatus::Tipped,ConstDealStatus::Rejected,ConstDealStatus::Closed))){
                            echo $html->link('Aprobar',          array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'approveNowDeal', 'admin' => false, 'now_deal_id' => $nowdeal['NowDeal']['id'],'view'=>$view), array('class'=>'link-button-generic') );
						}
						if(!in_array($nowdeal['NowDeal']['deal_status_id'],array(ConstDealStatus::Open,ConstDealStatus::Tipped,ConstDealStatus::Upcoming,ConstDealStatus::Rejected,ConstDealStatus::Draft,ConstDealStatus::Closed))){
							echo $html->link('Rechazar',         array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'rejectNowDeal',  'admin' => false, 'now_deal_id' => $nowdeal['NowDeal']['id'],'view'=>$view), array('class'=>'link-button-generic') );
						}
						if(!in_array($nowdeal['NowDeal']['deal_status_id'],array(ConstDealStatus::Open,ConstDealStatus::Tipped,ConstDealStatus::Draft, ConstDealStatus::Tipped,ConstDealStatus::Closed))){
							echo $html->link('Pasar a borrador', array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'draftNowDeal',   'admin' => false, 'now_deal_id' => $nowdeal['NowDeal']['id'],'view'=>$view), array('class'=>'link-button-generic') );
						}
						if(!in_array($nowdeal['NowDeal']['deal_status_id'],array(ConstDealStatus::Draft,ConstDealStatus::PendingApproval,ConstDealStatus::Rejected,ConstDealStatus::Closed))){
							echo $html->link('Cerrar', array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'closeNowDeal',   'admin' => false, 'now_deal_id' => $nowdeal['NowDeal']['id'],'view'=>$view), array('class'=>'link-button-generic') );
						}
						echo $html->link('Ver', array ('admin' => false, 'plugin' => 'now', 'controller' => 'now_deals', 'action' => 'detalles',$nowdeal['NowDeal']['id']), array('class'=>'link-button-generic cbox', 'target'=> 'blank') );
                    ?>
                </td>
            </tr>
            <?php
                }
            }else{

            ?>
            <td><h2>No Hay Resultados!!!</h2></td>
        </table>
    <?php } ?>
</div>