<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix">
        <h2 class="fmt-h2">Tu compra</h2>
            
            <!-- Contenidos -->
                <p>
                    Lo sentimos, se produjo un error.
                    <br />
                    Revis&aacute; los datos ingresados e intentalo nuevamente.
                </p>
                
                <div class="links">
                    <?php
                            
                    if($is_hidden_city)
                    {
                        echo $html->link ('Volver', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>'ciudad-de-buenos-aires'), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> 'btn-gris', 'escape' => false));
                    }
                    else
                    {
                        echo '<a href="/' . $citySlug . '" class="btn-gris">Volver</a>';
                       //echo '<a href="/" class="btn-gris">Volver</a>';
                    }

                    ?>
                </div>   

            
            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>

