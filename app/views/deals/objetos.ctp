<center>
    <div id = "wrapper_top">
        <?php echo $html->image ('landing-full/logo_clubcupon.png', array ('width' => 152, 'height' => 67)); ?>
    </div>
    <div id = "bg2">
        <?php echo $html->image ('landing-full/fondo_objetos.jpg', array ('width' => 1600, 'height' => 1200)); ?>
    </div>
    <div id = "wrapper_all">
        <div id = "wrapper_content">
            <div class = "bg_transparente">
                <div id = "content_all">
                    <div class = "tit">
                        <?php
                        $array = array("accesorios", "aeromodelismo", "automodelismo", "bijouterie", "blusas", "bolsos", "botas", "calzado", "camperas", "camperas de cuero", "carteras", "chaquetas", "carteras de diseñadores", "colgantes", "corbatas", "faldas", "fotografía", "fotos", "jeans", "juegos de consola", "juegos de mesa", "juegos de pc", "lentes", "libros", "modelismo naval", "outlets", "outlet", "pantalones", "faldas", "faldas elegantes", "faldas largas", "faldas moda", "remeras", "revistas", "ropa de hombre", "ropa de mujer", "ropa deportiva", "sandalias", "vinos", "tienda de deportes", "tiendas de aeromodelismo", "tiendas outlet", "zapatillas", "outlet de ropa", "zapatos");
                        if(isset ($_GET ['kw']) && in_array($_GET['kw'], $array)){
                            echo $_GET['kw'];
                        } else {
                            echo "OPORTUNIDADES &Uacute;NICAS";
                        }
                        ?>
                    </div>
                    <div align = "left">
                        <?php echo $html->image ('landing-full/descuento.png', array ('width' => 528, 'height' => 37, 'alt' => 'descuente de hasta el 80%')); ?>
                    </div>
                    <div align = "left"  style = "padding-top: 20px;">
                        <?php echo $html->image ('landing-full/descuentos_en_todo.png', array ('width' => 639, 'height' => 118)); ?>
                    </div>
                    <div class = "formulario">
                        <?php
                        
                            echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform'));
                                echo '<div class = "form">';
                                    echo '<div class = "campo_email">';

                                        echo '<div class = "input_text_required">';
                                            //echo '<label for = "SubscriptionEmail"></label>';
                                            echo $form->input ('email',         array('value' => 'Ingres&aacute; tu mail', 'label'=>false, 'escape'=>false, 'div'=>false, 'onclick' => "this.value == 'Ingres&aacute; tu mail' ? this.value = '' : '';", 'id' => "SubscriptionEmail", 'div'=>false));
                                            echo $form->hidden('from',          array('value' => 'landing'));
                                            echo $form->hidden('utm_source',    array('value' => 'AdWords'));
                                            echo $form->hidden('city_id',       array('value' => Configure::read ('Actual.city_id')));
                                            echo $form->hidden('landing',       array('value' => 'objetos'));
                                        echo '</div>';
                                    echo '</div>';
                                    echo '<div class = "submit">';
                                        echo $form->submit(' ', array('class'=>'bt_suscribirme', 'div'=>false));
                                    echo '</div>';
                                    echo '<div class = "call">';
                                        echo '<small>No divulgaremos tu direcci&oacute;n de correo.</small>';
                                    echo '</div>';

                                    echo '<div class="regis"><a href="http://www.clubcupon.com.ar/?from=affiliate">Ya estoy suscripto</a></div>';

                                echo '</div>';
                            echo $form->end();
                            
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>