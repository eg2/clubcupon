
<style>
#DealAddForm .input,#DealAddForm .checkbox,#DealAddForm .radio,#DealAddForm .select,#DealAddForm .submit
	{
	width: 100% !important;
}

span#coupon_duration_days {
	padding-top: 20px;
	padding-left: 20px;
}

.portal input,.portal label {
	float: none;
}

.readOnly {
	background: none repeat scroll 0 0 #DDDDDD !important;
	color: #999999 !important;
}

.form select {
	margin: 10px;
}

.productCampaignId,.productProductId {
	clear: both;
}

.mensajeEmpresa {
	clear: both;
	background: #ccc;
	font-weight: 700;
	margin: 10px;
	padding: 5px;
	text-align: center;
	display: none;
}
.disabledbutton {
    pointer-events: none;
    opacity: 0.4;
}

</style>

<script>
 
    <?php $is_subdeal = isset ($parent_deal_id); ?>  
    var cancelLoading=false;
    function settingsForCouponExpirationVariable(){
		/* Según si usa el Tipo de Vencimiento de Cupon Variable habilito o deshabilito el campo de Dias de validez*/
		
    	if($('#DealIsVariableExpiration option:selected').text()=='Variable'){
        	
    			  $("#DealCouponDuration").attr("readonly", false);
        		  $("#couponExpiryDiv").addClass("disabledbutton");
        };
	}
    $(document).ready(function() {
    	settingsForCouponExpirationVariable();
		/* Según si usa el Tipo de Vencimiento de Cupon Variable habilito o deshabilito el campo de Dias de validez*/    	
    	$('#DealIsVariableExpiration').change(function() {
    		  //$("#DealCouponDuration").val(0);
    		  cleanDates();
    		  if($('#DealIsVariableExpiration option:selected').text()=='Variable'){
        		  $("#DealCouponDuration").attr("readonly", false);
        		  $("#couponExpiryDiv").addClass("disabledbutton");
        		  
                          
    		  }else{
    			  $("#DealCouponDuration").attr("readonly", true);
    			  $("#couponExpiryDiv").removeClass("disabledbutton");
    			  
    		  }
    	});
    	/* Dispara el evento que calcula la Fecha de Vencimiento de Canje de Cupon de la Oferta cuando mientras ingresa el Numero de dias*/
    	$( "#DealCouponDuration" ).keyup(function( event ) {
    		$("#DealCouponDuration").val($.trim($("#DealCouponDuration").val()));
    		if($( "#DealCouponDuration" ).val().length>0){
    			 calculateCouponExpiryDateVariable();
    			 showCouponExpiryDate();
    		}
		});
 
    	

         $('#DealIsShippingAddress').click(function() {
            if ($('#DealIsShippingAddress').is(':checked')) {
                $('#DealCustomCompanyAddress1').parent().removeAttr('class');
                $('#DealCustomCompanyAddress1').parent().attr('class', 'input text');
            } else {
               $('#DealCustomCompanyAddress1').parent().attr('class', 'input text required');
            }
        });
        //disabled parent category
        disabledParentCategory();
        //changeCommission();
        
        // live query checkbox  DealPayByRedeemed
         $('#DealPayByRedeemed').livequery('change', function() {
              if ($('#DealPayByRedeemed').is(':checked')) {
                $('#DealPaymentTerm option[value=7]').attr('selected', 'selected');
                //console.log('Debería checkear el campo 7');
              } else {
                $('#DealPaymentTerm option[value=30]').attr('selected', 'selected');
                //console.log('Debería checkear el campo 30');
              }               
           }
         );
        
        
                      
        $('#DealAddForm').submit(function() {
        	/*deshabilito el boton para evitar el clickeo múltiple*/
        	document.getElementById('toupcomming').disabled=true;
        	document.getElementById('toupcomming').value='Enviando datos, espere...';
        	
            var ext = $('#AttachmentFilename').val().split('.').pop().toLowerCase();
            if(ext){
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('Extension de imagen no valida');
                    $.scrollTo('3200', 800);
                     $('.file .info').css({'background-color' : 'red', 'font-weight' : 'bolder', 'color' : 'white'});
                    return false;
                }
            }

            var bonusamount = parseInt($('#DealBonusAmount').val());
            var commissionpercentage = parseInt($('#DealCommissionPercentage').val());

            if (bonusamount==0 && commissionpercentage==0){
			$('#DealBonusAmountInteger').focus();
			$.scrollTo('3700', 800);
			$('#DealBonusAmountInteger').css({'border': '1px', 'border-color': '#FF3004', 'border-style': 'solid'});
			$('#DealCommissionPercentage').css({'border': '1px', 'border-color': '#FF3004', 'border-style': 'solid'});
			alert('Se debe definir el costo fijo o un porcentaje de comisi&oacute;n');
			return false;
            }

           if (navigator.appName=='Microsoft Internet Explorer') {
                calculatePercentage();
            }
        });
        //ibazan -inicio
        $("#dialog").dialog({
        	autoOpen: false,
        	height: 300,
        	width:700,
        	modal: true,
        	title: 'Seleccione una oferta similar',
        	buttons: {
        		'Aceptar': function() {
        			if(!$('#id-selected').val()==''){
        				$('#textOfertaSimilar').html(  $('#name-selected').html());
        				$('#DealSimilarDealId').val( $('#id-selected').val());
        			}
        			$(this).dialog('close');
        		},
        		'Cancelar': function() {

        			$(this).dialog('close');
        		}
        	},
        	close: function() {

        		//allFields.val('').removeClass('ui-state-error');
        	}
        });
        $("#dialog2").dialog({
        	autoOpen: false,
        	height: 300,
        	width:700,
        	modal: true,
        	title: 'Seleccione una oferta similar',
        	buttons: {

        		'Cancelar': function() {
        			cancelLoading=true;
        			$(this).dialog('close');
        		}
        	},
        	close: function() {
        		cancelLoading=true;
        		//allFields.val('').removeClass('ui-state-error');
        	}
        });
        $( "#dialog-message" ).dialog({
			modal: true,
			autoOpen: false,
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
				}
			}
		});
        var myArray = [{'height':'54px','width':'300px','float':'left','background':'none repeat scroll 0 0 #EEEEEE','border-radius':'5px 5px 5px 5px','padding':'3px 0 3px 5px'}];
        $('#textOfertaSimilar').css(myArray[0]);

        $('#seleccionar-btn').click(function() {

        	if(!$('#DealCompanyId').val()=='' && $('#DealDealCategoryId').val()>0){
        		listOfertasSimilares();
        	}else{
        		$('#dialog-message').dialog('option', 'title', 'Error');
        		$('#dialog-message').html('Debe seleccionar una <b>Empresa</b> y una <b>Categor&iacute;a</b> para buscar ofertas similares.');
        		$('#dialog-message').dialog('open');
        	}

        })
        .hover(
        	function(){
        		$(this).addClass("ui-state-hover");
        	},
        	function(){
        		$(this).removeClass("ui-state-hover");
        	}
        ).mousedown(function(){
        	$(this).addClass("ui-state-active");
        })
        .mouseup(function(){
        		$(this).removeClass("ui-state-active");
        });
         //ibazan -fin
        //
        <?php if (!$is_subdeal) { ?>
          
        	showDays('#DealCouponDuration','#cakedate4','#fecha-vencimiento');
           
        <?php } else {?>
    		$("#divIsVariableExpiration").addClass("disabledbutton");
        <?php } ?>
                     
                                        
        /*dispa el evento despues de que se selecciona la hora del Vencimiento del cupon
         * para mostrar en la etiqueta ese dato*/
        var horario = '#caketime4';
        $(horario).livequery('blur', function() {
            	                                
                                                $('#salida-fecha').show('slow');
                                                showDays('#DealCouponDuration',
                                                '#cakedate4',
                                                '#fecha-vencimiento');
                                                }
        );
        
        /*disparo el evento del calculo de dias de validez del cupon luego de que
        se cierren alguna de las ventanas de datepicker tanto de inicio como de vencimiento de coupon 
        */
       
        $('#datewrapper3:hidden').livequery(function() {
        	if($('#DealIsVariableExpiration option:selected').text()=='Variable'){
        		calculateCouponExpiryDateVariable();
              	showCouponExpiryDate();
        	}else{
        		calculateDaysDuration();
        	}
        	
        });
        $('#datewrapper4:hidden').livequery(function() {
        	calculateDaysDuration();
        });
       
        /*ejecuto el evento de calculo de fecha de expiracion de cupon cuando cierro la ventana de fecha de finalizacion de la oferta*/
        $('#datewrapper2:hidden').livequery(function() {
        	if($('#DealIsVariableExpiration option:selected').text()=='Variable'){
       	 		calculateCouponExpiryDateVariable();
       	 		showCouponExpiryDate();
        	}
       });
           

    });

    function showCouponExpiryDate(){
    	fromdate=parseInt($("#DealCouponExpiryDateDay").val());
    	frommonth=parseInt($("#DealCouponExpiryDateMonth").val()) - 1;
    	fromyear=parseInt($("#DealCouponExpiryDateYear").val());

    	if(!isNaN(fromdate) && !isNaN(frommonth) && !isNaN(fromyear) ){
        	var newDate = new Date(fromyear,frommonth,fromdate);
        	newDate.setHours(0);
        	newDate.setMinutes(0);
        	/*alert('showCouponExpiryDate');*/
    		$("#fecha-vencimiento").text(formatdate(newDate));
    	}
    }
    function showDays(days, start_date, whereput) {
    	
    		start_date='#cakedate4';
        	var start = $(start_date).datepicker('getDate');
        	
        	if (!start) return;
		    var daysvalidity = 0;

        	hora     = $('select#DealCouponExpiryDateHour').val();
        	min      = $('select#DealCouponExpiryDateMin').val();
        	meridian = $('select#DealCouponExpiryDateMeridian').val();

        	if (meridian =='pm' && hora !='12') { hora = parseInt(hora) + 12;}
        	if (meridian =='am' && hora =='12') { hora = '00';}

        	start.setHours(hora);
        	start.setMinutes(min);

        	if ($.find('span#coupon_duration_days')=='') {
            	$(days).after('<span id="coupon_duration_days"> </span>');
        	}

        	newDate = new Date(start.getTime() + daysvalidity);
        	
        	$(whereput).text(formatdate(newDate));
        	
    	
    }

    function cleanDates(){
   
    	//$('span[class^="js-date-display"]').html('Seleccionar');/*limpia los textos de todas las fechas*/
    	//$('input[id^="caketime"]').val('');/*limpia valores de fecha de inicio*/
    	
    	//$('select[id^="DealStartDate"]').val('');/*limpia valores de fecha de inicio*/
   
    	
    	//$('select[id^="DealEndDate"]').val('');/*limpia valores de fecha de fin*/

    	//$('select[id^="DealCouponStartDate"]').val('');/*limpia valores de fecha de inicio de canje*/
    	//$('select[id^="DealCouponExpiryDate"]').val('');/*limpia valores de fecha de fin de canje*/

    	//$("#fecha-vencimiento").text("");
    	
    	
    }
    
    function calculateCouponExpiryDateVariable(){
    	if($('#DealIsVariableExpiration option:selected').text()=='Variable'){
    	 var fromdate = document.getElementById("DealEndDateDay").value; 
    	 fromdate = parseInt(fromdate);

    	 var fromdateCoupon = document.getElementById("DealCouponStartDateDay").value; 
    	 fromdateCoupon = parseInt(fromdateCoupon);
    	 
    	 diffdays = parseInt($( "#DealCouponDuration" ).val());
    	 if(!isNaN(fromdateCoupon) && !isNaN(fromdate) && !isNaN(diffdays)){
	        var frommonth = document.getElementById("DealEndDateMonth").value;; 
	        frommonth = parseInt(frommonth);
		    var fromyear = document.getElementById("DealEndDateYear").value;; 
	        fromyear = parseInt(fromyear); 
	        var firstDateDeal = new Date(fromyear,frommonth-1,fromdate);

	        var frommonthCoupon = document.getElementById("DealCouponStartDateMonth").value;; 
	        frommonthCoupon = parseInt(frommonthCoupon);
		    var fromyearCoupon = document.getElementById("DealCouponStartDateYear").value;; 
	        fromyearCoupon = parseInt(fromyearCoupon); 
	        var firstDateCoupon = new Date(fromyearCoupon,frommonthCoupon-1,fromdateCoupon);

	        var firstDate;     
			if(firstDateDeal.getTime()>firstDateCoupon.getTime()){
				firstDate = firstDateDeal;
			}else{
				firstDate = firstDateCoupon;
			}

	    	        
	        var secondDate = new Date();
	        secondDate.setTime(firstDate.getTime() + diffdays*86400000 );//si se hace con getDate()+diffDays NO cambia el mes
	        
	        secondDate.setHours(0);
	        secondDate.setMinutes(0);
	        var day=secondDate.getDate().toString();
	        var month=secondDate.getMonth()+1;
	        month=month.toString();
	        if(day.length<2){
		        day="0"+day;
	        }
	        if(month.length<2){
	        	month="0"+month;
	        }
	        
	        
	        $("#DealCouponExpiryDateDay").val(day);
            $("#DealCouponExpiryDateMonth").val(month);
	        $("#DealCouponExpiryDateYear").val(secondDate.getFullYear());

	        $('select#DealCouponExpiryDateHour').val(12);
        	$('select#DealCouponExpiryDateMin').val(0);
        	$('select#DealCouponExpiryDateMeridian').val("am");

        	$('.js-date-display-4').text( $("#DealCouponExpiryDateYear").val()+"-"+$('#DealCouponExpiryDateMonth option:selected').text().substr(0, 3)+"-"+$("#DealCouponExpiryDateDay").val());
        	$("#caketime4").val("12:00 am");
    	 }
    	}
    }
    function calculateDaysDuration(){
      if($('#DealIsVariableExpiration option:selected').text()=='Fijo'){
    	
    	var fromdate = document.getElementById("DealCouponStartDateDay").value; 

    	fromdate = parseInt(fromdate);
        
        var frommonth = document.getElementById("DealCouponStartDateMonth").value;; 
        frommonth = parseInt(frommonth);

        var fromyear = document.getElementById("DealCouponStartDateYear").value;; 
        fromyear = parseInt(fromyear);

        var todate = document.getElementById("DealCouponExpiryDateDay").value; 
        todate = parseInt(todate);

        var tomonth = document.getElementById("DealCouponExpiryDateMonth").value; 
        tomonth = parseInt(tomonth);

        var toyear = document.getElementById("DealCouponExpiryDateYear").value; 
        toyear = parseInt(toyear);

        var diffDays;
        if(isNaN(fromdate) || isNaN(todate) ){
        	diffDays = 0;
        }else{
            var oneDay = 24*60*60*1000;
            var firstDate = new Date(fromyear,frommonth,fromdate);
            var secondDate = new Date(toyear,tomonth,todate);
            diffDays = (firstDate.getTime() - secondDate.getTime())/(oneDay);
            
            if(secondDate<firstDate){
               //alert('La fecha de Vencimiento del Cupón debe ser posterior a la del Inicio de Canje');
               diffDays = 0;
            }else{
               diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));	
            }
        }
        document.getElementById("DealCouponDuration").value=diffDays;
      } 
    }


  //ibazan -inicio
    function listOfertasSimilares(){
    	cancelLoading=false;
    	var data = "company_id="+ $('#DealCompanyId').val()+"&category_id="+$('#DealDealCategoryId').val();
    	data+="&deal_id=";
    	var urlList="<?php echo Router::url(array('controller' => 'deals', 'action' => 'listsimilares'), false)?>";
    	//alert(urlList);
    	$('#dialog2').html('<span style="background:url(/img/loading.gif) right no-repeat;">Espere mientras se cargan las ofertas ... </span>');
    	$('#dialog2').dialog('open');

    	$.ajax({
    		type: "post",  // Request method: post, get
    		url:urlList,
    		data: data,  // post data
    		success: function(response) {

    		    if(cancelLoading==false){ //s�lo si NO se cancel� el popup con la imagen de loading
    		    	$('#dialog2').dialog('close');
    				document.getElementById("dialog").innerHTML = response;
    				$('#dialog').dialog('open');
    			}
    		},
    		error:function (XMLHttpRequest, textStatus, errorThrown) {
    			alert(textStatus);
    		}
    	});

    	return false;
    }
  //ibazan -fin

</script>


<div id="dialog"></div>
<div id="dialog2"></div>
<!-- este div es s�lo para mostrar la animaci�n de "cargando..." antes de mostrar los resultados en el otro dialog-->

<div id="dialog-message"></div>
<div class="deals form edit_deal">


	<script src="/js/libs/jquery.scrollTo-1.4.2-min.js"
		type="text/javascript"></script>
	<STYLE type="text/css">
label {
	width: 200px;
	text-align: right;
}

form .info {
	padding: 5px 10px;
}
</STYLE>
 <?php //var_dump($_REQUEST);  
 //var_dump($deal_categories);
 //die('viendo las categorías');
 //var_dump($deal_categories);
 //die ('Testeando las categorías');
 ?>
<?php echo $this->element ('js_tiny_mce_setting', array ('cache' => Configure::read ('tiny_mce_cache_time.listing_element_cache_duration'))); ?>
<?php echo $form->create ('Deal', array ('action' => 'add', 'class' => 'normal', 'enctype' => 'multipart/form-data')); ?>
<fieldset>
		<h2><?php echo __l('Add Deal'); ?></h2>
  <?php if ($auth->user ('user_type_id') == ConstUserTypes::Company) { ?>
    <div class="adddeal-img-block">
      <?php // echo $html->image ('company-deal-flow.jpg', array ('alt'=> __l('[Image: Company Deal Flow]'), 'title' => __l('Company Deal Flow'))); ?>
    </div>
  <?php } else { ?>
    <div>
      <?php // echo $html->image ('admin-deal-flow.jpg', array ('alt'=> __l('[Image: Administrator Deal Flow]'), 'title' => __l('Administrator Deal Flow'))); ?>
    </div>
  <?php } ?>
  <fieldset class="form-block">
			<legend>General</legend>
    <?php
    		echo $form->input ('parent_deal_id', array ('type' => 'hidden'));
    		echo $form->input ('similar_deal_id', array ('type' => 'hidden'));
    	    echo $form->input ('user_id', array ('type' => 'hidden'));
      if (!$is_subdeal) {
        echo $form->input ('publication_channel_type_id', array ('label' => 'Canal de Publicacion', 'type' => 'select', 'options' => Configure::read('publication_channel.ALL'), 'escape' => false, 'style' => 'width:300px;'));
      }

      echo $form->input ('name', array ('label' => __l('Name'), 'class' => 'inputlong'));
      echo $form->input ('subtitle', array ('label' => 'Subtitulo', 'class' => 'inputlong'));
      echo $form->input ('descriptive_text', array ('type' => 'text', 'label' => 'Texto descriptivo'));
      echo $form->input ('redirect_url', array ('label' => 'URL de redirección', 'info'=>'Si esta oferta se agota o finaliza, se redirigirá directamente a la URL ingresada.<br /> Dejar en blanco para evitar la redirección.<br /><br />'));
      
      if (ConstUserTypes::isPrivilegedUserOrAdmin ($auth->user ('user_type_id'))) {
        //
        echo '<div class="inventoryStrategyGroup">';
        echo '<hr />';
        echo $form->input ('company_id', array ('label' => __l('Company'), 'empty' => __l('Please Select')));
        echo '<div class="mensajeEmpresa"></div>';
        echo $form->input ('product_inventory_strategy_id', array ('label' => 'Estrategia de inventario de producto', 'class' => 'inputlong', 'options' => $productInventoryStrategyOptions, 'empty' => 'Seleccione una estrategia'));
        echo $form->input ('product_campaign_id', array ('label' => 'Seleccione una campa&ntilde;a', 'class' => 'inputlong', 'options' => '', 'empty' => 'Seleccione una campa&ntilde;a', 'escape'=>false, 'div'=>'productCampaignId'));
        echo $form->input ('product_product_id',  array ('label' => 'Seleccione un producto',        'class' => 'inputlong', 'options' => '', 'empty' => 'Seleccione un producto',        'escape'=>false, 'div'=>'productProductId'));
        if($this->validationErrors['Deal']['Deal.product_product_id']){
                echo '<div class="error-message">'.$this->validationErrors['Deal']['Deal.product_product_id'].'</div>';
        }
        
        echo $form->hidden('secret_product_inventory_strategy_id',  array('value' => $this->data['Deal']['product_inventory_strategy_id']) );
        echo $form->hidden('secret_product_campaign_id',            array('value' => $this->data['Deal']['product_campaign_id']) );
        echo $form->hidden('secret_product_product_id',             array('value' => $this->data['Deal']['product_product_id']) );
        
        echo '</div>';
        echo '<hr />';
        //
        
      } else {
        echo $form->input ('company_id', array ('type' => 'hidden'));
      }
      echo $form->input ('deal_category_id', array ('label' => 'Categor&iacute;a', 'escape' => false, 'style' => 'width:300px;', 'empty'=>'Seleccione una Categoría'));
      echo $form->input ('gender', array (
        'label' => 'Genero',
        'type' => 'select',
        'options' => Configure::read('deal_gender.ALL'),
        'escape' => false,
        'style' => 'width:300px;'
      ));      

      echo $form->input ('sales_forecast', array ('label' => "Pronostico de Ventas"));
      if($creatingParentDeal) {
        $extraOptions = array( 'value' => $city_id );
      }
      
      echo $form->input('city_id', $extraOptions);
      
      //
//      // Vendedor/a
//      echo $form->input ('seller_id', array (
//          'label'	=> 'Vendedor/a',
//          'options'	=> $sellers_id,
//          'type'	=> 'select',
//          'empty'	=> true
//      ));
      ?>
      <div class="input text">
				<label>Oferta similar</label> <span id="textOfertaSimilar">Ninguna</span><?php echo $form->button ('Buscar', array ('class' => 'link-button-generic', 'id' => 'seleccionar-btn')); ?>
      </div>
      <?php
      echo $form->input ('cluster_id', array ('label' => __l('Cluster de usuarios'), 'info' => __l('El cluster asociado por el cual se determinar&aacute; a que usuarios registrados se enviar&aacute; la oferta.')));
      ?>
      <div id="divIsVariableExpiration">
     <?php
     	echo $form->input ('is_variable_expiration', array (
     		'label' => 'Tipo de Vencimiento de Cup&oacute;n',
     		'type' => 'select',
     		'options' => array(0=>'Fijo',1=>'Variable'),
     		'escape' => false,
     		'style' => 'width:300px;',
			'info' => __l('[ Fijo ] Debe ingresar Fecha de Vencimiento de Cupon - [ Variable ] Debe ingresar Dias de Validez.')
    	 ));
        ?>
        </div>
    <div class="campo_fechas">
        <?php if (!$is_subdeal) { ?>
          <div class="clearfix date-time-block">
			<div class="clearBoth input date-time clearfix required">
				<div class="js-datetime">
                <?php echo $form->input ('start_date', array ('label' => __l('Start Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
              	</div>
			</div>
			<div class="clearBoth input date-time end-date-time-block clearfix required">
				<div class="js-datetime">
                <?php echo $form->input ('end_date', array ('label' => __l('End Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
            	</div>
			</div>
			<div class="clearBoth input date-time clearfix required">
				<div class="js-datetime">
        			<?php echo $form->input ('coupon_start_date', array ('label' => __l('Fecha de inicio de canje'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
        		</div>
			</div>
			<div class="clearBoth input date-time end-date-time-block clearfix required" id="couponExpiryDiv">
				<div class="js-datetime">
    			<?php echo $form->input ('coupon_expiry_date', array ('label' => __l('Fecha de vencimiento del cupon'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
    			</div>
			</div>
		  </div>
        <?php } else { ?>
          <div class="clearfix">
			<div class="clearBoth input clearfix required">
              <?php echo $form->input ('start_date', array ('label' => __l('Start Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
            </div>
			<div class="clearBoth input clearfix required">
              <?php echo $form->input ('end_date', array ('label' => __l('End Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
            </div>
			<div class="clearBoth input clearfix required">
            <?php echo $form->input ('coupon_start_date', array ('label' => __l('Fecha de inicio de canje'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
            </div>
			<div class="clearBoth input clearfix required" id="couponExpiryDiv">
              <?php echo $form->input ('coupon_expiry_date', array ('label' => __l('Fecha de vencimiento del cupon'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
            </div>
		</div>
        <?php } ?>
    </div>
    <?php echo $form->input('coupon_duration',
                                array ('label' => 'Dias de validez',
                                        'info' => '',
                                        'readonly' => true,
                                        )); ?>
                                        
    <div class="notification">
				<p>
					La fecha de vencimiento de los cupones es: <span
						id="fecha-vencimiento" style="color: #666; font-style: italic;"><?php echo isset($this->data['Deal']['coupon_expiry_date'])?date( 'd-m-Y h:i:s', strtotime( $this->data['Deal']['coupon_expiry_date']) ):""; ?></span>
				</p>
			</div>

			<div class="clearfix input-blocks">
				<div class="input-block-left">
        <?php echo $form->input ('min_limit', array ('label' => __l('No. of min. buyers'))); ?>
      </div>
				<div class="input-block-right">
        <?php echo $form->input ('max_limit', array ('label' => __l('No. of max. buyers'), 'info' => 'M&aacute;ximo de cupones permitidos')); ?>
      </div>
			</div>
			<div class="clearfix input-blocks">
				<div class=" input-block-left">
        <?php echo $form->input ('buy_min_quantity_per_user', array ('label' => __l('Minimum Buy Quantity'), 'info' => __l('Minimum purchase per user including gifts.'))); ?>
      </div>
				<div class="input-block-right">
        <?php echo $form->input ('buy_max_quantity_per_user', array ('label' => __l('Maximum Buy Quantity'), 'info' => __l('Maximum purchase per user including gifts. Leave blank for no limit.'))); ?>
      </div>
			</div>

			<!-- FIBERTEL -->

			<div class="fibertel_block deal_type_box">
				<img src="/img/chapa_fibertel.png" />
				<div style="clear: both;">&nbsp;</div>
        <?php echo $form->input ('is_fibertel_deal', array ('label' => 'Es una oferta exclusiva de Fibertel? ', 'type' => 'checkbox', 'class' => 'deal_type_check')); ?>
        <div style="clear: both;">&nbsp;</div>
			</div>

			<div class="ticketportal_block deal_type_box">
				<img src="/img/chapa_ticketportal.gif" />
				<div style="clear: both;">&nbsp;</div>
        <?php echo $form->input ('is_ticketportal_deal', array ('label' => 'Es una oferta exclusiva de Ticket Portal? ', 'type' => 'checkbox', 'class' => 'deal_type_check')); ?>
        <?php echo $form->input ('ticketportal_deal_pin', array ('label' => 'Pin de TicketPortal', 'info'=>'Especificar el codigo de pin de promoc&iacute;on de TicketPortal.')); ?>
        <?php echo $form->input ('ticketportal_deal_url', array ('Url de TicketPortal ' => 'Pin', 'info'=>'Especificar la url de la promoc&iacute;on en TicketPortal.<br/>ej http://www.ticketportal.com.ar/eventperformances.asp?evt=350')); ?>
        <div style="clear: both;">&nbsp;</div>
			</div>

			<div class="clarin365_block deal_type_box">
				<img src="/img/chapa_clarin365.jpg" />
				<div style="clear: both;">&nbsp;</div>
        <?php echo $form->input ('is_clarin365_deal', array ('label' => 'Es una oferta exclusiva de Clarin 365? ', 'type' => 'checkbox', 'class' => 'deal_type_check')); ?>
        <div style="clear: both;">&nbsp;</div>
			</div>

			<div class="discount_mp deal_type_box">
				<div style="clear: both;">&nbsp;</div>
		 <?php echo $form->input ('is_discount_mp', array ('label' => 'Tiene descuento con Mercado Pago? ', 'type' => 'checkbox', 'class' => 'deal_type_check')); ?>
		<div style="clear: both;">&nbsp;</div>

			</div>
    
    <?php
      if (Configure::read ('deal.is_side_deal_enabled')) {
        echo $form->input ('is_side_deal', array ('label' => __l('Side Deal'), 'info' => __l('Side deals will be displayed in the side bar of the home page.')));
      }
      
      echo $form->input ('is_tourism', array ('label' => 'Es oferta de turismo', 'type'=>'hidden'));


      $portal_options = array(
            'is_normal' => 'Es oferta normal',
            'is_tourism' => 'Es oferta de turismo',
            'is_product' => 'Es oferta de productos'
      );
      if($is_subdeal)
      {
          foreach($portal_options as $k => $v){
              if($this->data['Deal']['portal']!=$k){
                  unset($portal_options[$k]);
              }

          }
      }

      echo $form->input('portal',array ('options' =>
                                               $portal_options,
                                                'type'=>'radio',
                                                'multiple'=>'radio',
                                                'legend' =>  false,
                                                'div' => 'portal checkbox',
                                                'default' => 'is_normal'
                                                 ));
      ?>
     <div class="wholesaler deal_type_box" disabled="true">
				<div style="clear: both;">&nbsp;</div>
         <?php echo $form->input ('is_wholesaler', array ('label' => 'Es Mayorista de Turismo? ', 'type' => 'checkbox', 'class' => 'deal_type_check')); ?>
        <div style="clear: both;">&nbsp;</div>
			</div>
     <?php
     echo $form->input ('is_end_user'            ,array ('label' => 'Se factura al usuario final'));?>
                <div style="clear: both;">&nbsp;</div>
      <?php
      echo $form->input ('show_sold_quantity', array ('label' => '&iquest;Mostrar cantidad vendida?'));
      echo $form->input ('is_subscription_mail_sent', array ('label' => '&iquest;Ya fue enviada en el newsletter?'));
    ?>
  </fieldset>

		<fieldset class="form-block">
			<legend>Opciones para la liquidaci&oacute;n</legend>
              
        <?php echo $this->element ('deal_trade_agreement', array ('dealTradeAgreementOptions' => $dealTradeAgreementOptions,'dealTradeAgreementConditions' => $dealTradeAgreementConditions,'is_subdeal'=>$is_subdeal)); ?>
        
        <script>
            $(document).ready(function(){

                $("#DealIsPaymentTermException").change(function(){
                    revealOrHidePaymentPeriodFields();
                });
            
            function revealOrHidePaymentPeriodFields(){
                if($('#DealIsPaymentTermException').attr("checked")) {
                        console.log('Show!');
                        $('#custom_payment_term').show();
                        $('#payment_term').hide();
                    } else {
                        console.log('Hide!');
                        $('#custom_payment_term').hide();
                        $('#payment_term').show();
                    }
            }
            
            revealOrHidePaymentPeriodFields();
                
             //campaign_code
            $("#DealCampaignCode").change(function(){
              $("#DealCampaignCodeType").val("E");
              $("#DealCampaignCode").removeAttr("readonly");
              $("#DealCampaignCode").removeClass("readOnly");
            });
        });
        </script>
        <?php
            if(!$is_subdeal){
                
            echo $form->input ('payment_term', array (
                'label'	  => 'Plazo de pago',
                'options' => $paymentPeriods,
                'type'	  => 'select',
                'div'     => array('id' => 'payment_term'),
                ));
            
            echo $form->input ('custom_payment_term',  array (
                'label'   => 'Plazo de pago a medida',
                'info'    => 'Especificar la cantidad de días de plazo de pago',
                'div'     =>  array(
                    'id'    => 'custom_payment_term',
                    'style' =>'display:none;')
                ));
            
            echo $form->input ('is_payment_term_exception', array (
                'label'   => 'Indicar un plazo de pago diferente',
                'type'	  => 'checkbox',
                'info'	  => 'Tildar si se desea agregar un plazo de pago diferente.',
                ));
            }
            
            echo $form->input ('campaign_code',  array (
                'label'   => 'Id de campa&ntilde;a',
                'info'    => 'Indica la relación existente entre varias ofertas diferentes',
                'original_value' => $this->data['Deal']['campaign_code'],
                ));
            
            echo $form->input ('requisition_number',  array (
                'label'   => 'N&#186; de solicitud de pedido',
            	'class'	  => 'js-round-price',
                'original_value' => $this->data['Deal']['requisition_number'],
                ));    
            
            echo $form->input ('campaign_code_type', array (
              'type'	  => 'hidden',
            ));

            echo $form->input ('is_replicated', array (
              'type'	  => 'hidden',
              'original_value' => $this->data['Deal']['is_replicated'],
            ));

        ?>
        </fieldset>

		<fieldset class="form-block">
			<legend>Comisi&oacute;n</legend>
			<div class="clearfix">
                <?php echo $form->input ('agreed_percentage', array ('default' => 0, 'type' => 'text', 'label' => 'Porcentaje acordado para la primera liquidación', 'info' => 'Debe ser un valor numérico entre 0 y 100')); ?>
            </div>
			<div class="clearfix">
                <?php echo $form->input ('is_pending_accounting_event', array ('default' => 0, 'type' => 'checkbox', 'label' => 'Tiene eventos contables pendientes', 'info' => 'Tildar para marcar que tiene eventos contables pendientes, este campo no es hereado a subofertas.')); ?>
            </div>
			<br />
			<div class="clearfix">
      <?php echo $form->input ('pay_by_redeemed', array ('type' => 'checkbox', 'label' => 'Liquidar por cupones redimidos')); ?>
    </div>
			<!--
    <div class = "page-info">
      <?php echo __l('Total Commission Amount = Bonus Amount + ((Discounted Price * Number of Buyers) * Commission Percentage/100))'); ?>
    </div>
    -->
			<div class="clearfix">
				<div class="amount-block commision-form-block">
        <?php
          //echo $form->input ('bonus_amount', array ('label' => __l('Bonus Amount'), 'class' => 'js-round-price', 'after' => ' <span class = "info"> Esta es la tarifa plana que la empresa va a pagar por toda la oferta </span>'));
          echo $form->input ('commission_percentage', array ('info' => ' Este es el porcentaje de la comisi&oacute;n que la compa&ntilde;&iacute;a va a pagar por la totalidad de la oferta ', 'class' => 'js-price', 'label' => __l('Commission (%)')));
          if (ConstUserTypes::isPrivilegedUserOrAdmin ($auth->user ('user_type_id'))) {
            echo $form->input ('private_note', array ('type' => 'textarea', 'label' => __l('Private Note'), 'info' => ' Este campo es de referencia para la administraci&oacute;n. No se mostrar&aacute; a otros usuarios.'));
          }
          echo $form->input ('downpayment_percentage', array ('info' => ' El fondo de garant&iacute;a es el porcentaje que se retiene del total de cupones vendidos y no se le paga al comercio hasta tanto no haya finalizado la vigencia del cup&oacute;n.', 'label' => 'Fondo de garant&iacute;a'));
        ?>
      </div>
			</div>
		</fieldset>
		<fieldset class="form-block">
			<legend>Opciones de env&iacute;o</legend>
        <?php
            echo $form->input ('is_receivership_delivery', array ('label' => '&iquest;Retiro en receptor&iacute;as?'));
            echo $form->input ('is_home_delivery',         array ('label' => '&iquest;Env&iacute;o a domicilio?'));
            echo $form->input ('is_shipping_address',      array ('label' => '&iquest;Usa Puntos de retiro?'));
            echo $form->input ('is_shipping_adress_user',  array ('label' => '&iquest;El comprador ingresa su direcci&oacute;n?'));
        ?>
  </fieldset>
  
  <?php if (ConstUserTypes::isLikeAdminOrPartner ($auth->user ('user_type_id'))) { ?>
    <fieldset class="form-block">
			<legend>Formas de Pago</legend>
			<div class="clearfix ">
				<div class="clearBoth required  payment-settings-container">
          <?php
          echo  $adminpagos->payment_settings ($paymentOptions, $paymentOptionPlans, $errorPaymentOption);
            if (empty ($errorPaymentOption) && empty ($paymentOptions)) {
              echo $adminpagos->adminSelectAll (default_payment_setting_id ($city_slug));
              // echo $adminpagos->adminSelectAll (Configure::read ('payment.default_payment_setting_id'));
            }

          ?>
        </div>
				<div class="js-payment-options-list"></div>
			</div>
		</fieldset>
  <?php } ?>

    <?php if($is_subdeal){ ?>
        <div class="notification">
			<p>Los medios de pago no son editables.</p>
		</div>
    <?php } ?>


  <fieldset class="form-block">
			<legend>Personalizaci&oacute;n</legend>
    <?php
      echo $form->input ('custom_subject', array ('label' => 'Subject del mail', 'class' => 'inputlong', 'div' => false));
      echo $form->input ('custom_company_name', array ('label' => 'Nombre de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('custom_company_contact_phone', array ('label' => 'Tel&eacute;fono de contacto de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('custom_company_address1', array ('label' => 'Direcci&oacute;n de la compa&ntilde;&iacute;a (1)', 'class' => 'inputlong'));
      echo $form->input ('custom_company_address2', array ('label' => 'Direcci&oacute;n de la compa&ntilde;&iacute;a (2)', 'class' => 'inputlong'));
      echo $form->input ('custom_company_city', array ('label' => 'Ciudad de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('custom_company_state', array ('label' => 'Provincia de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('custom_company_country', array ('label' => 'Pa&iacute;s de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('custom_company_zip', array ('label' => 'C&oacute;digo Postal de la compa&ntilde;&iacute;a', 'class' => 'inputlong'));
      echo $form->input ('priority', array ('label' => 'Prioridad'));
      echo $form->input ('risk', array (
          'type' => 'select',
          'options' => array (
              ConstRisk::Low => ConstRisk::getFriendly (ConstRisk::Low),
              ConstRisk::Medium => ConstRisk::getFriendly (ConstRisk::Medium),
              ConstRisk::High => ConstRisk::getFriendly (ConstRisk::High),
          ),
          'empty' => true,
          'label' => 'Riesgo',
      ));
    ?>
    <div style="padding-left: 210px;">
            <?php echo $form->input ('show_map', array ('label' => 'Mostrar Mapa')); ?>
    </div>
		</fieldset>
		<fieldset class="form-block">
			<legend>Precio</legend>
			<div style="padding-left: 210px;">
				<p>
					Indicar como se muestra el precio de la oferta <br />
				</p>
				<select id="displayPrecio" name="DisplayPrecio">
					<option value="regular"
						<?php echo !$this->data['Deal']['hide_price'] && !$this->data['Deal']['only_price'] ? 'selected': ''; ?>>
						Normalmente</option>
					<option value="hidePrice"
						<?php echo  $this->data['Deal']['hide_price'] ? 'selected': ''; ?>>
						Ocultar el precio</option>
					<option value="onlyPrice"
						<?php echo  $this->data['Deal']['only_price'] ? 'selected': ''; ?>>
						Mostrar s&oacute;lo el precio</option>
				</select>
			</div>
			<div class="is_tourism deal_type_box" id="amount_iva_tourism"
				style="display: none;">  
        <?php
        echo $form->input ('amount_full_iva', array ('label' => 'Conceptos Mayoristas Gravados al 21%')); 
        echo $form->input ('amount_half_iva', array ('label' => 'Conceptos Mayoristas Gravados al 10,5%')); 
        echo $form->input ('amount_exempt', array ('label' => 'Conceptos Mayoristas Exentos')); 
        echo $form->input ('amount_resolution_3450', array ('label' => 'Resolución 3550')); 
        echo $form->input ('amount_retail', array ('label' => 'Conceptos Minoristas','readonly'=>true, 'class'=>'readonly'));
        ?>
    </div>
    <?php
      echo $form->input ('original_price', array ('label' => __l('Original price'), 'class' => 'js-round-price'));
      echo $form->input ('discounted_price', array ('label' => __l('Discounted price'), 'type' => 'text', 'class' => 'js-round-price'));
    ?>
    <div class="two-col-form clearfix">
      <?php
        echo $form->input ('discount_percentage', array ('label' => __l('Descuento Redondeado: (%)'), 'readonly' => 'readonly'));
        echo $form->input ('discount_amount', array ('label' => __l('Discount Amount'), 'readonly' => 'readonly','class' => 'js-round-price'));
        echo $form->input ('savings', array ('type' => 'text', 'label' => __l('Savings'), 'readonly' => 'readonly', 'class' => 'js-round-price'));
      ?>
    </div>
		</fieldset>
		<fieldset class="form-block">
			<legend><?php echo __l('Financiamiento'); ?></legend>
    <?php
      echo $form->input ('minimal_amount_financial', array (
        'label' => 'Monto de financiaci&oacute;n m&iacute;nimo',
        'class' => 'js-round-price',
        'info' => 'Indica el monto m&iacute;nimo financiable (cuotas) para esta oferta.'));
    ?>
    <div class="two-col-form clearfix"></div>
		</fieldset>
		<fieldset class="form-block">
			<legend>Im&aacute;genes</legend>
    
      <?php if (!$is_subdeal){ ?>
    <div class="required" id="multipleAttachmentBlock">
      <?php
        echo $form->input ('Attachment.filename', array ('type' => 'file', 'label' => __l('Product Image'), 'info' => 'Tama&ntilde;o sugerido 437x264 p&iacute;xeles. S&oacute;lo im&aacute;genes en formato jpg, jpeg, gif, png o bmp'));?>
</div>
			<div id="multipleAttachmentBlock">
<?php

echo $form->input ('AttachmentNewsLetter.filename', array ('type' => 'file', 'label' => __l('NewsLetter Image'), 'info' => 'Tama&ntilde;o sugerido 609x183 p&iacute;xeles. S&oacute;lo im&aacute;genes en formato jpg, jpeg, gif, png o bmp'));
?>
             </div>
			<div class="required" id="multipleAttachmentBlock">    <?php
echo $form->input ('AttachmentMultiple.1.filename', array ('type' => 'file', 'label' => __l('Imagenes adicionales'), 'info' => 'Tama&ntilde;o sugerido 437x264 p&iacute;xeles. S&oacute;lo im&aacute;genes en formato jpg, jpeg, gif, png o bmp'));

        echo $form->button ('Agregar otra imagen', array ('class' => 'js-add-other-images', 'id' => 'add_image_button'));

      ?>
        </div>
      <?php }// (!$is_subdeal): ?>

    <?php
      if (!$is_subdeal) {
        echo $form->input ('description', array ('label' => 'Descripci&oacute;n - [ M&aacute;s Informaci&oacute;n ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('review',      array ('label' => 'Revisi&oacute;n', 'type' => 'textarea', 'class' => 'js-editor'));
      } else {
        echo $form->input ('description', array ('label' => 'Descripci&oacute;n - [ M&aacute;s Informaci&oacute;n ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('review',      array ('label' => 'Revisi&oacute;n', 'type' => 'textarea', 'class' => 'js-editor'));
      }
    ?>
  </fieldset>
		<fieldset class="form-block">
			<legend>Cup&oacute;n</legend>
    <?php // if (!$is_subdeal) { ?>
      <!--
        <div class = "input clearfix required date-time-block">
          <div class = "js-datetime">
            <?php // echo $form->input ('coupon_expiry_date', array ('label' => __l('Coupon Expiry Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
          </div>
        </div>
      -->
    <?php // } else { ?>
      <!--
        <div class = "input clearfix required">
          <div>
            <?php // echo $form->input ('coupon_expiry_date', array ('label' => __l('Coupon Expiry Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
          </div>
        </div>
      -->
    <?php // } ?>

    <script type="text/javascript">
        //revision inicial (para unificar con la vista de edicion)
        $(document).ready(checkCompanyPaymentMethods);
        //Listener del select de empresas,
        $('#DealCompanyId').change(checkCompanyPaymentMethods);
        checkFieldsCommission();
    </script>

    <?php

      //echo $form->input ('closure_modality', array('label' => 'Modalidad de liquidacion','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Redimido', '0' => 'Vendido')));
      //echo $form->input ('deferred_payment', array ('info' => 'Indica cuando se debera realizar el primer pago de la oferta',   'label' => 'Plazos de pago'));
      //echo $form->input ('first_closure_percentage', array ('info' => 'Porcentaje a pagar al comercio en la primera fecha de pago',   'label' => 'Porcentaje de 1era liquidacion'));
      echo $form->input ('payment_method', array('label' => 'Medio de pago','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Cuenta', '0' => 'Cheque')));
      echo '<div id="medios_pago_alerta" ></div>'; //para feedback de eleccion de empresa

      if (!$is_subdeal) {
        echo $form->input ('coupon_condition',  array ('label' => 'Condiciones - [ Condici&oacute;n de descuento ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('coupon_highlights', array ('label' => 'Destacados - [ Aspectos destacados ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('comment',           array ('label' => 'Comentarios', 'type' => 'textarea', 'class' => 'js-editor'));
      } else {
        echo $form->input ('coupon_condition',  array ('label' => 'Condiciones - [ Condici&oacute;n de descuento ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('coupon_highlights', array ('label' => 'Destacados - [ Aspectos destacados ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('comment',           array ('label' => 'Comentarios', 'type' => 'textarea', 'class' => 'js-editor'));
      }
    ?>
  </fieldset>
		<fieldset class="form-block">
			<legend><?php echo __l('SEO'); ?></legend>
    <?php
      echo $form->input ('meta_keywords', array ('label' => __l('Meta Keywords')));
      echo $form->input ('meta_description', array ('label' => __l('Meta Description')));

    ?>
  </fieldset>
		<fieldset class="form-block">
			<legend><?php echo 'Previsualizar'; ?></legend>
			<script language="JavaScript">
      
      /**
      *@var  hpoco is an abreviation for "has payment options changed once?"
      */
      var hpoco = false;
      var payment_setting_for_products =<?php echo  json_encode(Configure::read('deal.paymentOptions')).';'?>;
      var payment_setting_for_tourism = <?php echo  json_encode(Configure::read('deal.paymentOptionsTurism')).';'?>;
      var not_exlusive_payment_value_for_products = <?php echo json_encode(Configure::read('deal.not_exclusive_paymentOptions'))?>;
      $(document).ready (function () {
          
          $('#DealIsWholesaler').bind('click', function() {
               validate_tourism_enduser();
               toggle_amount_iva();
          }); 
    
          $('input[type=radio][name="data[Deal][portal]"]').bind('click', function(){
              $('#DealIsWholesaler').attr('disabled', 1);
              
              var toDisplay = [];
              var payment_value_for_products;
              var payment_value_for_tourism;
              payment_setting_for_products = <?php echo  json_encode(Configure::read('deal.paymentOptions')).';'?>;
              
              if($(this).val()=='is_tourism'){
                  payment_setting_for_products = <?php echo  json_encode(Configure::read('deal.paymentOptionsTurism')).';'?>;
              }
              payment_value_for_products = $('#DealPaymentSettings').find("option").filter(function() {
                  return $.inArray($(this).text(), payment_setting_for_products)!=-1;
              }).attr("value");

  		        payment_value_for_tourism = $('#DealPaymentSettings').find("option").filter(function() {
                   return $.inArray($(this).text(), payment_setting_for_tourism)!=-1;
              }).attr("value");

              if(hpoco){
                $('input[type=checkbox][name="paymentOptionValue[]"]').removeAttr('checked');
              }
              hpoco = true;
              $('.payment-setting-group-selector.exclusive').hide();

              if($(this).val()=='is_tourism'){
                  $('#DealIsWholesaler').attr('disabled', 0);
                  $('#DealIsTourism').val(1);
                  toggle_amount_iva();
                  $('#DealPaymentSettings').find("option").filter(function() {
                      if ($.inArray($(this).text(), payment_setting_for_tourism)!=-1){
                        toDisplay.push($(this).val()); //medios de pagos a mostrar
                        $(this).show();
                      } else {
                        $(this).attr('disabled', 1); //oculta el resto de medios de pagos
                        $('.js-payment-setting-group-' + $(this).val()).hide();
                        //Desmarcamos los que no sean de turismo
                        $('.js-payment-setting-group-' + $(this).val()).find('input:checkbox[name*=paymentOptionValue]').attr('checked', false);
                      }
                      return ($.inArray($(this).text(), payment_setting_for_tourism)!=-1);
                  }).attr("value");
                  
                  //habilita turismo
                  $.each(toDisplay, function (index, value){
                    groupContainer = $('.js-payment-setting-group-' + value);
                    groupContainer.show();
                    $('#DealPaymentSettings').find('option[value='+value+']').removeAttr('disabled');
                    $('#DealPaymentSettings').find('option[value='+value+']').removeAttr('disabled');
                    $('#DealPaymentSettings').val(value);
                    $('#DealPaymentSettings').find('option[value='+value+']').attr('class', 'notDisable');
                  });
                  //$('.payment-setting-group-selector.not_exclusive').show();
                  /*$('#DealPaymentSettings').find("option").filter(function() {
                        return      $.inArray($(this).text(), not_exlusive_payment_value_for_products)!=-1;
                      }).each(function(){
                            $(this).removeAttr('disabled');
                      });*/
                  return;
              }
              else{
                  
                  $('#DealIsTourism').val('');
                  if($(this).val()=='is_product'){
                      toggle_amount_iva();
                     
                      //show and disable at select
                      $('#DealPaymentSettings').find("option").filter(function() {
                          
                          //Deshabilitamos del select de medios de pago, las opciones que no corresponden al tipo de oferta (normal, turismo, productos)
                          if ($.inArray($(this).text(), payment_setting_for_products)!=-1){
                            toDisplay.push($(this).val()); //medios de pagos a mostrar
                            $(this).show();
                          } else {
                            $(this).attr('disabled', 1); //oculta el resto de medios de pagos
                            $('.js-payment-setting-group-' + $(this).val()).hide();
                            $('.js-payment-setting-group-' + $(this).val()).find('input:checkbox[name*=paymentOptionValue]').attr('checked', false);
                          }
                          
                      return ($.inArray($(this).text(), payment_setting_for_products)!=-1);
                      });
                      $.each(toDisplay, function (index, value){
                          var groupContainer = $('.js-payment-setting-group-' + value);
                          groupContainer.show();
                          $('#DealPaymentSettings').find('option[value='+value+']').removeAttr('disabled');
                          $('#DealPaymentSettings').val(value);
                      });
                        $('.payment-setting-group-selector.not_exclusive').show();
                        //$('#DealPaymentSettings').find('option[value='+payment_value_for_products+']').siblings().attr('disabled',1);
                        $('#DealPaymentSettings').find("option").filter(function() {
                            return      $.inArray($(this).text(), not_exlusive_payment_value_for_products)!=-1;
                        }).each(function(){
                            $(this).removeAttr('disabled');
                        });
                     return ;

                  }
                  toggle_amount_iva();

	            }
              $('.payment-setting-group-selector').show();
              $('#DealPaymentSettings').val(0);

              
              var allSpecialPayment = 
                      $.merge(payment_setting_for_products, 
                      payment_setting_for_tourism);


            	var excludeFromMerge=<?php echo  json_encode(Configure::read('deal.paymentOptionForNormal')).';'?>;
            	   
            	 
            	
              	
              
              if($(this).val()=='is_normal'){

            	  allSpecialPayment = $.grep(allSpecialPayment, function( n, i ) {
                  	  return $.inArray( n, excludeFromMerge ) < 0 ;
                  	});
                  
                  $('#DealPaymentSettings').find("option").filter(function() {
                      if ($.inArray($(this).text(), allSpecialPayment)!=-1 ) {
                          $(this).attr('disabled', 1);
                          $(this).hide();
                          $('.js-payment-setting-group-' + $(this).val()).find('input:checkbox[name*=paymentOptionValue]').attr('checked', false);
                          $('.js-payment-setting-group-' + $(this).val()).hide()
                      } else {
                          $(this).removeAttr('disabled');
                          $(this).show();
                      }
                      return ;
                  });
                  
              }
            
          });
          $('input[type=radio][name="data[Deal][portal]"]:checked').trigger("click");


          $("#previsualizar").click (function () {
  	       	
          	data=$('#DealAddForm').serialize();
          	popupWelcome ('/deals/email_preview/', 645, 600,'Cerrar','post',data);
          	return false;
          });
        <?php if ($is_subdeal) { ?>
          //changeCommission();
          $("#DealAgreedPercentage").attr ("disabled", true);
          $("#DealName").attr ("disabled", true);
          $("#DealCompanyId").attr ("disabled", true);
          $("#DealCityId").attr ("disabled", true);
          $("#DealSellerId").attr ("disabled", true);
          $("#DealClusterId").attr ("disabled", true);
          $("#DealStartDate").attr ("disabled", true);
          $("#DealEndDate").attr ("disabled", true);
          $("#DealCouponStartDate").attr ("disabled", true);
          $("#DealCouponExpiryDate").attr ("disabled", true);
          $("#DealIsSideDeal").attr ("disabled", true);
          $("#DealIsSideDeal_").attr ("disabled", true);
          $("#DealIsTourism").attr ("disabled", true);
          $("#DealIsTourism_").attr ("disabled", true);
          $("#DealPaymentSettings").attr ("disabled", true);

          $('.payment-setting-group-selector').hide();/*oculto todos los divs contenedores de tipos de pago*/
          
          $('.payment-setting-group-selector').find('input[type=checkbox][name^=paymentOptionValue]:checked').hide();/*oculto el check de los elementos checkeds*/
          $('.payment-setting-group-selector').find('input[type=checkbox][name^=paymentOptionValue]:checked').closest('p').parent().show();/*muestro el div contenedor del grupo que tiene elementos chekeds*/
          $('.payment-setting-group-selector').find('input[type=checkbox][name^=paymentOptionValue]:not(:checked)').closest('p').hide();/*oculto todo el elemento no checked*/

          $('.payment-setting-group-selector p').find('select[name*=PaymentPlanId]').find('option').not(':selected').attr("disabled", "disabled");
          $('#DealPaymentSettings').parent().hide();/*oculta el combo de los tipos de pago a mostrar */
          $('.actions-payment').hide(); /*oculta las acciones de los tipos de pago : Seleccionar todos | Ninguno*/

         
          
           $("#DealIsEndUser").addClass('readonly');
          $("#DealIsEndUser_").attr ("readonly", true);
          $("#DealCustomSubject").attr ("disabled", true);
          $("#DealCustomCompanyName").attr ("disabled", true);
          $("#DealCustomCompanyContactPhone").attr ("disabled", true);
          $("#DealCustomCompanyAddress1").attr ("disabled", true);
          $("#DealCustomCompanyAddress2").attr ("disabled", true);
          $("#DealCustomCompanyCity").attr ("disabled", true);
          $("#DealCustomCompanyState").attr ("disabled", true);
          $("#DealCustomCompanyCountry").attr ("disabled", true);
          $("#DealCustomCompanyZip").attr ("disabled", true);
          $("#DealPriority").attr ("disabled", true);
          $("#DealRisk").attr ("disabled", true);
          $("#DealShowMap").attr ("disabled", true);
          $("#DealShowMap_").attr ("disabled", true);
          $("#DealHidePrice").attr ("disabled", true);
          $("#DealHidePrice_").attr ("disabled", true);
          $("#DealOnlyPrice").attr ("disabled", true);
          $("#DealOnlyPrice_").attr ("disabled", true);
          $("#add_image_button").attr ("disabled", true);
          $("#DealCouponExpiryDate").attr ("disabled", true);
          $("#DealMetaKeywords").attr ("disabled", true);
          $("#DealMetaDescription").attr ("disabled", true);
          $("#DealDealCategoryId").attr ("disabled", true);
          $("#DealCouponDuration").attr ("disabled", true);
          $("#DealIsDiscountMp").addClass('readonly');

          /* fix para readonly en checkbox */
          $(':checkbox.readonly').click(function(){ return false; });

          $("#multipleAttachmentBlock").attr ("hidden", true);
        <?php } ?>
          $(".deal_type_check").click(function () {
              $(".deal_type_check:not(#"+$(this).attr('id')+")").attr('checked', false);
          });
      });
    </script>
    <?php echo $html->link ('Previsualizar', '#', array ('class' => 'link-button-generic', 'id' => 'previsualizar')); ?>
  </fieldset>
		<fieldset>

			<div class="submit-block clearfix">
            <?php
            echo $form->submit (__l('Add'), array ('id' => 'toupcomming'));
            echo $form->submit (__l('Save as Draft'), array ('name' => 'data[Deal][save_as_draft]'));
            ?>
        </div>

			<small class="info">
            <?php echo __l('Save this deal as a draft and view the preview of the deal. You can make changes untill you send it to upcoming status. Use the update button in edit page to send it to upcoming status.'); ?>
        </small>

		</fieldset>
  <?php echo $form->end (); ?>

</fieldset>
	<script type="text/javascript">

            var  webChannelDeal =  function () {
                  $('#DealOriginalPrice').parent().show();
                  $('#DealDiscountAmount').parent().show();
                  $('#DealSavings').parent().show();

                  $('#DealDiscountPercentage').attr('readonly', 'readonly');
             };

          var  smsChannelDeal = function () {
                  $('#DealOriginalPrice').parent().hide();
                  $('#DealDiscountAmount').parent().hide();
                  $('#DealSavings').parent().hide();
                  $('#DealOriginalPrice').val('0');
                  $('#DealDiscountAmount').val('0');
                  $('#DealSavings').val('0');

                  $('#DealDiscountPercentage').removeAttr('readonly');
              };

            var dealPublicationChannelTypeIdChange =  function (event, val) {
              var selectId = '';
              if(val == null) {
                if(event == null) {
                  selectId = '#DealPublicationChannelTypeId';
                } else {
                  selectId = event.target.id;
                }
                val = $('#' + selectId + ' option:selected').val();
              }

              if(val == '1') { // Is WEB
                webChannelDeal();
              } else if(val == '2') {  // IS SMS
                smsChannelDeal();
              }
            };

            $('#DealPublicationChannelTypeId').change( function (event) { dealPublicationChannelTypeIdChange(event); } );

            $('#DealDiscountedPrice').blur( function (event) {
              if($('#DealPublicationChannelTypeId option:selected').val() == '2') {
                $('#DealOriginalPrice').val($('#' + event.target.id).val());
              }
            });

            $(document).ready(function() { dealPublicationChannelTypeIdChange();


    });
</script>

	<script type="text/javascript">//<![CDATA[
   
  $('#DealCompanyId').change (function () {
    var val = $('#DealCompanyId option:selected').attr ('value');
   var hasPins = {};
   
    var usesSecondaryPortal = {};
    var isEndUser = new Array();
    <?php
    
      foreach ($companies_is_tourism as $id => $is_tourism) {
        echo 'usesSecondaryPortal [' . $id . '] = ' . ((int) $is_tourism) . ';' . "\n";
      }
      foreach ($companies_is_end_user as $id => $is_end_user) {
        echo 'isEndUser [' . $id . '] = ' . ((int) $is_end_user) . ';' . "\n";
      }
    ?>

    if (val) {
    
      
      $('#DealIsTourism').attr ('checked', usesSecondaryPortal [val]);

      $('#DealIsEndUser').attr ('checked', isEndUser [val]);


    }
  });
  
  //ocultamos los selects de campaña y producto
  
  <?php if ($this->validationErrors ) { ?>
    companyId = $('#DealCompanyId').val();    
    getAndPopulateCompanyCampaigns(companyId);
  <?php } ?>
      

  
  var pluginFileInputCounter = 1;
  var pluginFileInputLimit = <?php echo Configure::read ('deal.additionalImages.limit'); ?>;

//]]></script>

<?php
    //SCRIPTS DE CONTROL DE CARGA DE CAMPAÑAS Y PRODUCTOS
    //INDEPENDIENTES AL USARSE EN LA CARGA Y LA EDICION DE OFERTAS
    echo $this->element(Configure::read('theme.theme_name') . 'deal_ABM_stock_strategy_js_functions' );
?>

</div>
