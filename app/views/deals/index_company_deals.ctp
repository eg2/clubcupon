<style>
    /*Excepciones*/
    #main{margin-left:-115px;}
    #main2, #main_container{width:1170px!important;}
    .menu-submenu, .paging-links-wrapper, .table_container{width:1152px!important;}
    .table_container{width:1140px!important;}
    table{width:1130px!important;}
    td, td a, th, table a{color:#777!important;}
    .table_container{ border:1px solid #DDDDDD; padding:5px;}
    th{border-bottom: none; border-right: 1px solid #DDD;}
    .firstrow{height:5px;line-height:0; padding:0!important;margin:0!important;}
    .last{border:none!important;}
    .paging-links-wrapper{border:none!important;}
    td, td a{font-size: 10px!important;}
    #ccnow #tabs{margin:0; width:1100px;}
    #ccnow #tabs li{padding:10px 10px;}
    
    .selected{    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 white;
    border-color: #CCCCCC;
    border-image: none;
    border-radius: 5px 5px 0 0;
    border-style: solid;
    border-width: 1px 1px 0;
    color: #000000;
    font-weight: bold;
    padding: 10px 10px!important;
    position: relative;
    top: 1px;}
    .selected:hover{
        background:#fff!important;
    }
    td{text-align: center;}
</style>

<div id="main2" class="container_16 main_frt vista_ofertas_empresa">
<?php
    echo $this->element('dashboard/company_dashboard_menu_tabs');
?>
    <div class="grid_16 pushBelow curveBlock" style="width:1140px; background: #fff;">
        <div class="bloque-home clearfix ofertas_perfil_empresa">

            <!-- Contenidos -->

            <?php $this->params ['named']['filter_id'] = isset ($this->params ['named']['filter_id']) ? $this->params ['named']['filter_id'] : ''; ?>

            <!-- class = "js-tabs">-->
            <div class="mc20px">
                <ul id="tabs">
                    <?php

                        $activeTabClass = '';
                        if($this->params ['named']['filter_id'] == ConstDealStatus::Open)
                        {
                            $activeTabClass = 'selected';
                            echo '<li class="'.$activeTabClass.'">Abierta</li>';
                        }
                        else
                        {
                            echo '<li>' . $html->link (sprintf ('Abierta', $dealStatusesCount [ConstDealStatus::Open]), array ('filter_id' => ConstDealStatus::Open, 'company' => $company_slug), array ('style' => $activeTabClass, 'title' => 'Abierta')) . '</li>';                            
                        }
                        $all = $dealStatusesCount [ConstDealStatus::Open];

                        foreach ($dealStatuses as $id => $dealStatus)
                        {
                            if (!in_array ($id, array (ConstDealStatus::Open,ConstDealStatus::Rejected)))
                            {
								$stat_name = array_shift(explode(" ",__l($dealStatus)));
								if(strlen($stat_name)<5){
									$stat_name = __l($dealStatus);
								}
                                $activeTabClass = '';
                                if($id == $this->params ['named']['filter_id']){
                                    $activeTabClass = 'selected';
                                    echo '<li class="'.$activeTabClass.'">' . $stat_name . '</li>';
                                }
                                else
                                {    
                                    echo '<li>' . $html->link ($stat_name . '', array ( 'filter_id' => $id, 'company' => $company_slug), array ('class' => $activeTabClass, 'title' => __l($dealStatus), 'rel'=>$dealStatusesCount[$id])) . '</li>';
                                }
                                $all += $dealStatusesCount [$id];
                            }
                        }
                        $activeTabClass = '';
                        if(empty($this->params ['named']['filter_id'])){
                            $activeTabClass = 'selected';
                            echo '<li class="'.$activeTabClass.'">' . __l('All')  . '</li>';
                        }else{    
							echo '<li>' . $html->link(__l('All'), array( 'type' => 'all', 'company' => $company_slug), array('title' => __l('All'), 'class' => $activeTabClass)) . '</li>';
                        }
                    ?>
                </ul>
            </div>

    <?php

            if (!empty ($this->params ['named']['filter_id']) && (!empty ($dealStatusesCount [$this->params ['named']['filter_id']])))
            {
                $id = $this->params ['named']['filter_id'];
            }
            else if (!empty ($this->params ['named']['type']) && ($this->params ['named']['type'] == 'all'))
            {
                $id = $this->params['named']['type'];
            }
    ?>

    <div class="table_container">
        <style type="text/css">


        </style>
        <?php echo $this->element ('paging_counter'); ?>
        <table>
            <tr>
                <?php if (!empty   ($this->params ['named']['filter_id']) && in_array ($this->params ['named']['filter_id'], array ( ConstDealStatus::Upcoming, ConstDealStatus::Rejected, ConstDealStatus::Canceled, ConstDealStatus::Draft)))
                {
                ?>

                    <th><?php echo $paginator->sort (                'ID Padre' , 'Deal.parent_deal_id'  ); ?></th>
                    <th><?php echo $paginator->sort (            'Nombre Oferta', 'Deal.name'            ); ?></th>
                    <th>Suboferta  </div></th>
                    <th>Imagen     </div></th>
                    <th><?php echo $paginator->sort ( 'Precio original ($)'     , 'Deal.original_price'  ); ?></th>
                    <th><?php echo $paginator->sort ( 'Precio con descuento ($)', 'Deal.discounted_price'); ?></th>
                    <th>Desde</th>
                    <th>Hasta</th>
                <?php
                }
                else
                {
                ?>
                    <?php if (empty ($this->params ['named']['filter_id'])|| $this->params ['named']['filter_id'] == ConstDealStatus::Tipped || $this->params ['named']['filter_id'] == ConstDealStatus::Closed){ ?>
                        <th rowspan = "2" style="width:80px;" >Acciones</th>
                    <?php } ?>
                    <th rowspan = "2" >Suboferta</th>
                    <th rowspan = "2" ><?php echo $paginator->sort ('Oferta', 'Deal.name'); ?></th>

                    <?php if (!empty ($this->params ['named']['type']) && ($this->params ['named']['type'] == 'all')) { ?>
                        <th rowspan = "2" ><?php echo $paginator->sort ('Estado', 'DealStatus.name'); ?></th>
                    <?php } ?>

                    <th rowspan = "2" ><?php echo $paginator->sort (      'Precio original ($)', 'Deal.original_price'  ); ?></th>
                    <th rowspan = "2" ><?php echo $paginator->sort ( 'Precio con descuento ($)', 'Deal.discounted_price'); ?></th>
                    <th colspan = "2" >Cantidad</th>
                    <th colspan = "2" >Cantidad ($)</th>

                    <?php if (!empty ($this->params ['named']['filter_id']) || !empty($this->params ['named']['type'])) { ?>
                        <th rowspan = "2" ><?php echo $paginator->sort (       'Comisión', 'Deal.commission_percentage') . ' (%)'                                        ; ?></th>
                        <!--<th rowspan = "2" ><?php echo $paginator->sort (     'Costo fijo', 'Deal.bonus_amount'         ) . ' (' . Configure::read ('site.currency') . ')'; ?></th>-->
                        <th rowspan = "2" ><?php echo $paginator->sort ( 'Monto comisión', 'total_commission_amount'   ) . ' (' . Configure::read ('site.currency') . ')'; ?></th>
                    <?php }

                    if (!empty ($this->params ['named']['filter_id']) && in_array ($this->params ['named']['filter_id'], array ( ConstDealStatus::Open, ConstDealStatus::Closed, ConstDealStatus::PaidToCompany, ConstDealStatus::Tipped, ConstDealStatus::PendingApproval)))
                    {
                    ?>
                        <th rowspan = "2" ><?php echo $paginator->sort ('Cantidad Vendida', 'Deal.deal_user_count'); ?></th>
                    <?php
                    }
                    ?>
                    <th colspan = "2" style="width:80px;" class="last" >Vigencia</th>
                </tr>
                <tr>
                    <th>Objetivo</th>
                    <th>Meta alcanzada</th>
                    <th>Objetivo</th>
                    <th>Meta alcanzada</th>
                    <th>Desde</th>
                    <th class="last">Hasta</th>
                </tr>
                <tr><td class ="firstrow"></td></tr>
            <?php
            }
            ?>

            <?php
                if (!empty ($deals))
                {
                    $i = 0;
                    foreach ($deals as $deal)
                    {
                        
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' bg-linea';
                        }
                        
						if(!isset($deal['Deal'])){
							$deal['Deal'] = $deal['NowDeal'];
						}
					
                        $is_subdeal    = $deal ['Deal']['parent_deal_id'] != $deal ['Deal']['id'];
                        $nombre_oferta = $clubcupon->shorten_strings_with_html_entities($deal ['Deal']['name'], 25);
                        
                        if (!empty ($this->params ['named']['filter_id']) && in_array ($this->params['named']['filter_id'], array ( ConstDealStatus::Upcoming, ConstDealStatus::Rejected, ConstDealStatus::Canceled, ConstDealStatus::Draft)))
                        {
            ?>
                        <tr class="<?php echo $class; ?>">

                            <!-- ID Padre -->
                            <td><?php echo $html->cInt ($deal ['Deal']['parent_deal_id']); ?></td>

                            <!-- Nombre Deal-->
                            <?php if (empty ($this->params ['named']['filter_id'])){ ?>
                            <td>
                            <?php } ?>
                                <!-- Menu opciones (hover)-->
                                <?php
                                    if (!empty ($this->params ['named']['filter_id']) && $this->params ['named']['filter_id'] == ConstDealStatus::Draft)
                                    {
                                ?>
                                    <div class = "actions-block">
                                        <div class = "actions round-5-left">
                                            <span><?php echo $html->link ('Editar', array( 'action' => 'edit', $deal ['Deal']['id']),        array ('class' => 'edit js-edit', 'title' => 'Editar')); ?></span>
                                            <span><?php echo $html->link ('Guardar',array( 'action' => 'update_deal', $deal ['Deal']['id']), array ('class' => 'add js-delete','title' => 'Guardar y enviar para aprobacion del administrador')); ?></span>
                                        </div>
                                    </div>
                                <?php
                                    }
                                ?>
                                <!-- FIN Menu opciones (hover)-->
                                
                                <?php
                                
                                    if($is_subdeal)
                                    {
                                        echo $nombre_oferta;
                                    }
                                    else
                                    {
                                        echo $html->link ($nombre_oferta, array ( 'controller' => 'deals', 'action' => 'view', $deal ['Deal']['slug']), array ('title' => $deal ['Deal']['name']));
                                    }
                                
                                ?>
                                
                            <?php if (empty ($this->params ['named']['filter_id'])){ ?>
                            </td>
                            <?php } ?>

                            <!-- Subdeal -->
                            <td class = "dl"><?php echo $html->cBool ($is_subdeal); ?></td>

                            <!-- Imagen -->
                            <td>
                                <?php echo $html->showImage ('Deal', $deal ['Attachment'], array ('dimension' => 'medium_thumb', 'alt' => sprintf (__l('[Image: %s]'), $html->cText ($deal ['Company']['name'] . ' ' . $deal ['Deal']['name'], false)), 'title' => $html->cText ($deal ['Company']['name'] . ' ' . $deal ['Deal']['name'], false))); ?>
                            </td>

                            <!-- Precio original -->
                            <td class = "dr"><?php echo $html->cCurrency ($deal ['Deal']['original_price'  ]); ?></td>

                            <!-- Precio con descuento -->
                            <td class = "dr"><?php echo $html->cCurrency ($deal ['Deal']['discounted_price']); ?></td>
                            
                            <td><?php echo $deal['Deal']['start_date']; ?></td>
                            <td><?php echo $deal['Deal']['end_date']; ?></td>

                        </tr>
                    <?php
                        }
                        else
                        {
                    ?>
                        <tr class="<?php echo $class; ?>">
                            <td>
                                <?php if (!empty ($this->params ['named']['filter_id']) && in_array ($this->params ['named']['filter_id'], array ( ConstDealStatus::Tipped, ConstDealStatus::Closed, ConstDealStatus::PaidToCompany, ConstDealStatus::PendingApproval))) { ?>
                                    <div class = "actions-block">
                                        <div class = "actions round-5-left">
                                            <span><?php echo $html->link ($html->image('icon-export.png', array('alt' => 'CSV de los cupones')),       array ('action' => 'coupons_export',    'filter_id' => $id,                                  'deal_id' => $deal ['Deal']['id'],                             'ext'       => 'csv'  ), array('escape' => false,'title' => 'CSV de los cupones')); ?></span>
                                            <span><?php echo $html->link ($html->image('print.png',       array('alt' => 'Impresion de los cupones')), array ('action' => 'deals_print',       'filter_id' => $this->params ['named']['filter_id'], 'deal_id' => $deal ['Deal']['id'], 'company' => $company_slug, 'page_type' => 'print'), array('escape' => false,'title' => 'Impresion de los cupones', 'target' => '_blank')); ?></span>
                                            <span><?php echo $html->link ($html->image('view.png',        array('alt' => 'Ver los cupones')),          array ('controller' => 'deal_users', 'action' => 'index_for_company', 'filter_id' => $this->params ['named']['filter_id'], 'deal_id' => $deal ['Deal']['id'], 'company' => $company_slug                        ), array('escape' => false,'title' => 'Ver los cupones')); ?></span>
                                        </div>
                                    </div>
                    <?php
                        }
                    ?>
                        </td>

                        <!-- Subdeal -->
                        <td class = "dl"><?php echo $html->cBool ($is_subdeal); ?></td>

                        <!-- Imagen -->
                        <td align="center">
                            <?php echo $html->showImage ('Deal', $deal ['Attachment'], array ('dimension' => 'medium_thumb', 'alt' => sprintf (__l('[Image: %s]'), $html->cText ($deal ['Company']['name'] . ' ' . $deal ['Deal']['name'], false)), 'title' => $html->cText ($deal ['Company']['name'] . ' ' . $deal ['Deal']['name'], false))); ?>
                            <br />
                            <?php
                            
                                if($is_subdeal)
                                {
                                    echo $nombre_oferta;
                                }
                                else
                                {
                                    echo $html->link ($nombre_oferta, array ( 'controller' => 'deals', 'action' => 'view', $deal ['Deal']['slug']), array ('title' => $deal ['Deal']['name'], 'escape'=>false));
                                }
                                
                            ?>
                        </td>
                        
                        <!-- Estado -->
                        <?php
                            if (!empty ($this->params ['named']['type']) && ($this->params ['named']['type'] == 'all'))
                            {
                        ?>
                            <td><?php echo $html->cText (__l($deal ['DealStatus']['name']), false); ?></td>
                        <?php
                            }
                        ?>
                        <!-- Precio original -->
                        <td><?php echo $html->cCurrency ($deal ['Deal']['original_price']  ); ?></td>

                        <!-- Precio descuento-->
                        <td><?php echo $html->cCurrency ($deal ['Deal']['discounted_price']); ?></td>

                        <!-- Cantidad minima-->
                        <td><?php echo $html->cInt ($deal['Deal']['min_limit']      ); ?></td>

                        <!-- Cantidad -->
                        <td><?php echo $html->cInt ($deal[0]['sold_coupons']); ?></td>

                        <!-- Precio descuento -->
                        <td><?php echo $html->cCurrency (Precision::mul ($deal ['Deal']['discounted_price'], $deal ['Deal']['min_limit']      )); ?></td>

                        <!-- vendido ($) -->
                        <td><?php echo $html->cCurrency (Precision::mul ($deal ['Deal']['discounted_price'], $deal ['Deal']['deal_user_count'])); ?></td>


                        <?php
                            if (!empty ($this->params ['named']['filter_id']) || !empty ($this->params ['named']['type']))
                            {
                        ?>
                                <!-- % comision -->
                                <td><?php echo $html->cFloat ($deal ['Deal']['commission_percentage']); ?></td>

                                <!-- -->
                                <!--<td><?php echo $html->cCurrency ($deal ['Deal']['bonus_amount']           ); ?></td>-->

                                <!--  -->
                                <td><?php echo $html->cCurrency ($deal ['Deal']['total_commission_amount']); ?></td>
                        <?php
                            }

                            if (!empty ($this->params ['named']['filter_id']) && in_array ($this->params ['named']['filter_id'], array ( ConstDealStatus::Open, ConstDealStatus::Closed, ConstDealStatus::PaidToCompany, ConstDealStatus::Tipped, ConstDealStatus::PendingApproval)))
                            {
                        ?>
                            <td><?php echo $html->cInt ($deal ['Deal']['deal_user_count'], false); ?></td>
                        <?php
                            }
                        ?>
                            <td><?php echo date("d-m-Y h:i", strtotime($deal['Deal']['start_date'] )); ?></td>
                            <td><?php echo date("d-m-Y h:i", strtotime($deal['Deal']['end_date']   )); ?></td>
                    </tr>
            <?php
                        }
                    }
                }
                else
                {
            ?>
                <tr><td class = "notice"  colspan = "11">No hay ofertas disponibles</td></tr>
            <?php
                }
            ?>
        </table>
    </div>
     <div class="paging-links-wrapper" >
        <?php
                //Hack horrible hasta el revamp!
                $prev_link = str_replace('/all', '', $paginator->prev('Anterior'));
                $next_link = str_replace('/all', '', $paginator->next('Siguiente'));

                if(!empty ($prev_link))
                {
                    echo $prev_link;
                }
                //
                if(!empty ($next_link))
                {
                    echo $next_link;
                }
            ?>

    </div>

            <!-- / Contenidos -->

        </div>
    </div>
</div>

