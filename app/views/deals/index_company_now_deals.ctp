<?php
    echo $this->element('dashboard/company_dashboard_menu_tabs');
?>
<div class="grid_16 adds-suc oflist pushBelow">
	<div class="contenido-form" id="tool">
		<div class="contenedor-sub">
			<ul class="submenu">
				<li class="active">CLUB CUPON</li>
				<li><?php echo $html->link('CLUB CUPON YA!',array('plugin'=>'now','controller'=>'now_deals','action'=>'my_deals'));?></li>
			</ul>
		</div>
		<div class="encabezado">

		</div>
		<form>
		<div class="listasuc listofertas clearfix">
			<div class="selall clearfix">
				<select id="filter_select">
					<option value="0">Todas las Ofertas</option>
					<?php foreach($dealStatuses as $key=>$dealStatus){?>
                        <option value="<?php echo $key;?>" <?php if(@$filter_id == $key) echo 'selected';?>><?php echo __l($dealStatus);?></option>
					<?php }?>
				</select>
				<p>Filtrar ofertas por estado: </p>
			</div>
		<?php if(count($deals)){ ?>
			<div class="linea-item titlelist clearfix">
				<label class="lab6">Estado</label>
				<label class="lab1">Nombre de la Oferta</label>
				<label class="lab2">Categor&iacute;a</label>
				<label class="lab5">Cupones Vendidos</label>
				<label class="lab3">Precio Final</label>
				<label class="lab2">Validez</label>
                                <label class="lab4" style="text-align:right;">Acciones</label>
			</div>
			<?php foreach($deals as $key=>$deal){ ?>
			<?php //var_dump($deal);die(); ?>
			<div class="linea-item clearfix <?php if($key%2){echo "bg-linea";}?>">
				<label class="lab6"><?php echo ConstDealStatus::getFriendly($deal['Deal']['deal_status_id']);?></label>
				<label class="lab1"><?php echo $html->cText($deal['Deal']['name']);?></label>
				<label class="lab2"><?php echo $html->cText(($deal['DealCategory']['name'] != '' ? $deal['DealCategory']['name'] : 'Sin Asignar'));?></label>
				<label class="lab5"><?php echo empty($deal[0]['sold_coupons']) ? "0" : $html->cText($deal[0]['sold_coupons']);?></label>
				<label class="lab3"><?php echo $html->cText($deal['Deal']['discounted_price']);?></label>
				<label class="lab4"><?php echo $time->format("d-m-Y",$deal['Deal']['start_date']);?> al <br /><?php echo $time->format("d-m-Y",$deal['Deal']['end_date']);?></label>
                                <label class="label5" style="float:right">
                                    <div class = "actions-block">
                                        <div class = "actions round-5-left">
                                            <span><?php echo $html->link ($html->image('icon-export.png', array('alt' => 'CSV de los cupones')),       array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'download_my_cupons',    'filter_id' => $deal['Deal']['id'],                         ), array('escape' => false,'title' => 'CSV de los cupones')); ?></span>
                                            <!--<span><?php echo $html->link ($html->image('print.png',       array('alt' => 'Impresion de los cupones')),array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'my_cupons',    'filter_id' => $deal['Deal']['id'],'print'=>1                         ), array('escape' => false,'title' => 'Impresion de los cupones', 'target' => '_blank')); ?></span>-->
                                           <span><?php echo $html->link ($html->image('view.png',        array('alt' => 'Ver los cupones')),array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'my_cupons',    'filter_id' => $deal['Deal']['id'],                         ), array('escape' => false,'title' => 'Ver los cupones')); ?></span>
                                        </div>
                                    </div>
                                </label>
			</div>
			<?php }?>
		<?php }else{?>
			<div class="linea-item titlelist clearfix">
				<center>
                    <p style="font: 16px arial;">
                        Actualmente no ten&eacute;s Ofertas <?php if($filter_id!=0){?>en estado: <b><?php echo __l(ConstDealStatus::getFriendly($filter_id));?></b><?php }?>
                    </p>
                </center>
			</div>
		<?php }?>
		</div>
		</form>

<?php if(count($deals) && $paginator->hasPage(null,2)){ ?>
        <script>
            $(document).ready(function() {

                $('#countLinkWrapper').css("margin",'0 auto');

                //Para forzar el centrado del paginador
                anchoPaginador = 0;
                $('.pagborder span').each(function(){
                        anchoPaginador += $(this).outerWidth()+5;
                });
                $('.pagborder a').each(function(){
                        anchoPaginador += $(this).outerWidth();
                });

                $('#countLinkWrapper').css("width",anchoPaginador);
                $('#countLink p').css("width",anchoPaginador);


            });
        </script>
		<div class="dealspaginador clearfix" id="countLinkWrapper">
		<div class="pagborder" id="countLink">
			<?php


            $paginator->options(array(
                'url' => array_merge(array(
                    'controller' => $this->params['controller'],
                    'action' => $this->params['action'],
                ) , $this->params['pass'], $this->params['named'])
            ));

            //Hack horrible hasta el revamp!
            $prev_link      = str_replace('/all', '', $paginator->prev('Anterior' , array('class' => 'prev','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'prev')));
            $numbers_link   = str_replace('/all', '', $paginator->numbers(array('modulus' => 2,'skip' => '<span class="skip">&hellip;.</span>','separator' => " \n",'before' => null,'after' => null,'escape' => false)));
            $next_link      = str_replace('/all', '', $paginator->next('Próximo', array('class' => 'next','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'next')));

            if(!empty ($prev_link))
            {
                echo $prev_link."\n";
            }
            //
            if(!empty ($numbers_link))
            {
                echo $numbers_link."\n";
            }
            if(!empty ($next_link))
            {
                echo $next_link."\n";
            }

			echo $paginator->counter(array(
				'format' => '<p><span>%start%-%end%</span> de %count% Ofertas</p>'
			));
			?>
		</div>
		</div>
<?php }?>
	</div>
</div>

<script>
$(document).ready(function(){
	//$('#filter_select').unbind('change');
	$('#filter_select').change(function(e){

        //http://trunk/deals/filter_id:1/company:matecito04/city:ciudad-de-buenos-aires

        selectedIndex = this.options[this.selectedIndex].value;
        window.location.href = 'http://<?php echo$_SERVER['HTTP_HOST']; ?>/deals/filter_id:'+selectedIndex+'/company:<?php echo $company_slug; ?>/city:<?php echo $city_slug; ?>';



	});

	$('a.no-edit').click(function(e){
		e.preventDefault();
		alert('Solo se pueden editar la ofertas que esten en estado "Borrador"');
	});

	$('a.ajax').colorbox({
		height:580,
		width:815,
		onLoad:function(){
			$('#cboxClose').remove();
		}
	});
});
</script>