<div id="main" class="container_16 main_frt">

    <!-- Contenidos -->
    <div class="grid_16 fmt-bg">

        <h2 class="fmt-h2">Tu compra</h2>

        <?php
            $current_user_details = array('username' => $auth->user('username'), 'user_type_id' => $auth->user('user_type_id'), 'id' => $auth->user('id'));
            $cantidad        = $html->cCurrency ($dealExternal['DealExternal']['quantity'] , 'small');
            $precio          = $html->cCurrency ($deal['Deal']['discounted_price']         , 'small');
            $precio_original = $html->cCurrency ($deal['Deal']['original_price']           , 'small');
            $total           = $html->cCurrency (sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$dealExternal['DealExternal']['quantity'])), 'small');
            $pagoTerceros= sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$dealExternal['DealExternal']['quantity']))-$internalAmount;
         //   $internalAmount  = $html->cCurrency (sprintf('%.2f',$internalAmount), 'small');
            $pagoTerceros=  $dealExternal['DealExternal']['parcial_amount'];
            //formating
            $pagoTerceros = $html->cCurrency($pagoTerceros,'GBP');
            $creditousado = array_sum(array($internalAmount, $dealExternal['DealExternal']['gifted_amount'], $dealExternal['DealExternal']['discounted_amount']));
            $creditousado = $html->cCurrency($creditousado,'GBP');

            $is_discount_mp = false;
            if ($deal['Deal']['is_discount_mp'] && $is_payment_type_mp) {
                $is_discount_mp = true;
            }

        ?>

        <p class="gracias">&iexcl;Muchas gracias por tu compra, <?php echo $html->getUserLink($current_user_details); ?>!</p>


        <?php

        if($deal['Deal']['is_now']==0){
        ?>
            <h3 class="leyenda-oferta">Una vez finalizada la oferta, te enviaremos el cup&oacute;n y toda la informaci&oacute;n necesaria para que disfrutes de tu descuento <span>Club Cup&oacute;n</span></h3>
        <?php
        }else{

            $cuando   = $time->isToday($deal['Deal']['start_date']) ? "Hoy" : 'Ma&ntilde;ana';
            $h_inicio = substr($deal['Deal']['start_date'], 11, -3);
            $h_fin    = substr($deal['Deal']['end_date'],   11, -3);
            if($h_fin=='00:00') $h_fin='24';
        ?>
            <h3 class="leyenda-oferta">Para utilizar <?php echo $cuando; ?> desde las <?php echo $h_inicio; ?>hs hasta las <?php echo $h_fin; ?>hs. Disfrut&aacute; de tu descuento <span>Club Cup&oacute;n YA</span></h3>
        <?php
        }
        ?>

        <div class="grid_16 bloque-tabla compra-mensaje alpha omega">
            <div class="grid_16">

                <table class="fmt-tabla tabla-compras">

                    <tr>
                        <th scope="col">Resumen</th>
                        <th scope="col">Su Pedido</th>
                        <th scope="col">Precio Total</th>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td><?php echo "$cantidad x \${$precio}" ;?></td>
                        <td class="talingl"><?php echo "\${$total}";?></td>
                    </tr>




                    <tr>
                        <td>
                            <?php echo $html->showImage('Deal', $deal['Attachment'], array('class' => 'bigImage', 'dimension' => 'small_big_thumb', 'alt' => sprintf('[Foto: %s]', $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)), 'title' => $html->cText($deal['Deal']['name'], false))); ?>
                            <p>
                                <?php
                                    if($deal['Deal']['only_price'])
                                    {
                                        echo '$' . $precio_original . ' por ' . $deal['Deal']['name'];
                                    }
                                    else
                                    {
                                        echo 'Desde $' . $precio . ' en vez de $' . $precio_original . ' por ' . $deal['Deal']['name'];
                                    }
                                ?>
                            </p>
                        </td>
                        <td class="va-middle">
                            <?php if($is_discount_mp) {?>
                                Cr&eacute;dito Otorgado<br />
                            <?php } else { ?>
                                Cr&eacute;dito Usado<br />
                            <?php } ?>

                            <?php if($pagoTerceros >0){?>
                                Pagado por medios de pago de terceros
                            <?php } ?>
                        </td>
                        <td class="va-middle">
                           <?php echo "&nbsp;&nbsp;\${$creditousado}";?>
                            <br />
                            <?php if($pagoTerceros >0){ echo "&nbsp;&nbsp;\${$pagoTerceros}";} ?>
                        </td>
                    </tr>


                    <tr>
                        <td></td>
                        <td class="fmt-totaltabla">Total Pagado:</td>
                        <td class="fmt-totaltabla"><?php echo "\${$total}";?></td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td align="center">
                            <?php
                            
                            if($is_hidden_city)
                            {
                                echo $html->link ('Volver', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>'ciudad-de-buenos-aires'), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> 'btn-gris', 'escape' => false));
                            }
                            else
                            {
                                echo '<a href="/" class="btn-gris">Volver</a>';
                            }
                            
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $html->link('Ir a mis cupones', array('controller'=>'deal_users', 'action' => 'index'), array('class' => 'btn-gris')); ?>
                        </td>
                    </tr>

                </table>

            </div>
            <div>
        		<a target="_blank" id="fbShare">
            	<?php $image_dir = '/img/theme_clean/';?>
  				<img src="<?php echo $image_dir; ?>facebook_compartir.gif" alt="club cupon" title="Club Cupon" border="0" />
				</a>
			</div>
            
        </div>
    </div>
    <!-- / Contenidos -->


</div>
<script>
    $(document).ready(function() {
        var citySlug=location.href.substr(location.href.indexOf("city")+"city".length+1);
		var urlShare='<?php echo STATIC_DOMAIN_FOR_CLUBCUPON; ?>/'+citySlug+'/deal/<?php echo $deal['Deal']['slug']?>';
    	$('#fbShare').attr('href','https://www.facebook.com/sharer/sharer.php?u='+urlShare);
    });
</script>
<?php
    echo $this->element('web/trackingCompraOk');
    echo '<!-- EmtTracker -->';
    echo $this->element('theme_clean/emt/tracking_conversion');
    echo '<!-- / EmtTracker -->';
?>