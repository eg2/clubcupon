<?php
  $count = 1;
  $activeDeal = false;
  $inactiveDeal = false;
  $statusesActive = array (ConstDealStatus::Open, ConstDealStatus::Tipped);
  $statusesInactive = array (ConstDealStatus::Closed, ConstDealStatus::PaidToCompany);


  foreach ($deals as $deal) {
    if (in_array ($deal ['Deal']['deal_status_id'], $statusesActive)) {
      $activeDeal = $deal;
      // break;
    } else if (in_array ($deal ['Deal']['deal_status_id'], $statusesInactive)) {
      $inactiveDeal = $deal;
    }
    $count++;
  }

  if ($activeDeal !== false) {
    // at least one active deal
	$this->viewVars['mainDeal'] = $activeDeal['Deal'];
    echo $this->element ('../deals/view', array (
        'deal' => $activeDeal,
        'isDealActive' => true,
        'count' => $count,
        'get_current_city' => $get_current_city,
        'hasStock' => $activeDeal['Deal']['hasStock'],
        'has_subdeals' => $activeDeal['Deal']['has_subdeals'],
        'cache' => array (
            'time' => Configure::read ('site.element_cache'),
        ),
    ));
  } else if ($inactiveDeal) {
    // only inactive deal
    echo $this->element ('../deals/view', array (
        'deal' => $inactiveDeal,
        'isDealActive' => false,
        'count' => $count,
        'get_current_city' => $get_current_city,
        'has_subdeals' => $inactiveDeal['Deal']['has_subdeals'],
        'cache' => array (
            'time' => Configure::read('site.element_cache'),
        ),
    ));
  } else {
    // no deals at all
    echo $this->element ('../deals/landing');
  }

  if ($from_subscription)
    echo $this->element ('tracking-subscription-ok');
?>