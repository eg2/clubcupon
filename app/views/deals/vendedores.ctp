<style>
    
    form{
        margin-top:10px;
    }
    
    input{
        height:30px;
        border:none;
        padding:0 5px;
    }
    
    .bt_suscribirme{
        background: #ff5c1c;
        color:#fff;
        font-weight:bold;
        width:152px;
        font-size:12px;
        height:35px;
    }
    
    .orange{
        margin: 5px;
        width:120px;
        height:20px;
        border:none;
    }
    
</style>

<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            
            <!-- Contenidos -->
    
    <?php echo $html->image ('landing-full/logo_clubcupon.png', array ('width' => 152, 'height' => 67)); ?>
            
            
            
        <?php
        
            echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform'));
            
            echo $form->input ('email', array ('value' => 'Ingres&aacute; tu mail', 'label'=>false, 'escape'=>false, 'div'=>false, 'onclick' => "this.value == 'Ingres&aacute; tu mail' ? this.value = '' : '';"));
            $options = array();
            foreach ($clubcupon->activeCitiesWithDeals () as $city)
            {
                $options[$city ['City']['id']] = $city ['City']['name'];
            }
            echo $form->select('city_id', $options,Configure::read ('Actual.city_id'), array('class'=>'orange', 'div'=>false, 'style'=> "width: 180px; line-height: 30px;")) ;
            echo $form->hidden('from', array('value' => 'landing'));
            echo $form->hidden('utm_source', array('value' => 'logisticaPromociones'));
            echo '<div style="padding:5px;">';
            echo $form->submit('REGISTRAR', array('class'=>'bt_suscribirme', 'div'=>false));
            echo '</div>';
                
            echo $form->end();

        ?>
            
            <!-- / Contenidos -->

        </div>
    </div>
</div>