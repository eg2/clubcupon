<--[if IE]>
    <style>
        .btn-orange{
            background: repeat 0 0 transparent url("/img/theme_clean/dynamic/bg_orange.gif");
            border:none;
        }
        #mailingresado2{padding-right:5px;}
    </style>
<![endif]-->

<!--[if IE 7]>
    <style>
        #msgerroremail{margin-left:120px;}
        .emailmob{margin-top:6px!important;}
    </style>
<![endif]-->

<!--[if IE 8]>
    <style>
        #btnRegisterSubmit{margin-left:180px!important;}
    </style>
<![endif]-->
<?php
		//echo var_dump($user['UserProfile']);
//exit;
		?>
<style type="text/css">
    div .input{background:none!important;}
    .gift_discount_code {
        background:none repeat scroll 0 0 #DDEACF;
        width:92%;
        margin-left:auto;
        margin-right:auto;
        border-radius:5px;
        padding:5px;
        margin-bottom:10px;
        text-align:center;
    }

    .gift_discount_code div.input.text{
        display:inline-block;
    }

    #gift_pin_code
    {
        font-weight:bold;
        color:green;
        text-align:right;
        border-style: solid;
    }
    div .input {
      background: none repeat scroll 0 0 white;
    }
 </style>

<?php if(Configure::read('isLiteBoxEnabled')){ ?>

<script type="text/javascript" src="<?php echo Configure::read('bac.static_content_url');?>/backend_web/WEB-JS/crearPagoLiteBox.js"></script>
<!--<script type="text/javascript" src="<?php echo Configure::read('bac.static_content_url');?>/backend_web/WEB-JS/renderMP.js"></script>-->
<script type="text/javascript" src="http://mp-tools.mlstatic.com/buttons/render.js"></script>
<script type="text/javascript" src="<?php echo Configure::read('bac.static_content_url');?>/backend_web/WEB-JS/json2.js"></script>

<?php } ?>

<div id="main" style="clear:both; padding-top: 1px; min-height:555px"><!-- para que no se corra el main a la derecha y pueda tomar fondo white-->

<?php
    echo $form->create ('Deal', array ('url'=>array('action' => 'buy',$this->data ['Deal']['deal_id']), 'autocomplete' => 'off'));
    echo $form->input  ('deal_id',       array ('type' => 'hidden'));
    echo $form->input  ('circa_company', array ('type' => 'hidden'));
    echo $form->input  ('circa_code',    array ('type' => 'hidden'));
    echo $form->input  ('user_id',       array ('type' => 'hidden'));
    echo $form->input  ('is_gift',       array ('type' => 'hidden'));
    echo $form->input  ('deal_amount',   array ('type' => 'hidden'));
    echo $form->input  ('user_dni',       array ('type' => 'hidden'));
    
?>

<!--inicio COMPRAS -->
<div class="container ">
	<div class="row compra-blq back-white">
		<section class="twelvecol">
			<h2>Tu compra</h2>
		</section>

		<section class="threecol fila1">
			<article class="contendor-img">
				<<p class="titulo-tabla" style="text-align: left;">Tu opci&oacute;n Elegida</p>
				<!--img src="img/pic-mini.jpg" alt="" /-->
				<?php
						echo $html->showImage('Deal', $deal['Attachment'], array('id' => 'deal_image', 'dimension' => 'medium_thumb', 'alt' => $html->cText($deal['Company']['name'] . ' ' . $deal['Deal']['name'], false), 'title' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)));
                ?>

				<h3 class="descripcion-prod"><?php echo $clubcupon->shorten_strings_with_html_entities($deal ['Deal']['name'], 142); ?></h3>

			</article>
		</section>

		<section class="sevencol">
			<article class="tabla-precio">
				<ul class="clearfix">
					<?php

                        $min_info = $deal ['Deal']['buy_min_quantity_per_user'];
                        $stock_max = $deal['Deal']['max_limit'];

                        if($stock_max)
                        {
                        	 //$stock_bought = $deal['Deal']['payed_cupons_qty'];
                        	//$stock_bought = $deal['Deal']['bought_cupons_qty'];//incluye los A,P
                            //$current_stock = $stock_max -$stock_bought ;
                        	$current_stock = $deal['Deal']['available_qty'] ;
                        }

                        if(isset($current_stock) && ($current_stock<$deal['Deal']['buy_max_quantity_per_user'] ))
                        {
                            $max_info = $current_stock;
                        } else {
                            $max_info =  $deal ['Deal']['buy_max_quantity_per_user'];
                        }

                        $min_info = ($this->data['Deal']['quantity'] < $min_info) ? $min_info : $this->data['Deal']['quantity'];
                      
                        $readonly = (empty($error))&& ($max_info == $min_info) && !empty($max_info) && !empty($min_info) && (!($this->data['Deal']['quantity']==$deal['Deal']['buy_max_quantity_per_user']) || ($deal ['Deal']['buy_min_quantity_per_user']==$deal['Deal']['buy_max_quantity_per_user']));


                        $paymentTypes = $adminpagos->getPaymentTypesFor ($deal ['Deal']['id']);
                        $dealHasWallet = false;
                        //echo $adminpagos->printJsLogoTable ();

                        if (!$auth->sessionValid () || !($user_available_balance > 0) || $user_wallet_blocked)
                        {
                            if(isset($paymentTypes['wallet']))
                            {
                                $dealHasWallet = true;
                                $adminpagos->printJsWalletCard(
                                    $paymentTypes['wallet']['id_gateway'],
                                    $adminpagos->getWalletCard($paymentTypes['wallet']['cards'])
                                    );
                                $paymentTypes ['wallet']['cards'] = array_filter ($paymentTypes ['wallet']['cards'], 'wallet_filter');
                            }
                        }

                        if (!$auth->sessionValid () || !($user_available_points > 0) || $user_wallet_blocked)
                        {
                            if(isset($paymentTypes['wallet']))
                            {
                                $paymentTypes ['wallet']['cards'] = array_filter ($paymentTypes ['wallet']['cards'], 'points_filter');
                            }
                        }

                        if (empty ($paymentTypes ['wallet']['cards'])){ unset ($paymentTypes ['wallet']); }
                    ?>
					<li class="fila2"><p class="titulo-tabla">Cantidad</p>
					<?php

					if($readonly)
                    {
                        echo $form->input ('quantity', array ('value'=>$min_info,'label' => false, 'class' => 'js-quantity', 'onclick' => 'this.focus (); this.select ();', 'readonly' =>'readonly'));
                    }else{
                        echo $form->input ('quantity', array ('value'=>$min_info,'label' => false, 'class' => 'js-quantity', 'onclick' => 'this.focus (); this.select ();'));
                    }
					echo $adminpagos->printJsLogoTable ();
					 echo $form->input ('user_available_balance', array ('type' => 'hidden', 'value' => $user_available_balance));
					if (!empty ($min_info)){ echo '<p class="minmax">Cantidad m&iacute;nima: ' . $deal ['Deal']['buy_min_quantity_per_user'] . '</p>'; }
                    if (!empty ($max_info)){ echo '<p class="minmax">Cantidad m&aacute;xima: ' . $max_info . '</p>'; }
                    echo '<div class="min-quantity" style="display:none">'.(!empty ($min_info)?$deal ['Deal']['buy_min_quantity_per_user']:"")  . '</div>';
					?>
					<!--p class="minmax">cantidad minima:1</p><p class="minmax">cantidad maxima:10</p-->
					</li>
					<li class="fila3"><p class="titulo-tabla">Precio por <span>ClubCup&oacute;n</span></p>
						<p class="centertext"><?php
                            echo Configure::read ('site.currency');
                        	echo $html->cCurrency ($this->data ['Deal']['deal_amount'], 'small');
                        ?>
                        </p>
					</li>
					<li class="fila4"><p class="titulo-tabla">Total</p>
					<p class="centertext">
					<span>$</span><span class="js-deal-total"><?php echo $html->cCurrency ($this->data ['Deal']['total_deal_amount'], 'small'); ?></span>
					</p></li>
				</ul>
			</article>
                    </section>
            <section class="twocol last">
                <?php if(!$deal ['Deal']['only_price']){ ?>
                <article class="ahorro-blq">
                    <p>Ahorraste</p>
                    <p class="precionum"><?php echo $deal ['Deal']['discount_percentage'];?></p>
                    <span>%</span>
                </article>
                <?php } ?>
            </section>
            <?php if ($auth->sessionValid () && isset ($user) ){ //inicio LOGUEADO?>
            <div class="toggle_on_gift_pin">
                    <a href="#gift_code_on"  style="display: block;text-indent:20px;margin-bottom:5px;clear:both"> Tengo un c&oacute;digo de descuento</a>
            </div>
        
            <section class="tencol fila1 collapsable_gift_pin"  style="display:none">

                <article class="contendor-img">

				<!--img src="img/pic-mini.jpg" alt="" /-->

                            <?php
                            echo $form->input (false, array ('type' => 'hidden', 'class' => 'readonly_discount_code',  'id' =>'gift_pin_code', 'name' =>'data[gift_pin_code]', 'value'=>'', 'default' => '', 'readonly' =>'readonly', 'div'=> false));
                           echo $form->input (false, array ('type' => 'hidden',  'id' =>'gift_monto', 'name' =>'data[gift_monto]','value'=>'', 'default' => '',  'div'=> false))
                                ?>



			</article>
                </section>

            <section class="twelvecol collapsable_gift_pin"  style="display:none">
                    <article class="gift_discount_code">
                        <!-- <div class="wallet-bonification" style="display: none;text-indent:20px; margin-bottom:5px;">Se te acreditar&aacute;n <span id="wallet-bonification-amount"></span> en tu Cuenta Club Cup&oacute;n.</div> -->
                        <div class="pincode_result" style="display:inline">
                            <img src="/img/blank.gif" id="pincode_result_img" style="vertical-align:middle;"/>
                        </div>
                        <?php
                    echo $form->input (false, array ('type' => 'text', 'class' => 'discount_code', 'label' =>'Código de descuento', 'id' =>'discount_code', 'name' =>'discount_code', 'style' =>''));
                    ?>
                   <?php echo $form->button ('Validar', array ( 'class' => 'subm','div' => false, 'onclick' =>'checkGiftCode()'));?>
                        <div class="toggle_off_gift_pin" style="display:inline">
                            <a href="#gift_code_off"  style="text-align: left;text-indent: 10px;"> (cancelar)</a>
                        </div>
                        <div class="gift_pin_code_error_msg  error-message" style="float:none!important;display:none">El c&oacute;digo no existe o ya fue utilizado</div>
                    </article>
		</section>
            <?php } // LOGUEADO?>

	</div>
</div>
<!--fin COMPRAS-->

<?php 
	$hasDni=isset($user['UserProfile']['dni']) && !empty($user['UserProfile']['dni']);
	$prueba='algo';
	if ($auth->sessionValid () && isset ($user) ){ //inicio LOGUEADO
	
	//si el usuario logueado no posee dni registrado se le solicita que ingrese uno
	//if(isset($user['UserProfile']['dni']) && empty($user['UserProfile']['dni'])){
	?>
	<div class="container" id="buy-dni" style="<?php echo $hasDni?'display:none':'display:block';?>">
		<div class="row compra-blq back-white">
			<section class="twelvecol">
			<h2>Ingres&aacute; tu <span>DNI</span> para avanzar en tu compra</h2>
			</section>
			<div class="back-white">
				<section class="sevencol">
					<article class="ing-blq">
					<p class="ingresacorreo">Ingres&aacute; tu <span>DNI</span></p>
					<div class="correo">
						<label>N&uacute;mero de DNI:</label>
						<?php echo $form->text('userprofile_dni', array( 'id' => 'userprofile_dni'));?>
						
						<?php echo $form->button('Continuar', array( 'class' => 'subm','div' => false,'type' => 'button','id'=>'btn-continuar'));?>
						<div class="error-message" id="msgerrordni" style="float:left !important;padding-top: 5px;"></div>
					</div>
					</article>
				</section>
			</div>
		</div>
	</div>

<!--inicio MEDIO DE PAGO -->

<div class="container" id="vista4">

<div class="row compra-blq back-white">
	<section class="twelvecol">

		<h2>Seleccion&aacute; el <span>medio de pago</span> y continu&aacute; con la compra</h2>
	</section>

	<section class="twelvecol">
		<article>
			<?php
                if ($deal['Deal']['is_discount_mp'])
                {
                    echo '<img src="/img/mp_discount.jpg"></img>';
                }
                else
                {
                    echo '<p class="medios">Medios de pago</p>';
                }
            ?>
			<div class="tarjetas clearfix">

				<?php

                        function wallet_filter ($var) { return !(isset ($var ['payment_type_id']) && $var ['payment_type_id'] == 5000); }
                        function points_filter ($var) { return !(isset ($var ['payment_type_id']) && $var ['payment_type_id'] == 5001); }

                        $paymentTypes = $adminpagos->getPaymentTypesFor ($deal ['Deal']['id']);
                        echo $adminpagos->printJsLogoTable ();

                        if (!$auth->sessionValid () || !($user_available_balance > 0) || $user_wallet_blocked) {
                            $paymentTypes ['wallet']['cards'] = array_filter ($paymentTypes ['wallet']['cards'], 'wallet_filter');
                        }
                        if (!$auth->sessionValid () || !($user_available_points > 0) || $user_wallet_blocked) {
                            $paymentTypes ['wallet']['cards'] = array_filter ($paymentTypes ['wallet']['cards'], 'points_filter');
                        }
                        if (empty ($paymentTypes ['wallet']['cards'])) {
                            unset ($paymentTypes ['wallet']);
                        }

                        $paymentOptions = array();
                       
                        foreach ($paymentTypes as $paymentKey => $paymentDefinition) {
                            //var_dump($paymentTypes[$paymentKey]);
                            //test analizo los casos cuando es promo MP
                            foreach($paymentDefinition ['cards'] as $keyCard => $cardValue)
                            {
                                $keyPrefix = $cardValue['id_gateway'];
                                //var_dump($cardValue['PaymentPlans']);
                                
                                if(  $deal['Deal']['is_discount_mp'] )
                                {
                                    if(  $deal['Deal']['is_discount_mp'] && ($keyPrefix == 11 || $keyPrefix == 23 || $keyPrefix == 24 || $cardValue['name']==="Cuenta Club Cupon" ) )
                                    {
                                        
                                        if($cardValue['PaymentPlans']){$cardValue ['name'] = $cardValue ['name'] . ' <span>[ En cuotas ]</span>';}
                                        
                                        $paymentOptions [implode ('_', 
                                                array ($keyPrefix, 
                                                    $cardValue ['payment_type_id'], 
                                                    $cardValue ['payment_option_id'], 
                                                    $cardValue ['bac_payment_type']))] =
                                                '<img src="'.$cardValue ['logo'].'" />' . $cardValue ['name'] ;
                                        
                                        if (!empty($cardValue['PaymentPlans'])) {
                                            $paymentOptions [implode ('_', 
                                                    array ($keyPrefix, 
                                                        $cardValue ['payment_type_id'], 
                                                        $cardValue ['payment_option_id'], 
                                                        $cardValue ['bac_payment_type']))] .= 
                                                    $form->input ('cantidad_cuotas', array (
                                                        'class' => 'quote paymentOptionSelect',
                                                        'id'=> 'cantidad_cuotas_'.$cardValue ['payment_option_id'],
                                                        'type' => 'select',
                                                        'label'=>false,
                                                        'options' => $cardValue['PaymentPlans']));
                                        }
                                    }

                                }
                                else
                                {
                                    
                                    if($cardValue['PaymentPlans']){$cardValue ['name'] = $cardValue ['name'] . ' <span>[ En cuotas ]</span>';}
                                    
                                    $paymentOptions [implode ('_', 
                                            array ($keyPrefix, 
                                                $cardValue ['payment_type_id'], 
                                                $cardValue ['payment_option_id'], 
                                                $cardValue ['bac_payment_type']))] =
                                            '<img src="'.$cardValue ['logo'].'" />' . $cardValue ['name'] ;
                                    
                                    if (!empty($cardValue['PaymentPlans'])) {
                                        $paymentOptions [implode ('_', 
                                                array ($keyPrefix, 
                                                    $cardValue ['payment_type_id'], 
                                                    $cardValue ['payment_option_id'], 
                                                    $cardValue ['bac_payment_type']))] .= 
                                                $form->input ('cantidad_cuotas', array (
                                                    'class' => 'quote paymentOptionSelect', 
                                                    'id'=> 'cantidad_cuotas_'.$cardValue ['payment_option_id'],
                                                    'type' => 'select', 
                                                    'label'=>false, 
                                                    'options' => $cardValue['PaymentPlans']));
                                    }
                                }

                            }
                        }
                        echo $form->input ('payment_type_id', array ('type' => 'radio', 'legend' => '','div' => false , 'before' => '<ul><li><div>','after' => '</li></ul>', 'separator' => '</div></li><li><div>','options' => $paymentOptions, 'class' => 'js-payment-type cardLogo', 'label' => false));
                        echo $form->input ('Deal.minimal_amount_financial', array ('type' => 'hidden'));
                        //echo '<div class="hide">';
                        //echo 
                       // echo '</div>';
                        if (!is_null ($user_wallet_blocked) && $user_wallet_blocked) {
                            echo '<div class="blocked-wallet"><span>Cuenta Club Cup&oacute;n en uso</span></div>';
                        }

                    ?>
			</div>
		</article>
	</section>
    <style>
        .tarjetas ul li div:first-child{width:235px;}
        
    </style>
	<section class="sixcol">
		<a class="regala" href="#" id="buy_form_gift_button" > Regal&aacute; este Cup&oacute;n a un amigo</a>
	</section>

	<section class="sixcol last">
    <input type='hidden' value="<?php echo count($paymentTypes['wallet']); ?>" id="dealHasWallet">
		<input class="btn-orange finalizarcompra" type="submit" value="Pagar" />
	</section>
  </div>
</div>

<!--fin MEDIO DE PAGO-->

<?php if(Configure::read('isLiteBoxEnabled')){ ?>
    <div class = "hidden">
        <div id="divBAC"></div>
        
        <iframe id="buy-frame" name="buy-frame" src="about:blank"></iframe>
    </div>
<script>
$(document).ready(function(){

    //seleccionamos el primero
    lista = $(".tarjetas :radio");
    $('#'+$(lista[0]).attr('id')).attr('checked', true);


    function updateTarget(){

        radioValue = $("input[name='data[Deal][payment_type_id]']:checked").val()

        substr = radioValue.split('_');


        if(jQuery.inArray(substr[0], ['11','23','24'])!=-1)
        {

            $('#DealAddForm').attr('target','buy-frame');
            $('#DealAddForm').bind('pagoSeguroIniciado', function(event,idGateway,idPago,idPortal,secureHash){

                if(idGateway){
                    data = {'ID_GATEWAY':idGateway,'ID_PAGO':idPago,'ID_PORTAL':idPortal,'SECURE_HASH':secureHash};
                    crearPagoLiteBox(data,"<?php echo Configure::read('BAC.crearPagoSeguro');?>");
                }else{
                    var message = $("<div id='xMessage'>No se pudo crear el Pago!</div>").addClass('message').css('display','none');
                    $('body').prepend(message).find("div#xMessage.message").slideDown().click(function(e){
                        $(this).slideToggle(400,function(){
                            $(this).remove();
                        })
                    });
                }
            });
        }
        else
        {
            $('#DealAddForm').unbind();
            $('#DealAddForm').removeAttr('target');
        }

    }

    $('#DealAddForm').submit(function() {
      updateTarget();
    });


	$('#buy-frame').load(function(e){
		$("div#errorMessage.message").remove();
		$("div#successMessage.message").remove();
		var errorMessage = $("#buy-frame").contents().find("div.error-message").html();
		/*console.log(errorMessage);*/
		var successMessage = $("#buy-frame").contents().find("div#successMessage.message").html();
	/*	console.log(successMessage); */

		if(errorMessage){
			var message = $("<div id='errorMessage' >" + errorMessage + "</div>").addClass('message').css('display','none');
			$('body').prepend(message).find("div#errorMessage").slideDown().click(function(e){
				$(this).slideToggle(400);
			});
		}else if(successMessage){
			var message = $("<div id='successMessage'>" + successMessage + "</div>").addClass('message').css('display','none');
			$('body').prepend(message).find("div#successMessage.message").slideDown().click(function(e){
				$(this).slideToggle(400);
			});
		}/*else{
			var message = $("<div id='successMessage'>Ya cargo el IFRAME</div>").addClass('message').css('display','none');
			$('body').prepend(message).find("div#successMessage.message").slideDown().click(function(e){
				$(this).slideToggle(400);
			});
		}*/
	});

});
</script>
<?php } 
  else { ?>
  <script>
  $(document).ready(function(){
    function disableCantidadCuotas() {
        
        radioValue = $("input[name='data[Deal][payment_type_id]']:checked").val();
        substr = radioValue.split('_');
        payment_option_id = substr[2];     
        
        $("select[id*=cantidad_cuotas_]").each(function(d, i){
          paymentoptionid = this.id.split('_');
          paymentoptionid = paymentoptionid[2];
       
          if (paymentoptionid != payment_option_id){
            $(this).attr('disabled', 'true');
          }
         
        });
        
        
    }
    $('#DealAddForm').submit(function() {
      disableCantidadCuotas();
    });
  });
  </script>
<?php } ?>
<!-- VENTANA GIFT -->
<div class = "hidden">
    <div id = "buy_form_gift"  class = "gift">
	<div class="input clearfix" style="width: 499px;">
        <?php
            echo '<div><h2>REGALASELO A UN AMIGO</h2><h1>Datos de Tu Amigo:</h1></div>';
            echo '<div><div class="label-aligned-left label-centered">Nombre</div>
			'.preg_replace('/^<div([^\>])*class=\"([^\"]*)\"/','<div$1class="$2 field-aligned-right"',$form->input ('gift_to', array ('label' => false))).'
			</div>';
        ?>
        <?php //echo $form->input ('gift_dni', array ('label' => 'DNI de tu amigo'));
		echo '<div><div class="label-aligned-left label-centered">DNI</div>
			'.preg_replace('/^<div([^\>])*class=\"([^\"]*)\"/','<div$1class="$2 field-aligned-right"',$form->input ('gift_dni', array ('label' => false))).'
			</div>';
		?>
		<div style="clear:both"></div>
		<div><div class="label-aligned-left label-centered">Fecha de Nacimiento</div>
		<div class = "input date-time clearfix">
            <div class = "js-datetime-gift">
                <?php echo $form->input ('gift_dob', array ('type' => 'date','label' => false, 'empty' => 'Seleccionar', 'div' => false, 'maxYear' => date ('Y'), 'minYear' => date ('Y') - 100)); ?>
            </div>
        </div>

		</div>
		<div style="clear:both"></div>
        <div class = "input"  style = "text-align: center; margin: 0 0 10px 0;">
            <br />
            <a href = "#"  onclick = "showEmails (); return false;" class="fmt-1 padd-5p">Enviar por mail</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href = "#"  onclick = "hideEmails ();" class="fmt-1 padd-5p">Para imprimir</a>
        </div>
        <div id = "print_notice"  class = "clearfix input" style = "display: none; background-color: pink; text-align: center; font-size: larger;">
            &iexcl;Va a llegar un mail a tu casilla para que imprimas y regales!
        </div>

        <div id = "email_data">
            <?php echo '<div><div class="label-aligned-left">Mail</div>
					'.preg_replace('/^<div([^\>])*class=\"([^\"]*)\"/','<div$1class="$2 field-aligned-right"',$form->input ('gift_email', array ('label' => false))).'
					</div>';
					//echo $form->input ('gift_email', array ('label' => 'Mail del Amigo')); ?>
            <div class="input required">
                <?php echo '<div><div class="label-aligned-left">Repet&iacute; el mail</div>
						'.preg_replace('/^<div([^\>])*class=\"([^\"]*)\"/','<div$1class="$2 field-aligned-right"',$form->input ('confirm_gift_email', array ('label' => false))).'
						</div>';
						//echo $form->input ('confirm_gift_email', array ('label' => 'Repet&iacute; el email', 'div' => false)); ?>
            </div>
        </div>
        <div id = "message_data">
            <?php echo '<div><div class="label-aligned-left">Mensaje</div>
						'.preg_replace('/^<div([^\>])*class=\"([^\"]*)\"/','<div$1class="$2 field-aligned-right"',$form->input ('gift_message', array ('type' => 'textarea', 'label' => false))).'
						</div>';
				//echo $form->input ('gift_message', array ('type' => 'textarea', 'label' => 'Mensaje')); ?>
        </div>
        <div id = "cboxSave"  class = "save">Salvar</div>
	</div>
    </div>
</div>
<!-- /GIFT -->
<!--inicio PRE LOGIN -->

<?php echo $form->end ();
?>
<?php } else {//fin logueado

//inicio secciones no logueado:
  echo $form->end ();

	$forward_url = 'deals/buy/' . $this->data ['Deal']['deal_id'] . '/' . $this->data ['Deal']['quantity'];
	$pos=false;
	$user_mail='';
    if(isset($this->params['url']['url']) && $this->params['url']['url']!=false) {
         /* we try to forward to the current url*/
    		 $pos = strrpos($this->params['url']['url'], '/user_mail');
    	     if($pos===false){//not found
    	    	 $forward_url= $this->params['url']['url'];
    	     }else{
    	    	 $forward_url= substr($this->params['url']['url'], 0,$pos+1);	 //sin el extra del "user_mail"
    	    	 $user_mail=substr($this->params['url']['url'],$pos+11);
    	     }

             //echo '<h1>'.$forward_url.'</h1>';
             //echo '<h2>'.$pos.'</h2>';
    }

	?>


<?php echo $form->create ('User', array ('url'=>array('action' => 'login'), 'autocomplete' => 'off','id'=>'UserAddFormPrelogin'));
		?>

<div class="container" id="buy-prelogin" <?php if(isset($has_register_errors) || $pos){ echo "style='display:none'";}?>>
	<div class="row compra-blq back-white">
		<section class="twelvecol">
			<h2>Ingres&aacute; tu <span>direcci&oacute;n de mail</span> o inici&aacute; sesi&oacute;n con <span>Facebook</span> para avanzar en tu compra</h2>
		</section>



		<div class="back-white">
			<section class="sevencol">
				<article class="ing-blq">
					<p class="ingresacorreo">Ingres&aacute; tu <span>mail</span></p>
					<div class="correo">
						<label>Mail</label>
						<?php echo $form->text('email2check');?>
						<?php echo $form->submit ('Continuar', array ( 'class' => 'subm','div' => false));?>
					</div>
					<div class="error-message" style="display:none;float:left" id="msgerroremail">Debe ser un email v&aacute;lido</div>
				</article>
			</section>
            <?php
                echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection');
            ?>
			<section class="fivecol last bor-l">
				<article class="faceb">
					<p>&#191;ya ten&eacute;s una cuenta en <a href="">Facebook</a>?</p>
					<p>Usala para comprar en <span>Club Cup&oacute;n</span></p>
                    <?php echo $html->link($html->image('/img/theme_clean/dynamic/fb-ing.jpg'), '#', array('class' => 'fb-link', 'rel'=> 'nofollow','title' => 'fconnect', 'id'=>'facebookLoginButton'), null, false); ?>
				</article>
			</section>
		</div>
	</div>
</div>
<!--fin PRE LOGIN -->
<?php echo $form->end ();?>
<?php echo $form->create ('User', array ('url'=>array('action' => 'login'), 'autocomplete' => 'off','id'=>'UserAddFormLogin'));
?>
<!--inicio LOGIN -->
<div class="container" id="vista3" <?php if($pos){ echo "style='display:block'";}else{ echo "style='display:none'";}?>>
<div class="row compra-blq back-white">
	<section class="twelvecol">
		<h2>Coloc&aacute; tu <span>contrase&ntilde;a</span> y avanz&aacute; con tu compra</h2>
	</section>

	<div class="back-white">
		<section class="sixcol">
			<article class="ing-blq">
			<p class="ingresacorreo">Tu mail est&aacute; registrado en <span>Club Cup&oacute;n.</span> Ingres&aacute; tu contrase&ntilde;a.</p>
				<div class="correo clearfix" style="border:0px solid">
                    <div>

                        <label class="mailingresado" style="width:235px !important"><p id="mailingresado2"><?php echo $user_mail;?></p> <a href="#" id="cambiarmailok">cambiar</a></label>
                        <label class="emailmob">Email</label>
                    </div>
                    <div style="clear:both;">
                        <label style="float:left;">Contrase&ntilde;a</label>
                        <div style="float:right; width:250px;">
                            <input id="orange_btn" class="continuar btn-orange" type="submit" value="Continuar" />
                            <?php echo $form->password ('passwd');?>
                        </div>
                    </div>
				</div>
				<?php
						if ($pos){echo $form->hidden('email', array('value' => $user_mail));}
						else{	echo $form->hidden('email');}
				?>
				<?php
					if (!empty($forward_url)){ echo $form->input('f', array(/*'id'=>'flogin',*/ 'type' => 'hidden', 'value' => $forward_url)); }
				?>

			</article>
		</section>
		<section class="sec-cheks sixcol bor-l last"></section>
	</div>
</div>
</div>
<!--fin LOGIN -->
<?php echo $form->end ();?>
<!--inicio REGISTRO-->
<?php echo $form->create ('User', array ('url'=>array('controller' => 'deals', 'action' => ($is_subdeal_selected?'buy_without_subdeal':'buy'), $this->data ['Deal']['deal_id']), 'autocomplete' => 'off','id'=>'UserAddFormRegister','onsubmit'=>"updateUserAddFormRegisterQty()"));
?>
<div class="container" id="vista2" <?php echo (isset($has_register_errors))? "style='display:block'": "style='display:none'" ;?>>
  <div class="row compra-blq back-white">
	<section class="twelvecol">
		<h2>Complet&aacute; tus <span>Datos de Registro</span> para avanzar con tu compra</h2>
	</section>

	<div class="back-white">
		<section class="sixcol">
			<article class="ing-blq">

				<div class="correo clearfix">
					<!--label for="UserUserEmail">Email</label-->
                    <?php echo $form->input('email', array('label' => 'Email', 'id' => 'input_mailingresado','readonly'=>'readonly')); ?>
					<label class="mailingresado" id="bloque_mailingresado" <?php //if(isset($has_register_errors)) echo "style='display:none'" ;?>>
						<p id="mailingresado1"></p>
						<a href="#" id="regresar-prelogin">cambiar</a>
					</label>
                    <?php

                        echo '<div style="clear:both">' . $form->input('username',           array('info'  => '', 'label' => 'Nombre de usuario')) . '</div>';
                        echo '<div style="clear:both">' . $form->input('passwd',             array('label' => 'Contrase&ntilde;a')) . '</div>';
                        echo '<div style="clear:both">' . $form->input('confirm_password',   array('type'  => 'password', 'label' => 'Confirmar contrase&ntilde;a')) . '</div>';
                        echo '<div style="clear:both">' . $form->input('UserProfile.dni',    array('label' => 'DNI')) . '</div>';
                        echo '<div style="clear:both">' . $form->input('only_register',      array('type'  => 'hidden', 'value' => '1')) . '</div>';
                        if (!empty($forward_url)){ echo $form->input('fwd', array('type' => 'hidden', 'value' => $forward_url)); }
                      //  echo $form->input('quantity', array('type' => 'hidden'));
                    ?>
				</div>
			</article>
		</section>

		<section class="sec-cheks sixcol last">
            <?php if($hideSubscription){ ?>
			<article class="checks bor-bot">
				<?php echo $form->checkbox('Deal.notifications', array('checked' => 'checked','class' => 'check-st')); ?>
				<label>Si, quiero recibir emails con las ofertas diarias de Club Cup&oacute;n</label>
			</article>
            <?php } ?>
			<article class="checks">
				<?php echo $form->checkbox('Deal.terms'); ?>
				<label>He le&iacute;do y acepto los
				<span>
				<?php echo $html->link ('T&eacute;rminos y Condiciones', array ('controller' => 'pages', 'action' => 'terms', 'admin' => false), array ('escape' => false, 'target' => '_blank'));?>
				</span> del sevicio</label>
			</article>
			<div class="suscripto">
				<p>Puede que ya estes suscripto a nuestras newsletters pero no tengas una cuenta. <span>Registrate Gratis</span> completando tus datos y empez&aacute; a disfrutar de todos los beneficios.</p>
			</div>
			<input class="continuar btn-orange" type="submit" value="Continuar" id="btnRegisterSubmit" />
		</section>
	  </div>
   </div>
</div>
<!--fin REGISTRO-->
<?php echo $form->end ();?>

<?php } //fin secciones no logueado:
?>

</div>
<script>
function validateEmail(sEmail) {
	var filter =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}

	//return true;
}
$(document).ready (function () {
	
	$('#btn-continuar').click(function(){
		userprofile_dni=$.trim($("#userprofile_dni").val());
		if(!userprofile_dni==''){
			//valido el dni
			if(!isPositiveInteger(userprofile_dni)){
				$("#msgerrordni").html("Debe ingresar un valor num&eacute;rico");
				//$("#msgerrordni").css("display", "block");
				//alert('Debe ingresar un valor numerico');
				
			}else{
				$("#msgerrordni").html("");
				//$("#msgerrordni").css("display", "none");
				//si ingres� un valor de dni valido lo dejo continuar
				$("#buy-dni").css("display", "none");
				$("#vista4").css("display", "block");	
			}
			
			
			
			 
		}else{
			//si NO ingres� el dni NO lo dejo continuar
			$("#msgerrordni").html("Debe ingresar el DNI para poder continuar con la compra");
			//alert('Debe ingresar el DNI para poder continuar con la compra');
			$("#userprofile_dni").focus();
		}
	});
	

    $('.toggle_on_gift_pin a').bind('click',function(){
    $(".gift_pin_code_error_msg").hide();
    $("#gift_pin_code").val("");
    $("#gift_monto").val("");
    $(".gift_pin_code_error_msg").html("");
    $("#discount_code[name=discount_code]").val("");
    $(this).parent().hide();
    $('.collapsable_gift_pin').show();


    });

    $('.toggle_off_gift_pin a').bind('click',function(){
        $("#pincode_result_img").attr('src', '/img/blank.gif');

       $('.toggle_on_gift_pin').show()
    $(".gift_pin_code_error_msg").html("");
    $("#gift_pin_code").val("");
    $("#gift_monto").val("");
    $('.collapsable_gift_pin').hide();
		updateJsQuantity();

    });

    updateJsQuantity();

	// si el campo de cantidad es readonly actualizo los totales
	if('.js-quantity[readonly="readonly"]'){
		updateJsQuantity();
        checkShowCuotas();
	}

/**/
	<?php if($hasDni){?>
	
	 $("#vista4").css("display", "block");
	<?php }else{?>
	
	 $("#vista4").css("display", "none");
	<?php }?>

	/**/
	
	/*validacion de form de registro antes de enviar*/
	$('#UserAddFormRegister').submit(function(){

		var isChecked = $('#DealTerms').attr('checked')?true:false;
		if(isChecked==false)
			alert('Debe aceptar los T\xE9rminos y Condiciones	');
        return isChecked;


	});
	$('#UserAddFormRegister').submit(function(){
		var selectedQuantity = $('.js-quantity').val();

		//$('#UserQuantity').val(5);
		//alert($('#UserQuantity').val());
		return true;
	});
	/*
	$('#UserAddFormRegister').submit(function(){

		var selectedQuantity = $('.js-quantity').val();
		var extra_data = '&quantity="'+selectedQuantity+'"';
		alert(extra_data);
		var action=$('#UserAddFormRegister').attr("action");

		$.post(action,
			    $('#UserAddFormRegister').serialize()+extra_data);

		return true;

	});
	*/

	/* validacion de email ingresado*/
	$('#UserAddFormPrelogin').submit(function() {
		$("#UserEmail2check").val($.trim($("#UserEmail2check").val()));
		/*validacion de email segun formato*/
		var EmailText = $("#UserEmail2check").val();
		//alert(EmailText);
		if ($.trim(EmailText).length == 0 || !validateEmail(EmailText)) {
			//alert("debe ingresar un email");
			$('#msgerroremail').css("display","block");
		}
		else{
		  $.post("<?php echo Router::url(array('controller'=>'users','action'=>'buy_prelogin'),false) ?>",
		    $('#UserAddFormPrelogin').serialize(),
				function(data) {
					//oculto seccion de pre-login :
        			$('#buy-prelogin').css("display","none");
        			$('#msgerroremail').css("display","none");
        			//alert(data.dni+':'+data.userprofile_id);
			        if(data.existe){//el email existe est� registrado en la bd
			        	//muestro seccion de login (ingreso de contrase�a) :
			        	$('#UserEmail').val(data.email);
			        	$('#mailingresado2').text(data.email);
			        	
			        	$('#vista3').css("display","block");
			        }else{//el mail no existe en la bd
			        	//muestro seccion de registro  :
			        	$('#bloque_mailingresado').css("display","block");
			        	//$('#mailingresado1').css("display","none");
			        	//$('#mailingresado1').text(data.email);
			        	//$('#input_mailingresado').css("display","none");
			        	$('#input_mailingresado').val(data.email);
			        	//$('#UserAddFormRegister').field('data[User][email]', data.email); //text
			        	$('#vista2').css("display","block");
			        }
				},
				"json");
		}
		return false;
	  });
	/* link CAMBIAR desde registro.*/
	$('#cambiarmail').click(function() {
		$('#bloque_mailingresado').css("display","none");

		$('#input_mailingresado').css("display","block");
		$('#UserEmail').val($('#mailingresado1').text());
		$('#UserEmail').focus();
	});
	/* link CAMBIAR desde login.*/
	$('#cambiarmailok').click(function() {
		//muestro de nuevo la seccion de pre-login :
		$('#vista3').css("display","none");
		$('#UserEmail2check').focus();
		$('#buy-prelogin').css("display","block");
	});
	/* link SI YA TENES USUARIO desde registro.*/
	$('#regresar-prelogin').click(function() {
		//muestro la seccion de pre-login :
		$('#vista2').css("display","none");
		$('#UserEmail2check').focus();
		$('#buy-prelogin').css("display","block");
	});

  $(".js-payment-type:first").attr("checked", "checked"); // make checkbox or radio checked
  $('#email_data'  ).hide ();
  $('#message_data').hide ();
  $('#print_notice').hide ();
  <?php if (isset($buyAsGift) && $buyAsGift) { ?>
  $('#buy_form_gift_button').click ();
  <?php } ?>

<?php if($dealHasWallet) { ?>

  function verifyCreditForSubscription(email) {

      url = '../../../subscriptions/find_total_available_balance_by_email/';
      $.post(url, {email : email},
          function(data){
//              console.log(data);
              if(data.success && data.amount > 0) {
                  enableWallet();
              } else {
                  disableWallet();
              }
          },
              "json");

      return false;
  }

  function enableWallet() {
//      console.log('enable wallet');
      id = 'DealPaymenTypeId' + walletGatewayId + '_' + walletCard.payment_type_id + '_' + walletCard.payment_option_id;
      value = walletGatewayId + '_' + walletCard.payment_type_id + '_' + walletCard.payment_option_id;
      html = '<input type="radio" id="' + id + '" class="js-payment-type" value="' + value + '" name="data[Deal][payment_type_id]">';
      html += '<label for="' + id + '">Cuenta Club Cup&oacute;n</label>';
      $('#DealPaymentTypeIdDummy').before(html);
  }

  function disableWallet() {
      id = 'DealPaymenTypeId' + walletGatewayId + '_' + walletCard.payment_type_id + '_' + walletCard.payment_option_id;
      $('#'+id).removeAttr("checked"); // uncheck the checkbox or radio
      $(".js-payment-type:first").attr("checked", "checked"); // make checkbox or radio checked
      $('#'+id).remove(); // uncheck the checkbox or radio
      $('label[for=\'DealPaymenTypeId5000_5000_75\']').remove(); // uncheck the checkbox or radio
  }


  $('.compra-izq #UserEmail').change(function() {
      if ($(this).val() && $(this).val() == $('.register #UserConfirmEmail').val()) {
          verifyCreditForSubscription($(this).val());
      } else {
          disableWallet();
      }
  });

  $('.compra-izq #UserConfirmEmail').change(function() {
      if ($(this).val() && $(this).val() == $('.register #UserEmail').val()) {
          verifyCreditForSubscription($(this).val());
      } else {
          disableWallet();
      }
  });

<?php } ?>
});
</script>

<!-- HEAT MAP -->

<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
if(document.location.protocol!='https:')
  document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
if(typeof ClickTale=='function') ClickTale(162,0.5,"www14");
</script>
<!-- ClickTale end of Bottom part -->

<!-- FIN HEAT MAP -->
<script type = "text/javascript">
  function hideEmails () {
    $('#DealGiftEmail'       ).val ('<?php echo $user ['User']['email']; ?>');
    $('#DealConfirmGiftEmail').val ('<?php echo $user ['User']['email']; ?>');
    $('#email_data'  ).hide ();
    $('#message_data').show ();
    $('#print_notice').show ();
  }

  function showEmails () {
    $("#DealGiftEmail"       ).val ('');
    $('#DealConfirmGiftEmail').val ('');
    $('#email_data'  ).show ();
    $('#message_data').show ();
    $('#print_notice').hide ();
  }

  $(document).ready (function () {
      
      
    $('.paymentOptionSelect').each( function() {
        if( $(this).find('option').length == 1 ) {
            $(this).parents().eq(1).find('span').hide()
        } 
    });
    
    var primerMedioDePagoSeleccionado = $("#DealAddForm input[type='radio']:checked").parent().get(0);
    $(primerMedioDePagoSeleccionado).find('select').toggle();
      
    //Manejo de selects en las opciones de pago 
    $(".js-payment-type").change(function(){ 
        hideSelects();
        var parentTag = $(this).parent().get(0);
        $(parentTag).find('select').toggle();
        });
        
    function hideSelects() {
        $('.paymentOptionSelect').hide();
    }
      
      
    $('#email_data'  ).hide ();
    $('#message_data').hide ();
    $('#print_notice').hide ();
    <?php if (isset($buyAsGift) && $buyAsGift) { ?>
    $('#buy_form_gift_button').click ();
    <?php } ?>
  });
</script>
