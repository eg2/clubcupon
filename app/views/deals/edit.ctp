<?php

    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA
    //ESTA VISTA NO SE USA MAS, DEBERIAMOS BORRARLA

?>

<?php $is_subdeal = $this->data ['Deal']['parent_deal_id'] != $this->data ['Deal']['id']; ?>
<?php echo $this->element ('js_tiny_mce_setting', array ('cache' => Configure::read ('tiny_mce_cache_time.listing_element_cache_duration'))); ?>
<div class = "deals form">
  <?php echo $form->create ('Deal', array ('class' => 'normal', 'enctype' => 'multipart/form-data'));?>
    <h2><?php echo __l('Edit Deal'); ?></h2>
    <fieldset class = "form-block">
    <legend><?php echo __l('General'); ?></legend>
    <?php echo $html->link ($html->showImage ('Deal', $this->data ['Attachment'], array ('dimension' => 'normal_thumb', 'alt' => sprintf (__l('[Image: %s]'), $html->cText ($this->data ['Deal']['name'], false)), 'title' => $html->cText ($this->data ['Deal']['name'], false))), array ('controller' => 'deals', 'action' => 'view',  $this->data ['Deal']['slug'], 'admin' => false), null, null, false); ?>
    <?php
      echo $form->input ('id');
      echo $form->input ('parent_deal_id', array ('type' => 'hidden'));
      echo $form->input ('deal_status_id', array ('type' => 'hidden'));
      echo $form->input ('name', array ('label' => __l('Name')));
      echo $form->input ('descriptive_text', array ('type' => 'text', 'label' => 'Texto descriptivo'));
      echo $form->input ('city_id', array ('label' => __l('City')));
    ?>
    <?php
      if ($this->data ['Deal']['deal_status_id'] != ConstDealStatus::Tipped && $this->data ['Deal']['deal_status_id'] != ConstDealStatus::Open) {
        if (!$is_subdeal) {
    ?>
      <div class = "input clearfix required">
        <div class = "js-datetime">
          <?php echo $form->input ('start_date', array ('label' => __l('Start Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
        </div>
      </div>
    <?php } else { ?>
      <div class = "input clearfix required">
        <?php echo $form->input ('start_date', array ('label' => __l('Start Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
      </div>
    <?php
        }
      } else {
        echo $form->input ('start_date', array ('type' => 'hidden'));
      }
    ?>
    <?php
      if (!$is_subdeal) {
    ?>
      <div class = "input clearfix required">
        <div class = "js-datetime">
          <?php echo $form->input ('end_date', array ('label' => __l('End Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
        </div>
      </div>
    <?php } else { ?>
      <div class = "input clearfix required">
        <?php echo $form->input ('end_date', array ('label' => __l('End Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
      </div>
    <?php
      }
    ?>
    <?php
      echo $form->input ('min_limit', array ('label' => __l('No. of min. buyers')));
      echo $form->input ('max_limit', array ('label' => __l('No. of max. buyers')));
      echo $form->input ('buy_min_quantity_per_user', array ('label' => __l('Minimum Buy Quantity'), 'info' => __l('How much minimum coupons user should buy for himself. Default 1')));
      echo $form->input ('buy_max_quantity_per_user', array ('label' => __l('Maximum Buy Quantity'), 'info' => __l('How much coupons user can buy for himself. Leave blank for no limit.')));
    ?>
    <?php
      if (Configure::read ('deal.is_side_deal_enabled')) {
        echo $form->input ('is_side_deal', array ('label' => __l('Side Deal'), 'info' => __l('Side deals will be displayed in the side bar of the home page.')));
      }
    ?>
    <?php echo $form->input ('has_pins', array ('label' => 'Cantidad de pines por cupón')); ?>
    <?php echo $form->input ('is_tourism', array ('label' => 'Es oferta de turismo')); ?>
    <fieldset class = "form-block">
    <legend>Personalizaci&oacute;n</legend>
    <?php echo $form->input ('custom_subject', array ('label' => 'Subject del mail', 'class' => 'inputlong', 'div' => false)); ?>
    <?php echo $form->input ('custom_company_name', array ('label' => 'Nombre de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_contact_phone', array ('label' => 'Tel&eacute;fono de contacto de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_address1', array ('label' => 'Direcci&oacute;n de la compa&ntilde;&iacute;a (1)', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_address2', array ('label' => 'Direcci&oacute;n de la compa&ntilde;&iacute;a (2)', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_city', array ('label' => 'Ciudad de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_state', array ('label' => 'Provincia de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_country', array ('label' => 'Pa&iacute;s de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('custom_company_zip', array ('label' => 'C&oacute;digo Postal de la compa&ntilde;&iacute;a', 'class' => 'inputlong')); ?>
    <?php echo $form->input ('priority', array ('label' => 'Prioridad')); ?>
    <?php echo $form->input ('risk', array (
              'type' => 'select',
              'options' => array (
                ConstRisk::Low => ConstRisk::getFriendly (ConstRisk::Low),
                ConstRisk::Medium => ConstRisk::getFriendly (ConstRisk::Medium),
                ConstRisk::High => ConstRisk::getFriendly (ConstRisk::High),
              ),
              'empty' => true,
              'label' => 'Riesgo',
          ));
    ?>
    <?php echo $form->input ('show_map', array('label' => 'Mostrar Mapa')); ?>
    <?php echo $form->input ('hide_price', array('label' => 'Ocultar el precio')); ?>
    </fieldset>
    <fieldset class = "form-block">
    <legend><?php echo __l('Price'); ?></legend>
    <?php echo $form->input ('original_price', array ('class' => 'js-price', 'after' => Configure::read ('site.currency'))); ?>
    <div class = "two-col-form clearfix">
      <?php echo $form->input ('discount_percentage', array ('label' => __l('Discount (%)'))); ?>
      <?php echo $form->input ('discount_amount', array ('label' => __l('Discount Amount'), 'after' => Configure::read ('site.currency'))); ?>
      <?php echo $form->input ('savings', array ('type'=>'text',  'label' => __l('Savings'), 'readonly' => 'readonly', 'after' => Configure::read ('site.currency'))); ?>
    </div>
    <?php echo $form->input ('discounted_price', array ('label' => __l('Discounted Price'), 'type' => 'text', 'readonly' => 'readonly', 'after' => Configure::read ('site.currency'))); ?>
    </fieldset>
    <fieldset class = "form-block">
    <legend><?php echo __l('Financiamiento'); ?></legend>
    <?php echo $form->input ('minimal_amount_financial', array ('label' => 'Monto de financiación mínimo',
                                                                'class' => 'js-price',
                                                                'info' => 'Indica el monto mínimo financiable (cuotas) para esta oferta.')); ?>
    <div class = "two-col-form clearfix"></div>
    </fieldset>
    <fieldset class = "form-block">
    <legend><?php echo __l('Commission'); ?></legend>
    <div class = "clearfix">
      <?php echo $form->input ('pay_by_redeemed', array ('type' => 'checkbox', 'label' => 'Liquidar por cupones redimidos')); ?>
    </div>
    <div class = "page-info">
      <?php echo __l('Total Commission Amount = Bonus Amount + ((Discounted Price * Number of Minimum Buyers) * Commission Percentage/100))'); ?>
    </div>
    <div class = "clearfix">
      <div class = "amount-block commision-form-block">
        <?php
          echo $form->input ('bonus_amount', array ('label' => __l('Bonus Amount'), 'after' => Configure::read ('site.currency').' <span class = "info">' . __l('This is the flat fee that the company will pay for the whole deal.') . '</span>'));
          echo $form->input ('commission_percentage', array ('label' => __l('Commission Percentage'), 'info' => __l('This is the commission that company will pay for the whole deal in percentage.')));
          if (ConstUserTypes::isLikeAdmin ($auth->user ('user_type_id'))) {
            echo $form->input ('private_note', array ('type' =>'textarea', 'label' => __l('Private Note'), 'info' => __l('This is for admin reference. It will not be displayed for other users.')));
          }
        ?>
      </div>
      <div class = "calculator-block round-5">
        <?php echo $this->element ('../deals/commission_calculator', array ('cache' =>array ('time' => Configure::read ('site.element_cache'), 'key' => $auth->user ('id')))); ?>
      </div>
    </div>
    <div id = "multipleAttachmentBlock">
    <?php echo $form->input ('Attachment.filename', array ('type' => 'file', 'label' => __l('Product Image'))); ?>
    </div>
    <?php
      if (ConstUserTypes::isLikeAdmin ($auth->user ('user_type_id'))) {
        echo $form->input ('is_subscription_mail_sent', array ('label' => __l('Subscription Mail Sent to Users')));
      }
      if (!$is_subdeal) {
        echo $form->input ('description', array ('label' => 'Descripción - [ Más Información ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('review',      array ('label' => 'Revisión', 'type' => 'textarea', 'class' => 'js-editor'));
      } else {
        echo $form->input ('description', array ('label' => 'Descripción - [ Más Información ]', 'type' => 'textarea', 'class' => 'js-editor-ro'));
        echo $form->input ('review',      array ('label' => 'Revisión', 'type' => 'textarea', 'class' => 'js-editor-ro'));
      }
    ?>
    </fieldset>
    <fieldset class = "form-block">
    <legend><?php echo 'Cup&oacute;n'; ?></legend>
    <table>
      <tr>
        <td>
          <?php
            // TODO cambiar el input helper para que no haya que hacer esta aberracion de tabla...
            echo $tip->ayuda ('Especificar los dias de validez del cupon desde el momento en que se realiza la compra');
          ?>
        </td>
        <td>
          <?php echo $form->input ('coupon_duration', array ('label' => 'Dias de validez')); ?>
        </td>
      </tr>
    </table>
    <?php // if (!$is_subdeal) { ?>
      <!--
        <div class = "input clearfix required">
          <div class = "js-datetime">
            <?php // echo $form->input ('coupon_expiry_date', array ('label' => __l('Coupon Expiry Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
          </div>
        </div>
      -->
    <?php // } else { ?>
      <!--
        <div class = "input clearfix required">
          <div>
            <?php // echo $form->input ('coupon_expiry_date', array ('label' => __l('Coupon Expiry Date'), 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => __l('Please Select'), 'type' => 'text')); ?>
          </div>
        </div>
      -->
    <?php // } ?>
    <?php
      if (!$is_subdeal) {
        echo $form->input ('coupon_condition',  array ('label' => 'Condiciones - [ Condición de descuento ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('coupon_highlights', array ('label' => 'Destacados - [ Aspectos destacados ]', 'type' => 'textarea', 'class' => 'js-editor'));
        echo $form->input ('comment',           array ('label' => 'Comentarios', 'type' => 'textarea', 'class' => 'js-editor'));
      } else {
        echo $form->input ('coupon_condition',  array ('label' => 'Condiciones - [ Condición de descuento ]', 'type' => 'textarea', 'class' => 'js-editor-ro'));
        echo $form->input ('coupon_highlights', array ('label' => 'Destacados - [ Aspectos destacados ]', 'type' => 'textarea', 'class' => 'js-editor-ro'));
        echo $form->input ('comment',           array ('label' => 'Comentarios', 'type' => 'textarea', 'class' => 'js-editor-ro'));
      }
    ?>
    </fieldset>
    <fieldset class = "form-block">
    <legend><?php echo __l('SEO'); ?></legend>
    <?php
      echo $form->input ('meta_keywords', array ('label' => __l('Meta Keywords')));
      echo $form->input ('meta_description', array ('label' => __l('Meta Description')));
    ?>
    </fieldset>
    </fieldset>
    <script language = "JavaScript">
      <?php if (in_array ($auth->user ('user_type_id'), array (
                ConstUserTypes::Agency,
                ConstUserTypes::Company,
            ))) { ?>
        $("#DealHasPins").attr ("disabled", true);
        $("#DealIsTourism").attr ("disabled", true);
        $("#DealIsTourism_").attr ("disabled", true);
      <?php } ?>
      <?php if ($is_subdeal) { ?>
        $(document).ready (function () {
          $("#DealName").attr ("disabled", true);
          $("#DealCompanyId").attr ("disabled", true);
          $("#DealCityId").attr ("disabled", true);
          $("#DealSellerId").attr ("disabled", true);
          $("#DealClusterId").attr ("disabled", true);
          $("#DealStartDate").attr ("disabled", true);
          $("#DealEndDate").attr ("disabled", true);
          $("#DealIsSideDeal").attr ("disabled", true);
          $("#DealIsSideDeal_").attr ("disabled", true);
          $("#DealIsTourism").attr ("disabled", true);
          $("#DealIsTourism_").attr ("disabled", true);
          $("#DealPaymentSettings").attr ("disabled", true);
          $("#DealCustomSubject").attr ("disabled", true);
          $("#DealCustomCompanyName").attr ("disabled", true);
          $("#DealCustomCompanyContactPhone").attr ("disabled", true);
          $("#DealCustomCompanyAddress1").attr ("disabled", true);
          $("#DealCustomCompanyAddress2").attr ("disabled", true);
          $("#DealCustomCompanyCity").attr ("disabled", true);
          $("#DealCustomCompanyState").attr ("disabled", true);
          $("#DealCustomCompanyCountry").attr ("disabled", true);
          $("#DealCustomCompanyZip").attr ("disabled", true);
          $("#DealPriority").attr ("disabled", true);
          $("#DealRisk").attr ("disabled", true);
          $("#DealShowMap").attr ("disabled", true);
          $("#DealShowMap_").attr ("disabled", true);
          $("#DealHidePrice").attr ("disabled", true);
          $("#DealHidePrice_").attr ("disabled", true);
          $("#DealOnlyPrice").attr ("disabled", true);
          $("#DealOnlyPrice_").attr ("disabled", true);
          $("#add_image_button").attr ("disabled", true);
          $("#DealCouponExpiryDate").attr ("disabled", true);
          $("#DealMetaKeywords").attr ("disabled", true);
          $("#DealMetaDescription").attr ("disabled", true);
          $("#DealDealCategoryId").attr ("disabled", true);
          $("#DealCouponDuration").attr ("disabled", true);
          /* */
          $("#multipleAttachmentBlock").attr ("hidden", true);
        });
      <?php } ?>
    </script>
    <div class = "submit-block">
      <?php echo $form->submit (__l('Update'), array ('name' => 'data[Deal][send_to_admin]')); ?>
      <?php if (ConstUserTypes::isLikeAdmin ($auth->user ('user_type_id'))) { ?>
      <div class = "cancel-block">
        <?php echo $html->link (__l('Cancel'), array ('controller' => 'deals', 'action' => 'index', 'admin' => true), array ('class' => 'cancel-button')); ?>
      </div>
      <?php } else { ?>
      <?php echo $form->submit (__l('Update Draft'), array ('name' => 'data[Deal][save_as_draft]')); ?>
      <div class = "cancel-block">
        <?php echo $html->link (__l('Cancel'), array ('controller' => 'deals', 'action' => 'index', 'admin' => false), array ('class' => 'cancel-button')); ?>
      </div>
      <?php } ?>
    </div>
  <?php echo $form->end ();  ?>
</div>