<div class="side1">
  <?php if ($deal['Deal']['deal_status_id'] == ConstDealStatus::Open || $deal['Deal']['deal_status_id'] == ConstDealStatus::Tipped) {
  ?>
    <div class="share">
      <span><?php echo __l('Share: '); ?></span>
      <ul>
      <?php $bityurl = $deal['Deal']['bitly_short_url_prefix']; ?>
      <li><?php echo str_replace('##DEAL_URL##', urlencode(Router::url('/', true) . 'deal/' . $deal['Deal']['slug']), Configure::read('facebook.like_button_code')); ?></li>
      <li><?php echo $html->link(__l('Tweet!'), 'http://www.twitter.com/home?status=' . urlencode($deal['Deal']['name'] . ' - ') . $bityurl, array('target' => 'blank', 'title' => __l('Tweet this deal'), 'class' => 'tweet')); ?></li>
      <li><?php echo $html->link(__l('Quick! Email a friend!'), 'mailto:?body=' . __l('Check out the great deal on ') . Configure::read('site.name') . '-' . Router::url('/', true) . 'deal/' . $deal['Deal']['slug'] . '&amp;subject=' . urlencode(__l('I think you should get ') . Configure::read('site.name') . __l(': ') . $deal['Deal']['discount_percentage']), array('target' => 'blank', 'title' => __l('Send a mail to friend about this deal'), 'class' => 'quick')); ?></li>
    </ul>
  </div>
  <?php } ?>
    <div class="block1 topic-discussion">

        <?php if(!empty($deal['Deal']['name'])) : ?>
          <h2 class="title">
                            <?php echo $html->link($deal['Deal']['name'], array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), array('title' => sprintf(__l('%s'), $deal['Deal']['name']))); ?>
          </h2>
          <p class="price">
            <?php
              if ($deal ['Deal']['hide_price']) {
                echo '$$$';
              } else {
                echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price'], 'small');
              }
            ?>
          </p>

                          <?php
                          if ($html->isAllowed($auth->user('user_type_id')) && $deal['Deal']['deal_status_id'] != ConstDealStatus::Draft && $deal['Deal']['deal_status_id'] != ConstDealStatus::PendingApproval) {
                            if ($deal['Deal']['deal_status_id'] == ConstDealStatus::Open || $deal['Deal']['deal_status_id'] == ConstDealStatus::Tipped) {
                              echo '<div class="btns">';
                              if ($city_slug == 'circa') {
                                echo $html->link('', 'javascript:void(0)', array('onclick' => 'modalPopupGeneric("' . Router::url(array('controller' => 'firsts', 'action' => 'circa', $deal['Deal']['id']), false) . '", 810, 301);', 'escape' => false, 'title' => __l('Buy'), 'class' => 'button-anchor'));
                              } else {
                                echo $html->link(__l('Buy'), array('controller' => 'deals', 'action' => 'buy', $deal['Deal']['id']), array('title' => __l('Buy'), 'class' => 'button-anchor'));
                              }
                              echo '    </div>';
                            } else {
                              echo '<div style="background-color: #FFC410; color: #000;" class="btns">';
                              echo '<strong  class="no-more-available"> '.  __l('No Longer Available') . '</strong>';
                              echo '    </div>';
                            }
                          }
                          ?>

          <?php else: ?>
            <h2 class="title">
              <?php echo $deal['Topic']['name'] ?>
            </h2>
        <?php endif; ?>
  </div>
</div>
