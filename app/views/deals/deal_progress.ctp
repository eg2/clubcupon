<?php
    if (!empty ($deal) && $deal ['Deal']['min_limit'] > $deal ['Deal']['deal_user_count']) {
        $pixels = round (($deal ['Deal']['deal_user_count'] / $deal ['Deal']['min_limit']) * 100);
?>
        <div class = "">
            <?php if ($deal['Deal']['deal_user_count'] != 0) { ?>
                <span><?php echo $deal ['Deal']['deal_user_count']; ?></span> compradas
            <?php } else { ?>
                <span>&iexcl;S&eacute; el primero en comprarlo!</span>
            <?php } ?>
        </div>

        <div>
            <div style = "left: <?php echo $pixels; ?>px;"  class = ""></div>
            <div>
                <div></div>
                <div style = "width: <?php echo $pixels; ?>px;"  class = ""></div>
            </div>
            <span>0</span>
            <span><?php echo $deal ['Deal']['min_limit']; ?></span>
        </div>
        <p>
            <span>
                <?php echo $deal ['Deal']['min_limit'] - $deal ['Deal']['deal_user_count']; ?>
            </span>
            m&aacute;s para que la oferta este en marcha	
   
        </p>
    <?php } else { ?>
        
        <div class = "deal-bought-block">
            <h5 class = "deal-bought"><span><?php echo $deal ['Deal']['deal_user_count']; ?></span> compradas</h5>
            <p class = "deal-start">&iexcl;La oferta est&aacute; en marcha!</p>
            <p class = "tipped-info">
                En marcha desde <?php echo $html->cDateTimeHighlight ($deal ['Deal']['deal_tipped_time']) . ' con ' . $deal ['Deal']['min_limit']; ?> compradas.
            </p>
        </div>
<?php  } ?>