<?php echo $this->element ('js_tiny_mce_setting', array ('cache' => Configure::read ('tiny_mce_cache_time.listing_element_cache_duration'))); ?>


<style>
    #edicion-oferta-cc-ya{
        padding:15px;
    }

    #edicion-oferta-cc-ya h1 {color: #F59D0F!important; font-size:30px; margin-bottom:10px;}
    #edicion-oferta-cc-ya h2 {font-size:25px!important;}
    #edicion-oferta-cc-ya h3 {background:#ccc; color: #777!important; padding:5px!important; border-top: 1px solid #999; font-size:18px!important; text-transform:none!important;}

    #edicion-oferta-cc-ya label{
        width:200px;
        text-align:right;
    }

    #edicion-oferta-cc-ya .seo label{
        width:200px;
        float:left!important;
        text-align:right;
    }

    #edicion-oferta-cc-ya #vencimiento{
        clear:both;
        padding:15px 0;
        text-align: center;
    }

    #edicion-oferta-cc-ya #vencimiento p{
        background:#ccc;
        font-size: 20px;
        padding:15px;
        clear:both;
        color: #666;
        font-weight:bold;
    }

    #edicion-oferta-cc-ya .checkbox{
        width:250px!important;
        margin-top:15px!important;
    }

    #edicion-oferta-cc-ya .grupo{}

    #edicion-oferta-cc-ya .cierre{
        clear:both;
    }

    #edicion-oferta-cc-ya #DealPaymentSettings{
        margin-bottom:10px;
    }

    #edicion-oferta-cc-ya .payment-setting-group-selector{
        width:400px;
    }

    #edicion-oferta-cc-ya #DealMetaDescription{
        float:none;
    }

    #edicion-oferta-cc-ya #DealMetaDescription{
        float:none;
    }

    #edicion-oferta-cc-ya #medios_pago_alerta{
        clear:both;
        padding:0!important;
        margin:0!important;
    }

    #edicion-oferta-cc-ya .datos-cupon label{
        font-weight:bold;
    }

	.error-message{
		float:left;
	}




 .contedor-hora {
    border: 1px solid #DDDDDD;
    margin-top: 5px;
    padding: 10px;
    clear: both;
    float: none;
    height: auto;
    margin-bottom: 5px;

}


        #CalendarToggler{
            border: 1px solid #DDDDDD;
            margin-top: 5px;
            clear: both;
            float: none;
            height: auto;
            margin-bottom: 5px;
            color:#0098CA;
            font-weight: bold;
            margin-top: 20px;
            font-size: 12px;
        }



 .validez {
       height: 160px;
    margin-top: 30px;
    float: left;
    width: 300px;
}
.grupoinp label {
    clear: both;
    display: inline;
    float: left;
    margin: 5px 5px 0;
    min-height: 20px;
    text-align: left !important;
    width: auto !important;
}

.grupoinp span {
    border: 0 none;
    font-size: 100%;
    margin: 0;
    outline: 0 none;
    padding: 0;
    vertical-align: baseline;
    color: #F57E22;
    display: block;
    float: right;
    font: bold 17px arial;
    margin: 0 0 0 3px;
}

#collapsable .grupoinp select {
    border: 1px solid #DDDDDD;
    clear: both;
    float: left;
    padding: 5px;
    text-align: left;
    width: 200px;
}

.grupoinp span {
    border: 0 none;
    font-size: 100%;
    margin: 0;
    outline: 0 none;
    padding: 0;
    vertical-align: baseline;
}
.grupoinp .sprite {
    background-image: url("/img/now//img/now/img/theme_clean/sprite.png");
    background-position: 0 -1471px;
    float: left;
    height: 30px;
    line-height: 30px;
    width: 60px;
}
.grupoinp span {
    color: #F57E22;
    display: block;
    float: right;
    font: bold 17px arial;
    margin: 0 0 0 3px;
}
 .validez p {
    color: #8E8E8E;
    float: left;
    font: 12px arial;
    margin: 5px;
}

p.info {
    font: 11px arial;
    margin: 0 0 20px 5px;
    width: 295px;
    padding-left:0 !important;
    color: #8E8E8E;
    float: left;
    font: 12px arial;
    margin: 5px;
}

#collapsable label{
    font-size: inherit !important;
    margin: none !important;
    min-width: auto !important;
    padding: none !important;
}

#collapsable  .checkbox {
    margin-top: 0 !important;
    width: auto !important;
}
</style>

<script>

    $(document).ready(function() {
        //alert('ready!');
    });

</script>

<div id = "edicion-oferta-cc-ya">

    <h1>Editar Oferta</h1>

    <?php
        echo $form->create ('NowDeal', array ('url'=>array('controller' => 'deals', 'action' => 'audit_now_deal'), 'controller'=>'Deals', 'plugin'=>'','class' => 'normal', 'enctype' => 'multipart/form-data'));
        //echo $form->create ('Deal', array ('action' => 'audit_now_deal', 'controller'=>'Deals', 'plugin'=>'','class' => 'normal', 'enctype' => 'multipart/form-data'));
    ?>

    <div class ="general grupo">
        <h3>General</h3>
        <?php

            //Nombre
            echo $form->input ('name',                      array ('label' => 'Nombre'));

            //Imagen principal
            echo $form->input ('Attachment.filename',      array ('label' => 'Imagen principal del producto', 'type' => 'file', 'info'=> 'Tama&ntilde;o sugerido 437x264 p&iacute;xeles. S&oacute;lo im&aacute;genes en formato jpg, jpeg, gif, png o bmp'));


            //Imagen adjuntada
            if($this->data ['Attachment']){
                echo $html->showImage ('Deal', $this->data ['Attachment'],  array ('dimension' => 'normal_thumb' ));
            }

            //Categoria
            echo $form->input ('deal_category_id',          array (
                                                                'type'    => 'select',
                                                                'options' => $deal_categories,
                                                                'empty'   =>  false,
                                                                'label'   => 'Categor&iacute;a',
                                                                'escape' => false,
                                                            ));

            //Fecha Inicio

            echo'<div class = "input clearfix required"><div class = "">';

            echo $form->input ('start_date', array ('label'=>'Fecha de inicio','div'=>null,'readonly'=>'readonly','minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'type' => 'text', 'value'=> $start_date));
            echo '</div></div>';
            echo '<div class="notification">';
            echo '<p>Formato: (AAAA-MM-DD) - La oferta puede ser comprada durante este d&iacute;a (desde las 00.00 hs hasta las 23.59 hs).</p>';
            echo '</div>';

            echo '<div class="cierre">&nbsp;</div>';

            echo '<div class="grupoinp horainicio">';
            echo $form->input('time_range_start', array(
                                        'label' => 'Hora de inicio',
                                        'options' => array_slice(NowTimeSpanOptions::horas(),0,-1,true),
                                        'type' => 'select',
                                        'class' =>"sel sel-rempresas",
                                        'div' => null,
                                        ), false
                                    );
            echo '</div>';

            echo '<div class="grupoinp horafin">';
            echo $form->input('time_range_end', array(
										'label' => false,
										'options' => array_slice(NowTimeSpanOptions::horas(),1,null,true),
										'type' => 'select',
										'class' =>"sel sel-rempresas",
										'div' => null,

                                        ), false
									);
            echo '</div>';
            ?>
            <?php
                                                $display_canlendarizacion = (Configure::read('NowPlugin.deal.enable_sheduled_deals')== true) ? 'display:block;':'display:none;';
                                                ?>
<fieldset class="contedor-hora" style="height:auto;float:none;margin-bottom: 5px;clear:both;<?php echo $display_canlendarizacion?>">
						<legend>Calendarizaci&oacute;n (<a href="javascript:void(0)" id="CalendarToggler" style="font-size:12px;">
  habilitar
                                                    </a>)</legend>
                                                <div  id="collapsable" style="width:610px;display: none;">
                                                <div class="grupoinp validez" style="margin-top:30px;height:200px;">
								<label>Fecha Final de publicaci&#243;n<span>*</span></label>
								<?php echo $form->input ('scheduled_deals_end_date', array ('error'=>false,'label'=>false,'div'=>null,'readonly'=>'readonly','minYear' => date ('Y'),  'maxYear' => date ('Y') + 10, 'type' => 'text')); ?>
								<span class="sprite"></span>
								<?php if(isset($this->validationErrors['NowDeal']['scheduled_deals_end_date'])){ // VALIDACION POSICIONADA A MANO! SINO PINCHA EL LAYOUT!?>
								<div class="error-message"><?php echo $this->validationErrors['NowDeal']['scheduled_deals_end_date'] ?></div>
								<?php }?>
								<p>Formato: (AAAA-MM-DD)</p><br>
								<p class="info f11">Tu oferta puede ser comprada hasta este d&#237;a </p>
						</div>


                                <div class="">

								<?php echo $form->input('scheduled_deals',
                                                                        array ('options' =>$dias ,
                                                                            'type'=>'select',
                                                                            'multiple'=>'checkbox',
                                                                            'legend' => false,
                                                                            'div' => 'weekdays',
                                                                            'selected' =>$dias_selected,
                                                                            'label' => 'Publicar los siguientes días',  )); ?>
                                    </div>
<div class="clearfix"></div>
                                                    <div class="grouplabel radio">
                                <?php
                                    echo $form->radio('scheduled_deals_is_disabled',
                                            array(0 =>'activa',1=>'en pausa'),
                                            array('legend'=>'Estado de calendarizacion'));
                                    ?>
                            </div>
                                                    </div>



                                </fieldset>
            <?
            echo '<div class="notification">';
            echo '<p>Esta es la franja horaria en la que el comprador podr&aacute; canjear el cup&oacute;n.</p>';
            echo '</div>';


            //echo '</div>';

            //Descripcion
            echo $form->input ('description',               array ('label' => 'Descripción - [ Más Información ]', 'type' => 'textarea'/*, 'class' => 'js-editor'*/));

            ?>

        <div class="cierre">&nbsp;</div>
    </div>

    <div class ="precio grupo">
        <h3>Precio</h3>

        <?php

            //Comision
            echo $form->input ('commission_percentage',     array ('info' => ' Este es el porcentaje de la comisi&oacute;n que la compa&ntilde;&iacute;a va a pagar por la totalidad de la oferta ', 'label' => 'Comisión (%)'));

            //precio original
            echo $form->input ('original_price',            array ('label' => 'Precio original'));

            //Precio con descuento
            echo $form->input ('discounted_price',          array ('label' => 'Precio con descuento', 'type' => 'text'));

            //% descuento
            echo $form->input ('discount_percentage',       array ('label' => 'Descuento (%)','readonly' => 'readonly'));

            //Descuento
            echo $form->input ('discount_amount',           array ('label' => 'Importe a descontar', 'readonly' => 'readonly'));

            //Ahorro
            echo $form->input ('savings',                   array ('label' => 'Ahorro', 'readonly' => 'readonly', 'type' => 'text'));

        ?>

        <div class="cierre">&nbsp;</div>
    </div>



    <div class ="datos-cupon grupo">
        <h3>Cupones</h3>

        <?php

            //Nro. max. de cupones por oferta
            echo $form->input ('max_limit',                 array ('label' => 'N&uacute;mero m&aacute;ximo de cupones por oferta'));

            //Cant. max. de cupones por persona
            echo $form->input ('buy_max_quantity_per_user', array ('label' => 'Cantidad m&aacute;xima de cupones por persona', 'info' => 'Cantidad de cupones que el usuario puede comprar para s&iacute; mismo. Dejar en blanco para ning&uacute;n l&iacute;mite.'));

        ?>

        <div class="cierre">&nbsp;</div>
    </div>
    <div class ="datos-branches grupo">
    <h3 >Sucursales</h3>
    <?php if(!@count($branches)){ ?>
            <div class="grouplabel">
                    <div class='error-message'>No hay Sucursales</div>
            </div>
    <?php }else{?>
            <?php if($form->isFieldError('branches')){ echo $form->error('branches');}?>
            <?php  echo $form->input ('company_id', array ('type' => 'hidden')); ?>
            <?php if(count($branches) == 1){ ?>
                    <div class="grouplabel">
                            <input id="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>" type="checkbox" value="<?php echo $branches[0]['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
                            <label for="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>"><?php echo $branches[0]['NowBranch']['name'];?></label>
                    </div>
            <?php }else{?>
                    <?php foreach($branches as $sucursal){?>
                    <div class="grouplabel checkbox ">
                            <?php if(isset($this->data['NowDeal']['branches'])){ ?>
                            <?php $chkd = (in_array($sucursal['NowBranch']['id'],array_values($this->data['NowDeal']['branches'])))?true:false;?>
                            <input id="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>"    type="checkbox" value="<?php echo $sucursal['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" <?php if($chkd){ ?>checked="checked"<?php }?> />
                            <?php }else{ ?>
                            <input id="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>" type="checkbox" value="<?php echo $sucursal['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
                            <?php }?>
                            <label for="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>"><?php echo $sucursal['NowBranch']['name'];?></label>
                    </div>
                    <?php }?>
            <?php }?>
    <?php }?>
    </div>
    <div class = "submit-block grupo">

            <?php

                echo $form->submit ('Guardar cambios', array ('div'=>false, 'class'=>'link-button-generic'));

                //Cancelar
                echo $html->link('Cancelar',           array ('plugin' => '',    'controller' => 'deals',     'action' => 'now_deals_management', 'admin' => true), array('class'=>'link-button-generic'), "¿Seguro querés Cancelar? Se pierden los datos no guardados" );

                if( $this->data ['NowDeal']['deal_status_id'] != ConstDealStatus::Closed )
                {
                    if($this->data ['NowDeal']['deal_status_id'] != ConstDealStatus::Rejected){
                        echo $html->link('Rechazar',         array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'rejectNowDeal',  'admin' => false, 'now_deal_id' => $this->data ['NowDeal']['id']), array('class'=>'link-button-generic'), "¿Seguro querés Rechazar esta oferta? Este cambio NO puede deshacerse" );
                    }

                    if($this->data ['NowDeal']['deal_status_id'] != ConstDealStatus::Draft ){
                        echo $html->link('Pasar a borrador', array ('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'draftNowDeal',   'admin' => false, 'now_deal_id' => $this->data ['NowDeal']['id']), array('class'=>'link-button-generic'), "¿Seguro querés pasar a Borrador esta oferta?" );
                    }

                }


            ?>


    </div>

    <?php echo $form->end (); ?>

</div>
<script type="text/javascript">


     function toggleOnCalendarRadioOn()
    {
        var fecha = new Date;


        <?php if(isset($js_end_date) ){ ?>
        $("#NowDealScheduledDealsEndDate").val('<?php echo $js_end_date;?>');
        <?php } else { ?>
        $("#NowDealScheduledDealsEndDate").val(fecha.getFullYear()+"-"+(("0" + (fecha.getMonth() + 1)).slice(-2))+"-"+("0" + fecha.getDate()).slice(-2));
        <?php } ?>
        $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('disabled',false);
     //   $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('checked',true);

        $("#NowDealScheduledDealsEndDate").removeAttr('disabled');


    }
    function toggleOnCalendarRadioOff()
    {
        $("#NowDealScheduledDealsEndDate").val('');
        $("#NowDealScheduledDealsEndDate").attr('disabled',true);

     //   $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('checked',false);
        $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('disabled',true);


    }



        $('#CalendarToggler').click(function(){
            $('#collapsable').slideToggle("slow");

            if($("#NowDealScheduledDealsEndDate").val()==""){
                toggleOnCalendarRadioOn()

                $(this).text('deshabilitar');
            } else {
                $(this).text('habilitar');
                toggleOnCalendarRadioOff()

            }


        });



    $(document).ready(function(e){


   $("#NowDealStartDate" ).datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
   $("#NowDealScheduledDealsEndDate" ).datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});

        <?php

        if(isset($js_end_date)||  isset($this->validationErrors['NowDeal']['scheduled_deals_end_date'])){ ?>
            $('#collapsable').slideToggle("slow");
            toggleOnCalendarRadioOn()
                $('#CalendarToggler').text('deshabilitar');
<?php } ?>

        $('#NowDealTimeRangeEnd').change(function(e){
			if(parseInt($('#NowDealTimeRangeStart').val()) > parseInt($('#NowDealTimeRangeEnd').val())){
				$('#NowDealTimeRangeStart').val($('#NowDealTimeRangeEnd').val());

			}

		});

		$.isNumeric = function(number){
			if(number.match(/[^0-9\.]/)!=null) return false;
			return true;
		}

		$('#NowDealOriginalPrice, #NowDealDiscountedPrice').bind('blur',function(e){
			if($.isNumeric($(this).attr('value'))){
				$(this).attr('value',parseInt($(this).attr('value'),10));
				$(this).css('border','1px #DDD solid');
			}else if($(this).attr('value') == ""){
				$(this).css('border','1px #DDD solid');
			}else{
				$(this).attr('value','');
				$(this).css('border','1px red solid');
			}
			if($('#NowDealOriginalPrice').attr('value') != "" && $('#NowDealDiscountedPrice').attr('value') != ""){
				if($.isNumeric($('#NowDealOriginalPrice').attr('value')) && $.isNumeric($('#NowDealDiscountedPrice').attr('value'))){
					var op = parseInt($('#NowDealOriginalPrice').attr('value'),10);
					var dp = parseInt($('#NowDealDiscountedPrice').attr('value'),10);

					$('#NowDealOriginalPrice').attr('value',op);
					$('#NowDealDiscountedPrice').attr('value',dp);

					if(op<=dp || op == 0 || dp == 0){
						$('#NowDealOriginalPrice').css('border','1px red solid');
						$('#NowDealDiscountedPrice').css('border','1px red solid');
						$('#NowDealDiscountPercentage').attr('value','');
						$('#NowDealDiscountAmount').attr('value','');
						$('#NowDealSavings').attr('value','');
					}else{
						$('#NowDealOriginalPrice').css('border','1px green solid');
						$('#NowDealDiscountedPrice').css('border','1px green solid');
						$('#NowDealDiscountPercentage').attr('value',(100-parseInt(((dp*100)/op),10)));
						$('#NowDealDiscountAmount').attr('value',(op-dp));
						$('#NowDealSavings').attr('value',((dp-op)*-1));
					}
				}
			}else{
					$('#NowDealDiscountPercentage').attr('value','');
					$('#NowDealDiscountAmount').attr('value','');
					$('#NowDealSavings').attr('value','');
			}
		});


                $('#NowDealEditForm').submit(function(e){
                    $('input[name="data[NowDeal][ScheduledDeals][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][scheduled_deals][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos un día!");
			}
		});

                });

                $('input[name="data[NowDeal][branches][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][branches][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos una sucursal!");
			}
		});




                $('input[name="data[NowDeal][scheduled_deals][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][scheduled_deals][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos un día!");
			}
		});


    });
</script>

<style>
    form .info{
        clear:both;
        padding-left:215px;
    }

</style>