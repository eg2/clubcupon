<center>
    <div id = "wrapper_top">
        <?php echo $html->image ('landing-full/logo_clubcupon.png', array ('width' => 152, 'height' => 67)); ?>
    </div>
    <div id = "bg2">
        <?php echo $html->image ('landing-full/fondo_belleza.jpg', array ('width' => 1118, 'height' => 662)); ?>
    </div>
    <div id = "wrapper_all">
        <div id = "wrapper_content">
            <div class = "bg_transparente">
                <div id = "content_all">
                    <div class="tit">
                        <?php
                        $array = array("aceites esenciales", "anticelulíticos", "aromaterapia", "centros de belleza", "cirugías estéticas", "clínica de estética", "dermoestética", "cosmética", "cremas de belleza", "cremas humectantes", "day spa pilar", "depilación", "depilación definitiva", "depilación láser", "dermoestética", "dietas", "dietas para adelgazar", "dietas para perder peso", "electroterapia", "estancias", "estética", "estética corporal", "estética en argentina", "estilistas", "itness", "gimnasios", "hotel spa", "hotel spa en capital federal", "magnetoterapia", "manicura", "maquillaje", "maquillaje profesional", "masaje reductivo", "masajes", "masajes antiestrés", "masajes en argentina", "masajes en capital federal", "masajes reductores", "masajes relajantes", "masoterapia", "medicina estética", "mesoterapia", "moda", "pedicuría", "peinados", "peinados para bodas", "peinados para fiestas", "peluquería", "perfumerías", "personal trainer", "productos de belleza", "productos de tocador", "reflexología", "reiki", "relajación", "relax", "salón de belleza", "spa", "spa en argentina", "spa zona belgrano", "spa en capital federal");
                        if(isset ($_GET ['kw']) && in_array($_GET['kw'], $array)){
                            echo $_GET['kw'];
                        } else {
                            echo "OPORTUNIDADES &Uacute;NICAS";
                        }
                        ?>
                    </div>
                    <div align = "left">
                        <?php echo $html->image ('landing-full/descuento.png', array ('width' => 528, 'height' => 37, 'alt' => 'descuente de hasta el 80%')); ?>
                    </div>
                    <div align = "left"  style = "padding-top: 20px;">
                        <?php echo $html->image ('landing-full/elegi_estar_divina_siempre.png', array ('width' => 639, 'height' => 118)); ?>
                    </div>
                    <div class = "formulario">
                        
                        
                        <?php
                        
                            echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform'));
                                echo '<div class = "form">';
                                    echo '<div class = "campo_email">';

                                        echo '<div class = "input_text_required">';
                                            //echo '<label for = "SubscriptionEmail"></label>';
                                            echo $form->input ('email',         array('value' => 'Ingres&aacute; tu mail', 'label'=>false, 'escape'=>false, 'div'=>false, 'onclick' => "this.value == 'Ingres&aacute; tu mail' ? this.value = '' : '';", 'id' => "SubscriptionEmail", 'div'=>false));
                                            echo $form->hidden('from',          array('value' => 'landing'));
                                            echo $form->hidden('utm_source',    array('value' => 'AdWords'));
                                            echo $form->hidden('city_id',       array('value' => Configure::read ('Actual.city_id')));
                                            echo $form->hidden('landing',       array('value' => 'belleza'));
                                        echo '</div>';
                                    echo '</div>';
                                    echo '<div class = "submit">';
                                        echo $form->submit(' ', array('class'=>'bt_suscribirme', 'div'=>false));
                                    echo '</div>';
                                    echo '<div class = "call">';
                                        echo '<small>No divulgaremos tu direcci&oacute;n de correo.</small>';
                                    echo '</div>';

                                    echo '<div class="regis"><a href="http://www.clubcupon.com.ar/?from=affiliate">Ya estoy suscripto</a></div>';

                                echo '</div>';
                            echo $form->end();
                        
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>