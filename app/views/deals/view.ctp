<!--[if IE]>
    <style>
        .menu-oferta li{ margin-bottom:-5px;}
    </style>
<![endif]-->


<!--[if IE 7]>
    <style>
        #home-v2-exclusive .content-der{margin-left:0!important;}
        .submit{padding:8px 5px 7px 5px!important;}
    </style>
<![endif]-->
<?php
echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection'); 
?>
<!-- mastercard oferta -->
<?php
    if( $deal['Deal']['is_tourism'] ) {
        $mainDealIsTourism = 'si';
    } else {
        $mainDealIsTourism = 'no';
    }
    echo $this->element(Configure::read('theme.theme_name') . 'e-planning-background', array('mainDealIsTourism'=> $mainDealIsTourism) );
    
    if( $is_corporate_city ){
        echo'<div id="home-v2-exclusive">';
        echo $html->css(Configure::read('theme.asset_version') . 'exclusive');
    } else {
        echo '<div id="home-v2">';
    }
    
?>

    <!-- HEAT MAP -->
    <!-- ClickTale Top part -->
    <script type="text/javascript">
    var WRInitTime=(new Date()).getTime();
    </script>
    <!-- ClickTale end of Top part -->
    <!-- FIN HEAT MAP -->

    <?php if( $is_corporate_city && !$is_corporate_guest ){ ?>
    <style>
        body{background:#181818!important;}
        #main, #header{visibility:hidden;}
        #cboxContent,
        #cboxMiddleLeft,
        #cboxMiddleRight,
        #cboxTopLeft,
        #cboxTopCenter,
        #cboxTopRight,
        #cboxBottomLeft,
        #cboxBottomCenter,
        #cboxBottomRight,
        #cboxLoadedContent{margin:0!important;background:#000;}
        #cboxClose{height:0; visibility:hidden;}
        #cboxLoadedContent{background:#000;}

    </style>
    <?php } ?>

    <?php if($is_corporate_city && $deal ['Deal']['hide_price']){ ?>
        <style>
            .col-izq .modulos-compra ul.precio{
                background:url("/img/theme_clean/exclusive/gb-grad2.jpg") repeat scroll 3px 0 transparent!important;
            }
        </style>
    <?php } ?>

    <?php
        //Esta variable me determina si puedo mostrar o no el boton de COMPRAR
        $isAccesibleForBuy = $deal['Deal']['isAccesibleForBuy'];
        $image = $html->showImage('Deal', $deal['Attachment'], array('id' => 'deal_image', 'dimension' => 'medium_big_thumb', 'alt' => $html->cText($deal['Company']['name'] . ' ' . $deal['Deal']['name'], false), 'title' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)));
    ?>

    <?php if(!Configure::read('debug')){ // En debug 1 y 2 el script asincronico falla por cuando el Framework le attacha la toolbar del debuger la atacha al script!!! en vez de al tag de body del sitio!! ?>
    <script language="JavaScript" type="text/javascript">
        <!--
        var inIframe = false;
        var siW = self.innerWidth;
        var siH = self.innerHeight;
        var dE = document.documentElement;
        var dB = document.body;
        var sw = (siW ? siW : (dE && dE.clientWidth) ? dE.clientWidth : dB ? dB.clientWidth : "");
        var sh = (siH ? siH : (dE && dE.clientHeight) ? dE.clientWidth : dB ? dB.clientHeight : "");
        var wh = (sw && sh) ? (sw.toString(16) + "x" + sh.toString(16)) : "";
        var eplRnd = (new String(Math.random())).substring(2, 8) + (((new Date()).getTime()) & 262143);
        
        var eplSeccionId = 'Ficha';
        var ua = navigator.userAgent.toLowerCase();
        var av = navigator.appVersion.toLowerCase();
        var eplB = new Object();
        eplB.is_mac = (ua.indexOf("mac") != -1);
        eplB.is_win = (!eplB.is_mac && ((ua.indexOf("win") != -1) || (ua.indexOf("16bit") != -1)));
        var ieP = av.indexOf('msie');
        var ffP = ua.indexOf("firefox");
        eplB.is_o = (ua.indexOf("opera") != -1);
        eplB.is_ie = (ieP != -1 && eplB.is_win && !eplB.is_o);
        eplB.is_ff = (ffP != -1);
        if (eplB.is_ie) {
            eplB.minV = parseFloat(av.substring(ieP + 5, av.indexOf(';', ieP)));
            eplB.majV = parseInt(eplB.minV);
        }
        if (eplB.is_ff) {
            if (ua.length > ffP + 8) eplB.majV = parseInt(ua.substring(ffP + 8));
            if (ua.length > ffP + 10) eplB.minV = parseInt(ua.substring(ffP + 10));
        }
        eplB.ffOk = (eplB.is_ff && ((eplB.majV >= 1 && eplB.minV >= 5) || (eplB.majV >= 2)));
        eplB.ieOk = (eplB.is_ie && eplB.minV >= 5);
        var eplFV = 0;
        var nmT = navigator.mimeTypes;
        var eplFV = 0;
        var nmT = navigator.mimeTypes;
        var swF = 'application/x-shockwave-flash';
        var isWinIE = eplB.is_ie && eplB.is_win;
        var fP = (nmT && nmT[swF]) ? nmT[swF].enabledPlugin : 0;
        var fvT;
        var cs = document.charset || document.characterSet;
        if (fP) {
            var nP = navigator.plugins;
            var sf = "Shockwave Flash";
            if (nP[sf] != null) fvT = nP[sf].description;
        } else if (isWinIE) {
            var e;
            try {
                var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
                axo.AllowScriptAccess = "always";
                fvT = axo.GetVariable("$version");
            } catch (e) {}
        }
        if (fvT) {
            var fvW = fvT.split(" ");
            for (var i = 0; i < fvW.length; ++i) {
                if (isNaN(parseInt(fvW[i]))) continue;
                eplFV = fvW[i];
            }
        }



        function eplADU(eID, t) {
            var wID = '9b30';
            var sID = 'i';
            var adU = 'http://ads.e-planning.net/eb/3/' + wID + '/' + sID + '/' + eID + '?o=' + t + '&vcha\n\
    =' + eplFV + (wh ? '&n=' + wh : '') + '&rnd=' + eplRnd + '&crs=' + cs;
            return adU;
        }



        function eplAD(eID, w, h) {
            if (eID) {
                if (w && h) {
                    var adU = eplADU(eID, 'f');
                    document.write('<iframe id="' + eID + '_static" width="' + w + '" height="' + h + '" frameborder="0" scrolling="no" src="' + adU + '"></iframe>');
                } else {
                    var adU = eplADU(eID, 'j');
                    var iDT = '<html><head><title>Ad</title></head><body id="' + eID + '" leftmargin="0" topmargin="0" style="margin:0px; padding:0px; background-color:transparent;">';
                    iDT += '<div id="eplParentContainer' + eID + '"></div><scr' + 'ipt type="text/javascript">var inIframe=true;</scr';
                    iDT += 'ipt> <scr' + 'ipt type="text/javascript" src="' + adU + '"></scr';
                    iDT += 'ipt> <scr' + 'ipt> setTimeout("document.close()", 8000); </scr' + 'ipt></body></html>';
                    if (eplB.ieOk) {
                        document.write('<iframe id="' + eID + '" src="about:blank" width="1" height="0" frameborder="0" scrolling="no" allowtransparency="allowtransparency"></iframe>');
                        document.frames[eID].document.write(iDT);
                    } else if (eplB.ffOk) {
                        document.write('<iframe id="' + eID + '" src="about:blank" width="1" height="0" frameborder="0" scrolling="no"></iframe>');
                        var i = document.getElementById(eID);
                        if (i != null) i.contentDocument.write(iDT);
                    } else {
                        document.write('<scr' + 'ipt type="text/javascript" src="' + adU + '"></scr' + 'ipt>');
                    }
                }
            }
        }
        //-->
    </script>
    <?php }else{ // Para que no fallen las llamadas defino la funcion y logueo los argumentos!!?>
        <script language="JavaScript" type="text/javascript">
                function eplAD() {console.log(arguments);}
        </script>
    <?php }?>

    <div id="main" class="container_16 main_frt">
           <!-- Banner publicitario 16 columnas -->
            <div class="grid_16 banner_a">
                
                <?php if(false/*$is_corporate_city*/) { ?>
                
                    <!-- e-planning v3 - Comienzo espacio ClubCupon _ Corporate _ Top_940 -->
                    <script type="text/javascript" language="JavaScript1.1">
                    <!--
                    var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
                    var cs = document.charset || document.characterSet;
                    document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/Corporate/Top_940?o=j&rnd=' + rnd + '&crs=' + cs + '&kw_ciudades=<?php echo Configure::read ('Actual.city_slug'); ?>&kw_turismo=<?php echo $mainDealIsTourism; ?>&kw_idaviso=<?php echo $deal['Deal']['id']; ?>"></scr' + 'ipt>'); 
                    //-->
                    </script>
                    <noscript><a href="http://ads.e-planning.net/ei/3/9b30/Corporate/Top_940?it=i&rnd=$RANDOM" target="_blank"><img alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/Corporate/Top_940?o=i&rnd=$RANDOM&kw_ciudades=<?php echo Configure::read ('Actual.city_slug'); ?>&kw_turismo=<?php echo $mainDealIsTourism; ?>&kw_idaviso=<?php echo $deal['Deal']['id']; ?>" border=0></a></noscript>
                    <!-- e-planning v3 - Fin espacio ClubCupon _ Corporate _ Top_940 -->

                <?php }elseif(false){ ?>
                
                    <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ Top_940 -->
                    <script type="text/javascript" language="JavaScript1.1">
                    <!--
                    var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
                    var cs = document.charset || document.characterSet;
                    document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/a09dd7a82d90d544?o=j&rnd=' + rnd + '&crs=' + cs + '&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>"></scr' + 'ipt>');
                    //-->
                    </script>
                    <noscript><a href="http://ads.e-planning.net/ei/3/9b30/a09dd7a82d90d544?it=i&rnd=$RANDOM" target="_blank"><img alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/Ficha/a09dd7a82d90d544?o=i&rnd=$RANDOM&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>" border=0></a></noscript>
                    <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ Top_940 -->
            
                <?php } ?>
                
            </div>
            <!-- / Banner publicitario 16 columnas -->
  
        <!--   Ofertas relacionadas cuando la ppal essta agotada -->
        <?php
            if(!$isAccesibleForBuy && !$is_corporate_city){
                //Este bloque solo debe aparecer con cualquier oferta
                echo $this->element('theme_clean/upper_secondary_deals', $upper_secondary_deals_parameters);
            }
        ?>
        <!-- / Ofertas relacionadas cuando la ppal essta agotada -->


        <?php
        //muestro el formulario de suscripcion si esta AGOTADA la oferta
        if(!$isAccesibleForBuy   && !$city_is_group && !$is_corporate_city){
        //    if(!$isAccesibleForBuy && !$city_is_group){
        ?>
        <div class="grid_16 banner_a">
            <div class="recibi-alertas">

                <?php echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top')); ?>



                    <label>
                            <p>No te pierdas otra oferta.<span>Recib&iacute; alertas por email</span> </p>
                            <?php echo $form->input('email', array('type' => 'text', 'class'=>'text','tabindex'=>1,  'default'=>'Ingresá tu mail', 'label'=>false, 'div'=>false)); ?>
                            <?php echo $form->hidden('city_id', array('value'=>Configure::read ('Actual.city_id'))); ?>
                            <input id="susc"  class="submit" type="submit" value="Suscribirme!" />
                            <?php  //echo $form->submit(array('value'=>'Suscribirme', 'class'=>'submit', 'name'=>'buscar submit', 'div' =>false)); ?>
                            <?php  echo $form->end(); ?>

                    </label>
                    <br>
                    <label style="padding-left:45px;font-size:13px">
                    	<strong>&#191;Ten&eacute;s cuenta en Facebook? usala para suscribirte en ClubCup&oacute;n
							<?php 
        					echo $html->link($html->image('/img/theme_clean/fb-btn.jpg'), '#', array('rel'=> 'nofollow','title' => 'fconnect', 'id' => 'facebookSubscribeButton'), null, false);
        					?> 
						</strong>
                    </label>
                    
            </div>
        </div>
        <?php } ?>


        <!-- Cuerpo de la oferta PPAL -->
        <div class="grid_12">


            <div class="bloque-home clearfix">

                <h1 class="h1-format">
                    <?php
                        $deal_name = (strlen($deal['Deal']['name']) > 160) ? mb_strcut($deal['Deal']['name'],0,160).'...' : $deal['Deal']['name'];
                        echo $html->link( $deal_name, array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), array('title' => sprintf('%s', $deal['Deal']['name'])));
                    ?>
                </h1>

                <?php if(!empty($deal['Deal']['subtitle'])){ ?>
                <h2 class="subtitle">
                    <?php
                        $deal_name = (strlen($deal['Deal']['subtitle']) > 63) ? substr($deal['Deal']['subtitle'],0,60).'...' : $deal['Deal']['subtitle'];
                        echo $html->link( $deal['Deal']['subtitle'], array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), array('title' => sprintf('%s', $deal['Deal']['subtitle']) , 'escape' => false));
                    ?>
                </h2>
                <?php } ?>

                <div class="grid_4 alpha omega col-izq" id="buySideBar">

                    <img class="bg-img" src="/img/theme_clean/bg-compras-rib.png" alt=""/>

                        <!-- BOTON DE COMPRA NUEVO-->

                        <div class="boton-comprar-bg">

                        <?php

                        //Escondemos el precio?
                        if (!$deal ['Deal']['hide_price']){

                            echo '<p>'. Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price']) . '</p>';
                        }
                        else
                        {
                            echo '<p style="height:25px;"></p>';
                        }

                        //Verifico si tengo que mostrar el boton o no

                            if ($isAccesibleForBuy)
                            {
                                $enlace = $deal['Deal']['deal_buy_link'][0];
                                if(($deal['Deal']['deal_promotion_type'] == ConstDealTypes::Fibertel) && $has_subdeals!=true)
                                {
                                    $enlace = "javascript:void(0)";
                                }

                                //Es un boton de compra, mas allá del caso
                                echo '<div class="comprar">';
                                echo $html->link('&iexcl;Comprar!', $enlace, array('escape'=>false, 'id' => 'bt_comprar', 'rel' =>$has_subdeals? "has_subdeals_1":"has_subdeals_0"));
                                echo '</div>';



                                if($deal['Deal']['deal_promotion_type'] == ConstDealTypes::Fibertel)
                                {
                                    //Es un deal para socio de FIBERTEL
                                    if($logged_user_on_cc)
                                    {
                                        //El usuario esta logueado en CC, lo redireccionamos a Fibertel
                                        $url_redirection = 'http://www.cablevisionfibertel.com.ar/handler/oauth.ashx?ClientId=49310de4ac8c4c1a84a7ed8f6cf4afe8';

                                ?>
                                        <script>
                                            $(document).ready(function() {

                                                $('#bt_comprar[rel=has_subdeals_0]').click(function()
                                                {

                                                    var answer = confirm('Usted sera redireccionado al sitio web de FIBERTEL para poder acreditarse como cliente y acceder a este descuento. Luego de ingresar sus datos, volverá al Sitio web de Club Cupón para confirmar la compra de esta oferta')
                                                    if (answer)
                                                    {
                                                        //Vital setear el dominio para la correcta lectura por parte de CAKE
                                                        $.cookie("fibertel_deal_id",'<?php echo $deal['Deal']['id'] ?>',{path:__cfg('path_relative')});
                                                        window.location = '<?php echo $url_redirection;?>';
                                                    }

                                                });
                                            });
                                        </script>

                    <?php
                                    }


                    } elseif (!empty ($deal['Deal']['is_ticketportal_deal'])) {
                        echo $html->link('', '#dialog_ticketportal',
                            array ('escape'=>false,
                            'title' => $deal ['Deal']['name'],
                            'id' => 'bt_comprar',
                            'class' => 'bt_comprar'));
            ?>
            <script>
            $(document).ready(function() {
                ticketportal_config = {inline:true, width:"400px", height:"350px", close: '',
                    onOpen:function(){
                        $("#cboxTitle").attr({'style': 'visibility: hidden'});
                        $(".boton_clarin365_holder").attr({'style': 'margin-top:30px'});
                    },
                     onLoad:function(){
                            ticketportal_deal_url = "<?php echo $clubcupon->addHttp($deal['Deal']['ticketportal_deal_url']); ?>";
                            ticketportal_deal_pin = "<?php echo $deal['Deal']['ticketportal_deal_pin']; ?>";

                            $("#ticketportal_deal_pin_input").val(ticketportal_deal_pin);
                     }
                    };

                $( "#bt_comprar_grande" ).colorbox(ticketportal_config);
                $( "#bt_comprar" ).colorbox(ticketportal_config);
                $( "#bt_comprar_gif" ).colorbox(ticketportal_config);
            });

            </script>

                <?php
                    } elseif (!empty ($deal['Deal']['is_clarin365_deal'])&& !Configure::read('orderPlugin.enabled')) {
                        echo $html->link('', '#dialog_clarin365',
                            array (
                            'title' => $deal ['Deal']['name'],
                            'escape' => false,
                            'id' => 'bt_comprar',
                            'class' => 'bt_comprar'));
            ?>
            <script>
            var allFields = "";
            var clarin365_messages = "";

            $(document).ready(function() {
                clarin365_config = {inline:true, width:"400px", height:"310px",
                    close: '',
                    onOpen:function(){
                        $("#cboxTitle").attr({'style': 'visibility: hidden'});
                    },
                        onLoad:function(){
                            $('#clarin365_form').attr('action', '../deals/buy/<?php echo $deal ['Deal']['id']; ?>');
                            var clarin365CardNumber = $( "#clarin365CardNumber" );
                            allFields = $( [] ).add( clarin365CardNumber );
                            clarin365_messages = $( "#clarin365_messages" );
                        },
                            onComplete:function(){
                            },
                                onCleanup:function(){
                                    allFields.removeClass( "clarin365-state-error" );
                                    resetMessageClarin365();
                                },
                                    onClosed:function(){
                                        resetMessageClarin365();
                                    }
                };

                clarin365_gift_config = {inline:true, width:"400px", height:"310px",
                    close: '',
                    onOpen:function(){
                        $("#cboxTitle").attr({'style': 'visibility: hidden'});
                    },
                        onLoad:function(){
                            $('#clarin365_form').attr('action', '../deals/buy_as_gift/<?php echo $deal ['Deal']['id']; ?>');
                            var clarin365CardNumber = $( "#clarin365CardNumber" );
                            allFields = $( [] ).add( clarin365CardNumber );
                            clarin365_messages = $( "#clarin365_messages" );
                        },
                            onComplete:function(){
                            },
                                onCleanup:function(){
                                    allFields.removeClass( "clarin365-state-error" );
                                    resetMessageClarin365();
                                },
                                    onClosed:function(){
                                        resetMessageClarin365();
                                    }
                };



                $( "#bt_comprar_grande" ).colorbox(clarin365_config);
                $( "#bt_comprar" ).colorbox(clarin365_config);
                $( "#bt_comprar_gif" ).colorbox(clarin365_gift_config);
            });

            </script>

                <?php
                               }


                            }
                            else
                            {
                                //No esta accesible para comprar
                                echo '<div class="comprar-out"><p>Oferta Finalizada</p></div>';
                            }
                    ?>




                </div>


                        <!-- FIN BOTON DE COMPRA NUEVO-->

                    <div class="modulos-compra">
                        <?php
                            //Escondemos el precio?
                            if ($deal ['Deal']['hide_price']){
                        ?>

                        <ul class="precio clearfix">
                            <li style="text-align: center; width:100%;">
                                <?php echo 'Descuento <br />' . $html->cInt($deal['Deal']['discount_percentage']) . "%"; ?>
                            </li>
                        </ul>

                       <?php }else{ ?>


                                <?php if (!$deal ['Deal']['only_price']){ ?>
                                <ul class="precio clearfix">
                                    <li>
                                        <?php echo 'Descuento <br />' . $html->cInt($deal['Deal']['discount_percentage']) . "%"; ?>
                                    </li>

                                    <li>
                                        <?php echo 'Valor Real <br />' . Configure::read('site.currency') . $html->cCurrency($deal['Deal']['original_price']); ?>
                                    </li>
                                <?php } ?>
                            <!--
                       <?php
                            //RECOMENDAR (solo si esta disponible para comprar y no soy empresa)
                            if ($html->isAllowed($auth->user('user_type_id')) &&
                            $deal['Deal']['deal_status_id'] != ConstDealStatus::Draft &&
                            $deal['Deal']['deal_status_id'] != ConstDealStatus::PendingApproval &&
                            @!$deal['Deal']['is_ticketportal_deal'])
                            {
                                if ($isAccesibleForBuy)
                                {
                                    $bityurl = $deal['Deal']['bitly_short_url_prefix'];
                                    echo '<ul class="precio clearfix">';
                                    echo '<li class="li-first">';
                                    if($auth->sessionValid())
                                    {

                                        if (!empty ($deal['Deal']['is_clarin365_deal']) && $deal['Deal']['is_clarin365_deal'] == 1) {
                                            echo $html->link('Regal&aacute; a un amigo', '#dialog_clarin365', array('escape' => false, 'id' => 'bt_comprar_gif', 'class' =>'regala'));
                                        } elseif (!empty ($deal['Deal']['is_ticketportal_deal']) && $deal['Deal']['is_ticketportal_deal'] == 1) {
                                            echo $html->link('Regal&aacute; a un amigo', '#dialog_ticketportal', array('escape' => false, 'id' => 'bt_comprar_gif', 'class' =>'regala'));
                                        } else {
                                            echo $html->link('Regal&aacute; a un amigo', array('id' => 'bt_gif', 'controller' => 'deals', 'action' => 'buy_as_gift', $deal['Deal']['id']), array('escape' => false, 'class' =>'regala'));
                                        }
                                    }
                                    else
                                    {
                                        echo $html->link('Regal&aacute; a un amigo', array('controller' => 'users', 'action' => 'login'), array('escape' => false, 'class' =>'regala'));
                                    }
                                    echo '</li>';
                                }
                            }


                         ?>
                            -->

                        </ul>

                        <?php } ?>




                            <?php if($isAccesibleForBuy) { ?>
                            <div class="tiempo-cupon fmt-1 hidden">
                            <?php } ?>
                                <?php if($isAccesibleForBuy) { ?>
                                <span id="deal_countdown_amout" class="hidden"><?php echo intval(strtotime($deal['Deal']['end_date']) - time()); ?></span>

                                Este cup&oacute;n termina en:
                                <ul>
                                    <li class="horas-num"><?php echo intval(strtotime($deal['Deal']['end_date']) - time()); ?></li>
                                    <li class="horas-txt"><span>Horas</span><span>Min.</span><span>Seg.</span></li>
                                </ul>


                                    <p class="cupon-activo fmt-3">Cup&oacute;n Activo!</p>

                                <?php } ?>

                                <div class="ya-compraron">
                                    <?php if(!empty($deal['Deal']['show_sold_quantity']) && $deal['Deal']['show_sold_quantity']) { ?>
                                        <p class="fmt-2"><span><?php echo $deal['Deal']['deal_external_count']; ?></span> personas</p>
                                        <p class="fmt-3">ya compraron esta oferta!</p>
                                    <?php } ?>

                                </div>
                                <?php if($isAccesibleForBuy && $hideSubscription) { ?>
                                    <div class="recomendar clearfix">
                                        <p>Compartilo</p>
                                        <ul class="s-redes">
                                            <li>
                                            <a class="fb" href="javascript:void(0)" onclick="<?php echo "fbFeed_deal('" . $deal['Deal']['name'] . "', '" . $bityurl . "', '" . intval($deal['Deal']['discount_percentage']) . "');"; ?>">
                                                <?php echo $html->image('social_actions/facebook.png'); ?>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="tw" href = "http://twitter.com/share?url=<?php echo rawurlencode($bityurl); ?>&text=<?php echo rawurlencode($deal['Deal']['name']); ?>&via=<?php echo $city_twitter_username; ?>&lang=es" title="<?php echo 'Twiteá esta oferta'; ?>">
                                                <?php echo $html->image('social_actions/twitter.png'); ?>
                                            </a>
                                        </li>

                                        <li>
                                            <?php
                                                if($isAccesibleForBuy)
                                                {
                                                    if($auth->sessionValid())
                                                    {
                                                        echo $html->link('email', 'javascript:void(0)', array('onclick' => 'popupGeneric("' . Router::url(array('controller' => 'firsts', 'action' => 'recommend', $deal['Deal']['id']), false) . '", 960, 455, "Cerrar");', 'escape' => false, 'class'=>'email'));
                                                    } else {
                                                        echo $html->link('email', array('controller' => 'users', 'action' => 'login'), array('escape' => false, 'class'=>'email'));
                                                    }
                                                }
                                            ?>
                                        </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            <?php if($isAccesibleForBuy) { ?>
                            </div>
                            <?php } ?>

                    </div>

                        <!-- datos de la empresa -->
                    <div class="datos-empresa clearfix">
                        <?php
                            if(!empty($deal['Company']['logo_image'])){
                                echo '<img src="'.$deal['Company']['logo_image'].'" />';
                            }

                        ?>
                        <h3><?php echo $deal['Company']['name']; ?></h3>
                        <p>
                            <?php

                                $city     = !empty($deal ['Deal']['custom_company_city'])     ? $deal ['Deal']['custom_company_city']     : $deal ['Company']['City']['name'];
                                $address1 = !empty($deal ['Deal']['custom_company_address1']) ? $deal ['Deal']['custom_company_address1'] : $deal ['Company']['address1'];
                                $state    = !empty($deal ['Deal']['custom_company_state'])    ? $deal ['Deal']['custom_company_state']    : $deal ['Company']['State']['name'];
                                $zip      = !empty($deal ['Deal']['custom_company_zip'])      ? $deal ['Deal']['custom_company_zip']      : $deal ['Company']['zip'];

                                if (!empty ($address1)) {
                                    echo $html->cText ($address1) . '<br />';
                                }

                                if (!empty ($city) ){
                                    echo $html->cText ($city)     . ', ';
                                }

                                if (!empty ($zip)) {
                                    echo '('.$html->cText ($zip).')';
                                }
                            ?>
                        </p>
                        <p>
                            <?php
                                if (!empty ($state)) {
                                echo $html->cText ($state);
                                }
                            ?>
                        </p>
                        <?php
                                if (!empty ($deal['Company']['url'])) {
                                    echo $html->link('Ver Web', $deal['Company']['url'], array('class' => '', 'target' => '_blank', 'style'=>'float:none;'));
                                }
                            ?>







                        <?php if ($deal ['Deal']['show_map']){ ?>
                            <script>
                            $(document).ready(function() {

                                      $("#mapa_link").colorbox({
                                          close:'',
                                          inline:true,
                                          href:"#mapa_empresa",
                                          widthinnerWidth:500,
                                          innerHeight:500,
                                          onComplete:function() {
                                            //El boton esta oculto, lo muestro solo en este caso
                                            $('#cboxboton_cierre').show();
                                          }
                                      });
                              });



                            </script>
                            <?php if (!empty ($deal['Company']['url'])&& $deal ['Deal']['show_map']) { echo ' | '; } ?>
                              <a href="#" id="mapa_link" style="float:none;">Ver Mapa</a>

                        <?php } ?>


                        <span class="arrow-decoration"></span>
                    </div>
                    <!-- FIN datos de la empresa -->


                </div>

                <!-- Contenidos de la oferta ppal -->
                <div class="grid_8 alpha omega col-der">

                        <div class="content-der">

                                <?php

                                    //Mostramos el ribbon solo si esta agotada
                                    if($isAccesibleForBuy)
                                    {
                                        echo $html->link($image,$deal['Deal']['deal_buy_link'][0] ,array('title' => sprintf(__l('%s'), $deal['Deal']['name'])), null, false);
                                    }
                                    else
                                    {
                                        echo $image;
                                        echo '<img alt="" src="/img/theme_clean/finalizada.png" class="finalizada" />';
                                    }
                                ?>
                            
                           <?php if (false): ?>
                                <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ PromoTop1 -->
                                <script type="text/javascript" language="JavaScript1.1">
                                <!--
                                var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
                                var cs = document.charset || document.characterSet;
                                document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/d622a76d2e218082?o=j&rnd=' + rnd + '&crs=' + cs + '&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>"></scr' + 'ipt>');
                                //-->
                                </script>
                                <noscript><a href="http://ads.e-planning.net/ei/3/9b30/d622a76d2e218082?it=i&rnd=$RANDOM" target="_blank"><img alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/d622a76d2e218082?o=i&rnd=$RANDOM&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>" border=0></a></noscript>
                                <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ PromoTop1 -->

                           <?php endif; ?>     
                                <!-- / mastercard oferta -->
                            
                                <div class="bloque-tabs">
                                    <ul class="menu-oferta clearfix">
                                        <?php if(!empty($deal['Deal']['coupon_condition'])){ ?>
                                            <li class="mitem-1 clearfix"><a href="#tab-1" id="tabA" class="select condiciones menu-select">Condiciones</a></li>
                                        <?php } ?>
                                        <?php if(!empty($deal['Deal']['coupon_highlights'])){ ?>
                                            <li class="mitem-2 clearfix"><a href="#tab-2" id="tabB"class="destacados">Destacados</a></li>
                                        <?php } ?>
                                        <?php if(!empty($deal['Deal']['description'])){ ?>
                                            <li class="mitem-2 clearfix"><a href="#tab-3" id="tabC"class="destacados">M&aacute;s informaci&oacute;n</a></li>
                                        <?php } ?>
                                    </ul>
                                </div>

                                <?php if(!empty($deal['Deal']['coupon_condition'])){ ?>
                                <!-- CONDICIONES -->
                                <div id="tab-1">
                                    <div class="grupo-contenidos" id="grupocontenidos">
                                        <div class="grid_8 alpha contenidos" id="conten">
                                            <?php
                                            	$vigencia="Válido para canjear desde el ";	
                                            	$coupon_start_date=htmlentities (strftime ("%d/%m/%Y", strtotime ($deal ['Deal']['coupon_start_date'])), ENT_QUOTES, 'UTF-8');
                            				    $coupon_expiry_date=htmlentities (strftime ("%d/%m/%Y", strtotime ($deal ['Deal']['coupon_expiry_date'])), ENT_QUOTES, 'UTF-8');
                            				    $vigencia.=$coupon_start_date.' hasta el '.$coupon_expiry_date;
                                            		?>
                                        	
                                            <?php echo $html->cHtml($deal['Deal']['coupon_condition']); ?>
                                            <?php echo $html->cHtml("<ul><li><b>".$vigencia."</b></li></ul>"); ?>
                                            <!-- mastercard oferta -->
                                            
                                            <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ PromoBottom1 -->
                                            <script type="text/javascript" language="JavaScript1.1">
                                            <!--
                                            var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
                                            var cs = document.charset || document.characterSet;
                                            document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/b91172ba0d0dfd95?o=j&rnd=' + rnd + '&crs=' + cs + '&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>"></scr' + 'ipt>');
                                            //-->
                                            </script>
                                            <noscript><a href="http://ads.e-planning.net/ei/3/9b30/b91172ba0d0dfd95?it=i&rnd=$RANDOM" target="_blank"><img alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/b91172ba0d0dfd95?o=i&rnd=$RANDOM&kw_ciudades=<?php echo Configure::read ('Actual.city_slug').'&kw_turismo='.$mainDealIsTourism.'&kw_idaviso='.$deal['Deal']['id']; ?>" border=0></a></noscript>
                                            <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ PromoBottom1 -->

                                            
                                            
                                            <!-- / mastercard oferta -->
                                            <div class="fader"></div>
                                        </div>
                                        <a class="vermascont" id="verm">Ver M&aacute;s</a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if(!empty($deal['Deal']['coupon_highlights'])){ ?>
                                <!-- DESTACADOS -->
                                    <div id="tab-2">
                                        <div class="grupo-contenidos" id="grupocontenidosB">
                                            <div class="grid_8 alpha contenidos" id="contenB">
                                                <?php echo $html->cHtml($deal['Deal']['coupon_highlights']); ?>
                                                <div class="fader"></div>
                                            </div>
                                            <a class="vermascont2" id="verm2">Ver M&aacute;s</a>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if(!empty($deal['Deal']['description'])){ ?>
                                <!-- DESCRIPCION -->
                                    <div id="tab-3">
                                        <div class="grupo-contenidos" id="grupocontenidosC">
                                            <div class="grid_8 alpha contenidos" id="contenC">
                                                <?php echo $html->cHtml($deal['Deal']['description']); ?>
                                                <div class="fader"></div>
                                            </div>
                                            <a class="vermascont3" id="verm3">Ver M&aacute;s</a>
                                        </div>
                                    </div>
                                <?php } ?>

                        </div>
                </div>

            </div>

        </div>
        <!-- FIN cuerpo oferta PPAL -->

        <!-- SIDEBAR -->
        <?php  echo $this->element('theme_clean/sidebar',array('deal' => $deal));  ?>
        
        <!-- lower secondary deals -->
        <?php
            if(!$is_corporate_city)
            {
                echo $this->element('theme_clean/lower_secondary_deals');
                echo $this->element('theme_clean/social-gadgets');
                echo $this->element('theme_clean/e-planning_ads',array('deal' => $deal, 'is_corporate_city' => $is_corporate_city)); 
            } else {
                echo $this->element('theme_clean/e-planning_ads_corporate',array('deal' => $deal, 'is_corporate_city' => $is_corporate_city)); 
            }
        ?>

 </div>


<?php
    if (!(isset($force_popup) && $force_popup == false)) {
        if ((isset($force_popup) && $force_popup == true ) || (FIRST_TIME_USER && !$auth->sessionValid ())) {
?>
<script>
    $(document).ready(function(){

        <?php
          $popup_w = 755; // ancho default
          $popup_h = 530; // alto default
          $popup = array ();

          switch (Configure::read ('Actual.city_slug'))
            {
              //popup para Cordoba
              case 'cordoba':
                $popup ['controller'] = 'firsts';
                $popup ['action']     = 'index';
                $popup ['city']     = Configure::read ('Actual.city_slug');
                break;

              // popup para BsAs
              case 'ciudad-de-buenos-aires':
                $popup ['controller'] = 'firsts';
                $popup ['action']     = 'index';
                $popup ['city']     = Configure::read ('Actual.city_slug');
                break;
            // popup para circa
             case 'circa':
                 $popup = null;
                break;

              //popup para el resto
              default:
                $popup ['controller'] = 'firsts';
                $popup ['action']     = 'index';
                $popup ['city']     = Configure::read ('Actual.city_slug');
                break;
            }

          switch ($from_banner)
            {
              case 'beta':
                $popup ['controller'] = 'firsts';
                $popup ['action']     = 'beta';
                $popup ['city']     = Configure::read ('Actual.city_slug');
                break;

              default:
                break;
            }

          $urlPopup = 'http://www.clubcupon.com.ar/' . Configure::read ('Actual.city_slug') . '/firsts';
          $parametros='';
          if(isset($_GET['utm_source']) && $_GET['utm_source']!=''){
              $parametros ='utm_source='.$_GET['utm_source'];
          }
          if(isset($_GET['utm_medium']) && $_GET['utm_medium']!=''){
              $parametros .='&utm_medium='.$_GET['utm_medium'];
          }
          if(isset($_GET['utm_term']) && $_GET['utm_term']!=''){
              $parametros .='&utm_term='.$_GET['utm_term'];
          }
          if(isset($_GET['utm_content']) && $_GET['utm_content']!=''){
              $parametros .='&utm_content='.$_GET['utm_content'];
          }
          if(isset($_GET['utm_campaign']) && $_GET['utm_campaign']!=''){
              $parametros .='&utm_campaign='.$_GET['utm_campaign'];
          }

          if(count ($popup) != 0
            && !$is_corporate_city) {

				//echo 'alert('.$parametros.')';
                echo 'popupWelcome(\'' . Router::url ($popup, false) .'?'.$parametros.'\', ' . $popup_w . ', ' . $popup_h . ', function () { $.cookie(\'CakeCookie[first_time_user]\', 1,  {path: \'/\'}); })';
          }
        ?>
  });
</script>
<?php } ?>
<?php } ?>


<div style='display:none'>

    <?php if ($deal ['Deal']['show_map']){ ?>
        <!-- Mapa ubicacion Empresa -->
        <div id="mapa_empresa" >
            <img border="0" alt="[Image:
            <?php echo $deal['Company']['name']; ?>]" src="http://maps.google.com/maps/api/staticmap?center=<?php echo $deal['Company']['latitude'] . ',' . $deal['Company']['longitude'] ?>&amp;markers=color:blue|label:A|<?php echo $deal['Company']['latitude'] . ',' . $deal['Company']['longitude'] ?>&amp;zoom=<?php echo Configure::read('GoogleMap.static_map_zoom_level'); ?>&amp;size=500x500&amp;sensor=false"/>
        </div>
    <?php } ?>

    <div id="dialog_ticketportal">
        <div class="tickeprotal_log"><img src="/img/chapa_ticketportal.gif"></div>
        <div align="left" class="ticketportal_descripcion">Este es tu pin:</div>
        <div id="ticketportal_pin_div" class="clarin365_pin">
         <input id="ticketportal_deal_pin_input" class="ticketportal_relleno" readonly="readonly" size="36"/>
        </div>
        <div class="ticketportal_info">Copialo ya que vas a tener que ingresarlo para confirmar tu compra en Ticket Portal</div>
        <div class="boton_ticketportal_holder"><a id="bt_go_ticketportal" href="javascript:void(0)" title="Comprar Ticket" class="btn-naranja">&iexcl;Comprar!</a></div>
    </div>
    <script>
        var ticketportal_deal_url = null;
        var ticketportal_deal_pin = null;

        $( "#bt_go_ticketportal" ).click(function() {
             window.location = ticketportal_deal_url;
             return false;
        });
    </script>

    <div id="dialog_clarin365"   style='padding:10px; background:#fff;'>
        <div class="clarin365_logo"><img src="/img/clarin365_logo.jpg"></div>
        <div align="left" class="clarin365_ingresa">Ingres&aacute; tu Nro de Tarjeta <b>Clar&iacute;n 365</b></div>

        <form id="clarin365_form" method="POST">
            <fieldset>
                <div class="clarin365_tarjeta"><img src="/img/clarin365_tarjeta.png"></div>
                <div align="left" class="clarin365_texto1">Nro de Tarjeta:</div>
                <div id="clarin365_input">
                <input type="text" name="clarin365CardNumber" id="clarin365CardNumber" class="clarin365_relleno" value="" size="36"/>
                </div>

            </fieldset>
        </form>
        <div align="left" id="clarin365_messages" class="clarin365-messages">
        </div>
        <div class="boton_clarin365_holder"><a id="boton_clarin365_comprar" href='javascript:void(0)' class="btn-naranja">&iexcl;Comprar!</a></div>
    </div>
    <script>
      $('#boton_clarin365_comprar').click(function() {
            var bValid = true;
            var clarin365CardNumber = $( "#clarin365CardNumber" );
            allFields.removeClass( "clarin365-state-error" );
            bValid = bValid && checkUrlClarin365( clarin365CardNumber, /^\d{19}$/, "No es un numero de tarjeta valido." );
            $(".boton_clarin365_holder").attr({'style': 'margin-top:10px'});
            if ( bValid ) {
              checkClarin365CardNumber(clarin365CardNumber.val());
            }

      });
      $('#boton_clarin365_cancelar').click(function() {
            allFields.removeClass( "clarin365-state-error" );
            resetMessageClarin365();
      });


    </script>

</div>

<?php

    //Si estamos en exclusive traemos los elementos extras necesarios
    //Div de apertura-login-pedido_pin
    //JSs necesarios de validación
    if( $is_corporate_city && !$is_corporate_guest )
    {
        echo $this->element('theme_clean/exclusive_addons');
        echo $javascript->link(Configure::read('theme.asset_version') . '/funciones_exclusive');
     }

 ?>


<!-- HEAT MAP -->

<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
if(document.location.protocol!='https:')
  document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
if(typeof ClickTale=='function') ClickTale(162,0.5,"www14");
</script>
<!-- ClickTale end of Bottom part -->

<!-- FIN HEAT MAP -->
</div>
<?php
    echo $this->element('theme_clean/emt/tracking_visitas');
?>