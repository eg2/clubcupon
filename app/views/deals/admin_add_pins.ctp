<div class = "form">
<?php echo $form->create ('Deal', array ('class' => 'normal', 'action' => 'admin_add_pins', 'admin' => 'true', 'enctype' => 'multipart/form-data')); ?>
  <h2><?php echo 'Agregar PINes'; ?></h2>
  <p>La oferta cuenta con <strong><?php echo (int) $num_pins_ok; ?></strong> PINes v&aacute;lidos y <strong><?php echo (int) $num_pins_ko; ?></strong> usados.</p>
	<?php
		echo $form->input ('id', array ('type' => 'hidden', 'value' => $deal_id));
		echo $form->input ('pins', array ('type' => 'textarea', 'label' => ''));
	?>
  <div class = "submit-block">
    <?php
      echo $form->submit (__l ('Add'));
      echo $html->link ('<input type = "button"  value = "' . __l ('Cancel') . '"  class = "cancel-button" />', array ('controller' => 'deals', 'action' => 'index', 'admin' => true), array ('escape' => false));
    ?>
  </div>
  <?php echo $form->end (); ?>
</div>