<div id="main" class="container_16 main_frt">

    <!-- Contenidos -->
    <div class="grid_16 fmt-bg">

        <h2 class="fmt-h2">Tu compra</h2>

        <?php
            $current_user_details = array('username' => $auth->user('username'), 'user_type_id' => $auth->user('user_type_id'), 'id' => $auth->user('id'));
            $cantidad             = $dealExternal['DealExternal']['quantity'];
            $precio               = $deal['Deal']['discounted_price'];
            $precio_original      = $deal['Deal']['original_price'];
              $total                = sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$cantidad));
            $pagoTerceros= sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$dealExternal['DealExternal']['quantity']))-$dealExternal['DealExternal']['discounted_amount']-$internalAmount;

            //formating

            $total = $html->cCurrency($total,'GBP');
            $precio_original = $html->cCurrency($precio_original,'GBP');
            $precio = $html->cCurrency($precio,'GBP');
            $creditousado = array_sum(array($internalAmount, $dealExternal['DealExternal']['gifted_amount'], $dealExternal['DealExternal']['discounted_amount']));
            $pagoTerceros = $html->cCurrency(sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$cantidad))-$creditousado,'GBP');

            $internalAmount = $html->cCurrency($internalAmount,'GBP');

            //e-formating
            $is_discount_mp = false;
            if ($deal['Deal']['is_discount_mp'] && $is_payment_type_mp) {
                $is_discount_mp = true;
            }
        ?>

        <p class="gracias">Muchas gracias por tu compra, <?php echo $html->getUserLink($current_user_details);; ?>.</p>

        <?php

        if($deal['Deal']['is_now']==0){
        ?>
            <h3 class="leyenda-oferta">Luego de acreditado tu pago te enviaremos el cup&oacute;n y toda la informaci&oacute;n necesaria para que disfrutes de tu descuento <span>Club Cup&oacute;n</span></h3>
        <?php
        }else{

            $cuando   = $time->isToday($deal['Deal']['start_date']) ? "Hoy" : 'Ma&ntilde;ana';
            $h_inicio = substr($deal['Deal']['start_date'], 11, -3);
            $h_fin    = substr($deal['Deal']['end_date'],   11, -3);

        ?>
            <h3 class="leyenda-oferta">Para utilizar <?php echo $cuando; ?> desde las <?php echo $h_inicio; ?>hs hasta las <?php echo $h_fin; ?>hs. Disfrut&aacute; de tu descuento <span>Club Cup&oacute;n YA</span></h3>
        <?php
        }
        ?>

        <div class="grid_16 bloque-tabla compra-mensaje alpha omega">
            <div class="grid_16">

                <table class="fmt-tabla tabla-compras">

                    <tr>
                        <th scope="col">Resumen</th>
                        <th scope="col">Su Pedido</th>
                        <th scope="col">Precio Total</th>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td><?php echo "$cantidad x \${$precio}" ;?></td>
                        <td class="talingl"><?php echo "\${$total}";?></td>
                    </tr>

                    <tr>
                        <td rowspan="2">
                            <?php echo $html->showImage('Deal', $deal['Attachment'], array('class' => 'bigImage', 'dimension' => 'small_big_thumb', 'alt' => sprintf('[Foto: %s]', $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)), 'title' => $html->cText($deal['Deal']['name'], false))); ?>
                            <p>
                                <?php
                                    if($deal['Deal']['only_price'])
                                    {
                                        echo '$' . $precio_original . ' por ' . $deal['Deal']['name'];
                                    }
                                    else
                                    {
                                        echo 'Desde $' . $precio . ' en vez de $' . $precio_original . ' por ' . $deal['Deal']['name'];
                                    }
                                ?>
                            </p>
                        </td>
                        <td class="va-middle">
                           <?php if($is_discount_mp) {?>
                                Cr&eacute;dito Otorgado<br />
                            <?php } else { ?>
                                Cr&eacute;dito Usado<br />
                            <?php } ?>
                            Pagado por medios de pago de terceros
                        </td>
                        <td class="va-middle">
                            <?php echo "\${$creditousado}";?><br />
                            <?php echo "\${$pagoTerceros}";?>
                        </td>
                    </tr>

                    <tr>
                        <td class="fmt-totaltabla">Total Pagado:</td>
                        <td class="fmt-totaltabla"><?php echo "\${$total}";?></td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td align="center">
                            <?php
                            
                           /* if($is_hidden_city)
                            {*/
                                echo $html->link ('Volver', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>$city_slug), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> 'btn-gris', 'escape' => false));
                           /* }
                            else
                            {
                                echo '<a href="/" class="btn-gris">Volver</a>';
                            }*/
                            
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $html->link('Ir a mis cupones', array('controller'=>'deal_users', 'action' => 'index', 'type'=>'available'), array('class' => 'btn-gris')); ?>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
         <div>
        <a target="_blank" id="fbShare">
        <?php //echo 'dealUrl:'.$dealUrl;?>
            <?php $image_dir = '/img/theme_clean/';?>
  			<img src="<?php echo $image_dir; ?>facebook_compartir.gif" alt="club cupon" title="Club Cupon" border="0" />
		</a>
		</div>
    </div>
    <!-- / Contenidos -->


</div>
<script>
    $(document).ready(function() {
        
		var urlShare='<?php echo $dealUrl?>';
    	$('#fbShare').attr('href','https://www.facebook.com/sharer/sharer.php?u='+urlShare);
    });
</script>
<?php
    echo $this->element('tracking-compra-ok');
    echo $this->element('theme_clean/emt/tracking_conversion');
?>