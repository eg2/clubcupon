<style type="text/css">
    /*<![CDATA[*/
    .input.radio img{
        margin-left: 5px;
        margin-right: 5px;
    } 
    div .input {
      background: none repeat scroll 0 0 white;
    }
    
    /*]]>*/
</style>
<div id="main" class="container_16 main_frt do_payment_bac">
  <div class="grid_16">
    <div class="bloque-home clearfix">
    <!-- Contenidos -->
		<?php
     
      echo $javascript->link('bac-form', true);
			$price = $deal ['Deal']['discounted_price'];
			$currency=Configure::read('site.currency');
			// Formulario a agregar en cada compra para permitir pagar con el sistema BAC-NPS
      App::import('component','bac');
		  $bac = new BacComponent();
      $dealExt['DealExternal']['payment_type_id'] = $bac_payment_type;
		?>
		<input type="hidden" id="WALLET" value="<?php echo ConstPaymentTypes::Wallet;?>" />
    <?php $pagoDirecto = false;
    
    //Parametros comunes en combinado y pago directo BAC 
    $id_portal = $bac->getPortalId($deal['Deal']['is_tourism']);
    
    if($payment_type_id == ConstPaymentTypes::Wallet){
      //convertimos los multiples productos en un solo producto con el total amount - la plata del wallet
      $priceCombinado = Precision::sub($total_deal_amount,$user_available_balance);
      $priceCombinado_mp = Precision::sub( $priceCombinado,$total_discount_mp_combined);

      // Dinero mail requiere un rango de pago mayor a 3 para pagocombinado
      // es un problema.
      $alertaCargoFueraDeRango = false;
      $aRestarPorMontoMinimo = 0;

      if(Precision::lessEqual($priceCombinado,'0')) {
        $pagoDirecto = true;
        $user_available_balance = Precision::mul($quantity,$deal['Deal']['discounted_price']);
        $aRestarPorMontoMinimo = 0;
      }

      if(!$pagoDirecto && Precision::lessEqual($priceCombinado,'3')) {
        $aRestarPorMontoMinimo = Precision::sub(3,$priceCombinado);
        $priceCombinado = Precision::add($priceCombinado,$aRestarPorMontoMinimo);
        $alertaCargoFueraDeRango = true;
      }

      $priceCombinado = number_format((float)$priceCombinado, 2, '.', ' ');
      $total_deal_amount = number_format((float)$total_deal_amount, 2,'.', ' ');
      $priceCombinadobac = $priceCombinado;

      if (!empty($precioCombinado)) {$priceCombinado = $html->cCurrency ( $priceCombinado, 'small');}
      
      if ($pagoDirecto){
        echo "<h2> Registrando su pago </h2>";
      } 
      else {
        echo "<h2> ABONAR DIFERENCIA </h2>";
      }
			?>
			<input type="hidden" id="user_available_balance" value="<?php echo ($user_available_balance - $aRestarPorMontoMinimo);?>" />
      <form id="payment-type" action="<?php echo Router::url(array('action'=>'updateDealExternalPaymentType'));?>">
        <div class="<? if($pagoDirecto) echo 'hidden';?> paragraph">
          <p>El monto a abonar supera a su estado de Cuenta Club Cup&oacute;n, debe pagar la diferencia mediante uno de los medios de pagos alternativos: </p>
        </div>
        <div style="margin:10px;" class="<? if($pagoDirecto) echo 'hidden';?> lista_detalle_do_payment_bac">
          <ul>
            <li><strong>Monto a abonar:</strong> <?php echo $currency . $total_deal_amount;?></li>

            <li><strong>Monto a pagar con Cuenta Club Cup&oacute;n:</strong> <?php echo $currency . $user_available_balance;?></li>

            <li><strong>Diferencia a pagar por medio de pago: </strong> <span style="color: red"><?php echo $currency . ($priceCombinado);?></span></li>
            
            <?php if($alertaCargoFueraDeRango){ ?>
            <p style="font-size: 10px; margin: 4px;">(El pago m&iacute;nimo permitido en un medio de pago externo es de <strong>$3</strong>.)</p>
            <?php } ?>
          </ul>
        </div>
        <div id="buying-form" class="<? if($pagoDirecto) echo 'hidden';?>">
        <?php
        $paymentTypes = $adminpagos->getPaymentTypesFor($deal['Deal']['id']);
        echo $adminpagos->printJsLogoTable();
        unset($paymentTypes['wallet']);
        $paymentOptions = array();

        foreach($paymentTypes as $paymentKey => $paymentDefinition) {
          $keyPrefix = $paymentDefinition['id_gateway'];
          $paymentOptionId = $paymentDefinition['payment_option_id'];
          $bac_payment_type = $paymentDefinition['bac_payment_type'];

          foreach ($paymentDefinition['cards'] as $keyCard => $cardValue) {
            if ($deal['Deal']['is_discount_mp']) {
              if ($deal['Deal']['is_discount_mp'] && ($keyPrefix == 11 || $keyPrefix == 23 || $keyPrefix == 24 || $cardValue['name'] === "Cuenta Club Cupon" )) {
                $paymentOptions [implode('_', 
                                 array($keyPrefix, 
                                       $cardValue ['payment_type_id'], 
                                       $cardValue ['payment_option_id'], 
                                       $cardValue ['bac_payment_type']))] =
                                         '<img src="' . $cardValue ['logo'] . '" />' . $cardValue ['name'];
              }
            } else {
                $paymentOptions [implode('_', array($keyPrefix, $cardValue ['payment_type_id'], $cardValue ['payment_option_id'], $cardValue ['bac_payment_type']))] = '<img src="' . $cardValue ['logo'] . '" />' . $cardValue ['name'];
            }
          }
        }
        
        $paymentOptions['Dummy'] = '';
       
        echo '<h2>Forma de pago con la cual desea abonar la diferencia: </h2>';
        echo $form->input('payment_type_id', array('type' => 'radio', 'separator' => '<div class="cardLogo"></div><br clear="all">', 'options' => $paymentOptions,'class' => 'js-payment-type'));
        ?>
        </div>
        <input type="hidden" id="BASE" value="<?php echo $this->webroot;?>" />
        <input type="hidden" id="DEAL_EXTERNAL_ID" value="<?php echo $dealExt['DealExternal']['id'];?>" />
        <div id="submit">
          <input type="submit" value="Aceptar" id="submit" class="btn-azul-centrado-atencion-cliente" />
        </div>
        </form>
     <?php
    $dealExt['DealExternal']['internal_amount'] = 
    $bacFormat->addDiscount(
            $dealExt['DealExternal']['internal_amount'], 
            $user_available_balance); 
    
    // MP Discount
    if (!$is_discount_mp) {
      unset($total_discount_mp_combined);
    }
    //Pin code
    if (isset($giftPinAmount) && !empty($giftPinAmount) && $giftPinAmount > 0) {
      $dealExt['DealExternal']['internal_amount'] = 
      $bacFormat->addDiscount(
            $dealExt['DealExternal']['internal_amount'], 
            $giftPinAmount);
    }
    
    $productos = $bac->getParamsToProducts(
            $dealExt, 
            $deal,
            $deal['Deal']['is_tourism'], 
            $deal['Deal']['is_wholesaler'], 
            (!$pagoDirecto));
    
    $productos = json_encode($productos);
    ?>
    <form id="initpay">
      <input type="hidden" id="basecc" name="basecc" value="<?php echo $html->url(array("controller" => "deals","action" => "buyCanceled"), true);?>">
      <input type="hidden" id="urlinitpay" name="urlinitpay" value="<?php echo $html->url(array("plugin"=>"api", "controller" => "api_deals","action" => "initialize_payment.json"), true);?>">
      <input type="hidden" id="idUsuarioPortal" name="idUsuarioPortal" value="<?php echo $user_id; ?>">
      <input type="hidden" id="idPortal" name="idPortal" value="<?php echo $id_portal; ?>">
      <input type="hidden" id="productos" name="productos" value='<?php echo $productos; ?>'>
      <input type="hidden" id="idPagoPortal" name="idPagoPortal" value="<?php echo $dealExt['DealExternal']['id']; ?>">
      <input type="hidden" id="idMedioPago" name="idMedioPago" value="">
      <input type="hidden" id="idGateway" name="idGateway" value="">
      <input type="hidden" id="tmount_mp_combined" name="tmount_mp_combined" value="<?php echo $total_discount_mp_combined;?>">
      <input type="hidden" id="cantidadCuotas" name="cantidadCuotas" value="<?php echo $cantidad_cuotas;?>">
      <input type="hidden" id="descripcion" name="descripcion" value="<?php echo $text->truncate($dealdescription, 95, array('ending' => '...', 'exact' => true));  ?>">
    </form>
    
    <?php
    } else { // IF NOT WALLET
      if (!Configure::read('isLiteBoxEnabled')) {
        echo "<h2>Enviando informaci&oacute;n al medio de pago. No refrescar esta p&aacute;gina</h2>";
        $pagoDirecto = true;
        //ver price @linea7

        // MP Discount
        if ($is_discount_mp && $is_payment_mp) {
          $dealExt['DealExternal']['internal_amount'] = 
          $bacFormat->addDiscount(
              $dealExt['DealExternal']['internal_amount'], 
              $total_discount_mp); 
          $pagoDirecto = false;
        }
        //pin code
        if (isset($giftPinAmount) && !empty($giftPinAmount) && $giftPinAmount > 0) {
          $dealExt['DealExternal']['internal_amount'] = 
          $bacFormat->addDiscount(
              $dealExt['DealExternal']['internal_amount'], 
              $giftPinAmount);
          $pagoDirecto = false;
        }
        //Obtiene los parametros de BAC
        $paramsBac = (array) $bac->getParamsToBac($dealExt, 
                                          $deal, 
                                          $cantidad_cuotas, 
                                          $id_gateway, 
                                          $pagoDirecto);
        //Inicia Pago Seguro
        $res = $bac->iniciarPagoSeguroApi($paramsBac);
        $secure_code = $bac->getSecureCode($deal['Deal']['is_tourism']);

        //exception
        if(!$res['status']){
          $url = $html->url(array("controller" => "deals",
                                  "action" => "buyCanceled"), true);
          header('Location: ' .$url.'/'.$dealExt['DealExternal']['id']); 
          exit();
        } else {
          $secure_hash = md5($id_gateway.$res['idPago'].$id_portal.$secure_code);
        }
      }
    }
    //fin
    //Pago Combinado 
   
   ?>
<?php
         
          if($payment_type_id != ConstPaymentTypes::Wallet && Configure::read('isLiteBoxEnabled') && (in_array($this->data['Deal']['id_gateway'], array(11,23,24)) )){
				  $idProducto = $bacFormat->getProductId($deal['Deal']['is_tourism'], $deal['Deal']['is_end_user']);
          $idProductoDebito = $bacFormat->getProductDebitId($deal['Deal']['is_tourism']);
          $user_id = $dealExt['DealExternal']['user_id'];
          
          App::import('component','bac');
					$bac = new BacComponent();
          $aDebitar = 0;
					$id_portal = $bac->getPortalId($portal);
        
          //adding discounts
					if (isset($giftPinAmount) && !empty($giftPinAmount) && $giftPinAmount > 0) {
						$aDebitar = $aDebitar + $giftPinAmount;
					}
					if (!empty($total_discount_mp_combined)&& $is_payment_mp && $is_discount_mp) {
            $aDebitar = $aDebitar + $total_discount_mp_combined;
					}
          if ($is_discount_mp && $is_payment_mp) {
            $aDebitar = $aDebitar + $total_discount_mp;
            //$productos .= ';idProducto='.$idProductoDebito.',cantidad=1,monto=-'.$aDebitar.',moneda='.Configure::read ('BAC.pesos');
          }

          // PAGO DIRECTO
          $productos = array();
          if ($deal['Deal']['is_tourism'] && $deal['Deal']['is_wholesaler']) {
              $isDebito = false;
              $moneda = Configure::read ('BAC.pesos');
              //solo Turismo
              $idProductoFullIva = $bacFormat->getProductIdFullIva();
              $idProductoHalfIva = $bacFormat->getProductIdHalfIva();
              $idProductoExempt = $bacFormat->getProductIdExempt();
              $idProductoRetail = $bacFormat->getProductIdRetail();
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $fullIva, 
                  $moneda, $isDebito, $idProductoFullIva);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $halfIva, 
                  $moneda, $isDebito, $idProductoHalfIva);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $exempt, 
                  $moneda, $isDebito, $idProductoExempt);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $retail, 
                  $moneda, $isDebito, $idProductoRetail);
          } else {
              $credito = new stdClass();
              $credito->idProducto = $idProducto;
              $credito->cantidad = $quantity;
              $credito->monto = $price;
              $credito->moneda = Configure::read ('BAC.pesos');   
          }

          if ($is_discount_mp || isset($giftPinAmount)) {
            $debito = new stdClass();
            $debito->idProducto = $idProductoDebito;
            $debito->cantidad = 1;
            $debito->monto = $aDebitar *(-1);
            $debito->moneda = Configure::read ('BAC.pesos');
          }
          if ($deal['Deal']['is_tourism'] && $deal['Deal']['is_wholesaler'] && $debito) {
              $productos[] = (array) $debito;
              
          } else {
              if ($debito) {
                $productos[] = (array) $credito;
                $productos[] = (array) $debito;
              } else {
                $productos[] = (array) $credito;
              } 
          }
          //END PAGO DIRECTO
          
					if ($payment_type_id == ConstPaymentTypes::Wallet) { // PAGO COMBINADO
						$aPagar = $deal ['Deal']['discounted_price'];

                        if (isset($giftPinAmount) && !empty($giftPinAmount) && $giftPinAmount > 0) {
                           $aDebitar = $aDebitar + $giftPinAmount;
                        }
                        if (!empty($total_discount_mp_combined)&& $is_payment_mp && $is_discount_mp ) {
                           $aDebitar = $aDebitar + $total_discount_mp_combined;
                        }
            if ($deal['Deal']['is_tourism'] && $deal['Deal']['is_wholesaler']) {
              $isDebito = false;
              //solo Turismo
              $idProductoFullIva = $bacFormat->getProductIdFullIva();
              $idProductoHalfIva = $bacFormat->getProductIdHalfIva();
              $idProductoExempt = $bacFormat->getProductIdExempt();
              $idProductoRetail = $bacFormat->getProductIdRetail();
              
              $moneda = Configure::read ('BAC.pesos');
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $fullIva, 
                  $moneda, $isDebito, $idProductoFullIva);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $halfIva, 
                  $moneda, $isDebito, $idProductoHalfIva);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $exempt, 
                  $moneda, $isDebito, $idProductoExempt);
              
              $productos[] = $bacFormat->getProductsForBac(
                  $deal, $quantity, $retail,
                  $moneda, $isDebito, $idProductoRetail);
            } else {
               $prod = new stdClass();
               $prod->idProducto = $idProducto;
               $prod->item->cantidad = $quantity;
						   $prod->item->monto = $price;
						   $prod->item->moneda = Configure::read ('BAC.pesos');
            }
						$aDebitar = $aDebitar + $user_available_balance;

						if (Precision::lessEqual ($priceCombinadobac, '0')) {

							$prodDeb = new stdClass();
							$prodDeb->item->idProducto = $idProductoDebito;
							$prodDeb->item->cantidad = 1;
							$prodDeb->item->monto = $aDebitar *(-1);
							$prodDeb->item->moneda = Configure::read ('BAC.pesos');

							//$productos .= ';idProducto='.$idProductoDebito.',cantidad=1,monto=-'.$user_available_balance.',moneda='.Configure::read ('BAC.pesos');
							$total -= $user_available_balance;
						} else {

							$prodDeb = new stdClass();
							$prodDeb->item->idProducto = $idProductoDebito;
							$prodDeb->item->cantidad = 1;
							$prodDeb->item->monto = Precision::sub($aDebitar, $aRestarPorMontoMinimo) * (-1);
							$prodDeb->item->moneda = Configure::read ('BAC.pesos');
							$total -= Precision::sub ($aDebitar, $aRestarPorMontoMinimo);
						}
            if ($deal['Deal']['is_tourism'] && $deal['Deal']['is_wholesaler']) {
						     $productos[] = (array) $prodDeb; 
            } else {
                 $productos = array($prod,$prodDeb);
            }
           
          }
          
					$params = new stdClass();
					$params->idUsuarioPortal = $user_id;
					$params->idPortal        = $id_portal;
					$params->productos       = $productos;
					$params->idPagoPortal    = $dealExt['DealExternal']['id'];
					$params->idMedioPago     = $bac_payment_type;
					$params->idGateway       = $id_gateway;
					$params->cantidadCuotas  = 1;

          $dealdescription = $bacFormat->cleanChars($dealdescription);
          $dealdescription = utf8_encode($dealdescription);

					$params->descripcion     = $dealdescription;
          $res = $bac->iniciarPagoSeguro($params);
                    
					if($res['status']){
					$secure_code = 
            $bacFormat->getSecureCode($deal['Deal']['is_tourism']);
          
					$secure_hash = md5($id_gateway.$res['ret'].$id_portal.$secure_code);
          
					?>
          <script>
					 $(document).ready(function(){
						parent.$('#DealAddForm').trigger("pagoSeguroIniciado",["<?php echo $id_gateway;?>","<?php echo $res['ret'];?>","<?php echo $id_portal;?>","<?php echo $secure_hash;?>"]);
					});
					</script>
					<?php }else{
          ?>
					<script>
					$(document).ready(function(){
						parent.$('#DealAddForm').trigger("pagoSeguroIniciado",[false,false,false,false]);       
            //Disabled fields cantidad cuotas.
					});
					</script>
					<?php }
				}else{ ?>
               
                <?php 
                if (empty($cantidad_cuotas)){ $cantidad_cuotas = 1; }
                $id_portal = $bac->getPortalId($deal['Deal']['is_tourism']);
                ?>
                <form action="<?php echo Configure::read('BAC.crearPagoSeguro');?>" method="post" id="bac-form">
                  <input type = "hidden" name = "ID_GATEWAY"        id="ID_GATEWAY"   value="<?php echo $id_gateway; ?>" />
                  <input type = "hidden" name = "ID_PAGO"    id="ID_PAGO"  value = "<?php echo $res['idPago'];?>" />
                  <input type = "hidden" name = "ID_PORTAL"      id="ID_PORTAL"   value = "<?php echo $id_portal; ?>" />
                  <input type = "hidden" name = "SECURE_HASH" id="SECURE_HASH" value = "<?php echo $secure_hash;?>" />
                  <noscript>
                    <input type="submit" value="Enviar" />
                  </noscript>
                </form>
     <?php } ?>
                <?php if (false){ ?>

                    <form action = "<?php echo Configure::read ('MP.btn.url'); ?>"  method = "post"  id = "mp-form">

                        <!-- identificacion -->
                        <input type = "hidden"  name = "acc_id"          value = "<?php echo Configure::read ('MP.acc');      ?>" />
                        <input type = "hidden"  name = "enc"             value = "<?php echo Configure::read ('MP.enc');      ?>" />
                        <input type = "hidden"  name = "seller_op_id"    value = "<?php echo $dealExt ['DealExternal']['id']; ?>" />

                        <!-- urls -->
                        <input type = "hidden"  name = "url_cancel"      value = "<?php echo Configure::read ('MP.url.cancel')     . $dealExt ['DealExternal']['id']; ?>" />
                        <input type = "hidden"  name = "url_process"     value = "<?php echo Configure::read ('MP.url.process')    . $dealExt ['DealExternal']['id']; ?>" />
                        <input type = "hidden"  name = "url_succesfull"  value = "<?php echo Configure::read ('MP.url.succesfull') . $dealExt ['DealExternal']['id']; ?>" />

                        <!-- producto -->
                        <input type = "hidden"  name = "item_id"         value = "<?php echo Configure::read ('MP.item.id');   ?>" />
                        <input type = "hidden"  name = "name"            value = "<?php echo Configure::read ('MP.item.name'); ?>" />

                        <!-- precio -->
                        <input type = "hidden"  name = "currency"        value = "ARG"                   />
                        <input type = "hidden"  name = "price"           value = "<?php echo $total; ?>" />

                        <!-- envio -->
                        <input type = "hidden"  name = "ship_cost_mode" />
                        <input type = "hidden"  name = "op_retira"      />

                    </form>
                <?php }
        if($pagoDirecto){

        ?>
            <script>
                var pagoDirecto = true;
                $('#payment-type').submit();
            </script>
        <?php } ?>
                <!-- / Contenidos -->
        </div>
    </div>
</div>
