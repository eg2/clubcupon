<table width = "983"  height = "1151"  border = "0"  align = "center"  cellpadding = "0"  cellspacing = "0"  id = "Table_01"  style = "background: url(../../img/landing-full-full/bg.jpg) repeat-x; width: 100%; text-align: center; margin-top: 0px;">
  <tr><td width = "983"  height = "208"><img src = "/img/landing-full-full/index_01.jpg"  width = "983"  height = "208"  alt = ""></td></tr>
  <tr><td width = "983"  height = "318"><img src = "/img/landing-full-full/index_02.jpg"  width = "983"  height = "318"  alt = ""></td></tr>
  <tr><td width = "983"  height = "258"><img src = "/img/landing-full-full/index_03.jpg"  width = "983"  height = "258"  alt = ""></td></tr>
  <tr><td width = "983"  height = "251"><img src = "/img/landing-full-full/index_04.jpg"  width = "983"  height = "251"  alt = ""></td></tr>
  <tr><td width = "983"  height = "116"><img src = "/img/landing-full-full/index_05.jpg"  width = "983"  height = "116"  alt = ""></td></tr>
</table>
<script>
  $(document).ready (function () {
    <?php
      $popup_w = 687; // ancho default
      $popup_h = 486; // alto default
      $popup = array ();

      switch (Configure::read ('Actual.city_slug'))
        {
          /* popup para Cordoba */
          case 'cordoba':
            $popup ['controller'] = 'firsts';
            $popup ['action']     = 'index_cordoba';
            $popup_w = 700; // ancho default
            $popup_h = 500; // alto default
            break;

          /* popup para BsAs */
          case 'ciudad-de-buenos-aires':
            $popup ['controller'] = 'firsts';
            $popup ['action']     = 'index';
            break;

              /* popup para el resto */
          default:
            $popup ['controller'] = 'firsts';
            $popup ['action']     = 'index';
            break;
        }

      switch ($from_banner)
        {
          case 'beta':
            $popup ['controller'] = 'firsts';
            $popup ['action']     = 'beta';
            break;

          default:
            break;
        }

      /*if (count ($popup) != 0) echo 'modalPopupGeneric(\'' . Router::url ($popup, true) . '\', ' . $popup_w . ', ' . $popup_h . ')';*/
      if (count ($popup) != 0) echo 'modalPopupGeneric(\'' . Router::url($popup, true) . '?redirect\', ' . $popup_w . ', ' . $popup_h . ')';
    ?>
  });
</script>