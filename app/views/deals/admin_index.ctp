<script>
    $(document).ready(function() {
      $('#buscar').click(function() {
         largo = $('#DealQ').val().length ;
         largoById=$('#DealQid').val().length ;
         largoByCompanyId=$('#DealQcid').val().length ;
         if(largo < 3 && largoById < 3 && largoByCompanyId < 3){
             alert('El criterio de busqueda debe contener al menos 3 caracteres');
             $('#DealQ').focus();
             return false;
         }
       });
    });
</script>


<div id="contents">
    <div class="tabs">
        <ul>

            <?php
            foreach ($dealStatuses as $id => $dealStatus)
              {
                echo '<li><a ' ;
                if($id == $selected){ echo 'class="active" ';}
                echo 'href="/admin/deals/tab:'. $id .'" >'.__l ($dealStatus).'</a></li>';
              }
          ?>
        </ul>
    </div>

    <div id="buscador" style="float:left;width:90%">
    <?php
        echo '<div class="der ancho" style="width:60%">';
        //En ajuste...
        echo $form->create("Deal",array('action' => 'search'));
        echo '<div  class="izq" style="width:600px;">';
        echo '<div  style="width:33%;float:left">';
        	echo $form->input('qcid'    , array('div'=>false,'label' => 'Id de compañía'));
        echo '</div>';
        echo '<div  style="width:33%;float:left">';
      	echo $form->input('qid'    , array('div'=>false,'label' => 'Id de oferta'));
        echo '</div>';
        echo '<div  style="width:33%;float:left">';
        	echo $form->input('q'    , array('div'=>false,'label' => 'Texto' ));
        	echo $form->input('selected', array('type'  => 'hidden',        'value'=> $selected));
        echo '</div>';
        echo '</div>';
        echo '<div class="der" style="margin-top:10px;">';

        $submit_options    = array(
            'id'    => 'buscar',
            'label' => 'Buscar!',
            );
        echo $form->end($submit_options);

        echo '<br />';
        echo '</div>';
        echo '</div>';
        echo '<div  class="izq">';
    ?>
        <table style="margin:15px auto;">
            <tr>
                <td width="55"><?php echo $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLink'), null, array('class'=>'disabled')); ?></td>
                <td><?php echo $paginator->numbers(array('class'=>'pagLink')); ?></td>
                <td width="80"><?php echo $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLink'), null, array('class'=>'disabled')); ?></td>
            </tr>
        </table>
    <?php
        echo '</div>';
        echo '</div>';
        echo '<div id="deals_commands" >';
        echo $html->link ('A&ntilde;adir oferta', array ('controller' => 'deals', 'action' => 'add'), array ('escape'=>false, 'class' => 'add', 'title' => 'A&ntilde;adir oferta'));
        echo ' | ';
        if (ConstUserTypes::isLikeAdmin ($auth->user ('user_type_id'))){
            echo $html->link ('Actualizar c&aacute;lculos para liquidaci&oacute;n', array ('controller' => 'deals', 'action' => 'admin_update_billing_statement'), array ('escape'=>false, 'title' => 'Actualizar c&aacute;lculos para liquidaci&oacute;n'));
        }
        echo '</div>';
    ?>



    <div class="js-search-responses">

    <?php
        echo $form->create ('Deal', array ('class' => 'normal', 'action' => 'update'));
        echo $form->input ('r', array ('type' => 'hidden', 'value' => $this->params['url']['url']));
    ?>
      <style type="text/css">
        th, th a{font-size: 10px!important;}
      </style>
      <table class="list">
        <tr>
          <th colspan="15" style="background:#de3d02"> Datos generales</th>
          <th colspan="4"  style="background:#c73302"> Precio ($)</th>
          <th colspan="2"  style="background:#de3d02"> L&iacute;mite de usuarios</th>
          <th colspan="2"  style="background:#c73302"> Cantidad vendida</th>
          <th rowspan="2"  style="background:#de3d02"> Total compra ($)</th>
          <th colspan="2"  style="background:#c73302"> Comisi&oacute;n</th>
          <th rowspan="2"  style="background:#de3d02"> Notas </th>
        </tr>
        <tr>

          <th style="background:#de3d02"> </th>


  <?php if ($selected == -1): ?>
            <th style="background:#de3d02">
                <?php
                    echo $paginator->sort ('Estado', 'DealStatus.name');
                ?>
            </th>
  <?php endif; ?>

          <th style="background:#de3d02"><?php echo $paginator->sort ('Añadido', 'Deal.created'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('ID-Padre', 'Deal.parent_deal_id'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Oferta', 'Deal.name'); ?></th>
          <th style="background:#de3d02"><a href="#">Suboferta</a></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Tipo de Pago', 'Deal.pay_by_redeemed'); ?>
          </th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Usuario', 'User.username'); ?></th>
          <th style="background:#de3d02; width:100px;"><?php echo $paginator->sort ('Empresa', 'Company.name'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Vendedor', 'User.username'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Turismo', 'Deal.is_tourism'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Producto', 'Deal.is_product'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Fibertel', 'Deal.is_fibertel_deal'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Ciudad', 'City.name'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Lateral', 'Deal.is_side_deal'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Riesgo', 'Deal.risk'); ?></th>

            <?php if (!empty($this->params['named']['type']) && ($this->params['named']['type'] == 'all')) { ?>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Estado', 'DealStatus.name'); ?></th>
  <?php } ?>

          <th style="background:#de3d02"><?php echo $paginator->sort ('Inicio', 'Deal.start_date'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Fin', 'Deal.end_date'); ?></th>
          <th style="background:#c73302"><?php echo $paginator->sort ('Original', 'Deal.original_price'); ?></th>
          <th style="background:#c73302"><?php echo $paginator->sort ('Descontado', 'Deal.discounted_price'); ?></th>
          <th style="background:#c73302; width:70px;"><?php echo $paginator->sort ('% desc.', 'Deal.discount_percentage'); ?></th>
          <th style="background:#c73302; width:80px;"><?php echo $paginator->sort ('$ a descontar', 'Deal.discount_amount'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Minimo', 'Deal.min_limit'); ?></th>
          <th style="background:#de3d02"><?php echo $paginator->sort ('Maximo', 'Deal.max_limit'); ?></th>
          <th style="background:#c73302"><?php echo $paginator->sort ('Liquidados', 'paid_coupons'); ?></th>
          <th style="background:#c73302; width:60px;"><?php echo $paginator->sort ('A Liquidar', 'unpaid_coupons'); ?></th>

          <th style="background:#c73302; width:60px;"><?php echo $paginator->sort ('Costo fijo', 'Deal.bonus_amount'); ?></th>
          <th style="background:#c73302; width:60px;"><?php echo $paginator->sort ('%', 'Deal.commission_percentage'); ?></th>

        </tr>
        <?php
        if (!empty ($deals)):
          $i = 0;
          foreach ($deals as $deal):
        	$tipo_pago=$deal ['Deal']['pay_by_redeemed']>0?'Redimido':'Vendido';
          	
          	$is_subdeal = $deal ['Deal']['parent_deal_id'] != $deal ['Deal']['id'];
            $status_class = '';
            $class = null;
            if ($i++ % 2 == 0):
              $class = ' class="altrow"';
            endif;
            if ($deal['Deal']['deal_status_id'] == ConstDealStatus::Open):
              $status_class = ' js-checkbox-active';
            endif;
            if ($deal['Deal']['deal_status_id'] == ConstDealStatus::PendingApproval):
              $status_class = ' js-checkbox-inactive';
            endif;
            ?>
            <tr<?php echo $class; ?>>

            <?php if ($selected == -1): ?>
                <td>
                    <?php
                        //Mostramos el nombre del estado en el que se encuentra la oferta, (cuando se marca listar todos...)
                        echo __l($deal ['DealStatus']['name']);
                    ?>
                </td>
            <?php endif; ?>

       <td valign="middle">
      <?php if (!empty ($moreActions)): ?>

                  <!-- Fecha de creacion & submenues (2)-->

                  <!-- Inicio menu de opciones 1/2 -->
                  <div class="actions-block">
                    <div class="actions round-5-left">
                      <!-- Ver oferta -->
                      <strong><span><?php echo $html->link ($html->cText ($deal['Deal']['name']), array ('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug'], 'city' => $deal['City']['slug'], 'admin' => false,), array ('class' => 'view-icon', 'title' => 'Ver Oferta', 'title' => $html->cText ($deal['Deal']['name'], false), 'escape' => false)); ?></span></strong>

                      <!-- Mostrar/ocultar cant vendida-->
                                    <?php
                                    /*
                                    if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id')) && $deal['Deal']['show_sold_quantity'] && in_array($deal['Deal']['deal_status_id'], array(ConstDealStatus::Open, ConstDealStatus::Tipped, ConstDealStatus::Upcoming))){
                                        echo '<span>'.$html->link('Ocultar la cantidad vendida', array('controller' => 'deals', 'action' => 'update_show_sold_quantity', $deal['Deal']['id'], 'admin' => false), array('class' => 'js-update-show-sold-quantity', 'title' => 'Ocultar la cantidad vendida')) . '</span>';
                                     }else{
                                        echo '<span>' . $html->link('Mostrar la cantidad vendida', array('controller' => 'deals', 'action' => 'update_show_sold_quantity', $deal['Deal']['id'], 'admin' => false), array('class' => 'js-update-show-sold-quantity', 'title' => 'Mostrar la cantidad vendida')) . '</span>';
                                    };
                                    */
                                    ?>
                      <!-- CSV / Imprimir-->
                      <?php if (!empty ($this->params['named']['filter_id']) && (($this->params['named']['filter_id'] == ConstDealStatus::Tipped) || ($this->params['named']['filter_id'] == ConstDealStatus::Closed) || ($this->params['named']['filter_id'] == ConstDealStatus::PaidToCompany))): ?>
                        <span><?php echo $html->link ('CSV', array ('controller' => 'deals', 'action' => 'coupons_export', 'deal_id' => $deal['Deal']['id'], 'filter_id' => $id, 'ext' => 'csv', 'admin' => false), array ('class' => 'export', 'title' => 'CSV')); ?></span>
                        <span> <?php echo $html->link ('Imprimir', array ('controller' => 'deals', 'action' => 'deals_print', 'filter_id' => $this->params['named']['filter_id'], 'page_type' => 'print', 'deal_id' => $deal['Deal']['id']), array ('title' => 'Imprimir', 'class' => 'print-icon')); ?></span>
                      <?php endif; ?>

                      <!-- Vendidos / Pendientes / Totales-->
                      <?php
                          if (!empty($deal['Deal']['deal_status_id']) && $deal['Deal']['deal_status_id'] != ConstDealStatus::PendingApproval && $deal['Deal']['deal_status_id'] != ConstDealStatus::Rejected && $deal['Deal']['deal_status_id'] != ConstDealStatus::Draft && $deal['Deal']['deal_status_id'] != ConstDealStatus::Upcoming) {
                             if (!in_array($auth->user('user_type_id'), array(ConstUserTypes::Agency, ConstUserTypes::Partner))) {

                          echo $html->link ('Cupones vendidos  (' . max (0,($deal['Deal']['publication_channel_type_id']==2?$deal['0']['used_coupons']:$deal['0']['sold_coupons'])) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones vendidos'));
                          echo $html->link ('Cupones pendientes  (' . max (0, $deal['0']['pending_coupons']) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones pendientes'));
                          echo $html->link ('Cupones totales  (' . max (0, $deal['0']['total_coupons']) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones totales'));
                          echo $html->link ('Cupones Anulados ('. max (0, $deal['0']['anulled_coupons']).')','#', array ('class' => 'coupon-sold', 'title' => 'Cupones anulados'));
                          }
                        }
                      ?>

                     

                      <!-- Editar-->
                      <?php
                                    if (ConstUserTypes::isLikeSuperAdmin ($auth->user ('user_type_id')) || $deal['Deal']['deal_status_id'] == ConstDealStatus::PendingApproval || $deal['Deal']['deal_status_id'] == ConstDealStatus::Upcoming || $deal['Deal']['deal_status_id'] == ConstDealStatus::Open || $deal['Deal']['deal_status_id'] == ConstDealStatus::Tipped || $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft || $deal['Deal']['deal_status_id'] == ConstDealStatus:: Rejected) {
                        echo $html->link ('Editar', array ('controller' => 'deals', 'action' => 'edit', $deal['Deal']['id']), array ('class' => 'edit js-edit', 'title' => 'Editar'));
                        }
                      ?>

                      <!-- Replicar Oferta-->

                      <?php
                      /* No se muestra link replicar si es una suboferta
                       * http://jira.int.clarin.com/browse/CC-687
                       */
                       if (!($deal['Deal']['id'] <> $deal['Deal']['parent_deal_id'])): ?>
                      <?php  echo $html->link ('Replicar Oferta', array ('controller' => 'offers', 'action' => 'replicate', 'deal_id' => $deal['Deal']['id']), array ('class' => 'add', 'title' => 'Replicar oferta')); ?>
                      <?php  endif;  ?>
      				  <span>
      				  <?php
      				  if($deal ['Deal']['is_shipping_address']){ 
      				  	echo $html->link ('Puntos de Retiro', array ('controller' => 'shippingAddressDeals', 'action' => 'admin_associate', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'homepage','title' => 'Administrar Puntos de Retiro'));
      				  } 
      				  ?>
      				  </span>
               
                      <!-- SubOferta -->
                      <?php
                      if(ConstUserTypes::isPrivilegedUserOrAdmin($auth->user('user_type_id')) && @$deal ['Deal']['publication_channel_type_id'] == Configure::read('publication_channel.web') && in_array($deal ['Deal']['deal_status_id'], array(ConstDealStatus::Open, ConstDealStatus::Tipped, ConstDealStatus::Upcoming)) && !$is_subdeal) {
                      ?>
                        <span><?php echo $html->link ('Agregar suboferta', array ('controller' => 'deals', 'action' => 'add', 'parent_deal_id' => $deal ['Deal']['id']), array ('title' => 'Agregar suboferta')); ?></span>
                      <?php
                       }
                      ?>
                      <?php
                      if(Deal::isForResell($deal ['Deal']['id'],$deal ['Deal']['parent_deal_id'],$deal ['Deal']['deal_trade_agreement_id'],$deal ['Deal']['coupon_expiry_date'],$deal ['Deal']['deal_status_id']))  
					  {
					  	 echo '<span>';
					  		echo $html->link ('Revender Oferta', array ('controller' => 'offers', 'action' => 'resell', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'add','title' => 'Revender Oferta'));
            		  	 echo '</span>';
				
					  }
                      ?>
                    </div>
                  </div><!-- Fin Menu opciones 1/2-->




      <?php endif; ?>
                  <!-- Checkbox seleccion -->
                  <?php echo $form->input ('Deal.' . $deal['Deal']['id'] . '.id', array ('type' => 'checkbox', 'id' => "admin_checkbox_" . $deal['Deal']['id'], 'label' => false, 'class' => 'js-checkbox-list ' . $status_class . '')); ?>
              </td>
              <td>
      <?php if (empty ($moreActions)): ?>

                  <!-- Inicio menu de opciones 2/2-->
                  <div class="actions-block">
                    <div class="actions round-5-left">


                      <!-- Ver oferta -->
                      <?php
                        //Ajuste del titulo...
                        $titulo = $deal['Deal']['name'];
                        $titulo_oferta = substr($titulo, 0, 21);
                        //agrego unos puntos suspensivos en el caso de que hayamos hecho un corte en el titulo...
                        if(strlen($titulo)>strlen($titulo_oferta)){$titulo_oferta .= '...';}
                      ?>


                      <strong><span><?php echo $html->link ($html->cText ($titulo_oferta), array ('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug'], 'city' => $deal['City']['slug'], 'admin' => false,), array ('class' => 'view-icon', 'title' => 'Ver Oferta', 'title' => $html->cText ($deal['Deal']['name'], false), 'escape' => false)); ?></span></strong>

                      <!-- Exportar/Imprimir-->
                      <?php
                      if (!empty ($this->params['named']['filter_id']) && (($this->params['named']['filter_id'] == ConstDealStatus::Tipped) || ($this->params['named']['filter_id'] == ConstDealStatus::Closed) || ($this->params['named']['filter_id'] == ConstDealStatus::PaidToCompany))):
                        ?>
                        <span><?php echo $html->link ('CSV', array ('controller' => 'deals', 'action' => 'coupons_export', 'deal_id' => $deal['Deal']['id'], 'ext' => 'csv', 'admin' => false), array ('class' => 'export', 'title' => 'CSV')); ?></span>
                        <span> <?php echo $html->link ('Imprimir', array ('controller' => 'deals', 'action' => 'deals_print', 'filter_id' => $this->params['named']['filter_id'], 'page_type' => 'print', 'deal_id' => $deal['Deal']['id']), array ('title' => 'Imprimir', 'class' => 'print-icon')); ?></span>
                      <?php endif; ?>

                      <!-- Vendidos/pendientes/totales -->
                      <?php
                                    if (!empty($deal['Deal']['deal_status_id']) && $deal['Deal']['deal_status_id'] != ConstDealStatus::PendingApproval && $deal['Deal']['deal_status_id'] != ConstDealStatus::Rejected && $deal['Deal']['deal_status_id'] != ConstDealStatus::Draft && $deal['Deal']['deal_status_id'] != ConstDealStatus::Upcoming) {
                                        if (ConstUserTypes::isNotPrivilegedUser($auth->user('user_type_id'))) {

                          echo $html->link ('Cupones vendidos  (' . max (0,($deal['Deal']['publication_channel_type_id']==2?$deal['0']['used_coupons']:$deal['0']['sold_coupons'])) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones vendidos'));
                          echo $html->link ('Cupones pendientes  (' . max (0, $deal['0']['pending_coupons']) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones pendientes'));
                          echo $html->link ('Cupones totales  (' . max (0, $deal['0']['total_coupons']) . ')', array ('controller' => 'deal_users', 'action' => 'index', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'edit js-edit coupon-sold', 'title' => 'Cupones totales'));
                          echo $html->link ('Cupones Anulados ('. max (0, $deal['0']['anulled_coupons']).')','#', array ('class' => 'coupon-sold', 'title' => 'Cupones anulados'));
                          }
                        }
                      ?>

                     

                      <!-- Editar/Borrar -->
                      <?php
                                    if ($deal['Deal']['deal_status_id'] == ConstDealStatus::PendingApproval || $deal['Deal']['deal_status_id'] == ConstDealStatus::Upcoming || $deal['Deal']['deal_status_id'] == ConstDealStatus::Open || $deal['Deal']['deal_status_id'] == ConstDealStatus::Tipped || $deal['Deal']['deal_status_id'] == ConstDealStatus::Draft || $deal['Deal']['deal_status_id'] == ConstDealStatus:: Rejected) {
                        echo $html->link ('Editar', array ('controller' => 'deals', 'action' => 'edit', $deal['Deal']['id']), array ('class' => 'edit js-edit', 'title' => 'Editar'));
                        }

                      ?>

                      <!-- Cerrar -->
                      <?php
                      /* No se muestra link replicar si es una suboferta
                      * http://jira.int.clarin.com/browse/CC-687
                      */
                      if (!($deal['Deal']['id'] <> $deal['Deal']['parent_deal_id'])):  ?>
                      <?php echo $html->link ('Replicar oferta', array ('controller' => 'offers', 'action' => 'replicate', 'deal_id' => $deal['Deal']['id']), array ('class' => 'add', 'title' => 'Replicar oferta')); ?>
                      <?php  endif; ?>

                      <!-- Agregar suboferta -->
                      <?php
                        if (ConstUserTypes::isPrivilegedUserOrAdmin($auth->user('user_type_id')) && $deal ['Deal']['publication_channel_type_id'] == Configure::read('publication_channel.web') && in_array($deal ['Deal']['deal_status_id'], array(ConstDealStatus::Open, ConstDealStatus::Tipped, ConstDealStatus::Upcoming)) && !$is_subdeal) {
                      ?>
                        <span><?php echo $html->link ('Agregar suboferta', array ('controller' => 'deals', 'action' => 'add', 'parent_deal_id' => $deal ['Deal']['id']), array ('title' => 'Agregar suboferta')); ?></span>
                      <?php
                        }
                      ?>
                      <span>
                      <?php
                      if($deal ['Deal']['is_shipping_address']){
                      	echo $html->link ('Puntos de Retiro', array ('controller' => 'shippingAddressDeals', 'action' => 'admin_associate', 'deal_id' => $deal ['Deal']['id']), array ('class' => 'homepage','title' => 'Administrar Puntos de Retiro'));
                      } 
                      ?>
                      </span>
                    </div>
                  </div><!-- Fin Menu opciones 2/2 -->

      <?php endif; ?>

                <!-- Fecha de creacion-->
                <?php echo $html->cDateTimeHighlight ($deal['Deal']['created']); ?>

              </td>
                <!-- ID Padre -->
                <td><?php echo $html->cInt ($deal ['Deal']['parent_deal_id']); ?></td>

              <!-- IMAGEN  -->
              <td>
                <?php
                echo $html->showImage ('Deal', $deal['Attachment'], array ('dimension' => 'medium_thumb', 'alt' => sprintf (__l ('[Image: %s]'), $html->cText ($deal['Deal']['name'], false)), 'title' => $html->cText ($deal['Deal']['name'], false)));
        if (Cache::read('site.city_url', 'long') == 'prefix') {
                  ?>

                  <?php
                        } elseif (Cache::read('site.city_url', 'long') == 'subdomain') {
                  $subdomain = substr (env ('HTTP_HOST'), 0, strpos (env ('HTTP_HOST'), '.'));
                  $sitedomain = substr (env ('HTTP_HOST'), strpos (env ('HTTP_HOST'), '.'));
                            if (strlen($subdomain) > 0) {
                    ?>
                    <a href="http://<?php echo $deal['City']['slug'] . $sitedomain . 'deal/' . $deal['Deal']['slug']; ?>" title="<?php echo $deal['Deal']['name']; ?>"><?php echo $deal['Deal']['name']; ?></a>
                    <?php
            } else {
                    echo $html->link ($html->cText ($deal['Deal']['name']), array ('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug'], 'admin' => false), array ('title' => $html->cText ($deal['Deal']['name'], false), 'escape' => false));
                    }
                  }
                ?>
              </td>

              <!-- Suboferta -->
              <td><?php echo $html->cBool ($is_subdeal); ?></td>
              <td><?php echo $html->cBool ($tipo_pago); ?></td>
              

              <!-- Usuario -->
              <td><?php echo $html->getUserLink ($deal['User']); ?></td>

              <!-- Empresa -->
              <td>
                <!-- <?php // echo $html->link(!empty ($deal ['Deal']['custom_company_name']) ? $deal ['Deal']['custom_company_name'] : $deal['Company']['name'], array('controller' => 'deals', 'action'=>'index', 'company' => $deal['Company']['slug']),array('title' => sprintf(__l('%s'), !empty ($deal ['Deal']['custom_company_name']) ? $deal ['Deal']['custom_company_name'] : $deal['Company']['name'])));  ?> -->
                <?php echo $html->link ($deal['Company']['name'], array ('controller' => 'deals', 'action' => 'index', 'company' => $deal['Company']['slug']), array ('title' => sprintf (__l ('%s'), $deal['Company']['name']))); ?>
              </td>

              <!-- Empresa -->
              <td>
                <?php
                    if(!empty($deal['0']['seller_first_name']))
                    {
                        echo $deal['0']['seller_first_name'] . ' '.  $deal['0']['seller_last_name'];
                    }
                    else
                    {
                        echo $deal['0']['seller_username'];
                    }
                ?>
              </td>

              <!-- Turismo -->
              <td><?php echo $html->cBool ($deal['Deal']['is_tourism']); ?></td>
              <!-- Producto -->
              <td><?php echo $html->cBool ($deal['Deal']['is_product']); ?></td>


              <!-- Fibertel? -->
              <td><?php echo $html->cBool (!empty($deal['Deal']['is_fibertel_deal'])); ?></td>


              <!-- Ciudad -->
                    <td><?php echo $html->link($deal['City']['name'], array('controller' => 'deals', 'action' => 'index', 'city_slug' => $deal['City']['slug']), array('title' => sprintf(__l('%s'), $deal['City']['name']))); ?></td>

              <!-- Lateral -->
              <td><?php echo $html->cBool ($deal['Deal']['is_side_deal']); ?></td>

              <!-- Riesgo -->
              <td><?php echo ConstRisk::getFriendly ($deal ['Deal']['risk']); ?></td>


              <!-- Riesgo -->
              <?php
                    if (!empty($this->params['named']['type']) && ($this->params['named']['type'] == 'all')) {
                ?>
                <td><?php echo $html->cText ($deal['DealStatus']['name']); ?></td>
        <?php
        }
      ?>

              <!-- Fecha Inicio -->
              <td><?php echo $html->cDateTime ($deal['Deal']['start_date']); ?></td>

              <!-- Fecha Finalizacion -->
              <td><?php echo $html->cDateTime ($deal['Deal']['end_date']); ?></td>

              <!-- Precio Original -->
                    <td>$ <?php echo $deal['Deal']['original_price']; ?></td>

              <!-- Precio con descuento -->
                    <td>$ <?php echo $deal['Deal']['discounted_price']; ?></td>

              <!-- % Descuento -->
              <td><?php echo $html->cFloat ($deal['Deal']['discount_percentage']); ?></td>

              <!-- Monto Descuento -->
                    <td>$ <?php echo $deal['Deal']['discount_amount']; ?></td>

              <!-- Usuarios Minimo -->
              <td><?php echo $html->cInt ($deal['Deal']['min_limit']); ?></td>

              <!-- Usuarios Maximo -->
              <td><?php echo $deal['Deal']['max_limit'] ? $html->cInt ($deal['Deal']['max_limit']) : 'Sin limite'; ?></td>

              <!-- Liquidados -->
              <td><?php echo $html->cInt ($deal['0']['paid_coupons']); ?></td>

              <!-- A liquidar -->
              <td><?php echo $html->cInt ($deal['Deal']['publication_channel_type_id']==2?$deal['0']['used_coupons']:$deal['0']['unpaid_coupons']); ?></td>

              <!-- Total compra -->
              <td><?php echo $html->cInt ($deal['Deal']['publication_channel_type_id']==2?$deal['0']['redeemed_amount']:$deal['0']['total_purchased_amount']); ?></td>

              <!-- Costo fijo -->
              <td><?php echo $html->cCurrency ($deal['Deal']['bonus_amount']); ?></td>

              <!-- % comision -->
              <td><?php echo $html->cFloat ($deal['Deal']['commission_percentage']); ?></td>

              <!-- Notas -->
              <td><div><?php echo $html->cText ($deal['Deal']['private_note']); ?></div></td>

            </tr>

            <?php
          endforeach;
        else:
          ?>
          <tr>
            <td colspan="20" class="notice" bgcolor="#ffffff">No hay ofertas disponibles</td>
          </tr>
        <?php
        endif;
        ?>
      </table>

          <?php if (!empty ($deals)): ?>
        <div class="admin-select-block">


            <?php
            if (!empty($moreActions))
              {
                echo '<div id="pie_deals">';
                echo '<div  class="izq">';
                echo ' <strong>';
              echo $html->link ('Todos', '#', array ('class' => 'js-admin-select-all', 'title' => 'Todos'));
                echo ' - ';
              echo $html->link ('Ninguno', '#', array ('class' => 'js-admin-select-none', 'title' => 'Ninguno'));
                echo ' </strong>';
                echo '</div>';
            ?>


          <div class="admin-checkbox-button">
            <?php
                echo '<div  class="der">';
                echo $form->input('more_action_id', array('class' => 'js-admin-index-autosubmit', 'label' => false, 'options'	=> $moreActions, 'empty' => 'Mas acciones'));
                echo '</div>';
            }
            ?>
          </div>

          <div class="hide">
        <?php
        echo $form->submit ('Enviar');
        ?>
          </div>

        </div>

        <div class="js-pagination"><?php //echo $this->element('paging_links'); ?></div>
  <?php endif; ?>
  <?php echo $form->end (); ?>

    </div>

</div>
