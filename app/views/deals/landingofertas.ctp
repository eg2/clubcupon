<!--[if IE 8]>
    <style>
    #header{
        margin-top:-80px;
    }
    </style>
<![endif]-->
<style>
    #main .bloque-promo-big p{
        text-align: left;
    }
</style>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="main" class="container_16">
    
    <!-- Banner publicitario 16 columnas -->
    <div class="grid_16 banner_a">
        <?php if(false): ?>
        <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ Top -->
        <script type="text/javascript" language="JavaScript1.1">
            <!--
                var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
                var cs = document.charset || document.characterSet;
                document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/8b411e3d8c63fd0b?o=j&rnd=' + rnd + '&crs=' + cs + '&kw_ciudades=<?php echo Configure::read ('Actual.city_slug')?>"</scr' + 'ipt>');
            //-->
        </script>
        <noscript><a href="http://ads.e-planning.net/ei/3/9b30/8b411e3d8c63fd0b?it=i&rnd=$RANDOM" target="_blank"><img alt="e-planning.net ad" src="http://ads.e-planning.net/eb/3/9b30/8b411e3d8c63fd0b?o=i&rnd=$RANDOM&kw_ciudades=<?php echo Configure::read ('Actual.city_slug')?>" border=0></a></noscript>
        <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ Top -->
        <?php endif; ?>
    </div>
    <!-- / Banner publicitario 16 columnas -->
    
    <?php
    
    if(!empty($all_deals)){
    ?>
	<h1 class="grid_16 todas-ofertas">Todas las Ofertas</h1>
	<div class="grid_12 omega">
            <div class="grid_12 alpha">
                
                <?php
                	$index=1;
                    foreach($all_deals as $idx => $small_deal){
                    
                        $small_deal_title        = $clubcupon->shorten_strings_with_html_entities($small_deal['Deal']['name'], 35);
                        $small_deal_hover_title  = $clubcupon->shorten_strings_with_html_entities($small_deal['Deal']['name'], 12);
                        
                        if($small_deal['Deal']['hide_price']){
                            $titulo_oferta = $titulo_oferta_hover = $small_deal['Deal']['discount_percentage'] . '% de descuento' . ' <span>' . $small_deal_title . '</span>';
                        }else{
                            if($small_deal['Deal']['only_price']){
                                $titulo_oferta          = 'Pag&aacute; s&oacute;lo $' . $html->cCurrency (floor($small_deal['Deal']['discounted_price']) , 'small') . ' <span>' . $small_deal_title . '</span>';
                                $titulo_oferta_hover    = 'Pag&aacute; $'      . $html->cCurrency (floor($small_deal['Deal']['discounted_price']) , 'small');
                            }
                            else
                            {
                                $titulo_oferta          = 'Pag&aacute; s&oacute;lo $' . $html->cCurrency (floor($small_deal['Deal']['discounted_price']) , 'small') . ' y obten&eacute; un descuento de $' . $html->cCurrency (floor($small_deal['Deal']['discount_amount']) , 'small') .' <span>' . $small_deal_title . '</span>';
                                $titulo_oferta_hover    = 'Pag&aacute; $'      . $html->cCurrency (floor($small_deal['Deal']['discounted_price']) , 'small') . ' en vez de  $' . $html->cCurrency (floor($small_deal['Deal']['original_price']) , 'small');
                            }
                        }
                        
                       
                        //Hack horrible para el formato de imagenes, a remover en el segundo release. Ver notas de Juan, implica todar el método Attachment
                        $image_tag = $html->showImage('Deal', $small_deal['Attachment'], array('dimension' => 'medium_big_thumb', 'alt' => $html->cText($small_deal['Company']['name'] . ' ' .$small_deal['Deal']['name'], false), 'title' => $html->cText($small_deal['Company']['name'] . ' ' .$small_deal['Deal']['name'], false)));
                        $image_tag = str_replace('width="440"',  'width="233"',  $image_tag);
                        $image_tag = str_replace('height="292"', 'height="153"', $image_tag);
                        
                        $sOferta=$small_deal['Deal']['slug'];
                        $sUrl='/'.$city_slug .'/deal/'.$sOferta;
                        
                        echo '<div class="bloque-promo-big">';
                        
                            echo '<p>';
                            echo $html->link($titulo_oferta, array('controller' => 'deals', 'action' => 'view', $small_deal['Deal']['slug']), array('title' => $html->cText($small_deal['Deal']['name'], false), 'escape' => false));
                            echo '</p>';
                            
                            echo '<div class="imagen-js">';
                                echo $html->link($image_tag,     array('controller' => 'deals', 'action' => 'view', $small_deal['Deal']['slug']), array('title' => sprintf(__l('%s'), $small_deal['Deal']['name'])), null, false);
                            
                            //Div modal
                            echo '<div class="div-modal" id="modal'.$index.'">';
                                echo '<div class="modalFondo"></div>';//velo negro
                                echo '<div class="modal">';
                                    //echo '<div class="titulo">' . $small_deal_hover_title . '</div>';
                                    echo '<div class="titulo">&nbsp;</div>';
                                    echo '<div class="descripcion">' . $titulo_oferta_hover . '</div>';
                                    echo $html->link('Ver m&aacute;s', array('controller' => 'deals', 'action' => 'view', $small_deal['Deal']['slug']), array('class'=>'btnVerMas','title' => sprintf(__l('%s'), $small_deal['Deal']['name'])), null, false);
                                    if (!$small_deal ['Deal']['only_price']){
                                        echo '<div class="porcentaje"><span> ' . $small_deal['Deal']['discount_percentage'] . '%</span></div>';
                                    }
                                echo '</div>';
                            echo '</div>';
                            //fin Div Modal
                            $nombreModal='modal'.$index; 
                            echo '</div>';
                            
                        echo '</div>';
                        ?>
                        <script type="text/javascript">

                        $(document).ready(function () {

                            $("#<?php echo $nombreModal;?>").click(function () {
                               
                            	window.location = "<?php echo $sUrl;?>"
                            });
                        });
                        </script>
                        <?php
                        		 $index++;
                    }
                ?>
                
            </div>
	</div>
	<?php }else{ ?>
        <h1 class="grid_16 todas-ofertas">No contamos con ofertas activas disponibles.</h1>
    <?php } ?>
        
</div>
