<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix">
        <h1 class="h1-format">Tu compra</h1>
        <h2>Seleccion&aacute; una opci&oacute;n</h2>

            <!-- Contenidos -->

        <div id = "buying-form">
            <div class = "clearboth"></div>
            <?php echo $html->showImage ('Deal', $parent ['Attachment'], array ('style' => 'float: right;', 'dimension' => 'small_big_thumb', 'alt' => sprintf (__l('[Image: %s]'), $html->cText ($deal ['Deal']['name'], false)), 'title' => $html->cText ($deal ['Deal']['name'], false))); ?>
            <?php echo $parent ['Deal']['name']; ?>
            <table>
                <tr>
                    <th>Descripci&oacute;n</th>
                    <th>Precio</th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($sub_deals as $sub_deal) { ?>
                    <tr>
                        <td class = "description">
                        <?php
                            echo empty ($sub_deal ['Deal']['descriptive_text']) ? $sub_deal ['Deal']['name'] : $sub_deal ['Deal']['descriptive_text'];
                        ?>
                        </td>
                        <td>
                            <?php echo Configure::read ('site.currency'); ?><?php echo $html->cCurrency ($sub_deal ['Deal']['discounted_price'], 'small'); ?>
                        </td>
                        <td>


                        <?php

                            //Variable de control para mostrar o no el boton de compra
                            $dealIsAvailableForBuy = $sub_deal['Deal']['dealIsAvailableForBuy'];

                            if ($dealIsAvailableForBuy)
                            {

                                //Mostramos el boton de comprar, pero cual?
                                //
                                //Es gift?
                                if (isset($buyAsGift) && $buyAsGift)
                                {
                                    echo $html->link ('Comprar', array ('controller' => 'deals', 'action' => 'buy_without_subdeal', $sub_deal ['Deal']['id'], null, $circa_company, $circa_code, '?' => array ('buyAsGift' => true)), array ('title' => $sub_deal ['Deal']['name'], 'class' => 'btn-azul-centrado', 'style' => 'float: right;'));
                                }

                                //Es fibertel?
                                else if(!empty ($sub_deal['Deal']['is_fibertel_deal']))
                                {

                                    //Defino la URL, en funcion de si el usuario ya se logueo o no a CC
                                    if($logged_user_on_cc)
                                    {
                                        $url_redirection = 'http://www.fibertel.com.ar/oauth.ashx?client_id=49310de4ac8c4c1a84a7ed8f6cf4afe8';

                        ?>
                                        <script>

                                            function boton_fibertel_<?php echo $sub_deal['Deal']['id'];?>()
                                            {
                                                    var answer = confirm('Usted sera redireccionado al sitio web de FIBERTEL para poder acreditarse como cliente y acceder a este descuento. Luego de ingresar sus datos, volverá al Sitio web de Club Cupón para confirmar la compra de esta oferta')
                                                    if (answer)
                                                    {
                                                        //Vital setear el dominio para la correcta lectura por parte de CAKE
                                                        $.cookie("fibertel_deal_id",'<?php echo $sub_deal['Deal']['id'] ?>',{path:__cfg('path_relative')});
                                                        window.location = '<?php echo $url_redirection;?>';
                                                    }
                                            }

                                        </script>

                        <?php

                                        if(!isset($_COOKIE['fibertelUser']) || !$_COOKIE['fibertelUser']){
                                            //Nunca se logueó con fibertel, o limpió todas sus cookies, lo mandamos para Fibertel
                                            echo $html->link('Comprar', 'javascript:void(0)', array ('title' => $sub_deal ['Deal']['name'], 'id' => 'boton_fibertel','class' => 'btn-azul-centrado boton_fibertel_js', 'style' => 'float: right; ', 'onclick'=> 'boton_fibertel_'.$sub_deal['Deal']['id'].'()'));
                                        }else{
                                            //Ya se registro contra Fibertel, lo mandamos a la compra...
                                            echo $html->link ('Comprar', array ('controller' => 'deals', 'action' => 'buy_without_subdeal', $sub_deal ['Deal']['id'], null, $circa_company, $circa_code, true), array ('title' => $sub_deal ['Deal']['name'], 'class' => 'btn-azul-centrado', 'style' => 'float: right; '));
                                        }


                                    }else{

                                        $url_redirection = '../../users/login/?f=deals/buy/'.$parent ['Deal']['id'];

                             echo $html->link('Comprar', $url_redirection , array ('title' => $sub_deal ['Deal']['name'], 'id' => 'boton_fibertel','class' => 'btn-azul-centrado boton_fibertel_js', 'style' => 'float: right;'));
                         }

                } elseif (!empty ($sub_deal['Deal']['is_clarin365_deal'])&& !Configure::read('orderPlugin.enabled')) {
                    echo $html->link('Comprar', '#dialog_clarin365',
                        array ('title' => $sub_deal ['Deal']['name'],
                        'id' => 'boton_clarin365_deal_'.$sub_deal ['Deal']['id'],
                        'class' => 'btn-azul-centrado',
                        'style' => 'float: right;'));
        ?>
        <script>
	var allFields = "";
	var clarin365_messages = "";
        $(document).ready(function() {
            $( "#boton_clarin365_deal_<?php echo $sub_deal ['Deal']['id']; ?>" ).colorbox({inline:true, width:"400px", height:"350px",
                    close: '',
                    onOpen:function(){
                    $("#cboxTitle").attr({'style': 'visibility: hidden'});
                    $(".boton_clarin365_holder").attr({'style': 'margin-top:30px'});
                    },
                    onLoad:function(){
                        $('#clarin365_form').attr('action', '../buy_without_subdeal/<?php echo $sub_deal ['Deal']['id']; ?>');
                        var clarin365CardNumber = $( "#clarin365CardNumber" );
                        allFields = $( [] ).add( clarin365CardNumber );
                        clarin365_messages = $( "#clarin365_messages" );
                    },
                    onComplete:function(){
                    },
                    onCleanup:function(){
                        resetMessageClarin365();
                    },
                    onClosed:function(){
                        resetMessageClarin365();
                    }
                });
            });

        </script>
            <?php
                } elseif (!empty ($sub_deal['Deal']['is_ticketportal_deal'])) {
                    echo $html->link('Comprar', '#dialog_ticketportal',
                        array ('title' => $sub_deal ['Deal']['name'],
                        'id' => 'bt_comprar_small',
                        'class' => 'btn-azul-centrado',
                        'style' => 'float: right;'));
        ?>
        <script>
        $(document).ready(function() {
            //console.log('oferta de ticketportal');
            ticketportal_config = {inline:true, width:"400px", height:"350px", close: '',
                onOpen:function(){
                    $("#cboxTitle").attr({'style': 'visibility: hidden'});
                },
                 onLoad:function(){
                        ticketportal_deal_url = "<?php echo $clubcupon->addHttp($sub_deal['Deal']['ticketportal_deal_url']); ?>";
                        ticketportal_deal_pin = "<?php echo $sub_deal['Deal']['ticketportal_deal_pin']; ?>";

                       // console.log(ticketportal_deal_url);
                       // console.log(ticketportal_deal_pin);

                        $("#ticketportal_deal_pin_input").val(ticketportal_deal_pin);
                 }
                };

            $( "#bt_comprar_small" ).colorbox(ticketportal_config);
        });
        </script>


            <?php
                                } else{
                                    //Es una suboferta comun, mostramos el boton generico
                                    echo $html->link ('Comprar', array ('controller' => 'deals', 'action' => 'buy_without_subdeal', $sub_deal ['Deal']['id'], null, $circa_company, $circa_code), array ('title' => $sub_deal ['Deal']['name'], 'class' => 'btn-azul-centrado', 'style' => 'float: right; '));
                                }
                            } else{
                                //Esta suboferta o no esta abierta, o no esta en marcha o no tiene stock
                                echo '<div class = "btn-gris-centrado"  style = "float: right; ">Agotada</div>';
                            }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div> <!-- /.buying-form -->

        <?php
    $user_balance = '';
    if ($auth->sessionValid ()) {
        $user_balance = Configure::read ('site.currency') . $html->cCurrency ($user_available_balance);
    }
    // echo $this->element ('sidebar_buy', array ('logged_in' => $auth->sessionValid (), 'user_balance' => $user_balance));
    echo $form->input ('circa_company', array ('type' => 'hidden'));
    echo $form->input ('circa_code', array ('type' => 'hidden'));
?>
            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>

<div style='display:none'>
    <div id="dialog_ticketportal">
        <div class="tickeprotal_log"><img src="/img/chapa_ticketportal.gif"></div>
        <div align="left" class="ticketportal_descripcion">Este es tu pin:</div>
        <div id="ticketportal_pin_div" class="clarin365_pin">
         <input id="ticketportal_deal_pin_input" class="ticketportal_relleno" readonly="readonly" size="36"/>
        </div>
        <div class="ticketportal_info">Copialo ya que vas a tener que ingresarlo para confirmar tu compra en Ticket Portal</div>
        <div class="boton_ticketportal_holder"><a id="bt_go_ticketportal" href="javascript:void(0)" title="Comprar Ticket" class="btn-naranja">&iexcl;Comprar!</a></div>
    </div>
    <script>
        var ticketportal_deal_url = null;
        var ticketportal_deal_pin = null;

        $( "#bt_go_ticketportal" ).click(function() {
            // console.log('Redireccionando a ticket portal url: ' + ticketportal_deal_url);
             window.location = ticketportal_deal_url;
             return false;
        });
    </script>


    <div id="dialog_clarin365"   style='padding:10px; background:#fff;'>
        <div class="clarin365_logo"><img src="/img/clarin365_logo.jpg"></div>
        <div align="left" class="clarin365_ingresa">Ingres&aacute; tu Nro de Tarjeta <b>Clar&iacute;n 365</b></div>

        <form id="clarin365_form" method="POST">
            <fieldset>
                <div class="clarin365_tarjeta"><img src="/img/clarin365_tarjeta.png"></div>
                <div align="left" class="clarin365_texto1">Nro de Tarjeta:</div>
                <div id="clarin365_input">
                <input type="text" name="clarin365CardNumber" id="clarin365CardNumber" class="clarin365_relleno" value="" size="36"/>
                </div>

            </fieldset>
        </form>
        <div align="left" id="clarin365_messages" class="clarin365-messages">
        </div>
        <div class="boton_clarin365_holder"><a id="boton_clarin365_comprar" href='javascript:void(0)' class="btn-naranja">&iexcl;Comprar!</a></div>
</div>
  <script>
      
    $(document).ready(function() {

        $(function(){
            document.body.style.overflow = 'auto';
            positionFooter();
            
            function positionFooter()
            {
                $("#footer").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#footer").height()-$("#header").height()+6)+"px"})
            }
            //Capturamos los eventos de ventana
            $(window)
            .scroll(positionFooter)
            .resize(positionFooter)
        });

    });
      
  $('#boton_clarin365_comprar').click(function() {
        var bValid = true;
        var clarin365CardNumber = $( "#clarin365CardNumber" );
        allFields.removeClass( "clarin365-state-error" );
        bValid = bValid && checkUrlClarin365( clarin365CardNumber, /^\d{19}$/, "No es un numero de tarjeta valido." );
        $(".boton_clarin365_holder").attr({'style': 'margin-top:30px'});
       if ( bValid ) {
            checkClarin365CardNumber(clarin365CardNumber.val());
       }

  });
  $('#boton_clarin365_cancelar').click(function() {
        allFields.removeClass( "clarin365-state-error" );
        resetMessageClarin365();
  });

</script>
</div>
