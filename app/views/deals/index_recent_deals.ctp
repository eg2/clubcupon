<div id="content" class="box recent_deals">
  <h1 class="titleBig"><?php echo __l('Recent Deals'); ?></h1>
  <ol id="recent_deals">
    <?php if (!empty($deals)) { ?>
      <?php foreach ($deals as $deal) { ?>
      <li onclick="window.location.replace('<?php echo Router::url(array('controller' => 'deals', 'action' => 'view', $deal['Deal']['slug']), false); ?>');" class="cursorPointer">
        <h3><?php echo $html->truncate($deal['Deal']['name'], 70); ?></h3>
        <div class="mini_tag">

          <?php
            if (!$deal ['Deal']['only_price']) {
            //mostramos la vista extendida
          ?>
          <small><?php echo __l('Price'); ?></small>
          <strong class="orange"><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price']); ?></strong>
          <small><?php echo __l('Value'); ?></small>
          <strong><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['original_price']); ?></strong>
          <small><?php echo __l('Savings'); ?></small>
          <strong><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discount_amount']); ?></strong>
          <?php
            }else{
            //mostramos la vista resumida
          ?>
          <div id="price_only_small">
              <small><?php echo __l('Price'); ?></small>
              <strong class="orange" style="font-size: 40px;"><?php echo Configure::read('site.currency') . $html->cCurrency($deal['Deal']['discounted_price']); ?></strong>
          </div>
         <?php
          }
          ?>
        </div>
        <?php echo $html->showImage('Deal', $deal['Attachment'], array('dimension' => 'small_big_thumb', 'alt' => sprintf(__l('[Image: %s]'), $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)), 'title' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false))); ?>
      </li>
      <?php } ?>
    <?php } else { ?>
      <li><p class="notice"><?php echo __l('No Deals available'); ?></p></li>
    <?php } ?>
  </ol>
  <?php // echo !empty($deals) ? $this->element('paging_links') : ''; ?>
</div>
<?php echo $this->element('sidebar',array('from' => 'recent')); ?>