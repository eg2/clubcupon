<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <!-- Layout admin.ctp -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <script>
      window.print();
    </script>
    <h2><?php echo $html->cText($deal_list['deal_name']);?> </h2>
    <p><?php echo __l('Total Quantity Sold').': '.$html->cInt($deal_list['deal_user_count']);?> </p>
    <table border="1">
        <tr>
            <th><?php echo __l('Username'); ?></th>
            <th><?php echo __l('DNI'); ?></th>
            <th><?php echo __l('DNI del amigo'); ?></th>
            <th><?php echo __l('Fec Nac Amigo'); ?></th>
            <th><?php echo __l('Nombre Completo'); ?></th>
            <th><?php echo __l('Precio'); ?></th>
            <th><?php echo __l('Coupon Code'); ?></th>
            <th><?php echo __l('Es gift'); ?></th>
            <th><?php echo __l('Expires On'); ?></th>
        </tr>
    <?php if(!empty($deals)): ?>

    <?php foreach($deals as $deal): ?>

        <tr>
            <td><?php echo $html->cText($deal['Deal']['username']);?></td>
            <td><?php echo $html->cText($deal['Deal']['dni']);?></td>
            <td><?php echo $html->cText($deal['Deal']['gift_dni']);?></td>
            <td><?php echo $html->cText($deal['Deal']['gift_dob']);?></td>
            <td><?php echo $html->cText($deal['Deal']['full_name']);?></td>
            <td><?php echo Configure::read('site.currency').$html->cCurrency($deal['Deal']['discount_amount']); ?></td>
            <td><?php echo $html->cText($deal['Deal']['coupon_code']); ?></td>
            <td><?php echo $html->cText($deal['Deal']['is_gift'] ? 'Si' : 'No'); ?></td>
            <td><?php echo $html->cDateTime($deal['Deal']['coupon_expiry_date']); ?></td>
        </tr>
    <?php endforeach; ?>
    <?php else: ?>
        <tr><td colspan="11"><?php echo __l('No Deals available');?></td></tr>
    <?php endif; ?>
    </table>
  </body>
</html>