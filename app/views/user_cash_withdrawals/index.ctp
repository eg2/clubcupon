<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h1 class="h1-format">Solicitud de retiro de fondos</h1>
            
            <!-- Contenidos -->
            <?php
            
                if(!empty($user['UserProfile']['paypal_account']))  {
                    echo $this->element('../user_cash_withdrawals/add', array('cache' => array('time' => Configure::read('site.element_cache'))));
                }else{
                    if($auth->user('user_type_id') != ConstUserTypes::Company){
		echo $html->link(__l('Your PayPal account is empty, so click here to update PayPal account in your profile .'), array('controller' => 'users', 'action'=>'my_stuff'. __l('#My_Account')), array('title' => __l('Edit paypal account')));
                }else{
		$company = $html->getCompany($auth->user('id'));
		echo $html->link(__l('Your PayPal account is empty, so click here to update PayPal account in your profile .'), array('controller' => 'companies', 'action'=>'edit', $company['Company']['id']), array('title' => __l('Edit paypal account')));
                }
                }
                echo '<div class="paging-links-wrapper">';
                echo $this->element('paging_counter');
                echo '</div>';
        ?>
        <table>
            <tr>
                <th><?php echo $paginator->sort(__l('Requested On'), 'UserCashWithdrawal.created');?></th>
                <th><?php echo $paginator->sort('Amount ('.Configure::read('site.currency').')', 'UserCashWithdrawal.amount');?></th>
                <th><?php echo $paginator->sort(__l('Status'),'WithdrawalStatus.name');?></th>
            </tr>
            <?php
                if (!empty($userCashWithdrawals)){
                    $i = 0;
                    foreach ($userCashWithdrawals as $userCashWithdrawal){
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
            ?>
                        <tr<?php echo $class;?>>
                            <td><?php echo $html->cDateTime($userCashWithdrawal['UserCashWithdrawal']['created']);?></td>
                            <td><?php echo $html->cCurrency($userCashWithdrawal['UserCashWithdrawal']['amount']);?></td>
                            <td><?php echo $html->cText($userCashWithdrawal['WithdrawalStatus']['name']);?></td>
                        </tr>
            <?php
                    }
                }else{
            ?>
                <tr>
                    <td colspan="8">No hay solicitudes de retiro de fondos disponibles</td>
                </tr>
            <?php
                }
            ?>
        </table>

      <div class="paging-links-wrapper"> <?php echo $this->element('paging_links'); ?> </div>
<?php
if (!empty($userCashWithdrawals)) {
    echo '<div class="paging-links-wrapper">';
    echo $this->element('paging_links');
    echo '</div>';
}
?>
