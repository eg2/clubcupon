<div class="userCashWithdrawals form js-ajax-form-container js-responses">
	<div class="page-info">
            El monto solicitado ser&aacute; descontado de tu Cuenta Club Cup&oacute;n y la cantidad ser&aacute; bloqueada hasta que sea aprobada o rechazada por el administrador. Una vez que este aprobado, el monto solicitado ser&aacute; enviado a tu cuenta PayPal. En caso de falla, la cantidad se reembolsar&aacute; en tu Cuenta Club Cup&oacute;n.
        </div>
    <?php echo $form->create('UserCashWithdrawal', array('action' => 'add','class' => "normal js-ajax-add-form {container:'js-ajax-form-container',responsecontainer:'js-responses'}"));?>
	<fieldset>
	<?php
		if($auth->user('user_type_id') == ConstUserTypes::User){
			$min = Configure::read('user.minimum_withdraw_amount');
			$max = Configure::read('user.maximum_withdraw_amount');	
		}else if($auth->user('user_type_id') == ConstUserTypes::Company){
			$min = Configure::read('company.minimum_withdraw_amount');
			$max = Configure::read('company.maximum_withdraw_amount');
		}
		echo $form->input('amount',array('after' => Configure::read('site.currency') . '<span class="info">' . sprintf(__l('Minimum withdraw amount: %s%s <br/> Maximum withdraw amount: %s%s'),Configure::read('site.currency'),$html->cCurrency($min),Configure::read('site.currency'), $html->cCurrency($max) . '</span>')));
		echo $form->input('user_id',array('type' => 'hidden'));
		echo $form->input('user_type_id',array('type' => 'hidden','value'=>$auth->user('user_type_id')));
	?>
	</fieldset>
<?php echo $form->end(__l('Withdraw and Confirm'));?>
</div>
