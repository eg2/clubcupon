<div class="userCashWithdrawals form">
<?php echo $form->create('UserCashWithdrawal', array('class' => 'normal'));?>
	<fieldset>
 		<h2><?php echo __l('Edit Withdraw Fund Request');?></h2>
	<?php
		echo $form->input('id');
		echo $form->input('user_id',array('label' => __l('User')));
		echo $form->input('withdrawal_status_id',array('label' => __l('Withdrawal Status ')));
		echo $form->input('amount',array('label' => __l('Amount'),'after' => Configure::read('site.currency')));
		echo $form->input('remark',array('label' => __l('Remark')));
	?>
	</fieldset>
<?php echo $form->end(__l('Update'));?>
</div>
