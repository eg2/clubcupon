<div class="dealCategories">
  <h2>Formatos de Banners</h2>
  <div class="clearfix add-block1">
	<?php echo $html->link("Agregar Formato", array('controller' => 'banner_types', 'action' => 'add'), array('class' => 'add','title' => 'Agregar Formato')); ?>
  </div>


	<table class="list">
		<tr>
			<th>Nombre</th>
			<th>Ancho</th>
			<th>Alto</th>
            <th width="100">Acciones</th>
		</tr>
		
		<?php

		foreach ($bannertypes as $banner_type) { 
				
				$edit	    = $html->link("Editar",	array('action' => 'edit',   $banner_type ['BannerType']['id']), array('class'=>'link-button-generic'));
		        $delete     = $html->link("Borrar", array('action' => 'delete', $banner_type ['BannerType']['id']), array('class'=>'js-confirm-action-link link-button-generic'));
                 
				$output	  = "<tr>";
				$output	 .= "<td>" . $banner_type ['BannerType']['label'] . "</td>";
				$output	 .= "<td>" . $banner_type ['BannerType']['width'] . "</td>";
				$output	 .= "<td>" . $banner_type ['BannerType']['height'] . "</td>";
				$output  .= "<td>" . $edit ."<br />" . $delete . "</td>";
				$output	 .= "</tr>";

				echo $output;
			}
        
		?>

		</tr>
	</table>
	
</div>