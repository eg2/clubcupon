<fieldset>
	<h2>Editar Formato</h2>
	<?php

		echo $form->create	('BannerType',	array('class' => 'normal'));

        echo $form->input	('id');
        
		echo $form->input	('label',		array('label' => 'Nombre'));

		echo $form->input	('width',		array('label' => 'Ancho' ));

        echo $form->input	('height',		array('label' => 'Alto'	 ));
        
		echo $form->end('Modificar Formato');
	?>
</fieldset>