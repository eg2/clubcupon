<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <!--
            <h1 class="h1-format"><a href="">Titulo H1</a></h1>
            <h2><a href="">Titulo H2</a></h2>
        -->
            
            <!-- Contenidos -->





<?php if(empty($this->params['named']['type']) && empty($this->params['named']['sort'])){ ?> 

    <div class="menu-submenu">
    <ul class="bloque-menu clearfix">
            <li><?php echo $html->link(sprintf('Recibidas (%s)', $received),        array('controller'=> 'gift_users', 'action'=>'index', 'type' => 'received'), array('title' => 'Recibieron tarjetas de regalo')); ?></li>
            <li><?php echo $html->link(sprintf('Enviadas (%s)',  $sent),            array('controller'=> 'gift_users', 'action'=>'index', 'type' => 'sent'),     array('title' => 'Enviaron tarjetas de regalo	')); ?></li>
            <li><?php echo $html->link(sprintf('Todas (%s)',   ( $sent+$received)), array('controller'=> 'gift_users', 'action'=>'index', 'type' => 'all'),      array('title' => 'Todas las Tarjetas de Regalo'));  ?></li>
        </ul>
    </div>

<?php }else{ ?>

   
        <?php 
            echo $html->link('COMPRA UNA TARJETA DE REGALO', array('controller' => 'gift_users', 'action' => 'add'), array('class' => 'buy-gift','title'=>'COMPRA UNA TARJETA DE REGALO')); 
            if (!empty($this->params['named']['type']) && ($this->params['named']['type'] == 'received' || $this->params['named']['type'] == 'all')){ 
                echo $html->link(__l('Redeem a Gift Card'),'#',array('title'=>__l('CANJEAR UNA TARJETA DE REGALO'),'class' => 'js-toggle-show redeem-gift {"container" : "js-redeem-form"}')); 
                echo $this->element('gift_users-redeem_gift', array('cache' => array('time' => Configure::read('site.element_cache')), 'plugin' => 'site_tracker'));
                }
        ?>
       
        <div class="paging-links-wrapper">
            <?php echo $this->element('paging_counter');?>
        </div>
            
        <table>
            <tr>
                
                <th><?php echo $paginator->sort('FECHA DE COMPRA','created');?></th>
                
                <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'received'){ ?>
                
                <th><?php echo $paginator->sort('RECIBIDO DE','user_id');?></th>
                    
                <?php } ?>
        
                <th><?php echo $paginator->sort('CODIGO PROMOCIONAL','coupon_code');?></th>
                
                <th><?php echo $paginator->sort('CANTIDAD','amount').' ('.Configure::read('site.currency').')';?></th>
                
                <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] != 'received'){ ?>
                
                <th><?php echo $paginator->sort('Send To','gifted_to_user_id');?></th>
                    
                <?php } ?>
                
                <th><?php echo $paginator->sort('MENSAJE','message');?></th>
                
                <th><?php echo $paginator->sort('CANJEADO', 'is_redeemed');?></div></th>
                
                <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'sent'){ ?>
                
                <th><a href="#">Acci&oacute;n</a></th>
                    
                <?php } ?>
                
            </tr>
        <?php
            if (!empty($giftUsers)):
                $i = 0;
                foreach ($giftUsers as $giftUser):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
        ?>
                    <tr<?php echo $class;?>>
                    <td><?php echo $html->cDateTime($giftUser['GiftUser']['created']);?></td>
                    <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'received'){ ?>
                        <td>
                            <?php 
                                echo $html->getUserAvatarLink($giftUser['User'], 'micro_thumb',true);
                            ?>
                            <?php echo $html->getUserLink($giftUser['User']);?>
                        </td>
                    <?php } ?>
                    <td>
                        <?php echo $html->link($html->cText($giftUser['GiftUser']['coupon_code']), array('controller'=> 'gift_users', 'action'=>'view_gift_card', $giftUser['GiftUser']['coupon_code']), array('class' => 'js-thickbox','title'=>$giftUser['GiftUser']['coupon_code'],'escape' => false));?>
                    </td>
                    <td><?php echo $html->cCurrency($giftUser['GiftUser']['amount']);?></td>
                    <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] != 'received'): ?>
                        <td>
                            <?php 
                                if($giftUser['GiftUser']['is_redeemed']){
                                    echo $html->getUserAvatarLink($giftUser['GiftedToUser'], 'micro_thumb',false);
                                    echo $html->getUserLink($giftUser['GiftedToUser']);
                                }else{
                                    echo 'N/A';
                                }
                            ?>
                        </td>
                    <?php endif; ?>
                    <td>
                        <?php echo $html->cText($giftUser['GiftUser']['message']);?>
                    </td>
                    <td>
                        <?php echo $html->cBool($giftUser['GiftUser']['is_redeemed']);?>
                    </td>
                    <?php if (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'sent'): ?>
                        <td>
                            <?php 
                                if($giftUser['GiftUser']['is_redeemed']){
                                    echo 'N/A';
                                }else{
                                    echo $html->link('Vuelva a enviar', array('controller'=> 'gift_users', 'action'=>'resend', $giftUser['GiftUser']['id']), array('class' => 'resend js-delete', 'title' => 'Vuelva a enviar','escape' => false));
                                }					
                            ?>
                        </td>
                    <?php endif; ?>
                </tr>
    <?php
            endforeach;
        else:
    ?>
        <tr>
            <td colspan="11">No hay disponibles tarjetas de regalo</td>
        </tr>
    <?php
        endif;
    ?>
    </table>
    
    <?php
        if (!empty($giftUsers)) {
    ?>
        <div class="paging-links-wrapper"><?php echo $this->element('paging_links');?></div>
    <?php
        }
    ?>
   
<?php } ?>

            
                        <!-- / Contenidos -->

        </div>
    </div>
</div>
