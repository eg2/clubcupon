<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            
            <!-- Contenidos -->

            <?php
                echo $form->create('GiftUser',    array('action' => 'redeem_gift','class' => "normal js-ajax-form"));
                
                echo $form->input('coupon_code',  array('label'  => 'Código de redención'));
                echo $form->input('submit',       array('type'   => 'hidden'));
                echo $form->submit('Redimir',     array('name'   => 'data[GiftUser][submit]'));
                echo $html->link('Cancelar', '#', array('class'  => 'cancel-button js-toggle-show {"container" : "js-redeem-form"}'));
                echo $form->end();
            ?>

            <!-- / Contenidos -->

        </div>
    </div>
</div>
