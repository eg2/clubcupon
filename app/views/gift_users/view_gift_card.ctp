<div style="background-color: #000">
    
    <div class="gift-milio-header">
        <h2>Tarjeta de Regalo</h2>
    </div>

    <div class="gift-milio-box1">
        <p><strong><span id="js-gift-from"><?php echo $giftUser['User']['username']; ?></span></strong>
        te ha dado	
        <p class="card-amount"><?php echo Configure::read('site.currency'); ?> <span id="js-gift-amount"><?php echo $giftUser['GiftUser']['amount']; ?></span></p>
        <div class="remeber-block">
            <p>
                C&oacute;digo de redenci&oacute;n
                <span class="code-info">
                    <?php echo $giftUser['GiftUser']['coupon_code']; ?>
                </span>
            </p>
        </div>
        <?php if (!$auth->user('id')): ?>
            <div class="gift-login login-right-block">
                <div class="login-right-inner-block">
                    <div class = "js-tabs">
                        <ul>
                            <li>
                                <?php echo $html->link('Login', '#js-gift-card-login', array('title' => 'Entrar al sitio')); ?>
                            </li>
                            <li>
                                <?php echo $html->link('Registro', '#js-gift-card-register', array('title' => 'Registrar al sitio')); ?>
                            </li>
                        </ul>
                        <div id ="js-gift-card-login">
                            <?php echo $this->element('users-login', array('f' => 'gift_users/redeem/' . $giftUser['GiftUser']['coupon_code'], 'cache' => array('time' => Configure::read('site.element_cache')))); ?>
                        </div>
                        <div id ="js-gift-card-register">
                            <?php echo $this->element('users-register', array('f' => 'gift_users/redeem/' . $giftUser['GiftUser']['coupon_code'], 'cache' => array('time' => Configure::read('site.element_cache')))); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php elseif (!$giftUser['GiftUser']['is_redeemed'] && ConstUserTypes::isNotLikeAdmin($auth->user('user_type_id')) && $giftUser['GiftUser']['friend_mail'] == $auth->user('email')): ?>
            
            <div class="reedeem-block-milio">
                <?php echo $html->link('Redimir', array('controller' => 'gift_users', 'action' => 'redeem', $giftUser['GiftUser']['coupon_code']), array('title' => 'Redimir', 'class' => 'button-anchor', 'escape' => false)); ?>
            </div>
        <?php endif; ?>
        
    </div>
    <div class="gift-milio-box2">
        <div class="card-info">
            <p>Para  <span id="js-gift-to"> <?php echo $giftUser['GiftUser']['friend_name']; ?></span></p>
        </div>
        <p id="js-gift-message" class="card-message">
            <?php echo $giftUser['GiftUser']['message']; ?>
        </p>
    </div>
</div>



