<div class="giftUsers form">
  <h2><?php echo __l('Customize Your Gift Card'); ?></h2>
  <?php echo $form->create('GiftUser', array('class' => 'normal')); ?>
  <div class="clearfix">
    <div class="gift-card clearfix">
      <div class="header"></div>
      <div class="gift-side1">
        <h3 class="gift-title"><span id="js-gift-from"><?php echo $this->data['GiftUser']['from']; ?></span></h3>
        <p> <?php echo __l('has given you'); ?></p>
        <p class="card-amount"><?php echo Configure::read('site.currency'); ?> <span id="js-gift-amount"><?php echo $this->data['GiftUser']['amount']; ?></span></p>
        <div class="remeber-block">
          <p><?php echo __l('Redemption Code:'); ?>
          </p>
          <p class="code-info">
            <strong>xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx</strong>
          </p>
        </div>
      </div>
      <div class="gift-side2">
        <dl class="card-info clearfix">
          <dt><?php echo __l('to'); ?></dt>
          <dd id="js-gift-to"><?php echo $this->data['GiftUser']['friend_name']; ?></dd>
        </dl>
        <p id="js-gift-message" class="card-message">
          <?php echo $this->data['GiftUser']['message']; ?>
        </p>
      </div>
    </div>
    <div class="gift-form">
      <?php
          echo $form->input('user_id', array('type' => 'hidden'));
          echo $form->input('from', array('label' => __l('From'), 'info' => __l('Name you want the recipient to see'), 'class' => '{"update" : "js-gift-from", "default_value" : "Gift Buyer"}'));
          echo $form->input('friend_name', array('label' => __l('Friend Name'), 'class' => '{"update" : "js-gift-to", "default_value" : "Gift Receiver"}'));
          echo $form->input('friend_mail', array('label' => __l('Delivery to Email')));
          echo $form->input('message', array('label' => __l('Personal Message (Optional)'), 'class' => '{"update" : "js-gift-message", "default_value" : "Your Message"}'));
          echo $form->input('amount', array('label' => __l('Gift Card Amount'), 'after' => Configure::read('site.currency'), 'class' => '{"update" : "js-gift-amount", "default_value" : "0"}'));
      ?>
        </div>
      </div>
  <?php
          if (!empty($low_user_balance)):
            echo $html->link(__l('Agregar a mi Cuenta Club Cup&oacute;n y comprar'), array('controller' => 'users', 'action' => 'add_to_wallet'), array('escape'=>false, 'title' => 'Agregar a mi Cuenta Club Cup&oacute;n y comprar'));
          else:
            echo $form->submit(__l('Complete Purchase'));
          endif;
  ?>
  <?php echo $form->end(); ?>

</div>
