<div class="dealCategories">
  <h2>Categor&iacute;as</h2>
  <h4><?php echo $html->link("Agregar categor&iacute;a", array('action' => 'add'), array('escape'=>false)); ?></h4>


  <table class="list">
    <tr>
      <th>Nombre</th>
      <th>Categor&iacute;a padre</th>
      <th></th>
    </tr>
    <?php foreach ($deal_categories as $key => $value): ?>
      <tr>
        <?php
        $edit = $html->link("Editar", array('action' => 'edit', $key));
        $delete = $html->link("Eliminar", array('action' => 'delete', $key),array('class' => 'js-delete'));
        ?>
        <td><?php echo $html->cText($value['name']); ?></td>
        <td><?php echo $html->cText($value['parentName']); ?></td>
        <td><?php echo "[$edit]&nbsp;[$delete]"; ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>
