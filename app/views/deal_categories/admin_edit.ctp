<h2>Categor&iacute;as</h2>
<?php echo $html->link('Volver a listado', array('controller' => 'deal_categories', 'action' => 'index'), array('escape'=>true,'title' => 'Categor&iacute;as de oferta')); ?>
<h3>Editar Categor&iacute;a</h3>

<?php
echo $form->create('DealCategory');
echo $form->input('id');
echo $form->input('parent_id',array('label'=>'Categoria padre'));
echo $form->input('name',array('label'=>'Nombre'));
echo $form->end('Modificar');
