<div class="js-tabs">
    
  <ul class="clearfix">
      
    <?php 
        if ($auth->user('user_type_id') != ConstUserTypes::Company)
        {    
            //echo $html->link(__l('Privacy Settings'), array('controller' => 'user_permission_preferences', 'action' => 'edit', $user_id, 'admin' => false), array('title' => __l('Privacy Settings'), 'rel'=> '#'. __l('Privacy_Settings')));
        }
        if (Configure::read('site.is_api_enabled'))
        {
            echo '<li>' . $html->link(__l('My') . ' ' . Configure::read('site.name') . ' ' . __l(' API'), array('controller' => 'users', 'action' => 'my_api', $auth->user('id'), 'admin' => false), array('title' => __l('Request API Key'), 'rel' => '#' . __l('Request_API_Key'))) . '</li>';
        }
        
        echo '<li>' . $html->link(__l('Actualizar datos'), array('controller' => 'user_profiles', 'action' => 'edit', $user_id, 'admin' => false, 'nocache' => time()), array('title' => __l('My Profile'), 'rel' => '#' . __l('My_Profile'))) . '</li>';
            
        if (!$auth->user('fb_user_id') && !$auth->user('is_openid_register'))
        {
            echo '<li>' . $html->link(__l('Change Password'), array('controller' => 'users', 'action' => 'change_password'), array('title' => __l('Change Password'), 'rel' => '#' . __l('Change_Password'))) . '</li>';
        } 
     ?>
      
  </ul>
    
</div>