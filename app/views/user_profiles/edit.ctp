<style>
    .error-message{padding:0 0 25px 350px; width: 147px; *margin-top:-40px;}
    .error{background-position:100% 4px;height:50px;}
    .errormessage{color:#f00; margin:0 0 12px 186px; clear:both;}
</style>

<script>
    $(document).ready(function($) {
        
        //hack para remover los separadores de fecha, sin tocar el helper
        var f = $('.js-datetime'); 
        f.html(f.html().replace(/-/g, "")); 
        
        
    });
    
</script>

<?php

    if ($auth->user('user_type_id') == ConstUserTypes::Company)
    { 
        echo '<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=' . Configure::read('GoogleMap.api_key') . '" type="text/javascript"></script>';
        echo $this->element('js_tiny_mce_setting', array('cache' => Configure::read('tiny_mce_cache_time.listing_element_cache_duration')));
    }

?>

<form method="post" action="">
<div id="main" class="container_16 main_frt formulario-datos-de-usuario bg-white">
    <div class="bloque-home clearfix">
    
<?php 
                echo $this->element('theme_clean/my_stuff'); 
                echo $form->create ('UserProfile', array ('action' => 'edit', 'class' => 'normal ', 'enctype' => 'multipart/form-data'));
                
                $user_details = array(
                    'username'     => $this->data ['User']['username'],
                    'user_type_id' => $this->data ['User']['user_type_id'],
                    'id'           => $this->data ['User']['id'],
                    );
            ?>


    
    
	<div class="grid_9" >
		
                    
                    <div class="formulario-micuenta-datosu clearfix">
                        	<div class="contenedor-formmicuenta">
            
            
                    <h2>Datos del Usuario</h2>

                    <?php
                        if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id')))
                        {
                            echo $form->input ('User.id',      array ('label' => 'Usuario'));
                        }
                        echo $form->input ('User.username',    array ('label' => 'Mi Nickname'));
                        echo '<div style="clear:both;">';
                        echo $form->input ('first_name',       array ('label' => 'Nombre *'   ));
                        echo '</div>';
                        echo '<div style="clear:both;">';
                        echo $form->input ('last_name',        array ('label' => 'Apellido *' ));
                        echo '</div>';
                        echo '<div class="input text required">';
                        echo $form->label ('gender', 'Genero', array ('class' => ''  )) . $form->select ('gender', ConstGenders::getGenders (), $this->data ['UserProfile']['gender'], array ('class' => ''), '-- Seleccione --');
                        echo '</div>';
                    ?>

                    
                    <div class = "js-datetime">
                        <?php echo $form->input ('dob', array ('label' => 'Fecha de Nacimiento *', 'empty' => 'Selecciona', 'div' => false, 'maxYear' => date ('Y'), 'minYear' => date ('Y') - 100)); ?>
                    </div>
                    
                    
                    <div style="clear:both;">
                        <?php
                            echo $form->input ('dni',  array ('label' => 'DNI'));
                        ?>
                    </div>
                    
                    <div style = "clear: both; text-align: center; display: none;"  id = "geo-loading"><?php echo $html->image ('loading.gif'); ?></div>

                    <?php
                    echo '<div style="clear:both;"></div>';
                        echo $form->input ('country_id',       array ('empty' => '-- ' . 'Selecciona' . ' --', 'id'    => 'UserProfileCountryId', 'label' => 'Pais *'));
                        
                        echo $form->input ('state_id',         array ('type' => 'select', 'empty' => '-- ' . 'Selecciona' . ' --', 'id'    => 'UserProfileStateId',   'label' => 'Provincia *'));
                        echo $form->input ('city_id',          array ('empty' => '-- ' . 'Selecciona' . ' --', 'id'    => 'UserProfileCityId',    'label' => 'Ciudad'));
                        echo '<div class="clear_divs"></div>';
                        echo $form->input ('neighbourhood_id', array ('empty' => '-- ' . 'Selecciona' . ' --', 'label' => 'Barrio'));
                        echo '<div class="clear_divs"></div>';
                        echo $form->input ('zip_code',         array ('label' => 'Código postal *'));
                        echo '<div class="clear_divs"></div>';
                        echo $form->input ('User.email',       array ('label' => 'email', 'readonly' => 'readonly'));
                        echo '<div class="clear_divs"></div>';
                    ?>
                </div>
                        
                        
                        
		</div>

                    <div class="formulario-micuenta datos-celu datos-celu clearfix" >
                    <h2>Datos de Celular</h2>

                    <span class="clearfix">
                        <label>Nro. de celular:</label> <?php echo $form->text ('cell_prefix', array ('class' => 'quince-num', 'maxlength' => 5)); ?> <label style="width:34px;">&ndash; 15 &ndash; </label><?php echo $form->text ('cell_number', array ('class' => 'quince-inp', 'maxlength' => 8)); ?>
                    </span>
                    <span class="clearfix">
                        <label class="proveedor">Proveedor*</label>
                        <?php

                            echo $form->error ('cell_prefix');

                            $carriers = Configure::read ('SMS.celco');
                            $carriers [0] = '-- Seleccione --';
                            ksort ($carriers);
                            echo $form->input ('cell_carrier', array ('label' => false, 'options' => $carriers, 'class'=>'sel'));
                        ?>
                    </span>
                        <?php
                            echo $form->input ('is_sms_enabled', array ('label' => array ('text' => 'Acepto recibir información por SMS', 'style' => 'width: 250px;')));
                        ?>
                </div>
                    
                    <div class="formulario-micuenta clearfix preguntas" >
			<h2>Datos de Perfil del Usuario</h2>
			<h3>Descripcion</h3>

                    <?php
                        echo $form->input ('about_me', array ('label' => false));
                        echo '<span class="clearfix">';
                        echo $clubcupon->questions ('estadocivil',    $this->data ['UserAnswer'], null, false);
                        echo $clubcupon->questions ('hijos',          $this->data ['UserAnswer'], null, false);
                        echo $clubcupon->questions ('edaddehijos',    $this->data ['UserAnswer'], null, false);

                        $out = $clubcupon->questions ('salidas',        $this->data ['UserAnswer'], null, false);
						echo str_replace('<label for="question','<label class="question-label" for="question',$out);
                        $out = $clubcupon->questions ('comidafavorita', $this->data ['UserAnswer'], null, false);
                        echo str_replace('<label for="question','<label class="question-label" for="question',$out);
						$out = $clubcupon->questions ('ofertasinteres', $this->data ['UserAnswer'], null, false);
						echo str_replace('<label for="question','<label class="question-label" for="question',$out);
                        $out = $clubcupon->questions ('otrossitios',    $this->data ['UserAnswer'], null, false);
						echo str_replace('<label for="question','<label class="question-label" for="question',$out);
                        echo '</span>';
                    ?>

                    <script type="text/javascript">
                        // opcional: se puede migrar esto a algún .js
                        $(function(){
                            $('.js-question').change();
                            $('.js-question').bind('change',updateQuestion);

                            $('#UserProfileCountryId').change(function (){
                                $('#geo-loading').show();
                                var parent_id=$(':selected',this).val();
                                var url=cfg.cfg.path_relative+'user_profiles/geoAjax/state/'+parent_id;
                                $('#UserProfileStateId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#UserProfileStateId').change();
                                });
                            });
                            $('#UserProfileStateId').change(function (){
                                $('#geo-loading').show();
                                var parent_id=$(':selected',this).val();
                                var url=cfg.cfg.path_relative+'user_profiles/geoAjax/city/'+parent_id;
                                $('#UserProfileCityId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#UserProfileCityId').change();
                                });
                            });
                            $('#UserProfileCityId').change(function (){
                                $('#geo-loading').show();
                                var parent_id=$(':selected',this).val();
                                var url=cfg.cfg.path_relative+'user_profiles/geoAjax/neigh/'+parent_id;
                                $('#UserProfileNeighbourhoodId').load(url,function () {
                                    $('#geo-loading').hide();
                                    $('#UserProfileNeighbourhoodId').change();
                                });
                            });
                            $('#UserProfileNeighbourhoodId').change(function (){
                                var div=$('#UserProfileNeighbourhood').closest('div');
                                if($(':selected',this).val()=='0')
                                div.show();
                                else
                                div.hide();
                            });
                            $('#question_7').change (function () {
                                if ($(':selected', this).val () == '930')
                                    $('#question_8_div').hide ();
                                else
                                    $('#question_8_div').show ();
                            });
                            $('#question_7').change ();
                            $('html, body').animate ({ scrollTop: 0 }, 0);
                        });
                    </script>
                    
                    <div style="float: left">
                        <p style="color:#999999;font-size: xx-small; padding: 0px; line-height: 20px">
                                Recabamos sus datos con la finalidad de administrar y gestionar la actividad de acceso a nuestros servicios que lo vinculan a nuestra Empresa, conforme lo establecido por el art. 6 de Ley 25323 Usted es responsable por la inexactitud o negativa de proporcionar sus datos.<br /><br />
                                .El titular de datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter&eacute;s leg&iacute;timo al efecto, conforme lo establecido en el art&iacute;culo 14, inciso 3 de la Ley n&ordm; 25.326".<br />
                                La direcci&oacute;n Nacional de Datos Personales, &Oacute;rgano de Control de la Ley n&ordm; 25.326, tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales."<br />
                                Usted podr&aacute; ejercer el derecho de acceso, rectificaci&oacute;n, y supresi&oacute;n de sus datos conforme lo establecido en el art. 6 de la Ley 25326, enviando un email a datospersonales@clubcupon.com.ar o comunicandose telefonicamente al &nbsp;+5411 49438700<br /><br />
                                <span style="font-weight: bold">Para contactar a la Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales:<br /></span>
                                Sarmiento 1118, 5&ordm; piso (C1041AAX)<br />
                                Tel. 4383-8510/12/13/15<br />
                                <a href="http://www.jus.gov.ar/datospersonales">www.jus.gov.ar/datospersonales</a><br />
                                <a href="mailto:infodnpdp@jus.gov.ar">infodnpdp@jus.gov.ar</a>
                        </p>
                    </div>
                    
                    <div class="bloque-form-micuenta">
                        <?php echo $form->submit ('Actualizar mis datos', array ('class'=> 'btn-azul-d micuenta-btn', 'value' => '')); ?>
                    </div>
                    
                </div>
                
                               
                
                    
                    
                </div>
            
                <div class="grid_6 formulario-actualizacion" >
                    <div class="info-actualizacion">
                        <h2>Actualizaci&oacute;n de Datos</h2>
			<h3>Por favor, verific&aacute; que tus datos sean correctos: Nombre, Apellido y DNI.</h3>
                        <h3>Si ten&eacute;s alg&uacute;n inconveniente, contactate con nosotros a trav&eacute;s de nuestro <a href="http://soporte.clubcupon.com.ar/customer/portal/emails/new" target="_blank" >Formulario de Contacto</a></h3>
                        <input name = ""  type = "checkbox"  value = "" class="fl" /><label class="si-quie">Sí, quiero recibir emails con las ofertas diarias de Club Cupón.</label>
			
                        <?php 
                            //echo $form->submit ('Actualizar mis datos', array ('class' => 'btn-gris', 'style'=>'float:left;'));
                        		//echo $form->submit ('Actualizar mis datos', array ('class' => 'btn-gris'));
                        		echo $form->submit ('Actualizar mis datos', array ('class' => 'btn-azul-d'));
                            //echo $form->end ();
                        ?>
                    </div>
                </div>
    
                <!-- fin contenidos -->
                </div>
            </div>
   <?php echo $form->end ();?>
