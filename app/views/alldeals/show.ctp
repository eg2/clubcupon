<script>
$(document).ready(function() {
    
 	$(".blq-item").mouseenter(function() {;
		$(".bloquealpha", this).show();
	});
	
 	$(".blq-item").mouseleave(function() {;
		$(".bloquealpha", this).hide();
	});	

});  //fin doc ready
</script>

<div id="main" class="container_16  suscripciones-css">
	<div class="grid_16 conten-todaslasofertas">
        
        <p class="categorias-todas"><?php echo $titleforpage; ?></p>
        
		<div class="sec-izq">
            
            <?php echo $this->element('theme_clean/cities_sidebar'); ?>
            
		</div>
		
		<div class="sec-der">
            
            <?php
            
            if(!empty($dealsoftheday))
            { 
            
                $index=1;
                foreach($dealsoftheday as $idx => $small_deal)
                {
                    
                    $small_deal_title    = $clubcupon->shorten_strings_with_html_entities($small_deal['Deal']['name'],     35);
                    $small_deal_subtitle = $clubcupon->shorten_strings_with_html_entities($small_deal['Deal']['subtitle'], 35);
                    
                    if($small_deal['Deal']['hide_price'])
                    {
                        $titulo_oferta = $titulo_oferta_hover = $small_deal['Deal']['discount_percentage'] . '% de descuento';
                    }
                    else
                    {
                        $titulo_oferta = 'Pag&aacute; $' . $html->cCurrency (floor($small_deal['Deal']['discounted_price']) , 'small') . ' en vez de  $' . $html->cCurrency (floor($small_deal['Deal']['original_price']) , 'small');
                    }
                    
                    $image_tag   = $html->showImage('Deal', $small_deal['Attachment'], array('dimension' => 'medium_big_thumb', 'alt' => $html->cText($small_deal['Company']['name'] . ' ' .$small_deal['Deal']['name'], false), 'title' => $html->cText($small_deal['Company']['name'] . ' ' .$small_deal['Deal']['name'], false)));
                    $image_tag   = str_replace('width="440"',  'width="212"',  $image_tag);
                    $image_tag   = str_replace('height="292"', 'height="140"', $image_tag);
                    $nombreModal = 'modal'.$index; 
                    $sOferta     = $small_deal['Deal']['slug'];
                    $sUrl        = '/'.$city_slug .'/deal/'.$sOferta;
                    $index++;
                    
            ?>
            
			<div class="blq-item">
				<div class="bloquealpha" id="<?php echo $nombreModal; ?>">
					<div class="descripcion"><?php echo $titulo_oferta; ?></div>
					<div class="btnVerMas">
                        <?php echo $html->link('Ver m&aacute;s', array('controller' => 'deals', 'action' => 'view', $small_deal['Deal']['slug']), array('title' => $html->cText($small_deal['Deal']['name'], false), 'escape' => false)); ?>                        
                    </div>
					<div class="porcentaje"><span><?php echo $small_deal['Deal']['discount_percentage']; ?>%</span></div>
				</div>
				<?php echo $html->link($image_tag,     array('controller' => 'deals', 'action' => 'view', $small_deal['Deal']['slug']), array('title' => sprintf(__l('%s'), $small_deal['Deal']['name'])), null, false); ?>
				<h3><?php echo $small_deal_title; ?></h3>
				<p><?php echo $small_deal_subtitle; ?></p>
			</div>
            
            <script type="text/javascript">
                $(document).ready(function () {

                    $("#<?php echo $nombreModal;?>").click(function () {

                        window.location = "<?php echo $sUrl;?>"
                    });
                });
            </script>
            <?php
            
                }
            }
            
            ?>
		</div>
	</div>
</div> <!-- CIERRE MAIN-->