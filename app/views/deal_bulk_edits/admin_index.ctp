<style>
    <!--
    table {
        float: left;
        width: 100%;
    }
    th {
        background: linear-gradient(to bottom, #f9c667 0%, #f79621 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #fff;
        font-size: 16px;
        font-weight: bold;
        min-height: 25px;
        padding: 5px !important;
        text-align: center;
    }
    td {
        padding-left: 5px;
        text-align: center;
    }
    #pagingLinks{}
    #pagingLinks li{
        float:left;
        padding: 5px;
    }
    .pagLinkArrow{
        font-weight: bold;
    }
    .input.text{
        margin: 0px !important;
    }
    -->
</style>
<div id ="contents" class="wide">
    <h2>Administraci&oacute;n de Ofertas:</h2>
    <?php
    echo $this->element(Configure::read('theme.theme_name') . 'deal_bulk_edits_search');
    if (isset($statusSave)) {
        if ($statusSave) {
            echo '<div style="padding:0px 0px 10px 10px;float:left"><div style="float:left;background-color: #dff0d8;border-color: #d6e9c6;color: #3c763d;border-radius:4px;font-size:20px;padding: 15px;">';
            echo 'Los Ofertas fueron actualizadas';
            echo '</div></div>';
        } else {
            echo '<div style="padding:0px 0px 10px 10px;float:left"><div style="float:left;background-color: #f2dede;border-color: #ebccd1;color: #a94442;border-radius:4px;font-size:20px;padding: 15px;">';
            echo 'Algunas Ofertas no fueron actualizadas.';
            echo '</div></div>';
        }
    }
    if (isset($dealsEndDateErrors) && !empty($dealsEndDateErrors)) {
        echo '<div style="padding:0px 0px 10px 10px;float:left"><div style="float:left;background-color: #f2dede;border-color: #ebccd1;color: #a94442;border-radius:4px;font-size:20px;padding: 15px;">';
        echo 'Las fechas en rojo tienen errores y no fueron actualizadas';
        echo '</div></div>';
    }
    echo $form->create('deal_bulk_edits', array('action' => 'index'));
    echo $form->hidden('status', array('value' => $dealStatusSelected));
    echo $form->hidden('company_slug', array('value' => $companySlugValue));
    echo $form->hidden('city_slug', array('value' => $citySlugValue));
    echo $form->hidden('start_year', array('value' => $dealStartDateSelected['year']));
    echo $form->hidden('start_month', array('value' => $dealStartDateSelected['month']));
    echo $form->hidden('start_day', array('value' => $dealStartDateSelected['day']));
    echo $form->input('action', array('type' => 'hidden', 'value' => 'save'));
    echo '<table>';
    echo '<thead>';
    echo '<tr>';
    echo '<th>Id</th>';
    echo '<th>Oferta</th>';
    echo '<th style="width:100px;text-align:left">Enviado</th>';
    echo '<th style="width:150px;text-align:center">Prioridad</th>';
    echo '<th>Estado</th>';
    echo '<th>Tipo</th>';
    echo '<th>Fecha de inicio</th>';
    echo '<th style="width:150px;text-align:center">Fecha de fin</th>';
    echo '<th>Empresa</th>';
    echo '<th>Ciudad</th>';
    echo '</tr>';
    echo '</thead><tbody>';
    foreach ($this->data['Deals'] as $deal) {
        echo '<tr>';
        ?>
        <td><?php echo $deal['Deal']['id']; ?>
        </td>
        <td><?php echo $deal['Deal']['name']; ?>
        </td>
        <td><?php
            echo $form->input('Deal.is_subscription_mail_sent-' . $deal['Deal']['id'], array('type' => 'checkbox', 'id' => 'mail_' . $deal['Deal']['id'], 'div' => false, 'label' => false, 'checked' => empty($deal['Deal']['is_subscription_mail_sent']) ? false : true));
            ?></td>
        <td><?php
            echo $form->input('Deal.priority-' . $deal['Deal']['id'], array('id' => 'priority_' . $deal['Deal']['id'], 'label' => '', 'value' => $deal['Deal']['priority']));
            ?></td>
        <td><?php echo ConstDealStatus::getFriendly($deal['DealStatus']['id']); ?></td>
        <td><?php echo $deal['Deal']['is_variable_expiration'] ? "Variable" : "Fijo"; ?></td>
        <td><?php
            echo strftime("%d/%m/%Y", strtotime($deal[0]['start_date']));
            echo $form->hidden('Deal.start_date-' . $deal['Deal']['id'], array('value' => strftime("%Y-%m-%d %H:%M:%S", strtotime($deal[0]['start_date']))));
            echo $form->hidden('Deal.coupon_expiry_date-' . $deal['Deal']['id'], array('value' => strftime("%Y-%m-%d %H:%M:%S", strtotime($deal[0]['coupon_expiry_date']))));
            echo $form->hidden('Deal.end_date_original-' . $deal['Deal']['id'], array('value' => strftime("%Y-%m-%d %H:%M:%S", strtotime($deal[0]['end_date']))));
            echo $form->hidden('Deal.is_variable_expiration-' . $deal['Deal']['id'], array('value' => $deal['Deal']['is_variable_expiration']));
            echo $form->hidden('Deal.coupon_duration-' . $deal['Deal']['id'], array('value' => $deal['Deal']['coupon_duration']));
            echo $form->hidden('Deal.coupon_start_date-' . $deal['Deal']['id'], array('value' => strftime("%Y-%m-%d %H:%M:%S", strtotime($deal['Deal']['coupon_start_date']))));
            ?></td>
        <td>
            <?php echo $form->input('Deal.end_date-' . $deal['Deal']['id'], array('label' => false, 'id' => 'end_date_' . $deal['Deal']['id'])); ?>
        </td>
        <td><?php echo $deal['Company']['name']; ?></td>
        <td><?php echo $deal['City']['name']; ?></td>
        <?php
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '<hr>';
    echo $form->end('Guardar');
    ?>
    <ul id="pagingLinks">
        <li>
            <?php echo str_replace('/all', '/' . $param_search, str_replace($param_search, '', $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class' => 'disabled')))); ?>
        </li>
        <li>
            <?php echo str_replace('/all', '/' . $param_search, str_replace($param_search, '', $paginator->numbers(array('class' => 'pagLink')))); ?>
        </li>
        <li>
            <?php echo str_replace('/all', '/' . $param_search, str_replace($param_search, '', $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class' => 'disabled')))); ?>
        </li>
    </ul>
</div>
<script>
    $(document).ready(function () {
        $('.displaydate.displaydate1').css('border-radius', '5px');
        $('.displaydate.displaydate1').css('background', 'none repeat scroll 0 0 #eee');
        $('.timepicker').css('display', 'none');
        $('.time-desc.datepicker-container').css('width', '150px');
        $("input[id^='end_date']").datepicker({dateFormat: 'dd/mm/yy'});
<?php foreach ($dealsEndDateErrors as $dealError) { ?>
            $('#end_date_<?php echo $dealError; ?>').css('background', '#f2dede');
<?php } ?>
    });
</script>