<?php

?>
  
<script>
    
    $(document).ready(function() {
        
        clearview();
    

    });
</script>
<style>

form .info {
padding-bottom: 10px !important;
width:100% !important;
}
    
    .top{
        height:110px;
        background:#f0f0f0;
        background: url(/img/web/clubcupon/new/images/hicon-beacon.png) no-repeat scroll center center transparent;
        border-bottom: 1px solid #eee;
    }
    .top .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
            
        }
    
    .left-block, .right-block{
    	/*height:320px*/
    }
    .left-block{
        padding:15px;
        float:left;
        width:275px;
       /* background:#fafafa;*/
    }
        .left-block .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }
        .left-block p{
            font-style: italic;
            font-size:12px;
        }
    
    .right-block{
        padding:3px 15px;
        float:right;
        width:910px;
        background:#fff;
        position: relative;
    }
    
    .calendarTop{width:212px!important;}
    h3{width:100%!important;}
    
    input{float:none;}
    table{margin-top:15px;}
    .formatDateInput{background:none; border:none; color:#f79621; font-weight:bold; width:325px; padding-left:0;}
    .timeTitle{
        font-weight: bold;
        font-size: 14px;
        font-style: italic;
        margin-top:15px;
        display:block;
        }
        
    #wrapper-since, #wrapper-until, #wrapper-execution, .submitButton{
        position:absolute;
    }
    .ui-datepicker-inline{
        min-height: 255px;
    }
    
    #wrapper-since{
        left:0;
        width:211px;
    }
    #wrapper-until{
        left:238px;
        width:211px;
    }
    #wrapper-execution{
        left:476px;
        width:448px;
    }
        #wrapper-execution .calendarTop{ width:436px!important;}
        
    .submitButton{
       /* top:312px;*/
        left:370px;
        bottom:20px;
    }
    .submitButton input{
        background: orange;
        font-weight:bold;
        border:none;
        color:#fff;
        height:35px;
        width:170px;
    }
    
    .ui-datepicker-current {visibility: hidden!important;}
    
    .error-message{
        float:left;
        width:100%;
        margin-bottom:15px;
    }
    .required {
    background: url(/img/required_label_bg.png) no-repeat center right;
}
</style>
<?php 

//var_dump($this->validationErrors);

$titleForm="Creaci&oacute;n de dispositivo BEACON";
$actionForm='add';
if(isset($this->data['BeaconDevice']['id']) && !empty($this->data['BeaconDevice']['id'])){
	//$actionForm='edit';
	$titleForm="Edici&oacute;n de dispositivo BEACON";
}
?>
<div class="top">
    <span class="title1"><?php echo $titleForm;?></span>
</div>


<?php echo $form->create('BeaconDevice',array ('action'    => $actionForm,         'id'    => 'beaconDeviceForm')); ?>
<?php
//if($actionForm=='edit'){
echo $form->input   ('id',     array ('type'   => 'hidden'));
//}
echo $form->input('actionForm',     array ('type'   => 'hidden','value' => $actionForm));
?>
<div class="left-block">
 	<p class="required" style="color:red;width:100px;padding-left:400px">
    Campos obligatorios
    </p>
    <span class="title2">Dispositivo</span>
    <p>
        
         <span class="timeTitle required">Nombre:</span>
        <?php echo $form->input   ('name',     array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        <span class="info">Nombre descriptivo. Por ejemplo: Beacon violeta 001</span><br>
        <span class="timeTitle required">mac:</span>
        <?php echo $form->input   ('mac',     array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
 		<span class="info">un identificador único de dispositivo. Por ejemplo: DF:73:95:B6:2F:95  </span><br>
       
        <span class="timeTitle required">uuid:</span>
        <?php echo $form->input   ('uuid', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        <span class="info">ID de proximidad. Por ejemplo: 2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6 </span>
      
        <span class="timeTitle required">major:</span>
        <?php echo $form->input   ('major', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        <span class="info">N&uacute;mero entero de 0 a 65535  </span><br><br><br>
      
      <span class="timeTitle required">minor:</span>
        <?php echo $form->input   ('minor', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        <span class="info">N&uacute;mero entero de 0 a 65535  </span><br>
      
     </p>

    <?php
          echo $form->submit('Guardar',array ('div'       => 'submitButton'));
    ?>
</div>
<?php echo $form->end(); ?>