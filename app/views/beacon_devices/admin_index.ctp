<script>
    $(document).ready(function () {
        clearview();
        $(':checkbox.readonly').click(function () {
            return false;
        });
    });
</script>
<style>
    .top{
        height:70px;
        background:#f0f0f0;
    }
    .top .title1{
        font-weight: bold;
        font-size: 23px;
        padding: 35px 0 0 5px;
        display: block;
        color:#444;
        float:left;
    }
    .left-block, .right-block{min-height:380px}
    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
    .left-block .title2{
        font-weight:bold;
        font-size:18px;
        padding-bottom: 4px;
        display: block;
        color:#f79621;
    }
    .left-block p{
        font-style: italic;
        font-size:12px;
        text-align:right;
    }
    .legend p{
        font-style: italic;
        font-size:12px;
    }
    .legend{
        padding: 8px 0 0 15px;
        float:left;
    }
    .right-block{
        padding:3px;
        float:right;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block{
        padding:3px;
        float:left;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block table{
        width:100%;

    }
    .right-block table{
        width:924px;

    }
    thead td, .orangeHeader
    {
        padding:        5px!important;
        min-height:     25px;
        min-width:    100px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }
    tr td{padding:5px;}
    .deleteIcon, .editIcon, .approveIcon, .rejectIcon, .detailsIcon .updateVisibility{
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0px;
        width:80px;
        display:block;
        float:left;
    }
    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    .detailsIcon{
        background-color: #7F7F7F;
    }
    .updateVisibility{
        background-color: #e7c3c3;
    }
    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
        float: right;
    }
    #pagingLinks{}
    #pagingLinks li{
        float:left;
        padding: 5px;
    }
    .pagLinkArrow{
        font-weight: bold;
    }
    .tblActions{
    }
    .tblActions td{
        margin:0px;
        padding:0px;
        padding-bottom:2px;
    }
</style>

<div class="top">
    <span class="title1">Dispositivos Beacons</span>
    <span class="legend">
        <p>
            <?php
            
            echo $html->link('Crear un nuevo beacon', array(
                'controller' => 'beacon_devices',
                'action' => 'add'
                    ), array(
                'escape' => false,
                'class' => 'resetLink'
            ));
            
            ?>
        </p>
    </span>
</div>

<div class="center-block">
    <table>
        <thead>
        <td>ID</td>
        <td>NOMBRE</td>
        <td>MAC</td>
        <td>UUID</td>
        <td>MAJOR</td>
        <td>MINOR</td>
        <td>Acciones</td>
        </thead>

        <?php
        foreach ($beacondevices as $beacondevice) {
            $edit = $html->link("Editar", array(
                'action' => 'edit',
                $beacondevice['BeaconDevice']['id']
                    ), array(
                'class' => 'editIcon'
            ));
            $delete = $html->link("Borrar", array(
                'action' => 'delete',
                $beacondevice['BeaconDevice']['id']
                    ), array(
                'class' => 'deleteIcon'
                    ), '¿Seguro querés borrar este dispositivo?');
           
            echo '<tr>';
            $form->create('BeaconDevice');
            echo '<td>' . $beacondevice['BeaconDevice']['id'] . '</td>';
            echo '<td>' . $beacondevice['BeaconDevice']['name'] . '</td>';
            echo '<td>' . $beacondevice['BeaconDevice']['mac'] . '</td>';
            echo '<td>' . $beacondevice['BeaconDevice']['uuid'] . '</td>';
            echo '<td>' . $beacondevice['BeaconDevice']['major'] . '</td>';
            echo '<td>' . $beacondevice['BeaconDevice']['minor'] . '</td>';
            $form->end();
            echo '<td style="float:left">';
            echo '<table class="tblActions">';
            
                echo '<tr><td>';
                
                echo $html->link("Editar", array(
					'controller' => 'beacon_devices',
                    'action' => 'edit',
                    $beacondevice['BeaconDevice']['id']
                        ), array(
                    'class' => 'link-button-generic',
                    'style' => 'float:left;width:200px;'
                ));
               
                echo '</td></tr>';
                echo '<tr><td>';
                
                echo $html->link("Asignar Ofertas", array(
                		'controller' => 'beacon_device_deals',
                		'action' => 'assign',
                		$beacondevice['BeaconDevice']['id']
                ), array(
                		'class' => 'link-button-generic',
                		'style' => 'float:left;width:200px'
                ));
                 
                echo '</td></tr>';
                 
           
                echo '<tr><td>';
               
                echo $html->link("Borrar", array(
                    'action' => 'delete',
                    $beacondevice['BeaconDevice']['id']
                        ), array(
                    'class' => 'js-confirm-action-link link-button-generic',
                    'style' => 'float:left'
                        ));
                        
                echo '</td></tr>';
            
            
            echo '<tr><td>';
           
            echo '</td></tr>';
            echo '</table>';
            echo '</td>';
            echo '</tr>';
        }
        ?>

    </table>
    <ul id="pagingLinks">
        <li>
            <?php
            echo $paginator->prev('< Anterior ', array(
                'escape' => false,
                'class' => 'pagLinkArrow'
                    ), null, array(
                'class' => 'disabled'
            ));
            ?>
        </li>
        <li>
            <?php
            echo $paginator->numbers(array(
                'class' => 'pagLink'
            ));
            ?>
        </li>
        <li>
            <?php
            echo $paginator->next(' Siguiente >', array(
                'escape' => false,
                'class' => 'pagLinkArrow'
                    ), null, array(
                'class' => 'disabled'
            ));
            ?>
        </li>
    </ul>
</div>