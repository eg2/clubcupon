<fieldset>
  <h2 class = "green-head">Alta de banner</h2>
  <?php
    $statusOptions = array (0 => 'Inactivo', 1 => 'Activo');
    // $this->data ['Banner'] = $this->data;  // esto es para sacarlo rapido, la posta es decirle a cada input que valor va a atomar y no esperar que CakePHP lo infiera
    echo $form->create ('Banner', array ('class' => 'normal', 'enctype' => 'multipart/form-data'));
    echo $form->input ('name', array ('label' => 'Nombre'));
    echo $form->input ('status', array (
        'type'    => 'select',
        'options' => $statusOptions,
        'empty'   => false,
        'label'   => 'Estado',
    ));
    echo $form->input ('order', array ('label' => 'Orden'));
    echo $form->input ('banner_type_id', array (
        'type'    => 'select',
        'options' => $banner_types,
        'empty'   =>  false,
        'label'   => 'Tipo',
    ));
    echo $form->input ('position', array ('value' => '', 'type' => 'hidden'));  // posible feature futuro
    // echo $form->input ('style', array ('label' => 'Estilo CSS', 'value' => 'box_banner'));
    echo $form->input ('style', array ('label' => 'Estilo CSS', 'value' => 'box_banner', 'type' => 'hidden'));  // Prevengo que quede null
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    $cities = $clubcupon->activeCitiesForBanners ();
    $groups = $clubcupon->activeGroups ();

    // Lo formateo en un nuevo array de datos
    $cities_list = array ('' => 'Todas');  // para todas las ciudades
    foreach ($cities as $city) {
      $tmp_city_id                = $city ['City']['id'];
      $tmp_city_name              = $city ['City']['name'];
      $cities_list [$tmp_city_id] = $tmp_city_name;
    }
    
    foreach ($groups as $group) {
      $tmp_group_id                = $group ['City']['id'];
      $tmp_group_name              = $group ['City']['name'];
      $cities_list [$tmp_group_id] = $tmp_group_name;
    }
    
    //Lo aplico al elemento
    echo $form->input ('city_id', array (
        'type'     => 'select',
        'options'  => $cities_list,
        'empty'    => false,
        'label'    => 'Ciudad',
        'selected' => $city_id,
    ));
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // formato para los campos de fecha
    echo "<div class=\"input clearfix required\">";
    echo   "<div class=\"js-datetime\">";
    echo     $form->input ('start_date', array ('label' => 'Fecha Inicio', 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => 'Seleccionar fecha de inicio'));
    echo   "</div>";
    echo "</div>";
    echo  "<div class=\"input clearfix required\">";
    echo    "<div class=\"js-datetime\">";
    echo      $form->input ('end_date', array ('label' => 'Fecha Cierre', 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => 'Seleccionar fecha de cierre'));
    echo    "</div>";
    echo  "</div>";
    echo $form->input ('image', array ('label' => 'Imagen', 'type' => 'file'));
    echo $form->input ('link', array ('label' => 'Link'));
    echo $form->input ('alt', array ('label' => 'Alt'));
    echo $form->input ('tracker_url', array ('label' => 'Tracker URL'));
    echo $form->end ('Crear Banner');
  ?>
</fieldset>