<fieldset>
  <h2>Editar Banner</h2>
  <?php
    $statusOptions = array (0 => 'Inactivo', 1 => 'Activo');
    // $this->data ['Banner'] = $this->data;
    echo $form->create ('Banner', array ('class' => 'normal', 'enctype' => 'multipart/form-data'));
    echo $form->input ('name', array ('label' => 'Nombre'));
    echo $form->input ('status', array (
        'type'    => 'select',
        'options' => $statusOptions,
        'empty'   => false,
        'label'   => 'Estado',
    ));
    echo $form->input ('order', array ('label' => 'Orden'));
    echo $form->input ('banner_type_id', array (
        'type'    => 'select',
        'options' => $banner_types,
        'empty'   =>  false,
        'label'   => 'Tipo',
    ));
    echo $form->input ('position', array ('value' => '', 'type' => 'hidden'));  // posible feature futuro
    // echo $form->input ('style', array ('label' => 'Estilo CSS', 'value' => 'box_banner'));
    echo $form->input ('style', array ('label' => 'Estilo CSS', 'value' => 'box_banner', 'type' => 'hidden'));  // Prevengo que quede null
    echo $form->input('id', array('type'=>'hidden', 'value' => $this->data ['Banner']['id']));
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    $cities = $clubcupon->activeCitiesForBanners ();
    $groups = $clubcupon->activeGroups ();
    
    // Lo formateo en un nuevo array de datos
    $cities_list = array ('' => 'Todas');  // para todas las ciudades
    foreach ($cities as $city) {
      $tmp_city_id                = $city ['City']['id'];
      $tmp_city_name              = $city ['City']['name'];
      $cities_list [$tmp_city_id] = $tmp_city_name;
    }

    foreach ($groups as $group) {
      $tmp_group_id                = $group ['City']['id'];
      $tmp_group_name              = $group ['City']['name'];
      $cities_list [$tmp_group_id] = $tmp_group_name;
    }
    
    
    
    //Lo aplico al elemento
    echo $form->input ('city_id', array (
        'type'     => 'select',
        'options'  => $cities_list,
        'empty'    => false,
        'label'    => 'Ciudad',
        'selected' => $this->data['Banner']['city_id'],
    ));
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //formato para los campos de fecha
    echo "<div class=\"input clearfix required\">";
    echo   "<div class=\"js-datetime\">";
    echo     $form->input ('start_date', array ('label' => 'Fecha Inicio', 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => 'Seleccionar fecha de inicio'));
    echo   "</div>";
    echo "</div>";
    echo "<div class=\"input clearfix required\">";
    echo   "<div class=\"js-datetime\">";
    echo     $form->input ('end_date', array ('label' => 'Fecha Cierre', 'minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'div' => false, 'empty' => 'Seleccionar fecha de cierre'));
    echo   "</div>";
    echo "</div>";
    echo $form->input ('image', array ('label' => 'Imagen', 'type' => 'file'));
    echo "<div style=\"padding:15px; width:260px; clear:both; \">";
		echo   $html->image ($this->data ['Banner']['image'], array ('alt' => $this->data ['Banner']['alt']));
    echo "</div>";
    echo $form->input ('link', array ('label' => 'Link'));
    echo $form->input ('alt', array ('label' => 'Alt'));
    echo $form->input ('tracker_url', array ('label' => 'Tracker URL'));
    echo $form->end ('Modificar Banner');
  ?>
</fieldset>