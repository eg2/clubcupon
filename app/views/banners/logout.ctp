<?php
if (isset($banners)) {
	foreach ($banners as $banner) {
		$htmlOutput  = "<div class=\"" . $banner['Banner']['style'] . "\" >";
		$htmlOutput .= "<a href=\""    . $banner['Banner']['link']  . "\" >";
		$htmlOutput .= "<img src=\""   . $banner['Banner']['image'] . "\"  ";
		$htmlOutput .= "alt=\""        . $banner['Banner']['alt']   . "\" >";
		$htmlOutput .= "</a>";
		$htmlOutput .= "</div>";
		echo $htmlOutput;
	}
}else{
	echo '<div class="box_banner"><img alt="#" src="/img/Banner/banner_logout-default.jpg"></div>';
}