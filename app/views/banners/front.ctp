<?php
if (isset($banners)) {
	foreach ($banners as $banner) {
		$htmlOutput  = "<div class=\"" . $banner->style . "\" >";
		$htmlOutput .= "<a href=\""    . $banner->link  . "\" >";
		$htmlOutput .= "<img src=\""   . $banner->image . "\"  ";
		$htmlOutput .= "alt=\""        . $banner->alt   . "\" >";
		$htmlOutput .= "</a>";
		$htmlOutput .= "</div>";
		echo $htmlOutput;
	}
}