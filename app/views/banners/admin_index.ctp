<div class = "dealCategories">
  <h2>Banners</h2>
  <div class = "clearfix add-block1">
    <?php echo $html->link ("Agregar Banner", array ('controller' => 'banners', 'action' => 'add'), array ('class' => 'add', 'title' => 'Agregar Banner')); ?>
  </div>
  
  <?php echo $this->element('paging_links'); ?>
  
  <table class = "list">
    <tr>
      <th>Nombre</th>
      <th>Estado</th>
      <th>Tipo</th>
      <th>Orden</th>
      <th>Ciudad</th>
      <th>Fecha Inicio</th>
      <th>Fecha Cierre</th>
      <th>Imagen</th>
      <th>Link</th>
      <th>Descripci&oacute;n</th>
      <th>Tracker URL</th>
      <th width="80">Acci&oacute;n</th>
    </tr>
    <?php
      // var_dump ($banners); die;
      $banners = Set::map ($banners, 'BannerRow');
      foreach ($banners as $banner) {
        $edit       = $html->link ("Editar", array ('action' => 'edit',   $banner->id), array ('class' => 'link-button-generic'));
        $delete     = $html->link ("Borrar", array ('action' => 'delete', $banner->id), array ('class' => 'js-confirm-action-link link-button-generic'));
        //$bannerLink = Router::url ('/', true) . $banner->link;
        $bannerLink = $banner->link;

        $output  = "<tr>";
        $output .= "<td>" . $banner->name . "</td>";
        if ($banner->status == 1) {
          $bannerStatus = 'Publicado';
          $icon = 'Banner/icon-published.png';
        } else {
          $bannerStatus = 'Despublicado';
          $icon = 'Banner/icon-unpublished.png';
        }
        $output .= "<td>" . $html->image ($icon, array ('alt' => $bannerStatus)) . "</td>";
        $output .= "<td>" . $banner->BannerType->label . "</td>";
        $output .= "<td>" . $banner->order . "</td>";
        if ($banner->City->name == '') {
          $cityLabel = 'Todas';
        } else {
          $cityLabel = $banner->City->name;
        }
        $output .= "<td>" . $cityLabel . "</td>";
        $output .= "<td>" . $banner->start_date . "</td>";
        $output .= "<td>" . $banner->end_date . "</td>";
        $output .= "<td>" . $html->image ($banner->image, array ('alt' => $banner->alt, 'width' => '125')) . "</td>";
        // $output .= "<td>" . $banner->link . "</td>";
        $output .= "<td><a href=\"" . $bannerLink . "\" target=\"_blank\" >" . $bannerLink . "</a></td>";
        $output .= "<td>" . $banner->alt . "</td>";
        $output .= "<td>" . $banner->tracker_url . "</td>";
        $output .= "<td>" . $edit . "<br />" . $delete . "</td>";
        $output .= "</tr>";

        echo $output;
      }
    ?>
  </table>  
</div>