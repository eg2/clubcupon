<script>

</script>
<style>
    
    .top{
        height:55px;
        background:#f0f0f0;
    	padding:5px;
    }
        .top .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 5px;
            display: block;
            color:#444;
        	float:left;
        	width: 100%;
        }
    
    .left-block, .right-block{min-height:380px}
    
    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
        .left-block .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }
        .left-block p{
            font-style: italic;
            font-size:12px;
        	text-align:right;
        }
        .legend p{
            font-style: italic;
            font-size:12px;
        }
        .legend{
        	padding: 8px 0 0 15px;
        	float:left;
        }
    
    .right-block{
        padding:3px;
        float:right;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block{
        padding:3px;
        float:left;
        width:937px;
        background:#fff;
        position: relative;
    }
    .center-block table{
        width:924px;
        
    }
    .right-block table{
        width:924px;
        
    }
    .listaVacia{
    	font-color:#CAC9C7;
    	font-style:italic
    }
    
    
    thead td, .orangeHeader
    {
        padding:        5px!important;
        min-height:     25px;
        background:     #f9c667;
        background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
        background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
        background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
        background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
        filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
        color:#fff;
        font-size:16px;
        font-weight:bold;
    }
    
    tr td{padding:5px;}
    
    
    .deleteIcon, .editIcon, .approveIcon, .rejectIcon, .detailsIcon {
        color:#fff;
        font-weight:bold;
        padding:2px 5px;
        font-size: 12px;
        margin:0 3px;
        width:80px;
        display:block;
        float:left;
    }
    
    .deleteIcon{
        background-color: #E31948;
    }
    .editIcon{
        background-color: #97C41A;
    }
    .approveIcon{
        background-color: #5BAAEB;
    }
    .rejectIcon{
        background-color: #333;
    }
    .detailsIcon{
    	background-color: #7F7F7F;
    }
    
    .resetLink{
        background: #333;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        float: left;
        margin-top: 15px;
    	float: right;
    }
    
    #pagingLinks{}
    
        #pagingLinks li{
            float:left;
            padding: 5px;
        }
    
        .pagLinkArrow{
            font-weight: bold;
        }
        
    .input{
    	/*padding-top:10px;*/
    	margin-top:0px;
    } 
    .submit{
    	width:100px;
    }
    .submit input{
    	margin-top:0px;
    }
    .submit{
    	margin-top:0px;
    }
  
    .izq label{
    	 min-width: 0px;
    }
    .input, .text{
    	width:230px;
    }
    .legend{
    	padding-top:0px;
    }
    .legend p {
        font-size: 11px;
		padding-left:210px;
    }
    .top .title1 {
    	padding-top:5px;
    }
</style>
<script>
function procesar(combo){
  
	if(combo.value=='model'){
		document.getElementById('AccountingItemQ').style.display = 'none';
		document.getElementById('subQ').style.display = 'none';
		document.getElementById('AccountingItemFilterMode').style.display = 'inline';
	}else{
		if(combo.value=='all'){
			document.getElementById('AccountingItemQ').style.display = 'none';
			document.getElementById('subQ').style.display = 'none';
			document.getElementById('AccountingItemFilterMode').style.display = 'none';
			  //window.location.href = 'http://<?php echo$_SERVER['HTTP_HOST']; ?>/admin/accounting_items/index/<?php echo $calendar["AccountingCalendar"]["id"]; ?>';
			window.location.href = 'http://'+document.domain+'/admin/accounting_items/index/<?php echo $calendar["AccountingCalendar"]["id"]; ?>';
			
		}else{
			document.getElementById('AccountingItemQ').style.display = 'inline';
			document.getElementById('subQ').style.display = 'inline';
			document.getElementById('AccountingItemFilterMode').style.display = 'none';
			
		}
	}
}
</script>

<div class="top">
<?php
        
        echo $form->create("AccountingItem",array('action' => 'search','style'=>'width:700px;height:55px;'));
        echo '<div  class="izq" style ="float:left;width:460px;">';
        echo $form->input ('filter', array(
        		'label' => 'Buscar por:',
        		'options' => array(
        				'all'=>' ver todos ',
        				'id'=>'Id Item',
        				'model'=>'Tipo Usuario',
        				'model_id'=>'Id Usuario',
        				'deal_id'=>'Id Oferta',
        				
        		 		),
        		 'onchange' => 'procesar(this);'));
   
        $displayQ='none';
        $display='none';
        if(isset($this->data['AccountingItem']['filter'])){ 
        	if($this->data['AccountingItem']['filter']=='model' || $this->data['AccountingItem']['filter']=='all'){
        		$display='inline';
        		$displayQ='none';
        	}else{
        		$display='none';
        		$displayQ='inline';
        	}
        }
        echo $form->input('q',array('label'=>false,'style'=>'display:'.$displayQ));
        echo $form->input ('filter_mode', array(
        		'label' => false,
        		'options' => array(
        				AccountingItem::MODEL_COMPANY=>'Comercio',
        				AccountingItem::MODEL_USER=>'Usuario Final'
        				),
        		'style'=>'display:'.$display.';float:left;margin-top: 0px;',
        		'class'=>'clsTipoUsuario'));
        echo $form->input('accounting_calendar_id', array('type'  => 'hidden',  'value'=> $calendar['AccountingCalendar']['id']));
        if(isset($param_search) && !empty($param_search)){
        	echo $form->input('param_search', array('type'  => 'hidden',        'value'=> $param_search));
        }else{
        	echo $form->input('param_search', array('type'  => 'hidden',        'value'=> ''));        	
        }
        ?>
        <span class="legend" style="font-size:10px;display:<?=$displayQ?>" id="subQ">
        <p>
        Si quer&eacute;s buscar parte de un Id escribilo entre signos de "%".
        Ej: %123% 
        </p>

        </span>
        <?php
        echo '</div>';
        
        $submit_options    = array(
            'id'    => 'buscar',
            'label' => 'Buscar',
            
            );
        echo $form->end($submit_options);
        
        
?>

<span class="title1">Calendario (<?php echo $calendar['AccountingCalendar']['id'];?>)</span>
</div>
<div class="center-block">
<table>
<thead>
    <td>Desde</td>
    <td>Hasta</td>
    <td>Fecha de ejecuci&oacute;n</td>
    <td>Estado</td>
</thead>
<tr>
<?php
		switch($calendar['AccountingCalendar']['status']){
                    case AccountingCalendar::STATUS_NEW:
                        $status = 'Nuevo';
                        break;
                    case AccountingCalendar::STATUS_PRE_ACCOUNTED:
                        $status = 'Pre Contabilizado';
                        break;
                    case AccountingCalendar::STATUS_ACCEPTED:
                        $status = 'Aceptado';
                        break;
                    case AccountingCalendar::STATUS_ACCOUNTED:
                        $status = 'Contabilizado';
                        break;
                    case AccountingCalendar::STATUS_REJECTED:
                        $status = 'Rechazado';
                        break;
        }
		echo '<td>' . $time->format('d-m-Y', $calendar['AccountingCalendar']['since'] )      . '</td>';
        echo '<td>' . $time->format('d-m-Y', $calendar['AccountingCalendar']['until'] )      . '</td>';
        echo '<td>' . $time->format('d-m-Y',  $calendar['AccountingCalendar']['execution'] ) . '</td>';
        echo '<td>' . $status. '</td>';
        
?>
</tr>
</table>
</div>

<div class="center-block">
    <?php $form->create('AccountingItem');
    echo $form->input('accounting_calendar_id', array('type'  => 'hidden',        'value'=> $calendar['AccountingCalendar']['id']));
     ?>
    <table>
        <thead>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Id', 'AccountingItem.id'))); ?></td>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Tipo Usuario', 'AccountingItem.model'))); ?></td>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Id Usuario', 'AccountingItem.model_id'))); ?></td>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Id Oferta', 'AccountingItem.deal_id'))); ?></td>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Cantidad', 'AccountingItem.total_quantity'))); ?></td>
        	<td><?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->sort ('Monto Total', 'AccountingItem.total_amount'))); ?></td>
        		        	
            <td>Acciones</td>
            <td>Errores</td>
        </thead>
    
        <?php
            if(!empty($items)){
            foreach($items as $item)
            {
                echo '<tr>';
                //$form->create('AccountingItem');
                echo '<td>'.$item['AccountingItem']['id'].'</td>';
                $destinatario_name='';
                switch($item['AccountingItem']['model']){
                	case AccountingItem::MODEL_COMPANY:
                		$destinatario_name = 'Comercio';
                    break;
                	case AccountingItem::MODEL_USER:
                		$destinatario_name = 'Usuario Final';
                    break;
                }
           
                echo '<td>'.$destinatario_name.'</td>';
                echo '<td>'.$item['AccountingItem']['model_id'].'</td>';
           
                
                echo '<td>'.$item['AccountingItem']['deal_id'].'</td>';
                echo '<td>'.$item['AccountingItem']['total_quantity'].'</td>';
                echo '<td>'.$item['AccountingItem']['total_amount'].'</td>';
                echo '<td>';
                if($calendar['AccountingCalendar']['status'] == AccountingCalendar::STATUS_PRE_ACCOUNTED){
                	//echo $html->link ("Excluir",   array ('action' => 'exclude', $item['AccountingItem']['id']), array ('class' => 'rejectIcon'),   '¿Seguro querés excluir este item?');
                	echo $html->link ("Excluir",  array ('action' => 'cancel', $item['AccountingItem']['id'],$calendar['AccountingCalendar']['id']), array ('class' => 'deleteIcon'),   '¿Seguro querés cancelar este item?');
                }
                echo '</td>';
                echo '<td>'.$item[0]['error'].'</td>';
                
                //$form->end();
                echo '</tr>';
            }
            }else{
            	if(!isset($param_search)){
            		echo '<tr>'.
            		'<td colspan="5" class="listaVacia">El Calendario no tiene items</td>'.
            		'</tr>';
            	}else{
            		echo '<tr>'.
            		'<td colspan="5" class="listaVacia">No se encontraron datos para ese criterio de búsqueda</td>'.
            		'</tr>';
            	}
            }

        ?>
        
    </table>
    <?php $form->end();?>
    <ul id="pagingLinks">
        <li>
            <?php echo str_replace('/all', '', $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
        </li>
        <li>
            <?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->numbers(array('class'=>'pagLink')))); ?>
        </li>
        <li>
            <?php echo str_replace('/all', '', $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
        </li>
    </ul>
</div>