<div class="nowLayoutBodyContour">
    <a class="closeButton top" href=""></a>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Modalidad</th>
                <th class="dateColumn">Cantidad de cupones</th>
                <th>Total Liquidaci&oacute;n</th>
                <th> </th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($redeemedByMaxDate)) {?>
            <tr id="sDR_<?php echo $maxPaymentDate; ?>_R" class="detail detRow_TR<?php echo $maxPaymentDate; ?>" href="company_deals_by_max_payment_date_and_mode/<?php echo $maxPaymentDate?>/Redimido" title="Presione para ver el detalle">
                <td>Redimido</td>
                <td> <?php echo $redeemedByMaxDate[0]['AccountingRedeemedGrouped']['liquidate_total_quantity'];?></td>
                <td><strong> <?php echo '$' . $redeemedByMaxDate[0]['AccountingRedeemedGrouped']['liquidate_total_amount'];?></strong></td>
                <td><div style="width: 65px;"><a href="#" class="detSub1">Ver detalles</a></div><div class="loading"><div class="spinner"><div class="mask"><div class="maskedCircle"></div></div></div></div></td>
            </tr>
            <?php }?>
            <?php if (isset($notRedeemedByMaxDate)) {?>
            <tr id="sDR_<?php echo $maxPaymentDate; ?>_V" class="detail detRow_TR<?php echo $maxPaymentDate; ?>" href="company_deals_by_max_payment_date_and_mode/<?php echo $maxPaymentDate?>/Vendido" title="Presione para ver el detalle">
                <td>Vendido</td>
                <td> <?php echo $notRedeemedByMaxDate[0]['AccountingRedeemedGrouped']['liquidate_total_quantity'];?></td>
                <td><strong> <?php echo '$' . $notRedeemedByMaxDate[0]['AccountingRedeemedGrouped']['liquidate_total_amount'];?></strong></td>
                <td><div style="width: 65px;"><a href="#" class="detSub1">Ver detalles</a></div><div class="loading"><div class="spinner"><div class="mask"><div class="maskedCircle"></div></div></div></div></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>
