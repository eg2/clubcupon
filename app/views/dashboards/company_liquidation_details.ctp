<?php
    echo $this->element('dashboard/company_dashboard_menu_tabs');
?>
<div class="nowLayoutBodyContour company_liquidation_details">
<div class="row-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="img-rounded">
                    <div class="pull-right">
                        <ul class="nav nav-tabs company_liquidation_details_nav">
                          <li class="active"><a href="#liquidacion"       data-toggle="tab">Datos de la liquidaci&oacute;n</a></li>
                          <li>               <a href="#cupones" data-toggle="tab">Cupones liquidados</a></li>
                        </ul>
                    </div>
                    <div id="myTabContent" class="tab-content" style="clear:both;">
                        <div class="tab-pane fade active in" id="liquidacion">
                            <table class="table table-hover table-striped">
                                <tr>
                                    <td><strong>ID oferta</strong></td>
                                    <td><?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['deal_id']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha de la oferta</strong></td>
                                    <td><?php echo date("d-m-Y", strtotime($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['deal_start_date'])); ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Título de la oferta</strong></td>
                                    <td><?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['deal_name']; ?> </td>
                                </tr>
                                <tr>
                                    <td><strong>Secci&oacute;n / Ciudad</strong></td>
                                    <td><?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['city_name']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Cupones liquidados</strong></td>
                                    <td><?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_total_quantity']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Per&iacute;odo de liquidaci&oacute;n</strong></td>
                                    <td>
                                        <?php
                                            $accountingCalendarFormattedSinceDate = date("d-m-Y", strtotime($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['calendar_since']) );
                                            $accountingCalendarFormattedUntilDate = date("d-m-Y", strtotime($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['calendar_until']) );
                                            echo $accountingCalendarFormattedSinceDate . ' al ' . $accountingCalendarFormattedUntilDate;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha de pago m&aacute;xima</strong></td>
                                    <td><?php echo date("d-m-Y", strtotime($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['max_payment_date'] )); ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Precio del cup&oacute;n</strong></td>
                                    <td>$ <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['deal_discounted_price']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Monto total</strong></td>
                                    <td>$ <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_gross_amount']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Comisi&oacute;n</td>
                                    <td>
                                        Monto: $ -<?php echo number_format($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_total_amount'] - $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_iva_amount'], 2); ?> ( <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_billing_commission_percentage']; ?>% )
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>IVA</strong></td>
                                    <td>
                                        Monto: $ -<?php echo (float)$dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_iva_amount']; ?> ( <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['bill_iva_percentage']; ?>% )
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Percepci&oacute;n IIBB</strong></td>
                                    <td>
                                        Monto: $ <?php echo (float)$dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_iibb_amount']; ?> ( <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_iibb_percentage']; ?>% )
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha de liquidaci&oacute;n</strong></td>
                                    <td>
                                        <?php
                                            if( !empty($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_bac_sent'] )) {
                                            $accountingCalendarFormattedExecutionDate = date("d-m-Y", strtotime($dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_bac_sent']) );
                                            echo $accountingCalendarFormattedExecutionDate;
                                            } else {
                                                echo '----';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <?php if ($showLiquidateGuaranteeFunds) { ?>
                                <tr>
                                    <td><strong>Fondo de garant&iacute;a</strong></td>
                                    <td><strong>$ -<?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_guarantee_funds']; ?></strong></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td><strong>Total Liquidaci&oacute;n</strong></td>
                                    <td><strong>$ <?php echo $dealAndAccountingItemDetails['AccountingCampaignItemExtra']['liquidate_total_amount']; ?></strong></td>
                                </tr>
                        </table>
                            
                        </div>
                        <div class="tab-pane fade" id="cupones">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <?php 
                                            //Liquida por redimidos
                                            if(!empty($accountingItemsDetails[0]['AccountingCampaignItemDetail']['deal_pay_by_redeemed']) && $accountingItemsDetails[0]['AccountingCampaignItemDetail']['deal_pay_by_redeemed']) {
                                        ?>
                                            <th>C&oacute;digo de cup&oacute;n</th>
                                            <th>C&oacute;digo posnet</th>
                                            <th>Fecha de uso</th>
                                            <th>Modo de redenci&oacute;n</th>
                                        <?php } else {  ?>
                                            <th>C&oacute;digo de cup&oacute;n</th>
                                            <th>Fecha de compra</th>
                                            <th>Fecha de uso</th>
                                            <th>Modo de redenci&oacute;n</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($accountingItemsDetails as $accountingItemsDetail){?>
                                        <tr>
                                            <?php 
                                                //Liquida por redimidos
                                                if(!empty($accountingItemsDetail['AccountingCampaignItemDetail']['deal_pay_by_redeemed']) && $accountingItemsDetail['AccountingCampaignItemDetail']['deal_pay_by_redeemed']) {
                                            ?>
                                                <td><?php echo $accountingItemsDetail['AccountingCampaignItemDetail']['deal_user_coupon_code']; ?></td>
                                                <td><?php echo $accountingItemsDetail['AccountingCampaignItemDetail']['redemption_posnet_code']; ?></td>
                                                <td>
                                                    <?php
                                                        if(!empty($accountingItemsDetail['AccountingCampaignItemDetail']['redemption_redeemed'])){
                                                            echo date("d-m-Y", strtotime( $accountingItemsDetail['AccountingCampaignItemDetail']['redemption_redeemed'] ) );
                                                        } else {
                                                            echo '----';
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if(!empty($accountingItemsDetail['AccountingCampaignItemDetail']['redemption_way'])) {
                                                            echo $accountingItemsDetail['AccountingCampaignItemDetail']['redemption_way'];
                                                        } else {
                                                            echo 'A&uacute;n sin redimir';
                                                        }
                                                    ?>
                                                </td>
                                            <?php } else { ?>
                                                <td><?php echo $accountingItemsDetail['AccountingCampaignItemDetail']['deal_user_coupon_code']; ?></td>
                                                <td><?php echo date("d-m-Y", strtotime( $accountingItemsDetail['AccountingCampaignItemDetail']['deal_external_created'] ) ); ?></td>
                                                <td>
                                                    <?php
                                                        if(!empty($accountingItemsDetail['AccountingCampaignItemDetail']['redemption_redeemed'])){
                                                            echo date("d-m-Y", strtotime( $accountingItemsDetail['AccountingCampaignItemDetail']['redemption_redeemed'] ) );
                                                        } else {
                                                            echo '----';
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if(!empty($accountingItemsDetail['AccountingCampaignItemDetail']['redemption_way'])) {
                                                            echo $accountingItemsDetail['AccountingCampaignItemDetail']['redemption_way'];
                                                        } else {
                                                            echo 'A&uacute;n sin redimir';
                                                        }
                                                    ?>
                                                </td>
                                            <?php }  ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            
                            
                        </div>
                        
                    </div>
				
				</div>
            
        </div>
    </div>
</div>
</div>
