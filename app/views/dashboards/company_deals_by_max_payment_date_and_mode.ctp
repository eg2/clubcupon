<div class="nowLayoutBodyContour">
    <a class="closeButton sub" href=""></a>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID Oferta</th>
                <th>Fecha de la Oferta</th>
                <th>Título de la Oferta</th>
                <th>Ciudad / grupo</th>
                <th>Precio del cupón</th>
                <th>Cantidad de cupones</th>
                <th>Monto total</th>
                <th>Fondo de garantía Retenido</th>
                <th>Fondo de garantía Devuelto</th>
                <th>Comisión</th>
                <th>Total Liquidación</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($accountingEvents as $accountingEvent){?>
            <tr id="" href="" class="detail" title="">
                <?php 
                    $url = '/'.$citySlug.'/dashboards/company_liquidation_details/'.$accountingEvent['AccountingDealIdGrouped']['liquidate_id'];
                    $isOnlyFundReturned = $mode=='Vendido' && $accountingEvent['AccountingDealIdGrouped']['liquidate_guarantee_fund_amount_neg'] == 0
                      && $accountingEvent['AccountingDealIdGrouped']['accounting_type'] == 'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL';
                ?>
                <td>
                <?php if ($showOfferLink) { ?>
                    <a href="<?php echo $url ?>">
                <?php } ?>
                <?php echo $accountingEvent['AccountingDealIdGrouped']['deal_id']; ?>
                <?php if ($showOfferLink) { ?>
                    </a>
                <?php } ?>
                </td>
                <td><?php echo date("d-m-Y", strtotime($accountingEvent['AccountingDealIdGrouped']['deal_start_date'])); ?></td>
                <td><?php echo $accountingEvent['AccountingDealIdGrouped']['deal_name']; ?></td>
                <td><?php echo $accountingEvent['AccountingDealIdGrouped']['city_name']; ?></td>
                <td><?php echo '$' . $accountingEvent['AccountingDealIdGrouped']['liquidate_discounted_price']; ?></td>
                <?php if ($mode != 'Fondo' && !$isOnlyFundReturned) {?>
                <td><?php echo $accountingEvent['AccountingDealIdGrouped']['liquidate_total_quantity']; ?></td>
                <?php } else { ?>
                <td><?php echo '---'; ?></td>
                <?php } ?>
                <?php if ($mode != 'Fondo' && !$isOnlyFundReturned) {?>
                  <td><?php echo '$' . $accountingEvent['AccountingDealIdGrouped']['liquidate_total_quantity'] * $accountingEvent['AccountingDealIdGrouped']['liquidate_discounted_price']; ?></td>
                <?php } else { ?>
                <td><?php echo '---'; ?></td>
                <?php } ?>
                <?php if ($mode == 'Vendido') {?>
                <td><?php 
                      $signo = '$';
                      if ($accountingEvent['AccountingDealIdGrouped']['liquidate_guarantee_fund_amount_neg'] > 0) {
                        $signo = $signo . '-';
                      } else {
                        $signo = $signo . ' ';
                      }
                      echo $signo . $accountingEvent['AccountingDealIdGrouped']['liquidate_guarantee_fund_amount_neg']; ?></td>
                <?php } else { ?>
                <td><?php echo '---'; ?></td>
                <?php } ?>
                <?php if ($mode == 'Vendido') {?>
                <td><?php echo '$ ' . $accountingEvent['AccountingDealIdGrouped']['liquidate_guarantee_fund_amount_pos']; ?></td>
                <?php } else { ?>
                <td><?php echo '---'; ?></td>
                <?php } ?>
                <?php if ($mode != 'Fondo' && !$isOnlyFundReturned) {?>
                <td><?php echo '$-' . $accountingEvent['AccountingDealIdGrouped']['bill_total_amount']; ?></td>
                <?php } else { ?>
                <td><?php echo '---'; ?></td>
                <?php } ?>
                <?php if ($mode != 'Fondo' && !$isOnlyFundReturned) {?>
                <td><strong><?php echo '$' . $accountingEvent['AccountingDealIdGrouped']['liquidate_total_amount']; ?></strong></td>
                <?php } else { ?>
                <td><strong><?php echo '$ ' . $accountingEvent['AccountingDealIdGrouped']['liquidate_guarantee_fund_amount_pos']; ?></strong></td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
