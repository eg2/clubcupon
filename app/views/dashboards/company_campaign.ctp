<?php echo $this->element('dashboard/company_dashboard_menu_tabs'); ?>
<div class="nowLayoutBodyContour">
    
    <?php if(!empty($dealsGroupedByCampaignCode)) { ?>
    
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>C&oacute;digo campaña</th>
                    <th><strong>Titulo de la oferta</strong></th>
                    <th>Cantidad de cupones</th>
                    <th>Comisi&oacute;n</th>
                    <th>Total liquidaci&oacute;n</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dealsGroupedByCampaignCode as $dealsGroup){ ?>
                    <tr href="company_deals_by_campaign_code/<?php echo  $dealsGroup['AccountingCampaign']['deal_campaign_code']; ?>" data-target="#myModal" class="detail altrow" id="TR<?php echo $dealsGroup['AccountingCampaign']['deal_campaign_code']; ?>" title="Presione para ver el detalle de la campa&ntilde;a">
                        <td><?php echo $dealsGroup['AccountingCampaign']['deal_campaign_code']; ?></td>
                        <td><strong><?php echo $dealsGroup['AccountingCampaign']['deal_name'];?></strong></td>
                        <td><?php echo $dealsGroup['AccountingCampaign']['liquidate_total_quantity'];?></td>
                        <td><?php echo '$'.$dealsGroup['AccountingCampaign']['bill_total_amount'];?></td>
                        <td><?php echo '$'.$dealsGroup['AccountingCampaign']['liquidate_total_amount'];?></td>
                        <td>
                            <a href="#">Ver detalles</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <ul id="pagingLinks">
            <?php if ($paginator->hasPrev()) { ?>
            <li class="prev">
                <?php echo $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled')); ?>
            </li>
            <?php } ?>
            <li class="numbers">
                <?php echo $paginator->numbers(array('class'=>'pagLink')); ?>
            </li>
            <?php if ($paginator->hasNext()) { ?>
            <li class="next">
                <?php echo $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled')); ?>
            </li>
            <?php } ?>
        </ul>
    
    <?php } else {?>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    No hay liquidaciones para mostrar
                </td>
            </tr>
        </table>
    <?php } ?>
</div>
<?php if (!$paginator->hasPrev()) { ?>
<style>
    .numbers span:first-child {
        border-left:1px solid #ccc;
    }
</style>
<?php } ?>
<script>
    $(document).ready(function() {
        var largoPaginador = $('.prev').outerWidth() + $('.numbers').outerWidth()+  $('.next').outerWidth();
        $('#pagingLinks').width(largoPaginador);
        $('#pagingLinks').css('margin', '15px auto');
        
        
        $('.numbers').html( $('.numbers').html().replace(/\|/gi, '') );
        
        var closed = true;
        $('.detail').on('click', function(e) {
            $('#detRow').remove();

                var url = 'http://'+document.domain+'/dashboards/'+$(this).attr('href');
                $('<tr id="detRow"><td colspan="6" id="detCel"><div></div></td></tr>').insertAfter($(this).closest('tr'));
                $('#detRow').hide();
                $("#detCel div").load(url, function(){
                    
                    var loadedPage = $(this)[0].innerHTML;
                    if (loadedPage.indexOf("GINA NO ENCONTRADA")!=-1) {
                        window.location.href = 'http://'+document.domain+"/users/login";
                    } else {
                        $('.liquidationDetailsLink').click(function() {
                            var url = $(this).attr('href');
                            window.open(url, '_parent');
                        });
                        $('#detRow').show();
                        
                    }
                });

        });

    });
</script>
