<div class="nowLayoutBodyContour">
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th>ID liquidaci&oacute;n</th>
            <th>ID oferta</th>
            <th class="dateColumn">Fecha de la oferta</th>
            <th>T&iacute;tulo de la oferta</th>
            <th>Ciudad / grupo</th>
            <th class="dateColumn">Cantidad de cupones</th>
            <th>Comisi&oacute;n</th>
            <th>Total Liquidaci&oacute;n</th>
            <th class="dateColumn">Fecha de liquidaci&oacute;n</th>
            <th class="dateColumn">Fecha de pago m&aacute;xima</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($dealsByCampaignCode as $dealByCampaignCode){?>
        <tr class="liquidationDetailsLink detail" title="Presione para ver el detalle de la liquidación" href="company_liquidation_details/<?php echo  $dealByCampaignCode['AccountingCampaignItem']['liquidate_id']; ?>">
            
            <td> <a href="#"><?php echo $dealByCampaignCode['AccountingCampaignItem']['liquidate_id'];?></a></td>
            <td> <?php echo $dealByCampaignCode['AccountingCampaignItem']['deal_id'];?></td>
            <td> <?php echo date("d-m-Y", strtotime($dealByCampaignCode['AccountingCampaignItem']['deal_start_date']));?></td>
            <td> <?php echo $dealByCampaignCode['AccountingCampaignItem']['deal_name'];?></td>
            <td> <?php echo $dealByCampaignCode['AccountingCampaignItem']['city_name'];?></td>
            <td> <?php echo $dealByCampaignCode['AccountingCampaignItem']['liquidate_total_quantity'];?></td>
            <td>$<?php echo $dealByCampaignCode['AccountingCampaignItem']['bill_total_amount'];?></td>
            <td>$<?php echo $dealByCampaignCode['AccountingCampaignItem']['liquidate_total_amount'];?></td>
            <td>
                <?php
                     echo is_null($dealByCampaignCode['AccountingCampaignItem']['liquidate_bac_sent']) ? '----' : date("d-m-Y", strtotime($dealByCampaignCode['AccountingCampaignItem']['liquidate_bac_sent'] ));
                ?>
            </td>
            <td>
                <?php
                    echo is_null($dealByCampaignCode['AccountingCampaignItem']['max_payment_date']) ? '----' : date("d-m-Y", strtotime($dealByCampaignCode['AccountingCampaignItem']['max_payment_date'] ));
                ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
</div>