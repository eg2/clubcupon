
<?php echo $this->element('dashboard/company_dashboard_menu_tabs'); ?>
<?php 
      if (!$this->params['named']['deal_id']) { ?>
        <div class="nowLayoutBodyContour">

            <?php if(!empty($dealsGroupedByMaxPaymentDate)) { ?>

                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Fecha de pago M&aacute;xima</th>
                            <th>Total liquidaci&oacute;n</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dealsGroupedByMaxPaymentDate as $dealsGroup){?>
                            <tr href="company_deals_by_max_payment_date/<?php echo $dealsGroup['AccountingMaxPaymentDateGrouped']['max_payment_date']; ?>" data-target="#myModal" class="detail altrow" id="TR<?php echo $dealsGroup['AccountingMaxPaymentDateGrouped']['max_payment_date']; ?>" title="Presione para ver el listado de ofertas agrupadas">
                                <td><?php echo date("d-m-Y", strtotime($dealsGroup['AccountingMaxPaymentDateGrouped']['max_payment_date'])); ?></td>
                                <td><strong><?php echo '$' . $dealsGroup['AccountingMaxPaymentDateGrouped']['liquidate_total_amount']; ?></strong></td>
                                <td>
                                    <div style="width: 65px;">
                                        <a href="#" class="det1">Ver detalles</a>
                                    </div>
                                    <div class="loading"><div class="spinner"><div class="mask"><div class="maskedCircle"></div></div></div></div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else {?>
                <table class="table table-hover table-striped">
                    <tr>
                        <td>
                            No hay liquidaciones para mostrar
                        </td>
                    </tr>
                </table>
            <?php } ?>
        </div>
<?php  } 
      else { // end si NO existe deal_id ?> 
      <?php if (!empty($accountingEvents) ) {?>
          <div class="nowLayoutBodyContour" id="list_liquidations">
              <table class="table table-hover table-striped">
                  <thead>
                      <tr>
                          <th>ID Oferta</th>
                          <th>Fecha de pago Máxima</th>
                          <th>Fecha de la Oferta</th>
                          <th>Título de la Oferta</th>
                          <th>Ciudad / grupo</th>
                          <?php if ($mode != 'Fondo') {?>
                          <th>Cantidad de cupones</th>
                          <?php }?>
                          <th>Comisión</th>
                          <th>Total Liquidación</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php foreach($accountingEvents as $accountingEvent){?>
                      <tr id="" href="" class="" title="">
                         <?php    $url = '/'.$citySlug.'/dashboards/company_liquidation_details/'.$accountingEvent['AccountingDealIdGrouped']['liquidate_id'];  ?>
                          <td>
                          <a href="<?php echo $url ?>">
                            <?php echo $accountingEvent['AccountingDealIdGrouped']['deal_id'];?>
                          </a>
                          </td>
                          <td><?php echo date("d-m-Y", strtotime($accountingEvent['AccountingDealIdGrouped']['max_payment_date'])); ?></td>
                          <td><?php echo date("d-m-Y", strtotime($accountingEvent['AccountingDealIdGrouped']['deal_start_date'])); ?></td>
                          <td><?php echo $accountingEvent['AccountingDealIdGrouped']['deal_name']; ?></td>
                          <td><?php echo $accountingEvent['AccountingDealIdGrouped']['city_name']; ?></td>
                          <?php if ($mode != 'Fondo') {?>
                          <td><?php echo $accountingEvent['AccountingDealIdGrouped']['liquidate_total_quantity']; ?></td>
                          <?php }?>
                          <td><?php echo '$' . $accountingEvent['AccountingDealIdGrouped']['bill_total_amount']; ?></td>
                          <td><strong><?php echo '$' . $accountingEvent['AccountingDealIdGrouped']['liquidate_total_amount']; ?></strong></td>
                      </tr>
                      <?php } ?>
                  </tbody>
              </table>
              <?php if(count($accountingEvent) && $paginator->hasPage(null,2)) { ?>
              <div class="dealspaginador clearfix">
                <div class="pagborder">
                    <?php 
                     $prev_link      = str_replace('/all', '', $paginator->prev('Anterior' , array('class' => 'prev','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'prev')));
                     $numbers_link   = str_replace('/all', '', $paginator->numbers(array('modulus' => 2,'skip' => '<span class="skip">&hellip;.</span>','separator' => " \n",'before' => null,'after' => null,'escape' => false)));
                     $next_link      = str_replace('/all', '', $paginator->next('Próximo', array('class' => 'next','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'next')));
                     if(!empty ($prev_link)) {  echo $prev_link."\n"; }
                     if(!empty ($numbers_link)) { echo $numbers_link."\n";  }
                     if(!empty ($next_link)) { echo $next_link."\n"; }
                    ?>
                    <?php echo $paginator->options(array('url' => array_merge(array('controller' => $this->params['controller'],'action' => $this->params['action'],),$this->params['pass'], $this->params['named']))); ?>
                    <?php echo $paginator->counter(array('format' => '<p><span>%start%-%end%</span> de %count% Ofertas</p>')); ?>
                </div>
              </div>
              
              <?php } ?>
          </div>
       <?php } /*end count($accountingEvent) */ else { ?>
         <div class="nowLayoutBodyContour">
         <table class="table table-hover table-striped">
                    <tr>
                        <td align="center">
                            No hay liquidaciones para mostrar
                        </td>
                    </tr>
         </table>
         </div>
       <?php }?>
<?php } ?>
<script>
    var SELLERSPANEL_OPENVELOCITY = <?php echo Configure::read('sellersPanel.openVelocity'); ?>;
    $(document).ready(function() {
        $('.detail').on('click', function(e) {
            $(this).find(".loading").show();
            var url = 'http://'+document.domain+'/dashboards/'+$(this).attr('href');
            var detID = 'detRow_'+$(this).attr('id');
            if (!$('#'+detID).length) {
                $('<tr class="detRow" id="'+detID+'"><td colspan="11" id="detCel"><div></div></td></tr><tr></tr>').insertAfter($(this).closest('tr'));
                $('#'+detID).hide();
                $('#'+detID+' td div').load(url, function(){

                    var loadedPage = $(this)[0].innerHTML;
                    if (loadedPage.indexOf("GINA NO ENCONTRADA")!=-1) {
                        window.location.href = 'http://'+document.domain+"/users/login";
                    }

                    var sDetID = '.'+$(this).parent().parent().attr('id');
                    $(sDetID).on('click', function(e) {

                    	$(this).find(".loading").show();
                        var subUrl = 'http://'+document.domain+'/dashboards/'+$(this).attr('href');
                        var subDetID = 'subDetRow_'+$(this).attr('id');
                        if (!$('#'+subDetID).length) {
                            $('<tr id="'+subDetID+'"><td colspan="11" id="detCelSub"><div></div></td></tr><tr></tr>').insertAfter($(this).closest('tr'));
                            $('#'+subDetID).hide();

                            $('#'+subDetID+' td div').load(subUrl, function(){
                            	$(this).parent().parent().show();
                                $(this).parent().parent().find("#detCelSub").animate({paddingBottom: "20px"},SELLERSPANEL_OPENVELOCITY);
                                $(this).parent().parent().find(".cuerpoBlank").hide().slideDown(SELLERSPANEL_OPENVELOCITY);
                                $(this).parent().parent().prev().find(".loading").delay(SELLERSPANEL_OPENVELOCITY).hide(0);
                                $(this).find(".closeButton.sub").on('click', function(e) { 
                                    $(this).parent().parent().parent().parent().parent().prev().click();
                                    return false;
                                });
                            });

                        } else {
                        	if($('#'+subDetID).is(':visible')) {
                        		$('#'+subDetID).find("#detCelSub").animate({paddingBottom: "0px"},SELLERSPANEL_OPENVELOCITY);
                        	    $('#'+subDetID).find(".cuerpoBlank").slideUp(SELLERSPANEL_OPENVELOCITY);
                               	$('#'+subDetID).delay(SELLERSPANEL_OPENVELOCITY).hide(0);
                        	} else {
                               	$('#'+subDetID).show(0);
                               	$('#'+subDetID).find("#detCelSub").animate({paddingBottom: "20px"},SELLERSPANEL_OPENVELOCITY);
                        	    $('#'+subDetID).find(".cuerpoBlank").slideDown(SELLERSPANEL_OPENVELOCITY);
                        	}
                        	$(this).find(".loading").hide(0);
                        }

                    });
                    $(this).parent().parent().show();
                    $(this).parent().parent().find("#detCel").animate({paddingBottom: "20px"},SELLERSPANEL_OPENVELOCITY);
                    $(this).parent().parent().find("#detCel > div > .cuerpoBlank").hide().slideDown(SELLERSPANEL_OPENVELOCITY);
                    $(this).parent().parent().prev().find(".loading").delay(SELLERSPANEL_OPENVELOCITY).hide(0);
                    $('.detSub1').unbind('click');
                    $('.detSub1').on('click', function(e) {
                        $(this).parent().parent().parent().click();
                        return false;
                    });
                    $(this).find(".closeButton.top").on('click', function(e) {
                        $(this).parent().parent().parent().parent().parent().prev().click();
                        return false;
                    });

                });
            } else {
            	if($('#'+detID).is(':visible')) {
            		$('#'+detID).find("#detCel").animate({paddingBottom: "0px"},SELLERSPANEL_OPENVELOCITY);
            	    $('#'+detID).find("#detCel > div > .cuerpoBlank").slideUp(SELLERSPANEL_OPENVELOCITY);
                   	$('#'+detID).delay(SELLERSPANEL_OPENVELOCITY).hide(0);
            	} else {
                   	$('#'+detID).show(0);
            		$('#'+detID).find("#detCel").animate({paddingBottom: "20px"},SELLERSPANEL_OPENVELOCITY);
            	    $('#'+detID).find("#detCel > div > .cuerpoBlank").slideDown(SELLERSPANEL_OPENVELOCITY);
            	}
            	$(this).find(".loading").hide(0);
            }
        });
        $('.det1').on('click', function(e) { 
            $(this).parent().parent().parent().click();
            return false; 
        });
    });
</script>
