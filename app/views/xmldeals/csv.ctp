<?php

header('Content-Type: text/csv; charset=utf-8');
$header = array(
    'ID',
    'ID2',
    'Item title',
    'Final URL',
    'Image URL',
    'Item subtitle',
    'Item description',
    'Item category',
    'Price',
    'Sale price',
    'Contextual keywords',
    'Item address',
    'Tracking template',
    'Custom parameter'
);
$str_header = implode(",", $header);
?>
<?php

echo $str_header;
?>

<?php

if (!empty($cities)) {
    foreach ($cities as $city) {
        if (!empty($city['deal_collection'])) {
            foreach ($city['deal_collection'] as $deal) {
                $dealId = $deal['Deal']['id'];
                $dealName = $deal['Deal']['name'];
                $destinationUrl = STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $deal['City']['slug'] . '/deal/' . $deal['Deal']['slug'];
                $imageUrl = Router::url($html->getImageUrl('Deal', $deal['Attachment'], array(
                                    'dimension' => 'medium_big_thumb'
                                )), true);
                $subtitle = $deal['Deal']['subtitle'];
                $description = strip_tags($deal['Deal']['description']);
                $price = $deal['Deal']['original_price'] . ' ARS';
                $salePrice = $deal['Deal']['discounted_price'] . ' ARS';
                $price = '"' . $price . '"';
                $salePrice = '"' . $salePrice . '"';
                $category = str_replace(" | ", "/", $deal ['DealFlatCategory'] ['path']);
                $address = htmlentities($deal['Company']['address1'] . ' ' . $deal['Company']['address2'], ENT_QUOTES, 'utf-8');
                $city = $deal['City']['name'];
                $discount_percentage = $deal['Deal']['discount_percentage'];
                $custom_parameter = '';
                $traking_template = '';
                $id2 = $city . '_' . $category;
                $contextualKeywords = null;
                if ($deal['Deal']['original_price'] != $deal['Deal']['discounted_price']) {
                    $contextualKeywords = '"' . $deal['Deal']['discount_percentage'] . '% de descuento"';
                }
                if (strpos($dealName, ",") !== false) {
                    $dealName = '"' . $dealName . '"';
                }
                if (strpos($subtitle, ",") !== false) {
                    $subtitle = '"' . $subtitle . '"';
                }
                if (strpos($description, ",") !== false) {
                    $description = '"' . $description . '"';
                }
                if (strpos($category, ",") !== false) {
                    $category = '"' . $category . '"';
                }
                if (strpos($address, ",") !== false) {
                    $address = '"' . $address . '"';
                }
                if (strpos($id2, ",") !== false) {
                    $id2 = '"' . $id2 . '"';
                }
                $separator = ',';
                $row = $dealId . $separator;
                $row.= $id2 . $separator;
                $row.= $dealName . $separator;
                $row.= $destinationUrl . $separator;
                $row.= $imageUrl . $separator;
                $row.= $subtitle . $separator;
                $row.= $contextualKeywords . $separator;
                $row.= $category . $separator;
                $row.= $salePrice . $separator;
                $row.= $salePrice . $separator;
                $row.= /* $contextualKeywords . */ $separator;
                $row.= /* $address . */ $separator;
                $row.= $traking_template . $separator;
                $row.= $custom_parameter;
                echo $row . "\n";
            }
        }
    }
}
