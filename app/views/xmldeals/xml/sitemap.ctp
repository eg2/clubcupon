<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'concursofacebook', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'faq', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'terms', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'learn', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'protection', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'policy', 'admin' => false)); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'pages', 'action' => 'who', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'users', 'action' => 'login', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'users', 'action' => 'logout', 'admin' => false),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.2</priority>
  </url>

<?php
        $cities = $_this->City->find('all', array(
                'recursive' => - 1,
                'fields' => array('slug','modified'),
                'conditions' => array(
                    'City.is_approved' => 1,

                ),

            ));
        foreach($cities as $city) {
          ?>
  <url>
      <loc><?php echo Router::url(array('controller' => 'deals', 'action' => 'index', 'city' => $city['City']['slug']),true); ?></loc>
      <lastmod><?php echo date('Y-m-d'); ?></lastmod>
      <changefreq>daily</changefreq>
      <priority>1.0</priority>
  </url>
  <url>
    <loc><?php echo Router::url(array('controller' => 'deals', 'action' => 'recent', 'admin' => false, 'city' => $city['City']['slug']),true); ?></loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
  </url>
        <?php } ?>



  <?php
          $i = 0;
        do {
            $offset = $i;
            $deals = $_this->Deal->find('all', array(
                    'limit' => 20,
                    'recursive' => 1,
                    'contain' => array(
                      'City' => array(
                        'fields' => array(
                            'City.slug',
                    ))),
                    'fields' => array('Deal.slug','Deal.modified','Deal.deal_status_id'),
                    'offset' => $offset
                ));
            if (!empty($deals)) {
                foreach ($deals as $deal):
                  $priority = 0.2;
                  $changefreq = 'never';
                  $isActive = in_array($deal['Deal']['deal_status_id'], array(ConstDealStatus::Open,  ConstDealStatus::Tipped));
                  if($isActive) {
                    $priority = 0.6;
                    $changefreq = 'daily';
                  }
                  ?>
    <url>
        <!-- Offset <?php echo $i; ?> -->
        <loc><?php echo Router::url(array('controller'=>'deals', 'action'=>'view', 'city' => $deal['City']['slug'], $deal['Deal']['slug']),true); ?></loc>
        <lastmod><?php echo $time->toAtom($deal['Deal']['modified']); ?></lastmod>
        <priority><?php echo $priority;?></priority>
        <changefreq><?php echo $changefreq;?></changefreq>
    </url>
                <?php endforeach;?>
<?php
            }
            $i+= 20;
        }
        while (!empty($deals));
?>
        //


</urlset>
