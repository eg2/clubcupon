<?php header ("Content-Type:text/xml"); ?>
<?php echo '<?xml version = "1.0"  encoding = "UTF-8"?>' . "\n"; ?>
<dealandiaApi:city-list xmlns:dealandiaApi = "http://www.dealandia.com/ns/2010/09"  xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"  xsi:schemaLocation = "http://www.dealandia.com/ns/2010/09  http://dealandia.com/schema/2011/04/dealandia-api-schema.xsd">
<?php if (!empty ($cities)) { ?>
<?php   foreach ($cities as $city) { ?>
<?php     if (count ($city ['deal_collection'])) { ?>
  <city-deals>
    <coupons-city-name><?php echo $city ['City']['slug'] ?></coupons-city-name>
<?php       foreach ($city ['deal_collection'] as $deal) { ?>
    <deal>
      <url><?php echo  Router::url (array ('controller' => 'deals', 'action' => 'view', $deal ['Deal']['slug'], 'admin' => false), true); ?></url>
      <imageUrl><?php echo Router::url ($html->getImageUrl ('Deal', $deal ['Attachment'], array ('dimension' => 'medium_big_thumb')), true); ?></imageUrl>
      <title><?php echo htmlentities ($deal ['Deal']['name'], ENT_QUOTES, 'utf-8'); ?></title>
      <description><?php echo htmlentities ($deal ['Deal']['description'], ENT_QUOTES, 'utf-8'); ?></description>
      <price><?php echo $deal ['Deal']['discounted_price']; ?></price>
<?php         if ($deal['Deal']['hide_price'] == 0) { ?>
      <marketPrice><?php echo $deal ['Deal']['original_price']; ?></marketPrice>
      <discount><?php echo $deal ['Deal']['discount_amount']; ?></discount>
<?php         } ?>
      <beginTime><?php echo date ('c',strtotime ($deal ['Deal']['start_date'])); ?></beginTime>
      <endTime><?php echo date ('c',strtotime ($deal ['Deal']['end_date'])); ?></endTime>
      <purchased><?php echo $deal ['Deal']['deal_external_count']; ?></purchased>
<?php         if (!empty ($deal ['Deal']['max_limit'])) { ?>
      <quantity><?php echo $deal ['Deal']['max_limit']; ?></quantity>
<?php         } ?>
      <coupon-id><?php echo $deal ['Deal']['slug']; ?></coupon-id>
      <dealandiaApi:business-locations>
        <dealandiaApi:business-location address = "<?php echo htmlentities ($deal ['Company']['address1'] . ' '  . $deal ['Company']['address2'], ENT_QUOTES, 'utf-8'); ?>"  name = "<?php echo htmlentities ($deal ['Company']['name'], ENT_QUOTES, 'utf-8'); ?>">
          <dealandiaApi:geo latitude = "<?php echo $deal ['Company']['latitude']; ?>"  longitude = "<?php echo $deal ['Company']['longitude']; ?>" />
        </dealandiaApi:business-location>
      </dealandiaApi:business-locations>
    </deal>
<?php       } ?>
  </city-deals>
<?php     } ?>
<?php   } ?>
<?php } ?>
</dealandiaApi:city-list>