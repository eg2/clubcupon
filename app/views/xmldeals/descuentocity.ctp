<?php header("Content-Type:text/xml"); ?>
<data>
    <deals>
        <?php
        $search = array("&", "<", ">", '"', "'");
        $replace = array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;");
        if (!empty($cities)) {
            foreach ($cities as $city) {
                if (!empty($city ['deal_collection'])) {
                    foreach ($city ['deal_collection'] as $deal) {
                        ?>
                        <deal>
                            <title><?php echo str_replace($search, $replace, $deal['dealTitle']); ?></title>
                            <brand><?php echo str_replace($search, $replace, $deal['brand']); ?></brand>
                            <address1><?php echo $deal['brand_address']; ?></address1>
                            <lat><?php echo $deal['companyLatitude']; ?></lat>
                            <lng><?php echo $deal['companyLongitude']; ?></lng>
                            <address2><?php echo $deal['companyAddress']; ?></address2>
                            <comercio><?php echo str_replace($search, $replace, $deal['companyName']); ?></comercio>
                            <categoria><?php echo $deal['dealCategory']; ?></categoria>
                            <city><?php echo $deal['dealCity']; ?></city>
                            <image_url><?php echo Router::url($html->getImageUrl('Deal', $deal ['dealAttachment'], array('dimension' => 'medium_big_thumb')), true); ?></image_url>
                            <price><?php echo $deal['dealDiscountedPrice']; ?></price>
                            <orig_price><?php echo $deal['dealOriginalPrice']; ?></orig_price >
                            <link><?php echo $deal['dealLink']; ?></link>
                            <description><![CDATA[<?php echo $deal['dealDescription']; ?>]]></description>
                            <startDate><?php echo $deal['dealStartDate']; ?></startDate>
                            <endDate><?php echo $deal['dealEndDate']; ?></endDate>
                        </deal>
                        <?php
                    }
                }
            }
        }
        ?>
    </deals>
</data>