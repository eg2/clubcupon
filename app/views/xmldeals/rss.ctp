<?php $channelTitle = 'Ofertas del día'; if (count ($cities) == 1) { $channelTitle .= ' en ' . $cities[0]['City']['name']; } ?>
<?php header ("Content-Type:text/xml"); ?>
<rss version = "2.0"  xmlns:content = "http://purl.org/rss/1.0/modules/content/"  xmlns:wfw = "http://wellformedweb.org/CommentAPI/"  xmlns:dc = "http://purl.org/dc/elements/1.1/"  xmlns:atom = "http://www.w3.org/2005/Atom"  xmlns:sy = "http://purl.org/rss/1.0/modules/syndication/"  xmlns:slash = "http://purl.org/rss/1.0/modules/slash/">
  <channel>
    <title><?php echo Configure::read ('site.name'); ?></title>
    <link><?php echo Router::url ('/', true); ?></link>
    <description><?php echo $channelTitle; ?></description>
    <language>es-ar</language>
    <pubDate><?php echo date ("Y-m-d H:i:s", gmmktime ()); ?></pubDate>
    <docs>http://blogs.law.harvard.edu/tech/rss</docs>
    <generator>ClubCupon</generator>
    <managingEditor>contacto@clubcupon.com.ar</managingEditor>
    <webMaster>contacto@clubcupon.com.ar</webMaster>
<?php if (!empty ($cities)) { ?>
<?php   foreach ($cities as $city) { ?>
<?php     if (!empty ($city ['deal_collection'])) { ?>
<?php       foreach ($city ['deal_collection'] as $deal) { ?>
<?php         $link = STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $deal['City']['slug'] . '/deal/' . $deal['Deal']['slug']; ?>        
<?php         $img = Router::url ($html->getImageUrl ('Deal', $deal ['Attachment'], array ('dimension' => 'medium_big_thumb')), true); ?>
<?php         $title = $deal ['Deal']['name']; ?>
    <item>
      <title><?php echo $title; ?></title>
      <link><?php echo $link; ?></link>
      <pubDate><?php echo $time->nice ($time->gmt ($deal ['Deal']['start_date'])) . ' GMT'; ?></pubDate>
      <guid><?php echo $link; ?></guid>
      <description><![CDATA[
<p>
  <a href = "<?php echo $link; ?>"  title = "<?php echo $title; ?>">
    <img src = "<?php echo $img; ?>"  alt = "<?php echo $title; ?>" />
  </a>
</p>
<?php         echo $deal ['Deal']['description']; ?><br />
Pag&aacute; <?php echo Configure::read ('site.currency'); ?><?php echo $html->cCurrency ($deal ['Deal']['discounted_price']); ?>
<?php         if ($deal ['Deal']['only_price'] == 0) { ?>
en vez de <?php echo Configure::read ('site.currency'); ?><?php echo $html->cCurrency ($deal ['Deal']['original_price']); ?>
<?php         } ?>.
<div>
<?php         echo $deal ['Deal']['coupon_condition']; ?>
</div>
<div>
<?php         echo $deal ['Deal']['coupon_highlights']; ?>
</div>
<div>
  <h3><?php echo $deal ['Company']['name']; ?></h3>
  <p>
<?php         echo htmlentities ($deal ['Company']['address1'] . ' '  . $deal ['Company']['address2'], ENT_QUOTES, 'utf-8'); ?>
  </p>
</div>
]]></description>
      <content:encoded><![CDATA[
<p>
  <a href = "<?php echo $link; ?>"  title = "<?php echo $title; ?>">
    <img src = "<?php echo $img; ?>"  alt = "<?php echo $title; ?>" />
  </a>
</p>
<?php         echo $deal ['Deal']['description']; ?>
]]></content:encoded>
    </item>
<?php       } ?>
<?php     } ?>
<?php   } ?>
<?php } ?>
  </channel>
</rss>