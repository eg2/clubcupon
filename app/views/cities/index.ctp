<div id="main" class="container_16 main_frt">
    <div class="grid_16">
            <div class="bloque-home clearfix">
                
                <!-- Contenidos -->
                <ol start="<?php echo $paginator->counter(array(
                    'format' => '%start%'
                    ));?>">
                <?php
                    if (!empty($cities)){
                            $i = 0;
                            foreach ($cities as $city){
                                    $class = null;
                                    if ($i++ % 2 == 0):
                                            $class = ' class="altrow"';
                                    endif;
                                    if($city['City']['slug'] == $city_slug) :
                                            $select_class = 'active';
                                    else:
                                            $select_class = '';
                                    endif;
                            ?>
                                    <li <?php echo $select_class;?> >
                                            <?php
                                                    if (Cache::read('site.city_url', 'long') == 'prefix'){
                                                            echo $html->link($city['City']['name'], array('controller' => 'deals', 'action' => 'index', 'city' => $city['City']['slug']), array('class' => "$select_class", 'title' => $city['City']['name'], 'escape' => false));
                                                        }elseif (Cache::read('site.city_url', 'long') == 'subdomain'){
                                                            $subdomain = substr(env('HTTP_HOST'), 0, strpos(env('HTTP_HOST'), '.'));			
                                                            $sitedomain = substr(env('HTTP_HOST'), strpos(env('HTTP_HOST'), '.'));
                                                            $url = env('HTTP_HOST');
                                                            switch($subdomain){
                                                                    case 'www':	
                                                                            $url = "http://".$city['City']['slug']. $sitedomain;
                                                                            break;
                                                                    case 'm':
                                                                            $url = "http://m.".$city['City']['slug']. $sitedomain;
                                                                            break;
                                                                    case Configure::read('site.domain');
                                                                                    $url = "http://".$city['City']['slug'].'.'. env('HTTP_HOST');
                                                                            break;
                                                                    default:
                                                                            $url = "http://".$city['City']['slug']. $sitedomain;
                                                            }						
                                                    ?>
                                                    <a href="<?php echo $url;?>" title="<?php echo $city['City']['name']; ?>" class="<?php echo $select_class; ?>"><?php echo $city['City']['name']; ?></a>
                                            <?php }?>
                                            <?php if($city['City']['active_deal_count']){?>
                                                            <span class="callout"><?php echo $city['City']['active_deal_count']; ?></span>
                                            <?php  }?>
                                    </li>
                    <?php
                        }
                    }?>
                </ol>
                <p class="suggestion-link"><?php echo $html->link('&iquest;No encontr&aacute;s tu ciudad?', array('controller' => 'city_suggestions', 'action' => 'add'), array('title' => 'Sugerir una ciudad', 'escape' => false)); ?></p>                
                <!-- / Contenidos -->
                
                </div>
            </div>
        </div>
</div>


