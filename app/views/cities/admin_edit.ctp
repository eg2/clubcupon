<script>
$(document).ready(function() {
     $('#CityEmailFrom').livequery('blur', function() {
        var lowercase = $('#CityEmailFrom').val().toLowerCase();
        $('#CityEmailFrom').val(lowercase);
     });
});
</script>
<div class="cities form">
	<h2>Editar ciudad</h2>
	<?php
		echo $form->create('City', array('class' => 'normal','action'=>'edit', 'enctype' => 'multipart/form-data'));
		echo $form->input('id');
		if(!empty($id_default_city))
		{
			echo $form->input('name',array('label' => 'Nombre','readonly' => true, 'info' => 'No podes cambiar el nombre por defecto de la ciudad'));
		} else {
			echo $form->input('name',array('label' => 'Nombre'));
		}
		echo $form->input ('country_id',     array('label' => 'Pais', 'empty'=>'Seleccionar'));
		echo $form->input ('state_id',       array('label' => 'Provincia', 'empty'=>'Seleccionar'));
        echo "<div style=\"padding:15px 0 0 82px; width:260px; clear:both; \">";
		echo 'Slug: '.$this->data['City']['slug'];
        echo "</div>";
		echo $form->input ('language_id',    array('label' => 'Idioma', 'empty'=>'Seleccionar'));
        echo $form->input ('image',          array('label' => 'Imagen/Logo', 'type' => 'file'));
        echo '<div style="clear:both; padding-left:115px;">';
        echo 'Las imágenes deben tener hasta 72 px de alto, y ser .jpg, .jpeg, o .png';
        echo '</div>';
        if(!empty($this->data ['City']['image'])){
            echo "<div style=\"padding:15px 115px; width:260px; clear:both; \">";
            echo $html->image ($this->data ['City']['image'], array ('alt' => ''));
            echo $form->hidden ('stored_image');
            echo "</div>";
        }
        echo $form->input ('order',            array('label' => 'Orden'));
		echo $form->input ('latitude',         array('label' => 'Latitud'));
		echo $form->input ('longitude',        array('label' => 'Longitud'));
        echo $form->input ('email_from',       array('label' => 'Mail Newsletter', 'size' => '38'));
		echo $form->input ('fb_api_key',       array('label' => 'API key de Facebook'));
		echo $form->input ('is_approved',      array('label' => 'Aprobado?'));
        echo $form->input ('is_selectable',    array('label' => 'Seleccionable en el perfil de usuario'));
        echo $form->input ('is_group',         array('label' => 'Es un Grupo de Afinidad', 'type' => 'checkbox'));
        echo $form->input ('has_newsletter',   array('label' => 'Manda Newsletters', 'type' => 'checkbox'));
        echo $form->input ('week_days',          array('label' => 'Días de la semana que manda Newsletters'));
        echo $form->input ('is_corporate',     array('label' => 'Es corporate', 'type' => 'checkbox'));
        echo $form->input ('is_business_unit', array('label' => 'Es Nuestros Beneficios', 'type' => 'checkbox'));
        echo $form->input ('is_hidden',        array('label' => 'Está oculta', 'type' => 'checkbox'));
        echo $form->input ('is_searchable',    array('label' => 'Es buscable', 'type' => 'checkbox'));
        echo $form->input ('is_user_subscription_allowed', array('label' => 'La suscripcion de usuarios esta habilitada', 'type' => 'checkbox'));
        echo $form->input ('newsletter_sender_engine_code', array('label' => 'Usa segmentación','options' => array(
    		    '1'=>'No',
    		    '2'=>'Si',
    		 )));
	?>
		<div class="submit-block">
		<?php echo $form->submit('Actualizar datos');	?>
		</div>
		<?php echo $form->end(); ?>
</div>