<script>
$(document).ready(function() {
     $('#CityEmailFrom').livequery('blur', function() {
        var lowercase = $('#CityEmailFrom').val().toLowerCase();
        $('#CityEmailFrom').val(lowercase);
     });
});
</script>
<div class="cities form">
  <h2>Agregar ciudad</h2>
  <?php
      echo $form->create('City',           array('class' => 'normal', 'action' => 'add', 'enctype' => 'multipart/form-data'));
      echo $form->input ('country_id',     array('label' => 'País', 'empty' => 'Seleccionar'));
      echo $form->input ('state_id',       array('label' => 'Provincia', 'empty' => 'Seleccionar'));
      echo $form->input ('language_id',    array('label' => 'Idioma', 'empty' => 'Seleccionar'));
      echo $form->input ('image',          array('label' => 'Imagen/Logo', 'type' => 'file'));
      echo '<div style="clear:both; padding-left:115px;">';
      echo 'Las imágenes deben tener exactamente 90px de ancho, 72 px de alto, y ser .jpg, .jpeg, o .png';
      echo '</div>';
      echo $form->input ('order',              array('label' => 'Orden'));
      echo $form->input ('name',               array('label' => 'Nombre'));
      echo $form->input ('latitude',           array('label' => 'Latitud'));
      echo $form->input ('longitude',          array('label' => 'Longitud'));
      echo $form->input ('email_from',         array('label' => 'Mail remitente del Newsletter', 'size' => '38'));
      echo $form->input ('is_approved',        array('label' => 'Ciudad para cargar ofertas (Aprobada)'));
      echo $form->input ('is_selectable',      array('label' => 'Seleccionable en el perfil de usuario'));
      echo $form->input ('is_group',           array('label' => 'Es un Grupo de Afinidad', 'type' => 'checkbox'));
      echo $form->input ('has_newsletter',     array('label' => 'Manda Newsletters', 'type' => 'checkbox'));
      echo $form->input ('week_days',          array('label' => 'Días de la semana que manda Newsletters'));
      echo $form->input ('is_corporate',       array('label' => 'Es corporate', 'type' => 'checkbox'));
      echo $form->input ('is_business_unit',   array('label' => 'Es Nuestros Beneficios', 'type' => 'checkbox'));
      echo $form->input ('is_hidden',          array('label' => 'Está oculta', 'type' => 'checkbox'));
      echo $form->input ('is_searchable',      array('label' => 'Es buscable', 'type' => 'checkbox'));
      echo $form->input ('is_user_subscription_allowed', array('label' => 'La suscripcion de usuarios esta habilitada', 'type' => 'checkbox'));
      echo $form->input ('newsletter_sender_engine_code', array('label' => 'Usa segmentación','options' => array(
    		    '1'=>'No',
    		    '2'=>'Si',
    		 )));
  ?>
  <div class="submit-block">
    <?php echo $form->submit('Crear ciudad'); ?>
  </div>
  <?php echo $form->end(); ?>
</div>