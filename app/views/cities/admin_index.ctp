<?php if(empty($this->params['isAjax'])): ?>
    <div  class="js-tabs">
    	<ul class="clearfix">
            <li><?php echo $html->link('Registros aprobados',  array('controller' => 'cities', 'action' => 'index', 'filter_id' => ConstMoreAction::Active),  array('title' => 'Registros aprobados')); ?></li>
            <li><?php echo $html->link('Registros rechazados', array('controller' => 'cities', 'action' => 'index', 'filter_id' => ConstMoreAction::Inactive),array('title' => 'Registros rechazados')) ?></li>
            <li><?php echo $html->link('Registros totales',    array('controller' => 'cities', 'action' => 'index'),array('title' => 'Registros totales')) ?></li>
        </ul>
    </div>
<?php else: ?>
	<?php if(empty($this->data) && empty($this->params['named']['page'])): ?>
        <div class="cities index js-responses">
			<h2><?php echo $pageTitle; ?></h2>
            <?php echo $form->create('City', array('type' => 'post', 'class' => 'normal search-form clearfix js-ajax-form {"container" : "js-search-responses"}', 'action'=>'index')); ?>
            <div class="filter-section">
                <div>
                    <?php echo $form->input('q', array('label' => __l('Keyword'))); ?>
                </div>
                <div class="submit-block">
                    <?php echo $form->submit('Buscar');?>
                </div>
            </div>
            <?php echo $form->end(); ?>
            <div class="add-block1">
                <?php echo $html->link('Cargar ciudad',array('controller'=>'cities','action'=>'add'),array('class' => 'add', 'title' => 'Cargar ciudad'));?>
            </div>
    <?php endif; ?>
     <div class="js-search-responses js-response ">
		<?php
        echo $form->create('City', array('action' => 'update','class'=>'normal')); ?>
        <?php echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url'])); ?>
        <?php if(!empty($this->params['named']['filter_id'])){?>
        <?php echo $form->input('redirect_url', array('type' => 'hidden', 'value' => $this->params['named']['filter_id'])); ?>
        <?php } ?>
        <?php echo $this->element('paging_counter');?>
        <table class="list">
            <tr>
                <th>&nbsp;</th>
                <th><?php echo $paginator->sort('Nombre', 'City.name');?></th>
                <th>Orden</th>
                <th><?php echo $paginator->sort('Pais', 'Country.name',    array('url'=>array('controller'=>'cities', 'action'=>'index')));?></th>
                <th><?php echo $paginator->sort('Provincia', 'State.name', array('url'=>array('controller'=>'cities', 'action'=>'index')));?></th>
                <th><?php echo $paginator->sort('Idioma', 'Language.name');?></th>
                <th><?php echo $paginator->sort('Seleccionable', 'City.is_selectable');?></th>
                <th><?php echo $paginator->sort('Grupo de Afinidad', 'City.is_group');?></th>
                <th><?php echo $paginator->sort('Manda Newsletters', 'City.has_newsletter');?></th>
                <th><?php echo $paginator->sort('Días', 'City.week_days');?></th>
                <th><?php echo $paginator->sort('Es Corporate', 'City.is_corporate');?></th>
                 <th><?php echo $paginator->sort('Está oculta', 'City.is_hidden');?></th>
                  <th><?php echo $paginator->sort('Es buscable', 'City.is_searchable');?></th>
            </tr>
            <?php
            		
            if (!empty($cities)):
                $i = 0;
                foreach ($cities as $city):
                    $class = null;
                    if ($i++ % 2 == 0) :
                        $class = ' class="altrow"';
                    endif;
                    if($city['City']['is_approved'])  :
                        $status_class = 'js-checkbox-active';
                    else:
                        $status_class = 'js-checkbox-inactive';
                    endif;
                    
                ?>
                    <tr<?php echo $class;?>>
                        <td>
                            <div class="actions-block">
                                <div class="actions round-5-left">
                                  <?php
                                        echo $html->link(__l('Edit'), array('action'=>'edit', $city['City']['id']), array('class' => 'edit js-edit', 'title' => __l('Edit')));
                                        if(Configure::read('site.city') != $city['City']['slug'])
                                        {
                                            echo $html->link(__l('Delete'), array('action'=>'delete', $city['City']['id']), array('class' => 'delete js-delete', 'title' => __l('Delete')));
                                        }
                                        echo $html->link(__l('Update Twitter Credentials'), array('action' => 'update_twitter', $city['City']['id']), array('class' => 'twitter-link', 'target' => '_blank', 'title' => __l('Update Twitter Credentials')));
                                    ?>
                                </div>
                            </div>
                            <?php
                                echo $form->input('City.'.$city['City']['id'].'.id',array('type' => 'checkbox', 'id' => "admin_checkbox_".$city['City']['id'],'label' => false , 'class' => $status_class.' js-checkbox-list'));
                            ?>
                        </td>

                        <?php
                        if(false): // Se comenta desactivar ciudades por un click. Mejor que se utilice actualizar.
                        // if(Configure::read('site.city') != $city['City']['slug']):
                                if($city['City']['is_approved']):
                                        echo $html->link(__l('Approved'),array('controller'=>'cities','action'=>'update_status',$city['City']['id'],'disapprove'),array('class' =>'approve','title' => __l('Click here to Disapprove')));
                                else:
                                        echo $html->link(__l('Disapproved'),array('controller'=>'cities','action'=>'update_status',$city['City']['id'],'approve') ,array('class' =>'pending','title' => __l('Click here to Approve')));
                                  endif;
                          endif;
                        ?>

  			<td><?php echo $html->cText($city['City']['name'],    false);?></td>
                        <td><?php echo $html->cText($city['City']['order'],   false);?></td>
                        <td><?php echo $html->cText($city['Country']['name'], false);?></td>
                        <td><?php echo $html->cText($city['State']['name'],   false);?></td>
                        <td><?php echo !empty($city['Language']['name']) ? $html->cText($city['Language']['name'], false) : 'N/A';?></td>
                        <td><?php echo $city['City']['is_selectable'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                        <td><?php echo $city['City']['is_group'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                        <td><?php echo $city['City']['has_newsletter'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                        <td><?php echo $html->cText($city['City']['week_days'], false);?></td>
                        <td><?php echo $city['City']['is_corporate'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                        <td><?php echo $city['City']['is_hidden'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                        <td><?php echo $city['City']['is_searchable'] ? $html->cText('S&iacute;') : $html->cText('No');?></td>
                     </tr>
                <?php
                endforeach;
                else:
                ?>
                <tr>
                    <td class="notice" colspan="10"><?php echo __l('No cities available');?></td>
                </tr>
                <?php
                endif;
                ?>
        </table>
		<?php
            if (!empty($cities)) :
                ?>
                 <div class="admin-select-block">
                <div>
                    Seleccionar
                    <?php
                        echo $html->link('Todos', '#',   array('class' => 'js-admin-select-all', 'title' => 'Todos'));
                        echo $html->link('Ninguno', '#', array('class' => 'js-admin-select-none','title' => 'Ninguno'));
                        if(!isset($this->params['named']['filter_id']))
                        {
                            echo $html->link('No aprobados', '#', array('class' => 'js-admin-select-pending','title' => 'No aprobados')); 
                            echo $html->link('Aprobados', '#',    array('class' => 'js-admin-select-approved','title' => 'Aprobados')); 
                        }
                    ?>
                </div>
                   <div>
                    <?php echo $form->input('more_action_id', array('class' => 'js-admin-index-autosubmit', 'label' => false, 'empty' => '-- Mas acciones --')); ?>
                </div>
                </div>
                <div class="js-pagination">
                    <?php  echo $this->element('paging_links'); ?>
                </div>

                <div class="hide">
                    <?php echo $form->submit('Submit');  ?>
                </div>
                <?php
            endif;
        ?>
    <?php
    echo $form->end();
    ?>
    </div>
<?php if(!empty($this->params['named']['main_filter_id']) && empty($this->params['named']['filter_id']) && empty($this->data)): ?>
	</div>
<?php endif; ?>

<?php endif; ?>