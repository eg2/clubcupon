<?php

?>
  
<script>
    
    $(document).ready(function() {
        
        clearview();
    

    });
</script>
<style>
    
    .top{
        height:70px;
        background:#f0f0f0;
    }
        .top .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
        }
    
    .left-block, .right-block{height:320px}
    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
        .left-block .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }
        .left-block p{
            font-style: italic;
            font-size:12px;
        }
    
    .right-block{
        padding:3px 15px;
        float:right;
        width:910px;
        background:#fff;
        position: relative;
    }
    
    .calendarTop{width:212px!important;}
    h3{width:100%!important;}
    
    input{float:none;}
    table{margin-top:15px;}
    .formatDateInput{background:none; border:none; color:#f79621; font-weight:bold; width:325px; padding-left:0;}
    .timeTitle{
        font-weight: bold;
        font-size: 14px;
        font-style: italic;
        margin-top:15px;
        display:block;
        }
        
    #wrapper-since, #wrapper-until, #wrapper-execution, .submitButton{
        position:absolute;
    }
    .ui-datepicker-inline{
        min-height: 255px;
    }
    
    #wrapper-since{
        left:0;
        width:211px;
    }
    #wrapper-until{
        left:238px;
        width:211px;
    }
    #wrapper-execution{
        left:476px;
        width:448px;
    }
        #wrapper-execution .calendarTop{ width:436px!important;}
        
    .submitButton{
       /* top:312px;*/
        left:200px;
    }
    .submitButton input{
        background: orange;
        font-weight:bold;
        border:none;
        color:#fff;
        height:35px;
        width:170px;
    }
    
    .ui-datepicker-current {visibility: hidden!important;}
    
    .error-message{
        float:left;
        width:100%;
        margin-bottom:15px;
    }
    
</style>

<div class="top">
    <span class="title1">Creaci&oacute;n de dispositivo</span>
</div>
<?php echo $form->create  ('Beacondevice',array ('action'    => 'add',         'id'    => 'beaconDeviceForm')); ?>
<div class="left-block">
    <span class="title2">Nuevo Dispositivo</span>
    <p>
        
         <span class="timeTitle">Nombre:</span>
        <?php echo $form->input   ('name',     array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>

        <span class="timeTitle">mac:</span>
        <?php echo $form->input   ('mac',     array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>

        <span class="timeTitle">uuid:</span>
        <?php echo $form->input   ('uuid', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        
        <span class="timeTitle">major:</span>
        <?php echo $form->input   ('major', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        
      <span class="timeTitle">minor:</span>
        <?php echo $form->input   ('minor', array ('type'   => 'text', 'class' => 'formatInput', 'label' => false, 'div' => false)); ?>
        
     </p>

    <?php
          echo $form->submit  ('Crear dispositivo',             array ('div'       => 'submitButton'));
    ?>
</div>
<?php echo $form->end(); ?>