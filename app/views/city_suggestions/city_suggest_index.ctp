<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="grid_12">
            <!--
                <h1 class="h1-format"><a href="">Titulo H1</a></h1>
                <h2><a href="">Titulo H2</a></h2>
            -->
            <div class="bloque-home clearfix">
                
                <!-- Contenidos -->
                <?php if(empty($this->params['named']['name'])){?>
                    <h2>Sugerencias recientes</h2>
                <?php }else{?>
                    <h2>Sugirieron</h2>
                <?php }?>

                <?php echo $this->element('paging_counter');?>
                <table class="list">
                    <tr>
                        <?php if(empty($this->params['named']['name'])){?>
                             <th class = "dl">Nombre de la ciudad</th>
                        <?php }?>
                        <th class = "dl">Mail del usuario</th>
                    </tr>
                <?php
                    if (!empty($citySuggestions)){

                    $i = 0;
                        foreach ($citySuggestions as $citySuggestion){
                            $class = null;
                            if ($i++ % 2 == 0) {
                                $class = ' class="altrow"';
                            }
                ?>
                            <tr<?php echo $class;?>>
                                <?php if(empty($this->params['named']['name'])){?>
                                    <td><?php echo $html->cText($citySuggestion['CitySuggestion']['name']);?></td>
                                <?php }?>
                                <td><?php echo $html->cText($citySuggestion['CitySuggestion']['email']);?></td>
                            </tr>
                <?php
                        }
                    }else{
                ?>
                        <tr>
                            <td colspan="6" class="notice">No hay sugerencias de ciudad disponibles</td>
                        </tr>
                <?php
                    }
                ?>
                </table>

                <?php
                    if (!empty($citySuggestions)) {
                        echo $this->element('paging_links');
                    }
                ?>
                
                <!-- / Contenidos -->

            </div>
        </div>
    </div>
</div>




    
