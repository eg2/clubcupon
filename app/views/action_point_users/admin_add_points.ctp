<div class="externalservices form">
	<h2><?php echo __l('Agregar puntos');?></h2>
  <?php echo $form->create('ActionPointUser', array('class' => 'normal','action'=>'admin_add_points'));?>
  <?php echo $form->input('user_id',array('value' => $user['User']['id'], 'type' => 'hidden')); ?>
	<fieldset>
    <p>Agregar puntos al usuario: "<strong><?php echo htmlentities($user['User']['username'],ENT_QUOTES,"UTF-8"); ?></strong>".</p>
    <br />
    <div>
      <p>Puntos actuales del usuario: <strong><?php echo $user['User']['available_points']; ?></strong></p>
    </div>
    <div>
      <?php echo $form->input('amount',array('label' => __l('Cantidad a cargar'), 'value' => '0')); ?>
    </div>
	</fieldset>


	<div class="submit-block clearfix">
    <div class="floatLeft">
        <?php echo $form->submit(__l('Aceptar'));?>
    </div>
		<div class=" floatLeft">
			<?php echo $html->link(__l('Volver a usuarios'), array('controller' => 'users', 'action' => 'index'), array('class' => 'cancel-button', 'title' => __l('Cancel'), 'escape' => false));?>
		</div>
	</div>
	<?php echo $form->end(); ?>

</div>