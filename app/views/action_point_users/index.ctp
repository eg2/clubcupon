<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
            <!--
            <h1 class="h1-format"><a href="">Titulo H1</a></h1>
            <h2><a href="">Titulo H2</a></h2>
            -->
            
            <div class="grid_15 omega alpha ancho">
                <table class = "list">
                    <tr>
                        <th><?php echo 'Concepto'; ?></th>
                        <th><?php echo 'Fecha'; ?></th>
                        <th><?php echo 'Puntos'; ?></th>
                    </tr>
                    <?php
                        if (!empty ($actionPointUsers)){
                            $i = 0;
                            foreach ($actionPointUsers as $actionPointUser){
                                $class = null;
                                if ($i++ % 2 == 0){
                                    $class = ' class = "altrow"';
                                }
                    ?>
                                <tr<?php echo $class; ?>>
                                    <td><?php echo $html->cText ($actionPointUser ['ActionPoint']['Action']['name']); ?></td>
                                    <td><?php echo $html->cText ($actionPointUser ['ActionPointUser']['modified']); ?></td>
                                    <td><?php echo $html->cText ($actionPointUser ['ActionPoint']['points']); ?></td>
                                </tr>
                    <?php
                            }
                        }else{
                    ?>
                        <tr>
                        <td colspan = "7"  class = "notice"><?php echo 'No hay puntos disponibles'; ?></td>
                        </tr>
                    <?php
                        }
                    ?>
                </table>
                
            </div>

        </div>

    </div>

</div>
