<style>
    .top{
        height:70px;
        background:#f0f0f0;
    }
    .title1{
        font-weight: bold;
        font-size: 23px;
        padding: 35px 0 0 15px;
        display: block;
        color:#444;
    }

    .title2{
        font-weight:bold;
        font-size:18px;
        padding-bottom: 4px;
        display: block;
        color:#f79621;
    }


    .form {
        font-weight: bold;
        padding:5px 15px;
    }
    .listado{width:200px;}
    .error-message{width:350px; padding-top:20px;}
    form{width:800px;}
    .input{width:350px;}

    .botones{
        float: left;
        border: 1px solid #9d9d9d;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -khtml-border-radius: 5px;
        border-radius: 5px;
        background: #eee;
        font-size: 11px;
        font-weight: 700;
        cursor: pointer;
        margin: 10px;
        padding: 3px 15px;
        color:#333!important;
    }

</style>

<div id ="contents">

    <div class="top">
        <span class="title1">Productos</span>
    </div>

    <div class="form">
        <span class="title2">Editar producto</span>
        <div id="original_has_pins" style="display:none"><?php echo $this->data['ProductProduct']['has_pins'] ?></div>
        <?php
        echo $form->create('products', array('class' => 'normal', 'action' => 'edit', 'url' => array('productId' => $this->data['ProductProduct']['id'], 'campaignId' => $this->data['ProductProduct']['product_campaign_id'])));
        echo $form->hidden('ProductProduct.product_campaign_id');
        echo $form->hidden('ProductProduct.id');
        echo $form->input('ProductProduct.name', array('label' => 'Nombre', 'escape' => false));
        if ($validationErrors['ProductProduct']['name']) {
            echo '<div class="error-message">' . $validationErrors['ProductProduct']['name'] . '</div>';
        }

        echo '<div style="clear:both;"></div>';
        echo $form->input('ProductProduct.stock', array('label' => 'Stock', 'escape' => false));
        if ($this->validationErrors['ProductProduct']['stock']) {
            echo '<div class="error-message">' . $this->validationErrors['ProductProduct']['stock'] . '</div>';
        }

        echo '<div style="clear:both;"></div>';
        echo $form->input('ProductProduct.decremented_units', array('size' => 7, 'maxlength' => 7,'label' => 'Unidades a descontar', 'escape' => false));
        if ($this->validationErrors['ProductProduct']['decremented_units']) {
            echo '<div class="error-message">' . $this->validationErrors['ProductProduct']['decremented_units'] . '</div>';
        }

        echo '<div style="clear:both;"></div>';
        echo $form->input('ProductProduct.has_pins', array('label' => 'Usa PINes? ', 'type' => 'checkbox'));

        echo '<div style="clear:both;"></div>';
        echo $form->input('ProductProduct.product_inventory_id', array('label' => 'Producto Externo:', 'type' => 'select', 'empty' => 'Ninguno', 'options' => $externalProducts));

        echo '<div style="clear:both;"></div>';
        echo $form->input('ProductProduct.libertya_code', array('label' => 'Código de artículo en Libertya:', 'type' => 'text'));
        echo '<div class="error" style="color: red; display: none; padding-top: 60px;">* Ingrese s&oacute;lo numeros (0 - 9)</div>';

        echo '<div style="clear:both;"></div>';
        echo '<hr />';
        echo $form->submit('Editar producto', array('div' => false, 'escape' => false, 'class' => 'botones', 'id' => 'tosave'));
        echo $html->link("Cancelar", array('action' => 'index', $this->data['ProductProduct']['product_campaign_id']), array('class' => 'botones', 'escape' => false));
        echo $form->end();
        ?>
    </div>
    <hr />

</div>
<script>
    $(document).ready(function () {
        clearview();
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $("#ProductProductLibertyaCode").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $("#ProductProductLibertyaCode").bind("paste", function (e) {
            return false;
        });
        $("#ProductProductLibertyaCode").bind("drop", function (e) {
            return false;
        });
    });
</script>