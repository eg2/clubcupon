<style>
    .top{
        height:70px;
        background:#f0f0f0;
    }
        .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
        }
        
        .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }

        .deleteIcon, .editIcon, .detailIcon, .addIcon, .createIcon, .addPines{
            color:#fff;
            font-weight:bold;
            padding:5px;
            font-size: 12px;
            margin:3px;
            width:80px;
            display:block;
            float:right;
            text-align: center;
            }

        .deleteIcon{ background-color: #E31948; }
        .editIcon{ background-color: #97C41A; }
        .addIcon{ background-color: #F20CAD; width:100px; }
        .createIcon{ background-color: #97C41A; width:100px; float:right }
        .addPines{ background-color: #0088CC;width:120px; }
        
        
        .buscador {
            font-weight: bold;
            padding:5px 15px;
        }
        
        .buscador .label{ padding: 12px 0 0 0; }
        .buscador .submit{ margin:10px 0 0 0;}
        
        table{
            width:100%;
            float:left;
        }
            th{
                padding:        5px!important;
                min-height:     25px;
                background:     #f9c667;
                background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
                background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
                background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
                filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
                color:#fff;
                font-size:16px;
                font-weight:bold;
                text-align: center;
            }
                th a{color:#fff;}
            td{padding-left:5px; text-align: center;}
                
            .wide tr{
                border-bottom: 1px solid #ccc;
            }
        .no-results{ text-align:center; font-weight: bold; padding: 10px;}
        .listado{width:200px;}
        
        #pagingLinks{}
    
        #pagingLinks li{
            float:left;
            padding: 5px;
        }
    
        .pagLinkArrow{
            font-weight: bold;
        }
        
</style>
<?php
//App::import('Model', 'Product.ProductPin'); 
?>
<div id ="contents">

    <div class="top">
        <span class="title1">Campa&ntilde;a: <?php echo $campaign['ProductCampaign']['name']; ?>, de la empresa: <?php echo $companyname; ?></span>
    </div>
    
    <?php echo $this->element(Configure::read('theme.theme_name') . 'campaigns_products_search' ); ?>
    
    <div class="wide">
        <?php echo $html->link ("Crear producto", array ('action' => 'add', $campaign['ProductCampaign']['id']), array ('class' => 'createIcon', 'escape'=>false)); ?>
    </div>
    <div class="wide">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Stock</th>
                    <th>PINes</th>
                    <th>Producto inventario?</th>
                    <th>Unidades a descontar</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($products)) { ?>
                    <?php foreach ($products as $product) { ?>
                    <tr>
                        <td><?php echo  $product['ProductProduct']['id']; ?></td>
                        <td><?php echo  $product['ProductProduct']['name']; ?></td>
                        <td><?php echo  $product['ProductProduct']['stock']; ?></td>
                        <td>
                        <?php
                        if ($product['ProductProduct']['has_pins']) {
							echo  ProductPin::getTotalPinsByProductId($product['ProductProduct']['id']) ;
						} 
                         
                        ?>
                        </td>
                        <td>
                        <?php
                        if (empty($product['ProductProduct']['product_inventory_id'])) {
							echo  'NO' ;
						}else{
							echo  'SI' ;
						} 
                         
                        ?>
                        </td>
                        <td><?php echo  $product['ProductProduct']['decremented_units']; ?></td>
                        <td>
                            <?php
                                echo $html->link ("Editar", array ('action' => 'edit',   'productId'=>$product['ProductProduct']['id'], 'campaignId'=>$product['ProductProduct']['product_campaign_id'] ), array ('class' => 'editIcon'));
                                echo $html->link ("Borrar", array ('action' => 'delete', 'productId'=>$product['ProductProduct']['id'], 'campaignId'=>$product['ProductProduct']['product_campaign_id'] ), array ('class' => 'deleteIcon'), '¿Seguro querés borrar esta producto?');
                                if ($product['ProductProduct']['has_pins']) {
                                	echo $html->link ('Agregar PINes', array ('action' => 'admin_add_pins', $product['ProductProduct']['id']), array ('class' => 'addPines', 'title' => 'Agregar PINes'));
                                }
                                
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan ="5" class="no-results">No hay productos asignados a esta campa&ntilde;a para mostrar</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        
        <ul id="pagingLinks">
            <li>
                <?php echo str_replace('/all', '', $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
            </li>
            <li>
                <?php echo str_replace('/all', '', $paginator->numbers(array('class'=>'pagLink')) ); ?>
            </li>
            <li>
                <?php echo str_replace('/all', '', $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
            </li>
            
        </ul>
        
    </div>

</div>
<script>
    $(document).ready(function() {
        clearview();
    });
</script>