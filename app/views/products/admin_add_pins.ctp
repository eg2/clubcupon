
<style>
<!--
.control-group{
    float: left !important;
    width: 100% !important;
    padding-top: 20px;
}
textarea{
        height:300px;
        width:400px;
        resize:none;
}
-->
</style>

<div>
<div>
  <div class="alert alert-info">
    <h2><?php echo 'Agregar PINes'; ?></h2>
    <div >  
                <strong>Producto:&nbsp;</strong>&nbsp;<?php echo  $product_name?><br>
                <strong>Pines v&aacute;lidos:&nbsp;</strong>&nbsp;<?php echo  (int) $num_pins_ok;?><br>
                <strong>Pines usados:</strong>&nbsp;<?php echo  (int) $num_pins_ko?><br>
    </div> 
  </div>
  <div style = "margin-left:10px;">
    <?php echo $form->create ('product', array ('class' => 'normal', 'action' => 'admin_add_pins', 'admin' => 'true', 'enctype' => 'multipart/form-data')); ?>
  	<?php echo $form->hidden('product_campaign_id');?>
  	<?php
  	    echo '<div class="alert alert-info">Para poder agregar varios Pines presione [Enter] después de ingresar uno.</div>';
		echo $form->input ('id', array ('type' => 'hidden', 'value' => $product_id));
		echo $form->input ('pins', array ('type' => 'textarea', 'label' => ''));
	?>
	<div class="control-group">
	  <div class="controls" style="float:left;width:115px">       
                &nbsp;
      </div>
      <div class="controls">
          <input type="button" class="btn btn-large" value="Cancelar" onclick="window.location='<?php echo Router::url(array('controller'=>'products','action' => 'index', $this->data['product']['product_campaign_id']));?>'">            	
	  </div>
      <div class="controls" style="float:left;width:190px">       
                &nbsp;
      </div>
      <?php
      echo '<div class="controls">';
      echo $form->end(array('label' => __l('Save'), 'class' => 'btn btn-primary btn-large', 'div' => false));
      echo '</div>';
      ?>
  	</div>
   </div>
</div>
</div>
<script type="text/javascript">
 $(document).ready(function() {
 
});
</script>