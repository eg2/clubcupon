<style>
    .top{
        height:70px;
        background:#f0f0f0;
    }
        .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
        }
        
        .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }

        
        .form {
            font-weight: bold;
            padding:5px 15px;
        }
        .listado{width:200px;}
        
        .error-message{width:350px; padding-top:20px;}
        form{width:800px;}
        .input{width:350px;}
        
        .botones{
            float: left;
            border: 1px solid #9d9d9d;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
            background: #eee;
            font-size: 11px;
            font-weight: 700;
            cursor: pointer;
            margin: 10px;
            padding: 3px 15px;
            color:#333!important;
        }
        label {width:120px;}
            label span{color:red;float: right;}
        
</style>

<div id ="contents">

    <div class="top">
        <span class="title1">Campa&ntilde;as</span>
    </div>

    <div class="form">
        <span class="title2">Crear campa&ntilde;a</span>
        <?php         
            echo $form->create('campaign', array('class' => 'normal', 'action' => 'add'));
            
            echo $form->input ('company_id', array('label' => 'Empresa <span>*</span>', 'options' => $companiesList, 'escape' => false, 'empty' => 'Seleccione una empresa', 'class' => 'listado'));
            if($this->validationErrors['ProductCampaign']['company_id']){
                echo '<div class="error-message">'.$this->validationErrors['ProductCampaign']['company_id'].'</div>';
            }
            
            echo '<div style="clear:both;"></div>';
            echo $form->input ('name', array('label' => 'Nombre Campa&ntilde;a: <span>*</span>', 'escape' => false));
            if($this->validationErrors['ProductCampaign']['name']){
                echo '<div class="error-message">'.$this->validationErrors['ProductCampaign']['name'].'</div>';
            }
            echo '<hr />';
            echo $form->submit('Crear campa&ntilde;a', array('div' => false,'escape' => false, 'class' => 'botones'));
            echo $html->link ("Cancelar", array ('action' => 'index'), array ('class' => 'botones', 'escape'=>false));
            echo $form->end();
        ?>
    </div>
    <hr />
    
</div>
<script>
    $(document).ready(function() {
        clearview();
    });
</script>