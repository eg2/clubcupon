<style>
    .top{
        height:70px;
        background:#f0f0f0;
    }
        .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
        }
        
        .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }

        .deleteIcon, .editIcon, .detailIcon, .addIcon, .createIcon{
            color:#fff;
            font-weight:bold;
            padding:5px;
            font-size: 12px;
            margin:3px;
            width:80px;
            display:block;
            float:left;
            text-align: center;
            }

        .deleteIcon{ background-color: #E31948; }
        .editIcon{ background-color: #97C41A; }
        .detailIcon{ background-color: #F28F0C;  }
        .addIcon{ background-color: #F20CAD; width:120px; }
        .createIcon{ background-color: #97C41A; width:100px; float:right }
        hr{margin-bottom:0;}
        
        
        table{
            width:100%;
            float:left;
        }
            th{
                padding:        5px!important;
                min-height:     25px;
                background:     #f9c667;
                background:     -moz-linear-gradient(top, #f9c667 0%, #f79621 100%);
                background:     -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621));
                background:     -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     -o-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     -ms-linear-gradient(top, #f9c667 0%,#f79621 100%);
                background:     linear-gradient(to bottom, #f9c667 0%,#f79621 100%);
                filter:         progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 );
                color:#fff;
                font-size:16px;
                font-weight:bold;
            }
                th a{color:#fff;}
            td{text-align:center;}
            .wide tr{
                border-bottom: 1px solid #ccc;
            }
        .no-results{ text-align:center; font-weight: bold; padding: 10px;}
        .listado{width:200px;}
        #pagingLinks{}
    
        #pagingLinks li{
            float:left;
            padding: 5px;
        }
    
        .pagLinkArrow{
            font-weight: bold;
        }
        
        
</style>

<div id ="contents">

    <div class="top">
        <span class="title1">Campa&ntilde;as</span>
    </div>

    <?php echo $this->element(Configure::read('theme.theme_name') . 'campaigns_products_search' ); ?>
    
    <div class="wide">
        <?php echo $html->link ("A&ntilde;adir campa&ntilde;a", array ('action' => 'add'), array ('class' => 'createIcon', 'escape'=>false)); ?>
    </div>
    <div class="wide">
        <table>
            <thead>
                <tr>
                    <th width="050" >ID</th>
                    <th width="275" >Nombre</th>
                    <th width="200" >Productos</th>
                    <th width="274" >Empresa</th>
                    <th width="500" >Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($campaigns)) { ?>
                    <?php foreach ($campaigns as $campaign) { ?>
                    <tr>
                        <td><?php echo  $campaign['ProductCampaign']['id']; ?></td>
                        <td><?php echo  $campaign['ProductCampaign']['name']; ?></td>
                        <td><?php echo count($campaign['ProductProduct']); ?></td>
                        <td><?php echo  $campaign['Company']['name']; ?></td>
                        <td>
                            <?php
                                echo $html->link ("Editar",           array ('action' => 'edit',   $campaign['ProductCampaign']['id']), array ('class' => 'editIcon'));
                                echo $html->link ("Borrar",           array ('action' => 'delete', $campaign['ProductCampaign']['id']), array ('class' => 'deleteIcon'), '¿Seguro querés borrar esta campaña?');
                                echo $html->link ("Detalle",          array ('controller' => 'products', 'action' => 'index',  $campaign['ProductCampaign']['id']), array ('class' => 'detailIcon'));
                                echo $html->link ("Agregar producto", array ('controller' => 'products', 'action' => 'add',    $campaign['ProductCampaign']['id']), array ('class' => 'addIcon'));
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan ="5" class="no-results">No hay campa&ntilde;as para mostrar</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        
        <ul id="pagingLinks">
            <li>
                <?php echo $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled')); ?>
            </li>
            <li>
                <?php echo $paginator->numbers(array('class'=>'pagLink')); ?>
            </li>
            <li>
                <?php echo $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled')); ?>
            </li>
        </ul>
        
    </div>

</div>
<script>
    $(document).ready(function() {
        clearview();
    });
</script>