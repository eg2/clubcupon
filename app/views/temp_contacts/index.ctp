<div id="temp-contacts-<?php echo $member; ?>" class="js-response js-responses ">
	<?php 
		if(!empty($this->params['isAjax'])):
			echo $this->element("flash_message");
		endif;
	?>
	<div class="tempContacts index">
		<?php if($member == 1) { ?>
			<div class="info-msg"><h3><?php echo __l('Contacts found in your friends list'); ?></h3></div>
		<?php } else if($member == 2) { ?>
			<div class="info-msg"><h3><?php echo sprintf(__l('Contacts found in %s'), Configure::read('site.name')); ?></h3></div>
		<?php } else if($member == 3) { ?>
			<div class="info-msg"><h3><?php echo sprintf(__l('Invite your contacts to %s'), Configure::read('site.name')); ?></h3></div>
		<?php } ?>
		<?php
			if (!empty($tempContacts) && $member != 2):
				echo $form->create('UserFriend', array('id' => 'temp-contacts-form-' . $member, 'action' => 'importfriends', 'class' => 'normal js-ajax-form'));// js-ajax-form
				echo $form->input('contacts_source', array('id' => 'contact_source_field' . $member, 'type' => 'hidden', 'value' => $contacts_source));
				echo $form->input('member', array('id' => 'mem_' . $member, 'type' => 'hidden', 'value' => $member));
			endif;
			if(!empty($deal_slug)):
				echo $form->input('deal_slug',array('type' => 'hidden', 'label' => false, 'value' => $deal_slug));
			endif;
		?>
		<table class="list">
			<?php
				if (!empty($tempContacts)):
					$i = 0;
			?>
			<tr>
				<th><?php echo __l('Contact Name') ;?></th>
				<th><?php echo __l('Contact Mail') ;?></th>
				<?php if($member == 3) { ?>
					<th colspan="4" class="invite-contacts-details"><?php echo $form->input('invite_all', array('type' => 'select', 'class' => 'js-invite-all', 'label' => false, 'options' => $invite_friend_options)); ?></th>
				<?php } ?>
			</tr>
			<?php
				foreach ($tempContacts as $tempContact):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
			?>
			<tr<?php echo $class;?>>
			    <td><?php if($member == 2){?><div class="actions-block">
						<div class="actions round-5-left">
							 <span><?php echo $html->link(__l('Add as Friend'), array('controller' => 'user_friends', 'action' => 'add', 'email'=>urlencode($tempContact['TempContact']['contact_email'])), array('class' => 'add add-friend js-add-friend', 'title' => __l('Add as Friend')));?></span>				 
						</div>
					</div>
					<?}?>
					<?php echo $html->truncate($tempContact['TempContact']['contact_name'], 25); ?>
					</td>				
				<td><?php echo $html->cText($tempContact['TempContact']['contact_email']); ?></td>
				<?php if($tempContact['TempContact']['is_member'] == 3) { ?>
					<td><?php echo $form->input($tempContact['TempContact']['id'], array('type' => 'select', 'class' => 'invite-select', 'label' => '', 'options' => $invite_friend_options,'name'=>'data[UserFriend][FriendList]['.$tempContact['TempContact']['id'].']')); ?></td>
				<?php } ?>
			</tr>
			<?php
				endforeach;
				else:
			?>
			<tr>
				<?php if($member == 1) { ?>
					<td colspan="8" class="notice"><?php echo sprintf(__l('No %s friends available in your mail'), Configure::read('site.name')); ?></td>
				<?php } else if($member == 2) { ?>
					<td colspan="8" class="notice"><?php echo sprintf(__l('No %s contacts available in your mail'), Configure::read('site.name'));?></td>
				<?php } else if($member == 3) { ?>
					<td colspan="8" class="notice"><?php echo __l('No contacts available in your mail');?></td>
				<?php } ?>
			</tr>
			<?php
				endif;
			?>
		</table>
		<?php
			if (!empty($tempContacts) && $member != 2): ?>
            	    <div class="submit-block clearfix">
                        <span class="submit fix-custom-display-milio">
                          <?php echo $form->submit(__l('Submit'),array('class' => 'fix-custom-display-milio', 'div' => false));?>
                        </span>
                        <span class="cancel-block">
                            <?php echo $html->link(__l('Cancel'), array('controller' => 'users', 'action' => 'my_stuff#Import_friends','admin' => false), array('class' => 'cancel-button')); ?>
                        </span>
                   </div>
		<?php
				echo $form->end(null);
			endif;
		?>
		  <div class="clearfix js-pagination">
		<?php
			if (!empty($tempContacts)) {
				echo $this->element('paging_links');
			}
		?>
		</div>
	</div>
</div>