
<?php 
//var_dump($ShippingAddress);
?>
<style>
.control-group {
    float: left !important;
    width: 100% !important;
   
}
.help-block {
   /*border:1px solid;*/
   display: inline;
   padding-left:20px
   
}
span.required {
   color: #FF0000;
   font-weight: bold;
}
input[type="text"] {
   height:25px;
}
</style>
<div>
    <div>
        <div class="alert alert-info">
            <h2><?php echo $this->pageTitle; ?> </h2>
            <div >  
			 <strong>Empresa:&nbsp;</strong>&nbsp;<?php echo  $currentCompany['Company']['name']?>
		
			</div> 
        </div>
        <div style = "margin-left:10px;">
            <?php

            $inputDefaults = array(
            		'class' => 'form-control',
            		'div' => array('class' => 'control-group'),
            		'label' => array('class' => 'control-label'),
            		'error' => array('wrap' => 'span', 'class' => 'help-block')
            );

            ?>
            
            <?php echo $form->create('ShippingAddress',  array('action'=>$action));?>
     			<p class="help-block" style="padding-left: 420px;">
					<span class="required">*</span>
					Campos obligatorios
				</p>
      			<?php  
      			echo $form->hidden('company_id',array('value' => $currentCompany['Company']['id']	));
      			
      			if($action=='update'){
					echo $form->hidden('id');
				}
      			
      			?>
        
                <div class="control-group">
            	<div class="controls">
      			<?php  echo $form->input('name', $inputDefaults);?>
      			
        		</div>
            </div>         
            <div class="control-group">
            	<div class="controls">
              <?php  echo $form->input('bac_storehouse_id', array_merge($inputDefaults, array('label' => 'Id de Almacén')));?>      			
        		</div>
            </div> 
            <div class="control-group">
            	<div class="controls">
              <?php  echo $form->input('shipping_address_type_id', array_merge($inputDefaults, array('label' => 'Tipo', 'options'=>$shippingAddressTypes, 'type' => 'select')));?>      			
        		</div>
            </div> 
            <div class="control-group">
            	<div class="controls">
              <?php  echo $form->input('libertya_delivery_mode_id', array_merge($inputDefaults, array('label' => 'Id de M.Envío', 'readonly'=>true, 'value'=>Configure::read('libertya.delivery_mode_id'))));?>      			
        		</div>
            </div> 
            <div class="control-group">
             	<div class="controls">
                		<?php  echo $form->input('country_id',$inputDefaults);?>
                		
             	</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('state_id',array_merge($inputDefaults,array('empty'=>__l('Please Select'))));?>
                <div style = "clear: both; text-align: left; display: none;"  id = "geo-loading"><?php echo $html->image ('loading.gif'); ?></div>
             	</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('city_id',array_merge($inputDefaults,array('empty'=>__l('Please Select'))));?>
               	</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('neighbourhood_id',array_merge($inputDefaults,array('empty'=>__l('Please Select'))));?>
               	</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('street',array_merge($inputDefaults,array('label' => 'Calle')));?>
     			</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('number',array_merge($inputDefaults,array('label' => 'Altura')));?>
             	</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                  <?php  echo $form->input('floor_number',array_merge($inputDefaults,array('label' => 'Piso')));?>
         		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('apartment',array_merge($inputDefaults,array('label' => 'Dpto.')));?>
         		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('postal_code',array_merge($inputDefaults,array('label' => 'Código Postal')));?>
          		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('custom_address',array_merge($inputDefaults,array('label' => 'Descripción')));?>
          		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('link_bukeala',array_merge($inputDefaults,array('label' => 'Link Bukeala')));?>
          		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
                <?php  echo $form->input('telephone',$inputDefaults);?>
            	</div>
            </div>
            <div class="control-group">
             	<div class="controls">     
             	      <?php  echo $form->input('fax_number',array_merge($inputDefaults));?>
                 </div>
            </div>
            <div class="control-group">
             	<div class="controls">
                <?php  echo $form->input('email',$inputDefaults);?>
                </div>
            </div>
            <div class="control-group">
             	<div class="controls">
                <?php  echo $form->input('details',array_merge($inputDefaults,array('label' => 'Dias y Horarios')));?>
         		</div>
            </div>
            <div class="control-group">
             	<div class="controls">       
					<input type="button" id="btn_add_shipping_address" class="btn btn-large" value="Cancelar" onclick="window.location='<?php echo Router::url(array('controller'=>'shipping_addresses','action'=>'admin_index', $currentCompany['Company']['id']));?>'">            	</div>
            	<div class="controls" style="float:left;width:200px">       
                    &nbsp;
               	</div>
           	  	<div class="controls">
           	  		<?php  echo $form->end(array('label' => __l('Save'), 'class' => 'btn btn-primary btn-large', 'div' => false));?>
               	</div>
            </div>
           
			</div>
     </div>
            
</div>
            


<script type='text/javascript'>


	$(document).ready(function() {

		/* 
		se modifica los labels dinamicamente (para el caso que no estén definidos) 
		asi se logra que cuando genere el estilo de error estas etiquetas también 
		aparezcan en rojo
		*/
		$("label[for='ShippingAddressNeighbourhoodId']").text('Barrio');
		$("label[for='ShippingAddressFaxNumber']").text('Fax');
		
	
		$('.form-control').removeClass('form-control');
    	$('.form-error').removeClass('form-error').addClass('error');
	});
        
            
    $('#ShippingAddressCountryId').change(function() {
        $('#geo-loading').css('display', 'inline');
        var parent_id=$(':selected',this).val();
        //alert('ShippingAddressCountryId:'+parent_id);

        var url=cfg.cfg.path_relative+'shipping_addresses/geoAjax/state/'+parent_id;
        console.log(url);
        $('#ShippingAddressStateId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#ShippingAddressStateId').change();
                              });
    });
    $('#ShippingAddressStateId').change(function() {
    	$('#geo-loading').css('display', 'inline');;
        var parent_id=$(':selected',this).val();
        //alert('ShippingAddressStateId:'+parent_id);
        
        var url=cfg.cfg.path_relative+'shipping_addresses/geoAjax/city/'+parent_id;
        console.log(url);
        $('#ShippingAddressCityId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#ShippingAddressCityId').change();
                                    
       });
    });
    $('#ShippingAddressCityId').change(function() {
        $('#geo-loading').css('display', 'inline');;
        var parent_id=$(':selected',this).val();
        //alert('ShippingAddressCityId:'+parent_id);
        
        var url=cfg.cfg.path_relative+'shipping_addresses/geoAjax/neighbourhood/'+parent_id;
        console.log(url);
        $('#ShippingAddressNeighbourhoodId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#ShippingAddressNeighbourhoodId').change();
       });
    });
    
</script>