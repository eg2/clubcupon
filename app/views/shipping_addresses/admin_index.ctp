
<?php

// var_dump($shippingAddresses);die;
?>
<style>
<!--
#grid_shipping_addresses {
	margin-left: 0;
}

#btn_add_shipping_address {
	float: right;
	margin-right: 50px;
}

#pagingLinks {
	
}

#pagingLinks li {
	float: left;
	padding: 5px;
}

.pagLinkArrow {
	font-weight: bold;
}
-->
</style>
<div class="grid_16 adds-suc">
	<div class="alert alert-info">
		<h2><?php echo __l('Puntos de Retiro'); ?> </h2>
		<div>
			<strong>Empresa:&nbsp;</strong>&nbsp;<?php echo  $currentCompany['Company']['name']?>
		</div>
	</div>
	<div class="contenido-form" id="tool">
		<div class="encabezado">
			<div class="selall clearfix">
				<input type="submit" id="btn_add_shipping_address"
					class="btn btn-primary btn-medium" value="Agregar Punto de Retiro"
					onclick="window.location='<?php echo Router::url(array('action'=>'add', $currentCompany['Company']['id']));?>'">
			</div>
		</div>

		<table class="table table-striped span7" id="grid_shipping_addresses">
			<thead>
				<th>Punto de Retiro</th>
				<!-- th>Compa&ntilde;ia</th-->
				<th>Direcci&oacute;n</th>
				<th>Localidad</th>
				<th>Barrio</th>
				<th>Telefono</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
			<?php
			
if (count ( $shippingAddresses )) {
				foreach ( $shippingAddresses as $key => $shippingAddress ) {
					?>
				<tr>
					<td><?php echo $html->cText($shippingAddress['ShippingAddress']['name']); ?></td>
					<!--  td-->
					<?php //echo $html->cText($shippingAddress['Company']['name']); ?>
					<!-- /td-->
					<td>
					
						<?php echo ShippingAddress::getLabelAddressToShow($shippingAddress); ?>
					
					</td>
					<td><?php echo $html->cText($shippingAddress['City']['name']);?></td>
					<td><?php echo $html->cText($shippingAddress['Neighbourhood']['name']); ?></td>
					<td><?php echo $html->cText($shippingAddress['ShippingAddress']['telephone']); ?></td>
					<td>
					<?php echo $html->link('Modificar',array('action'=>'update',$shippingAddress['ShippingAddress']['id'],$currentCompany['Company']['id']),array('class'=>'btn btn-success btn-sm'));?>
					<?php echo $html->link('Eliminar',array('action'=>'delete',$shippingAddress['ShippingAddress']['id'],$currentCompany['Company']['id']),array('class'=>'btn btn-danger btn-sm'),"¿Estás seguro que deseas eliminar este Punto de Retiro?");?>
					</td>
				</tr>
				<?php
				
}
			} else {
				?>
				<tr>
					<td colspan="6">No hay Puntos de Retiro registrados.</td>
				</tr>
			<?php }?>
			</tbody>
		</table>
		<ul id="pagingLinks">
			  <li>
            <?php echo str_replace('/all', '', $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
        </li>
        <li>
            <?php echo str_replace('/search', '/search/'.$param_search,str_replace('/all', '', $paginator->numbers(array('class'=>'pagLink')))); ?>
        </li>
        <li>
            <?php echo str_replace('/all', '', $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), null, array('class'=>'disabled'))); ?>
        </li>
		</ul>

	</div>
</div>

