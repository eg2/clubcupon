<?php
    echo $html->css(Configure::read('theme.asset_version') . '/components/datepicker');
    echo $html->css(Configure::read('theme.asset_version') . 'jquery-ui');
    echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.8.3');
    echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery-ui');
?>
  
<script>
    
    $(document).ready(function() {
        
        clearview();
    
        $("#since-date").datepicker({
            numberOfMonths:     1,
            showButtonPanel:    true,
            showWeek:           true,
            altField:           "#AccountingCalendarSince",
            altFieldTimeOnly:   false,
            stepMinute:         15,
            dateFormat:         'yy-mm-dd'
        });
        
        
        
        $("#until-date").datepicker({
            numberOfMonths:     1,
            showButtonPanel:    true,
            showWeek:           true,
            altField:           "#AccountingCalendarUntil",
            altFieldTimeOnly:   false,
            stepMinute:         15,
            dateFormat:         'yy-mm-dd'
        });
        $("#execution-date").datepicker({
            numberOfMonths:     2,
            showButtonPanel:    true,
            showWeek:           true,
            altField:           "#AccountingCalendarExecution",
            altFieldTimeOnly:   false,
            stepMinute:         15,
            dateFormat:         'yy-mm-dd'
        });
    });
</script>
<style>
    
    .top{
        height:70px;
        background:#f0f0f0;
    }
        .top .title1{
            font-weight: bold;
            font-size: 23px;
            padding: 35px 0 0 15px;
            display: block;
            color:#444;
        }
    
    .left-block, .right-block{height:320px}
    .left-block{
        padding:15px;
        float:left;
        width:325px;
        background:#fafafa;
    }
        .left-block .title2{
            font-weight:bold;
            font-size:18px;
            padding-bottom: 4px;
            display: block;
            color:#f79621;
        }
        .left-block p{
            font-style: italic;
            font-size:12px;
        }
    
    .right-block{
        padding:3px 15px;
        float:right;
        width:910px;
        background:#fff;
        position: relative;
    }
    
    .calendarTop{width:212px!important;}
    h3{width:100%!important;}
    
    input{float:none;}
    table{margin-top:15px;}
    .formatDateInput{background:none; border:none; color:#f79621; font-weight:bold; width:325px; padding-left:0;}
    .timeTitle{
        font-weight: bold;
        font-size: 14px;
        font-style: italic;
        margin-top:15px;
        display:block;
        }
        
    #wrapper-since, #wrapper-until, #wrapper-execution, .submitButton{
        position:absolute;
    }
    .ui-datepicker-inline{
        min-height: 255px;
    }
    
    #wrapper-since{
        left:0;
        width:211px;
    }
    #wrapper-until{
        left:238px;
        width:211px;
    }
    #wrapper-execution{
        left:476px;
        width:448px;
    }
        #wrapper-execution .calendarTop{ width:436px!important;}
        
    .submitButton{
        top:312px;
        left:0;
    }
    .submitButton input{
        background: orange;
        font-weight:bold;
        border:none;
        color:#fff;
        height:35px;
        width:170px;
    }
    
    .ui-datepicker-current {visibility: hidden!important;}
    
    .error-message{
        float:left;
        width:100%;
        margin-bottom:15px;
    }
    
</style>

<div class="top">
    <span class="title1">Creaci&oacute;n de calendario</span>
</div>
<?php echo $form->create  ('AccountingCalendar',array ('action'    => 'add',         'id'    => 'accountingCalendarForm')); ?>
<div class="left-block">
    <span class="title2">Nuevo calendario</span>
    <p>
        
        Todos los calendarios constan de un rango de fechas, (inicio y fin), para determinar el alcance, y una fecha de ejecuci&oacute;n
        <span class="timeTitle">Fecha y hora de inicio:</span>
        <?php echo $form->input   ('since',     array ('type'   => 'text', 'class' => 'formatDateInput', 'label' => false, 'div' => false)); ?>

        <span class="timeTitle">Fecha y hora de finalizaci&oacute;n:</span>
        <?php echo $form->input   ('until',     array ('type'   => 'text', 'class' => 'formatDateInput', 'label' => false, 'div' => false)); ?>

        <span class="timeTitle">Fecha y hora de ejecuci&oacute;n:</span>
        <?php echo $form->input   ('execution', array ('type'   => 'text', 'class' => 'formatDateInput', 'label' => false, 'div' => false)); ?>
        
    </p>
</div>
<div class="right-block">
    <?php
        echo $this->element ('theme_clean/dateTimePicker',   array ('idName'    => 'since',       'title' => 'Fecha de inicio',                 'textfield' => false));
        echo $this->element ('theme_clean/dateTimePicker',   array ('idName'    => 'until',       'title' => 'Fecha de finalizaci&oacute;n',    'textfield' => false));
        echo $this->element ('theme_clean/dateTimePicker',   array ('idName'    => 'execution',   'title' => 'Fecha de ejecuci&oacute;n',       'textfield' => false));
        echo $form->submit  ('Crear calendario',             array ('div'       => 'submitButton'));
    ?>
</div>
<?php echo $form->end(); ?>