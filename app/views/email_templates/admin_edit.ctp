<div class="js-responses">
<?php 
	/*if($this->data['EmailTemplate']['is_html']):
		echo $this->element('js_tiny_mce_setting', array('cache' => Configure::read('tiny_mce_cache_time.listing_element_cache_duration')));
	endif;*/
?>
<div class="info-details">
<?php echo $html->cText($this->data['EmailTemplate']['description'], false); ?>
</div>
<?php
	echo $form->create('EmailTemplate', array('id' => 'EmailTemplateAdminEditForm'.$this->data['EmailTemplate']['id'], 'class' => 'normal js-insert js-ajax-form', 'action' => 'edit'));
	echo $form->input('id');
	echo $form->input('from', array('label' => __l('From'),'id' => 'EmailTemplateFrom'.$this->data['EmailTemplate']['id'], 'info' => __l('(eg. "displayname &lt;email address>")')));
	echo $form->input('reply_to', array('label' => __l('Reply To'),'id' => 'EmailTemplateReplyTo'.$this->data['EmailTemplate']['id'], 'info' => __l('(eg. "displayname &lt;email address>")')));
	echo $form->input('subject', array('label' => __l('Subject'),'class' => 'js-email-subject', 'id' => 'EmailTemplateSubject'.$this->data['EmailTemplate']['id'],  'info' => $html->cText($this->data['EmailTemplate']['email_variables'], false)));
        ?>
<span class="email-template"><?php echo __l('Email Type');?></span>
<?php
    echo $form->input('is_html', array('label' => __l('Is Html'),'type' => 'radio', 'legend' =>false, 'class' => 'js-toggle-editor', 'options' => array('0' => 'text', '1' => 'html')));
/*	if($this->data['EmailTemplate']['is_html']):
		echo $html->link(__l('Plain Text'), '#' , array('class' => 'js-editor-toggle-link js-plain-text'));
	endif;
*/	echo $form->input('email_content', array('label' => __l('Email Content'),'type' =>'textarea', 'class' => 'js-email-content email-content js-editor', 'id' => 'EmailTemplateEmailContent'.$this->data['EmailTemplate']['id'], 'info' => $html->cText($this->data['EmailTemplate']['email_variables'], false)));
	echo $form->end(__l('Update'));
?>
</div>