<h2><?php echo __l('Ver todos los mails');?></h2>

	<?php
		if (!empty($emailTemplates)):
	?>
      <ul>
		<?php
				foreach ($emailTemplates as $emailTemplate):
		?>		
				<li>
					<?php echo $html->link($html->cText($emailTemplate['EmailTemplate']['name'], false), array('controller' => 'email_templates', 'action' => 'see', $emailTemplate['EmailTemplate']['id']), array('escape' => false,'target'=> '_blank'));?>
				</li>
		<?php
				endforeach;
		?>
	</ul>
	<?php
		else:
	?>
		<p class= "notice"><?php echo __l('No mail templates added yet.'); ?></p>
	<?php
		endif;
	?>	
