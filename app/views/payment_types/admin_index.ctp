<div class="paymentTypes index">
<h2><?php echo __l('Payment Types');?></h2>
<?php echo $this->element('paging_counter');?>
<table class="list">
    <tr>   
        <th><?php echo $paginator->sort(__l('Name'),'name');?></th>  
    </tr>
<?php
if (!empty($paymentTypes)):

$i = 0;
foreach ($paymentTypes as $paymentType):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td class="dl">
		   <div class="actions-block">
			<div class="actions round-5-left">
				  <?php echo $html->link(__l('Edit'), array('action'=>'edit', $paymentType['PaymentType']['id']), array('class' => 'edit js-edit', 'title' => __l('Edit')));?>
			</div>
			</div>
			<?php echo $html->cText($paymentType['PaymentType']['name']);?>
		</td>	
	</tr>
<?php
    endforeach;
else:
?>
	<tr>
		<td colspan="5" class="notice"><?php echo __l('No Payment Types available');?></td>
	</tr>
<?php
endif;
?>
</table>

<?php
if (!empty($paymentTypes)) {
    echo $this->element('paging_links');
}
?>
</div>
