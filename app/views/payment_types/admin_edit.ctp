<div class="paymentTypes form">
	<div>
        <div>
            <h2><?php echo __l('Edit Payment Type - ').$html->cText($this->data['PaymentType']['name']); ?></h2>
        </div>
		<div>
<?php echo $form->create('PaymentType', array('class' => 'normal'));?>	
	<?php
		echo $form->input('id');
		echo $form->input('name');
		 echo $form->input('is_active', array('label' => __l('Active')));
	?>	
<?php echo $form->end(__l('Update'));?>
		</div>
	</div>
</div>



