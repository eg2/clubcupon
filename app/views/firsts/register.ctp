<div class="popupBox-recommend">
  <div class="header-recommend">
    <h1>Registrate y sum&aacute;á cr&eacute;dito</h1>
    <div class="closeitem"></div>
    <h2>En menos de un minuto podr&aacute;s aumentar tu saldo en hasta <span class="red-color">200$</span></h2>
    <p><strong>Registrate, invit&aacute; amigos y obten&eacute; hasta 200&eacute; de cr&eacute;dito en tu Cuenta Club Cup&oacute;n.
      </strong></p>
  </div>
  <h3>Ingresa tus datos</h3>
  <p>Los campos marcados con un aster&iacute;tco (<span class="red-color">*</span>) son obligatorios</p>
  <div class="clear"></div>
  <h3>Instrucciones Especiales</h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam leo ut est pharetra pharetra. Proin lectus leo, dictum a feugiat quis, rhoncus at massa. Aenean congue libero nec enim posuere sed gravida neque faucibus. Nam nec justo vitae nisi dictum aliquet ut a risus. Proin in sagittis nulla. Sed dictum dapibus odio eget rutrum. </p>
  <div class="inside-m">
    <?php echo $form->create('User', array('action' => 'register', 'class' => 'normal js-register-form ')); ?>
    <fieldset>
      <?php
      echo $form->input('username',array('info' => __l('Must start with an alphabet. <br/> Must be minimum of 3 characters and <br/> Maximum of 20 characters <br/> No special characters and spaces allowed'),'label' => __l('Username')));
      echo $form->input('email',array('label' => __l('Email')));
      echo $form->input('referred_by_user_id',array('type' => 'hidden',));
      if(empty($this->data['User']['fb_user_id'])):
        echo $form->input('passwd', array('label' => __l('Password')));
        echo $form->input('confirm_password', array('type' => 'password', 'label' => __l('Password Confirmation')));
        echo $form->input('type',array('type' => 'hidden', 'value' => ''));
      endif;

      if (!empty($this->data['User']['f'])):
        echo $form->input('f', array('type' => 'hidden'));
      endif;
      ?>

      <?php

      echo $form->input('country_iso_code', array('type' => 'hidden','id' => 'country_iso_code'));
      echo $form->input('State.name', array('type' => 'hidden'));
      echo $form->input('City.name', array('type' => 'hidden'));

      if(!empty($refer)) {
        if(isset($_GET['refer']) && ($_GET['refer']!='')) {
          $refer = $_GET['refer'];
        }
        echo $form->input('referer_name', array('value' => $refer, 'label'=>__l('Reference Code')));
      }else {
        echo $form->input('referer_name', array('type' => 'hidden'));
      }
      ?>
      <?php
      if(!empty($this->data['User']['fb_user_id'])):
        echo $form->input('fb_user_id', array('type' => 'hidden', 'value' => $this->data['User']['fb_user_id']));
      endif;
      ?>
      <div class="input captcha-block clearfix js-captcha-container">
        <div class="captcha-left">
          <?php echo $html->image(Router::url(array('controller' => 'users', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('alt' => __l('[Image: CAPTCHA image. You will need to recognize the text in it; audible CAPTCHA available too.]'), 'title' => __l('CAPTCHA image'), 'class' => 'captcha-img'));?>
        </div>
        <div class="captcha-right">
          <?php echo $html->link(__l('Reload CAPTCHA'), '#', array('class' => 'js-captcha-reload captcha-reload', 'title' => __l('Reload CAPTCHA')));?>
          <?php echo $html->link(__l('Audible CAPTCHA'), array('controller' => 'users', 'action' => 'captcha_play'), array('class' => 'captcha-audio', 'title' => __l('Audio Version'), 'rel' => 'nofollow')); ?>
        </div>
      </div>
      <?php echo $form->input('captcha', array('label' => __l('Security Code'), 'class' => 'js-captcha-input')); ?>
      <?php echo $form->input('is_agree_terms_conditions', array('label' => sprintf(__l('I have read, understood &amp; agree to the %s'), $html->link('Términos y Condiciones', array('controller' => 'pages', 'action' => 'view', 'term-and-conditions'), array('target' => '_blank'))))); ?>
      <?php echo $form->hidden('from_popup', array('value' => 'true')); ?>
      <div class="submit-block-register clearfix">
        <?php echo $form->submit(__l('Submit')); ?>
      </div>
    </fieldset>
    <?php  echo $form->end()
    ;?>
  </div>


  <div class="bg-popup-recommend">
  </div>
</div>