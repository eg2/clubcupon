<?php $this->element('head'); ?>
  <body class="noBackground">
    <div id="popup_recommend">
      <h2>
        
        <input id="selectAll" checked="true" type="checkbox" value="" /><strong>Seleccionar todo</strong>
        <span>Seleccionados: <span id="contactsSelected"><?php echo count($contacts); ?></span>/100</span>
      </h2>
      <?php echo $form->create('SelContacts', array('class' => 'normal','url' => array('controller' => 'firsts', 'action' => 'selcontacts'))); ?>
        <select id="selectContacts"   name="contacts[]" multiple="multiple" class="multiple">
          <?php foreach ($contacts as $email=>$name): ?>
            <?php echo "<option  value='{$email}||-||{$name}' type='checkbox'  selected='selected'>"; ?>
            <?php echo "{$name}  - {$email}" ;  ?>
            <?php echo "</option>" ?>
          <?php endforeach; ?>
        </select>
        <input type="hidden" name="oiSessionId" value="<?php echo $oiSessionId; ?>" />
        <input type="hidden" name="provider" value="<?php echo $provider; ?>" />
      <?php echo $form->end(); ?>
      <input name="" onclick="$('#SelContactsAddForm').submit();" type="button" class="bt_enviar" value=" " />
      <div class="clear"></div>
      <div class="tag" style="right:0;"></div>
    </div>
  </body>
</html>

<script type="text/javascript">
  $(document).ready(function() {
    $('#selectContacts').change(updateContactsSelected);
    $('#selectAll').click(
     function() {
       if (!$(this).attr('checked')) {
         $("option").each(function(){  
               $(this).attr("selected","");
         });       
       } else {
       $("option").each(function(){  
             $(this).attr("selected","selected");
       });
      }
      updateContactsSelected();
    });
    updateContactsSelected();
    
  });
  function updateContactsSelected() {
    var sel = $('#selectContacts :selected').length;
    $('#contactsSelected').html(sel);
  }
</script>
