
<?php 
echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection');
?>

<script>

    $(document).ready(function() {

        var submitObj = document.getElementById('susc');

        verifyEmail();

        $("#SubscriptionEmail").bind({
            'click': function(){
                $(this).val('');
            },
            'blur': function(){
                if($(this).val()==''){
                    $(this).val('Ingresá tu mail');
                }
            }
        });

        function verifyEmail()
        {

            var status = false;
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

            if ($('.mail').val().search(emailRegEx) == -1)
            {
                //$("input.comprar").css({background: "#ccc", cursor:"default" });
				$("input.suscription").addClass('compraroff');
                submitObj.disabled = true;
            }
            else
            {
                //$("input.comprar").css({background: "#e79108", cursor:"pointer" });
                $("input.suscription").removeClass('compraroff');
				submitObj.disabled = false;
                status = true;
            }

            return status;

        }

        $(".mail").keyup(function() {;
            verifyEmail();
        });

    });  //fin doc ready

</script>
<div class="bloque-pop clearfix">
	<div id="cc-popup">     
        
		<img src="/img/web/clubcupon/banner_popup.jpg" alt="alter" width="620" height="313"/>

        <?php echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform')); ?>
	        <?php echo $form->input('email', array('type' => 'text', 'class'=>'mail', 'default'=>'Ingresá tu mail', 'label'=> false)); ?>

			<?php if(isset($_GET['utm_source'])){?>
				<?php echo $form->hidden('utm_source', array('value' => $_GET['utm_source'])); ?>
			<?php }?>

			<?php if(isset($_GET['utm_medium'])){?>
				<?php echo $form->hidden('utm_medium', array('value' => $_GET['utm_medium'])); ?>
			<?php }?>

			<?php if(isset($_GET['utm_term'])){?>
				<?php echo $form->hidden('utm_term', array('value' => $_GET['utm_term'])); ?>
			<?php }?>

			<?php if(isset($_GET['utm_content'])){?>
				<?php echo $form->hidden('utm_content', array('value' => $_GET['utm_content'])); ?>
			<?php }?>

			<?php if(isset($_GET['utm_campaign'])){?>
				<?php echo $form->hidden('utm_campaign', array('value' => $_GET['utm_campaign'])); ?>
			<?php }?>
			
			<?php
				$options = array();
				foreach ($clubcupon->activeCitiesWithDeals () as $city){
					$options[$city ['City']['id']] = $city ['City']['name'];
				}
				echo $form->input('city_id', array( 'label' => false, 'empty' => false, 'selected' => Configure::read ('Actual.city_id'), 'options' => $options));
			?>
                
			<input id="susc"  class="suscription" type="submit" value="Suscribirme!" />
		<?php echo $form->end(); ?>

		<article>
			<a id="facebookSubscribeButton" href="#" class="suscribirme">Suscribirme con Facebook</a>
			<p>Seguí nuestros descuentos en</p>
			<ul class="redes">
				<li><a href="<?php echo $city_facebook_url; ?>" target="_blank" id="icon_facebook">Facebook</a></li>
				<li><a href="<?php echo $city_twitter_url; ?>" target="_blank" id="icon_twitter">Twitter</a></li>
			</ul>
		</article>
	</div>
	
	
	
	
	<!-- div class="pie">
		<?php echo $html->link ('Pol&iacute;ticas de Privacidad', array ('controller' => 'pages', 'action' => 'policy'), array ('title' => 'Pol&iacute;ticas de Privacidad', 'target' => '_top', 'escape'=>true)) ?>

		<?php if (isset ($redirect)) { ?>
			<a href="#" onclick="$.cookie('CakeCookie[first_time_user]', 'no'); parent.window.location.replace('<?php echo $redirect; ?>'); return false;">Ya estoy suscripto</a>
		<?php } else { ?>
			<a href="#" onclick="parent.$.fn.colorbox.close(); $.cookie('CakeCookie[first_time_user]', 'no'); return false;">Ya estoy registrado</a>
		<?php } ?>
    </div -->
</div>