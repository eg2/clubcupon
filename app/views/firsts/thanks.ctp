<?php $this->element('head'); ?>
  <body class="noBackground">
    <div id="popup_recommend">
        <!-- pasar los inline styles a popups.css -->
        <div style="text-align: center; padding: 20px 0 0 0;">
            <?php echo $html->image('popup_recommend/txt_gracias.png'); ?>
        </div>
        <div style="width:550px; margin:0 auto; padding: 30px 0;">
            <div style="float:left; padding: 10px 0;">
            <?php echo $html->image('popup_recommend/txt_tambien_en.png'); ?>
            </div>
            <div style="float:right; border:1px dashed #ccc; width:250px; height: 70px; padding: 10px 0;margin:0 0 50px 0; text-align: center;">
                <span id="facebookTarget">
                    <a class="fb" href="javascript:void(0)" onclick="<?php echo "fbFeed_deal('" . $deal_name . "', '" . $bityurl . "', '" . intval($deal_discount_percentage) . "');"; ?>">
                                            <?php echo $html->image('social_actions/facebook.png'); ?>
                    </a>
                </span>
                <span id="twitterTarget">
                    <a class="tw" target="_blank" href = "http://twitter.com/share?url=<?php echo rawurlencode($bityurl); ?>&text=<?php echo rawurlencode($deal_name); ?>&via=<?php echo $city_twitter_username; ?>&lang=es" title="<?php echo 'Twite&aacute; esta oferta'; ?>">
                                            <?php echo $html->image('social_actions/twitter.png'); ?>
                    </a>
                </span>  
            </div>
        </div>

      <div class="clear"></div>
              

    </div>
    <script type="text/javascript">
        
        <?php
        /////////////////////////////////////////////////////////////////////////////////////
        //Al insertarse los iconos de facebook y twitter, clonamos un elemento del parent.
        //A fin de poder trasladar cambios, si es que se cambia como se muestran
        //los links de facebook, twitter, etc. y respetando el nuevo diseño
        //reemplazo la ruta la de la imagen por la nueva, especifica de este popup
        /////////////////////////////////////////////////////////////////////////////////////
        ?>
            
        var haystackText = "";
        function findMyText(needle, replacement) {
            if (haystackText.length == 0) {
              haystackText = document.getElementById("popup_recommend").innerHTML;
            }
             var match = new RegExp(needle, "ig");     
             var replaced = "";
             if (replacement.length > 0) {
                  replaced = haystackText.replace(match, replacement);
             }
             else {
                  var boldText = "<div>" + needle + "</div>";
                  replaced = haystackText.replace(match, boldText);
             }
             document.getElementById("popup_recommend").innerHTML = replaced;
        }
                
        //////////////////////////////////////////////////////////////////////

      var $p = window.parent.$;
      if($p('#twitterSocialAction').length){
        $('#twitterTarget').append($p('#twitterSocialAction').clone());
        findMyText("social_actions/twitter.png", "popup_recommend/twitter_comparti.jpg");
      }
    
      if($p('#facebookSocialAction').length){   
      $('#facebookTarget').append($p('#facebookSocialAction').clone());
        findMyText("social_actions/facebook.png", "popup_recommend/face_comparti.jpg");
      }
      
  
      function makeLinks(text)
      {
        text = text.replace('/img/social_actions/facebook.png', 'blaaaaa');
        return text;
      }

  
    </script>
    <div id="fb-root"></div>
    <?php echo $html->image('popup_recommend/banner_footer.jpg'); ?>
  </body>
</html>