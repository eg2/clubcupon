<?php
class AdminpagosHelper extends AppHelper {

  var $helpers = array('Form', 'Html');


  function filterPaymentOptionBySettingId($settingId) {
    $ret = array();
    foreach($this->paymentOptions as $paymentOption) {
      if($paymentOption['PaymentSetting']['id'] == $settingId) {
        $ret[] = $paymentOption;
      }
    }
    return $ret;
  }
  function hasPaymentPlans($optionId) {
    if(!isset($this->PaymentOptionPlans)) {
        $this->PaymentOptionPlans = &ClassRegistry::init('PaymentOptionPlans');
    }
    $result = $this->PaymentOptionPlans->findByPaymentOptionId($optionId);
    
    if(empty($result)){
        return false;
    } else {
        return true;
    }
  }

  function payment_settings($selectedValues, $paymentOptionPlans, $error = false) {
    $html = '';
    $form = $this->Form;
    if(!isset($this->PaymentSetting)) {
     $this->PaymentSetting = &ClassRegistry::init('PaymentSetting');
    }
    if(!isset($this->PaymentOption)) {
     $this->PaymentOption = &ClassRegistry::init('PaymentOption');
    }    
    $this->paymentOptions  = $this->PaymentOption->find('all');

    $html .= '<script type="text/javascript">';
    $html .=  'var paymentOptionsData = ' . json_encode($this->paymentOptions) .';' . "\n\n";
    $html .=  '$(document).ready(function(){$(".js-payment-settings-group").change()});' . "\n";

    $html .= '</script>';
    $paymentSettings = $this->PaymentSetting->find('list');
    if($error) {
      $html .= '<div class="paymentError">';
      $html .=  $error;
      $html .= '</div>';
    }
    $paymentOptionsHtml = '';
    $paymentSettingsToShow = array('0' => 'Ver todos');
    foreach($paymentSettings as $id => $val) {
      $options = $this->filterPaymentOptionBySettingId($id);
      if(!empty($options)) {
          $exlusivity_class="exclusive";

          if(in_array($val, Configure::read('deal.not_exclusive_paymentOptions')))
          {
              $exlusivity_class="not_exclusive";
          }

        $paymentOptionsHtml .= '<div class="js-payment-setting-group-'.$id.' payment-setting-group-selector '.$exlusivity_class.'">';
        $paymentOptionsHtml .= '<h4>'.$val.'</h4>';
        $paymentOptionsHtml .= '<input type="hidden" class="js-payment-setting-group-id" value="'.$id.'" />';
        $paymentSettingsToShow[$id] = $val;
        foreach($options as $option) {
          $selected = '';
          if(in_array($option['PaymentOption']['id'],$selectedValues)) {
            $selected = ' checked="checked" ';
          }
          $paymentOptionsHtml .='<p>';
          $paymentOptionsHtml .= '<input type="checkbox" name="paymentOptionValue['.$option['PaymentOption']['id'].'][PaymentOptionId]" value="'.$option['PaymentOption']['id'].'" '.$selected.' />' . $option['PaymentOption']['name'];

          if ($this->hasPaymentPlans($option['PaymentOption']['id'])){
            $paymentOptionsHtml = $this->getHTMLPlansByOptionId($paymentOptionsHtml, $option, $paymentOptionPlans);
          }
          
          $paymentOptionsHtml .='</p>';

        }
        $paymentOptionsHtml .= '<div class="actions-payment">';
        $paymentOptionsHtml .= '<a href="#" class="js-payment-setting-group-select-all" >Seleccionar todos</a> | ';
        $paymentOptionsHtml .= '<a href="#" class="js-payment-setting-group-select-none" >Ninguno</a>';
        $paymentOptionsHtml .= '</div>';
        $paymentOptionsHtml .= '</div>';
      }
    }
    $html .= $form->input('payment_settings',array(
                                        'options'=> $paymentSettingsToShow,
                                        'label'=> 'Medios de pago',
                                        'class' => 'js-payment-settings-group',
                                        'empty' => '-- Seleccione un grupo de gateways --',
                                        ));
    $html .= $paymentOptionsHtml;
    return $html;
  }

  function getHTMLPlansByOptionId($html, $options, $selectedPlanId=null) {
      if (!isset($this->PaymentOptionPlan)) {
        $this->PaymentOptionPlan = &ClassRegistry::init('PaymentOptionPlan');
      }
      if (!isset($this->PaymentPlan)) {
          $this->PaymentPlan = &ClassRegistry::init('PaymentPlan');
      }
      $optionId = $options['PaymentOption']['id'];
      
      $paymentOptionPlanFindOptions = array(
            'conditions' => array(
                'payment_option_id =' => $options['PaymentOption']['id'],
             ),
            'order' => 'PaymentOptionPlan.order ASC',
        );
      
      $optionPlans = $this->PaymentOptionPlan->find('all', $paymentOptionPlanFindOptions);
        
      $html .= '<select name="paymentOptionValue['.$options['PaymentOption']['id'].'][PaymentPlanId]">';
      
      foreach ($optionPlans as $optionp) {
          $plan  = $this->PaymentPlan->read(null,$optionp['PaymentOptionPlan']['payment_plan_id']);
          $html  = $html . '<option value='.$plan['PaymentPlan']['id'];
          if($this->hasPlanIdSelected($plan['PaymentPlan']['id'], $optionId, $selectedPlanId)) {
            $html .= ' selected="selected" '; 
          }
          $html .= ' >';
          $html .= $plan['PaymentPlan']['name'];
          $html .='</option>';
      }
      $html .= '</select>';
      return $html;
  }

  function hasPlanIdSelected($paymentPlanId, $optionId, $selectedPlanId) {
      if (array_key_exists($optionId, $selectedPlanId) && 
              $paymentPlanId==$selectedPlanId[$optionId]['payment_plan_id']) {
        return true;
      } else {
        return false;
      }
  }
  
  function getPaymentTypesFor($idDeal) {
    $html = '';
    $this->logos = array();
    $form = $this->Form;
    if(!isset($this->PaymentOptionDeal)) {
     $this->PaymentOptionDeal = &ClassRegistry::init('PaymentOptionDeal');
    }
    if (!isset($this->PaymentPlanItem)) {
     $this->PaymentPlanItem = &ClassRegistry::init('PaymentPlanItem');
     $this->PaymentPlanItem->recursive = 1;
    }   
    $options  = $this->PaymentOptionDeal->find('all',array(
        'conditions' => array('PaymentOptionDeal.deal_id' => $idDeal),
        'recursive' => 2
    ));
    $ret = array();
    foreach($options as $opt) {
      $plans = array();
      $type = $opt['PaymentOption']['PaymentType'];
     
      if (!empty($opt['PaymentOptionDeal']['payment_plan_id'])) {
          $planId = $opt['PaymentOptionDeal']['payment_plan_id'];
          $plans = 
             $this->PaymentPlanItem->find('list',
               array(
                    'fields' => array('PaymentPlanItem.id','PaymentPlanItem.name'),
                    'conditions' => array ('PaymentPlanItem.payment_plan_id' => $planId)
                   ));
      }
   
      $method = $opt['PaymentOption']['PaymentMethod'];
      if (!empty($type['gateway_key'])) {
          $ret[$type['gateway_key']]['id_gateway'] = $type['gateway_id'];
          $ret[$type['gateway_key']]['cards'][$opt['PaymentOption']['id']] = array(
          'payment_type_id' =>  $type['id'],
          'payment_option_id' => $opt['PaymentOption']['id'],
          'logo' => '/img/card_logos/' . $method['logo'],
          'name' => $opt['PaymentOption']['name'],
          'bac_payment_type' => $type['bac_payment_type'],
          'id_gateway' => $type['gateway_id'],
          'PaymentPlans' => $plans,
          );
      }
      
      //$ret[$type['gateway_key']]['cards'][$opt['PaymentOption']['id']]['PaymentPlan'] = $plans;
      $this->logos[$type['gateway_id'] . '_' . $type['id'] . '_'. $opt['PaymentOption']['id']] = $method['logo'];
    }
    return $ret;
  }

  function printJsLogoTable() {
    echo '<script type="text/javascript">';
    echo '  var logoBuyTable = ' . json_encode($this->logos) . ";\n";
    echo '</script>';
  }

  function adminSelectAll($settingId) {
    echo '<script type="text/javascript">' . "\n";
    echo '  $(document).ready(function(){' . "\n";
    echo ' $(".js-payment-settings-group").val(0).change();' . "\n";
    echo ' $(".js-payment-setting-group-'.$settingId.' .js-payment-setting-group-select-all").click();';
    echo '}) ' . ";\n";
    echo '</script>' . "\n";

  }

  function printJsWalletCard($walletGatewayId, $walletCard) {
      echo '<script type="text/javascript">';
      echo '  var walletGatewayId = ' . json_encode($walletGatewayId) . ";\n";
      echo '  var walletCard = ' . json_encode($walletCard) . ";\n";
      echo '</script>';
  }


  function getWalletCard ($cards) {
      $key = $this->recursiveArraySearch($cards, '5000', 'payment_type_id');
      return $cards[$key];
  }

  function recursiveArraySearch($haystack, $needle, $index = null)
  {
      $aIt     = new RecursiveArrayIterator($haystack);
      $it    = new RecursiveIteratorIterator($aIt);

      while($it->valid())
      {
          if (((isset($index) AND ($it->key() == $index)) OR (!isset($index))) AND ($it->current() == $needle)) {
              return $aIt->key();
          }
          $it->next();
      }
      return false;
  }

}
