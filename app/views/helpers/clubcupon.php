<?php

App::import('Model', 'web.WebCity');

class ClubcuponHelper extends AppHelper {
  var $helpers = array ('Form', 'Html');

  function mainDeal () {
    if (!isset ($this->Deal)) {
      $this->Deal = & ClassRegistry::init ('Deal');
    }

    $not_conditions = array ();
    $conditions     = array ();

    $conditions ['Deal.is_side_deal'] = 0;
    $conditions ['Deal.city_id'] = Configure::read ('Actual.city_id');
    $conditions ['Deal.deal_status_id'] = array (ConstDealStatus::Open, ConstDealStatus::Tipped);
    $conditions [] = 'Deal.parent_deal_id = Deal.id';

    $not_conditions ['Not']['Deal.deal_status_id'] = array (
        ConstDealStatus::PendingApproval,
        ConstDealStatus::Upcoming
    );

    $main_deal = $this->Deal->find ('first', array (
        'conditions' => array (
            $conditions,
            $not_conditions,
        ),
        'contain' => array (
            'Company',
            'City',
            'Attachment',
        ),
        'recursive' => 1,
        'order' => array (
            'Deal.id' => 'desc',
        ),
    ));

    return $main_deal;
  }

  function sideDeals () {
    if (!isset ($this->Deal)) {
      $this->Deal = &ClassRegistry::init ('Deal');
    }

    $not_conditions = array ();
    $conditions     = array ();

    $conditions ['Deal.is_side_deal'] = 1;
    $conditions ['Deal.city_id'] = Configure::read ('Actual.city_id');
    $conditions ['Deal.deal_status_id'] = array (ConstDealStatus::Open, ConstDealStatus::Tipped);
    $conditions [] = 'Deal.parent_deal_id = Deal.id';

    $not_conditions ['Not']['Deal.deal_status_id'] = array (
        ConstDealStatus::PendingApproval,
        ConstDealStatus::Upcoming
    );

    $side_deals = $this->Deal->find ('all', array (
        'conditions' => array (
            $conditions,
            $not_conditions,
        ),
        'contain' => array (
            'Company',
            'City',
            'Attachment',
        ),
        'recursive' => 1,
        'order' => array (
            'Deal.priority'       => 'DESC',
            'Deal.deal_status_id' => 'ASC',
            'Deal.start_date'     => 'ASC',
            'Deal.id'             => 'ASC'
        )
    ));

    return $side_deals;
  }
  
  
  function _findQuestionAndAnswers ($questionName) {
    $options = array (
        'conditions' => array (
            'Question.name' => $questionName,
        ),
        'recursive' => 1,
    );

    $original = $this->Question->hasMany['Answer']['conditions'];
    $this->Question->hasMany['Answer']['conditions'] = array ('Answer.creator' => "backend");

    $ret = $this->Question->find ('first', $options);

    $this->Question->hasMany['Answer']['conditions'] = $original;

    return $ret;
  }

  function questions ($questionName, $default = array (), $otherText = '', $showOther = true) {
    // inicializamos Question si no lo esta ya
    if (!isset ($this->Question)) {
      $this->Question = & ClassRegistry::init ('Question');
    }

    // buscamos preguntas y respuestas para la pregunta en cuestion y las partimos en variables
    $questionAndAnswers = $this->_findQuestionAndAnswers ($questionName);
    $question = $questionAndAnswers ['Question'];
    $answers  = $questionAndAnswers ['Answer'  ];

    // inputName sera el nombre generico de todo el select
    $inputName = 'question_' . $question ['id'];

    // answersForSelect es solo un reformateo de las preguntas/respuestas en el formato que espera Cake
    $answersForSelect = array ();
    foreach ($answers as $answer) {
      $answersForSelect [$answer ['id']] = $answer ['content'];
    }

    // si hay defaults, los manejamos aca:
    // $default va a ser una array de valores que pueden estar seleccionados
    $defaultAnswer = array ();
    foreach ($default as $def) {
      if ($def ['question_id'] === $question ['id']) {
        $defaultAnswer [] = $def ['answer_id'];
      }
    }

    if ($showOther) {
      if ($otherText == '') {
        $answersForSelect ['0'] = '-- Otra opción --';
      } else {
        $answersForSelect ['0'] = $otherText;
      }
    }

    /*
    $defaultForSelect = null;
    $allValues = array_keys ($answersForSelect);
    if (in_array ($defaultAnswer, $allValues)) {
      $defaultForSelect = $defaultAnswer;
      $defaultAnswer = '';
    } else if ($defaultAnswer !== null) {
      // find default Answer
      if (!isset ($this->Answer)) {
        $this->Answer = &ClassRegistry::init('Answer');
      }
      $answer = $this->Answer->findById ($defaultAnswer);
      $defaultAnswer = $answer ['Answer']['content'];
      $defaultForSelect = '0';
    } else {
      $defaultAnswer = '';
    }
    */

    // iniciamos la salida
    $out = "\n";
    $out .= '<div id = "' . $inputName . '_div">';
    // manejamos el caso de respuestas multiples o no
    if ($question ['multiple'] == 1) {
      $out .= $this->Form->input ($inputName, array (
          'options'  => $answersForSelect,
          // 'label'    => $question ['content'] . '<br /><i style = "font-size: xx-small;">(pod&eacute;s seleccionar m&aacute;s de una apretando la tecla Ctrl)</i>',
          'label'    => '<strong>' . $question ['content'] . '</strong>',
          'class'    => 'js-question',
          'id'       => 'question_' . $question ['id'],
          'default'  => $defaultAnswer,
          'multiple' => 'checkbox',
      ));
	  
    } else {
      $out .= $this->Form->input ($inputName, array (
          'options' => $answersForSelect,
          'label'    => '<strong>' . $question ['content'] . '</strong>',
          'class'   => 'js-question',
          'id'      => 'question_' . $question ['id'],
          'default' => $defaultAnswer,
          'empty'   => '-- Seleccione una opción --',
      ));
    }
    // manejamos la entrada libre
    if ($showOther) {
      $inputName = 'questionFree_' . $question ['id'];
      $out .= $this->Form->input ($inputName, array (
          'div'    => 'input text invisible',
          'before' => '<div class = "freeresponse invisible">',
          'type'   => 'text',
          'label'  => 'Otra opción',
          'class'  => '',
          'id'     => 'questionfree_' . $question ['id'],
          'value'  => $defaultAnswer,
          'after'  => '</div>'
      ));
    }
    // finalizamos la salida
    
	$out .= '</div>';

    return $this->output ($out);
  }

  function activeCities () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveCities ();
  }

  function activeGroupsWithActiveDeals () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveGroupsWithDeals (Configure::read('corporate.city_name'));
  }
  function activeCitiesWithActiveDeals () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveCitiesWithActiveDeals ();
  }

  function activeCitiesWithDeals () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveCitiesWithDeals ();
  }

  function activeCitiesWithDealsPre() {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }
    $myCity = new WebCity();
    $resp = $myCity->getActiveCitiesWithDeals();
    return $resp;
  }
  
  function activeCitiesForBanners () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveCitiesForBanners ();
  }

  function activeGroups () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveGroups ();
  }

  function activeGroupsWithDeals () {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveGroupsWithDeals (array('mcdonalds'));
  }

  function activeAllowedCities ($userid) {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveAllowedCities ($userid);
  }

  function activeAllowedCitiesWithDeals ($userid) {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveAllowedCitiesWithDeals ($userid);
  }

  function activeAllowedGroups ($userid) {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveAllowedGroups ($userid);
  }

  function activeAllowedGroupsWithDeals ($userid) {
    if (!isset ($this->City)) {
      $this->City = & ClassRegistry::init ('City');
    }

    return $this->City->getActiveAllowedGroupsWithDeals ($userid);
  }

  function renderBanners () {
    if (!isset ($this->Banner)) {
      $this->Banner = & ClassRegistry::init ('Banner');
    }

    $today = date ("Y-m-d");
    $currentCity = Configure::read ('Actual.city_id');

    $this->set ('banners', $this->Banner->find ('all', array (
        'conditions' => array (
            'status =' => '1',
            'OR' => array (
                array ('city_id = ' => ''),
                array ('city_id = ' => $currentCity)
            ),
            'start_date <=' => $today,
            'end_date >=' => $today,
        ),
        'order' => 'Banner.order ASC',
    )));
  }
  function traeOfertasActuales(){
    if (!isset ($this->Deal)) {
      $this->Deal = &ClassRegistry::init ('Deal');
    }
    $not_conditions = array ();
    $conditions     = array ();

    $conditions ['Deal.city_id'] = Configure::read ('Actual.city_id');
    $conditions ['Deal.deal_status_id'] = array (ConstDealStatus::Open, ConstDealStatus::Tipped);
    $conditions [] = 'Deal.parent_deal_id = Deal.id';
    
    $not_conditions ['Not']['Deal.deal_status_id'] = array (
        ConstDealStatus::PendingApproval,
        ConstDealStatus::Upcoming
    );

    $actual_deals = $this->Deal->find ('all', array (
        'conditions' => array (
            $conditions,
            $not_conditions,
        ),
        'contain' => array (
            'Company',
            'City',
            'Attachment',
        ),
        'recursive' => 1,
        'order' => array (
            'Deal.priority'       => 'DESC',
            'Deal.deal_status_id' => 'ASC',
            'Deal.start_date'     => 'ASC',
            'Deal.id'             => 'ASC'
        )
    ));

    return $actual_deals;
  }
  
  function ordenaOfertas(){
      
      if(isset($this->params['pass'])){
         //traigo las ofertas actuales independientemente de si son o no laterales 
         $actual_deals = $this->traeOfertasActuales();
        
         $aux = array();
         $side_deals = array();
         
            foreach($actual_deals as $idx => $actual_deal){
              
                if (isset($this->params['pass']['0']) && $actual_deal['Deal']['slug'] == $this->params['pass']['0']){
                    $pos = $idx;
                     $slice = array_slice($actual_deals, $pos);
                      
                       array_splice($actual_deals, $pos);
                        $side_deals = array_merge($slice,$actual_deals);
                        return $side_deals;
                    break;
                }
                
            }
      return $actual_deals;
      }
    }

    function addHttp($url) {
      if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
          $url = "http://" . $url;
      }
      return $url;
    }
    
    
    
    /*
     * Acorta strings, a un límite, respetando las entidades html
     * 
     */
    
    function shorten_strings_with_html_entities($s, $l = 25){
        // En el caso de que nos pasen algo no utf-8
        $s   = utf8_decode($s);
        //Quitamos las entidades html
        $s   = html_entity_decode($s);
        //Corto en función del largo
        $sub = (strlen($s) > $l) ? substr($s,0,$l).'...' : $s;
        //Devolvemos el string, con entidades html
        return htmlentities($sub);
    }
    
    function show_24_hs($time_fin){
    	if($time_fin<1) $time_fin='24';
		return $time_fin;
    }
    
    // Reemplaza todos los caracteres especiales, ver correspondencia 1<->1
    function cleanChars($string) {
        // revision extra para evitar acentos y caracteres bizarros...
        $string = str_replace(array('á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò', 'ù', 'ä', 'ë', 'ï', 'ö', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü'),
                              array('a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U'), $string);
        return $string;
    }
}
