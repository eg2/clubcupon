<?php
class BacFormatHelper extends AppHelper {

  var $helpers = array ('Form', 'Html', 'Number');
  private $moneda_pesos   = null;
  const BAC_IVA_CONDITION_CODE_EXCENTO = 7;
  const BAC_IVA_CONDITION_CODE_MONOTRIBUTO = 5;
  const BAC_IVA_CONDITION_CODE_RI = 1;
  const BAC_TOURISM_PRODUCTID_IVA_FULL = 103033;
  const BAC_TOURISM_PRODUCTID_IVA_HALF = 103032;
  const BAC_TOURISM_PRODUCTID_EXEMPT = 103029;
  const BAC_TOURISM_PRODUCTID_RETAIL = 103034;

  public function __construct() {
    $this->moneda_pesos = Configure::read ('BAC.pesos');
  }

  public function do_something() {
    echo 'Hola Mundo BacFormat';
  }

  /* Acorta strings, a un límite, respetando las entidades html
   * @param unknown $s (string)
   * @param unknown $l (optional)
   * @return unknown
   */
  function shorten_strings_with_html_entities($s, $l = 25) {
    // En el caso de que nos pasen algo no utf-8
    $s   = utf8_decode($s);
    //Quitamos las entidades html
    $s   = html_entity_decode($s);
    //Corto en función del largo
    $sub = (strlen($s) > $l) ? substr($s, 0, $l).'...' : $s;
    //Devolvemos el string, con entidades html
    return htmlentities($sub);
  }
  /**
   *
   *
   * @param unknown $time_fin
   * @return unknown
   */
  function show_24_hs($time_fin) {
    if ($time_fin<1) $time_fin='24';
    return $time_fin;
  }
  /**
   * Reemplaza todos los caracteres especiales, ver correspondencia 1<->1
   *
   * @param unknown $string
   * @return unknown
   */
  function cleanChars($string) {
    // revision extra para evitar acentos y caracteres bizarros...
    $string = str_replace(array('á', 'é', 'í', 'ó', 'ú', 'à', 'è', 'ì', 'ò',
        'ù', 'ä', 'ë', 'ï', 'ö', 'ü', 'Á', 'É', 'Í', 'Ó', 'Ú', 'À', 'È',
        'Ì', 'Ò', 'Ù', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü'),
      array('a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'o',
        'u', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I',
        'O', 'U', 'A', 'E', 'I', 'O', 'U', 'A', 'E', 'I', 'O', 'U'),
      $string);
    return $string;
  }
  /**
   * Obtiene el Id de Producto que representa el iva completo 
   * para Las ofertas de Turismo y Mayoristas.
   * @return ProductId
   */
  public function getProductIdFullIva() {
    return Configure::read('BAC.bac_tourism_product_id_full_iva');
  }
  /**
   * Obtiene el Id de Producto que representa el iva half 
   * para Las ofertas de Turismo y Mayoristas.
   * @return ProductId
   */
  public function getProductIdHalfIva() {
    return Configure::read ('BAC.bac_tourism_product_id_half_iva');
  }
  /**
   * Obtiene el Id de Producto que representa el precio exento
   * para Las ofertas de Turismo y Mayoristas.
   * @return ProductId
   */
  public function getProductIdExempt() {
    return Configure::read ('BAC.bac_tourism_product_id_exempt');
  }
  /**
   * Obtiene el Id de Producto que representa el precio detallado
   * para Las ofertas de Turismo y Mayoristas.
   * @return ProductId
   */
  public function getProductIdRetail() {
    return Configure::read ('BAC.bac_tourism_product_id_retail');
  }
  /**
   * Obtiene el Id de producto para el resto de las Ofertas
   * se utiliza para acreditar en BAC
   * @param ProductId $isDealTourism (Ofertas de Turismo)
   * @param ProductId $isEndUser     (Se Factura al usuario Final)
   * @return ProductId
   */
  public function getProductId($isDealTourism, $isEndUser) {

    $loger  = ApiLogger::getInstance();
    $loger->notice(__METHOD__.'Parametros recibidos en getProductId',
      array(
        'isDealTourism'=> $isDealTourism,
        'isEndUser' => $isEndUser,
      ));

    if ($isEndUser) {
      $idProducto = Configure::read ('BAC.id_producto_final');
    } else {
      $idProducto = Configure::read ('BAC.id_producto');
    }
    $loger->notice('Obteniendo el Id de Producto para crédito de la Compra',
      array(
        'isDealTourism'=> $isDealTourism,
        'isEndUser' => $isEndUser,
        'idProducto' => $idProducto
      ));
    return $idProducto;
  }
  /**
   * Obtiene el Id de producto para el resto de las Ofertas
   * y se utiliza para debitar en BAC (descuentos al usuario, pago combinado)
   * @param Boolean $isDealTourism
   * @return ProductId
   */
  public function getProductDebitId($isDealTourism) {
    $loger  = ApiLogger::getInstance();
    $loger->notice('Parametros recibidos getProductDebitId',
      array('isDealTourism'=> $isDealTourism, ));

    $idProductoDebito = Configure::read ('BAC.id_producto_debito');
    $loger->notice('Obteniendo el Id de Producto para débito de la Compra',
      array(
        'isDealTourism'=> $isDealTourism,
        'idProductoDebito' => $idProductoDebito
      ));
    return $idProductoDebito;
  }
  /**
   * Obtiene el Monto Detallado de la Oferta 
   * para Las ofertas de Turismo y Mayoristas.
   * @param Deal $deal
   * @return Amount (Float)
   */
  public function getAmountRetail($deal) {
    $price         = $deal['Deal']['discounted_price'];
    $amount_exempt = $deal['Deal']['amount_exempt'];
    $amount_full_iva = $deal['Deal']['amount_full_iva'];
    $amount_half_iva = $deal['Deal']['amount_half_iva'];
    return $price - ($amount_exempt+ $amount_full_iva + $amount_half_iva);
  }
  /**
   * Obtiene el string a Enviar a BAC por pagos en NPS
   * y Mercado libre
   * @param unknown $productos
   * @return string
   */
  public function generateProductString($productos) {
    foreach ($productos as $val) {
      $producto = (array) $val;
      foreach ($producto as $nameattr => $valattr) {
        $stringProducts =
          $stringProducts ."$nameattr=$valattr,";
      }
      $stringProducts = substr($stringProducts, 0, -1);
      $stringProducts = $stringProducts.';';
    }
    $stringProducts = substr($stringProducts, 0, -1);
    return $stringProducts;
  }

  public function addDiscount($mount, $sub){
      return Precision::sub ($mount, $sub);
  }
  /**
   * Obtiene el codigo de seguridad para realizar la llamada a
   * CrearPagoSeguro (POST form)
   * @param unknown $isDealTourism
   * @return unknown
   */
  public function getSecureCode($isDealTourism) {
    $loger  = ApiLogger::getInstance();
    $secureCode = Configure::read('cc.bac.secure_code');
    $loger->notice('Obteniendo Código de Seguridad para la Compra',
      array(
        'isDealTourism'=> $isDealTourism,
        'secureCode' => $secureCode
      ));
    return $secureCode;
  }
}
