<?php
/*
 * Permite mostrar un texto de ayuda
  */
class TipHelper extends AppHelper {
    var $helpers = array('Html');
    
    function ayuda($texto)
    {
        $helpDiv = '<img style="float:left;" src="/img/tip.png" title="'. $texto .'" class="vtip" />';
        return $helpDiv;
    }
}
?>