<script src="/js/libs/jquery.scrollTo-1.4.2-min.js" type="text/javascript"></script>
<?php echo $this->element('js_tiny_mce_setting', array('cache' => Configure::read('tiny_mce_cache_time.listing_element_cache_duration')));?>
<?php
    if(!empty($page)):
        ?>
        <div class="js-tabs">
        <ul>
            <li><span><?php echo $html->link(__l('Preview'), '#preview'); ?></span></li>
            <li><span><?php echo $html->link(__l('Change'), '#add'); ?></span></li>
        </ul>
        <div id="preview">
            <div class="page">
                <h2><?php echo $page['Page']['title']; ?></h2>
                <div class="entry">
                   <?php echo $page['Page']['content']; ?>
                </div>
            </div>
        </div>
        <?php
    endif;
?>
<div id="add">
    <div class="pages form">      
        <fieldset>
            <h2><?php echo 'Editar P&aacute;gina'; ?></h2>
            <?php
				echo $form->create('Page', array('class' => 'normal'));
                echo $form->input('id');
                echo $form->input('title', array('between' => '', 'label' => 'T&iacute;tulo'));

                $draftOptions = array(0 => 'No',1 => 'Si');
                echo $form->input	('is_disclaimer', array(
                    'type'		=>	'select',
                    'options'	=>	$draftOptions,
                    'empty'		=>	false,
                    'label'		=>	'Son bases y condiciones?'));

                $draftOptions = array(0 => 'Despublicado',1 => 'Publicado');
                echo $form->input	('draft', array(
                    'type'		=>	'select',
                    'options'	=>	$draftOptions,
                    'empty'		=>	false,
                    'label'		=>	'Estado'));
                

                echo $form->input('content', array('type' => 'textarea', 'class' => 'js-editor', 'label' =>__l('Body'), 'info' => __l('Available Variables: ##SITE_NAME##, ##SITE_URL##, ##ABOUT_US_URL##, ##CONTACT_US_URL##, ##FAQ_URL##, ##SITE_CONTACT_PHONE##, ##SITE_CONTACT_EMAIL##')));                
                echo $form->input('description_meta_tag',array('label' => 'Descripci&oacute;n [Meta Tag]'));
                echo $form->input('slug',array('label' => __l('Slug'),'info' => __l('If you change value of this field then don\'t forget to update links created for this page. It should be page/value of this field.')));
				?>
            
            <script language="JavaScript">
                $(document).ready(function()
                {
                    $("#previsualizar").click(function ()
                    { 
                        $.post("<?php echo Router::url('/', true); ?>ciudad-de-buenos-aires/pages/preview/", $("#PageEditForm").serialize(),
                            function( data )
                            {
                                $( "#result" ).empty().append( data );
                                 $.scrollTo('1100', 800); 
                            }
                        );
                            return false;
                    });
                });
            </script>
            
            <?php echo $form->submit('Actualizar',         array('name' => 'data[Page][Update]')); ?>
            <br />
            <?php echo $html->link  ('Previsualizar', '#', array('class'=>'link-button-generic', 'id'=>'previsualizar')); ?>
            <br />
            <?php echo $html->link  ('Cancelar',      'index', array('class'=>'link-button-generic')); ?>
            <br />
          	<?php echo $form->end(); ?>
        </fieldset>
    </div>
        
    <!-- output -->
    <div id="result"></div>
    
</div>
<?php
    if(!empty($page)):
    ?>
    </div> <!-- js-tabs end -->
    <?php
endif;
?>