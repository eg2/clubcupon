<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix pages-container">
        <h1 class="h1-format"><?php echo $page['Page']['title']; ?></h1>
            
            <!-- Contenidos -->

            <?php 

              echo $page['Page']['content'];
              
              if($page ['Page']['is_disclaimer'] || $page['Page']['slug']=='concurso')
              {
                 echo $this->element('links_disclaimer');
              }
            ?>
        
            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>