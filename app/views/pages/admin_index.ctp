<style>
  .add-block1 {
    float: right;
    padding: 15px 0;
    text-align: right;
    width: 40%;
}

div.submit{
  float: left;
}
</style>
<div class="pages index">
    <h2>
        <?php echo 'P&aacute;ginas';?>
    </h2>
    
    <div class="clearfix add-block1">
        <?php echo $html->link('Crear p&aacute;gina', array('controller' => 'pages', 'action' => 'add'), array('escape'=>false, 'class' => 'add','title' => 'Crear p&aacute;gina')); ?>
    </div>
    <div class="clearfix" style="float: left;">
        <?php echo $form->create("Page",array('action' => 'admin_index')); ?>
        <?php echo $form->input('title', array('label' => 'Buscar Página')); ?>
        <?php $submit_options    = array(
            'id'    => 'buscar',
            'label' => 'Buscar!',
            ); ?>
        <?php echo $form->end($submit_options); ?>
    </div>
    <div class="staticpage index">
        <?php echo $this->element('paging_counter');?>
        <table class="list">
            <tr>
                <th class="dl">
                    <?php echo $paginator->sort('Titulo','title');?>
                </th>
                <th class="dl" style="text-align:center;">
                    Legales?
                </th>
                <th class="dl" style="text-align:center;">
                    Estado
                </th>
                <th class="dl">
                    <?php echo $paginator->sort('Contenido','content');?>
                </th>
                <th class="dl">
                    URL
                </th>
                <th class="dl" style="padding-left: 15px;">
                    Acci&oacute;n
                </th>
            </tr>
            
            <?php
            
            $output  ='';
            
            if (!empty($pages))
            {
                $i = 0;
                foreach ($pages as $page)
                {
                    $edit     = $html->link("Editar",	array('action' => 'edit',      $page ['Page']['id']), array('class'=>'link-button-generic'));
                    $delete   = $html->link("Borrar", array('action' => 'delete',    $page ['Page']['id']), array('class'=>'js-confirm-action-link link-button-generic'));
                    $pageLink = Router::url('/', true)."pages/".$page['Page']['slug'];
                    
                    $class = null;
                    if ($i++ % 2 == 0)
                    {
                        $class = ' class="altrow"';
                    }
                    $output .= "<tr " . $class . ">";
                    $output .= "<td class=\"dl\">";
                    $output .= $html->cText($page['Page']['title']);
                    $output .= "</td>";
                    
                    //columna 'is_disclaimer'
                    $output .= "<td class=\"dl\" style=\"text-align:center;\">";
                    if($page ['Page']['is_disclaimer'] == 1)
                    {
                        //le usamos los iconos a banner ;)
                        $pageStatus = 'Bases y condiciones';
                        $icon = 'Banner/icon-legal.png';
                    }
                    else
                    {
                        $pageStatus = 'Pagina normal';
                        $icon = 'Banner/icon-nolegal.png';
                    }
                    $output  .= $html->image($icon, array('alt' => $pageStatus));
                    $output .= "</td>";
                    //
                    
                    //Columna 'draft'
                    $output .= "<td class=\"dl\" style=\"text-align:center;\">";
                    if($page ['Page']['draft'] == 1)
                    {
                        //le usamos los iconos a banner ;)
                        $pageStatus = 'Publicado';
                        $icon = 'Banner/icon-published.png';
                    }
                    else
                    {
                        $pageStatus = 'Despublicado';
                        $icon = 'Banner/icon-unpublished.png';
                    }
                    $output  .= $html->image($icon, array('alt' => $pageStatus));
                    $output .= "</td>";
                    //
                    $output .= "<td class=\"dl\">";
                    $output .= $html->cText($html->truncate($html->cText($page['Page']['content'])));
                    $output .= "</td>";
                    $output .= "<td class=\"dl\">";
                    $output .= "<a href=\"". $pageLink ."\" target=\"_blank\" >" . $pageLink . "</a>";
                    $output .= "</td>";
                    $output .= "<td class=\"dl\">";
                    $output .= $edit;
                    $output .= "<br />";
                    $output .= $delete;
                    $output .= "</td>";
                    $output .= "</tr>";
                }
            }
            else
            {
                $output  .='<tr>';
                $output  .='<td colspan=\"3\" class=\"notice\" > No hay paginas cargadas</td>';
                $output  .='</tr>';
            }
            echo $output;
        ?>
       </table>

    <?php
    if (!empty($pages))
    {
        echo $this->element('paging_links');
    }
    ?>

    </div>
</div>