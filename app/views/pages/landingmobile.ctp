<div id="ads">
    <?php if(false): ?>
    <!-- e-planning v3 - Comienzo espacio ClubCupon _ Landing _ Top -->
    <script type="text/javascript" language="JavaScript1.1">
    <!--
    var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
    var cs = document.charset || document.characterSet;
    document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/Landing/Top?o=j&rnd=' + rnd + '&crs=' + cs + '"></scr' + 'ipt>');
    //-->
    </script>
    <noscript><a href="http://ads.e-planning.net/ei/3/9b30/Landing/Top?it=i&rnd=$RANDOM" target="_blank"><img width="950" height="50" alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/Landing/Top?o=i&rnd=$RANDOM" border=0></a></noscript>
    <!-- e-planning v3 - Fin espacio ClubCupon _ Landing _ Top -->
    <?php endif; ?>
</div>
<script>
	// perform JavaScript after the document is scriptable.
	$(function() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	$("ul.tabs").tabs("div.panes > div");
	});
</script>
<div id="contentGaleria" style=" vertical-align: middle;margin-left:auto;margin-right:auto;">	
    <div class="panes">
        <div class="main" style="display: block;" id='main1'><!--articulo app mobile-->
        	<div class="containerCol">
            	<div class="colUno">
                	<span class="titulo">Nunca m&aacute;s te pierdas<br />una oferta</span>
                    <p><span>&iexcl;Lleg&oacute; ClubCup&oacute;n a tu celular!</span><br/>La primera aplicaci&oacute;n m&oacute;vil<br/> para que lleves las ofertas con vos <br /> y accedas a las mismas cuando quieras</p>
                    <div class="contListaApp">
                    	<ul>
                        	<li><p>Compr&aacute; desde donde est&eacute;s</p></li>
                            <li><p>Canje&aacute; tus cupones sin imprimirlos mostrando el código desde tu celular</p></li>
                        </ul>
                    </div>
                    <p><span>Descarg&aacute; tu aplicaci&oacute;n ahora </span></p>
                </div>
                <div class="colDos">
                	<img src="/img/mobile/landing/app.jpg" alt="" title="" border="0" />
                </div>
            </div>
        </div><!--articulo app mobile-->
        <div class="main" style="display: none;" id='main2'><!--articulo Iphone-->
        	<div class="containerCol">
            	<div class="colUno">
                	<p>Aplicaci&oacute;n para:</p>
                	<span class="titulo">iPhone</span>                    
                    <div class="contListaGeneral">
                    	<ul>
                        	<li>
                            	<span class="spriteIcon sprOferta"></span>
                                <p>Acced&eacute; a ofertas imperdibles y aprovech&aacute; la posibilidad de administrar tus cupones desde tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprTime"></span>
                                <p>Ahorr&aacute; papel y tiempo: &iexcl;no imprimas un cup&oacute;n nunca m&aacute;s! Present&aacute; el cup&oacute;n directamente en tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprPlace"></span>
                                <p>Encontr&aacute; nuevos lugares: ofertas para comprar y usar hoy mismo en ClubCup&oacute;n YA.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprCheck"></span>
                                <p>Passbook enabled</p>
                            </li>
                        </ul>
                    </div>
                    <a href="https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8" target="_blank"><img src="/img/mobile/landing/botonApp.jpg" alt="" border="0" /></a> 
                </div>
                <div class="colDos padIphone">
                	<img src="/img/mobile/landing/iphone.jpg" alt="" title="" border="0" />
                    <img border="0" title="" alt="" src="/img/mobile/landing/qr.png" class="qrsprite">
                    <div id="wrapperIphone"><!--galeria-->
                        <div class="slider-wrapperI theme-default">
                            <div id="slider2" class="nivoSlider">
                                <img src="/img/mobile/landing/iphone/1.jpg" data-thumb="/img/mobile/landing/iphone/1.jpg" width ="163" alt="" />
                                <img src="/img/mobile/landing/iphone/2.jpg" data-thumb="/img/mobile/landing/iphone/2.jpg" width ="163" alt="" title="" />
                                <img src="/img/mobile/landing/iphone/3.jpg" data-thumb="/img/mobile/landing/iphone/3.jpg" width ="163" alt=""/>
                                <img src="/img/mobile/landing/iphone/4.jpg" data-thumb="/img/mobile/landing/iphone/4.jpg" width ="163" alt="" title="" />
                            </div>            
                        </div>
                    </div><!--galeria-->
                </div>
            </div>
        </div><!--articulo Iphone-->
        <div class="main" style="display: none;" id='main3'><!--articulo Android-->
        	<div class="containerCol">
            	<div class="colUno">
                	<p>Aplicaci&oacute;n para:</p>
                	<span class="titulo">Android</span>                    
                    <div class="contListaGeneral">
                    	<ul>
                        	<li>
                            	<span class="spriteIcon sprOferta"></span>
                                <p>Acced&eacute; a ofertas imperdibles y aprovech&aacute; la posibilidad de administrar tus cupones desde tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprTime"></span>
                                <p>Ahorr&aacute; papel y tiempo: &iexcl;no imprimas un cup&oacute;n nunca m&aacute;s! Present&aacute; el cup&oacute;n directamente en tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprPlace"></span>
                                <p>Encontr&aacute; nuevos lugares: ofertas para comprar y usar hoy mismo en ClubCup&oacute;n YA.</p>
                            </li>                            
                        </ul>
                    </div>
                    <a href="https://play.google.com/store/apps/details?id=com.cmd.clubcupon" target="_blank"><img src="/img/mobile/landing/botonG.jpg" alt="" border="0" /></a> 
                </div>
                <div class="colDos padIphone">
                	<img src="/img/mobile/landing/android.jpg" alt="" title="" border="0" /> 
                    <div id="wrapperAndroid"><!--galeria-->
                        <div class="slider-wrapperA theme-default">
                            <div id="slider2" class="nivoSlider">
                                <img src="/img/mobile/landing/Android/1.jpg" data-thumb="/img/mobile/landing/Android/1.jpg" alt="" />
                                <img src="/img/mobile/landing/Android/2.jpg" data-thumb="/img/mobile/landing/Android/2.jpg" alt="" title="" />
                                <img src="/img/mobile/landing/Android/3.jpg" data-thumb="/img/mobile/landing/Android/3.jpg" alt=""/>
                                <img src="/img/mobile/landing/Android/4.jpg" data-thumb="/img/mobile/landing/Android/4.jpg" alt="" title="" />
                            </div>            
                        </div>
                    </div><!--galeria-->                   
                </div>
            </div>
        </div><!--articulo Android-->
        <div class="main" style="display: none;" id='main4'><!--articulo para celulares-->
        	<div class="containerCol">
            	<div class="colUno">
                	<p>Aplicaci&oacute;n para:</p>
                	<span class="titulo">BlackBerry</span>                    
                    <div class="contListaGeneral">
                    	<ul>
                        	<li>
                            	<span class="spriteIcon sprOferta"></span>
                                <p>Acced&eacute; a ofertas imperdibles y aprovech&aacute; la posibilidad de administrar tus cupones desde tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprTime"></span>
                                <p>Ahorr&aacute; papel y tiempo: &iexcl;no imprimas un cup&oacute;n nunca m&aacute;s! Present&aacute; el cup&oacute;n directamente en tu celular.</p>
                            </li>
                            <li>
                            	<span class="spriteIcon sprPlace"></span>
                                <p>Encontr&aacute; nuevos lugares: ofertas para comprar y usar hoy mismo en ClubCup&oacute;n YA.</p>
                            </li>                            
                        </ul>
                    </div>
                    <a href="http://appworld.blackberry.com/webstore/content/24574893/?lang=es&countrycode=AR" target="_blank"><img src="/img/mobile/landing/botonB.jpg" alt="" border="0" /> </a>
                </div>
                <div class="colDos padIphone">
                	<img src="/img/mobile/landing/celular.jpg" alt="" title="" border="0" />
                </div>
            </div>
        </div><!--articulo para celulares-->
    </div>
    <ul class="tabs">
        <li><a id='tab1' onclick="tabs(this)" class="bordeIzq current" style="cursor:pointer;"><span class="sprite"></span> App Mobile</a></li>
        <li><a id='tab2' onclick="tabs(this)" style="cursor:pointer;"><span class="sprite sprIphone"></span>iPhone · iPad</a></li>
        <li><a id='tab3' onclick="tabs(this)" style="cursor:pointer;"><span class="sprite sprAn"></span> Android</a></li>
        <li><a id='tab4' onclick="tabs(this)" class="bordeDer" style="cursor:pointer;"><span class="sprite sprBb"></span> BlackBerry</a></li>
    </ul>
</div>

<div id="ads">
    <?php if(false): ?>
    <!-- e-planning v3 - Comienzo espacio ClubCupon _ Landing _ Bottom -->
    <script type="text/javascript" language="JavaScript1.1">
    <!--
    var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
    var cs = document.charset || document.characterSet;
    document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/Landing/Bottom?o=j&rnd=' + rnd + '&crs=' + cs + '"></scr' + 'ipt>');
    //-->
    </script>
    <noscript><a href="http://ads.e-planning.net/ei/3/9b30/Landing/Bottom?it=i&rnd=$RANDOM" target="_blank"><img width="950" height="50" alt="Club Cupón" src="http://ads.e-planning.net/eb/3/9b30/Landing/Bottom?o=i&rnd=$RANDOM" border=0></a></noscript>
    <!-- e-planning v3 - Fin espacio ClubCupon _ Landing _ Bottom -->
    <?php endif; ?>
</div>


<script type="text/javascript">
	$(window).load(function() {
		$('#slider, #slider2, #slider3').nivoSlider();
	});
</script>

