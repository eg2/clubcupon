<script>
    $(document).ready(function() {
        $(function(){
            document.body.style.overflow = 'hidden';
            positionFooter();
            function positionFooter(){
                $("#footer").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#footer").height()-$("#header").height()+6)+"px"})
            }
            //Capturamos los eventos de ventana
            $(window)
                .scroll(positionFooter)
                .resize(positionFooter)
        });
    });
</script>
<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h1 class="h1-format">P&Aacute;GINA NO ENCONTRADA</h1>

            <!-- Contenidos -->

            <p>La p&aacute;gina solicitada no se encuentra disponible.</p>
            <p>
                <?php echo $html->link('Pod&eacute;s ver ofertas disponibles!', array('plugin'=>'',  'controller' => 'deals', 'action' => 'index'), array('id' => 'bt_volver_big', 'title' => 'VOLVER', 'escape'=>false)); ?>
            </p>

            <!-- / Contenidos -->

        </div>
    </div>
</div>
