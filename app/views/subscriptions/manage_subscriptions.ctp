<!--[if IE ]>        
    <style>
        .blqform-izq label{padding-top:5px; width:310px!important;}
        .line {
            list-style-image: none;
            list-style-type: none;
        }
    </style>
<![endif]-->
<!-- Contenidos -->
<div id="main" class="container_16 main_frt">

    <div class="grid_16 conten-suscripciones">
		<h2>Gestioná tus Suscripciones</h2>
		<h3>Elegí los <span>newsletters</span> que quieras recibir en tu <strong>mail.</strong></h3>
		<?php echo $form->create('Subscription', array('url'=>array('action'=>'manage_subscriptions'),'class' => 'normal')); ?>
			<div class="blqform-izq">
				<h4>Ciudades</h4>
                
                <?php
                    

                    foreach ($active_cities as $city)
                    {

                        echo $form->input($city['City']['id'], 
                            array( 
                                //'div'=>false, 
                                'label'     => $city['City']['name'],
                                'type'      => 'checkbox', 
                                'before'    => '<li class="line clearfix">', 
                                'after'     => '</li>', 
                                'value'     => $city['Subscription']['id'],
                                'checked'   => $city['City']['checked'],
                                ) 
                        ); 
                    }
                ?>
                
			</div>
			
			<div class="blqform-der">
				<h4>Ofertas Especiales</h4>
                <?php
                    

                    foreach ($active_groups as $city)
                    {

                        echo $form->input($city['City']['id'], 
                            array( 
                                //'div'=>false, 
                                'label'     => $city['City']['name'],
                                'type'      => 'checkbox', 
                                'before'    => '<li class="line clearfix">', 
                                'after'     => '</li>', 
                                'value'     => $city['Subscription']['id'],
                                'checked'   => $city['City']['checked'],
                                ) 
                        ); 
                    }
                ?>
                
                
				
				
			</div>
            
            <div style="clear:both">&nbsp;</div>
        
            <?php
                echo $form->submit('Guardar Cambios', array('div'=>false, 'class'=>'btn-azul-d btn-naranja-submit'),array('title' => 'Actualizar', 'class'=>'btn-naranja'));
                echo $html->link  ('Cancelar',   array('controller'=>'deals','action' => 'index'),array('escape'=>false, 'class' => 'cancelar', 'title' => 'Uy, cambié de opinión'));
                echo $form->end();
            ?>
        
		
	</div>
    
</div>
