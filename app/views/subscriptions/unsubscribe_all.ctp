<script>
    $(document).ready(function() {
        $(function(){
            document.body.style.overflow = 'hidden';
            positionFooter(); 
            function positionFooter(){
                $("#footer").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#footer").height()-$("#header").height()+6)+"px"})	
            }
            //Capturamos los eventos de ventana
            $(window)
                .scroll(positionFooter)
                .resize(positionFooter)
        });
    });
</script>

<style>
    .cancel-button{
        font-size: 14px;
        padding-left: 15px;
        display: block;
        padding-top: 6px;
    }
    
    
</style>
<div id="main" class="container_16 main_frt">
    <div class="grid_16 conten-suscripciones">
        
        <h2>&iquest;Est&aacute;s seguro que te quer&eacute;s dar de baja de todas tus suscripciones?</h2>
            
        <br />
        <br />
        <br />
            <!-- Contenidos -->
            
            <?php
                echo $form->create('Subscription', array('url'=>array('action'=>'unsubscribe_all'),'class' => 'normal'));
                
                echo $form->input('id',array('type' => 'hidden'));
                
                echo $form->submit('Darme de baja', array('title' => 'Darme de baja', 'class'=>'btn-naranja btn-naranja-submit'));
                echo $html->link('&iquest;Cambiaste de opini&oacute;n? Mir&aacute; estas Ofertas que te pueden interesar.', array('controller'=>'deals','action' => 'index','city'=>$city_slug),array('escape'=>false, 'class' => 'cancel-button', 'title' => 'Mirá estas Ofertas que te pueden interesar.'));
                echo $form->end();
                
            ?>
            
            <!-- / Contenidos -->

        
    </div>
</div>
