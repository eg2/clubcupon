<div class="subscriptions form">
<?php echo $form->create('Subscription', array('class' => 'normal'));?>
	<div>
 		<h2><?php echo __l('Add Subscription');?></h2>
    </div>
    <div>
    	<?php
    		echo $form->input('user_id',array('label' => __l('User')));
    		echo $form->input('city_id',array('label' => __l('City')));
    		echo $form->input('email',array('label' => __l('Email')));
    		echo $form->input('campaign',array('label' => 'Campaña'));
    	?>
	</div>
    <?php echo $form->end(__l('Add'));?>
</div>
