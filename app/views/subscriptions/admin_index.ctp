<?php if(empty($this->params['isAjax'])): ?>
<h2><?php echo $pageTitle;?></h2>
	<div class="js-tabs">
        <ul class="clearfix">
            <li><?php echo $html->link('Suscriptos ', array('controller' => 'subscriptions', 'action' => 'index', 'type' => 'subscribed'),array('title' => __l('Subscribed'))); ?></li>
            <li><?php echo $html->link('No suscriptos', array('controller' => 'subscriptions', 'action' => 'index', 'type' => 'unsubscribed'),array('title' => __l('Unsubscribed'))); ?></li>
            <li><?php $total = $subscribed + $unsubscribed; echo $html->link('Todos', array('controller' => 'subscriptions', 'action' => 'index'),array('title' => __l('All'))); ?></li>
        </ul>
    </div>
<?php else: ?>
	<div class="subscriptions index js-response js-responses">
	<?php echo $form->create('Subscription', array('type' => 'post', 'class' => 'normal search-form clearfix js-ajax-form', 'action'=>'index')); ?>

                <?php echo $form->input('q', array('label' => __l('Keyword'))); ?>
                <?php echo $form->input('type', array('type' => 'hidden')); ?>
                   <div class="input text">
                   <?php
                      $city_opts = array ();
                      foreach ($clubcupon->activeAllowedCitiesWithDeals ($auth->user('id')) as $acwd)
                          $city_opts [$acwd ['City']['id']] = $acwd ['City']['name'];
                      echo $form->label ('city_id', 'Ciudad');
                      echo $form->select ('city_id', $city_opts);
                   ?>
                   </div>
                <?php echo $form->submit(__l('Search'));?>
          <?php echo $form->end(); ?>
		  <div class="add-block1">
		  <?php if(!empty($subscriptions)) {?>
		  <?php echo $html->link(__l('CSV'), array('controller' => 'subscriptions', 'action' => 'export'.$param_string, 'ext' => 'csv'), array('class' => 'export', 'title' => __l('CSV Export'), 'escape' => false)); ?>
		  <?php } ?>
		  </div>
  <?php
     echo $form->create('Subscription' , array('class' => 'normal','action' => 'update'));
?>
  <?php echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url'])); ?>
  <?php echo $this->element('paging_counter'); ?>
  <table class="list">
    <tr>
      <th>
        <?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
          <?php echo __l('Select'); ?>
        <?php endif; ?>
      </th>
      <th><div class="js-pagination"><?php echo $paginator->sort('Suscriptos el','Subscription.created'); ?></div></th>
      <th class="dl"><div class="js-pagination"><?php echo $paginator->sort('Email','Subscription.email'); ?></div></th>
      <th class="dl"><div class="js-pagination"><?php echo $paginator->sort('Localidad','City.name'); ?></div></th>
      <th class="dl"><div class="js-pagination"><?php echo $paginator->sort('Campa&ntilde;a','Subscription.campaign'); ?></div></th>
      <?php if(empty($this->params['named']['type'])) { ?>
        <th><div class="js-pagination"><?php echo $paginator->sort('Suscripto','Subscription.is_subscribed'); ?></div></th>
      <?php } ?>
    </tr>
    <?php
if (!empty($subscriptions)):
$i = 0;
foreach ($subscriptions as $subscription):
	$class = null;
	if ($i++ % 2 == 0):
		$class = ' class="altrow"';
	endif;
    if($subscription['Subscription']['is_subscribed']):
        $status_class = 'js-checkbox-active';
    else:
        $status_class = 'js-checkbox-inactive';
    endif;
	$online_class = 'offline';
	if (!empty($user['CkSession']['user_id'])) {
		$online_class = 'online';
	}
?>
    <tr<?php echo $class;?>>
      <td>
        <?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
          <div class="actions-block">
            <div class="actions round-5-left">
                <span><?php echo $html->link('Borrar', array('action'=>'delete', $subscription['Subscription']['id']), array('class' => 'delete js-delete', 'title' => __l('Delete')));?></span>
            </div>
          </div>
          <?php echo $form->input('Subscription.'.$subscription['Subscription']['id'].'.id', array('type' => 'checkbox', 'id' => "admin_checkbox_".$subscription['Subscription']['id'], 'label' => false, 'class' =>$status_class.' js-checkbox-list', $online_class.' js-checkbox-list')); ?>
        <?php endif; ?>
      </td>
      <td><?php echo $html->cDateTime($subscription['Subscription']['created']);?></td>
      <td class="dl"><?php echo $html->cText($subscription['Subscription']['email']);?></td>
      <td class="dl"><?php echo $html->cText($subscription['City']['name']);?></td>
      <td class="dl"><?php echo $html->cText($subscription['Subscription']['campaign']);?></td>
      <?php if(empty($this->params['named']['type'])) { ?>
        <td><?php echo $html->cBool($subscription['Subscription']['is_subscribed']);?></td>
      <?php } ?>
    </tr>
    <?php
    endforeach;
else:
?>
    <tr>
      <td colspan="14" class="notice"><?php echo __l('No Subscriptions available');?></td>
    </tr>
    <?php
endif;
?>
  </table>
  <?php
if (!empty($subscriptions)):
?>
  <div class="js-pagination"> <?php echo $this->element('paging_links'); ?> </div>
       <?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
             <div class="admin-select-block">
          <div>
            <?php echo __l('Select:'); ?>
            <?php echo $html->link(__l('All'), '#', array('class' => 'js-admin-select-all', 'title' => __l('All'))); ?>
            <?php echo $html->link(__l('None'), '#', array('class' => 'js-admin-select-none', 'title' => __l('None'))); ?>
            <?php if(!isset($this->params['named']['type'])) { ?>
                        <?php echo $html->link(__l('Subscribed'), '#', array('class' => 'js-admin-select-approved', 'title' => __l('Subscribed'))); ?>
                        <?php echo $html->link(__l('Unsubscribed'), '#', array('class' => 'js-admin-select-pending', 'title' => __l('Unsubscribed'))); ?>
                <?php } ?>
          </div>

          <div class="admin-checkbox-button"><?php echo $form->input('more_action_id', array('class' => 'js-admin-index-autosubmit', 'label' => false, 'empty' => __l('-- More actions --'))); ?></div>
          </div>
      <?php endif; ?>
  <?php endif; ?>
  <?php echo $form->end(); ?>
</div>
<?php endif; ?>