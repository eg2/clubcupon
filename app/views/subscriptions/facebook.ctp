<script type='text/javascript'>  
$(document).ready(function() {

        // Additional JS functions here
        window.fbAsyncInit = function(){
            FB.init({
                appId      : __cfg('api_key'), // App ID
                channelUrl : '<?php echo STATIC_DOMAIN_FOR_CLUBCUPON; ?>', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : true  // parse XFBML
            });


        };

        function doFacebookSuscribe(){
            FB.login(function(response){

                //this also work if(response.status == 'connected')
                if (response.authResponse){
                    //get city form
                    city_id = $('#SubscriptionCityId').val();
                    // connected
                    var goTo  = '/subscriptions/facebook';
                    goTo     += '?city_id='+city_id
                    window.location.href = goTo;  
                } else {
                   console.log('falla en la conexion.');
                }
            }, {scope:'email'});
        }

         // Load the SDK Asynchronously
        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[1];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
        
        //Disparamos la conexion
        $('#facebookSuscribeButton').click(function(){doFacebookSuscribe();});  
}); 
</script>
<!--[if IE]>
	<style>
            #cc-popup form span input.comprar{ height:27px!important; padding:0!important; font-size:22px!important;}
	</style>
<![endif]-->

<!--[if IE 7]>
	<style>
            #cc-popup FORM SPAN INPUT {padding:3px 1px;}
	</style>
<![endif]-->

<!--[if IE 8]>
	<style>
            #cc-popup form span select{padding:2px 5px!important;}
            .mail, select{margin-top:10px!important; padding:2px 5px;}
            #cc-popup FORM SPAN INPUT {padding:4px 0 3px 10px;}
            /*#cc-popup form span input.comprar{margin-top:8px!important}*/
            #cc-popup form span input.comprar{height: 45px !important; margin-top: 0px !important; display: block!important;}
           
	</style>
<![endif]-->
<style>
 #cc-popup form {
              background: -moz-linear-gradient(center top , #198FB7 0%, #0E7394 100%) repeat scroll 0 0 transparent;
              border-radius: 5px 5px 5px 5px;
              bottom: 20px !important;
              height: 95px;
              left: 17px;
              position: absolute;
              width: 650px;
            }
#cc-popup form span {
    display: block;
    margin: 25px 100px 50px 90px;
    width: 620px;
}
#cc-popup form span input.comprar {
    background: url("/img/pre/theme_clean/bg-suscribe-fb.png") scroll transparent !important;
}
</style>
<div class="bloque-pop clearfix">
  <div id="cc-popup">
    <img src="/img/pre/theme_clean/bgPopUp_facebook.jpg" alt="alter" class="backimg"/>
    <?php echo $form->create('Subscription', array('url' => '#', 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top', 'name'=>'myform')); ?>

			<span class="clearfix">

                                <?php if(isset($_GET['utm_source'])){?>
                                    <?php echo $form->hidden('utm_source', array('value' => $_GET['utm_source'])); ?>
                                <?php }?>

                                <?php if(isset($_GET['utm_medium'])){?>
                                <?php echo $form->hidden('utm_medium', array('value' => $_GET['utm_medium'])); ?>
                                <?php }?>

                                <?php if(isset($_GET['utm_term'])){?>
                                    <?php echo $form->hidden('utm_term', array('value' => $_GET['utm_term'])); ?>
                                <?php }?>

                                <?php if(isset($_GET['utm_content'])){?>
                                    <?php echo $form->hidden('utm_content', array('value' => $_GET['utm_content'])); ?>
                                <?php }?>

                                <?php if(isset($_GET['utm_campaign'])){?>
                                    <?php echo $form->hidden('utm_campaign', array('value' => $_GET['utm_campaign'])); ?>
                                <?php }?>
                                    <?php
                                    $options = array();
                                     foreach ($clubcupon->activeCitiesWithDeals () as $city){
                                     $options[$city ['City']['id']] = $city ['City']['name'];
                                     }
                                     echo $form->input('city_id', array( 'label' => false, 'empty' => false, 'selected' => Configure::read ('Actual.city_id'), 'options' => $options));
                                ?>
                                   <input id="facebookSuscribeButton"  class="comprar compraroff" type="button" value="" />
                                   <?php  echo $form->end(); ?> 
                                   
                                
			</span>
   </div>
</div>