<?php
if (preg_match('/subscribe/s', $this->params['url']['url']) || preg_match('/subscribeadd/s', $this->params['url']['url'])) {
  echo '<div id="content" class="subscribe box">';
  echo '<h2 class="title">Suscribite</h2>';
  echo '<p>';
  echo sprintf(__l('Every day, %s mails you one exclusive offer to do, see, taste, or experience something amazing in Sunshine Coast at an unbeatable price.'), Configure::read('site.name'));
  echo '<br /><br />';
  echo 'Registrate gratis ahora, y preparate para disfrutar de los mejores descuentos!';
  echo '</p>';
  echo $form->create('Subscription', array('class' => 'normal', 'url' => array('controller' => 'subscriptions', 'action' => 'add')));
  echo $form->input('email', array('label' => 'Ingresá tu mail'));
  echo '<small>No divulgaremos tu direcci&oacute;n de correo.</small>';
  echo $form->input('city_id', array('label' => __l('Choose your city:'), 'options' => $cities));
  echo $form->input('campaign', array ('type' => 'hidden', 'value' => $campaign));
  echo $form->submit(' ', array('class' => 'bt_suscribirme'));
  echo $form->end();
  echo '</div>';
  echo $this->element('sidebar');
} else {
  echo $form->create('Subscription');
  $this->data['Subscription']['city_id'] = $city_id;
  echo $form->input('email', array('label' => '', 'onclick' => '$(this).val() == \'Ingresá tu mail\' ? $(this).val(\'\') : \'\';', 'onblur' => '$(this).val() == \'\' ? $(this).val(\'Ingresá tu mail\') : \'\';', 'value' => 'Ingresá tu mail'));
  echo $form->input('city_id', array('type' => 'hidden', 'value' => $this->data['Subscription']['city_id']));
  echo $form->input('campaign', array ('type' => 'hidden', 'value' => $campaign));
  echo $form->submit(' ', array('class' => 'bt_suscribirme'));
  echo $form->end();
}
?>
