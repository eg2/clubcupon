
<div id="pagingLinks">
  <ul>
  <?php
	$paramsSearch=isset($paramsSearch)?$paramsSearch:'';
	?>
    <li>
      <?php echo str_replace('/companies_for_admin_index', $paramsSearch, str_replace($paramsSearch, '', $paginator->prev('< Anterior ', array('escape' => false, 'class' => 'pagLinkArrow'), $campaign['ProductCampaign']['id'], array('class'=>'disabled')))); ?>
    </li>
    <li>
      <?php echo str_replace('/companies_for_admin_index', $paramsSearch, str_replace($paramsSearch, '', $paginator->numbers(array('class'=>'pagLink')) ) ); ?>
    </li>
    <li>
      <?php echo str_replace('/companies_for_admin_index', $paramsSearch, str_replace($paramsSearch, '', $paginator->next(' Siguiente >', array('escape' => false, 'class' => 'pagLinkArrow'), $campaign['ProductCampaign']['id'], array('class'=>'disabled')))); ?>
    </li>
  </ul>
</div>
