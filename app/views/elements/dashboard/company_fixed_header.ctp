<style>
  div .input {
    width: 250px;
  }
  .company-fixed-header{
  	padding: 10px 5px 5px;
  }
  .company-fixed-header .content{
  	height: 125px; padding: 10px;
  	border: 1px solid #DDDDDD;
    border-radius: 5px;
    box-shadow: 1px 1px 1px #DDDDDD;
  }
  .company-fixed-header .content .valida-cupon{
    float: left;
    width: 50%;
  }
 .company-fixed-header .content .proxima-liquidacion{
    float: left;
    width: 50%;
  }
  .company-fixed-header .content .proxima-liquidacion .title{
 	width: 60%; float: left;
  }
  
  .company-fixed-header .content .proxima-liquidacion .date{
    width: 35%; float: left;
    display: inline;
    margin-top: 8px;
  }
/*   .company-fixed-header .btn-azul-d { */
/*     float: none; */
/*     background: url("/img/now/landing-img/bg-btn.jpg") repeat-x scroll 0 0 #109AC8; */
/*     border-radius: 5px; */
/*     color: #FFFFFF; */
/*     /* display: block; */ */
/*     float: right; */
/*     font: bold 13px arial; */
/*     margin: 0 100px 0 0; */
/*     padding: 4px 10px; */
/*     text-align: center; */
/*   } */
  .company-fixed-header h2{
    display: inline;
    color: #000000;
    margin: 20px 0 0;
    font: bold 19px arial;
  }
  .company-fixed-header label{
    display: block;
    margin-bottom: 5px;
    font-size: 14px;
    font-weight: normal;
    line-height: 20px;
    font-family: arial;
  }
  
  .company-fixed-header input[type="text"]{
  
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
   
    border-radius: 4px;
    color: #555555;
    display: inline-block;
    font-size: 14px;
    height: 20px;
    line-height: 20px;
    margin-bottom: 10px;
    padding: 4px 6px;
    vertical-align: middle;
    
    margin-left: 0;
    
    width: 246px;
    
    
  }
  .company-fixed-header input[type="text"]:focus{
  
   	border-color: rgba(82, 168, 236, 0.8);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(82, 168, 236, 0.6);
    outline: 0 none;
   
  }
</style>


<div class="company-fixed-header">
  <div class="content">
	<div class="valida-cupon">
  		<h2 style="display:inline">Redim&iacute; tu cup&oacute;n</h2>
  		<div class="redimir_cupon">
          <?php
// array('url' => Router::url('/', true) . 'redemptions/redeemcoupon'
          echo $form->create('Cupon',  array('url' => Router::url('/', true) . 'redemptions/redeemcoupon', 'id' => 'coupon-redeemption-form'));
          /*echo $form->create('Cupon',array(
            'inputDefaults' => array(
                'div' => 'control-group',
                'label' => array('class' => 'control-label'),
                'between' => '<div class="controls">',
                'after' => '</div>',
                'class' => '',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-error'))),
            'class' => 'form-horizontal'));
            */
            echo '<div style="float:left;padding-top:15px"><div style="float:left">';
            echo $form->input ('codigo',          array('label' => 'Ingrese los 18 d&iacute;gitos del C&oacute;digo Posnet:&nbsp','style' => 'margin-top:2px', 'div'=>false, 'id' => 'coupon-redeemption-code'));
            //echo '</div>';
            
          ?>
          &nbsp;
           <!-- div class="submit-block" style="padding-top:20px"--> 
            <?php echo $form->button('Redimir',array('style' => 'float:right','class' => 'btn-azul-d', 'div'=>false, 'id'=>'coupon-redeemption-button')); ?>
           <!-- /div-->
           <?php  echo '</div>';
           
           
           ?> 
            <p "coupon-redeemption-shorcut-message" class="message"></p>
            <div id="output"></div>
       </div>
  		<?php echo $form->end(); ?>
	</div>  
 </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		//$('form').unbind('submit');
		$("#coupon-redeemption-button").click(function() {
      $.value = $("#coupon-redeemption-code").attr('value');
      $("#output").html('');

        var request = $.ajax({
          url : '<?php echo Router::url('/', true); ?>redemptions/redeemcoupon',
            type: "POST",
            data: {posnet_code: $.value, only_posnet_code: 1},
            dataType: "html",
            success: function(response){
              $("#output").html(response);
              try {
                result = JSON.parse(response);
                if(result.status==1) {
                    $("#output").css('color', 'green');
                    $("#output").html(result.message);
                } else {
                    $("#output").css('color', 'red');
                    $("#output").html(result.message);
                }
              } catch (e) {
                $("#output").css('color', 'red');
                $("#output").html('Error al redimir');
              }
              $("#output").slideDown(100).delay(100000).slideUp(9000);
              }
         });
      });
    });

</script>
	
