<!-- UL desplegable Cuenta nuevo -->
    <ul>
        <?php
            foreach($menuOptions as $menuOption ) {

                
                if($menuOption['type'] == 'link') {
                    echo '<li>' . $html->link (
                                $menuOption['label'],
                                    array (
                                    'plugin'        => $menuOption['plugin'],
                                    'controller'    => $menuOption['controller'],
                                    'action'        => $menuOption['action'],
                                    'admin'         => $menuOption['admin'],
                                    $menuOption['parameters'],
                                    ),
                                    array(
                                        'title'     => $menuOption['label'],
                                        'class'     => $menuOption['format']['class'],
                                    )
                                ) . '</li>';
                }
                if($menuOption['type'] == 'externalLink') {
                    echo '<li class="'.$menuOption['format']['class'].'">' . $html->link (
                                                                        $menuOption['label'],
                                                                        $menuOption['url'],
                                                                        array(
                                                                            'target' => $menuOption['target'],
                                                                            'title' => $menuOption['title']),null,false) .'</li>';
                }
                
                if($menuOption['type'] == 'string') {
                    echo '<li class="'.$menuOption['format']['class'].'">'.$menuOption['string'].'</li>';
                }

            }
        ?>
        
        <!--<li><?php echo $html->link ('Ayuda', Configure::read('ayuda.comercios'), array('target'=>'_blank','title' => 'Centro de atención al comercio'),null,false); ?> </li>-->
        
        <li>
            <?php 
                echo $html->link('Salir', array(
                    'plugin' => '',
                    'controller' => 'users',
                    'action' => 'logout',
                    'admin' => false,
                    'parameters' => '',
                    ),
                    array( 'title' => '', 'class' => 'item-salir'));
            ?>
        </li>
        
        <?php /* 
        <li><a href="#" class="closeMenu menu-js-off" title="Cerrar Menú">Cerrar Men&uacute;</a></li>
        */?>
    </ul>
<!-- FIN UL desplegable Cuenta -->