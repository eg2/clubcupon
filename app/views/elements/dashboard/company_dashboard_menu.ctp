<div class="navbar">
    <div class="navbar-inner">
        <ul class="nav">
            <li class="<?php echo $section == 'index' ? 'active' :''; ?>">
                <?php echo $html->link('Inicio', array('controller' => 'dashboards', 'action' => 'index', 'admin' => true), array('escape'=>false, 'title' => 'Inicio')); ?>
            </li>
            <li class="<?php echo $section == 'company_details' ? 'active' :''; ?>">
                <?php echo $html->link('Datos de empresa', array('controller' => 'dashboards', 'action' => 'company_details', 'admin' => true), array('escape'=>false, 'title' => 'Datos de empresa')); ?>
            </li>
            <li class="<?php echo $section == 'company_branches' ? 'active' :''; ?>">
                <?php echo $html->link('Sucursales', array('controller' => 'dashboards', 'action' => 'company_branches', 'admin' => true), array('escape'=>false, 'title' => 'Sucursales')); ?>
            </li>
            <li class="<?php echo $section == 'company_deals' ? 'active' :''; ?>">
                <?php echo $html->link('Ofertas', array('controller' => 'dashboards', 'action' => 'company_deals', 'admin' => true), array('escape'=>false, 'title' => 'Ofertas')); ?>
            </li>
            <li class="<?php echo $section == 'company_coupons' ? 'active' :''; ?>">
                <?php echo $html->link('Cupones', array('controller' => 'dashboards', 'action' => 'company_coupons', 'admin' => true), array('escape'=>false, 'title' => 'Cupones')); ?>
            </li>
            <li class="<?php echo $section == 'company_liquidations' ? 'active' :''; ?>">
                <?php echo $html->link('Liquidaciones', array('controller' => 'dashboards', 'action' => 'company_liquidations_tabs', 'admin' => true), array('escape'=>false, 'title' => 'Liquidaciones')); ?>
            </li>
            <li>
        </ul>
    </div>
</div>