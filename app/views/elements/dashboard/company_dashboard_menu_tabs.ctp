<?php if (($showLiquidationsHeader) || $this->params['controller']=='dashboards' ) { ?>
<?php echo $this->element('dashboard/company_fixed_header'); ?>
<?php } ?>
<div id="liquidationsAlternativeMenu">
<?php 


?>
    <ul class="nav nav-tabs">
        <?php
            foreach($menuOptions as $menuOption ) {
                
                if($menuOption['type'] == 'link') {
               
                    $class = ( $this->params['action'] == $menuOption['action'] && $this->params['controller'] == $menuOption['controller'] ) || 
                             ( $this->params['action'] == $menuOption['action']  && $this->params['controller'] == 'deals' )  ||
                             ( $this->params['action'] == $menuOption['action']  && $this->params['controller'] == 'dashboards' ) ||
                             ( $this->params['action'] == 'add' && $menuOption['label'] == 'Mis Sucursales') ||
                             ( $this->params['action'] == 'save' && $menuOption['label'] == 'Mis Ofertas') ||
                             ( $this->params['action'] == 'change_password' && $menuOption['label'] == 'Mi Empresa' ) ? 'active' : '';
                             //BORRAR :: Buscar un mejor metodo para analizar los alias, esto es muy grasa ^... ya está hecho, cambiarlo ahora no sirve
                
                    echo '<li class='.$class.'>' . $html->link (
                        $menuOption['label'],
                            array (
                            'plugin'        => $menuOption['plugin'],
                            'controller'    => $menuOption['controller'],
                            'action'        => $menuOption['action'],
                            'admin'         => $menuOption['admin'],
                            $menuOption['parameters'],
                            ),
                            array(
                                'title'     => $menuOption['label'],
                                'class'     => $menuOption['format']['class'],
                            )
                        ) . '</li>';

                }
            }
        ?>
        
        <li class="modcuenta pull-right">
            <a href="http://comercios.clubcupon.com.ar/" target="_blank" title="Centro de Atencion al Comercio" style="font-weight:bold; font-size:12px;">Centro de Atención al Comercio</a>
        </li>
        
    </ul>
</div>
