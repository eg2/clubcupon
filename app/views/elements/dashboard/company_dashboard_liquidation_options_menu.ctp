<style>
        .submenu{
            margin:5px auto;
            padding:5px;
            width:200px;
        }
        hr{
            margin:5px ;
        }
            .submenu ul{ margin:0; padding:0;}
                .submenu ul li{
                    display:inline;
                    list-style: none;
                    color: #0098CA;
                    font: bold 13px arial;
                    text-transform: uppercase;
                    }
                    .submenu ul li:first-child{
                        margin-right:15px;
                        padding-right:15px;
                        border-right: 2px solid #333;
                    }
                    .submenu ul li a.active{
                        color:#333;
                        }
    </style>
    
    <div class="submenu">
        <ul>
            <?php
            
                if($this->params['action'] == 'company_campaign_liquidations') {
                    echo '<li style="color:#333;">Campa&ntilde;a</li>'; 
                } else {
                echo '<li class="">' . $html->link (
                        'Campa&ntilde;a',
                        array ( 'plugin'     => '',
                                'controller' => 'dashboards',
                                'action'     => 'company_campaign_liquidations',
                                'admin'      => false,
                                'parameters' => '',
                        ),
                        array(
                            'title'     => 'Ver por c&oacute;digo de campa&ntilde;a',
                            'escape' => false,
                            'class' => $class1
                        )
                    ) . '</li>';
                }
                if($this->params['action'] == 'company_calendar_liquidations') {
                echo '<li style="color:#333;">Liquidaciones</li>';    
                } else {
                
                echo '<li class="">' . $html->link (
                        'Liquidaciones',
                        array ( 'plugin'     => '',
                                'controller' => 'dashboards',
                                'action'     => 'company_calendar_liquidations',
                                'admin'      => false,
                                'parameters' => '',
                        ),
                        array(
                            'title'     => 'Ver por liquidaciones por calendario',
                            'class' => $class2
                        )
                    ) . '</li>';
                }
            ?>
        </ul>
    </div>
    <hr />