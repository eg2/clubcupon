<ul class="admin-links">

    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
        <?php $class = ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_stats') ? ' class="active"' : null; ?>
        <li <?php echo $class; ?>><?php echo $html->link(__l('Site Stats'), array('controller' => 'users', 'action' => 'stats'), array('title' => __l('Site Stats'))); ?></li>
    <?php } ?>

    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
        <li class = "no-bor" >
            <h4><?php echo 'Beacons'; ?></h4>
            <ul class = "admin-sub-links">
                <?php $class = ($this->params ['controller'] == 'beacon_devices') ? ' class="active"' : null; ?>
                <li><?php echo $html->link('Dispositivos', array('controller' => 'beacon_devices', 'action' => 'index'), array('title' => 'Dispositivos', 'escape' => false)); ?></li>
            </ul>
        </li>
    <?php } ?>

    <li class="no-bor">
        <h4><?php echo 'Slots'; ?></h4>
        <ul class="admin-sub-links">
            <li><?php echo $html->link('Administración', array('controller' => 'links', 'action' => 'index', '?' => array('linkType' => ConstSearchLinksTypes::Destacado)), array('title' => 'Administración')); ?></li>
            <li><?php echo $html->link('Copia', array('controller' => 'links', 'action' => 'copy'), array('title' => 'Copia')); ?></li>
        </ul>
    </li>

    <li class="no-bor">
        <h4><?php echo __l('Users'); ?></h4>
        <ul class="admin-sub-links">
            <?php $class = ($this->params['controller'] == 'user_profiles' || $this->params['controller'] == 'banned_ips' || ($this->params['controller'] == 'users' && ($this->params['action'] == 'admin_index' || $this->params['action'] == 'change_password' || $this->params['action'] == 'admin_add' )) ) ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link(__l('Users'), array('controller' => 'users', 'action' => 'index'), array('title' => __l('Users'))); ?></li>
<!--            <li <?php //echo $class;            ?>><?php //echo $html->link(__l('Clusters usuarios'), array('controller' => 'clusters', 'action' => 'index'), array('title' => __l('Cluster usuarios')));            ?></li>-->
            <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
                <?php $class = ( $this->params['controller'] == 'user_profiles') ? ' class="active"' : null; ?>
                <?php $class = ($this->params['controller'] == 'user_logins') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('User Logins'), array('controller' => 'user_logins', 'action' => 'index'), array('title' => __l('User Logins'))); ?></li>
            <?php } ?>
        </ul>
    </li>


    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
        <li class = "no-bor" >
            <h4><?php echo 'P&aacute;ginas & Banners'; ?></h4>
            <ul class = "admin-sub-links">
                <?php $class = ($this->params ['controller'] == 'pages') ? ' class="active"' : null; ?>
                <li><?php echo $html->link('P&aacute;ginas', array('controller' => 'pages', 'action' => 'index'), array('title' => 'P&aacute;ginas', 'escape' => false)); ?></li>
                <?php $class = ($this->params ['controller'] == 'banners') ? ' class="active"' : null; ?>
                <li><?php echo $html->link('Banners', array('controller' => 'banners', 'action' => 'index'), array('title' => __l('Banners'))); ?></li>
            </ul>
        </li>
    <?php } ?>

    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
        <li class = "no-bor" >
            <h4>Campa&ntilde;as</h4>
            <ul class = "admin-sub-links">
                <li><?php echo $html->link('Listar campa&ntilde;as', array('controller' => 'Campaigns', 'action' => 'index'), array('title' => 'Listar campa&ntilde;as', 'escape' => false)); ?></li>
                <li><?php echo $html->link('Crear campa&ntilde;a', array('controller' => 'Campaigns', 'action' => 'add'), array('title' => 'Crear campa&ntilde;a', 'escape' => false)); ?></li>
            </ul>
        </li>
    <?php } ?>

    <li class="no-bor">
        <h4>Calendarios</h4>
        <ul class="admin-sub-links">
            <?php $class = ($this->params['controller'] == 'accounting_calendars') ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link('Calendarios', array('controller' => 'accounting_calendars', 'action' => 'index'), array('title' => 'Calendarios de eventos')); ?></li>
        </ul>
    </li>

    <li class="no-bor">
        <h4>Newsletters</h4>
        <ul class="admin-sub-links">
            <?php $class = ($this->params['controller'] == 'newsletter_subjects') ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link('Asuntos', array('controller' => 'newsletter_subjects', 'action' => 'index'), array('title' => 'Asuntos del Newsletter')); ?></li>
        </ul>
    </li>

    <li class="no-bor">
        <h4><?php echo __l('Companies'); ?></h4>
        <ul class="admin-sub-links">
            <?php $class = ($this->params['controller'] == 'companies') ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link(__l('Companies'), array('controller' => 'companies', 'action' => 'index'), array('title' => __l('Companies'))); ?></li>
        </ul>
    </li>

    <?php
    //CCNow
    if (false && Configure::read('now.is_enabled') && ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) {
        ?>

        <li class="no-bor">
            <h4>Club Cup&oacute;n YA</h4>
            <ul class="admin-sub-links">
                <li><?php echo $html->link('Moderaci&oacute;n de Empresas', array('controller' => 'company_candidates', 'action' => 'index'), array('escape' => false)); ?></li>
                <li><?php echo $html->link('Moderaci&oacute;n de Ofertas', array('controller' => 'deals', 'action' => 'now_deals_management'), array('escape' => false)); ?></li>
            </ul>
        </li>

    <?php } ?>

    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
        <?php //$class = ($this->params['controller'] == 'user_cash_withdrawals') ? ' class="active"' : null; ?>
                                        <!--        <li <?php //echo $class;           ?>><?php //echo $html->link(__l('Withdraw Fund Requests'), array('controller' => 'user_cash_withdrawals', 'action' => 'index'), array('title' => __l('Withdraw Fund Request')));           ?></li>-->
        <?php $class = ($this->params['controller'] == 'transactions') ? ' class="active"' : null; ?>
        <li <?php echo $class; ?>><?php echo $html->link(__l('Transactions'), array('controller' => 'transactions', 'action' => 'index'), array('title' => __l('Transactions'))); ?></li>
        <?php $class = ($this->params['controller'] == 'translations') ? ' class="active"' : null; ?>
    <?php endif; ?>
    <?php $class = (($this->params['controller'] == 'deal_bulk_edits' ) && ($this->params['action'] == 'admin_index' )) ? ' class="active"' : null; ?>
    <li class="no-bor">
        <h4><?php echo __l('Deals'); ?></h4>
        <ul class="admin-sub-links">
            <li <?php echo $class; ?>>
                <?php echo $html->link('Administración', array('controller' => 'deal_bulk_edits', 'action' => 'index'), array('title' => 'Administración')); ?>
            </li>

            <?php $class = $this->params['controller'] == 'payment_options' ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>>
                <?php echo $html->link('Administración global de opciones de pago', array('controller' => 'payment_options', 'action' => 'assign'), array('title' => 'Administración global de opciones de pago')); ?>
            </li>

            <?php $class = ($this->params['controller'] == 'deals' && ($this->params['action'] == 'admin_index' || $this->params['action'] == 'admin_edit')) ? ' class="active"' : null; ?>

            <li <?php echo $class; ?>>
                <?php echo $html->link(__l('Deals'), array('controller' => 'deals', 'action' => 'index'), array('title' => __l('Deals'))); ?>
            </li>
            <!-- //// -->
            <li>
                <?php echo $html->link('Mandar Ofertas Venideras', array('controller' => 'deals', 'action' => 'admin_send_upcoming_deals'), array('title' => __l('Send Upcoming Deals'))); ?>
            </li>
            <!-- //// -->
            <?php $class = ($this->params['controller'] == 'deals' && $this->params['action'] == 'add') ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link('A&ntilde;adir Oferta', array('controller' => 'deals', 'action' => 'add'), array('escape' => false, 'title' => 'A&ntilde;adir Oferta')); ?></li>
            <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
                <?php $class = ($this->params ['controller'] == 'deal_categories') ? ' class = "active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link('Categorías', array('controller' => 'deal_categories', 'action' => 'index'), array('title' => 'Categorías de oferta')); ?></li>
            <?php } ?>
            <?php if (ConstUserTypes::isPrivilegedUserOrAdmin($auth->user('user_type_id'))): ?>
                <?php $class = ($this->params['controller'] == 'deal_users') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('Deal Coupons'), array('controller' => 'deal_users', 'action' => 'index'), array('title' => __l('Deal Coupons'))); ?></li>
            <?php endif; ?>
        </ul>
    </li>

    <li class="no-bor">
        <h4><?php echo __l('Subscriptions'); ?></h4>
        <ul class="admin-sub-links">
            <?php $class = ($this->params['controller'] == 'subscriptions') ? ' class="active"' : null; ?>
            <li <?php echo $class; ?>><?php echo $html->link(__l('Subscriptions'), array('controller' => 'subscriptions', 'action' => 'index'), array('title' => __l('Subscriptions'))); ?></li>
        </ul>
    </li>
    <?php if (ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))): ?>
        <?php $class = ($this->params['controller'] == 'gift_users') ? ' class="active"' : null; ?>
        <li <?php echo $class; ?>><?php echo $html->link(__l('Gift Cards'), array('controller' => 'gift_users', 'action' => 'index'), array('title' => __l('Gift Cards'))); ?></li>
        <?php $class = ($this->params['controller'] == 'city_suggestions') ? ' class="active"' : null; ?>
    <!--        <li <?php //echo $class;      ?>><?php //echo $html->link(__l('City Suggestions'), array('controller' => 'city_suggestions', 'action' => 'index'), array('title' => __l('City Suggestions')));      ?></li>-->
        <li class="no-bor">
            <h4><?php echo __l('Masters'); ?></h4>
            <ul class="admin-sub-links">
                <?php $class = ($this->params['controller'] == 'settings') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('Settings'), array('controller' => 'settings', 'action' => 'index'), array('title' => __l('Settings'))); ?></li>
                <!--                <?php //$class = ($this->params['controller'] == 'payment_gateways') ? ' class="active"' : null;    ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Payment Gateways'), array('controller' => 'payment_gateways', 'action' => 'index'), array('title' => __l('Payment Gateways')));    ?></li>
                <?php //$class = ($this->params['controller'] == 'email_templates') ? ' class="active"' : null; ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Email Templates'), array('controller' => 'email_templates', 'action' => 'index'), array('title' => __l('Email Templates')));    ?></li>
                <?php //$class = ($this->params['controller'] == 'transaction_types') ? ' class="active"' : null; ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Transaction Types'), array('controller' => 'transaction_types', 'action' => 'index'), array('title' => __l('Transaction Types')));    ?></li>
                <?php //$class = ($this->params['controller'] == 'payment_types') ? ' class="active"' : null; ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Payment Types'), array('controller' => 'payment_types', 'action' => 'index'), array('title' => __l('Payment Types')));    ?></li>
                <?php //$class = ($this->params['controller'] == 'translations') ? ' class="active"' : null; ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Translations'), array('controller' => 'translations', 'action' => 'index'), array('title' => __l('Translations')));    ?></li>
                <?php //$class = ($this->params['controller'] == 'languages') ? ' class="active"' : null; ?>
                                <li <?php //echo $class;    ?>><?php //echo $html->link(__l('Languages'), array('controller' => 'languages', 'action' => 'index'), array('title' => __l('Languages')));    ?></li>-->
                <?php $class = ($this->params['controller'] == 'banned_ips') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('Banned IPs'), array('controller' => 'banned_ips', 'action' => 'index'), array('title' => __l('Banned IPs'))); ?></li>
                <?php $class = ($this->params['controller'] == 'cities') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('Cities'), array('controller' => 'cities', 'action' => 'index'), array('title' => __l('Cities'))); ?></li>
                <?php $class = ($this->params['controller'] == 'states') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('States'), array('controller' => 'states', 'action' => 'index'), array('title' => __l('States'))); ?></li>
                <?php $class = ($this->params['controller'] == 'countries') ? ' class="active"' : null; ?>
                <li <?php echo $class; ?>><?php echo $html->link(__l('Countries'), array('controller' => 'countries', 'action' => 'index'), array('title' => __l('Countries'))); ?></li>
            </ul>
        </li>

        <!--        <li class = "no-bor">
                    <h4><?php //echo 'Puntos';      ?></h4>
                    <ul class = "admin-sub-links">
        <?php //$class = ($this->params ['controller'] == 'action_points') ? ' class = "active"' : null; ?>
                        <li <?php //echo $class;      ?>><?php //echo $html->link('Acciones', array('controller' => 'action_points', 'action' => 'index'), array('title' => 'Acciones'));      ?></li>
        <?php //$class = ($this->params ['controller'] == 'rates') ? ' class = "active"' : null; ?>
                        <li <?php //echo $class;      ?>><?php //echo $html->link('Conversi&oacute;n', array('controller' => 'rates', 'action' => 'index', 'admin' => 'true'), array('escape' => false, 'title' => 'Conversi&oacute;n'));      ?></li>
                    </ul>
                </li>-->
    <?php endif; ?>

    <!-- Posnet -->
    <?php if (false && ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))) { ?>
        <li class = "no-bor" >
            <h4>Redenci&oacute;n POSNET</h4>
            <ul class = "admin-sub-links">
                <li><?php echo $html->link('Importar', array('controller' => 'posnets', 'action' => 'import'), array('title' => 'Importar')); ?></li>
                <li><?php echo $html->link('Exportar', array('controller' => 'posnets', 'action' => 'export'), array('title' => 'Exportar')); ?></li>
            </ul>
        </li>
    <?php } ?>



</ul>
