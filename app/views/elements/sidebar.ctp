<?php
    if($is_corporate_city) {
        $tagPrefix = 'Corporate';
    } else {
        $tagPrefix = 'Ficha';
    }
?>

<div id="sidebar">
    <!--
    <div class="box cursorPointer ">

        <?php
        if($auth->sessionValid()) {
        echo $html->link($html->image('banner_invite.png', array('alt' => 'Suma $10 por cada amigo que traigas a ' . Configure::read('site.name'))), 'javascript:void(0)', array('onclick' => 'popupGeneric("' . Router::url(array('controller' => 'firsts', 'action' => 'recommend'), false) . '", 960, 440);', 'escape' => false, 'alt' => 'Suma $10 por cada amigo que traigas a ' . Configure::read('site.name')));
        } else {
        echo $html->link($html->image('banner_invite.png', array('alt' => 'Suma $10 por cada amigo que traigas a ' . Configure::read('site.name'))), array('controller' => 'users', 'action' => 'login'), array('escape' => false, 'alt' => 'Suma $10 por cada amigo que traigas a ' . Configure::read('site.name')));
        }
        ?>
    </div>
-->
	<?php
		echo $this->requestAction(array('controller' => 'banners', 'action' => 'front'), array('return'));
   // echo $clubcupon->activeGroups();
	?>

    <div class="box_banner">
      <?php if (Configure::read ('Actual.city_slug') == 'ciudad-de-buenos-aires'): ?>
        <?php if(false): ?>
        <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ Right -->
        <script type="text/javascript" language="JavaScript1.1">
        <!--
        var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
        var cs = document.charset || document.characterSet;
        document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/<?php echo $tagPrefix; ?>/Right?o=j&rnd=' + rnd + '&crs=' + cs + '"></scr' + 'ipt>');
        //-->
        </script>
        <noscript><a href="http://ads.e-planning.net/ei/3/9b30/<?php echo $tagPrefix; ?>/Right?it=i&rnd=$RANDOM" target="_blank"><img alt="e-planning.net ad" src="http://ads.e-planning.net/eb/3/9b30/<?php echo $tagPrefix; ?>/Right?o=i&rnd=$RANDOM" border=0></a></noscript>
        <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ Right -->
        <?php endif; ?>
      <?php else: ?>
        <?php if(false): ?>
        <!-- e-planning v3 - Comienzo espacio ClubCupon _ Ficha _ Right -->
        <script type="text/javascript" language="JavaScript1.1">
        <!--
        var rnd = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
        var cs = document.charset || document.characterSet;
        document.write('<scri' + 'pt language="JavaScript1.1" type="text/javascript" src="http://ads.e-planning.net/eb/3/9b30/<?php echo $tagPrefix; ?>/Right?o=j&rnd=' + rnd + '&crs=' + cs + '"></scr' + 'ipt>');
        //-->
        </script>
        <noscript><a href="http://ads.e-planning.net/ei/3/9b30/<?php echo $tagPrefix; ?>/Right?it=i&rnd=$RANDOM" target="_blank"><img alt="e-planning.net ad" src="http://ads.e-planning.net/eb/3/9b30/<?php echo $tagPrefix; ?>/Right?o=i&rnd=$RANDOM" border=0></a></noscript>
        <!-- e-planning v3 - Fin espacio ClubCupon _ Ficha _ Right -->
        <?php endif; ?>
      <?php endif; ?>
    </div>

    <div class="box">
        <?php
          $side_deals = $clubcupon->sideDeals ();
          $this_deal = null;
          if (isset ($deal) && isset ($deal ['Deal'])){
              $side_deals=$clubcupon->ordenaOfertas();
              $this_deal = $deal ['Deal']['id'];
            }
        ?>
        <?php if (count($side_deals)) { ?>
        <?php foreach($side_deals as $idx => $side_deal):
                if ($side_deal ['Deal']['id'] === $this_deal) continue;
        ?>
        <div class="side_offer <?php if($idx == 0) { echo 'first'; } ?>">
            <?php
            $openTag  = '<h4 class="titleDeal">';
            $closeTag  = '</h4>';
            if(!empty($from)) {
              if(($from == 'register') ||
                 ($from == 'protection') ) {
                $openTag  = '<h3 class="titleDeal">';
                $closeTag  = '</h3>';
              }
            } ?>
            <?php echo $openTag; ?>
              <?php

                //Imprimo el titulo de la oferta con el link
                echo $html->link($side_deal['Deal']['name'], array('controller' => 'deals', 'action' => 'view', $side_deal['Deal']['slug']), array('title' => $html->cText($side_deal['Deal']['name'], false)));

              ?>
            <?php echo $closeTag; ?>
            <?php echo $html->link($html->showImage('Deal', $side_deal['Attachment'], array('dimension' => 'small_big_thumb', 'alt' => $html->cText($side_deal['Company']['name'] . ' ' .$side_deal['Deal']['name'], false), 'title' => $html->cText($side_deal['Company']['name'] . ' ' .$side_deal['Deal']['name'], false))), array('controller' => 'deals', 'action' => 'view', $side_deal['Deal']['slug']), array('title' => sprintf(__l('%s'), $side_deal['Deal']['name'])), null, false); ?>



                <?php
                    if (!$side_deal  ['Deal']['only_price']) {
                    //mostramos la vista extendida
                ?>
                <div class="prices">
                 <div class="first">
                    <span>ORIGINAL</span>
                    <strong id="side_offer_original">
                      <?php
                        if ($side_deal ['Deal']['hide_price']) {
                          echo '$$$';
                        } else {
                          echo Configure::read('site.currency') . $html->cCurrency($side_deal['Deal']['original_price'], 'small');
                        }
                      ?>
                    </strong>
                </div>
                <div class="middle">
                    <span>PRECIO</span>
                    <strong id="side_offer_discounted">
                      <?php
                        if ($side_deal ['Deal']['hide_price']) {
                          echo '$$$';
                        } else {
                          echo Configure::read('site.currency') . $html->cCurrency($side_deal['Deal']['discounted_price'], 'small');
                        }
                      ?>
                    </strong>
                </div>
                <div class="last">
                    <span>AHORR&Aacute;</span>
                    <strong id="side_offer_percentage">
                      <?php
                        if ($side_deal ['Deal']['hide_price']) {
                          echo '%%%';
                        } else {
                          echo $html->cInt($side_deal['Deal']['discount_percentage'], 'small') . "%";
                        }
                      ?>
                    </strong>
                </div>
                    </div>
                <?php
                    }else{
                    //mostramos la vista resumida
                ?>
            <div class="prices single_price">
                <div class="first"></div>
                <div class="middle">
                    <span>PRECIO</span>
                    <strong id="side_offer_discounted">
                      <?php
                        if ($side_deal ['Deal']['hide_price']) {
                          echo '$$$';
                        } else {
                          echo Configure::read('site.currency') . $html->cCurrency($side_deal['Deal']['discounted_price'], 'small');
                        }
                      ?>
                    </strong>
                </div>
                </div>
                <?php
                    }
                ?>

            <p>Disfrut&aacute; ahora de este descuento</p>
            <?php echo $html->link('', array('controller' => 'deals', 'action' => 'view', $side_deal['Deal']['slug']), array('class' => 'mas_informacion')) ?>
        </div>
        <?php endforeach; ?>
        <?php } ?>

        <?php
        if (Configure::read('facebook.is_facebook_feed_enabled')) {
        echo '<div id="facebook_sidebox">';
            echo str_replace('###FANPAGE_URL###', urlencode($city_facebook_url), Configure::read('facebook.feeds_code'));
            echo '</div>';
        }

        if (isset($get_current_city)) {
        if (Configure::read('twitter.is_twitter_feed_enabled')) {
        echo '<div id="twitter_sidebox">';
            // echo strtr(Configure::read('twitter.tweets_around_city'), array('##CITY_NAME##' => ucwords($get_current_city)));
        ?>



<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
  new TWTR.Widget (
    {
      version: 2,
      type: 'search',
      rpp: 4,
      search: 'from:<?php echo $city_twitter_username; ?>',
      interval: 6000,
      title: '¡Todos los días una alegría!',
      subject: '<a href = "http://twitter.com/<?php echo $city_twitter_username; ?>">ClubCupón</a>',
      width: 'auto',
      height: 300,
      theme:
        {
          shell:  { background: '#333333', color: '#d47125'                   },
          tweets: { background: '#000000', color: '#bbbbbb', links: '#f25507' }
        },
      features: { scrollbar: false, loop: false, live: true, hashtags: true, timestamp: false, avatars: false, toptweets: false, behavior: 'all' }
    }).render ().start ();
</script>
        <?php
           echo '</div>';
        }
        }
        ?>
    </div>

    <div class="box cursorPointer">
        <?php echo '<a href="/now/now_registers/">' . $html->image('banner_company.png') . '</a>'; ?>
    </div>

    <div class="box">
        <div id="costumer_service">
            <h1><?php echo $html->image('txt_servicio_al_cliente.png'); ?></h1>
            <p>
                Si ten&eacute;s dudas respecto del servicio, ingres&aacute; a nuestro <strong>soporte al cliente.</strong>
                <br />
            <div style="text-align: center;">
                <a href="http://soporte.clubcupon.com.ar/" target="_blank" >
                    <img src="/img/soporte_cc_boton_small.png" alt="Acceder a la web de soporte de Club Cupon" />
                </a>
            </div>

            </p>
        </div>
    </div>

</div>
