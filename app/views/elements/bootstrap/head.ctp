<!DOCTYPE html>
<html lang="es">
    
    <head>
        <meta name="description" content="<?php echo Configure::read('web.meta.description')?>"/>
        <?php
            echo $html->charset(), "\n";
            $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
            $title_for_layout = Configure::read('site.name') . ' | ' . $html->cText($title_for_layout, false);
            echo '<title>' . $title_for_layout . '</title>';

            echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

            if (isset ($og_data)) {
                foreach ($og_data as $key => $value) {
                    if (is_array ($value)) {
                        foreach ($value as $val) {
                            echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                        }
                    } else {
                        echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                    }
                }
            }
            
            echo $html->css('themes/'.Configure::read('theme.version_number').'club-cupon');
            echo $html->css('themes/'.Configure::read('theme.version_number').'panel-vendedores');
            echo $html->css('bootstrap/bootstrap.min');
            echo $javascript->link('libs/jquery-1.9.1.min');
            echo $javascript->link('bootstrap/bootstrap.min');

            //echo $html->css('plugins/jquery-ui-1.7.1.custom');
            //echo $javascript->link('libs/jquery-ui-1.7.2.custom.min');
            
            echo $html->css(Configure::read('search.asset_version').'jquery-ui-1.10.3'); 
            //echo $javascript->link(Configure::read('search.asset_version').'jquery-ui-1.8.18.min'); 
            echo $javascript->link('theme_clean/libs/jquery-ui');
            
            
            //metodos genéricos
            //echo $javascript->link(Configure::read('theme.asset_version') . 'funciones');

        ?>
        <!-- facebook -->
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />

        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />
        
    </head>
