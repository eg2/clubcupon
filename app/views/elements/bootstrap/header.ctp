<script type="text/javascript">
    var ClickTaleSSL=1;
    if(typeof ClickTale=='function') ClickTale(162,0.12,"www14");
</script>
<!-- ClickTale end of Bottom part -->

<?php

    $cities = $clubcupon->activeCitiesWithDeals ();
    $groups = $clubcupon->activeGroupsWithDeals ();

    //ya que los grupos van a continuacion de las ciudades, unificamos en 1 solo arreglo.
    $results = array_merge($cities, $groups );

?>

<div id="header">
    <div class="top">
        <!-- div class="row"-->
            <div class="logo pull-left">
                <?php
                    $logo_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'theme_clean/';
                    if (Configure::read ('Actual.city_is_group') == 0 && !$is_corporate_city)  {
                        echo $html->link ($html->image ('web/clubcupon/logo.png', array ('alt' => 'CLUB CUPÓN.')), array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                    	
                    } else {
                        echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => 'CLUB CUPÓN.')), array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                    }
                ?>
            </div>
            <div class="select-city pull-left">
                <div class="grid_5 navigation">
                    &nbsp;
            <!--
                    <p id="label_como_ciudades">Eleg&iacute; tu ciudad</p>
                    <a href="#" class="city cityToggle"><?php echo $city_name; ?></a>
            -->
                </div>
            </div>
            <div class="suscribe-link pull-left" style="position:relative">
                &nbsp;
                <!--
                <a href="#" class="text cross">Recibir Ofertas Por Email</a>
                
                <div class="ingresa-tu-correo">
                    <span class="sprite-mas cross"></span>
                    <?php 
                        echo $form->create('Subscription', array('url' => array('controller' => 'subscriptions', 'action' => 'add'), 'class' => 'normal', 'enctype' => 'multipart/form-data', 'target'=> '_top'));
                        echo '<label>';
                            echo '<p>Ingres&aacute; tu email para recibir las ofertas de <?php echo $city_name; ?></p>';
                            echo $form->input('email', array('type' => 'text', 'class'=>'text','tabindex'=>1,  'default'=>'Ingresá tu mail', 'label'=>false, 'div'=>false));
                            echo $form->hidden('city_id', array('value'=>Configure::read ('Actual.city_id')));
                            echo $form->submit ('Aceptar', array('class'=>'submit', 'name'=>'buscar submit', 'div' =>false));
                            echo $form->end();
                        echo '<label>';
                    ?>
                </div>
                -->
            </div>

            
            
            
                <?php if ($is_corporate_city && !in_array($corporateCity['City']['slug'], Configure::read('corporate_exceptions.city_name'))){ ?>
						<div class="grid_4 opciones-login <?php echo $push_class; ?>">
    	                    <ul class="ingresa">
                            <style>
                                ul.mi-cuenta{top:104px!important;}
                            </style>
                                <li style="margin-right: 103px;margin-top: -32px; padding-left: 9px;">
                                    <?php 
                                        if($corporateCity['City']['image']) { 
                                            $cityLogoURL = $corporateCity['City']['image'];
                                        } else if(file_exists("img/theme_clean/exclusive/logo-".$corporateCity['City']['slug'].".png")) {
                                            $cityLogoURL = "/img/theme_clean/exclusive/logo-". $corporateCity['City']['slug'].".png";
                                        }
                                    ?>
                                    <img src="<?php echo $cityLogoURL; ?>" title="<?php echo $corporateCity['City']['name']; ?>"/>
                                </li>
	                        </ul>
						</div>
                <?php } ?>
                            
                                
            	<?php if (!$is_corporate_city){ ?>
                	<?php if ($auth->sessionValid ()){ ?>
                		<ul id="login" class="logged opciones_login parent">
	                    	<li class="<?php echo $is_corporate_city  ? 'corporate' : ''; ?>">
	                    		<span class="loginEmail">
		                        	<?php
		                            	echo ($auth->user ('email') ? $auth->user ('email') : $html->link ('Mi Perfil', array ('plugin' => '', 'controller' => 'user_profiles', 'action' => 'edit'), array ('title' => 'Mi perfil', 'rel' => 'nofollow'))) ;
		                            ?>
		                        </span>    
	                        </li>
                      	</ul>
                   	<?php }else{ ?>
                    	<ul id="login">
							<li><?php echo $html->link ('Ingresar', array ('plugin' => '', 'controller' => 'users', 'action' => 'login'), array ('title' => 'Ingresar', 'rel' => 'nofollow', 'class'=> 'item-salir')); ?></li>
							<li>|</li>
    	                	<li><?php echo $html->link('Registrarse', array('plugin' => '','controller' => 'users', 'action' => 'register'), array('title' => __l('Signup'), 'class' => 'item-salir')); ?></li>
			    		</ul>
                    <?php } ?>
				<?php } ?>

			<!-- UL desplegable Cuenta LOGUEADO -->
            <div id="desplegable_login">
				<img src="/img/web/clubcupon/flecha_top.png" class="flecha" alt="flecha">
				<div class="menu_final">
					<?php echo $this->element('dashboard/user_menu_links'); ?>
            	</div>
            </div>
			<!-- FIN UL desplegable Cuenta LOGUEADO -->

        <!-- div class="row" style="border:1px solid red"-->
        
    </div>    
        
            <div id="menu_top" >
                <ul>
                    <li><a href="/ofertas-nacionales">Ofertas Nacionales</a></li>
    				<li><a href="/turismo">Turismo</a></li>
    				<li><a href="/cel/landing">Móviles</a></li>
    				<li><a href="/ciudad-de-buenos-aires/pages/promocinoes">Promociones</a></li>
                </ul>
            </div>
        <!-- /div-->

<?php /* 
        <!-- Desplegable -->
        <!-- div class="row"-->
            <div class="span12 desplegable">
            <?php
                if (!empty ($results)) {
                    echo '<ul class="clearfix" >';
                        //Ordeno el array dado a que el modelo no esta reconociendo el orden default.
                        $cities_clean_array = array();
                            foreach ( $results as $city ){ array_push($cities_clean_array , array($city['City']['name'],$city['City']['slug'] )); }
                            $counter = 0;
                            for($a=0; $a < count($cities_clean_array); $a++) {
                                if($counter == 0 ) {echo '<li>';}
                                echo $html->link ($cities_clean_array[$a][0], array ('controller' => 'deals', 'action' => 'index', 'city' => $cities_clean_array[$a][1]), array ('title' => $cities_clean_array[$a][0]));
                                if($counter >= 3 ) {echo '</li>'; $counter = 0;} else{ echo ' | '; $counter++;}

                            }
                    echo '</ul>';
                    //para bajar la segunda lista
                    //echo '<div style="clear:both;"></div>';
                    //echo '<a href="#" class="btn-cerrar cityToggle">x Cerrar</a>';

                }
            ?>
            </div>
        <!-- /div-->
        <!-- FIN Desplegable-->
*/?>

	
</div>

<?php /* 
<!-- itemdesplegable -->
<div class="jscontentmenu">
    <div class="item-desplegable">
        <span>&#9668;</span>
        <a href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de Atenci&oacute;n al Cliente</a>
    </div>
</div>
<!-- itemdesplegable -->
*/?>


<script>
$(document).ready(function() {
    
    $('.suscribe-link .cross').click(function(){
        $('.ingresa-tu-correo').toggle();
    });
    
    $('.cityToggle').click(function(){
        $('.desplegable').slideToggle();
    });
    

    $("input[name='data[Subscription][email]']").bind({
            'click': function(){
                $(this).val('');
            },
            'blur': function(){
                if($(this).val()==''){
                    $(this).val('Ingresá tu mail');
                }
            }
        });

	$('.item-desplegable').mouseover(function() {
	  $(".item-desplegable").animate({
	    right: '0'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	$('.jscontentmenu').mouseleave(function() {
	  $(".item-desplegable").animate({
	    right: '-194'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	
	//compatibilidad plugin web
    $('.parent').click(function(event){
		event.stopPropagation();
		
		$(this).children('.desplegable').slideDown();
		$(this).children('.flecha').show();
	});

	 $('html').click(function(e){
        e.stopPropagation();
        if($(e.target).parents('.desplegable').length==0) {
        	if($('.desplegable:visible').length>0) $('.desplegable').slideUp(200);
        	$('.parent').removeClass('open');
        	$('.parent .flecha').hide();
        }
        if($(e.target).parents('#desplegable_login').length==0) {
        	if($('#desplegable_login').length>0) $('#desplegable_login').slideUp(200);
        	$('.parent').removeClass('open');
        }

    });

 	$(".opciones_login").click(function(event) {
		event.preventDefault();
		$('.opciones_login').addClass('open');
		
		$('.especiales .desplegable').slideUp();
		$('.especiales .flecha').hide();
		$('#btn_desplegar .desplegable').slideUp();
		$('#btn_desplegar .flecha').hide();
		
		$("#desplegable_login").slideToggle();
		//$('#desplegable_login .flecha').show();
	});
});  //fin doc ready
</script>
