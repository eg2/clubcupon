<div id="sidebar">
  <?php
    if($logged_in) {
      echo '<div id="side_balance" class="box">';
      echo '  <small>' . $user_balance . '</small>';
      echo '</div>';
    }
  ?>
  <div class="cursorPointer">
    <?php echo $html->link($html->image('banner_gift.png', array ('style' => 'padding-left: 3px; margin-top: 10px;')), '#', array('escape' => false, 'id' => 'buy_form_gift_button')); ?>
  </div>
</div>
