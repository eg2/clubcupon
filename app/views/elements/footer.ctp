<div id="footer">
  <div class="center">
    <?php echo $html->link($html->image('logo_footer.png', array('alt' => 'CLUB CUP&Oacute;N')), array('controller' => 'deals', 'action' => 'index', 'admin' => false), array('class' => 'floatLeft', 'escape' => false)); ?>
    <ul id="bottom_nav">
        <li class="noBorder" style="margin: 0px 10px 0px 0px"><img src="/img/logo_gc.png"/></li>
      <li><?php echo $html->link('Qui&eacute;nes Somos', array('controller' => 'pages', 'action' => 'who', 'admin' => false), array('escape' => false)); ?></li>
      <li>
      <?php
        if($auth->sessionValid()) {
          echo $html->link('Recomend&aacute;', 'javascript:void(0)', array('onclick' => 'popupGeneric("' . Router::url(array('controller' => 'firsts', 'action' => 'recommend'), false) . '", 960, 440);', 'escape' => false,'rel' => 'nofollow'));
        } else {
          echo $html->link('Recomend&aacute;', array('controller' => 'users', 'action' => 'login'), array('escape' => false,'rel' => 'nofollow'));
        }
      ?>
      </li>
      <li><?php echo $html->link('Qui&eacute;nes Somos', array('controller' => 'pages', 'action' => 'who', 'admin' => false), array('escape' => false)); ?></li>
      <li class="small"><a href="<?php echo FULL_BASE_URL.'/exclusive';?>">Exclusive</a></li>
      <li class="small"><?php echo $html->link('Concursos', array('controller'=>'pages','action'=>'concurso', 'admin'=> false), array('escape'=>false)); ?> </li>
      
      <li class="small"><?php echo $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false), array('escape' => false)); ?></li>
      <li class="small"><?php echo $html->link('Pol&iacute;ticas de Privacidad', array('controller' => 'pages', 'action' => 'policy', 'admin' => false), array('escape' => false)); ?> </li>
      <li class="small" style="padding-left:73px;"><a href="pages/Protection" rel=nofollow>Protecci&oacute;n de Datos Personales</a></li>
      
      <li class="small">Copyright &copy; <?php echo date('Y'); ?> Derechos Reservados.</li>
    </ul>
    <ul class="social_actions">
<!--      <li>Seguinos en</li>-->
      <?php if (preg_match ('/recent/s', $this->params['url']['url'])): ?>
        <li style = ""><?php echo $html->link($html->image('social_actions/rss.png'), $rss_feed_url_for_city . '/recent', array( 'escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <?php else: ?>
        <li style = ""><?php echo $html->link($html->image('social_actions/rss.png'), $rss_feed_url_for_city, array( 'escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <?php endif; ?>
      <li><?php echo $html->link($html->image('social_actions/facebook.png'), $city_facebook_url, array('target' => '_blank', 'escape' => false)); ?></li>
      <li><?php echo $html->link($html->image('social_actions/twitter.png'), $city_twitter_url, array('target' => '_blank', 'escape' => false)); ?></li>
    </ul>
  </div>
</div>
<div id="fb-root"></div>
<div id="shadow"></div>