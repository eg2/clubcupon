    <div class="contenedor-formmicuenta-reg clearfix">
		<h2>Datos Generales</h2>
            <?php
	        echo $form->input('name'                       ,array ('label' => 'Nombre de fantas&#237;a'));
			echo $form->input('phone'                      ,array ('label' => 'Tel&#233;fono', 'style' => 'width:10.7em'));
            echo $form->input('contact_phone'              ,array ('label' => 'Tel&#233;fono de contacto', 'info' => 'Ingresar s&oacute;lo n&uacute;meros. Este telefono ser&#225; publicado como dato de contacto en el cup&#243;n.'));
			echo $form->input('url'                        ,array ('label' => 'URL', 'info' => __l('eg. http://www.example.com')));
		/*
		echo $form->input('User.id'              ,array ('type' => 'hidden'));
		echo $form->input('User.username'              ,array ('info'  => 'M&#237;nimo 3 caracteres, M&#225;ximo 20. Sin espacios y caracteres especiales.','label' => 'Nickname'));
                echo $form->input('User.email'                 ,array ('label' => 'Email'));
                echo $form->input('User.passwd'                ,array ('label' => 'Contrase&ntilde;a'));
                echo $form->input('User.confirm_password'      ,array ('type'  => 'password', 'label' => 'Confirmar contrase&ntilde;a'));
                echo $form->input('is_online_account'          ,array ('type'  => 'hidden','value' => '1'));
                echo $form->input('has_pins'                   ,array ('label' => 'N&#250;mero de PINes que usa'));
                echo $form->input ('is_tourism'                ,array ('label' => 'Es compa&#241;&#237;a de turismo'));
                echo $form->input ('is_end_user'               ,array ('label' => 'Se factura al usuario final'));
		*/
            ?>
    </div>
	<div class="contenedor-formmicuenta-reg clearfix">
		<h2>Direcci&oacute;n</h2>
            <?php
                echo $form->input('address1'                   ,array('label' => 'Direcci&#243;n'));
                //echo $form->input('country_id'                 ,array('label' => 'Pa&iacute;s'));
                echo $form->input('CompanyCandidate.state_id', array('label' => 'Provincia'));
				echo $form->input('CompanyCandidate.city_id', array('label' => 'Ciudad'));
				echo $form->input('zip'                        ,array('label' => 'C&oacute;digo Postal', 'style' => 'width:6em'));
            ?>
    </div>
	<div class="contenedor-formmicuenta-reg clearfix" style="display:none">
		<h2><?php echo __l('Locate yourself on google maps'); ?></h2>&nbsp;
		<script src="http://maps.googleapis.com/maps/api/js?&sensor=false" type="text/javascript"></script>
		<div id="show-map" style="height: 260px; width: 630px;">

		</div>
		<?php
			echo $form->input('latitude' ,array('type' => 'hidden', 'id'=>'latitude'));
			echo $form->input('longitude',array('type' => 'hidden', 'id'=>'longitude'));
		?>
		<script type="text/javascript">
		var myOptions = {
          <?php if(isset($this->data['CompanyCandidate']['latitude']) && @strlen($this->data['CompanyCandidate']['latitude'])){?>
		  center: new google.maps.LatLng(<?php echo $this->data['CompanyCandidate']['latitude'];?>, <?php echo $this->data['CompanyCandidate']['longitude'];?>),
		  zoom: 15,
          <?php }else{?>
		  center: new google.maps.LatLng(-34.612, -58.450),
		  zoom: 8,
		  <?php }?>
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("show-map"),myOptions);
		var marker = new google.maps.Marker({
				position: myOptions.center,
				map: map,
				visible: <?php if(isset($this->data['CompanyCandidate']['latitude']) && @strlen($this->data['CompanyCandidate']['latitude'])){?> true <?php }else{?> false <?php }?>,
				animation: google.maps.Animation.b,
				title: 'Aqui estoy!'
			});

		google.maps.event.addListener(map, 'click', function(ev) {
			$('input#latitude').attr('value',ev.latLng.lat());
			$('input#longitude').attr('value',ev.latLng.lng());
			marker.setPosition(ev.latLng);
			marker.setVisible(true);
			marker.setAnimation(google.maps.Animation.b);
		  });

		</script>
	</div>
	<div class="contenedor-formmicuenta-reg clearfix">
	  <h2><?php echo __l('Datos Fiscales'); ?></h2>
	  <?php
		echo $form->input('fiscal_name'                 ,array('label' => 'Razón Social'));
		echo $form->input('fiscal_address'              ,array('label' => 'Domicilio Comercial'));
		//echo $form->autocomplete('FiscalCity.name'      ,array('label' => 'Localidad', 'acFieldKey' => 'FiscalCity.id',  'acFields' => array('City.name'),  'acSearchFieldNames' => array('City.name'),  'maxlength' => '255'));
		//echo $form->autocomplete('FiscalState.name'     ,array('label' => 'Provincia', 'acFieldKey' => 'FiscalState.id', 'acFields' => array('State.name'), 'acSearchFieldNames' => array('State.name'), 'maxlength' => '255'));
		echo $form->input('CompanyCandidate.fiscal_state_id', array('label' => 'Provincia'));
		echo $form->input('CompanyCandidate.fiscal_city_id', array('label' => 'Localidad'));
		echo $form->input('fiscal_phone'                ,array('label' => 'Tel&#233;fono', 'style' => 'width:10.7em'));
		echo $form->input('fiscal_fax'                  ,array('label' => 'Fax', 'style' => 'width:10.7em'));
		echo $form->input('fiscal_cuit'                 ,array('label' => 'CUIT', 'style' => 'width:6em'));
		echo $form->input('fiscal_iibb'                 ,array('label' => 'Nro Insc. IIBB', 'style' => 'width:6.5em'));
		//echo $form->input('percepcion_iibb_caba'        ,array('label' => 'Percepci&#243;n IIBB CABA','type' => 'select',    'options' => array('' => 'Seleccionar','1' => 'Exento', '0' => 'No Exento')));
		echo $form->input('fiscal_cond_iva'             ,array('label' => 'Condici&#243;n frente al IVA', 'style' => 'width:13em','type' => 'select', 'options' => array('monotributo' => 'Monotributo', 'exento' => 'Exento', 'ri' => 'Responsable Inscripto')));
		echo $form->input('fiscal_bank'                 ,array('label' => 'Banco'));
		echo $form->input('fiscal_bank_account'         ,array('label' => 'Nro de Cuenta'));
		echo $form->input('fiscal_bank_cbu'             ,array('label' => 'CBU'));
		//echo $form->input('persona'                     ,array('label' => 'persona','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Titular', '0' => 'No titular')));
		//echo $form->input('declaracion_jurada_terceros' ,array('label' => 'Declaraci&#243;n jurada de pago a terceros','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Si', '0' => 'No')));
		//echo '<legend>Datos de pago por cheque: ';
		echo $form->input('cheque_noorden'              ,array('label' => 'Cheque No a la orden ', 'style'=>'width:8.5em','type' => 'select', 'options' => array('' => 'Seleccionar','1' => 'Si', '0' => 'No')));
		//echo '</legend>';


		if(isset($upd_files['extban'])){
			$file_info = pathinfo($upd_files['extban'][0]);
			$user = array_pop(explode(DIRECTORY_SEPARATOR,$file_info['dirname']));
			$file = $file_info['basename'];
			$ext_ban_link = '<a href="'.Router::url(array('action' => 'getfile','user' => $user,'file' => $file),array('escape' => false)).'"><img src="/img/icon-transaction.png"></a>';
			//echo "<label class=\"descripcion-reg\">Archivo subido el ".$upd_files['extban'][1]." para concervarlo deje este campo vacio!</label>";
		}else{
			$ext_ban_link = "";
		}

		echo '<div class="input text"><label for="extban">Extracto Bancario '.$ext_ban_link.'</label>';
		echo $form->file('extban');
		echo "</div>";

		if(isset($upd_files['encfac'])){
			$file_info = pathinfo($upd_files['encfac'][0]);
			$user = array_pop(explode(DIRECTORY_SEPARATOR,$file_info['dirname']));
			$file = $file_info['basename'];
			$enc_fac_link = '<a href="'.Router::url(array('action' => 'getfile','user' => $user,'file' => $file),array('escape' => false)).'"><img src="/img/icon-transaction.png"></a>';
			//echo "<label class=\"descripcion-reg\">Archivo subido el ".$upd_files['encfac'][1]." para concervarlo deje este campo vacio!</label>";
		}else{
			$enc_fac_link = "";
		}

		echo '<div class="input text"><label for="encfac">Encabezado de Factura o Constancia de IIBB '.$enc_fac_link.'</label>';
		echo $form->file('encfac');
		echo "</div>";
	  ?>
	</div>
