<?php $cities = $clubcupon->activeCitiesWithDeals(); ?>
<div class="city-list box">
  <h3>Encontr&aacute; tus ofertas en</h3>
  <ul>
  <?php foreach($cities as $city): ?>
    <?php if($city['City']['slug'] == $city_slug): ?>
       <li class="active">
    <?php else: ?>
       <li class="">
    <?php endif; ?>
    <?php echo $html->link($city['City']['name'],
                           array('controller' => 'deals',
                                 'action' => 'index',
                                 'city' => $city['City']['slug']),
                                 array('title' => $city['City']['name'],
                                       'escape' => false)); ?>
     <?php endforeach; ?>
    </li>
  </ul>
</div>
