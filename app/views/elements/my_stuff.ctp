<div id="my-account">
        
    <div class="width100 floatLeft">

        <h2 class="title floatLeft" style="height:20px">MI CUENTA</h2>

        <ul id="my_stuff_tabs" style="font-weight: bold;" >

            <?php
                if($auth->user('user_type_id') == ConstUserTypes::Company)
                {
                    $user = $html->getCompany($auth->user('id')); 
                    echo '<li>' . $html->link('Mis Datos',              array('controller' => 'companies',     'action' => 'edit',$user['Company']['id']),   array('title' => 'Mis Datos' )) . '</li>';
                }
                else
                {
                    echo '<li>' . $html->link('Mi Cuenta',   array('controller' => 'user_profiles', 'action' => 'edit',$auth->user('id')), array('title' => 'Mis Datos')) . '</li>';
                }

                if($auth->sessionValid() && $html->isAllowed($auth->user('user_type_id')))
                {
                    echo '<li>' . $html->link('Mis Cupones',            array('controller' => 'deal_users',    'action' => 'index'),                         array('title' => 'Mis Cupones')) . '</li>';
                    echo '<li>' . $html->link('Mis Cupones de Regalo',  array('controller' => 'deal_users',    'action' => 'gifted_deals','admin' => false), array('title' => 'Mis Cupones de Regalo')) . '</li>';

                    if(Configure::read('friend.is_enabled'))
                    {
                        echo '<li>' . $html->link('Mis amigos',         array('controller' => 'user_friends',  'action' => 'lst',   'admin' => false),       array('title' => 'Mis amigos')) . '</li>';
                        echo '<li>' . $html->link('Importar amigos',    array('controller' => 'user_friends',  'action' => 'import','admin' => false),       array('title' => 'Importar amigos')) . '</li>';
                    }

                }
                if(!$auth->user('fb_user_id') && !$auth->user('is_openid_register'))
                {
                    echo '<li>' . $html->link('Cambiar contrase&ntilde;a',array('controller'=> 'users', 'action'=>'change_password'),array('escape'=>false, 'title' => 'Cambiar contrase&ntilde;a')) . '</li>';
                }
                
                
            ?>

        </ul>

    </div>

</div>


<div class="tabs" style="clear:both;">
    <ul>
        <li></li>
        
    </ul>
</div>