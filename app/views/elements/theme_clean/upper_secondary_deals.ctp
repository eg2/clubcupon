    <?php if(!empty($secondaryDeals)){ ?>
        <div class="grid_16 bloque-horizontal">
            <h2 class="clearfix">
                <p>
                    M&aacute;s ofertas en <?php echo $secondaryDeals[0]['City']['name']; ?>
                </p>
                <?php echo $html->link('Ver todas las ofertas en '.$secondaryDeals[0]['City']['name'].' > ', array('controller' => 'deals', 'action' => 'landingofertas'), array('title' => 'Ver todas las ofertas > ', 'escape' => false)); ?>
            </h2>
            <div class="bloque-horizontal-contenido clearfix">            
                <?php
                    for ($i = 0; $i < Configure::read('secondaryDeals.upperDealsLimit'); $i++)
                    {
                        
                        if(empty($secondaryDeals[$i])) { break;}
                        
                        $single_upper_secondary_deal = $secondaryDeals[$i];
                        
                        $single_upper_secondary_deal_title    = $clubcupon->shorten_strings_with_html_entities($single_upper_secondary_deal['Deal']['name'], 76);
                        $single_upper_secondary_deal_subtitle = $clubcupon->shorten_strings_with_html_entities($single_upper_secondary_deal['Deal']['subtitle'], 25);
                        
                        
                        if (!$single_upper_secondary_deal['Deal']['hide_price']) {
                            
                            $hover_titulo_oferta = $clubcupon->shorten_strings_with_html_entities($single_upper_secondary_deal['Deal']['name'], 105);
                            
                            if (!$single_upper_secondary_deal ['Deal']['only_price']){
                                
                                $titulo_oferta       = 'Pag&aacute; s&oacute;lo $' . $html->cCurrency ($single_upper_secondary_deal['Deal']['discounted_price'] , 'small') . ' y obtene un descuento de $' . $html->cCurrency ($single_upper_secondary_deal['Deal']['discount_amount'] , 'small') .' <span>' . $single_upper_secondary_deal_title . '</span>';
                                $hover_desc_oferta   = 'Pag&aacute; $'      . $html->cCurrency ($single_upper_secondary_deal['Deal']['discounted_price'] , 'small') . ' en vez de $' . $html->cCurrency ($single_upper_secondary_deal['Deal']['original_price'] , 'small');
                            
                            }else{
                                
                                $titulo_oferta       = 'Pag&aacute; s&oacute;lo $' . $html->cCurrency ($single_upper_secondary_deal['Deal']['discounted_price'] , 'small')  . ' <br /><span>' . $single_upper_secondary_deal_title . '</span>';
                                $hover_desc_oferta   = 'Pag&aacute; $'      . $html->cCurrency ($single_upper_secondary_deal['Deal']['discounted_price'] , 'small') ;
                                
                            }

                        }else{
                            
                            $titulo_oferta       = $single_upper_secondary_deal['Deal']['discount_percentage'] . '% de descuento' . ' <br /><span>' . $single_upper_secondary_deal_title . '</span>';
                            $hover_titulo_oferta = $clubcupon->shorten_strings_with_html_entities($single_upper_secondary_deal['Deal']['name'], 110);
                            $hover_desc_oferta   = $single_upper_secondary_deal['Deal']['discount_percentage'] . '% de descuento';
                            
                        }
                        
                        //Hack horrible para el formato de imagenes, a remover en algnn futuro release. Ver notas de Juan, implica todar el método Attachment
                        $image_tag = $html->showImage('Deal', $single_upper_secondary_deal['Attachment'], array('dimension' => 'medium_big_thumb', 'alt' => $html->cText($single_upper_secondary_deal['Company']['name'] . ' ' .$single_upper_secondary_deal['Deal']['name'], false), 'title' => $html->cText($single_upper_secondary_deal['Company']['name'] . ' ' .$single_upper_secondary_deal['Deal']['name'], false)));
                        $image_tag = str_replace('width="440"',  'width="277"',  $image_tag);
                        $image_tag = str_replace('height="292"', 'height="183"', $image_tag);
                        
                        $sOferta=$single_upper_secondary_deal['Deal']['slug'];
                        $sUrl='/'.$city_slug .'/deal/'.$sOferta;
                        $nombreModal='upper_secondary_modal'.$i; 
                        
                        echo '<div class="bloque-h-interior">';
                        
                            echo '<div class="imagen-js">';

                                echo $html->link($image_tag,     array('controller' => 'deals', 'action' => 'view', $single_upper_secondary_deal['Deal']['slug']), array('title' => sprintf(__l('%s'), $single_upper_secondary_deal['Deal']['name'])), null, false);

                                //modal - hover
                                echo '<div style="display: none; position:relative" class="div-modal">';
                                    echo '<div class="modalFondo"></div>';
                                    echo '<div class="modal" id="' . $nombreModal . '">';
                                        echo '<div class="titulo">&nbsp;</div>';
                                        echo '<div class="descripcion">' . $hover_desc_oferta . '</div>';
                                        echo '<div class="btnVerMas">';
                                            echo $html->link('Ver m&aacute;s', array('controller' => 'deals', 'action' => 'view', $single_upper_secondary_deal['Deal']['slug']), array('title' => $html->cText($single_upper_secondary_deal['Deal']['name'], false), 'escape' => false));
                                        echo '</div>';
                                        if (!$single_upper_secondary_deal ['Deal']['only_price'])
                                        {
                                            echo '<div class="porcentaje"><span>';
                                            echo  $single_upper_secondary_deal['Deal']['discount_percentage'];
                                            echo '%</span></div>';
                                        }
                                    echo '</div>';
                                echo '</div>';
                                //FIN modal - hover
                            
                            echo '</div>';
                            echo '<div class="bloque_nombre_oferta">';
                                echo $html->link($hover_titulo_oferta, array('controller' => 'deals', 'action' => 'view', $single_upper_secondary_deal['Deal']['slug']), array('title' => $html->cText($single_upper_secondary_deal['Deal']['name'], false), 'escape' => false));
                            echo '</div>';
                            echo '<p>' . $single_upper_secondary_deal_subtitle . '&nbsp;</p>';
                        
                        echo '</div>';
                        
                        ?>
                        <script type="text/javascript">

                        $(document).ready(function () {

                            $("#<?php echo $nombreModal;?>").click(function () {
                                
                            	window.location = "<?php echo $sUrl;?>"
                            });
                        });
                        </script>
                        <?php

                    }
                        
                    //Limpiamos todo
                    unset($titulo_oferta);
                    unset($single_upper_secondary_deal_title);
                    unset($hover_titulo_oferta);
                    unset($hover_desc_oferta);
                    unset($image_tag);
                    
                ?>    
            </div>
	</div>

<?php
        }
?>
