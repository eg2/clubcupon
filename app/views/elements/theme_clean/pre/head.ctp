<?php

    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $browserIsLinuxHost = (strpos($browser, 'Linux') !== false);
    $browserIsIE7 = (strpos($browser, 'MSIE 7.0') !== false);
    $browserIsIE8 = (strpos($browser, 'MSIE 8.0') !== false);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml"<?php if (isset ($og_data)) echo 'xmlns:og = "http://ogp.me/ns#"  xmlns:fb = "http://www.facebook.com/2008/fbml"'; ?>>
    <head>
        <?php echo $html->charset(), "\n"; ?>

        <!--[if IE 7]>
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

        <!--[if IE 8]>
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

        <title>
        <?php
            if(preg_match('/'.Configure::read('site.name').'/', $title_for_layout)) {
                $title = Configure::read('site.name') . ' | ' .$html->cText($title_for_layout, false);
            } else {
                $title = Configure::read('site.name') . ' | ' . $html->cText($title_for_layout, false);
            }
            $title = 'Suscripción - '.Configure::read('web.meta.description');
            echo $title;
        ?>
        </title>

        <?php

              echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

              if (isset ($og_data)) {
                // var_dump ($og_data);
                foreach ($og_data as $key => $value) {
                  if (is_array ($value)) {
                    foreach ($value as $val) {
                      echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                    }
                  } else {
                    echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                  }
                }
              }
              $this->log('head:description->'.Configure::read('web.meta.description'));
              ?>
              <meta name="description" content="Suscripción en Club Cupón - <?php echo Configure::read('web.meta.description')?>"/>
              <?php
              //CSSs
            echo $html->css(Configure::read('theme.asset_version') . 'reset');      //unifica los estados de los navegadores
            //
            //Analizamos que hoja de estilo vamos a usar...


            switch (@$css_layout)
            {
                case 'popup':
                    $css_grilla = 'popup';
                    break;
                case 'wide':
                    //estamos en un layout del tipo WIDE
                    $css_grilla = 'custom-grid';
                    break;
                default:
                    //Cualquier tipo de layout
                    $css_grilla = '960_16_col';
            }

            echo $html->css(Configure::read('theme.asset_version') . $css_grilla); //fw de css
            echo $html->css(Configure::read('theme.asset_version') . 'global');     //estilos funcionales
            echo $html->css(Configure::read('theme.asset_version') . 'search_header');     //estilos funcionales
            echo $html->css(Configure::read('theme.asset_version') . 'colorbox');   //popup
            echo $html->css(Configure::read('theme.asset_version') . 'jquery-ui-1.7.3.custom');   //timepicker
            echo $html->css(Configure::read('theme.asset_version') . 'jquery.ui.datepicker');   //timepicker
            echo $html->css(Configure::read('search.asset_version') . 'jquery-ui-1.10.3');   //autocomplete Search


          


            //JSs
            //Jquery
            echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true));
            //CakeLog::write('debug', $js_vars_for_layout);
            echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.7.1.min');

			echo $javascript->link(Configure::read('theme.asset_version') . 'jquery.colorbox');
            //facebook
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.cookie');//lo usa funciones
            //echo $javascript->link(Configure::read('theme.asset_version') . 'facebook');
            //echo $javascript->link(Configure::read('theme.asset_version') . '/libs/facebook-sdk');//lo usa facebook

            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.form');
            
            //buy
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.countdown');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.livequery');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.ui.datepicker');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/date.format');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.metadata');

            //echo $javascript->link(Configure::read('theme.asset_version') . '/libs/mathcontext');
            //echo $javascript->link(Configure::read('theme.asset_version') . '/libs/BigDecimal');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/BigDecimal-all-last.min');
            // + lib corners para iexplorer7, ver mas abajo


            // echo $javascript->link(Configure::read('theme.theme_name') . 'facebook');

            //metodos genéricos
            echo $javascript->link(Configure::read('theme.asset_version') . 'funciones');


            echo $javascript->link(Configure::read('search.asset_version').'jquery-ui-1.8.18.min');
            echo $javascript->link(Configure::read('search.asset_version').'search-Plugin-1.0');

            //echo $html->meta('description',  $meta_for_layout['description']), "\n";

        ?>
        <!-- facebook -->
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />

        <!--[IF IE]>
            <script type="text/javascript" src="/js/theme_clean//libs/jq-corners.js"></script>
        <![endif]-->


        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />
    </head>
