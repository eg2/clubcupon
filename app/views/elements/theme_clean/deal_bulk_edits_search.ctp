<style>
    .buscador {
        font-weight: bold;
        padding:5px 15px;
        clear:both;
        width:780px;
    }
    .buscador .label{ padding: 0; }
    .buscador .submit{ margin:10px 0 0 0;}
    .separador{padding-top:15px;}
    .timepicker{display:none}
</style>
<script>

    $(document).ready(function () {
        $('div').removeClass('input select required')
        $('.displaydate.displaydate1').css('border-radius', '5px');
        $('.displaydate.displaydate1').css('background', 'none repeat scroll 0 0 #eee');
        $('.timepicker').css('display', 'none');

        $('#datewrapper1:hidden').livequery(function () {
            start_date = '#cakedate1';
            var start = $(start_date).datepicker('getDate');
            newDate = new Date(start.getTime());
            $('#DealStartDate').val(formatdate(newDate));
        });

    });

</script>
<div class="buscador">

    <?php echo $form->create('deal_bulk_edits', array('class' => 'normal', 'action' => 'index')); ?>
    <table>
        <tr>
            <td>
                <?php
                echo $form->input(
                        'Company.slug', array(
                    'type' => 'select',
                    'label' => 'Compañía',
                    'options' => $companiesOptions,
                    'value' => $companySlugValue,
                        )
                );
                ?>
            </td>
            <td>
                <?php
                echo $form->input(
                        'City.slug', array(
                    'type' => 'select',
                    'label' => 'Ciudad',
                    'options' => $citiesOptions,
                    'value' => $citySlugValue,
                        )
                );
                ?>
            </td>
            <td>
                <?php
                echo $form->input(
                        'Deal.status', array(
                    'type' => 'select',
                    'label' => 'Estados',
                    'options' => $dealStatusesOptions,
                    'value' => $dealStatusSelected,
                        )
                );
                ?>
            </td>
            <td>
                <div class = "clearfix date-block">
                    <div class = "clearBoth input date clearfix">
                        <div class = "js-datetime">
                            <?php echo $form->input('Deal.start_date', array('label' => __l('Start Date'), 'minYear' => date('Y'), 'maxYear' => date('Y') + 10, 'div' => false, 'empty' => __l('Please Select'))); ?>
                        </div>
                    </div>
                </div>
            </td>            
        </tr>
        <tr><td><?php echo $form->submit('Buscar'); ?></td></tr>
    </table>
    <?php echo $form->end(); ?>
</div>
<hr />