<?php
    if(Configure::read('themeBrown.enabled')){ 
        echo $this->element(Configure::read('theme.theme_name') . 'themeBrown');
    }
?>
<div class="header-format">
<div class="container">
  <header class="clearfix">
	<div class="row" style="overflow: visible;">
		<section class="twocol logo">
			<article>
			<h1>
				<?php
	                    
                        $logo_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'theme_clean/';
                        
                        if (Configure::read ('Actual.city_is_group') == 0 && !$is_corporate_city)
                        {
                            echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => '--CLUB CUPÓN.')), array ('controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                        }
                        else
                        {
                            echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => '**CLUB CUPÓN.')), array ('controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                        }
                    
                    ?>
			</h1>
			</article>
		</section>
  	    <section class="tencol last barranav">
		  <nav>
			<ul id="ul-menu" >
		    	
            	<li class="loginuser ccback">
            	<?php
            			 if (Configure::read ('Actual.city_is_group') == 0 && !$is_corporate_city)
                         {
                             echo $html->link ('Volver a p&aacute;gina principal', array ('controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                         }
                         else
                         {
                             echo $html->link ('Volver a p&aacute;gina principal', array ('controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                         }
            	?>
            	
            	</li>
		    	<?php if ($auth->sessionValid ()){ ?>
                    <li class="loginuser">
                        <p href="#" class="usrnamefrm"><?php echo $user['User']['username'];?></p>
                    </li>
            	<?php } ?>
			  
        	</ul>
          </nav>
        </section>
    </div>
  </header>
</div>	
</div>