
<div class="footer-format">
	<div class="container">
		<footer class="clearfix">
		<div class="row">
			<section class="twocol logo">
				<a title="CLUB CUP&oacute;N." href="#">
				<?php
		                $img_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'/img/';
		                echo $html->link($html->image($img_url . 'logo_footer.png', array('alt' => 'CLUB CUP&Oacute;N.','class' => 'hiddenn', 'escape' => false)), array('controller' => 'deals', 'action' => 'index', 'admin' => false), array('class' => 'hiddenn', 'escape' => false));
		            ?>
				<!--img class="hiddenn" alt="CLUB CUP&oacute;N." src="img/logo-mini.png"-->
				</a>
			</section>

			<section class="onecol">
				<!--img class="cl-logo hiddenn" alt="" src="img/logo_gc.png"-->
				 <?php echo $html->image($img_url . 'logo_gc.png', array('alt' => 'Grupo Clarin','class' => 'cl-logo hiddenn', 'escape' => false));?>
			</section>

			<section class="sixcol">
			<ul class="bottom_nav">

			<li class="hiddenn"><?php echo $html->link('Qui&eacute;nes Somos', array('controller' => 'pages', 'action' => 'who', 'admin' => false), array('escape' => false)); ?></li>
            <li class="hiddenn">
                <?php
					if(isset($mainDeal['isAccesibleForBuy']) && $mainDeal['isAccesibleForBuy']){
						if($auth->sessionValid()){
							echo $html->link('Recomend&aacute;', 'javascript:void(0)', array('onclick' => 'popupGeneric("' . Router::url(array('controller' => 'firsts', 'action' => 'recommend'), false) . '", 960, 460, "Cerrar");', 'escape' => false, 'class'=>'email'));
						}else{
							echo $html->link('Recomend&aacute;', array('controller' => 'users', 'action' => 'login'), array('escape' => false, 'class'=>'email'));
						}
					}
                ?>

            </li>
            <li class="hiddenn"><a title="Centro de Atenci&oacute;n al Cliente" href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de Atenci&oacute;n al Cliente</a></li>
            <li><a href="<?php echo FULL_BASE_URL.'/exclusive';?>">Exclusive</a></li>
            <li><?php echo $html->link('Concursos', array('controller'=>'pages','action'=>'concurso', 'admin'=> false), array('escape'=>false)); ?> </li>
           
            
            <li><?php echo $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false), array('escape' => false)); ?></li>
            <li><?php echo $html->link('Pol&iacute;ticas de Privacidad', array('controller' => 'pages', 'action' => 'policy', 'admin' => false), array('escape' => false)); ?> </li>
            <li><?php echo $html->link('Protecci&oacute;n de Datos Personales', array('controller' => 'pages', 'action' => 'protection'), array('escape' => false, 'class'=>'hiddenn')); ?></li>
            <li><a target="_blank" rel="nofollow" href="http://www.buenosaires.gob.ar/" title="Dirección General de Defensa y Protección al Consumidor - Consultas y/o denuncias" class="txtl">Dirección General de Defensa y Protección al Consumidor</a></li>
            <li class="last" >Copyright &copy; 2012 Derechos Reservados.</li>               


			</ul>
			</section>

            <section class="onecol" style="text-align:center; margin-left:-41px; width:118px;">
                <?php
                    if( time() < Configure::read('tec_dia.start_date') ) {
                        echo '<a href="https://servicios1.afip.gov.ar/clavefiscal/qr/mobilePublicInfo.aspx?req=e1ttZXRob2Q9Z2V0UHVibGljSW5mb11bcGVyc29uYT0zMDcwOTE4NjI5NV1bdGlwb2RvbWljaWxpbz0wXVtzZWN1ZW5jaWE9MF1bdXJsPWh0dHA6Ly93d3cuY2x1YmN1cG9uLmNvbS5hci9dfQ==" target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" width="62"></a>';
                    } else  {
                        echo '<a href="http://qr.afip.gob.ar/?qr=aaBUq_tGRAX3UzK3EQuMmg,," target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" width="62"></a>';
                    }
                ?>
            </section>
            
			<section class="twocol last social_icons_group">
				<ul class="social_actions hiddenn">
						<?php
		                    if (preg_match ('/recent/s', $this->params['url']['url'])) {
		                        echo '<li>' . $html->link($html->image('social_actions/rss.png'), $rss_feed_url_for_city . '/recent', array( 'escape' => false, 'style' => 'padding: 0px;')) . '</li>';
		                    }elseif(isset($rss_feed_url_for_city)){
		                        echo '<li>' . $html->link($html->image('social_actions/rss.png'), $rss_feed_url_for_city, array( 'escape' => false, 'style' => 'padding: 0px;')) . '</li>';
		                    }
		                ?>
		                <li><?php echo $html->link($html->image('social_actions/facebook.png'), $city_facebook_url, array('target' => '_blank', 'escape' => false)); ?></li>
		                <li><?php echo $html->link($html->image('social_actions/twitter.png'),  $city_twitter_url,  array('target' => '_blank', 'escape' => false)); ?></li>


				</ul>
			</section>
		</div>
		</footer>
	</div>

</div>
