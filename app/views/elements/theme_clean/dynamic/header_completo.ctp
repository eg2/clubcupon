<div class="header-format"  id="header">
<div class="container">
<header class="clearfix">
<div class="row" style="overflow: visible;">
	<section class="twocol logo">
		<article>
			<h1>
				
				<?php
	                    
                        $logo_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'theme_clean/';
                        
                        if (Configure::read ('Actual.city_is_group') == 0 && !$is_corporate_city)
                        {
                            echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => '--CLUB CUPÓN.')), array ('controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                        }
                        else
                        {
                            echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => '**CLUB CUPÓN.')), array ('controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN. '));
                        }
                    
                    ?>
				
			</h1>
		</article>
		</section>
		<?php if(!$is_corporate_city || !$is_deal_view ){ ?>
        <section class="grid_5 navigation">
                <p id="label_como_ciudades">Eleg&iacute; tu ciudad</p>
                <a href="#" class="city"><?php echo $city_name; ?></a> 
        </section>
    <?php } ?>

    <?php if(!$city_is_group && !$is_corporate_city) { ?>
<section class="grid_4 navigation">

<a href="#" class="text">Recibir Ofertas Por Email</a>
</section>
    <?php } 
    
    //Usada para compensar cuando borro algo
    $push_class = $city_is_group ? 'push_4' : '';
    $push_class = $is_corporate_city ? 'push_9' : '';
    //$push_class = !$is_deal_view ? 'push_4' : 'push_9';
    
    ?>

    <section class="grid_4 opciones-login <?php echo $push_class; ?>">
    <nav>
    
            <ul class="ingresa">
                <?php if ($auth->sessionValid ()){ ?>
                    <li><?php echo $html->getUserLink (array ('username' => $auth->user ('username'), 'user_type_id' => $auth->user ('user_type_id'), 'id' => $auth->user ('id')));?></li>
                    <li><a href="#" class="menu-js"><span>|</span> Men&uacute;</a></li>
                <?php }else{ ?>
                    <li><?php echo $html->link ('Ingresar', array ('controller' => 'users', 'action' => 'login'), array ('title' => 'Ingresar', 'rel' => 'nofollow', 'class'=> 'item-salir')); ?></li>
                    <li><span>|</span><?php echo $html->link('Registrarse', array('controller' => 'users', 'action' => 'register'), array('title' => __l('Signup'), 'class' => 'item-salir')); ?></li>
                <?php } ?>
                
            </ul>
            </nav>
            <nav>
                <!-- UL desplegable Cuenta LOGUEADO -->
                <?php
                    echo $this->element('dashboard/user_menu_links');
                ?>
                <!-- FIN UL desplegable Cuenta LOGUEADO -->			
            </nav>
</section>
	
	
	<section class="tencol last barranav">
		<nav class="push_3 grid_13 nav_bar">
			<ul id="ul-menu" >
			<?php if(!$is_corporate_city){ ?>
            <li class="<?php echo $li_class = $is_deal_view && $get_current_city != 'turismo' && $get_current_city != 'productos' ? 'select' : '';  ?>"><?php echo $html->link ('Ofertas del D&iacute;a', array ('controller' => 'deals', 'action' => 'index', 'admin' => false), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'escape' => false)); ?></li>
            <li class="<?php echo $li_class = $get_current_city == 'turismo'   ? 'select' : '';  ?>"><a title="Viajes y Escapadas" href="/turismo">Viajes y Escapadas</a></li>
            <li class="<?php echo $li_class = $get_current_city == 'productos' ? 'select' : '';  ?>"><a title="Productos" href="/productos">Productos</a></li>
        <?php } ?>
        <li><a title="Centro de Atenci&oacute;n al Cliente" href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de Atenci&oacute;n al Cliente</a></li>
        
			</ul>
		</nav>
	</section>
</div>
</header>
</div>	
</div>