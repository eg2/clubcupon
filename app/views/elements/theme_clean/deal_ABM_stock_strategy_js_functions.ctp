<script type = "text/javascript">//<![CDATA[
    
    //
    // UTILIZADO UNICAMENTE EN EL ALTA Y EDICION DE OFERTAS
    // CONTROLAN LA SELECCION DE CAMPAÑAS Y PRODUCTOS
    //
    
  
    //Eventos
    $('#DealCompanyId').change(function() {
        if($(this).val()){
            initializeStockControlDropdowns();
        } else {
            $('.mensajeEmpresa').show().html('Por favor, seleccionar una Empresa');
            disableAndResetStrategiesCampaignsAndProductsDropdowns();
        }
    });
    
    $('#DealProductInventoryStrategyId').change(function(){
        strategyId = $(this).val();
        
        if(strategyId == '1'){
            disableAndResetCampaignsAndProductsDropdowns();
            $('.mensajeEmpresa').show().html('La estrategia de inventario de producto es ahora "Stock No Administrado" y no permite seleccionar una campa&ntilde;a');
        } else if(strategyId == '2'){
            companyId = $('#DealCompanyId').val();    
            getAndPopulateCompanyCampaigns(companyId);
        } else {
            disableAndResetCampaignsAndProductsDropdowns();
        }
        
    });
    
    $('#DealProductCampaignId').change(function(){
        campaingId = $(this).val();
        if(campaingId != ''){
            getAndPopulateCampaignsProducts();
        } else {
            $('.mensajeEmpresa').show().html('Por favor, seleccionar una campaña');
            disableAndResetProductsDropdown();
        }
    });
    
    $('#DealProductProductId').change(function(){
        productId = $(this).val();
        if(productId != ''){
            $('.mensajeEmpresa').show().html('producto seleccionado');
        } else {
            $('.mensajeEmpresa').show().html('Por favor, seleccionar un producto');
            
        }
    });
  

    //Facade de inicializacion
    function initializeStockControlDropdowns() {
        getAndPopulateCompanyCampaigns($('#DealCompanyId').val());
        disableAndResetStrategiesDropdown();
        disableAndResetCampaignsDropdown();
        disableAndResetProductsDropdown();
        setStrategiesDropdownForStockStrategy();
    }
    
    function getAndPopulateCompanyCampaigns(selectedCompanyId){
        
        console.log('getCompanyCampaigns::');
        console.log('selectedCompanyId:'+selectedCompanyId);
        
        baseUrl = "<?php echo Router::url(array('admin' => true ,'controller' => 'campaigns', 'action' => 'all_by_company_id'), false)?>";
        fullCampaignsUrl = baseUrl + '/' + selectedCompanyId;
        
        $.ajax({
          type: "GET",
          cache: false,
          url: fullCampaignsUrl,
          data: "{}",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success : function(campaigns) {
              console.log('EL CONTROLADOR DEVUELVE RESULTADOS');
              console.log(campaigns);
              populateCampaignsDropdown(campaigns);
              
          },
          error: function(error) {
              console.log('ERROR AL RECUPERAR LAS CAMPAÑAS\n' + error);
              return NULL;
          }
        });
      
    }
    
    
    function populateCampaignsDropdown(jSonData) {
      
      
      console.log('populateCampaignsDropdown::');
      
        if(jSonData != null) {
                var listItems= '<option value="">Seleccione una campaña</option>';
                for (var i = 0; i < jSonData.length; i++) {
                    listItems+= "<option value='" + jSonData[i].ProductCampaign.id + "'>" + jSonData[i].ProductCampaign.name + "</option>";
                }
                $("#DealProductCampaignId").html(listItems);
                
                dealStockStrategy = $('#DealProductInventoryStrategyId').val();
                
                if(dealStockStrategy == 1) {
                    disableCampaignsDropdown();
                    $('.mensajeEmpresa').show().html('La estrategia de inventario de producto es ahora "Stock No Administrado" y no permite seleccionar una campa&ntilde;a');
                }else if(dealStockStrategy == 2) {
                    enableCampaignsDropdown();
                    $('.mensajeEmpresa').show().html('La estrategia de inventario de producto es ahora "Stock Administrado".<br /> Por favor, seleccione una campa&ntildea. ');
                } else {
                    $('.mensajeEmpresa').show().html('Por favor, seleccionar una estrategia de inventario de producto');
                }
                
                dealInitialCampaign = $('#DealSecretProductCampaignId').val();
                if(dealInitialCampaign && dealUsesStockControlStrategy()) {
                  $('#DealProductCampaignId').val(dealInitialCampaign);
                }
                
                
                getAndPopulateCampaignsProducts();
                
                
            } else {
                //Reset general para el first run
                disableAndResetStrategiesCampaignsAndProductsDropdowns();
                
                if($('#DealCompanyId').val()){
                    //Dejamos seteada la estrategia
                    setStrategiesDropdownForNoStockStrategy();
                    $('#DealCompanyId').focus();
                    $('.mensajeEmpresa').html('La estrategia de inventario de producto es ahora "Stock No Administrado" dado que esta empresa no posee campa&ntilde;as activas');
                }
                
        }
    }
    
    function getAndPopulateCampaignsProducts(){
        
        console.log('getAndPopulateCampaignsProducts::');
        
        selectedCampaignId = $('#DealProductCampaignId').val();
        baseUrl = "<?php echo Router::url(array('admin' => true ,'controller' => 'products', 'action' => 'all_selectables_by_campaign_id'), false)?>";
        fullCampaignsUrl = baseUrl + '/' + selectedCampaignId;
        
        $.ajax({
          type: "GET",
          cache: false,
          url: fullCampaignsUrl,
          data: "{}",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success : function(products) {
              console.log('EL CONTROLADOR DE PRODUCTOS RESPONDE');
              console.log(products);
              
              dealStockStrategy = $('#DealProductInventoryStrategyId').val();
              dealProductCampaignId = $('#DealProductCampaignId').val();
                
                if(dealStockStrategy == 2 && dealProductCampaignId != '' ) {
                  populateProductsDropdown(products);
                }
                
          },
          error: function(error) {
              console.log('ERROR AL RECUPERAR LOS PRODUCTOS\n' + error);
              return NULL;
          }
        });
        
    }
    
    function populateProductsDropdown(jSonData) {
      
        console.log('populateProductsDropdown::');
      
        if(jSonData != null) {
            var listItems= '<option value="">Seleccione un producto</option>';
                for (var i = 0; i < jSonData.length; i++) {
                    listItems+= "<option value='" + jSonData[i].ProductProduct.id + "'>" + jSonData[i].ProductProduct.name + "</option>";
                }
                $("#DealProductProductId").html(listItems);
                $('.mensajeEmpresa').show().html('Por favor, seleccionar un producto');
                enableProductsDropdown();
                
                dealInitialproduct  = $('#DealSecretProductProductId').val();
                if(dealInitialproduct) {
                  $('#DealProductProductId').val(dealInitialproduct);
                }
                
        } else {

            $('.mensajeEmpresa').html('Esta campa&ntilde;a no cuenta aun con productos cargados, por favor, seleccione otra');

            $('#DealProductCampaignId').focus();
             disableAndResetProductsDropdown();
        }
    }
  
  
    function dealUsesStockControlStrategy() {
        return $('#DealProductInventoryStrategyId').val() == 2;
    }
  
    function disableAndResetStrategiesCampaignsAndProductsDropdowns() {
        //Strategies
        disableAndResetStrategiesDropdown();
        //Campaigns
        disableAndResetCampaignsDropdown();
        //Products
        disableAndResetProductsDropdown();
    }
    
    function disableAndResetCampaignsAndProductsDropdowns() {
        disableAndResetCampaignsDropdown();
        disableAndResetProductsDropdown();
    }
  
    
    function setStrategiesDropdownForNoStockStrategy() {
        $('#DealProductInventoryStrategyId').val(1);
        disableStrategiesDropdown();
    }
    
    function setStrategiesDropdownForStockStrategy() {
        dealInitialStockStrategy = $('#DealSecretProductInventoryStrategyId').val();
        enableStrategiesDropdown();
        if(dealInitialStockStrategy == 1) {
            disableCampaignsDropdown();
        }
        $('#DealProductInventoryStrategyId').val(dealInitialStockStrategy);
    }
    
    //DD RESET Strategies
    function disableAndResetStrategiesDropdown() {
        $('#DealProductInventoryStrategyId').val(0);
        disableStrategiesDropdown();
    }
    
    //DD RESET Campaigns
    function disableAndResetCampaignsDropdown() {
        $('#DealProductCampaignId option:nonEmptyValue').remove();
        $('#DealProductCampaignId').val(0);
        disableCampaignsDropdown()
    }
    
    //DD RESET PRODUCTS
    function disableAndResetProductsDropdown() {
        $('#DealProductProductId option:nonEmptyValue').remove();
        $('#DealProductProductId').val(0);
        disableProductsDropdown();
    }
    
    //Recorre y devuelve las opciones que tienen un valor asignado
    $.expr[':'].nonEmptyValue = function(obj) {
        return $(obj).val() != '';
    };
  
    //DD Estrategias
    function enableStrategiesDropdown() {
        $('#DealProductInventoryStrategyId').removeAttr('readonly');   
        $('#DealProductInventoryStrategyId').removeClass('disabledCombobox');
    }
    function disableStrategiesDropdown() {
        $('#DealProductInventoryStrategyId').attr('readonly', 'readonly');
        $('#DealProductInventoryStrategyId').addClass('disabledCombobox');
    }
    
    //DD Campañas
    function enableCampaignsDropdown() {
        $('#DealProductCampaignId').removeAttr('readonly');
        $('#DealProductCampaignId').removeClass('disabledCombobox');
    }
    function disableCampaignsDropdown() {
        $('#DealProductCampaignId').attr('readonly', 'readonly');
        $('#DealProductCampaignId').addClass('disabledCombobox');
    }
    
    //DD Productos
    function enableProductsDropdown() {
        $('#DealProductProductId').removeAttr('readonly');
        $('#DealProductProductId').removeClass('disabledCombobox');
    }
    function disableProductsDropdown() {
        $('#DealProductProductId').attr('readonly', 'readonly');
        $('#DealProductProductId').addClass('disabledCombobox');
    }


    //CON TODO SETEADO, INICIALIZAMOS. ES INDISTINTO SI ES ALTA, EDICION
    initializeStockControlDropdowns();

//]]></script>

<style>
    .disabledCombobox{
        background:#eee;
        color:#aaa;
    }
</style>
    
    