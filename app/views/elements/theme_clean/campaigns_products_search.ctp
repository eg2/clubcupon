<style>
    .buscador {
        font-weight: bold;
        padding:5px 15px;
        clear:both;
        width:680px;
        }
        .buscador .label{ padding: 0; }
        .buscador .submit{ margin:10px 0 0 0;}
    .separador{padding-top:15px;}
</style>
<div class="buscador">
    <span class="title2">Buscador de campa&ntilde;as</span>
    <?php echo $form->create('campaigns', array('class' => 'normal', 'action' => 'index')); ?>
        <table>
            <tr>
                <td>
                    <?php echo $form->input ('company_id', array('label' => false, 'options' => $companiesList, 'escape' => false, 'empty' => 'Seleccione una empresa', 'class' => 'listado', 'default' => $searchDefaultCompany)); ?>
                </td>
                <td class="label">Nombre Campa&ntilde;a:</td>
                <td>
                    <?php echo $form->input ('campaign_name', array('label' => false, 'value'=> $searchDefaultCampaignName)); ?>
                </td>
                <td><?php echo $form->submit('Buscar Campaña'); ?></td>
            </tr>
        </table>
    <?php echo $form->end(); ?>
</div>

<div class="buscador separador">
    <span class="title2">Buscador de productos</span>
    <?php echo $form->create('products', array('class' => 'normal', 'action' => 'search')); ?>
        <table>
            <tr>
                <td>
                    <?php echo $form->input ('campaign_id', array('label' => false, 'options' => $campaignsList, 'escape' => false, 'empty' => 'Seleccione una campa&ntilde;a', 'class' => 'listado', 'default' => $searchDefaultCampaign)); ?>
                </td>
                <td class="label">Nombre producto:</td>
                <td>
                    <?php echo $form->input ('product_name', array('label' => false)); ?>
                </td>
                <td><?php echo $form->submit('Buscar Producto'); ?></td>
            </tr>
        </table>
    <?php echo $form->end(); ?>
</div>
<hr />