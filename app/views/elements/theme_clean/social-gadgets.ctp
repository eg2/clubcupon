<div class="grid_16 bloque-horizontal">
    <div class="bloque-horizontal-contenido clearfix">
        
            <?php
                if (Configure::read('facebook.is_facebook_feed_enabled'))
                {
                    echo '<div class="bloque-h-social">';
                    echo str_replace('###FANPAGE_URL###', urlencode($city_facebook_url), Configure::read('facebook.feeds_code'));
                    echo '</div>';
                }
            ?>
            
                
        <?php
            

            if (isset($get_current_city)) {
                if (Configure::read('twitter.is_twitter_feed_enabled')) {
                    echo '<div class="bloque-h-social social-tw">';
                    echo'<div class="tw-head">
				<img class="logocctw" src="/img/theme_clean/cc-logotw.jpg" alt="Grupo Clarin">
			</div>';
        ?>

                    <script src="http://widgets.twimg.com/j/2/widget.js"></script>
                    <script>
                      new TWTR.Widget (
                        {
                          version: 2,
                          type: 'search',
                          rpp: 3,
                          search: 'from:<?php echo $city_twitter_username; ?>',
                          interval: 6000,
                          title: '¡Todos los días una alegría!',
                          subject: '<a href = "http://twitter.com/<?php echo $city_twitter_username; ?>">ClubCupón</a>',
                          width: 'auto',
                          height: 158,
                          theme:
                            {
                              shell:  { background: '#ffffff', color: '#303030'                   },
                              tweets: { background: '#ffffff', color: '#333333', links: '#0098CA' }
                            },
                          features: { scrollbar: false, loop: false, live: true, hashtags: true, timestamp: false, avatars: false, toptweets: false, behavior: 'all' }
                        }).render ().start ();
                    </script>



        <?php
               echo '</div>';
               }
            }
        ?>

        <div class="bloque-h-social">
            <div class="centroatencion">
                <div class="centroat-contenido clearfix">
                    <h2>Centro de Atenci&oacute;n al Cliente</h2>
                    <a href="http://soporte.clubcupon.com.ar/" target="_blank" >Contactanos</a>
                </div>
            </div>

            <div class="agregarcc">
                <div class="agregarcc-contenido clearfix">
                <a href="http://soporte.clubcupon.com.ar/customer/portal/emails/new" target="_blank">Agreg&aacute; tu Empresa a Club Cup&oacute;n</a>
                </div>
            </div>
            
            <?php if (Configure::read('now.is_menu_enabled')) {?>
            <div class="bannerccya">
            <?php
         		   echo $html->link($html->image('now/banner-LandingEmpresas.jpg'),
 							array ('controller' => 'now_registers', 'plugin'=>'now','action' => 'landing' ), array('escape' => false));
 							
            ?>
            </div>
            <?php }?>
        </div>		
        
                    
    </div>
</div>