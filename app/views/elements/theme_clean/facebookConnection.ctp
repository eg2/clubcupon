<script>
    $(document).ready(function() {

        // Additional JS functions here
        window.fbAsyncInit = function() {
            var api_key=__cfg('api_key');
            var channel='<?php echo STATIC_DOMAIN_FOR_CLUBCUPON; ?>';

            //alert('api_key:'+api_key+'channel:'+channel);
            
            FB.init({
                appId: api_key, // App ID
                channelUrl: channel, // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });


        };

        function doFacebookLogin() {
            fbAsyncInit();
            FB.login(function(response) {
                //alert('response.authResponse:'+response.authResponse);
                if (response.authResponse) {
                    // connected
                    
                    FB.api('/me', function (response) {
                        //alert('Welcome, ' + response.name + "!");
                        //alert('Your email id is : '+ response.email);
                        if (!response.email) {
                            alert('No es posible obtener tu mail en Facebook. Por favor completá tu mail para avanzar con la compra.');
                        } else {
                            //alert('Ok.');
                            var goTo = '/users/login';
                            goTo += '?isFacebookLogin=true&f=' + window.location.href;
                            window.location.href = goTo;
                        }
                    });
                } else {
                    //console.log('falla en la conexion.');
                }
            }, { 
                scope: 'public_profile,basic_info,email,contact_email,user_friends',
                return_scopes: true
            });
        }

        function doFacebookSubscribe() {
            var subscription__city_id = $('#SubscriptionCityId').val();
            var subscription__utm_source = $('#SubscriptionUtmSource').val();
            var subscription__from = $('#SubscriptionFrom').val();
            var subscription__landing = $('#SubscriptionLanding').val();

            fbAsyncInit();
            FB.login(function(response) {
            	//alert('response.authResponse:'+response.authResponse);
                if (response.authResponse) {
                    // connected
                    var goTo = '/subscriptions/add';
                    var params = '?required_facebook_subscribe=true' +
                            '&subscription__city_id=' + subscription__city_id +
                            '&subscription__from=' + subscription__from +
                            '&subscription__utm_source=' + subscription__utm_source +
                            '&subscription__landing=' + subscription__landing;

                    goTo += params;
                    window.location.href = goTo;
                } else {

                }
            }, {
                scope: 'email'
            });
        }

        /*Ideal para testear si la sesion con facebook fue exitosa*/
        /*
         function testAPI(){
         console.log('Recuperando datos.... ');
         FB.api('/me', function(response) {
         console.log('Buenas, ' + response.name + '.');
         });
         }
         */
        // Load the SDK Asynchronously
        (function(d) {
            var js, id = 'facebook-jssdk',
                    ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

        //Disparamos la conexion
        $('#facebookLoginButton').click(function() {
            doFacebookLogin();
        });

        $('#facebookSubscribeButton').click(function() {
            doFacebookSubscribe();
        });

    });
</script>