<h1>MI CUENTA</h1>

<div class="menu-submenu">

    <ul class="bloque-menu clearfix">
        <?php
            if($auth->user('user_type_id') == ConstUserTypes::Company)
            {
                $user = $html->getCompany($auth->user('id')); 
                echo '<li>' . $html->link('Mis Datos',              array('controller' => 'companies',     'action' => 'edit',$user['Company']['id']),   array('title' => 'Mis Datos' )) . '</li>';
            }
            else
            {
                echo '<li>' . $html->link('Mi Cuenta',   array('controller' => 'user_profiles', 'action' => 'edit',$auth->user('id')), array('title' => 'Mis Datos','class' => strrpos($this->pageTitle, __l('Edit Profile'))===false?'':'menu-activo-sub')) . '</li>';
            }

            if($auth->sessionValid() && $html->isAllowed($auth->user('user_type_id')))
            {
                echo '<li>' . $html->link('Mis Cupones',            array('controller' => 'deal_users',    'action' => 'index','type'=>'available'),                         array('title' => 'Mis Cupones','class' => ($this->pageTitle == 'Mis Cupones')?'menu-activo-sub':'')) . '</li>';
                echo '<li>' . $html->link('Mis Cupones de Regalo',  array('controller' => 'deal_users',    'action' => 'gifted_deals','admin' => false, 'type'=>'available'), array('title' => 'Mis Cupones de Regalo','class' => ($this->pageTitle=='Mis Cupones de Regalo')?'menu-activo-sub':'')) . '</li>';

                if(Configure::read('friend.is_enabled'))
                {
                    echo '<li>' . $html->link('Mis amigos',         array('controller' => 'user_friends',  'action' => 'lst',   'admin' => false),       array('title' => 'Mis amigos')) . '</li>';
                    echo '<li>' . $html->link('Importar amigos',    array('controller' => 'user_friends',  'action' => 'import','admin' => false),       array('title' => 'Importar amigos')) . '</li>';
                }

            }
            if(!$session->read('is_facebook_session') && !$auth->user('is_openid_register'))
            {
                echo '<li>' . $html->link('Cambiar contrase&ntilde;a',array('controller'=> 'users', 'action'=>'change_password'),array('escape'=>false, 'title' => 'Cambiar contraseña','class' => strrpos($this->pageTitle, __l('Cambio de contraseña'))===false?'':'menu-activo-sub')) . '</li>';
            }

            echo '<li>' . $html->link ('Suscripciones', array ('controller' => 'subscriptions', 'action' => 'manage_subscriptions', 'admin' => false),array('title' => 'Suscripciones')) . '</li>';
        ?>
    </ul>
    
</div>