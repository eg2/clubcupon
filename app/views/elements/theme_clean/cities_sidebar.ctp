<?php

    $cities = $clubcupon->activeCitiesWithActiveDeals ();
    foreach ($cities as $city) {
        if ($city['City']['is_selectable'] == 0) {
            unset($city['City']);
        }
    }
    
    $groups = $clubcupon->activeGroupsWithActiveDeals();
    foreach ($groups as $group) {
        if ($group['City']['is_selectable'] == 0) {
            unset($group['City']);
        }
    }

    if(!empty($cities))
    {
        
        echo '<a href="#" class="titulo jsitem2" style="border:none;">Ciudades</a>';
        echo '<ul class="jsul2">';
        foreach($cities as $city){
            
            $class = $city['City']['name'] == $selected_city_name ? 'select': '';
            
            echo '<li>';
            echo $html->link ($city['City']['name'], array ('controller' => 'alldeals', 'action' => 'show', 'city' => $city['City']['slug'], $city['City']['id']), array ('title' => $city['City']['name'], 'class' => $class));
            echo '</li>';            
        }
        echo '</ul>';
        
    }
    
    if(!empty($groups))
    {
        
        echo '<a href="#" class="titulo jsitem1">Ofertas Especiales</a>';
        echo '<ul class="jsul1">';
        foreach($groups as $group){
            if($group['City']['name'] != Configure::read('corporate.city_name')){
                
                $class = $group['City']['name'] == $selected_city_name ? 'select': '';

                echo '<li>';
                echo $html->link ($group['City']['name'], array ('controller' => 'alldeals', 'action' => 'show', 'city' => $group['City']['slug'], $group['City']['id']), array ('title' => $city['City']['name'], 'class' => $class));
                echo '</li>';
            }
        }
        echo '</ul>';
        
    }
    
?>


<script>
$(document).ready(function() {

	//$(".jsul1").slideToggle(0);
    //$(".jsul2").slideToggle(0);
				
 	$(".jsitem1").click(function() {;
	
		if($(".jsul1").css("display") == "block"){
			$(".jsitem1").css({backgroundPosition: "-3px 12px"});
		}else{
			$(".jsitem1").css({backgroundPosition: "-3px -28px"});
		}
		$(".jsul1").slideToggle();		
		return false;	
	});

 	$(".jsitem2").click(function() {;

		if($(".jsul2").css("display") == "block"){
			$(".jsitem2").css({backgroundPosition: "-3px 12px"});
		}else{
			$(".jsitem2").css({backgroundPosition: "-3px -28px"});
		}	
		$(".jsul2").slideToggle();		
		return false;
	});

 	$(".jsitem3").click(function() {;

		if($(".jsul3").css("display") == "block"){
			$(".jsitem3").css({backgroundPosition: "-3px 12px"});
		}else{
			$(".jsitem3").css({backgroundPosition: "-3px -28px"});
		}					
		$(".jsul3").slideToggle();		
		return false;
	});	

});  //fin doc ready
</script>