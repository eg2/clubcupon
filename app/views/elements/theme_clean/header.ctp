<?php
    if(Configure::read('themeBrown.enabled')){ 
        echo $this->element(Configure::read('theme.theme_name') . 'themeBrown');
    }
?>

<script type="text/javascript">
    var ClickTaleSSL=1;
    if(typeof ClickTale=='function') ClickTale(162,0.12,"www14");
</script>
<!-- ClickTale end of Bottom part -->

<?php
    if(!isset($is_corporate_city)) {
      $is_corporate_city = false;
    }
    if(!isset($is_deal_view)) {
      $is_deal_view = false;
    }
    if(!isset($get_current_city)) {
      $get_current_city = false;
    }

    if ($auth->sessionValid () && $auth->user ('user_type_id') == ConstUserTypes::Company)
    {
    $company = $html->getCompany ($auth->user ('id'));
    }

    $cities = $clubcupon->activeCitiesWithDeals ();
    if ($cities == null){
        $cities = array();
    }
    $groups = $clubcupon->activeGroupsWithDeals ();
    if ($groups == null){
        $groups = array();
    }
    //ya que los grupos van a continuacion de las ciudades, unificamos en 1 solo arreglo.
    $results = array_merge($cities, $groups );

?>
<?php if($is_corporate_city){ ?>
<style>
    .finalizada{
        display: none;
    }
</style>
<?php } ?>
<?php if($is_corporate_city && $deal ['Deal']['only_price']&& !$deal ['Deal']['hide_price']){ ?>
    <style>
        .col-izq .modulos-compra ul.precio{
            background:none!important;
            border:none!important;
            height:0!important;
            margin:0!important;
        }
    </style>
<?php } ?>

<?php if($is_corporate_city){ ?>
    <style>
        #header .nav_bar ul{margin:0 156px 0 0;}
        #header .nav_bar li{float:right!important;}
    </style>
    <!--[if IE]>
        <style>
            #cboxTopLeft, #cboxTopCenter, #cboxTopRight{height:0px!important;}
            #cboxBottomLeft, #cboxBottomCenter, #cboxBottomRight{height:0px!important;}
        </style>
    <![endif]-->
    <!--[if IE 8]>
        <style>
            
            #home-v2-exclusive .content-der{margin-left:0!important;}
        </style>
    <![endif]-->
<?php } ?>


<!--[if IE 7]>
    <style>
        #header{
            height:110px !important;
        }

        #header .nav_bar{
            margin-top:-2px;
        }
    </style>
    <?php if($is_corporate_city){ ?>
        <style>
            
            #cboxBottomLeft, #cboxBottomCenter, #cboxBottomRight{height:0px!important;}
            #header .nav_bar{
                margin-top:4px!important;
                }
            .ftm-p{
                overflow-y: hidden;
                }
            #globo_email{
                left:160!important;
                top:470px!important;
            }
            #header .nav_bar li .select{
                margin-top:0!important;
            }
        </style>
    <?php }else{ ?>
        <script>
           $(".nav_bar ul li a").corner("top");
        </script>
    <?php } ?>
<![endif]-->

<!--[if IE 8]>
    <style>
    #header{
        height:105px !important;
    }
    </style>
    <?php if($is_corporate_city){ ?>
        <style>
            #header .nav_bar{
                margin-top:4px!important;
                }
            .ftm-p{
                overflow-y: hidden;
                }
            .select{
                padding:8px 13px;
                margin-top:-8px;
            }
        </style>
    <?php }else{ ?>
        <script>
           $(".nav_bar ul li a").corner("top");
        </script>
    <?php } ?>
<![endif]-->
<?php if($is_corporate_city){ ?>
    <script>
        $(document).ready(function() {
           //reemplazo de imagen, acorde a exclusive
           $('img.finalizada').attr("src",'/img/theme_clean/exclusive/finalizada.png').show('slow');

        });
    </script>
<?php } ?>

<div id="header">
	<div class="container_16 center clearfix pos-r fix_ie_menu">
		<div id="title_logo" class="grid_3">

                    <?php

                        $logo_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'web/clubcupon/';

                        if (Configure::read ('Actual.city_is_group') == 0 && !$is_corporate_city)
                        {
                            echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => 'CLUB CUPÓN.')), array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                        }
                        else
                        {
                            if($hideSubscription) {
                                echo $html->link ($html->image ($logo_url.'logo.png', array ('alt' => 'CLUB CUPÓN.')), array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN.'));
                            } else {
                                echo '<a href="#">'.$html->image ($logo_url.'logo.png', array ('alt' => 'CLUB CUPÓN.')).'</a>';
                            }
                        }
                    ?>

		</div>
        
		<?php if(!$is_corporate_city || !$is_deal_view ){ ?>
                    <div class="grid_9 navigation">
                        <?php 
                            $fromSearchPg = 'no';
                            if ($this->action == 'makesearch') {
                                $fromSearchPg = 'yes';
                                $get_current_city = $city_s_slug;
                            }
                            $query = '';
                            if (isset($search)) {
                                if ($search['Query']['Q'] != '*:*') {
                                    $query = $search['Query']['Q'];
                                }
                            }
                        ?>
                        <!-- <form class="headerInputQuery" action='/search/search_querys/makesearch' id="searchFormRight" accept-charset="utf-8" method="get">
                            <input class="qSearch" id="q" name="q" type="text" value="<?php echo $query; ?>"  placeholder="¿Qué estás buscando?"/>
                            <input type="hidden" id="qr" name="qr" value="reset"/>
                            <input type="hidden" id="currentCitySlug" name="currentCitySlug" value="<?php echo $get_current_city; ?>"/>
                            <input type="hidden" id="fromSearchPg" name="fromSearchPg" value="<?php echo $fromSearchPg; ?>"/>
                        </form> -->
                        <!-- <script type="text/javascript">
                        c    var autocompFField = '<?php echo  $GLOBALS['autocomplete']['FacetField']?>';
                            var autocompItems = <?php echo  $GLOBALS['autocomplete']['Items']?>;
                            var extraFilter = '<?php echo  $GLOBALS['extraFilters'][0]?>';
                            var autocompSolrAsyncUrlBridge =  '<?php echo  $GLOBALS['autocomplete']['SolrAsyncUrlBridge']?>';
                            var autocompSolrAsyncUrl = '<?php echo  $GLOBALS['autocomplete']['SolrAsyncUrl']?>';
                            initAutocomplete();
                        </script> -->
                        <!-- <span class="posimg">
                            <a href="#" class="city"><?php echo $city_name; ?></a>
                        </span> -->
                    </div>
                <?php 
                
                }
                //Usada para compensar cuando borro algo

                if($city_is_group && !$is_corporate_city )
                {
                    //$push_class = 'push_4';
                    $push_class = '';
                }
                else if($is_corporate_city)
                {
                    ?>
                    <style>
                        #header .text{
                            color: #B0AC6D!important;
                        }
                        #header .ingresa-tucorreo{
                            border:3px solid #B0AC6D!important;
                        }
                        
                        span.sprite-mas{
                            background: url("/img/theme_clean/sprite.png") no-repeat scroll -4px -1869px transparent!important;
                        }
                        
                        .ingresa-tucorreo form input.submit{
                            background: #d3d1a0; /* Old browsers */
                            background: -moz-linear-gradient(top, #d3d1a0 0%, #a7a268 50%, #756f3c 100%); /* FF3.6+ */
                            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d3d1a0), color-stop(50%,#a7a268), color-stop(100%,#756f3c)); /* Chrome,Safari4+ */
                            background: -webkit-linear-gradient(top, #d3d1a0 0%,#a7a268 50%,#756f3c 100%); /* Chrome10+,Safari5.1+ */
                            background: -o-linear-gradient(top, #d3d1a0 0%,#a7a268 50%,#756f3c 100%); /* Opera 11.10+ */
                            background: -ms-linear-gradient(top, #d3d1a0 0%,#a7a268 50%,#756f3c 100%); /* IE10+ */
                            background: linear-gradient(to bottom, #d3d1a0 0%,#a7a268 50%,#756f3c 100%); /* W3C */
                            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d3d1a0', endColorstr='#756f3c',GradientType=0 ); /* IE6-9 */
                            border: 1px solid #0E0F13!important;
                        }
                        .opciones-login .corporate {color:#B0AC6D;}
                    </style>
                    <?php
                }
                else
                {
                    $push_class = '';
                }
                //Refactorizar! ... TODO ABSOLUTAMENTE TODO ES PARA TIRAR Y REHACER
                if($is_corporate_city && $escape_header){
                     $push_class = 'push_4';
                }


                ?>

                    
                <?php if ($is_corporate_city && !in_array($corporateCity['City']['slug'], Configure::read('corporate_exceptions.city_name'))){ ?>
						<div class="grid_4 opciones-login <?php echo $push_class; ?>">
    	                    <ul class="ingresa">
                            <style>
                                ul.mi-cuenta{top:104px!important;}
                            </style>
                                <li style="margin-right: 103px;margin-top: -32px; padding-left: 9px;">
                                    <?php 
                                        if($corporateCity['City']['image']) { 
                                            $cityLogoURL = $corporateCity['City']['image'];
                                        } else if(file_exists("img/theme_clean/exclusive/logo-".$corporateCity['City']['slug'].".png")) {
                                            $cityLogoURL = "/img/theme_clean/exclusive/logo-". $corporateCity['City']['slug'].".png";
                                        }
                                    ?>
                                    <img src="<?php echo $cityLogoURL; ?>" title="<?php echo $corporateCity['City']['name']; ?>"/>
                                </li>
	                        </ul>
						</div>
                <?php } ?>
                            
                                
            	<?php if (!$is_corporate_city){ ?>
                	<?php if ($auth->sessionValid ()){ ?>
                		<ul id="login" class="logged opciones_login parent">
	                    	<li class="<?php echo $is_corporate_city  ? 'corporate' : ''; ?>">
	                    		<span class="loginEmail">
		                        	<?php
		                            	echo ($auth->user ('email') ? $auth->user ('email') : $html->link ('Mi Perfil', array ('plugin' => '', 'controller' => 'user_profiles', 'action' => 'edit'), array ('title' => 'Mi perfil', 'rel' => 'nofollow'))) ;
		                            ?>
		                        </span>    
	                        </li>
                      	</ul>
                   	<?php }else{ ?>
                    	<ul id="login">
							<li><?php echo $html->link ('Ingresar', array ('plugin' => '', 'controller' => 'users', 'action' => 'login'), array ('title' => 'Ingresar', 'rel' => 'nofollow', 'class'=> 'item-salir')); ?></li>
							<li>|</li>
    	                	<li><?php echo $html->link('Registrarse', array('plugin' => '','controller' => 'users', 'action' => 'register'), array('title' => __l('Signup'), 'class' => 'item-salir')); ?></li>
			    		</ul>
                    <?php } ?>
				<?php } ?>

			<!-- UL desplegable Cuenta LOGUEADO -->
            <div id="desplegable_login">
				<img src="/img/web/clubcupon/flecha_top.png" class="flecha" alt="flecha">
				<div class="menu_final">
					<?php echo $this->element('dashboard/user_menu_links'); ?>
            	</div>
            </div>
			<!-- FIN UL desplegable Cuenta LOGUEADO -->


        <?php

        $push_div = Configure::read('now.is_menu_enabled') &&  !$is_corporate_city ? 'push_1' : 'push_2 ';

        ?>

		<div id="menu_top" class="<?php echo $push_div; ?> grid_15 nav_bar">
			<ul>
			    <li><a href="/ofertas-nacionales">Ofertas Nacionales</a></li>
    			<li><a href="/turismo">Turismo</a></li>
    			<li><a href="/cel/landing">Móviles</a></li>
    			<li><a href="/ciudad-de-buenos-aires/pages/promocinoes">Promociones</a></li>
			    
			    
			    <?php /*
			                <!--
                            <?php if(!$is_corporate_city){ ?>
                                <li>
                                    <?php
                                    
                                        if($is_hidden_city)
                                        {
                                            echo $html->link ('Ofertas nacionales', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>'ofertas-nacionales'), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> $li_class, 'escape' => false));
                                        }
                                        else
                                        {
                                            $li_class = $get_current_city == 'ofertas-nacionales' ? 'select' : '';
                                            echo $html->link ('Ofertas nacionales', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>'ofertas-nacionales'), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> $li_class, 'escape' => false));
                                        }
                                    ?>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'turismo'   ? 'select' : '';  ?>
                                    <a title="Viajes y Escapadas" href="/turismo" class="<?php echo $li_class; ?>">Turismo</a>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'productos' ? 'select' : '';  ?>
                                    <a title="Productos" href="/productos" class="<?php echo $li_class; ?>">Productos</a>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'especial-belleza' ? 'select' : '';  ?>
                                    <a title="Belleza" href="/especial-belleza" class="<?php echo $li_class; ?>">Belleza</a>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'festival-gastronomico' ? 'select' : '';  ?>
                                    <a title="Gastronom&iacute;a" href="/festival-gastronomico" class="<?php echo $li_class; ?>">Gastronom&iacute;a</a>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'especial-indumentaria' ? 'select' : '';  ?>
                                    <a title="Indumentaria" href="/especial-indumentaria" class="<?php echo $li_class; ?>">Indumentaria</a>
                                </li>
                                <li>
                                    <?php $li_class = $get_current_city == 'exclusive' ? 'select' : '';  ?>
                                    <a title="Vinos" href="/feria-navidena" class="<?php echo $li_class; ?>">Vinos</a>
                                </li>
                            <li>
                                <?php
                                		if(!Configure::read('navidad.is_menu_enabled'))
                                        {
                                    //$li_class = strtolower($this->params['pass'][0]) == 'novedades' ? 'select' : '';
                                		$li_class = strtolower($this->params['url']['url']) == 'cel/landing' ? 'select' : '';

                                    ?>
                                    <a title="M&oacute;viles" href="<?php echo FULL_BASE_URL;?>/cel/landing" class="<?php echo $li_class; ?>">M&oacute;viles</a><?php
                                 }
                                ?>
                            </li>
                            <li>
                            <?php
                                if(Configure::read('navidad.is_menu_enabled') &&  !$is_corporate_city)
                                {
                                	$li_class = $get_current_city == 'especial-navidad' ? 'select' : '';
                                    echo '<a title="Especial Navidad" href="'.FULL_BASE_URL.'/especial-navidad" class="'.$li_class.'">Navidad</a>';
                                }
                            ?>
                            </li>
                            <?php } ?>
                            <li>
                                <?php
                                    if(Configure::read('now.is_menu_enabled') &&  !$is_corporate_city)
                                    {
                                        echo '<a title="Club Cup&oacute;n YA" href="'.FULL_BASE_URL.'/ya" >Club Cup&oacute;n YA</a>';
                                    }
                                ?>
                            </li>

                    -->
					*/?>
                </ul>
		</div>
                    
	</div>
	<div id="backgroundNav">
    </div>
</div>

<?php /*

<!-- Container para desplegable-->

<div class="container_16 content-desplegable">
	<div class="grid_16 desplegable">

                        <?php

                            if (!empty ($results))
                            {
                            echo '<ul class="clearfix" >';
                            //Ordeno el array dado a que el modelo no esta reconociendo el orden default.
                            $cities_clean_array = array();
                                foreach ($results as $city){array_push($cities_clean_array , array($city['City']['name'],$city['City']['slug'] ));}
                                $counter = 0;
                                for($a=0; $a < count($cities_clean_array); $a++)
                                {
                                    if($counter == 0 ) {echo '<li>';}
                                    echo $html->link(
                                        $cities_clean_array[$a][0], 
                                        array (
                                            'plugin' => '', 
                                            'controller' => 'deals', 
                                            'action' => 'index', 
                                            'city' => $cities_clean_array[$a][1]
                                        ), 
                                        array ('title' => $cities_clean_array[$a][0])
                                    );
                                    if($counter >= 3 ) {echo '</li>'; $counter = 0;} else{ echo ' | '; $counter++;}

                                }
                            echo '</ul>';

                            //para bajar la segunda lista
                            echo '<div style="clear:both;"></div>';
                            echo '<a href="#" class="btn-cerrar">x Cerrar</a>';
                            }

                        ?>

	</div>
 
	<!-- itemdesplegable -->
	<div class="jscontentmenu">
		<div class="item-desplegable">
			<span>&#9668;</span>
			<a href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de Atenci&oacute;n al Cliente</a>
		</div>
	</div>	
</div>


</div>
*/?>

<style>
    #mobileAd{
        display:none;
        position:absolute;
        z-index:99;
        top:50px;
        left:50%;
        }
        
        #mobileAd .ad{
            margin-left:-50%;
            height:90px;
            width:560px;
            padding:14px 20px;
            background: #fffde3;
            border-radius: 20px;
            border:2px solid #e3c624;
            position: relative;
            }
            
            #mobileAd .ad .close{
                position: absolute;
                right: -8px;
                width: 36px;
                height: 34px;
                background: #fff;
                top: -8px;
                border-radius: 46px;
                }
            
            #mobileAd .ad .logoIcon{
                width: 46px;
                height: 48px;
                float: left;
                margin: 20px 0;
            }
            #mobileAd .ad .text {
                width: 305px;
                margin: 5px 215px 5px 65px;
                padding-top: 10px;
                }
                #mobileAd .ad .text, #mobileAd .ad .text a{
                    font-size: 17px!important;
                    color:#395596;                    
                    }
                #mobileAd .ad .text a{
                    font-weight: bold;
                    }
                
            #mobileAd .ad .downloadLink {
                float:right;
                width:170px;
                margin-top:15px;
                }
                #mobileAd .ad .downloadLink a{
                    height:15px;
                    border-radius: 15px;
                    display:block;
                    width:126px;
                    color:#fff;
                    font-weight: bold;
                    padding: 15px 22px 22px 22px;
                    font-size: 18px;
                    text-align: center;
                    
                    background: #45494f; /* Old browsers */
                    background: -moz-linear-gradient(top, #45494f 0%, #020203 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#45494f), color-stop(100%,#020203)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(top, #45494f 0%,#020203 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(top, #45494f 0%,#020203 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(top, #45494f 0%,#020203 100%); /* IE10+ */
                    background: linear-gradient(to bottom, #45494f 0%,#020203 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45494f', endColorstr='#020203',GradientType=0 ); /* IE6-9 */
                    
                    }
        
</style>


<div id="mobileAd">
    <div class="ad">
        
        <div class="close">
            <a href="#"><img src="/img/colorbox/cross.png" width="40" /></a>
        </div>
        
        <div class="logoIcon">
            <img />
        </div>
        
        <div class="downloadLink">
            <a>Descargar</a>
        </div>
        
        <div class="text">
            Descarg&aacute; la aplicaci&oacute;n de <strong>Club Cup&oacute;n</strong> para tu celular y disfrut&aacute; de las mejores ofertas.
        </div>
        
    </div>
</div>

<!-- FIN Container para desplegable-->
<script>
$(document).ready(function() {

            $("input[name='data[Subscription][email]']").bind({
            'click': function(){
                $(this).val('');
            },
            'blur': function(){
                if($(this).val()==''){
                    $(this).val('Ingresá tu mail');
                }
            }
        });

	$('.item-desplegable').mouseover(function() {
	  $(".item-desplegable").animate({
	    right: '0'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	$('.jscontentmenu').mouseleave(function() {
	  $(".item-desplegable").animate({
	    right: '-194'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	$('.item-desplegable-search').mouseover(function() {
	  $(".item-desplegable-search").animate({
	    right: '0'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	$('.jscontentmenusearch').mouseleave(function() {
        if (!($(".item-desplegable-search input").is(":focus"))) {
    	  $(".item-desplegable-search").animate({
    	    right: '-194'
    	  }, 200, function() {
    	    // Animation complete.
    	  });
        }
	});

    $(".item-desplegable-search input").focusout(function(){
        setTimeout(function (){
          $(".item-desplegable-search").animate({
            right: '-194'
          }, 200, function() {
            // Animation complete.
          });
        }, 1000);
    });

    // //////////////////////////////////////////

    $('#mobileAd .ad .close a').click(function(){
        $('#mobileAd').toggle();
    })

    settingsForMobileAd = returnSettingsForMobileAd();
    if(settingsForMobileAd) {
        renderMobileAd(settingsForMobileAd);
    }

    
	//compatibilidad plugin web
    $('.parent').click(function(event){
		event.stopPropagation();
		
		$(this).children('.desplegable').slideDown();
		$(this).children('.flecha').show();
	});

	 $('html').click(function(e){
        e.stopPropagation();
        if($(e.target).parents('.desplegable').length==0) {
        	if($('.desplegable:visible').length>0) $('.desplegable').slideUp(200);
        	$('.parent').removeClass('open');
        	$('.parent .flecha').hide();
        }
        if($(e.target).parents('#desplegable_login').length==0) {
        	if($('#desplegable_login').length>0) $('#desplegable_login').slideUp(200);
        	$('.parent').removeClass('open');
        }

    });

 	$(".opciones_login").click(function(event) {
		event.preventDefault();
		$('.opciones_login').addClass('open');
		
		$('.especiales .desplegable').slideUp();
		$('.especiales .flecha').hide();
		$('#btn_desplegar .desplegable').slideUp();
		$('#btn_desplegar .flecha').hide();
		
		$("#desplegable_login").slideToggle();
		//$('#desplegable_login .flecha').show();
	});
    



});  //fin doc ready
</script>
