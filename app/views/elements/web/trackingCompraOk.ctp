<!-- ABR14 -->
<!-- Tracking de Google Analytics (Conversion Venta) [BEGIN] -->
<!-- Google Code for Compra Conversion Page -->
<script type = "text/javascript">
  /* <![CDATA[ */
  var google_conversion_id       = 981091258;
  var google_conversion_language = "en";
  var google_conversion_format   = "3";
  var google_conversion_color    = "ffffff";
  var google_conversion_label    = "vsVCCLaEoQIQuofp0wM";
  var google_conversion_value    = 0;
  /* ]]> */
</script>
<script type = "text/javascript"  src = "http://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
  <div style = "display: inline;">
    <img height = "1"  width = "1"  style = "border-style: none;"  alt = ""  src = "http://www.googleadservices.com/pagead/conversion/981091258/?label=vsVCCLaEoQIQuofp0wM&amp;guid=ON&amp;script=0" />
  </div>
</noscript>
<!-- Tracking de Google Analytics (Conversion Venta) [END] -->

<!-- Tracking de Google Analytics (modulo de eCommerce) [BEGIN] -->
<script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol ) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try{
  var pageTracker = _gat._getTracker("UA-4436008-37");
  pageTracker._trackPageview();
  pageTracker._addTrans(
      "<?php echo $dealExternal ['DealExternal']['id'];?>",            // order ID - required
      "ClubCupon",                                                     // affiliation or store name
      "<?php echo $dealExternal ['DealExternal']['final_amount']; ?>", // total - required
      "0.0",                                                           // tax
      "0.0",                                                           // shipping
      "<?php echo $deal ['Deal']['city_id'];?>",                                      // city
      "",                                                              // state or province
      "Argentina"                                                      // country
    );
   // add item might be called for every item in the shopping cart
   // where your ecommerce engine loops through each item in the cart and
   // prints out _addItem for each 
   pageTracker._addItem(
      "<?php echo $dealExternal ['DealExternal']['id'];?>",       // order ID - necessary to associate item with transaction
      "<?php echo $deal ['Deal']['id'];?>",                       // SKU/code - required
      "<?php echo $deal ['Deal']['id'];?>",                       // product name
      "",                                                         // category or variation
      "<?php echo $deal ['Deal']['discounted_price']; ?>",        // unit price - required
      "<?php echo $dealExternal ['DealExternal']['quantity'];?>"  // quantity - required
   );
   pageTracker._trackTrans();                                     //submits transaction to the Analytics servers
} catch(err) {}
</script>
<!-- Tracking de Google Analytics (modulo de eCommerce) [END] -->