<?php
	//var_dump($this);die();
?>

<?php
    if ($auth->sessionValid () && $auth->user ('user_type_id') == ConstUserTypes::Company)
    {
    $company = $html->getCompany ($auth->user ('id'));
    }
    
    $cities = $clubcupon->activeCitiesWithDeals ();
    $groups = $clubcupon->activeGroupsWithDeals ();
    
    //ya que los grupos van a continuacion de las ciudades, unificamos en 1 solo arreglo.
    $results = array_merge($cities, $groups );
    
?>

<div id="desplegable_ciudades_nuevo"  style="display:none; width:100%; background-color:#373737; height:100px; padding-top:0px; padding-bottom:5px;" >
    <div id="Nav" style="width:980px; height:90px; margin:0 auto; " align="center">
        <style type = "text/css">
            body { margin:0px; padding:0px; }
            UL { margin-top:0px; padding:0px;}
            #Nav LI { float: left; width:163px; line-height: 30px; font-family: Arial ; font-size: 13px; color: #FFFFFF; list-style:none; margin-left:-1px;}
            #Nav LI A { color: #FFF; display: block; border:1px dotted #999;text-decoration:none; border-top:none;  }
            #Nav LI A:hover { background-color:#ff3000; text-decoration:none; color:#fff;}
            #Nav LI A:active { background-color:#ff3000; text-decoration:none; color:#fff;}
            /*
            #Nav LI:first-child A { border-left: 1px dotted #999;}
            #Nav LI:last-child A { border-right: 1px dotted #999;}
            */
        </style>

        <?php
          if (!empty ($results))
          {
                echo '<ul>';
                //Ordeno el array dado a que el modelo no esta reconociendo el orden default.
                $cities_clean_array = array();
                foreach ($results as $city){array_push($cities_clean_array , array($city['City']['name'],$city['City']['slug'] ));}
                
                for($a=0; $a < count($cities_clean_array); $a++)
                {
                    echo '<li>' . $html->link ($cities_clean_array[$a][0], array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'city' => $cities_clean_array[$a][1]), array ('title' => $cities_clean_array[$a][0])) . '</li>';
                   
                }
                echo '</ul>';
                
                //para bajar la segunda lista
                echo '<div style="clear:both;"></div>';
          }
        ?>

    </div>    

    </div>


</div>

<div id="header">
  <div class="top_frame">
    <ul>
      <li class="right">&nbsp;</li>
      <?php
      if ($auth->sessionValid ())
        {
        ?>
        <li><?php echo $html->link ('Salir','/users/logout', array ('title' => 'Salir')); ?></li>
        <?php
        }
      else
        {
        ?>
        <li><?php echo $html->link ('Ingresar', array ('plugin'=>'','controller' => 'users', 'action' => 'login'), array ('title' => 'Ingresar', 'rel' => 'nofollow')); ?></li>
        <li><?php echo $html->link ('Ingresar con Facebook', 'javascript:void(0)', array ('onclick' => 'doFacebookLogin(); return false;', 'title' => 'Ingresar con Facebook', 'rel' => 'nofollow')); ?></li>
        <?php
        }
      ?>
<?php if(isset($my_company_url) && $my_company_url) { ?>
		  <li><?php echo $html->link ('Mi Empresa', $my_company_url, array('title' => 'Mi empresa')); ?> </li>
<?php } ?>
<?php if(isset($my_deals_url) && $my_deals_url){ ?>
          <li><?php echo $html->link ('Mis Ofertas', $my_deals_url, array ('title' => 'Mis Ofertas')); ?></li>
 <?php } ?>
<?php if(isset($my_cupons_url) && $my_cupons_url) { ?>
         <li><?php echo $html->link ('Mis Cupones', $my_cupons_url, array ('title' => 'Mis cupones')); ?></li>

 <?php } ?>

<?php
        //Habilitamos la vista de MI CUENTA para los usuarios, los admin y los superAdmin
        if ($auth->user ('user_type_id') == ConstUserTypes::User || $auth->user ('user_type_id') == ConstUserTypes::SuperAdmin || $auth->user ('user_type_id') == ConstUserTypes::Admin)
        {
            
            //Dado a que el modo de escape de URLs absolutas de Cake no anda...
            $fixed_url =  $html->link('Mi Cuenta',   array('plugin'=>'','controller' => 'user_profiles', 'action' => 'edit',$auth->user('id')));
            $fixed_url = str_replace('/admin'  , "", $fixed_url );
            echo '<li>' . $fixed_url . '</li>';
                    
                    
            
        }
        
        if (($auth->user ('user_type_id') != ConstUserTypes::Partner) && ($auth->user ('user_type_id') != ConstUserTypes::Agency))
        {
            //
        }
        
        if (ConstUserTypes::isLikeAdmin ($auth->user ('user_type_id')))
          {
          ?>
          <li><a href="/admin" title="Admin">Admin</a></li>

          <?php
          }
        else if (ConstUserTypes::isPrivilegedUser ($auth->user ('user_type_id')))
          {
          ?>
          <li><?php echo $html->link ('Admin', array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => true)); ?></li>
          <?php
          }
        ?>
        <?php
            if (!empty ($cities))
            {
        ?>
        <li class="submenu"  style = "width: 160px;">
            <?php echo $html->link ('Ciudades', 'javascript:void(0)', array ('rel' => 'other_cities', 'title' => 'Ciudades', 'id'=>'other_cities')); ?>
        </li>
        <?php
            }
        ?>
        
      <li class="left">&nbsp;</li>
  <?php
  if ($auth->sessionValid ())
    {
    echo '<li id="greet">&iexcl;Hola ' . $html->getUserLink (array ('username' => $auth->user ('username'), 'user_type_id' => $auth->user ('user_type_id'), 'id' => $auth->user ('id'))) . '!</li>';
    }
  ?>
    </ul>
  </div>

  <?php
  // bueno acá una lógica ridícula para los ridículos consejos SEO.
  $openTag = '<h1 class="titleLogo">';
  $closeTag = '</h1>';
  if (!empty ($certificaPath))
    {
    if (($certificaPath == 'ClubCupon/deal/recent') ||
            ($certificaPath == 'ClubCupon/page/Learn') ||
            ($certificaPath == 'ClubCupon/page/Who') ||
            ($certificaPath == 'ClubCupon/page/Policy') ||
            ($certificaPath == 'ClubCupon/page/Faq') ||
            ($certificaPath == 'ClubCupon/page/Terms') ||
            ($certificaPath == 'ClubCupon/page/Protection') ||
            ($certificaPath == 'ClubCupon/user/register'))
      {

      $openTag = '<h5 class="titleLogo">';
      $closeTag = '</h5>';
      }
    }
  ?>
        <?php
        echo $openTag;
        if (Configure::read ('Actual.city_is_group') == 0)
          {
          echo $html->link ($html->image ('logo.png', array ('alt' => 'CLUB CUP&Oacute;N.')), array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUP&Oacute;N.'));
          }
        else
          {
          echo $html->link ($html->image ('logo.png', array ('alt' => 'CLUB CUP&Oacute;N.')), array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUP&Oacute;N.'));
          }
        echo $closeTag;
        ?>
  <div class="right_frame">
    <h2>Las mejores ofertas en <strong><?php echo Configure::read ('Actual.city_name'); ?></strong></h2>
<?php
if (!preg_match ('/subscribe/s', $this->params['url']['url']))
  {
  ?>
      <form action="/subscribeadd" method="post">
        <input id="subscription_email_field" name="data[Subscription][email]" type="text" value="Ingres&aacute; tu mail" onclick="this.value == 'Ingres&aacute; tu mail' ? this.value = '' : '';" onblur="this.value == '' ? this.value = 'Ingres&aacute; tu mail' : '';" />
        <select name="data[Subscription][city_id]" class="orange">
  <?php foreach ($clubcupon->activeCitiesWithDeals () as $city): ?>
            <option<?php echo $city['City']['id'] == Configure::read ('Actual.city_id') ? ' selected="selected"' : ''; ?> value="<?php echo $city['City']['id']; ?>"><?php echo $city['City']['name']; ?></option>
        <?php endforeach; ?>
        </select>
        <input name="data[Subscription][from]" type="hidden" value="header" />
        <input name="" type="submit" value="SUSCRIBIRME" class="submit" onclick="$('#subscription_email_field').val() == 'Ingres&aacute; tu mail' ? $('#subscription_email_field').val('') : '';" />
      </form>
  <?php
  }
?>
  </div>
  <div class="bottom_frame">
    <div class="left"></div>
    <ul>
      <li><?php echo $html->link ('Ofertas del D&iacute;a', array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => false), array ('title' => 'Ofertas del D&iacute;a ' . Configure::read ('site.name'), 'escape' => false)); ?></li>
      <li class="separator"></li>
      <li><?php echo $html->link ('Ofertas Recientes', array ('plugin'=>'','controller' => 'deals', 'action' => 'recent', 'admin' => false), array ('title' => 'Ofertas Recientes ' . Configure::read ('site.name'), 'escape' => false)); ?></li>
      <li class="separator"></li>
      <li><a title="Viajes y Escapadas" href="/turismo">Viajes y Escapadas</a></li>
      <li class="separator"></li>
      <li><a title="Productos" href="/productos">Productos</a></li>
      <li class="separator"></li>
      <!-- <li><a title="Alegr&oacute;a" href="/alegria-club-cupon">Alegr&iacute;a</a></li>
      <li class="separator"></li> -->
      <li><?php echo $html->link ('C&oacute;mo Funciona', array ('plugin'=>'','controller' => 'pages', 'action' => 'learn', 'admin' => false), array ('title' => 'Cómo Funciona ' . Configure::read ('site.name'), 'escape' => false)); ?></li>
<?php
if (!$auth->sessionValid ()):
  ?>
        <li class="separator"></li>
        <li><?php echo $html->link ('Registrarme', array ('plugin'=>'','controller' => 'users', 'action' => 'register'), array ('title' => 'Registrarme en ' . Configure::read ('site.name'), 'escape' => false, 'rel' => 'nofollow')); ?></li>
        <li style = "width: 75px;"></li>
  <?php
else:
  ?>
        <li style = "width: 75px;"></li>
<?php
endif;
?>
      <li>Seguinos en:&nbsp;&nbsp;&nbsp;</li>
      <li style = "padding-top: 5px; padding-right: 6px;"><?php echo $html->link ($html->image ('social_actions/facebook.png'), $city_facebook_url, array ('target' => '_blank', 'escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <li style = "padding-top: 5px; padding-right: 6px;"><?php echo $html->link ($html->image ('social_actions/twitter.png'), $city_twitter_url, array ('target' => '_blank', 'escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <?php if (preg_match ('/recent/s', $this->params['url']['url'])): ?>
        <li style = "padding-top: 5px;"><?php echo $html->link ($html->image ('social_actions/rss.png'), $rss_feed_url_for_city . '/recent', array ('escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <?php else: ?>
        <li style = "padding-top: 5px;"><?php echo $html->link ($html->image ('social_actions/rss.png'), "http://www.clubcupon.com".$rss_feed_url_for_city, array ('escape' => false, 'style' => 'padding: 0px;')); ?></li>
      <?php endif; ?>
    </ul>
    <div class="right"></div>
  </div>
</div>

<!-- ----------------------------- -->
