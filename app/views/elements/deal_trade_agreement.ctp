<?php 

echo $form->input ('deal_trade_agreement_id', array (
		'label'	  => 'Modalidades de Oferta',
		'options' => $dealTradeAgreementOptions,
		'empty' => 'Seleccione una modalidad',
		'type'	  => 'select',
		'div'     => array('id' => 'deal_trade_agreement_options'),
		'onchange'=> 'javascript:procesar(this)'
));

?>
<script>
<?php
$js_array = json_encode($dealTradeAgreementConditions);
echo "var dealTradeAgreementConditions = ". $js_array . ";\n"; 
?>

var isEditable;
$("#DealIsEndUser").ready (function () {

	var changeIsEndUSer=function() {
		return isEditable;
	};
	$("#DealIsEndUser").bind( "click", changeIsEndUSer );
	$("#DealIsEndUser").bind( "keydown", changeIsEndUSer );

	
});
$("#DealDealTradeAgreementId").ready (function () {
	<?php if($is_subdeal){?>
		$('#DealDealTradeAgreementId').attr ('disabled', 'disabled');
		$('#DealIsEndUser').attr ('readonly', true);
	<?php }?>
});
function procesar(form){
	//alert('form.value=='+form.value);
	var is_checked,is_enabled;

	
	for(var i = 0; i < dealTradeAgreementConditions.length; ++i){
		option_id=dealTradeAgreementConditions[i]['DealTradeAgreement'].id;
		//alert('option_id=='+option_id);
		if(option_id==form.value){
			   is_end_user_default=dealTradeAgreementConditions[i]['DealTradeAgreement'].is_end_user_default;
			   is_end_user_enabled=dealTradeAgreementConditions[i]['DealTradeAgreement'].is_end_user_enabled;
			   //alert('is_end_user_default=='+is_end_user_default+'//'+'is_end_user_enabled=='+is_end_user_enabled);
	           if(is_end_user_default==1){
	        	   is_checked=true;
	           }else{
	        	   is_checked=false;
	           }

	           if(is_end_user_enabled==1){
	        	   is_enabled=true;
	           }else{
	        	   is_enabled=false;
	           }
	           setIsEndUser(is_checked,is_enabled);
	           break;
			
		}
	}
	
}

function setIsEndUser(is_checked,is_enabled){

	//alert('setIsEndUser,is_checked:'+is_checked+'/is_enabled:'+is_enabled);
	$('#DealIsEndUser').attr ('readonly', is_enabled);
	$('#DealIsEndUser').attr ('checked', is_checked);
	
	isEditable=is_enabled;
}

</script>