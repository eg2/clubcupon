<?php
$title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
$browser = $_SERVER['HTTP_USER_AGENT'];
$browserIsLinuxHost = (strpos($browser, 'Linux') !== false);
$browserIsIE7 = (strpos($browser, 'MSIE 7.0') !== false);
$browserIsIE8 = (strpos($browser, 'MSIE 8.0') !== false);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml"<?php if (isset ($og_data)) echo 'xmlns:og = "http://ogp.me/ns#"  xmlns:fb = "http://www.facebook.com/2008/fbml"'; ?>>
  <head>
    <?php echo $html->charset(), "\n"; ?>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>
      <?php
      if(preg_match('/'.Configure::read('site.name').'/', $title_for_layout)) {
        $title = $html->cText($title_for_layout, false);
      } else {
        $title = Configure::read('site.name') . ' | ' . $html->cText($title_for_layout, false);
      }
      echo $title;
      ?>
    </title>
    <?php
      echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

      if (isset ($og_data)) {
        // var_dump ($og_data);
        foreach ($og_data as $key => $value) {
          if (is_array ($value)) {
            foreach ($value as $val) {
              echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
            }
          } else {
            echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
          }
        }
      }

      $html->css('_reset', null, null, false);
      $html->css('_useful', null, null, false);
      $html->css('styles', null, null, false);
      $html->css('header', null, null, false);
      $html->css('menu', null, null, false);
      $html->css('content', null, null, false);
      $html->css('sidebar', null, null, false);
      $html->css('footer', null, null, false);
      $html->css('admin', null, null, false);
      $html->css('buy', null, null, false);
      $html->css('messages', null, null, false);
      $html->css('popups', null, null, false);
      $html->css('plugins/sifr', null, null, false);
      $html->css('plugins/jquery.autocomplete', null, null, false);
      $html->css('plugins/jquery-ui-1.7.1.custom', null, null, false);
      $html->css('plugins/ui.timepickr', null, null, false);
      $html->css('plugins/colorbox', null, null, false);
      $html->css('styles_paula', null, null, false);
      $html->css('jcarousel-skin-clubcupon', null, null, false);

      //$html->css('ui-lightness/ui.all', null, null, false);

      $html->css('ui-lightness/ui.core', null, null, false);
      $html->css('ui-lightness/ui.resizable', null, null, false);
      $html->css('ui-lightness/ui.dialog', null, null, false);
      $html->css('ui-lightness/ui.theme', null, null, false);

      if($browserIsIE7) {
        $html->css('ie7', null, null, false);
      }

      if($browserIsIE7 || $browserIsIE8) {
        $html->css('ie7and8', null, null, false);
      }

      if (isset($javascript)) {
        $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => false));
		echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.7.1.min');
        $javascript->link('libs/underscore.min', false);
        $javascript->link('libs/jquery', false);
        $javascript->link('facebook', false);
        $javascript->link('libs/jquery.form', false);
        $javascript->link('libs/jquery.blockUI', false);
        $javascript->link('libs/jquery.livequery', false);
        $javascript->link('libs/jquery.metadata', false);
        $javascript->link('libs/jquery.autocomplete', false);
        $javascript->link('libs/jquery-ui-1.7.2.custom.min', false);
        $javascript->link('libs/ui.core', false);
        $javascript->link('libs/ui.resizable', false);
        $javascript->link('libs/ui.selectable', false);
        $javascript->link('libs/ui.dialog', false);

        $javascript->link('libs/jquery.countdown', false);
        $javascript->link('libs/jquery.timepickr', false);
        $javascript->link('libs/jquery.overlabel', false);
        $javascript->link('libs/jquery.colorbox', false);
        $javascript->link('libs/date.format', false);
        $javascript->link('libs/jquery.cookie', false);
        $javascript->link('libs/jquery.truncate-2.3', false);
        $javascript->link('libs/jquery.jmap', false);
        $javascript->link('libs/jquery.address-1.2.1', false);
        $javascript->link('libs/jquery.jcarousel', false);
        $javascript->link('libs/facebook-sdk', false);

		echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.metadata');
		echo $javascript->link(Configure::read('theme.asset_version') . '/libs/BigDecimal-all-last.min');

        //$javascript->link('libs/mathcontext', false);
        //$javascript->link('libs/bigdecimal', false);
        $javascript->link('common.js?v=7', false);
        $javascript->link('vtip-min.js', false);
        $javascript->link('libs/jquery.scrollTo-1.4.2-min.js', false);
      }
      echo $html->meta('description',  $meta_for_layout['description']), "\n";
      ?>
      
      <!-- facebook -->
      <!-- script src="//connect.facebook.net/en_US/all.js"></script-->
      
      <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />
      <?php echo $asset->scripts_for_layout();  ?>
      <?php if(!$browserIsLinuxHost): ?>
          <script src="/js/libs/sifr/sifr.js" type="text/javascript"></script>
          <script src="/js/libs/sifr/sifr-config.js" type="text/javascript"></script>
      <?php endif; ?>
      <?php echo $html->meta('favicon.png', '/favicon.png', array('type' => 'icon')); ?>
    <link rel="image_src" href="../img/logo-facebook.jpg" />

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
    
  </head>