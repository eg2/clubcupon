
<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">

            <!-- Contenidos -->

    <?php

    echo $this->element(Configure::read('theme.theme_name') . 'my_stuff');

        if(!empty($pageTitle))
        {
            if($paymentsPending > 0)
            {
                echo '<div style="font-weight: bold; width: 100%;text-align: center; padding-bottom: 30px;"><p>Usted posee <span style="color:red;">';
                echo $paymentsPending;
                echo '</span> pagos en proceso de verificaci&oacute;n.</p></div>';
            }
        }
?>

    <div class="menu-submenu">
        <ul class="bloque-menu clearfix submenu">
            <li><?php echo $html->link('Todos ',
              array('controller'=> 'deal_users', 'action'=>'gifted_deals','deal_id'=>$deal_id, 'type'  => 'all'),
              array('title' => 'Todos', 'class' => ($selected_type=='all'?'menu-activo-sub':''))); ?></li>
            <li><?php echo $html->link('Vencidos',   array('controller'=> 'deal_users', 'action'=>'gifted_deals','deal_id'=>$deal_id, 'type'  => 'expired'   ),
              array('title' => 'Vencidos', 'class' => ($selected_type=='expired'?'menu-activo-sub':''))); ?></li>
            <li><?php echo $html->link('Disponibles',array('controller'=> 'deal_users', 'action'=>'gifted_deals','deal_id'=>$deal_id, 'type'  => 'available' ),
              array('title' => 'Disponibles', 'class' => ($selected_type=='available'?'menu-activo-sub':''))); ?></li>
            <li><?php echo $html->link('Pendientes', array('controller'=> 'deal_users', 'action'=>'gifted_deals','deal_id'=>$deal_id, 'type'  => 'open'      ),
              array('title' => 'Pendientes', 'class' => ($selected_type=='open'?'menu-activo-sub':''))); ?></li>
        </ul>
    </div>

    <div>
    <?php
        if(($auth->user('user_type_id') == ConstUserTypes::Company) && (!empty($this->params['named']['deal_id'])))
        {
            echo $form->create('DealUser', array('type' => 'post', 'class' => 'normal search-form clearfix', 'action'=>'index'));
            echo '<div>';
            echo $form->input('coupon_code', array('label' => 'Código promocional'));
            echo $form->input('deal_id',     array('type' => 'hidden'));
            echo $form->submit('Buscar');
            echo '</div>';
            echo $form->end();
        }

        echo $form->create('DealUser' , array('class' => 'normal','action' => 'update'));

        echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url']));

        if(!empty($this->params['named']['deal_id']))
        {
            echo $form->input('deal_id', array('type' => 'hidden', 'value' => $this->params['named']['deal_id']));
        }
        elseif(!empty($this->params['named']['type']))
        {
            echo $form->input('type', array('type' => 'hidden', 'value' => $this->params['named']['type']));
        }

        echo $this->element('paging_counter');

    ?>

    <table>
        <tr>
            <?php

                //Hack horrible hasta que rehagamos esta vista. Lo hacemos para evitar toquetear el router.php

                $sort_date            = str_replace('/all', '', $paginator->sort( 'Fecha de compra',      'DealUser.created'));
                $sort_coupon_code     = str_replace('/all', '', $paginator->sort( 'Código promocional',   'DealUser.coupon_code'));
                $sort_username        = str_replace('/all', '', $paginator->sort( 'Nickname',             'User.username'));
                $sort_discount_amount = str_replace('/all', '', $paginator->sort( 'Cantidad',             'Deal.discount_amount'));
                $sort_name            = str_replace('/all', '', $paginator->sort( 'Oferta',               'Deal.name'));
                $sort_real_expiration     = str_replace('/all', '', $paginator->sort( 'Fecha de vencimiento', 'Deal.real_expiration'));
                $sort_status          = str_replace('/all', '', $paginator->sort( 'Estado del cupón',               'Deal.deal_status_id'));
                
                 
                if(!empty($this->params['named']['type']) && (($this->params['named']['type']=='available') )  || (!empty($this->params['named']['deal_id'])))
                {
                    echo '<th class="actions">' .  'ACCION' . '</th>';
                }

            ?>

            <th><?php echo $sort_date;?></th>

            <?php

                if(($auth->user('user_type_id') == ConstUserTypes::Company) && !empty($this->params['named']['deal_id']))
                {
                    echo '<th>' . $sort_coupon_code . '</th>';
                    echo '<th>' . $sort_username    . '</th>';
                }

                if(!empty($deal_id) || !empty($this->params['named']['deal_id']))
                {
                    echo '<th>' . $sort_discount_amount . ' ('.Configure::read('site.currency').')' . '</th>';
                }
                else
                {
                    echo '<th>' . $sort_name  . '</th>';
                }
            ?>
               <th><?php echo $sort_real_expiration;?></th>
                <th><?php echo $sort_status; ?></th>
                <th>C&oacute;digo Posnet</th>
				
               
                
        </tr>

        <?php

            if (!empty($dealUsers))
            {
                $i = 0;
                foreach ($dealUsers as $dealUser)
                {
                    $class = null;
                    if ($i++ % 2 == 0)
                    {
                        $class = ' class="altrow"';
                    }
                    if($dealUser['DealUser']['is_used_user'])
                    {
                        $status_class = 'js-checkbox-active';
                    }
                    else
                    {
                        $status_class = 'js-checkbox-inactive';
                    }

                    echo '<tr' . $class .'>';

                        if(!empty($this->params['named']['type']) && (($this->params['named']['type']=='available'))  || (!empty($this->params['named']['deal_id'])))
                        {

                            if($dealUser['DealUser']['is_used_user'] ==1){
                                    $class = 'used';
                                    $statusMessage = 'Cambiar estado a no usado';
                            }else{
                                    $class = 'not-used';
                                    $statusMessage = 'Cambiar estado a usado';
                            }
                            if($dealUser['Deal']['company_id'] == $user['Company']['id'])
                            {
                                $confirmation_message =  "{'divClass':'js-company-confirmation'}";
                            }
                            else
                            {
                                $confirmation_message = "{'divClass':'js-user-confirmation'}";
                            }
                            ?>


                            <td class='<?php echo "status-".$dealUser['DealUser']['is_used_user']?>'>
                                <?php
                                    if(empty($this->params['named']['deal_id']))
                                    {
                                        echo $html->link( $html->image('print.png')    ,array('controller' => 'deal_users', 'action' => 'view', $dealUser['DealUser']['id'],$dealUser['Deal']['id'],'type' => 'print'),array('escape'=> false,'target'=>'_blank', 'title' => 'Imprimir', 'class'=>'print-icon'));
                                        echo $html->link( $html->image('view.png')     ,array('controller' => 'deal_users', 'action' => 'view', $dealUser['DealUser']['id'],$dealUser['Deal']['id'],'admin' => false), array('escape'=> false,'title' => 'Ver cupon', 'class'=>'view-icon js-thickbox'));
                                    }

                                    $user = $html->getCompany($auth->user('id'));

                                    if (!empty($dealUser['DealUser']['is_used_user']) && $dealUser['Deal']['company_id'] == $user['Company']['id'])
                                    {
                                        echo $html->link(!empty($dealUser['DealUser']['is_used_user'])? 'No utilizado': 'Utilizado' ,array('controller'=>'deal_users','action'=>'update_status',$dealUser['DealUser']['id'],'is_used_user'),array('class'=>$class.' js-update-status','title'=>$statusMessage, 'target' => '_self'));
                                    }

                                    if ($class == 'not-used' && $dealUser['Deal']['company_id'] == $user['Company']['id'])
                                    {
                                        echo $html->link(!empty($dealUser['DealUser']['is_used_user'])? 'No utilizado' : 'Utilizado',array('controller'=>'deal_users','action'=>'update_status',$dealUser['DealUser']['id'],'is_used_user'),array('class'=>$class.' '.$confirmation_message.' js-update-status','title'=>$statusMessage, 'target' => '_self'));
                                    }
                        }
                                ?>
                            </td>

                            <td class="dl" >
                             <?php echo $time->format("d-m-Y",$dealUser['DealUser']['created']);?>
                            </td>

                            <?php
                                if(($auth->user('user_type_id') == ConstUserTypes::Company) && !empty($this->params['named']['deal_id']))
                                {
                                    echo '<td class="dl">' . $html->cText($dealUser['DealUser']['coupon_code']) . '</td>';
                                    echo '<td class="dl">' . $html->cText($dealUser['User']['username']) . '</td>';
                                }

                                if(!empty($deal_id) || !empty($this->params['named']['deal_id']))
                                {
                                    echo '<td class="dr">' . $html->cCurrency($dealUser['DealUser']['discount_amount']) . '</td>';
                                }
                                else
                                {
                                    echo '<td>';
                                    if($dealUser['Deal']['is_now']) {
                                      echo $html->showImage('Deal', $dealUser['Deal']['Attachment'], array('dimension' => 'medium_thumb', 'title' => $html->cText($dealUser['Deal']['name'], false)));
                                    } else {
                                      echo $html->link($html->showImage('Deal', $dealUser['Deal']['Attachment'], array('dimension' => 'medium_thumb', 'title' => $html->cText($dealUser['Deal']['name'], false))),array('controller' => 'deals', 'action' => 'view', $dealUser['Deal']['slug']),array('title'=>$dealUser['Deal']['name'],'escape' =>false, 'target' => '_self'));
                                    }
                                    echo '<br />';
                                    if($dealUser['Deal']['is_now']) {
                                      echo $html->cText($dealUser['Deal']['name']);
                                    } else {
                                      echo $html->link($html->cText($dealUser['Deal']['name']), array('controller'=> 'deals', 'action'=>'view', $dealUser['Deal']['slug']), array('escape' => false, 'title' => $dealUser['Deal']['name'], 'target' => '_self'));
                                    }
                                    echo '</td>';
                                }
                            ?>

                            <td><?php echo $time->format("d-m-Y",$dealUser['0']['real_expiration']);?></td>
                            <td>
                                <?php

                                    //hasta que rehagamos la vista y el controller, de momento, reduce la consulta a base...
                                    echo $dealUser['0']['coupon_status'];
                                ?>
                            </td>
                            <td>
                                    <?php if(isset($dealUser['Redemption']) &&
                                            isset($dealUser['Redemption'][0])): ?>
                                            <?php echo  $html->cText(substr($dealUser['Redemption'][0]['posnet_code'],0,8)); ?><span style="font-weight:bold"><?php echo  $html->cText(substr($dealUser['Redemption'][0]['posnet_code'],8,10)); ?></span>
                                    <?php endif; ?>
                                <?php if(!isset($dealUser['Redemption']) ||
                                            !isset($dealUser['Redemption'][0])): ?>
                                            <?php echo __l('No aplica'); ?>
                                    <?php endif; ?>
                            </td>
							
							
                    </tr>

                <?php
                }  //Fin foreach
            }
            else
            {
                //No hay cupones,
                echo '<tr>';
                echo '<td colspan="14">';
                echo 'No hay cupones disponibles';
                echo '</td>';
                echo '</tr>';
            }
        ?>

    </table>

    <?php

        if (!empty($dealUsers) && isset($this->params['named']['type']) && $this->params['named']['type'] == 'available')
        {
    ?>
            <div class="hide">
                <?php echo $form->submit('Submit'); ?>
            </div>
    <?php
        }
    ?>
        <div class="paging-links-wrapper">


            <?php
                //Hack horrible hasta el revamp!
                $prev_link = str_replace('/all', '', $paginator->prev('« Atras',    array('class'=>'prev')));
                $next_link = str_replace('/all', '', $paginator->next('Adelante »', array('class'=>'prev')));

                if(!empty($prev_link))
                {

                    echo $prev_link;

                }
                if(!empty($next_link))
                {
                //

                    echo $next_link;

                }
            ?>

        </div>

        <?php echo $form->end();?>

    </div>

            <!-- / Contenidos -->

        </div>
    </div>
</div>
