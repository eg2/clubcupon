<?php echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.form'); ?>
<style>

    .submitDivContainer{
        height:40px;
        }

    .contenedorCurvoResultados{
        clear:both;
        padding:5px;
        border:1px solid #ccc;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 1px 1px 1px #DDDDDD;    
        }
    
    
    #main_container{
        display:none;
    }

    #redepm-btn{
        margin-left:140px!important;
        margin-top: 15px;
    }

    .status-0 a img{
        margin-top:7px;
    }


    #ccnow .contenido-form{
        border:none;
        border-radius:none;
        box-shadow:none;
        padding:0;
        width:920px;
    }

    #loadForm{
        width: 200px;
        height: 140px;
        }

    #mensajes{
        padding:25px 0;
        text-align: center;
        background:#057FA6;
        color:#fff;
        font-weight: bold;
        display:none;
        clear:both;
        }
        
    .btn-azul-d {
        margin-bottom:15px;
    }
    /****************************/
    #shadow{
        background:url(/img/shadow.png);
        position:absolute;
        left:0;
        top:0;
        width:100%;
        z-index:1000;
        }

        #posnet-is_returned,
        #posnet-form{
            width:390px;
            margin:0 auto;
            margin-top:180px;
            background:#fff;
            color:#333;
            position: relative;
            -moz-box-shadow: 10px 5px 5px black;
            -webkit-box-shadow: 10px 5px 5px black;
            box-shadow: 10px 5px 5px black;
            }

            #posnet-is_returned .close-shadow,
            #posnet-form .close-shadow{
                position: absolute;
                right:12px;
                top:  12px;
                cursor: pointer;
                }

            #posnet-is_returned form, #posnet-is_returned input,
            #posnet-form form, #posnet-form input{
                float:none;
                }
            #posnet-is_returned h2, #posnet-is_returned h3, #posnet-is_returned form,
            #posnet-form h2, #posnet-form h3, #posnet-form form{
                padding: 10px;
                }
            #posnet-is_returned h2,
            #posnet-form h2{
                background:#90C400;
                margin:0;
                color:#fff;
                font-weight:bold;
                font-size:20px;
                }
            #posnet-is_returned h3,
            #posnet-form h3{
                margin:0;
                color:#777;
                font-weight:bold;
                }

            #posnet-form #output{
                margin:0;
                color:#777;
                font-weight:bold;
                padding:15px;
                }

                #posnet-form form .redimir{
                    background:#F57F24;
                    color:#fff;
                    font-weight:bold;
                    border:none;
                    padding: 3px;
                    width: 80px;
                    }

                    #posnet-is_returned h3{
                        padding: 10px;
                    }

</style>

<script>

$(document).ready(function() {
    updated = false;
    $("#main_container").attr("style", "position:relative;margin-top: 10px;margin-left: 5px;");

    $(function(){
                //document.body.style.overflow = 'hidden';
                positionFooter();
                function positionFooter(){
                    if(updated == false){
                        $("#footer").css({position: "absolute",top:($(window).scrollTop()+$(window).height()-$("#footer").height()-$("#header").height()+6)+"px"})
                    }
                }
                //Capturamos los eventos de ventana

                $(window)
                    .scroll(positionFooter)
                    .resize(positionFooter)

            });





  //popup tyc
	$('.ver-tyc').colorbox({
		inline:true,
		onLoad:function(){
			$('div#cboxClose').html('Continuar');
		}
	});

  //REDENCION ------------------------------------------------------------------
  //
  //
  //Ajustamos altura y escondemos el velo

  $("#posnet-form").appendTo("#shadow"); //div usado de velo, elements/footer.ctp
  $("#posnet-is_returned").appendTo("#shadow");
  $("#shadow").hide(); // ubicado en el footer
  $("#mensajes").fadeOut();

  //Eventos --------------------------------------------------------------------

  //Apertura
  $('.redencion-posnet').click(function(){

      $.id = $(this).attr('name');
      $.is_sms = $(this).attr('is_sms');
      $.is_returned = $(this).attr('is_returned');

      $("#shadow").show();
      if($.is_returned==1)
      {
          $("#posnet-is_returned").show();
          $("#posnet-form").hide();
      } else {

          $("#posnet-form").show();
          $("#posnet-is_returned").hide();

                if( $.is_sms == 1 ) {
          $("#posnet_price_div").show();
      }
      $('#output').empty();
      $('#posnet_code').val('');
      $('#posnet_price').val($(this).attr('discounted_price'));
        $("#shadow").css("height", $(document).height());
    //Opciones para el envio de redencion del cupon
    var options = {
        target:  '#output',
        success: exito,
        type:    'post',
        data:    {deal_user_id: $.id},
        beforeSubmit:function()
        {
            $('#output').html('enviando datos');
        },
        //clearForm: true,
        url : '<?php echo Router::url('/', true); ?>redemptions/redeemcoupon'
    };
     // bind form
    $('#posnet_code_submit').ajaxForm(options);
    //evitamos saltos...
    //return false;
      }

    $("#shadow").css("height", $(document).height());


  })

$('.no_match_on_redemption').click(updateDealWithNoRedemptionMatch);


function updateDealWithNoRedemptionMatch()
{
    var titulo = $(this).attr('name');
    var request = $.ajax({
    url : '<?php echo Router::url('/', true); ?>redemptions/redeemcoupon',
    type: "POST",
    data: {deal_user_id : titulo, posnet_code:'no_aplica'},
    dataType: "html",
    success: function(html){
        $('#cupon'+titulo).remove();
        $("#mensajes").slideDown(1000).delay(5000).slideUp(500);
    }

    });

            }
  //Cierre

  $('.close-shadow').live('click',function(){
  closeDialog();

  });

    //Internals
    function exito(responseText, statusText, xhr, $form)  {

        if(responseText == 'La redención fue exitosa')
        {
                                fecha = new Date();


                                                $('#cupon'+$.id).replaceWith("<br/><span  style='display:block;float:left;padding:3px;margin-left:5px;'>Redimido el <?php echo date("d/m/Y"); ?> <br/>via web.</span>")

                    closeDialog();
             $("#mensajes").slideDown(1000).delay(2000).slideUp(500);
        }

        //
    }

function zeroPad(num,count)
{
    var numZeropad = num + '';
    while(numZeropad.length < count) {
    numZeropad = "0" + numZeropad;
    }
    return numZeropad;
}

  // / REDENCION ---------------------------------------------------------------

    $('#main_container').show();
});

 function closeDialog(){
      $("#shadow").hide();
      $("#posnet_price_div").hide();
      $('#output').empty();
      $('#posnet_code').val('');
      $('#posnet_price').val('');
      }
//

</script>

<!--inicio POPUP TYC -->
<div style="display:none">
    <?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'terminos_condiciones', array('plugin'=>'now')); ?>
</div>
<!--fin POPUP TYC -->

<div id="main" class="container_15 main_frt index_for_company">
    <div class="grid_15">
        <div class="bloque-home clearfix">
        <!--
            <h1 class="h1-format"><a href="">Titulo H1</a></h1>
            <h2><a href="">Titulo H2</a></h2>
        -->

            <!-- Contenidos -->

<?php if(empty($this->params['isAjax'])){ ?>
<div class="box ">
  <?php if (!empty ($deal)) { ?>
        <h2><?php echo $deal['Deal']['name'] ?></h2>
  <?php } ?>
<?php } ?>

    <div style="width:915px;float: left;">

        

        <?php
            echo $this->element('dashboard/company_dashboard_menu_tabs');
        ?>
        <div class="contenedorCurvoResultados pushBelow">
        <!--
            <div class="grid_8"id="ccnow" style="float:right;">
                <div class="bloque-ccya clearfix">
                    <img src="/img/now/logoYApop.png">
                    <p class="title">Bienvenido a <span>Club Cup&oacute;n YA!</span></p>
                    <p>Para utilizar este nuevo servicio ten&eacute;s que aceptar los <a href="#re-ccya-pop-up" class="eula_check ver-tyc" title="T&eacute;rminos y Condiciones del servicio">T&eacute;rminos y Condiciones del servicio</a></p>
                    <?php echo $form->create('Company',array('url'=>array('plugin'=>'now','controller' => 'now_registers', 'action' => 'eula'))); ?>
                    <?php echo $form->input('now_eula_ok',array('type'=>'checkbox','label'=>'Acepto los T&eacute;rminos y Condiciones','div'=>'eula_check')); ?>
                    <div style="clear:both;"></div>
                    <?php echo $form->submit('Continuar', array('class'=>'btn-azul-d ')); ?>
                    <div class="conocemas">
                        <?php echo $html->link ('Conoc&eacute; m&aacute;s de Club Cup&oacute;n YA!', array ('controller' => 'now_registers', 'plugin'=>'now','action' => 'landing' ), array('escape' => false)); ?>
                    </div>

                    <?php echo $form->end(); ?>
                </div>
            </div>
        -->
        <br />
        <p>Ingrese alg&uacute;n criterio y presione el bot&oacute;n buscar<br /><br /></p>
        <?php
            echo $form->create('DealUser' ,         array('class' => 'normal js-ajax-form','action' => 'index_for_company'));
            echo $form->input ('r',                 array('type' => 'hidden', 'value' => $this->params['url']['url']));
            echo $form->input ('search_dni',        array('label'=>'Buscar por DNI'));
            echo $form->input ('search_friend_dni', array('label'=>'Buscar por DNI de amigo'));
            echo $form->input ('search_coupon',     array('label'=>'Buscar por nro de cupón'));
            echo $form->input ('search_username',   array('label'=>'Buscar por nombre de usuario'));

            echo $form->input('posnet_code', array('label' => __l('Posnet code')));
            if (!empty ($this->params['named']['deal_id']))
            {
                echo $form->input('deal_id', array('type' => 'hidden', 'value' => $this->params['named']['deal_id']));
            }
            echo $form->submit('Buscar', array('class'=>'btn-azul-d ',  'div' => 'submitDivContainer'));
            echo $form->end();
            echo $form->create('DealUser' , array('class' => 'normal js-ajax-form','action' => 'update'));
            echo $form->input('r', array('type' => 'hidden', 'value' => $this->params['url']['url']));

            if(!empty($this->params['named']['deal_id']))
            {
                echo $form->input('deal_id', array('type' => 'hidden', 'value' => $this->params['named']['deal_id']));
            }elseif(!empty($this->params['named']['type'])){
        	echo $form->input('type', array('type' => 'hidden', 'value' => $this->params['named']['type']));
            }
            if (!empty ($dealUsers))
            {
                echo $this->element('paging_counter');
        ?>
        <div id="mensajes">
            El cup&oacute;n fue redimido exitosamente
        </div>
        <div style="clear:both">&nbsp;</div>

            <?php
            if (!empty($dealUsers))
            {
?>


        <script>
            $(document).ready(function() {
                updated = true;
            });
        </script>
                <div class="contenido-form clearfix">
            <div class="listasuc listacup clearfix">

                <div class="linea-item titlelist clearfix">
                    <label class="lab1" ><?php echo $paginator->sort('Fecha de compra','DealUser.created');?></label>
                    <label class="lab2" ><?php echo $paginator->sort('Cupón','DealUser.coupon_code');?></label>
                    <label class="lab3" ><?php echo $paginator->sort('User name','User.username');?></label>
                    <label class="lab4" >Nombre completo</label>
                    <label class="lab5" ><?php echo $paginator->sort('DNI','UserProfile.dni');?><br>&nbsp;</label>
                    <label class="lab6" ><?php echo $paginator->sort('Monto pagado', 'Deal.discount_amount') . ' ('.Configure::read('site.currency').')';?></label>
                    <label class="lab7" ><?php echo $paginator->sort('Fecha de vencimiento', 'Deal.coupon_expiry_date');?></label>
                    <label class="lab8" ><?php echo $paginator->sort('Es regalo', 'DealUser.is_gift');?></label>
                    <label class="lab9" ><?php echo $paginator->sort('Dni amigo', 'DealUser.gift_dni');?><br>&nbsp;</label>
                    <label class="lab10"><?php echo $paginator->sort('FecNac amigo', 'DealUser.gift_dob');?></label>
                    <label class="lab11">Acciones</label>
                </div>

                <?php

                $i = 0;
                foreach ($dealUsers as $dealUser)
                {
                    $has_remdemption_record = !empty($dealUser['Redemption']);
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' bg-linea';
                    }
                    if($dealUser['DealUser']['is_used']) {
                        $status_class = 'js-checkbox-active';
                    } else {
                        $status_class = 'js-checkbox-inactive';
                    }
                ?>



                    <div class="linea-item data clearfix <?php echo $class;?>">
                        <?php if((!empty($this->params['named']['type']) && ($this->params['named']['type']!='gifted_deals')) && $this->params['named']['type'] == 'available'){?>
                        <label class="lab1">
                          <?php echo $form->input('DealUser.'.$dealUser['DealUser']['id'].'.id', array('type' => 'checkbox', 'id' => "admin_checkbox_".$dealUser['DealUser']['id'], 'label' => false, 'class' => $status_class.' js-checkbox-list')); ?>
                        </label>
                        <?php } ?>


                        <?php
                            if($dealUser['DealUser']['is_used']==1) {
                              $class = 'used';
                              $statusMessage = 'Cambiar estado a no usado';
                            } else {
                              $class = 'not-used';
                              $statusMessage = 'Cambiar estado a usado';
                            }

                            if($dealUser['Deal']['company_id'] == $user['Company']['id']) {
                              $confirmation_message =  "{'divClass':'js-company-confirmation'}";
                            } else {
                              $confirmation_message = "{'divClass':'js-user-confirmation'}";
                            }
                        ?>


                            <label class="lab1" ><?php echo $html->cDateTime($dealUser['DealUser']['created']);?></label>
                            <label class="lab2" ><?php echo $html->cText($dealUser['DealUser']['coupon_code']);?></label>
                            <label class="lab3" id="<?php echo $dealUser['DealUser']['id']?>" ><?php echo $html->cText($dealUser['User']['username']);?></label>
                            <label class="lab4" ><?php echo $html->cText($dealUser['UserProfile']['first_name'] .' '. $dealUser['UserProfile']['last_name']);?></label>
                            <label class="lab5" ><?php echo $html->cText($dealUser['UserProfile']['dni']);?></label>
                            <label class="lab6" ><?php echo $html->cCurrency($dealUser['Deal']['discounted_price']); ?></label>
                            <label class="lab7" ><?php echo $html->cDate($dealUser['Deal']['coupon_expiry_date']);?></label>
                            <label class="lab8" ><?php echo $html->cText($dealUser['DealUser']['is_gift'] ? 'Si' : 'No');?></label>
                            <label class="lab9" ><?php echo $html->cText($dealUser['DealUser']['is_gift'] ? $dealUser['DealUser']['gift_dni'] : '-');?></label>
                            <label class="lab10"><?php echo $html->cText($dealUser['DealUser']['is_gift'] ? $dealUser['DealUser']['gift_dob'] : '-');?></label>

                            <span class="<?php echo "status-".$dealUser['DealUser']['is_used']?>">
                                <div style="float: right;">
                                <?php
                                    echo $html->link( $html->image('print.png')    ,array('controller' => 'deal_users', 'action' => 'view', $dealUser['DealUser']['id'],'type' => 'print'),array('escape'=> false,'target'=>'_blank', 'title' => __l('Print'), 'class'=>'print-icon','style'=>'float:right'));
                                    $user = $html->getCompany($auth->user('id'));
                                //
                                        echo $html->link( $html->image('view.png')    ,array('controller' => 'deal_users', 'action' => 'view',$dealUser['DealUser']['id'],'admin' => false),array('escape'=> false,'title' => __l('View Coupon'), 'class'=>'view-icon js-thickbox','style'=>'float:right'));
                                  if ($user_type_id != ConstUserTypes::Company) {

                                        if ($class == 'not-used' && $dealUser['Deal']['company_id'] == $user['Company']['id']){
                                            echo $html->link('',array('controller'=>'deal_users','action'=>'update_status',$dealUser['DealUser']['id'],'is_used_user'),array('class'=>$class.' '.$confirmation_message.' js-update-status','id' => $dealUser['DealUser']['id'] ,'title'=>!empty($dealUser['DealUser']['is_used_user'])? __l("Cambiar a no utilizado"): __l("Redimir")));
                                        }else{
                                            echo $html->link('',array('controller'=>'deal_users','action'=>'update_status',$dealUser['DealUser']['id'],'is_used_user'),array('class'=>$class.' '.$confirmation_message.' js-update-status','id' => $dealUser['DealUser']['id']."-r",'title'=>!empty($dealUser['DealUser']['is_used_user'])? __l("Cambiar a no utilizado"): __l("Redimir")));
                                        }
                                    }



                               ?>


                              <!-- Redencion cupones PosNet   -->
                              <?php
                                  //revisamos si esta redimido o no...

                                      if((empty($dealUser['Redemption'][0]['redeemed'])&& empty($dealUser['Redemption'][0]['way'] ) && $user_type_id == ConstUserTypes::Company)&& $dealUser['DealUser']['is_used']==0 )
                                      {

                                      $css_class =$has_remdemption_record ? 'redencion-posnet' : 'no_match_on_redemption';
                                      echo '<a href="javascript:void(0)" name="' . $dealUser['DealUser']['id'] . '" is_returned="' . (($dealUser['DealUser']['is_returned']==1)?1:0) . '" id="cupon' . $dealUser['DealUser']['id'] . '" discounted_price="' . $dealUser['Deal']['discounted_price'] . '" is_sms="'.($dealUser['Deal']['publication_channel_type_id'] == 2 ? "1" : "0").'"class="'.$css_class.' btn-azul-d" title="Redimir cupon via POSNET" style="float:right">';
                                      echo 'Redimir CUP&Oacute;N';
                                      echo '</a>';
                                      } else {
                                           echo "<br/><span style='display:block;float:left;padding:3px;margin-left:5px;'>Redimido el ". date("d/m/Y", strtotime($dealUser['Redemption'][0]['redeemed'])). "<br/>via ".$dealUser['Redemption'][0]['way']."</span>";
                                      }



                              ?>
                              <!-- / Redencion cupones PosNet -->
                                </div>
                             </span>


                    </div>


        <?php
                }
?>

                </div></div>
  <?php
            }
            else
            {
        ?>
                <table>
                    <tr>
                        <td colspan="14" class="notice"><?php echo sprintf(__l('No coupons available'));?></td>
                    </tr>
                </table>
        <?php
            }
        ?>


        <div class="paging-links-wrapper" >
            <?php echo $this->element('paging_links'); ?>
        </div>
        <?php
            }
        ?>

        <div class="hide">
            <?php echo $form->submit('Submit'); ?>
        </div>

        <?php echo $form->end();?>





    </div>
    </div>

    <?php
        if(empty($this->params['isAjax']))
        {
            ?>
            </div>

<?php
        }
    ?>
<?php if(empty($this->params['isAjax'])){ ?>
  <div id="posnet-form" style="display:none;">
            <div class="close-shadow"><img src="/img/colorbox/cross.png" alt="Cerrar"/></div>
                <h2>Redimir Cup&oacute;n</h2>

                <form action="#" name="" id="posnet_code_submit">
                    <h3>Ingrese los &uacute;ltimos 4 d&iacute;gitos del pin posnet para poder redimir:</h3>
                    <input id="posnet_code" name="posnet_code" type="text" maxlength="4" /><br/>
                    <div id="posnet_price_div" style="display:none">
                    <h3>Ingrese el importe </h3>
                    <input id="posnet_price" name="posnet_price" type="text" maxlength="12" />
                    </div>
                    <input type="submit" value="Redimir" class="btn-azul-d" id="redepm-btn"/>
                </form>
                <div id="output"></div>
            </div>
            <div id="posnet-is_returned"  style="display:none;">
        <div class="close-shadow"><img src="/img/colorbox/cross.png" alt="Cerrar"/></div>
            <h2>Redimir Cup&oacute;n</h2>
            <h3>No se puede redimir el cupón</h3>

    </div>


<div id="divForm">
    <div id="loadForm">
    </div>

<?php } ?>
</div>

            <!-- / Contenidos -->

        </div>
    </div>
</div>
