<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="grid_12">
            <div class="bloque-home clearfix">
            <!--
                <h1 class="h1-format"><a href="">Titulo H1</a></h1>
                <h2><a href="">Titulo H2</a></h2>
            -->
                
                <!-- Contenidos -->
                <?php if(!empty($user_deals)){ ?>
                    <ol class="deal-user-list">
                        <?php
                            foreach($user_deals as $user_deal){ 
                                if(!empty($user_deal['DealUser']['is_used']) && ($user_deal['DealUser']['is_used'] == 1)){
                                    $class = 'used';
                                } else {
                                    $class = 'not-used';
                                }
                        ?>
                                <li class = "clearfix <?php echo $class;?>">
                                    <div class="company-list-image">
                                            <?php echo $html->showImage('Deal', $user_deal['Deal']['Attachment'], array('dimension' => 'medium_thumb', 'alt' => sprintf(__l('[Image: %s]'), $html->cText($user_deal['Deal']['name'], false)), 'title' => $html->cText($user_deal['Deal']['name'], false)));?>
                                    </div>
                                    <div class="company-list-content">
                                        <h3><?php echo $html->link($user_deal['Deal']['name'], array('controller' => 'deals', 'action' => 'view', $user_deal['Deal']['slug']),array('title' => sprintf('%s',$user_deal['Deal']['name'])));?></h3>
                                        <dl class="list statistics-list">
                                            <dt>Comprado el: </dt>
                                            <dd><?php echo $html->cDate($user_deal['DealUser']['created']);?></dd>
                                            <dt>Cantidad</dt>
                                            <dd><?php echo $html->cInt($user_deal['DealUser']['quantity']);?></dd>
                                        </dl>
                                    </div>
                                </li>
                        <?php } ?>
                    </ol>
                <?php
                }else { echo 'No hay ofertas disponibles';} 
                ?>
                
                <!-- / Contenidos -->

            </div>
        </div>
    </div>
</div>





