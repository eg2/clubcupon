<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Club Cup&oacute;n</title>
	 <?php
        echo $html->css(Configure::read('theme.asset_version') . 'reset');
        echo $html->css(Configure::read('theme.asset_version') . 'deal_users_styles'); //nueva hoja, unificada de las anteriores
    ?>
  </head>
  <body <?php echo (!empty($this->params['named']['type']) && $this->params['named']['type'] == 'print') ? 'onload="window.print();" style="margin:0;"' : ''; ?>>
    <!--[if IE]>
    <div class="popupBox" style="width:600px; margin:0px 4px;height:1080px;">
        <style>
            #dio{margin-left:0px;}
        </style>
    <![endif]-->
    <!--[if !IE]> -->
	<div class="popupBox" style="width:600px; margin:0px 4px;height:800px; overflow: auto!important; -ms-overflow-x: hidden; overflow-x: hidden; overflow-x: visible;">
    <!-- <![endif]-->
    <table width="645" border="0" align="center" cellpadding="15" cellspacing="0" id="dio">
      <tr>
        <td align="center" valign="middle" bgcolor="#e4e4e4">
          <table width="615" border="0" align="center" cellpadding="15" cellspacing="0">
            <tr>
              <td align="center" valign="middle" bgcolor="#FFFFFF">
                <table width="585" border="0" align="center" cellpadding="0" cellspacing="0">

                  <tr>
                    <td width="326" align="left" valign="top" bgcolor="#FFFFFF">
                    	<img src="/img/email/coupon/header2.jpg" width="326" style="display:block;"/>
                    </td>
                    <td width="259" align="center" valign="middle" bgcolor="#ECECEC" style="font-size:14px; font-weight:bold; font-family:sans-serif;vertical-align: middle;">
					<?php echo $html->cText($dealUser['DealUser']['coupon_code']); ?><br/>
					  <?php
                    if(isset($dealUser['Redemption']) && ConstUserTypes::Company !=$auth->user('user_type_id') &&
                   isset($dealUser['Redemption'][0])): ?>
                   <?php echo __l("Posnetcode: ");?><?php echo  $html->cText(substr($dealUser['Redemption'][0]['posnet_code'],0,8)); ?><span style="font-weight:bold"><?php echo  $html->cText(substr($dealUser['Redemption'][0]['posnet_code'],8,10)); ?></span>
                   <?php endif; ?></td>
                  </tr>

                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:14px;">
                      <br>&iexcl;Felicitaciones! &iexcl;Compraste la oferta <strong style="color:#f25607; font-size:16px;">
					   <?php echo ($dealUser['Deal']['name']);?></strong><strong style="color:#f25607; font-size:16px;"><?php echo $dealUser['Deal']['descriptive_text']; ?></strong>, ya pod&eacute;s disfrutarla!<br><br>
                      El monto de la compra ya fue abonado. La transacci&oacute;n se confirm&oacute; con &eacute;xito.
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><table width="585" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="356" align="left" valign="top"><br><img src="/img/email/coupon/tit_titular.gif" width="154" height="16" style="display:block;"></td>
                          <td width="17" align="left" valign="top">&nbsp;</td>
                          <td width="212" align="left" valign="top"><br><img src="/img/email/coupon/tit_canjear.gif" width="95" height="16" style="display:block;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                            Usuario: <strong><?php echo $html->cText($dealUser['User']['username']); ?></strong><br>
                            DNI: <strong><?php echo $html->cText((isset($dealUser['User']['UserProfile']['dni']) ? $dealUser['User']['UserProfile']['dni'] : '')); ?></strong>
						 <?php if (!empty($dealUser['DealUser']['is_gift']) && !empty($dealUser['DealUser']['gift_dni'])) echo '<br>DNI amigo: <strong>' . $dealUser ['DealUser']['gift_dni'] . '</strong>'; ?>
						 <?php if (!empty($dealUser['DealUser']['is_gift']) && !empty($dealUser['DealUser']['gift_dob'])) echo '<br>Fec Nac. amigo: <strong>' . $dealUser ['DealUser']['gift_dob'] . '</strong>'; ?>

							<?php $dealUser ['DealUser']['pin_code']?></td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                              <?php
                            		  foreach($canjear_en as $suc){
                                  			echo $suc.'<br/><br/>';
                              		  }
                                    ?></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><!-- br-->
                            <img src="/img/email/coupon/tit_vencimiento.gif" width="200" height="16" style="display:block;"></td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                             <?php echo  strftime ("%d/%m/%Y %H:%M", strtotime ($dealUser ['Deal']['coupon_expiry_date']));?></td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td rowspan="3" align="left" valign="top"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><br>
                            <img src="/img/email/coupon/tit_detalles.gif" width="69" height="16" style="display:block;"></td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;" colspan="3"><!-- br-->
                            <?php echo $html->cText($dealUser['Deal']['description']); ?></td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><img src="/img/email/coupon/004.gif" width="585" height="287" border="0" usemap="#mapa" style="display:block;"></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:12px;">
                      <!-- br-->Ahora s&oacute;lo resta que disfrutes de este beneficio y que sigas visitando <strong>Club Cup&oacute;n</strong> para descubrir incre&iacute;bles ofertas todos los d&iacute;as.<br>
                      Te esperamos.<br><br>
                      &iexcl;Muchas Gracias por tu compra!<br><br>
                      <strong>El equipo de Club Cup&oacute;n</strong><br>

                      <a href="http://www.clubcupon.com.ar/?from=mailing" target="_blank" style="color:#f25607;">www.clubcupon.com.ar</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
	</div>
    <map name="Map">
        <area shape="rect" coords="330,119,504,137" href="mailto:soporte@clubcupon.com">
    </map>
  </body>
</html>