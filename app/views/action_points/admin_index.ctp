<div class = "index js-response">
  <h2><?php echo 'Acciones';?></h2>
  <?php echo $form->create ('ActionPoint' , array ('class' => 'normal', 'action' => 'update')); ?>
  <table class = "list">
    <tr>
      <th class = "dl"><div class = "js-pagination">Activa</div></th>
      <th class = "dl"><div class = "js-pagination">Nombre</div></th>
      <th class = "dl"><div class = "js-pagination">Descripci&oacute;n</div></th>
      <th class = "dl"><div class = "js-pagination">Puntos</div></th>
      <th><div class = "js-pagination"><?php echo 'Fecha'; ?></div></th>
    </tr>
    <?php
      if (!empty ($actions)):
        $i = 0;
        foreach ($actions as $action):
          $class = null;
          if ($i++ % 2 == 0):
            $class = ' class = "altrow"';
          endif;
    ?>
    <tr<?php echo $class; ?>>
      <td class = "dl"><?php echo $html->cText ($action ['Action']['is_active'] ? 'S&iacute;' : 'No'); ?></td>
      <td class = "dl">
        <div class = "actions-block">
          <div class = "actions round-5-left">
            <?php echo $html->link ('Editar', array ('admin' => true, 'action' => 'edit', $action ['Action']['id']), array ('class' => 'edit js-edit', 'title' => 'Editar')); ?>
          </div>
        </div>
        <?php echo $html->cText ($action ['Action']['name']); ?>
      </td>
      <td class = "dl"><?php echo $html->cText ($action ['Action']['description']); ?></td>
      <td class = "dl"><?php echo $html->cText ($action ['ActionPoint']['points']); ?></td>
      <td><?php echo $html->cText ($action ['ActionPoint']['created']); ?></td>
    </tr>
    <?php
        endforeach;
			else:
    ?>
    <tr>
      <td colspan = "7"  class = "notice"><?php echo 'No hay acciones disponibles'; ?></td>
    </tr>
    <?php
			endif;
    ?>
  </table>
  <?php if (!empty ($actions)): ?>
  <div class = "hide">
    <?php echo $form->submit ('Submit');  ?>
  </div>
  <?php endif; ?>
  <?php echo $form->end (); ?>
</div>