<div class = "form">
<?php echo $form->create ('ActionPoint', array ('class' => 'normal', 'action' => 'edit', 'admin' => 'true', 'enctype' => 'multipart/form-data')); ?>
  <h2><?php echo 'Editar Acci&oacute;n'; ?></h2>
	<?php
    echo $form->input ('Action.id', array ('type' => 'hidden'));
    echo $form->input ('ActionPoint.id', array ('type' => 'hidden'));
    echo $form->input ('ActionPoint.action_id', array ('type' => 'hidden'));
    //
		echo $form->input ('Action.is_active', array ('label' => __l ('Activa')));
		echo $form->input ('Action.name', array ('label' => __l ('Name')));
		echo $form->input ('Action.description', array ('label' => __l ('Description'), 'type' =>'textarea'));
		echo $form->input ('ActionPoint.points', array ('label' => 'Puntos'));
	?>
  <div class = "submit-block">
    <?php
      echo $form->submit (__l ('Update'));
      echo $html->link ('<input type = "button"  value = "' . __l ('Cancel') . '"  class = "cancel-button" />', array ('controller' => 'action_points', 'action' => 'index', 'admin' => true), array ('escape' => false));
    ?>
  </div>
  <?php echo $form->end (); ?>
</div>