<?php

App::import('Core', 'Component');
App::import('Core', 'Router');

App::import('Component', 'newsletter.PowerMta');

class SubscriptionLinkComponent extends Object {

  var $components = array('PowerMta');

  function __construct() {
    parent::__construct();
    $this->staticDomainForMails = Configure::read('static_domain_for_mails');
    $this->pmtaSubscriberKey = Configure::read('EmailFilesGenerator.Pmta.SubscriberKey');
    $this->pmtaSubscriberHashKey = Configure::read('EmailFilesGenerator.Pmta.SubscriberHashKey');
    $this->PowerMta = new PowerMtaComponent();
  }

  function getLinkWithTags() {
    //Armo el link y le quito la hash
    $linkWithHash = $this->staticDomainForMails . Router::url(array(
      'base' => false,
      'controller' => 'subscriptions',
      'action' => 'unsubscribe',
      $this->PowerMta->getCompleteTag($this->pmtaSubscribenKey),
      $this->PowerMta->getCompleteTag($this->pmtaSubscriberHashKey),
      'admin' => false,
      'plugin' => null
    ), false);

    return substr($linkWithHash, 0, strrpos($linkWithHash, '/'));
  }

  function getUnsuscribeAllLinkWithTags() {

    //Armo el link y le quito la hash
    $linkWithHash = $this->staticDomainForMails . Router::url(array(
      'base' => false,
      'controller' => 'subscriptions',
      'action' => 'unsubscribe_all',
      $this->PowerMta->getCompleteTag($this->pmtaSubscriberKey),
      $this->PowerMta->getCompleteTag($this->pmtaSubscriberHashKey),
      'admin' => false,
      'plugin' => null
    ), false);


    return $linkWithHash;
  }

  function getHash($subscription){
    //Armo el link y le quito la hash
    $linkWithHash = $this->staticDomainForMails . Router::url(array(
      'base' => false,
      'controller' => 'subscriptions',
      'action' => 'unsubscribe',
      $subscription['NewsletterSubscription']['id'],
      'admin' => false,
      'plugin' => null
    ), false);

    return substr(strrchr($linkWithHash, '/'), 1);
  }

}

?>
