<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class NewsletterNewsletterSubjectComponent extends Component {

    public function makeMailSubjectByDealsWithMainDeal($deals) {
        $subject = "";
        $mainDeal = false;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if (!$deal['NewsletterDeal']['is_side_deal']) {
                    $mainDeal = $deal;
                    break;
                }
            }
            $subject = $this->makeMailSubjectByDeals($deals, $mainDeal);
        }
        return $subject;
    }

    public function makeMailSubjectByDeals($deals, $importantDeal = false) {
        $subject = "";
        if (!empty($deals)) {
            if (empty($importantDeal)) {
                $importantDeal = $deals[0];
            }
            if (empty($importantDeal['NewsletterDeal']['custom_subject'])) {
                if (count($deals) == 1) {
                    $subject = $importantDeal['NewsletterDeal']['name'];
                } else if (count($deals) == 2) {
                    $subject = $importantDeal['NewsletterDeal']['name'] . ' Y 1 Super Oferta más.';
                } else {
                    $subject = $importantDeal['NewsletterDeal']['name'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas más.';
                }
            } else {
                $subject = $importantDeal['NewsletterDeal']['custom_subject'];
            }
        }
        return $subject;
    }

    public function makeMailSubject($deals) {
        return $this->makeMailSubjectByDealsWithMainDeal($deals);
    }

}
