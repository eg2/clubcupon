<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'newsletter.PowerMta');
App::import('Component', 'newsletter.SubscriptionLink');
App::import('Component', 'newsletter.NewsletterNewsletterSubject');
App::import('Component', 'newsletter.NewsletterNewsletterBody');

class NewsletterPowerMtaFileComponent extends Component {

    private $dotsProgressShell;

    const EOF = "\n";

    function __construct() {
        $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
        $this->outputFolder = Configure::read('EmailFilesGenerator.Directory.Output');
        $this->defaultMailFrom = Configure::read('EmailFilesGenerator.From.Mail');
        $this->isXackOffEnabled = Configure::read('EmailFilesGenerator.xack_off.enabled');
        $this->isRsyncEnabled = Configure::read('EmailFilesGenerator.Rsync.Enabled');
        $this->rsyncCommand = Configure::read('EmailFilesGenerator.Rsync.Command');
        $this->EmtKey = Configure::read('EmailFilesGenerator.Emt.Key');
        $this->BounceDomain = Configure::read('EmailFilesGenerator.Bounce.Domain');
        $this->pmtaSubscriberKey = Configure::read('EmailFilesGenerator.Pmta.SubscriberKey');
        $this->pmtaSubscriberHashKey = Configure::read('EmailFilesGenerator.Pmta.SubscriberHashKey');
        $this->pmtaFrecuencyEventClassCodeKey = Configure::read('EmailFilesGenerator.Pmta.FrecuencyEventClassCodeKey');
        $this->dotsProgressShell = Configure::read('shells.DotsProgressShellMin');
        $this->NewsletterNewsletterSubject = new NewsletterNewsletterSubjectComponent();
        $this->NewsletterNewsletterBody = new NewsletterNewsletterBodyComponent();
        $this->PowerMta = new PowerMtaComponent();
        $this->SubscriptionLink = new SubscriptionLinkComponent();
    }

    public function createAndSyncFile($city, $newsletterProfile, $subscriptions, $dealsToSend, $newsletterDate, $pageNumber, $forceDealsMailSent = false, $enableMinifyHtml = false, $multipart = false) {
        $this->ApiLogger->trace("Iniciando " . __METHOD__, array(
            'city' => $city,
            'newsletterProfile' => $newsletterProfile,
            'subscriptions' => $subscriptions,
            'dealsToSend' => $dealsToSend,
            'newsletterDate' => $newsletterDate,
            'pageNumber' => $pageNumber
        ));
        $subject = $this->NewsletterNewsletterSubject->makeMailSubject($dealsToSend);
        $this->ApiLogger->notice('Obteniendo Contenido para los mails');
        $this->ApiLogger->echoConsoleWithTime('Obteniendo Contenido para los mails');
        $htmlBodyEmail = $this->NewsletterNewsletterBody->makeHtmlBody($city, $newsletterProfile, $forceDealsMailSent, $enableMinifyHtml);
        $textBodyEmail = "Si no puedes visualizar correctamente este correo entra en " . Configure::read('EmailFilesGenerator.MessageTextPlain') . $this->NewsletterNewsletterBody->makeHtmlUrl($city, $newsletterProfile, $forceDealsMailSent);
        $fullFileName = $this->outputFolder . $this->getFileName($city, $newsletterProfile, $newsletterDate, $pageNumber);
        $emailFrom = !empty($city['NewsletterCity']['email_from']) ? $city['NewsletterCity']['email_from'] : $this->defaultMailFrom;
        $emailFrom = !empty($newsletterProfile['NewsletterNewsletterProfile']['email_from']) ? $newsletterProfile['NewsletterNewsletterProfile']['email_from'] : $emailFrom;
        $this->ApiLogger->trace('', array(
            'subject' => $subject,
            'fullFileName' => $fullFileName,
            'emailFrom' => $emailFrom
        ));
        $handle = fopen($fullFileName, "w");
        $this->ApiLogger->notice('Creando Archivo #' . $pageNumber . ' [ ' . $fullFileName . ' ]');
        $this->ApiLogger->echoConsoleWithTime('Creando Archivo #' . $pageNumber . ' [ ' . $fullFileName . ' ]');
        if ($this->isXackOffEnabled) {
            fwrite($handle, "XACK OFF" . self::EOF);
        }
        fwrite($handle, "XMRG FROM:<" . $emailFrom . ">" . self::EOF);
        $index = 1;
        $countSubscriptions = count($subscriptions);
        foreach ($subscriptions as $subscription) {
            if (fmod($index, $this->dotsProgressShell) == 0) {
                $this->ApiLogger->echoConsole(".", false);
                $this->ApiLogger->notice('Procesando Suscripcion [ ' . $index . '/' . $countSubscriptions . ' ]');
            }
            fwrite($handle, $this->getSubscriberLines($city, $newsletterProfile, $subscription, $newsletterDate));
            $index++;
        }
        fwrite($handle, "XPRT 1 LAST" . self::EOF);
        fwrite($handle, "Mime-Version: 1.0" . self::EOF);
        if ($multipart) {
            fwrite($handle, "Content-Type: multipart/alternative; boundary=\"NextPart\" " . self::EOF);
        } else {
            fwrite($handle, "Content-Type: text/html; charset=\"UTF-8\"" . self::EOF);
        }
        fwrite($handle, "From: \"ClubCupon - " . $this->normaliza($city['NewsletterCity']['name']) . "\" " . chr(60) . $emailFrom . chr(62) . self::EOF);
        fwrite($handle, "To: [*to]" . self::EOF);
        fwrite($handle, "Message-ID: <[msgid]@clubcupon.com.ar>" . self::EOF);
        fwrite($handle, "X-MC-Subaccount: ccnews" . self::EOF);
        fwrite($handle, "Date: [*date]" . self::EOF);
        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
        fwrite($handle, "Subject: " . $subject . self::EOF);
        if ($multipart) {
            fwrite($handle, "--NextPart" . self::EOF);
            fwrite($handle, "Content-type: text/plain; charset=\"UTF-8\"" . self::EOF);
            fwrite($handle, $textBodyEmail . self::EOF);
            fwrite($handle, "--NextPart" . self::EOF);
            fwrite($handle, "Content-Type: text/html; charset=\"UTF-8\"" . self::EOF);
        }
        fwrite($handle, $htmlBodyEmail . self::EOF);
        if ($multipart) {
            fwrite($handle, "--NextPart--" . self::EOF);
        }
        fwrite($handle, self::EOF);
        fwrite($handle, ".");
        fclose($handle);
        if ($this->isRsyncEnabled) {
            $command = str_replace('[ORIGEN]', $fullFileName, $this->rsyncCommand);
            exec($command, $output, $return_var);
            if ($return_var != 0) {
                $result = implode("\n", $output);
                $text = "** Ocurrio un error en la sincronizacion del archivo para la instancia $this->instanceNumber **" . self::EOF;
                $text.= "Comando: $command" . self::EOF;
                $text.= "Resultado: $result" . self::EOF;
                $this->ApiLogger->echoConsole($text);
                $this->ApiLogger->error($text);
                throw new SynchronizationException($text);
            }
        }
    }

    function normaliza($incoming_string) {
        $tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
        $replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
        return utf8_encode(strtr(utf8_decode($incoming_string), utf8_decode($tofind), $replac));
    }

    function getSubscriberLines($city, $newsletterProfile, $subscription, $newsletterDate) {
        $string = $this->PowerMta->getXDFNLine("*vmta", $newsletterProfile['NewsletterNewsletterProfile']['vmta']);
        $string.= $this->PowerMta->getXDFNLine("City", $city['NewsletterCity']['name']);
        $string.= $this->PowerMta->getXDFNLine($this->pmtaSubscriberKey, $subscription['NewsletterSubscription']['id']);
        $string.= $this->PowerMta->getXDFNLine($this->pmtaSubscriberHashKey, $this->SubscriptionLink->getHash($subscription));
        $string.= $this->PowerMta->getXDFNLine($this->pmtaFrecuencyEventClassCodeKey, $subscription['NewsletterFrecuencyEventClass']['code']);
        $string.= $this->PowerMta->getXDFNLine("msgid", String::uuid());
        $string.= $this->PowerMta->getXDFNLine("*from", $this->getFromVerpEmail($city, $newsletterProfile, $subscription, $newsletterDate));
        $string.= $this->PowerMta->getXDFNLine('FromEmail', $newsletterProfile['NewsletterNewsletterProfile']['email_from']);
        $string.= $this->PowerMta->getRCPTLine($subscription['NewsletterSubscription']['email']);
        $this->ApiLogger->trace('Generando linea de Pmta para un Subscrito', array(
            'subscription' => $subscription,
            'linea power mta' => $string
        ));
        return $string;
    }

    function getFromVerpEmail($city, $newsletterProfile, $subscription, $newsletterDate) {
        $verpMail = $city['NewsletterCity']['id'] . "+";
        $verpMail.= $newsletterDate . "+";
        $verpMail.= $this->EmtKey . "+";
        $verpMail.= $subscription['NewsletterFrecuencyEventClass']['code'] . "+";
        $verpMail.= str_replace('@', '=', $subscription['NewsletterSubscription']['email']);
        $verpMail.= "@" . $this->BounceDomain;
        return $verpMail;
    }

    public function getFileName($city, $newsletterProfile, $newsletterDate, $pageNumber) {
        return $newsletterDate . '_' . $city['NewsletterCity']['id'] . '_' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . '_' . $pageNumber . '.txt';
    }

}
