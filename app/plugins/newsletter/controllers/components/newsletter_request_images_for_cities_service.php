<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'newsletter.PowerMta');
App::import('Component', 'newsletter.NewsletterPowerMtaFile');
App::import('Component', 'newsletter.UnsubscriberLink');
App::import('Model', 'newsletter.NewsletterCity');
App::import('Model', 'efg.EfgDeal');

App::import('Model', 'newsletter.NewsletterSubscription');
App::import('Model', 'newsletter.NewsletterEmailProfile');
App::import('Model', 'newsletter.NewsletterNewsletterProfile');
App::import('Core', 'HttpSocket');

class NewsletterRequestImagesForCitiesServiceComponent extends Component {

  public $name = 'NewsletterPowerMtaService';

  const PARAM_NEWSLETTER_FOR_LOG = '?newsletter=1';
  
  protected $forceDealsMailSent;
  
  function __construct() {
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');

    $this->NewsletterPowerMtaFile = new NewsletterPowerMtaFileComponent();
    $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
    $this->NewsletterEmailProfile = new NewsletterEmailProfile();
    $this->NewsletterSubscription = new NewsletterSubscription();
    $this->NewsletterCity = new NewsletterCity();
    $this->Deal = new EfgDeal();    
    $this->ignoreGroupsInNormalProfile = Configure::read('NewsletterPowerMta.IgnoreGroupsInNormalProfile');
    $this->ignoreLowerProfile = Configure::read('NewsletterPowerMta.IgnoreLowerProfile');
    $this->staticHost = Configure::read('static_domain_for_mails');
    $this->arrayHosts = Configure::read('EmailFilesGenerator.ArrayHostForRequestImages');
  }

  public function requestImagesForCities($newsletterDate, $forceDealsMailSent = false) {
  	$this->forceDealsMailSent = $forceDealsMailSent;
  	
    $this->ApiLogger->trace("Iniciando ".__METHOD__);

    $citiesToSend = $this->NewsletterCity->findCitiesToSendNewsletter($newsletterDate);
    $this->ApiLogger->notice("Se obtubieron las ciudades a enviar newsletter", array(
      'cantidad_de_ciudades_a_enviar' => count($citiesToSend),
      'ciudades' => $citiesToSend,
    ));
    $this->ApiLogger->echoConsole("\n_________________________________________________________________________");
    $this->ApiLogger->echoConsoleWithTime('Se obtubieron '.count($citiesToSend).' ciudades a enviar newsletter');
    foreach ($citiesToSend as $cityToSend) {
      $this->requestImagesForCity($cityToSend, $newsletterDate);
    }

    $this->ApiLogger->notice("Se procesaron todas las ciudades", array(
      'cantidad_de_ciudades_a_enviar' => count($citiesToSend),
      'ciudades' => $citiesToSend,
    ));
    $this->ApiLogger->echoConsoleWithTime('Se procesaron todas las ciudades');

  }

  

  public function requestImagesForCity($city, $newsletterDate) {
    $this->ApiLogger->trace("Iniciando ".__METHOD__, array('city' => $city));
    $this->ApiLogger->notice(
      "Se inicia el procesamiento de imagenes para las ofertas de la ciudad|grupo", array(
        'ciudad' => $city,
      ));
    $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
    $this->ApiLogger->echoConsole("");   
    $this->ApiLogger->echoConsole("Obteniendo ofertas para la ciudad [ ".$city['NewsletterCity']['id']." | ".$city['NewsletterCity']['name']." ]");
    $this->ApiLogger->notice("Obteniendo ofertas para la ciudad [ ".$city['NewsletterCity']['id']." | ".$city['NewsletterCity']['name']." ]");
    
    if($this->forceDealsMailSent){
        $dealsToSend = $this->Deal->GetDealsOfTheDayAndForce($city['NewsletterCity']['id']);
    }else{
        $dealsToSend = $this->Deal->GetDealsOfTheDay($city['NewsletterCity']['id']);
    }

    if ( count($dealsToSend)>0 ) {
          foreach ($dealsToSend as $deal){
              $this->requestImageForDeal($deal);
          }
          $this->ApiLogger->echoConsoleWithTime('Se finalizo el procesamiento de imagenes para las ofertas de la ciudad|grupo: [ '.$city['NewsletterCity']['id'].' | '.$city['NewsletterCity']['name'].' ]');
          $this->ApiLogger->notice('Se finalizo el procesamiento de imagenes para las ofertas de la ciudad|grupo: [ '.$city['NewsletterCity']['id'].' | '.$city['NewsletterCity']['name'].' ]');
    } else {
          $this->ApiLogger->echoConsoleWithTime('No se procesaron imagenes ya que la ciudad|group no tiene ofertas a enviar: [ '.$city['NewsletterCity']['id'].' | '.$city['NewsletterCity']['name'].' ]');
          $this->ApiLogger->notice('No se procesaron imagenes ya que la ciudad|group no tiene ofertas a enviar: [ '.$city['NewsletterCity']['id'].' | '.$city['NewsletterCity']['name'].' ]');
    }
    $this->ApiLogger->echoConsole("");
    $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
    return true;
  }
   
  public function requestImageForDeal($deal){
      $this->ApiLogger->echoConsole("__________________________________________________________________");
      $this->ApiLogger->echoConsoleWithTime("Procesando oferta");
      $this->ApiLogger->notice("Procesando oferta");
      $image_options = array(
            'dimension' => 'medium_big_thumb',
            'class' => '',
            'alt' => $deal['Deal']['name'],
            'title' => $deal['Deal']['name'],
            'type' => 'jpg'
        );
      $imageUrl = $this->imageUrl($deal['Deal']['id']);
      
      $this->ApiLogger->echoConsoleWithTime("Request Image");
      $this->ApiLogger->notice("Request image");
      $this->curlUrlImage($imageUrl);
      $this->ApiLogger->echoConsoleWithTime("End Request");
      $this->ApiLogger->notice("End Request");
      $this->ApiLogger->echoConsole("__________________________________________________________________");
  }
  
  function curlUrl($url){
      $this->ApiLogger->echoConsole("Request to: ".$url);
      $this->ApiLogger->notice("Request to: ".$url);
      $ch = curl_init ($url);
      curl_setopt($ch, CURLOPT_FAILONERROR, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_TIMEOUT, 90);
      $chResult = curl_exec($ch);
      if (!empty($chResult)) {
          curl_close($ch);
          $this->ApiLogger->echoConsole("Request OK");
          $this->ApiLogger->notice("Request OK");
          return true;
      } else {
          $this->ApiLogger->echoConsole("Request Not Found Image");
          $this->ApiLogger->notice("Request Not Found Image");
          return false;
      }
  }
  
  function curlUrlImage($url){  
     // $this->curlUrl($this->staticHost.$url.self::PARAM_NEWSLETTER_FOR_LOG);
      foreach($this->arrayHosts as $host){
          $this->curlUrl($host.$url.self::PARAM_NEWSLETTER_FOR_LOG);
      }
      return true;
  }

  
  function imageUrl($deal_id){
      return '/api/img/medium_big_thumb/Deal/'.$deal_id.'.jpg';
  }
  
  public function live() {
    return "A LIVE ....";
  }
}

