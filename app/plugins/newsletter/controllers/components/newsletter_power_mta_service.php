<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'newsletter.PowerMta');
App::import('Component', 'newsletter.NewsletterPowerMtaFile');
App::import('Component', 'newsletter.UnsubscriberLink');
App::import('Model', 'newsletter.NewsletterCity');
App::import('Model', 'newsletter.NewsletterDeal');
App::import('Model', 'newsletter.NewsletterSubscription');
App::import('Model', 'newsletter.NewsletterEmailProfile');
App::import('Model', 'newsletter.NewsletterNewsletterProfile');

class NewsletterPowerMtaServiceComponent extends Component {

    public $name = 'NewsletterPowerMtaService';
    protected $forceDealsMailSent;

    function __construct() {
        $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
        $this->NewsletterPowerMtaFile = new NewsletterPowerMtaFileComponent();
        $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
        $this->NewsletterEmailProfile = new NewsletterEmailProfile();
        $this->NewsletterSubscription = new NewsletterSubscription();
        $this->NewsletterCity = new NewsletterCity();
        $this->NewsletterDeal = new NewsletterDeal();
        $this->ignoreGroupsInNormalProfile = Configure::read('NewsletterPowerMta.IgnoreGroupsInNormalProfile');
        $this->ignoreLowerProfile = Configure::read('NewsletterPowerMta.IgnoreLowerProfile');
        $this->pmtaPageSize = Configure::read('EmailFilesGenerator.Pmta.PageSize');
        $this->forceIndexQuerySubscriptionsCities = Configure::read('EmailFilesGenerator.Pmta.forceSubscriptionQueryCities');
        $this->forceIndexQuerySubscriptionsGroups = Configure::read('EmailFilesGenerator.Pmta.forceSubscriptionQueryGroups');
        $this->enableMinifyHtml = Configure::read('EmailFilesGenerator.Pmta.enableMinifyHtml');
        $this->enableEmailMultiPart = Configure::read('EmailFilesGenerator.Pmta.enableEmailMultiPart');
    }

    public function generateFilesForCities($newsletterDate, $forceDealsMailSent = false) {
        $this->forceDealsMailSent = $forceDealsMailSent;
        $this->ApiLogger->trace("Iniciando " . __METHOD__);
        $citiesToSend = $this->NewsletterCity->findCitiesToSendNewsletter($newsletterDate);
        $this->ApiLogger->notice("Se obtubieron las ciudades a enviar newsletter", array(
            'cantidad_de_ciudades_a_enviar' => count($citiesToSend),
            'ciudades' => $citiesToSend
        ));
        $this->ApiLogger->echoConsoleWithTime('Se obtubieron ' . count($citiesToSend) . ' ciudades a enviar newsletter');
        foreach ($citiesToSend as $cityToSend) {
            $this->generateFilesForCity($cityToSend, $newsletterDate);
        }
        $this->ApiLogger->notice("Se procesaron todas las ciudades a enviar", array(
            'cantidad_de_ciudades_a_enviar' => count($citiesToSend),
            'ciudades' => $citiesToSend
        ));
        $this->ApiLogger->echoConsoleWithTime('Se procesaron todas las ciudades a enviar');
    }

    public function generateFilesForCityId($cityId, $newsletterDate, $forceDealsMailSent = false) {
        $this->forceDealsMailSent = $forceDealsMailSent;
        if ($cityId == null) {
            $this->ApiLogger->error("El id de ciudad no puede ser vacio.");
            return false;
        }
        $city = $this->NewsletterCity->findById($cityId);
        if ($city == null) {
            $this->ApiLogger->error("El id de ciudad no existe.");
            return false;
        }
        return $this->generateFilesForCity($city, $newsletterDate);
    }

    public function generateFilesForCity($city, $newsletterDate) {
        $this->ApiLogger->trace("Iniciando " . __METHOD__, array(
            'city' => $city
        ));
        $this->ApiLogger->notice("Se inicia la generacion de los archivos power mta para el envio de mails de la ciudad|grupo", array(
            'ciudad' => $city
        ));
        $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
        $this->ApiLogger->echoConsole("");
        $this->ApiLogger->echoConsoleWithTime("Se inicia la generacion de los archivos para la ciudad|grupo: [ " . $city['NewsletterCity']['id'] . " | " . $city['NewsletterCity']['name'] . " ]");
        $this->ApiLogger->echoConsole("");
        $this->ApiLogger->echoConsole("Obteniendo ofertas a enviar");
        $this->ApiLogger->notice("Obteniendo ofertas a enviar");
        $dealsToSend = $this->NewsletterDeal->findToNewsletterByCityId($city['NewsletterCity']['id'], $this->forceDealsMailSent);
        $this->ApiLogger->notice("Se obtuvieron las ofertas a enviar en el newsletter para la ciudad|grupo", array(
            'ciudad' => $city['NewsletterCity']['id'],
            'ofertas' => $dealsToSend
        ));
        $this->ApiLogger->echoConsoleWithTime('Se obtuvieron ' . count($dealsToSend) . ' Ofertas');
        if (count($dealsToSend) > 0) {
            $frecuencyEventClassesToSend = null;
            if ($this->NewsletterCity->isCity($city)) {
                $newsletterProfilesToSend = $this->NewsletterNewsletterProfile->findToSentForCities();
            } else {
                $newsletterProfilesToSend = $this->NewsletterNewsletterProfile->findToSentForGroups();
            }
            $this->ApiLogger->notice("Se obtuvieron las distintas configuraciones de envio de newsletter a enviar para la ciudad|grupo", array(
                'ciudad' => $city['NewsletterCity']['id'],
                'es_ciudad' => $this->NewsletterCity->isCity($city),
                'newsletterProfilesToSend' => $newsletterProfilesToSend
            ));
            $this->ApiLogger->echoConsoleWithTime('Se obtuvieron ' . count($newsletterProfilesToSend) . ' configuraciones de news para la ciudad|grupo: [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
            $this->ApiLogger->echoConsole("");
            foreach ($newsletterProfilesToSend as $aNewsletterProfileToSend) {
                $this->generateFileForCityAndNewsletterProfile($city, $aNewsletterProfileToSend, $dealsToSend, $newsletterDate);
            }
            $this->ApiLogger->echoConsole("");
            $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
            $this->ApiLogger->echoConsole("");
            $this->ApiLogger->echoConsoleWithTime('Se procesaron ' . count($newsletterProfilesToSend) . ' configuraciones de news  para la ciudad|grupo: [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
            $this->ApiLogger->trace("Se finalizo la generacion de los archivos power mta para el envio de mails de la ciudad|grupo", array(
                'ciudad' => $city
            ));
            if (count($dealsToSend) > 0) {
                if (!$this->forceDealsMailSent) {
                    $hasUpdateDealsAsSended = $this->NewsletterDeal->updateDealsAsSendedByCityId($city['NewsletterCity']['id']);
                    $hasNoMoreDealsToSendToDeal = count($this->NewsletterDeal->findToNewsletterByCityId($city['NewsletterCity']['id'])) == 0;
                    if ($hasUpdateDealsAsSended && $hasNoMoreDealsToSendToDeal) {
                        $this->ApiLogger->echoConsole('Se marcaron como enviadas a ' . count($dealsToSend) . ' Ofertas');
                        $this->ApiLogger->notice("Se marcaron como enviadas las ofertas de la ciudad|grupo.", array(
                            'ciudad' => $city,
                            'cantidad_de_ofertas' => count($dealsToSend),
                            'ofertas' => $this->ApiLogger->iterateItems($dealsToSend, 'NewsletterDeal', 'id')
                        ));
                    } else {
                        $this->ApiLogger->echoConsole('No se pudo marcar como eviadas a las ' . count($dealsToSend) . ' Ofertas');
                        $this->ApiLogger->notice('No se pudo marcar como eviadas a las ' . count($dealsToSend) . ' Ofertas', array(
                            'ciudad' => $city,
                            'cantidad_de_ofertas' => count($dealsToSend),
                            'ofertas' => $this->ApiLogger->iterateItems($dealsToSend, 'NewsletterDeal', 'id')
                        ));
                    }
                } else {
                    $this->ApiLogger->echoConsole('No se van a marcar como eviadas a las ' . count($dealsToSend) . ' Ofertas se utilizo forzado f:true');
                    $this->ApiLogger->notice('No se van a  marcar como eviadas a las ' . count($dealsToSend) . ' Ofertas se utilizo forzado f:true', array(
                        'ciudad' => $city,
                        'cantidad_de_ofertas' => count($dealsToSend),
                        'ofertas' => $this->ApiLogger->iterateItems($dealsToSend, 'NewsletterDeal', 'id')
                    ));
                }
            }
            $this->ApiLogger->echoConsoleWithTime('Se finalizo la generacion de los archivos para la ciudad|grupo: [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        } else {
            $this->ApiLogger->echoConsoleWithTime('No se generaron archivos ya que la ciudad|group no tiene ofertas a enviar: [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        }
        $this->ApiLogger->echoConsole("");
        $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
        return true;
    }

    public function generateFileForCityAndNewsletterProfile($city, $newsletterProfile, $dealsToSend, $newsletterDate) {
        $this->ApiLogger->trace("Iniciando " . __METHOD__, array(
            'city' => $city,
            'newsletterProfile' => $newsletterProfile,
            'dealsToSend' => $dealsToSend,
            'newsletterDate' => $newsletterDate
        ));
        $this->ApiLogger->echoConsole("_________________________________________________________________________________________________________________");
        $this->ApiLogger->echoConsoleWithTime('Se inicia la generacion de archivos para el segmento [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        $this->ApiLogger->notice('Se inicia la generacion de archivos para el segmento 
                    [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        $totalSubscriptionsSended = 0;
        $pageNumber = 1;
        $lastSubscriptionIdProcess = 0;
        $this->ApiLogger->echoConsole('');
        $this->ApiLogger->echoConsoleWithTime('Obteniendo suscripciones para el segmento');
        $subscriptionsToSend = $this->findNewsletterSubscriptionsByCityAndNewsletterProfile($city, $newsletterProfile, $lastSubscriptionIdProcess, $pageNumber);
        $totalSubscriptionsSended = $totalSubscriptionsSended + count($subscriptionsToSend);
        $this->ApiLogger->trace('', array(
            'subscriptionsToSend' => $subscriptionsToSend,
            'SQL_QUERY' => $this->NewsletterSubscription->getLastQuery()
        ));
        if ($subscriptionsToSend) {
            $this->ApiLogger->echoConsoleWithTime('Se procesaran ' . count($subscriptionsToSend) . ' suscripciones [ultimo id de subscripcion procesado:' . $lastSubscriptionIdProcess . ']');
            $this->ApiLogger->notice('Se procesaran ' . count($subscriptionsToSend) . ' suscripciones [ultimo id de subscripcion procesado:' . $lastSubscriptionIdProcess . ']');
        }
        while ($subscriptionsToSend) {
            $this->NewsletterPowerMtaFile->createAndSyncFile($city, $newsletterProfile, $subscriptionsToSend, $dealsToSend, $newsletterDate, $pageNumber, $this->forceDealsMailSent, $this->enableMinifyHtml, $this->enableEmailMultiPart);
            $lastSubscriptionIdProcess = $subscriptionsToSend[sizeof($subscriptionsToSend) - 1]['NewsletterSubscription']['id'];
            $this->ApiLogger->echoConsoleWithTime("\nSe procesaron " . count($subscriptionsToSend) . ' suscripciones', array(
                'last_subscription_id_process' => $lastSubscriptionIdProcess
            ));
            $this->ApiLogger->notice('Se procesaron ' . count($subscriptionsToSend) . ' suscripciones', array(
                'last_subscription_id_process' => $lastSubscriptionIdProcess
            ));
            $this->ApiLogger->echoConsole("");
            $pageNumber = $pageNumber + 1;
            $subscriptionsToSend = $this->findNewsletterSubscriptionsByCityAndNewsletterProfile($city, $newsletterProfile, $lastSubscriptionIdProcess, $pageNumber);
            if ($subscriptionsToSend) {
                $this->ApiLogger->echoConsoleWithTime('Se procesaran ' . count($subscriptionsToSend) . ' suscripciones [ultimo id de subscripcion procesado:' . $lastSubscriptionIdProcess . ']');
                $this->ApiLogger->notice('Se procesaran ' . count($subscriptionsToSend) . ' suscripciones [ultimo id de subscripcion procesado:' . $lastSubscriptionIdProcess . ']');
            }
            $totalSubscriptionsSended = $totalSubscriptionsSended + count($subscriptionsToSend);
            $this->ApiLogger->trace('', array(
                'subscriptionsToSend' => $subscriptionsToSend,
                'SQL_QUERY' => $this->NewsletterSubscription->getLastQuery()
            ));
        }
        if ($totalSubscriptionsSended != 0) {
            $this->ApiLogger->echoConsoleWithTime('Se procesaron en Total ' . $totalSubscriptionsSended . ' subscripciones para el segmento [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
            $this->ApiLogger->notice('Se procesaron en Total ' . $totalSubscriptionsSended . ' subscripciones para el segmento [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        } else {
            $this->ApiLogger->echoConsoleWithTime('No se obtuvieron suscripciones para el segmento [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
            $this->ApiLogger->notice('No se obtuvieron suscripciones para el segmento [ ' . $newsletterProfile['NewsletterNewsletterProfile']['id'] . ' | ' . $newsletterProfile['NewsletterNewsletterProfile']['code'] . ' ] de la ciudad [ ' . $city['NewsletterCity']['id'] . ' | ' . $city['NewsletterCity']['name'] . ' ]');
        }
        $this->ApiLogger->trace("Finalizando " . __METHOD__, array(
            'cantidad de subscripciones enviadas' => $totalSubscriptionsSended
        ));
        return true;
    }

    public function findNewsletterSubscriptionsByCityAndNewsletterProfile($city, $newsletterProfile, $lastSubscriptionIdProcess, $pageNumber) {
        if ($this->NewsletterCity->isCity($city)) {
            if ($this->forceIndexQuerySubscriptionsCities) {
                $this->ApiLogger->notice("Obteniendo subscipciones Force Index Query");
                $subscriptionsToSend = $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileIdForCitiesForceIndex($city['NewsletterCity']['id'], $newsletterProfile['NewsletterNewsletterProfile']['id'], $lastSubscriptionIdProcess, 0, $this->pmtaPageSize);
            } else {
                $this->ApiLogger->notice("Obteniendo subscipciones SIN FORCE INDEX");
                $subscriptionsToSend = $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileIdForCities($city['NewsletterCity']['id'], $newsletterProfile['NewsletterNewsletterProfile']['id'], $lastSubscriptionIdProcess, 0, $this->pmtaPageSize);
            }
        } else {
            if ($this->forceIndexQuerySubscriptionsGroups) {
                $this->ApiLogger->notice("Obteniendo subscipciones Force Index Query");
                $subscriptionsToSend = $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileIdForGroupsForceIndex($city['NewsletterCity']['id'], $newsletterProfile['NewsletterNewsletterProfile']['id'], $lastSubscriptionIdProcess, 0, $this->pmtaPageSize);
            } else {
                $this->ApiLogger->notice("Obteniendo subscipciones SIN FORCE INDEX");
                $subscriptionsToSend = $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileIdForGroups($city['NewsletterCity']['id'], $newsletterProfile['NewsletterNewsletterProfile']['id'], 0, $pageNumber, $this->pmtaPageSize);
            }
        }
        return $subscriptionsToSend;
    }

    public function generateFilesForGroups($newsletterDate, $forceDealsMailSent = false) {
        $this->forceDealsMailSent = $forceDealsMailSent;
        $this->ApiLogger->notice("Iniciando " . __METHOD__);
        $groupsToSend = $this->NewsletterCity->findGroupsToSendNewsletter($newsletterDate);
        $this->ApiLogger->notice("Se obtubieron los grupos a enviar newsletter", array(
            'cantidad_de_grupos_a_enviar' => count($groupsToSend),
            'grupos' => $groupsToSend
        ));
        foreach ($groupsToSend as $groupToSend) {
            $this->generateFilesForCity($groupToSend, $newsletterDate);
        }
        $this->ApiLogger->notice("Se procesaron todas los grupos a enviar", array());
    }

    public function live() {
        return "A LIVE ....";
    }

}
