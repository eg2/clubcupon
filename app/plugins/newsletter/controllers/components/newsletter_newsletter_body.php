<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class NewsletterNewsletterBodyComponent extends Component {

    public function makeHtmlBody($city, $newsletterProfile, $forceDealsMailSent, $enableMinifyHtml) {
        $confDebug = Configure::read('debug');
        Configure::write('debug', 0);
        if ($forceDealsMailSent) {
            $html = $this->requestHtmlMailBody($city, $newsletterProfile, 'force');
        } else {
            $html = $this->requestHtmlMailBody($city, $newsletterProfile, 'no');
        }
        Configure::write('debug', $confDebug);
        if ($enableMinifyHtml) {
            return $this->compressHtml($html);
        } else {
            return $html;
        }
    }

    function compressHtml($buf) {
        return str_replace(array(
            "\t",
            "  "
                ), '', $buf);
    }

    private function requestHtmlMailBody($city, $newsletterProfile, $force) {
        $htmlBody = $this->requestAction(array(
            'plugin' => 'efg',
            'controller' => 'emails',
            'action' => 'newsletter',
            $city['NewsletterCity']['id'],
            date("Ymd"),
            $newsletterProfile['NewsletterNewsletterProfile']['template_name'],
            $force
                ), array(
            'return'
        ));
        return $htmlBody;
    }

    public function makeHtmlUrl($city, $newsletterProfile, $forceDealsMailSent) {
        return Configure::read('static_domain_for_mails') . '/efg/emails/newsletter/' . $city['NewsletterCity']['id'] . '/' . date("Ymd") . '/' . $newsletterProfile['NewsletterNewsletterProfile']['template_name'] . '/force ';
    }

}
