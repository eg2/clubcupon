<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Model', 'Newsletter.NewsletterEmailProfile');
App::import('Model', 'Newsletter.NewsletterNewsletterProfile');
App::import('Model', 'Newsletter.NewsletterFrecuencyEventClass');
App::import('Model', 'Event.EventEvent');
App::import('Model', 'Event.EventEventType');

class NewsletterEmailProfileServiceComponent extends Component {

    public $name = 'NewsletterEmailProfileService';
    private $pageSize = 1000;
    private $initPage = 1;
    private $rangeFrequencyType = null;
    private $secondsInADay = 86400;
    private $eventTypes = null;
    private $VIOMAIL = array(
        'OPENMAIL' => '',
        'CLICKMAIL' => ''
    );
    private $noProcess = array(
        NewsletterFrecuencyEventClass::NO_CLASS_CODE
    );

    function __construct() {
        Configure::load('update_email_profile_generator_config');
        $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
        $this->EventEvent = new EventEvent();
        $this->NewsletterEmailProfile = new NewsletterEmailProfile();
        $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
        $this->EventEventType = new EventEventType();
        $this->FrecuencyEventClass = new NewsletterFrecuencyEventClass();
        $this->pageSize = Configure::read('newsletter.page_size');
        $this->pageSizeForEmailProfiles = Configure::read('newsletter.segment_extra.page_size');
        $this->pageSizeForCommit = Configure::read('newsletter.page_size_for_commit');
        $this->noClassFrequencyEventClass = $this->FrecuencyEventClass->findByCode(NewsletterFrecuencyEventClass::NO_CLASS_CODE);
        $this->naNewsletterProfile = $this->NewsletterNewsletterProfile->findByCode(NewsletterNewsletterProfile::NA_CODE);
        $this->initUpdateEmailProfiles();
    }

    private function initUpdateEmailProfiles() {
        $this->rangeFrequencyType = Configure::read('newsletter.range_frecuency_type');
        $this->rangeFrequencyTypeExtra = Configure::read('newsletter.range_frecuency_type_extra');
        $this->mapEventTypeCodeToEventTypeId();
        $this->mapEventTypeCodeToEventTypeIdForVIOMAIL();
    }

    private function mapEventTypeCodeToEventTypeIdForVIOMAIL() {
        foreach ($this->VIOMAIL as $code => $id) {
            $eventType = $this->EventEventType->findByCode($code);
            $this->VIOMAIL[$code] = $eventType['EventEventType']['id'];
        }
    }

    private function mapEventTypeCodeToEventTypeId() {
        $config = $this->rangeFrequencyType;
        $this->eventTypes = array();
        $eventTypes = array();
        foreach ($config as $segment => $options) {
            $frecuencies = $options['frecuency'];
            foreach ($frecuencies as $codeEventType => $rangeDate) {
                if (!in_array($codeEventType, $eventTypes)) {
                    $eventTypes[] = $codeEventType;
                }
            }
        }
        foreach ($eventTypes as $codeEvent) {
            $event = $this->EventEventType->findByCode($codeEvent);
            $this->eventTypes[$codeEvent] = $event['EventEventType']['id'];
        }
        foreach ($config as $segment => $options) {
            $frecuencies = $options['frecuency'];
            foreach ($frecuencies as $codeEventType => $rangeDate) {
                $this->rangeFrequencyType[$segment]['frecuency'][$this->eventTypes[$codeEventType]] = $rangeDate;
                unset($this->rangeFrequencyType[$segment]['frecuency'][$codeEventType]);
            }
        }
    }

    private function hasCorrectStructure($options) {
        $result = true;
        if (!array_key_exists('default', $options) || !array_key_exists('frecuency', $options)) {
            $result = false;
        }
        if ((array_key_exists('domain', $options) && !(count($options['domain']) > 0))) {
            $result = false;
        }
        return $result;
    }

    private function checkConfigLoaded() {
        $frecuencies = null;
        $optionsFrecuencies = $this->rangeFrequencyType;
        foreach ($optionsFrecuencies as $segmentName => $options) {
            if (!$this->hasCorrectStructure($options)) {
                throw new DomainException('Arreglo de configuración mal estructurado');
                $this->ApiLogger->error('Arreglo de configuración mal estructurado en' . $segmentName);
            }
            foreach ($options['frecuency'] as $eventTypeId => $range) {
                $frecuencies[$eventTypeId][] = $range;
            }
        }
        foreach ($frecuencies as $eventTypeId => $frecuencyRange) {
            if ($this->hasSuperimposeFrecuencyRange($frecuencyRange)) {
                $msg = 'Error en variable de configuración: ' . $eventTypeId . ' newsletter.range_frecuency_type ' . '
                  - Existe superposición entre los rangos';
                $this->ApiLogger->error('Existe superposición entre los rangos', array(
                    'frecuencias' => $frecuencyRange,
                    'eventTypeId' => $eventTypeId
                ));
                throw new DomainException($msg);
            }
            $rangosFaltantes[$eventTypeId] = $this->hasMissingRanges($frecuencyRange);
            if (!empty($rangosFaltantes[$eventTypeId])) {
                $msg = 'Segmento de Frecuencia o Rango Frecuencia Faltante' . $eventTypeId . "\n";
                $this->ApiLogger->error($msg, array(
                    'Range Faltante' => $rangosFaltantes[$eventTypeId],
                    'EventTypeId' => $eventTypeId
                ));
                throw new DomainException($msg);
                break;
            }
        }
    }

    private function hasSuperimposeFrecuencyRange($frecuencies) {
        foreach ($frecuencies as $frecuency) {
            foreach ($frecuencies as $frecuencyToCheck) {
                if (!($frecuency[0] == $frecuencyToCheck[0] && $frecuency[1] == $frecuencyToCheck[1])) {
                    $min = $this->isBetween($frecuency[0], $frecuencyToCheck[0], $frecuencyToCheck[1]);
                    $max = $this->isBetween($frecuency[0], $frecuencyToCheck[0], $frecuencyToCheck[1]);
                    if ($min || $max) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function hasMissingRanges($frecuencyRange) {
        $frecuencyByEventType = array();
        $rangosFaltantes = array();
        foreach ($frecuencyRange as $frecuency) {
            $frecuencyByEventType[] = $frecuency[0];
            $frecuencyByEventType[] = $frecuency[1];
        }
        $minVal = min($frecuencyByEventType);
        $maxVal = max($frecuencyByEventType);
        $rangeToCheck = array_fill($minVal, $maxVal, 1);
        foreach ($frecuencyRange as $frecuency) {
            $min = $frecuency[0];
            $max = ($frecuency[1] == "0") ? $frecuency[0] : $frecuency[1];
            foreach (range($min, $max) as $key) {
                $rangeToCheck[$key] = 0;
            }
        }
        if (array_keys($rangeToCheck, 1)) {
            $key = array_keys($rangeToCheck, 1);
            $min = min($key);
            $max = max($key);
            $rangosFaltantes = array(
                $min,
                $max
            );
        }
        return $rangosFaltantes;
    }

    private function moveOnPage($emails) {
        return (!empty($emails));
    }

    private function setFieldsEmailProfile($email, $newsletterProfileIdForCities, $newsletterProfileIdForGroups, $segmentId, $lastEventDate) {
        $this->ApiLogger->trace('Datos antes de Actualizar:', array(
            '$email' => $email,
            '$newsletterProfileIdForCities' => $newsletterProfileIdForCities,
            '$newsletterProfileIdForGroups' => $newsletterProfileIdForGroups,
            '$segment' => $segmentId,
            '$lastEventDate' => $lastEventDate
        ));
        $emailProfile = array();
        $emailProfile['NewsletterEmailProfile']['email'] = $email;
        $emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_cities'] = $newsletterProfileIdForCities;
        $emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_groups'] = $newsletterProfileIdForGroups;
        $emailProfile['NewsletterEmailProfile']['last_event_date'] = $lastEventDate;
        $emailProfile['NewsletterEmailProfile']['frecuency_event_class_id'] = $segmentId;
        return $emailProfile;
    }

    private function findOptionsByCodeInConfig($segment, $config) {
        $this->ApiLogger->trace('Buscando Opciones para .' . $segment, array(
            'config' => $config,
            '$segment' => $segment
        ));
        $opt = array();
        foreach ($config as $segmentkey => $options) {
            if ($segmentkey == $segment) {
                $opt = $options;
                break;
            }
        }
        return $opt;
    }

    public function updateEmailProfileOnlyExtraSegment($extraSegment) {
        if (empty($extraSegment)) {
            throw new DomainException('Extrasegmento vacio, extrasegment:[D,E]');
        }
        $segment = $this->FrecuencyEventClass->findByCode($extraSegment);
        if (empty($segment)) {
            throw new DomainException('Extrasegmento no definido');
        }
        $config = $this->rangeFrequencyTypeExtra;
        foreach ($config as $segment => $options) {
            if ($segment == $extraSegment) {
                $emails = $this->updateEmailProfileByExtraSegmento($extraSegment);
            }
        }
        return count($emails);
    }

    public function cleanBySegment($segmento) {
        $field_frecuency_class_id = $this->FrecuencyEventClass->findByCode(NewsletterFrecuencyEventClass::NO_CLASS_CODE);
        $condition_frecuency_class_id = $this->FrecuencyEventClass->findByCode($segmento);
        if (!empty($field_frecuency_class_id) && !empty($condition_frecuency_class_id)) {
            $this->NewsletterEmailProfile->query('update email_profiles set ' . ' previous_frecuency_event_class_id =  frecuency_event_class_id' . "," . ' frecuency_event_class_id = ' . $field_frecuency_class_id['NewsletterFrecuencyEventClass']['id'] . ' where frecuency_event_class_id = ' . $condition_frecuency_class_id['NewsletterFrecuencyEventClass']['id']);
        }
    }

    public function cleanFrequencyClassId() {
        $this->ApiLogger->echoConsoleWithTime("Actualizando el Frequency Class Id a todos los EmailProfiles");
        $this->ApiLogger->debug("Actualizando el Frequency Class Id a todos los EmailProfiles", array(
            'noClassFrecuencyEventClassId' => $this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'],
            'naNewsletterProfileId' => $this->naNewsletterProfile['NewsletterNewsletterProfile']['id']
        ));
        $result = $this->NewsletterEmailProfile->updateAllFrequencyEventClassIdAndNewsletterProfileIds($this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'], $this->naNewsletterProfile['NewsletterNewsletterProfile']['id'], $this->naNewsletterProfile['NewsletterNewsletterProfile']['id']);
        $this->ApiLogger->echoConsoleWithTime("Actualizacion Terminada, se actualizo el Frequency Class Id a todos los EmailProfiles");
        $this->ApiLogger->debug("Actualizacion Terminada, se actualizo el Frequency Class Id a todos los EmailProfiles", array(
            'noClassFrecuencyEventClassId' => $this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'],
            'naNewsletterProfileId' => $this->naNewsletterProfile['NewsletterNewsletterProfile']['id'],
            'result' => $result
        ));
    }

    public function updateEmailProfileAllSegments() {
        $today = date('Y-m-d 00:00:00');
        $this->cleanFrequencyClassId();
        $frecuencyEventClass = $this->FrecuencyEventClass->findAllSegmentsSort();
        $codeToLog = array();
        foreach ($frecuencyEventClass as $frecuency) {
            $codeToLog[] = $frecuency['NewsletterFrecuencyEventClass']['code'];
        }
        $this->ApiLogger->echoConsole('Segmentos a procesar: ' . (join(',', $codeToLog)));
        $this->ApiLogger->notice("Segmentos a procesar", array(
            'cantidad_de_segmentos' => count($frecuencyEventClass),
            'frecuencyEventClass' => (join(',', $codeToLog))
        ));
        foreach ($frecuencyEventClass as $row) {
            if (!in_array($row['NewsletterFrecuencyEventClass']['code'], $this->noProcess)) {
                $segment = $row['NewsletterFrecuencyEventClass']['code'];
                $options = $this->findOptionsByCodeInConfig($segment, $this->rangeFrequencyType);
                $this->updateEmailProfilesBySegmento($segment, $today);
            } else {
                $this->ApiLogger->notice("Segmento ignorado", array(
                    'frecuencyEventClass' => $row['NewsletterFrecuencyEventClass']['code']
                ));
            }
        }
    }

    public function updateEmailProfilesAndExtraSegment($hasCleanFrequencyClassId = false) {
        $today = date('Y-m-d 00:00:00');
        if ($hasCleanFrequencyClassId) {
            $this->cleanFrequencyClassId();
        }
        $frecuencyEventClass = $this->FrecuencyEventClass->findAllSegmentsSort();
        $codeToLog = array();
        foreach ($frecuencyEventClass as $frecuency) {
            $codeToLog[] = $frecuency['NewsletterFrecuencyEventClass']['code'];
        }
        $this->ApiLogger->echoConsole('Segmentos a procesar: ' . (join(',', $codeToLog)));
        $this->ApiLogger->notice("Segmentos a procesar", array(
            'cantidad_de_segmentos' => count($frecuencyEventClass),
            'frecuencyEventClass' => (join(',', $codeToLog))
        ));
        $extraSegmentation = array();
        foreach ($frecuencyEventClass as $row) {
            if (!in_array($row['NewsletterFrecuencyEventClass']['code'], $this->noProcess)) {
                $segment = $row['NewsletterFrecuencyEventClass']['code'];
                $options = $this->findOptionsByCodeInConfig($segment, $this->rangeFrequencyType);
                if (!empty($options)) {
                    $this->updateEmailProfilesBySegmento($segment, $today);
                } else {
                    $extraSegmentation[] = $row;
                }
            } else {
                $this->ApiLogger->notice("Segmento ignorado", array(
                    'frecuencyEventClass' => $row['NewsletterFrecuencyEventClass']['code']
                ));
            }
        }
        foreach ($extraSegmentation as $row) {
            if (!in_array($row['NewsletterFrecuencyEventClass']['code'], $this->noProcess)) {
                $this->updateEmailProfileByExtraSegmento($row['NewsletterFrecuencyEventClass']['code']);
            }
        }
        return true;
    }

    public function updateEmailProfilesByPageOrLimitForFrecuencyRange($limit, $page, $segment, $options, $today) {
        $this->ApiLogger->trace('Inicio updateEmailProfilesByPageOrLimitForFrecuencyRange', array(
            'limit' => $limit,
            'segment' => $segment,
            'options' => $options
        ));
        $extraConditions = null;
        $this->ApiLogger->echoConsoleWithTime('Obteniendo registros con frecuencia (event_type_id:[min, max]) : ' . json_encode($options['frecuency']));
        $this->ApiLogger->notice("Obteniendo registros", array(
            'segment' => $segment,
            'limit' => $limit,
            'executionDate' => $today,
            'options' => $options
        ));
        $emails = $this->EventEvent->findLastEventBetweenDates($limit, $page, $options, $extraConditions, $this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'], $today);
        $totalEmails = count($emails);
        $this->ApiLogger->trace('SQL Busqueda de Eventos por Segmento: .' . $segment, array(
            'SQL' => $this->EventEvent->getLastQuery(),
            'debug' => Configure::read('debug')
        ));
        $this->ApiLogger->echoConsoleWithTime('Iniciando proceso de datos: ' . ' Cantidad: ' . $totalEmails . "/" . $limit . " Segmento: " . $segment);
        $this->ApiLogger->notice("Iniciando proceso de datos: " . date('Y-m-d H:i:s') . " Cantidad: " . $totalEmails . "/" . $limit . " Segmento: " . $segment);
        $frecuencyEventClass = $this->FrecuencyEventClass->findByCode($segment);
        $segmentId = $frecuencyEventClass['NewsletterFrecuencyEventClass']['id'];
        if (!empty($emails)) {
            $this->NewsletterEmailProfile->transaction();
            $index = 1;
            foreach ($emails as $emailEvent) {
                $emailEvent = $this->updateEmailProfileByEmailEvent($emailEvent, $segment, $options, $segmentId);
                if (fmod($index, $this->pageSizeForCommit) == 0) {
                    $this->ApiLogger->echoConsole(".", false, false);
                    $this->NewsletterEmailProfile->commit();
                    $this->NewsletterEmailProfile->transaction();
                }
                $index++;
            }
            if ($totalEmails <= $this->pageSizeForCommit || $totalEmails % $this->pageSizeForCommit) {
                $this->NewsletterEmailProfile->commit();
            }
        }
        $this->ApiLogger->echoConsoleWithTime("Lote procesado");
        $this->ApiLogger->notice('Lote procesado');
        $this->ApiLogger->trace("Lote procesado - updateEmailProfilesByPageOrLimitForFrecuencyRange");
        return $emails;
    }

    private function updateEmailProfileByEmailEvent($emailEvent, $segment, $options, $segmentId) {
        $this->ApiLogger->trace("Inicio updateEmailProfileByEmailEvent", array(
            'emailEvent' => $emailEvent,
            'options' => $options,
            'segment' => $segment
        ));
        $newsletterProfileCodes = $this->getNewsletterProfileCode($emailEvent['EventEvent']['email'], $segment, $options);
        $newsletterProfileForCities = $this->NewsletterNewsletterProfile->findByCode($newsletterProfileCodes['for_cities']);
        $newsletterProfileForGroups = $this->NewsletterNewsletterProfile->findByCode($newsletterProfileCodes['for_groups']);
        if (empty($newsletterProfileCodes) || empty($newsletterProfileForCities) || empty($newsletterProfileForGroups)) {
            $this->ApiLogger->error('updateEmailProfileByEmailEvent Perfil Inexistente', array(
                'newsletterProfileCodes' => $newsletterProfileCodes,
                '$newsletterProfileForCities' => $newsletterProfileForCities,
                '$newsletterProfileForGroups' => $newsletterProfileForGroups
            ));
            throw new DomainException('Código de Perfil o Perfil Inexistente');
        }
        $this->ApiLogger->trace("Perfil a Asignar:", array(
            'params' => $newsletterProfileCodes
        ));
        $emailProfile = $this->setFieldsEmailProfile($emailEvent['EventEvent']['email'], $newsletterProfileForCities['NewsletterNewsletterProfile']['id'], $newsletterProfileForGroups['NewsletterNewsletterProfile']['id'], $segmentId, $emailEvent[0]['last_event_date']);
        $this->ApiLogger->trace("Insertando o Actualizando EmailProfile ");
        $emailProfileSaved = $this->NewsletterEmailProfile->insertOrUpdateEmailProfileTransaction($emailProfile);
        $this->ApiLogger->trace("Finalizo updateEmailProfileByEmailEvent");
        return $emailProfile;
    }

    private function isBetween($days, $min, $max) {
        if ($max == 0) {
            $isBetween = $days >= $min;
        } else {
            $isBetween = ($days >= $min && $days <= $max);
        }
        return $isBetween;
    }

    private function getCodeFrecuencyByDomain($email, $options) {
        $code = null;
        foreach ($options['domain'] as $domain => $id) {
            $domain = strtolower($domain);
            if (strpos($email, $domain)) {
                $code = $id;
            }
        }
        if (empty($code)) {
            $code = $options['default'];
        }
        return $code;
    }

    private function getNewsletterProfileCode($email, $segment, $options) {
        $this->ApiLogger->trace('Inicio getNewsletterProfileCode', array(
            'emailEvent' => $email,
            'segment' => $segment,
            'options' => $options
        ));
        if (array_key_exists('domain', $options)) {
            $codes = $this->getCodeFrecuencyByDomain($email, $options);
        } else {
            $codes = $options['default'];
        }
        $this->ApiLogger->trace("Codigo de Perfil a Asignar", array(
            'Codigos de Newsletter Perfil: ' => $codes
        ));
        return $codes;
    }

    private function getDiffDays($maxDate) {
        $todayTm = time();
        $maxDateTm = strtotime($maxDate);
        $interval = abs($maxDateTm - $todayTm);
        $days = intval($interval / $this->secondsInADay);
        $this->ApiLogger->notice("Fechas: ", array(
            'today' => date('d-m-Y H:i:s', time()),
            'last_event' => $maxDate,
            'days' => $days
        ));
        return (intval($days));
    }

    public function updateEmailProfilesBySegmento($segmento, $today) {
        $this->ApiLogger->echoConsole('Procesando Segmento: ' . $segmento . ' Limit ' . $this->pageSize);
        $opt = $this->findOptionsByCodeInConfig($segmento, $this->rangeFrequencyType);
        if (empty($opt)) {
            $this->ApiLogger->echoConsole('El Segmento no se encuentra definido');
            throw new DomainException('Segmento o NewsletterProfile no se' . ' encuentra en variable de configuracion range_frecuency_type');
        } else {
            $page = $this->initPage;
            $this->ApiLogger->echoConsole('Page: ' . $page . ', ' . 'Limit: ' . $this->pageSize);
            $emails = $this->updateEmailProfilesByPageOrLimitForFrecuencyRange($this->pageSize, 1, $segmento, $opt, $today);
            while ($emails) {
                $page = $page + 1;
                $this->ApiLogger->echoConsole('Page: ' . $page . ', ' . 'Limit: ' . $this->pageSize);
                $emails = $this->updateEmailProfilesByPageOrLimitForFrecuencyRange($this->pageSize, 1, $segmento, $opt, $today);
            }
        }
        $this->ApiLogger->echoConsole('Segmento Procesado OK: ' . $segmento . ' Limit: ' . $this->pageSize);
        $this->ApiLogger->notice('Segmento Procesado OK: ' . $segmento . ' Limit: ' . $this->pageSize);
        $this->ApiLogger->echoConsole('________________________________________________');
    }

    public function updateEmailProfileByExtraSegmento($codeExtraSegment) {
        $this->ApiLogger->echoConsole('Procesando Segmento: ' . $codeExtraSegment . ', Limit: ' . $this->pageSize);
        $this->ApiLogger->notice("[SegmentoExtra] Procesando Segmento: " . $codeExtraSegment . " , Limit: $this->pageSize", array(
            'params' => $codeExtraSegment
        ));
        $this->ApiLogger->echoConsole('Obteniendo Registros para ' . $codeExtraSegment);
        $segment = $this->FrecuencyEventClass->findSegmentBaseByExtraSegment($codeExtraSegment);
        $page = $this->initPage;
        $eventTypeIds = array_values($this->VIOMAIL);
        $extraConditions = null;
        $emails = $this->NewsletterEmailProfile->findEmailsByFrecuencyEventClassId($segment['NewsletterFrecuencyEventClass']['id'], $eventTypeIds, $this->pageSizeForEmailProfiles, 1, $extraConditions);
        while ($emails) {
            $this->ApiLogger->echoConsoleWithTime("Iniciando proceso de datos, page: $page cantidad: " . count($emails) . "/" . $this->pageSize);
            $this->ApiLogger->echoConsoleWithTime("Page: $page, Limit: $this->pageSize");
            $this->ApiLogger->notice("[SegmentoExtra] Iniciando proceso de datos: " . date('Y-m-d H:i:s') . " Page: $page Cantidad: " . count($emails) . "/" . $this->pageSize . " ExtraSegmento: " . $codeExtraSegment);
            $sql = $this->NewsletterEmailProfile->getLastQuery();
            $this->ApiLogger->trace("[SegmentoExtra] SQL Emails a procesar: ", array(
                'SQL' => $sql
            ));
            $this->ApiLogger->notice("[SegmentoExtra] Emails a procesar: " . count($emails), array(
                'SegmentoExtra' => $codeExtraSegment,
                'Segmento' => $segment['NewsletterFrecuencyEventClass']['code']
            ));
            $index = 1;
            $this->NewsletterEmailProfile->transaction();
            foreach ($emails as $email) {
                $emailProfile = $this->updateEmailProfileByEmailAndExtraSegment($email, $segment, $codeExtraSegment);
                if (fmod($index, 1000) == 0) {
                    $this->ApiLogger->echoConsole('.', false, false);
                }
                $index++;
                $this->ApiLogger->trace("[SegmentoExtra] Procesando Email ", array(
                    'email' => $email['NewsletterEmailProfile']['email'],
                    'emailSaved' => $emailProfile
                ));
            }
            $this->NewsletterEmailProfile->commit();
            $this->ApiLogger->echoConsoleWithTime('Lote procesado');
            $emails = $this->NewsletterEmailProfile->findEmailsByFrecuencyEventClassId($segment['NewsletterFrecuencyEventClass']['id'], $eventTypeIds, $this->pageSizeForEmailProfiles, 1, $extraConditions);
            $page++;
        }
        $this->ApiLogger->echoConsole('Segmento Procesado OK: ' . $codeExtraSegment . ' , Limit: ' . $this->pageSize);
        $this->ApiLogger->echoConsole('________________________________________________');
        $this->ApiLogger->trace("[SegmentoExtra] Finalizo updateEmailProfilesAndExtraSegmento");
    }

    public function updateEmailProfileByEmailAndExtraSegment($email, $segment, $extraSegment) {
        $this->ApiLogger->trace("[SegmentoExtra] Codigo de Perfil a Asignar", array(
            'Codigo de Perfil: ' => $extraSegment
        ));
        $options = $this->findOptionsByCodeInConfig($extraSegment, $this->rangeFrequencyTypeExtra);
        $newsletterProfileCodes = $this->getNewsletterProfileCode($email['NewsletterEmailProfile']['email'], $segment, $options);
        $newsletterProfileForCities = $this->NewsletterNewsletterProfile->findByCode($newsletterProfileCodes['for_cities']);
        $newsletterProfileForGroups = $this->NewsletterNewsletterProfile->findByCode($newsletterProfileCodes['for_groups']);
        $this->ApiLogger->trace('[SegmentoExtra] updateEmailProfileByEmailAndExtraSegment', array(
            'newsletterProfileCodes' => $newsletterProfileCodes,
            '$newsletterProfileForCities' => $newsletterProfileForCities,
            '$newsletterProfileForGroups' => $newsletterProfileForGroups
        ));
        if (empty($newsletterProfileCodes) || empty($newsletterProfileForCities) || empty($newsletterProfileForGroups)) {
            throw new DomainException('Código de Perfil o Perfil Inexistente');
        }
        $frecuencyEventClass = $this->FrecuencyEventClass->findByCode($extraSegment);
        $emailProfile = $this->setFieldsEmailProfile($email['NewsletterEmailProfile']['email'], $newsletterProfileForCities['NewsletterNewsletterProfile']['id'], $newsletterProfileForGroups['NewsletterNewsletterProfile']['id'], $frecuencyEventClass['NewsletterFrecuencyEventClass']['id'], $email['NewsletterEmailProfile']['last_event_date']);
        $emailProfileSaved = $this->NewsletterEmailProfile->insertOrUpdateEmailProfile($emailProfile);
        $this->ApiLogger->trace('[SegmentoExtra] Actualizado EmailProfile', array(
            'emailProfileSaved' => $emailProfileSaved
        ));
        return $emailProfileSaved;
    }

    public function updateOrCreateEmailProfileByEmail($email) {
        $idNA = $this->FrecuencyEventClass->findByCode(NewsletterFrecuencyEventClass::NO_CLASS_CODE);
        $newsletterProfileNA = $this->NewsletterNewsletterProfile->find('first', array(
            'conditions' => array(
                'code' => NewsletterNewsletterProfile::NA_CODE
            )
        ));
        $emailProfile['NewsletterEmailProfile'] = array(
            'last_event_date' => date('Y-m-d'),
            'frecuency_event_class_id' => $idNA['NewsletterFrecuencyEventClass']['id'],
            'newsletter_profile_id_for_cities' => $newsletterProfileNA['NewsletterNewsletterProfile']['id'],
            'newsletter_profile_id_for_groups' => $newsletterProfileNA['NewsletterNewsletterProfile']['id'],
            'email' => $email
        );
        $emailProfile = $this->NewsletterEmailProfile->insertOrUpdateEmailProfileTransaction($emailProfile);
        return $emailProfile;
    }

    public function insertAndUpdateEmailProfileByEmail($email) {
        $this->ApiLogger->notice(__METHOD__, array(
            'email' => $email
        ));
        $email_profile = $this->NewsletterEmailProfile->insertOrUpdateEmailProfile(array(
            'NewsletterEmailProfile' => array(
                'email' => $email,
                'last_event_date' => date('Y-m-d H:i:s'),
                'frecuency_event_class_id' => $this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'],
                'newsletter_profile_id_for_cities' => $this->naNewsletterProfile['NewsletterNewsletterProfile']['id'],
                'newsletter_profile_id_for_groups' => $this->naNewsletterProfile['NewsletterNewsletterProfile']['id']
            )
        ));
        $this->ApiLogger->notice("NewsletterEmailProfileInsert", array(
            'email_profile' => $email_profile,
            'last_query' => $this->NewsletterEmailProfile->getLastQuery()
        ));
        if ($this->updateEmailProfileByEmail($email)) {
            return true;
        }
        return false;
    }

    public function updateEmailProfileByEmail($email) {
        $today = date('Y-m-d 00:00:00');
        if (empty($email)) {
            throw new DomainException('parametro email vacio, eg, email: email@domain.com');
        }
        $this->ApiLogger->notice(__METHOD__, array(
            'email' => $email
        ));
        $this->updateOrCreateEmailProfileByEmail($email);
        $frecuencyEventClass = $this->FrecuencyEventClass->findAllSegmentsSort();
        $extraConditions = array(
            'email' => $email
        );
        foreach ($frecuencyEventClass as $row) {
            if (!in_array($row['NewsletterFrecuencyEventClass']['code'], $this->noProcess)) {
                $segment = $row['NewsletterFrecuencyEventClass']['code'];
                $options = $this->findOptionsByCodeInConfig($segment, $this->rangeFrequencyType);
                if (!empty($options)) {
                    $this->ApiLogger->notice('findLastEventBetweenDates', array(
                        'betweenDates' => $options,
                        'extraConditions' => $extraConditions
                    ));
                    $emailEvent = $this->EventEvent->findLastEventBetweenDates($this->pageSize, $this->initPage, $options, $extraConditions, $this->noClassFrequencyEventClass['NewsletterFrecuencyEventClass']['id'], $today);
                    $this->ApiLogger->trace('SQL Busqueda de Eventos por Segmento: ', array(
                        'SQL' => $this->EventEvent->getLastQuery(),
                        'debug' => Configure::read('debug')
                    ));
                    $emailEvent = $emailEvent[0];
                    if (!empty($email) && !(is_null($emailEvent))) {
                        $this->ApiLogger->notice('Email tiene evento asociado: ', array(
                            'emailEvent' => $emailEvent
                        ));
                        $emailProfile = $this->updateEmailProfileByEmailEvent($emailEvent, $segment, $options, $row['NewsletterFrecuencyEventClass']['id']);
                        break;
                    }
                } else {
                    $eventTypeIds = array_values($this->VIOMAIL);
                    $extraSegment = $row['NewsletterFrecuencyEventClass']['code'];
                    $segment = $this->FrecuencyEventClass->findSegmentBaseByExtraSegment($extraSegment);
                    $extraConditions = array(
                        'email' => $email
                    );
                    $emailProfile = $this->NewsletterEmailProfile->findEmailsByFrecuencyEventClassId($segment['NewsletterFrecuencyEventClass']['id'], $eventTypeIds, $this->pageSizeForEmailProfiles, 1, $extraConditions);
                    if (!empty($emailProfile)) {
                        $emailProfile = $this->updateEmailProfileByEmailAndExtraSegment($emailProfile['NewsletterEmailProfile']['email'], $segment, $extraSegment);
                    }
                }
            }
        }
        if (empty($emailEvent)) {
            $this->ApiLogger->error('No existen eventos asociados al email', array(
                'email' => $email
            ));
        } else {
            $this->ApiLogger->notice('EmailProfile Actualizado/Creado: ', array(
                'emailProfile' => $emailProfile
            ));
        }
        return $emailProfile;
    }

    public function cleaningTestingSegments() {
        $queryClean = "delete from events where email like 'test_individuo%';";
        $this->EventEvent->query($queryClean);
        $queryCleanEmailProfile = "delete from email_profiles where email like 'test_individuo%';";
        $this->EventEvent->query($queryCleanEmailProfile);
    }

    public function testingSegments() {
        $queryDataTest = "insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo0@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo0@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo0@mailinator.com',DATE_ADD(now(), INTERVAL -70 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo0@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);
     
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo2@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo2@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 4);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo2@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo2@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 3);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo2@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo3@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo3@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo3@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo3@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo3@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo4@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo4@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo4@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo4@mailinator.com',DATE_ADD(now(), INTERVAL -30 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo4@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);
 
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo5@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo5@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo5@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo5@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo5@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -30 DAY), 99);
  
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo6@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo6@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo6@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo6@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo6@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo7@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo7@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo7@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo7@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo7@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo8@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo8@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo8@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo8@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo8@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo9@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo9@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo9@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo9@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo9@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo10@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo10@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo10@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo10@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo10@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo11@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo11@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo11@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo11@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo11@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo17@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo17@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo18@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo18@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo13@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo13@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);

      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo16@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo16@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo14@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo14@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo15@mailinator.com',DATE_ADD(now(), INTERVAL -90 DAY), 1);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo15@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
  
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo21@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo21@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo21@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 3);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo21@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 4);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo21@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
     
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo20@mailinator.com',DATE_ADD(now(), INTERVAL -200 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo20@mailinator.com',DATE_ADD(now(), INTERVAL -220 DAY), 2);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo20@mailinator.com',DATE_ADD(now(), INTERVAL -230 DAY), 3);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo20@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      -- Casos Bordes
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo22@mailinator.com',DATE_ADD(now(), INTERVAL -180 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo22@mailinator.com',DATE_ADD(now(), INTERVAL -180 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo22@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -200 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo23@mailinator.com',DATE_ADD(now(), INTERVAL -179 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo23@mailinator.com',DATE_ADD(now(), INTERVAL -179 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo23@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -179 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo24@mailinator.com',DATE_ADD(now(), INTERVAL -60 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo24@mailinator.com',DATE_ADD(now(), INTERVAL -60 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo24@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -60 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo25@mailinator.com',DATE_ADD(now(), INTERVAL -59 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo25@mailinator.com',DATE_ADD(now(), INTERVAL -59 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo25@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -59 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo26@mailinator.com',DATE_ADD(now(), INTERVAL -61 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo26@mailinator.com',DATE_ADD(now(), INTERVAL -61 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo26@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -61 DAY), 99);
      
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo27@mailinator.com',DATE_ADD(now(), INTERVAL -181 DAY), 1);
      insert into events (created, modified, deleted, email, last_event_date, event_type_id)
      values (now(), now(), 0, 'test_individuo27@mailinator.com',DATE_ADD(now(), INTERVAL -181 DAY), 2);
      insert into email_profiles (created, modified, deleted, email, newsletter_profile_id_for_cities, newsletter_profile_id_for_groups, last_event_date, frecuency_event_class_id)
      values (now(), now(), 0, 'test_individuo27@mailinator.com', 1,1, DATE_ADD(now(), INTERVAL -181 DAY), 99)
      ";
        $AllFeatures = split(';', $queryDataTest);
        foreach ($AllFeatures as $sql) {
            $this->EventEvent->query($sql);
        }
    }

    public function getEmailProfilesForTesting($email) {
        return $this->NewsletterEmailProfile->find('first', array(
                    'fields' => array(
                        'email',
                        'frecuency_event_class_id'
                    ),
                    'conditions' => array(
                        'email' => $email
                    )
        ));
    }

    public function countRowBySegment($segment) {
        $frecuency = $this->FrecuencyEventClass->findByCode($segment);
        $count = $this->NewsletterEmailProfile->countEmailsByFrecuencyEventClassId($frecuency['NewsletterFrecuencyEventClass']['id']);
        return $count;
    }

}
