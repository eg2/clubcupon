<?php
App::import('Component', 'newsletter.newsletterEmailProfileService');
class EmailProfilesController extends AppController {

    var $name = 'EmailProfiles';
    
    public function __construct() {
      $this->EmailProfileService = new NewsletterEmailProfileServiceComponent();
      parent::__construct();
    }
    public function beforeFilter() {
      //  parent::beforeFilter();
      //  $this->staticDomain = Configure::read('static_domain_for_mails');
      //  Configure::write('debug', 0);
    }

   public function update_email_profiles_cronjob() {
     $email = $this->params['form']['email'];
     $page = $this->params['form']['page'];
     $limit = $this->params['form']['limit'];
     $result = array( 
         'error' => 0, 
         'msg' => '', 
         'params' => $this->params['form']);
     
     try {
        if (!empty($email)) {
          $this->EmailProfileService->updateEmailProfileByEmail($email);
        } elseif (!empty($page) || !empty($limit) ) {
          $this->EmailProfileService->updateEmailProfilesByPageOrLimit($limit, $page);
        } else {
          $this->EmailProfileService->updateAllEmailProfiles();
        }
        $result['msg'] = 'OK';
     } catch (DomainException  $e) { 
        $result['error'] = 1;
        $result['msg'] = $e->getMessage();
     } catch (Exception $e) {
        $result['error'] = 1;
        $result['msg'] = $e->getMessage();
     }
     $this->layout = false;
     $this->autoRender = false;
     echo json_encode($result);
   }
}
