<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);

App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'newsletter.NewsletterEmailProfileService');

class UpdateEmailProfileTestShell extends Shell {

  public $params = null;

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
    $this->initComponents();
    
  }
  function initComponents() {
    try {
      $this->NewsletterEmailProfileService  = new NewsletterEmailProfileServiceComponent();
    } catch (DomainException  $e) { 
      $this->ApiLogger->error($e->getMessage());
      echo "\n\n".$e->getMessage()."\n\n";
      die("\n".'No se creo la Instancia de NewsletterEmailProfileServiceComponent '.
          "\n".'proceso ABORTADO, error en variable de configuracion: '.
          'efg.range_frecuency_type'."\n\n");
    }
    catch (Exception $e) {
       $this->ApiLogger->error($e->getMessage());
    }
  }
  
  function main() {
    $msg = 'Test Finalizado';    $error = 0; $hasCleanFrequencyClassId = 1;
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = $this->ApiShellUtils->getOptions($this->args);    
    $this->ApiLogger->debug(
      "Iniciando Proceso de TEST ".__class__, array(
      'args' => $this->args,
      'params' => $this->params,
      'configurationsOverride' => array_keys($this->configurationsOverride),
      'configurationsOverrideValues' => $this->configurationsOverride,
      'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
      'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
    ));
    $testCountSegment = array(
        'A' => 6,
        'B' => 11,
        'C' => 3,
        'D' => 3,
        'E' => 3
    );
    $testEmailProfile = array (
        'test_individuo0@mailinator.com' => '1',
        'test_individuo@mailinator.com' =>  '1',
        'test_individuo2@mailinator.com' => '1',
        'test_individuo3@mailinator.com' => '1',
        'test_individuo4@mailinator.com' => '2',
        'test_individuo5@mailinator.com' => '2',
        'test_individuo6@mailinator.com' => '2',
        'test_individuo7@mailinator.com' => '2',
        'test_individuo8@mailinator.com' => '2',
        'test_individuo23@mailinator.com' => '2',
        'test_individuo10@mailinator.com' => '2',
        'test_individuo11@mailinator.com' => '2',
        'test_individuo22@mailinator.com' => '2',
        'test_individuo17@mailinator.com' => '5',
        'test_individuo18@mailinator.com' => '5',
        'test_individuo13@mailinator.com' => '5',
        'test_individuo16@mailinator.com' => '4',
        'test_individuo14@mailinator.com' => '4',
        'test_individuo15@mailinator.com' => '4',
        'test_individuo21@mailinator.com' => '3',
        'test_individuo20@mailinator.com' => '3',
        'test_individuo9@mailinator.com' => '3',
        'test_individuo24@mailinator.com' => '1',
        'test_individuo25@mailinator.com' => '1',
        'test_individuo26@mailinator.com' => '2',
        'test_individuo27@mailinator.com' => '2'
        
    );   
    try {
        $countDB = array();
        $countDBFail = array();
        $this->NewsletterEmailProfileService->cleaningTestingSegments();
        $this->NewsletterEmailProfileService->testingSegments();
        $this->NewsletterEmailProfileService->updateEmailProfileAllSegments();
        //$this->NewsletterEmailProfileService->updateEmailProfilesAndExtraSegment($hasCleanFrequencyClassId);
        $this->NewsletterEmailProfileService->cleaningTestingSegments();
        
        foreach ($testCountSegment as $code => $countEmail) {
          $countDB[$code] = $this->NewsletterEmailProfileService->countRowBySegment($code);
          $countDB[$code] = (is_null($countDB[$code]) ? 0: $countDB[$code]) ;
          if ($countDB[$code] === $countEmail) {
            echo "\n $code OK ". $countDB[$code];
          } else {   
            $countDBFail[$code] = $countDB[$code];
            unset($countDB[$code]);
          }
        }
        $totalSegment = count($testCountSegment);
        echo "\n".'Segmentos a Probar: '.$totalSegment;
        echo "\n".'Segmentos Fallidos: '. count($countDBFail). '/'.$totalSegment;        
        foreach ($countDBFail as $code => $countFail) {
          echo "\n :::.Fallo Segmento $code esperado $testCountSegment[$code] obtenido $countFail";
        }
        echo "\n".'Segmentos Ok: '. count($countDB)."\n";
       
        echo '--------------------------------------------------'."\n";
        echo 'Resumen Diferencia de cada individuo: '."\n";
        foreach ($testEmailProfile as $email => $class) {
          $emailProfile = 
                  $this->NewsletterEmailProfileService->getEmailProfilesForTesting($email);
          if (!($emailProfile['NewsletterEmailProfile']['frecuency_event_class_id']==$class && !empty($emailProfile))) {
            echo "\n".'Email | Esperado <> obtenido'."\n";
            echo "\n".$emailProfile['NewsletterEmailProfile']['email'].' '.$class. ' <> '.$emailProfile['NewsletterEmailProfile']['frecuency_event_class_id']."\n";
          } 
        }
        echo '--------------------------------------------------'."\n";
        sleep(10);
        //$this->NewsletterEmailProfileService->cleaningTestingSegments();
        $result = true;
      
    } catch (DomainException  $e) { 
        $error = 1;  $result = null;
        $this->ApiLogger->error(
                $e->getMessage(), 
                array( 'params' => $this->params,));
        
    } catch (Exception $e) {
        $error = 1;  $result = null;
        $this->ApiLogger->error(
                $e->getMessage(), 
                array( 'params' => $this->params,));
    }
    if ($error) { $msg = $msg . ' con Errores'; }     
    $this->ApiLogger->debug(
      $msg, array(
        'params' => $this->params,
        'result' => $result
    ));
    echo $msg;
  }

}
