<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'newsletter.NewsletterPowerMtaService');
App::import('Component', 'newsletter.NewsletterRequestImagesForCitiesService');

class NewsletterPowerMtaShell extends Shell {

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
        $this->ApiShellUtils = new ApiShellUtilsComponent();
        $this->NewsletterPowerMtaService = new NewsletterPowerMtaServiceComponent();
        $this->NewsletterRequestImagesForCitiesService = new NewsletterRequestImagesForCitiesServiceComponent();
    }

    function main() {
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
        $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
        $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->params = $this->ApiShellUtils->getOptions($this->args);
        $this->ApiLogger->notice("Iniciando Proceso " . __class__, array(
            'args' => $this->args,
            'params' => $this->params,
            'configurationsOverride' => array_keys($this->configurationsOverride),
            'configurationsOverrideValues' => $this->configurationsOverride,
            'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
            'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues
        ));
        $this->ApiLogger->echoConsoleWithTime("Iniciando proceso " . __class__);
        if (!empty($this->params['c'])) {
            $command = $this->params['c'];
        } else {
            $command = null;
        }
        if (!empty($this->params['d'])) {
            $date = $this->params['d'];
        } else {
            $newsletterDate = date('Ymd');
        }
        $forceDealsMailSent = false;
        if (!empty($this->params['f'])) {
            if ($this->params['f'] == 'true') {
                $forceDealsMailSent = true;
            }
        }
        switch ($command) {
            case "create_files_for_cities":
                $this->ApiLogger->notice('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de Ciudades.');
                $this->ApiLogger->echoConsole('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de Ciudades.');
                $this->NewsletterPowerMtaService->generateFilesForCities($newsletterDate, $forceDealsMailSent);
                break;

            case "create_files_for_groups":
                $this->ApiLogger->notice('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de Grupos');
                $this->ApiLogger->echoConsole('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de Grupos');
                $this->NewsletterPowerMtaService->generateFilesForGroups($newsletterDate, $forceDealsMailSent);
                break;

            case "create_files_for_city":
                $cityId = $this->params['city_id'];
                $this->ApiLogger->notice('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de una Ciudad.', array(
                    'city_id' => $cityId
                ));
                $this->ApiLogger->echoConsole('Iniciando la generacion de archivos POWER MTA para el envio de Newsletter de una Ciudad. city_id:' . $cityId);
                $this->NewsletterPowerMtaService->generateFilesForCityId($cityId, $newsletterDate, $forceDealsMailSent);
                break;

            case "newsletter_request_images_for_cities":
                $this->ApiLogger->notice('Iniciando');
                $this->ApiLogger->echoConsole('Iniciando la generacion de imagenes de las ofretas para el envio de Newsletter');
                $this->NewsletterRequestImagesForCitiesService->requestImagesForCities($newsletterDate, $forceDealsMailSent);
                break;

            default:
                $this->help();
        }
    }

    function help() {
        echo "\n\n";
        echo 'NAME:' . "\t" . 'newsletter_power_mta' . "\n To create files for power mta";
        echo "\n\n" . 'SYNOPSIS:' . "\n\n";
        echo "newsletter_power_mta \n\n c:[create_files_for_cities | create_files_for_groups | \n" . " create_files_for_city ]\n\n";
        echo "OPTIONS:\n\n";
        echo 'c:create_files_for_cities - create files for all cities' . "\n";
        echo 'c:create_files_for_groups - create files for all groups' . "\n";
        echo 'c:create_files_for_city   - create files for especify city' . "\n";
        echo 'c:[ command ] f:true      - force sending mails for deals' . "\n";
        echo 'c:newsletter_request_images_for_cities   - request images for cities' . "\n";
        echo "\n\nDEBUG: \n\n";
        echo "+DLog.Verbose:1 +Ddebug:2 +DLog.PrefixFileName:file_name.log\n";
        echo "\n\nEXAMPLES:\n\n";
        echo 'newsletter_power_mta c:create_files_for_cities' . "\n";
        echo 'newsletter_power_mta c:create_files_for_groups f:true' . "\n \n";
        echo 'newsletter_power_mta c:create_files_for_citi city_id:12345' . "\n";
        echo 'newsletter_power_mta c:newsletter_request_images_for_cities' . "\n";
    }

}
