<?php 
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Model', 'newsletter.NewsletterNewsletterProfile');
App::import('Model', 'newsletter.NewsletterSubscription');
App::import('Model', 'event.EventHit');

class NewsletterNewsletterProfileTestShell extends Shell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    Configure::load('email_files_generator_config');
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
  }

  function main() {
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = $this->ApiShellUtils->getOptions($this->args);
    $this->ApiLogger->debug(
      "Iniciando Proceso ".__class__, array(
        'args' => $this->args,
        'params' => $this->params,
        'configurationsOverride' => array_keys($this->configurationsOverride),
        'configurationsOverrideValues' => $this->configurationsOverride,
        'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
        'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
      ));

    $command = $this->params['c'];

    $this->setUp($this->params);
    $this->ApiLogger->notice(
      'Run Command.', array(
        'command' => $command
      ));

    switch ($command) {
    case "test":
      $this->test();
      break;

    case "test_find_to_sent":
      $this->testFindToSent();
      break;

    case "test_find_hits":
      $this->testFindHits();
      break;

    default:
      echo "ERROR: Debe especificar un comando.\n";
      $this->ApiLogger->error("Debe especificar un comando.");
    }

  }

  public function setUp($params) {
    $this->NewsletterSubscription = new NewsletterSubscription();
    $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
    $this->EventHit = new EventHit();
  }

  public function test() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));

    $this->ApiLogger->trace('', array(
          'NewsletterSubscription::findToSendNewsByCityIdAndProfileId' => $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileIdForGroups(
            '42550',
            '1',
            '1',
            '1'),
          'QUERY' => $this->NewsletterSubscription->getLastQuery(),
    ));

    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }


  public function testFindToSent() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));
    $this->ApiLogger->trace('', array(
      'NewsletterNewsletterProfile::findToSentForGroup' => $this->NewsletterNewsletterProfile->findToSentForGroups(),
      'NewsletterNewsletterProfile::findToSentForCities' => $this->NewsletterNewsletterProfile->findToSentForCities()
    ));
    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }

  public function testFindHits() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));
    $this->ApiLogger->trace('', array(
      'EventHit::find' => $this->EventHit->find('first'),
    ));
    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }

  public function testNewsletterSubscriptionFind() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));
    $this->ApiLogger->trace('', array(
          'NewsletterSubscription::findToSendNewsByCityIdAndProfileId' => $this->NewsletterSubscription->findToSendNewsByCityIdAndProfileId(
            '42550',
            '1',
            '1',
            '1'),
          'QUERY' => $this->NewsletterSubscription->getLastQuery(),
    ));
    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }


}


