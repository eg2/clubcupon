<?php 
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Model', 'newsletter.NewsletterNewsletterProfile');

class NewsletterTestShell extends Shell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    Configure::load('email_files_generator_config');
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
  }

  function main() {
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = $this->ApiShellUtils->getOptions($this->args);
    $this->ApiLogger->debug(
      "Iniciando Proceso ".__class__, array(
        'args' => $this->args,
        'params' => $this->params,
        'configurationsOverride' => array_keys($this->configurationsOverride),
        'configurationsOverrideValues' => $this->configurationsOverride,
        'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
        'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
      ));

    $command = $this->params['c'];

    $this->setUp($this->params);
    switch ($command) {
    case "test":
      $this->ApiLogger->notice(
        'Run Command.', array(
          'command' => $command
        ));
      $this->test();
      break;

    default:
      echo "ERROR: Debe especificar un comando.\n";
      $this->ApiLogger->error("Debe especificar un comando.");
    }

  }

  public function setUp($params) {
    $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
  }

  public function test() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));

    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }
}


