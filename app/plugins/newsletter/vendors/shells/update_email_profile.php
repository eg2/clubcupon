<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);

App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'newsletter.NewsletterEmailProfileService');

class UpdateEmailProfileShell extends Shell {

  public $params = null;

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
    $this->initComponents();
  }

  function initComponents() {
    try {
      $this->NewsletterEmailProfileService  = new NewsletterEmailProfileServiceComponent();
    } catch (DomainException  $e) { 
      $this->ApiLogger->error($e->getMessage());
      echo "\n\n".$e->getMessage()."\n\n";
      die("\n".'No se creo la Instancia de NewsletterEmailProfileServiceComponent '.
        "\n".'proceso ABORTADO, error en variable de configuracion: '.
        'efg.range_frecuency_type'."\n\n");
    }
    catch (Exception $e) {
      $this->ApiLogger->error($e->getMessage());
    }
  }
  function help() {
    echo "\n\n";
    echo 'NAME:'."\t". 'update_email_profile'.
            "\n To update profiles in accordance to the events frequency";
    echo "\n\n".'SYNOPSIS:'."\n\n";
    echo "update_email_profile \n\n c:[update_email_profile | update_email_profile_by_segment_code | \n".
         " update_email_profile_by_email ]\n\n";
    echo "OPTIONS:\n\n";
    echo 'c:update_email_profile - categorize ALL segments'."\n";
    echo 'c:update_email_profile_by_segment_code segment:[A|B|C]'."\n";
    echo 'c:update_email_profile_by_email email:email@mailinator.com'."\n";  
    echo "\n\nDEBUG: \n\n";
    echo "+DLog.Verbose:1 +Ddebug:2 +DLog.PrefixFileName:file_name.log\n";
    echo "\n\nEXAMPLES:\n\n";
    echo 'update_email_profile c:update_email_profile_by_segment_code:B'."\n";
    echo 'update_email_profile c:update_email_profile_by_email:mail@domain.com'."\n";      
  }
  function main() {
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = 
      $this->ApiShellUtils->getOptions($this->args);
    $command = (!empty($this->params['c'])? $this->params['c']:null);
    $help = (!empty($this->params['h'])? $this->params['h']:null);
    
    $this->ApiLogger->debug(
      "Iniciando Proceso ".__class__, array(
      'Hora de Inicio' => array(date('Y-m-d H:i:s')),
      'args' => $this->args,
      'params' => $this->params,
      'configurationsOverride' => array_keys($this->configurationsOverride),
      'configurationsOverrideValues' => $this->configurationsOverride,
      'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
      'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
    ));
    
    $msg = 'Proceso Finalizado';
    $error = 0;

    if ($help) { $this->help();  die(); }  
    try {
      switch ($command) {
        case "update_email_profile":
          $this->NewsletterEmailProfileService->updateEmailProfileAllSegments();
          break;
        case "update_email_profile_by_segment_code":
          $segmento = (!empty($this->params['segment'])? $this->params['segment']:null);
          $this->NewsletterEmailProfileService->cleanBySegment($segmento);
          $today = date('Y-m-d 00:00:00');
          $this->NewsletterEmailProfileService->updateEmailProfilesBySegmento($segmento, $today);
          break;
        case "update_email_profile_by_email":
          $email = (!empty($this->params['email'])? $this->params['email']:null);
          $emailProfile = $this->NewsletterEmailProfileService->updateEmailProfileByEmail($email);
          if (empty($emailProfile)) {
            throw new DomainException('Email No tiene eventos Asociados');
          }
          break;
        default:
          $this->help();
          break;
      }     
      $result = true;
    } catch (DomainException  $e) { 
      $error = 1;  $result = null;
      echo $e->getMessage()."\n";
      $this->ApiLogger->error(
        $e->getMessage(), 
        array( 'params' => $this->params,));

    } catch (Exception $e) {
      $error = 1;  $result = null;
      $this->ApiLogger->error(
        $e->getMessage(), 
        array( 'params' => $this->params,));
    }
    if ($error) { $msg = $msg . ' con Errores'; }     
    $this->ApiLogger->debug(
      $msg, array(
        'Hora de Fin' => array(date('Y-m-d H:i:s')),
        'params' => $this->params,
        'result' => $result
    ));
    echo "\n".$msg;   
  }

}
