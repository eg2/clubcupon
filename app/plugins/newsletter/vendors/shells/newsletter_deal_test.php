<?php 
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Model', 'newsletter.NewsletterDeal');

class NewsletterDealTestShell extends Shell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    Configure::load('email_files_generator_config');
    $this->ApiLogger = new ApiLogger(get_class($this), 'newsletter');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
  }

  function main() {
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = $this->ApiShellUtils->getOptions($this->args);
    $this->ApiLogger->debug(
      "Iniciando Proceso ".__class__, array(
        'args' => $this->args,
        'params' => $this->params,
        'configurationsOverride' => array_keys($this->configurationsOverride),
        'configurationsOverrideValues' => $this->configurationsOverride,
        'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
        'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
      ));

    $command = $this->params['c'];

    $this->setUp($this->params);
    $this->ApiLogger->notice(
      'Run Command.', array(
        'command' => $command
      ));

    switch ($command) {
    case "test":
      $this->test();
      break;

    case "test_find_to_sent_by_city":
      // $this->testFindToSent();
      break;

    case "test_update_sended_by_city":
      // $this->testFindHits();
      break;

    default:
      echo "ERROR: Debe especificar un comando.\n";
      $this->ApiLogger->error("Debe especificar un comando.");
    }

  }

  public function setUp($params) {
    $this->NewsletterDeal = new NewsletterDeal();
  }

  public function test() {
    $this->ApiLogger->trace(__METHOD__."::BEGIN", array('params' => $this->params));

    $buenosAiresCityId = 42550;

    $dealsToSendForBuenosAires = $this->NewsletterDeal->findToNewsletterByCityId(
      $buenosAiresCityId
    );

    $this->ApiLogger->trace('', array(
      'NewsletterDeal::findToSend('.count($dealsToSendForBuenosAires).')' => $this->_iterateDeals(
        $dealsToSendForBuenosAires,
        $fields=array('id'),
        $showField=false
      ),
      //'__QUERY__' => $this->NewsletterDeal->getLastQuery(),
    ));

    $this->ApiLogger->trace('', array(
      'NewsletterDeal::updateDealsAsSendedByCityId', $this->NewsletterDeal->updateDealsAsSendedByCityId(
        $buenosAiresCityId
      ),
    ));

    $this->ApiLogger->trace(__METHOD__."::END", array('params' => $this->params));
  }

  public function _iterateItems($items=null, $alias=null, $fields=null, $showField=false) {
    if(is_null($fields)) {
      $fields = array('id');
    }
    $compactItems;
    foreach ($items as $item) {
      $valueFields = array();
      foreach ($fields as $field) {
        if($showField) {
          $valueFields[$field] = $item[$alias][$field];
        } else {
          $valueFields[] = $item[$alias][$field];
        }
      }
      $compactItems[] = $valueFields;
    }
    return $compactItems;
  }

  public function _iterateDeals($deals=null, $fields=null, $showField=false) {
    if(is_null($fields)) {
      $fields = array('id');
    }
    return $this->_iterateItems($deals, 'NewsletterDeal', $fields, $showField);
  }


}


