<?php

class NewsletterEmailProfile extends NewsletterAppModel {

    public $useTable = 'email_profiles';
    public $name = 'NewsletterEmailProfile';
    public $alias = 'NewsletterEmailProfile';
    public $recursive = -1;
    
    public function getByEmail($email) {
        return $this->find('first', array(
                    'conditions' => array(
                        'email' => $email
                    ),
                    'recursive' => -1
                ));
    }
    public function notExistsQueryVIOMAIL($EventTypeIds) {
      $strEventTypeIds = join(',', $EventTypeIds);
      $sql = 'NOT EXISTS (select 1 from events e where e.event_type_id '.
             'in ('.$strEventTypeIds.') and '.
             'e.email= NewsletterEmailProfile.email)';
      return $sql;
    }
    public function findEmailsByFrecuencyEventClassId($frecuencyEventClassId, $EventTypeIds, $limit, $page, $extraConditions) { 
      $conditions[] = 
              array('frecuency_event_class_id' => $frecuencyEventClassId,);
      $conditions[] = DboSource::expression(
              $this->notExistsQueryVIOMAIL($EventTypeIds));
      if (!empty($extraConditions)) {
        $conditions[] = $extraConditions;
      }
      $emailProfiles = $this->find('all', array(
                   // 'fields' => $this->fields,
                    'limit' => $limit,
                    'page' => $page,
                    'conditions' => $conditions,
                    'recursive' => -1
                    ));
      return $emailProfiles;
    }
    
    private function generateEmailProfileSQLInsert($date, $email, $lastEventDate, $frecuency_event_class_id, $newsletter_profile_id_for_cities, $newsletter_profile_id_for_groups){
    	return "INSERT INTO email_profiles (created,modified,deleted,email,last_event_date, frecuency_event_class_id, newsletter_profile_id_for_cities,newsletter_profile_id_for_groups)
    	VALUES ('".$date."','".$date."',0,'".$email."','".$lastEventDate."',".$frecuency_event_class_id.",".$newsletter_profile_id_for_cities.",".$newsletter_profile_id_for_groups.")
    	ON DUPLICATE KEY UPDATE modified = '".$date."', ".
      "last_event_date = '".$lastEventDate."', ".
      "frecuency_event_class_id = '".$frecuency_event_class_id."', ".
      "newsletter_profile_id_for_cities = '".$newsletter_profile_id_for_cities."', ".
      "newsletter_profile_id_for_groups = '".$newsletter_profile_id_for_groups."';";
    }

	function transaction(){
    	$this->actsAs = array('Transactional');
    	return $this->begin();
    }
    
    public function insertOrUpdateEmailProfileTransaction($emailProfile){
    	$sql = $this->generateEmailProfileSQLInsert(date('Y-m-d H:i:s'), $emailProfile['NewsletterEmailProfile']['email']
    			, $emailProfile['NewsletterEmailProfile']['last_event_date'], 
    			$emailProfile['NewsletterEmailProfile']['frecuency_event_class_id'], 
    			$emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_cities'], 
    			$emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_groups']);
      return $this->query($sql);
    }
    
    public function insertOrUpdateEmailProfile($emailProfile) {
        $registro = $this->emailExists($emailProfile['NewsletterEmailProfile']['email']); 
        if ($registro) {
        	if(($registro['NewsletterEmailProfile']['frecuency_event_class_id'] == 2) && ($emailProfile['NewsletterEmailProfile']['frecuency_event_class_id'] == 1)){
        		echo "CASO NORMAL CAMBIO: ".$registro['NewsletterEmailProfile']['id'];
        	}
        	if(($registro['NewsletterEmailProfile']['frecuency_event_class_id'] == 3) && ($emailProfile['NewsletterEmailProfile']['frecuency_event_class_id'] == 1)){
        		echo "CASO PREMIUM CAMBIO: ".$registro['NewsletterEmailProfile']['id'];
        	}
          $fields = 
                  array('newsletter_profile_id_for_cities' => 
                  $emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_cities'],
                    'newsletter_profile_id_for_groups' => 
                  $emailProfile['NewsletterEmailProfile']['newsletter_profile_id_for_groups'],
                      'frecuency_event_class_id' =>
                  $emailProfile['NewsletterEmailProfile']['frecuency_event_class_id'],
                      'modified' => "'".date('Y-m-d H:i:s', time())."'",
                      'last_event_date' =>
                  "'".$emailProfile['NewsletterEmailProfile']['last_event_date']."'",
                      );
          $conditions = 
                  array('email' => 
                  $emailProfile['NewsletterEmailProfile']['email'] );
          
          $this->updateAll($fields, $conditions); 
        } else {
          $this->create($emailProfile);
        
          if ($this->save($emailProfile)) {$emailProfile['NewsletterEmailProfile']['id'] = $this->getLastInsertID();}
          else {  return false;  }
        }
        return $emailProfile;
    }

    public function emailExists($email){
      return $this->getByEmail($email);
    }

    public function ping() {
      return true;
    }


    public function updateAllFrequencyEventClassIdAndNewsletterProfileIds($frecuencyEventClassId, $newsletterProfileIdForCities, $newsletterProfileIdForGroups) {
      return $this->query(
        'update email_profiles set '. 
        ' previous_frecuency_event_class_id =  frecuency_event_class_id'.','.
        ' frecuency_event_class_id = '.$frecuencyEventClassId .','.
        ' newsletter_profile_id_for_cities = '.$newsletterProfileIdForCities .','.
        ' newsletter_profile_id_for_groups = '.$newsletterProfileIdForGroups .
        ';');
    }
    public function countEmailsByFrecuencyEventClassId($frecuencyEventClassId) {
      return $this->find('count', 
            array(
          'conditions' => 
                  array ('frecuency_event_class_id'=> $frecuencyEventClassId)
      ));
    }
}
