<?php

class NewsletterFrecuencyEventClass extends NewsletterAppModel {

    const NO_CLASS_CODE = 'NC';

    var $useTable = 'frecuency_event_classes';
    var $name = 'NewsletterFrecuencyEventClass';
    var $alias = 'NewsletterFrecuencyEventClass';
    public $recursive = - 1;
    public $mapSegments = array(
        'D' => 'B',
        'E' => 'C',
        NewsletterFrecuencyEventClass::NO_CLASS_CODE => NewsletterFrecuencyEventClass::NO_CLASS_CODE
    );

    public function ping() {
        return true;
    }

    public function findSegmentBaseByExtraSegment($extraSegment) {
        $code = $this->mapSegments[$extraSegment];
        $segment = $this->findByCode($code);
        return $segment;
    }

    public function findAllSegmentsSort() {
        return $this->find('all', array(
                    'order' => array(
                        'NewsletterFrecuencyEventClass.priority' => 'ASC'
                    )
        ));
    }

}
