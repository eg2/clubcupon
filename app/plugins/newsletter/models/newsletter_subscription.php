<?php


class NewsletterSubscription extends NewsletterAppModel {

    var $useTable = 'subscriptions';
    var $name = 'NewsletterSubscription';
    var $alias = 'NewsletterSubscription';
    public $recursive = -1;

    
    public function findToSendNewsByCityIdAndProfileIdForCitiesForceIndex($newsletterCityId, $newsletterProfileId, $fromSubscriptionId, $page, $limit){
        try {
            if ($page > 0) {
              $desde= ($page -1)*$limit;
            } else {
              $desde = 0;
            }
            $sql = "SELECT 
                        NewsletterSubscription.id,
                        NewsletterSubscription.email,
                        NewsletterEmailProfile.id,
                        NewsletterEmailProfile.frecuency_event_class_id,
                        NewsletterFrecuencyEventClass.code 
                    FROM subscriptions NewsletterSubscription 
                        FORCE INDEX (fk_subscriptions_cities1)
                        INNER JOIN email_profiles AS NewsletterEmailProfile
                            ON (NewsletterSubscription.email = NewsletterEmailProfile.email)
                        INNER JOIN frecuency_event_classes AS NewsletterFrecuencyEventClass
                             ON (NewsletterEmailProfile.frecuency_event_class_id = NewsletterFrecuencyEventClass.id)
                    WHERE
                        NewsletterSubscription.is_subscribed = 1 
                        AND NewsletterSubscription.city_id = ".$newsletterCityId."
                        AND NewsletterEmailProfile.newsletter_profile_id_for_cities = ".$newsletterProfileId."
                        AND NewsletterSubscription.id > ".$fromSubscriptionId."
                    ORDER BY NewsletterSubscription.id ASC
                    LIMIT ".$desde.",".$limit.";";
           $data = $this->query($sql);
           return  $data;
        }catch (Exception $e){
            $this->ApiLogger->error(__METHOD__,'Query Force Index Exception:',$e);
            return null;
        }
    }
    
    public function findToSendNewsByCityIdAndProfileIdForGroupsForceIndex($newsletterCityId, $newsletterProfileId, $fromSubscriptionId, $page, $limit){
        try {
            if ($page > 0) {
              $desde= ($page -1)*$limit;
            } else {
              $desde = 0;
            }
            $sql = "SELECT
            NewsletterSubscription.id,
            NewsletterSubscription.email,
            NewsletterEmailProfile.id,
            NewsletterEmailProfile.frecuency_event_class_id,
            NewsletterFrecuencyEventClass.code
            FROM subscriptions NewsletterSubscription
            FORCE INDEX (fk_subscriptions_cities1)
            INNER JOIN email_profiles AS NewsletterEmailProfile
            ON (NewsletterSubscription.email = NewsletterEmailProfile.email)
            INNER JOIN frecuency_event_classes AS NewsletterFrecuencyEventClass
            ON (NewsletterEmailProfile.frecuency_event_class_id = NewsletterFrecuencyEventClass.id)
            WHERE
            NewsletterSubscription.is_subscribed = 1
            AND NewsletterSubscription.city_id = ".$newsletterCityId."
            AND NewsletterEmailProfile.newsletter_profile_id_for_groups = ".$newsletterProfileId."
            AND NewsletterSubscription.id > ".$fromSubscriptionId."
            ORDER BY NewsletterSubscription.id ASC
            LIMIT ".$desde.",".$limit.";";
            $data = $this->query($sql);
            return  $data;
        }catch (Exception $e){
            $this->ApiLogger->error(__METHOD__,'Query Force Index Exception',$e);
            return null;
        }
    }
    
    public function findToSendNewsByCityIdAndProfileIdForCities($newsletterCityId, $newsletterProfileId, $fromSubscriptionId, $page, $limit) {
      return $this->find('all', array(
        'fields' =>  array(
            'NewsletterSubscription.id',
            'NewsletterSubscription.email',
            'NewsletterEmailProfile.id',
            'NewsletterEmailProfile.frecuency_event_class_id',
            'NewsletterFrecuencyEventClass.code',
        ),
        'order' => array(
                'NewsletterSubscription.id' => 'ASC',
        ),
        'conditions' => array(
            'NewsletterSubscription.city_id' => $newsletterCityId,
            'NewsletterSubscription.is_subscribed' => 1,
            'NewsletterEmailProfile.newsletter_profile_id_for_cities' => $newsletterProfileId,
             'NewsletterSubscription.id >'=>$fromSubscriptionId
          ),
        'joins' => array(
          array(
            'table' => 'email_profiles',
            'alias' => 'NewsletterEmailProfile',
            'type' => 'inner',
            'foreignKey' => false,
            'conditions' => array(
              'NewsletterSubscription.email = NewsletterEmailProfile.email'
            )
          ),
          array(
            'table' => 'frecuency_event_classes',
            'alias' => 'NewsletterFrecuencyEventClass',
            'type' => 'inner',
            'foreignKey' => false,
            'conditions' => array(
              'NewsletterEmailProfile.frecuency_event_class_id = NewsletterFrecuencyEventClass.id'
            )
          ),

          ),
        //'page' => $page,
        'limit' => $limit,
        ));
    }

    public function findToSendNewsByCityIdAndProfileIdForGroups($newsletterCityId, $newsletterProfileId, $fromSubscriptionId, $page, $limit) {
       return $this->find('all', array(
        'fields' =>  array(
            'NewsletterSubscription.id',
            'NewsletterSubscription.email',
            'NewsletterEmailProfile.id',
            'NewsletterEmailProfile.frecuency_event_class_id',
            'NewsletterFrecuencyEventClass.code',
        ),
        'order' => array(
                                       'NewsletterSubscription.id' => 'ASC',
        ),
        'conditions' => array(
            'NewsletterSubscription.city_id' => $newsletterCityId,
            'NewsletterSubscription.is_subscribed' => 1,
            'NewsletterEmailProfile.newsletter_profile_id_for_groups' => $newsletterProfileId,
            'NewsletterSubscription.id >'=>$fromSubscriptionId
          ),
        'joins' => array(
          array(
            'table' => 'email_profiles',
            'alias' => 'NewsletterEmailProfile',
            'type' => 'inner',
            'foreignKey' => false,
            'conditions' => array(
              'NewsletterSubscription.email = NewsletterEmailProfile.email'
            )
          ),
          array(
            'table' => 'frecuency_event_classes',
            'alias' => 'NewsletterFrecuencyEventClass',
            'type' => 'inner',
            'foreignKey' => false,
            'conditions' => array(
              'NewsletterEmailProfile.frecuency_event_class_id = NewsletterFrecuencyEventClass.id'
            )
          ),
          ),
        'page' => $page,
        'limit' => $limit,
        ));
    }

    public function findEmailsToSendNews() {
      return $this->find('all', array(
        'fields' => array(
          'distinct(NewsletterSubscription.email)'
        ),
        'conditions' => array(
            'NewsletterSubscription.is_subscribed' => 1,
        )
       ));
    }

    public function live() {
      return true;
    }
}

