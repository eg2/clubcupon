<?php

class NewsletterNewsletterProfile extends NewsletterAppModel {

  const NA_CODE = 'NA';

    var $useTable = 'newsletter_profiles';
    var $name = 'NewsletterNewsletterProfile';
    var $alias = 'NewsletterNewsletterProfile';
    public $recursive = -1;
    
    public function __construct($id = false, $table = null, $ds = null) {
      parent::__construct($id, $table, $ds);
    }

    public function findToSentForGroups() {
      return $this->find('all', array(
        'conditions' => array(
          'NewsletterNewsletterProfile.send_to_groups' => 1,
        ),
      ));
    }

    public function findToSentForCities() {
      return $this->find('all', array(
        'conditions' => array(
          'NewsletterNewsletterProfile.send_to_cities' => 1,
        ),
      ));
    }
}
