<?php

class NewsletterDeal extends NewsletterAppModel {

  var $useTable = 'deals';
  var $name = 'NewsletterDeal';
  var $alias = 'NewsletterDeal';

  public $recursive = -1;

  protected $toSendConditions = array(
    'NewsletterDeal.parent_deal_id = NewsletterDeal.id',
    'NewsletterDeal.deal_status_id' => array(
      ConstDealStatus::Open,
      ConstDealStatus::Tipped,
    ),
    'NewsletterDeal.is_subscription_mail_sent' => 0,
    'NewsletterDeal.publication_channel_type_id' => 1,
  );

  protected $toSendConditionsForce = array(
  		'NewsletterDeal.parent_deal_id = NewsletterDeal.id',
  		'NewsletterDeal.deal_status_id' => array(
  				ConstDealStatus::Open,
  				ConstDealStatus::Tipped,
  		),
  		'NewsletterDeal.publication_channel_type_id' => 1,
  );

  protected $toSendConditionsFinal = null;
  
  
  public function findToNewsletterByCityId($cityId,$forceDealsNoSend = false) {
  	if($forceDealsNoSend){
  		$this->toSendConditionsFinal = $this->toSendConditionsForce;
  	}else{
  		$this->toSendConditionsFinal = $this->toSendConditions;
  	}
    return $this->find('all', array(
      'fields' =>  array(
        'NewsletterDeal.id',
        'NewsletterDeal.name',
        'NewsletterDeal.custom_subject',
        'NewsletterDeal.is_side_deal',
      ),
      'order' => array(
        'NewsletterDeal.is_side_deal' => 'ASC',
        'NewsletterDeal.priority' => 'DESC',
        'NewsletterDeal.deal_status_id' => 'ASC',
        'NewsletterDeal.start_date' => 'ASC',
      ),
      'conditions' => array_merge(
        array(
          'NewsletterDeal.city_id' => $cityId,
        ),
        $this->toSendConditionsFinal
      ),

    ));
  }

  public function updateDealsAsSendedByCityId($cityId) {
    return $this->updateAll(array('NewsletterDeal.is_subscription_mail_sent' => 1),
      array_merge(
        array(
          'NewsletterDeal.city_id' => $cityId,
        ),
        $this->toSendConditions
      )
    );
  }

  public function live() {
    return "Hello :)";
  }

}

