<?php

class NewsletterCity extends NewsletterAppModel {

    var $useTable = 'cities';
    var $name = 'NewsletterCity';
    var $ales = 'NewsletterCity';
    public $recursive = - 1;

    function isDayOfWeekInListSql() {
        $sql = " IFNULL(LOCATE(DAYOFWEEK(DATE(NOW())), NULLIF(NewsletterCity.week_days,'')), DAYOFWEEK(DATE(NOW()))) > 0 ";
        return DboSource::expression($sql);
    }

    function fields() {
        return array(
            'NewsletterCity.id',
            'NewsletterCity.name',
            'NewsletterCity.is_group'
        );
    }

    function conditions($isGroup) {
        return array(
            'NewsletterCity.is_group' => $isGroup,
            'NewsletterCity.is_approved' => 1,
            'NewsletterCity.has_newsletter' => true,
            $this->isDayOfWeekInListSql()
        );
    }

    public function findGroupsToSendNewsletter() {
        $fields = $this->fields();
        $conditions = $this->conditions(1);
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    public function findCitiesToSendNewsletter() {
        $fields = $this->fields();
        $conditions = $this->conditions(0);
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    public function isGroup($city) {
        return $city['NewsletterCity']['is_group'] == 1;
    }

    public function isCity($city) {
        return !$this->isGroup($city);
    }

}
