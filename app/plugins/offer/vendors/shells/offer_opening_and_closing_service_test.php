<?php

App::import('Behavior', 'offer.OfferDeal');
App::import('Model', 'api.ApiDeal');
App::import('Component', 'offer.OfferService');

class OfferOpeningAndClosingServiceTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiDeal = null;
        $this->OfferService = null;
        $this->id = 41778;
        $this->parentDealId = 43217;
    }

    private function debug() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    private function prepareFindableData() {
        echo __METHOD__ . "\n";
        $result = $this->ApiDeal->updateAll(array(
            'start_date' => "'2016-06-01 00:00:00'",
            'end_date' => "'2016-07-31 00:00:00'",
            'deal_status_id' => ApiDeal::STATUS_UPCOMMING
                ), array(
            'id' => $this->id
        ));
    }

    private function prepareNotFindableData() {
        echo __METHOD__ . "\n";
        $this->ApiDeal->updateAll(array(
            'start_date' => "'2013-10-01 18:00:00'",
            'end_date' => "'2013-10-15 15:00:00'",
            'deal_status_id' => ApiDeal::STATUS_UPCOMMING
                ), array(
            'id' => $this->id
        ));
    }

    private function setUp() {
        echo __METHOD__ . "\n";
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('OfferDeal');
        $this->OfferService = new OfferServiceComponent();
    }

    private function tearDown() {
        echo __METHOD__ . "\n";
    }

    private function thowExceptionIfFindableIsNotFound($deals) {
        echo __METHOD__ . "\n";
        $found = false;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if ($deal['Deal']['id'] == $this->id) {
                    $found = true;
                    break;
                }
            }
        }
        if (!$found) {
            throw new DomainException();
        }
    }

    private function thowExceptionIfNotFindableIsFound($deals) {
        echo __METHOD__ . "\n";
        $found = false;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if ($deal['Deal']['id'] == $this->id) {
                    $found = true;
                    break;
                }
            }
        }
        if ($found) {
            throw new DomainException();
        }
    }

    private function thowExceptionIfEmpty($parameter) {
        echo __METHOD__ . "\n";
        if (empty($parameter)) {
            throw new DomainException();
        }
    }

    private function thowExceptionIfNotEqual($a, $b) {
        echo __METHOD__ . "\n";
        if (!($a == $b)) {
            throw new DomainException();
        }
    }

    private function testFindableShouldBeFound() {
        echo __METHOD__ . "\n";
        $this->prepareFindableData();
        $deals = $this->ApiDeal->findAllDealsThatStartInOneMonthAndEndsInAnother();
        $this->thowExceptionIfFindableIsNotFound($deals);
    }

    private function testNotFindableShouldNotBeFound() {
        echo __METHOD__ . "\n";
        $this->prepareNotFindableData();
        $deals = $this->ApiDeal->findAllDealsThatStartInOneMonthAndEndsInAnother();
        $this->thowExceptionIfNotFindableIsFound($deals);
    }

    private function testFindableShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $this->prepareFindableData();
        $deal = $this->ApiDeal->findById($this->id);
        $replica = array();
        if (!empty($deal)) {
            $replica = $this->OfferService->replicateDealThatStartInOneMonthAndEndsInAnother($deal);
        }
        $replicaSaved = $this->ApiDeal->findById($replica['Deal']['id']);
        $this->thowExceptionIfEmpty($replicaSaved);
        $this->thowExceptionIfNotEqual($replicaSaved['Deal']['start_date'], '2016-07-01 00:00:00');
        $this->thowExceptionIfNotEqual($replicaSaved['Deal']['deal_status_id'], ApiDeal::STATUS_UPCOMMING);
        $this->thowExceptionIfNotEqual($replicaSaved['Deal']['is_subscription_mail_sent'], 1);
        $dealSaved = $this->ApiDeal->findById($this->id);
        $this->thowExceptionIfNotEqual($dealSaved['Deal']['end_date'], '2016-06-30 23:59:00');
        $this->thowExceptionIfNotEqual($dealSaved['Deal']['redirect_url'], STATIC_DOMAIN_FOR_CLUBCUPON . '/deal/' . $replica['Deal']['slug']);
    }

    public function testDealAndSubdealsShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $deal = $this->ApiDeal->findById($this->parentDealId);
        $subdeals = $this->ApiDeal->findAllSubdealsByParentDealId($deal['Deal']['id']);
        $this->thowExceptionIfEmpty($deal);
        $this->thowExceptionIfEmpty($subdeals);
        $replica = $this->OfferService->replicate($deal['Deal']['id']);
        $this->thowExceptionIfEmpty($replica);
        $subdealsReplica = $this->ApiDeal->findAllSubdealsByParentDealId($replica['Deal']['id']);
        $this->thowExceptionIfNotEqual(count($subdeals), count($subdealsReplica));
        foreach ($subdealsReplica as $subdealReplica) {
            echo "  " . __METHOD__ . __LINE__ . "\n";
            $this->thowExceptionIfNotEqual($subdealReplica['Deal']['parent_deal_id'], $replica ['Deal']['id']);
        }
    }

    public function testDealAndSubdealsThatStartInOneMonthAndEndsInAnotherShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $deal = $this->ApiDeal->findById($this->parentDealId);
        $subdeals = $this->ApiDeal->findAllSubdealsByParentDealId($deal['Deal']['id']);
        $this->thowExceptionIfEmpty($deal);
        $this->thowExceptionIfEmpty($subdeals);
        $replica = $this->OfferService->replicateDealThatStartInOneMonthAndEndsInAnother($deal);
        $this->thowExceptionIfEmpty($replica);
        $subdealsReplica = $this->ApiDeal->findAllSubdealsByParentDealId($replica['Deal']['id']);
        $this->thowExceptionIfNotEqual(count($subdeals), count($subdealsReplica));
        foreach ($subdealsReplica as $subdealReplica) {
            $this->thowExceptionIfNotEqual($subdealReplica['Deal']['parent_deal_id'], $replica ['Deal']['id']);
        }
    }

    public function testQuery() {
        $fields = array(
            'Deal.id',
            'Deal.name',
            'Company.name',
            'City.name',
            'User.username'
        );
        $conditions = array(
            'Deal.id' => $this->id
        );
        $joins = array(
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'inner',
                'conditions' => array(
                    'Company.id = Deal.company_id'
                )
            ),
            array(
                'table' => 'cities',
                'alias' => 'City',
                'type' => 'inner',
                'conditions' => array(
                    'City.id = Deal.city_id'
                )
            ),
            array(
                'table' => 'users',
                'alias' => 'User',
                'type' => 'left',
                'conditions' => array(
                    'User.id = Deal.created_by'
                )
            )
        );
        $data = $this->ApiDeal->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'joins' => $joins,
            'recursive' => -1
        ));
    }

    public function main() {
        echo __METHOD__ . "\n";
        $this->setUp();
//        $this->testFindableShouldBeFound();
//        $this->testNotFindableShouldNotBeFound();
        $this->testFindableShouldBeReplicated();
//        $this->testDealAndSubdealsShouldBeReplicated();
//        $this->testDealAndSubdealsThatStartInOneMonthAndEndsInAnotherShouldBeReplicated();
        $this->tearDown();
    }

}
