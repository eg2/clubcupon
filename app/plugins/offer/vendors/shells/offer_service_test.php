<?php

App::import('Behavior', 'offer.OfferDeal');
App::import('Behavior', 'offer.OfferPaymentOptionDeal');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiPaymentOptionDeal');
App::import('Component', 'offer.OfferService');

class OfferServiceTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->OfferService = null;
        $this->example = null;
        $this->replica = null;
    }

    private function setUp() {
        echo __METHOD__ . "\n";
        $this->OfferService = new OfferServiceComponent();
        $this->ApiAttachment = new ApiAttachment();
        $this->ApiAttachment->Behaviors->attach('OfferAttachment');
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('OfferDeal');
        $this->ApiPaymentOptionDeal = new ApiPaymentOptionDeal();
        $this->ApiPaymentOptionDeal->Behaviors->attach('OfferPaymentOptionDeal');
        $this->example = array(
            'Deal' => array(// row #0
                'id' => 41778,
                'parent_deal_id' => 41778,
                'created' => '2013-10-23 17:30:47',
                'modified' => '2013-10-23 17:30:47',
                'user_id' => 307263,
                'seller_id' => 81903,
                'name' => 'REPLICA cambiando a MP Oferta CUPON 24-10 01hs al 06-11 13hs',
                'descriptive_text' => 'texto descriptivo de la oferta',
                'accepts_points' => 0,
                'hide_price' => 0,
                'has_pins' => NULL,
                'custom_subject' => '',
                'custom_company_name' => 'NOMBRE COMP  Desde La Oferta',
                'custom_company_contact_phone' => '',
                'custom_company_address1' => 'DIRE de la COMPA Desde la Oferta',
                'custom_company_address2' => '',
                'custom_company_city' => '',
                'custom_company_state' => '',
                'custom_company_country' => '',
                'custom_company_zip' => NULL,
                'show_map' => 0,
                'priority' => NULL,
                'risk' => NULL,
                'slug' => 'replica-cambiando-a-mp-oferta-cupon-24-10-01hs-al-06-11-13hs',
                'bitly_short_url' => NULL,
                'description' => '<p>asdf</p>',
                'private_note' => '',
                'original_price' => 10.00,
                'discounted_price' => 3.00,
                'minimal_amount_financial' => 0.00,
                'start_date' => '2013-10-23 18:00:00',
                'end_date' => '2013-11-04 15:00:00',
                'coupon_duration' => 20,
                'discount_percentage' => 70,
                'discount_amount' => 7.00,
                'savings' => 7.00,
                'min_limit' => 1,
                'max_limit' => NULL,
                'city_id' => 42550,
                'company_id' => 1780,
                'is_tourism' => 0,
                'is_end_user' => 0,
                'review' => '<p>asdf</p>',
                'deal_user_count' => 0,
                'deal_external_count' => 0,
                'payment_failed_count' => 0,
                'buy_min_quantity_per_user' => 1,
                'buy_max_quantity_per_user' => NULL,
                'deal_tipped_time' => '0000-00-00 00:00:00',
                'coupon_start_date' => '2013-10-23 18:00:00',
                'coupon_expiry_date' => '2013-11-12 18:00:00',
                'closure_modality' => 0,
                'deferred_payment' => 0,
                'first_closure_percentage' => 0,
                'payment_method' => 0,
                'coupon_condition' => '<p>asdf</p>',
                'coupon_highlights' => '<p>asdf</p>',
                'comment' => '<p>asdf</p>',
                'is_subscription_mail_sent' => 0,
                'is_side_deal' => 1,
                'deal_status_id' => 1,
                'bonus_amount' => 0.00,
                'commission_percentage' => 10.00,
                'downpayment_percentage' => 8,
                'pay_by_redeemed' => 0,
                'total_commission_amount' => 0.00,
                'total_purchased_amount' => 0.00,
                'net_profit' => 0.00,
                'show_sold_quantity' => 1,
                'meta_keywords' => '',
                'meta_description' => '',
                'bitly_short_url_subdomain' => 'http://bit.ly/18cDNzj',
                'bitly_short_url_prefix' => 'http://bit.ly/18cDNzj',
                'cluster_id' => 1,
                'deal_category_id' => 37,
                'only_price' => 0,
                'is_fibertel_deal' => 0,
                'created_by' => 307263,
                'modified_by' => 307263,
                'created_ip' => '127.0.0.1',
                'modified_ip' => '127.0.0.1',
                'similar_deal_id' => 41767,
                'is_ticketportal_deal' => 0,
                'ticketportal_deal_pin' => '',
                'ticketportal_deal_url' => '',
                'is_clarin365_deal' => 0,
                'subtitle' => 'Subtitulo de la oferta',
                'is_now' => 0,
                'publication_channel_type_id' => 1,
                'is_discount_mp' => 0,
                'redirect_url' => '',
                'is_product' => 0,
                'is_pending_accounting_event' => 0,
                'liquidate_count' => 0,
                'agreed_percentage' => 0,
                'amount_exempt' => NULL,
                'amount_full_iva' => NULL,
                'amount_half_iva' => NULL,
                'segmentation_profile_id' => 1537,
                'sales_forecast' => 100,
                'is_wholesaler' => 0,
                'is_receivership_delivery' => 0,
                'is_home_delivery' => 0,
                'payment_term' => 30,
                'campaign_code' => '99',
                'campaign_code_type' => 'E',
                'gender' => 'f',
                'product_campaign_id' => NULL,
                'product_product_id' => NULL,
                'product_inventory_strategy_id' => 1,
                'replicated_deal_id' => NULL,
            ),
        );
    }

    private function tearDown() {
        echo __METHOD__ . "\n";
        $successApiPaymentOptionDeal = $this->ApiPaymentOptionDeal->delete(array(
            'conditions' => array(
                'PaymentOptionDeal.deal_id' => $this->replica['Deal']['id']
            )
        ));
        $successApiAttachment = $this->ApiAttachment->delete(array(
            'conditions' => array(
                'Attachment.foreign_id' => $this->replica['Deal']['id']
            )
        ));
        $successApiDeal = $this->ApiDeal->delete($this->replica['Deal']['id']);
        if (!($successApiDeal || $successApiPaymentOptionDeal || $successApiAttachment)) {
            throw new DomainException();
        }
    }

    private function testExampleDealDealShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $replica = $this->OfferService->replicateDeal($this->example);
        if (empty($replica)) {
            throw new DomainException();
        }
    }

    private function testNotReplicableFieldsShouldNotBeReplicated() {
        echo __METHOD__ . "\n";
        $default = $this->ApiDeal->newDeal();
        $replica = $this->OfferService->replicateDeal($this->example);
        $nonReplicableFields = array_diff(array_keys($default['Deal']), $this->ApiDeal->replicableFileds());
        foreach ($nonReplicableFields as $field) {
            if ($replica['Deal'][$field] != $default['Deal'][$field]) {
                if ($replica['Deal'][$field] == $this->example['Deal'][$field]) {
                    throw new DomainException();
                }
            }
        }
    }

    private function testReplicableFiledsShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $replica = $this->OfferService->replicateDeal($this->example);
        $replicableFields = $this->ApiDeal->replicableFileds();
        foreach ($replicableFields as $field) {
            if ($replica['Deal'][$field] != $this->example['Deal'][$field]) {
                throw new DomainException();
            }
        }
    }

    private function assertPaymentOptionDealsShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $paymentOptionDeals = $this->ApiPaymentOptionDeal->findByDealId($this->replica['Deal']['id']);
        if (empty($paymentOptionDeals)) {
            throw new DomainException();
        }
    }

    private function assertAttachmentsShouldBeReplicated() {
        echo __METHOD__ . "\n";
        $attachments = $this->ApiAttachment->findAllByForeignId($this->replica['Deal']['id']);
        if (empty($attachments)) {
            throw new DomainException();
        }
    }

    private function testReplicaShouldBeSaved() {
        echo __METHOD__ . "\n";
        $replica = $this->OfferService->replicate($this->example['Deal']['id']);
        $this->replica = $replica;
        if ($replica['Deal']['id'] == null) {
            throw new DomainException();
        }
        $this->assertPaymentOptionDealsShouldBeReplicated();
        $this->assertAttachmentsShouldBeReplicated();
    }

    public function main() {
        echo __METHOD__ . "\n";
        $this->setUp();
        $this->testExampleDealDealShouldBeReplicated();
        $this->testNotReplicableFieldsShouldNotBeReplicated();
        $this->testReplicableFiledsShouldBeReplicated();
        $this->testReplicaShouldBeSaved();
        $this->tearDown();
    }

}
