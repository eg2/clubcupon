<?php

App::import('Component', 'offer.OfferPromotionApplicationService');

class OfferAutoassignPaymentMethodsByPromotionsShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->OfferPromotionApplicationService = new OfferPromotionApplicationServiceComponent();
        $this->paymentOptionIdWithPromotion = Configure::read("Promotions.PaymentOptionIdWithPromotion");
        $this->weekDaysWithPromotion = Configure::read("Promotions.WeekDayWithPromotion");
    }

    function main() {
        echo __METHOD__."\n";
        echo "Id de la Opcion de Pago con Promocion:".  $this->paymentOptionIdWithPromotion . "\n";
        echo "Dias de la Semana con Promocion:". implode(',', $this->weekDaysWithPromotion). "\n";
        if(isset($this->args[0])) {
          echo "Fecha recibida como Parametro: ". $this->args[0] . "\n";
          $this->dayOfWeek = date('N', strtotime($this->args[0]));
        } else {
          echo "Sin argumentos, utilizando la fecha de hoy \n";
          $this->dayOfWeek = date('N');
        }
        echo "Dia de la Semana: " . $this->dayOfWeek . "\n";

        if (in_array($this->dayOfWeek, $this->weekDaysWithPromotion)) {
          echo "Procesando Dia con Promocion, aplicandole a todas las ofertas activas la opcion de pago. \n";
          $this->OfferPromotionApplicationService->setPaymentMethodForAllDeals(
            $this->paymentOptionIdWithPromotion);
        } else {
          echo "Procesando Dia sin Promocion, quitandole a todas las ofertas activas la opcion de pago. \n";
          $this->OfferPromotionApplicationService->unsetPaymentMethodForAllDeals(
            $this->paymentOptionIdWithPromotion);
        }

        echo "Proceso Terminado.\n";
    }

}
