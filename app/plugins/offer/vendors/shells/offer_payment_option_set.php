<?php

App::import('Model', 'ConnectionManager');

class OfferPaymentOptionSetShell extends Shell {

    const DAY_OF_WEEK_SUNDAY = '1';
    const DAY_OF_WEEK_MONDAY = '2';
    const DAY_OF_WEEK_TUESDAY = '3';
    const DAY_OF_WEEK_WEDNESDAY = '4';
    const DAY_OF_WEEK_THURSDAY = '5';
    const DAY_OF_WEEK_FRIDAY = '6';
    const DAY_OF_WEEK_SATURDAY = '7';
    const IS_TOURISM_YES = 1;
    const IS_TOURISM_NO = 0;

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
    }

    private function addPaymentOptionIfIsdayOfWeek($id, $dayOfWeek, $isTourism, $paymentPlanId) {
        echo "  " . __METHOD__ . " " . json_encode(array($id, $dayOfWeek, $isTourism)) . "\n";
        $sql = " insert into payment_option_deals (deal_id, payment_option_id, payment_plan_id) " .
                " select Deal.id, {$id}, {$paymentPlanId} " .
                " from deals as Deal " .
                " where Deal.deal_status_id in (2, 5) " .
                " and IFNULL(LOCATE(DAYOFWEEK(DATE(NOW())), NULLIF('{$dayOfWeek}','')), DAYOFWEEK(DATE(NOW()))) > 0 " .
                " and Deal.city_id not in (44782) " .
                " and Deal.is_tourism = {$isTourism} ";
        $dataSource = ConnectionManager::getDataSource('master');
        return $dataSource->query($sql);
    }

    private function removePaymentOptionIfIsdayOfWeek($id, $dayOfWeek) {
        echo "  " . __METHOD__ . " " . json_encode(array($id, $dayOfWeek)) . "\n";
        $sql = " delete from payment_option_deals " .
                " where payment_option_id in ({$id}) " .
                " and IFNULL(LOCATE(DAYOFWEEK(DATE(NOW())), NULLIF('{$dayOfWeek}','')), DAYOFWEEK(DATE(NOW()))) > 0 ";
        $dataSource = ConnectionManager::getDataSource('master');
        return $dataSource->query($sql);
    }

    public function main() {
        echo __METHOD__ . " start " . "\n";
        $this->addPaymentOptionIfIsdayOfWeek(137, self::DAY_OF_WEEK_MONDAY, self::IS_TOURISM_NO, 7);
        $this->addPaymentOptionIfIsdayOfWeek(137, self::DAY_OF_WEEK_MONDAY, self::IS_TOURISM_YES, 7);
        $this->addPaymentOptionIfIsdayOfWeek(138, self::DAY_OF_WEEK_MONDAY, self::IS_TOURISM_NO, 3);
        $this->addPaymentOptionIfIsdayOfWeek(139, self::DAY_OF_WEEK_MONDAY, self::IS_TOURISM_YES, 3);
        $everyDayButMonday = '134567';
        $this->removePaymentOptionIfIsdayOfWeek(137, $everyDayButMonday);
        $this->removePaymentOptionIfIsdayOfWeek(138, $everyDayButMonday);
        $this->removePaymentOptionIfIsdayOfWeek(139, $everyDayButMonday);
        echo __METHOD__ . " end " . "\n";
    }

}
