<?php

App::import('Component', 'offer.OfferLogisticsAndStockValidator');
App::import('Model', 'product.ProductDeal');

class OfferValidatorsTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->OfferLogisticsAndStockValidator = null;
        $this->ProductDeal = null;
    }

    private function setUp() {
        echo __METHOD__ . "\n";
        $this->OfferLogisticsAndStockValidator = new OfferLogisticsAndStockValidatorComponent();
        $this->ProductDeal = new ProductDeal();
        $this->deals = array(
            43282
        );
    }

    private function tearDown() {
        echo __METHOD__ . "\n";
    }

    private function testOfferLogisticsAndStockValidator() {
        echo __METHOD__ . "\n";
        foreach ($this->deals as $id) {
            $deal = $this->ProductDeal->findById($id);
            var_dump($deal);
            echo "\n";
            $errors = $this->OfferLogisticsAndStockValidator->validate($deal['ProductDeal']);
            var_dump($errors);
            echo "----------------------------------------------------------------\n";
        }
    }

    public function main() {
        echo __METHOD__ . "\n";
        $this->setUp();
        $this->testOfferLogisticsAndStockValidator();
        $this->tearDown();
    }

}
