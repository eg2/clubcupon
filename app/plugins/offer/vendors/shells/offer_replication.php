<?php

App::import('Behavior', 'offer.OfferDeal');
App::import('Model', 'api.ApiDeal');
App::import('Component', 'offer.OfferService');

class OfferReplicationShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('OfferDeal');
        $this->OfferService = new OfferServiceComponent();
    }

    public function main() {
        echo __METHOD__ . " Inicio del proceso.\n";
        $deals = $this->ApiDeal->findAllDealsThatStartInOneMonthAndEndsInAnother();
        $replicas = array();
        if (!empty($deals)) {
            echo __METHOD__ . " Se encontraron " . count($deals) . " ofertas para replicar.\n";
            foreach ($deals as $deal) {
                $replica = $this->OfferService->replicateDealThatStartInOneMonthAndEndsInAnother($deal);
                if (!empty($replica)) {
                    echo __METHOD__ . " Se creo la replica " . $replica['Deal']['id'] . " para la oferta " . $deal['Deal']['id'] . ".\n";
                    $replicas[] = $replica;
                } else {
                    echo __METHOD__ . " No se pudo replicar la oferta " . $deal['Deal']['id'] . ".\n";
                }
            }
            echo __METHOD__ . " Se crearon " . count($replicas) . " replicas.\n";
        } else {
            echo __METHOD__ . " No se encontraron ofertas para replicar.\n";
        }
        echo __METHOD__ . " Fin del proceso.\n";
    }

}
