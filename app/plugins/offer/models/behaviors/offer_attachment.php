<?php

App::import('Core', 'Behavior');
App::import('Model', 'api.ApiAttachment');

class OfferAttachmentBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function findAllByForeignId(&$Model, $foreignId) {
        $fields = array(
            'Attachment.id',
            'Attachment.created',
            'Attachment.modified',
            'Attachment.class',
            'Attachment.foreign_id',
            'Attachment.filename',
            'Attachment.dir',
            'Attachment.mimetype',
            'Attachment.filesize',
            'Attachment.height',
            'Attachment.width',
            'Attachment.thumb',
            'Attachment.description'
        );
        $conditions = array(
            'Attachment.class' => array(
                ApiAttachment::CLASS_DEAL,
                ApiAttachment::CLASS_DEAL_MULTIPLE,
                ApiAttachment::CLASS_DEAL_NEWSLETTER,
                ApiAttachment::CLASS_NOW_DEAL
            ),
            'Attachment.foreign_id' => $foreignId
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

}
