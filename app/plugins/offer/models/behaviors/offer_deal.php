<?php

App::import('Core', 'Behavior');
App::import('Model', 'api.ApiDeal');

class OfferDealBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function replicableFileds() {
        return array(
            // 'id',
            // 'parent_deal_id',
            // 'created',
            // 'modified',
            'user_id',
            'seller_id',
            'name',
            'descriptive_text',
            'accepts_points',
            'hide_price',
            'has_pins',
            // 'custom_subject',
            'custom_company_name',
            'custom_company_contact_phone',
            'custom_company_address1',
            'custom_company_address2',
            'custom_company_city',
            'custom_company_state',
            'custom_company_country',
            'custom_company_zip',
            'show_map',
            'priority',
            'risk',
            // 'slug',
            'bitly_short_url',
            'description',
            'private_note',
            'original_price',
            'discounted_price',
            'minimal_amount_financial',
            'start_date',
            'end_date',
            'coupon_duration',
            'discount_percentage',
            'discount_amount',
            'savings',
            'min_limit',
            'max_limit',
            'city_id',
            'company_id',
            'is_tourism',
            'is_end_user',
            'review',
            // 'deal_user_count',
            // 'deal_external_count',
            // 'payment_failed_count',
            'buy_min_quantity_per_user',
            'buy_max_quantity_per_user',
            // 'deal_tipped_time',
            'coupon_start_date',
            'coupon_expiry_date',
            'closure_modality',
            'deferred_payment',
            'first_closure_percentage',
            'payment_method',
            'coupon_condition',
            'coupon_highlights',
            'comment',
            // 'is_subscription_mail_sent',
            'is_side_deal',
            // 'deal_status_id',
            'bonus_amount',
            'commission_percentage',
            'downpayment_percentage',
            'pay_by_redeemed',
            // 'total_commission_amount',
            // 'total_purchased_amount',
            // 'net_profit',
            'show_sold_quantity',
            'meta_keywords',
            'meta_description',
            'bitly_short_url_subdomain',
            'bitly_short_url_prefix',
            'cluster_id',
            'deal_category_id',
            'only_price',
            'is_fibertel_deal',
            // 'created_by',
            // 'modified_by',
            // 'created_ip',
            // 'modified_ip',
            'similar_deal_id',
            'is_ticketportal_deal',
            'ticketportal_deal_pin',
            'ticketportal_deal_url',
            'is_clarin365_deal',
            'subtitle',
            'is_now',
            'publication_channel_type_id',
            'is_discount_mp',
            // 'redirect_url',
            'is_product',
            // 'is_pending_accounting_event',
            // 'liquidate_count',
            'agreed_percentage',
            'amount_exempt',
            'amount_full_iva',
            'amount_half_iva',
            'is_wholesaler',
            'is_receivership_delivery',
            'is_home_delivery',
            'payment_term',
            'campaign_code',
            'campaign_code_type',
            'segmentation_profile_id',
            'sales_forecast',
            'gender',
            'product_campaign_id',
            'product_product_id',
            'product_inventory_strategy_id',
            // 'replicated_deal_id',
            'deal_trade_agreement_id',
            'amount_resolution_3450',
            'is_shipping_address',
            'requisition_number',
            'is_variable_expiration',
            'is_shipping_adress_user'
        );
    }

    function isReplicable(&$Model, $deal) {
        return true;
    }

    function isReplicableField(&$Model, $field) {
        return in_array($field, $this->replicableFileds());
    }

    function newDeal() {
        return array(
            'Deal' => array(
                'id' => null,
                'parent_deal_id' => null,
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'user_id' => null,
                'seller_id' => null,
                'name' => null,
                'descriptive_text' => null,
                'accepts_points' => 0,
                'hide_price' => 0,
                'has_pins' => 0,
                'custom_subject' => null,
                'custom_company_name' => null,
                'custom_company_contact_phone' => null,
                'custom_company_address1' => null,
                'custom_company_address2' => null,
                'custom_company_city' => null,
                'custom_company_state' => null,
                'custom_company_country' => null,
                'custom_company_zip' => null,
                'show_map' => 0,
                'priority' => null,
                'risk' => null,
                'slug' => null,
                'bitly_short_url' => null,
                'description' => null,
                'private_note' => null,
                'original_price' => null,
                'discounted_price' => null,
                'minimal_amount_financial' => null,
                'start_date' => date("Y-m-d H:i:s"),
                'end_date' => date("Y-m-d H:i:s"),
                'coupon_duration' => null,
                'discount_percentage' => null,
                'discount_amount' => null,
                'savings' => null,
                'min_limit' => 1,
                'max_limit' => null,
                'city_id' => ApiDeal::CITY_ID_DEFAULT,
                'company_id' => null,
                'is_tourism' => 0,
                'is_end_user' => 0,
                'review' => null,
                'deal_user_count' => 0,
                'deal_external_count' => 0,
                'payment_failed_count' => 0,
                'buy_min_quantity_per_user' => 1,
                'buy_max_quantity_per_user' => null,
                'deal_tipped_time' => '0000-00-00 00:00:00',
                'coupon_start_date' => null,
                'coupon_expiry_date' => null,
                'closure_modality' => 0,
                'deferred_payment' => 0,
                'first_closure_percentage' => 0,
                'payment_method' => 0,
                'coupon_condition' => null,
                'coupon_highlights' => null,
                'comment' => null,
                'is_subscription_mail_sent' => 0,
                'is_side_deal' => 0,
                'deal_status_id' => ApiDeal::STATUS_DRAFT,
                'bonus_amount' => 0,
                'commission_percentage' => 0,
                'downpayment_percentage' => 0,
                'pay_by_redeemed' => 0,
                'total_commission_amount' => 0,
                'total_purchased_amount' => 0,
                'net_profit' => 0,
                'show_sold_quantity' => 0,
                'meta_keywords' => null,
                'meta_description' => null,
                'bitly_short_url_subdomain' => 'http://bit.ly/1czOeo0',
                'bitly_short_url_prefix' => 'http://bit.ly/1czOeo0',
                'cluster_id' => 1,
                'deal_category_id' => 0,
                'only_price' => 0,
                'is_fibertel_deal' => 0,
                'created_by' => ApiDeal::CREATED_BY_DEFAULT,
                'modified_by' => ApiDeal::MODIFIED_BY_DEFAULT,
                'created_ip' => ApiDeal::CREATED_BY_IP_DEFAULT,
                'modified_ip' => ApiDeal::MODIFIED_BY_IP_DEFAULT,
                'similar_deal_id' => null,
                'is_ticketportal_deal' => 0,
                'ticketportal_deal_pin' => null,
                'ticketportal_deal_url' => null,
                'is_clarin365_deal' => 0,
                'subtitle' => null,
                'is_now' => 0,
                'publication_channel_type_id' => ApiDeal::PUBLICATION_CHANNEL_TYPE_WEB,
                'is_discount_mp' => 0,
                'redirect_url' => null,
                'is_product' => 0,
                'is_pending_accounting_event' => 0,
                'liquidate_count' => 0,
                'agreed_percentage' => 0,
                'amount_exempt' => null,
                'amount_full_iva' => null,
                'amount_half_iva' => null,
                'segmentation_profile_id' => ApiDeal::SEGMENTATION_PROFILE_ID_DEFAULT,
                'sales_forecast' => null,
                'is_wholesaler' => 0,
                'is_receivership_delivery' => 0,
                'is_home_delivery' => 0,
                'payment_term' => ApiDeal::PAYMENT_TERM_DEFAULT,
                'campaign_code' => null,
                'campaign_code_type' => 'G',
                'gender' => ApiDeal::GENDER_FEMALE,
                'product_campaign_id' => null,
                'product_product_id' => null,
                'product_inventory_strategy_id' => null,
                'replicated_deal_id' => null,
                'deal_trade_agreement_id' => null,
                'is_variable_expiration' => 0,
                'is_shipping_adress_user' => 0
            ),
        );
    }

    private function isStartInOneMonthAndEndsInAnotherSql() {
        return "((YEAR(Deal.start_date) = YEAR(DATE_ADD(Deal.end_date, INTERVAL -1 MINUTE)) AND MONTH(Deal.start_date) < MONTH(DATE_ADD(Deal.end_date, INTERVAL -1 MINUTE))) OR (YEAR(Deal.start_date) < YEAR(DATE_ADD(Deal.end_date, INTERVAL -1 MINUTE))))";
    }

    private function isNotSubdealSql() {
        return "Deal.id = Deal.parent_deal_id";
    }

    private function isSubdealSql() {
        return "Deal.id <> Deal.parent_deal_id";
    }

    private function isDurationAtLeastOneSecond() {
        return "DATEDIFF(Deal.end_date,Deal.start_date) > 0";
    }

    public function isVariableExpiration($deal) {
        return $deal['Deal']['is_variable_expiration'];
    }

    public function findAllDealsThatStartInOneMonthAndEndsInAnother() {
        $fields = array(
            'Deal.id',
            'Deal.start_date',
            'Deal.end_date'
        );
        $conditions = array(
            'Deal.deal_status_id' => array(
                ApiDeal::STATUS_UPCOMMING,
                ApiDeal::STATUS_OPEN,
                ApiDeal::STATUS_TIPPED
            ),
            'YEAR(Deal.start_date) >=' => 2010,
            $this->isStartInOneMonthAndEndsInAnotherSql(),
            $this->isNotSubdealSql(),
            $this->isDurationAtLeastOneSecond()
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    public function findAllSubdealsByParentDealId(&$Model, $parentDealId) {
        $conditions = array(
            'Deal.parent_deal_id' => $parentDealId,
            'Deal.deal_status_id' => array(
                ApiDeal::STATUS_UPCOMMING,
                ApiDeal::STATUS_OPEN,
                ApiDeal::STATUS_TIPPED,
                ApiDeal::STATUS_DRAFT,
                ApiDeal::STATUS_CLOSED
            ),
            $this->isSubdealSql()
        );
        return $this->model->find('all', array(
                    'conditions' => $conditions
        ));
    }

}
