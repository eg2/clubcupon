<?php

App::import('Core', 'Behavior');

class OfferPaymentOptionDealBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function findAllByDealId(&$Model, $dealId) {
        $fields = array(
            'PaymentOptionDeal.id',
            'PaymentOptionDeal.deal_id',
            'PaymentOptionDeal.payment_option_id',
            'PaymentOptionDeal.payment_plan_id'
        );
        $conditions = array(
            'PaymentOptionDeal.deal_id' => $dealId
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

}
