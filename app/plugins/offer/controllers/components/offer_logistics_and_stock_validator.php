<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'product.ProductDeal');
App::import('Model', 'product.ProductProduct');
App::import('Model', 'shipping.ShippingAddress');
App::import('Model', 'shipping.ShippingAddressDeal');

class OfferLogisticsAndStockValidatorComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct('offer');
        $this->ProductDeal = new ProductDeal();
        $this->ProductProduct = new ProductProduct();
        $this->ShippingAddress = new ShippingAddress();
        $this->ShippingAddressDeal = new ShippingAddressDeal();
    }

    public function validate($data) {
        $errors = array();
        $deal = $this->asProductDeal($data);
        if (!empty($deal)) {
            if ($this->ProductDeal->isPrepurchaseWithOwnLogistics($deal)) {
                $endUserErrors = $this->endUserErrors($deal);
                $associatedProductErrors = $this->associatedProductErrors($deal);
                $shippingAdressErrors = $this->shippingAdressErrors($deal);
                $errors = array_merge($endUserErrors, $associatedProductErrors, $shippingAdressErrors);
            }
        }
        return $errors;
    }

    private function asProductDeal($data) {
        $deal['ProductDeal'] = array();
        foreach ($data as $key => $value) {
            $deal['ProductDeal'][$key] = $value;
        }
        return $deal;
    }

    private function endUserErrors($deal) {
        $error = array();
        if (!$this->ProductDeal->isEndUser($deal)) {
            $error['endUserError'] = 'Las ofertas de precompra deben ser de facturación al usuario final';
        }
        return $error;
    }

    private function associatedProductErrors($deal) {
        $error = array();
        $productProduct = $this->ProductProduct->findById($deal['ProductDeal']['product_product_id']);
        if (!empty($productProduct)) {
            if (!$this->ProductProduct->hasProductInventory($productProduct)) {
                $error['associatedProductError'] = 'Las ofertas de precompra deben tener un producto con producto inventario asociado';
            }
        } else {
            $error['associatedProductError'] = 'Las ofertas de precompra deben tener un producto asociado';
        }
        return $error;
    }

    private function shippingAdressErrors($deal) {
        $error = array();
        $shippingAddressDeals = $this->ShippingAddressDeal->findAllByDealId($deal['ProductDeal']['id']);
        if (!empty($shippingAddressDeals)) {
            $storehouseError = false;
            foreach ($shippingAddressDeals as $shippingAddressDeal) {
                $shippingAddress = $this->ShippingAddress->findById($shippingAddressDeal['ShippingAddressDeal']['shipping_address_id']);
                if (!empty($shippingAddress)) {
                    $storehouseError = $storehouseError || !$this->ShippingAddress->hasStorehouseId($shippingAddress);
                }
            }
            if ($storehouseError) {
                $error['shippingAdressError'] = 'Las ofertas de precompra deben tener todos sus punto de retiro asociados con identificador de almacén configurado';
            }
        } else {
            $error['shippingAdressError'] = 'Las ofertas de precompra deben tener un punto de retiro asociado';
        }
        return $error;
    }

}
