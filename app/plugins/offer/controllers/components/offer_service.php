<?php

App::import('Behavior', 'offer.OfferAttachment');
App::import('Behavior', 'offer.OfferDeal');
App::import('Behavior', 'offer.OfferPaymentOptionDeal');
App::import('Behavior', 'api.Sluggable');
App::import('Behavior', 'api.WhoDidIt');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiAttachmentService');
App::import('Component', 'offer.OfferDateUtils');
App::import('Component', 'offer.OfferLogisticsAndStockValidator');
App::import('Model', 'ConnectionManager');
App::import('Model', 'api.ApiAttachment');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiPaymentOptionDeal');
App::import('Model', 'shipping.ShippingAddressDeal');

class OfferServiceComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct('offer');
        $this->ApiAttachment = new ApiAttachment();
        $this->ApiAttachment->Behaviors->attach('OfferAttachment');
        $this->ApiAttachmentService = new ApiAttachmentServiceComponent();
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('OfferDeal');
        $this->ApiDeal->Behaviors->attach('Sluggable', array(
            'label' => array(
                'name'
            ),
            'overwrite' => false
        ));
        $this->ApiDeal->Behaviors->attach('WhoDidIt');
        $this->ApiPaymentOptionDeal = new ApiPaymentOptionDeal();
        $this->ApiPaymentOptionDeal->Behaviors->attach('OfferPaymentOptionDeal');
        $this->OfferDateUtils = new OfferDateUtilsComponent();
        $this->OfferLogisticsAndStockValidator = new OfferLogisticsAndStockValidatorComponent();
        $this->ShippingAddressDeal = new ShippingAddressDeal();
    }

    static function instance() {
        if (self::$instance == null) {
            self::$instance = new OfferServiceComponent();
        }
        return self::$instance;
    }

    function replicateDeal($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $replica = array();
        if ($this->ApiDeal->isReplicable($deal)) {
            $replica = $this->ApiDeal->newDeal();
            $replica['Deal']['replicated_deal_id'] = $deal['Deal']['id'];
            foreach ($deal['Deal'] as $key => $value) {
                if ($this->ApiDeal->isReplicableField($key)) {
                    $replica['Deal'][$key] = $value;
                }
            }
        }
        return $replica;
    }

    function createAndSaveDealReplica($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $replica = $this->replicateDeal($deal);
        if (!empty($replica)) {
            $this->ApiDeal->save($replica);
            $replica = $this->ApiDeal->findById($this->ApiDeal->getLastInsertID());
            if (!empty($replica)) {
                $replica['Deal']['parent_deal_id'] = $replica['Deal']['id'];
                $this->ApiDeal->save($replica);
            }
        }
        return $replica;
    }

    function createAndSavePaymentOptionDealsReplica($paymentOptionDeals, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $paymentOptionDeals,
            $deal
        ));
        $paymentOptionDealsReplica = array();
        foreach ($paymentOptionDeals as $paymentOptionDeal) {
            $paymentOptionDeal['PaymentOptionDeal']['id'] = null;
            $paymentOptionDeal['PaymentOptionDeal']['deal_id'] = $deal['Deal']['id'];
            $paymentOptionDealsReplica[] = $paymentOptionDeal;
        }
        $this->ApiPaymentOptionDeal->saveAll($paymentOptionDealsReplica);
        return $paymentOptionDealsReplica;
    }

    function createAndSaveAttachmentReplica($attachment, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $attachment,
            $deal
        ));
        $this->ApiAttachmentService->replicate($attachment, $attachment['Attachment']['class'], $deal['Deal']['id']);
    }

    function createAndSaveShippingAddressDealReplica($shippingAddressIds, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $shippingAddressIds,
            $deal
        ));
        $shippingAddressDealsReplica = array();
        foreach ($shippingAddressIds as $shippingAddressId) {
            $replicaShippingAddressDeal = array();
            $replicaShippingAddressDeal['ShippingAddressDeal']['shipping_address_id'] = $shippingAddressId;
            $replicaShippingAddressDeal['ShippingAddressDeal']['deal_id'] = $deal['Deal']['id'];
            $shippingAddressDealsReplica[] = $replicaShippingAddressDeal;
        }
        $this->ShippingAddressDeal->saveAll($shippingAddressDealsReplica);
        return $shippingAddressDealsReplica;
    }

    function addPaymentOptions($deal, $replica) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $replica
        ));
        $paymentOptionDeals = $this->ApiPaymentOptionDeal->findAllByDealId($deal['Deal']['id']);
        if (!empty($paymentOptionDeals)) {
            $this->createAndSavePaymentOptionDealsReplica($paymentOptionDeals, $replica);
        }
    }

    function addAttachments($deal, $replica) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $replica
        ));
        $attachments = $this->ApiAttachment->findAllByForeignId($deal['Deal']['id']);
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $this->createAndSaveAttachmentReplica($attachment, $replica);
            }
        }
    }

    function addShippingAddress($deal, $replica) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $replica
        ));
        $shippingAddressIds = $this->ShippingAddressDeal->findAddressesByDealId($deal['Deal']['id'], true);
        if (!empty($shippingAddressIds)) {
            $this->createAndSaveShippingAddressDealReplica($shippingAddressIds, $replica);
        }
    }

    function createAndSaveDealReplicaWithAssociatedData($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $replica = $this->createAndSaveDealReplica($deal);
        if (!empty($replica)) {
            $this->addPaymentOptions($deal, $replica);
            if (!$this->ApiDeal->isSubDeal($deal)) {
                $this->addAttachments($deal, $replica);
            }
            $this->addShippingAddress($deal, $replica);
            $this->addToSearchIndex($replica);
        }
        return $replica;
    }

    function replicate($dealId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $dealId
        ));
        $replica = array();
        $deal = $this->ApiDeal->findById($dealId);
        if (!empty($deal)) {
            $replica = $this->createAndSaveDealReplicaWithAssociatedData($deal);
        }
        $subdeals = $this->ApiDeal->findAllSubdealsByParentDealId($deal['Deal']['id']);
        if (!empty($subdeals)) {
            foreach ($subdeals as $subdeal) {
                $subdealReplica = $this->createAndSaveDealReplicaWithAssociatedData($subdeal);
                $subdealReplica['Deal']['parent_deal_id'] = $replica['Deal']['id'];
                $this->ApiDeal->save($subdealReplica);
            }
        }
        return $replica;
    }

    function replicateForResell($dealId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $dealId
        ));
        $replica = $this->replicate($dealId);
        if (!empty($replica)) {
            $replica['Deal']['is_resell'] = 1;
            $replica['Deal']['commission_percentage'] = 82.64;
            $this->ApiDeal->save($replica);
        }
        return $replica;
    }

    function validateLogisticsAndStock($dealId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $dealId
        ));
        return $this->OfferLogisticsAndStockValidator->validate($dealId);
    }

    function isDurationAtLeastOneSecond($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $startDate = $this->OfferDateUtils->firstDayOfNextMonth($deal['Deal']['start_date']);
        $endDate = $deal['Deal']['end_date'];
        return strtotime($endDate) - strtotime($startDate) > 0;
    }

    function addToSearchIndex($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $dataSource = ConnectionManager::getDataSource('master');
        $company = $dataSource->query("select name from companies as Company where Company.id = {$deal['Deal']['company_id']};");
        $username = '';
        $user = $dataSource->query("select name from users as User where User.id = {$deal['Deal']['created_by']};");
        if (!empty($user)) {
            $username = $user[0]['User']['username'];
        }
        $city = $dataSource->query("select name from cities as City where City.id = {$deal['Deal']['city_id']};");
        $data = "'" . $deal['Deal']['id'] . ' ' . $deal['Deal']['name'] . ' ' . $company[0]['Company']['name'] . ' ' . $username . ' ' . $city[0]['City']['name'] . "'";
        $sql = "INSERT INTO search_index (association_key, model, `data`, created, modified) VALUES ({$deal['Deal']['id']}, 'Deal', {$data}, now(), now());";
        $dataSource->rawQuery($sql);
    }

    function setValuesAndSave($deal, $replica) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $replica
        ));
        $replica['Deal']['start_date'] = $this->OfferDateUtils->firstDayOfNextMonth($deal['Deal']['start_date']);
        $days = $this->OfferDateUtils->daysBetween($deal['Deal']['coupon_start_date'], $deal['Deal']['start_date']);
        if ($days > 0) {
            $replica['Deal']['coupon_start_date'] = $this->OfferDateUtils->addDays($replica['Deal']['start_date'], $days);
        } else {
            $replica['Deal']['coupon_start_date'] = $replica['Deal']['start_date'];
        }
        if ($this->ApiDeal->isVariableExpiration($deal)) {
            $replica['Deal']['coupon_expiry_date'] = $this->ApiDeal->couponLastExpirationDate($replica);
        }
        if ($replica['Deal']['coupon_expiry_date'] < $replica['Deal']['end_date']) {
            $replica['Deal']['end_date'] = $replica['Deal']['coupon_expiry_date'];
        }
        $replica['Deal']['deal_status_id'] = ApiDeal::STATUS_UPCOMMING;
        $replica['Deal']['is_subscription_mail_sent'] = 1;
        $replica['Deal']['private_note'] = 'Replica automatica';
        $deal['Deal']['end_date'] = $this->OfferDateUtils->lastDayOfMonth($deal['Deal']['start_date']);
        $deal['Deal']['redirect_url'] = STATIC_DOMAIN_FOR_CLUBCUPON . '/deal/' . $replica['Deal']['slug'];
        $this->ApiDeal->save($replica);
        $this->ApiDeal->save($deal);
    }

    function copyFieldsToSubdeals($dealId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $dealId
        ));
        $deal = $this->ApiDeal->findById($dealId);
        $subdeals = $this->ApiDeal->findAllSubdealsByParentDealId($deal['Deal']['id']);
        if (!empty($subdeals)) {
            foreach ($subdeals as $subdeal) {
                $subdeal['Deal']['start_date'] = $deal['Deal']['start_date'];
                $subdeal['Deal']['end_date'] = $deal['Deal']['end_date'];
                $subdeal['Deal']['coupon_start_date'] = $deal['Deal']['coupon_start_date'];
                $subdeal['Deal']['coupon_expiry_date'] = $deal['Deal']['coupon_expiry_date'];
                $subdeal['Deal']['coupon_duration'] = $deal['Deal']['coupon_duration'];
                $subdeal['Deal']['deal_status_id'] = $deal['Deal']['deal_status_id'];
                $subdeal['Deal']['is_subscription_mail_sent'] = $deal['Deal']['is_subscription_mail_sent'];
                $subdeal['Deal']['private_note'] = $deal['Deal']['private_note'];
                $this->ApiDeal->save($subdeal);
            }
        }
    }

    function replicateDealThatStartInOneMonthAndEndsInAnother($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $replica = array();
        if ($this->isDurationAtLeastOneSecond($deal)) {
            $replica = $this->replicate($deal['Deal']['id']);
            if (!empty($replica)) {
                $this->setValuesAndSave($deal, $replica);
                $this->copyFieldsToSubdeals($deal['Deal']['id']);
                $this->copyFieldsToSubdeals($replica['Deal']['id']);
            }
        }
        return $replica;
    }

}
