<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiAttachmentService');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiPaymentOptionDeal');

class OfferPromotionApplicationServiceComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct('offer');
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('OfferDeal');
        $this->ApiPaymentOptionDeal = new ApiPaymentOptionDeal();
        $this->ApiPaymentOptionDeal->Behaviors->attach('OfferPaymentOptionDeal');
    }

    static function instance() {
        if (self::$instance == null) {
            self::$instance = new OfferPromotionApplicationServiceComponent();
        }
        return self::$instance;
    }

    public function setPaymentMethodForAllDeals($paymentOptionId) {
      $this->ApiLogger->info(__METHOD__, array('paymentOptionId' => $paymentOptionId));

      $page = 1;
      $limit = 50;
      $deals = $this->ApiDeal->findAllForApi(array(), $limit, $page);

      while (!empty($deals)) {
        $this->ApiLogger->info("Procesando Pagina de Ofertas: ", array(
          'pagina' => $page,
          'cantidad de ofertas' => count($deals)
         ));

        foreach ($deals as $deal) {
          $this->ApiLogger->put('DealId', $deal['Deal']['id']);
          $this->ApiLogger->info("Procesando Oferta", array('Deal' =>$deal));
          $isOptionSelected = $this->hasDealPaymentOptionSelected($deal['Deal']['id'], $paymentOptionId);

          if($isOptionSelected) {
            $this->ApiLogger->info("Opcion de Pago Ya Seleccionada.");
          } else {
              $this->ApiLogger->info("Opcion de Pago no Seleccionada, Agregando opcion.");
            $paymentOptionDeal = $this->ApiPaymentOptionDeal->newPaymentOptionDeal();
            $paymentOptionDeal['PaymentOptionDeal']['payment_option_id'] = $paymentOptionId;
            $paymentOptionDeal['PaymentOptionDeal']['deal_id'] = $deal['Deal']['id'];
            if($this->ApiPaymentOptionDeal->saveAll(array($paymentOptionDeal))) {
              $this->ApiLogger->info("Opcion de Pago Seleccionada.");
            } else {
              $this->ApiLogger->info("Error Opcion de Pago Seleccionada.");
            }
          }
        }
        $page = $page + 1;
        $deals = $this->ApiDeal->findAllForApi(array(), $limit, $page);
      }

      $this->ApiLogger->info("Sin Mas Ofertas a Procesar.");
    }

    public function unsetPaymentMethodForAllDeals($paymentOptionId) {
      $this->ApiLogger->info(__METHOD__, array('paymentOptionId' => $paymentOptionId)); 

      $page = 1;
      $limit = 50;
      $deals = $this->ApiDeal->findAllForApi(array(), $limit, $page);

      while (!empty($deals)) {
        $this->ApiLogger->info("Procesando Pagina de Ofertas: ", array(
          'pagina' => $page,
          'cantidad de ofertas' => count($deals)
         ));

        foreach ($deals as $deal) {
          $this->ApiLogger->put('DealId', $deal['Deal']['id']);
          $this->ApiLogger->info("Procesando Oferta", array('Deal' =>$deal));
          $paymentOptionDeal = $this->findDealPaymentOptionDeal($deal['Deal']['id'], $paymentOptionId);

          if(!is_null($paymentOptionDeal)) {
            $this->ApiLogger->info("Opcion de Pago Seleccionada, Quitando Opcion.");
            if($this->ApiPaymentOptionDeal->delete($paymentOptionDeal['PaymentOptionDeal']['id'])) {
              $this->ApiLogger->info("Se Quito la Opcion de Pago.");
            } else {
              $this->ApiLogger->info("Error al Intentar Quitar la Opcion de Pago.");
            }

          } else {
            $this->ApiLogger->info("Opcion de Pago No Seleccionada.");
          }
        }

        $page = $page + 1;
        $deals = $this->ApiDeal->findAllForApi(array(), $limit, $page);
      }

      $this->ApiLogger->info("Sin Mas Ofertas a Procesar.");
    }

    public function hasDealPaymentOptionSelected($dealId, $paymentOptionId) {
      $paymentOptionsByDeal =
        $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($dealId);

      foreach ($paymentOptionsByDeal as $paymentOptionDeal) {
        if ($paymentOptionDeal['PaymentOption']['id'] == $paymentOptionId) {
          return true;
        }
      }
      return false;
    }

    public function findDealPaymentOptionDeal($dealId, $paymentOptionId) {
      $paymentOptionsByDeal =
        $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($dealId);

      foreach ($paymentOptionsByDeal as $paymentOptionDeal) {
        if ($paymentOptionDeal['PaymentOption']['id'] == $paymentOptionId) {
          return $paymentOptionDeal;
        }
      }
      return null;
    }


}
