<?php

class PromotionsChristmasController extends PromotionAppController {

    public $name = 'PromotionsChristmas';
    public $uses = array(
    );
    public $days = array(
        'monday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/village-cines-entrada-en-sala-tradicional-valido-para-todos-los-dias-en-todos-los-complejos-174'
        ),
        'tuesday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/1-mes-de-tratamiento-integral-en-orthomolecular-buenos-aires'
        ),
        'wednesday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/2-copas-de-champagne-36-piezas-de-sushi-y-2-cervezas-en-azul-profundo-8'
        ),
        'thursday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/2-cajas-de-bombones-garoto-x-355-gramos-53'
        ),
        'friday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/100-fotos-10x15-o-13x18-en-imagena-envio-gratis-a-todo-el-pais-536'
        ),
        'saturday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/album-13x18-para-200-fotos-color-verde-1'
        ),
        'sunday' => array(
            'title' => 'Navidad 2014',
            'href' => '/ofertas-especiales/deal/tableta-de-chocolate-georgalos-duo-de-145-grs-32'
        )
    );

    private function initializeEnvironment() {
        $this->Security->enabled = false;
    }

    private function initializeDependencies() {
        
    }

    public function beforeFilter() {
        $this->initializeEnvironment();
        $this->initializeDependencies();
        parent::beforeFilter();
    }

    public function beforeRender() {
        $this->layout = 'promotion';
        parent::beforeRender();
    }

    public function year2014() {
        $dayOfWeek = strtolower(date("l"));
        $this->set('dayOfWeek', $dayOfWeek);
        $this->set('days', $this->days);
    }

}
