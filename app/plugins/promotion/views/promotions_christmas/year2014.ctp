<div class="content">
    <img src="/img/promotion/<?php echo $dayOfWeek; ?>_2.jpg" width="700" height="854" alt="Navidad 2014" usemap="#promomap">
    <map name="promomap">
        <area title="<?php echo $days[$dayOfWeek]['title']; ?>" shape="rect" coords="0,0,699,825" href="<?php echo $days[$dayOfWeek]['href']; ?>" alt="Navidad 2014">
        <area title="Club Cupón" shape="rect" coords="0,826,375,853" href="http://www.clubcupon.com.ar/" alt="Club Cupón">
        <area title="Facebook" shape="rect" coords="376,826,471,853" href="http://www.facebook.com/ClubCupon" alt="Facebook">
        <area title="Twitter" shape="rect" coords="472,826,584,853" href="http://twitter.com/ClubCupon" alt="Twitter">
        <area title="Google+" shape="rect" coords="585,826,700,853" href="https://plus.google.com/u/0/105273446517623181990/posts" alt="Google+">
    </map> 
</div>