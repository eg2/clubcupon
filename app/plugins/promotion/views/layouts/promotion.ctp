<html>
    <head>
        <title>Club Cupón</title>
        <style type="text/css">
            .clearfix:after {
                visibility: hidden;
                display: block;
                font-size: 0;
                content: " ";
                clear: both;
                height: 0;
            }
            .clearfix { display: inline-table; }
            /* Hides from IE-mac \*/
            * html .clearfix { height: 1%; }
            .clearfix { display: block; }
            /* End hide from IE-mac */


            body{
                background-color: #1f435b;
                margin: 0;
                font-family: 'Roboto', sans-serif;
            }

            header {
                width: 100%;
                text-align: center;
            }

            .content {
                width: 700px;
                margin: 0 auto;
                height: auto;
            }

            footer {
                width: 700px;
                margin: 0 auto;
            }
        </style>
    </head>
    <body>
        <?php
        if (Configure::read('app.tracking.enable')) {
            echo $this->element('tracking-dataexpand');
            echo $this->element('trackingGoogle');
            echo $this->element('trackingWeb');
        }
        ?> 
        <div>
            <?php
            echo $content_for_layout;
            ?>
        </div>
        <?php
        echo $this->element('clubcupon/tagCxense');
        echo $this->element('clubcupon/tagEplanningProduct5');
        echo '<div style="display:none">';
        echo $this->element('tracking', array(
            'certificaPath' => isset($certificaPath) ? $certificaPath : false
        ));
        echo '</div>';
        ?>
    </body>
</html>