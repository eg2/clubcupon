<table style="border:1px solid #cccccc" cellpadding="0" cellspacing="0" align="<?php echo $align; ?>" bgcolor="#FFFFFF" border="0" width="48%">
		<tbody>
			<tr>
				<td class="responsive-image" colspan="2" align="center">
					<span class="responsive-image">
						<a href="<?php echo $deal ['DEAL_LINK']; ?>" target="_blank" style="text-decoration:none; color:#0484cb; font-size:15px">
							<img src="<?php echo $deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($deal ['DEAL_NAME'],150); ?>" style="display:block;height:237px;width:354px" height="237" width="354"/>
						</a>
					</span>
				</td>
			</tr>

			<tr>
				<td height="90" valign="top" style="padding:10px;" colspan="2">
					<h2 style="color:#333333; font-weight: normal; margin:0; padding: 0px; font-size: 15px;"><?php echo myTruncate2($deal ['DEAL_NAME'],100); ?></h2>
				</td>
			</tr>

			<tr>
				<td width="50%">
					<table border="0" cellspacing="10" cellpadding="0">
						<tr>
						<?php if ($deal ['ONLY_PRICE'] == 0) { ?>
							<!-- td style="color:#e6791e; font-size:20px"><strong><?php echo $deal ['DISCOUNT']; ?></strong></td-->
						<?php }?>
						<?php if ($deal ['HIDE_PRICE'] == 0) { ?>
                        <?php   if ($deal ['ONLY_PRICE'] == 0) { ?>
                                <td style="color:#535353; font-size:19px; text-decoration:line-through; padding:10px"> <?php echo $deal ['ORIGINAL_PRICE'];?></td>
								<td style="color:#e6791e; font-size:24px"><strong> <?php echo $deal ['BUY_PRICE']; ?></strong></td>
                        <?php   } else { ?>
                                <td style="color:#e6791e; font-size:24px; padding:10px"><strong> <?php echo $deal ['BUY_PRICE']; ?></strong></td>
						<?php   } ?>
                        <?php } else {?>
                                <td style="color:#e6791e; font-size:24px; padding:10px"><strong> <?php echo $deal ['Deal']['discount_percentage']; ?></strong></td>
                        <?php } ?>
						</tr>
					</table>
				</td>
				<td width="50%" align="right" style="padding:0 10px;">
					<a href="<?php echo $deal ['DEAL_LINK']; ?>" target="_blank" style="text-decoration: none; display: block; background-color: orange; width: 78px; text-align: center; color: rgb(255, 255, 255); padding: 5px 5px 8px; font-weight: normal; font-size: 17px;">Comprar</a>
				</td>
			</tr>
		</tbody>
</table>