<!-- MODULO 2 PRODUCTOS -->
                        <table width="48%" border="0" cellpadding="0" cellspacing="0" align="<?php echo $align; ?>" class="deviceWidth" bgcolor="#FFFFFF" style="border-bottom: 2px solid #fe7f00">
                            <tr>
                                <td align="center">
                                    <!-- The paragraph tag is important here to ensure that this table floats properly in Outlook 2007, 2010, and 2013
                                    To learn more about this fix check out this link: http://www.emailonacid.com/blog/details/C13/removing_unwanted_spacing_or_gaps_between_tables_in_outlook_2007_2010
                                    This fix is used for all floating tables in this responsive template
                                    The margin set to 0 is for Gmail -->
                                    <p style="mso-table-lspace:0;mso-table-rspace:0; margin:0">
                                    	<a href="<?php echo $deal ['DEAL_LINK']; ?>">
                                    		<img width="278" height="189" src="<?php echo $deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($deal ['DEAL_NAME'],150); ?>" border="0" style="width: 278px; border:1px solid #d3d3d3" class="deviceWidth" />
                                    	</a>
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 12px; color: #666666; font-weight: normal; text-align: left; font-family:Arial, sans-serif; line-height: 19px; vertical-align: top; padding:10px 8px 10px 8px">

                                    <table width="100%" style="border-bottom: 1px solid #d3d3d3">
                                        <tr>
                                            <!--<td valign="top" style="padding:0 10px 10px 0">
                                                <img  src="http://www.emailonacid.com/images/blog_images/Emailology/2013/free_template_1/3.jpg" alt="" border="0" align="left" />
                                            </td>-->
                                            <td valign="top" style="padding:0 10px 10px 0; height:77px;">
                                            	<a href="<?php echo $deal ['DEAL_LINK']; ?>" style="height: 20px; overflow: hidden; text-decoration: none; font-size: 16px; color: #333333; font-weight: bold; font-family:Arial, sans-serif" title="<?php echo htmlspecialchars(myTruncate2($deal ['DEAL_NAME'],150)); ?>"><?php echo myTruncate2($deal ['DEAL_NAME'],24); ?></a>
                                            </td>
                                        </tr>
                                    </table>
                      
                                    <table width="100%">
                                        <tr>
                                            <td valign="middle">
                                                <table width="120" align="left">
                                                    <tbody>
                                                        <tr>
                                                        <?php if ($deal ['ONLY_PRICE'] == 0) { ?>
															<!--  td style="color:#e6791e; font-size:20px; padding:6px; font-family:Arial, sans-serif"><strong><?php echo $deal ['DISCOUNT']; ?></strong></td-->
														<?php }?>
														<?php if ($deal ['HIDE_PRICE'] == 0) { ?>
								                        <?php   if ($deal ['ONLY_PRICE'] == 0) { ?>
								                                <td style="color:#535353; font-size:18px; padding:6px 0; text-decoration:line-through; font-family:Arial, sans-serif"> <?php echo $deal ['ORIGINAL_PRICE'];?></td>
																<td style="color:#e6791e; font-size:20px; padding:6px; font-family:Arial, sans-serif"><strong> <?php echo $deal ['BUY_PRICE']; ?></strong></td>
								                        <?php   } else { ?>
								                                <td style="color:#e6791e; font-size:20px; padding:6px; font-family:Arial, sans-serif"><strong> <?php echo $deal ['BUY_PRICE']; ?></strong></td>
														<?php   } ?>
								                        <?php } else {?>
								                                <td style="color:#e6791e; font-size:20px; padding:6px; font-family:Arial, sans-serif"><strong> <?php echo $deal ['Deal']['discount_percentage']; ?></strong></td>
								                        <?php } ?>
								                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td valign="middle">
                                                <table width="90" align="right">
                                                    <tbody>
                                                        <tr>
                                                            <td background="img/blue_back.jpg" bgcolor="#cc701b" style="padding:6px 0;background-color:#cc701b; border-top:1px solid #e68711; background-repeat:repeat-x" align="center">
                                                                <a style="color:#FFF;font-size:12px;font-weight:bold;text-align:center;text-decoration:none;font-family:Arial, sans-serif;-webkit-text-size-adjust:none;" href="<?php echo $deal ['DEAL_LINK']; ?>" target="_blank">
                                                                Comprar
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
						</table>
