<?php
if (!function_exists('myTruncate2')) {

    function myTruncate2($string, $limit, $break = " ", $pad = "...") {
        if (strlen($string) <= $limit)
            return htmlentities   ($string, ENT_COMPAT, "UTF-8");
        $string = substr($string, 0, $limit);
        if (false !== ($breakpoint = strrpos($string, $break))) {
            $string = substr($string, 0, $breakpoint);
        }
        return htmlentities   ($string . $pad, ENT_COMPAT, "UTF-8");
    }

}

$main_deal = $vars['main_deal'];

//$image_dir = $vars['SITE_LINK'] . '/img/theme_clean/email_template/news/';

$image_dir = '/img/theme_clean/email_template/news/';

//var_dump($main_deal);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Newsletter Club Cup&oacute;n</title>

<body>



<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#e2e2e2">

<style type="text/css">
body {margin:0; background: #FFFFFF; font-size: 13px; font-family: arial, sans-serif;}
table {/*border-collapse: collapse;*/}
td {font-family: arial, sans-serif; color: #333333;}
h2, h5 { margin:0 }
	.espacio {
		display:none;
		height:15px;
		clear:both
		}
@media only screen and (max-width: 480px) {
	
	body,table,td,p,a,li,blockquote { -webkit-text-size-adjust:none !important; }
	table {width: 100% !important;}
	.espacio {
		display:block;
		}
	.responsive-image img {
		height: auto !important;
		max-width: 100% !important;
		width: 100% !important;
		}
	.responsive-image-title img {
		height: auto !important;
		max-width: 80% !important;
		width: 80% !important;
		}
	.responsive-image-icon img {
		height: auto !important;
		max-width: 32px !important;
		width: 32px !important;
		}
	.responsive-image-cucarda img {
		height: auto !important;
		max-width: 100px !important;
		width: 100px !important;
		}
}
</style>

<tr>
	<td>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="font-family:Arial; font-size:11px;">
	<tr>
		<td style="padding:15px;" align="center">
		Club Cup&oacute;n de <?php echo $vars['CITY_NAME']; ?> | <a href="<?php echo $vars['SITE_LINK'];?>" style="text-decoration:none; color:#0098CA" target="_blank">Clubcupon.com.ar</a> | <a href="mailto:info@clubcupon.com.ar" style="text-decoration:none; color:#0098CA" target="_blank">info@clubcupon.com.ar</a> | Precios v&aacute;lidos hasta la medianoche.<br />Aseg&uacute;rese de agregarnos a su libreta de direcciones para que nuestros mails lleguen a su bandeja de entrada
		</td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" bgcolor="#323232">
			<span class="responsive-image" style="font-size:0;padding:0">
				<img src="<?php echo $image_dir; ?>header.jpg" alt="header Imagena" style="display:block" />
			</span>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">



<!-- ************************************************************** DESTACADO ************************************************************** -->

<tr style="padding: 10px 0; display:block">
	<td colspan="4" style="padding:0" width="740">
	<table style="border:1px solid #cccccc" cellpadding="0" cellspacing="10" align="left" bgcolor="#FFFFFF" border="0" width="100%">
		<tbody>
			<tr>
				<td style="padding:0" colspan="2">
					<table cellpadding="0" cellspacing="0" align="right" border="0" width="49%">
						<tbody>
							<tr>
								<td colspan="2">
                                <span class="responsive-image">
									<img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                        150); ?>" width="350" height="232" align="right" style="display:block;"/>								</span>                    			</td>
               			  </tr>
                    	</tbody>
                    </table>

					<table cellpadding="0" cellspacing="0" align="left" border="0" width="49%">
						<tbody>
							<tr>
								<td style="padding:10px" colspan="2">
									<h2 style="color:#0484cb; padding:5px 0 15px; margin:0"> <?php echo myTruncate2($main_deal ['DEAL_NAME'], 150); ?></h2>
									<h5 style="color:#333333; font-size:14px; padding:0 0 15px">
									<?php echo $this->element('newsDealPlace',array ('deal' => $main_deal,'type'=>'main')); ?>	
									</h5>
									<span style="color:#333333"><?php echo myTruncate2($main_deal ['SUBTITLE'], 150); ?></span>
								</td>
							</tr>

							<tr>
								<td width="50%" style="padding-top:15px;">
									<a href="<?php echo $main_deal ['DEAL_LINK']; ?>" target="_blank">
										<img alt="comprar" src="<?php echo $image_dir; ?>btn_ver_mas.jpg" style="display:block" align="left" border="0" hspace="10" />
									</a>
								</td>
								<td width="50%" align="left" style="padding-top:15px;">
									<table border="0" cellspacing="10" cellpadding="0">
										<tr>
										<?php if ($main_deal['newsletterimage']) { ?>
										<?php    if ($main_deal ['ONLY_PRICE'] == 0 && $main_deal ['HIDE_PRICE'] == 0) { ?>
											<td style="color:#535353; font-size:18px; text-decoration:line-through"> <?php echo $main_deal ['ORIGINAL_PRICE'];?></td>
											<td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['BUY_PRICE']; ?></strong></td>
									    <?php    } ?>
									    <?php    if ($main_deal ['ONLY_PRICE'] == 1 && $main_deal ['HIDE_PRICE'] == 0) { ?>
                           					<td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['BUY_PRICE']; ?></strong></td>
						                <?php    } ?>
						                <?php    if ($main_deal ['ONLY_PRICE'] == 0 && $main_deal ['HIDE_PRICE'] == 1) { ?>
						                	<td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['DISCOUNT']; ?></strong></td>
                                        <?php    } ?>
                                        <?php }else {
                                        	     if ($main_deal ['ONLY_PRICE'] == 0) { ?>
												   <td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['DISCOUNT']; ?></strong></td>
                                        <?php     } 
                                        		 if ($main_deal ['HIDE_PRICE'] == 0) { 
                                        		   if ($main_deal ['ONLY_PRICE'] == 0) { ?>
                               		  				<td style="color:#535353; font-size:18px; text-decoration:line-through"> <?php echo $main_deal ['ORIGINAL_PRICE'];?></td>
													<td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['BUY_PRICE']; ?></strong></td>
							            <?php      }else{ ?>
							            			<td style="color:#e6791e; font-size:20px"><strong> <?php echo $main_deal ['BUY_PRICE']; ?></strong></td>
							           <?php       }
                                                 }else{

												 }
                                             }?>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>

				</td>
			</tr>
		</tbody>
	</table>
	</td>
</tr>
<?php


// ************************************************************** DOS PRODUCTOS ************************************************************** -->

	$i=0;
	foreach ($vars ['deals'] as $deal) {
	  if ($deal ['Deal']['id'] != $main_deal ['Deal']['id']) {
		if(($i % 2)==0){
            if($i>0){?>
		    	</td>
		    	</tr>
				<tr>
					<td colspan="4" style="padding:12px 0">
		<?php 
			}else{?>
				<tr>
				<td colspan="4" style="padding:12px 0">
			<?php
			}
		}
		echo $this->element('deal_record',
				array("deal" => $deal, 'image_dir' => $image_dir, 'main_deal' => $main_deal));
		if(($i % 2)==0){
			echo '<div class="espacio"></div>';
		}
		$i++;
	  }
	}
	if(!empty($vars ['deals'])){
		echo '</td>	</tr>';
	}



?>
<!-- ************************************************************** LEGALES ************************************************************** -->
<tr>
	<td>
	<table bgcolor="#FFFFFF">
		<tr>
			<td style="padding:0 0 20px; border-top:3px solid #ea7412; border-bottom:3px solid #e2e2e2; font-size:11px; color:#4c4c4c" align="center">
            <table width="100%" border="0" cellspacing="15" cellpadding="0">
              <tr>
                <td><a href="<?php echo $vars ['FACEBOOK_URL']; ?>" target="_blank"><img src="<?php echo $image_dir; ?>facebook.jpg" alt="" border="0" align="right" style="display:block" /></a></td>
                <td><a href="<?php echo $vars ['TWITTER_URL']; ?>" target="_blank"><img src="<?php echo $image_dir; ?>twitter.jpg" alt="" style="display:block" align="left" border="0" /></a></td>
              </tr>
            </table>

				¿Necesit&aacute;s ayuda? ¿Ten&eacute;s comentarios? No dudes en contactarte <a href="mailto:info@clubcupon.com.ar" target="_blank" style="text-decoration:none; color:#0098CA">con nosotros</a>
			</td>
		</tr>
		<tr>
			<td style="padding:20px; font-size:11px; color:#4c4c4c">
			Recibiste este email porque te suscribiste al Newsletter de ClubCup&oacute;n Feria Navide&ntilde;a
			Para no perderte ninguna oferta de ClubCupon agreg&aacute; la direcci&oacute;n <a href="mailto:info@clubcupon.com.ar" target="_blank" style="text-decoration:none; color:#0098CA">info@clubcupon.com.ar</a> a tu lista de contactos,
			y marcanos como correo deseado.
			<br />
			Si no dese&aacute;s recibir mas el newsletter de <?php echo $vars['CITY_NAME']; ?>, por favor <a href="<?php echo $vars['UNSUBSCRIBE_LINK']; ?>" target="_blank" style="text-decoration:none; color:#0098CA">visit&aacute; este enlace</a>. 
			<br />
			Si quer&eacute;s darte de baja de todos nuestros newsletters, por favor <a href="<?php echo $vars['UNSUBSCRIBE_ALL_LINK']; ?>" target="_blank" style="text-decoration:none; color:#0098CA">segu&iacute; este enlace</a>.
			</td>
		</tr>
	</table>
	</td>
</tr>

</table>
 
</td>
</tr>
</table>
</body>
</html>