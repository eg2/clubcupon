<?php
if (!function_exists('myTruncate2')) {

    function myTruncate2($string, $limit, $break = " ", $pad = "...") {
        if (strlen($string) <= $limit)
            return htmlentities   ($string, ENT_COMPAT, "UTF-8");
        $string = substr($string, 0, $limit);
        if (false !== ($breakpoint = strrpos($string, $break))) {
            $string = substr($string, 0, $breakpoint);
        }
        return htmlentities   ($string . $pad, ENT_COMPAT, "UTF-8");
    }

}

$main_deal = $vars['main_deal'];

$image_dir = $vars['SITE_LINK'] . '/img/theme_clean/email_template/';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv = "Content-Type"    content = "text/html; charset=utf-8" />
        <meta http-equiv = "X-UA-Compatible" content = "IE=edge" />
        <title>Newsletter Club Cup&oacute;n</title>
    </head>
    <body>
    <!-- template_name: simple_columna.ctp -->
        <center>
            <table width="630" border="0" cellspacing="0" cellpadding="0" style="background:#ffffff; width:580px; display:block;" >
                <tr>
                    <td style="padding:10px 0;">
                        <table align="center" width="630" border="0" cellpadding="0" cellspacing="0" style="width:630px; display:block;">
                            <tr>
                                <td style="border:solid 4px #bebebe; padding:6px;">
                                    <table width="610" border="0" cellpadding="0" cellspacing="5" style="width:610px; display:block;">
                                        <tr>
                                            <td colspan="2" bgcolor="#1c1d1f" style="padding:10px;">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="width:580px; display:block;">
                                                    <tr>
                                                        <td width="30%" align="left"  style="width:174px; "><a target="_blank" href="<?php echo $vars ['SITE_LINK']; ?>"><img src="<?php echo $image_dir; ?>logo.png" alt="club cupon" title="Club Cupon" border="0" /></a></td>
                                                        <td width="70%"  style="width:406px;">
                                                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:right; padding:0; margin:0; color:#FFFFFF; line-height:20px;">
                                                                <?php echo date("d - m - y"); ?><br />
                                                                Las mejores ofertas de <span style="font-weight:bold; font-size:16px;"><?php echo $vars['CITY_NAME']; ?></span>
                                                            </p>
                                                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; text-align:right; padding:0; margin:0; color:#FFFFFF; line-height:20px;">
                Agreg&aacute; <a style="color:#ccc" href="mailto:[FromEmail]">[FromEmail]</a> a tu lista de contactos.
                  </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        
                                        <?php if (!empty($vars['BANNER_TOP_IMG_PATH'])) { ?>
                                            <tr>
                                                <td colspan="4"><img width="1" height="1" src="<?php echo $vars['BANNER_TOP_URL_TRACKER']; ?>" border="0"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="responsive-image" align="center" style="border:1px solid #cccccc">
                                                    <a target="_blank" href="<?php echo $vars['BANNER_TOP_IMG_LINK']; ?>" style="border:none;">
                                                        <img src="<?php echo $vars ['SITE_LINK'] . $vars ['BANNER_TOP_IMG_PATH']; ?>" alt="banner" style="display:block" />
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <tr>
                                            <td colspan="2">
                                                
                                                
                                                <?php if ($main_deal['newsletterimage']) { ?>
                                                
                                                
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:10px; width:609px; display:block;">
                                                        <tbody>
                                                            <tr style="line-height: 0 !important">
                                                                <td style="line-height: 0 !important">
                                                                    <a target="_blank" href="<?php echo $main_deal['DEAL_LINK']; ?>"><img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                        150); ?>"  width="<?php echo Configure::read('deal.NewsLetterImage.width'); ?>" border="0"></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border:solid 1px #cfcfcf;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                                                                        <tbody><tr>
                                                                                <?php if ($main_deal ['ONLY_PRICE'] == 0 && $main_deal ['HIDE_PRICE'] == 0) { ?>
                                                                                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="25%">
                                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                            Beneficio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['DISCOUNT']; ?> </span>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="25%">
                                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                            Precio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td width="25%">
                                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#54b7d7; font-weight:bold;">
                                                                                            <?php 
                                                                                                echo $main_deal ['BUY_PRICE'] . ' en vez de ' . $main_deal ['ORIGINAL_PRICE'];
                                                                                            ?>
                                                                                        </p>
                                                                                    </td>
                                                                                <?php } ?>
                                                                                
                                                                                
                                                                                <?php if ($main_deal ['ONLY_PRICE'] == 1 && $main_deal ['HIDE_PRICE'] == 0) { ?>
                                                                                
                                                                                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="75%">
                                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                            Precio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span>
                                                                                        </p>
                                                                                    </td>
                                                                                
                                                                                <?php } ?>
                                                                                
                                                                                <?php if ($main_deal ['ONLY_PRICE'] == 0 && $main_deal ['HIDE_PRICE'] == 1) { ?>
                                                                                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="75%">
                                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                            Beneficio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['DISCOUNT']; ?> </span>
                                                                                        </p>
                                                                                    </td>
                                                                                <?php } ?>
                                                                                
                                                                                
                                                                                <td align="center" width="25%"><a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>"><img src="<?php echo $image_dir; ?>buttom.jpg" alt="" border="0"></a></td>
                                                                                
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>                
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color:#f2f2f2; border-left:solid 1px #cfcfcf; border-right:solid 1px #cfcfcf; border-bottom:solid 1px #cfcfcf; padding:5px 10px;">
                                                                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#3b3b3b; text-align:left; margin:0; padding:0;">
                                                                        <?php echo myTruncate2($main_deal ['DEAL_NAME'], 150); ?>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <?php
                                                }
                                                else {
                                                    ?>

                                                    
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0"  style="width:600px; display:block;">
                                                        <tr>
                                                            <td style="width:300px;">
                                                                <a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>">
                                                                    <img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                        150); ?>" title="<?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                                             150); ?>" height = "184"  border = "0" style="display:block; height:184px; width:277px;"/>
                                                                </a>
                                                            </td>
                                                            <td rowspan="2" bgcolor="#f58229" style="padding:10px; vertical-align:top; width:300px;" height="230">
                                                                <p style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#FFFFFF; padding:0; margin:0;">
                                                                <?php if ($main_deal ['ONLY_PRICE'] == 0) { ?>
                                                                    <span style="font-size:24px;"><?php echo $main_deal ['DISCOUNT']; ?></span> de descuento
                                                                <?php } ?>
                                                                </p>
                                                                <?php if ($main_deal ['HIDE_PRICE'] == 0) { ?>

                                                                    <?php if ($main_deal ['ONLY_PRICE'] == 0) { ?>

                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                                                                            Pag&aacute; s&oacute;lo <span style="font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span> en vez de <span style="font-weight:bold;"><?php echo $main_deal ['ORIGINAL_PRICE']; ?></span><br />por <?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                                        150); ?>
                                                                        </p>

                                                                    <?php }
                                                                    else { ?>

                                                                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                                                                            Pag&aacute; s&oacute;lo <span style="font-weight:bold;"><?php echo $main_deal ['BUY_PRICE'] . '</span> <br />por '. myTruncate2($main_deal ['DEAL_NAME'], 150); ?>
                                                                        </p>
                                                                    <?php } ?>

                                                                <?php }
                                                                else { ?>
                                                                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                                                                        <?php echo myTruncate2($main_deal ['DEAL_NAME'],
                                                                                150); ?>
                                                                    </p>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#f2f2f2" style="border:solid 1px #cfcfcf; padding:10px;">

                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="33%" style="border-right:1px solid #cfcfcf;">

                                                                            <?php if ($main_deal ['ONLY_PRICE'] == 0) { ?>
                                                                                <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                    Beneficio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['DISCOUNT']; ?></span>
                                                                                </p>
                                                                            <?php } ?>

                                                                        </td>
                                                                        <td width="33%">
                                                                            <?php if ($main_deal ['HIDE_PRICE'] == 0) { ?>
                                                                                <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                                                                    Precio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span>
                                                                                </p>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td width="33%" align="center"><a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>"><img src="<?php echo $image_dir; ?>buttom.jpg" alt="" border="0" /></a></td>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>
                                                    </table>

                                                <?php } ?>

                                            </td>
                                        </tr>

                                        <!-- ----------------------------------BANNER-------------------------- -->
										<?php
											if (!empty($vars['BANNER_IMG_PATH'])) {
											?>
											<tr><td colspan="4"><img width="1" height="1" src="<?php echo $vars['URL_TRACKER']; ?>" border="0"/></td></tr>
													<tr>
														<td colspan="4" class="responsive-image" align="center" style="border:1px solid #cccccc">
  	  														<a target="_blank" href="<?php echo $vars['BANNER_IMG_LINK']; ?>" style="border:none;">
																<img src="<?php echo $vars ['SITE_LINK'] . $vars ['BANNER_IMG_PATH']; ?>" alt="banner" style="display:block" />
      														</a>
														</td>
													</tr>
											<?php } ?>

                                        <?php
                                        foreach ($vars ['deals'] as $deal) {
                                            if ($deal ['Deal']['id'] != $main_deal ['Deal']['id']) {
                                                echo $this->element('deal_record_simple',
                                                        array("deal" => $deal, 'image_dir' => $image_dir, 'main_deal' => $main_deal));
                                            }
                                        }
                                        ?>

                                        <tr>
                                            
                                            <td width="50%" style=" padding:10px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td style="padding:0 0 10px 0;">
                                                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; color:#2fa9d2; padding:0; margin:0; text-align:center">
                                                                Seguinos en:
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="100" border="0" cellspacing="5" cellpadding="0">
                                                                <tr>
                                                                    <td><a target="_blank" href="<?php echo $vars ['FACEBOOK_URL']; ?>"><img src="<?php echo $image_dir; ?>icon_f.jpg" alt="" border="0" title="facebook" /></a></td>
                                                                    <td><a target="_blank" href="<?php echo $vars ['TWITTER_URL']; ?>"><img src="<?php echo $image_dir; ?>icon_t.jpg" alt="" border="0" title="twitter" /></a></td>
                                                                    <td><a target="_blank" href="<?php echo $vars ['RSS_FEED']; ?>"><img src="<?php echo $image_dir; ?>icon-rss.jpg" alt="" border="0" title="rss" /></a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:10px 0 0 0;">
                                                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#3c3c3c; padding:0; margin:0; text-align:center">
                                                                Y s&eacute; el primero en enterarte de nuestras ofertas!
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#d9d9d9" style="background-color:#d9d9d9; border-top:solid 3px #cfcfcf; padding:5px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:621px; display:block;">
                            <tr>
                                <td width="23%" align="right" style="padding:5px;"><img src="<?php echo $image_dir; ?>logo_club_footer.jpg" alt="" border="0" /></td>
                                <td width="62%" style="padding:5px;">
                                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:11px; text-align:center; padding:0; margin:0; color:#3b3b3b;">
                                        Recibiste este email porque te suscribiste al Newsletter <br />de ClubCup&oacute;n <?php echo $vars['CITY_NAME']; ?><br>
                                        Para no perderte ninguna oferta de ClubCupon agreg&aacute; la direcci&oacute;n <a style="color:#137ba2;" href="mailto:[FromEmail]">[FromEmail]</a> a tu lista de contactos,<br /> y marcanos como correo deseado.<br />
                                        <br />
                                        Si no dese&aacute;s recibir mas el newsletter de <?php echo $vars['CITY_NAME']; ?>,<br /> por favor <a target="_blank" style="color:#137ba2;" href="<?php echo $vars['UNSUBSCRIBE_LINK']; ?>">visit&aacute; este enlace.</a>
                                        <br />
                                        <br />
                                        Si quer&eacute;s darte de baja de todos nuestros newsletters,<br /> por favor <a target="_blank" style="color:#137ba2;" href="<?php echo $vars['UNSUBSCRIBE_ALL_LINK']; ?>">segu&iacute; este enlace.</a>
                                        <br />
										 Este es un env&iacute;o autom&aacute;tico de mail por favor no responda el mismo. Gracias.
		 								<br />
		 								Por cualquier consulta, ingres&aacute; ac&aacute; <a href="http://soporte.clubcupon.com.ar" >http://soporte.clubcupon.com.ar</a>
                                    </p>
                                </td>
                                <td width="15%" align="left" style="padding:5px;"><img src="<?php echo $image_dir; ?>logo_clarin_footer.jpg" alt="" border="0" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px;">
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:11px; text-align:left; padding:0; margin:0; color:#3b3b3b;">
                            LEY 25.326, ART. 27, INC. 3 (ARCHIVOS, REGISTROS O BANCOS DE DATOS CON FINES DE PUBLICIDAD): EL TITULAR PODR&Aacute; EN CUALQUIER MOMENTO SOLICITAR EL RETIRO O BLOQUEO DE SU NOMBRE DE LOS BANCOS DE DATOS A LOS QUE SE REFIERE EL PRESENTE ART&Iacute;CULO. DECRETO 1558/2001, ART. 27, P&Aacute;RR. 3&ordm;: &quot;EN TODA COMUNICACI&Oacute;N CON FINES DE PUBLICIDAD QUE SE REALICE POR CORREO, TEL&Eacute;FONO, CORREO ELECTR&Oacute;NICO, INTERNET U OTRO MEDIO A DISTANCIA A CONOCER, SE DEBER&Aacute; INDICAR, EN FORMA EXPRESA Y DESTACADA, LA POSIBILIDAD DEL TITULAR DEL DATO DE SOLICITAR EL RETIRO O BLOQUEO, TOTAL O PARCIAL, DE SU NOMBRE DE LA BASE DE DATOS. A PEDIDO DEL INTERESADO, SE DEBER&Aacute; INFORMAR EL NOMBRE DEL RESPONSABLE O USUARIO DEL BANCO DE DATOS QUE PROVEY&Oacute; LA INFORMACI&Oacute;N.&quot; EL DERECHO DE ACCESO SE EJERCE GRATUITAMENTE ANTE NUESTRA EMPRESA, A INTERVALOS NO INFERIORES A 6 MESES SALVO INTER&Eacute;S LEG&Iacute;TIMO AL EFECTO. LA DIRECCI&Oacute;N NACIONAL DE PROTECCI&Oacute;N DE DATOS PERSONALES, &Oacute;RGANO DE CONTROL DE LA LEY 25.326, TIENE LA ATRIBUCI&Oacute;N DE ATENDER LAS DENUNCIAS Y RECLAMOS QUE SE INTERPONGAN CON RELACI&Oacute;N AL INCUMPLIMIENTO DE LAS NORMAS SOBRE PROTECCI&Oacute;N DE DATOS PERSONALES. PARA EJERCER SUS DERECHOS DEBER&Aacute; COMUNICARSE A LOS TE.: +5499 49438700 Y AL CORREO ELECTRONICO <a href="mailto:datospersonales@clubcupon.com.ar">datospersonales@clubcupon.com.ar</a>
                        </p>
                    </td>
                </tr>
            </table>
        </center>
         <!-- via common linux market -->
        <img src="<?php echo $vars['EMT_IMAGE_URL'] ?>" />
    </body>
</html>
