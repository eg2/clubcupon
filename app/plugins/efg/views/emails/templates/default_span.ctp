<?php
if (!function_exists('myTruncate2')) {

    function myTruncate2($string, $limit, $break = " ", $pad = "...") {
        if (strlen($string) <= $limit)
            return htmlentities   ($string, ENT_COMPAT, "UTF-8");
        $string = substr($string, 0, $limit);
        if (false !== ($breakpoint = strrpos($string, $break))) {
            $string = substr($string, 0, $breakpoint);
        }
        return htmlentities   ($string . $pad, ENT_COMPAT, "UTF-8");
    }

}

$main_deal = $vars['main_deal'];

$image_dir = $vars['SITE_LINK'] . '/img/theme_clean/email_template/news/';

//$image_dir = '/img/theme_clean/email_template/news/';

//var_dump($main_deal);
setlocale(LC_ALL,"esp");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    	<title>Club Cupon</title>
    
    <style type="text/css">
		body {margin:0; background: #FFFFFF; font-size: 13px; font-family: arial, sans-serif;}
		table {border-collapse: collapse;}
		td {font-family: arial, sans-serif; color: #333333;}
		h2, h5 { margin:0 }
			.espacio {
				display:none;
				height:15px;
				clear:both
				}
		@media only screen and (max-width: 480px) {
			
			body,table,td,p,a,li,blockquote { -webkit-text-size-adjust:none !important; }
			table {width: 100% !important;}
			.espacio {
				display:block;
				}
			.responsive-image img {
				height: auto !important;
				max-width: 100% !important;
				width: 100% !important;
				}
			.responsive-image-title img {
				height: auto !important;
				max-width: 80% !important;
				width: 80% !important;
				}
			.responsive-image-icon img {
				height: auto !important;
				max-width: 32px !important;
				width: 32px !important;
				}
			.responsive-image-cucarda img {
				height: auto !important;
				max-width: 100px !important;
				width: 100px !important;
				}
		}
	</style>
    	
	</head>
<body>
<!-- template_name: default_span.ctp -->
<?php /*<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ececec">

<tr>
	<td>
*/ ?>

<span style="background-color: #ececec; display:table; width:100%; ">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="font-family:Arial; font-size:11px;padding:0 15px">
	<tr>
		<td style="padding:15px;" align="center">
			Recibiste este mail porque te suscribiste a Club Cup&oacute;n. Si no quer&eacute;s recibir mails de Club Cup&oacute;n, pod&eacute;s darte de baja haciendo 
			<a href="<?php echo $vars['UNSUBSCRIBE_ALL_LINK']; ?>" style="text-decoration:none; color:#0098CA" target="_blank">clic aqu&iacute;</a>.<br />
			Agreg&aacute; <span style="text-decoration:none; color:#0098CA">[FromEmail]</span> a tu lista de contactos.
		</td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" bgcolor="#323232">

			<table width="740" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left">
						<img src="<?php echo $image_dir; ?>logo.jpg" alt="logo ClubCupon" style="display:block" />
					</td>
					<td align="right">

						<table border="0" align="right" cellpadding="0" cellspacing="0">
							<tr>
								<td>

									<table border="0" align="right" cellpadding="0" cellspacing="3">
										<tr>
											<td align="center">
												<a href="<?php echo $vars ['FACEBOOK_URL']; ?>" target="_blank">
												
												<img src="<?php echo $image_dir; ?>facebook_h.jpg" alt="facebook" style="display:block" border="0" hspace="3" /></a>
											</td>
											<td align="center">
												<a href="<?php echo $vars ['TWITTER_URL']; ?>" target="_blank">
												<img src="<?php echo $image_dir; ?>twitter_h.jpg" alt="twitter" style="display:block" border="0" hspace="3" /></a>
											</td>
											<td align="center">
												<a href="<?php echo $vars ['GOOGLE_URL']; ?>" target="_blank">
												<img src="<?php echo $image_dir; ?>google_h.jpg" alt="google" style="display:block" border="0" hspace="3" /></a>
											</td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
							  <td colspan="4" align="center" style="color:#FFF; padding-top:15px;"><?php echo $vars['CITY_NAME']; ?> | <?php echo strftime("%d de %B del %Y");;?></td>
						  </tr>
						</table>
							
				  </td>
				</tr>
			</table>

		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">

<tr>
	<td style="padding: 15px 0">
		<h1 style="margin-bottom:0; font-size:23px; font-weight:normal;">Oferta del D&iacute;a</h1>
	</td>
</tr>

<!-- ----------------------------------BANNER TOP-------------------------- -->
<?php
if (!empty($vars['BANNER_TOP_IMG_PATH'])) {
?>
<tr><td colspan="4"><img width="1" height="1" src="<?php echo $vars['BANNER_TOP_URL_TRACKER']; ?>" border="0"/></td></tr>
<tr>
	<td colspan="4" class="responsive-image" align="center" style="border:1px solid #cccccc">
  	  <a target="_blank" href="<?php echo $vars['BANNER_TOP_IMG_LINK']; ?>" style="border:none;">
		<img src="<?php echo $vars ['SITE_LINK'] . $vars ['BANNER_TOP_IMG_PATH']; ?>" alt="banner" style="display:block" />
      </a>
	</td>
</tr>
<?php } ?>
<!-- ----------------------------------BANNER TOP-------------------------- -->    
    
</table>
<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">
<!-- ************************************************************** DESTACADO ************************************************************** -->
<tr  style="padding-bottom: 10px;">
	<td colspan="4" style="padding-top: 10px; padding-bottom: 10px;"  width="740">
	<table style="border:1px solid #cccccc" cellpadding="0" cellspacing="10" align="left" bgcolor="#FFFFFF" border="0" width="100%">
		<tbody>
			<tr>
				<td style="padding:0" colspan="2">
					<table cellpadding="0" cellspacing="0" align="right" border="0" width="49%">
						<tbody>
							<tr>
								<td colspan="2">
									<span class="responsive-image">
									  <a href="<?php echo $main_deal ['DEAL_LINK']; ?>" target="_blank">
										<img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2($main_deal ['DEAL_NAME'],150); ?>" width="350" height="232" align="right" style="display:block; color: #FFA500; font-family: arial; font-size: 20px; font-weight: bold; text-transform: uppercase" />
									  </a>
									</span>
								</td>
							</tr>
						</tbody>
					</table>

					<table cellpadding="0" cellspacing="0" align="left" border="0" width="49%">
						<tbody>
							<tr>
								<td style="padding:10px" colspan="2" height="130" valign="top">
									<h2 style="color:#333333; padding:5px 0 15px; margin:0; font-size:19px; font-weight: normal"><?php echo myTruncate2($main_deal ['DEAL_NAME'], 150); ?></h2><br />
									<span style="color:#333333; font-size:12px;"><?php echo $main_deal['SUBTITLE']; ?></span>
								</td>
							</tr>

							<tr>
								<td width="50%" align="left" style="padding-top:15px;">
									<table border="0" cellspacing="10" cellpadding="0">
										<tr>
										<?php if ($main_deal ['HIDE_PRICE'] == 0) { ?>
                        				<?php   if ($main_deal ['ONLY_PRICE'] == 0) { ?>
                                				<td style="color:#535353; font-size:18px; text-decoration:line-through;padding:10px"> <?php echo $main_deal ['ORIGINAL_PRICE'];?></td>
												<td style="color:#e6791e; font-size:27px"> <?php echo $main_deal ['BUY_PRICE']; ?></td>
                        				<?php   } else { ?>
                                				<td style="color:#e6791e; font-size:27px"> <?php echo $main_deal ['BUY_PRICE']; ?></td>
										<?php   } ?>
                        				<?php } else {?>
                                				<td style="color:#e6791e; font-size:27px"> <?php echo $main_deal ['Deal']['discount_percentage']; ?></td>
                        				<?php } ?>
										</tr>
									</table>
								</td>
								<td width="50%" style="padding-top:15px;">
									<a href="<?php echo $main_deal ['DEAL_LINK']; ?>" target="_blank">
										<img alt="comprar" src="<?php echo $image_dir; ?>btn_ver_mas.jpg" style="display:block" align="right" border="0" hspace="10" />
									</a>
								</td>
							</tr>
						</tbody>
					</table>

				</td>
			</tr>
		</tbody>
	</table>
	<br />
	</td>
</tr>

</table>
<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">


<!-- ************************************************************** BANNER ************************************************************** -->
<?php
if (!empty($vars['BANNER_IMG_PATH'])) {
?>
<tr><td colspan="4"><img width="1" height="1" src="<?php echo $vars['URL_TRACKER']; ?>" border="0"/></td></tr>
<tr>
	<td colspan="4" class="responsive-image" align="center" style="border:1px solid #cccccc">
  	  <a target="_blank" href="<?php echo $vars['BANNER_IMG_LINK']; ?>" style="border:none;">
		<img src="<?php echo $vars ['SITE_LINK'] . $vars ['BANNER_IMG_PATH']; ?>" alt="banner" style="display:block" />
      </a>
	</td>
</tr>

<?php } ?>
<!-- ************************************************************** OFERTAS ************************************************************** -->

</table>
<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">

<tr>
	<td>
		<h1 style="margin-bottom:0; font-size:23px; font-weight:normal; float:left">M&aacute;s ofertas</h1>
		<a href="<?php echo $vars['URL_ALL_DEALS'];?>" target="_blank" style="text-decoration:none; color:#0484cb; font-size:12px; float:right; padding-top:27px">Ver todas las ofertas</a>
	</td>
</tr>



<?php
// ************************************************************** DOS PRODUCTOS ************************************************************** -->

	$i=0;
	foreach ($vars ['deals'] as $deal) {
	  if ($deal ['Deal']['id'] != $main_deal ['Deal']['id']) {
		if(($i % 2)==0){
            if($i>0){?>
		    	</td>
		    	</tr>
		    	
		    	</table>
				<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">

				<tr style="page-break-before: always">
					<td colspan="4" style="padding:12px 0">
		<?php 
			}else{?>
				</table>
				<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">
				
				<tr style="page-break-before: always">
				<td colspan="4" style="padding:12px 0">
			<?php
			}
		}
		
		
		if(($i % 2)==0){
			echo $this->element('deal_record',
				array("deal" => $deal, 'image_dir' => $image_dir, 'main_deal' => $main_deal,'align'=>'left'));
			echo '<div class="espacio"></div>';
		}else{
			echo $this->element('deal_record',
				array("deal" => $deal, 'image_dir' => $image_dir, 'main_deal' => $main_deal,'align'=>'right'));
		}
		$i++;
	  }
	}
	if(!empty($vars ['deals'])){
		echo '</td>	</tr> </table>';
	}



?>
<table cellpadding="0" cellspacing="0" align="center" border="0" width="740">
<tr>
	<td align="center" style="padding:5px 0 15px;">
		<a href="<?php echo $vars['URL_ALL_DEALS'];?>" target="_blank" style="text-decoration:none; color:#0484cb; font-size:20px; display:block">Ver todas las ofertas &raquo;</a>
	</td>
</tr>

<!-- ************************************************************** LEGALES ************************************************************** -->
<tr>
	<td>
	<table bgcolor="#FFFFFF">
		<tr>
			<td style="border-bottom:1px solid #e2e2e2; font-size:11px; color:#4c4c4c" align="center">
				<table width="100%" border="0" cellspacing="15" cellpadding="0">
					<tr>
						<td>&iquest;Necesit&aacute;s ayuda? &iquest;Ten&eacute;s comentarios? No dudes en contactarte con nosotros.</td>
						<td>
							<table width="100" border="0" align="right" cellpadding="0" cellspacing="0">
								<tr>
									<td align="center" style="padding:5px;">
										<a href="<?php echo $vars ['FACEBOOK_URL']; ?>" target="_blank">
										<img src="<?php echo $image_dir; ?>facebook.jpg" alt="facebook" style="display:block" border="0" hspace="3" />
										</a>
									</td>
									<td align="center" style="padding:5px;">
										<a href="<?php echo $vars ['TWITTER_URL']; ?>" target="_blank">
										<img src="<?php echo $image_dir; ?>twitter.jpg" alt="twitter" style="display:block" border="0" hspace="3" />
										</a>
									</td>
									<td align="center" style="padding:5px;">
										<a href="<?php echo $vars ['GOOGLE_URL']; ?>" target="_blank">
										<img src="<?php echo $image_dir; ?>google.jpg" alt="google" style="display:block" border="0" hspace="3" />
										</a>
									</td>
									
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="15" cellpadding="0">
					<tr>
						<td align="left" style="padding:10px;">
							<img src="<?php echo $image_dir; ?>logo_cc_footer.jpg" alt="club cupon" style="display:block" />
						</td>
						<td align="right" style="padding:10px;">
							<img src="<?php echo $image_dir; ?>grupo_clarin.jpg" alt="grupo clarin" style="display:block" />
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td style="padding:0 20px 20px; font-size:11px; color:#4c4c4c; text-align:justify;">
			Si no dese&aacute;s recibir m&aacute;s el newsletter de <?php echo $vars['CITY_NAME']; ?>, por favor visit&aacute; <a href="<?php echo $vars['UNSUBSCRIBE_LINK']; ?>" target="_blank" style="text-decoration:none; color:#0098CA">este enlace</a>. Si quer&eacute;s darte de baja de todos nuestros newsletters, segu&iacute; <a href="<?php echo $vars['UNSUBSCRIBE_ALL_LINK']; ?>" target="_blank" style="text-decoration:none; color:#0098CA">este enlace</a>. Este es un env&iacute;o autom&aacute;tico de mail por favor no responda el mismo. Gracias. Por cualquier consulta, ingres&aacute; a <a href="http://soporte.clubcupon.com.ar" target="_blank" style="text-decoration:none; color:#0098CA">soporte.clubcupon.com.ar</a>.<br /><br />

			LEY 25.326, ART. 27, INC. 3 (ARCHIVOS, REGISTROS O BANCOS DE DATOS CON FINES DE PUBLICIDAD): EL TITULAR PODR&Aacute; EN CUALQUIER MOMENTO SOLICITAR EL RETIRO O BLOQUEO DE SU NOMBRE DE LOS BANCOS DE DATOS A LOS QUE SE REFIERE EL PRESENTE ART&Iacute;CULO. DECRETO 1558/2001, ART. 27, P&Aacute;RR. 3&ordm;: &quot;EN TODA COMUNICACI&Oacute;N CON FINES DE PUBLICIDAD QUE SE REALICE POR CORREO, TEL&Eacute;FONO, CORREO ELECTR&Oacute;NICO, INTERNET U OTRO MEDIO A DISTANCIA A CONOCER, SE DEBER&Aacute; INDICAR, EN FORMA EXPRESA Y DESTACADA, LA POSIBILIDAD DEL TITULAR DEL DATO DE SOLICITAR EL RETIRO O BLOQUEO, TOTAL O PARCIAL, DE SU NOMBRE DE LA BASE DE DATOS. A PEDIDO DEL INTERESADO, SE DEBER&Aacute; INFORMAR EL NOMBRE DEL RESPONSABLE O USUARIO DEL BANCO DE DATOS QUE PROVEY&Oacute; LA INFORMACI&Oacute;N.&quot; EL DERECHO DE ACCESO SE EJERCE GRATUITAMENTE ANTE NUESTRA EMPRESA, A INTERVALOS NO INFERIORES A 6 MESES SALVO INTER&Eacute;S LEG&Iacute;TIMO AL EFECTO. LA DIRECCI&Oacute;N NACIONAL DE PROTECCI&Oacute;N DE DATOS PERSONALES, &Oacute;RGANO DE CONTROL DE LA LEY 25.326, TIENE LA ATRIBUCI&Oacute;N DE ATENDER LAS DENUNCIAS Y RECLAMOS QUE SE INTERPONGAN CON RELACI&Oacute;N AL INCUMPLIMIENTO DE LAS NORMAS SOBRE PROTECCI&Oacute;N DE DATOS PERSONALES. PARA EJERCER SUS DERECHOS DEBER&Aacute; COMUNICARSE A LOS TE.: +5499 49438700 Y AL CORREO ELECTRONICO <a href="mailto:datospersonales@clubcupon.com.ar">datospersonales@clubcupon.com.ar</a>
			</td>
		</tr>
	</table>
	</td>
</tr>

</table>
 
</span>
<?php  /* 
</td>
</tr>
</table>
*/?>
 <img src="<?php echo $vars['EMT_IMAGE_URL'] ?>" />
</body>
</html>
