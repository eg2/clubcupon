<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
class EmailsController extends EfgAppController {

    var $name = 'Emails';
    var $components = array(
        'PowerMta',
        'UnsubscriberLink',
        'EfgUserProfileUpdater',
        'EfgLog'
    );
    var $uses = array(
        'Efg.EfgDeal',
        'Efg.EfgAttachment',
        'Efg.EfgCity',
        'Banner'
    );
    var $layout = 'efg.default';
    var $staticDomain;
    var $cityId;
    var $date;

    public function beforeFilter() {
        parent::beforeFilter();
        $this->staticDomain = Configure::read('static_domain_for_mails');
        Configure::load('email_files_generator_config');
        Configure::write('debug', 0);
    }

    function _getImageUrl($model, $attachment, $options) {
        $default_options = array(
            'dimension' => 'big_thumb',
            'class' => '',
            'alt' => 'alt',
            'title' => 'title',
            'type' => 'jpg'
        );
        $options = array_merge($default_options, $options);
        $image_hash = $options['dimension'] . '/';
        $image_hash.= $model . '/';
        $image_hash.= $attachment['id'] . '.' . md5(Configure::read('Security.salt') . $model . $attachment['id'] . $options['type'] . $options['dimension'] . Configure::read('site.name')) . '.' . $options['type'];
        return $this->staticDomain . '/img/' . $image_hash;
    }

    function _buildGATracking($name, &$medium = null, $content = null, &$source = null, &$term = null) {
        if (is_null($source))
            $source = Configure::read('Campaign.source');
        if (is_null($medium))
            $medium = Configure::read('Campaign.subscription_medium');
        return 'utm_source' . '=' . $source . '&' . 'utm_medium' . '=' . $medium . (is_null($term) ? '' : '&' . 'utm_term' . '=' . $term) . (is_null($content) ? '' : '&' . 'utm_content' . '=' . $content) . '&' . 'utm_campaign' . '=' . $name;
    }

    function _getEmtTrackingParams($dealId) {
        $emtParams = 'emt_campaign=' . $this->cityId;
        $emtParams.= '&emt_date=' . $this->date;
        $emtParams.= '&emt_goal=' . $dealId;
        $emtParams.= '&emt_user=' . $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberKey'));
        $emtParams.= '&emt_site=' . Configure::read('EmailFilesGenerator.Emt.Key');
        $emtParams.= '&emt_segment=' . $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.FrecuencyEventClassCodeKey'));
        return $emtParams;
    }

    function _getEmImageUrl() {
        $emtImageUrl = Configure::read('EmailFilesGenerator.Emt.FakeImageFullUrl');
        $emtImageUrl = str_replace("[site]", Configure::read('EmailFilesGenerator.Emt.Key'), $emtImageUrl);
        $emtImageUrl = str_replace("[campaign]", $this->cityId, $emtImageUrl);
        $emtImageUrl = str_replace("[date]", $this->date, $emtImageUrl);
        $emtImageUrl = str_replace("[user]", $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberKey')), $emtImageUrl);
        $emtImageUrl = str_replace("[segment]", $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.FrecuencyEventClassCodeKey')), $emtImageUrl);
        return $emtImageUrl;
    }

    function _setGloablLinks(&$vars, $city) {
        $vars['SITE_LINK'] = $this->staticDomain;
        $vars['CITY_NAME'] = $city['City']['name'];
        $banner_ad_data = $this->Banner->__getNewsletterBanner($this->cityId);
        $vars['BANNER_IMG_PATH'] = $banner_ad_data['Banner']['image'];
        $vars['BANNER_IMG_LINK'] = $banner_ad_data['Banner']['link'];
        $vars['URL_TRACKER'] = $banner_ad_data['Banner']['tracker_url'];
        $banner_newsletter_top = $this->Banner->findNewsletterTopBanner($this->cityId);
        $vars['BANNER_TOP_IMG_PATH'] = $banner_newsletter_top['Banner']['image'];
        $vars['BANNER_TOP_IMG_LINK'] = $banner_newsletter_top['Banner']['link'];
        $vars['BANNER_TOP_URL_TRACKER'] = $banner_newsletter_top['Banner']['tracker_url'];
        $vars['FACEBOOK_URL'] = empty($city['City']['facebook_url']) ? Configure::read('facebook.site_facebook_url') : $city['City']['facebook_url'];
        $vars['TWITTER_URL'] = empty($city['City']['twitter_url']) ? Configure::read('twitter.site_twitter_url') : $city['City']['twitter_url'];
        $vars['RSS_FEED'] = $this->staticDomain . '/xmldeals/rss/' . $city['City']['slug'];
        $vars['EMT_IMAGE_URL'] = $this->_getEmImageUrl();
        $vars['UNSUBSCRIBE_LINK'] = $this->UnsubscriberLink->getLinkWithTags();
        $vars['UNSUBSCRIBE_ALL_LINK'] = $this->UnsubscriberLink->getUnsuscribeAllLinkWithTags();
        $vars['URL_ALL_DEALS'] = $this->staticDomain . '/' . $city['City']['slug'] . '/todos';
        $vars['GOOGLE_URL'] = 'https://plus.google.com/u/0/105273446517623181990/posts';
    }

    function _setNewsletterValues(&$vars) {
        $attachment_newsletter = $this->EfgAttachment->getByDeal($vars['main_deal']['Deal']['id']);
        $vars['main_deal']['newsletterimage'] = !empty($attachment_newsletter);
        if ($vars['main_deal']['newsletterimage']) {
            $src = $this->_getImageUrl('DealNewsLetter', array(
                'id' => $attachment_newsletter['Attachment']['id']
                    ), array(
                'dimension' => 'original'
            ));
            $vars['main_deal']['DEAL_IMAGE'] = $src;
        }
    }

    function curlUrlImage($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        $chResult = curl_exec($ch);
        if (!empty($chResult)) {
            curl_close($ch);
            return true;
        } else {
            return false;
        }
    }

    function _setDealLinks(&$deal) {
        $medium = Configure::read('Campaign.subscription_medium');
        $source = Configure::read('Campaign.source');
        $gaParams = $this->_buildGATracking($deal['Deal']['id'], $medium, null, $source);
        $emtParams = $this->_getEmtTrackingParams($deal['Deal']['id']);
        $dealUrl = $this->staticDomain;
        if (!empty($deal['City']['slug'])) {
            $dealUrl.= '/' . $deal['City']['slug'];
        }
        $dealUrl.= Router::url(array(
                    'base' => false,
                    'controller' => 'deals',
                    'action' => 'view',
                    $deal['Deal']['slug'],
                    'admin' => false,
                    'plugin' => null
                        ), false);
        $dealUrl.= '?' . $gaParams . '&' . $emtParams . '&popup=no';
        $image_options = array(
            'dimension' => 'medium_big_thumb',
            'class' => '',
            'alt' => $deal['Deal']['name'],
            'title' => $deal['Deal']['name'],
            'type' => 'jpg'
        );
        $imageUrl = $this->_getImageUrl('Deal', $deal['Attachment'], $image_options);
        $deal['DEAL_NAME'] = $deal['Deal']['name'];
        $deal['SUBTITLE'] = $deal['Deal']['subtitle'];
        $deal['ORIGINAL_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['original_price']);
        $deal['BUY_PRICE'] = Configure::read('site.currency') . removeZeroDecimals($deal['Deal']['discounted_price']);
        $deal['DISCOUNT'] = $deal['Deal']['discount_percentage'] . '%';
        $deal['HIDE_PRICE'] = $deal['Deal']['hide_price'];
        $deal['ONLY_PRICE'] = $deal['Deal']['only_price'];
        $deal['DEAL_LINK'] = $dealUrl;
        $deal['DEAL_IMAGE'] = $imageUrl;
        $deal['COMPANY_ADDRESS'] = !empty($deal['Deal']['custom_company_address1']) ? $deal['Deal']['custom_company_address1'] : $deal['Company']['address1'];
        $deal['Company']['City'];
        $deal['COMPANY_CITY'] = $deal['Company']['City']['name'];
        $deal['COMPANY_NAME'] = $deal['Company']['name'];
    }

    function newsletter($cityId = null, $date = null, $template_name = 'simple_columna.ctp', $force = 'no') {
        $search = array(
            ".ctp"
        );
        $remplace = array(
            ""
        );
        $template_name = str_replace($search, $remplace, $template_name);
        $this->EfgLog->log(__METHOD__ . "**** PARAMS: " . json_encode(array(
                    "cityId" => $cityId,
                    "date" => $date,
                    "template_name" => $template_name,
                    "params" => $this->params
        )));
        $profileUserId = false;
        $useSegmentationOrder = false;
        if (empty($cityId)) {
            throw new Exception('Parametros incorrectos');
        } else {
            $this->cityId = $cityId;
            $this->date = $date;
        }
        if ($force == 'force') {
            $deals = $this->EfgDeal->GetDealsOfTheDayAndForce($this->cityId);
        } else {
            $deals = $this->EfgDeal->GetDealsOfTheDay($this->cityId);
        }
        if ($useSegmentationOrder) {
            $deals = $this->EfgUserProfileUpdater->orderDealForUserProfile($deals, $profileUserId);
        }
        $city = $this->EfgCity->getById($this->cityId);
        $vars = array();
        $dealIds = array();
        $this->_setGloablLinks($vars, $city);
        $firstDeal = $deals[0];
        $this->_setDealLinks($firstDeal, $city);
        $vars['main_deal'] = $firstDeal;
        foreach ($deals as & $deal) {
            $this->_setDealLinks($deal, $city);
            $dealIds[] = $deal['Deal']['id'];
        }
        $this->_setNewsletterValues($vars);
        $vars['deals'] = $deals;
        $this->set('vars', $vars);
        $this->render('/emails/templates/' . $template_name);
    }

}
