<?php

App::import('Core', 'Component');
App::import('Component', 'Email');
App::import('Component', 'Efg.EfgLog');

class EfgEmailComponent extends EmailComponent {

    var $emailComponent;
    var $logComponent;

    function __construct() {
        parent::__construct();
        Configure::load('email_files_generator_config');
        $this->emailComponent = & new EmailComponent();
        $this->logComponent = & new EfgLogComponent();
    }

    function send($subject, $body) {
        $this->emailComponent->from = Configure::read('EmailFilesGenerator.Email.From');
        $tos = explode(',', Configure::read('EmailFilesGenerator.Email.To'));
        
        $this->emailComponent->subject = $subject;

        if (Configure::read('EmailFilesGenerator.Email.IsSmtp')) {
            $this->emailComponent->smtpOptions = array('port' => '25',
                'timeout' => '30',
                'host' => Configure::read('EmailFilesGenerator.Email.SmtpHost'),
                'username' => Configure::read('EmailFilesGenerator.Email.SmtpAccount'),
                'password' => Configure::read('EmailFilesGenerator.Email.SmtpPassword')
            );
            $this->emailComponent->delivery = 'smtp';
        }

        foreach ($tos as $to) {
            $this->emailComponent->to = $to;
            $this->emailComponent->send($body);
        }


        if (!empty($this->emailComponent->smtpError)) {
            echo $this->emailComponent->smtpError;
            $this->logComponent->error($this->emailComponent->smtpError);
        }
    }

}