<?php

App::import('Core', 'Component');
App::import('Model', 'efg.EfgSegmentationProfile');
App::import('Component', 'efg.EfgProfile');
App::import('Component', 'efg.EfgLog');
App::import('Model', 'efg.EfgDeal');
App::import('Model', 'efg.EfgUser');
App::import('Model', 'efg.EfgUserProfile');

class EfgUserProfileUpdaterComponent extends Component {

    public $name = 'EfgUserProfileUpdater';
    const USER_RANGE = 10000;

    function __construct() {
        Configure::load('email_files_generator_config');
        $this->minimumNumberOfPurchases = Configure::read('EmailFilesGenerator.MinimumNumberOfPurchases');
        $this->SegmentationProfile = new EfgSegmentationProfile();
        $this->Deal = new EfgDeal();
        $this->User = new EfgUser();
        $this->UserProfile = new EfgUserProfile();
        $this->EfgLog = new EfgLogComponent();
    }

    private function getMaxUserId() {
        $result = $this->User->findMaxId();
        return $result[0]['max_id'];
    }
    
    public function generateProfile() {
        $this->_setAllUserSegmentationProfileTypes();
        $currentUserIndex = 0;
        $this->EfgLog->log("PRINCIPIO de generacion de perfiles de usuarios \n");            
        $maxUserId = $this->getMaxUserId();
        $this->EfgLog->log("El Id Maximo de Usuario es ".$maxUserId."\n");            
        $hasProcessedAllUsers = false;
        while (!$hasProcessedAllUsers) {
            $this->EfgLog->log("trantado lote de usuarios $currentUserIndex \n");            
            $usersForCurrentRange = $this->User->findUserLot($currentUserIndex, EfgUserProfileUpdaterComponent::USER_RANGE);

            if (sizeof($usersForCurrentRange) == 0) {
                $hasProcessedAllUsers = ($currentUserIndex + EfgUserProfileUpdaterComponent::USER_RANGE) > $maxUserId ;
            } else {
              $dealsByUserId = $this->_findLastDealsByUser(
                $currentUserIndex,
                EfgUserProfileUpdaterComponent::USER_RANGE
              );

              foreach ($usersForCurrentRange as $user) {
                $userId = $user['User']['id'];
                if (isset($dealsByUserId[$userId])) {
                  $this->_updateUserProfile($user, $dealsByUserId[$userId]);
                } else {
                  $this->_updateUserProfile($user);
                }
              }
            }
            $currentUserIndex += EfgUserProfileUpdaterComponent::USER_RANGE;
        }
        $this->EfgLog->log("FIN de de la actualizacion de los profiles de usuarios");  
    }

    /**
     * Agrupa los deals que tuvieron más de 3 intentos de compra por usuarios
     * 
     * @param type $compras
     * @return boolean
     */
    private function _groupByUser($compras) {

        $dealsByUser = array();
        foreach ($compras as $deal) {
            $user_id = $deal['DealExternal']['user_id'];
            $category_id = $deal['DealFlatCategory']['l1_id'];
            $count = $deal[0]['COUNT(*)'];
            if ($count >= $this->minimumNumberOfPurchases) {
                $dealsByUser[$user_id][$category_id] = true;
            }
        }
        return $dealsByUser;
    }

    /**
     * Busca todos los intentos de compra para un lote de usuarios dado en los 90 ultimos dias.
     * @param type $startUserId
     * @param type $rangeSize
     * @return type Un array de 2 dimensiones que tiene por keys usuario_id y categoria_id seteado a true 
     * en el caso de que el usuario tenga un intento de compra
     */
    private function _findLastDealsByUser($startUserId, $rangeSize) {
        $lastDeals = $this->Deal->findLastDealsByUser($startUserId, $rangeSize);
        return $this->_groupByUser($lastDeals);
    }

    private function _updateUserProfile($user, $lastDeals = null) {
        $userId = $user['User']['id'];
        $userGender = $this->_convertGenderToProfileConstant($user['UserProfile']['gender']);
        $hastravel = 0;
        $hasproduct = 0;
        $hasbeauty = 0;
        $hasgastronomy = 0;
        $hasEntertainment = 0;
        $hasPhotography = 0;
        $hasServices = 0;
        $hasCountryDay = 0;
        $hasShopping = 0;
        $hasPet = 0;
        $hasNowRoot = 0;
        $hasOther = 0;

        if (isset($lastDeals)) {
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_PRODUCT])) {
                $hasproduct = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_BEAUTY])) {
                $hasbeauty = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_GASTRONOMY])) {
                $hasgastronomy = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_TRAVEL])) {
                $hastravel = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_ENTERTAINMENT])) {
                $hasEntertainment = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_PHOTOGRAPHY])) {
                $hasPhotography = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_SERVICES])) {
                $hasServices = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_COUNTRY_DAY])) {
                $hasCountryDay = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_SHOPPING])) {
                $hasShopping = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_PET])) {
                $hasPet = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_NOW_ROOT])) {
                $hasNowRoot = 1;
            }
            if (isset($lastDeals[ConstSegmentationCategoryIds::CATEGORY_OTHER])) {
                $hasOther = 1;
            }
         
        }
        $userProfile = new EfgProfileComponent($userGender, $hasproduct, $hasbeauty, $hastravel, 
                $hasgastronomy, $hasEntertainment, $hasPhotography, $hasServices,
                $hasCountryDay, $hasShopping, $hasPet, $hasNowRoot, $hasOther);
        
        $profile_id = $this->profileIdByProfileUserKey[$userProfile->generateUserProfileIdentity()];
        $this->UserProfile->updateProfileId($userId, $profile_id);
    }

    private function _convertGenderToProfileConstant($gender) {
        if ($gender == 'm') {
            return ConstSegmentationSex::MALE;
        } else if ($gender == 'f') {
            return ConstSegmentationSex::FEMALE;
        } else {
            return ConstSegmentationSex::UNDEFINED;
        }
    }

    private function _setAllUserSegmentationProfileTypes() {
        $profiles = $this->_getAllProfileAsObject();
        $profileIdByProfileUserKey = array();
        foreach ($profiles as $profile) {
            $profileIdByProfileUserKey[$profile->generateUserProfileIdentity()] = $profile->id;
        }
        $this->profileIdByProfileUserKey = $profileIdByProfileUserKey;
    }
    
    private function _getAllProfileById() {
        $profiles = $this->_getAllProfileAsObject();
        $profilesById = array();
        foreach ($profiles as $profile) {
            $profilesById[$profile->id] = $profile;
        }
        return $profilesById;
    }

    private function _getAllProfileAsObject() {
        $profiles = $this->SegmentationProfile->find('all');
        $profilesArray = array();
        foreach ($profiles as $profile) {
            $sex = $profile['SegmentationProfile']['sexo'];
            $producto = $profile['SegmentationProfile']['producto'];
            $belleza = $profile['SegmentationProfile']['belleza'];
            $turismo = $profile['SegmentationProfile']['turismo'];
            $gastronomia = $profile['SegmentationProfile']['gastronomia'];
            $entretenimiento = $profile['SegmentationProfile']['entretenimiento'];
            $fotografia = $profile['SegmentationProfile']['fotografia'];
            $servicios = $profile['SegmentationProfile']['servicios'];
            $dia_de_campo = $profile['SegmentationProfile']['dia_de_campo'];
            $shopping = $profile['SegmentationProfile']['shopping'];
            $mascotas = $profile['SegmentationProfile']['mascotas'];
            $now_root = $profile['SegmentationProfile']['now_root'];
            $otras = $profile['SegmentationProfile']['otras'];
            $id = $profile['SegmentationProfile']['id'];
            $profilesArray[] = new EfgProfileComponent($sex, $producto, $belleza, $turismo, $gastronomia, 
                    $entretenimiento, $fotografia, $servicios, $dia_de_campo, $shopping,
                    $mascotas, $now_root, $otras, $id);
        }
        return $profilesArray;
    }

    /**
     * Ordena los deals pasados en parametros segun el perfil de segmentacion del usuario pasado en parametro
     * @param type $deals
     * @param type $profileUserId
     */
    public function orderDealForUserProfile($deals, $profileUserId = false) {
        $profiles = $this->_getAllProfileById();
        $dealsConScoreAndPriority = array();
        foreach ($deals as $deal) {
            $segmentationProfileId = $deal['Deal']['segmentation_profile_id'];
            $user_profile = $profiles[$profileUserId];
            $score = $user_profile->calculateScoreForDealProfile($profiles[$segmentationProfileId]);
            $deal['score'] = $score;
            $salesForecast = $deal['Deal']['sales_forecast'] == 0 ? 1 : $deal['Deal']['sales_forecast'];
            $commissionPercentage = $deal['Deal']['commission_percentage'];
            $orignialPrice = $deal['Deal']['original_price'];
            $priority = $salesForecast * $orignialPrice * $commissionPercentage;
            $deal['priority'] = $priority;
            
            $dealsConScoreAndPriority[] = $deal;
        }
        $this->EfgLog->log("******** ANTES ordenando los deals ********\n");
        foreach ($dealsConScoreAndPriority as $deal) {
            $this->EfgLog->log($deal['Deal']['id'] . "-" . $deal['score']. "-". $deal['priority']);
        }
        usort($dealsConScoreAndPriority, array('EfgUserProfileUpdaterComponent', 'dealComparator'));
        $this->EfgLog->log("******** DESPUES ordenando los deals para el profile_user_id $profileUserId ********\n");
        foreach ($dealsConScoreAndPriority as $deal) {
            $this->EfgLog->log($deal['Deal']['id'] . "-" . $deal['score']. "-".$deal['priority'] ."-".$deal['Deal']['name']);
        }
        return $dealsConScoreAndPriority;
    }

	private static function dealComparator($deal1, $deal2) {
			if ($deal1['score'] == $deal2['score']) {
					if ($deal1['priority'] > $deal2['priority']) {
							return -1;
					} else {
							return 1;
					}
			} else {
					if ($deal1['score'] > $deal2['score']) {
							return -1;
					} else {
							return 1;
					}
			}
	}


}

?>
