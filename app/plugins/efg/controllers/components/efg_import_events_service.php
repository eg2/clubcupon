<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiLogger');
App::import('Model', 'Efg.EfgEventType');

class EfgImportEventsServiceComponent extends Component {

    const EMT_EVENT_TYPE_OPEN = 1;

    public $name = 'EfgImportEventsService';

    public $eventTypeByEmt = null;

//    public $components = array(
//        'api.ApiLogger',
//    );


    function __construct() {
      $this->ApiLogger = new ApiLogger(get_class($this), 'efg');
      $this->eventTypeByEmt = $this->_initEventTypeByEmt();

    }

    function _initEventTypeByEmt() {
      return array(
          EMT_EVENT_TYPE_OPEN => EfgEventType::EVENT_TYPE_OPEN
      );
    }

    /**
     * @TODO
     * consultar el Servicio ordenado por fecha
     * @return la fecha del evento ultimo.
     *
     **/
    public function importEvents($startDate=null) {
      $this->ApiLogger->debug("Iniciando inportacion de Eventos Emt", array(
        'startDate' => $startDate
      ));

      // consulto servicio
      // itero por eventos 
          //  por cada evento consulto el email asociado a la subscription id
          // tradusco el event_type de emt al equivalente de clubcupon;
          //  actualizo el registro de events por (email, event_type)

      return $lastEventDate;
    }

    public function showEvents($startDate=null, $email=null, $eventTypeIds=array()) {
    }

}

