<?php

/**
 * Representa tanto un perfil de producto como de usuario.
 * Para un User el sexo del profile corresponde a lo que completo en su perfil (puede ser desconocido)
 * Para un Deal, el sexo indica a quién está destinado, un publico masculino, feminino, o unisex (en este caso se setea a desconocido)
 */
class EfgProfileComponent extends Component {

    public $name = 'EfgProfile';
    var $id;
    var $sex;
    var $isCategoryProduct;
    var $isCategoryBeauty;
    var $isCategoryGastronomy;
    var $isCategoryTravel;
    var $isCategoryEntertainment;
    var $isCategoryPhotography;
    var $isCategoryServices;
    var $isCategoryCountryDay;
    var $isCategoryShopping;
    var $isCategoryPet;
    var $isCategoryNowRoot;
    var $isCategoryOther;

    public function __construct($sex, $isCategoryProduct, $isCategoryBeauty, $isCategoryGastronomy, 
            $isCategoryTravel, $isCategoryEntertainment, $isCategoryPhotography, $isCategoryServices, 
            $isCategoryCountryDay, $isCategoryShopping, $isCategoryPet, $isCategoryNowRoot, $isCategoryOther, $id = -1) {

        $this->id = $id;
        $this->sex = $sex;
        $this->isCategoryProduct = $isCategoryProduct;
        $this->isCategoryBeauty = $isCategoryBeauty;
        $this->isCategoryGastronomy = $isCategoryGastronomy;
        $this->isCategoryTravel = $isCategoryTravel;
        $this->isCategoryEntertainment = $isCategoryEntertainment;
        $this->isCategoryPhotography = $isCategoryPhotography;
        $this->isCategoryServices = $isCategoryServices;
        $this->isCategoryCountryDay = $isCategoryCountryDay;
        $this->isCategoryShopping = $isCategoryShopping;
        $this->isCategoryPet = $isCategoryPet;
        $this->isCategoryNowRoot = $isCategoryNowRoot;
        $this->isCategoryOther = $isCategoryOther;
    }

    public function calculateScoreForUserProfile($userProfile) {
        return $this->calculateScore($this, $userProfile);
    }

    public function calculateScoreForDealProfile($dealProfile) {
        return $this->calculateScore($dealProfile, $this);
    }

    private function calculateScore($dealProfile, $userProfile) {
        $total = 0;
        if ($this->_isProductSexCompatibleWithUserSex($userProfile->sex, $dealProfile->sex)) {
            $total += 1;
        }
        if ($userProfile->isCategoryProduct == $dealProfile->isCategoryProduct) {
            $total += $userProfile->isCategoryProduct;
        }
        if ($userProfile->isCategoryBeauty == $dealProfile->isCategoryBeauty) {
            $total += $userProfile->isCategoryBeauty;
        }
        if ($userProfile->isCategoryGastronomy == $dealProfile->isCategoryGastronomy) {
            $total += $userProfile->isCategoryGastronomy;
        }
        if ($userProfile->isCategoryTravel == $dealProfile->isCategoryTravel) {
            $total += $userProfile->isCategoryTravel;
        }
        if ($userProfile->isCategoryEntertainment == $dealProfile->isCategoryEntertainment) {
            $total += $userProfile->isCategoryEntertainment;
        }
        if ($userProfile->isCategoryPhotography == $dealProfile->isCategoryPhotography) {
            $total += $userProfile->isCategoryPhotography;
        }
        if ($userProfile->isCategoryServices == $dealProfile->isCategoryServices) {
            $total += $userProfile->isCategoryServices;
        }
        if ($userProfile->isCategoryCountryDay == $dealProfile->isCategoryCountryDay) {
            $total += $userProfile->isCategoryCountryDay;
        }
        if ($userProfile->isCategoryShopping == $dealProfile->isCategoryShopping) {
            $total += $userProfile->isCategoryShopping;
        }
        if ($userProfile->isCategoryPet == $dealProfile->isCategoryPet) {
            $total += $userProfile->isCategoryPet;
        }
        if ($userProfile->isCategoryNowRoot == $dealProfile->isCategoryNowRoot) {
            $total += $userProfile->isCategoryNowRoot;
        }
        if ($userProfile->isCategoryOther == $dealProfile->isCategoryOther) {
            $total += $userProfile->isCategoryOther;
        }
        return $total;
    }

    private function _isProductSexCompatibleWithUserSex($userSex, $dealSex) {
        if ($dealSex == ConstSegmentationSex::UNDEFINED) {
            return true;
        }
        else
            return $dealSex == $userSex;
    }

    function generateUserProfileIdentity() {
        return $this->sex . $this->isCategoryProduct . $this->isCategoryBeauty . $this->isCategoryGastronomy . $this->isCategoryTravel 
                . $this->isCategoryEntertainment . $this->isCategoryPhotography . $this->isCategoryServices 
                . $this->isCategoryCountryDay . $this->isCategoryShopping . $this->isCategoryPet . $this->isCategoryNowRoot .
                $this->isCategoryOther;
    }

}

?>
