<?php

App::import('Core', 'Component');
App::import('Core', 'Router');

class UnsubscriberLinkComponent extends Object {

    var $components = array('PowerMta');
    
    function __construct() {
        parent::__construct();
        Configure::load('email_files_generator_config');
    }

    function getLinkWithTags() {
        
        //Armo el link y le quito la hash
        $linkWithHash = Configure::read('static_domain_for_mails') . Router::url(array('base' => false,
                    'controller' => 'subscriptions',
                    'action' => 'unsubscribe',
                    $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberKey')),
                    $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberHashKey')),
                    'admin' => false,
                    'plugin' => null
                        ), false);
        
        return substr($linkWithHash, 0, strrpos($linkWithHash, '/'));
    }
    
    function getUnsuscribeAllLinkWithTags() {
        
        //Armo el link y le quito la hash
        $linkWithHash = Configure::read('static_domain_for_mails') . Router::url(array('base' => false,
                    'controller' => 'subscriptions',
                    'action' => 'unsubscribe_all',
                    $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberKey')),
                    $this->PowerMta->getCompleteTag(Configure::read('EmailFilesGenerator.Pmta.SubscriberHashKey')),
                    'admin' => false,
                    'plugin' => null
                        ), false);

        
        return $linkWithHash;
    }
    
    function getHash($subscriberId){
        //Armo el link y le quito la hash
        $linkWithHash = Configure::read('static_domain_for_mails') . Router::url(array('base' => false,
                    'controller' => 'subscriptions',
                    'action' => 'unsubscribe',
                    $subscriberId,
                    'admin' => false,
                    'plugin' => null
                        ), false);

        return substr(strrchr($linkWithHash, '/'), 1);
    }

}

?>
