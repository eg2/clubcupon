<?php

class EfgLogComponent extends Object {

    function log($message) {
        CakeLog::write('efg_log', $message);
    }

    function error($ex) {
        if (is_string($ex))
            CakeLog::write('efg_error', $ex);
        else
            CakeLog::write('efg_error', $ex->getMessage());
    }

}