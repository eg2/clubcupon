<?php

class PowerMtaComponent extends Object {
    const EOF = "\n";

    function _geLine($function, $key, $value) {
        return $function . " " . $key . "=\"" . $value . "\"" . self::EOF;
    }

    function getCompleteTag($key) {
        return Configure::read('EmailFilesGenerator.Pmta.BeginTag') . $key . Configure::read('EmailFilesGenerator.Pmta.EndTag');
    }

    function getXDFNLine($key, $value) {
        return $this->_geLine("XDFN", $key, $value);
    }

    function getRCPTLine($mail) {
        return "RCPT TO: <" . $mail . ">". self::EOF;        
    }

}