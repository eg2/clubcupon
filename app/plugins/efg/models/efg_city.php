<?php

class EfgCity extends EfgAppModel {

    const SIMPLE_SENDER_ID = 1;
    const SEGMENTATION_SENDER_ID = 2;

    var $name = 'EfgCity';
    var $alias = 'City';
    var $useTable = 'cities';

    function __construct($id = false, $table = null, $ds = null) {
      parent::__construct($id, $table, $ds);
    }

    function getCitiesOrGroupsToSend($option) {
        $order = array(
            'City.order' => 'DESC'
        );
        $conditions = array(
            'City.is_approved' => 1,
            'City.has_newsletter' => true,
        );


        if ($option == 'cities') {
            $conditions ['City.is_group'] = 0;
        } else if ($option == 'groups') {
            $conditions ['City.is_group'] = 1;
        }


        $fields = array(
            'City.id',
            'City.name',
            'City.email_from',
            'City.vmta_for_no_buyer',
            'City.vmta_for_buyer'
        );
        return $this->find('all', array(
                    'order' => $order,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'recursive' => - 1,
                ));
    }

    function getById($cityId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $cityId
                    ),
                    'recursive' => - 1
                ));
    }

    public function hasSegmentationEnable($cityId) {
      $city = $this->findById($cityId);
      return $city['City']['newsletter_sender_engine_code'] == self::SEGMENTATION_SENDER_ID;
    }

}
