<?php

class EfgDeal extends EfgAppModel {

    var $name = 'EfgDeal';
    var $alias = 'Deal';
    var $useTable = 'deals';
    var $belongsTo = array(
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
    );
    var $hasOne = array(
        'Attachment' => array(
            'className' => 'Attachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'Attachment.class =' => 'Deal'
            ),
            'dependent' => true
        ),
    );

    protected $toSendConditions = array(
    		'Deal.parent_deal_id = EfgDeal.id',
    		'Deal.deal_status_id' => array(
    				ConstDealStatus::Open,
    				ConstDealStatus::Tipped,
    		),
    		'Deal.is_subscription_mail_sent' => 0,
    		'Deal.publication_channel_type_id' => 1,
    );
    
    protected $toSendConditionsForce = array(
    		'Deal.parent_deal_id = EfgDeal.id',
    		'Deal.deal_status_id' => array(
    				ConstDealStatus::Open,
    				ConstDealStatus::Tipped,
    		),
    		'Deal.publication_channel_type_id' => 1,
    );
    
    public function GetDealsOfTheDayAndForce($idCity) {
        return $this->find('all', array(
                    'order' => array(
                        'Deal.is_side_deal' => 'ASC',
                        'Deal.priority' => 'DESC',
                        'Deal.deal_status_id' => 'ASC',
                        'Deal.start_date' => 'ASC',
                    ),
                    'conditions' => array(
                        'Deal.parent_deal_id = Deal.id',
                        'Deal.deal_status_id' => array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                        ),
                        'Deal.city_id' => $idCity,
                        'Deal.publication_channel_type_id' => 1,
                    ),
                    'contain' => array(
                        'Attachment',
                        'City',
                        'Company' => array(
                            'City',
                            'State',
                            'Country',
                        ),
                    ),
                        )
        );

    }
    
    
    function GetDealsOfTheDay($idCity) {

        return $this->find('all', array(
                    'order' => array(
                        'Deal.is_side_deal' => 'ASC',
                        'Deal.priority' => 'DESC',
                        'Deal.deal_status_id' => 'ASC',
                        'Deal.start_date' => 'ASC',
                    ),
                    'conditions' => array(
                        'Deal.parent_deal_id = Deal.id',
                        'Deal.deal_status_id' => array(
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                        ),
                        'Deal.is_subscription_mail_sent' => 0,
                        'Deal.city_id' => $idCity,
                        'Deal.publication_channel_type_id' => 1,
                    ),
                    'contain' => array(
                        'Attachment',
                        'City',
                        'Company' => array(
                            'City',
                            'State',
                            'Country',
                        ),
                    ),
                        )
        );
    }

    function setDealsOfTheDaySendedByCity($idCity) {
        $this->updateAll(array('Deal.is_subscription_mail_sent' => 1), array(
            'Deal.parent_deal_id = Deal.id',
            'Deal.deal_status_id' => array(
                ConstDealStatus::Open,
                ConstDealStatus::Tipped,
            ),
            'Deal.is_subscription_mail_sent' => 0,
            'Deal.city_id' => $idCity,
            'Deal.publication_channel_type_id' => 1,
        ));
    }

    function findLastDealsByUser($startUserId, $rangeSize) {

      $days_for_profile_calculation = Configure::read('EfgUserProfileUpdate.days_for_profile_calculation');
      $from_date = date('Y-m-d', strtotime("-$days_for_profile_calculation days"));

      $lastDeals = $this->find('all', array(
        'recursive' => -1,
        'fields' => array('DealFlatCategory.l1_id', 'DealExternal.user_id', 'COUNT(*)'),
        'group' => array('DealFlatCategory.l1_id', 'DealExternal.user_id'),
        'joins' => array(
          array(
            'table' => 'deal_externals',
            'alias' => 'DealExternal',
            'type' => 'INNER',
            'conditions' => array(
              'DealExternal.deal_id = Deal.id',
            )
          ),
          array(
            'table' => 'deal_flat_categories',
            'alias' => 'DealFlatCategory',
            'type' => 'INNER',
            'conditions' => array(
              'DealFlatCategory.id = Deal.deal_category_id',
            )
          ),
        ),
        'order' => array('DealExternal.user_id ASC'),
        'conditions' => array(
          'DealExternal.user_id >=' => $startUserId,
          'DealExternal.user_id <=' => $startUserId + $rangeSize,
          'DealExternal.created  >=' => $from_date,
          'DealFlatCategory.l1_id' => array(
            ConstSegmentationCategoryIds::CATEGORY_BEAUTY,
            ConstSegmentationCategoryIds::CATEGORY_COUNTRY_DAY,
            ConstSegmentationCategoryIds::CATEGORY_ENTERTAINMENT,
            ConstSegmentationCategoryIds::CATEGORY_GASTRONOMY,
            ConstSegmentationCategoryIds::CATEGORY_NOW_ROOT,
            ConstSegmentationCategoryIds::CATEGORY_OTHER,
            ConstSegmentationCategoryIds::CATEGORY_PET,
            ConstSegmentationCategoryIds::CATEGORY_PHOTOGRAPHY,
            ConstSegmentationCategoryIds::CATEGORY_PRODUCT,
            ConstSegmentationCategoryIds::CATEGORY_SERVICES,
            ConstSegmentationCategoryIds::CATEGORY_SHOPPING,
            ConstSegmentationCategoryIds::CATEGORY_TRAVEL,
          )
        )));
      return $lastDeals;
    }

}
