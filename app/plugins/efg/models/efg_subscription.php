<?php

class EfgSubscription extends EfgAppModel {

    var $name = 'EfgSubscription';
    var $alias = 'Subscription';
    var $useTable = 'subscriptions';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

    function getPagedSubscriptionMailForCity($cityId, $limit, $page, $instanceNumber, $instanceQuantity) {
        $conditions = array('Subscription.city_id' => $cityId, 'is_subscribed' => 1);

        if ($instanceQuantity > 1) {
            $conditions[] = ' MOD(Subscription.id,' . $instanceQuantity . ') = ' . $instanceNumber;
        }

        $joins =  array(
          array(
            'table' => 'user_profiles',
            'alias' => 'UserProfile',
            'type' => 'LEFT',
            'conditions' => array(
              'Subscription.user_id = UserProfile.user_id'
            )
          ));

        return $this->find('all', array(
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'order' => array('Subscription.is_voluntary DESC'),
                    'fields' => array('Subscription.id',
                        'Subscription.email', 'UserProfile.segmentation_profile_id'),
                    'limit' => $limit,
                    'page' => $page,
                    'recursive' => -1));
    }
    
    function getPagedSubscriptionMailForCityAndProfile($cityId, $limit, $page, $instanceNumber, $instanceQuantity, $profileId) {
        $defaultProfileId = Configure::read('EfgUserProfileUpdate.default_profile_id');
        $joins = "";
        $conditions = array('Subscription.city_id' => $cityId, 'Subscription.is_subscribed' => 1);
        if ($profileId != $defaultProfileId) {
            $conditions[] = 'UserProfile.segmentation_profile_id='. $profileId;
            $joins =  array(
                       array(
                           'table' => 'user_profiles',
                           'alias' => 'UserProfile',
                           'type' => 'INNER',
                           'conditions' => array(
                               'Subscription.user_id = UserProfile.user_id'                               
                           )
                       ));
        } else {
           $joins =  array(
                        array(
                            'table' => 'user_profiles',
                            'alias' => 'UserProfile',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Subscription.user_id = UserProfile.user_id'
                            )
                        ));
           $conditions[] = array("OR" => array (
                             'UserProfile.segmentation_profile_id' => $profileId,
                            'Subscription.user_id' => null)
                            );
        }
        if ($instanceQuantity > 1) {
            $conditions[] = ' MOD(Subscription.id,' . $instanceQuantity . ') = ' . $instanceNumber;
        }

        return $this->find('all', array(
                    'recursive' => -1,
                    'conditions' => $conditions,
                    'order' => array('Subscription.is_voluntary DESC'),
                    'fields' => array('Subscription.id',
                        'Subscription.email', 'UserProfile.segmentation_profile_id'),
                    'limit' => $limit,
                    'joins' => $joins,
                    'page' => $page,
                    'recursive' => -1));
    }

}

