<?php

class EfgEmailsFilesProcess extends EfgAppModel {

    var $name = 'EfgEmailsFilesProcess';
    var $alias = 'EmailsFilesProcess';
    var $useTable = 'emails_files_process';

//    function startAll() {
//        $this->updateAll(array('is_stopped' => false));
//    }
    //Para la ejecucion y la resetea para que arranque desde 0 la proxima vez
    function cancelAll() {
        $this->updateAll(array('is_stopped' => 1, 'city_id' => 0, 'page_number' => 0));
    }

    //Enciende los flags para que el proceso pueda correr nuevamente desde donde termino
    function resetAll() {
        $this->updateAll(array('is_stopped' => 0));
    }

    //Para la ejecucion y la deja lista para que vuelva a empezar desde donde termino la corriente
    function pauseAll() {
        $this->updateAll(array('is_stopped' => 1));
    }

    function trackStatus($cityId, $pageNumber, $serverInstance) {

        $tracking = $this->find('first',
                array('conditions' => array(
//                'city_id' => $cityId,
//                'page_number' => $pageNumber,
                'server_instance' => $serverInstance)
                )
        );

        if (empty($tracking)) {
            //creo el registro
            $this->create();
            $this->set('city_id', $cityId);
            $this->set('page_number', $pageNumber);
            $this->set('server_instance', $serverInstance);
            $this->save();
        }
        else {
            $tracking['EmailsFilesProcess']['page_number'] = $pageNumber;
            $tracking['EmailsFilesProcess']['city_id'] = $cityId;
            $this->save($tracking);
        }
    }

    function isStopped($serverInstance) {
        $tracking = $this->find('first',
                array(
            'conditions' => array('server_instance' => $serverInstance),
            'fields' => array('is_stopped')
                )
        );

        return (!empty($tracking) && $tracking['EmailsFilesProcess']['is_stopped']);
    }

    function getStatus($serverInstance) {
        $tracking = $this->find('first',
                array(
            'conditions' => array('server_instance' => $serverInstance),
            'fields' => array('city_id', 'page_number')
                )
        );

        if (empty($tracking))
            return null;
        else
            return $tracking;
    }

}