<?php

class EfgUser extends EfgAppModel {

    var $name = 'EfgUser';
    var $alias = 'User';
    var $useTable = 'users';

    function findUserLot($startUserId = 0, $lotSize = 100) {
        return $this->find('all', array(
                    'recursive' => -1,
                    'fields' => array('User.id', 'UserProfile.gender'),
                    'joins' => array(
                        array(
                            'table' => 'user_profiles',
                            'alias' => 'UserProfile',
                            'type' => 'INNER',
                            'conditions' => array(
                                'UserProfile.user_id = User.id',
                            )
                        )),
                    'conditions' => array(
                        'User.id >=' => $startUserId,
                        'User.id <=' => $startUserId + $lotSize,
        )));
    }

    function findMaxId() {
      return $this->find('first', array(
          'fields' => array(
            'MAX(User.id) as max_id'
          ),
        )
      );
    }
}

