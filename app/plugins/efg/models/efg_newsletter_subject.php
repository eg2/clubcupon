<?php 

class EfgNewsletterSubject extends EfgAppModel {

  var $name = "EfgNewsletterSubject";
  var $alias = "EfgNewsletterSubject";
  var $useTable = "newsletter_subjects";

  function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
  }


  function findByCityIdAndScheduledDay($cityId, $date) {
    $conditions = array(
      'EfgNewsletterSubject.scheduled' => $date,
      'EfgNewsletterSubject.city_id' => $cityId,
    );

    return $this->find('first', array(
      'conditions' => $conditions,
      'recursive' => - 1,
    ));
  }


}
