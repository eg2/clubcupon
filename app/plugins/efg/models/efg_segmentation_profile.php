<?php

class EfgSegmentationProfile extends EfgAppModel {

    var $useTable = 'segmentation_profiles';
    var $name = 'EfgSegmentationProfile';
    var $alias = 'SegmentationProfile';
    
    function getById($segmentationProfileId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $segmentationProfileId
                    ),
                    'recursive' => - 1
                ));
    }


    public function findSingleMatchCategory($categoryId, $extranConditions) {
      ApiLogger::getInstance()->debug(__METHOD__);

      $conditions = $this->_appendSingleMatchCategoryCondition($categoryId);
      $conditions = array_merge($conditions, $extranConditions);

      $result = $this->find('first', array(
        'conditions' => $conditions
      ));
      
      ApiLogger::getInstance()->trace('segmentation_profile', $result);
      return $result;
    }

    function _appendSingleMatchCategoryCondition($categoryId) {
      $conditions = array();

      $hasTravel = 0;
      $hasProduct = 0;
      $hasBeauty = 0;
      $hasGastronomy = 0;
      $hasEntertainment = 0;
      $hasPhotography = 0;
      $hasServices = 0;
      $hasCountryDay = 0;
      $hasShopping = 0;
      $hasPet = 0;
      $hasNowRoot = 0;
      $hasOther = 0;

      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_PRODUCT) {
        $hasProduct = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_BEAUTY) {
        $hasBeauty = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_GASTRONOMY) {
        $hasGastronomy = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_TRAVEL) {
        $hasTravel = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_ENTERTAINMENT) {
        $hasEntertainment = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_PHOTOGRAPHY) {
        $hasPhotography = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_SERVICES) {
        $hasServices = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_COUNTRY_DAY) {
        $hasCountryDay = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_SHOPPING) {
        $hasShopping = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_PET) {
        $hasPet = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_NOW_ROOT) {
        $hasNowRoot = 1;
      }
      if ($categoryId ==ConstSegmentationCategoryIds::CATEGORY_OTHER) {
        $hasOther = 1;
      }

      $conditions['producto']        = $hasProduct;
      $conditions['belleza']         = $hasBeauty ;
      $conditions['turismo']         = $hasTravel ;
      $conditions['gastronomia']     = $hasGastronomy ;
      $conditions['entretenimiento'] = $hasEntertainment ;
      $conditions['fotografia']      = $hasPhotography ;
      $conditions['servicios']       = $hasServices ;
      $conditions['dia_de_campo']    = $hasCountryDay;
      $conditions['shopping']        = $hasShopping;
      $conditions['mascotas']        = $hasPet;
      $conditions['now_root']        = $hasNowRoot;
      $conditions['otras']           = $hasOther;

      return $conditions;
    }


    public function findWithUsersToSendNewsletterByCity($cityId, $defaultProfileId) {
      return $this->find('all', array(
        'conditions' => array(
          DboSource::expression(
            "( SegmentationProfile.id = " . $defaultProfileId . 
            "   OR " . $this->hasSubscriptionsToSendForCityAndProfileSql($cityId) .
            " ) "),
        )));
    }

    private function hasSubscriptionsToSendForCityAndProfileSql($cityId) {
      $subscriptionsByCity = " SELECT 1 " .
        " FROM user_profiles AS UserProfile, " .
        "  subscriptions AS Subscription " .
        " WHERE Subscription.city_id = ". $cityId .
        " AND Subscription.user_id = UserProfile.user_id " .
        " AND UserProfile.segmentation_profile_id = SegmentationProfile.id ";
      return " EXISTS ({$subscriptionsByCity}) ";
    }


}
