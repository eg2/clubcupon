<?php
class EfgAttachment extends EfgAppModel
{
    var $name = 'EfgAttachment';
    var $alias = 'Attachment';
    var $useTable = 'attachments';
    
    function getByDeal($dealId) {
        return $this->find('first',
                        array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $dealId,
                        'Attachment.class = ' => 'DealNewsLetter'
                    ),
                    'recursive' => -1,
                        )
        );
    }
}
