<?php

class EfgUserProfile extends EfgAppModel {

    var $name = 'EfgUserProfile';
    var $alias = 'UserProfile';
    var $useTable = 'user_profiles';

    function updateProfileId($user_id, $profile_id) {
        return $this->updateAll(
                        array('segmentation_profile_id' =>
                    $profile_id), array('UserProfile.user_id' => $user_id));
    }

}

