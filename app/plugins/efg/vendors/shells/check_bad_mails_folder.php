<?php

App::import('Core', 'Controller');
App::import('Core', 'Router');
App::import('Core', 'Component');
App::import('Component', 'Efg.EfgLog');
App::import('Component', 'Efg.EfgEmail');

class CheckBadMailsFolderShell extends Shell {

    var $logComponent;
    var $efgEmailComponent;
    const EOF = "\n";

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->logComponent = & new EfgLogComponent();
        $this->efgEmailComponent = & new EfgEmailComponent();
        Configure::write('debug', 0);
    }

    function main() {
        try {
            $command = Configure::read('EmailFilesGenerator.BadMail.Command');
            exec($command, &$output, &$return_var);
            if (count($output) != 0) {
                $body = "Se encontraron archivos en el directorio: " . $path . self::EOF;
                $body .="Archivos:" . self::EOF;
                $body .= implode(self::EOF, $output);
                $this->efgEmailComponent->send('ClubCupon - Existen archivos que PMTA no pudo procesar', $body);
            }
        }
        catch (Exception $exc) {
            echo 'Proceso terminado con errores, revisar el log.';
            $this->logComponent->error($exc);
        }
    }

}