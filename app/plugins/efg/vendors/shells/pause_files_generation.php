<?php

App::import('Core', 'Controller');
App::import('Core', 'Router');
App::import('Core', 'Component');
App::import('Component', 'Efg.EfgLog');
App::import('Model', 'Efg.EfgEmailsFilesProcess');

class PauseFilesGenerationShell extends Shell {

    var $uses = array('EfgEmailsFilesProcess');
    var $instanceNumber;
    var $instanceQuantity;
    var $logComponent;
    const EOF = "\n";

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->logComponent = & new EfgLogComponent();
        $this->instanceNumber = Configure::read('efg.instanceNumber');
        $this->instanceQuantity = Configure::read('efg.instanceQuantity');
        Configure::write('debug', 0);
    }

    function main() {
        
        $this->logComponent->log("Proceso Pausado. Intancia: $this->instanceNumber");

        //Para la ejecucion y la deja lista para que vuelva a empezar desde donde termino la corriente
        $this->EfgEmailsFilesProcess->pauseAll();
        
        echo "El proceso de generacion de archivos de mails para PowerMTA fue detenido. Para poder voler a correr el proceso primero debe ejectutar el comanto 'cake reset_files_generation'.\n";
    }

}