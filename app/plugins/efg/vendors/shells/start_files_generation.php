<?php

App::import('Core', 'Controller');
App::import('Core', 'Router');
App::import('Core', 'Component');
App::import('Controller', 'Efg.Emails');
App::import('Component', 'Efg.PowerMta');
App::import('Component', 'Efg.EfgLog');
App::import('Component', 'Efg.EfgEmail');
App::import('Component', 'Efg.UnsubscriberLink');
App::import('Component', 'Efg.EfgUserProfileUpdater');
App::import('Model', 'Efg.EfgDeal');
App::import('Model', 'Efg.EfgCity');
App::import('Model', 'Efg.EfgSubscription');
App::import('Model', 'Efg.EfgEmailsFilesProcess');
App::import('Model', 'Efg.EfgSegmentationProfile');
App::import('Model', 'Efg.EfgNewsletterSubject');

class SynchronizationException extends Exception { // Redefine the exception so message isn't optional

    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class DummyLogComponent {

    public function log($text) {
        
    }

    public function error($text) {
        
    }

}

class StartFilesGenerationShell extends Shell {

    var $uses = array(
        'EfgCity',
        'EfgDeal',
        'EfgSubscription',
        'EfgEmailsFilesProcess',
        'EfgSegmentationProfile',
        'EfgNewsletterSubject',
    );
    var $powerMtaComponent;
    var $instanceNumber;
    var $instanceQuantity;
    var $logComponent;
    var $efgEmailComponent;
    var $unsubscriberLinkComponent;
    var $defaultProfileId;
    var $profileIdsWithoutClasification;

    const EOF = "\n";

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->powerMtaComponent = & new PowerMtaComponent();
        $this->isLogEnable = Configure::read('EmailFilesGenerator.LogEnable');
        $this->isLogVerbose = Configure::read('EmailFilesGenerator.LogVerbose');
        if ($this->isLogEnable) {
            $this->logComponent = & new EfgLogComponent();
        } else {
            $this->logComponent = & new DummyLogComponent();
        }
        $this->logComponent = & new EfgLogComponent();
        $this->efgEmailComponent = & new EfgEmailComponent();
        $this->unsubscriberLinkComponent = & new UnsubscriberLinkComponent();
        $this->efgUserProfileUpdater = & new EfgUserProfileUpdaterComponent();
        $this->instanceNumber = Configure::read('efg.instanceNumber');
        $this->instanceQuantity = Configure::read('efg.instanceQuantity');
        $this->genderFieldConversor = array(
          "MASC" => "male_text",
          "FEM" => "female_text",
          "DESCONOCIDO" => "unisex_text",
        );

        $this->defaultProfileId = Configure::read('EfgUserProfileUpdate.default_profile_id');
        $this->profileIdsWithoutClasification = Configure::read('EfgUserProfileUpdate.profiles_without_clasification');
        Configure::write('debug', 0);
    }

    function _getFromVerpEmail($date, $cityId, $suscriber) {
        // Armo el mail de respuesta con el formato:
        // IdCiudad+YYYYMMDD+ClaveDeAplicacionEnEMT+MailSinArroba@DominioBounce
        $verpMail = $cityId . "+";
        $verpMail.= $date . "+";
        $verpMail.= Configure::read('EmailFilesGenerator.Emt.Key') . "+";
        $verpMail.= str_replace('@', '=', $suscriber);
        $verpMail.= "@" . Configure::read('EmailFilesGenerator.Bounce.Domain');
        return $verpMail;
    }

    function _getHashUnsubscriberLink($subscriberId) {
        $t_url = $url;
        unset($t_url['plugin']);
        unset($t_url['admin']);
        $skip = array_flip(array(
            'bare',
            'action',
            'controller',
            'plugin',
            'ext',
            '?',
            '#',
            'prefix',
            'admin',
            'pass',
            'named',
            'url',
            'form',
            'autoRender',
            'return',
            'requested',
            'id'
        ));
        foreach ($t_url as $key => $value) {
            if (is_string($key) && !array_key_exists($key, $skip)) {
                unset($t_url[$key]);
            }
        }
        $_secure_hash = md5(Configure::read('Security.salt') . 'secureurlhash' . implode('|', $t_url));
        $url = array_merge($url, array(
            $_secure_hash
        ));
    }

    function getVmta($segmentationProfileId, $city) {
      $vmta = null;

      if (is_null($segmentationProfileId) || in_array($segmentationProfileId, $this->profileIdsWithoutClasification))  {
        if (!is_null($city['City']['vmta_for_no_buyer'])) {
          $vmta = $city['City']['vmta_for_no_buyer'];
        } else {
          $vmta = Configure::read('EmailFilesGenerator.Pmta.vmta_for_no_buyer');
        }
      } else {
        if (!is_null($city['City']['vmta_for_buyer'])) {
          $vmta = $city['City']['vmta_for_buyer'];
        } else {
          $vmta = Configure::read('EmailFilesGenerator.Pmta.vmta_for_buyer');
        }
      }

      return $vmta;
    }

    // Devuelve las lineas correspondientes a un suscripto.
    function _getSubscriberLines($date, $city, $subscriber) {
        $vmta = $this->getVmta($subscriber['UserProfile']['segmentation_profile_id'], $city);
        if ($this->isLogVerbose) {
          $this->logComponent->log("VMTA ".$vmta .", Seleccionado por : " . json_encode(array(
            'SegmentationProfileID' => $subscriber['UserProfile']['segmentation_profile_id'],
            'City' => $city)));
        }
        $string = $this->powerMtaComponent->getXDFNLine("*vmta", $vmta);
        $string.= $this->powerMtaComponent->getXDFNLine("City", $city['City']['name']);
        $string.= $this->powerMtaComponent->getXDFNLine(Configure::read('EmailFilesGenerator.Pmta.SubscriberKey'), $subscriber['Subscription']['id']);
        $string.= $this->powerMtaComponent->getXDFNLine(Configure::read('EmailFilesGenerator.Pmta.SubscriberHashKey'), $this->unsubscriberLinkComponent->getHash($subscriber['Subscription']['id']));
        $string.= $this->powerMtaComponent->getXDFNLine("msgid", String::uuid());
        $string.= $this->powerMtaComponent->getXDFNLine("*from", $this->_getFromVerpEmail($date, $city['City']['id'], $subscriber['Subscription']['email']));
        $string.= $this->powerMtaComponent->getRCPTLine($subscriber['Subscription']['email']);
        return $string;
    }

    function _getFileName($date, $idCity, $pageNumber, $segmentation_profile_id) {
        return $date . '_' . $this->instanceNumber . '_' . $idCity . '_' . $segmentation_profile_id . '_' . $pageNumber . '.txt';
    }

    function _createAndSyncFile($date, $city, $pageNumber, $subject, $cityEmailFrom, $subscriptions, $htmlEmail, $segmentation_profile_id) {
        $directory = Configure::read('EmailFilesGenerator.Directory.Output');
        $fullFileName = $directory . $this->_getFileName($date, $city['City']['id'], $pageNumber, $segmentation_profile_id);
        $emailFrom = !empty($cityEmailFrom) ? $cityEmailFrom : Configure::read('EmailFilesGenerator.From.Mail');
        $handle = fopen($fullFileName, "w");
        // Header del archivo
        if (Configure::read('EmailFilesGenerator.xack_off.enabled')) {
            fwrite($handle, "XACK OFF" . self::EOF);
        }
        fwrite($handle, "XMRG FROM:<" . $emailFrom . ">" . self::EOF);
        // Por cada suscripto
        foreach ($subscriptions as $subscriber) {
            fwrite($handle, $this->_getSubscriberLines($date, $city, $subscriber));
        }
        // Info General
        fwrite($handle, "XPRT 1 LAST" . self::EOF);
        fwrite($handle, "Mime-Version: 1.0" . self::EOF);
        fwrite($handle, "Content-Type: text/html; charset=\"UTF-8\"" . self::EOF);
        fwrite($handle, "From: \"ClubCupon - " . $city['City']['name'] . "\" " . chr(60) . $emailFrom . chr(62) . self::EOF);
        fwrite($handle, "To: [*to]" . self::EOF);
        fwrite($handle, "Message-ID: <[msgid]@clubcupon.com.ar>" . self::EOF);
        fwrite($handle, "X-MC-Subaccount: ccnews" . self::EOF);
        fwrite($handle, "Date: [*date]" . self::EOF);
        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
        fwrite($handle, "Subject: " . $subject . self::EOF);
        // Cuerpo del mail
        fwrite($handle, $htmlEmail);
        // Fin del archivo
        fwrite($handle, self::EOF);
        fwrite($handle, ".");
        fclose($handle);
        if (Configure::read('EmailFilesGenerator.Rsync.Enabled')) {
            $command = str_replace('[ORIGEN]', $fullFileName, Configure::read('EmailFilesGenerator.Rsync.Command'));
            exec($command, $output, $return_var);
            if ($return_var != 0) {
                $result = implode("\n", $output);
                $text = "** Ocurrio un error en la sincronizacion del archivo para la instancia $this->instanceNumber **" . self::EOF;
                $text.= "Comando: $command" . self::EOF;
                $text.= "Resultado: $result" . self::EOF;
                $this->efgEmailComponent->send('Hubo un error en la sincronización', $text);
                throw new SynchronizationException($text);
            }
        }
    }

    function _retrieveMailSubject($cityId, $profileUserId, &$subject) {
        $deals = $this->EfgDeal->GetDealsOfTheDay($cityId);
        $deals = $this->efgUserProfileUpdater->orderDealForUserProfile($deals, $profileUserId);
        $hasAlmostMainDeal = false;
        $mainDeal = false;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if (!$deal['Deal']['is_side_deal']) {
                    $hasAlmostMainDeal = true;
                    $mainDeal = $deal;
                    break;
                }
            }
            if (empty($mainDeal['Deal']['custom_subject'])) {
                if (count($deals) == 1) {
                    $subject = $mainDeal['Deal']['name'];
                } else if (count($deals) == 2) {
                    $subject = $mainDeal['Deal']['name'] . ' Y 1 Super Oferta más.';
                } else {
                    $subject = $mainDeal['Deal']['name'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas más.';
                }
            } else {
                $subject = $mainDeal['Deal']['custom_subject'];
            }
        }
        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";      
    }

    // Permite saber si para esa ciudad existe una oferta principal.
    // Si existe, devuelve el subject del mail, que lo obtiene de la oferta principal.
    function _hasAlmostMainDeal($cityId, &$subject) {
        $hasAlmostMainDeal = false;
        $deals = $this->EfgDeal->GetDealsOfTheDay($cityId);
        $mainDeal = null;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if (!$deal['Deal']['is_side_deal']) {
                    $hasAlmostMainDeal = true;
                    $mainDeal = $deal;
                    break;
                }
            }
            if (empty($mainDeal['Deal']['custom_subject'])) {
                if (count($deals) == 1) {
                    $subject = $mainDeal['Deal']['name'];
                } else if (count($deals) == 2) {
                    $subject = $mainDeal['Deal']['name'] . ' Y 1 Super Oferta más.';
                } else {
                    $subject = $mainDeal['Deal']['name'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas más.';
                }
            } else {
                $subject = $mainDeal['Deal']['custom_subject'];
            }
        }
        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
        return $hasAlmostMainDeal;
    }

    function _validateArguments($arguments) {
        $valid = array(
            'cities',
            'groups',
            'cities_and_groups'
        );
        if (!in_array($arguments[0], $valid)) {
            $message = 'Argumento invalido, se esperaba cities, groups o cities_and_groups y se encontro: <' . $arguments[0] . '>';
            $this->out($message);
            throw new Exception($message);
        }
    }

    function main() {
        $this->logComponent->log("Inicio del proceso. Intancia: $this->instanceNumber");
        $this->logComponent->log("VMTA Disponibles:" . json_encode(array(
          'vmta_for_no_buyer' => Configure::read('EmailFilesGenerator.Pmta.vmta_for_no_buyer'),
          'vmta_for_buyer' => Configure::read('EmailFilesGenerator.Pmta.vmta_for_buyer'),
        )));
        try {
            $lastCityId = 0;
            $lastPage = 0;
            $pageSize = Configure::read('EmailFilesGenerator.Pmta.PageSize');
            $date = date("Ymd");
            $this->logComponent->log($date);
            $this->_validateArguments($this->args);
            $cities = $this->EfgCity->getCitiesOrGroupsToSend($this->args[0]);
            if (empty($cities)) {
                throw new Exception('No se encontraron Ciudades. Se aborta el proceso.');
            }
            $lastRunStatus = $this->EfgEmailsFilesProcess->getStatus($this->instanceNumber);
            if ($lastRunStatus != null) {
                $lastCityId = $lastRunStatus['EmailsFilesProcess']['city_id'];
                $lastPage = $lastRunStatus['EmailsFilesProcess']['page_number'];
            }
            echo "******** Instancia -> $this->instanceNumber ********\n";

            echo "*** Cantidad de Ciudades a Procesar -> ". count($cities) ."***\n";

            $index = 0;
            for ($index = 0; $index < count($cities); $index++) {
                $city = $cities[$index];
                $cityId = $cities[$index]['City']['id'];
                $cityName = $cities[$index]['City']['name'];
                $cityEmailFrom = $cities[$index]['City']['email_from'];

                $this->logComponent->log("Procesando Ciudad: " . $cityId . " | " . $extraLogInfoForCity);
                echo "*** Procesando Ciudad -> $cityId ***\n";
                // Retomo desde la ultima ciudad si es que fue parada la ejecucion
                if ($lastCityId != 0) {
                    if ($lastCityId == $cityId) {
                        $lastCityId = 0;
                    } else {
                        continue;
                    }
                }
                $subject = "";
                // Si por lo menos existe un Main Deal se envia el mail.
                if ($this->_hasAlmostMainDeal($cityId, $subject)) {

                    $defaultProfileId = Configure::read('EfgUserProfileUpdate.default_profile_id');

                    if ($this->EfgCity->hasSegmentationEnable($cityId)) {
                        $this->logComponent->log("Segmentacion Activa... buscando perfiles.");

                        $profiles = $this->EfgSegmentationProfile->findWithUsersToSendNewsletterByCity(
                          $cityId, $defaultProfileId );

                        echo "*** Cantidad de Perfiles a Procesar -> ". count($profiles) ."***\n";
                        $this->logComponent->log("Profiles a Procesar :" . json_encode($profiles));

                        // para cada profile de usuario distinto, se arma un mail distinto con deal ordenados segun la segmentacion.
                        // Primero tratamos los usuarios
                        foreach ($profiles as $profile) {

                            echo "*******  Procesando perfil:". json_encode($profile) . " \n ";
                            $this->logComponent->log("Procesando perfil:" . json_encode($profile));
                            $segmentationProfileId = $profile['SegmentationProfile']['id'];
                            // Obtengo el html del mail
                            // Obtengo la pagina de suscripciones para esa ciudad-instancia
                            $lotOfSusbscriptions = $this->EfgSubscription->getPagedSubscriptionMailForCityAndProfile($cityId, $pageSize, 1, $this->instanceNumber, $this->instanceQuantity, $segmentationProfileId);
                            // Si no existen suscripciones para este perfil de segmentacion, no se genera el mail y se pasa al perfil siguiente
                            if (empty($lotOfSusbscriptions)) {
                                $this->logComponent->log("Sin subscripciones.");
                                continue;
                            } else {
                                $this->logComponent->log("Con subscripciones.");
                            }

                            if ($segmentationProfileId == $this->defaultProfileId) {

                              $email = $this->_makeMailWithoutSegmentationOrder($cityId, $date);
                            } else {

                              $email = $this->_makeMailWithSegmentationOrder($cityId, $date, $segmentationProfileId);
                            }

                            // echo "**** Email Generado " . json_encode($email) . " ***** \n";
                            $this->logComponent->log("Email Generado: " . json_encode($email));

                            $subject = $email['subject'];
                            $htmlEmail = $email['htmlBody'];
                            if ($lastPage == 0)
                                $page = 1;
                            else {
                                $page = $lastPage + 1;
                                $lastPage = 0;
                            }
                            $i = 0;
                            while ($page != 0 && !$this->EfgEmailsFilesProcess->isStopped($this->instanceNumber)) {
                                // Reconecto si es que se cayó la conexion.
                                if (!$this->EfgCity->getDatasource()->isConnected()) {
                                    $this->EfgCity->getDatasource()->connect();
                                }
                                // Guardo el status de la ciudad-pagina-instancia que estoy procesando
                                $this->EfgEmailsFilesProcess->trackStatus($cityId, $page, $this->instanceNumber);
                                // Obtengo la pagina de suscripciones para esa ciudad-instancia
                                $lotOfSusbscriptions = $this->EfgSubscription->getPagedSubscriptionMailForCityAndProfile($cityId, $pageSize, $page, $this->instanceNumber, $this->instanceQuantity, $segmentationProfileId);
                                try {
                                    if (!empty($lotOfSusbscriptions))
                                        $this->logComponent->log("Sincronizando archivo.");
                                    $this->_createAndSyncFile($date, $city, $page, $subject, $cityEmailFrom, $lotOfSusbscriptions, $htmlEmail, $segmentationProfileId);
                                } catch (SynchronizationException $exc) {
                                    $this->logComponent->error($exc);
                                }
                                if (!empty($lotOfSusbscriptions) && count($lotOfSusbscriptions) == $pageSize)
                                    $page++;
                                else {
                                    $page = 0;
                                }
                            }
                        }
                    } else {

                        echo "******  Segmentacion Pausada, procesando perfil por defecto:" . json_encode($profile);
                        $this->logComponent->log("Segmentacion Pausada, procesando perfil por defecto:" . json_encode($profile));
                        echo __METHOD__."::".__LINE__."\n";
                        $segmentationProfileId = $profile['SegmentationProfile']['id'];
                        echo __METHOD__."::".__LINE__."\n";
                        // Obtengo el html del mail
                        // Obtengo la pagina de suscripciones para esa ciudad-instancia
                        $lotOfSusbscriptions = $this->EfgSubscription->getPagedSubscriptionMailForCity($cityId, $pageSize, 1, $this->instanceNumber, $this->instanceQuantity);
                        echo __METHOD__."::".__LINE__."\n";
                        // Si no existen suscripciones para este perfil de segmentacion, no se genera el mail y se pasa al perfil siguiente
                        if (empty($lotOfSusbscriptions)) {
                            echo __METHOD__."::".__LINE__."\n";
                            $this->logComponent->log("Sin subscripciones.");
                            echo __METHOD__."::".__LINE__."\n";
                            continue;
                        }

                        echo __METHOD__."::".__LINE__."\n";
                        $email = $this->_makeMailWithoutSegmentationOrder($cityId, $date);
                        echo "**** Email Generado " . json_encode($email) . " ***** ";
                        $subject = $email['subject'];
                        echo __METHOD__."::".__LINE__."\n";
                        $htmlEmail = $email['htmlBody'];
                        echo __METHOD__."::".__LINE__."\n";

                        if ($lastPage == 0) {
                            $page = 1;
                        } else {
                            $page = $lastPage + 1;
                            $lastPage = 0;
                        }
                        while ($page != 0 && !$this->EfgEmailsFilesProcess->isStopped($this->instanceNumber)) {
                            // Reconecto si es que se cayó la conexion.
                            if (!$this->EfgCity->getDatasource()->isConnected()) {
                                $this->EfgCity->getDatasource()->connect();
                            }
                            // Guardo el status de la ciudad-pagina-instancia que estoy procesando
                            $this->EfgEmailsFilesProcess->trackStatus($cityId, $page, $this->instanceNumber);
                            // Obtengo la pagina de suscripciones para esa ciudad-instancia
                            $lotOfSusbscriptions = $this->EfgSubscription->getPagedSubscriptionMailForCity($cityId, $pageSize, $page, $this->instanceNumber, $this->instanceQuantity);
                            $this->logComponent->log(count($lotOfSusbscriptions) . " subscripciones a procesar ....");
                            try {
                                if (!empty($lotOfSusbscriptions))
                                    $this->logComponent->log("Sincronizando archivo.");
                                $this->_createAndSyncFile($date, $city, $page, $subject, $cityEmailFrom, $lotOfSusbscriptions, $htmlEmail, $segmentationProfileId);
                            } catch (SynchronizationException $exc) {
                                $this->logComponent->error($exc);
                            }
                            if (!empty($lotOfSusbscriptions) && count($lotOfSusbscriptions) == $pageSize)
                                $page++;
                            else {
                                $page = 0;
                            }
                        }
                    }
                    // Cuando termino de procesar una ciudad los marco como enviados
                    $isMain = Configure::read('efg.is_main');
                    if ($isMain == 1) {
                        $this->EfgDeal->setDealsOfTheDaySendedByCity($cityId);
                    }
                } else {
                    $this->logComponent->log("No hay Deals.");
                    echo "No hay Deals" . self::EOF;
                }
            }
            if ($index == count($cities) && !$this->EfgEmailsFilesProcess->isStopped($this->instanceNumber)) {
                // Si el proceso termino correctamente reseto el status
                $this->EfgEmailsFilesProcess->trackStatus(0, 0, $this->instanceNumber);
            }
        } catch (Exception $e) {
            echo 'Proceso terminado con errores, revisar el log.';
            $this->logComponent->error($e);
        }
        echo "Fin del proceso.";
        $this->logComponent->log("Fin del proceso. Intancia: $this->instanceNumber");
    }


    private function _makeMailWithoutSegmentationOrder($cityId, $date) {
      echo __METHOD__."::".__LINE__."\n";
      return $this->_makeMail($cityId, $date, false, false);
    }

    private function _makeMailWithSegmentationOrder($cityId, $date, $segmentationProfileId) {
      echo __METHOD__."::".__LINE__."\n";
      return $this->_makeMail($cityId, $date, $segmentationProfileId, true);
    }

    private function _makeMail($cityId, $date, $segmentationProfileId = false, $enableSegmenationOrder = false) {
      echo __METHOD__."::params:".json_encode(array(
        'cityId' => $cityId,
        'date' => $date,
        'segmentationProfileId' => $segmentationProfileId,
        'enableSegmenationOrder' => $enableSegmenationOrder))."\n";
        $deals = $this->EfgDeal->GetDealsOfTheDay($cityId);
        if ($segmentationProfileId && $segmentationProfileId != $this->defaultProfileId) {
          $deals = $this->efgUserProfileUpdater->orderDealForUserProfile($deals, $segmentationProfileId);
        }

        $this->logComponent->log("Ofertas a Enviar:" . json_encode($deals));
        $subject = $this->_makeMailSubject($cityId, $date, $segmentationProfileId, $deals);
        $htmlBody = $this->_requestHtmlMailBody($cityId, $date, $segmentationProfileId, $enableSegmenationOrder);
        return array('subject' => $subject, 'htmlBody' => $htmlBody);
    }

    private function _makeMailSubject($cityId, $date, $segmentationProfileId, $deals) {
      $this->logComponent->log(__FUNCTION__ . json_encode(array($cityId, $date, $segmentationProfileId, $deals)));
      $subjectText = null;

      $subjectOfDay = $this->EfgNewsletterSubject->findByCityIdAndScheduledDay($cityId, $date);
      $this->logComponent->log("Subject Of Day: " . json_encode($subjectOfDay));


      if ( !empty($segmentationProfileId)
        && !empty($subjectOfDay)) {
        $segmentationProfile = $this->EfgSegmentationProfile->findById($segmentationProfileId);
        $genderField = $this->genderFieldConversor[$segmentationProfile['SegmentationProfile']['sexo']];
        $subjectText = $subjectOfDay['EfgNewsletterSubject'][$genderField];

      } elseif (empty($segmentationProfileId)) {
        $subjectText = $this->_makeMailSubjectByDealsWithMainDeal($deals);

      } else {
        $subjectText = $this->_makeMailSubjectByDeals($deals);

      }

      return $subjectText;
    }


    private function _makeMailSubjectByDealsWithMainDeal($deals) {
        $subject = "";
        $mainDeal = false;
        if (!empty($deals)) {
            foreach ($deals as $deal) {
                if (!$deal['Deal']['is_side_deal']) {
                    $mainDeal = $deal;
                    break;
                }
            }
            $subject = $this->_makeMailSubjectByDeals($deals, $mainDeal);
        }
        return $subject;
    }


    private function _makeMailSubjectByDeals($deals, $importantDeal=false) {
        $subject = "";

        if (!empty($deals)) {

            if (empty($importantDeal)) {
              $importantDeal = $deals[0];
            }

            if (empty($importantDeal['Deal']['custom_subject'])) {
                if (count($deals) == 1) {
                    $subject = $importantDeal['Deal']['name'];
                } else if (count($deals) == 2) {
                    $subject = $importantDeal['Deal']['name'] . ' Y 1 Super Oferta más.';
                } else {
                    $subject = $importantDeal['Deal']['name'] . ' Y ' . (count($deals) - 1) . ' Super Ofertas más.';
                }
            } else {
                $subject = $importantDeal['Deal']['custom_subject'];
            }
        }

        return $subject;
     //   return "=?UTF-8?B?" . base64_encode($subject) . "?=";
    }


    private function _requestHtmlMailBody($cityId, $date, $segmentationProfileId = false, $enableSegmenationOrder = true) {
  //    echo "\n" . __METHOD__ . json_encode(array(
  //      'cityId' => $cityId,
  //      "date" => $date,
  //      "segmentationProfileId" => $segmentationProfileId,
  //      "enableSegmenationOrder" => $enableSegmenationOrder
  //    )). "\n";

      $htmlBody = $this->requestAction( array(
        'plugin' => 'efg',
        'controller' => 'emails',
        'action' => 'newsletter',
        $cityId,
        $date,
        $segmentationProfileId,
        $enableSegmenationOrder,
      ), array(
        'return'
      ));

      return $htmlBody;
    }


}
