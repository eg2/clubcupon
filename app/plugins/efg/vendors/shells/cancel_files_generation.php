<?php

App::import('Core', 'Controller');
App::import('Core', 'Router');
App::import('Core', 'Component');
App::import('Component', 'Efg.EfgLog');
App::import('Model', 'Efg.EfgEmailsFilesProcess');

class CancelFilesGenerationShell extends Shell {

    var $uses = array('EfgEmailsFilesProcess');
    var $instanceNumber;
    var $instanceQuantity;
    var $logComponent;
    const EOF = "\n";

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->logComponent = & new EfgLogComponent();
        $this->instanceNumber = Configure::read('efg.instanceNumber');
        $this->instanceQuantity = Configure::read('efg.instanceQuantity');
        Configure::write('debug', 0);
    }

    function main() {
        
        $this->logComponent->log("Proceso Cancelado. Intancia: $this->instanceNumber");
        
        //Para la ejecucion y la resetea para que arranque desde 0 la proxima vez
        $this->EfgEmailsFilesProcess->cancelAll();
        
        echo "El proceso de generacion de archivos de mails para PowerMTA fue detenido y reseteado. Para poder voler a correr el proceso primero debe ejectutar el comanto 'cake reset_files_generation'.\n";
    }

}
