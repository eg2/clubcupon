<?php

App::import('Component', 'efg.EfgUserProfileUpdater');
App::import('Model', 'efg.EfgSubscription');
App::import('Model', 'efg.EfgSegmentationProfile');

class UpdateUserProfilesShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->SegmentationProfile = new EfgSegmentationProfile();
        $this->EfgUserProfileUpdater = new EfgUserProfileUpdaterComponent();
    }

    function main() {
        Configure::write('debug', 0);
        $this->EfgUserProfileUpdater->generateProfile();
    }
    
    
    /**
     * Permite generar el conjuntos de perfiles es decir la combinación de las 12 categorias y 3 sexos
     */
   function createSegmentationProfileFromScratch() {
        $sexos = array("FEM", "MASC", "DESCONOCIDO");
        for ($i = 0; $i < 4096; $i++) {
            foreach ($sexos as $sexo) {
                $this->SegmentationProfile->create();
                $binario = str_pad(decbin($i), 12, "0", STR_PAD_LEFT);
                $arrayBin = str_split($binario);
                $this->data['SegmentationProfile']['sexo'] = $sexo;
                $this->data['SegmentationProfile']['producto'] = intval($arrayBin[0]);
                $this->data['SegmentationProfile']['belleza'] = intval($arrayBin[1]);
                $this->data['SegmentationProfile']['gastronomia'] = intval($arrayBin[2]);
                $this->data['SegmentationProfile']['turismo'] = intval($arrayBin[3]);
                $this->data['SegmentationProfile']['entretenimiento'] = intval($arrayBin[4]);
                $this->data['SegmentationProfile']['fotografia'] = intval($arrayBin[5]);
                $this->data['SegmentationProfile']['servicios'] = intval($arrayBin[6]);
                $this->data['SegmentationProfile']['dia_de_campo'] = intval($arrayBin[7]);
                $this->data['SegmentationProfile']['shopping'] = intval($arrayBin[8]);
                $this->data['SegmentationProfile']['mascotas'] = intval($arrayBin[9]);
                $this->data['SegmentationProfile']['now_root'] = intval($arrayBin[10]);
                $this->data['SegmentationProfile']['otras'] = intval($arrayBin[11]);
                $this->SegmentationProfile->save($this->data);
            }
        }
    }
}
?>
