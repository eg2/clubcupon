<?php

App::import('Core', 'Controller');
App::import('Core', 'Router');
App::import('Core', 'Component');
App::import('Component', 'Efg.EfgLog');
App::import('Model', 'Efg.EfgEmailsFilesProcess');

class ResetFilesGenerationShell extends Shell {

    var $uses = array('EfgEmailsFilesProcess');
    var $instanceNumber;
    var $instanceQuantity;
    var $logComponent;
    const EOF = "\n";

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->logComponent = & new EfgLogComponent();
        $this->instanceNumber = Configure::read('efg.instanceNumber');
        $this->instanceQuantity = Configure::read('efg.instanceQuantity');
        Configure::write('debug', 0);
    }

    function main() {
        
        $this->logComponent->log("Proceso reinicializado. Intancia: $this->instanceNumber");
        
        //Apaga los flags para que el proceso pueda correr nuevamente desde donde termino
        $this->EfgEmailsFilesProcess->resetAll();
        
        echo "El proceso de generacion de archivos de mails para PowerMTA esta listo para ser corrido nuevamente.\n";
    }

}