<?php

class CompanyCandidate extends NowAppModel {

    var $name = 'CompanyCandidate';
    var $alias = 'CompanyCandidate';
    var $useTable = 'company_candidates';
    var $actsAs = array(
        'Sluggable' => array(
            'label' => array(
                'name'
            )
        )
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'FiscalCity' => array(
            'className' => 'City',
            'foreignKey' => 'fiscal_city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'FiscalState' => array(
            'className' => 'State',
            'foreignKey' => 'fiscal_state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function asertMinLength($check, $limit) {
        $value = array_values($check);
        return strlen($value[0]) >= $limit;
    }

    function asertMaxLength($check, $limit) {
        $value = array_values($check);
        return strlen($value[0]) <= $limit;
    }

    function asertLength($check, $limit) {
        $value = array_values($check);
        return strlen($value[0]) == $limit;
    }

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array(
                'rule3' => array(
                    'rule' => array(
                        'asertMaxLength',
                        64
                    ),
                    'message' => 'El nombre debe tener un máximo de 64 caracteres.'
                ),
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                ),
                'required' => true
            ),
            'fiscal_name' => array(
                'rule2' => array(
                    'rule' => 'isUnique',
                    'message' => __l('Company is already exist')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                ),
                'required' => true
            ),
            'fiscal_address' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'fiscal_city_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'fiscal_state_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'fiscal_cuit' => array(
                'rule4' => array(
                    'rule' => array(
                        'comparison',
                        '<=',
                        39000000000
                    ),
                    'message' => 'El CUIT debe ser inferior a 39.000.000.000.'
                ),
                'rule3' => array(
                    'rule' => array(
                        'asertLength',
                        11
                    ),
                    'message' => 'El CUIT debe contener 11 números.'
                ),
                'rule2' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debes ingresar sólo números'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                ),
                'required' => true
            ),
            'fiscal_iibb' => array(
                'rule3' => array(
                    'rule' => array(
                        'asertLength',
                        12
                    ),
                    'message' => 'El IIBB debe contener 12 números.'
                ),
                'rule2' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debes ingresar sólo números'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => 'Requerido'
                ),
                'required' => true
            ),
            'percepcion_iibb_caba' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Seleccione el tipo de percepción',
                'required' => true
            ),
            'fiscal_cond_iva' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'fiscal_phone' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Debes ingresar sólo números, sin espacios, ni guiones',
                'required' => true
            ),
            'contact_phone' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Debes ingresar sólo números, sin espacios, ni guiones',
                'required' => true
            ),
            'fiscal_bank_cbu' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'Debes ingresar sólo números',
                'required' => true
            ),
            'persona' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique el tipo de persona',
                'required' => true
            ),
            'declaracion_jurada_terceros' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si es una declaración jurada de pago a terceros',
                'required' => true
            ),
            'cheque_noorden' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Especifique si el pago por cheque es o no a la orden',
                'required' => true
            ),
            'fiscal_bank_account' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'Debes ingresar sólo números',
                'required' => true
            ),
            'email' => array(
                'rule' => 'email',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'Requerido'
            ),
            'url' => array(
                'rule2' => array(
                    'rule' => array(
                        'url'
                    ),
                    'message' => 'Debe ser una URL válida, comenzando con http://',
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => array(
                        'custom',
                        '/^http:\/\//'
                    ),
                    'message' => 'Debe ser una URL válida, comenzando con http://',
                    'allowEmpty' => true
                ),
                'required' => true
            ),
            'fiscal_bank' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'fiscal_bank_account' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            ),
            'zip' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => 'Requerido',
                'required' => true
            )
        );
        $this->moreActions = array(
            ConstMoreAction::Active => __l('Activate'),
            ConstMoreAction::Inactive => __l('Deactivate')
        );
    }

}
