<?php

class NowScheduledDeals extends AppModel {

    var $name = 'NowScheduledDeals';
    var $alias = 'NowScheduledDeals';
    var $useTable = 'scheduled_deals';
    var $belongsTo = array(
        'NowDeal' => array(
            'recursive' => 1,
            'className' => 'now.NowDeal',
            'foreignKey' => 'deal_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findByDealId($deal_id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'NowScheduledDeals.deal_id' => $deal_id
                    )
        ));
    }

    function getScheduledNowDeals($fecha) {
        return $this->find('all', array(
                    'conditions' => array(
                        'NOT' => array(
                            'NowDeal.deal_status_id' => array(
                                ConstDealStatus::Draft,
                                ConstDealStatus::Rejected,
                                ConstDealStatus::PendingApproval
                            ),
                            'NowDeal.start_date' => $fecha . ' 00:00:00'
                        ),
                        'NowScheduledDeals.' . date("l", strtotime($fecha)) => true,
                        'NowScheduledDeals.start_date <=' => $fecha,
                        'NowScheduledDeals.end_date >=' => $fecha,
                        'NowScheduledDeals.is_disabled' => 0
                    ),
                    'fields' => array(
                        'id',
                        'deal_id'
                    )
        ));
    }

}
