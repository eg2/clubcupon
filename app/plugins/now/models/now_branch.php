<?php

class NowBranch extends NowAppModel {

    var $name = 'NowBranch';
    var $alias = 'NowBranch';
    var $useTable = 'branches';
    var $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Requerido'
        ),
        'street' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Requerido'
        ),
        'number' => array(
            'rule' => 'numeric',
            'required' => true,
            'message' => 'La altura es obligatoria. Sólo se admiten números.'
        ),
        'postal_code' => array(
            'rule' => 'alphaNumeric',
            'required' => true,
            'message' => 'El código postal es obligatorio. Sólo se admiten letras y números.'
        ),
        'latitude' => array(
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Sólo se admiten números.'
        ),
        'longitude' => array(
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Sólo se admiten números.'
        ),
        'telephone' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'required' => true,
                'message' => 'Sólo se admiten números.'
            ),
            'rule2' => array(
                'rule' => array(
                    'maxLength',
                    12
                ),
                'message' => 'Sólo se admiten 12 caracteres.'
            )
        ),
        'fax_number' => array(
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Sólo se admiten números.'
        )
    );
    var $belongsTo = array(
        'Company',
        'Country',
        'State',
        'City',
        'Neighbourhood'
    );
    var $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

    function findAllById($id) {
        return $this->find('all', array(
                    'conditions' => array(
                        'id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

    function findByCompanyId($company_id) {
        return $this->find('all', array(
                    'conditions' => array(
                        'company_id' => $company_id
                    ),
                    'recursive' => - 1
        ));
    }

    function beforesave() {
        App::import('Model', 'NowBranchesDeals');
        $this->NowBranchesDeals = new NowBranchesDeals();
        $this->NowBranchesDeals->updateAll(array(
            'latitud' => $this->data['NowBranch']['latitude'],
            'longitud' => $this->data['NowBranch']['longitude']
                ), array(
            'now_branch_id' => $this->data['NowBranch']['id']
        ));
        return true;
    }

    public function formatAddress($branches_id) {
        $result = $this->findAllById($branches_id);
        $address_template = "%s - %s %s  %s %s";
        $return = array();
        foreach ($result as $branch) {
            $return[] = sprintf($address_template, '<strong>' . $branch[$this->name]['name'] . '</strong>', $branch[$this->name]['street'], $branch[$this->name]['number'], $branch[$this->name]['floor'] ? "Piso: " . $branch[$this->name]['floor'] : '', $branch[$this->name]['apartament'] ? "Departamento: " . $branch[$this->name]['apartament'] : '');
        }
        if (empty($return)) {
            Debugger::log("No se pudo recuperar branches de estas sucursales %s" . join(",", $branches_id), LOG_DEBUG);
            return 'No se pudo recuperar branches de estas sucursales';
        }
        return $return;
    }

}
