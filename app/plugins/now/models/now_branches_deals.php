<?php

class NowBranchesDeals extends NowAppModel {

    var $name = 'NowBranchesDeals';
    var $alias = 'NowBranchesDeals';
    var $useTable = 'branches_deals';
    var $belongsTo = array(
        'now.NowDeal',
        'now.NowBranch',
        'DealCategory'
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public function beforeSave($options) {
        $this->data[$this->name]['modified'] = date("Y-m-d H:i:s");
        App::import('Component', 'Auth');
        $this->Auth = new AuthComponent();
        $modifier_id = $_SESSION['Auth']['User']['id'];
        $this->data[$this->name]['modified_by'] = $modifier_id;
        return true;
    }

    public function afterSave($created) {
        
    }

    public function guardar($branch_id, $deal_id, $ts_inicio, $ts_fin) {
        $saved = false;
        App::import('Component', 'Auth');
        $this->Auth = new AuthComponent();
        $creator_id = $_SESSION['Auth']['User']['id'];
        App::import('Model', 'now.NowBranch');
        App::import('Model', 'now.NowDeal');
        $branch = new NowBranch;
        $deal = new NowDeal;
        $current_branch = $branch->find('first', array(
            'fields' => array(
                'latitude',
                'longitude'
            ),
            'conditions' => array(
                'NowBranch.id' => $branch_id
            )
        ));
        $current_deal = $deal->find('first', array(
            'fields' => array(
                'deal_status_id',
                'deal_category_id'
            ),
            'conditions' => array(
                'NowDeal.id' => $deal_id
            )
        ));
        Debugger::log(print_r(array(
            'deal' => $current_deal,
            'branch' => $current_branch
                        ), 1));
        $saved = $this->save(array(
            'NowBranchesDeals' => array(
                'now_branch_id' => $branch_id,
                'now_deal_id' => $deal_id,
                'created_by' => $creator_id,
                'now_deal_status_id' => $current_deal['NowDeal']['deal_status_id'],
                'deal_category_id' => $current_deal['NowDeal']['deal_category_id'],
                'latitud' => $current_branch['NowBranch']['latitude'],
                'longitud' => $current_branch['NowBranch']['longitude'],
                'ts_inicio' => $ts_inicio,
                'ts_fin' => $ts_fin
            )
        ));
        return $saved;
    }

    public function deleteByDealId($deal_id) {
        return $this->NowBranchesDeals->updateall(array(
                    'NowBranchesDeals.deleted' => 1,
                    'NowBranchesDeals.now_deal_status_id' => ConstDealStatus::Closed
                        ), array(
                    'NowBranchesDeals.now_deal_id' => $deal_id
        ));
    }

    function findByDealId($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'now_deal_id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

    function findBranchesByDealId($id) {
        return $this->find('all', array(
                    'fields' => array(
                        'now_branch_id'
                    ),
                    'conditions' => array(
                        'now_deal_id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

    function deletesoftByDealId($dealId) {
        return $this->updateAll(array(
                    'NowBranchesDeals.deleted' => 1
                        ), array(
                    'NowBranchesDeals.now_deal_id' => $dealId
        ));
    }

    public function getBranchAddressForCupons($deal_id) {
        $this->Behaviors->detach('SoftDeletable');
        $datos = $this->findBranchesByDealId($deal_id);
        $res = array();
        foreach ($datos as $branches) {
            $res[] = $branches[$this->name]['now_branch_id'];
        }
        App::import('Model', 'now.NowBranch');
        $branch = new NowBranch;
        $branch->Behaviors->detach('SoftDeletable');
        $branchAddressForCupons = $branch->formatAddress($res);
        return $branchAddressForCupons;
    }

}
