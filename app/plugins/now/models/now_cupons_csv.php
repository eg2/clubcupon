<?php

class NowCuponsCsv extends AppModel {

    var $name = 'NowCuponsCsv';
    var $useTable = false;
    var $_schema = array(
        'start_date' => array(
            'type' => 'date'
        ),
        'end_date' => array(
            'type' => 'date'
        ),
    );
    var $validate = array(
        'start_date' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false,
        ),
        'end_date' => array(
            'rule1' => array(
                'rule' => '_isValidEndDate',
                'message' => 'Fecha de finalización debe ser mayor que la fecha de inicio',
                'allowEmpty' => false,
            ),
            'rule2' => array(
                'rule' => 'notempty',
                'message' => 'Requerido',
                'allowEmpty' => false,
            ),
        ),
    );

    function _isValidEndDate() {
        return strtotime($this->data[$this->name]['end_date']) >= strtotime($this->data[$this->name]['start_date']);
    }

}
