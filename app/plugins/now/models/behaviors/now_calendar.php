<?php

App::import('Core', 'Behavior');
App::import('Model', 'accounting.AccountingCalendar');
App::import('Model', 'accounting.AccountingType');

class NowCalendarBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    private function joins() {
        return array(
            array(
                'table' => 'accounting_items',
                'alias' => 'AccountingItem',
                'type' => 'inner',
                'conditions' => array(
                    'AccountingCalendar.id = AccountingItem.accounting_calendar_id'
                )
            )
        );
    }

    function findOptions(&$Model, $companyId, $dealIdList = array(), $accountingType = null) {
        $fields = array(
            'AccountingCalendar.id',
            'DATE(MIN(AccountingCalendar.since)) AS since',
            'DATE(MAX(AccountingCalendar.`until`)) AS `until`',
            'SUM(AccountingItem.total_amount) AS total_amount',
            'MAX(DATE(IF(SIGN(LOCATE(\'SOLD\', AccountingItem.accounting_type)) = 1, DATE_ADD(AccountingCalendar.execution, INTERVAL 35 DAY), DATE_ADD(AccountingCalendar.execution, INTERVAL 10 DAY)))) AS payment_date',
            'MAX(IF(AccountingItem.is_visible = 1, \'Pagado\', IF(AccountingItem.is_visible = 2, \'Compensado\', \'Pendiente\'))) AS payment_status'            
        );
        $conditions = array(
            'AccountingCalendar.`status`' => 'ACCOUNTED',
            'AccountingItem.model' => 'Company',
            'AccountingItem.model_id' => $companyId,
            'AccountingItem.deleted' => 0,
        );
        if ($accountingType) {
            $conditions['AccountingItem.accounting_type'] = $accountingType;
        }
        if (!empty($dealIdList)) {
            $conditions['AccountingItem.deal_id'] = $dealIdList;
        }
        $group = array(
            'AccountingCalendar.id'
        );
        return array(
            'fields' => $fields,
            'joins' => $this->joins(),
            'conditions' => $conditions,
            'group' => $group,
            'recursive' => - 1,
            'limit' => 20,
        	'order' => 'AccountingCalendar.id DESC',
        );
    }

    function findAllByCompanyId(&$Model, $companyId, $dealIdList = array(), $accountingType = null) {
        return $this->model->find('all', $this->findOptions($companyId, $dealIdList, $accountingType));
    }

    function findAllByCompanyIdAndDealIdList(&$Model, $companyId, $dealIdList = array(), $accountingType = null) {
        return $this->model->find('all', $this->findOptions($companyId, $dealIdList, $accountingType));
    }

    function findAllBetweenDatesByCompanyId(&$Model, $companyId, $since, $until, $accountingType = null) {
        $fields = array(
            'AccountingCalendar.id',
        );
        $conditions = array(
            'AccountingCalendar.`status`' => 'ACCOUNTED',
            'AccountingItem.model' => 'Company',
            'AccountingItem.model_id' => $companyId,
            'AccountingItem.deleted' => 0,
            'AccountingCalendar.since >=' => $since,
            'AccountingCalendar.`until` <=' => $until
        );
        if ($accountingType) {
            $conditions['AccountingItem.accounting_type'] = $accountingType;
        }
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $this->joins(),
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
