<?php

App::import('Sanitize');
App::import('Core', 'Behavior');
App::import('Model', 'accounting.AccountingItem');

class NowItemBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function fields() {
        return array(
            'AccountingItem.id',
            'AccountingCalendar.id AS calendar_id',
            'DATE(AccountingCalendar.since) AS calendar_since',
            'DATE(AccountingCalendar.`until`) AS calendar_until',
            'IF(SIGN(LOCATE(\'SOLD\', AccountingItem.accounting_type)) = 1, \'Vendido\', \'Redimido\') AS deal_modality',
            'Deal.id AS deal_id',
            'Deal.name AS deal_name',
            'DATE(Deal.start_date) AS deal_start_date',
            'AccountingItem.discounted_price AS discounted_price',
            'AccountingItem.total_quantity AS total_quantity',
            'ROUND(AccountingItem.total_quantity * Deal.discounted_price, 2) AS total_amount',
            'ROUND(Deal.commission_percentage) AS deal_commission_percentage',
            'ROUND(AccountingItem.liquidating_invoiced_amount, 2) AS invoiced_amount',
            'AccountingItem.liquidating_guarantee_fund_percentage AS guarantee_fund_percentage',
            'ROUND(AccountingItem.total_quantity * Deal.discounted_price * (AccountingItem.liquidating_guarantee_fund_percentage / 100), 2) AS guarantee_fund_amount',
            'ROUND(AccountingItem.total_amount, 2) AS liquidation_amount',
            'SIGN(LOCATE(\'BY_SOLD_EXPIRED\', AccountingItem.accounting_type)) AS is_guarantee_fund_return',
            'DATE(IF(SIGN(LOCATE(\'SOLD\', AccountingItem.accounting_type)) = 1, DATE_ADD(AccountingCalendar.execution, INTERVAL 35 DAY), DATE_ADD(AccountingCalendar.execution, INTERVAL 10 DAY))) AS payment_date',
            'IF(AccountingItem.is_visible = 1, \'Pagado\', IF(AccountingItem.is_visible = 2, \'Compensado\', \'Pendiente\')) AS payment_status'
        );
    }

    function joins() {
        return array(
            array(
                'table' => 'accounting_calendars',
                'alias' => 'AccountingCalendar',
                'type' => 'inner',
                'conditions' => array(
                    'AccountingCalendar.id = AccountingItem.accounting_calendar_id'
                )
            ),
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'conditions' => array(
                    'Deal.id = AccountingItem.deal_id'
                )
            )
        );
    }

    function conditions($calendarId, $companyId, $dealIdList = array(), $accountingType = null) {
        $conditions = array(
            'AccountingItem.accounting_calendar_id' => $calendarId,
            'AccountingItem.model' => 'Company',
            'AccountingItem.model_id' => $companyId,
            'AccountingItem.deleted' => 0,
            'AccountingItem.accounting_type' => array(
                'LIQUIDATE_BY_SOLD',
                'LIQUIDATE_BY_SOLD_EXPIRED',
                'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',
                'LIQUIDATE_BY_REDEEMED',
                'LIQUIDATE_BY_REDEEMED_EXPIRED'
            )
        );
        if ($accountingType) {
            $conditions['AccountingItem.accounting_type'] = $accountingType;
        }
        if (!empty($dealIdList)) {
            $conditions['AccountingItem.deal_id'] = $dealIdList;
        }
        return $conditions;
    }

    function findAllByCompanyIdAndCalendarId(&$Model, $companyId, $calendarId, $dealIdList = array(), $accountingType = null) {
        $fields = $this->fields();
        $joins = $this->joins();
        $conditions = $this->conditions($calendarId, $companyId, $dealIdList, $accountingType);
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function findAllByCompanyIdAndCalendarIdAsRows(&$Model, $companyId, $calendarId) {
        $rows = $this->findAllByCompanyIdAndCalendarId($Model, $companyId, $calendarId);
        $accountingItem = Set::classicExtract($rows, '{n}.AccountingItem.{(id|discounted_price|total_quantity|guarantee_fund_percentage)}');
        $accountingCalendar = Set::classicExtract($rows, '{n}.AccountingCalendar.{(calendar_id)}');
        $virtualFields = Set::classicExtract($rows, '{n}.0.{(calendar_since|calendar_until|deal_modality|deal_start_date|total_amount|deal_commission_percentage|invoiced_amount|guarantee_fund_amount|liquidation_amount|is_guarantee_fund_return)}');
        $deal = Set::classicExtract($rows, '{n}.Deal.{(deal_id|deal_name)}');
        return Set::merge($accountingItem, $accountingCalendar, $virtualFields, $deal);
    }

    function newExportRow($row) {
        return array(
            "Id Calendario" => $row['calendar_id'],
            "Calendario Periodo" => 'desde el ' . date("d-m-Y", strtotime($row['calendar_since'])) . ' hasta el ' . date("d-m-Y", strtotime($row['calendar_until'])),
            "Modalidad De Pago" => $row['deal_modality'],
            "Id Oferta" => $row['deal_id'],
            "Oferta" => Sanitize::paranoid($row['deal_name'], array(' ', '@', '-', '$', '+', '.', ':', ';')),
            "Fecha De Publicacion" => date("d-m-Y", strtotime($row['deal_start_date'])),
            "Precio Cupon" => $row['discounted_price'],
            "Q De Cupones" => $row['total_quantity'],
            "Total" => $row['total_amount'],
            "Comision Club Cupón %" => $row['deal_commission_percentage'],
            "Comision Club Cupón $" => $row['invoiced_amount'],
            "Retencion Fondo De Garantia %" => $row['guarantee_fund_percentage'],
            "Retencion Fondo De Garantia $" => $row['guarantee_fund_amount'],
            "Total Liquidacion" => $row['liquidation_amount']
        );
    }

    function findAllByCompanyIdAndCalendarIdAsExportRows(&$Model, $companyId, $calendarId) {
        $rows = $this->findAllByCompanyIdAndCalendarIdAsRows($Model, $companyId, $calendarId);
        $exportRows = array();
        $i = 0;
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $exportRows[$i] = $this->newExportRow($row);
                $i++;
            }
        }
        return $exportRows;
    }

}
