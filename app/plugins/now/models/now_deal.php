<?php

class NowDeal extends NowAppModel {

    const STATUS_OPEN = 2;
    const STATUS_TIPPED = 5;

    var $name = 'NowDeal';
    var $alias = 'NowDeal';
    var $useTable = 'deals';
    var $actsAs = array(
        'Searchable',
        'Sluggable' => array(
            'label' => array(
                'name'
            ),
            'overwrite' => false
        ),
        'WhoDidIt' => array()
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'City',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealStatus' => array(
            'className' => 'DealStatus',
            'foreignKey' => 'deal_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Cluster' => array(
            'className' => 'Cluster',
            'foreignKey' => 'cluster_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealCategory' => array(
            'className' => 'DealCategory',
            'foreignKey' => 'deal_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        )
    );
    var $hasOne = array(
        'Attachment' => array(
            'className' => 'Attachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'Attachment.class =' => 'NowDeal'
            ),
            'dependent' => true
        )
    );
    var $hasMany = array(
        'PaymentOptionDeal' => array(
            'className' => 'PaymentOptionDeal',
            'foreignKey' => 'deal_id'
        ),
        'NowBranchesDeals' => array(
            'className' => 'now.NowBranchesDeals',
            'conditions' => array(
                'NowBranchesDeals.now_deal_id' => 'NowDeal.id'
            ),
            'foreignKey' => 'now_deal_id'
        ),
        'DealUser' => array(
            'className' => 'DealUser',
            'foreignKey' => 'deal_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Pin' => array(
            'className' => 'Pin',
            'foreignKey' => 'deal_id',
            'dependent' => true
        ),
        'AttachmentMultiple' => array(
            'className' => 'Attachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'AttachmentMultiple.class =' => 'DealMultiple'
            ),
            'dependent' => true
        ),
        'AnulledCoupon' => array(
            'className' => 'AnulledCoupon',
            'foreignKey' => 'deal_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'DealExternal' => array(
            'className' => 'DealExternal',
            'foreignKey' => 'deal_id',
            'dependent' => true
        )
    );
    var $hasAndBelongsToMany = array(
        'NowBranch' => array(
            'className' => 'Now.NowBranch',
            'joinTable' => 'branches_deals',
            'foreignKey' => 'now_deal_id',
            'associationForeignKey' => 'now_branch_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'user_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'descriptive_text' => array(
                'rule1' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'branches' => array(
                'rule1' => array(
                    'rule' => array(
                        'comparison',
                        '!=',
                        0
                    ),
                    'required' => true,
                    'on' => 'create',
                    'message' => __l('Required')
                )
            ),
            'name' => array(
                'rule2' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                ),
                'rule1' => array(
                    'rule' => array(
                        '_checkEmailFriendly'
                    ),
                    'message' => __l('Incompatible con las convenciones de mail: incluye TO, BCC, CC, CCO u otros')
                )
            ),
            'paymentOptionValue' => array(
                'rule1' => array(
                    'rule' => array(
                        '_checkPaymentOptions'
                    ),
                    'message' => __l('Debe elegir al menos un medio de pago para la oferta')
                )
            ),
            'custom_subject' => array(
                'rule' => array(
                    '_checkEmailFriendly'
                ),
                'message' => 'Incompatible con las convenciones de mail: incluye TO, BCC, CC, CCO u otros'
            ),
            'original_price' => array(
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'discounted_price' => array(
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'scheduled_deals_end_date' => array(
                'rule2' => array(
                    'rule' => '_isValidScheduledDealsEndDate',
                    'message' => __l('End date should be greater than start date'),
                    'allowEmpty' => true
                )
            ),
            'min_limit' => array(
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_checkMinLimitOnUpdate',
                        'on' => 'update'
                    ),
                    'message' => 'La cantidad mínima no puede ser superior a la ya fijada.'
                )
            ),
            'max_limit' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number'),
                    'last' => true
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0'),
                    'last' => true
                ),
                'rule3' => array(
                    'rule' => array(
                        '_checkMaxLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum limit should be greater than or equal to minimum limit'),
                    'last' => true
                )
            ),
            'buy_min_quantity_per_user' => array(
                'rule3' => array(
                    'rule' => array(
                        '_compareDealAndBuyMinLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Minimum buy limit should be less than or equal to deal maximum limit')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                )
            ),
            'buy_max_quantity_per_user' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number'),
                    'last' => true
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Should be greater than 0'),
                    'last' => true
                ),
                'rule3' => array(
                    'rule' => array(
                        '_compareDealAndBuyMaxLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum buy limit should be less than or equal to deal maximum limit'),
                    'last' => true
                ),
                'rule4' => array(
                    'rule' => array(
                        '_checkMaxQuantityLimt'
                    ),
                    'allowEmpty' => true,
                    'message' => __l('Maximum limit should be greater than or equal to minimum limit'),
                    'last' => true
                ),
                'rule5' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => 'Debe tener un valor numérico',
                    'last' => true
                )
            ),
            'city_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'company_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'deal_status_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'quantity' => array(
                'rule5' => array(
                    'rule' => array(
                        '_isEligibleMaximumQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Quantity is more than the maximum quantity.')
                ),
                'rule4' => array(
                    'rule' => array(
                        '_isEligibleQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('You can\'t buy this quantity.')
                ),
                'rule3' => array(
                    'rule' => array(
                        '_isEligibleMinimumQuantity'
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Quantity is less than the minimum quantity.')
                ),
                'rule2' => array(
                    'rule' => array(
                        'comparison',
                        '>',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Should be a number')
                )
            ),
            'gift_email' => array(
                'rule2' => array(
                    'rule' => 'email',
                    'allowEmpty' => false,
                    'message' => __l('Must be a valid email')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'confirm_gift_email' => array(
                'rule1' => array(
                    'rule' => array(
                        'email',
                        'confirm_email'
                    ),
                    'message' => __l('El email y el email de confirmación deben coincidir, por favor intentá nuevamente')
                )
            ),
            'gift_to' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'gift_dni' => array(
                'rule4' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Escribir DNI sin puntos separadores, sólo números.')
                )
            ),
            'description' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'start_date' => array(
                'rule2' => array(
                    'rule' => '_isValidStartDate',
                    'message' => __l('Start date should be greater than today'),
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'end_date' => array(
                'rule2' => array(
                    'rule' => '_isValidEndDate',
                    'message' => __l('End date should be greater than start date'),
                    'allowEmpty' => false
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'coupon_duration' => array(
                'rule2' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => 'Ingresar por cuantos dias es valido el cupon desde su compra'
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Should be a number')
                )
            ),
            'discount_amount' => array(
                'rule2' => array(
                    'rule' => array(
                        '_checkDiscountAmount',
                        'original_price',
                        'discount_amount'
                    ),
                    'message' => __l('Discount amouont should be less than original amount.')
                ),
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'commission_percentage' => array(
                'rule4' => array(
                    'rule' => array(
                        'comparison',
                        '<=',
                        100
                    ),
                    'allowEmpty' => false,
                    'message' => 'debe estar entre 0 y 100'
                ),
                'rule3' => array(
                    'rule' => array(
                        'comparison',
                        '>=',
                        0
                    ),
                    'allowEmpty' => false,
                    'message' => __l('Should be greater than 0')
                ),
                'rule2' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe tener un valor numérico'
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'message' => __l('Required')
                )
            ),
            'downpayment_percentage' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => false,
                    'message' => 'Debe tener un valor numerico'
                )
            ),
            'bonus_amount' => array(
                'rule1' => array(
                    'rule' => 'numeric',
                    'allowEmpty' => true,
                    'message' => __l('Should be a number')
                )
            ),
            'deal_category_id' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            )
        );
        $this->validateCreditCard = array(
            'firstName' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'lastName' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'creditCardNumber' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Should be numeric')
            ),
            'cvv2Number' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Should be numeric')
            ),
            'zip' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'address' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'city' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'state' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'country' => array(
                'rule' => 'notempty',
                'allowEmpty' => false,
                'message' => __l('Required')
            )
        );
        $this->filters = array(
            ConstDealStatus::PendingApproval => __l('Pending Approval'),
            ConstDealStatus::Upcoming => __l('Upcoming'),
            ConstDealStatus::Open => __l('Open'),
            ConstDealStatus::Closed => __l('Closed')
        );
    }

    function _checkCommissionAndBonus() {
        $commission = (int) ($this->data[$this->name]['commission_percentage']);
        $bonus_amnt = (int) ($this->data[$this->name]['bonus_amount']);
        return !(($commission == 0) && ($bonus_amnt == 0));
    }

    function _checkEmailFriendly($check) {
        App::import("Component", 'Email');
        $this->Email = & new EmailComponent();
        foreach ($check as $value)
            if ($this->Email->__strip($value) != $value)
                return false;
        return true;
    }

    function _checkPaymentOptions($check) {
        return is_array($check['paymentOptionValue']) && count($check['paymentOptionValue']) > 0;
    }

    function _checkMinLimitOnUpdate($check) {
        $tmp = $this->find('first', array(
            'conditions' => array(
                'id' => $this->id,
                'deal_status_id' => array(
                    ConstDealStatus::Open,
                    ConstDealStatus::Tipped
                )
            ),
            'fields' => array(
                'min_limit'
            ),
            'recursive' => - 1
        ));
        if (!$tmp)
            return true;
        $min_limit = $tmp[$this->name]['min_limit'];
        return $min_limit >= $check['min_limit'];
    }

    function _checkDiscountAmount() {
        if ($this->data[$this->name]['discount_amount'] > $this->data[$this->name]['original_price']) {
            return false;
        }
        return true;
    }

    function _isValidExpiryDate() {
        if (strtotime($this->data[$this->name]['coupon_expiry_date']) > strtotime($this->data[$this->name]['end_date'])) {
            return true;
        }
        return false;
    }

    function _isValidStartDate() {
        if (!(strtotime($this->data[$this->name]['start_date']) <= strtotime(date('Y-m-d 00:00:00'))) || !Configure::read('env')) {
            return true;
        }
        return false;
    }

    function _isValidEndDate() {
        if (!empty($this->data[$this->name]['end_date']) && !empty($this->data[$this->name]['start_date']) && strtotime($this->data[$this->name]['end_date']) > strtotime($this->data[$this->name]['start_date'])) {
            return true;
        }
        return false;
    }

    function _isValidScheduledDealsEndDate() {
        $s = date("Y-m-d", strtotime($this->data[$this->name]['start_date']));
        $e = date("Y-m-d", strtotime($this->data[$this->name]['scheduled_deals_end_date']));
        $dias = $this->data[$this->name]['scheduled_deals'];
        $initial_date = false;
        $r = array();
        while ($s <= $e) {
            foreach ($dias as $dia) {
                $date_string = date("Y-m-d", strtotime($s . " first " . $dia . " this week"));
                if ($date_string >= date("Y-m-d", strtotime($s)) && $date_string <= $e) {
                    $r[] = $date_string;
                }
            }
            $s = date("Y-m-d", strtotime($s . " next week"));
            if (!empty($r)) {
                break;
            }
        }
        asort($r);
        $s = !empty($r) ? current($r) : $s;
        if (!empty($e) && !empty($s) && strtotime($e) >= strtotime($s)) {
            return true;
        }
        return false;
    }

    function isEligibleForBuy($deal_id, $user_id, $buy_max_quantity_per_user) {
        $deals_count = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.deal_id' => $deal_id,
                'DealUser.user_id' => $user_id
            ),
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        if (empty($buy_max_quantity_per_user) || empty($deals_count) || $deals_count[0]['total_count'] < $buy_max_quantity_per_user) {
            return true;
        }
        return false;
    }

    function _isEligibleMaximumQuantity() {
        $deals_count = $this->_countUserBoughtDeals();
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.buy_max_quantity_per_user'
            ),
            'recursive' => - 1
        ));
        $newTotal = (!empty($deals_count[0]['total_count']) ? $deals_count[0]['total_count'] : 0) + $this->data[$this->name]['quantity'];
        if (empty($deal[$this->name]['buy_max_quantity_per_user']) || $newTotal <= $deal[$this->name]['buy_max_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _isEligibleMinimumQuantity() {
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.buy_min_quantity_per_user',
                'Deal.deal_user_count',
                'Deal.max_limit'
            ),
            'recursive' => - 1
        ));
        if ($deal[$this->name]['buy_min_quantity_per_user'] > 1) {
            $deals_count = $this->_countUserBoughtDeals();
            $boughtTotal = (!empty($deals_count[0]['total_count']) ? $deals_count[0]['total_count'] : 0) + $this->data[$this->name]['quantity'];
            if ($boughtTotal >= $deal[$this->name]['buy_min_quantity_per_user'] || (!empty($deal[$this->name]['max_limit']) && ($deal[$this->name]['max_limit'] - $deal[$this->name]['deal_user_count']) < $deal[$this->name]['buy_min_quantity_per_user'])) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }

    function _countUserBoughtDeals() {
        $deals_count = $this->DealUser->find('first', array(
            'conditions' => array(
                'DealUser.deal_id' => $this->data[$this->name]['deal_id'],
                'DealUser.user_id' => $this->data[$this->name]['user_id']
            ),
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => array(
                'DealUser.user_id'
            ),
            'recursive' => - 1
        ));
        return $deals_count;
    }

    function _isEligibleQuantity() {
        $deal = $this->find('first', array(
            'conditions' => array(
                'Deal.id' => $this->data[$this->name]['deal_id']
            ),
            'fields' => array(
                'Deal.deal_user_count',
                'Deal.max_limit'
            ),
            'recursive' => - 1
        ));
        $newTotal = $deal[$this->name]['deal_user_count'] + $this->data[$this->name]['quantity'];
        if ($deal[$this->name]['max_limit'] <= 0 || $newTotal <= $deal[$this->name]['max_limit']) {
            return true;
        }
        if (preg_match("/./", $deal[$this->name]['max_limit'])) {
            return false;
        }
        return false;
    }

    function _compareDealAndBuyMinLimt() {
        if (empty($this->data[$this->name]['max_limit']) || $this->data[$this->name]['max_limit'] >= $this->data[$this->name]['buy_min_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _compareDealAndBuyMaxLimt() {
        if (empty($this->data[$this->name]['max_limit']) || $this->data[$this->name]['max_limit'] >= $this->data[$this->name]['buy_max_quantity_per_user']) {
            return true;
        }
        return false;
    }

    function _checkMaxLimt() {
        if ($this->data[$this->name]['max_limit'] >= $this->data[$this->name]['min_limit']) {
            return true;
        }
        return false;
    }

    function _checkMaxQuantityLimt() {
        if ($this->data[$this->name]['buy_max_quantity_per_user'] >= $this->data[$this->name]['buy_min_quantity_per_user']) {
            return true;
        }
        return false;
    }

    public function findDealById($id, $rec = - 1) {
        $nowdeal = $this->find('first', array(
            'fields' => array(
                '*',
                '`deal_external_count` as `total_coupons`'
            ),
            'conditions' => array(
                'NowDeal.id = ' => $id
            ),
            'recursive' => $rec
        ));
        return $nowdeal;
    }

    public function findDealBy($conditions) {
        $nowdeal = $this->find('all', array(
            'fields' => array(
                'id',
                'name',
                'created',
                'modified',
                'created_by',
                'modified_by',
                'deal_status_id'
            ),
            'contain' => array(
                'Company' => array(
                    'fields' => array(
                        'Company.name'
                    )
                ),
                'City' => array(
                    'fields' => array(
                        'City.name'
                    )
                )
            ),
            'conditions' => $conditions,
            'recursive' => 0
        ));
        return $nowdeal;
    }

    function beforeSave() {
        if (empty($this->data[$this->name]['campaign_code'])) {
            $this->data[$this->name]['campaign_code'] = substr(String::uuid(), -12);
            $this->data[$this->name]['campaign_code_type'] = 'G';
        }
        $this->data[$this->name]['is_now'] = true;
        $this->data[$this->name]['pay_by_redeemed'] = 1;
        return true;
    }

    function _updateDealBitlyURL($deal_slug, $city = '') {
        $bitly_short_url_prefix = Configure::read('static_domain_for_mails') . '/' . $this->name . '/' . $deal_slug;
        $bitly_short_url_prefix = $this->getBitlyUrl($bitly_short_url_prefix);
        $this->updateAll(array(
            'NowDeal.bitly_short_url_subdomain' => '\'' . $bitly_short_url_prefix . '\'',
            'NowDeal.bitly_short_url_prefix' => '\'' . $bitly_short_url_prefix . '\''
                ), array(
            'NowDeal.slug' => $deal_slug
        ));
    }

    function getBitlyUrl($url = null) {
        if (is_null($url)) {
            return false;
        }
        $ret = file_get_contents('http://api.bit.ly/shorten?version=2.0.1&longUrl=' . $url . '&login=' . Configure::read('bitly.username') . '&apiKey=' . Configure::read('bitly.api_key'));
        $result = json_decode($ret, true);
        if ($result['errorCode'] == '0' && $result['statusCode'] == 'OK') {
            return $result['results'][$url]['shortUrl'];
        } else {
            return $url;
        }
    }

    function getImageUrl($model, $attachment, $options) {
        $default_options = array(
            'dimension' => 'big_thumb',
            'class' => '',
            'alt' => 'alt',
            'title' => 'title',
            'type' => 'jpg'
        );
        $options = array_merge($default_options, $options);
        $image_hash = $options['dimension'] . '/' . $model . '/' . $attachment['id'] . '.' . md5(Configure::read('Security.salt') . $model . $attachment['id'] . $options['type'] . $options['dimension'] . Configure::read('site.name')) . '.' . $options['type'];
        return Configure::read('static_domain_for_mails') . '/img/' . $image_hash;
    }

    function countNowDealsByCompany($company_id) {
        return $this->find('count', array(
                    'conditions' => array(
                        'company_id' => $company_id
                    )
        ));
    }

    function findNowDealsByIds($ids) {
        $default_options = array(
            'NowDeal.is_now' => 1
        );
        $filter_conditions = array(
            'NowDeal.id' => $ids
        );
        $conditions = array_merge($default_options, $filter_conditions);
        $nowdeals = $this->find('all', array(
            'fields' => array(
                'id',
                'name',
                'created',
                'modified',
                'created_by',
                'modified_by',
                'deal_status_id'
            ),
            'contain' => array(
                'Company' => array(
                    'fields' => array(
                        'Company.name'
                    )
                ),
                'City' => array(
                    'fields' => array(
                        'City.name'
                    )
                )
            ),
            'conditions' => $conditions,
            'recursive' => 0
        ));
        return $nowdeals;
    }

    function findNowDealsByStatus($filter) {
        $default_options = array(
            'NowDeal.is_now' => 1
        );
        $filter_conditions = array(
            'NowDeal.deal_status_id' => $filter
        );
        if ($filter == ConstDealStatus::PendingApproval) {
            $filter_conditions['NowDeal.created <'] = date('Y-m-d H:i:s', strtotime('-0 minute'));
        }
        $conditions = array_merge($default_options, $filter_conditions);
        $nowdeals = $this->find('all', array(
            'fields' => array(
                'id',
                'name',
                'created',
                'modified',
                'created_by',
                'modified_by',
                'deal_status_id'
            ),
            'contain' => array(
                'Company' => array(
                    'fields' => array(
                        'Company.name'
                    )
                ),
                'City' => array(
                    'fields' => array(
                        'City.name'
                    )
                )
            ),
            'conditions' => $conditions,
            'recursive' => 0
        ));
        return $nowdeals;
    }

    function indexData() {
        if (!$this->data) {
            return false;
        }
        if (empty($this->data['NowDeal']['id']) && !empty($this->id)) {
            $this->data['NowDeal']['id'] = $this->id;
        }
        if (!empty($this->data['NowDeal']['id']) && (empty($this->data['NowDeal']['name']) || empty($this->data['Company']['name']))) {
            $data = $this->find('all', array(
                'conditions' => array(
                    'NowDeal.id' => $this->data['NowDeal']['id']
                ),
                'fields' => array(
                    'NowDeal.id',
                    'NowDeal.name'
                ),
                'contain' => array(
                    'Company' => array(
                        'fields' => array(
                            'Company.name'
                        )
                    )
                ),
                'recursive' => 0
            ));
            if ($data == NULL) {
                return false;
            }
            $deal_id = $data[0]['NowDeal']['id'];
            $deal_name = $data[0]['NowDeal']['name'];
            $company = $data[0]['Company']['name'];
            return $deal_id . ' ' . $deal_name . ' ' . $company;
        } else {
            return $this->data['NowDeal']['id'] . ' ' . $this->data['NowDeal']['name'] . ' ' . $this->data['Company']['name'];
        }
    }

    public function hasExpired($deal) {
        $now = strtotime(date("Y-m-d"));
        $dateDealExpired = strtotime($deal['NowDeal']['end_date']);
        return $dateDealExpired < $now;
    }

    public function isActive($deal) {
        return in_array($deal['NowDeal']['deal_status_id'], array(
            self::STATUS_OPEN,
            self::STATUS_TIPPED
        ));
    }

}
