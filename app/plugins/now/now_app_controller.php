<?php

App::import('Vendor', 'web.commons');
App::import('Component', 'web.WebViewDataCreator');
if (Configure::read('app.rDebug.enable')) {
    App::import('Vendor', 'web.ref');
}

class NowAppController extends AppController {

    var $uses = array(
        'User',
        'NowDeal'
    );

    public function __construct() {
        parent::__construct();
        App::import('vendor', 'Utility', array(
            'file' => 'Utility/ApiLogger.php'
        ));
        App::import('vendor', 'Utility', array(
            'file' => 'Utility/ApiXml.php'
        ));
    }

    public function beforeRender() {
        $this->set('user_type_id', $this->Auth->user('user_type_id'));
        parent::beforeRender();
        $layout_exceptions = array(
            'now_deal_preview',
            'add',
            'detail',
            'editar',
            'landing',
            'faq',
            'landingus',
            'download_my_deals',
            'download_my_cupons',
            'index'
        );
        if (!in_array($this->action, $layout_exceptions)) {
            if ($this->Cookie->read('city_slug')) {
                $this->set('slug', $this->Cookie->read('city_slug'));
            } else {
                $this->set('slug', Configure::read('web.default.citySlug'));
            }
            $this->layout = 'now';
        }
        if ($this->layout == 'now_web') {
            $this->generateViewParams();
        }
        if ($this->Auth->user('id') && $this->Auth->user('user_type_id') != ConstUserTypes::User) {
            $this->set('login', $this->User->findById($this->Auth->user('id')));
            $company = $this->User->Company->find('all', array(
                'conditions' => array(
                    'user_id' => $this->Auth->user('id')
                ),
                'recursive' => 0
            ));
            $this->set('company', $company);
            $this->set('now_deal_count', $this->NowDeal->countNowDealsByCompany($company[0]['Company']['id']));
            if ($this->User->isCompanyCandidate($this->Auth->user())) {
                $this->set('has_change_password', false);
                $this->set('current_step', 3);
                $this->set('show_steps', true);
                $this->set('showLiquidationsHeader', false);
            } else {
                $this->set('has_change_password', true);
                $this->set('current_step', false);
                $this->set('showLiquidationsHeader', true);
            }
        }
    }

    private function setTrackingRegisterOk() {
        $this->viewParams['registerOk'] = false;
        if (isset($_GET['popup'])) {
            if ($_GET['registerOk'] == '1') {
                $this->viewParams['registerOk'] = true;
            }
            if ($_GET['registerOk'] == '0') {
                $this->viewParams['registerOk'] = false;
            }
        }
    }

    protected function generateViewParams() {
        if (empty($this->viewParams['viewType'])) {
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_NOW_CC;
        }
        $this->viewParams['lastGeoCitySlug'] = $this->Cookie->read('geo_city_slug');
        $this->viewParams['categories'] = Configure::read('NowPlugin.home.categories');
        $this->viewParams['userEmail'] = $this->Auth->user('email');
        $this->viewParams['userType'] = $this->Auth->user('user_type_id');
        $this->viewParams['userID'] = $this->Auth->user('id');
        $this->viewParams['url'] = $this->params['url']['url'];
        $this->setTrackingRegisterOk();
        $this->WebViewDataCreator = new WebViewDataCreatorComponent();
        $Data = $this->WebViewDataCreator->createData($this->viewParams);
        $this->set('data', json_encode($Data));
    }

    function checkarAuth() {
        if (!$this->Auth->user('id')) {
            $cur_page = $this->params['controller'] . '/' . $this->params['action'];
            $this->Session->setFlash(__l('Authorization Required'));
            $is_admin = false;
            $this->redirect(array(
                'plugin' => '',
                'controller' => 'users',
                'action' => 'login',
                'admin' => $is_admin,
                '?f=' . $this->params['url']['url']
            ));
        }
    }

}
