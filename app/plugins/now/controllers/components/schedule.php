<?php

if (!function_exists('array_replace_recursive')) {

    function recursar($array, $array1) {
        foreach ($array1 as $key => $value) {
            if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key]))) {
                $array[$key] = array();
            }
            if (is_array($value)) {
                $value = recursar($array[$key], $value);
            }
            $array[$key] = $value;
        }
        return $array;
    }

    function array_replace_recursive($array, $array1) {
        $args = func_get_args();
        $array = $args[0];
        if (!is_array($array)) {
            return $array;
        }
        for ($i = 1; $i < count($args); $i++) {
            if (is_array($args[$i])) {
                $array = recursar($array, $args[$i]);
            }
        }
        return $array;
    }

}

class ScheduleComponent extends Object {

    function cloneNowDeal($deal_id, $fecha) {
        App::import('Model', 'now.NowDeal');
        App::import('Model', 'Preredemption');
        $deal = $this->getDeal($deal_id);
        if ($deal) {
            $cloned_deal = array();
            $cloned_deal['NowDeal'] = $this->sanitizeNowDeal($deal['NowDeal'], $fecha);
            $deal['NowDeal'] = $cloned_deal['NowDeal'];
            $this->remove_Key($deal);
            $preredemption = new Preredemption;
            $now_deal = new NowDeal;
            $now_deal->create();
            $now_deal->Behaviors->detach('Searchable');
            $now_deal->Behaviors->attach('Searchable', array(
                'rebuildOnUpdate' => false
            ));
            $now_deal->data = $cloned_deal;
            unset($now_deal->validate['branches']);
            if (!$now_deal->validates()) {
                
            } else {
                $now_deal->save($now_deal->data);
                $last_insert_id = $now_deal->getLastInsertID();
                $replacements = array(
                    'Attachment' => array(
                        'foreign_id' => $last_insert_id,
                        'dir' => 'NowDeal/' . $last_insert_id
                    ),
                    'NowDeal' => array(
                        'parent_deal_id' => $last_insert_id
                    )
                );
                $basket = array_replace_recursive($deal, $replacements);
                foreach ($deal['PaymentOptionDeal'] as $k => $po_deal) {
                    $basket['PaymentOptionDeal'][$k] = array_replace_recursive($po_deal, array(
                        'deal_id' => $last_insert_id
                    ));
                }
                foreach ($deal['NowBranchesDeals'] as $k => $branch) {
                    $basket['NowBranchesDeals'][$k] = array_replace_recursive($branch, array(
                        'now_deal_id' => $last_insert_id
                    ));
                }
                $this->remove_Key($basket);
                $this->cloneAttachment($basket['Attachment'], $deal_id);
                if (!$this->cloneBranchesDeals($basket['NowBranchesDeals'], $now_deal->data, $fecha)) {
                    Debugger::log("cloneBranchesDeals => devolvio falso => " . serialize($basket['NowBranchesDeals']), LOG_DEBUG);
                }
                if (!$this->clonePaymentOptionDeal($basket['PaymentOptionDeal'])) {
                    Debugger::log("clonePaymentOptionDeal => devolvio falso => " . serialize($basket['PaymentOptionDeal']), LOG_DEBUG);
                }
                Debugger::log(sprintf("antes de createPreRedemptions %s, %s", $last_insert_id, print_r($basket, 1)), LOG_DEBUG);
                if (!$preredemption->createPreRedemptions($last_insert_id, $basket['NowDeal']['max_limit'])) {
                    Debugger::log(sprintf(__METHOD__ . " No se pudo crear registros en preredemptions. Dealid = %s, max_limit = %s", $last_insert_id, $basket['NowDeal']['max_limit']), LOG_DEBUG);
                }
            }
        } else {
            Debugger::log(__METHOD__ . " => vacio deal_id=" . $deal_id, LOG_DEBUG);
        }
    }

    private function getDeal($deal_id) {
        $nowdeal = new NowDeal;
        return $nowdeal->find('first', array(
                    'conditions' => array(
                        'NowDeal.id' => $deal_id,
                        'NOT' => array(
                            'NowDeal.deal_status_id' => array(
                                ConstDealStatus::Draft,
                                ConstDealStatus::Rejected,
                                ConstDealStatus::PendingApproval
                            )
                        )
                    ),
                    'contain' => array(
                        'Attachment',
                        'NowBranchesDeals',
                        'PaymentOptionDeal',
                        'Company'
                    )
        ));
    }

    private static function remove_Key(&$a) {
        if (is_array($a)) {
            unset($a['id']);
            array_walk($a, __METHOD__);
        }
    }

    private static function replace_deal_id($deal_id, $keys_to_be_replaced, &$a) {
        if (is_array($a)) {
            foreach ($keys_to_be_replaced as $the_key) {
                if (array_key_exists($the_key, $a)) {
                    $a[$the_key] = $deal_id;
                }
            }
            array_walk($a, __METHOD__);
        }
    }

    private function sanitizeNowDeal($dealdata, $fecha) {
        unset($dealdata['parent_deal_id'], $dealdata['created'], $dealdata['modified'], $dealdata['deal_user_count'], $dealdata['deal_external_count'], $dealdata['payment_failed_count'], $dealdata['private_note'], $dealdata['deal_tipped_time'], $dealdata['closure_modality'], $dealdata['deferred_payment'], $dealdata['first_closure_percentage'], $dealdata['is_subscription_mail_sent'], $dealdata['total_commission_amount'], $dealdata['total_purchased_amount'], $dealdata['net_profit'], $dealdata['id']);
        $dealdata['private_note'] = false;
        $diff_hr = $this->getDateDiffFromNow($dealdata['start_date'], $dealdata['end_date']);
        list($ymd, $his) = explode(" ", $dealdata['start_date']);
        $dealdata['start_date'] = $fecha . " " . $his;
        $dealdata['end_date'] = date("Y-m-d H:i:s", strtotime($dealdata['start_date'] . " +" . $diff_hr . " hours"));
        $dealdata['coupon_expiry_date'] = $dealdata['end_date'];
        $dealdata['deal_status_id'] = ConstDealStatus::Upcoming;
        return $dealdata;
    }

    private function cloneAttachment($datos, $old_deal_id) {
        App::import("Model", "Attachment");
        $attachment = new Attachment;
        $attachment->Behaviors->detach('ImageUpload');
        $attachment->create();
        $attachment->set($datos);
        if (!$attachment->validates()) {
            Debugger::log(__METHOD__ . " No valido => " . serialize($datos), LOG_DEBUG);
        }
        if ($attachment->save(array(
                    'Attachment' => $datos
                        ), false)) {
            Debugger::log('MKDIR ' . $datos['dir']);
            if (!is_dir(APP_PATH . '/media/' . $datos['dir'])) {
                mkdir(APP_PATH . '/media/' . $datos['dir']);
            }
            copy(APP_PATH . '/media/NowDeal/' . $old_deal_id . '/' . $datos['filename'], APP_PATH . '/media/' . $datos['dir'] . '/' . $datos['filename']);
        }
    }

    private function cloneBranchesDeals($datos, $data, $fecha) {
        App::import("Model", "now.NowBranchesDeals");
        foreach ($datos as $k => $branch_deal) {
            $datos[$k]['now_deal_status_id'] = ConstDealStatus::Upcoming;
            $diff_hr = $this->getDateDiffFromNow($datos[$k]['ts_inicio'], $datos[$k]['ts_fin']);
            list($ymd, $his) = explode(" ", $datos[$k]['ts_inicio']);
            $datos[$k]['ts_inicio'] = $fecha . " " . $his;
            $datos[$k]['ts_fin'] = date("Y-m-d H:i:s", strtotime($datos[$k]['ts_inicio'] . " +" . $diff_hr . " hours"));
            $datos[$k]['deleted'] = 0;
        }
        $nb_deals = new NowBranchesDeals;
        $nb_deals->create();
        return $nb_deals->saveAll($datos, array(
                    'validate' => 'all'
        ));
    }

    function getDateDiffFromNow($s, $e) {
        $unixOriginalDate = strtotime($s);
        $unixNowDate = strtotime($e);
        $difference = $unixNowDate - $unixOriginalDate;
        $hours = (int) ($difference / 3600);
        return $hours;
    }

    private function clonePaymentOptionDeal($datos) {
        App::import("Model", "PaymentOptionDeal");
        $po_deal = new PaymentOptionDeal;
        $po_deal->create();
        return $po_deal->saveAll($datos, array(
                    'validate' => 'all'
        ));
    }

}
