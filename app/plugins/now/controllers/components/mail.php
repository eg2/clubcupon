<?php

class MailComponent extends Object {

    function sendActivationMail($array) {
        $user_email = $array['user_email'];
        $user_id = $array['user_id'];
        $hash = $array['hash'];
        $params = $array['params'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        App::import('Model', 'User');
        $this->EmailTemplate = & new EmailTemplate;
        $User = & new User;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $city = isset($params['named']['city']) ? $params['named']['city'] : '';
        $user = $User->find('first', array(
            'conditions' => array(
                'User.email' => $user_email
            ),
            'recursive' => - 1
        ));
        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##USERNAME##' => $user['User']['username'],
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ACTIVATION_URL##' => Router::url(array(
                'controller' => 'users',
                'action' => 'activation',
                'city' => $city,
                $user_id,
                $hash
                    ), true),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        if ($user['User']['user_type_id'] == ConstUserTypes::Company) {
            $email = $this->EmailTemplate->selectTemplate('Activation Request Candidate');
        } else {
            $email = $this->EmailTemplate->selectTemplate('Activation Request');
        }
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user_email;
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendMailCandidateInfoUpdate($user_id) {
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        App::import('Model', 'User');
        $this->EmailTemplate = & new EmailTemplate;
        $User = & new User;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $user = $User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'recursive' => - 1
        ));
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        $email = $this->EmailTemplate->selectTemplate('Candidate Info Update');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendCandidateAproveOrRejectedMail($array) {
        $user = $array['user'];
        $action = $array['action'];
        $message = $array['message'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##MESSAGE##' => $message,
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##PUBLISH_NOWDEALS_LINK##' => Router::url(array(
                'plugin' => 'now',
                'admin' => false,
                'controller' => 'now_deals',
                'action' => 'my_deals'
                    ), true)
        );
        if ($action) {
            $email = $this->EmailTemplate->selectTemplate('Candidate reject');
        } else {
            $email = $this->EmailTemplate->selectTemplate('Candidate acepted');
        }
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendCandidateDraftMail($array) {
        $user = $array['user'];
        $message = $array['message'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##MESSAGE##' => $message
        );
        $email = $this->EmailTemplate->selectTemplate('Candidate draft');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendCandidateEditMail($array) {
        $user = $array['user'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/'
        );
        $email = $this->EmailTemplate->selectTemplate('Candidate edit');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendDealApprovalMail($array) {
        $user = $array['user'];
        $deal_name = $array['deal_name'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##DEAL_NAME##' => $deal_name
        );
        $email = $this->EmailTemplate->selectTemplate('now deal approval');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendDealRejectionMail($array) {
        $user = $array['user'];
        $deal_name = $array['deal_name'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##DEAL_NAME##' => $deal_name
        );
        $email = $this->EmailTemplate->selectTemplate('now deal denial');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

    public function sendDealDraftMail($array) {
        $user = $array['user'];
        $deal_name = $array['deal_name'];
        App::import('Component', 'Email');
        App::import('Model', 'EmailTemplate');
        $this->EmailTemplate = & new EmailTemplate;
        $this->Email = & new EmailComponent;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $emailFindReplace = array(
            '##SITE_NAME##' => Configure::read('site.name'),
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
            '##DEAL_NAME##' => $deal_name
        );
        $email = $this->EmailTemplate->selectTemplate('now deal draft');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        return $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }

}
