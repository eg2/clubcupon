<?php

App::import('Behavior', 'now.NowCalendar');
App::import('Behavior', 'now.NowItem');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingCalendar');
App::import('Model', 'accounting.AccountingItem');

class NowLiquidateTabServiceComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct('now');
        $this->AccountingCalendar = new AccountingCalendar();
        $this->AccountingCalendar->Behaviors->attach('NowCalendar');
        $this->AccountingItem = new AccountingItem();
        $this->AccountingItem->Behaviors->attach('NowItem');
    }

    public function getAccountingCalendar() {
        return $this->AccountingCalendar;
    }

    public function getAccountingItem() {
        return $this->AccountingItem;
    }

}
