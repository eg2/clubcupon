<?php

class NowRegistersController extends NowAppController {

    var $name = 'NowRegisters';
    var $helpers = array(
        'adminpagos'
    );
    var $components = array(
        'now.Mail'
    );
    var $uses = array(
        'Now.NowDeal',
        'Company',
        'Now.CompanyCandidate',
        'DealUser',
        'User',
        'UserProfile',
        'DealExternal',
        'DealCategory',
        'Subscription',
        'Attachment',
        'AttachmentMultiple',
        'EmailTemplate',
        'Country',
        'State',
        'Neighbourhood',
        'AccountingMaxPaymentDateGrouped'
    );

    public function beforeFilter() {
        AppModel::setDefaultDbConnection('master');
        $securityDisabled = array(
            'register_company'
        );
        if (in_array($this->action, $securityDisabled)) {
            $this->Security->validatePost = false;
        }
        parent::beforeFilter();
    }

    function beforeRender() {
        $this->__setNextLiquidation();
        parent::beforeRender();
    }

    function __setNextLiquidation() {
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $conditionsForFindByCompanyId['conditions']['company_id'] = $company['Company']['id'];
        $nextLiq = $this->AccountingMaxPaymentDateGrouped->__findFirstByCompanyIdGroupedByMaxPaymentDate($conditionsForFindByCompanyId);
        $this->set('nextLiquidationDate', $nextLiq['AccountingMaxPaymentDateGrouped']['max_payment_date']);
        $this->set('nextLiquidationValue', $nextLiq['AccountingMaxPaymentDateGrouped']['liquidate_total_amount']);
    }

    function _myDealsNowRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_deals',
                    'action' => 'my_deals'
        ));
    }

    function _addBranchRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_branches',
                    'action' => 'add'
        ));
    }

    function _indexForCompanyRedirect() {
        return $this->redirect(array(
                    'plugin' => '',
                    'controller' => 'deal_users',
                    'action' => 'index_for_company'
        ));
    }

    function _myCompanyNowRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_registers',
                    'action' => 'my_company'
        ));
    }

    function _homeRedirect() {
        return $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index'
        ));
    }

    function empresaregistrada() {
        $this->log('empresaregistrada');
        $this->autoRender = false;
        if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
            $this->redirect(Router::url('/', true));
        } elseif ($this->User->isCompany($this->Auth->user())) {
            $company = $this->Company->findByUser($this->Auth->user());
            if (Configure::read('now.is_enabled') && $this->Company->isNow($company)) {
                if ($this->Company->isNowWithBranches($company)) {
                    return $this->_myDealsNowRedirect();
                } elseif ($this->Company->isNowWithoutBranches($company)) {
                    return $this->_addBranchRedirect();
                }
            } else {
                return $this->_indexForCompanyRedirect();
            }
        } elseif ($this->User->isCompanyCandidate($this->Auth->user())) {
            return $this->_myCompanyNowRedirect();
        } elseif ($this->Auth->user('user_type_id') == 7) {
            $this->redirect(Router::url('/', true));
        }
    }

    function landingus() {
        $this->layout = 'now_web';
        $this->set('certificaPath', 'ClubCupon/ya/landing');
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_NOW_LANDING;
    }

    function landing() {
        $nueva_empresa = false;
        $empresa_registrada = false;
        $nueva_empresa_url = '#';
        $empresa_registrada_url = '#';
        $this->layout = 'now_landing';
        if ($this->Auth->user()) {
            $this->log('logueado');
            if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
                $this->log('comprador');
                $nueva_empresa = true;
                $nueva_empresa_url = '/users/registernow';
            } elseif ($this->User->isCompany($this->Auth->user())) {
                $this->log('empresa del tipo ...');
                $company = $this->Company->findByUser($this->Auth->user());
                if ($this->Company->isNow($company)) {
                    $this->log('cc-now');
                    $empresa_registrada = true;
                    $empresa_registrada_url = '/now/now_registers/empresaregistrada';
                } else {
                    $this->log('solo empresa');
                    $nueva_empresa = true;
                    $nueva_empresa_url = '/deal_users/index_for_company';
                }
            } elseif ($this->User->isCompanyCandidate($this->Auth->user())) {
                $this->log('CompanyCandidate');
                $empresa_registrada = true;
                $empresa_registrada_url = '/now/now_registers/empresaregistrada';
            } elseif ($this->Auth->user('user_type_id') == 7) {
                $this->log('vendedor');
                $nueva_empresa = true;
                $nueva_empresa_url = '/users/registernow';
            }
        } else {
            $this->log('no logueado');
            $nueva_empresa = true;
            $nueva_empresa_url = '/users/registernow';
            $empresa_registrada = true;
            $empresa_registrada_url = '/users/login';
        }
        $this->set('btn_visible', array(
            'nueva_empresa' => $nueva_empresa,
            'empresa_registrada' => $empresa_registrada
        ));
        $this->set('btn_url', array(
            'nueva_empresa_url' => $nueva_empresa_url,
            'empresa_registrada_url' => $empresa_registrada_url
        ));
        $this->layout = 'now_web';
    }

    function faq() {
        $this->layout = 'now_landing';
        $nueva_empresa = false;
        $empresa_registrada = false;
        $nueva_empresa_url = '#';
        $empresa_registrada_url = '#';
        $this->layout = 'now_landing';
        if ($this->Auth->user()) {
            $this->log('logueado');
            if ($this->Auth->user('user_type_id') == ConstUserTypes::User) {
                $this->log('comprador');
                $nueva_empresa = true;
                $nueva_empresa_url = '/users/registernow';
            } elseif ($this->User->isCompany($this->Auth->user())) {
                $this->log('empresa del tipo ...');
                $company = $this->Company->findByUser($this->Auth->user());
                if ($this->Company->isNow($company)) {
                    $this->log('cc-now');
                    $empresa_registrada = true;
                    $empresa_registrada_url = '/now/now_registers/empresaregistrada';
                } else {
                    $this->log('solo empresa');
                    $nueva_empresa = true;
                    $nueva_empresa_url = '/deal_users/index_for_company';
                }
            } elseif ($this->User->isCompanyCandidate($this->Auth->user())) {
                $this->log('CompanyCandidate');
                $empresa_registrada = true;
                $empresa_registrada_url = '/now/now_registers/empresaregistrada';
            } elseif ($this->Auth->user('user_type_id') == 7) {
                $this->log('vendedor');
                $nueva_empresa = true;
                $nueva_empresa_url = '/users/registernow';
            }
        } else {
            $this->log('no logueado');
            $nueva_empresa = true;
            $nueva_empresa_url = '/users/registernow';
            $empresa_registrada = true;
            $empresa_registrada_url = '/users/login';
        }
        $this->set('btn_visible', array(
            'nueva_empresa' => $nueva_empresa,
            'empresa_registrada' => $empresa_registrada
        ));
        $this->set('btn_url', array(
            'nueva_empresa_url' => $nueva_empresa_url,
            'empresa_registrada_url' => $empresa_registrada_url
        ));
        $this->layout = 'now_web';
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_NOW_FAQ;
    }

    function gotopublishya() {
        $this->autoRender = false;
    }

    function index() {
        if (!empty($this->data)) {
            $this->data['User']['username'] = uniqid('cc');
            $this->data['User']['is_email_confirmed'] = 1;
            $this->data['User']['is_active'] = 1;
            $this->data['UserProfile']['dni'] = 0;
            $this->Auth->logout();
            $this->User->create();
            $this->User->set($this->data);
            if ($this->User->validates() && $this->UserProfile->validates()) {
                $userType = $this->User->UserType->find('first', array(
                    'conditions' => array(
                        'UserType.name' => 'Empresa'
                    ),
                    'recursive' => 0
                ));
                $this->data['User']['user_type_id'] = $userType['UserType']['id'];
                $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                if ($this->User->save($this->data)) {
                    $this->data['UserProfile']['user_id'] = $this->User->id;
                    if ($this->UserProfile->save($this->data)) {
                        $this->data['User']['password'] = $this->Auth->password($this->data['User']['passwd']);
                        $this->Auth->login($this->data);
                        $this->redirect(Router::url(array(
                                    'plugin' => 'now',
                                    'controller' => 'now_registers',
                                    'action' => 'my_company'
                                        ), true));
                    } else {
                        $this->Session->setFlash('Error al crear el Profile del Usuario', 'default', null, 'error');
                    }
                } else {
                    $this->Session->setFlash('Error al crear el Usuario', 'default', null, 'error');
                }
            }
        }
    }

    private function verifyUser() {
        if (!$this->Auth->user('id')) {
            $this->Session->setFlash('Si ya estas Registrado por favor confirma tu mail antes de continuar!', 'default', null, 'error');
            $this->redirect(Router::url('/', true));
        }
        if ($this->Auth->user('user_type_id') != 3) {
            $this->Session->setFlash('No estas registrado como empresa!', 'default', null, 'error');
            $this->redirect(Router::url('/', true));
        }
    }

    function my_company() {
        $this->layout = 'now_web';
        $this->verifyUser();
        $user_id = $this->Auth->user('id');
        $this->data = $this->CompanyCandidate->findbyUserId($user_id);
        $this->set('is_now', true);
        if (is_null($this->data)) {
            $this->data = $this->Company->findbyUserId($user_id);
            $this->data['CompanyCandidate'] = $this->data['Company'];
            $this->data['CompanyCandidate']['state'] = ConstCandidateStatuses::Approved;
        }
        if (isset($this->data['Company']['id'])) {
            $this->set('numero_comercio', $this->data['Company']['id']);
            $company = $this->Company->findById($this->data['Company']['id']);
            $this->set('is_now', $this->Company->isNow($company));
        }
        $CompMediaDir = APP . 'media' . DIRECTORY_SEPARATOR . 'companies' . DIRECTORY_SEPARATOR;
        $upd_files = array();
        foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . 'extban_*') as $nombre_archivo) {
            $upd_files['extban'] = array(
                $nombre_archivo,
                date("F d Y H:i:s.", filemtime($nombre_archivo))
            );
        }
        foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . 'encfac_*') as $nombre_archivo) {
            $upd_files['encfac'] = array(
                $nombre_archivo,
                date("F d Y H:i:s.", filemtime($nombre_archivo))
            );
        }
        $countries = $this->Country->find('first', array(
            'conditions' => array(
                'name' => 'Argentina'
            )
        ));
        $fiscal_states = $states = $this->State->find('list', array(
            'conditions' => array(
                'country_id' => $countries['Country']['id']
            )
        ));
        $fiscal_cities = $cities = $this->City->find('list', array(
            'conditions' => array(
                'state_id' => array_shift(array_keys($fiscal_states)),
                'is_group' => 0,
                'is_selectable' => 1
            )
        ));
        $this->set(compact('countries', 'states', 'cities', 'fiscal_states', 'fiscal_cities'));
        $this->set('user_id', $user_id);
        $user_name = $this->Auth->user('username');
        $this->set('user_name', $user_name);
        $this->set('upd_files', $upd_files);
        $this->layout = 'now_web';
        $this->render('register_company');
    }

    function register_company() {
        $this->layout = 'now_web';
        
        if (!$this->Auth->user('id')) {
            $this->Session->setFlash('Si ya estas Registrado por favor confirma tu mail antes de continuar!', 'default', null, 'error');
            $this->redirect("/");
        }
        $this->set('is_now', true);
        
    
        if (!empty($this->data)) {
            if ($this->data['accion'] == "Continuar") {
                $action = 0;
            } elseif ($this->data['accion'] == "Guardar Borrador") {
                $action = 0;
            } elseif (substr($this->data['accion'], 0, 6) == "Enviar") {
                $action = 1;
                $this->data['CompanyCandidate']['url'] = strlen($this->data['CompanyCandidate']['url']) ? $this->data['CompanyCandidate']['url'] : "http://www.clubcupon.com.ar";
            } else {
                die("accion no reconicida!");
            }
            $this->set('action', $action);
            $user_id = $this->Auth->user('id');
            $this->set('user_id', $user_id);
            
            if ($user_id) {
                $ComCan = $this->CompanyCandidate->findbyUserId($user_id);
                $this->CompanyCandidate->id = $ComCan['CompanyCandidate']['id'];
            }
            if (is_null($this->CompanyCandidate->id) || $this->CompanyCandidate->id == false) {
                $this->CompanyCandidate->create();
            } elseif (!in_array($this->CompanyCandidate->field('state'), array(
                        0,
                        1
                    ))) {
                $this->Session->setFlash('No se puede editar los datos de tu empresa en su estado actual!', 'default', null, 'error');
                $this->redirect(Router::url(array(
                            'plugin' => 'now',
                            'controller' => 'now_registers',
                            'action' => 'my_company'
                                ), true));
                return;
            }
            $this->data['CompanyCandidate']['country_id'] = Configure::read('country.default');
            $this->data['CompanyCandidate']['persona'] = ConstCandidate::NowCandidateTipoPers;
            $this->data['CompanyCandidate']['declaracion_jurada_terceros'] = ConstCandidate::NowCandidateDecJurTer;
            $this->data['CompanyCandidate']['address1'] = $this->data['CompanyCandidate']['fiscal_address'];
            $userRow = $this->User->findById($this->data['CompanyCandidate']['user_id']);
            $this->data['CompanyCandidate']['email'] = $userRow['User']['email'];
            if (!empty($this->data['City']['name'])) {
                $this->data['CompanyCandidate']['city_id'] = !empty($this->data['City']['id']) ? $this->data['City']['id'] : $this->CompanyCandidate->City->findOrSaveAndGetId($this->data['City']['name']);
            }
            if (!empty($this->data['FiscalCity']['name'])) {
                $this->data['CompanyCandidate']['fiscal_city_id'] = !empty($this->data['FiscalCity']['id']) ? $this->data['FiscalCity']['id'] : $this->CompanyCandidate->City->findOrSaveAndGetId($this->data['FiscalCity']['name'], 'FiscalCity');
            }
            if (!empty($this->data['State']['name'])) {
                $this->data['CompanyCandidate']['state_id'] = !empty($this->data['State']['id']) ? $this->data['State']['id'] : $this->CompanyCandidate->State->findOrSaveAndGetId($this->data['State']['name']);
            }
            if (!empty($this->data['FiscalState']['name'])) {
                $this->data['CompanyCandidate']['fiscal_state_id'] = !empty($this->data['FiscalState']['id']) ? $this->data['FiscalState']['id'] : $this->CompanyCandidate->State->findOrSaveAndGetId($this->data['FiscalState']['name'], 'FiscalState');
            }
            $this->CompanyCandidate->set($this->data);
            $CompMediaDir = APP . 'media' . DIRECTORY_SEPARATOR . 'companies' . DIRECTORY_SEPARATOR;
            if (!is_dir($CompMediaDir)) {
                mkdir($CompMediaDir);
            }
            if (!is_dir($CompMediaDir . $user_id . DIRECTORY_SEPARATOR)) {
                mkdir($CompMediaDir . $user_id . DIRECTORY_SEPARATOR);
            }
            if (is_uploaded_file($this->data['CompanyCandidate']['extban']['tmp_name'])) {
                $fdata = pathinfo($this->data['CompanyCandidate']['extban']['name']);
                if (in_array(strtoupper($fdata['extension']), ConstCandidate::allowedFiles())) {
                    foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . "extban_*") as $nombre_archivo) {
                        unlink($nombre_archivo);
                    }
                    $fileData = fread(fopen($this->data['CompanyCandidate']['extban']['tmp_name'], "r"), $this->data['CompanyCandidate']['extban']['size']);
                    file_put_contents($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . "extban_" . $this->data['CompanyCandidate']['extban']['name'], $fileData);
                } else {
                    $extban_e = true;
                }
            }
            if (is_uploaded_file($this->data['CompanyCandidate']['encfac']['tmp_name'])) {
                $fdata = pathinfo($this->data['CompanyCandidate']['encfac']['name']);
                if (in_array(strtoupper($fdata['extension']), ConstCandidate::allowedFiles())) {
                    foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . "encfac_*") as $nombre_archivo) {
                        unlink($nombre_archivo);
                    }
                    $fileData = fread(fopen($this->data['CompanyCandidate']['encfac']['tmp_name'], "r"), $this->data['CompanyCandidate']['encfac']['size']);
                    file_put_contents($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . "encfac_" . $this->data['CompanyCandidate']['encfac']['name'], $fileData);
                } else {
                    $encfac_e = true;
                }
            }
            $this->data['CompanyCandidate']['message'] = null;
            $this->data['CompanyCandidate']['state'] = 0;
            if ($this->CompanyCandidate->save($this->data, array(
                        'validate' => false
                    ))) {
                $this->data['CompanyCandidate']['id'] = $this->CompanyCandidate->id;
                if ($action) {
                    $this->CompanyCandidate->set($this->data);
                    if (($this->CompanyCandidate->validates($this->data) & $this->CompanyCandidate->City->validates() & $this->CompanyCandidate->State->validates() & !(isset($extban_e) || isset($encfac_e)))) {
                        if (!$this->CompanyCandidate->validFiles()) {
                            $this->data['CompanyCandidate']['state'] = 0;
                            $this->set('file_error', true);
                            $this->Session->setFlash('&#161;Ten&eacute;s que enviar todos los archivos solicitados!', 'default', null, 'error');
                        } else {
                            $this->data['CompanyCandidate']['state'] = $action;
                            $this->CompanyCandidate->saveField('state', $action);
                            $this->_sendMail($user_id);
                            $this->Session->setFlash('Los cambios se guardaron correctamente!', 'default', null, 'success');
                            $this->redirect('/now/now_registers/my_company?registerOk=1');
                        }
                    } else {
                        $this->data['CompanyCandidate']['state'] = 0;
                        $this->Session->setFlash('&#161;Hay errores en el formulario!', 'default', null, 'error');
                    }
                }
            } else {
                $this->data['CompanyCandidate']['state'] = 0;
                $this->Session->setFlash('&#161;No se Pudo Guardar! &#161;Revise los campos por favor!', 'default', null, 'error');
            }
            $CompMediaDir = APP . 'media' . DIRECTORY_SEPARATOR . 'companies' . DIRECTORY_SEPARATOR;
            $upd_files = array();
            foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . 'extban_*') as $nombre_archivo) {
                $upd_files['extban'] = array(
                    $nombre_archivo,
                    date("F d Y H:i:s.", filemtime($nombre_archivo))
                );
            }
            foreach (glob($CompMediaDir . $user_id . DIRECTORY_SEPARATOR . 'encfac_*') as $nombre_archivo) {
                $upd_files['encfac'] = array(
                    $nombre_archivo,
                    date("F d Y H:i:s.", filemtime($nombre_archivo))
                );
            }
            $countries = $this->Country->find('first', array(
                'conditions' => array(
                    'name' => 'Argentina'
                )
            ));
            $fiscal_states = $states = $this->State->find('list', array(
                'conditions' => array(
                    'country_id' => $countries['Country']['id'],
                    'is_selectable' => true,
                    'is_approved' => true
                )
            ));
            $fiscal_cities = $cities = $this->City->find('list', array(
                'conditions' => array(
                    'country_id' => $countries['Country']['id'],
                    'is_group' => 0,
                    'is_selectable' => true,
                    'is_approved' => true
                )
            ));
            $this->set(compact('countries', 'states', 'cities', 'fiscal_states', 'fiscal_cities'));
            $this->set('upd_files', $upd_files);
            if (isset($extban_e)) {
                $this->CompanyCandidate->validationErrors['extban'] = "Formatos v&aacute;lidos: " . implode(', ', ConstCandidate::allowedFiles());
            }
            if (isset($encfac_e)) {
                $this->CompanyCandidate->validationErrors['encfac'] = "Formatos v&aacute;lidos:" . implode(', ', ConstCandidate::allowedFiles());
            }
        } else {
        	
            $this->redirect(Router::url(array(
                        'plugin' => 'now',
                        'controller' => 'now_registers',
                        'action' => 'my_company'
                            ), true));
        }
    }

    function getfile() {
        $user = $this->params['named']['user'];
        $file = $this->params['named']['file'];
        $CompMediaDir = APP . 'media' . DIRECTORY_SEPARATOR . 'companies' . DIRECTORY_SEPARATOR;
        $dirfile = $CompMediaDir . $user . DIRECTORY_SEPARATOR . $file;
        if (file_exists($dirfile)) {
            header('Content-Description: File Transfer');
            header('Content-Type: binary/octet-stream');
            header('Content-Disposition: attachment; filename="' . $file . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($dirfile));
            ob_clean();
            flush();
            readfile($dirfile);
            exit;
        } else {
            die("No file found!");
        }
    }

    function download($comp, $id) {
        $this->view = 'Media';
        $params = array(
            'id' => $id,
            'name' => 'Extracto Bancario',
            'extension' => 'jpg',
            'mimeType' => array(
                'jpg' => 'image/jpeg'
            ),
            'path' => APP . 'media' . DS . 'companies' . DS . $comp . DS
        );
        $this->set($params);
    }

    function eula() {
        if ($this->data['Company']['now_eula_ok']) {
            $company = $this->Company->findByUserId($this->Auth->user('id'));
            $this->Company->id = $company['Company']['id'];
            $company['Company']['now_eula_ok'] = 1;
            if (Configure::read('NowPlugin.deal.commission_percentage') == '') {
                $deal_commission_percentage = $this->data['Company']['deal_commission_percentage'] = '30.01';
            } else {
                $deal_commission_percentage = $this->data['Company']['deal_commission_percentage'] = Configure::read('NowPlugin.deal.commission_percentage');
            }
            $company['Company']['deal_commission_percentage'] = $deal_commission_percentage;
            $this->Company->set($company);
            unset($this->Company->validate['fiscal_cuit']['rule5']);
            $this->Company->save();
            $this->Session->setFlash('Estas listo para vender en Club Cupon Ya!', 'default', null, 'success');
            $this->redirect(array(
                'plugin' => 'now',
                'controller' => 'now_branches',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash('Ten&eacute;s que aceptar los T&eacute;rminos y Condiciones para continuar', 'default', null, 'error');
            $this->redirect(array(
                'plugin' => '',
                'controller' => 'deal_users',
                'action' => 'index_for_company'
            ));
        }
    }

    function cities($state_id) {
        $cities = $this->City->find('list', array(
            'conditions' => array(
                'state_id' => $state_id,
                'is_group' => 0,
                'is_selectable' => 1
            )
        ));
        echo json_encode($cities);
        die();
    }

    function _sendMail($user_id) {
        Debugger::log("method " . __METHOD__ . " is obsolete, use method sendMailCandidateInfoUpdate from Mail component instead", LOG_DEBUG);
        return $this->Mail->sendMailCandidateInfoUpdate($user_id);
    }

}
