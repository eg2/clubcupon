<?php

class NowCompaniesController extends NowAppController {

    var $name = 'Companies';
    var $uses = array(
        'Company'
    );

    function beforeFilter() {
        $this->Security->disabledFields = array(
            'City',
            'FiscalCity',
            'FiscalState',
            'State',
            'Company.latitude',
            'Company.longitude',
            'UserAvatar.filename',
            'User.id',
            'Company.id',
            'Company.address1',
            'Company.address2',
            'Company.country_id',
            'Company.name',
            'Company.phone',
            'Company.url',
            'Company.zip',
            'Company.fiscal_name',
            'Company.fiscal_address',
            'Company.fiscal_phone',
            'Company.contact_phone',
            'Company.fiscal_fax',
            'Company.fiscal_bank',
            'Company.fiscal_bank_account',
            'Company.fiscal_bank_cbu',
            'Company.fiscal_cond_iva',
            'Company.fiscal_cuit',
            'Company.fiscal_iibb',
            'Company.percepcion_iibb_caba',
            'Company.declaracion_jurada_terceros',
            'Company.persona',
            'Company.cheque_noorden'
        );
        AppModel::setDefaultDbConnection('master');
        parent::beforeFilter();
    }

    function list_companies_data() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $companyID = $this->params['form']['compID'];
        $company_data = $this->Company->find('all', array(
            'conditions' => array(
                'Company.id' => $companyID
            ),
            'fields' => array(
                'Company.cheque_noorden',
                'Company.fiscal_bank_account'
            ),
            'recursive' => 0
        ));
        if ($company_data[0]['Company']['cheque_noorden'] && $company_data[0]['Company']['fiscal_bank_account'] != '') {
            return 1;
        } else {
            if ($company_data[0]['Company']['fiscal_bank_account'] != '') {
                return 2;
            }
            if ($company_data[0]['Company']['cheque_noorden']) {
                return 3;
            }
            return 0;
        }
    }

}
