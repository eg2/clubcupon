<?php

App::import('Behavior', 'now.NowCalendar');
App::import('Behavior', 'now.NowItem');

class NowExportsController extends NowAppController {

    const FILE_EXTENSION = '.csv';

    public $name = 'NowExports';
    public $components = array(
        'RequestHandler'
    );
    public $uses = array(
        'accounting.AccountingCalendar',
        'accounting.AccountingItem',
        'api.ApiCompany'
    );

    private function initializeEnvironment() {
        $this->autoRender = false;
        Configure::write('debug', 0);
    }

    private function initializeDependencies() {
        $this->AccountingCalendar->Behaviors->attach('NowCalendar');
        $this->AccountingItem->Behaviors->attach('NowItem');
    }

    function beforeFilter() {
        $this->initializeDependencies();
        $this->initializeEnvironment();
        parent::beforeFilter();
    }

    private function setHeaders($fileName) {
        header("Content-Type: text/plain; charset=utf-8");
        header("Content-Type: application/ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename={$fileName}");
    }

    private function columnNames() {
        return array(
            "Id Calendario",
            "Calendario Periodo",
            "Modalidad De Pago",
            "Id Oferta",
            "Oferta",
            "Fecha De Publicacion",
            "Precio Cupon",
            "Q De Cupones",
            "Total",
            "Comision Club Cupon %",
            "Comision Club Cupon $",
            "Retencion Fondo De Garantia %",
            "Retencion Fondo De Garantia $",
            "Total Liquidacion"
        );
    }

    private function writeColumnNames($output, $columns) {
        fputcsv($output, $columns);
    }

    private function writeRows($output, $rows) {
        foreach ($rows as $row) {
            fputcsv($output, $row);
        }
    }

    private function exportRowsAsCsv($fileName, $columnNames, $rows) {
        ob_clean();
        $out = fopen('php://output', 'w');
        $this->setHeaders($fileName);
        $this->writeColumnNames($out, $columnNames);
        $this->writeRows($out, $rows);
        fclose($out);
    }

    function liquidate($calendarId) {
        $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
        if (!empty($company)) {
            $rows = $this->AccountingItem->findAllByCompanyIdAndCalendarIdAsExportRows($company['Company']['id'], $calendarId);
            if (!empty($rows)) {
                $this->exportRowsAsCsv($calendarId . self::FILE_EXTENSION, $this->columnNames(), $rows);
            }
        }
    }

    private function listOfIds($calendars) {
        $listOfIds = array_unique(Set::classicExtract($calendars, '{n}.AccountingCalendar.{(id)}'));
        return $listOfIds[0];
    }

    function liquidate_between_dates() {
        $since = $this->params['named']['since'];
        $until = $this->params['named']['until'];
        $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
        if (!empty($company)) {
            $calendars = $this->AccountingCalendar->findAllBetweenDatesByCompanyId($company['Company']['id'], $since, $until);
            if (!empty($calendars)) {
                $listOfIds = $this->listOfIds($calendars);
                $rows = $this->AccountingItem->findAllByCompanyIdAndCalendarIdAsExportRows($company['Company']['id'], $listOfIds);
                if (!empty($rows)) {
                    $this->exportRowsAsCsv($since . '-' . $until . self::FILE_EXTENSION, $this->columnNames(), $rows);
                }
            }
        }
    }

    function liquidate_by_id() {
        $calendarId = $this->params['named']['calendar_id'];
        $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
        if (!empty($company)) {
            $rows = $this->AccountingItem->findAllByCompanyIdAndCalendarIdAsExportRows($company['Company']['id'], array($calendarId));
            if (!empty($rows)) {
                $this->exportRowsAsCsv($calendarId . self::FILE_EXTENSION, $this->columnNames(), $rows);
            }
        }
    }

    function verify_data_for_export() {
        $since = $this->params['named']['since'];
        $until = $this->params['named']['until'];
        $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
        if (!empty($company)) {
            $calendars = $this->AccountingCalendar->findAllBetweenDatesByCompanyId($company['Company']['id'], $since, $until);
            if (!empty($calendars)) {
                $listOfIds = $this->listOfIds($calendars);
                $rows = $this->AccountingItem->findAllByCompanyIdAndCalendarIdAsRows($company['Company']['id'], $listOfIds);
                if (empty($rows)) {
                    echo json_encode(array(
                        'status' => 'error',
                        'returned_val' => 'No se encontraron liquidaciones registradas para exportar'
                    ));
                } else {
                    echo json_encode(array(
                        'status' => 'success',
                        'returned_val' => 'todo OK'
                    ));
                }
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'returned_val' => 'No se encontraron calendarios registrados en ese periodo'
                ));
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'returned_val' => 'El usuario no tiene una Empresa asociada'
            ));
        }
    }

    function index() {
        $this->layout = 'now_exports/ajax';
        $this->render('form_export');
    }

}
