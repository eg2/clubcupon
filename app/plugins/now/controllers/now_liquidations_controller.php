<?php

App::import('Behavior', 'now.NowCalendar');
App::import('Behavior', 'now.NowItem');

class NowLiquidationsController extends NowAppController {

    public $name = 'NowLiquidations';
    public $uses = array(
        'accounting.AccountingCalendar',
        'accounting.AccountingItem',
        'now.NowDeal'
    );
    public $components = array(
        'RequestHandler',
        'now.NowLiquidateTabService'
    );
    public $paginate = array(
        'limit' => 2
    );

    private function initializeDependencies() {
        $this->AccountingCalendar->Behaviors->attach('NowCalendar');
        $this->AccountingItem->Behaviors->attach('NowItem');
    }

    function beforeFilter() {
        $this->initializeDependencies();
        parent::beforeFilter();
    }

    private function accountingTypeByFilter($filter) {
        $accountingType = AccountingType::liquidate();
        if ($filter == 'redeemed') {
            $accountingType = AccountingType::liquidateByRedeemed();
        }
        if ($filter == 'sold') {
            $accountingType = AccountingType::liquidateBySold();
        }
        return $accountingType;
    }

    function index() {
    	
        $deals = array();
        $result = array();
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        if (isset($this->params['named']['calendar_id'])) {//para el detalle
            if (!empty($company)) {
            	if (isset($this->params['named']['deal_id'])) :
            		$this->paginate = $this->AccountingItem->findAllByCompanyIdAndCalendarId($company['Company']['id'], $this->params['named']['calendar_id'],array($this->params['named']['deal_id']));
            		$this->set('deal_id', $this->params['named']['deal_id']);
            	else:
            		$this->paginate = $this->AccountingItem->findAllByCompanyIdAndCalendarId($company['Company']['id'], $this->params['named']['calendar_id']);
            	endif;
            	$this->set('filter', $this->params['named']['filter']);
                $result = $this->paginate;
            }
            $this->set('accountingItems', $result);
        } else {
            if (!empty($company)) {
                $dealIdList = array();
                $accountingType = $this->accountingTypeByFilter($this->params['named']['filter']);
                if (isset($this->params['named']['deal_id'])) {
                	$dealIdList[] = $this->params['named']['deal_id'];
                    $deals[] = $this->getDeal($this->params['named']['deal_id']);
                    
                    $this->set('deal_id', $this->params['named']['deal_id']);
                    
                }
                $this->paginate = $this->AccountingCalendar->findOptions($company['Company']['id'], $dealIdList, $accountingType);
                $result = $this->paginate();
            }
            $this->set('deals', $deals);
            $this->set('accountingCalendars', $result);
        }
    }

    private function getDeal($deal_id) {
        $deal = $this->NowDeal->find('first', array(
            'conditions' => array(
                'NowDeal.id' => $deal_id
            ),
            'fields' => array(
                'NowDeal.name'
            ),
            'recursive' => 0
        ));
        return $deal;
    }

    function liquidations() {
        if (isset($this->params['named']['deal_id'])) {
            $this->set('deal_id', $this->params['named']['deal_id']);
        }
        if (isset($this->params['named']['filter'])) {
        	$this->set('filter', $this->params['named']['filter']);
        }
    }

}
