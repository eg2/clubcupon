<?php

App::import('Sanitize');

class NowBranchesController extends NowAppController {

    var $name = 'NowBranches';
    var $uses = array(
        'now.NowBranch',
        'now.NowBranchesDeals',
        'now.NowDeal',
        'City',
        'Company',
        'Country',
        'State',
        'AccountingMaxPaymentDateGrouped',
        'Neighbourhood'
    );

    public function beforeFilter() {
        AppModel::setDefaultDbConnection('master');
        $securityDisabled = array(
            'add',
            'edit'
        );
        if (in_array($this->action, $securityDisabled)) {
            $this->Security->validatePost = false;
        }
        parent::beforeFilter();
    }

    function beforeRender() {
        $this->__setNextLiquidation();
        parent::beforeRender();
    }

    function __setNextLiquidation() {
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        $conditionsForFindByCompanyId['conditions']['company_id'] = $company['Company']['id'];
        $nextLiq = $this->AccountingMaxPaymentDateGrouped->__findFirstByCompanyIdGroupedByMaxPaymentDate($conditionsForFindByCompanyId);
        $this->set('nextLiquidationDate', $nextLiq['AccountingMaxPaymentDateGrouped']['max_payment_date']);
        $this->set('nextLiquidationValue', $nextLiq['AccountingMaxPaymentDateGrouped']['liquidate_total_amount']);
    }

    function is_own($id = null) {
        $is_own = false;
        $branch = $this->NowBranch->findById($id);
        if (!empty($branch)) {
            $company = $this->Company->findById($branch['NowBranch']['company_id']);
            $is_own = $company['Company']['user_id'] == $this->Auth->user('id');
        }
        return $is_own;
    }

    function index() {
        $this->layout = "now";
        $this->__redirectIfNotCompany();
        $this->pageTitle = 'Sucursales';
        $this->NowBranch->recursive = 0;
        $company = $this->Company->findByUserId($this->Auth->user('id'));
        if (!$this->Company->hasBranches($company)) {
            $this->_addBranchRedirect();
        }
        $this->paginate = array(
            'conditions' => array(
                'NowBranch.company_id' => $company['Company']['id']
            )
        );
        $this->set('branches', $this->paginate());
    }

    function add() {
        $this->__redirectIfNotCompany();
        $this->layout = "now";
        $this->pageTitle = 'Agregar sucursal';
        if (!empty($this->data)) {
            $this->NowBranch->create();
            $this->data['NowBranch']['name'] = Sanitize::html($this->data['NowBranch']['name'], array(
                        'encode' => false,
                        'remove_html' => true
            ));
            $this->data['NowBranch']['email'] = Sanitize::html($this->data['NowBranch']['email'], array(
                        'encode' => false,
                        'remove_html' => true
            ));
            $this->NowBranch->set($this->data);
            $company = $this->Company->findByUserId($this->Auth->user('id'));
            $this->NowBranch->set('company_id', $company['Company']['id']);
            if ($this->NowBranch->save($this->data)) {
                $this->Session->setFlash(sprintf('La sucursal fue agregada'), 'default', null, 'success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(sprintf('La sucursal no pudo ser agregada. Por favor, intent&aacute; nuevamente.'), 'default', null, 'error');
            }
        }
        $countries = $this->Country->find('list');
        $states = $this->State->find('list', array(
            'conditions' => array(
                'is_selectable' => true,
                'is_approved' => true
            ),
            'order' => 'name'
        ));
        $cities = $this->City->find('list', array(
            'conditions' => array(
                'is_selectable' => true,
                'is_group' => 0,
                'is_approved' => true
            )
        ));
        $neighbourhoods = $this->Neighbourhood->find('list');
        $this->set(compact('companies', 'countries', 'states', 'cities', 'neighbourhoods'));
    }

    function edit($id = null) {
        $this->__redirectIfNotCompany();
        $this->pageTitle = 'Editar sucursal';
        if (is_null($id) || !$this->is_own($id)) {
            $this->cakeError('error404');
        }
        if (!empty($this->data)) {
            $this->NowBranch->id = $this->data['NowBranch']['id'] = $id;
            $this->data['NowBranch']['name'] = Sanitize::html($this->data['NowBranch']['name'], array(
                        'encode' => false,
                        'remove_html' => true
            ));
            $this->data['NowBranch']['email'] = Sanitize::html($this->data['NowBranch']['email'], array(
                        'encode' => false,
                        'remove_html' => true
            ));
            if ($this->NowBranch->save($this->data)) {
                $this->Session->setFlash(sprintf('La sucursal "%s" fue actualizada', $this->data['NowBranch']['name']), 'default', null, 'success');
                return $this->redirect(array(
                            'plugin' => 'now',
                            'controller' => 'now_branches',
                            'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(sprintf('La sucursal "%s" no pudo ser actualizada. Por favor, intent&aacute; nuevamente.', $this->data['NowBranch']['name']), 'default', null, 'error');
            }
        } else {
            $this->data = $this->NowBranch->read(null, $id);
            if (empty($this->data)) {
                $this->cakeError('error404');
            }
        }
        $this->pageTitle.= ' - ' . $this->data['NowBranch']['id'];
        $countries = $this->Country->find('list');
        $states = $this->State->find('list', array(
            'conditions' => array(
                'is_selectable' => true,
                'is_approved' => true
            ),
            'order' => 'name'
        ));
        $cities = $this->City->find('list', array(
            'conditions' => array(
                'is_selectable' => true,
                'is_group' => 0,
                'is_approved' => true
            )
        ));
        $neighbourhoods = $this->Neighbourhood->find('list');
        $this->set(compact('companies', 'countries', 'states', 'cities', 'neighbourhoods'));
    }

    function delete($id = null) {
        if (is_null($id) || !$this->is_own($id)) {
            $this->cakeError('error404');
        }
        $this->NowBranch->bindModel(array(
            'hasAndBelongsToMany' => array(
                'NowDeal' => array(
                    'className' => 'Now.NowDeal',
                    'joinTable' => 'branches_deals',
                    'foreignKey' => 'now_branch_id',
                    'associationForeignKey' => 'now_deal_id',
                    'conditions' => array(
                        'deal_status_id' => array(
                            ConstDealStatus::Upcoming,
                            ConstDealStatus::Open,
                            ConstDealStatus::Tipped,
                            ConstDealStatus::PendingApproval
                        )
                    )
                )
            )
        ));
        $all = $this->NowBranch->find('all', array(
            'conditions' => array(
                'NowBranch.id' => $id
            )
        ));
        $company_id = $this->Company->field('id', 'user_id = ' . $this->Auth->user('id'));
        $drafts = $this->NowDeal->query('SELECT * FROM deals WHERE company_id = "' . $company_id . '" AND private_note REGEXP "' . mysql_real_escape_string('s:7:"braches";a:[0-9]+:{[^}]*s:[0-9]+:"' . $id . '";[^}]*}') . '"');
        if (count($all[0]['NowDeal']) > 0 || count($drafts) > 0) {
            $this->Session->setFlash('No se puede borrar la sucursal, existen ofertas relacionadas con la misma cuyo estado lo impide', 'default', null, 'error');
        } elseif ($this->NowBranch->find('count', array(
                    'conditions' => array(
                        'company_id' => $company_id
                    )
                )) > 1) {
            $this->NowBranch->del($id);
        } else {
            $this->Session->setFlash('Debe existir una sucursal como m&iacute;nimo', 'default', null, 'error');
        }
        $this->redirect(array(
            'plugin' => 'now',
            'controller' => 'now_branches',
            'action' => 'index'
        ));
    }

    function __redirectIfNotCompany() {
        if (!$this->Auth->user('id')) {
            $this->redirect(array(
                'plugin' => '',
                'controller' => 'users',
                'action' => 'login'
            ));
        }
        if (!$this->Company->findByUserId($this->Auth->user('id'))) {
            $this->Session->setFlash(sprintf('No esta logueado como empresa'), 'default', null, 'error');
            $this->redirect(array(
                'controller' => 'deals',
                'action' => 'index',
                'plugin' => ''
            ));
        }
    }

    function geoAjax($dimension = 'state', $parent_id = 1) {
        $this->autoRender = false;
        $ret = array();
        switch ($dimension) {
            case 'state':
                $ret = $this->NowBranch->State->find('list', array(
                    'conditions' => array(
                        'country_id' => $parent_id,
                        'is_selectable' => true,
                        'is_approved' => true
                    ),
                    'order' => 'name'
                ));
                break;

            case 'city':
                $ret = $this->NowBranch->City->find('list', array(
                    'conditions' => array(
                        'state_id' => $parent_id,
                        'is_selectable' => true,
                        'is_group' => 0,
                        'is_approved' => true
                    ),
                    'order' => 'name'
                ));
                break;
        }
        $this->set('geos', $ret);
        $this->render('geoAjax', 'ajax');
    }

    function _addBranchRedirect() {
        return $this->redirect(array(
                    'plugin' => 'now',
                    'controller' => 'now_branches',
                    'action' => 'add'
        ));
    }

}
