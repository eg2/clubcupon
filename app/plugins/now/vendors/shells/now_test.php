<?php

App::import('Model', 'now.NowDealView');
App::import('Model', 'AccountingCalendarsGrouped');

class NowTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->NowDealView = new NowDealView();
    }

    public function testFindAll() {
        echo __METHOD__ . "\n";
    }

    public function main() {
        echo __METHOD__ . "\n";
        $this->testFindAll();
        $this->NowDealView->hello();
        $conditions = array(
            "NowDealView.company_id" => "83",
            "NowDealView.deal_is_now" => "0"
        );
        var_dump($this->NowDealView->find('all', array(
                    'conditions' => $conditions)
        ));
    }

}
