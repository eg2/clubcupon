if (!window.console) {
  (function() {
    var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml",
    "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
    window.console = {};
    for (var i = 0; i < names.length; ++i) {
      window.console[names[i]] = function() {};
    }
  }());
}
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
    ? Math.ceil(from)
    : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
        this[from] === elt)
        return from;
    }
    return -1;
  };
}
/*
$(document).bind("keydown.cbox_close", function (e) {
        if (e.keyCode === 27) {
                e.preventDefault();
                throw new Error('No cerrar.');
        }});
*/


/* configuration registry */
var _configuration = {
  popupsDefault: {
    width:'800px',
    height:'600px',
    frase:'Cerrar'
  }
};

var popupGeneric = function(url, width, height, frase) {
  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  if(typeof(frase) == "undefined" || frase == null || frase == "") {
    frase = _configuration.popupsDefault.frase;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: frase,
    href: url,
    iframe: true,
    width: width,
    height: height
  };
  $.fn.colorbox(configColorbox);
  return false;
};
var modalPopupGeneric = function (url, width, height, frase) {
  if (typeof (width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if (typeof (height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  if(typeof(frase) == "undefined" || frase == null || frase == "") {
    frase = _configuration.popupsDefault.frase;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: frase,
    href: url,
    iframe: true,
    width: width,
    height: height,
    onLoad: function () {$('#cboxClose').remove ();}
  };
  $.fn.colorbox (configColorbox);
  return false;
};
var popupWelcome = function(url, width, height, onClose) {
  if(typeof(width) == "undefined" || width == null) {
    width = _configuration.popupsDefault.width;
  }

  if(typeof(height) == "undefined" || height == null) {
    height = _configuration.popupsDefault.height;
  }

  var configColorbox = {
    escKey: false,
    overlayClose: false,
    close: 'Ya estoy suscripto',//_configuration.popupsDefault.frase,
    href: url,
    iframe: true,
    width: width,
    height: height,
    onClosed: function () {$.cookie('CakeCookie[first_time_user]', 1);$.cookie('fullscreenClose', 1);}
  };

  $.fn.colorbox(configColorbox);

  return false;
};


var cleanFullScreen = function()
{
    $.scrollTo('0', 0);
    $('#fScreenDiv').fadeIn();
    $('body').css('overflow','hidden');
}

var popupFullScreen = function(url) {

  var configColorbox = {
    escKey: false,
    href: url,
    iframe: true,
    width: "100%",
    height: "100%"
  };

  $.fn.colorbox(configColorbox);
  $('#cboxClose').remove();

  return false;
};

function isPositiveInteger(val){
  if(val==null){
    return false;
  }
  for (var i = 0; i < val.length; i++) {
    var ch = val.charAt(i)
    if (ch < "0" || ch > "9") {
      return false
    }
  }
  return true;
}


var ciudadDeBuenosAiresId = '1';
var updatedRegion = function() {

  var $e = $(this);
  var actualVal = $e.val();

  if(actualVal !==  ciudadDeBuenosAiresId) {
    $('.js-neighbourhood-control').prev().hide();
    $('.js-neighbourhood-control').hide();
    $('.js-neighbourhood-control').val('');
  }else {
    $('.js-neighbourhood-control').prev().show();
    $('.js-neighbourhood-control').show();
  }
};

var updateQuestion = function() {

  var $select = $(this);
  var $input = $('#questionfree_'+$select.attr('id').split('_')[1]);


  if($select.val() === '0') {
    $input.parent().removeClass('invisible');
    $input.parent().parent().removeClass('invisible');
  }
  else {
    $input.parent().addClass('invisible');
    $input.parent().parent().addClass('invisible');
  }
};

var beforeSubmitProfile = function(formData, jqForm, options) {
  // validates errors in user profile form.
  // normalize the values of inputs (cleans free responses)
  // if an error occurs the body scrolls to the error.
  var errors = [];
  jqForm.find('input').each(function(){
    if($(this).parent().hasClass('freeresponse') && !$(this).parent().hasClass('invisible')) {
      // check free response
      if($.trim($(this).val()) === '') {
        errors.push($(this).attr('id'))
        $(this).addClass('formError');
      }
    }
    if($(this).parent().hasClass('freeresponse') && $(this).parent().hasClass('invisible')) {
      // deletes free response
      $(this).val('');
    }
  });
  if(errors.length) {
    $("body").animate({
      scrollTop: $('#'+errors[0]).offset().top
    }, 700 );
    return false;
  }
  return true;
};

var newAmountBD = null;
var updateJsQuantity = function() {
    if(!$('.js-quantity').length)
      return;
    var selectedQuantity = $('.js-quantity').val();
    if (selectedQuantity.length==0){
      lastQuantitySelected = selectedQuantity;
      return;
    }
    if (!isPositiveInteger(selectedQuantity)) {
      $(this).val(lastQuantitySelected);
      alert('Solo puede ingresar números');
      return;
    }
    if (selectedQuantity.length > 4) {
      $(this).val(lastQuantitySelected);
      alert('La cantidad es demasiado alta.');
      return;
    }
    lastQuantitySelected = selectedQuantity;

    var selectedQuantityBD = new BigDecimal(selectedQuantity);
    var dealAmount = $('#NowDealDealAmount').val();
    var dealAmountBD = new BigDecimal(dealAmount);
    newAmountBD = dealAmountBD.multiply(selectedQuantityBD);
    var availableAmount = $('#NowDealUserAvailableBalance').val();
    var availableAmountBD = new BigDecimal(availableAmount);
    var needToPayBD = null;
    $('.js-deal-total').html(newAmountBD.toString().replace('.',','));
    var loginUrl=$('#UserF').val();
    if(typeof loginUrl!='undefined' && selectedQuantity !=''){
      loginUrl=loginUrl.replace(/\/\d+$/,'\/'+ selectedQuantity);
      $('#UserF').val(loginUrl);
    }
    return false;
  };


var checkShowCuotas = function() {
  if(!$('#NowDealCantidadCuotas').length)
    return;
  var showCuotas = true;
  // checks if the new total is greather than the minimal financial amount
  var ceroBD = new BigDecimal('0');
  var minimalFinancialAmount = $('#NowDealMinimalAmountFinancial').val() ;
  if(!minimalFinancialAmount.length)
    minimalFinancialAmount = '0';
  var minimalFinancialAmountBD = new BigDecimal(minimalFinancialAmount);
  if(newAmountBD.compareTo(minimalFinancialAmountBD) === -1)
  {
    showCuotas = false;
  }
  if(showCuotas) {
    var cuotasIds = ['2_43','2_44'];
    var cuotaId = $('.js-payment-type:checked').val();
    cuotaId = cuotaId.split('_')[0] + '_' + cuotaId.split('_')[1];

    if(cuotasIds.indexOf(cuotaId) === -1)
    {
      showCuotas = false;
    }
  }
  if(!showCuotas)
  {
    // Ocultar cuotas
    //$('#NowDealCantidadCuotas').parent().hide('slow');
    $('#NowDealCantidadCuotas').parent().hide();
    lastCuotasSelected = $('#NowDealCantidadCuotas').val();
    $('#NowDealCantidadCuotas').val('1');
  }else{
    // Mostrar cuotas
    //if($('#NowDealCantidadCuotas').parent().is(':hidden')) { animar aca ? }
    $('#NowDealCantidadCuotas').parent().show('slow');
    $('#NowDealCantidadCuotas').val(lastCuotasSelected);
  }
};


var updaterMask = function(element) {

  var val = element.val();
  if(typeof(val)=="undefined") return;

  var idDecimal = element.attr('id') + 'Decimal' ;
  var idInteger = element.attr('id') + 'Integer';

  var valParsed = val.split('.');
  if(valParsed.length != 2) {
    valParsed = [val,''];
  }
  $('#'+idInteger).val(valParsed[0]);
  $('#'+idDecimal).val(valParsed[1]);
};

function __l(str, lang_code) {
  //TODO: lang_code = lang_code || 'en_us';
  return(cfg && cfg.lang && cfg.lang[str]) ? cfg.lang[str]: str;
}

function __cfg(c) {
  return(cfg && cfg.cfg && cfg.cfg[c]) ? cfg.cfg[c]: false;
} (function($) {
  $.fn.confirm = function() {
    this.livequery('click', function(event) {
      return window.confirm('Estas seguro que deseas ' + this.innerHTML.toLowerCase() + '?');
    });
  };
  $.fn.currencyMask = function() {
    $(this).livequery(function() {
      var $this = $(this);
      $this.hide();
      var id = $this.attr('id');
      var idDecimal = $this.attr('id') + 'Decimal' ;
      var idInteger = $this.attr('id') + 'Integer';
      var readOnly = '';
      if($this.attr('readonly')) {
        readOnly = 'readonly="readonly"';
      }
      var inputInteger = '<input class="currencyInteger" type="text" value="" id="' + idInteger  + '" '+readOnly+' />';
      var inputDecimal = '<input class="currencyDecimal" type="text" value="" id="' + idDecimal + '" '+readOnly+' />';
      $this.after(inputInteger + '<span class="currencySeparator">,</span>' + inputDecimal);
      var updater = function(){
        var valInt = $('#'+idInteger).val();
        var valDec = $('#'+idDecimal).val();

        if(valDec == '') {
          valDec = '00';
        }
        if(valInt == '') {
          valInt = '0';
        }
        $('#'+idInteger).removeClass('currencyError');
        $('#'+idDecimal).removeClass('currencyError');
        if(valDec.indexOf('.') !== -1 || valDec.indexOf(',') !== -1) {
          alert('No utilices el punto ni la coma.');
          $('#'+idDecimal).addClass('currencyError');
          $('#'+idDecimal).focus();
          return;
        }
        if(valInt.indexOf('.') !== -1 || valInt.indexOf(',') !== -1) {
          alert('No utilices el punto ni la coma.');
          $('#'+idInteger).addClass('currencyError');
          $('#'+idInteger).focus();
          return;
        }
        $this.val(valInt + '.' + valDec);
        $this.change()
      };
        updaterMask($this);
      $('#'+idInteger).keyup(updater);
      $('#'+idDecimal).keyup(updater);

    });

  };

  $.fn.flashMsg = function() {
    $(this).livequery(function() {
      var target = $(this);
      setTimeout(function(){
        target.fadeOut(1000, function() {
          target.remove();
        });
      }, 3500);
    });
  };

  $.fn.fautocomplete = function() {
    $(this).livequery(function() {
      var $this = $(this);
      $this.autocomplete($this.metadata().url, {
        minChars: 0,
        autoFill: true
      /* JSON autocomplete is flaky. Till the issue is sorted out in the jquery.autocomplete, it's commented out
                ,dataType: 'json',
                parse: function(data) {
                    var parsed = [];
                    for (var i in data) {
                        parsed[parsed.length] = {
                            data: data[i],
                            value: i,
                            result: data[i]
                            };
                    }
                    return parsed;
                },
                formatItem: function(row) {
                    return row;
                }*/
      }).result(function(event, data, formatted) {
        var targetField = $this.metadata().targetField.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
        var targetId = $this.metadata().id;
        if ( ! $('#' + targetId).length) {
          $this.after(targetField);
        }
        var tdata = data.toString();
        $('#' + targetId).val(tdata.split(',')[1]).attr('x-data', tdata.split(',')[0]);
      }).blur(function() {
        var targetId = $this.metadata().id;
        if ($('#' + targetId).length) {
          if ($this.val() != $('#' + targetId).attr('x-data')) {
            $('#' + targetId).remove();
          }
        }
      });
    });
  };
  $.fn.companyprofile = function(is_enabled) {
    if (is_enabled == 0) {
      $('.js-company_profile_show').hide();
    }
    if (is_enabled == 1) {
      $('.js-company_profile_show').show();
    }
  };
  $.fn.fajaxform = function(beforeSubmitOptional) {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {
          if(typeof(beforeSubmitOptional) !== "undefined") {
            if(!beforeSubmitOptional(formData, jqForm, options)) {
              $this.unblock();
              return false;
            }
          }
          $('input:file', jqForm[0]).each(function(i) {
            if ($('input:file', jqForm[0]).eq(i).val()) {
              options['extraData'] = {
                'is_iframe_submit': 1
              };
            }
          });
          $this.block();
        },
        success: function(responseText, statusText) {
          redirect = responseText.split('*');
          if (redirect[0] == 'redirect') {
            location.href = redirect[1];
          } else if ($this.metadata().container) {
            $('.' + $this.metadata().container).html(responseText);
          } else {
            $this.parents('.js-responses').html(responseText);
          }
          $this.unblock();
        }
      });
      return false;
    });
  };
  $.fn.fajaxaddform = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      invoice_id = $('#InvoiceId').val();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          if (responseText.indexOf($this.metadata().container) != '-1') {
            $('.' + $this.metadata().container).html(responseText);
          } else {
            $.get(__cfg('path_relative') + 'user_cash_withdrawals/index/', function(data) {
              $('.js-withdrawal_responses').html(data);
              return false;
            });
          }
          $this.unblock();
        }
      });
      return false;
    });
  };

  // --------------- Ajax login Starts ----------------------------
  $.fn.fajaxlogin = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          redirect = responseText.split('*');
          if (redirect[0] == 'redirect') {
            location.href = redirect[1];
          }else if(responseText == 'success') {
            window.location.reload();
          }
          else
              {
                  window.location.reload();
                //$this.parents('.js-login-response').html(responseText);
              }

        }
      });
      return false;
    });
  };
  $.fn.fcommentform = function() {
    $(this).livequery('submit', function(e) {
      var $this = $(this);
      $this.block();
      $this.ajaxSubmit( {
        beforeSubmit: function(formData, jqForm, options) {},
        success: function(responseText, statusText) {
          if (responseText.indexOf($this.metadata().container) != '-1') {
            $('.' + $this.metadata().container).html(responseText);
          }else {
            $('.commment-list .notice').hide();
            $('.js-comment-responses').prepend(responseText);
            $('.' + $this.metadata().container + ' div.input').removeClass('error');
            $('.error-message', $('.' + $this.metadata().container)).remove();
          }
          if (typeof($('.js-captcha-container').find('.captcha-img').attr('src')) != 'undefined') {
            captcha_img_src = $('.js-captcha-container').find('.captcha-img').attr('src');
            captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
            $('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
          }
          $this.unblock();
        },
        clearForm: true
      });
      return false;
    });
  };

  $.fn.fcolorbox = function() {
    $(this).livequery(function(e) {
      $(this).colorbox( {
        opacity: 0.30,
        innerWidth:600,
        innerHeight:820,
        close: 'cerrar',
        title:'&nbsp;',
        //onLoad: function () { }
      });
    });
  };

  // open thickbox
  $('a.js-thickbox').fcolorbox();

  //  date picker function starts here


  // date picker function ends here

  $.fn.foverlabel = function() {
    $(this).livequery(function(e) {
      $(this).overlabel();
    });
  };
  $.fn.fshowmap = function(point_y, point_x, drag) {
    $('#js-map').jmap('init', {
      mapCenter: [point_y, point_x],
      mapShowjMapIcon: true,
      mapZoom: 10,
      mapEnableDragging: true,
      mapEnableScrollZoom: true
    }, function(el, options) {
      //Adding marker to the user location
      $(el).jmap('addMarker', {
        pointLatLng: [point_y, point_x],
        pointIsDraggable: drag
      });
      map_reference = el.jmap;
      location_reference = new GLatLng(parseFloat(point_y), parseFloat(point_x));
    });
  };
})
(jQuery);
jQuery('html').addClass('js');



 var addOtherImages = function(event) {
     //Get plugin file inputs, clone them
     // AttachmentMultiple.1.filename
     var $button = $(event.target);

     $("input[name*='[AttachmentMultiple]["+pluginFileInputCounter+"]']").clone().each(function() {

        //Rename the fields increasing numbers:
        this.name = this.name.replace(/\[(\d+)\]/, function (m,x) {
            return '[' + (parseInt(x) + 1) + ']'
        });

        //Clear the values of the cloned fields:
        this.value = '';
        this.className = 'losBuenosMuchachosFiles';
        //Append them to the form:
        $button.before($(this));
     });
     pluginFileInputCounter++;
     if(pluginFileInputLimit <= pluginFileInputCounter) {
       $button.hide();
     }
 }
 function clickCarouselItem(ev) {

   var html = $(ev).attr('rel');

    $('.js-jcarousel-target').html(html);
 }

jQuery(document).ready(function($) { // $(document).ready starts


  var min_date = new Date();
  min_date.setDate(min_date.getDate()+NowPlugin_calendar_minium_date);
  $("#NowDealStartDate" ).datepicker({dateFormat: 'yy-mm-dd'});
  $("#NowDealEndDate" ).datepicker({dateFormat: 'yy-mm-dd'});
  $("#NowScheduledDealsEndDate" ).datepicker({dateFormat: 'yy-mm-dd', minDate: min_date});
  $("#NowDealCouponExpiryDate" ).datepicker({dateFormat: 'yy-mm-dd', minDate: min_date});
    var min_date_cupons_start = new Date();
    var min_date_cupons_end = new Date();
   $("#NowDealScheduledDealsEndDate" ).datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});

   min_date_cupons_start.setFullYear(min_date_cupons_start.getFullYear()-10);

   $("#NowCuponsCsvStartDate").datepicker({dateFormat: 'yy-mm-dd', minDate:  min_date_cupons_start, maxDate:min_date_cupons_end});
   $("#NowCuponsCsvEndDate").datepicker({dateFormat: 'yy-mm-dd', minDate:  min_date_cupons_start, maxDate:min_date_cupons_end});

  $('.js-payment-settings-group').change(function() {

    var $e = $(this);
    var paymentSettingId = $e.val();
    var paymentOptionsGroup = _(paymentOptionsData).filter(function(e){return e.PaymentSetting.id === paymentSettingId;});
    if(paymentSettingId == '0') {
      $('.payment-setting-group-selector').show();
      return false;
    }
    $('.payment-setting-group-selector').hide();
    var groupContainer = $('.js-payment-setting-group-' + paymentSettingId);

    groupContainer.show();
//    _(paymentOptionsGroup).each(function(option) {
//        console.log(option.PaymentOption.name);
//    });
    return false;
  });

  $('.js-payment-setting-group-select-all').click(function(){
    var $eContainer = $(this).closest('.payment-setting-group-selector');
    $eContainer.find('input').attr('checked','checked');
    return false;
  });
  $('.js-payment-setting-group-select-none').click(function(){
    var $eContainer = $(this).closest('.payment-setting-group-selector');
    $eContainer.find('input').removeAttr('checked');
    return false;
  });




  if($('#quantityBar').length != 0) {
      $('#quantityBar .bar').animate({
        width: $('#quantityBar .bar').html() + '%'
      }, 1500, 'linear');
    }

  if($('#side_offer').length > 0) {
    if(($('#side_offer_original small').html().length > 6) || ($('#side_offer_discounted small').html().length > 6) || ($('#side_offer_percentage small').html().length > 6)) {
      $('#side_offer').addClass('small');
    }
  }
  if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
  $('.js-price').currencyMask();

  // common confirmation delete function
  $('a.js-delete').confirm();

  // bind form using ajaxForm
  $('.js-ajax-form').fajaxform();
  $('.js-ajax-form-profile').fajaxform(beforeSubmitProfile);
  $('.js-ajax-add-form').fajaxaddform();

  // bind form comment using ajaxForm
  $('.js-comment-form').fcommentform();
  $('.js-ajax-login').fajaxlogin();
 // jquery ui tabs function
  $('.js-tabs').livequery(function() {
    $(this).tabs({
      ajaxOptions: {
        cache: false
      },
      cache: false,
      load: function() {
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
      }
    });
  });

  $(".ui-tabs .ui-tabs-nav li:only-child").css("float", "none");
  $('.js-mystuff-tabs').livequery(function() {
    $(this).tabs({
      ajaxOptions: {
        cache: false
      },
      cache: false,
      load: function() {
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements()
      }
    });
  });

  $('.js-people-find').livequery('click', function() {
    $('.js-mystuff-tabs').tabs( "select" , 5);
    return false;
  });

  $('.js-print').livequery(function() {
    window.print();
    return false;
  });

  $('.js-confirm-add-amount-to-wallet').submit(function() {
    var value = $('#ExternalservicesAmount').val();
    var valueBD;
    if(!value.length || !$('#ExternalservicesAmountInteger').val().length) {
      alert('El monto es inválido.');
      return false;
    }
    try {
      valueBD = new BigDecimal(value);

    } catch(e) {
      alert('El monto es inváido.');
      return false;
    }
    var r = confirm('¿Está seguro que desea cargar este monto sobre el usuario seleccionado? Su acción será registrada por los servidores.');
    if(!r) {
      return false;
    }
    return true;
  });

  $('.js-confirm-action-link').click(function() {
    return confirm('¿Está seguro que desea realizar esta acción?.');
  });



  $('.js-auto-submit').submit();

  // jquery autocomplete function
  $('.js-autocomplete, .js-multi-autocomplete').fautocomplete();

  // flash message function
  //deal view page
  $('#NowDeal_countdown').livequery(function() {
    var end_date = parseInt($(this).parents().find('#NowDeal_countdown_amout').html());
    $(this).countdown( {
      until: end_date,
      format: 'H M S',
      timeSeparator : '<span class="separator"></span>'
    });
  });

  $('img.js-open-datepicker').livequery('click', function() {
    var div_id = $(this).attr('name');
    $('#' + div_id).toggle();
    $(this).parent().parent().toggleClass('date-cont');
  });

  $('a.js-close-calendar').livequery('click', function() {
    $('#' + $(this).metadata().container).hide();
    $('#' + $(this).metadata().container).parent().parent().toggleClass('date-cont');
    return false;
  });

  $('a.js-no-date-set').livequery('click', function() {
    $this = $(this);
    $tthis = $this.parents('.input');
    $('div.js-datetime', $tthis).children("select[id$='Day']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Month']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Year']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Hour']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Min']").val('');
    $('div.js-datetime', $tthis).children("select[id$='Meridian']").val('');
    $('#caketime' + $this.metadata().container).val('');
    $('#caketime' + $this.metadata().container).parent('div.timepicker').find('label.overlabel-apply').css('text-indent','0px');
    $('.displaydate' + $this.metadata().container + ' span').html('Seleccionar');
    return false;
  });

  // jquery datepicker
  //this.regional[""]=


  //for js overlable
  $('.js-overlabel label').foverlabel();
  $('#errorMessage,#authMessage,#successMessage,#flashMessage').flashMsg();

  // admin side select all active, inactive, pending and none
  $('.js-admin-select-all').livequery('click', function() {
    $('.js-checkbox-list').attr('checked', 'checked');
    return false;
  });

  $('.js-admin-select-none').livequery('click', function() {
    $('.js-checkbox-list').attr('checked', false);
    return false;
  });

  $('.js-admin-select-pending').livequery('click', function() {
    $('.js-checkbox-active').attr('checked', false);
    $('.js-checkbox-inactive').attr('checked', 'checked');
    return false;
  });

  $('.js-admin-select-approved').livequery('click', function() {
    $('.js-checkbox-active').attr('checked', 'checked');
    $('.js-checkbox-inactive').attr('checked', false);
    return false;
  });

  $('.js-admin-action').livequery('click', function() {
    var active = $('input.js-checkbox-active:checked').length;
    var inactive = $('input.js-checkbox-inactive:checked').length;
    if (active <= 0 && inactive <= 0 && $(this).val() >= 1) {
      alert('Seleccioná al menos un registro');
      return false;
    } else {
      return window.confirm('Estas seguro que deseas realizar esta acción?');
    }
  });

  // captcha reload function
  $('.js-captcha-reload').livequery('click', function() {
    captcha_img_src = $(this).parents('.js-captcha-container').find('.captcha-img').attr('src');
    captcha_img_src = captcha_img_src.substring(0, captcha_img_src.lastIndexOf('/'));
    $(this).parents('.js-captcha-container').find('.captcha-img').attr('src', captcha_img_src + '/' + Math.random());
    if($('.js-captcha-input'))
    {
      $('.js-captcha-input').val('');
    }
    return false;
  });

  $('.js-admin-index-autosubmit').livequery('change', function() {
    if ($('.js-checkbox-list:checked').val() != 1 && $(this).val() >= 1) {
      alert('Seleccioná al menos un registro!');
      return false;
    } else if ($(this).val() >= 1) {
      if (window.confirm('Estas seguro que deseas realizar esta acción?')) {
        $(this).parents('form').submit();
        //Analizamos si estamos liquidando...
        if($('#NowDealMoreActionId').val() == 16)
        {
            //..estamos en una liquidacion, redireccionamos el form
            $(this).parents('form').attr("action", "/admin/deals/liquidar/" );
        }

        //Enviamos el formulario
      }
      else
      {
        $(this).val('');
      }
    }
  });

  $('.js-autosubmit').livequery('change', function() {
    $(this).parents('form').submit();
  });

  //***** For ajax pagination *****//
  $('.js-pagination a').live('click', function() {
    $this = $(this);
    $parent = $this.parents('div.js-response:eq(0)');
    $parent.block();
    $.get($this.attr('href'), function(data) {
      $parent.html(data);
      $parent.unblock();
    });
    return false;
  });

  $('.js-add-friend').live('click', function() {
    $this = $(this);
    $parent = $this.parent();
    $parent.block();
    $.get($this.attr('href'), function(data) {
      $parent.html(data);
      $parent.unblock();
    });
    return false;
  });

  $('.js-friend-delete').live('click', function() {
    _this = $(this);
    if (window.confirm('Estas seguro que deseas '+this.innerHTML.toLowerCase()+'?')) {
      _this.parent().parent('li').block();
      $.get(_this.attr('href'), {}, function(data) {
        container = _this.metadata().container;
        if(container != 'js-remove-friends')
          $('.'+ container).html(data);
        _this.parent().parent('li').unblock();
        _this.parent().parent('li').hide('slow');
      });
    }
    return false;
  });



/* para redimir o des-redimir cupones ************************************************************* */


  $(".js-update-status").livequery('click', function() {
    /*alert($('#butt a').html());
    return false;*/


    $this = $(this);

    $this.block();

    var valor = 0;

    var deal = $this.attr('id');
    var tr_id = $this.attr('id');
    //var nrrc = tr_id.lastIndexOf('r');

    //alert(nrrc);

    for(var i = 0; i < tr_id.length; i++){
         var caracter = tr_id.charAt(i);
        if(caracter == 'r'){
            if(confirm("desredimir")){

               $.post('http://cc_devel.local//deal_users/desredimir', {num: tr_id},function(data){

                  // $("#"+tr_id).addClass("not-used");

                    alert(data);

               });

            }

            return false;
        }

    }

      if(tr_id){

    $('#divForm').show();
    $('#loadForm').load("/deal_users/checkposnet",{deal_user_id : deal} );

        return false;

      }




    if($(this).metadata().divClass == 'js-user-confirmation')
    {
      message = 'Estas seguro que deseas cambiar el estado? Una vez cambiado no puedes deshacer esta acción.'

}
    else
    {
      message = 'Estás seguro que deseas realizar esta acción?';
    }
    if(window.confirm(message )) {
      $this.block();
      $.get($this.attr('href'), function(data) {
        data = data.substring(0,1);
        $class_td = $this.parents('td').attr('class');
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.parents('td').toggleClass('status-1','status-0');
          $this.parents('td').toggleClass('used','not-used');
          $this.parents('td').html('<a href=' + $href + ' title="Redimir" class="not-used js-update-status"></a>');
        }
        else {
          $this.parents('td').html('<a href=' + $href + ' title="Cambiar a no utilizado" class="used js-update-status"></a>');
          $this.parents('td').toggleClass('status-0','status-1');
          $this.parents('td').toggleClass('not-used','used');
        }
        return false;
      });
    }
    else {
      return false;
    }
    return false;
 }
  );

  $(".js-update-show-sold-quantity").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.parents('span').html('<a href=' + $href + ' title="Mostrar la cantidad vendida" class="js-update-show-sold-quantity">Mostrar la cantidad vendida</a>');
        }
        else {
          $this.parents('span').html('<a href=' + $href + ' title="Ocultar la cantidad vendida" class="js-update-show-sold-quantity">Ocultar la cantidad vendida</a>');
        }
        return false;
      });
    return false;
  });
  $(".js-update-wallet-blocked").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.closest('span').html('<a href=' + $href + ' title="Bloquear Monedero" class="js-update-wallet-blocked">Bloquear Monedero</a>');
        }
        else {
          $this.closest('span').html('<a href=' + $href + ' title="Desbloquear Monedero" class="js-update-wallet-blocked">Desbloquear Monedero</a>');
        }
        return false;
      });
    return false;
  });

  $(".js-update-is-visible").livequery('click', function() {
    $this = $(this);
    $this.block();
    $.get($this.attr('href'), function(data) {
        $href = $this.attr('href');
        $this.unblock();
        if(data == '0') {
          $this.closest('span').html('<a href=' + $href + ' title="Mostrar en la lista de Vendedores" class="js-update-is-visible">Mostrar</a>');
        }else {
          $this.closest('span').html('<a href=' + $href + ' title="Ocultar de la lista de Vendedores" class="js-update-is-visible">Ocultar</a>');
        }
        return false;
      });
    return false;
  });



  // For default hide and show
  $('.js-toggle-show').livequery('click', function() {
    $('.' + $(this).metadata().container).slideToggle('slow');
    if($('.' + $(this).metadata().hide_container))
    {
      if($('.' + $(this).metadata().hide_container ).is(':visible'))
        $('.' + $(this).metadata().hide_container ).slideToggle('slow');
    }
    return false;
  });

  // Code to remove the anchor button form Gift view page
  $('#gift_users-view_gift_card .js-cancel-block').hide();

  $('.js-change-action').livequery('change', function(event) {
    var $this = $(this);
    $('.' + $this.metadata().container).block();
    $.get(__cfg('path_relative') + $this.metadata().url + $this.val(), {}, function(data) {
      $('.' + $this.metadata().container).html(data);
      $('.' + $this.metadata().container).unblock();
    });
  });

  $('.js-toggle-div').livequery('click', function() {
    $('.' + $(this).metadata().divClass).toggle('slow');
    return false;
  });

/*
  var calculatePercentage = function() {
    $('#NowDealOriginalPrice').css('border','1px red solid');
    $('#NowDealDiscountedPrice').css('border','1px red solid');
  };

  $('#NowDealOriginalPrice, #NowDealDiscountedPrice').livequery('blur', calculatePercentage);
  $('#NowDealOriginalPrice, #NowDealDiscountedPrice').livequery('change', calculatePercentage);
*/

//$('#NowDealOriginalPrice, #NowDealDiscountedPrice').unbind('blur');
$('#NowDealOriginalPrice, #NowDealDiscountedPrice').bind('blur',function(e){
    if($.isNumeric($(this).attr('value'))){
        $(this).attr('value',parseInt($(this).attr('value'),10));
        $(this).css('border','1px #DDD solid');
    }else if($(this).attr('value') == ""){
        $(this).css('border','1px #DDD solid');
    }else{
        $(this).attr('value','');
        $(this).css('border','1px red solid');
    }
    if($('#NowDealOriginalPrice').attr('value') != "" && $('#NowDealDiscountedPrice').attr('value') != ""){
        if($.isNumeric($('#NowDealOriginalPrice').attr('value')) && $.isNumeric($('#NowDealDiscountedPrice').attr('value'))){
            var op = parseInt($('#NowDealOriginalPrice').attr('value'),10);
            var dp = parseInt($('#NowDealDiscountedPrice').attr('value'),10);

            $('#NowDealOriginalPrice').attr('value',op);
            $('#NowDealDiscountedPrice').attr('value',dp);

            if(op<=dp || op == 0 || dp == 0){
                $('#NowDealOriginalPrice').css('border','1px red solid');
                $('#NowDealDiscountedPrice').css('border','1px red solid');
                $('#NowDealDiscountPercentage').attr('value','');
                $('#NowDealDiscountAmount').attr('value','');
                $('#NowDealSavings').attr('value','');
            }else{
                $('#NowDealOriginalPrice').css('border','1px green solid');
                $('#NowDealDiscountedPrice').css('border','1px green solid');
                $('#NowDealDiscountPercentage').attr('value',(100-parseInt(((dp*100)/op),10)));
                $('#NowDealDiscountAmount').attr('value',(op-dp));
                $('#NowDealSavings').attr('value',((dp-op)*-1));
            }
        }
    }else{
            $('#NowDealDiscountPercentage').attr('value','');
            $('#NowDealDiscountAmount').attr('value','');
            $('#NowDealSavings').attr('value','');
    }
});


  //  var reCalculateDiscount2 = function() {
  //    var original_price = parseFloat($('#NowDealOriginalPrice').val());
  //    var discount_percentage = parseFloat($('#NowDealDiscountPercentage').val());
  //    var discount_amount = parseFloat($('#NowDealDiscountAmount').val());
  //
  //    if (original_price <= 0) {
  //      alert('Ingresa un precio original válido.');
  //    } else if (discount_amount > original_price) {
  //      alert('El monto de descuento debe ser menor que el precio original.');
  //    } else if (discount_amount >= 0) {
  //      savings = discount_amount;
  //      discount_percentage = (savings * 100) / original_price;
  //      $('#NowDealDiscountPercentage').val(isNaN(discount_percentage) ? 0: discount_percentage.toFixed(2));
  //      updaterMask($('#NowDealDiscountPercentage'));
  //      $('#NowDealSavings').val(isNaN(savings) ? 0: savings);
  //      updaterMask($('#NowDealSavings'));
  //      discounted_price = original_price - savings;
  //      $('#NowDealDiscountedPrice, #NowDealCalculatorDiscountedPrice').val(isNaN(discounted_price) ? 0: discounted_price);
  //      updaterMask($('#NowDealDiscountedPrice'));
  //      updaterMask($('#NowDealCalculatorDiscountedPrice'));
  //    }
  //  };

  //$('#NowDealDiscountAmount').livequery('blur', reCalculateDiscount2);
  //$('#NowDealDiscountAmount').livequery('change', reCalculateDiscount2);

  var reCalculateBonus = function() {
    $('#NowDealCalculatorBonusAmount').val($('#NowDealBonusAmount').val());
    updaterMask($('#NowDealCalculatorBonusAmount'));
    $('#NowDealCalculatorCommissionPercentage').val($('#NowDealCommissionPercentage').val());
    updaterMask($('#NowDealCalculatorCommissionPercentage'));
    $('#NowDealCalculatorMinLimit').val($('#NowDealMinLimit').val())
    updaterMask($('#NowDealCalculatorMinLimit'));
    var total_purchased_amount = parseFloat($('#NowDealCalculatorDiscountedPrice').val()) * parseInt($('#NowDealCalculatorMinLimit').val());
    var commission_amount = ($('#NowDealCalculatorCommissionPercentage').val() > 0) ? (parseFloat($('#NowDealCalculatorCommissionPercentage').val()) / 100): 0;
    $('.js-calculator-purchased').html(isNaN(total_purchased_amount) ? 0: total_purchased_amount);
    var total_commission_amount = eval((total_purchased_amount * commission_amount) + parseFloat($('#NowDealCalculatorBonusAmount').val()));
    $('.js-calculator-commission, .js-calculator-net-profit').html((isNaN(total_commission_amount) ? 0: total_commission_amount).toFixed(2));
  };

  $('#NowDealBonusAmount, #NowDealCommissionPercentage, #NowDealMinLimit').livequery('blur', reCalculateBonus );
  $('#NowDealBonusAmount, #NowDealCommissionPercentage, #NowDealMinLimit').livequery('change', reCalculateBonus );
  var reCalculateComission = function() {
    var total_purchased_amount = parseFloat($('#NowDealCalculatorDiscountedPrice').val()) * parseInt($('#NowDealCalculatorMinLimit').val());
    var commission_amount = ($('#NowDealCalculatorCommissionPercentage').val() > 0) ? (parseFloat($('#NowDealCalculatorCommissionPercentage').val()) / 100): 0;
    $('.js-calculator-purchased').html(isNaN(total_purchased_amount) ? 0: total_purchased_amount);
    var total_commission_amount = eval((total_purchased_amount * commission_amount) + parseFloat($('#NowDealCalculatorBonusAmount').val()));
    $('.js-calculator-commission, .js-calculator-net-profit').html((isNaN(total_commission_amount) ? 0: total_commission_amount).toFixed(2));
  };

  $('#NowDealCalculatorDiscountedPrice, #NowDealCalculatorBonusAmount, #NowDealCalculatorCommissionPercentage, #NowDealCalculatorMinLimit').livequery('blur',reCalculateComission);
  $('#NowDealCalculatorDiscountedPrice, #NowDealCalculatorBonusAmount, #NowDealCalculatorCommissionPercentage, #NowDealCalculatorMinLimit').livequery('change',reCalculateComission);
  $('.js-quantity').livequery('keyup',function() {
    updateJsQuantity();
    checkShowCuotas();
  });

  $('.js-buy-confirm').live('click', function() {
    if(($('#NowDealTerms').length>0) && !$('#NowDealTerms')[0].checked) {
      alert('Para continuar con el proceso de compras debes aceptar los Términos y Condiciones.');
      return false;
    }
    return window.confirm('Al hacer click en PAGAR serás redirigido a una página de pago seguro para proseguir tu compra. Si en cambio elegiste pagar con tu cuenta el monedero, el saldo será deducido inmediatamente. ¿Querés continuar?');
  });


  $('.js-register-form').livequery(function() {
    $.ajax( {
      type: 'GET',
      url: 'http://j.maxmind.com/app/geoip.js',
      dataType: 'script',
      cache: true,
      success: function() {
        $('#CityName').val(geoip_city());
        $('#StateName').val(geoip_region_name());
        $('#country_iso_code').val(geoip_country_code());
      }
    });
  });

  $('.js_company_profile').livequery('click', function() {
    $('.js-company_profile_show').toggle();
  });

  $('.js-invite-all').livequery('change',function(){
    $('.invite-select').val($(this).val());
  });

  $('.js-truncate').livequery(function() {
    var $this = $(this);
    $this.truncate(100, {
      chars: /\s/,
      trail: ["<a href='#' class='truncate_show'>" + __l(' more', 'en_us') + "</a> ... ", " ...<a href='#' class='truncate_hide'>" + __l('less', 'en_us') + "</a>"]
    });
  });

  $.cookie("cookie_test",'testing',{
    path:__cfg('path_relative')
  });

  var lastCuotasSelected = '1';
  $('.js-payment-type').livequery('click',function() {
    updateJsQuantity();
    checkShowCuotas();
  });

  $('.js-payment-type:first').attr('checked',1);

  $('#CompanyAddress1 , #CityName').livequery('blur', function() {
    if ($('#CompanyAddress1').val() != '' || $('#CityName').val() != '') {
      if($('#CompanyAddress1').val() != '' && $('#CityName').val() != '') {
        var address = $('#CompanyAddress1').val() + ', ' + $('#CityName').val();
      } else {
        if($('#CompanyAddress1').val() != '') {
          var address = $('#CompanyAddress1').val()
        }
        else if( $('#CityName').val() != '') {
          var address = $('#CityName').val();
        }
      }

      $('.show-map-block').show();
      geocoder = new GClientGeocoder();
      geocoder.getLatLng(address, function(point) {
        if ( ! point) {
          $('#js-map').html('Google Maps no puede encontrar tu localización, por favor intenta con otra localización.');
          $('#latitude').value='';
          $('#longitude').value='';
        } else {
          $('#js-map').fshowmap(point['y'], point['x'], true);
        }
      });
    }
  });

  $('#js-map').livequery(function() {
    var y = $('#latitude').val();
    var x = $('#longitude').val();
    $('#js-map').fshowmap(y, x, true);
  });
/*
  $.address.init(function(event) {
    $this = $(this);
    $('div.js-mystuff-tabs').tabs({
      // Content filter
      load: function(event, ui) {
        $this.next('.ui-tabs-panel').html($(ui.panel).html());
        if(typeof(replace_sIFR_elements) != "undefined") replace_sIFR_elements();
      },
      selected: $('.js-mystuff-tabs ul:first a').index($('a[rel=address:' + event.value + ']')),
      fx: {
        opacity: 'toggle'
      }
    }).css('display', 'block');
  }).externalChange(function(event) {
    if (event.value == '/') {
      $('.js-mystuff-tabs').tabs('select', 0);
    } else {
      // Select the proper tab
      setTimeout(function(){
        $('.js-mystuff-tabs').tabs('select', $('a[rel=address:' + event.value + ']').attr('href'));
        $('.js-mystuff-tabs').find('li').removeClass('ui-state-hover');
      }, 1400);

    }

  });
*/
  $(".js-accordion").accordion({
    header: "h3",
    autoHeight: false,
    active: false,
    collapsible: true
  });

  $("h3", ".js-accordion").click(function(e) {
    var contentDiv = $(this).next("div");
    if(!contentDiv.html().length)
    {
      $this=$(this);
      $this.block();
      $.get($(this).find("a").attr('href'), function(data) {
        contentDiv.html(data);
        $this.unblock();
      });
    }
  });

  $('.js_company_profile_enable').livequery('change',function(){
    if($('.js_company_profile_enable:checked').length)
    {
      $('.js-company_profile_show').show();
    } else {
      $('.js-company_profile_show').hide();
    }
  })

  $('#csv-form').livequery('submit',
    function(e) {
      var $this = $(this);
      var ext = $('#AttachmentFilename').val().split('.').pop().toLowerCase();
      var allow = new Array('csv','txt');
      if(jQuery.inArray(ext, allow) == -1) {
        $('div.error-message').remove();
        $('#AttachmentFilename').parent().append('<div class="error-message">Extensión inválida, solo csv y txt están permitidas.</div>');
        return false;
      }
    });





  var headerTopMenuItems = $('#header .top_frame ul li:not(.left,.right,#greet)');
  var headerTopMenuSeparators = 0;

  $('#header .top_frame ul li:not(.left,.right,#greet)').reverse().each(function() {
    if(headerTopMenuItems.length > 1 && (headerTopMenuSeparators < (headerTopMenuItems.length - 1))) {
      $(this).before('<li class="separator"></li>');
      headerTopMenuSeparators++;
    }
  });

 // $('#header .top_frame ul li.submenu > a').each(function() {
    $('#other_cities').click(function() {
      var target = $('#desplegable_ciudades_nuevo');

      if(target.css('display') == 'none') {
        target.slideDown("slow");
      } else {
        target.slideUp("slow");
      }

      $(this).toggleClass('opened');
    });
  //});







  /* not full width for debug toolbar */
  if($('#debug-kit-toolbar .panel-tab:not(.icon)').length) {
    $('#debug-kit-toolbar .panel-tab:not(.icon)').toggle();
  }

  updateJsQuantity();
  checkShowCuotas();
}); // $(document).ready ends

$.fn.reverse = function() {
  return this.pushStack(this.get().reverse(), arguments);
};



function checkCompanyPaymentMethods(){
    var companyID = $("#NowDealCompanyId").val();

    $.ajax({
        type: 'POST',
        url: 'http://'+document.location.hostname+'/now/now_companies/list_companies_data/', //cambiar a una URL bien
        data:{compID: companyID},

        //El controller nos devuelve 4 valores posibles, 1 para exito, 0, para falla de total, 2 para solo cuenta existente, 3 para solo cheque habilitado
        success:function(data){
            if(data==1){
                //habilitar el select
                $('#NowDealPaymentMethod').attr('disabled', false);
                $('#medios_pago_alerta').html('');
            }else if(data==0 || data==2 || data==3){
                //deshabilito el select de metodos de pago, y vuelvo su valor a ''
                $('#NowDealPaymentMethod').attr('disabled', true);
                $("#NowDealPaymentMethod").val('');
                $('#medios_pago_alerta').html('La empresa seleccionada no permite elegir el medio de pago');
            }
        },

        //hubo un error de algun tipo, notificamos
        error:function(){
            $('#medios_pago_alerta').html('Hubo un problema al verificar los datos de la empresa.');
        },

        timeout: 5000

    });

}
