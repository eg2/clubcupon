<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h1 class="h1-format">P&Aacute;GINA NO ENCONTRADA</h1>

            <!-- Contenidos -->

            <p>La p&aacute;gina solicitada no se encuentra disponible.</p>
            <p>
                <?php echo $html->link('Podes ver ofertas disponibles!', array('plugin' => 'now' , 'controller' => 'now_deals', 'action' => 'index'), array('id' => 'bt_volver_big', 'title' => 'VOLVER')); ?>
            </p>

            <!-- / Contenidos -->

        </div>
    </div>
</div>