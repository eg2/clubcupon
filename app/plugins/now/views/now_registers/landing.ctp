<div id="ccnow" class="landingp">

	<div class="subhead">
		<div  class="container_16 center clearfix">
				<div  class="grid_16">
					<div class="anuncioya">
						<img src="/img/now/landing-img/logoyaanuncio.png" />
						<h2>Cada d&iacute;a nuevos compradores buscan ofertas 
								de gastronom&iacute;a, entretenimiento y servicios 
								cerca de sus casas o sus lugares de trabajo.
						</h2>
						<p><span>Club Cup&oacute;n YA!</span> te permite crear ofertas instant&aacute;neas cuando m&aacute;s las necesit&aacute;s para atraer a estos nuevos clientes. 
								Los usuarios que se encuentren cerca de alguno de tus locales podr&aacute;n acceder 
								de forma inmediata a tu oferta tanto navegando desde su m&oacute;vil como en la web, comprarla y canjearla en el momento.
						</p>
						<h3>Ganar&aacute;s clientes de tu zona y con ello aumentar&aacute;n las probabilidades de que estos compradores vuelvan.</h3>
						<span class="linea-a"></span>
					</div>
				</div>
		</div>
	</div>	
	
	<div id="main" class="container_16 main_frt paso3addofertas adds-suc">
				<div  class="grid_16 maincentro">
					
					<div class="bloque-general-a bloque-1">
						<h3>Registro</h3>
						<p>Para acceder a publicar ofertas en Club Cup&oacute;n YA! deber&aacute;s crear una cuenta como empresa. Tus datos ser&aacute;n validados y aprobados y podr&aacute;s comenzar a gestionar tus propias ofertas. Si ya trabaj&aacute;s con nosotros simplemente deber&aacute;s cargar tus sucursales y crear tus ofertas instant&aacute;neas.</p>
						<span></span>
						<img src="/img/now/landing-img/ico1.png" />
						<span class="linea-b"></span>
					</div>
	
					<div class="bloque-general-a bloque-2">
						<h3>Publicaci&oacute;n</h3>
						<p>Para comenzar deber&aacute;s agregar las sucursales 
								con las que cuentes y crear una oferta atractiva
								que satisfaga las necesidades de tu negocio y atraiga a los clientes cercanos. Eleg&iacute; qu&eacute; ofrecer y cu&aacute;ndo publicar tus ofertas. Un ejecutivo de cuentas podr&aacute; asistirte en este proceso.
						</p>
						<span></span>
						<img src="/img/now/landing-img/ico2.png" />
						<span class="linea-b"></span>
					</div>	
					
					<div class="bloque-general-b bloque-3">
						<h3>Ofertas</h3>
						<p>Los compradores que se encuentren cerca de alguno de sus locales podr&aacute;n acceder de forma inmediata 
								a tu oferta tanto por su m&oacute;vil como en la web. Podr&aacute;n comprarla y recibir&aacute;n su c&oacute;digo de descuento para canjearla en el momento.
						</p>
						<span></span>
						<img src="/img/now/landing-img/ico3.png" />
						<span class="linea-b"></span>
					</div>
	
					<div class="bloque-general-b bloque-4">
						<h3>Canje de Cupones</h3>
						<p>Los clientes se acercar&aacute;n a la sucursal 
								m&aacute;s cercana con su c&oacute;digo de descuento para canjear sus cupones. Podr&aacute;s redimir los cupones v&iacute;a web 
								desde tu cuenta de empresa o ingresar 
								el c&oacute;digo a trav&eacute;s del servicio de posnet.
						</p>
						<span></span>
						<img src="/img/now/landing-img/ico4.png" />
						<span class="linea-b"></span>
					</div>	
					
					<div class="bloque-general-b bloque-5">
						<h3>Ganancia</h3>
						<p>Recibir&aacute;s el pago por los cupones redimidos directamente en la cuenta que hayas ingresado al registrarte. Los cupones no redimidos durante la validez de la oferta ser&aacute;n devueltos a los compradores.</p>
						<span></span>
						<img src="/img/now/landing-img/ico5.png" />
						<span class="linea-b"></span>
					</div>		
					
				</div>	
				
				<div  class="grid_16">
				<h4 class="suoferta"><strong>As&iacute; se ver&aacute; tu oferta</strong></h4>
					<img src="/img/now/landing-img/popup-preview.jpg" />
					<div class="linear">
						<ul  class="bloque-general-c">
							<li class="refer">Referencias:</li>
							<li class="item1"><strong>Mapa: </strong>El usuario podr&aacute; localizar r&aacute;pidamente el local m&aacute;s cercano al lugar en el que se encuentra.</li>
							<li class="item2"><strong>T&iacute;tulo: </strong>Contiene un resumen de la oferta.</li>
							<li class="item3"><strong>Horario de Canje: </strong>Indica al usuario en que franja horaria podr&aacute; utilizar su cup&oacute;n.</li>
							<li class="item4"><strong>Precio: </strong>Indica el precio real, el precio promocional y el descuento aplicado.</li>
							<li class="item5"><strong>Condiciones: </strong>Contiene informaci&oacute;n detallada de la oferta.</li>
						</ul>
									<span class="linea-b"></span>		
					</div>
				</div>	
				
				<div  class="grid_16" style="">
					<div class="btn-cont clearfix" style="">
					    <div class="anuncioya2">
					    <?php if($btn_visible['nueva_empresa'] || $btn_visible['empresa_registrada']){?>
					    	<h3>Empez&aacute; a publicar en Club Cup&oacute;n YA!</h3>
					    <?php }?>
					    </div>
					    <?php
					    	$marginleft='100px';
					    	if(!$btn_visible['nueva_empresa'] && $btn_visible['empresa_registrada'])
					    		$marginleft='185px';
					    	if($btn_visible['nueva_empresa'] && !$btn_visible['empresa_registrada'])
					    		$marginleft='200px';
				    	
					    ?>
					    <div  style=" float: left; margin-left:<?php echo $marginleft;?>">
					    <?php if($btn_visible['nueva_empresa']){?>
							<a href="<?php echo $btn_url['nueva_empresa_url'];?>">Nueva Empresa</a>
						<?php }?>
						<?php if($btn_visible['empresa_registrada']){?>
							<a href="<?php echo $btn_url['empresa_registrada_url'];?>">Empresa registrada</a>
						<?php }?>
						</div>
					</div>
					<div class="maincentro list-preguntas margin" >
						<ul>
							<li class="cust-li"><div class="btn-listtip clearfix" style="margin-left:220px;float:left"><p>&iquest;Ten&eacute;s m&aacute;s consultas?</p><a class="btn-azul-d" href="mailto:empresas@clubcupon.com.ar">Contactanos</a></div><div class="anuncioya2" style="float:left">
							
							<?php echo $html->link ('Preguntas Frecuentes', array ('controller' => 'now_registers', 'plugin'=>'now','action' => 'faq' ), array('escape' => false)); ?>
							
							</div></li>
						</ul>
					</div>
				</div>
	</div> <!-- CIERRE MAIN-->
	
</div>