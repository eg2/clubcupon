	<!--************************************************************ CONTENIDO ************************************************************-->

	<section id="banner-us">
		<a href="http://dmds-cmd.planisys.net/suscripcion2.php?id=7ced307f-069b-d94f-9a62-52f3d168e2dc" target="_blank"><img src="/img/web/clubcupon/banner_ccya_2.jpg" alt="banner"></a>
	</section>

	<section id="como_funciona">
            <p><span>Club Cup&oacute;n YA! es la nueva forma de encontrar ofertas cercanas que podés comprar y utilizar en el día.</span>
		Si buscás un lugar para comer, organizar una salida en <span>el momento</span> o utilizar un servicio cerca de tu casa o trabajo, simplemente ingresá a <span>Club Cupón Ya!</span> a través de la web o navegando desde de tu móvil, elegí la oferta, comprala y <span>disfrutala en el día.</span></p>
		<h1>¿Cómo funciona?</h1>
		<div id="pasos_comprar">
			<ul>
				<li>
					<h2>1</h2>
					<h3>Elegí la oferta</h3>
					<p>Navegá Club Cupón Ya en tu dispositivo móvil o desde la web. Buscá y encontrá las ofertas más cercanas a tu ubicación.</p>
				</li>
				<li>
					<h2>2</h2>
					<h3>Comprala</h3>
                                        <p>Elegí la oferta y comprala. Recibir&aacute;s tu código de descuento en el momento.</p>
				</li>
				<li>
					<h2>3</h2>
					<h3>Disfrutala en el momento</h3>
					<p>Canjeá y disfrutá de la oferta mostrando tu código desde tu Smartphone.</p>
				</li>
			</ul>
			<img src="/img/web/clubcupon/pc_cf.jpg" alt="pc mobil">
		</div>

		<h1>Dividimos los barrios en grupos para estar más cerca tuyo</h1>
		<div id="listado_zonas">
			<ul>
				<li class="sector_title">Norte</li>
				<li>Belgrano</li>
				<li>Colegiales</li>
				<li>Villa Urquiza</li>
				<li>Villa Pueyrredón</li>
				<li>Saavedra</li>
				<li>Coghland</li>
				<li class="last_item">Nuñez</li>

				<li class="sector_title">Noreste</li>
				<li>Palermo</li>
				<li>Recoleta</li>
				<li>Villa Crespo</li>
				<li>Paternal</li>
				<li>Chacarita</li>
				<li>Villa Ortuzar</li>
				<li>Parque Chas</li>
				<li>Agronomía</li>
			</ul>

			<ul>
				<li class="sector_title">Sureste</li>
				<li>Retiro</li>
				<li>Balvanera</li>
				<li>San Nicolas</li>
				<li>Monserrat</li>
				<li>Constitución</li>
				<li>Puerto Madero</li>
				<li>San Cristobal</li>
				<li>Parque Patricios</li>
				<li>Barracas</li>
				<li class="last_item">Boca</li>

				<li class="sector_title">Centro</li>
				<li>Nueva Pompeya</li>
				<li>Almagro</li>
				<li>Caballito</li>
				<li>Flores</li>
				<li>Parque Chababuco</li>
				<li>Boedo</li>
			</ul>

			<ul>
				<li class="sector_title">Suroeste</li>
				<li>Liniers</li>
				<li>Mataderos</li>
				<li>Parque Avellaneda</li>
				<li>Villa Soldati</li>
				<li>Villa Lugano</li>
				<li class="last_item">Villa Riachuelo</li>

				<li class="sector_title">Oeste</li>
				<li>Villa Gral. Mitre</li>
				<li>Villa Santa Rita</li>
				<li>Villa del Parque</li>
				<li>Villa Devoto</li>
				<li>Villa Real</li>
				<li>Monte Castro</li>
				<li>Versalles</li>
				<li>Villa Luro</li>
				<li>Velez Sarfield</li>
				<li>Floresta</li>
			</ul>

			<img src="/img/web/clubcupon/mapa.jpg" alt="mapa">
		</div>
		
	</section>

	<section id="descarga_app">
		<div>
			<h3>Descarga tu aplicación</h3>
			<ul>
				<li><a href="https://itunes.apple.com/ar/app/clubcupon/id603096819?ls=1&mt=8" target="_blank"><img src="/img/web/clubcupon/btn_ipad.jpg" alt="descarga ipad iphone"></a></li>
				<li><a href="https://play.google.com/store/apps/details?id=com.cmd.clubcupon" target="_blank"><img src="/img/web/clubcupon/btn_android.jpg" alt="descarga android"></a></li>
				<li><a href="http://appworld.blackberry.com/webstore/content/24574893/?lang=es&countrycode=AR" target="_blank"><img src="/img/web/clubcupon/btn_bb.jpg" alt="descarga blackberry"></a></li>
			</ul>
		</div>
	</section>

	<a href="/search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Club Cupón Ya &scV=-page--1-query--oooo" class="btn-naranja-landingus btn_fix">Ver ofertas</a>

