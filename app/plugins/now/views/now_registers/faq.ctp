<div id="ccnow" class="landingp">

	<div class="subhead pregfrec">
		<div  class="container_16 center clearfix">
				<div  class="grid_16">
					<div class="anuncioya">
						<img src="/img/now/landing-img/logominipr.jpg" />
						<h2>Preguntas Frecuentes</h2>
						<span class="linea-a"></span>
					</div>
				</div>
		</div>
	</div>	
	
	<div id="main" class="container_16 main_frt paso3addofertas adds-suc">
				<div  class="grid_16 maincentro list-preguntas">
				<h2 class="pfempresas">Preguntas Frecuentes Empresas</h2>
					<ul>
						<li><h3>&iquest;C&oacute;mo funciona Club Cup&oacute;n Ya?</h3>
								Club Cup&oacute;n YA te permite crear ofertas instant&aacute;neas cuando m&aacute;s las necesita para atraer a nuevos compradores que, todos los d&iacute;as, buscan ofertas cercanas. Podr&aacute;s elegir qu&eacute; ofrecer y cu&aacute;ndo publicarla.
						</li>
						
						<li><h3>&iquest;En qu&eacute; se diferencian las ofertas tradicionales de Club Cup&oacute;n y las ofertas de Club Cup&oacute;n YA? </h3>
								Las ofertas tradicionales de Club Cup&oacute;n y las ofertas de Club Cup&oacute;n YA son la combinaci&oacute;n ideal para promocionar tu comercio. 
								<br /><br />
								La modalidad de ofertas tradicionales ponen tu comercio al alcance de miles de potenciales clientes mientras 
								que Club Cup&oacute;n YA te acerca a los compradores activos de tu zona incrementando la posibilidad de que estos compradores vuelvan.
						</li>
						
						<li><h3>&iquest;Qu&eacute; pasos debo realizar para publicar una oferta?</h3>
								Para publicar una oferta deb&eacute;s estar registrado como empresa. Una vez validados tus datos deber&aacute;s cargar tus sucursales y podr&aacute;s comenzar a publicar. Nuestro equipo comercial podr&aacute; asistirte en programaci&oacute;n de tus ofertas.
						</li>
						
						<li><h3>&iquest;C&oacute;mo encuentran y compran los usuarios mis ofertas?</h3>
								Los usuarios que se encuentren cerca de alguno de tus locales podr&aacute;n acceder de forma inmediata a tu oferta tanto desde su m&oacute;vil como desde la web, comprarla y canjearla en el momento.
						</li>
						
						<li><h3>&iquest;Qu&eacute; sucede si el comprador no canjea su cup&oacute;n dentro del periodo de validez?</h3>
								Si un cliente no canjea su cup&oacute;n dentro del periodo de validez de la oferta Club Cup&oacute;n le reembolsa el monto de la compra como cr&eacute;dito en su cuenta de Club Cup&oacute;n para utilizar en futuras ofertas.
						</li>
						
						<li><h3>&iquest;Puedo publicar m&aacute;s de una oferta al mismo tiempo? </h3>
								S&iacute;, pod&eacute;s programar diferentes ofertas para ejecutar en distintos momentos del d&iacute;a o en diferentes d&iacute;as de la semana.
						</li>
						
						<li><h3>&iquest;Cu&aacute;ndo se publican las ofertas que cargo? </h3>
								Las ofertas ser&aacute;n validadas y publicadas el mismo d&iacute;a del canje hasta finalizar su periodo de validez o hasta agotar el 
								m&aacute;ximo de cupones permitidos.
						</li>
					</ul>
	
					<!--div class="btn-cont clearfix">
						<a href="/now/now_registers/landing">Conoc&eacute; m&aacute;s de Club Cup&oacute;n YA!</a>
						<a href="/now/now_registers/gotopublishya">Empez&aacute; a publicar en Club Cup&oacute;n YA!</a>
					</div>
					<div class="maincentro list-preguntas margin">
						<ul>
							<li class="cust-li"><div class="btn-listtip clearfix"><p>&iquest;Ten&eacute;s m&aacute;s consultas?</p><a class="btn-azul-d" href="mailto:empresas@clubcupon.com.ar">Contact&aacute;nos</a></div></li>
						</ul>
					</div-->
					
					
					<div  class="grid_16" style="">
						<div class="btn-cont clearfix" style="">
						
					    	<div class="anuncioya2">
					    	<?php if($btn_visible['nueva_empresa'] || $btn_visible['empresa_registrada']){?>
					    	<h3>Empez&aacute; a publicar en Club Cup&oacute;n YA!</h3>
					    	 <?php }?>
					    	</div>
					    	 <?php
								  	$marginleft='100px';
								   	if(!$btn_visible['nueva_empresa'] && $btn_visible['empresa_registrada'])
								    		$marginleft='185px';
								   	if($btn_visible['nueva_empresa'] && !$btn_visible['empresa_registrada'])
								    		$marginleft='200px';
							    	
							  ?>
							<div  style=" float: left; margin-left:<?php echo $marginleft;?>">
					    		<?php if($btn_visible['nueva_empresa']){?>
					    		<a href="<?php echo $btn_url['nueva_empresa_url'];?>">Nueva empresa</a>
					    		<?php }?>	
					    		<?php if($btn_visible['empresa_registrada']){?>
					    		<a href="<?php echo $btn_url['empresa_registrada_url'];?>">Empresa registrada</a>
					    		<?php }?>
					    	</div>
					    	
						</div>
						<div class="maincentro list-preguntas margin" >
						<ul>
							<li class="cust-li"><div class="btn-listtip clearfix" style="margin-left:220px;float:left"><p>&iquest;Ten&eacute;s m&aacute;s consultas?</p><a class="btn-azul-d" href="mailto:empresas@clubcupon.com.ar">Contactanos</a></div><div class="anuncioya2" style="float:left">
							
							<?php echo $html->link ('Conoc&eacute; m&aacute;s de Club Cup&oacute;n YA!', array ('controller' => 'now_registers', 'plugin'=>'now','action' => 'landing' ), array('escape' => false)); ?>
							
							</div></li>
						</ul>
						</div>
					</div> 
					
					
				</div>
	</div> <!-- CIERRE MAIN-->

</div>
