<div id="main" class="container_16 main_frt">
    <div class="grid_16">
        <div class="bloque-home clearfix">
        <h2>Registrarse</h2>
            
            <!-- Contenidos -->
            <div class="box" id="content">
<?php echo $form->create (null, array ('action'=>'index','autocomplete' => 'off')); ?>
<?php
      echo '<div class = "register">';
      echo '  <a name = "register_anchor"  id = "register_anchor"></a>';
      echo '  <h4 class = "subtitle">Registrarse</h4>';
      echo '  <p>Si no sos miembro de <strong>Club Cup&#243;n</strong> registrate aqu&#237;. Seleccion&#225; el m&#233;todo de pago y continu&#225; con la compra.</p>';
      echo '  <small>Los campos marcados con un asterisco (<sup>*</sup>) son obligatorios</small>';
      //echo $form->input ('User.username', array ('label' => __l('Username')));
      unset ($this->validationErrors ['User']['username']);
      echo $form->input ('User.email', array ('label' => __l('Mail')));
      echo $form->input ('User.confirm_email', array ('label' => __l('Confirmar mail')));
      //echo $form->input ('UserProfile.dni', array ('label' => __l('DNI')));
      echo $form->input ('User.passwd', array ('label' => __l('Password')));
      unset ($this->validationErrors ['User']['passwd']);
      echo $form->input ('User.confirm_password', array ('type' => 'password', 'label' => __l('Password Confirmation')));
	  
		echo $form->input('User.is_agree_terms_conditions', array('type' => 'checkbox', 'checked' => 'checked', 'label' => sprintf('He le&iacute;do y acepto los %s del servicio', $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false),array('target' => '_blank', 'escape' => false)))));
		echo $form->input('Now.is_agree_terms_conditions', array('type' => 'checkbox', 'checked' => 'checked', 'label' => sprintf('He le&iacute;do y acepto los %s de ClubCupon Now', $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false),array('target' => '_blank', 'escape' => false)))));
		//echo $form->input('User.is_agree_daily_subscriptions', array('type' => 'checkbox', 'checked' => 'checked', 'label' => 'S&iacute;, quiero recibir emails con las ofertas diarias de Club Cup&oacute;n'));
      echo '</div>';
    ?>
<?php echo $form->end ("Registrarse!"); ?>

            </div>
            <!-- / Contenidos -->


        </div>
    </div>
</div>