<style>
	#ccnow .contenido-form .grupo-campos span.info {
		margin: 6px 180px 6px 10px;
	  	float: right;
	}
</style>

<?php 
	$dataObj = json_decode($data);
	$registerOk = $dataObj->trackingEvent->registerOk;
?>

<div id="ccnow">

		<?php echo $form->create('CompanyCandidate', array('url' => array('plugin'=>'now', 'controller' => 'now_registers', 'action' => 'register_company'), 'type' => 'file','class'=>'clearfix'));?>
			<?php echo $form->input('CompanyCandidate.user_id' ,array ('type'  => 'hidden','value' => $user_id)); ?>
			<!-- div class="grid_16 mb10px">
				<div class="logonewccnow clearfix">
					<ul class="pasos clearfix">
						<li class="li1">1</li>
						<li class="li2">2</li>
						<li class="li3 select">3</li>
					</ul>
				</div>
			</div -->

			<div class="">
				<div class="contenido-form" id="tool">
						<?php if(($this->data['CompanyCandidate']['message'] != null) && $this->data['CompanyCandidate']['state'] == 0){?>
						<h2>Mensaje del Moderador (<?php echo date("d/m/Y H:i",strtotime($this->data['CompanyCandidate']['modified']));?>)</h2>
							<div class="grupo-campos clearfix">
								<?php echo $this->data['CompanyCandidate']['message'];?>
							</div>
						<?php }?>
						
				<?php if(isset($has_change_password) && $has_change_password) { ?>
						<div class="modcuenta">
						<?php //echo $html->link('Modificar contrase&ntilde;a',array('plugin'=>'','controller'=>'users','action'=>'change_password'), array('escape'=>false));?>
						</div>
	            <?php } ?>
			    
			   
					<!-- Cuenta-->
						<h2>Cuenta</h2>
						
						<div class="grupo-campos clearfix">
							<label>N&uacute;mero de Comercio</label>
							<div class="editable"><?php echo $numero_comercio;?></div>
						</div>
						<div class="grupo-campos clearfix">
							<label>Nombre de Fantas&#237;a<span>*</span></label>
							<?php echo $form->input('name',array ('label' => false,'div'=>false,'error'=>array('class'=>'sshoww')));?>
						</div>
						<div class="grupo-campos clearfix">
							<label>Usuario<span>*</span></label>
							<div class="editable"><?php echo $user_name;?></div>
						</div>
						<div class="grupo-campos clearfix">
							<label>contrase&ntilde;a<span>*</span></label>
							<?php echo $html->link('Modificar',array('plugin'=>'','controller'=>'users','action'=>'change_password'), array('escape'=>false,'style'=>'float:left;padding:5px'));?>
						</div>

						<span class="camp-obl" style="border-bottom:none">* Campos Obligatorios</span>
						<?php
							echo $form->input('latitude' ,array('type' => 'hidden', 'id'=>'latitude','value' =>'0'));
							echo $form->input('longitude',array('type' => 'hidden', 'id'=>'longitude','value' =>'0'));
							echo $form->input('cheque_noorden',array('type' => 'hidden', 'id'=>'longitude','value' =>Configure::read("NowPlugin.candidate.noalaorden")));
							echo $form->input('CompanyCandidate.user_id' ,array ('type'  => 'hidden','value' => $user_id));
						?>
                                                
						<?php if(isset($this->data['CompanyCandidate']['state']) && @$this->data['CompanyCandidate']['state'] == ConstCandidateStatuses::Pending){ ?>
                                                        <div id="msge-error" class="clearfix">
                                                            <p  class="info3 mensajes">Tus datos de empresa ser&aacute;n revisados. Te informaremos por mail el estado de tu solicitud.</p>
                                                        </div>
						<?php }elseif(isset($this->data['CompanyCandidate']['state']) && @$this->data['CompanyCandidate']['state'] == ConstCandidateStatuses::Reject){ ?>
                                                        <div id="msge-error" class="clearfix">
							<!-- ¡Tu Empresa ya fue rechazada, no se puede modificar! -->
                                                        <p class="error3 mensajes">Te informamos que tus datos de empresa no han sido aprobados.<br/>
                                                        <?php echo $this->data['CompanyCandidate']['message'] ?></p>
                                                        </div>
                                                <?php } elseif (!isset($this->data['Company'])) { ?>
							<?php if(isset($this->data['CompanyCandidate']['id'])){ ?>
							<div style="text-align:center;">
								<?php echo $form->submit('Guardar Borrador',array('name'=>'data[accion]','style'=>'display:inline;','class'=>'camp-obl-submit btn-azul','div'=>false)); ?>
								<?php echo $form->submit('Enviar a Moderación',array('name'=>'data[accion]','style'=>'display:inline;','class'=>'camp-obl-submit btn-azul','div'=>false)); ?>
							</div>
							<?php }else{ ?>
								<?php echo $form->submit('Continuar',array('name'=>'data[accion]','class'=>'camp-obl-submit btn-azul')); ?>
							<?php } ?>
						<?php } ?>
				<!-- Datos fiscales-->
						<h2>Datos Fiscales</h2>
						<div class="grupo-campos clearfix">
							<label>Raz&#243;n Social<span>*</span></label>
							<?php echo $form->input('fiscal_name',array('label' => false,'div'=>false,'error'=>array('class'=>'sshoww'))); ?>
						</div>

						<div class="grupo-campos clearfix">
							<label>Domicilio<span>*</span></label>
							<?php echo $form->input('fiscal_address', array('label' => false,'div'=>false,'error'=>array('class'=>'sshoww'), 'info' => __l('Por ejemplo: Rivadavia 2345, Piso 5, Oficina 28')));?>
						</div>

						<div class="grupo-campos clearfix">
							<label>Localidad / Barrio<span>*</span></label>
							<?php echo $form->input('CompanyCandidate.fiscal_city_id', array('div'=>false, 'label' => false,'error'=>array('class'=>'sshoww')));?>
						</div>
						
						<div class="grupo-campos clearfix">
							<label>Provincia<span>*</span></label>
							<?php echo $form->input('CompanyCandidate.fiscal_state_id', array('div'=>false, 'label' => false,'error'=>array('class'=>'sshoww')));?>
						</div>
						
						
						<div class="grupo-campos clearfix">
							<label>Tel&eacute;fono<span>*</span></label>
							<?php echo $form->input('fiscal_phone',array('label' => false,'div'=>false,'error'=>array('class'=>'sshoww'), 'info' => __l('Ejemplo: 01123456789'))); ?>
						</div>

           
						<div class="grupo-campos clearfix">
							<label>CUIT<span>*</span></label>
							<?php echo $form->input('fiscal_cuit',array('label' => false,'div'=>false,'class'=>'cuit','error'=>array('class'=>'sshoww'), 'info' => __l('Ingresar los números sin guiones, ni espacios.')));?>
						</div>
						
						<div class="grupo-campos clearfix">
							<label>Nro. Inscripci&#243;n IIBB</label>
							<?php echo $form->input('fiscal_iibb',array('label' => false,'div'=>false,'error'=>array('class'=>'sshoww'), 'info' => __l('Ingresar los números sin guiones, ni espacios.')));?>
						</div>
						
						<div class="grupo-campos clearfix">
							<label>Percepci&oacute;n frente a IIBB<span>*</span></label>
							<?php echo $form->input('percepcion_iibb_caba',array('label' => false,'div'=>false,'error'=>array('class'=>'sshoww'), 'info' => __l('Ingresar los números sin guiones, ni espacios.'),'type' => 'select', 'options' => array(''=>'','1' => 'Exento', '0' => 'No Inscripto', '2'=>'Inscripto')));?>
						</div>

						<div class="grupo-campos clearfix">
							<label>Condici&#243;n frente al IVA<span>*</span></label>
							<?php echo $form->input('fiscal_cond_iva', array('label'=>false,'div'=>false,'error'=>array('class'=>'sshoww'),'type'=>'select', 'options'=>array(''=>'','monotributo' => 'Monotributo', 'exento' => 'Exento', 'ri' => 'Responsable Inscripto')));?>
						</div>

						
						

           

	
			
				</div>
			</div>
		<?php echo $form->end();?>

</div>

<script type="text/javascript">
$(document).ready(function(){
	$("div.sshoww").prev("span.info").hide();
	$(".ajax").colorbox({close: 'Cerrar'});

	$("input[type='text']").each(function(i,o){
		if($(o).attr('value')=="") return;
		if($(o).parent().find('.sshoww').size()) return;
		div = $("<div class='editable'>" + $(o).attr('value') + "</div><a href='#' class='editme'>Modificar</a>");
		$(o).next('span').hide()
		$(o).after(div).hide();
	});

	$('select').each(function(i,o){
		if($(o).find('option:selected').html()=="") return;
		div = $("<div class='editable'>" + $(o).find('option:selected').html() + "</div><a href='#' class='editme'>Modificar</a>");
		$(o).next('span').hide()
		$(o).after(div).hide();
	});

	$('.editme').click(function(e){
		e.preventDefault();
		$(this).parent().find('input').show();
		$(this).parent().find('select').show();
		$(this).parent().find('span.info').show();
		$(this).parent().find('.editable').hide();
		$(this).parent().find('.editme').hide();

		$(this).parent().find('input').focus();
		$(this).parent().find('input').click();
	});

	$("input[type='file']").each(function(i,o){
		if($(o).parent().find('.editable').size()){
			$(o).parent().find('input,span.info').hide();
		}
	});

	$('input,select').live('change',function(e){
		if($(this).parent().find('div.editable').size()){
			if($(this).find('option:selected').size()){
				$(this).parent().find('div.editable').html($(this).find('option:selected').html());
			}else{
				$(this).parent().find('div.editable').html($(this).attr('value'));
			}

			$(this).parent().find('div.editable,a.editme').show();
			$(this).parent().find('select,input,span.info').hide();
		}
	});
	if(!$("input[type='submit']").size()){
		$('.editme').hide();
	}

	//$('#CompanyCandidateFiscalStateId').unbind('change');
	$('#CompanyCandidateFiscalStateId').change(function(e){
		$('#CompanyCandidateFiscalCityId').parent().find('select,.editable,.editme').hide();
		$.get('/now/now_registers/cities/'+$(this).val(),function(data){
			cities = $.parseJSON(data);
			sel = $('<select></select>').attr('id','CompanyCandidateFiscalCityId').attr('name','data[CompanyCandidate][fiscal_city_id]');
			nocities = true;
			for(citi in cities){
				opt = $('<option></option>').attr('value',citi).text(cities[citi]);
				$(sel).append(opt);
				nocities = false;
			}
			if(nocities){
				opt = $('<option></option>').attr('value',0).text("No Hay Ciudades");
				$(sel).append(opt);
			}
			$('#CompanyCandidateFiscalCityId').before(sel).remove();
			$('#CompanyCandidateFiscalCityId').parent().find('.editable,.editme').hide();
			$('#CompanyCandidateFiscalCityId').change();
		});
	});

});

</script>

<?php 
	if(Configure::read('app.tracking.enable')){
		if($registerOk){
			echo $this->element ('clubcupon/trackingRegisterOk');
		}
	}
?>

