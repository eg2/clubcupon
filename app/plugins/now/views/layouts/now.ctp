
<?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'head'); ?>

<body id="ccnow" class="kakaka">

<?php
	if (Configure::read('app.tracking.enable')) {
		echo $this->element('tracking-dataexpand'); 
		echo $this->element('trackingGoogle');
		echo $this->element('trackingWeb');
	}
?>
<style>
    #ccnow #tabs li a{ font-size:12px; }
    .mc20px { margin-top: 20px; }
    #main div.bloque-home{padding: 5px!important;}
</style>

<?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
?>
<div id="loading_now" style="height: 100%;position: fixed;width: 100%;opacity:0.7;background-color:#000;z-index:100;color:SeaShell;font-weight:bold;font-size:4.5em;text-align:center;display:none">
loading preview...
</div>

<!--  BEGIN GUSBAR -->
    <?php //echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'header'); ?>
    <?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'header_without_bar'); ?>
<!--  END GUSBAR -->

<!--  BEGIN CONTENT -->
<div id="content" class="container_16">
	<div class="container_16 main_frt" id="main">
		<?php if(isset($show_bars)){ ?>
		<?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'searchbox'); ?>
		<?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'categoriesbar'); ?>
		<div id="main_container" class="grid_16 columna-der" style="position:relative;">
			<div id="dealsoverlay" style="position: absolute; top: 0px; left: 0px; display: none; width: 100%; height: 100%; background: none repeat scroll 0% 0% white; opacity: 0.7; z-index: 100;">
				<div style="position: absolute; top: 10px; right: 20px;"><img src="/img/now/loading.gif"></div>
			</div>
		<?php }else{ ?>
		<div id="main_container" class="grid_16 bloque-home reg-emp" style="position:relative;margin-top: 10px;min-height: 640px;">
    <?php if(isset($show_steps) && $show_steps) { ?>
		<div class="grid_16 mb10px">
			<div class="logonewccnow clearfix">
				<img src="/img/theme_clean/logoYA.png" class="logoya">
				<h2 class="potencialesc">La nueva forma de publicar ofertas instant&#225;neas  y ganar nuevos clientes.</h2>
				<ul class="pasos clearfix">
        <li class="li1 <?php echo $current_step==ConstNowRegisterSteps::SignUp?'select':''; ?>">1</li>
        <li class="li2 <?php echo $current_step==ConstNowRegisterSteps::ConfirmMail?'select':''; ?>">2</li>
        <li class="li3 <?php echo $current_step==ConstNowRegisterSteps::MyCompany?'select':''; ?>">3</li>
				</ul>
        <?php if($current_step==ConstNowRegisterSteps::MyCompany) { ?>
          <p class="nuevaplataforma">Complet&aacute; los siguientes datos para continuar tu registro como empresa.</p>
        <?php } ?>
			</div>
		</div>
    <?php } ?>
                
            <?php
                echo $this->element('dashboard/company_dashboard_menu_tabs');
            ?>
			
		<?php } /*var_dump($this);die();*/?>
			<div id="deals_container">
				<?php echo $content_for_layout;?>
			</div>
		</div>
	</div>
</div>
<!--  END CONTENT -->


</div>
<!--  FOOTER: BEGIN  -->
<?php   
    echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'footer', array('slug'=>$slug)); 
?>
<!--   FOOTER END -->
<?php   echo $this->element('clubcupon/tagCxense');?>
<?php   echo $this->element('clubcupon/tagEplanningProduct5');?>
</body>
</html>


