<?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'head'); ?>

<body id="ccnow" class="landingp usv2">
<?php
if (Configure::read('app.tracking.enable')) {
            	echo $this->element('trackingGoogle');
            	echo $this->element('trackingWeb');
        	}
?>
<?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif; 
?>
<div id="loading_now" style="height: 100%;position: fixed;width: 100%;opacity:0.7;background-color:#000;z-index:100;color:SeaShell;font-weight:bold;font-size:4.5em;text-align:center;display:none">
loading preview...
</div>

<!--  BEGIN GUSBAR -->
    <?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'header'); ?>
<!--  END GUSBAR -->
	<?php echo $content_for_layout;?>
<!--  END CONTENT -->
<!--  FOOTER: BEGIN  -->
<?php   echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'footer'); ?>
<!--   FOOTER END -->
</div>
</body>
</html>



