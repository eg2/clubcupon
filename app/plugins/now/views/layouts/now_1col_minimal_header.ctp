<?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'head_minimal' , array('plugin'=> 'now')); ?>
<body id="ccnow">
<?php
	if (Configure::read('app.tracking.enable')) {
		echo $this->element('tracking-dataexpand'); 
	}
?>
    <div id="loading_now" style="height: 100%;position: fixed;width: 100%;opacity:0.7;background-color:#000;z-index:100;color:SeaShell;font-weight:bold;font-size:4.5em;text-align:center;display:none">
        loading preview...
    </div>
<!--  BEGIN GUSBAR -->
    <?php   echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'header_minimal' , array('plugin'=> 'now')); ?>
<!--  END GUSBAR -->

<!--  BEGIN CONTENT -->
<div id="main" class="container_16 main_frt reg-emp">
    <div class="grid_16">
<?php
    if ($session->check('Message.error')):
      $session->flash('error');
    endif;
    if ($session->check('Message.success')):
      $session->flash('success');
    endif;
    if ($session->check('Message.flash')):
      $session->flash();
    endif;
    ?>

    </div>
    <!--  BEGIN CONTROLLERS VIEW -->
    <div class="grid_16">
        <?php echo $content_for_layout; ?>
    </div>
    <!--  END CONTROLLERS VIEW -->

    <div class="clear"></div>
</div>
<!--  FOOTER: BEGIN  -->
<?php   echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'footer',  array('plugin'=> 'now')); ?>
<!--   FOOTER END -->


    <?php echo $this->element('trackingWeb',array('certificaPath'=> isset($certificaPath) ? $certificaPath : false )); ?>
</body>
</html>


