<style>

.alert {
    background-color: #fcf8e3;
    border: 1px solid #fbeed5;
    border-radius: 4px;
    margin-bottom: 20px;
    padding: 8px 35px 8px 14px;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
}

</style>     
     <div class="alert alert-error" id="error">
     </div>
 	 <div class="alert alert-success" id="success">
     </div>
	 <div class="alert alert-info" id="info">  
     </div>  

 <form role="form" style="font: 12px Arial">
   <div style="float:left">
     <div class="form-group">
       <label for="email">Desde:</label>
       <input  type="text" placeholder="seleccione fecha desde"  id="desde">
     </div>
     <div class="form-group">
       <label for="email">Hasta&nbsp;:</label>
       <input  type="text" placeholder="seleccione fecha hasta"  id="hasta">
     </div>
  </div>
  <div style="float:left;padding:23px 0px 0px 12px;">
     	<?php echo $html->link('Exportar', array('plugin'=>'now','controller'=>'now_exports','action'=>'liquidate_between_dates'),array('role'=>'button','class'=>'btn-azul-d','id'=>'btnExportar','style'=>'color:#fff;float:right')); ?>
  </div>
  <br><br>  
</form>
<script type="text/javascript">
   $(document).ready(function () {
	  function hideAlerts(){
			  $("#success").hide();
		   	  $("#error").hide();
		   	  $("#info").hide();
	  } 
   	  hideAlerts();
	  $('#desde').datepicker({
	        dateFormat: "yy-mm-dd"
      });  
	  $('#hasta').datepicker({
	        dateFormat: "yy-mm-dd"
	  }); 
	  
	  var handleClick = function(e) {
		  hideAlerts();
		  var url= $(this).attr('href');
		  var desde= $("#desde").val();
		  var hasta= $("#hasta").val();
		  
		  url='/now/now_exports/verify_data_for_export';
		  //desde='2014-01-12';hasta='2014-12-12';//error
		  //desde='2012-01-12';hasta='2014-12-12';//ok
		  if(desde && hasta){
			rango='/since:'+desde+'/until:'+hasta;
		  	url+=rango;
		  	
        	$("#info").html('Procesando...');
        	$("#info").show();
		  	
			
		  	$.ajax({
		        url: url,
		        type: "post",
		        contentType: "json",
		        success: function(data) {
			        var obj = jQuery.parseJSON( data );
			        hideAlerts();
			        if( obj.status == 'success' ){
			        	$("#success").html('Se generó con éxito el archivo a exportar.');
			        	$("#success").show();
			        	window.location='/now/now_exports/liquidate_between_dates'+rango;
			        }else{
			        	$("#error").html(obj.returned_val);
			        	$("#error").show();
			        }
		            //$("#result").html('Submitted successfully');
		        },
		        error:function(xhr, status, text) {
		            var response = $.parseJSON(xhr.responseText);
		            $("#error").html(response.error);
			        $("#error").show();
		        }
		    });
		  	e.preventDefault();
		  }else{
			  $("#error").html("Debe completar el rango de fechas");
	          $("#error").show();
			  
			  e.preventDefault();
		  }
		  
	  };
	  $('#btnExportar').on('click',handleClick);        
  });
</script>