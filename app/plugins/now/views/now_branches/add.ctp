<!--[if IE]>
    <style>
        .btn-azul-d{background:url(/img/now/btn-bg4.png) repeat-x; margin-left:200px;}
    </style>
<![endif]-->
<style>
    .btn-azul-d{
        float: none !important;
    }
    .submit {
        text-align: center;
    }
	
	@media screen and (-webkit-min-device-pixel-ratio:0) {
		#bt_enviar {
			margin: 0 0 0 177px;
		}
	}
	
</style>
<div class="grid_16 adds-suc">
	<div class="contenido-form" id="tool">
	<div class="content">
	<?php echo $form->create('NowBranch', array('class' => 'normal')); ?>
		<div class="encabezado">
			<h3>Ya sos parte de <strong>Club Cupón <span>Ya!</span></strong></h3>
			<p class="nuevaplataforma">Para continuar comenzá agregando tus Sucursales</p>
			<p class="subt">Agregar Sucursal</p>
		</div>
		<div class="colizq clearfix">
			<p class="titulo">Ubicación</p>
			<div class="form-fmt clearfix">
<!--fila 1-->
				<div class="grupoinp w1 clearfix">
					<label>Nombre de la Sucursal<span>*</span></label>
					<?php echo $form->input('name', array('label' => false,'div'=>null));?>
				</div>
				<div style = "clear: both; text-align: center; display: none;"  id = "geo-loading"><?php echo $html->image ('loading.gif'); ?></div>
<!--fila 2-->
				<div class="grupoinp w2 fleft clearfix">
					<label>País<span>*</span></label>
					<?php echo $form->input('country_id', array('label' => false,'div'=>null));?>
				</div>

				<div class="grupoinp w2 fleft clearfix">
					<label>Provincia<span>*</span></label>
					<?php echo $form->input('state_id', array('label' => false,'div'=>null));?>
				</div>

				<div class="grupoinp w2 fleft clearfix">
					<label>Localidad<span>*</span></label>
					<?php echo $form->input('city_id', array('label' => false,'div'=>null));?>
				</div>

				<div class="grupoinp w3 fleft clearfix">
					<label>Barrio<span>*</span></label>
					<?php echo $form->input('neighbourhood_id', array('label' => false,'div'=>null));?>
				</div>

<!--fila 3-->
				<div class="grupoinp w3 fleft clearfix">
					<label>Calle<span>*</span></label>
					<?php echo $form->input('street', array('label' => false,'div'=>false));?>
				</div>

				<div class="grupoinp w4 fleft clearfix">
					<label>Altura<span>*</span></label>
					<?php echo $form->input('number', array('label' => false,'div'=>false));?>
				</div>

				<div class="grupoinp w5 fleft clearfix">
					<label>Piso</label>
					<?php echo $form->input('floor', array('label' => false,'div'=>false));?>
				</div>

				<div class="grupoinp w5 fleft clearfix">
					<label>Dpto.</label>
					<?php echo $form->input('apartament', array('label' => false,'div'=>false));?>
				</div>

				<div class="grupoinp w6 fleft clearfix">
					<label>Código Postal<span>*</span></label>
					<?php echo $form->input('postal_code', array('label' => false,'div'=>false));?>
				</div>
			</div>
<!--Segundo Bloque-->
			<p class="titulo">Datos de Contacto</p>
			<div class="form-fmt clearfix">
					<div class="grupoinp w2 fleft clearfix">
						<label>Teléfono<span>*</span></label>
						<?php echo $form->input('telephone', array('label' => false,'div'=>false, 'maxlength'=>'12'));?>
						<p class="info">Ejemplo: 01123456789</p>
					</div>
					<div class="grupoinp w2 fleft clearfix">
						<label>Fax</label>
						<?php echo $form->input('fax_number', array('label' => false,'div'=>false));?>
						<p class="info">Ejemplo: 01123456789</p>
					</div>
<!--fila 2-->
					<div class="grupoinp w1 clearfix">
						<label>Mail</label>
						<?php echo $form->input('email', array('label' => false,'div'=>false));?>
						<p class="info">Ejemplo: minombre@ejemplo.com</p>
					</div>
					<span class="camp-obltabs">* Campos Obligatorios</span>
					<div>
						<?php echo $form->submit('Agregar Sucursal', array('id' => 'bt_enviar','class'=>'btn-azul-d','div'=>false));?>
					</div>
			</div>
		</div>
		<div class="colder">
			<div class="mapa">
				<div id="show-map" style="height: 247px; width: 313px;border: 1px black solid;"></div>

				<?php
					echo $form->input('latitude' ,array('type' => 'hidden', 'id'=>'latitude', 'value'=>Configure::read('NowPlugin.branches_latitude')));
					echo $form->input('longitude',array('type' => 'hidden', 'id'=>'longitude','value'=>Configure::read('NowPlugin.branches_longitude')));
				?>

			</div>
		</div>
		<?php echo $form->end();?>
	</div>
	</div>
</div>
<?php echo $javascript->link('/js/now/branch-search.js'); ?>
<script type="text/javascript" src="/js/now/branch-add.js"></script>
<script type="text/javascript">
var myOptions = {
        center: new google.maps.LatLng(-34.612, -58.450),
        zoom: 8,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map;
</script>
<script type='text/javascript'>
    $('#NowBranchCountryId').change(function() {
        $('#geo-loading').show();
        var parent_id=$(':selected',this).val();

        var url=cfg.cfg.path_relative+'now/now_branches/geoAjax/state/'+parent_id;
        console.log(url);
        $('#NowBranchStateId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#NowBranchStateId').change();
                              });
    });
    $('#NowBranchStateId').change(function() {
        $('#geo-loading').show();
        var parent_id=$(':selected',this).val();

        var url=cfg.cfg.path_relative+'now/now_branches/geoAjax/city/'+parent_id;
        console.log(url);
        $('#NowBranchCityId').load(url,function (){
                                    $('#geo-loading').hide();
                                    $('#NowBranchCityId').change();
                              });
    });

</script>
