<!--[if IE 8]>
        <style>
            .btn-azul-d{margin-left:350px!important; border:1px solid red green;}
        </style>
<![endif]-->

<style>
    
    @media screen and (-webkit-min-device-pixel-ratio:0) {
        /*Chrome CSS here*/
        .encabezado{
            margin-bottom:85px;
            
        }
            .encabezado .nuevaplataforma{padding:15px 3px;}
        
        .selall{
            
            position:absolute;
            left:400px;
            
        }
    }
    
    
    
    .btn-azul-d{
        float:none;
    }
    #ccnow .oflist.adds-suc .encabezado{
        text-align: center;
    }
</style>
<div class="grid_16 adds-suc">
	<div class="contenido-form" id="tool">
		<div class="encabezado">
			<p class="nuevaplataforma">Podés continuar agregando nuevas sucursales presionando el botón <strong>"Agregar Sucursales"</strong><?php if(count($branches)){ ?> o bien,<br />iniciar la carga de ofertas en la pestaña <strong>"Mis Ofertas".</strong><?php }?></p>
            <div class="selall clearfix">
                <input type="submit" class="btn-azul-d mt30" value="Agregar Sucursal" onclick="window.location='<?php echo Router::url(array('action'=>'add'));?>'">
            </div>
		</div>
		<?php if(count($branches)){ ?>
		<div class="listasuc clearfix">
			<div id="header-no-hover" class="linea-item titlelist clearfix">
				<label class="lab1">Nombre Sucursal</label>
				<label class="lab2">Dirección</label>
				<label class="lab3">Localidad</label>
				<label class="lab4">Teléfono</label>
			</div>
				<?php foreach($branches as $key=>$branch){ ?>
				<div class="linea-item <?php if($key%2){echo "select";}?> clearfix">
					<label class="lab1"><?php echo $html->cText($branch['NowBranch']['name']); ?></label>
					<label class="lab2">
						<?php echo $html->cText($branch['NowBranch']['street']); ?>
						<?php echo $html->cText($branch['NowBranch']['number']); ?>
						<?php echo $html->cText($branch['NowBranch']['floor']); ?>
						<?php echo $html->cText($branch['NowBranch']['apartament']); ?>
					</label>
					<label class="lab3"><?php echo $html->cText($branch['City']['name']);?></label>
					<label class="lab4"><?php echo $html->cText($branch['NowBranch']['telephone']); ?></label>
					<?php echo $html->link('Modificar',array('action'=>'edit',$branch['NowBranch']['id']),array('class'=>'link1'));?>
					<?php echo $html->link('Eliminar',array('action'=>'delete',$branch['NowBranch']['id']),array('class'=>'link3'),"¿Estás seguro que deseas eliminar esta sucursal?");?>
				</div>
				<?php }?>
		</div>
		<?php }?>
	</div>
</div>
