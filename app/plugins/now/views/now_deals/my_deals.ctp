<style>
  .link4 {
     float: left;
     width: 75px;
  }
    .actions_links {
      padding-top: 5px;
      float: left;
    }
.deal_detail {
  margin: 0;    
  border-radius: 5px;
  box-shadow: 1px 1px 1px;
  padding: 0 10px;
  background: none repeat scroll 0 0 #F5F5F5;
}
.deal_detail_background {
  background: none repeat scroll 0 0 #d1e4eb !important;
}
a.tip {
  position: relative;
}

a.tip span {
  display: none;
  position: absolute;
  top: 20px;
  left: -10px;
  width: 120px;
  padding: 5px;
  z-index: 100;
  background: #eee;
  color: #888;
    border:1px solid #ccc;
  -moz-border-radius: 5px; /* this works only in camino/firefox */
  -webkit-border-radius: 5px; /* this is just for Safari */

}

a:hover.tip {
  font-size: 99%; /* this is just for IE */
}

a:hover.tip span {
  display: block;
}

.cupons {
  padding-right: 300px;
  text-align: right;
}
</style>

<!--[if IE]>
    <style>
        .adjust360{
            margin-left:360px!important;
        }
    </style>
<![endif]-->

<style>

@media screen and (-webkit-min-device-pixel-ratio:0) {
    /*Chrome CSS here*/
    #ccnow .contenido-form .listasuc.listofertas{
    margin-top:75px;
    }
}

.btn-azul-d{
    float:none;
}

#ccnow .oflist.adds-suc .encabezado{
    text-align: center;
}

#ccnow .adds-suc .encabezado{
    padding:0!important;
}

</style>
<div class="adds-suc oflist">
  <div class="contenido-form" id="tool">
    <div class="contenedor-sub">
      <ul class="submenu">
        <li class="<?php if(!$isNow) { echo 'active'; } ?>" style="float:none; text-align: center;">
          <?php
          if(!$isNow) {
            echo 'CLUB CUPON';
            
          } else {
            echo $html->link('CLUB CUPON', array('plugin'=>'now','controller'=>'now_deals','action'=>'my_deals', 'is_now' => 0),array('style'=>'float:none'));
          }?>
        </li>
        <?php if($companyIsNow) { ?>
        <li class="<?php if($isNow) { echo 'active'; } ?>">
        <?php
          //if($isNow) {
           // echo 'CLUB CUPON YA!';
        //  } else {
          //  echo $html->link('CLUB CUPON YA!', array('plugin'=>'now','controller'=>'now_deals','action'=>'my_deals', 'is_now' => 1), array('class' => 'with-border-left'));
        //  }
          ?>
        </li>
        <?php } ?>
        <!-- </li> -->
      </ul>
	  <?php
          $link_txt = strtolower(ConstDealStatus::getFriendly($filter_id));
          if (!$link_txt) $link_txt = "Todas";
      ?>
      <?php // echo $html->link('Descargar listado de ofertas ('. $link_txt .')', array('action'=>'download_my_deals', 'ext' => 'csv','filter_id'=>$filter_id, 'is_now'=> $isNow), array("style"=>'float:right;margin-top:5px;')); ?>
    </div>

    <?php if($isNow) { ?>
    <div class="encabezado adjust360">
      <input type="submit" class="btn-azul-d mt30" value="Agregar Oferta" onclick="window.location='<?php echo Router::url(array('action'=>'save'));?>'">
    </div>
    <?php } ?>
    <form>
    <div class="listasuc listofertas clearfix">
      <div class="selall clearfix">
        <select id="filter_select">
          <option value="0">Todas las Ofertas</option>
          <option value="-1" <?php if(@$filter_id == '-1') echo 'selected';?>>Ofertas con Ventas</option>
          <?php foreach($dealStatuses as $key=>$dealStatus){?>
            <?php if(isset($dealStatusesCount[$key])){?>
          <option value="<?php echo $key;?>" <?php if(@$filter_id == $key) echo 'selected';?>><?php echo ConstDealStatus::getFriendly($key)." (".$dealStatusesCount[$key].")";?></option>
            <?php }?>
          <?php }?>
        </select>
        <p>Filtrar ofertas por estado: </p>
      </div>

    <?php if(count($deals)){ ?>
      <div class="linea-item titlelist clearfix">
        <label class="lab0" style="width:60px !important"> Id Oferta</label>
        <label class="lab7"> Fecha de publicación </label>
        <label class="lab1"> Oferta</label>
        <label class="lab6"> Estado</label>
        <label class="lab3" style="width:60px !important"> Precio Cup&oacute;n ($)</label>
        <label class="lab9"> Cupones Redimidos </label>
        <label class="lab9"> Cupones Vendidos</label>
        <label class="lab10"> Cupones Anulados</label>
        <label class="lab2"> Fecha de finalizaci&oacute;n</label>
        <label class="lab2"> Ciudad/ Grupo</label>
        <label class="lab4" style="text-align:left;">Acciones</label>
      </div>
      
      <?php foreach($deals as $key=>$deal){ ?>
      <div class="linea-item clearfix <?php if($key%2){echo "bg-linea";}?>">
        <label class="lab0" style="width:60px !important">
          <?php echo empty($deal['Deal']['id']) ? "&nbsp": $html->cText($deal['Deal']['id']); ?> 
        </label>
        <label class="lab7"> 
            <?php if (!$deal['Deal']['is_now']) { ?>
            <?php $deal_start_date = @$time->format("d-m-Y",$deal['Deal']['start_date']); ?>
            <?php echo empty($deal['Deal']['start_date']) ? "&nbsp": $html->cText($deal_start_date);?> 
            <?php } else {?>
            <?php $deal_start_hour = @$time->format("H\h\s",$deal['Deal']['start_date']); ?>
            <?php $deal_start_date = @$time->format("d-m-Y",$deal['Deal']['start_date']); ?>
            <?php $deal_start_date = $deal_start_date.' desde '.$deal_start_hour; ?>
            <?php echo empty($deal['Deal']['start_date']) ? "&nbsp": $html->cText($deal_start_date);?> 
            <?php } ?>
        </label>
        
        <label class="lab1">
        	<?php 
        		echo $html->link ($html->cText ($deal['Deal']['name']), array ('plugin'=>'','controller' => 'deals', 'action' => 'view', $deal['Deal']['slug'], 'city' => $deal['City']['slug'], 'admin' => false), array ('class' => 'view-icon', 'title' => 'Ver Oferta', 'title' => $html->cText ($deal['Deal']['name'], false), 'escape' => false,'target' => '_blank'));
            ?>
        </label>
        <label class="lab6"><?php echo ConstDealStatus::getFriendly($deal['Deal']['deal_status_id']);?></label>
        <label class="lab3"  style="width:60px !important"> <?php echo empty($deal['Deal']['discounted_price']) ? "&nbsp" : $html->cText($deal['Deal']['discounted_price']);?> </label>
        <label class="lab9"><?php echo empty($deal[0]['coupon_redemed_count']) ? "0" : $html->cText($deal['0']['coupon_redemed_count']);?></label>
        <label class="lab9">
          <?php if ($deal['Deal']['is_end_user'] || (in_array($deal['Deal']['company_id'], Configure::read('companies.show_no_redeemed'))) || (!$deal['Deal']['pay_by_redeemed'])) { ?>
          <?php echo empty($deal[0]['coupon_count']) ? "0" : $html->cText($deal[0]['coupon_count']);?>
          <?php } else {
                echo '&nbsp - &nbsp';
          }?>
        </label>
        <label class="lab10"> 
          <?php 
                    echo empty($deal[0]['coupon_returned_count']) ? "0" : $html->cText($deal[0]['coupon_returned_count']);  
          ?>
        </label>                
        <label class="lab2">
          <?php
          if ($deal['Deal']['is_now']) {
                if(!isset($deal['Deal']['start_date'])) {
                    $date = unserialize($deal['Deal']['private_note']);
                    $deal['Deal']['start_date'] = date("Y-m-d H:m:s",strtotime("+".$date['times']['time_range_start']." hours",strtotime($deal['Deal']['start_date'])));
                    $deal['Deal']['end_date'] = date("Y-m-d H:m:s",strtotime("+".$date['times']['time_range_end']." hours",strtotime($deal['Deal']['end_date'])));
                }
                if(!empty($deal['Deal']['start_date']) && !empty($deal['Deal']['end_date'])) {
                    $h_fin = @$time->format("H\h\s",$deal['Deal']['end_date']);
                    if($h_fin=='00hs') $h_fin='24hs';
                    $msgdate = @$time->format("d-m-Y",$deal['Deal']['end_date']) . '<br /> hasta <br />'. $h_fin;
                }
                else {
                    $msgdate = 'Sin asignar';
                }
          } else {
            //Oferta No Now
            $msgdate = @$time->format("d-m-Y",$deal['Deal']['end_date']);
          }
          echo $msgdate;?>
        </label>
        
        <label class="lab2"> 
		  <div class="categorycell"> 
		    <?php echo $html->cText(($deal['City']['name'] != '' ? $deal['City']['name'] : 'Sin asignar'));?> 
		  </div>
		</label>
        <label class="lab5">
      
          
          <?php if($deal['Deal']['deal_status_id'] == ConstDealStatus::Draft && in_array($filter_id,array(0,ConstDealStatus::Draft))) { ?>
          <div class="actions_links" >
          <?php echo $html->link('Modificar',array('action'=>'save',$deal['Deal']['id']),array('class'=>'link1', 'style'=>'text-align:left')); ?>
          </div>
          <?php } ?>
          
          <?php if($deal['Deal']['deal_status_id'] != ConstDealStatus::Draft ){ ?>
               <div class="actions_links" >
                <?php echo $html->link ('Exportar cupones ', array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'download_my_cupons',    'filter_id' => $deal['Deal']['id'], ), array('escape' => false, 'class'=> 'link2 tip', 'float'=>'left')); ?>
               </div>
                
                <div class="actions_links">
                <?php echo $html->link ('Ver cupones ', array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'my_cupons',             'filter_id' => $deal['Deal']['id'], ), array('escape' => false, 'class'=>'link4', 'style'=>'text-align:left'));    ?>
                </div>
                
                <div class="actions_links">
                <?php
                	$filter='all';
                	if($deal['Deal']['pay_by_redeemed']==1){
						$filter='redeemed';
					}else{
						if($deal['Deal']['pay_by_redeemed']==0){
							$filter='sold';
						}
					} 
                	echo $html->link ('Liquidaciones ', array ('plugin'=>'now','controller'=>'now_liquidations','action' => 'liquidations','deal_id' => $deal['Deal']['id'],'filter' => $filter ), array('escape' => false, 'class'=>'link4', 'style'=>'text-align:left'));    
                ?>
                </div>
                
          <?php } ?>
        </label>
        <!-- <label class="label5" >
          <div class = "actions-block">
            <div class = "actions round-5-left"> -->
              
           <!-- </div>
          </div>
        </label>-->
        </div> <!--- end div row -->
        <div class="linea-item clearfix deal_detail_background"  id="<?php echo 'deal_detail_'. $deal['Deal']['id'];?>" style="display: none">  <!-- begin deal detail -->
          <div class="deal_detail">
          <table>
            <tr>
              <td colspan="3"> Oferta: <?php echo $deal['Deal']['id'] .' '.$deal['Deal']['name']?> </td>
            </tr>
            <tr>
              <th colspan="3"> Datos principales de la oferta </th>
            </tr>
            <tr>
              <td> Fecha de Publicacion: </td>
              <td colspan="2" class="cupons"> <?php echo $deal_start_date;  ?></td>
            </tr>
            <tr>
              <td> Precio Original: </td>
              <td colspan="2" class="cupons"> <?php echo '$'. $deal['Deal']['original_price'];   ?></td>
            </tr>
            <tr>
              <td> Precio con Descuento: </td>
              <td colspan="2" class="cupons"> <?php echo '$'. $deal['Deal']['discounted_price'];  ?></td>
            </tr>
            <tr>
              <td> Porcentaje de Descuento: </td>
              <td colspan="2" class="cupons"> <?php echo $deal['Deal']['discount_percentage'].'%';   ?></td>
            </tr>
            <tr>
              <td> Porcentaje de comisión: </td>
              <td colspan="2"  class="cupons"> <?php echo $deal['Deal']['commission_percentage']. '%';   ?></td>
            </tr>
            <tr>
              <td> Fecha de vigencia del cupón: </td>
              <td colspan="2"  class="cupons" style="text-align: right"> <?php 
                                 $h_fin = @$time->format("H\h\s",$deal['Deal']['coupon_expiry_date']);
                                 if($h_fin=='00hs') $h_fin='24hs';
                                 $msgdate = @$time->format("d-m-Y",$deal['Deal']['coupon_expiry_date']) . ' hasta '. $h_fin;
                                 echo $msgdate;  
                               ?></td>
            </tr>
            <tr>
              <th colspan="3">
                Detalle de Cupones
              </th>
            </tr>
            <tr>
              <td> Cupones Vendidos: </td>
              <td style="text-align: right" class="cupons" colspan="2">
                <?php if ($deal['Deal']['is_end_user'] || (in_array($deal['Deal']['company_id'], Configure::read('companies.show_no_redeemed'))) || (!$deal['Deal']['pay_by_redeemed'])) { ?>
                <?php echo empty($deal[0]['coupon_count']) ? "0" : $html->cText($deal[0]['coupon_count']);?>
                <?php } else {
                  echo '&nbsp - &nbsp';
                }?>
              </td>
            </tr>
            <tr>
              <td> Cupones Redimidos: </td>
              <td style="text-align: right" class="cupons" colspan="2"> <?php echo empty($deal[0]['coupon_redemed_count']) ? "0" : $html->cText($deal[0]['coupon_redemed_count']);?></td>
            </tr>
            <tr>
              <td> Cupones Devueltos: </td>
              <td style="text-align: right" class="cupons" colspan="2">  <?php 
                    echo empty($deal[0]['coupon_returned_count']) ? "0" : $html->cText($deal[0]['coupon_returned_count']);  
              ?></td>
            </tr>
            <tr>
              <td> Cupones Liquidados: </td>
              <td style="text-align: right" class="cupons"  colspan="2">  <?php echo empty($deal['Deal']['coupon_billed']) ? "0" : $html->cText($deal['Deal']['coupon_billed']);?>  </td>
            </tr>
          </table>
          </div> <!-- end div deal detal -->
        </div> <!-- end deal detail -->
      <?php }?>
    <?php } else {?>
    <div class="linea-item titlelist clearfix">
      <center>
        <p style="font: 16px arial;">
        Actualmente no ten&eacute;s Ofertas <?php if($filter_id!=0){?>en estado: <b><?php echo htmlentities(ConstDealStatus::getFriendly($filter_id),ENT_COMPAT,'UTF-8');?></b><?php }?>
        </p>
      </center>
    </div>
    <?php }?>
    </div> <!-- end listasuc listofertas clearfix" -->
    </form>
 	<?php if(count($deals) && $paginator->hasPage(null,2)){ ?>
      <div class="dealspaginador clearfix">
      <div class="pagborder">
      <?php
      $paginator->options(array('url' => array_merge(array('controller' => $this->params['controller'],'action' => $this->params['action'],),$this->params['pass'], $this->params['named'])));
      //Hack horrible hasta el revamp!
      $prev_link      = str_replace('/all', '', $paginator->prev('Anterior' , array('class' => 'prev','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'prev')));
      $numbers_link   = str_replace('/all', '', $paginator->numbers(array('modulus' => 2,'skip' => '<span class="skip">&hellip;.</span>','separator' => " \n",'before' => null,'after' => null,'escape' => false)));
      $next_link      = str_replace('/all', '', $paginator->next('Próximo', array('class' => 'next','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'next')));
      if(!empty ($prev_link)) {
        echo $prev_link."\n";
      }
      if(!empty ($numbers_link)) {
        echo $numbers_link."\n";
      }
      if(!empty ($next_link)) {
        echo $next_link."\n";
      }
      echo $paginator->counter(array('format' => '<p><span>%start%-%end%</span> de %count% Ofertas</p>'));
      ?>
      </div>
      </div>
<?php } ?>
  </div> <!-- end contenido-form -->
</div> <!-- end adds-suc -->

<script>
$(document).ready(function(){
  //$('#filter_select').unbind('change');
  $('#filter_select').change(function(e){
    if($(this).attr('value')){
      window.location = "<?php echo Router::url(array('filter_id'=>'___', 'is_now' => $isNow));?>".replace(/___/gi,$(this).attr('value'));
    }else{
      window.location = "<?php echo Router::url(array());?>";
    }
  });

  $('a.no-edit').click(function(e){
    e.preventDefault();
    alert('Solo se pueden editar la ofertas que esten en estado "Borrador"');
  });

  $('a.ajax').colorbox({
    height:580,
      width:815,
      onLoad:function(){
        $('#cboxClose').remove();
    }
  });

  $('#abrir_tyc').live('click',function(e) {
    e.preventDefault();
    $('#tyc_block').toggle();
  });
  $('#cerrar_tyc').live('click',function(e) {
    e.preventDefault();
    $('#tyc_block').toggle();
  });
  //Para forzar el centrado del paginador
  anchoPaginador = 0;
  $('.pagborder span').each(function(){
    anchoPaginador += $(this).outerWidth()+10;
  });
  $('.pagborder a').each(function(){
    anchoPaginador += $(this).outerWidth();
  });

  $('.pagborder').width(anchoPaginador);
    
   
  $('#more_detail').live('click',function(e) {
    dealId = $(this).attr('deal_id');
    e.preventDefault();
    $('#deal_detail_'+dealId).toggle();
  });
});
</script>
