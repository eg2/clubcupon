<div class="grid_16 paso3addofertas adds-suc">
    <div class="contenido-form clearfix" style="position:relative;">
        <div id="form-overlay" style="display:none; position: absolute; width: 100%; height: 100%; background: none repeat scroll 0% 0% black; opacity: 0.2; left: 0px; top: 0px; border-radius: 5px 5px 5px 5px;"></div>
		<div class="encabezado">
                    <p class="subt">Recalendariz&#225; tu Oferta <span style="font-weight: bold"> <?php echo $deal_name;?></span></p>
		</div>
        <div class="form-fmt clearfix">
            <div class="colizq clearfix">
                <?php
                echo $form->create('NowScheduledDeals', array('plugin' => 'now', 'controller' => 'now_deals', 'action' => 'reschedule_deal','url' => '/now/now_deals/reschedule_deal/'.$this->data['NowScheduledDeals']['id'].'/deal_id:'.$this->data['NowScheduledDeals']['deal_id'],  'class' => 'normal', 'enctype' => 'multipart/form-data'));
                ?>
                <p class="titulo">Programaci&#243;n de la oferta</p>
                <div  id="collapsable" style="width:610px;">
                    <div class="grupoinp validez" style="margin-top:30px;height:160px;">
                        <label>Fecha Inicio de publicaci&#243;n<span>*</span></label>
                        <?php echo $form->hidden('id'); ?>
                        <?php echo $form->input('start_date', array('error' => false, 'readonly' => TRUE, 'label' => false, 'div' => null, 'readonly' => 'readonly', 'disabled' => true, 'minYear' => date('Y'), 'maxYear' => date('Y') + 10, 'type' => 'text')); ?>
                        <label>Fecha Final de publicaci&#243;n<span>*</span></label>
                        <?php echo $form->input('end_date', array('error' => false, 'label' => false, 'div' => null, 'readonly' => 'readonly', 'minYear' => date('Y'), 'maxYear' => date('Y') + 10, 'type' => 'text')); ?>
                        <span class="sprite"></span>
                        <?php if (isset($this->validationErrors['NowScheduledDeals']['end_date'])) { // VALIDACION POSICIONADA A MANO! SINO PINCHA EL LAYOUT! ?>
                            <div class="error-message"><?php echo $this->validationErrors['NowScheduledDeals']['end_date'] ?></div>
                        <?php } ?>
                        <p>Formato: (AAAA-MM-DD)</p><br>
                        <p class="info f11">Tu oferta puede ser comprada hasta este d&#237;a </p>
                    </div>



                    <div class="">
                        <?php
                        echo $form->input('scheduled_deals', array('options' => $dias,
                            'type' => 'select',
                            'multiple' => 'checkbox',
                            'legend' => false,
                            'div' => 'weekdays',
                            'selected' => $dias_selected,
                            'label' => 'Publicar los siguientes días',));

                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="">
                        <?php
                        echo $form->radio('is_disabled', array(0 =>'activa',1=>'en pausa'), array('legend'=>'Estado de calendarizacion'));


                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="btn-opciones">
                        <input type="submit"  name="guardar" class="btn-azul-d addsbottom" value="Enviar">
                        <input type="submit" name="guardar" class="btn-azul-d addsbottom" value="Eliminar Calendarización">
                    </div>
                    </div>
            </div>
        </div>
    </div>
</fieldset>
</div>
<script type="text/javascript">
/*<![CDATA[*/

$(document).ready(function(e){
    $('input[name="data[NowScheduledDeals][scheduled_deals][]"]').bind('click',function(e){
			if($('input[name="data[NowScheduledDeals][scheduled_deals][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos un día!");
			}
		});
    });
/*]]>*/
</script>