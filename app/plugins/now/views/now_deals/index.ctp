<?php if(isset($branchesDeals)){?>
        

	<div id="deals_contenedor" class="contenedor-tabs" style="padding-top: 20px;">
		<div id="deals_detalles" style="">
			<?php
				foreach($branchesDeals as $key=>$deal){
					echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'oferta', array('deal'=>$deal));
				}
			?>
		</div>

	</div>
	<?php if($total > $limit){ ?>
	<div class="grid_16 paginador">
		<ul>
            <?php if($page>1){?>
			<li class="pag-prev-next">
				<span <?php if($page>1){?> rel="<?php echo Router::url(array('plugin'=>'now','controller'=>'NowDeals','action'=>'index',$page-1,$limit ),false);?>" class="pag-link"<?php }?>>
					<a title="Anterior">Anterior</a>
				</span>
			</li>
            <?php }?>
			<?php for($i=1;$i<=ceil($total/$limit);$i++){ ?>
				<?php if($i!=$page){ ?>
				<li>
                    <span title="Pagina <?php echo $i;?>" rel="<?php echo Router::url(array('plugin'=>'now','controller'=>'NowDeals','action'=>'index',$i,$limit),false);?>" class="pag-link">
                        <?php echo $i;?>
                    </span>
                </li>
				<?php }else{ ?>
				<li>
                    <span title="Pagina <?php echo $i;?>" class="pag-selected">
						<?php echo $i;?>
					</span>
                </li>
				<?php } ?>
			<?php } ?>
                <?php if($page+1!=$i){?>
                    <li class="pag-prev-next">
                        <span <?php if($page+1!=$i){?> rel="<?php echo Router::url(array('plugin'=>'now','controller'=>'NowDeals','action'=>'index',$page+1,$limit),false);?>" class="pag-link"<?php }?>>
                            <a title="Siguiente">Siguiente</a>
                        </span>
                    </li>
                <?php }?>
		</ul>
		<p><span><?php echo ($limit*($page-1))+1;?>-<?php echo (($limit*($page-1))+$limit)<$total?(($limit*($page-1))+$limit):$total;?></span> de <?php echo $total;?> Ofertas</p>
	</div>
	<?php }?>
<?php }else{?>

    <ul class="sinresultados">
        
        <?php
        
            if(!isset($lugar) && !isset($catname))
            {
                echo '<li class="sr-line3">No hay resultados para tu búsqueda</li>';
            }
            else if(isset($catname) && $lugar == 'Barrio/Ciudad, Localidad')
            {
                echo '<li class="sr-line3">No hemos encontrado ofertas de ' . $catname . ' cerca de donde te encontrás.</li>';
            }
            else if($lugar != 'Barrio/Ciudad, Localidad')
            {
                echo '<li class="sr-line3">No hemos encontrado ofertas en ' . $lugar. ' .</li>';
            }
        
        ?>
        
        <li class="sr-line4"><p>Intentalo nuevamente o mir&aacute; todas nuestras ofertas de  <a href="<?php echo FULL_BASE_URL.'/ya';?>">Club Cup&oacute;n Ya!</a></p></li>        
		
	</ul>
<?php }?>

    <script>
        $(document).ready(function() {
            
            //Para forzar el centrado del paginador
            anchoPaginador = 0;
            $('.paginador ul li').each(function(){ 
                    anchoPaginador += $(this).outerWidth()+10; // LI's have padding
                
            });
            $('.paginador ul').width(anchoPaginador);
            
            
            
    });
    </script>
