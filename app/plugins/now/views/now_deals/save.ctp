<div class="grid_16 paso3addofertas adds-suc">
	<div class="contenido-form clearfix" style="position:relative;">
		<div id="form-overlay" style="display:none; position: absolute; width: 100%; height: 100%; background: none repeat scroll 0% 0% black; opacity: 0.2; left: 0px; top: 0px; border-radius: 5px 5px 5px 5px;"></div>
		<div class="encabezado">
			<p class="subt">Carg&#225; tu Oferta</p>
		</div>
	<?php echo $form->create ('NowDeal', array ('action' => 'save', 'class' => 'normal', 'enctype' => 'multipart/form-data')); ?>
	<?php echo $form->hidden('id');?>
		<div class="form-fmt clearfix">
			<div class="colizq clearfix">
				<p class="titulo">General</p>
				<div class="form-fmt clearfix">
					<div class="grupoinp w1 clearfix">
						<label>T&#237;tulo de la Oferta<span>*</span></label>
						<?php  echo $form->input ('name', array ('label' => false, 'div' => null)); ?>
						<label>Imagen del principal<span>*</span></label>
						<?php  echo $form->input ('Attachment.filename', array ('type' => 'file', 'label' => false, 'div'=> null)); ?>
						<?php if(isset($file_empty)){?>
							<div class="error-message" style="clear:both">¡Debe enviar una im&aacute;gen para la oferta!</div>
						<?php }else{?>
							<p class="info f11">Tamaño sugerido 437x264 píxeles.</p>
						<?php }?>
						<?php if(isset($this->data['Attachment'])){ ?>
						<?php echo $html->showImage ('now.NowDeal', $this->data ['Attachment'], array ('dimension' => 'normal_thumb','style'=>'float:left;')); ?>
						<?php echo $form->hidden ('Attachment.old_id', array ('value' => @$this->data ['Attachment']['id'])); ?>
						<?php } ?>
						<label>Categor&#237;a<span>*</span></label>
						<?php echo $form->input ('deal_category_id', array ('label' => false,'div'=> null,'type'=>'select','options'=>$deal_categories, 'empty' => 'Seleccionar Categoría')); ?>
						<label>Descripci&#243;n<span>*</span></label>
						<?php echo $form->input ('description', array ('type'=>'text','label'=>false,'div'=>null,'cols'=>50,'rows'=>5)); ?>
					</div>
					<p class="titulo titulo-precio">Precio</p>
					<div class="contenedor-precio clearfix">
							<div class="grupoinp precio item1 clearfix">
								<label>Precio Original<span>*</span></label>
								<?php echo $form->input ('original_price', array ('label' => false,'div'=>null,'class'=>'numeric-only'));?>
							</div>

							<div class="grupoinp precio item2 clearfix">
								<label>Con descuento<span>*</span></label>
								<?php echo $form->input ('discounted_price', array ('label' => false,'div'=>null,'class'=>'numeric-only'));?>
							</div>

							<div class="grupoinp precio item3 clearfix">
								<label>% descuento</label>
								<?php echo $form->input ('discount_percentage', array ('label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>

							<div class="grupoinp precio item4 clearfix">
								<label>Imp. a desc.</label>
								<?php echo $form->input ('discount_amount', array ('label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>

							<div class="grupoinp precio item5 clearfix">
								<label>Ahorro</label>
								<?php echo $form->input ('savings', array ('type' => 'text', 'label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>

							<div class="grupoinp precio item5 clearfix">
								<label>Comisi&oacute;n</label>
								<?php echo $form->input ('commission_percentage', array ('type' => 'text', 'label' => false,'div'=> null,'value'=>$company_commission_percentage, 'readonly' => 'readonly'));?>
							</div>
							<?php if(isset($amount_error)){?>
							<div class="error-message" style="clear:both">¡Revise los valores de Precio y Descuento!</div>
							<?php }?>
					</div>
	<!--Segundo Bloque-->
				<p class="titulo titulo-precio">Cupones</p>
				<div class="contenedor-precio clearfix">
						<div class="grupoinp cupones clearfix">
								<label>N&#250;mero m&#225;ximo de cupones por oferta<span>*</span></label>
								<?php echo $form->input ('max_limit', array ('label' => false,'div'=>null,'class'=>'numeric-only')); ?>
								<?php echo $form->hidden ('min_limit', array ('value'=>Configure::read ('NowPlugin.deal.min_limit'))); ?>
						</div>

						<div class="grupoinp cupones clearfix">
								<label>Cantidad m&#225;xima de cupones por persona<span>*</span></label>
								<?php echo $form->input ('buy_max_quantity_per_user', array ('label' =>false,'div'=>null,'class'=>'numeric-only')); ?>
								<?php echo $form->hidden ('buy_min_quantity_per_user', array ('value'=>Configure::read ('NowPlugin.deal.buy_min_quantity_per_user'))); ?>
						</div>
				</div>
	<!--fila 2-->

				<p class="titulo">Programaci&#243;n de la oferta</p>
						<div class="grupoinp validez">
								<label>Fecha de publicaci&#243;n<span>*</span></label>
								<?php echo $form->input ('start_date', array ('error'=>false,'label'=>false,'div'=>null,'readonly'=>'readonly','minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'type' => 'text')); ?>
								<span class="sprite"></span>
								<?php if(isset($this->validationErrors['NowDeal']['start_date'])){ // VALIDACION POSICIONADA A MANO! SINO PINCHA EL LAYOUT!?>
								<div class="error-message"><?php echo $this->validationErrors['NowDeal']['start_date'];?></div>
								<?php }?>
								<p>Formato: (AAAA-MM-DD)</p><br>
								<p class="info f11">Tu oferta puede ser comprada durante este d&#237;a <br>(desde las 00.00 hs hasta la hora de fin de canje).</p>
						</div>
						<fieldset class="contedor-hora">
						<legend>Franja horaria de canje del cup&oacute;n"</legend>
							<div class="grupoinp horainicio">
								<label>Hora de inicio<span>*</span></label>
								<?php echo $form->input('time_range_start', array(
										'label' => false,
										'options' => array_slice(NowTimeSpanOptions::horas(),0,-1,true),
										'type' => 'select',
										'class' =>"sel sel-rempresas",
										'div' => null,
										), false
									);?>
							</div>
							<div class="grupoinp horafin">
								<label>Hora de fin<span>*</span></label>
								<?php echo $form->input('time_range_end', array(
										'label' => false,
										'options' => array_slice(NowTimeSpanOptions::horas(),1,null,true),
										'type' => 'select',
										'class' =>'sel sel-rempresas',
										'div' => null,
										//'disabled' => isset($this->data['NowDeal']['time_range_end'])?'':'disabled',
										), false
									);?>
							</div>
						</fieldset>

						<p class="info2 f11">Esta es la franja horaria en la que el comprador podr&#225; canjear el cup&#243;n.</p>
                                                <?php
                                                $display_canlendarizacion = (Configure::read('NowPlugin.deal.enable_sheduled_deals')== true) ? 'display:block;':'display:none;';
                                                ?>
<fieldset class="contedor-hora" style="height:auto;float:none;margin-bottom: 5px;clear:both;<?php echo $display_canlendarizacion?>">
						<legend>Calendarizaci&oacute;n (<a href="javascript:void(0)" id="CalendarToggler" style="font-size:12px;">
  habilitar
                                                    </a>)</legend>
                                                <div  id="collapsable" style="width:610px;display: none;">
                                                <div class="grupoinp validez" style="margin-top:30px;height:160px;">
								<label>Fecha Final de publicaci&#243;n<span>*</span></label>
								<?php echo $form->input ('scheduled_deals_end_date', array ('error'=>false,'label'=>false,'div'=>null,'readonly'=>'readonly','minYear' => date ('Y'),  'maxYear' => date ('Y') + 10, 'type' => 'text')); ?>
								<span class="sprite"></span>
								<?php if(isset($this->validationErrors['NowDeal']['scheduled_deals_end_date'])){ // VALIDACION POSICIONADA A MANO! SINO PINCHA EL LAYOUT!?>
								<div class="error-message"><?php echo $this->validationErrors['NowDeal']['scheduled_deals_end_date'] ?></div>
								<?php }?>
								<p>Formato: (AAAA-MM-DD)</p><br>
								<p class="info f11">Tu oferta puede ser comprada hasta este d&#237;a </p>
						</div>



                                <div class="">
									<?php

                                                                        echo $form->input('scheduled_deals',
                                                                        array ('options' =>$dias ,
                                                                            'type'=>'select',
                                                                            'multiple'=>'checkbox',
                                                                            'legend' => false,
                                                                            'div' => 'weekdays',
                                                                            'selected' =>$dias_selected,
                                                                            'label' => 'Publicar los siguientes días',  )); ?>
                                    </div>
                                                    </div>
                                </fieldset>
				<!-- p class="titulo repetir">Repetir esta oferta</p>
						<div class="repetir-oferta">
							<label>Selecciona cuando deseas repetir esta oferta<span>*</span></label>
							<select>
								<option>1</option>
								<option>2</option>
								<option>3</option>
							</select>
							<p class="info f11">* no estamos teniendo en cuenta los dias feriados existentes</p>
						</div>
				-->
				<p class="titulo">Sucursales</p>
			<?php if(!@count($branches)){ ?>
				<div class="grouplabel">
					<div class='error-message'>No hay Sucursales</div>
				</div>
			<?php }else{?>
				<?php if($form->isFieldError('branches')){ echo $form->error('branches');}?>
				<?php  echo $form->input ('company_id', array ('type' => 'hidden')); ?>
				<?php if(count($branches) == 1){ ?>
					<div class="grouplabel">
						<input id="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>" type="checkbox" value="<?php echo $branches[0]['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
						<label for="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>"><?php echo $branches[0]['NowBranch']['name'];?></label>
					</div>
				<?php }else{?>
					<?php foreach($branches as $sucursal){?>
					<div class="grouplabel">
						<?php if(isset($this->data['NowDeal']['branches'])){ ?>
						<?php $chkd = (in_array($sucursal['NowBranch']['id'],array_values($this->data['NowDeal']['branches'])))?true:false;?>
						<input id="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>" type="checkbox" value="<?php echo $sucursal['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" <?php if($chkd){ ?>checked="checked"<?php }?> />
						<?php }else{ ?>
						<input id="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>" type="checkbox" value="<?php echo $sucursal['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
						<?php }?>
						<label for="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>"><?php echo $sucursal['NowBranch']['name'];?></label>
					</div>
					<?php }?>
				<?php }?>
			<?php }?>
					<?php echo $form->hidden ('save_as_draft',array('value'=>'0'));?>

							<div class="btn-opciones">
								<div class="guardarborrador fll">
									<a href="#" class="sub-link" rel="1">Guardar en borrador</a>
									<a href="#" class="deal-preview" rel="2">Previsualizar</a>
								</div>

								<div class="flr">
								<input type="submit" class="btn-azul-d addsbottom" value="Solicitar Aprobaci&#243;n">
								<?php echo $html->link('Cancelar',array('action'=>'my_deals'));?>
								</div>
							</div>
				</div>

				</div>


		</div>
		<?php echo $form->end (); ?>
	</div>
</div>
<script type="text/javascript">

     function toggleOnCalendarRadioOn()
    {
        var fecha = new Date;


        <?php if(isset($js_end_date) && $js_end_date != "0000-00-00"){ ?>
        $("#NowDealScheduledDealsEndDate").val('<?php echo $js_end_date;?>');
        <?php } else { ?>
        $("#NowDealScheduledDealsEndDate").val(fecha.getFullYear()+"-"+(("0" + (fecha.getMonth() + 1)).slice(-2))+"-"+("0" + fecha.getDate()).slice(-2));1
        <?php } ?>
        $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('disabled',false);
     //   $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('checked',true);
        $("#NowDealScheduledDealsEndDate").removeAttr('disabled');


    }
    function toggleOnCalendarRadioOff()
    {
        $("#NowDealScheduledDealsEndDate").val('');
        $("#NowDealScheduledDealsEndDate").attr('disabled',true);

     //   $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('checked',false);
        $("input[type=checkbox][name*='data[NowDeal][scheduled_deals][]']").attr('disabled',true);


    }



        $('#CalendarToggler').click(function(){
            $('#collapsable').slideToggle("slow");

            if($("#NowDealScheduledDealsEndDate").val()==""|| $.trim($('#CalendarToggler').text()) =="habilitar"){
                toggleOnCalendarRadioOn()
                $(this).text('deshabilitar');
            } else {
                $(this).text('habilitar');
                toggleOnCalendarRadioOff()

            }


        });

    $(document).ready(function(e){

if($("#NowDealScheduledDealsEndDate").val()=="0000-00-00")
    {
        $("#NowDealScheduledDealsEndDate").val("");
    }

        <?php

        if((isset($js_end_date) &&$js_end_date!="0000-00-00") || isset($this->validationErrors['NowDeal']['scheduled_deals_end_date'])){ ?>

            $('#collapsable').slideToggle("slow");
            toggleOnCalendarRadioOn()
                $('#CalendarToggler').text('deshabilitar');
<?php } ?>



//$("input[name^='NowDealScheduledDeals'").disable();


		//$('#NowDealSaveForm').unbind('submit');
		$('#NowDealSaveForm').submit(function(e){

                    if($("#NowDealScheduledDealsEndDate").val()=="0000-00-00")
                        {
                         $("#NowDealScheduledDealsEndDate").val("");
                        }

                    if($.trim($('#CalendarToggler').text()) =="habilitar")
                    {
                        $("#NowDealScheduledDealsEndDate").val("");
                    }
                      $('input[type=submit]').attr('disabled', 'disabled');


			if(!$('input[name="data[NowDeal][branches][]"]:checked').length){
				e.preventDefault();
				alert("¡Debés elegir al menos una sucursal!");
			}
			/*
			if(!$('#NowDealStartDate').attr('value').length){
				$('#NowDealStartDate').css('backgroundColor','#F00');
				$('#NowDealStartDate').animate({backgroundColor:  "#FFF"}, "slow");
				e.preventDefault();
			}
			if(!$('#NowDealDiscountPercentage').attr('value').length){
				$('#NowDealOriginalPrice').css('backgroundColor','#F00');
				$('#NowDealOriginalPrice').animate({backgroundColor:  "#FFF"}, "slow");
				$('#NowDealDiscountedPrice').css('backgroundColor','#F00');
				$('#NowDealDiscountedPrice').animate({backgroundColor:  "#FFF"}, "slow");
				e.preventDefault();
			}
			if($('#NowDealTimeRangeEnd').attr('disabled')){
				$('#NowDealTimeRangeStart').css('backgroundColor','#F00');
				$('#NowDealTimeRangeStart').animate({backgroundColor:  "#FFF"}, "slow");
				$('#NowDealTimeRangeEnd').css('backgroundColor','#F00');
				$('#NowDealTimeRangeEnd').animate({backgroundColor:  "rgb(236, 233, 216)"}, "slow");
				e.preventDefault();
			}
			*/
		});

		/* Si deselecciona todas las sucursales le doy una alerta y cancelo la ultima "des-seleccion" */
		//$('input[name="data[NowDeal][branches][]"]').unbind('click');
		$('input[name="data[NowDeal][branches][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][branches][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos una sucursal!");
			}
		});

                $('input[name="data[NowDeal][scheduled_deals][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][scheduled_deals][]"]:checked').length == 0){
				e.preventDefault();
				alert("¡Debés elegir al menos un día!");
			}
		});

		$('a.sub-link').click(function(e){

                     if($("#NowDealScheduledDealsEndDate").val()=="0000-00-00")
                     {
                       $("#NowDealScheduledDealsEndDate").val("");
                     }

                    if($.trim($('#CalendarToggler').text()) =="habilitar")
                    {
                        $("#NowDealScheduledDealsEndDate").val("");
                    }
			e.preventDefault();
			$('input#NowDealSaveAsDraft').attr('value',$(this).attr('rel'));
			$('form#NowDealSaveForm').submit();
		});


		jQuery.fn.reverse = [].reverse; // EL PLUGIN MAS CORTO DE LA HISTORIA!!!
		//$('#NowDealTimeRangeStart').unbind('change');
		$('#NowDealTimeRangeStart').change(function(e){
			$('option','#NowDealTimeRangeEnd').reverse().each(function(i,o){
				if(parseInt($(o).val()) <= parseInt($(e.target).val())){
					$(o).attr('disabled','disabled');
				}else{
					$(o).removeAttr('disabled');
				}
			});

			if(parseInt($('#NowDealTimeRangeStart').val()) >= parseInt($('#NowDealTimeRangeEnd').val())){
        $('#NowDealTimeRangeEnd').children('option').removeAttr('disabled');
				$('#NowDealTimeRangeEnd').val(parseInt($('#NowDealTimeRangeStart').val())+3);
			}


      $('#NowDealTimeRangeEnd').children('option').filter(function () {
        return parseInt($(this).val(), 10) < (parseInt($('#NowDealTimeRangeStart').val(), 10) + 3);
      }).attr('disabled','block');

			$('#NowDealTimeRangeEnd').css('backgroundColor','#FFF');
			$('#NowDealTimeRangeEnd').removeAttr('disabled');
		});

		//$('a.deal-preview').unbind('click');
		$('a.deal-preview').click(function(e){
			e.preventDefault();
			$('#form-overlay').fadeTo('slow',0.2);
			$.post("/now/now_deals/now_deal_preview", $("#NowDealSaveForm").serialize(),function(dat){
				$('#form-overlay').fadeOut();
				$.colorbox({
					html:dat,
					height:580,
					width:815,
					speed: 500,
					onLoad:function(){
						$('#cboxClose').remove();
					}
				});
			});
		});

		//$(".numeric-only").unbind("keyup");
		$(".numeric-only").keyup(function(e){
			$(this).val($(this).val().replace(/[^0-9]/g,''));
		});

		/*
		$('a.deal-preview').unbind('click');
		$('a.deal-preview').click(function(e){
			e.preventDefault();
			$('#form-overlay').fadeIn();
			$('#fofra').remove();
			var ifr = $('<iframe></iframe>').attr({'id':'fofra','name':'fofra','style':'display:none','src':'about:blank'}).load(function(e){
				$('input#NowDealSaveAsDraft').attr('value','0');
				$('#form-overlay').fadeOut();
				//$('#fofra').fadeIn();
				$('div.error-message').remove();
				$('#fofra').contents().find('div.error-message').each(function(i,o){
				  var it = $(o).prev().attr('id');
				  $('#'+it).after($(o));
				});
				var did = false;
				try{
				  did = jQuery.parseJSON($('#fofra').contents().text());
				}catch(e){
				  alert("Error al listar la oferta!");
				}
				if(did) $.colorbox({href:"/now/now_deals/detalles/"+did.id,height:580,width:815,onLoad:function(){$('#cboxClose').remove();}});
			});
			$('input#NowDealSaveAsDraft').attr('value','2');
			$('#NowDealSaveForm').attr('action','/now/now_deals/otro/').attr('target','fofra').after(ifr).submit();
		});
		*/
	});
</script>
