<?php
$horario = Configure::read ('NowPlugin.time_range');
?>




<div class="grid_16" style="width:520px;border: 1px solid; padding:3px;border-radius: 3px 3px;margin:5px;">
    <h3>General</h3>
<ul>
    <li>
        <span>
            Nombre:
        </span>
        <span style="font-weight:bold;"> <?php echo $deal['name']; ?></span>
    </li>
    <li>
        <span>
            texto descriptivo:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['descriptive_text']; ?></span>
    </li>
    <li>
        <span>
            Fecha de inicio:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['start_date']; ?></span>
    </li>
    <li>
        <span>
            Horario:
        </span>
        <span style="font-weight:bold;">
		<?php
			if(is_numeric($deal['time_range'])){
				$time_frame = NowTimeSpanOptions::vars();
				echo  $time_frame[$deal['time_range']][2];
			}else{
				$time_frame = NowTimeSpanOptions::fix();
				echo  $time_frame[$deal['time_range']];
			}
		?></span>
    </li>
    <li>
        <span>
            Categoria:
        </span>
        <span style="font-weight:bold;"><?php  echo AppHelper::getCategoryName($deal['deal_category_id']) ?></span>
    </li>

    <li>
        <span>
            Nro. Max de compradores:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['buy_max_quantity_per_user']; ?></span>
    </li>
    <li>
        <span>
            Cantidad máxima de compra:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['max_limit']; ?></span>
    </li>




</ul>
</div>

<div class="grid_16" style="width:520px;border: 1px solid; padding:3px;border-radius: 3px 3px;margin:5px;">
    <h3>Precio</h3>
<ul>
      <li>
        <span>
            Precio original:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['original_price']; ?></span>
      </li>
      <li>
        <span>
            Precio con descuento:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['discounted_price']; ?></span>
      </li>
      <li>
        <span>
            Descuento redondeado (%) :
        </span>
       <span style="font-weight:bold;"> <?php echo $deal['discount_percentage']; ?></span>
      </li>
        <span>
            Importe adescontar:
        </span>
        <span style="font-weight:bold;"><?php echo $deal['discount_amount']; ?></span>
      </li>
</ul>
<?php if(ConstUserTypes::isLikeAdminOrPartner($auth->user('user_type_id'))): ?>
<h3>Comision</h3>
<ul>
    <li>
        <span>
            Comision :
        </span>
        <span style="font-weight:bold;"><?php echo $deal['commission_percentage']; ?></span>
    </li>

</ul>
<?php endif; ?>

</div>
<div class = "clear"></div>
<div class="grid_16" style="width:520px;border: 1px solid; padding:3px;border-radius: 3px 3px; margin:5px;">
    <h3>Sucursales</h3>
<ul>
    <li>
        <?php if($has_branches){
            ?>
        <span style="font-weight:bold;">|
            <?php
            echo $branch_names;
            ?>
            </span>
            <?php


        }
        else {
            ?>
            <span class="error">No has seleccionado sucursales</span>
            <?php
        }
             ?>
    </li>
</ul>
</div>





<div class = "clear"></div>


<div class = "submit-block grid_12">
<input class="btn-naranja btn-reg-enviado"  type="button" value="Cargar oferta" onclick="$('#NowDealAddForm').trigger('submit')"/>



<input class="btn-naranja btn-reg-enviado"  type="button" value="Cancelar" onclick="$.fn.colorbox.close()"/>
</div>
</div>