<?php

echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.form'); ?>
<style>

  div.pagecount p {
    clear: both;
    color: gray;
    display: block;
    float: left;
    margin: 10px auto 15px;
    text-align: center;
    width: 100%;
  }

  div.pageinfo span {
    font: bold 13px arial;
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    float: left;
    font: 12px arial;
    margin: 0 5px;
    padding: 7px 13px;
  }

  div.pageinfo  {
    margin-top: 20px;
    display: block;
  }


  div.pageinfo span .current {
    border: medium none;
    font: bold 14px arial;
    padding: 6px 10px;
  }


  .export-cupons-link-disabled {
    color: #777;
  }

    .btn-azul-d{
        width:90px !important;
        float:left;
    }

    #ccnow .contenido-form{
        border:none;
        border-radius:none;
        box-shadow:none;
        padding:0;
        width:920px;
    }

    #loadForm{
        width: 200px;
        height: 140px;
        }

    #mensajes{
        padding:25px 0;
        text-align: center;
        background:#057FA6;
        color:#fff;
        font-weight: bold;
        /*visibility: hidden;*/
        }
    /****************************/
    #shadow{
        background:url(/img/shadow.png);
        position:absolute;
        left:0;
        top:0;
        width:100%;
        z-index:1000;
        }
        #posnet-is_returned,
        #posnet-form{
            width:390px;
            margin:0 auto;
            margin-top:180px;
            background:#fff;
            color:#333;
            position: relative;
            -moz-box-shadow: 10px 5px 5px black;
            -webkit-box-shadow: 10px 5px 5px black;
            box-shadow: 10px 5px 5px black;
            }


            #posnet-is_returned .close-shadow,
            #posnet-form .close-shadow{
                position: absolute;
                right:12px;
                top:  12px;
                cursor: pointer;
                }

            #posnet-is_returned form, #posnet-is_returned input,
            #posnet-form form, #posnet-form input{
                float:none;
                }

            #posnet-is_returned h2, #posnet-is_returnedh3, #posnet-is_returned form,
            #posnet-form h2, #posnet-form h3, #posnet-form form{
                padding: 10px;
                }
            #posnet-is_returned h2,
            #posnet-form h2{
                background:#90C400;
                margin:0;
                color:#fff;
                font-weight:bold;
                font-size:20px;
                }

            #posnet-is_returned h3,
            #posnet-form h3{
                margin:0;
                color:#777;
                font-weight:bold;
                }

                #posnet-is_returned output2,
                #posnet-form #output2{
                margin:0;
                color:#777;
                font-weight:bold;
                padding:15px;
                }

                #posnet-is_returned .redimir,
                #posnet-form form .redimir{
                    background:#F57F24;
                    color:#fff;
                    font-weight:bold;
                    border:none;
                    padding: 3px;
                    width: 80px;
                    }

                    #posnet-is_returned h3{
                        padding: 10px;
                    }
                    
.mcp1 {
    padding: 0 5px !important;
}
.mcp2 {
    padding: 0px !important;
    margin: 0px !important;
    width: 100% !important;
}

</style>
<?php if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){ ?>
<div class="paso3addofertas adds-suc mcp1">
	<div class="contenido-form clearfix mcp2" style="position:relative;">
		<div id="form-overlay" style="display:none; position: absolute; width: 100%; height: 100%; background: none repeat scroll 0% 0% black; opacity: 0.21; left: 0px; top: 0px; border-radius: 5px 5px 5px 5px;"></div>
		<div class="encabezado">
			<p class="subt" style="text-align:center;">B&uacute;squeda de Cupones</p>
		</div>
		<div class="form-fmt clearfix">
                    <?php
                    $display_filters = (!isset($this->params['named']['filter_id'])) ? '' :'display:none';
                    ?>
			<div class="search-cup clearfix" style="<?php echo$display_filters; ?>">
				<div class="form-fmt clearfix">
					<div class="grupoinp w8 clearfix">
						<label>Buscar por DNI</label>
						<?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
						<?php echo $form->input('dni',array('label'=>false,'div'=>null, 'fieldName' => 'dni'));?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
						<?php echo $form->end(); ?>
					</div>
					<div class="grupoinp w8 clearfix">
						<label>Buscar por DNI de un amigo</label>
						<?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
						<?php echo $form->input('friend_dni',array('label'=>false,'div'=>null, 'fieldName' => 'friend_dni'));?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
						<?php echo $form->end(); ?>
					</div>
					<div class="grupoinp w8 clearfix">
						<label>Buscar por Nro. de cup&#243;n</label>
						<?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
						<?php echo $form->input('cupon',array('label'=>false,'div'=>null, 'fieldName' => 'cupon'));?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
						<?php echo $form->end(); ?>
					</div>
					<div class="grupoinp w8 clearfix">
						<label>Buscar por Nombre de usuario</label>
						<?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
						<?php echo $form->input('username',array('label'=>false,'div'=>null, 'fieldName' => 'username'));?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
						<?php echo $form->end(); ?>
					</div>
					<div class="grupoinp w8 clearfix">
						<label>Buscar por Posnet code</label>
						<?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
						<?php echo $form->input('posnet',array('label'=>false,'div'=>null, 'fieldName' => 'posnet'));?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
						<?php echo $form->end(); ?>
					</div>
					<div class="grupoinp w8 clearfix">
            <label>Buscar por Fecha de compra</label>
            <?php echo $form->create('NowDeal', array ('action' => 'my_cupons', 'class' => 'normal searchFieldForm', 'enctype' => 'multipart/form-data'));?>
            <?php echo $form->input ('start_date', array ('label'=>false,'class'=>' searchFieldForm','div'=>null, 'fieldName' => 'start_date', 'readonly'=>'readonly','minYear' => date ('Y')-10, 'maxYear' => date ('Y') , 'type' => 'text', 'style'=>"width:69px; margin-right:3px; margin-left:3px;")); ?>
            <?php echo $form->input ('end_date', array ('label'=>false,'class'=>' searchFieldForm','div'=>null, 'fieldName' => 'end_date', 'readonly'=>'readonly','maxYear' => date ('Y'), 'maxYear' => date ('Y'), 'type' => 'text','style'=>"width:69px; clear:none;margin-right:3px; margin-left:3px;")); ?>
						<?php echo $form->input('page',array('label'=>false,'div'=>null, 'fieldName' => 'page', 'type' => 'hidden', 'value' => 1), array('class' => 'page'));?>
						<input type="image" src="/img/now/buscar-cupon.png" style="clear: none; width: 22px; border: 0px none;">
            <?php echo $form->end(); ?>
           		 <div style="float:right;padding-top:20px"> 
            		<?php echo $form->button('Limpiar Fechas',array('style'=>'text-align:center;width:140px !important','class' => 'btn-azul-d', 'div'=>false, 'id'=>'fecha-reset-button')); ?>
          		 </div>
          </div>


				</div>
			</div>

    <div class="export-form">
    <?php if(isset($dealUser) && !empty($dealUser)){ ?>
      <?php  echo $html->link (
                    'Exportar cupones '.$html->image('icon-export.png'),
                    array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'download_my_cupons', 'filter_id' => $this->params['named']['filter_id']),
                    array(
                          'href' => "javascript:void(0)",
                          'escape' => false,
                          'id' => 'export-cupons-link',
                          'class'=> 'export-cupons-link export-cupons-link-disabled link2 tip',
                          'style' => 'float: right; margin-top: 20px; margin-right: 5px;',
                          'base-url' => Router::url(array(
                            'plugin' => 'now',
                            'controller' => 'now_deals',
                            'action' => 'download_my_cupons'))
                          )); ?>
     <?php }?>
      </div>
  
			<div class="listasuc listacup clearfix" style="margin-top: 0px" >

			<div id="cupons_loader" style="display:none;width: 100%;text-align:center;"><img src="/img/now/loading.gif"></div>
			<div id="cupons_list">
<?php }?>
			<?php if(isset($dealUser)){?>
 <div style="float:rigth;width:100%">
                <?php  echo $html->link (
                    'Exportar cupones '.$html->image('icon-export.png'),
                    array ('plugin'=>'now','controller'=>'now_deals',  'action' => 'download_my_cupons', 'filter_id' => $this->params['named']['filter_id']),
                    array(
                          'href' => "javascript:void(0)",
                          'escape' => false,
                          'id' => 'export-cupons-link',
                          'class'=> 'export-cupons-link export-cupons-link-disabled link2 tip',
                          'style' => 'float: right; margin-top: 20px; margin-right: 5px;',
                          'base-url' => Router::url(array(
                            'plugin' => 'now',
                            'controller' => 'now_deals',
                            'action' => 'download_my_cupons'))
                          )); ?>
                </div>
    <!-- Ventana de redencion -->
    <div id="posnet-form" style="display:none;">
        <div class="close-shadow"><img src="/img/colorbox/cross.png" alt="Cerrar"/></div>
            <h2>Redimir Cup&oacute;n</h2>
            <form action="#" name="" id="posnet_code_submit">
                <h3>Ingrese los &uacute;ltimos 4 d&iacute;gitos del pin posnet para poder redimir:</h3>
                <input id="posnet_code" name="posnet_code" type="text" maxlength="4" /><br/>
                <div id="posnet_price_div" style="display:none">
                <h3>Ingrese el importe </h3>
                <input id="posnet_price" name="posnet_price" type="text" maxlength="12" />
                </div>
                <br />
                <input type="submit" value="Redimir" class="btn-azul-d" />
            </form>
        <div id="output2"></div>
    </div>

    <div id="posnet-is_returned"  style="display:none;">
        <div class="close-shadow"><img src="/img/colorbox/cross.png" alt="Cerrar"/></div>
            <h2>Redimir Cup&oacute;n</h2>
            <h3>No se puede redimir el cupón</h3>

    </div>
    <!-- Fin ventana de redencion -->

    <script>

        $(document).ready(function() {

            //REDENCION ------------------------------------------------------------------

            //Ordenamos visualmente
            closeDialog();
            $("#mensajes").hide();
            $("#posnet-form").appendTo("#shadow"); //div usado de velo, elements/footer.ctp
            $("#posnet-is_returned").appendTo("#shadow");

            //Eventos --------------------------------------------------------------------

            //Apertura
            $('.redencion-posnet').click(function()
            {
                $.id = $(this).attr('name');
                $.is_sms = $(this).attr('is_sms');
                $.is_returned = $(this).attr('is_returned');

                $("#shadow").css("height", $(document).height());

                $("#shadow").show();
                if($.is_returned==1) {
                    $("#posnet-is_returned").show();
                    $("#posnet-form").hide();
                } else {

                    $("#posnet-form").show();
                    $("#posnet-is_returned").hide();
                    if( $.is_sms == 1 )
                    {
                        $("#posnet_price_div").show();
                    }

                    $('#output2').empty();
                    $('#posnet_code').val('');
                    $('#posnet_price').val($(this).attr('discounted_price'));

                    //Opciones para el envio de redencion del cupon
                    var options = {
                        target:  '#output2',
                        success: exito,
                        type:    'post',
                        data:    {deal_user_id: $.id},
                        beforeSubmit:function()
                        {
                            $('#output2').html('enviando datos');
                        },
                        //clearForm: true,
                        url : '<?php echo Router::url('/', true); ?>redemptions/redeemcoupon'
                    };
                    // bind form
                    $('#posnet_code_submit').ajaxForm(options);

                    //evitamos saltos...
                    //return false;
                }
                $('html, body').animate({scrollTop:0}, 'slow');

                $("#shadow").css("height", $(document).height());

            })

            $('.no_match_on_redemption').click(updateDealWithNoRedemptionMatch);

            function updateDealWithNoRedemptionMatch()
            {
                var titulo = $(this).attr('name');
                var request = $.ajax({
                    url : '<?php echo Router::url('/', true); ?>redemptions/redeemcoupon',
                    type: "POST",
                    data: {deal_user_id : titulo, posnet_code:'no_aplica'},
                    dataType: "html",
                    success: function(html){
                        //$('#cupon'+titulo).remove();
                        //$("#mensajes").slideDown(1000).delay(2000).slideUp(500);
                    }
                });
            }

            //Cierre
            $('.close-shadow').live('click',function()
            {
                closeDialog();
            });

            //Internals
            function exito(responseText, statusText, xhr, $form)
            {
                if(responseText == 'La redención fue exitosa')
                {

                    fecha = new Date();


                                                $('#cupon'+$.id).replaceWith("<br/><span  style='display:block;float:left;padding:3px;margin-left:5px;'>Redimido el <?php echo date("d/m/Y"); ?> <br/>via web.</span>")

                    closeDialog();
                    $("#mensajes").slideDown(1000).delay(2000).slideUp(500);
                }
            //
            }

            // / REDENCION ---------------------------------------------------------------

        });

        function closeDialog()
        {
            $("#shadow").hide();
            $("#posnet-form").hide();
             $("#posnet-is_returned").hide();
            $("#posnet_price_div").hide();
            $('#output2').empty();
            $('#posnet_code').val('');
            $('#posnet_price').val('');
        }
    //
function zeroPad(num,count)
{
    var numZeropad = num + '';
    while(numZeropad.length < count) {
    numZeropad = "0" + numZeropad;
    }
    return numZeropad;
}
    </script>
 

                <div id="mensajes">
                    El cup&oacute;n fue redimido exitosamente
                </div>
                   
				<div class="linea-item titlelist clearfix">
					<label class="lab10">ID Oferta</label>
					<label class="lab1">Fecha de compra</label>
					<label class="lab2">Cupón<br>&nbsp;</label>
					<label class="lab3">Usuario</label>
					<label class="lab5">DNI<br>&nbsp;</label>
					<label class="lab6">Precio ($)</label>
					<label class="lab7">Fecha de vencimiento</label>
					<label class="lab8">Es regalo</label>
					<label class="lab9">DNI amigo<br>&nbsp;</label>
					<!-- label class="lab10">Fec. Nac. amigo</label-->
					<label class="lab11">Acciones</label>
				</div>
				<?php foreach($dealUser as $key=>$deal){?>
          <? $dob = (preg_match('/0000-00-00 00:00/', $deal['DealUser']['gift_dob']) || empty($deal['DealUser']['gift_dob'])) ? "":
                    date('Y-m-d',strtotime($deal['DealUser']['gift_dob'])); ?>
          <div class="linea-item data clearfix <?php if($key%2){?>bg-linea<?php }?>">
            <?php echo $html->link( $deal['DealUser']['deal_id'], array('plugin'=>'now','controller' => 'now_deals', 'action' => 'my_deals', 'deal_id' => $deal['DealUser']['deal_id'], 'is_now' => $deal['Deal']['is_now']), array('class' => 'link1'));   ?>
						<label class="lab1"><?php echo date("d-m-Y H:i",strtotime($deal['DealUser']['created']));?></label>
						<label class="lab2"><?php echo $deal['DealUser']['coupon_code']?></label>
						<label class="lab3"><?php echo $deal['User']['username']?></label>
						<label href="#" class="lab5"><?php echo $deal['UserProfile']['dni']?></label>
						<label href="#" class="lab6"><?php echo $deal['Deal']['discounted_price']?></label>
						<label href="#" class="lab7"><?php echo date("d-m-Y H:i",strtotime($deal['Deal']['coupon_expiry_date']));?></label>
						<label href="#" class="lab8"><?php echo empty($deal['DealUser']['gift_to'])?"No":"Si";?></label>
						<label href="#" class="lab9"><?php echo empty($deal['DealUser']['gift_to'])?"":$deal['DealUser']['gift_dni'];?></label>
						<!-- label href="#" class="lab10"><?php echo $dob; ?></label-->
						<?php echo $html->link('Imprimir Cup&oacute;n',array('plugin'=>'','controller' => 'deal_users', 'action' => 'view', $deal['DealUser']['id'],$deal['Deal']['id'],'type' => 'print'),array('escape'=> false,'target'=>'_blank', 'class'=>'link1', 'title'=>'Imprimir'));?>
						<?php echo $html->link('Ver Cup&oacute;n',array('plugin'=>'','controller' => 'deal_users', 'action' => 'view',$deal['DealUser']['id'],$deal['Deal']['id'],'admin' => false),array('escape'=> false,'class'=>'link1 js-thickbox2', 'title'=>'Ver Cupon'));?>

                        <script>
                            $(".js-thickbox2").colorbox({width:'690px', height:'800px'});
                        </script>



                        <!-- Redencion cupones PosNet   -->
                            <?php
                                //revisamos si esta redimido o no...
                   //             if((empty($deal['Redemption'][0]['redeemed'])&& empty($deal['Redemption'][0]['way']) && $user_type_id == ConstUserTypes::Company)&& $deal['DealUser']['is_used']==0 )
                    //            {
                                  // BOTON REDIMIR  visualizar BOTON 
                                  $hours=0;
                                  if ($deal['Deal']['is_now']){
                                    $hours = Configure::read('deal.extended_hours_to_redeem_now');   
                                  }
                                  if ($deal['Deal']['company_id']==18){
                                    $hours = Configure::read('deal.extended_hours_to_redeem_imagena');
                                  }
                                  if(($deal['DealUser']['is_returned']==0 && ($deal['Deal']['is_now']||$deal['Deal']['company_id']==18) && (strtotime($deal['Deal']['coupon_expiry_date'].'+'.$hours.' hours') >= time())) && $deal['DealUser']['is_used']==0 || strtotime($deal['Deal']['coupon_expiry_date']) >= time() && $deal['DealUser']['is_used']==0 && empty($deal['Redemption'][0]['way'])) {
                                    //IMPORTANTE REVISAR ESTA CONDICION!!!!!!!
                                    $css_class =$has_remdemption_record ? 'redencion-posnet' : 'no_match_on_redemption';
                                    echo '<a href="javascript:void(0)" name="' . $deal['DealUser']['id'] . '" id="cupon' . $deal['DealUser']['id'] . '" discounted_price="' . $deal['Deal']['discounted_price'] . '" is_sms="'.($deal['Deal']['publication_channel_type_id'] == 2 ? "1" : "0").'" class="redencion-posnet '.$css_class.' btn-azul-d" title="Redimir cupon via POSNET"  is_returned="'.($deal['DealUser']['is_returned'] == 1 ? "1" : "0").'">';
                                    echo 'Redimir CUP&Oacute;N';
                                    echo '</a>';

                                  }
                                  if ($deal['DealUser']['is_returned']==1 && ($deal['Deal']['is_now']||$deal['Deal']['company_id']==18) && $deal['DealUser']['is_used']==0)  {
                                    echo "</br> </br> <span style='display:block;float:left;padding:3px;margin-left:5px;'> Anulado</span>";
                                    if (strtotime($deal['Deal']['coupon_expiry_date']) <= time() && $deal['DealUser']['is_used']==0) {
                                        echo "</br> </br> <span style='display:block;float:left;padding:3px;margin-left:5px;'> Vencido</span>";
                                    }

                                  } else {
                                    //Visualizar Leyenda dependiendo de la modalidad
                                    if (strtotime($deal['Deal']['coupon_expiry_date'].'+'.$hours.' hours') <= time() && $deal['DealUser']['is_used']==0) {
                                        echo "</br> </br> <span style='display:block;float:left;padding:3px;margin-left:5px;'> Vencido</span>";
                                    } elseif ($deal['DealUser']['is_used']==1)  {  
                                        echo "<br/><span style='display:block;float:left;padding:3px;margin-left:5px;'>Redimido el ". date("d/m/Y", strtotime($deal['Redemption']['redeemed'])). "<br/>via ".$deal['Redemption']['way']."</span>";
                                    }
                                  }
                                     
                            ?>

                        <!-- / Redencion cupones PosNet -->


					</div>

				<?php }?>

        <div class='pagedata dealspaginador clearfix'>
        <div class='pageinfo pagborder'><?php echo $paginator->numbers(array('modulus' => 2,'skip' => '<span class="skip">&hellip;.</span>','separator' => " \n",'before' => null,'after' => null,'escape' => false)); ?></div> 
        <div class='pagecount'>
          <?php echo $paginator->counter(array('format' => '<p><span>%start%-%end%</span> de %count% Cupones</p>')); ?>
        </div>
        </div>

			<?php }else{
				
					?>
					
					<?php if(!$inicio){ ?>
					<center style="font: 16px arial; color:#777;">No se encontraron cupones.</center>
					<?php } ?>
			<?php }?>
<?php if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){ ?>

			</div>

		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//$('form').unbind('submit');
		$('.searchFieldForm').submit(function(e){
			e.preventDefault();
			formulario = this;
      $('.export-cupons-link').addClass('export-cupons-link-disabled');
      $('.export-cupons-link').attr('href', 'javascript:void(0)');
			if($(this).find('input[type="text"]').val()){
				$("#cupons_list").hide("blind",function(e){
					$("#cupons_loader").show("blind");
				});
				$(formulario).find('input[type="text"]').css('borderColor','#080');
        $(formulario).attr('current', '1');
				$.post($(this).attr('action'),$(this).serialize(),function(data){
					$(formulario).find('input[type="text"]').css('borderColor','#DDD');
					$("#cupons_list").html(data);
					$("#cupons_loader").hide("blind",function(){
						$("#cupons_list").show("blind")
					});

          href = $('.export-cupons-link' ).attr('base-url');
          $(formulario).find('input[type="text"]').each( function(index) {
              console.log(":::::::::::::::::::::::"+index+"::::::::::::::::::::");
              nameField = $(this).attr('fieldName');
              console.log("nameField");
              console.log(nameField);
              valueField = $(this).attr('value');
              console.log("valueField");
              console.log(valueField);
              href += '/' + nameField + ':' + valueField;
          });

          $('.export-cupons-link').attr('href', href);
      //    $('.export-cupons-link').show();
          $('.export-cupons-link').removeClass('export-cupons-link-disabled');

          $('.pageinfo a').attr('href', 'javascript:void(0)');
          $('.pageinfo a').click(function() {
            $('.searchFieldForm[current=1]').children('input#NowDealPage').attr('value', $(this).text());
            $('.searchFieldForm[current=1]').submit();
          });

          anchoPaginador = 0;
          $('.pagborder span').each(function(){
            anchoPaginador += $(this).outerWidth()+25;
          });
          $('.pagborder a').each(function(){
            anchoPaginador += $(this).outerWidth();
          });

          $('.pagborder').width(anchoPaginador);

				});
			}else{
				//alert("Ten&eacute; que completar el campo antes de buscar");
				$(formulario).find('input[type="text"]').css('backgroundColor','#F00').animate({'backgroundColor':'#FFF'},"slow");
			}
		});
                $('form.non_ajax').unbind('submit');
    
    $('#export-cupons-link').click(function() {
        baseUrl = $('#export-cupons-link').attr('base-url');
        href = $('#export-cupons-link').attr('href');
        if (href === baseUrl) {
            return false; 
        } else {
            return true;
        }
    });
    $('#fecha-reset-button').click(function() {
        
    	$('#NowDealStartDate').val('');
    	$('#NowDealEndDate').val(''); 
    });

	});
</script>
<?php }?>
