<?php
$field_names = "Título de la Oferta;Fecha de publicación;Nombre de usuario;DNI;Monto pagado ($);Cupón;Es regalo;Nombre del amigo;Fec Nac Amigo;DNI amigo;Fecha de expiración;Usado;Fecha de Redención;Modo de Redención";


  if($print) {
    $output .="<tr><td>";
    $output .= join ("</td><td>",  explode(";",$field_names ));
    $output .="</td></tr>";
  } else {
    $output =  html_entity_decode( $field_names ,ENT_COMPAT,'utf-8')."\r\n";
  }

if(isset($dealUser)){

    foreach($dealUser as $key=>$deal) {

        $row = false;
        /*
         * caracteres especiales se veian feos en el excell
         */
        $row[] = html_entity_decode($deal['Deal']['name'], ENT_COMPAT,'utf-8');
        $row[] = date("Y-m-d",strtotime($deal['Deal']['start_date']));
        $row[] = $deal['User']['username'];
        $row[] = $deal['UserProfile']['dni'];
        $row[] = $deal['Deal']['discounted_price'];
        $row[] = $deal['DealUser']['coupon_code'];

        $row[] = empty($deal['DealUser']['gift_to'])?"No":"Si";
        /*
         * caracteres especiales se veian feos en el excell
         */
        $row[] = html_entity_decode($deal['DealUser']['gift_to'], ENT_COMPAT,'utf-8');
        $row[] = $deal['DealUser']['gift_dob'];
        $row[] = $deal['DealUser']['gift_dni'];
        $row[] = date("Y-m-d",strtotime($deal['Deal']['coupon_expiry_date']));
        $row[] = $deal['DealUser']['is_used']? 'SI' : 'NO';
        $row[] = $deal['Redemption']['redeemed'];
        $row[] = $deal['Redemption']['way'];
        $output_row =  join(";", $row) ."\r\n";
        if($print){
            $output .="<tr><td>";
            $output .= join ("</td><td>",  explode(";",$output_row ));
            $output .="</td></tr>";
        } else {
            $output .= $output_row ;
        }
    }
}

echo $output;
