<div class="grid_16 paso3addofertas adds-suc">
	<div class="contenido-form clearfix" style="position:relative;">
		<div id="form-overlay" style="display:none; position: absolute; width: 100%; height: 100%; background: none repeat scroll 0% 0% black; opacity: 0.21; left: 0px; top: 0px; border-radius: 5px 5px 5px 5px;"></div>
		<div class="encabezado">
			<p class="subt">Cargá tu Oferta</p>
		</div>
	<?php echo $form->create ('NowDeal', array ('action' => 'add', 'class' => 'normal', 'enctype' => 'multipart/form-data')); ?>
		<div class="form-fmt clearfix">		
			<div class="colizq clearfix">
				<p class="titulo">General</p>
				<div class="form-fmt clearfix">
					<div class="grupoinp w1 clearfix">
						<label>Título de la Oferta<span>*</span></label>
						<?php  echo $form->input ('name', array ('label' => false, 'div' => null)); ?>								
						<label>Imagen del principal<span>*</span></label>
						<?php  echo $form->input ('Attachment.filename', array ('type' => 'file', 'label' => false, 'div'=> null)); ?>
						<label>Categoría<span>*</span></label>
						<?php echo $form->input ('deal_category_id', array ('label' => false,'div'=> null)); ?>
						<label>Descripción<span>*</span></label>
						<?php echo $form->input ('description', array ('type'=>'text','label'=>false,'div'=>null,'cols'=>50,'rows'=>5)); ?>
					</div>										
					<p class="titulo titulo-precio">Precio</p>
					<div class="contenedor-precio clearfix">
							<div class="grupoinp precio item1 clearfix">			
								<label>Precio Original</label>
								<?php echo $form->input ('original_price', array ('label' => false,'div'=>null));?>
							</div>
							
							<div class="grupoinp precio item2 clearfix">
								<label>Precio con descuento</label>
								<?php echo $form->input ('discounted_price', array ('label' => false,'div'=>null));?>
							</div>

							<div class="grupoinp precio item3 clearfix">
								<label>% descuento</label>
								<?php echo $form->input ('discount_percentage', array ('label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>		

							<div class="grupoinp precio item4 clearfix">
								<label>Imp. a desc.</label>
								<?php echo $form->input ('discount_amount', array ('label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>	

							<div class="grupoinp precio item5 clearfix">
								<label>Ahorro</label>
								<?php echo $form->input ('savings', array ('type' => 'text', 'label' => false,'div'=> null, 'readonly' => 'readonly'));?>
							</div>

							<div class="grupoinp precio item5 clearfix">
								<label>Comisi&oacute;n</label>
								<?php echo $form->input ('commission_percentage', array ('type' => 'text', 'label' => false,'div'=> null,'value'=>Configure::read ('NowPlugin.deal.commission_percentage'), 'readonly' => 'readonly'));?>
							</div>
					</div>			
	<!--Segundo Bloque-->							
				<p class="titulo">Cupones</p>
						
						<div class="grupoinp cupones clearfix">
								<label>Número máximo de cupones por oferta<span>*</span></label>
								<?php echo $form->input ('max_limit', array ('label' => false,'div'=>null)); ?>
								<?php echo $form->hidden ('min_limit', array ('value'=>Configure::read ('NowPlugin.deal.min_limit'))); ?>
						</div>
						
						<div class="grupoinp cupones clearfix">
								<label>Cantidad máxima de cupones por persona<span>*</span></label>
								<?php echo $form->input ('buy_max_quantity_per_user', array ('label' =>false,'div'=>null)); ?>
								<?php echo $form->hidden ('buy_min_quantity_per_user', array ('value'=>Configure::read ('NowPlugin.deal.buy_min_quantity_per_user'))); ?>
						</div>
	<!--fila 2-->								

				<p class="titulo">Programación de la oferta</p>
						<div class="grupoinp validez">
								<label>Fecha de publicación<span>*</span></label>
								<?php echo $form->input ('start_date', array ('label'=>false,'div'=>null,'readonly'=>'readonly','minYear' => date ('Y'), 'maxYear' => date ('Y') + 10, 'type' => 'text')); ?>
								<span class="sprite"></span>
								<p>Formato: (AAAA-MM-DD)</p><br>
								<p class="info f11">Tu oferta puede ser comprada durante este día <br>(desde las 00.00 hs hasta la hora de fin de canje).</p>

						</div>
						<fieldset class="contedor-hora">
						<legend>Franja horaria de canje del cup&oacute;n"</legend>
							<div class="grupoinp horainicio">
								<label>Hora de inicio<span>*</span></label>
								<?php echo $form->input('time_range_start', array(
										'label' => false,
										'options' => array_slice(NowTimeSpanOptions::horas(),0,-1,true),
										'type' => 'select',
										'class' =>"sel sel-rempresas",
										'div' => null,
										), false
									);?>
							</div>
							<div class="grupoinp horafin">
								<label>Hora de fin<span>*</span></label>
								<?php echo $form->input('time_range_end', array(
										'label' => false,
										'options' => array_slice(NowTimeSpanOptions::horas(),1,null,true),
										'type' => 'select',
										'class' =>"sel sel-rempresas",
										'div' => null,
										'disabled' => 'disabled',
										), false
									);?>
							</div>
						</fieldset>
						<p class="info2 f11">Esta es la franja horaria en la que el comprador podrá canjear el cupón.</p>									
						
				<!-- p class="titulo repetir">Repetir esta oferta</p>	
						<div class="repetir-oferta">
							<label>Selecciona cuando deseas repetir esta oferta<span>*</span></label>
							<select>
								<option>1</option>
								<option>2</option>
								<option>3</option>
							</select>
							<p class="info f11">* no estamos teniendo en cuenta los dias feriados existentes</p>
						</div>
				-->	
				<p class="titulo">Sucursales</p>
			<?php if(!count($branches)){ ?>
				<div class="grouplabel">
					<div class='error-message'>No hay Sucursales</div>
				</div>
			<?php }else{?>				
				<?php if($form->isFieldError('branches')){ echo $form->error('branches');}?>
				<?php  echo $form->input ('company_id', array ('type' => 'hidden')); ?>
				<?php if(count($branches) == 1){ ?>
					<div class="grouplabel">
						<input id="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>" type="checkbox" value="<?php echo $branches[0]['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
						<label for="NowBranch-<?php echo $branches[0]['NowBranch']['id'];?>"><?php echo $branches[0]['NowBranch']['name'];?></label>
					</div>					
				<?php }else{?>
					<?php foreach($branches as $sucursal){?>
					<div class="grouplabel">
						<input id="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>" type="checkbox" value="<?php echo $sucursal['NowBranch']['id'];?>"  name="data[NowDeal][branches][]" checked="checked" />
						<label for="NowBranch-<?php echo $sucursal['NowBranch']['id'];?>"><?php echo $sucursal['NowBranch']['name'];?></label>
					</div>
					<?php }?>
				<?php }?>
			<?php }?>
					<?php echo $form->hidden ('save_as_draft',array('value'=>'0'));?>

							<div class="btn-opciones">
								<div class="guardarborrador fll">
									<a href="#" class="sub-link" rel="1">Guardar en borrador</a>
									<a href="#" class="deal-preview" rel="2">Previsualizar</a>
								</div>
								
								<div class="flr">
								<input type="submit" class="btn-azul-d addsbottom" value="Solicitar Aprobación">
								<?php echo $html->link('Cancelar',array('action'=>'my_deals'));?>
								</div>
							</div>
				</div>		
				
				</div>

		</div>
		<?php echo $form->end (); ?>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(e){
		$("input[readonly='readonly']").css({'background':'#EEE','color':'#BBB'});
		$("span.sprite").click(function(e){
			$('#NowDealStartDate').focus();
		})
		//$('#NowDealAddForm').unbind('submit');
		$('#NowDealAddForm').submit(function(e){
			if(!$('#NowDealStartDate').attr('value').length){
				$('#NowDealStartDate').css('backgroundColor','#F00');
				$('#NowDealStartDate').animate({backgroundColor:  "#FFF"}, "slow");
				e.preventDefault();
			}
			if(!$('#NowDealDiscountPercentage').attr('value').length){
				$('#NowDealOriginalPrice').css('backgroundColor','#F00');
				$('#NowDealOriginalPrice').animate({backgroundColor:  "#FFF"}, "slow");
				$('#NowDealDiscountedPrice').css('backgroundColor','#F00');
				$('#NowDealDiscountedPrice').animate({backgroundColor:  "#FFF"}, "slow");
				e.preventDefault();
			}
			if(!$('input[name="data[NowDeal][branches][]"]:checked').length){
				e.preventDefault();
			}
			if($('#NowDealTimeRangeEnd').attr('disabled')){
				$('#NowDealTimeRangeStart').css('backgroundColor','#F00');
				$('#NowDealTimeRangeStart').animate({backgroundColor:  "#FFF"}, "slow");
				$('#NowDealTimeRangeEnd').css('backgroundColor','#F00');
				$('#NowDealTimeRangeEnd').animate({backgroundColor:  "rgb(236, 233, 216)"}, "slow");
				e.preventDefault();
			}
		});

		/* Si deselecciona todas las sucursales le doy una alerta y cancelo la ultima "des-seleccion" */
		//$('input[name="data[NowDeal][branches][]"]').unbind('click');
		$('input[name="data[NowDeal][branches][]"]').bind('click',function(e){
			if($('input[name="data[NowDeal][branches][]"]:checked').length == 0){
				e.preventDefault();
				alert("Debe elegir al menos una sucursal!");
			}
		});
		
		$('a.sub-link').click(function(e){
			e.preventDefault();
			$('input#NowDealSaveAsDraft').attr('value',$(this).attr('rel'));
			$('form#NowDealAddForm').submit();
		});
		
		jQuery.fn.reverse = [].reverse;
		//$('#NowDealTimeRangeStart').unbind('change');
		$('#NowDealTimeRangeStart').change(function(e){
			$('option','#NowDealTimeRangeEnd').reverse().each(function(i,o){
				if(parseInt($(o).val()) <= parseInt($(e.target).val())){
					$(o).attr('disabled','disabled');
				}else{
					$(o).removeAttr('disabled');
				}
			});
			
			if(parseInt($('#NowDealTimeRangeStart').val()) >= parseInt($('#NowDealTimeRangeEnd').val())){
				$('#NowDealTimeRangeEnd').val(parseInt($('#NowDealTimeRangeStart').val())+1);
			}
			$('#NowDealTimeRangeEnd').css('backgroundColor','#FFF');
			$('#NowDealTimeRangeEnd').removeAttr('disabled'); 
		});

		//$('a.deal-preview').unbind('click');
		$('a.deal-preview').click(function(e){
			e.preventDefault();
			$('#form-overlay').fadeIn();
			$.post("/now/now_deals/now_deal_preview", $("#NowDealAddForm").serialize(),function(dat){
				$('#form-overlay').fadeOut();
				$.colorbox({
					html:dat,
					height:580,
					width:815,
					speed: 500,
					onLoad:function(){
						$('#cboxClose').remove();
					}
				});
			});
		});
		
		/*
		$('a.deal-preview').unbind('click');
		$('a.deal-preview').click(function(e){
			e.preventDefault();
			$('#form-overlay').fadeIn();
			$('#fofra').remove();
			var ifr = $('<iframe></iframe>').attr({'id':'fofra','name':'fofra','style':'display:none','src':'about:blank'}).load(function(e){
				$('input#NowDealSaveAsDraft').attr('value','0');
				$('#form-overlay').fadeOut();
				//$('#fofra').fadeIn();
				$('div.error-message').remove();
				$('#fofra').contents().find('div.error-message').each(function(i,o){
				  var it = $(o).prev().attr('id');
				  $('#'+it).after($(o));
				});
				var did = false;
				try{
				  did = jQuery.parseJSON($('#fofra').contents().text());
				}catch(e){
				  alert("Error al listar la oferta!");
				}
				if(did) $.colorbox({href:"/now/now_deals/detalles/"+did.id,height:580,width:815,onLoad:function(){$('#cboxClose').remove();}});
			});
			$('input#NowDealSaveAsDraft').attr('value','2');
			$('#NowDealAddForm').attr('action','/now/now_deals/otro/').attr('target','fofra').after(ifr).submit();
		});
		*/
	});
</script>