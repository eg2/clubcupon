<div class="grid_16 paso3addofertas adds-suc">
	<div class="contenido-form clearfix">
		<div style="position: relative; height: 550px; padding-top: 10px;">
			<?php echo $this->render(null,false,'detalles');?>
		</div>
		<div>
		<ul>
			<li><?php echo __l('No. of max. buyers')." <b>".$deal[0]['NowDeal']['max_limit']."</b>";?></li>
			<li><?php echo __l("Cantidad de unidades disponibles")." <b>".$deal[0]['NowDeal']['buy_max_quantity_per_user']."</b>";?></li>
			<li>Publicar desde <b><?php echo $deal[0]['NowDeal']['start_date']?></b></li>
			<li>Publicar hasta <b><?php echo $deal[0]['NowDeal']['end_date']?></b></li>
			<li>Validez del cup&oacute;n <b><?php echo $deal[0]['NowDeal']['coupon_expiry_date']?></b></li>
			<li>Sucursales:
				<ul>
					<?php foreach($deal[0]['NowBranch'] as $branch){ ?>
					<li><b><?php echo $branch['name'];?></b></li>
					<?php }?>
				</ul>
			</li>
		</ul>
		</div>
		<div class="reg-emp" style="padding: 10px 130px;">
		<?php if($deal[0]['NowDeal']['deal_status_id']!= 11){ ?>
			<p>La Oferta ya fue enviada a moderacion no se puede editar!</p>
		<?php }else{ ?>

		<?php echo $form->create('NowDeal', array ('url' => array('action'=>'update_deal',$deal[0]['NowDeal']['id']),'id'=>'NowDealAddForm', 'class' => 'normal'));?>
			<?php echo $form->hidden('id', array ('value'=>$deal[0]['NowDeal']['id'])); ?>
			<input type="button" style="width:200px; display:inline;" id="EditButton" class="btn-naranja btn-reg-enviado" value="Editar" onclick="window.location='<?php echo Router::url(array('action'=>'edit',$deal[0]['NowDeal']['id']));?>'">
			<input type="submit" style="width:200px; display:inline;" id="CancelButton" class="btn-naranja btn-reg-enviado" name="action" value="Cancelar">
			<input type="submit" style="width:200px; display:inline;" id="SubmitButton" class="btn-naranja btn-reg-enviado" name="action" value="Enviar a Moderacion">
		<?php echo $form->end();?>

		<?php } ?>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('.lb-cerrar').unbind('click');
	$('.lb-cerrar').click(function(e){
		e.preventDefault();
		window.location = '<?php echo Router::url(array('plugin'=>'now','controller'=>'NowDeals','action'=>'my_deals'));?>';
	});
});
</script>