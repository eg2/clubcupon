<?php if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){ ?>
	<div class="lightbox" style="background: none repeat scroll 0% 0% white;">
	<div class="lb-header clearfix">
		<a class="lb-cerrar" href="/now"></a>
	</div>
<?php }else{ ?>
<script>
$(document).ready(function() {
	
	$('.lb-cerrar').unbind('click');
	$('.lb-cerrar').click(function(e){
		e.preventDefault();
		$("#dealsoverlay").fadeIn();
		$.post('/now',function(data){

		$("#dealsoverlay").fadeTo(400,1,function(e){
					$("#deals_container").html(data);
					$("#dealsoverlay").fadeTo(400,0.7,function(){
						$("#dealsoverlay").fadeOut();
					});
				});
		})


	});
});

</script>
	<div class="grid_1">&nbsp;</div>
	<div class="lightbox grid_14" style="">
		<div class="lb-header clearfix">
			<a class="lb-cerrar" href="#"></a>
		</div>
<?php } ?>
	<div class="lb-centro">
		<div class="lb-col1 clearfix">

			<p class="encabezado">$<?php echo number_format($deal[0]['NowDeal']['discounted_price'],0,'','');?> en vez de $<?php echo number_format($deal[0]['NowDeal']['original_price'],0,'','');?><br /> en <?php echo $clubcupon->shorten_strings_with_html_entities($deal[0]['Company']['name'],22);?></p>
			<p class="sub"><?php echo $clubcupon->shorten_strings_with_html_entities($deal[0]['NowDeal']['name'], 35); ?></p>
			<p class="lb-utiliza">Para utilizar Hoy de <?php echo date("H",strtotime($deal[0]['NowBranch'][0]['BranchesDeal']['ts_inicio']));?>hs a <?php echo $clubcupon->show_24_hs(date("H",strtotime($deal[0]['NowBranch'][0]['BranchesDeal']['ts_fin'])));?>hs!</p>

			<img alt="" src="/img/now/bg-compras-rib.png" class="bg-img">
			<div class="boton-comprar-bg">
				<p><span>$</span><?php echo number_format($deal[0]['NowDeal']['discounted_price'],0,'','');?></p>

					<?php
						if (@$auth->user('user_type_id') != ConstUserTypes::Company){
							//if($deal[0]['NowDeal']['deal_user_count'] < $deal[0]['NowDeal']['max_limit']){
							if($deal[0]['NowDeal']['is_purchasable']>0){
								echo '<div class="comprar">';
								echo $html->link('Comprar!',array('plugin'=>'','controller'=>'deals','action'=>'buy','id'=>$deal[0]['NowDeal']['id']));
								echo '</div>';
							}else{
								echo '<div class="comprar-out"><p>Comprar!</p></div>';
							}
						}
                        else{
                            echo '<div class="comprar-out"><p>Oferta Finalizada</p></div>';
                        }
					?>

			</div>
			<div class="modulos-compra">
				<ul class="precio clearfix">
					<li>Descuento <br>%<?php echo $deal[0]['NowDeal']['discount_percentage'];?></li>
					<li>Valor Real $<?php echo $deal[0]['NowDeal']['original_price'];?></li>

				</ul>
			</div>
			<p class="lb-descripcion"><?php echo $clubcupon->shorten_strings_with_html_entities($deal[0]['NowDeal']['description'], 150)?></p>
			<p class="lb-valido"><?php //echo $deal[0]['NowDeal']['coupon_condition'];?></p>
           <!--  <a id="abrir_tyc" class="ver-lasreglas" href="#" >Ver las reglas que se aplican a todas las ofertas</a>-->
		</div>

		<div class="lb-col2 clearfix">
			<?php if(isset($deal[0]['Attachment'])){?>
			<?php $image_url = Deal::getImageUrl('Deal',$deal[0]['Attachment'],array('dimension' => 'small_big_thumb')); ?>
			<?php }else{?>
			<?php $image_url = "/img/now/img-holder.png";?>
			<?php }?>
			<div style="height: 162px; width: 365px; background-color:#FFF; border: 1px solid black; text-align:center;">
                <?php echo $html->image($image_url, array("alt" =>$deal[0]['NowDeal']['name'], 'height'=>162));?>
            </div>
			<div class="lb-map clearfix">

                <div style="position:relative; float:right;">
				<a class="lb-lugar" href="<?php echo $deal[0]['Company']['url'];?>"><?php echo $clubcupon->shorten_strings_with_html_entities($deal[0]['Company']['name'],20);?></a><br /><br />
				<a href="<?php echo $deal[0]['Company']['url'];?>"><?php echo $clubcupon->shorten_strings_with_html_entities($deal[0]['Company']['url'], 25); ?></a><br /><br />
                    <div id="bloquedatosempresa" style="float:left; color:#6B6B6B;font: 12px arial;">
                        <?php echo $deal[0]['NowBranch'][0]['street']." ".$deal[0]['NowBranch'][0]['number'];?>
						<?php
							echo $deal[0]['NowBranch'][0]['floor']=="Piso"?"":" ".$deal[0]['NowBranch'][0]['floor'];
							echo $deal[0]['NowBranch'][0]['apartament']=="Dpto"?"":" ".$deal[0]['NowBranch'][0]['apartament'];
						?><br />
						<?php if(!empty($deal[0]['NowBranch'][0]['telephone'])){echo $deal[0]['NowBranch'][0]['telephone'].'<br />';}?>
						<?php echo $deal[0]['NowBranch'][0]['City']['name'];?>
                    </div>
				</div>

				<div id="gmap-cont" style="position:relative;  width: 189px;">
					<div style="position: absolute; width: 181px; padding: 0px 5px 0px 5px;">
						<a class="gmap-zoom" href="#" rel="+1" style="float: left;">zoom in</a>
						<a class="gmap-zoom" href="#" rel="-1" style="float: right;">zoom out</a>
					</div>
					<?php if(isset($deal[0]['NowBranch']['0']['latitude'])){?>
					<img id="gmap" width="189" height="172" alt="" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo @$deal[0]['NowBranch']['0']['latitude'];?>,<?php echo @$deal[0]['NowBranch']['0']['longitude'];?>&zoom=15&size=189x172&markers=icon:http://i44.tinypic.com/2ij1729.png%26chld=cafe%257C996600|<?php echo @$deal[0]['NowBranch']['0']['latitude'];?>,<?php echo @$deal[0]['NowBranch']['0']['longitude'];?>&sensor=false">
					<?php }else{?>
					<img id="gmap" width="189" height="172" alt="" src="http://maps.googleapis.com/maps/api/staticmap?center=-34.6037232,-58.381593&zoom=15&size=189x172&markers=icon:http://i44.tinypic.com/2ij1729.png%26chld=cafe%257C996600|-34.6037232,-58.381593&sensor=false">
					<?php }?>
				</div>

			</div>
		</div>
	</div>

	<div class="lb-social clearfix">
		<ul class="clearfix">
			<li>Compartilo con un amigo </li>
            <li><a class="fb" href="http://www.facebook.com/sharer.php?u=<?php echo FULL_BASE_URL.'/ya/now_deals/detalles/'.$deal[0]['NowDeal']['id'];?>&t=Club Cupon YA" target="blank" title="Compartir en Facebook">Compartir en Facebook</a></li>
			<li><a class="tw" href="http://twitter.com/share?url=<?php echo rawurlencode(Configure::read ('static_domain_for_mails').'/ya/now_deals/detalles/'.$deal[0]['NowDeal']['id']); ?>&text=<?php echo $deal[0]['NowDeal']['name'] .' '. rawurlencode($deal[0]['NowDeal']['name']); ?>&via=<?php echo $city_twitter_username; ?>&lang=es" title="<?php echo 'Twitea esta oferta'; ?>">Twitter</a></li>
            <li><a class="email" href="mailto:?subject= <?php echo $mailto_subject; ?>&body=<?php echo $mailto_body; ?>"></a></li>
		</ul>
	</div>

</div>

<!--inicio POPUP TYC -->
<div id="tyc_block" style="position:absolute; top:0; left:0; z-index:999; display:none; background: #fff; width:774px;">
    <div id="cerrar_tyc">Volver al detalle de oferta</div>
    <?php echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'terminos_condiciones', array('plugin'=>'now')); ?>
</div>
<!--fin POPUP TYC -->