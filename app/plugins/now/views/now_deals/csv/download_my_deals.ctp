<?php if(count($deals)){ ?>
Id;Fecha de publicacion;Oferta;Precio original;Precio con descuento;Porcentaje de Comision;Vigencia (desde - hasta);Estado; Cupones vendidos; Cupones Redimidos; Cupones devueltos; cupones liquidados
<?php foreach($deals as $key=>$deal){

$row = array();
$row[] = $deal['NowDealView']['deal_id'];
$row[] = $deal['NowDealView']['deal_start_date'];
$row[] = iconv("UTF-8","ISO-8859-1",html_entity_decode($deal['NowDealView']['deal_name'], ENT_COMPAT,'utf-8'));
$row[] = empty($deal['NowDealView']['deal_original_price']) ? " " : $deal['NowDealView']['deal_original_price'];
$row[] = empty($deal['NowDealView']['deal_discounted_price']) ? " " : $deal['NowDealView']['deal_discounted_price'];
$row[] =  $deal['NowDealView']['deal_commission_percentage'];
$vigencia = $deal['NowDealView']['deal_start_date'] ." al ".$deal['NowDealView']['deal_end_date'];
$row[] =$vigencia ;
$row[] = iconv("UTF-8","ISO-8859-1",html_entity_decode(ConstDealStatus::getFriendly($deal['NowDealView']['deal_status_id']), ENT_COMPAT,'utf-8'));
$row[] = empty($deal['NowDealView']['coupon_count']) ? "0" : $deal['NowDealView']['coupon_count'];
$row[] = empty($deal['NowDealView']['coupon_redemed_count']) ? '0' : $deal['NowDealView']['coupon_redemed_count'];
$row[] = empty($deal['NowDealView']['coupon_returned_count']) ? '0' : $deal['NowDealView']['coupon_returned_count'];
$row[] = empty($deal['NowDealView']['coupon_billed']) ? '0' : $deal['NowDealView']['coupon_billed'];

echo join(";", $row)."\r\n";
    }

}