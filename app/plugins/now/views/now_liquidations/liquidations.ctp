<?php 
 $tab_selected=0; 
 if (isset($filter)):
    if ($filter=='all'):
       	$tab_selected=0;
	endif; 
	if ($filter=='redeemed'):
	    $tab_selected=1;
	endif; 
	if ($filter=='sold'):
	    $tab_selected=2;
	endif; 
 endif; 
?>

<script>
    var tab_selected=<?php echo $tab_selected;?>;
    $(document).ready(function () {
        $("#tabs").tabs({
            selected: tab_selected,
        	select: function (event, ui) {
                if (ui.tab.text == "Todos") {
                    
                    $("#content0").show();
                    $("#content1").hide();
                    $("#content2").hide();
                    $("#content3").hide();
                }

                if (ui.tab.text == "Por redimido") {
                    $("#content0").hide();
                    $("#content1").show();
                    $("#content2").hide();
                    $("#content3").hide();
                }

                if (ui.tab.text == "Por vendido") {
                	
                    $("#content0").hide();
                    $("#content1").hide();
                    $("#content2").show();
                    $("#content3").hide();
                }

                if (ui.tab.text == "Exportar") {
                    $("#content0").hide();
                    $("#content1").hide();
                    $("#content2").hide();
                    $("#content3").show();
                }
               
            }
        });
    });
</script>

<div class="contenido-form" style="padding:0px; float: left;width: 100%;">

    <div id="tabs">
        <div id="content0">
            <table style="display:block;font: 12px Arial;">
                <tr>
                    <td colspan="4">
                        <p>
                            Listado de Liquidaciones de Ofertas por Vendido y Redimido agrupadas por Calendario.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="content1" style="display: none">
            <table style="display:block;font: 12px Arial;">
                <tr>
                    <td colspan="4">
                        <p>
                            Liquidaciones de Ofertas por Redimidos agrupadas por Calendario.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="content2" style="display: none">
            <table style="display:block;font: 12px Arial;">
                <tr>
                    <td colspan="4">
                        <p>
                            Liquidaciones de Ofertas por Vendidos agrupadas por Calendario.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="content3" style="display: none">
            <table style="display:block;font: 12px Arial;">
                <tr>
                    <td colspan="4">
                        <p>
                            Exporte las Liquidaciones comprendidas entre un rango de fechas.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        
        <ul>
            <li id="li_all">
                <?php
                if (isset($deal_id)) {
                    echo $html->link('Todos', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'all', 'deal_id' => $deal_id), array('escape' => false, 'title' => 'Todos'));
                } else {
                    echo $html->link('Todos', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'all',), array('escape' => false, 'title' => 'Todos'));
                }
                ?>
            </li>
            <li>
                <?php
                if (isset($deal_id)) {
                    echo $html->link('Por redimido', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'redeemed', 'deal_id' => $deal_id), array('escape' => false, 'title' => 'Por redimido'));
                } else {
                    echo $html->link('Por redimido', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'redeemed',), array('escape' => false, 'title' => 'Por redimido'));
                }
                ?>
            </li>
            <li>
                <?php
                if (isset($deal_id)) {
                    echo $html->link('Por vendido', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'sold', 'deal_id' => $deal_id), array('escape' => false, 'title' => 'Por vendido'));
                } else {
                    echo $html->link('Por vendido', array('plugin' => 'now', 'controller' => 'now_liquidations', 'action' => 'index', 'filter' => 'sold',), array('escape' => false, 'title' => 'Por vendido'));
                }
                ?>
            </li>   
            <li>
                <?php
                echo $html->link('Exportar', array('plugin' => 'now', 'controller' => 'now_exports', 'action' => 'index'), array('escape' => false, 'title' => 'Exportar'));
                ?>
            </li>                  
        </ul>      
        <div id="ui-tabs-1" style="display:none">

        </div>
        <div id="ui-tabs-2">

        </div>
        <div id="ui-tabs-3">

        </div>
        <div id="ui-tabs-4">

        </div>

    </div>
</div>  