<?php 
//var_dump($this->params['named']);
?>
<style>
#pagingLinks .disabled{
padding:0px !important;
}
#pagingLinks li{
float:left !important;
}
.liquidations-header th{
	font:12px arial;
	color:#777777;
	border-bottom:none;
}
.th-deal-name{
	font:12px arial;
	color:#777777;
	border-bottom:none;
}

.row-even{
background: none repeat scroll 0 0 #f5f5f5;
}
.row-odd{
background: none repeat scroll 0 0 #e8e6e7;

}
.row-even:hover{
background: none repeat scroll 0 0 #d1e4eb;
}
.linkDetail .linkExport{
color: #0098ca;
font:12px arial;
}
.linkBackLiquidations{
color: #0098ca;
font:12px arial;
}

</style>



<div id="contentGeneric-<?php echo $this->params['named']['filter']?>">

<?php if(!isset($this->params['named']['calendar_id'])){?>
<table id="contentLiquidations" style="display:<?php echo !isset($this->params['named']['calendar_id'])?"block":"none" ?>; font: 12px Arial;">
<thead>
<?php if(!empty($deals)){?>
<tr>
  
  <?php foreach($deals as $deal){?>
  <th colspan="4" style="border-bottom-color:#cccccc;color: #000000 !important;">
     <span class="th-deal-name">Oferta:</span>
     <span><?php echo $deal['NowDeal']['name'];?></span>
  </th>
  <?php }?>
</tr>

<?php }?>
<tr class="liquidations-header">
  <th> Id Calendario</th>
  <th> Periodo </th>
  <th> Total Liquidacion ($) </th>
  <?php if(in_array($this->params['named']['filter'], array('redeemed','sold'))) : ?>  
    <th> Fecha Tentativa de Pago </th>
    <th> Estado del Pago </th>
  <?php endif; ?> 
  <th> Acciones</th>  
</tr>
</thead>


<tbody id="content">
<?php 
$conta=0;
	foreach($accountingCalendars as $accountingCalendar){
$conta++;
?>
<tr class="row-<?php if($conta%2){echo "even";}else{echo "odd";}?>">
    <td>
        <?php echo $accountingCalendar['AccountingCalendar']['id'];?>
    </td>
    <td> 
        <?php echo 'desde el '. @$time->format("d-m-Y",$accountingCalendar[0]['since']). ' hasta el '.@$time->format("d-m-Y",$accountingCalendar[0]['until']);?>
    </td>
    <td> 
        <?php echo $accountingCalendar[0]['total_amount'];?>
    </td>
    <?php if(in_array($this->params['named']['filter'], array('redeemed','sold'))) : ?>
        <td> 
            <?php echo @$time->format("d-m-Y",$accountingCalendar[0]['payment_date']);?>
        </td>
        <td> 
            <?php echo $accountingCalendar[0]['payment_status'];?>
        </td>
    <?php endif; ?>
    <td>
       	 <?php 
       	 	if(isset($this->params['named']['deal_id'])):
       	 	  if(isset($this->params['named']['filter'])):
	       	  	echo $html->link('Ver detalle', array('plugin'=>'now','controller'=>'now_liquidations','action'=>'index','calendar_id'=>$accountingCalendar['AccountingCalendar']['id'],'deal_id'=>$this->params['named']['deal_id'],'filter'=>$this->params['named']['filter']),array('class'=>'linkDetail', 'style'=>'text-align:left'));
       	 	  else:
    		  	echo $html->link('Ver detalle', array('plugin'=>'now','controller'=>'now_liquidations','action'=>'index','calendar_id'=>$accountingCalendar['AccountingCalendar']['id'],'deal_id'=>$this->params['named']['deal_id']),array('class'=>'linkDetail', 'style'=>'text-align:left'));
       	 	  endif;
       	 	else:
    		  if(isset($this->params['named']['filter'])):
	       	 	echo $html->link('Ver detalle', array('plugin'=>'now','controller'=>'now_liquidations','action'=>'index','calendar_id'=>$accountingCalendar['AccountingCalendar']['id'],'filter'=>$this->params['named']['filter']),array('class'=>'linkDetail', 'style'=>'text-align:left'));
       	 	  else:
       	 		echo $html->link('Ver detalle', array('plugin'=>'now','controller'=>'now_liquidations','action'=>'index','calendar_id'=>$accountingCalendar['AccountingCalendar']['id']),array('class'=>'linkDetail', 'style'=>'text-align:left'));
       	 	  endif;
       	 	endif; 
       	    if($this->params['named']['filter']=='all'){
       	       echo '<br>';
       	       echo $html->link('Exportar', array('plugin'=>'now','controller'=>'now_exports','action'=>'liquidate',$accountingCalendar['AccountingCalendar']['id']),array('class'=>'linkExport', 'style'=>'text-align:left')); 
       	    }
       	 ?>
    </td>
</tr>
<?php } ?>
<?php if(!empty($accountingCalendars)) { ?>
</tbody>
</table>
<table  id="footerLiquidations" style="display:<?php echo !isset($this->params['named']['calendar_id'])?"block":"none" ?>;font: 12px Arial;">
<tr>
<td colspan="4">
  <div class="dealspaginador clearfix">
      <div class="pagborder">
            <?php echo str_replace('/all', '', $paginator->prev('Anterior', array('class' => 'prev','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'prev'))); ?>
            <?php echo str_replace('/all', '', $paginator->numbers(array('modulus' => 2,'skip' => '<span class="skip">&hellip;.</span>','separator' => " \n",'before' => null,'after' => null,'escape' => false))); ?>
            <?php echo str_replace('/all', '', $paginator->next('Siguiente', array('class' => 'next','escape' => false) , null, array('tag' => 'span','escape' => false,'class' => 'next'))); ?>
      </div>
    </div>
</td>
</tr>
<tr>
<td colspan="4">
<?php echo $paginator->counter(array('format' => '<p><span>%start%-%end%</span> de %count% Calendarios</p>')); ?>
</td>
</tr>
</table>
<?php } else {?>
<tr>
  <td colspan="4">
    No se encontraron liquidaciones
  </td>
</tr>
</tbody>
</table>
<?php } ?>
<?php }//fin de Todas las liquidaciones
?>
<?php 



if(isset($this->params['named']['calendar_id'])){?>

<?php
 
?>
<table id="contentDetail" style="font: 12px Arial;">
<thead>
<tr class="liquidations-header">
    <th>Id Oferta</th>
    <th>Oferta</th>
    <th>Modalidad De Pago</th>
    <th>Fecha De Publicaci&oacute;n</th>
    <th>Q De Cupones</th>
    <th>Precio Cup&oacute;n</th>
    <th>Total</th>
    <th>Comisi&oacute;n Club Cup&oacute;n</th>
    <th>Retención Fondo De Garant&iacute;a</th>
    <th>Fecha Tentativa de Pago</th>
    <th>Estado del Pago</th>
    <th>Total Liquidaci&oacute;n</th>  
</tr>
</thead>
<tbody id="content">
<?php 
	foreach($accountingItems as $accountingItem){
$conta++;
?>
<tr class="row-<?php if($conta%2){echo "even";}else{echo "odd";}?>">
    <td>
        <?php echo $accountingItem['Deal']['deal_id'];?>
    </td>
    <td> 
        <?php echo $accountingItem['Deal']['deal_name'];?>
    </td>
    <td> 
        <?php echo $accountingItem[0]['deal_modality'];?>
    </td>
    <td>
       	<?php echo @$time->format("d-m-Y",$accountingItem[0]['deal_start_date']);?>
    </td>
    <td> 
                <?php echo $accountingItem['AccountingItem']['total_quantity'] != 0 ? $accountingItem['AccountingItem']['total_quantity'] : 'FG'; ?>
    </td>
    <td> 
        <?php echo $accountingItem['AccountingItem']['discounted_price'];?>
    </td>
    <td>
       	<?php echo $accountingItem['AccountingItem']['total_quantity']*$accountingItem['AccountingItem']['discounted_price'];?>
    </td>
    <td> 
        <?php echo $accountingItem[0]['deal_commission_percentage'];?>
    </td>
    <td>
       	<?php echo $accountingItem[0]['guarantee_fund_amount'];?>
    </td>
    <td>
       	<?php echo @$time->format("d-m-Y",$accountingItem[0]['payment_date']);?>
    </td>
    <td>
       	<?php echo $accountingItem[0]['payment_status'];?>
    </td>
    <td>
       	<?php echo $accountingItem[0]['liquidation_amount'];?>
    </td>
    
</tr>
<?php 
	}
?>
</tbody>
</table>
<table id="footerDetail">
<tr>
<td colspan="10">
  	 <?php
  	 $urlListado= array('plugin'=>'now','controller'=>'now_liquidations','action'=>'index');

  	 if(isset($this->params['named']['deal_id'])){
		$urlListado[]="deal_id:".$this->params['named']['deal_id'];
	 }
 
  	 if(isset($this->params['named']['filter'])){
		$urlListado[]="/filter:".$this->params['named']['filter'];
	 }
	 
  	 echo $html->link('Ir al Listado', $urlListado,array('class'=>'linkBackLiquidations', 'style'=>'text-align:left')); 
  	 ?>
  	 
</td>
</tr>
</table>
<?php }//fin de Detalle liquidacion
?>

</div>

<script>




$(document).ready(function(){
	var handleClick = function(e) {
		
		var url= $(this).attr('href');
		if(!( url === undefined)) 
		{
			
	    	$.get(url, function(data){
				//$('#'+target).html(data);
				//$('#Todos').hide();
	        	$('#contentGeneric-<?php echo $this->params['named']['filter']?>').html(data);
			});
	    	e.preventDefault();
		}else{
			//no hay mas registros para mostrar
		}
	};
	var backLiquidations = function(){
		
	};
	
	$('.prev').on( 'click',handleClick);
	$('.next').on( 'click',handleClick);
	$('.pagLink').on('click',handleClick);
	$('.linkDetail').on('click',handleClick);
	$('.linkBackLiquidations').on('click',handleClick);
});
</script>