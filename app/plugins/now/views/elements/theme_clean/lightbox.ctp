	<!-- Lightbox-->
	<div id="cboxOverlay">
		
	</div>	
	<div class="lightbox clearfix">

		<div class="lb-header clearfix">
			<a class="lb-cerrar" href="#"></a>
		</div>
		
		<div class="lb-centro">
			<div class="lb-col1 clearfix">
				<p class="encabezado">$30 en vez de $60 en Katamandu Restaurant</p>
				<p class="sub">50% de descuento en comida y bebida</p>
				<p class="lb-utiliza">Para utilizar hoy de 11:30 am - 9:00 pm</p>
				<div class="comprar">
					<a href="#">&#161;Comprar Ahora!</a>
				</div>				
				<ul class="precio clearfix">
					<li class="li-first">Precio $249</li>
					<li>Descuento %55</li>
					<li>Valor Real $559</li>
				</ul>
				<p class="lb-descripcion">Rebanadas de cordero asado marinado o pollo se mezclan con la cebolla, el tomate y tzatziki en pan de pira en el casuales afro-mediterraneas deli</p>
				<p class="lb-valido">Valido para 1 por persona. Previa reserva telef&#243;nica.</p>
				<a class="ver-lasreglas" href="#">Ver las reglas que se aplican a todas las ofertas</a>
			</div>
			
			<div class="lb-col2 clearfix">
				<img alt="CLUB CUP&#211;N." src="/img/now/lb-img.png" class="br5">
				<div class="lb-map clearfix">
					<img alt="CLUB CUP&#211;N." src="/img/now/lb-map.png">
					<a class="lb-lugar" href="#">Katmandu Restaurant</a>
					<a href="#">www.katmandurestaurant.com</a>
					<p>Honduras 5300</p>
					<p>Capital Federal, Palermo</p>
					<p>4396 5748</p>
				</div>
			</div>
		</div>
		
		<div class="lb-social clearfix">
			<ul class="clearfix">
				<li>Compartilo con un amigo y recib&#237; $10 de regalo</li>
				<li><a class="fb" href="#">facebook</a></li>
				<li><a class="tw" href="#">Twitter</a></li>
				<li><a class="email" href="#">email</a></li>
			</ul>
		</div>		
	</div>
	<!-- FIN Lightbox-->