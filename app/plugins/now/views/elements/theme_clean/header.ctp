<?php
	if ($auth->sessionValid () && $auth->user ('user_type_id') == ConstUserTypes::Company){
		$company = $html->getCompany ($auth->user ('id'));
    }
?>
<div id="header">
	<div class="container_16 center-ccn clearfix pos-r">
		<div class="grid_2" id="title_logo-mini">
			<a title="CLUB CUPÓN." href="<?php echo FULL_BASE_URL; ?>">
			<?php echo $html->image('web/clubcupon/logo.png');?>
			</a>
		</div>


<?php if (isset($is_corporate_city) && $is_corporate_city && !in_array($corporateCity['City']['slug'], Configure::read('corporate_exceptions.city_name'))){ ?>
						<div class="grid_4 opciones-login <?php echo $push_class; ?>">
    	                    <ul class="ingresa">
                            <style>
                                ul.mi-cuenta{top:104px!important;}
                            </style>
                                <li style="margin-right: 103px;margin-top: -32px; padding-left: 9px;">
                                    <?php 
                                        if($corporateCity['City']['image']) { 
                                            $cityLogoURL = $corporateCity['City']['image'];
                                        } else if(file_exists("img/theme_clean/exclusive/logo-".$corporateCity['City']['slug'].".png")) {
                                            $cityLogoURL = "/img/theme_clean/exclusive/logo-". $corporateCity['City']['slug'].".png";
                                        }
                                    ?>
                                    <img src="<?php echo $cityLogoURL; ?>" title="<?php echo $corporateCity['City']['name']; ?>"/>
                                </li>
	                        </ul>
						</div>
                <?php } ?>
                            
                                
            	<?php if (isset($is_corporate_city) && !$is_corporate_city){ ?>
                	<?php if ($auth->sessionValid ()){ ?>
                		<ul id="login" class="logged opciones_login parent">
	                    	<li class="<?php echo $is_corporate_city  ? 'corporate' : ''; ?>">
	                    		<span class="loginEmail">
		                        	<?php
		                            	echo ($auth->user ('email') ? $auth->user ('email') : $html->link ('Mi Perfil', array ('plugin' => '', 'controller' => 'user_profiles', 'action' => 'edit'), array ('title' => 'Mi perfil', 'rel' => 'nofollow'))) ;
		                            ?>
		                        </span>    
	                        </li>
                      	</ul>
                   	<?php }else{ ?>
                    	<ul id="login">
							<li><?php echo $html->link ('Ingresar', array ('plugin' => '', 'controller' => 'users', 'action' => 'login'), array ('title' => 'Ingresar', 'rel' => 'nofollow', 'class'=> 'item-salir')); ?></li>
							<li>|</li>
    	                	<li><?php echo $html->link('Registrarse', array('plugin' => '','controller' => 'users', 'action' => 'register'), array('title' => __l('Signup'), 'class' => 'item-salir')); ?></li>
			    		</ul>
                    <?php } ?>
				<?php } ?>

			<!-- UL desplegable Cuenta LOGUEADO -->
            <div id="desplegable_login">
				<img src="/img/web/clubcupon/flecha_top.png" class="flecha" alt="flecha">
				<div class="menu_final">
					<?php echo $this->element('dashboard/user_menu_links'); ?>
            	</div>
            </div>
			<!-- FIN UL desplegable Cuenta LOGUEADO -->

        <?php

        $push_div = Configure::read('now.is_menu_enabled') &&  isset($is_corporate_city) && !$is_corporate_city ? 'push_1' : 'push_2 ';

        ?>
	</div>

        <div id="menu_top">
			<ul>
				<li><a href="/ofertas-nacionales">Ofertas Nacionales</a></li>
				<li><a href="/turismo">Turismo</a></li>
				<li><a href="/cel/landing">Móviles</a></li>
				<li><a href="/ciudad-de-buenos-aires/pages/promocinoes">Promociones</a></li>
			</ul>
		</div>


<?php /* 	
	<!-- itemdesplegable -->
	<div class="jscontentmenu">
		<div class="item-desplegable">
			<span>&#9668;</span>
			<a href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de atenci&oacute;n al cliente</a>
		</div>
	</div>
	<!-- itemdesplegable -->
*/?>
</div>
<script>
$(document).ready(function() {

	$('.item-desplegable').mouseover(function() {
	  $(".item-desplegable").animate({
	    right: '0'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	$('.jscontentmenu').mouseleave(function() {
	  $(".item-desplegable").animate({
	    right: '-194'
	  }, 200, function() {
	    // Animation complete.
	  });
	});

	
	//compatibilidad plugin web
    $('.parent').click(function(event){
		event.stopPropagation();
		
		$(this).children('.desplegable').slideDown();
		$(this).children('.flecha').show();
	});

	 $('html').click(function(e){
        e.stopPropagation();
        if($(e.target).parents('.desplegable').length==0) {
        	if($('.desplegable:visible').length>0) $('.desplegable').slideUp(200);
        	$('.parent').removeClass('open');
        	$('.parent .flecha').hide();
        }
        if($(e.target).parents('#desplegable_login').length==0) {
        	if($('#desplegable_login').length>0) $('#desplegable_login').slideUp(200);
        	$('.parent').removeClass('open');
        }

    });

 	$(".opciones_login").click(function(event) {
		event.preventDefault();
		$('.opciones_login').addClass('open');
		
		$('.especiales .desplegable').slideUp();
		$('.especiales .flecha').hide();
		$('#btn_desplegar .desplegable').slideUp();
		$('#btn_desplegar .flecha').hide();
		
		$("#desplegable_login").slideToggle();
		//$('#desplegable_login .flecha').show();
	});
});  //fin doc ready
</script>
