<?php
    if ($auth->sessionValid () && $auth->user ('user_type_id') == ConstUserTypes::Company)
    {
    $company = $html->getCompany ($auth->user ('id'));
    }

    $cities = $clubcupon->activeCitiesWithDeals ();
    $groups = $clubcupon->activeGroupsWithDeals ();

    //ya que los grupos van a continuacion de las ciudades, unificamos en 1 solo arreglo.
    $results = array_merge($cities, $groups );

?>


<div id="compras-header">
	<div class="container_16 center clearfix pos-r">
		<div id="title_logo" class="grid_3">
			     <?php

                        if (Configure::read ('Actual.city_is_group') == 0)
                        {
                            echo $html->link ($html->image ('theme_clean/logo.png', array ('alt' => 'CLUB CUPÓN. Todos los días, una alegría.')), array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => false), array ('escape' => false, 'title' => 'CLUB CUPÓN. Todos los días, una alegría.'));
                        }
                        else
                        {
                            echo $html->link ($html->image ('theme_clean/logo.png', array ('alt' => 'CLUB CUPÓN. Todos los días, una alegría.')), array ('plugin'=>'','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city' => 'ciudad-de-buenos-aires'), array ('escape' => false, 'title' => 'CLUB CUPÓN. Todos los días, una alegría.'));
                        }

                    ?>
		</div>

		<div class="push_9 grid_4 navigation">
			<a href="/now/now_deals" class="text">Volver a la home</a>
		</div>

	</div>
</div>



