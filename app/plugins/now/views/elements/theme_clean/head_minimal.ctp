<?php
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $browser = $_SERVER['HTTP_USER_AGENT'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml"<?php if (isset ($og_data)) echo 'xmlns:og = "http://ogp.me/ns#"  xmlns:fb = "http://www.facebook.com/2008/fbml"'; ?>>
    <head>
        <?php echo $html->charset(), "\n"; ?>
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <title>
        <?php

            echo Configure::read('site.name') . ' | '.$title_for_layout;
        ?>
        </title>
        <?php
            echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

            if (isset ($og_data)) {
                foreach ($og_data as $key => $value) {
                    if (is_array ($value)) {
                        foreach ($value as $val) {
                            echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                        }
                    } else {
                    echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                    }
                }
            }

            //CSSs
            echo $html->css("/now/css/".Configure::read('theme.theme_name') . 'reset');      //unifica los estados de los navegadores

            echo $html->css("/now/css/".Configure::read('theme.theme_name') . '960_16_col'); //fw de css
            echo $html->css("/now/css/".Configure::read('theme.theme_name') . 'global');     //estilos funcionales
            echo $html->css(Configure::read('theme.theme_name') . 'colorbox');   //popup


            //JSs
            //Jquery


         /*   echo $html->css('/now/css/now');
            echo $html->css('/now/css/styles');
            echo   $html->css('/now/css/admin');
      echo $html->css('buy');
      echo $html->css('messages');
      echo $html->css('popups');
      echo $html->css('plugins/sifr');*/
      echo $html->css('plugins/jquery.autocomplete');
      echo $html->css('plugins/jquery-ui-1.7.1.custom');
      echo $html->css('plugins/ui.timepickr');
      echo $html->css('plugins/colorbox');
      /*echo $html->css('styles_paula');*/
      echo $html->css('jcarousel-skin-clubcupon');

echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true));
echo $javascript->codeBlock('var NowPlugin_calendar_minium_date = ' .Configure::read('NowPlugin.calendar_minium_date'));
/*
    echo    $javascript->link('libs/underscore.min');*/
    echo  $javascript->link('libs/jquery');
    /*
    echo    $javascript->link('facebook');
    echo    $javascript->link('libs/jquery.form');
    echo    $javascript->link('libs/jquery.blockUI');*/
    echo    $javascript->link('libs/jquery.livequery');
    /*
    echo    $javascript->link('libs/jquery.metadata');
    echo    $javascript->link('libs/jquery.autocomplete');*/
    echo    $javascript->link('libs/jquery-ui-1.7.2.custom.min');
    echo    $javascript->link('libs/jquery.countdown');
    echo    $javascript->link('libs/jquery.timepickr');
    /*echo    $javascript->link('libs/jquery.overlabel');*/
    echo    $javascript->link('libs/jquery.colorbox');
    /*echo    $javascript->link('libs/date.format');*/
    echo    $javascript->link('libs/jquery.cookie');
  /*  echo    $javascript->link('libs/jquery.truncate-2.3');
    echo    $javascript->link('libs/jquery.jmap');*/
    echo    $javascript->link('libs/jquery.address-1.2.1');
    /*echo    $javascript->link('libs/jquery.jcarousel');*/
    echo    $javascript->link('libs/facebook-sdk');
    echo    $javascript->link('libs/mathcontext');
    echo    $javascript->link('libs/bigdecimal');
    echo    $javascript->link('/now/js/common.js?v=7');
    /*echo    $javascript->link('vtip-min.js');
    echo    $javascript->link('libs/jquery.scrollTo-1.4.2-min.js');

*/
    echo $javascript->link(Configure::read('theme.theme_name') . 'facebook');
    echo $javascript->link(Configure::read('theme.theme_name') . 'funciones');


        ?>

        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />

    </head>
