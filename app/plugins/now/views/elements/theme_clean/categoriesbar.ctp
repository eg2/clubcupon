<style>
    .spr-todas_first{
        background-image: none!important;
        padding-left:10px!important;
    }
</style>

<?php $this->data['help_category'] = array(); ?>
<div class="grid_16 columna-izq">
<div class="cont-borde">
	<ul class="ccn-menu clearfix">
            <li class="menu-item-todas">
                <a id="cat-todas" class="now-category spr-todas_first" rel="todas" name="todas" href="#">
                    Todas las Categor&iacute;as
                </a>
            </li>
	<?php foreach($ccnow_deal_categories as $key=>$ccnow_category){ ?>
		<li class="menu-item-<?php echo $ccnow_category['DealCategories']['id'];?>">
			<a id="cat-<?php echo $ccnow_category['DealCategories']['id'];?>" class="now-category spr-<?php echo $ccnow_category['DealCategories']['icon']; ?>" rel="<?php echo $ccnow_category['DealCategories']['id'];?>" name="<?php echo $ccnow_category['DealCategories']['icon'];?>" href="#tab-<?php echo $ccnow_category['DealCategories']['id']; ?>">
				<?php echo $ccnow_category['DealCategories']['name']; ?>
				<?php
					
					$this->data['help_category'][$ccnow_category['DealCategories']['id']]=$ccnow_category['DealCategories']['name'];
				?>
			</a>
		</li>
	<?php } ?>
        
    
    <li class="link-como-funciona" style="border:none; float:right;">
        
        <?php
            echo $html->link ('&iquest;C&oacute;mo funciona Club Cup&oacute;n Ya?', array ('plugin'=>'now','controller' => 'now_registers', 'action' => 'landingus', 'admin' => false), array('escape'=>false));
        ?>
    </li>
	</ul>
    
</div>
</div>