<h4 class="subtitle"><?php echo __l('Commission Calculator'); ?></h4>
<?php
	if(empty($this->data['NowDeal']['user_id'])):
		echo $form->create('NowDeal', array('plugin'=>'now', 'action'=> 'commission_calculator', 'class' => 'normal'));
	endif;
?>
<div class="clearfix">
	<?php
		echo $form->input('calculator_discounted_price',array('label'=>__l('Discounted Price')));
		echo $form->input('calculator_bonus_amount', array('label'=> __l('Bonus Amount')));
	?>
</div>
<div class="clearfix">
	<?php
		echo $form->input('calculator_commission_percentage', array('label'=>__l('Commission (%)')));
		echo $form->input('calculator_min_limit', array('label'=>__l('No. of buyers')));
	?>
</div>
<?php
	if(empty($this->data['NowDeal']['user_id'])):
		echo $form->end(__l('Calculate'));
	endif;
?>
<dl class="result-list clearfix">
	<dt><?php echo __l('Total Purchased Amount: '); ?></dt>
	<dd><?php echo Configure::read('site.currency'); ?><span class="js-calculator-purchased"><?php echo (!empty($this->data['NowDeal']['calculator_total_purchased_amount'])) ? $this->data['NowDeal']['calculator_total_purchased_amount'] : 0 ?></span></dd>
	<dt><?php echo __l('Total Commission Amount: '); ?></dt>
	<dd><?php echo Configure::read('site.currency'); ?><span class="js-calculator-commission"><?php echo (!empty($this->data['NowDeal']['calculator_total_commission_amount'])) ? $this->data['NowDeal']['calculator_total_commission_amount'] : 0 ?></span></dd>
	<?php if(ConstUserTypes::isLikeAdmin($auth->user('user_type_id'))):?>
		<dt><?php echo __l('Net Profit: '); ?></dt>
		<dd><?php echo Configure::read('site.currency'); ?><span class="js-calculator-net-profit"><?php echo (!empty($this->data['NowDeal']['calculator_net_profit'])) ? $this->data['NowDeal']['calculator_net_profit'] : 0 ?></span></dd>
	<?php endif; ?>
</dl>