<div id="main_container" class="grid_12 columna-der">
	<?php if(isset($branchesDeals)){?>
		<h1>Todas las Ofertas</h1>
		<h2>Mostrando <?php echo "1";?>-<?php echo "10";?> de <?php echo "TOTAL";?> Ofertas en <?php echo "LUGAR";?> disponibles <?php echo "CUANDO";?>.</h2>
		
		<div id="deals_contenedor" class="contenedor-tabs">
			<div id="deals_detalles" style="">
				<?php
					foreach($branchesDeals as $key=>$deal){
						echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'oferta', array('deal'=>$deal));
					}
				?>
			</div>
			
		</div>
		
		<div class="grid_12 paginador">
			<ul>
				<li class="pag-prev-next"><a href="#">Anterior</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li class="pag-prev-next"><a href="#">Siguiente</a></li>
			</ul>
			<p><span>11-20</span> de 105 Ofertas</p>
		</div>
	<?php }?>
	</div>