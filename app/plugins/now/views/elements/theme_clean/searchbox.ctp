<div class="grid_16 contenedorBuscar">
<div class="grid_16 buscar-ofertas">
		<div class="grid_4 toahora">
			<img alt="CLUB CUP&#211;N." src="/img/now/toahora.png">
		</div>
	<?php echo $form->create(null,array('id'=>'NowDealIndexForm','method'=>'POST','url'=>array('plugin'=>'now','controller'=>'NowDeals','action'=>'index'))); ?>
		<div class="grid_5 inp">
			<label class="clearfix">
				&#191;D&oacute;nde Est&#225;s?
				<?php echo $form->input('latlng',array('type'=>'hidden','value'=>'0')); ?>
				<?php echo $form->input('category',array('type'=>'hidden','value'=>'todas','rel'=>'todas')); ?>
				<input id="NowDealLocationId" type="hidden" name="data[NowDeal][locationid]" value="" rel="">
				<input id="NowDealLocation" type="text" name="data[NowDeal][location]" onblur="if (this.value=='') { this.value=window.nowDefText; this.rel=window.nowDefText;$('#city-ops').slideUp();}" value="" onclick="if (this.value==window.nowDefText) { this.value='' }" tabindex="1" class="text1" autocomplete="OFF" rel="">
        <span style="margin-left: -30px; display: inline-block">
          <div id="NowDealLocationLoading" class="loading" style="display: none; margin-top: -20px; margin-left: -10px;"><div class="spinner"><div class="mask"><div class="maskedCircle"></div></div></div></div>
        </span>
			</label>
			<div id="city-ops" style="display:none;background: none repeat scroll 0 0 white;border: 1px solid #CCCCCC;padding: 5px 0 12px 20px;position: relative;top: -8px;width: 251px;z-index: 100;border-top: none;border-radius:0px 0px 5px 5px;"></div>
		</div>		
		
		<div class="grid_3 inp omega">
				<label class="clearfix">
					Para usar
					<?php $fix = NowTimeSpanOptions::fix();?>
					<select name="data[NowDeal][timespan]" class="sel">
						<option value="H"><?php echo $fix['H']?></option>
						<option value="A"><?php echo $fix['A']?> (<?php echo date('H:i')?>)</option>
						<?php
						foreach(NowTimeSpanOptions::vars() as $option){
							if(date('G')<$option[1]){
								echo '<option value="'.$option[0].'.'.$option[1].'">'.$option[2].'</option>';
							}else{
								echo '<option value="'.$option[0].'.'.$option[1].'" disabled="disabled" style="color: #DDDDDD;">'.$option[2].'</option>';
							}
						}
						?>
					</select>
				</label>
		</div>

		<div class="grid_2 sum alpha">
				<label class="boton-buscar clearfix">
					<input type="submit" name="buscar submit" value="Buscar Ofertas" class="submit" id="submitCitySearch">
				</label>
		</div>
	<?php echo $form->end(); ?>
</div>
</div>
<script type="text/javascript">
nowDefText = 'Barrio/Ciudad, Localidad';
$(document).ready(function(){
<?php if(isset($this->data['NowDeal'])){ ?>
	$('option[value="<?php echo $this->data['NowDeal']['timespan'];?>"]').attr('selected','selected');
	$('#NowDealCategory').attr('value','<?php echo $this->data['NowDeal']['category'];?>');
	$('#NowDealLatlng').attr('value','<?php echo $this->data['NowDeal']['latlng'];?>');
	nowDefText = '<?php echo $this->data['NowDeal']['location'];?>';
	$("#catoverlay").fadeOut();
<?php }?>
	$("#NowDealLocation").attr('value',nowDefText);
	$("#NowDealLocation").attr('rel',nowDefText);
});
</script>
