<?php
	$title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $browser = $_SERVER['HTTP_USER_AGENT'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml"<?php if (isset ($og_data)) echo 'xmlns:og = "http://ogp.me/ns#"  xmlns:fb = "http://www.facebook.com/2008/fbml"'; ?>>
	<head>
        <?php echo $html->charset(), "\n"; ?>
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        <title>
        <?php

            echo Configure::read('site.name') . ' | '.$title_for_layout;
        ?>
        </title>
        <?php
            echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

            if (isset ($og_data)) {
                foreach ($og_data as $key => $value) {
                    if (is_array ($value)) {
                        foreach ($value as $val) {
                            echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                        }
                    } else {
                    echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                    }
                }
            }
            echo $html->css(Configure::read('now.asset_version').'960_16_col.css');
            echo $html->css(Configure::read('now.asset_version').'reset.css');
			echo $html->css(Configure::read('now.asset_version').'global.css');
			
			//echo $html->css("/now/css/".Configure::read('theme.theme_name') . 'global');     //estilos funcionales
            echo $html->css(Configure::read('theme.theme_name').Configure::read('theme.version_number'). 'colorbox');   //popup
			
		//JSs
        //Jquery
	  echo $html->css('plugins/'.Configure::read('theme.version_number').'jquery.autocomplete');
      echo $html->css('plugins/'.Configure::read('theme.version_number').'jquery-ui-1.7.1.custom');
      echo $html->css('plugins/'.Configure::read('theme.version_number').'ui.timepickr');
      echo $html->css('plugins/'.Configure::read('theme.version_number').'colorbox');
      echo $html->css('jcarousel-skin-clubcupon');

	echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true));
	echo $javascript->codeBlock('var NowPlugin_calendar_minium_date = ' .Configure::read('NowPlugin.calendar_minium_date'));
    //echo $javascript->link('libs/jquery');
	echo $javascript->link('/js/theme_clean/'.Configure::read('theme.version_number').'jquery-1.7.1.min');
	//echo $javascript->link('/js/theme_clean/jquery.colorbox');
	echo $javascript->link(Configure::read('now.asset_version').'jquery.colorbox.js');
    echo $javascript->link('libs/'.Configure::read('theme.version_number').'jquery.livequery');
	echo $javascript->link('libs/'.Configure::read('theme.version_number').'jquery-ui-1.7.2.custom.min');
    echo $javascript->link('libs/'.Configure::read('theme.version_number').'jquery.countdown');
    echo $javascript->link('libs/'.Configure::read('theme.version_number').'jquery.timepickr');
    echo $javascript->link('libs/'.Configure::read('theme.version_number').'jquery.cookie');
    //echo $javascript->link('libs/jquery.address-1.2.1');
    echo $javascript->link('libs/'.Configure::read('theme.version_number').'facebook-sdk');
    //echo $javascript->link('libs/mathcontext');
    //echo $javascript->link('libs/bigdecimal');
    echo $javascript->link('/now/js/common.js?v=7');

    echo $javascript->link(Configure::read('theme.theme_name') .Configure::read('theme.version_number'). 'facebook');
    //echo $javascript->link(Configure::read('theme.theme_name') . 'funciones');
			
			echo $javascript->link('http://maps.google.com/maps/api/js?sensor=false&libraries=geometry');
			echo $javascript->link(Configure::read('now.asset_version').'city-search.js');
			echo $javascript->link(Configure::read('now.asset_version').'funciones.js');
			
		?>
        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />
        
        
        
        <?php
        
            if (isset ($og_data))
            {
                // var_dump ($og_data);
                foreach ($og_data as $key => $value)
                {
                    if (is_array ($value))
                    {
                        foreach ($value as $val)
                        {
                            echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                        }
                    }
                    else
                    {
                        echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                    }
                }
            }
        
        ?>
        
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
	</head>
