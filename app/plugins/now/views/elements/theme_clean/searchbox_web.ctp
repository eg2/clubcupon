<?php 
/*
 * element: searchbox_web.ctp
 * @author: Pward 
 * params: 
 * 
 */

?>	

<?php echo $form->create(null,array('id'=>'NowDealForm','method'=>'POST','url'=>array('plugin'=>'now','controller'=>'NowDeals','action'=>'index'))); ?>
<section id="filtro">
	<p>Buscá las mejores ofertas</p>
	<div class="content">
		<label>de</label>
		<?php $this->data['help_categoryid'] = array(); ?>
		
		<select  name="data[NowDeal][categoryid]">
		<?php foreach($categories as $category): ?>
			<option value="<?php echo $category->id;?>"><?php echo $category->name; ?></option>
			<?php $this->data['help_category'][$category->id]=$category->name; ?>
		<?php endforeach; ?>
		</select>
		
		<label>cerca de</label>
		
		<?php echo $form->input('latlng',array('type'=>'hidden','value'=>'0')); ?>
		
		<input id="NowDealLocationId" type="hidden" name="data[NowDeal][locationid]" value="" rel="">
		<input id="NowDealLocation" type="text" name="data[NowDeal][location]" onblur="if (this.value=='') { this.value=window.nowDefText; this.rel=window.nowDefText;$('#city-ops').slideUp();}" value="" onclick="if (this.value==window.nowDefText) { this.value='' }" tabindex="1" class="text1" autocomplete="OFF" rel="">
		
		<div id="city-ops" style="display:none;"></div>
		
		<label>para usar</label>		
		<?php $fix = NowTimeSpanOptions::fix();?>
		<select name="data[NowDeal][timespan]" class="para_usar">
			<option value="H"><?php echo $fix['H']?></option>
			<option value="A"><?php echo $fix['A']?> (<?php echo date('H:i')?>)</option>
			<?php
				foreach(NowTimeSpanOptions::vars() as $option) {
					if(date('G')<$option[1]) {
						echo '<option value="'.$option[0].'.'.$option[1].'">'.$option[2].'</option>';
					}else {
						echo '<option value="'.$option[0].'.'.$option[1].'" disabled="disabled" style="color: #DDDDDD;">'.$option[2].'</option>';
					}
				}
			?>
		</select>
		
		<input type="submit" class="buscar" name="buscar submit" value="Buscar" id="submitCitySearch">
	</div>
</section>
<?php echo $form->end(); ?>
	
<script type="text/javascript">
	nowDefText = 'Barrio/Ciudad, Localidad';
	$(document).ready(function(){
	<?php if(isset($this->data['NowDeal'])){ ?>
		$('option[value="<?php echo $this->data['NowDeal']['timespan'];?>"]').attr('selected','selected');
		$('#NowDealCategory').attr('value','<?php echo $this->data['NowDeal']['category'];?>');
		$('#NowDealLatlng').attr('value','<?php echo $this->data['NowDeal']['latlng'];?>');
		nowDefText = '<?php echo $this->data['NowDeal']['location'];?>';
		$("#catoverlay").fadeOut();
	<?php }?>
		$("#NowDealLocation").attr('value',nowDefText);
		$("#NowDealLocation").attr('rel',nowDefText);
	});
</script>