<div class="bloque-ofertas">
	<div style="float: left; height: 177px; width: 271px; background-color:#148BB3; background-position:top right; background-repeat:no-repeat; background-size:cover;">
        <?php
            $image_url = Deal::getImageUrl('Deal',$deal['NowDeal']['Attachment'],array('dimension' => 'small_big_thumb'));
            
           echo $html->link( 
                $html->image($image_url), 
                array(
                    'plugin' => '',
                    'controller' => 'deals',
                    'action' => 'buy',
                    $deal['NowDeal']['id']
                    ), 
                array(
                    'title'=>$deal['Deal']['name'],
                    'class'=>'nowLink',
                ), 
                null, 
                false );
        ?>

        <style>
            .nowLink img {
                width:271px;
                height:177px;
            }
        </style>
        
	</div>
	<ul class="clearfix">

		<?php //$deal['NowDeal']['name']=substr($deal['NowDeal']['name'],0,34);?>
		<li class="title">
        <h3 class="titulo-of cat-icon-<?php echo $deal['DealCategory']['icon'];?>"><a id="testo" href="<?php if (@$auth->user ('user_type_id') != ConstUserTypes::Company) echo Router::url(array('plugin'=>'order','controller'=>'order_processings','action'=>'buy','deal_id'=>$deal['NowDeal']['id']));?>" style="font-size: 19px;" title="<?php echo $deal['NowDeal']['name'];?>"><?php echo $clubcupon->shorten_strings_with_html_entities($deal['NowDeal']['name'], 54);?></a></h3>
		</li>
		<li class="encabezado-promo">
			<span>$<?php echo number_format($deal['NowDeal']['discounted_price'],0,'','');?></span> en vez de <span>$<?php echo number_format($deal['NowDeal']['original_price'],0,'','');?></span> en <span><?php echo $deal['NowDeal']['Company']['name'];?></span>.
		</li>
        <li class="description" title="<?php echo htmlentities($deal['NowDeal']['description'],ENT_COMPAT,'UTF-8',false);?>" >
            <?php
                echo $clubcupon->shorten_strings_with_html_entities($deal['NowDeal']['description'], 300);
            ?>
        </li>
		<li class="buy">
		<?php
				/* si la fecha fin es 00hs la cambio por 24*/		
				$ts_fin=date("H",strtotime($deal['NowBranchesDeals']['ts_fin']));
				if($ts_fin<1) $ts_fin='24';
		?>
			<p class="usarhoy">
                    Para usar Hoy de <?php echo date("H",strtotime($deal['NowBranchesDeals']['ts_inicio']));?>hs a <?php echo $ts_fin;?>hs!
    				En <?php echo $deal['NowBranch']['street']." ".$deal['NowBranch']['number'];?>
				<?php
					echo $deal['NowBranch']['floor']=="Piso"?"":" ".$deal['NowBranch']['floor'];
					echo $deal['NowBranch']['apartament']=="Dpto"?"":" ".$deal['NowBranch']['apartament'];
				?>
			</p>
			<?php if (@$auth->user ('user_type_id') != ConstUserTypes::Company){?>
				<?php if ($deal['NowDeal']['deal_user_count'] < $deal['NowDeal']['max_limit']){?>
					<?php echo $html->link('Comprar!',array('plugin'=>'','controller'=>'deals','action'=>'buy','id'=>$deal['NowDeal']['id']),array('class'=>'btn-comprar'))?>
				<?php }else{ ?>
					<a style="width: auto;" class="btn-masinfo-azul" href="#">Comprar!</a>
				<?php } ?>
			<?php } ?>
			<?php echo $html->link('Ver Detalles',array('plugin'=>'now','controller'=>'now_deals','action'=>'detalles','id'=>$deal['NowDeal']['id'],$deal['NowBranchesDeals']['now_branch_id']),array('class'=>'detalles btn-masinfo-azul'))?>
		</li>
	</ul>
	<div class="utilice-hoy">
		<p>Descuento <?php echo floor(100-($deal['NowDeal']['discounted_price']*100/$deal['NowDeal']['original_price']));?>%</p><p><strong>Precio $<?php echo number_format($deal['NowDeal']['discounted_price'],0,'','');?></strong></p>
	</div>
</div>