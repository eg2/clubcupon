<?php

App::import('', 'accounting.AccountingAppService');
App::import('Model', 'accounting.AccountedItem');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');
App::import('Service', 'accounting.AccountingBacService');
App::import('Service', 'accounting.AccountingItemBacNotifier');
App::import('Component', 'api.ApiBacService');

class AccountingCompanyBillBacNotifier extends AccountingItemBacNotifier {

    function __construct() {
        parent::__construct();
        $this->ApiCompany = ClassRegistry::init('ApiCompany');
        $this->ApiDeal = ClassRegistry::init('ApiDeal');
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->ApiBacService = ClassRegistry::init('ApiBacServiceComponent');
        $this->AccountingBacService = ClassRegistry::init('AccountingBacService');
    }

    function notifyItemsToBac($accountingCalendar, $accountingPartner, $accountingItems, $refreshPartnerInBac = true) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingCalendar,
            $accountingPartner,
            $accountingItems,
            $refreshPartnerInBac
        ));
        if ($refreshPartnerInBac) {
            $this->refreshPartnerInBac($accountingPartner);
        }
        $this->AccountingBacService->createCompanyBill($accountingPartner, $accountingCalendar, $accountingItems);
    }

    function refreshPartnerInBac($accountingPartner) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingPartner
        ));
        $company = $accountingPartner;
        try {
            $result = $this->ApiBacService->createBacCompanyUser($company);
        } catch (Exception $e) {
            $this->ApiLogger->error('Ocurrio un error al actualizar la compania en bac', $e->getMessage());
        }
        return $result;
    }

    function findPartner($accountingItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingItem
        ));
        return $this->Company->findById($accountingItem['AccountingItem']['model_id']);
    }

}
