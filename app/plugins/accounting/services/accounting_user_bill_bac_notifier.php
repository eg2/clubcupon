<?php

App::import('', 'accounting.AccountingAppService');
App::import('Model', 'accounting.AccountedItem');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiDeal');
App::import('Service', 'accounting.AccountingBacService');
App::import('Service', 'accounting.AccountingItemBacNotifier');
App::import('Component', 'api.ApiBacService');

class AccountingUserBillBacNotifier extends AccountingItemBacNotifier {

    function __construct() {
        parent::__construct();
        $this->ApiUser = ClassRegistry::init('ApiUser');
        $this->ApiDeal = ClassRegistry::init('ApiDeal');
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->ApiBacService = ClassRegistry::init('ApiBacServiceComponent');
        $this->AccountingBacService = ClassRegistry::init('AccountingBacService');
    }

    function notifyItemsToBac($accountingCalendar, $accountingPartner, $accountingItems, $refreshPartnerInBac = true) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingCalendar,
            $accountingPartner,
            $accountingItems,
            $refreshPartnerInBac
        ));
        if ($refreshPartnerInBac) {
            $this->refreshPartnerInBac($accountingItems[0]);
        }
        $this->AccountingBacService->createUserBill($accountingPartner, $accountingCalendar, $accountingItems);
    }

    function refreshPartnerInBac($accountingPartner) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingPartner
        ));
        $user = $accountingPartner;
        try {
            $result = $this->ApiBacService->createBacUser($user);
        } catch (Exception $e) {
            $this->ApiLogger->error('Ocurrio un error al actualizar el usuario en bac', $e->getMessage());
        }
        return $result;
    }

    function findPartner($accountingItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingItem
        ));
        return $this->ApiUser->findById($accountingItem['AccountingItem']['model_id']);
    }

    function refreshPartnerInBacByItem($item) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $item
        ));
        return $this->refreshPartnerInBac($this->findPartner($item));
    }

}
