<?php

App::import('', 'accounting.AccountingAppService');
App::import('Service', 'accounting.AccountingCompanyBillBacNotifier');
App::import('Service', 'accounting.AccountingUserBillBacNotifier');

class AccountingBacNotifierFactory extends AccountingAppService {

    function __construct() {
        parent::__construct();
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
    }

    function getBacNotifier($item) {
        if ($this->AccountingItem->isForCompany($item)) {
            return new AccountingCompanyBillBacNotifier();
        } elseif ($this->AccountingItem->isForUser($item)) {
            return new AccountingUserBillBacNotifier();
        }
    }

}
