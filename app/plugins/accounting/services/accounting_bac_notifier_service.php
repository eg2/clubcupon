<?php

App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'accounting.AccountingItemDetail');
App::import('Model', 'accounting.AccountingType');
App::import('Model', 'api.ApiCompany');
App::import('Service', 'accounting.AccountingItemBacNotifierFactory');
App::import('', 'accounting.AccountingAppService');

class AccountingBacNotifierService extends AccountingAppService {

    public $name = 'AccountingBacNotifierService';

    function __construct() {
        parent::__construct('accounting');
        $this->AccountingItemBacNotifierFactory = ClassRegistry::init('AccountingItemBacNotifierFactory');
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->AccountingCalendar = ClassRegistry::init('AccountingCalendar');
        $this->ApiCompany = ClassRegistry::init('ApiCompany');
    }

    function notifyToBac() {
        $this->ApiLogger->info(__FUNCTION__, null);
        $calendarsPreAccounted = $this->AccountingCalendar->find('all', array(
            'conditions' => array(
                'AccountingCalendar.status' => AccountingCalendar::STATUS_ACCEPTED
            )
        ));
        foreach ($calendarsPreAccounted as $calendar) {
            $this->notifyCalendarToBac($calendar);
        }
    }

    function notifyCalendarToBac($calendar) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar
        ));
        $companyBillsToNotify = $this->AccountingItem->findBillsNotNotifiedToBac(array(
            'AccountingItem.accounting_calendar_id' => $calendar['AccountingCalendar']['id'],
            'AccountingItem.model' => AccountingItem::MODEL_COMPANY
        ));
        if (!empty($companyBillsToNotify)) {
            $this->notifyCompanyBillItemsToBac($calendar, $companyBillsToNotify);
        }
        $userBillsToNotify = $this->AccountingItem->findBillsNotNotifiedToBac(array(
            'AccountingItem.accounting_calendar_id' => $calendar['AccountingCalendar']['id'],
            'AccountingItem.model' => AccountingItem::MODEL_USER
        ));
        if (!empty($userBillsToNotify)) {
            $this->notifyUserBillItemsToBac($calendar, $userBillsToNotify);
        }
        $this->AccountingCalendar->updateAsAccounted($calendar);
    }

    function notifyCompanyBillItemsToBac($calendar, $items) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $items
        ));
        if (empty($items)) {
            throw new Exception("Items param must not be empty");
        }
        $i = 0;
        $prevCompanyId = null;
        $numberOfItems = count($items);
        $item = $items[$i];
        while ($i < $numberOfItems) {
            $prevCompanyId = $item['AccountingItem']['model_id'];
            $company = $this->ApiCompany->findById($prevCompanyId);
            $bacNotifier = $this->AccountingItemBacNotifierFactory->getBacNotifier($item);
            $hasRefreshCompanyInBac = $bacNotifier->refreshPartnerInBac($company);
            $itemsToNotifyForCompany = array();
            while ($i < $numberOfItems && $prevCompanyId == $item['AccountingItem']['model_id']) {
                $itemsToNotifyForCompany[] = $item;
                $i = $i + 1;
                if ($i < $numberOfItems) {
                    $item = $items[$i];
                }
            }
            if ($hasRefreshCompanyInBac) {
                try {
                    $bacNotifier->notifyItemsToBac($calendar, $company, $itemsToNotifyForCompany, false);
                } catch (Exception $e) {
                    $this->ApiLogger->error('Error al Processar el Item Calendar', $e->getMessage());
                }
            }
        }
    }

    function notifyUserBillItemsToBac($calendar, $items) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $items
        ));
        if (empty($items)) {
            throw new Exception("Items param must not be empty");
        }
        $i = 0;
        $prevModel = null;
        $numberOfItems = count($items);
        $item = $items[$i];
        while ($i < $numberOfItems) {
            $prevModel = $item['AccountingItem']['model'];
            $prevModelId = $item['AccountingItem']['model_id'];
            $bacNotifier = $this->AccountingItemBacNotifierFactory->getBacNotifier($item);
            $hasRefreshUserInBac = $bacNotifier->refreshPartnerInBacByItem($item);
            while ($i < $numberOfItems && $prevModel == $item['AccountingItem']['model'] && $prevModelId == $item['AccountingItem']['model_id']) {
                if ($hasRefreshUserInBac) {
                    try {
                        $bacNotifier->notifyToBacOneItem($item, false);
                    } catch (Exception $e) {
                        $this->ApiLogger->error('Error al Processar el Item Calendar', $e->getMessage());
                    }
                }
                $i = $i + 1;
                if ($i < $numberOfItems) {
                    $item = $items[$i];
                }
            }
        }
    }

}
