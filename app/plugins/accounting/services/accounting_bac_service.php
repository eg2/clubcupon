<?php

App::import('', 'accounting.AccountingAppService');
App::import('Component', 'api.ApiBac');
App::import('Component', 'api.ApiBacService');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'accounting.AccountingItemDetail');
App::import('Model', 'accounting.AccountingError');
App::import('Model', 'accounting.AccountingType');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiDeal');

class AccountingBacService extends AccountingAppService {

    public $name = 'AccountingBacService';

    function __construct() {
        parent::__construct('accounting');
        $this->ApiBac = ClassRegistry::init('ApiBacComponent');
        $this->ApiBacService = ClassRegistry::init('ApiBacServiceComponent');
        $this->AccountingCalendar = ClassRegistry::init('AccountingCalendar');
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->AccountingError = ClassRegistry::init('AccountingError');
        $this->ApiCompany = ClassRegistry::init('ApiCompany');
        $this->ApiDeal = ClassRegistry::init('ApiDeal');
        $this->ApiUser = ClassRegistry::init('ApiUser');
        $this->bacIdGatewayEffectivo = Configure::read('BAC.gateway_efectivo');
        $this->bacIdMedioDePagoCompensacion = Configure::read('BAC.id_medio_de_pago_compensacion');
        $this->bacIdMonedaPesos = Configure::read('BAC.pesos');
        $this->bacCondicionVentaContado = Configure::read('BAC.condicion_venta_contado');
        $this->bacIdPortal = Configure::read('BAC.id_portal');
        $this->bacIdProductoComisionCmd = Configure::read('BAC.id_producto_comision_cmd');
        $this->bacIdProductoDebito = Configure::read('BAC.id_producto_debito');
        $this->bacIdProducto = Configure::read('BAC.id_producto');
        $this->bacEndUserNoRedeemed = Configure::read('BAC.bac_end_user_no_redemeed');
        $this->bacIdProductoFinal = 18000;
        $this->bacMonedaPesos = 1;
        $this->bacIdPagoPortalShift = 500000000;
    }

    function getBacIdPortal($accountingItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $accountingItem
        ));
        $deal = $this->ApiDeal->findById($accountingItem['AccountingItem']['deal_id']);
        return $this->ApiBacService->getBacIdPortalByDeal($deal);
    }

    function createUserBill($user, $accountingCalendar, $accountingItems) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $user,
            $accountingCalendar,
            $accountingItems
        ));
        try {
            $createCargosParams = $this->buildCrearCargosBacDto($user['User']['id'], $accountingItems, $accountingCalendar['AccountingCalendar']['until']);
            $bacLoteId = $this->ApiBac->crearCargos($createCargosParams);
        } catch (Exception $e) {
            $this->ApiLogger->error('Error al cargos para el usuario', $e->getMessage());
            throw new Exception('no se pudo crear la factura de cliente en BAC');
        }
        if ($bacLoteId != null || is_numeric($bacLoteId)) {
            foreach ($accountingItems as $accountingItem) {
                $this->AccountingItem->updateAsBacNotified($accountingItem, $bacLoteId);
            }
        }
        $this->createPaymentForBill($user['User']['id'], $accountingItems, $bacLoteId, 209);
        return $bacLoteId;
    }

    function createCompanyBill($company, $accountingCalendar, $accountingItems) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $company,
            $accountingCalendar,
            $accountingItems
        ));
        try {
            $createCargosParams = $this->buildCrearCargosBacDto($company['Company']['user_id'], $accountingItems, $accountingCalendar['AccountingCalendar']['until']);
            $bacLoteId = $this->ApiBac->crearCargos($createCargosParams);
            if (!is_numeric($bacLoteId)) {
                throw new Exception($bacLoteId);
            }
        } catch (Exception $e) {
            $this->AccountingError->saveErrorList($accountingItems, __METHOD__, $e->getMessage());
            $this->ApiLogger->error('Error al cargos para la compaÃ±Ã­a', $e->getMessage());
            throw new Exception('No se pudo genear los Cargo por Factura de Compania en Bac ');
        }
        foreach ($accountingItems as $accountingItem) {
            $this->AccountingItem->updateAsBacNotified($accountingItem, $bacLoteId);
        }
        $this->createPaymentForBill($company['Company']['user_id'], $accountingItems, $bacLoteId, $this->bacIdMedioDePagoCompensacion);
        return $bacLoteId;
    }

    function createPaymentForBill($userId, $accountingItems, $bacLoteId, $idMedioPago) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $userId,
            $accountingItems,
            $bacLoteId,
            $idMedioPago
        ));
        try {
            $createPaymentDto = $this->buildCreatePaymentDto($userId, $accountingItems, $bacLoteId, $idMedioPago);
            $bacPaymentId = $this->ApiBac->createPayment($createPaymentDto);
            if (!is_numeric($bacPaymentId)) {
                throw new Exception($bacPaymentId);
            }
        } catch (Exception $e) {
            $this->AccountingError->saveErrorList($accountingItems, __METHOD__, $e->getMessage());
            $this->ApiLogger->error('Error los pagos', $e->getMessage());
            throw new Exception('No se pudo genear el pago por factura en Bac ');
        }
        foreach ($accountingItems as $accountingItem) {
            $this->AccountingItem->updateAsBacPaymentNotified($accountingItem, $bacPaymentId);
        }
        return $bacPaymentId;
    }

    private function buildCreatePaymentDto($userId, $accountingItems, $bacLoteId, $idMedioPago) {
        $totalAmount = 0;
        foreach ($accountingItems as $item) {
            $totalAmount+= $item['AccountingItem']['total_amount'];
        }
        $createPaymentDto = array(
            'idPortal' => $this->bacIdPortal,
            'idUsuarioPortal' => $userId,
            'idPagoPortal' => $bacLoteId + $this->bacIdPagoPortalShift,
            'idLote' => $bacLoteId,
            'idMoneda' => $this->bacMonedaPesos,
            'monto' => $totalAmount,
            'idGateway' => $this->bacIdGatewayEffectivo,
            'idMedioPago' => $idMedioPago
        );
        return $createPaymentDto;
    }

    private function buildCrearCargosBacDto($idUsuarioPortal, $accountingItems, $fechaContabilizacion) {
        $createCargosDto = array(
            'idPortal' => $this->bacIdPortal,
            'idUsuarioPortal' => $idUsuarioPortal,
            'condicionVenta' => $this->bacCondicionVentaContado,
            'fechaContabilizacion' => strtotime($fechaContabilizacion),
            'porcentajeComision' => 0,
            'productos' => array()
        );
        foreach ($accountingItems as $accountingItem) {
            $createCargosDto['productos'][] = array(
                'idProducto' => $this->getBacIdProducto($accountingItem['AccountingItem']['accounting_type']),
                'cantidad' => 1,
                'precio' => $accountingItem['AccountingItem']['total_amount'],
                'idMoneda' => $this->bacIdMonedaPesos,
                'idComprobantePortal' => $accountingItem['AccountingItem']['id']
            );
        }
        return $createCargosDto;
    }

    private function getBacIdProducto($accountingItemType) {
        switch ($accountingItemType) {
            case AccountingType::BILL_BY_SOLD_FOR_COMPANY:
                return $this->bacIdProductoComisionCmd;
            case AccountingType::BILL_BY_SOLD_FOR_FINAL_CONSUMER:
                return $this->bacEndUserNoRedeemed;
            case AccountingType::BILL_BY_REDEEMED_FOR_COMPANY:
                return $this->bacIdProductoComisionCmd;
            default:
                throw new Exception('El tipo de item contable no tiene un producto bac definido.' . json_encode(array(
                    'tipo de item contable' => $accountingItemType
                )));
        }
    }

}
