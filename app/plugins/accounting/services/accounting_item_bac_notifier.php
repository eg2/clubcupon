<?php

App::import('', 'accounting.AccountingAppService');
App::import('Model', 'accounting.AccountedItem');
App::import('Model', 'accounting.AccountedCalendar');

abstract class AccountingItemBacNotifier extends AccountingAppService {

    function __construct() {
        parent::__construct();
        $this->AccountingCalendar = ClassRegistry::init('AccountingCalendar');
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->ApiBacService = ClassRegistry::init('ApiBacServiceComponent');
        $this->AccountingBacService = ClassRegistry::init('AccountingBacService');
    }

    function notifyItemsToBac($accountingCalendar, $accountingPartner, $accountingItems, $refreshPartnerInBac) {
        throw new Exception(__METHOD__ . " no implementado.");
    }

    function refreshPartnerInBac($accountingItem) {
        throw new Exception(__METHOD__ . " no implementado.");
    }

    function findPartner($accountingItem) {
        throw new Exception(__METHOD__ . " no implementado.");
    }

    function refreshPartnerInBacByItem($item) {
        return $this->refreshPartnerInBac($this->findPartner($item));
    }

    function notifyToBacOneItem($item) {
        $calendar = $this->AccountingCalendar->findById($item['AccountingItem']['accounting_calendar_id']);
        $partner = $this->findPartner($item);
        return $this->notifyItemsToBac($calendar, $partner, array(
                    $item
                        ), false);
    }

}
