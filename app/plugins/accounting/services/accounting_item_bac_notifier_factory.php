<?php

App::import('', 'accounting.AccountingAppService');
App::import('Service', 'accounting.AccountingCompanyBillBacNotifier');
App::import('Service', 'accounting.AccountingUserBillBacNotifier');

class AccountingItemBacNotifierFactory extends AccountingAppService {

    function __construct() {
        parent::__construct();
        $this->AccountingItem = ClassRegistry::init('AccountingItem');
        $this->AccountingUserBillBacNotifier = ClassRegistry::init('AccountingUserBillBacNotifier');
        $this->AccountingCompanyBillBacNotifier = ClassRegistry::init('AccountingCompanyBillBacNotifier');
    }

    function getBacNotifier($item) {
        if ($this->AccountingItem->isForCompany($item)) {
            return $this->AccountingCompanyBillBacNotifier;
        } elseif ($this->AccountingItem->isForUser($item)) {
            return $this->AccountingUserBillBacNotifier;
        }
    }

}
