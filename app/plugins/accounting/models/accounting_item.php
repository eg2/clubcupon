<?php

App::import('Model', 'accounting.AccountingType');

class AccountingItem extends AccountingAppModel {

    const MODEL_COMPANY = 'Company';
    const MODEL_USER = 'User';

    public $name = 'AccountingItem';
    public $actsAs = array(
        'api.SoftDeletable',
    );
    public $belongsTo = array(
        'AccountingCalendar' => array(
            'className' => 'accounting.AccountingCalendar',
            'foreignKey' => 'accounting_calendar_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    private $conditionsForBill = array(
        'AccountingItem.accounting_type' => array(
            AccountingType::BILL_BY_REDEEMED_FOR_COMPANY,
            AccountingType::BILL_BY_SOLD_FOR_COMPANY,
            AccountingType::BILL_BY_SOLD_FOR_FINAL_CONSUMER,
        )
    );
    private $conditionsForNotNotifiedToBac = array(
        'AccountingItem.bac_response_id' => null
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function newItem() {
        return array(
            'AccountingItem' => array(
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'created_by' => 0,
                'modified_by' => null,
                'deleted' => 0,
                'accounting_calendar_id' => null,
                'accounting_item_id' => null,
                'accounting_type' => null,
                'model' => null,
                'model_id' => null,
                'deal_id' => null,
                'total_quantity' => 0,
                'total_amount' => 0,
                'bac_sent' => null,
                'bac_response_id' => null,
                'discounted_price' => 0,
                'billing_commission_percentage' => 0,
                'billing_iva_percentage' => 0,
                'liquidating_is_first' => 0,
                'liquidating_guarantee_fund_percentage' => 0,
                'liquidating_agreed_percentage' => 0,
                'liquidating_invoiced_amount' => 0,
                'liquidating_iibb_percentage' => 0,
            )
        );
    }

    function newBillBySoldForCompany($deal, $calendar, $dealUser, $iva) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::BILL_BY_SOLD_FOR_COMPANY;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $dealUser[0]['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['billing_commission_percentage'] = $deal['Deal']['commission_percentage'];
        $item['AccountingItem']['billing_iva_percentage'] = round($iva * 100, 2);
        return $item;
    }

    function newBillBySoldForFinalConsumer($deal, $calendar, $dealUser, $iva) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::BILL_BY_SOLD_FOR_FINAL_CONSUMER;
        $item['AccountingItem']['model'] = self::MODEL_USER;
        $item['AccountingItem']['model_id'] = $dealUser['DealUser']['user_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $dealUser[0]['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['billing_commission_percentage'] = $deal['Deal']['commission_percentage'];
        $item['AccountingItem']['billing_iva_percentage'] = round($iva * 100, 2);
        return $item;
    }

    function newBillByRedeemedForCompany($deal, $calendar, $dealUser, $iva) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::BILL_BY_REDEEMED_FOR_COMPANY;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $dealUser[0]['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['billing_commission_percentage'] = $deal['Deal']['commission_percentage'];
        $item['AccountingItem']['billing_iva_percentage'] = round($iva * 100, 2);
        return $item;
    }

    function newLiquidateBySold($calendar, $deal, $billItem) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_item_id'] = $billItem['AccountingItem']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::LIQUIDATE_BY_SOLD;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $billItem['AccountingItem']['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['liquidating_guarantee_fund_percentage'] = $deal['Deal']['downpayment_percentage'];
        $item['AccountingItem']['liquidating_agreed_percentage'] = $deal['Deal']['agreed_percentage'];
        $item['AccountingItem']['liquidating_invoiced_amount'] = $billItem['AccountingItem']['total_amount'];
        return $item;
    }

    function newLiquidateBySoldExpired($calendar, $deal, $billItem) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_item_id'] = $billItem['AccountingItem']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::LIQUIDATE_BY_SOLD_EXPIRED;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $billItem['AccountingItem']['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['liquidating_guarantee_fund_percentage'] = $deal['Deal']['downpayment_percentage'];
        $item['AccountingItem']['liquidating_agreed_percentage'] = $deal['Deal']['agreed_percentage'];
        $item['AccountingItem']['liquidating_invoiced_amount'] = $billItem['AccountingItem']['total_amount'];
        return $item;
    }

    function newLiquidateBySoldExpiredWithoutBill($calendar, $deal) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['liquidating_guarantee_fund_percentage'] = $deal['Deal']['downpayment_percentage'];
        $item['AccountingItem']['liquidating_agreed_percentage'] = $deal['Deal']['agreed_percentage'];
        return $item;
    }

    function newLiquidateByRedeemed($calendar, $deal, $billItem) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_item_id'] = $billItem['AccountingItem']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::LIQUIDATE_BY_REDEEMED;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $billItem['AccountingItem']['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['liquidating_guarantee_fund_percentage'] = $deal['Deal']['downpayment_percentage'];
        $item['AccountingItem']['liquidating_agreed_percentage'] = $deal['Deal']['agreed_percentage'];
        $item['AccountingItem']['liquidating_invoiced_amount'] = $billItem['AccountingItem']['total_amount'];
        return $item;
    }

    function newLiquidateByRedeemedExpired($calendar, $deal, $billItem) {
        $item = $this->newItem();
        $item['AccountingItem']['accounting_calendar_id'] = $calendar['AccountingCalendar']['id'];
        $item['AccountingItem']['accounting_item_id'] = $billItem['AccountingItem']['id'];
        $item['AccountingItem']['accounting_type'] = AccountingType::LIQUIDATE_BY_REDEEMED_EXPIRED;
        $item['AccountingItem']['model'] = self::MODEL_COMPANY;
        $item['AccountingItem']['model_id'] = $deal['Deal']['company_id'];
        $item['AccountingItem']['deal_id'] = $deal['Deal']['id'];
        $item['AccountingItem']['total_quantity'] = $billItem['AccountingItem']['total_quantity'];
        $item['AccountingItem']['discounted_price'] = $deal['Deal']['discounted_price'];
        $item['AccountingItem']['liquidating_guarantee_fund_percentage'] = $deal['Deal']['downpayment_percentage'];
        $item['AccountingItem']['liquidating_agreed_percentage'] = $deal['Deal']['agreed_percentage'];
        $item['AccountingItem']['liquidating_invoiced_amount'] = $billItem['AccountingItem']['total_amount'];
        return $item;
    }

    public function findBillsNotNotifiedToBac($extraConditions = null, $order = null, $limit = null, $page = null) {
        $conditionsForBillNotNotifiedToBac = array_merge($this->conditionsForBill, $this->conditionsForNotNotifiedToBac);
        if (!is_null($extraConditions)) {
            $conditionsForBillNotNotifiedToBac = array_merge($conditionsForBillNotNotifiedToBac, $extraConditions);
        }
        if (is_null($order)) {
            $order = array(
                'accounting_calendar_id',
                'model',
                'model_id'
            );
        }
        return $this->find('all', array(
                    'conditions' => $conditionsForBillNotNotifiedToBac,
                    'order' => $order,
                    'limit' => $limit,
                    'page' => $page,
        ));
    }

    public function isForUser($item) {
        return $item['AccountingItem']['model'] == self::MODEL_USER;
    }

    public function isForCompany($item) {
        return $item['AccountingItem']['model'] == self::MODEL_COMPANY;
    }

    public function isRegistrable($item) {
        return $item['AccountingItem']['total_amount'] > 0;
    }

    private function previousLiquidateConditions($types, $deal) {
        return array(
            'AccountingItem.deal_id' => $deal['Deal']['id'],
            'AccountingItem.accounting_type' => $types
        );
    }

    public function findAllPreviousLiquidateBySold($item) {
        $fields = array(
            'AccountingItem.total_quantity',
            'AccountingItem.discounted_price',
            'AccountingItem.liquidating_guarantee_fund_percentage'
        );
        $types = array(
            AccountingType::LIQUIDATE_BY_SOLD
        );
        $conditions = $this->previousLiquidateConditions($types, $item);
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    public function findFirstPreviousLiquidateBySoldExpired($deal) {
        $fields = array(
            'AccountingItem.id'
        );
        $types = array(
            AccountingType::LIQUIDATE_BY_SOLD_EXPIRED,
            AccountingType::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL
        );
        $conditions = $this->previousLiquidateConditions($types, $deal);
        return $this->find('first', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    public function updateAsBacNotified($item, $bacResponseId) {
        $this->trace('actualizando como notificado a bac al item contable.', array(
            'accountingItem' => $item,
            'bacResponseId' => $bacResponseId
        ));
        $fields = array(
            'AccountingItem.bac_response_id' => $bacResponseId,
            'AccountingItem.bac_sent' => "'" . date('Y-m-d H:i:s') . "'",
        );
        $conditions = array(
            'AccountingItem.id' => $item['AccountingItem']['id']
        );
        return $this->updateAll($fields, $conditions);
    }

    public function updateAsBacPaymentNotified($item, $bacPaymentId) {
        $this->trace('actualizando como notificado a bac al item contable.', array(
            'accountingItem' => $item,
            'bacPaymentId' => $bacPaymentId
        ));
        $fields = array(
            'AccountingItem.bac_payment_id' => $bacPaymentId,
            'AccountingItem.bac_payment_sent' => "'" . date('Y-m-d H:i:s') . "'",
        );
        $conditions = array(
            'AccountingItem.id' => $item['AccountingItem']['id']
        );
        return $this->updateAll($fields, $conditions);
    }

    public function updateAsDeleted($item_id) {
        $fields = array(
            'AccountingItem.deleted' => 1,
        );
        $conditions = array(
            'AccountingItem.id' => $item_id
        );
        return $this->updateAll($fields, $conditions);
    }

    public function updateItemsAsDeleted($item_ids) {
        $saved = false;
        if (!empty($item_ids)) {
            $list = implode(',', $item_ids);
            $query = " UPDATE `accounting_items` AS `AccountingItem` SET `AccountingItem`.`deleted` = 1 WHERE `AccountingItem`.`id` IN ({$list}) ";
            $saved = $this->query($query);
        }
        return $saved;
    }

    public function findItemsByCalendarAndDeal($calendar_id, $deal_id) {
        $fields = array(
            'AccountingItem.id'
        );
        $conditions = array(
            'AccountingItem.accounting_calendar_id' => $calendar_id,
            'AccountingItem.deal_id' => $deal_id,
        );
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function findById($id) {
        $conditions = array(
            'AccountingItem.id' => $id,
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    private function visibilityConditions($companyId, $calendarId, $accountingTypeList) {
        return array(
            'AccountingItem.model' => self::MODEL_COMPANY,
            'AccountingItem.model_id' => $companyId,
            'AccountingItem.accounting_calendar_id' => $calendarId,
            'AccountingItem.accounting_type' => $accountingTypeList
        );
    }

    function updateVisibilityByCompanyIdAndCalendarId($visibility, $companyId, $calendarId, $accountingTypeList) {
        $fields = array(
            'AccountingItem.is_visible' => $visibility,
        );
        $conditions = $this->visibilityConditions($companyId, $calendarId, $accountingTypeList);
        return $this->updateAll($fields, $conditions);
    }

}
