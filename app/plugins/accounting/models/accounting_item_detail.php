<?php

class AccountingItemDetail extends AccountingAppModel {

    public $name = 'AccountingItemDetail';
    public $actsAs = array(
        'api.SoftDeletable',
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function newItemDetail() {
        return array(
            'AccountingItemDetail' => array(
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'created_by' => 0,
                'modified_by' => null,
                'deleted' => 0,
                'accounting_item_id' => null,
                'deal_user_id' => null
            )
        );
    }

    public function findItemDetailsByItem($item) {
        return $this->find('all', array(
                    'conditions' => array(
                        'AccountingItemDetail.accounting_item_id ' => $item['AccountingItem']['id']
                    ),
                    'recursive' => - 1,
        ));
    }

    public function findCouponsByItem($itemIdList) {
        return $this->find('all', array(
                    'fields' => array(
                        'AccountingItemDetail.deal_user_id',
                    ),
                    'conditions' => array(
                        'AccountingItemDetail.accounting_item_id' => $itemIdList
                    ),
                    'recursive' => -1,
        ));
    }

    function findByItemId($itemId) {
        $fields = array(
            'AccountingItemDetail.deal_user_id'
        );
        $conditions = array(
            'AccountingItemDetail.accounting_item_id' => $itemId,
        );
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
