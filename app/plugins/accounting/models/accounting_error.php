<?php

class AccountingError extends AccountingAppModel {

    public $name = 'AccountingError';
    public $actsAs = array(
        'api.SoftDeletable',
    );

    function describe($message) {
        $descriptions = array(
            'CargoService.listadoProductosVacio' => 'El listado de productos no puede ser vacio',
            'CargoService.portalInexistente' => 'El portal no existe',
            'CargoService.usuarioInexistente' => 'El usuario no existe en el portal',
            'CargoService.clienteInexistente' => 'El cliente no existe para el portal',
            'CargoService.productoInexistente' => 'El producto no existe para ese portal',
            'CargoService.precioProductoInexistente' => 'El precio no existe para ese producto',
            'CargoService.monedaInexistente' => 'La moneda no existe',
            'CargoService.productosDistintasMonedas' => 'Productos en distinta moneda',
            'CargoService.parametrosFaltantes' => 'No se recibieron los parametros obligatorios esperados',
            'CargoService.productosDistintaNaturalezaContable' => 'Productos de distinta naturaleza contable',
            'CargoService.condicionVentaInexistente (new)' => 'La condicion de venta no existe',
            'CargoService.fechaContableFueraRango' => 'Fecha de Contabilización fuera de rango: N-10 y N+10 a la fecha de hoy o Se está contabilizando con una fecha posterior para ese punto de venta',
            'PagoService.portalInexistente' => 'El portal no existe',
            'PagoService.usuarioInexistente' => 'El usuario no existe en el portal',
            'PagoService.clienteInexistente' => 'El cliente no existe para el portal',
            'PagoService.monedaInexistente' => 'La moneda no existe',
            'PagoService.parametrosFaltantes' => 'No se recibieron los parametros obligatorios esperados',
            'PagoService.loteInexistente' => 'El lote no existe',
            'PagoService.loteSinCargos' => 'El lote no cotiene cargos asociados',
            'PagoService.monedaNoCoincide' => 'La moneda no coincide con la de los cargos',
            'PagoService.loteEstadoError' => 'Estado del Lote incongruente',
            'PagoService.montoNoCoincide' => 'El monto no coincide con el del Lote',
            'PagoService.errorGeneracionPago' => 'No se pudo generar el pago',
            'PagoService.comprobanteNoCoincide' => 'El comprobante no corresponde al de los cargos del lote'
        );
        return substr($descriptions[$message], 0, 255);
    }

    function newError($accountingItemId, $method, $message) {
        return array(
            'AccountingError' => array(
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'created_by' => 0,
                'modified_by' => null,
                'deleted' => 0,
                'accounting_item_id' => $accountingItemId,
                'method' => substr($method, 0, 255),
                'message' => substr($message, 0, 255),
                'description' => $this->describe($message)
            )
        );
    }

    function saveError($accountingItemId, $method, $message) {
        $error = $this->newError($accountingItemId, $method, $message);
        $this->create();
        $this->save($error);
    }

    function saveErrorList($accountingItemList, $method, $message) {
        foreach ($accountingItemList as $accountingItem) {
            $this->saveError($accountingItem['AccountingItem']['id'], $method, $message);
        }
    }

}
