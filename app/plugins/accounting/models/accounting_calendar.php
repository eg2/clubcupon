
<?php

class AccountingCalendar extends AccountingAppModel {

    const STATUS_NEW = 'NEW';
    const STATUS_PRE_ACCOUNTED = 'PRE_ACCOUNTED';
    const STATUS_ACCEPTED = 'ACCEPTED';
    const STATUS_REJECTED = 'REJECTED';
    const STATUS_ACCOUNTED = 'ACCOUNTED';

    public $name = 'AccountingCalendar';
    public $actsAs = array(
        'api.SoftDeletable',
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findByTodayExecution() {
        $expression = DboSource::expression('date(now())');
        $conditions = array(
            'date(AccountingCalendar.execution)' => $expression,
            'AccountingCalendar.status' => self::STATUS_NEW
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

    function updateAsPreAccounted($calendar) {
        $fields = array(
            'AccountingCalendar.status' => "'" . self::STATUS_PRE_ACCOUNTED . "'"
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->updateAll($fields, $conditions);
    }

    function updateAsAccounted($calendar) {
        $this->trace('actualizando como contabilizado al calendario.', $calendar);
        $fields = array(
            'AccountingCalendar.status' => "'" . self::STATUS_ACCOUNTED . "'"
        );
        $conditions = array(
            'AccountingCalendar.id' => $calendar['AccountingCalendar']['id']
        );
        return $this->updateAll($fields, $conditions);
    }

    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $parameters = compact('conditions');
        $this->recursive = $recursive;
        $count = $this->find('count', array_merge($parameters, $extra));
        if (isset($extra['group'])) {
            $count = $this->getAffectedRows();
        }
        return $count;
    }

}
