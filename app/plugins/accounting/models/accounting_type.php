
<?php

class AccountingType extends AccountingAppModel {

    public $name = 'AccountingType';
    public $useTable = false;

    const BILL_BY_SOLD_FOR_COMPANY = 'BILL_BY_SOLD_FOR_COMPANY';
    const BILL_BY_SOLD_FOR_FINAL_CONSUMER = 'BILL_BY_SOLD_FOR_FINAL_CONSUMER';
    const BILL_BY_REDEEMED_FOR_COMPANY = 'BILL_BY_REDEEMED_FOR_COMPANY';
    const LIQUIDATE_BY_SOLD = 'LIQUIDATE_BY_SOLD';
    const LIQUIDATE_BY_SOLD_EXPIRED = 'LIQUIDATE_BY_SOLD_EXPIRED';
    const LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL = 'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL';
    const LIQUIDATE_BY_REDEEMED = 'LIQUIDATE_BY_REDEEMED';
    const LIQUIDATE_BY_REDEEMED_EXPIRED = 'LIQUIDATE_BY_REDEEMED_EXPIRED';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    public static function liquidateBySold() {
        return array(
            self::LIQUIDATE_BY_SOLD,
            self::LIQUIDATE_BY_SOLD_EXPIRED,
            self::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL
        );
    }

    public static function liquidateByRedeemed() {
        return array(
            self::LIQUIDATE_BY_REDEEMED,
            self::LIQUIDATE_BY_REDEEMED_EXPIRED
        );
    }

    public static function liquidate() {
        return array(
            self::LIQUIDATE_BY_SOLD,
            self::LIQUIDATE_BY_SOLD_EXPIRED,
            self::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL,
            self::LIQUIDATE_BY_REDEEMED,
            self::LIQUIDATE_BY_REDEEMED_EXPIRED
        );
    }

}
