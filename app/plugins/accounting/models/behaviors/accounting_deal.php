<?php

App::import('Core', 'Behavior');
App::import('Model', 'api.ApiDeal');

class AccountingDealBehavior extends ModelBehavior {

    const ACCOUNTING_DEAL_START_DATE = '2013-03-01';

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function isPaymentBySold(&$Model, $deal) {
        return $deal['Deal']['pay_by_redeemed'] == 0;
    }

    function isPaymentByRedeemed(&$Model, $deal) {
        return $deal['Deal']['pay_by_redeemed'] == 1;
    }

    function isEffectiveDatePassed(&$Model, $deal) {
        $couponExpirationDate = $this->model->couponLastExpirationDate($deal);
        return strtotime($couponExpirationDate) < time();
    }

    function isPendingAccountingEvent(&$Model, $deal) {
        return $deal['Deal']['is_pending_accounting_event'] == 1;
    }

    function isBillable(&$Model, $deal) {
        return $deal['Deal']['commission_percentage'] > 0;
    }

    function isPreviouslyLiquidated(&$Model, $deal) {
        return $deal['Deal']['liquidate_count'] > 0;
    }

    private function isPendingAccountingEventSql($calendar) {
        $notBilledDealUsers = "SELECT 1 FROM deal_users AS DealUser WHERE DealUser.deal_id = Deal.id AND DealUser.is_billed = 0 AND DealUser.deleted = 0 ";
        $dealsWithExpiredDealUsers = " Deal.coupon_expiry_date >= '{$calendar['AccountingCalendar']['since']}' AND Deal.coupon_expiry_date <= '{$calendar['AccountingCalendar']['until']}' ";
        $sql = " (EXISTS ({$notBilledDealUsers}) OR ({$dealsWithExpiredDealUsers})) ";
        return DboSource::expression($sql);
    }

    function findAllByCompanyForAccounting(&$Model, $calendar, $company) {
        $fields = array(
            'Deal.id',
            'Deal.pay_by_redeemed',
            'Deal.is_pending_accounting_event',
            'Deal.coupon_expiry_date',
            'Deal.discounted_price',
            'Deal.commission_percentage',
            'Deal.company_id',
            'Deal.downpayment_percentage',
            'Deal.agreed_percentage',
            'Deal.liquidate_count',
            'Deal.is_variable_expiration',
            'Deal.coupon_duration',
            'Deal.end_date',
            'Deal.coupon_start_date',
        );
        $conditions = array(
            'Deal.company_id' => $company['Company']['id'],
            'Deal.deal_status_id' => array(
                ApiDeal::STATUS_CLOSED,
                ApiDeal::STATUS_TIPPED,
                ApiDeal::STATUS_PAIDTOCOMPANY
            ),
            'Deal.is_end_user' => 0,
            'Deal.start_date >=' => self::ACCOUNTING_DEAL_START_DATE,
            $this->isPendingAccountingEventSql($calendar)
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    function registerAccountingEvent(&$Model, $deal) {
        $fields = array(
            'Deal.is_pending_accounting_event' => 1
        );
        $conditions = array(
            'Deal.id' => $deal['Deal']['id']
        );
        return $this->model->updateAll($fields, $conditions);
    }

    function clearAccountingEvent(&$Model, $deal) {
        $fields = array(
            'Deal.is_pending_accounting_event' => 0
        );
        $conditions = array(
            'Deal.id' => $deal['Deal']['id']
        );
        return $this->model->updateAll($fields, $conditions);
    }

    function incrementLiquidateCount(&$Model, $deal) {
        $fields = array(
            'Deal.liquidate_count' => 'Deal.liquidate_count + 1'
        );
        $conditions = array(
            'Deal.id' => $deal['Deal']['id']
        );
        return $this->model->updateAll($fields, $conditions);
    }

}
