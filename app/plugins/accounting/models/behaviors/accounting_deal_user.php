<?php

App::import('Core', 'Behavior');

class AccountingDealUserBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    private function billedFields() {
        return array(
            'DealUser.is_billed' => 1,
            'DealUser.is_paid' => 1,
            'DealUser.paid_date' => '\'' . date('Y-m-d H:i:s.u') . '\''
        );
    }

    private function notBilledFields() {
        return array(
            'DealUser.is_billed' => 0,
            'DealUser.is_paid' => 0,
            'DealUser.paid_date' => null
        );
    }

    private function dealUserfields() {
        return array(
            'DealUser.deal_id',
            'DealUser.user_id',
            'DealUser.id'
        );
    }

    private function soldConditions($deal, $calendar) {
        return array(
            'DealUser.deleted' => 0,
            'DealUser.deal_id' => $deal['Deal']['id'],
            'DealUser.is_billed' => 0,
            'DealUser.created >=' => $calendar['AccountingCalendar']['since'],
            'DealUser.created <=' => $calendar['AccountingCalendar']['until']
        );
    }

    private function redeemedConditions($deal, $calendar) {
        return array(
            'DealUser.deleted' => 0,
            'DealUser.deal_id' => $deal['Deal']['id'],
            'DealUser.is_used' => 1,
            'DealUser.is_billed' => 0,
            'Redemption.redeemed >=' => $calendar['AccountingCalendar']['since'],
            'Redemption.redeemed <=' => $calendar['AccountingCalendar']['until']
        );
    }

    private function expiredConditions($deal, $calendar) {
        return array(
            'DealUser.deleted' => 0,
            'DealUser.deal_id' => $deal['Deal']['id'],
            'DealUser.is_billed' => 0,
            'DealUser.is_used' => 0,
            'Deal.coupon_expiry_date >=' => $calendar['AccountingCalendar']['since'],
            'Deal.coupon_expiry_date <=' => $calendar['AccountingCalendar']['until']
        );
    }

    private function expiredJoin() {
        return array(
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'conditions' => array(
                    'DealUser.deal_id = Deal.id'
                )
            )
        );
    }

    private function redeemedJoin() {
        return array(
            array(
                'table' => 'redemptions',
                'alias' => 'Redemption',
                'type' => 'inner',
                'conditions' => array(
                    'DealUser.id = Redemption.deal_user_id'
                )
            )
        );
    }

    function findByDateSold(&$Model, $deal, $calendar) {
        $fields = $this->dealUserfields();
        $conditions = $this->soldConditions($deal, $calendar);
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    function findByDateSoldGroupByDeal(&$Model, $deal, $calendar) {
        $fields = array(
            'DealUser.deal_id',
            'COUNT(1) as total_quantity'
        );
        $conditions = $this->soldConditions($deal, $calendar);
        $group = array(
            'DealUser.deal_id'
        );
        $value = $this->model->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'group' => $group
        ));
        return $value;
    }

    function findByDateExpired(&$Model, $deal, $calendar) {
        $fields = $this->dealUserfields();
        $joins = $this->expiredJoin();
        $conditions = $this->expiredConditions($deal, $calendar);
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function findByDateExpiredGroupByUserAndDeal(&$Model, $deal, $calendar) {
        $fields = array(
            'DealUser.deal_id',
            'DealUser.user_id',
            'COUNT(1) as total_quantity'
        );
        $joins = $this->expiredJoin();
        $conditions = $this->expiredConditions($deal, $calendar);
        $group = array(
            'DealUser.deal_id',
            'DealUser.user_id'
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'group' => $group
        ));
    }

    function findByDateRedeemed(&$Model, $deal, $calendar) {
        $fields = $this->dealUserfields();
        $joins = $this->redeemedJoin();
        $conditions = $this->redeemedConditions($deal, $calendar);
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function findByDateRedeemedGroupByDeal(&$Model, $deal, $calendar) {
        $fields = array(
            'DealUser.deal_id',
            'COUNT(1) as total_quantity'
        );
        $joins = $this->redeemedJoin();
        $conditions = $this->redeemedConditions($deal, $calendar);
        $group = array(
            'DealUser.deal_id'
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'group' => $group
        ));
    }

    function updateAllAsNotBilled(&$Model, $dealUserIdList) {
        $success = false;
        if (!empty($dealUserIdList)) {
            $fields = $this->notBilledFields();
            $conditions = array(
                'DealUser.id' => $dealUserIdList
            );
            $success = $this->model->updateAll($fields, $conditions);
        }
        return $success;
    }

    function updateAllAsBilled(&$Model, $dealUserIdList) {
        $success = false;
        if (!empty($dealUserIdList)) {
            $fields = $this->billedFields();
            $conditions = array(
                'DealUser.id' => $dealUserIdList
            );
            $success = $this->model->updateAll($fields, $conditions);
        }
        return $success;
    }

}