<?php

App::import('Core', 'Behavior');

class AccountingCompanyBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    private function excluded() {
        return array(
            'club-cupon',
            'imagena-productos',
            'imagena-fotos',
            'club-cupon-1',
            'tecdia-la-plata',
            'tecdia-caba',
            'tecdia-recoleta',
            'tecdia-nunez',
            'tecdia-obelisco',
            'imagena-productos',
            'imagena-fotos',
            'tecdia-liniers',
            'tecdia-mataderos',
            'tecdia-haedo',
            'tecdia-moron'
        );
    }

    private function isPendingAccountingEventSql($calendar) {
        $notBilledDealUsers = "SELECT 1 FROM deal_users AS DealUser WHERE DealUser.deal_id = Deal.id AND DealUser.is_billed = 0 AND DealUser.deleted = 0 ";
        $dealsWithNotBilledDealUsers = "SELECT 1 FROM deals AS Deal WHERE Deal.start_date >= '2013-03-01' AND Deal.company_id = Company.id AND Deal.deal_status_id IN (5, 6) AND EXISTS ({$notBilledDealUsers})";
        $dealsWithExpiredDealUsers = "SELECT 1 FROM deals AS Deal WHERE Deal.start_date >= '2013-03-01' AND Deal.company_id = Company.id AND Deal.deal_status_id IN (5, 6) AND Deal.coupon_expiry_date >= '{$calendar['AccountingCalendar']['since']}' AND Deal.coupon_expiry_date <= '{$calendar['AccountingCalendar']['until']}' ";
        $sql = " (EXISTS ({$dealsWithNotBilledDealUsers}) OR EXISTS ({$dealsWithExpiredDealUsers})) ";
        return DboSource::expression($sql);
    }

    function findAllForAccounting(&$Model, $calendar) {
        $fields = array(
            'Company.id'
        );
        $conditions = array(
            'not' => array(
                'Company.slug' => $this->excluded()
            ),
            $this->isPendingAccountingEventSql($calendar)
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    function registerAccountingEvent(&$Model, $company) {
        $fields = array(
            'Company.is_pending_accounting_event' => 1
        );
        $conditions = array(
            'Company.id' => $company['Company']['id']
        );
        $this->model->updateAll($fields, $conditions);
    }

    function clearAccountingEvent(&$Model, $company) {
        $fields = array(
            'Company.is_pending_accounting_event' => 0
        );
        $conditions = array(
            'Company.id' => $company['Company']['id']
        );
        $this->model->updateAll($fields, $conditions);
    }

    function findAllWithVisibleItemsByCalendarId(&$Model, $calendarId) {
        $fields = array(
            'Company.id',
            'Company.name',
            'IF(SIGN(LOCATE(\'SOLD\', AccountingItem.accounting_type)) = 1, \'Vendido\', \'Redimido\') AS deal_modality',
            'AccountingItem.accounting_calendar_id',
            'MIN(AccountingItem.is_visible) AS is_visible'
        );
        $joins = array(
            array(
                'table' => 'accounting_items',
                'alias' => 'AccountingItem',
                'type' => 'inner',
                'conditions' => array(
                    'Company.id = AccountingItem.model_id'
                )
            )
        );
        $conditions = array(
            'AccountingItem.accounting_calendar_id' => $calendarId,
            'AccountingItem.model' => 'Company',
            'AccountingItem.accounting_type' => array(
                'LIQUIDATE_BY_SOLD',
                'LIQUIDATE_BY_SOLD_EXPIRED',
                'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',
                'LIQUIDATE_BY_REDEEMED',
                'LIQUIDATE_BY_REDEEMED_EXPIRED'
            )
        );
        $group = array(
            'Company.id',
            'Company.name',
            'IF(SIGN(LOCATE(\'SOLD\', AccountingItem.accounting_type)) = 1, \'Vendido\', \'Redimido\')',
            'AccountingItem.accounting_calendar_id'
        );
        return $this->model->find('all', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'group' => $group,
                    'recursive' => -1
        ));
    }

}
