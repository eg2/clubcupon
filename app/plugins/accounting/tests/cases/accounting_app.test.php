<?php

App::import('', 'accounting.AccountingApp');
App::import('', 'api.ApiAppTestCase');

class AccountingAppTestCase extends ApiAppTestCase {
  public $autoFixtures = false;

  function startCase() {
    parent::startCase();
  }

  function testShouldDefined() {
    $this->assertTrue(class_exists("AccountingApp"));
  }

}
?>
