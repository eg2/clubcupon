<?php
  App::import('Model', 'accounting.AccountingItem');
  App::import('Model', 'accounting.AccountingType');
  App::import('Model', 'accounting.AccountingCalendar');
  App::import('', 'api.ApiAppTestCase');

  class AccountingItemTestCase extends ApiAppTestCase {
    var $fixtures = array(
      'plugin.accounting.accounting_item',
      'plugin.accounting.accounting_calendar',
    );

    function startCase() {
      parent::startCase();
      $this->AccountingItem = ClassRegistry::init('AccountingItem');
    }

    function testShouldExist() {
      $this->assertTrue(
        is_object($this->AccountingItem),
        'No se pudo instanciar un Item'
      );
    }

   function testFindBillNotNotifiedToBac() {
      $expectedItemIds = array('1', '3');
      $itemsToNotify = $this->AccountingItem->findBillsNotNotifiedToBac(
        array(
          'AccountingItem.accounting_calendar_id' => 1
        ));

      $this->assertEqual(
        $expectedItemIds,
        $this->extractAccountingItemIds($itemsToNotify),
        'No coinciden los Item Ids de facturas pendientes de notificar a bac. '.json_encode(array(
        'ids esperados'=>$expectedItemIds,
        'ids'=>$this->extractAccountingItemIds($itemsToNotify)
      )));
    }

    private function extractAccountingItemIds($items) {
      $ids = array();
      foreach ($items as $item) {
        $ids[] = $item['AccountingItem']['id'];
      }
      return $ids;
    }

  }
?>
