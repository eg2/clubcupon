<?php
App::import('Model', 'accounting.AccountingCalendar');
App::import('', 'api.ApiAppTestCase');

class AccountingCalendarTestCase extends ApiAppTestCase {
  var $fixtures = array(
    'plugin.accounting.accounting_calendar',
  );

  function startCase() {
    parent::startCase();
    $this->AccountingCalendar = ClassRegistry::init('AccountingCalendar');
  }

  function testShouldExist() {
    $this->assertTrue(is_object($this->AccountingCalendar));
  }

  function testUpdateAsAccounted() {
    $calendarBefore = $this->AccountingCalendar->findById('1');
    $this->AccountingCalendar->updateAsAccounted($calendarBefore);
    $calendarAfter = $this->AccountingCalendar->findById('1');

    $this->assertEqual(
      AccountingCalendar::STATUS_ACCOUNTED,
      $calendarAfter['AccountingCalendar']['status'],
      "El estado del calendario no es el correcto. ".json_encode(array(
        'estado esperado'=>AccountingCalendar::STATUS_ACCOUNTED,
        'estado real'=>$calendarAfter['AccountingCalendar']['id']
      ))
    );
  }
}
?>
