<?php

App::import('Model', 'accounting.AccountingCalendar');
App::import('Service', 'accounting.AccountingBacNotifierService');
App::import('', 'api.ApiAppTestCase');

class AccountingBacNotifierServiceTestCase extends ApiAppTestCase {
  var $fixtures = array(
    'plugin.accounting.accounting_calendar',
    'plugin.accounting.accounting_item',
    'plugin.api.api_attachment',
    'plugin.api.api_branch_deal',
    'plugin.api.api_city',
    'plugin.api.api_company',
    'plugin.api.api_deal',
    'plugin.api.api_deal_external',
    'plugin.api.api_payment_method',
    'plugin.api.api_payment_setting',
    'plugin.api.api_payment_type',
    'plugin.api.api_user',
    'plugin.api.api_user_profile',
  );

  function startCase() {
    parent::startCase();
    $this->AccountingItem = ClassRegistry::init('AccountingItem');
    $this->AccountingCalendar = ClassRegistry::init('AccountingCalendar');
    $this->AccountingBacNotifierService = ClassRegistry::init('AccountingBacNotifierService');
  }

  function testShouldExist() {
    $this->assertTrue(
      class_exists("AccountingBacNotifierService"),
      "La clase AccountingBacNotifierService debe existir.");
    $this->assertTrue(
      is_object($this->AccountingBacNotifierService),
      "Se debe poder instanciar un AccountingBacNotifierService object.");
  }

  /**
   * Que debo Verificar:
   * - que ids debe procesar por cliente bill pending to bac.
   * - que ids debe procesar por company bill to bac.
   * - que calendarios debe avisar que los termino de procesar.
   */
  function testNotifyAllBillsToBac() {
    $expectedUserBillIds = array( '3', '7', '9');
    $expectedCompanyBillIds = array( '1', '5');
    $expectedUserIdsUpdatedInBac = array('1', '1'); // el mismo usuario por cada calendario.
    $expectedCompanyIdsUpdatedInBac = array( );
    $expectedCalendarIdsUpdateAsAccounted = array(1, 2);

    $accountingCalendarMock = ClassRegistry::init('AccountingCalendarMock');
    $this->AccountingBacNotifierService->AccountingCalendar = $accountingCalendarMock;
    $accountingItemBacNotifierFactoryMock =
      new AccountingItemBacNotifierFactoryMock();
    $this->AccountingBacNotifierService->AccountingItemBacNotifierFactory =
      $accountingItemBacNotifierFactoryMock;

    // - que ids debe procesar por cliente bill pending to bac.
    $this->AccountingBacNotifierService->notifyToBac();

    $this->ApiLogger->trace('resultados.', array(
      'userBillsNotifyToBac' => $accountingItemBacNotifierFactoryMock->userBillsNotifyToBac,
      'companyBillsNotifyToBac' => $accountingItemBacNotifierFactoryMock->companyBillsNotifyToBac,
      'companyUpdateInBac' => $accountingItemBacNotifierFactoryMock->companysUpdateInBac,
      'usersUpdateInBac' => $accountingItemBacNotifierFactoryMock->usersUpdateInBac,
    ));


    $this->assertEqual(
      $expectedCalendarIdsUpdateAsAccounted,
      $accountingCalendarMock->calendarIdsUpdateAsAccounted,
      'Calendarios que deben ser actualizados como Procesados. '.json_encode(array(
        'ids esperados'=>$expectedCalendarIdsUpdateAsAccounted,
        'ids procesados'=>$accountingCalendarMock->calendarIdsUpdateAsAccounted,
      )));

    $this->assertEqual(
      $expectedUserBillIds,
      $accountingItemBacNotifierFactoryMock->userBillsNotifyToBac,
      'Las facturas de usuario que deben ser notificadas a bac. '.json_encode(array(
        'ids esperados'=>$expectedUserBillIds,
        'ids notificados'=>$accountingItemBacNotifierFactoryMock->userBillsNotifyToBac,
      )));

    $this->assertEqual(
      $expectedCompanyBillIds,
      $accountingItemBacNotifierFactoryMock->companyBillsNotifyToBac,
      'Las facturas de compania que deben ser notificadas a bac. '.json_encode(array(
        'ids esperados'=>$expectedCompanyBillIds,
        'ids notificados'=>$accountingItemBacNotifierFactoryMock->companyBillsNotifyToBac,
      )));

    $this->assertEqual(
      $expectedUserIdsUpdatedInBac,
      $accountingItemBacNotifierFactoryMock->usersUpdateInBac,
      'Los Usuarios que deben ser actualizados en bac. '.json_encode(array(
        'ids esperados'=>$expectedUserIdsUpdatedInBac,
        'ids notificados'=>$accountingItemBacNotifierFactoryMock->usersUpdateInBac,
      )));

    $this->assertEqual(
      $expectedCompanyIdsUpdatedInBac,
      $accountingItemBacNotifierFactoryMock->companysUpdateInBac,
      'Las Companias que deben ser acutualizadas en bac. '.json_encode(array(
        'ids esperados'=>$expectedCompanyIdsUpdatedInBac,
        'ids notificados'=>$accountingItemBacNotifierFactoryMock->companysUpdateInBac,
      )));
  }

}

class AccountingCalendarMock extends AccountingCalendar {
  public $name = 'AccountingCalendar';
  public $alias = 'AccountingCalendar';

  public $calendarIdsUpdateAsAccounted = array();

  function updateAsAccounted($calendar) {
    $this->debug('Actualizacion a Contabilizado del calendario Adaptada para el Test.');
    $this->calendarIdsUpdateAsAccounted[] = $calendar['AccountingCalendar']['id'];
    parent::updateAsAccounted($calendar);
    $this->trace(
      'ids de calendarios contabilizados al momento.',
      $this->calendarIdsUpdateAsAccounted
    );
  }
}

class AccountingItemBacNotifierDecorator extends AccountingAppService
  implements AccountingItemBacNotifier {

  function __construct($accountingItemBacNotifier, $tracker) {
    $this->AccountingItem = new AccountingItem();
    $this->accountingItemBacNotifier = $accountingItemBacNotifier;
    $this->tracker = $tracker;
  }

  public function notifyToBac($item, $refreshPartnerInBac) {
    if ($this->AccountingItem->isForCompany($item)) {
      $this->tracker->companyBillsNotifyToBac[] = $item['AccountingItem']['id'];
    } else {
      $this->tracker->userBillsNotifyToBac[] = $item['AccountingItem']['id'];
    }
    $this->accountingItemBacNotifier->notifyToBac($item, $refreshPartnerInBac);
  }

  public function refreshPartnerInBac($item) {
    if ($this->AccountingItem->isForCompany($item)) {
      $this->tracker->companyUpdateInBac[] = $item['AccountingItem']['model_id'];
    } else {
      $this->tracker->usersUpdateInBac[] = $item['AccountingItem']['model_id'];
    }
    $this->accountingItemBacNotifier->refreshPartnerInBac($item);
  }
}

class AccountingItemBacNotifierFactoryMock extends AccountingItemBacNotifierFactory {
  public $userBillsNotifyToBac = array();
  public $companyBillsNotifyToBac = array();
  public $companysUpdateInBac = array();
  public $usersUpdateInBac = array();
  public $accountingItemBacNotifier;

  public function getBacNotifier($item) {
    return new AccountingItemBacNotifierDecorator(
      parent::getBacNotifier($item),
      $this
    );
  }

}

?>
