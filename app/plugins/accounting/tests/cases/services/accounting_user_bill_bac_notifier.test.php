<?php

App::import('Service', 'accounting.AccountingBacNotifier', array('file' => 'accounting_bac_notifier.php'));
App::import('Service', 'accounting.AccountingUserBillBacNotifier');
App::import('', 'api.ApiAppTestCase');

class AccountingUserBillBacNotifierTestCase extends ApiAppTestCase {
  var $fixtures = array(
    'plugin.accounting.accounting_item',
    'plugin.accounting.accounting_calendar',
    'plugin.accounting.company',
    'plugin.accounting.user',
  );


  function startCase() {
    parent::startCase();
  }

  function testShouldExist() {
    $this->assertTrue(class_exists("AccountingUserBillBacNotifier"));
    $this->AccountingUserBillBacNotifierTest = ClassRegistry::init('AccountingUserBillBacNotifier');
    $this->assertTrue(is_object($this->AccountingUserBillBacNotifierTest));
  }

}
?>
