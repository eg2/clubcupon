<?php
App::import('', 'api.ApiAppTestCase');
App::import('Service', 'accounting.AccountingBacService');
App::import('Component', 'api.ApiBac');
App::import('Model', 'accounting.AccountingCalendar');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'api.ApiUser');

class AccountingBacServiceTestCase extends ApiAppTestCase {
  var $fixtures = array(
    'plugin.accounting.accounting_calendar',
    'plugin.accounting.accounting_item',
    'plugin.api.api_attachment',
    'plugin.api.api_branch_deal',
    'plugin.api.api_city',
    'plugin.api.api_company',
    'plugin.api.api_deal',
    'plugin.api.api_deal_external',
    'plugin.api.api_payment_method',
    'plugin.api.api_payment_setting',
    'plugin.api.api_payment_type',
    'plugin.api.api_user',
    'plugin.api.api_user_profile',
  );

  function startCase() {
    parent::startCase();
    $this->AccountingBacService = ClassRegistry::init('AccountingBacService');
    $this->AccountingItem = ClassRegistry::init('AccountingItem');
    $this->ApiUser = ClassRegistry::init('ApiUser');
  }

  function testShouldExist() {
    $this->assertTrue(
      class_exists("AccountingBacService"),
      "La clase AccountingBacService debe existir.");
    $this->assertTrue(
      is_object($this->AccountingBacService),
      "Se debe poder instanciar un AccountingBacService object.");
  }

  function testCreateUserBill() {
    $accountingItemId = 1;
    $accountingItem = $this->AccountingItem->findById($accountingItemId);
    $user = $this->ApiUser->findById($accountingItem['AccountingItem']['model_id']);

    $this->ApiLogger->trace('params', array(
      'accountingItem' => $accountingItem,
      'user' => $user,
    ));

    $expectedBacId = 999;
    $expectedCrearCargosBacParams = array (
      'idPortal' => 18,
      'idUsuarioPortal' => $accountingItem['AccountingItem']['model_id'],
      'porcentajeComision' => $accountingItem['AccountingItem']['billing_commission_percentage'],
      'condicionVenta' => Configure::read('BAC.condicion_venta_contado'),
      'productos'  => array (
        array(
          'idProducto' => Configure::read('BAC.id_producto_debito'),
          'cantidad' => $accountingItem['AccountingItem']['total_quantity'],
          'precio' => $accountingItem['AccountingItem']['discounted_price'],
          'idMoneda' => Configure::read('BAC.pesos'),
          'idComprobantePortal' => $accountingItem['AccountingItem']['deal_id'],
        ),
      ),
    );

    $apiBacMock = ClassRegistry::init('ApiBacMock');
    $apiBacMock->mockReturn = $expectedBacId;
    $this->AccountingBacService->ApiBac = $apiBacMock;
    $this->AccountingBacService->createBill($accountingItem);

    $this->assertEqual(
      1, count($apiBacMock->crearCargos),
      "Debe llamar al crearCargos una vez.".json_encode(
        array('llamadas' => count($apiBacMock->crearCargos)))
      );

    ksort($expectedCrearCargosBacParams);
    ksort($apiBacMock->crearCargos[0]);
    $this->assertEqual(
      $expectedCrearCargosBacParams,
      $apiBacMock->crearCargos[0],
      "Debe llamar crearCargos con los parametros esperados y solo ellos.".json_encode(
        array(
          'Esperados' => $expectedCrearCargosBacParams,
          'Reales' => $apiBacMock->crearCargos[0],
          'Parametros que estan en esperados y son distintos o no estan en los reales' =>  array_diff_assoc(
            $expectedCrearCargosBacParams, $apiBacMock->crearCargos[0]),
          'Parametros que estan en reales y son distintos o no estan en los esperados' =>  array_diff_assoc(
            $apiBacMock->crearCargos[0], $expectedCrearCargosBacParams),
        )
      )
    );

    $accountingItemAfter = $this->AccountingItem->findById($accountingItemId);
    $this->assertEqual(
      $expectedBacId,
      $accountingItemAfter['AccountingItem']['bac_response_id'],
      "El bac id debe guardarse en el campo AccountingItem.bac_response_id.".json_encode(array(
        'bac_id esperado' => $expectedBacId,
        'bac_id real' => $accountingItemAfter['AccountingItem']['bac_response_id'],))
      );
  }

  function xtestCreateCompanyBill() {
    $accountingItemId = 2;
    $accountingItem = $this->AccountingItem->findById($accountingItemId);

    $this->ApiLogger->trace('params', array(
      'accountingItem' => $accountingItem,
      'user' => $user,
    ));

    $expectedBacId = 999;
    $expectedCrearCargosBacParams = array (
      'idPortal' => 18,
      'idUsuarioPortal' => $accountingItem['AccountingItem']['model_id'],
      'porcentajeComision' => $accountingItem['AccountingItem']['billing_commission_percentage'],
      'condicionVenta' => Configure::read('BAC.condicion_venta_contado'),
      'productos'  => array (
        array(
          'idProducto' => Configure::read('BAC.id_producto_comision_cmd'),
          'cantidad' => $accountingItem['AccountingItem']['total_quantity'],
          'precio' => $accountingItem['AccountingItem']['discounted_price'],
          'idMoneda' => Configure::read('BAC.pesos'),
          'idComprobantePortal' => $accountingItem['AccountingItem']['deal_id'],
        ),
      ),
    );
  }

}

class ApiBacMock extends ApiBacComponent {

  public $crearCargos = array();
  public $mockReturn = null;

  public function crearCargos($params) {
    $this->crearCargos[] = $params;
    //return $this->mockReturn;
    return parent::crearCargos($params);
  }
}
?>
