<?php
App::import(
  'service',
  'accounting.AccountingBacNotifier',
  array('file' => 'accounting_bac_notifier.php')
);
App::import('', 'api.ApiAppTestCase');

class AccountingBacNotifierTestCase extends ApiAppTestCase {
  public $autoFixtures = false;

  function startCase() {
    parent::startCase();
  }

  function testShouldDefined() {
    $this->assertTrue(interface_exists("AccountingBacNotifier"));
  }

}

?>
