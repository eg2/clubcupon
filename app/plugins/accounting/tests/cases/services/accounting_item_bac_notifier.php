<?php
App::import(
  'service',
  'accounting.AccountingItemBacNotifier',
  array('file' => 'accounting_item_bac_notifier.php')
);
App::import('', 'api.ApiAppTestCase');

class AccountingItemBacNotifierTestCase extends ApiAppTestCase {
  public $autoFixtures = false;

  function startCase() {
    parent::startCase();
  }

  function testShouldDefined() {
    $this->assertTrue(interface_exists("AccountingItemBacNotifier"));
  }

}

?>
