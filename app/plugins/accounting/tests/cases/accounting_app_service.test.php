<?php

App::import('', 'accounting.AccountingAppService');
App::import('', 'api.ApiAppTestCase');

class AccountingAppServiceTestCase extends ApiAppTestCase {
  public $autoFixtures = false;

  function startCase() {
    parent::startCase();
  }

  function testShouldDefined() {
    $this->assertTrue(class_exists("AccountingAppService"));
  }

}
?>
