<?php

class AccountingCalendarFixture extends CakeTestFixture {

	var $name = 'AccountingCalendar';
//  var $import = array(
//    'model' => 'accounting.AccountingCalendar',
//    'records' => true,
//    'table' => 'accounting_calendars'
//  );
  //var $import = 'accounting.AccountingCalendar';

  var $fields = array(
		'id' => array('type' => 'integer', 'key' => 'primary'),
    'status' => array('type' => 'string', 'null' => false),
		'created' => 'datetime',
		'updated' => 'datetime',
    'delete' => 'string',
  );

	var $records = array(
    array(
      'id' => 1,
      'status' => AccountingCalendar::STATUS_ACCEPTED,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'delete' => '0',
    ),
    array(
      'id' => 2,
      'status' => AccountingCalendar::STATUS_ACCEPTED,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'delete' => '0',
    ),
    array(
      'id' => 3,
      'status' => AccountingCalendar::STATUS_NEW,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'delete' => '0',
    ),
    array(
      'id' => 4,
      'status' => AccountingCalendar::STATUS_ACCOUNTED,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'delete' => '0',
    ),
	);

}
?>
