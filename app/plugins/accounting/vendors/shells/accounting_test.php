<?php

App::import('Service', 'accounting.AccountingBacNotifierService');
App::import('Service', 'accounting.AccountingBacService');

class AccountingTestShell extends Shell {

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->AccountingBacNotifierService = ClassRegistry::init('AccountingBacNotifierService');
        $this->AccountingBacService = ClassRegistry::init('AccountingBacService');
    }

    function main() {
        $items[] = array(
            'AccountingItem' => array(
                'id' => '28339',
                'created' => '2013-05-07 00:02:10',
                'modified' => '2013-05-07 00:02:10',
                "created_by" => "0",
                "modified_by" => null,
                "deleted" => "0",
                "accounting_calendar_id" => "11",
                "accounting_item_id" => null,
                "accounting_type" => "BILL_BY_SOLD_FOR_FINAL_CONSUMER",
                "model" => "User",
                "model_id" => "1480",
                "deal_id" => "40178",
                "total_quantity" => "1",
                "total_amount" => "68.06",
                "bac_sent" => null,
                "bac_response_id" => null,
                "discounted_price" => "375.00",
                "billing_commission_percentage" => "15.00",
                "billing_iva_percentage" => "21",
                "liquidating_is_first" => "0",
                "liquidating_guarantee_fund_percentage" => "0",
                "liquidating_agreed_percentage" => "0",
                "liquidating_invoiced_amount" => "0.00",
                "liquidating_iibb_percentage" => "0"
            )
        );
        echo "[" . __METHOD__ . "] Begin.\n";
        $bacinfo = array();
        try {
            $bacinfo = $this->AccountingBacService->createBill($items[0]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        echo "\nVer Log and search for createCargosParams.\n";
        echo "[" . __METHOD__ . "] End.\n";
    }

}
