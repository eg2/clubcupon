<?php

App::import('Component', 'accounting.AccountingService');

class AccountingShell extends Shell {

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->AccountingService = new AccountingServiceComponent();
    }

    function main() {
        echo "[" . __METHOD__ . "] Begin.\n";
        $this->AccountingService->billAndLiquidate();
        echo "[" . __METHOD__ . "] End.\n";
    }

}
