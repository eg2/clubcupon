<?php

App::import('Service', 'accounting.AccountingBacNotifierService');

class AccountingBacNotifyShell extends Shell {

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->AccountingBacNotifierService = ClassRegistry::init('AccountingBacNotifierService');
    }

    function main() {
        echo "[" . __METHOD__ . "] Begin.\n";
        $this->AccountingBacNotifierService->notifyToBac();
        echo "[" . __METHOD__ . "] End.\n";
    }

}
