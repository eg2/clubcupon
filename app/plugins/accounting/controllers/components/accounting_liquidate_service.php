<?php

App::import('Behavior', 'accounting.AccountingDeal');
App::import('Component', 'accounting.AccountingSolverFactory');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'accounting.AccountingType');
App::import('Model', 'api.ApiDeal');

class AccountingLiquidateServiceComponent extends ApiBaseComponent {

    public $name = 'AccountingLiquidateService';

    function __construct() {
        parent::__construct('accounting');
        $this->AccountingItem = new AccountingItem();
        $this->AccountingType = new AccountingType();
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('AccountingDeal');
        $this->AccountingSolverFactory = new AccountingSolverFactoryComponent();
    }

    private function liquidate($type, $calendar, $deal, $billItem) {
        $solver = $this->AccountingSolverFactory->getLiquidateSolver($type);
        $liquidateItem = $solver->newLiquidate($calendar, $deal, $billItem);
        if (!$this->ApiDeal->isPreviouslyLiquidated($deal)) {
            $liquidateItem['AccountingItem']['liquidating_is_first'] = 1;
        }
        $liquidateItem['AccountingItem']['total_amount'] = $solver->liquidateAmount($deal, $billItem);
        $this->AccountingItem->create();
        if ($this->AccountingItem->isRegistrable($liquidateItem)) {
            $this->AccountingItem->save($liquidateItem);
            $this->ApiDeal->incrementLiquidateCount($deal);
        }
        return $liquidateItem;
    }

    function liquidateBySold($calendar, $deal, $billItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal,
            $billItem
        ));
        return $this->liquidate(AccountingType::LIQUIDATE_BY_SOLD, $calendar, $deal, $billItem);
    }

    function liquidateBySoldExpired($calendar, $deal, $billItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal,
            $billItem
        ));
        return $this->liquidate(AccountingType::LIQUIDATE_BY_SOLD_EXPIRED, $calendar, $deal, $billItem);
    }

    function liquidateBySoldExpiredWithoutBill($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        return $this->liquidate(AccountingType::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL, $calendar, $deal, null);
    }

    function liquidateByRedeemed($calendar, $deal, $billItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal,
            $billItem
        ));
        return $this->liquidate(AccountingType::LIQUIDATE_BY_REDEEMED, $calendar, $deal, $billItem);
    }

    function liquidateByRedeemedExpired($calendar, $deal, $billItem) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal,
            $billItem
        ));
        return $this->liquidate(AccountingType::LIQUIDATE_BY_REDEEMED_EXPIRED, $calendar, $deal, $billItem);
    }

}