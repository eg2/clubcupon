<?php

App::import('Core', 'Component');

class AccountingMathComponent extends Component {

    const IVA = 0.21;
    const PRECISION = 2;

    function __construct() {
        parent::__construct();
    }

    function iva() {
        return self::IVA;
    }

    function round($number) {
        return round($number, self::PRECISION);
    }

}