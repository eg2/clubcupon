<?php

App::import('Component', 'accounting.AccountingMath');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingItem');

abstract class AccountingLiquidateSolverComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct();
        $this->AccountingItem = new AccountingItem();
        $this->AccountingMath = new AccountingMathComponent();
    }

    function isGuaranteeFundsAlreadyReturned($deal) {
        $item = $this->AccountingItem->findFirstPreviousLiquidateBySoldExpired($deal);
        return !empty($item);
    }

    function totalGuaranteeFundsHeldInPreviousLiquidations($deal) {
        $totalGuaranteeFunds = 0;
        $items = $this->AccountingItem->findAllPreviousLiquidateBySold($deal);
        if (!empty($items)) {
            foreach ($items as $item) {
                $totalGuaranteeFunds+= $item['AccountingItem']['total_quantity'] * $item['AccountingItem']['discounted_price'] * ($item['AccountingItem']['liquidating_guarantee_fund_percentage'] / 100);
            }
        }
        return $totalGuaranteeFunds;
    }

    abstract function newLiquidate($calendar, $deal, $billItem);

    abstract function liquidateAmount($deal, $billItem);
}