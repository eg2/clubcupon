<?php

App::import('Behavior', 'accounting.AccountingCompany');
App::import('Behavior', 'accounting.AccountingDeal');
App::import('Component', 'accounting.AccountingBillService');
App::import('Component', 'accounting.AccountingLiquidateService');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingCalendar');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');

class AccountingServiceComponent extends ApiBaseComponent {

    public $name = 'AccountingService';

    function __construct() {
        parent::__construct('accounting');
        $this->AccountingCalendar = new AccountingCalendar();
        $this->AccountingBillService = new AccountingBillServiceComponent();
        $this->AccountingLiquidateService = new AccountingLiquidateServiceComponent();
        $this->ApiCompany = new ApiCompany();
        $this->ApiCompany->Behaviors->attach('AccountingCompany');
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('AccountingDeal');
    }

    function registerAccountingEvent($deal) {
        try {
            if (!$this->ApiDeal->isPendingAccountingEvent($deal)) {
                $this->ApiDeal->registerAccountingEvent($deal);
                $company = array(
                    'Company' => array(
                        'id' => $deal['Deal']['company_id']
                    )
                );
                $this->ApiCompany->registerAccountingEvent($company);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    private function processPaymentBySold($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        $billItem = $this->AccountingBillService->billBySoldForCompany($calendar, $deal);
        $isEffectiveDatePassed = $this->ApiDeal->isEffectiveDatePassed($deal);
        if (!empty($billItem)) {
            if (!$isEffectiveDatePassed) {
                $this->AccountingLiquidateService->liquidateBySold($calendar, $deal, $billItem);
            } else {
                $this->AccountingLiquidateService->liquidateBySoldExpired($calendar, $deal, $billItem);
            }
        } else if ($isEffectiveDatePassed) {
            $this->AccountingLiquidateService->liquidateBySoldExpiredWithoutBill($calendar, $deal);
        }
    }

    private function procesPaymentByRedeemed($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        $billItem = $this->AccountingBillService->billByRedeemedForCompany($calendar, $deal);
        $isEffectiveDatePassed = $this->ApiDeal->isEffectiveDatePassed($deal);
        if ($isEffectiveDatePassed) {
            $this->AccountingBillService->billBySoldForFinalConsumer($calendar, $deal);
        }
        if (!empty($billItem)) {
            if (!$isEffectiveDatePassed) {
                $this->AccountingLiquidateService->liquidateByRedeemed($calendar, $deal, $billItem);
            } else {
                $this->AccountingLiquidateService->liquidateByRedeemedExpired($calendar, $deal, $billItem);
            }
        }
    }

    private function processDeals($calendar, $deals) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            count($deals)
        ));
        foreach ($deals as $deal) {
            if ($this->ApiDeal->isPaymentBySold($deal)) {
                $this->processPaymentBySold($calendar, $deal);
            } else {
                $this->procesPaymentByRedeemed($calendar, $deal);
            }
            $this->ApiDeal->clearAccountingEvent($deal);
        }
    }

    private function processCompanies($calendar, $companies) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            count($companies)
        ));
        foreach ($companies as $company) {
            $deals = $this->ApiDeal->findAllByCompanyForAccounting($calendar, $company);
            if (!empty($deals)) {
                $this->processDeals($calendar, $deals);
            }
            $this->ApiCompany->clearAccountingEvent($company);
        }
    }

    private function processCalendar($calendar) {
        $this->ApiLogger->info(__FUNCTION__, $calendar);
        $companies = $this->ApiCompany->findAllForAccounting($calendar);
        if (!empty($companies)) {
            $this->processCompanies($calendar, $companies);
        }
        $this->AccountingCalendar->updateAsPreAccounted($calendar);
    }

    function billAndLiquidate() {
        $this->ApiLogger->info(__FUNCTION__, NULL);
        $calendar = $this->AccountingCalendar->findByTodayExecution();
        if (!empty($calendar)) {
            $this->processCalendar($calendar);
        }
    }

}