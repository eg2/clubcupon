<?php

App::import('Component', 'accounting.AccountingLiquidateSolver');
App::import('Component', 'accounting.AccountingMath');

class AccountingLiquidateByRedeemedSolverComponent extends AccountingLiquidateSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function liquidateAmount($deal, $billItem) {
        $amount = ($billItem['AccountingItem']['total_quantity'] * $billItem['AccountingItem']['discounted_price']) - $billItem['AccountingItem']['total_amount'];
        return $this->AccountingMath->round($amount);
    }

    function newLiquidate($calendar, $deal, $billItem) {
        return $this->AccountingItem->newLiquidateByRedeemed($calendar, $deal, $billItem);
    }

}