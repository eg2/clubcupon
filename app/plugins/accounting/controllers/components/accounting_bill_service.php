<?php

App::import('Behavior', 'accounting.AccountingDeal');
App::import('Behavior', 'accounting.AccountingDealUser');
App::import('Component', 'accounting.AccountingSolverFactory');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'accounting.AccountingItemDetail');
App::import('Model', 'accounting.AccountingType');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealUser');

class AccountingBillServiceComponent extends ApiBaseComponent {

    public $name = 'AccountingBillService';

    function __construct() {
        parent::__construct('accounting');
        $this->AccountingItem = new AccountingItem();
        $this->AccountingItemDetail = new AccountingItemDetail();
        $this->AccountingType = new AccountingType();
        $this->ApiDeal = new ApiDeal();
        $this->ApiDeal->Behaviors->attach('AccountingDeal');
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiDealUser->Behaviors->attach('AccountingDealUser');
        $this->AccountingSolverFactory = new AccountingSolverFactoryComponent();
    }

    private function updateDealUsersAsBilled($dealUsers) {
        foreach ($dealUsers as $dealUser) {
            $dealUserIdList[] = $dealUser['DealUser']['id'];
        }
        $this->ApiDealUser->updateAllAsBilled($dealUserIdList);
    }

    private function bill($type, $calendar, $deal) {
        $item = array();
        if ($this->ApiDeal->isBillable($deal)) {
            $solver = $this->AccountingSolverFactory->getBillSolver($type);
            $dealUsersGrouped = $solver->findDealUsersGrouped($deal, $calendar);
            if (!empty($dealUsersGrouped)) {
                foreach ($dealUsersGrouped as $dealUserGrouped) {
                    $item = $solver->newBill($deal, $calendar, $dealUserGrouped);
                    $item['AccountingItem']['total_amount'] = $solver->billAmount($dealUserGrouped, $deal);
                    $this->AccountingItem->create();
                    $this->AccountingItem->save($item);
                    $item['AccountingItem']['id'] = $this->AccountingItem->id;
                    $dealUsers = $solver->findDealUsers($deal, $calendar);
                    if (!empty($dealUsers)) {
                        $this->registerItemDetails($dealUsers, $item);
                        $this->updateDealUsersAsBilled($dealUsers);
                    }
                }
            }
        }
        return $item;
    }

    private function registerItemDetails($dealUsers, $item) {
        foreach ($dealUsers as $dealUser) {
            $itemDetail = $this->AccountingItemDetail->newItemDetail();
            $itemDetail['AccountingItemDetail']['accounting_item_id'] = $item['AccountingItem']['id'];
            $itemDetail['AccountingItemDetail']['deal_user_id'] = $dealUser['DealUser']['id'];
            $this->AccountingItemDetail->create();
            $this->AccountingItemDetail->save($itemDetail);
        }
    }

    function billBySoldForCompany($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        return $this->bill(AccountingType::BILL_BY_SOLD_FOR_COMPANY, $calendar, $deal);
    }

    function billByRedeemedForCompany($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        return $this->bill(AccountingType::BILL_BY_REDEEMED_FOR_COMPANY, $calendar, $deal);
    }

    function billBySoldForFinalConsumer($calendar, $deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $calendar,
            $deal
        ));
        return $this->bill(AccountingType::BILL_BY_SOLD_FOR_FINAL_CONSUMER, $calendar, $deal);
    }

}