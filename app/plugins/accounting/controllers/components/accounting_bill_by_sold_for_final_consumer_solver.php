<?php

App::import('Component', 'accounting.AccountingBillSolver');

class AccountingBillBySoldForFinalConsumerSolver extends AccountingBillSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function findDealUsersGrouped($deal, $calendar) {
        return $this->ApiDealUser->findByDateExpiredGroupByUserAndDeal($deal, $calendar);
    }

    function findDealUsers($deal, $calendar) {
        return $this->ApiDealUser->findByDateExpired($deal, $calendar);
    }

    function newBill($deal, $calendar, $dealUser) {
        $iva = $this->AccountingMath->iva();
        return $this->AccountingItem->newBillBySoldForFinalConsumer($deal, $calendar, $dealUser, $iva);
    }

    function billAmount($dealUser, $deal) {
        $amount = $dealUser[0]['total_quantity'] * $deal['Deal']['discounted_price'];
        return $this->AccountingMath->round($amount);
    }

}