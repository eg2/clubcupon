<?php

App::import('Behavior', 'accounting.AccountingDealUser');
App::import('Component', 'accounting.AccountingMath');
App::import('Component', 'api.ApiBase');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'api.ApiDealUser');

abstract class AccountingBillSolverComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct();
        $this->AccountingItem = new AccountingItem();
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiDealUser->Behaviors->attach('AccountingDealUser');
        $this->AccountingMath = new AccountingMathComponent();
    }

    abstract function findDealUsersGrouped($deal, $calendar);

    abstract function findDealUsers($deal, $calendar);

    abstract function newBill($deal, $calendar, $dealUser);

    abstract function billAmount($dealUser, $deal);
}