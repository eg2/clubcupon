<?php

App::import('Component', 'accounting.AccountingLiquidateSolver');

class AccountingLiquidateBySoldExpiredWithoutBillSolverComponent extends AccountingLiquidateSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function liquidateAmount($deal, $billItem) {
        $amount = 0;
        if (!$this->isGuaranteeFundsAlreadyReturned($deal)) {
            $amount = $this->totalGuaranteeFundsHeldInPreviousLiquidations($deal);
        }
        return $this->AccountingMath->round($amount);
    }

    function newLiquidate($calendar, $deal, $billItem) {
        return $this->AccountingItem->newLiquidateBySoldExpiredWithoutBill($calendar, $deal);
    }

}