<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'accounting.AccountingBillBySoldForCompanySolver');
App::import('Component', 'accounting.AccountingBillBySoldForFinalConsumerSolver');
App::import('Component', 'accounting.AccountingBillByRedeemedForCompanySolver');
App::import('Component', 'accounting.AccountingLiquidateBySoldSolver');
App::import('Component', 'accounting.AccountingLiquidateBySoldExpiredSolver');
App::import('Component', 'accounting.AccountingLiquidateBySoldExpiredWithoutBillSolver');
App::import('Component', 'accounting.AccountingLiquidateByRedeemedSolver');

class AccountingSolverFactoryComponent extends ApiBaseComponent {

    public function __construct() {
        $this->billBySoldForCompanySolver = new AccountingBillBySoldForCompanySolver();
        $this->billBySoldForFinalConsumerSolver = new AccountingBillBySoldForFinalConsumerSolver();
        $this->billByRedeemedForCompanySolver = new AccountingBillByRedeemedForCompanySolver();
        $this->liquidateBySoldSolver = new AccountingLiquidateBySoldSolverComponent();
        $this->liquidateBySoldExpiredSolver = new AccountingLiquidateBySoldExpiredSolverComponent();
        $this->liquidateBySoldExpiredWithoutBillSolver = new AccountingLiquidateBySoldExpiredWithoutBillSolverComponent();
        $this->liquidateByRedeemedSolver = new AccountingLiquidateByRedeemedSolverComponent();
        $this->liquidateByRedeemedExpiredSolver = new AccountingLiquidateByRedeemedSolverComponent();/*Es igual al anterior*/
    }

    function getBillSolver($type) {
        switch ($type) {
            case AccountingType::BILL_BY_SOLD_FOR_COMPANY:
                return $this->billBySoldForCompanySolver;
            case AccountingType::BILL_BY_SOLD_FOR_FINAL_CONSUMER:
                return $this->billBySoldForFinalConsumerSolver;
            case AccountingType::BILL_BY_REDEEMED_FOR_COMPANY:
                return $this->billByRedeemedForCompanySolver;
            default:
                return null;
        }
    }

    function getLiquidateSolver($type) {
        switch ($type) {
            case AccountingType::LIQUIDATE_BY_SOLD:
                return $this->liquidateBySoldSolver;
            case AccountingType::LIQUIDATE_BY_SOLD_EXPIRED:
                return $this->liquidateBySoldExpiredSolver;
            case AccountingType::LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL:
                return $this->liquidateBySoldExpiredWithoutBillSolver;
            case AccountingType::LIQUIDATE_BY_REDEEMED:
                return $this->liquidateByRedeemedSolver;
            case AccountingType::LIQUIDATE_BY_REDEEMED_EXPIRED:
                return $this->liquidateByRedeemedExpiredSolver;
            default:
                return null;
        }
    }

}