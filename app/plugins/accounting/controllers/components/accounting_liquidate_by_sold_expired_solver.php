<?php

App::import('Component', 'accounting.AccountingLiquidateSolver');

class AccountingLiquidateBySoldExpiredSolverComponent extends AccountingLiquidateSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function liquidateAmount($deal, $billItem) {
        $amount = ($billItem['AccountingItem']['total_quantity'] * $billItem['AccountingItem']['discounted_price']) - $billItem['AccountingItem']['total_amount'];
        if (!$this->isGuaranteeFundsAlreadyReturned($deal)) {
            $previousLiquidationsAmount = $this->totalGuaranteeFundsHeldInPreviousLiquidations($deal);
            $amount+= $previousLiquidationsAmount;
        }
        return $this->AccountingMath->round($amount);
    }

    function newLiquidate($calendar, $deal, $billItem) {
        return $this->AccountingItem->newLiquidateBySoldExpired($calendar, $deal, $billItem);
    }

}