<?php

App::import('Model', 'accounting.AccountingCalendar');
App::import('Model', 'accounting.AccountingItem');
App::import('Model', 'accounting.AccountingItemDetail');
App::import('Model', 'DealUser');
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiDealUser');
App::import('Behavior', 'accounting.AccountingDealUser');

class AccountingCalendarServiceComponent extends ApiBaseComponent {

    public $name = 'AccountingCalendarService';

    function __construct() {
        parent::__construct('accounting');
        $this->AccountingItem = new AccountingItem();
        $this->AccountingItemDetail = new AccountingItemDetail();
        $this->DealUser = new DealUser();
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiDealUser->Behaviors->attach('AccountingDealUser');
    }

    public function excludeAccountingItem($item) {
        $item_ids = array();
        $items = $this->AccountingItem->findItemsByCalendarAndDeal($item['AccountingItem']['accounting_calendar_id'], $item['AccountingItem']['deal_id']);
        foreach ($items as $value) {
            $item_ids[] = $value['AccountingItem']['id'];
        }
        $this->AccountingItem->updateItemsAsDeleted($item_ids);
    }

    public function cancelAccountingItem($item) {
        $deal_user_ids = array();
        $item_ids = array();
        $item_ids[] = $item['AccountingItem']['id'];
        if (isset($item['AccountingItem']['accounting_item_id']) && !empty($item['AccountingItem']['accounting_item_id'])) {
            $item_ids[] = $item['AccountingItem']['accounting_item_id'];
        }
        $deal_users = $this->AccountingItemDetail->findCouponsByItem($item_ids);
        foreach ($deal_users as $deal_user) {
            $deal_user_ids[] = $deal_user["AccountingItemDetail"]['deal_user_id'];
        }
        $rowsUpdated = $this->DealUser->updateCouponsAsNotBilled($deal_user_ids);
        unset($item_ids);
        $item_ids = array();
        $items = $this->AccountingItem->findItemsByCalendarAndDeal($item['AccountingItem']['accounting_calendar_id'], $item['AccountingItem']['deal_id']);
        foreach ($items as $value) {
            $item_ids[] = $value['AccountingItem']['id'];
        }
        $this->AccountingItem->updateItemsAsDeleted($item_ids);
    }

    function cancel($itemId) {
        $item = $this->AccountingItem->findById($itemId);
        if (!empty($item)) {
            $items = $this->AccountingItem->findItemsByCalendarAndDeal($item['AccountingItem']['accounting_calendar_id'], $item['AccountingItem']['deal_id']);
            foreach ($items as $itemToCancel) {
                $itemDetails = $this->AccountingItemDetail->findByItemId($itemToCancel['AccountingItem']['id']);
                if (!empty($itemDetails)) {
                    foreach ($itemDetails as $itemDetail) {
                        $dealUserIdList[] = $itemDetail['AccountingItemDetail']['deal_user_id'];
                    }
                    $this->ApiDealUser->updateAllAsNotBilled($dealUserIdList);
                }
                $this->AccountingItem->updateAsDeleted($itemToCancel['AccountingItem']['id']);
            }
        }
    }

}
