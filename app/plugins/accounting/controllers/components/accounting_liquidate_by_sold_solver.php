<?php

App::import('Component', 'accounting.AccountingLiquidateSolver');

class AccountingLiquidateBySoldSolverComponent extends AccountingLiquidateSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function liquidateAmount($deal, $billItem) {
        $amount = ($billItem['AccountingItem']['total_quantity'] * $billItem['AccountingItem']['discounted_price']) - $billItem['AccountingItem']['total_amount'] - (($billItem['AccountingItem']['total_quantity'] * $billItem['AccountingItem']['discounted_price']) * ($deal['Deal']['downpayment_percentage'] / 100));
        return $this->AccountingMath->round($amount);
    }

    function newLiquidate($calendar, $deal, $billItem) {
        return $this->AccountingItem->newLiquidateBySold($calendar, $deal, $billItem);
    }

}