<?php

App::import('Component', 'accounting.AccountingBillSolver');

class AccountingBillBySoldForCompanySolver extends AccountingBillSolverComponent {

    function __construct() {
        parent::__construct();
    }

    function findDealUsersGrouped($deal, $calendar) {
        return $this->ApiDealUser->findByDateSoldGroupByDeal($deal, $calendar);
    }

    function findDealUsers($deal, $calendar) {
        return $this->ApiDealUser->findByDateSold($deal, $calendar);
    }

    function newBill($deal, $calendar, $dealUser) {
        $iva = $this->AccountingMath->iva();
        return $this->AccountingItem->newBillBySoldForCompany($deal, $calendar, $dealUser, $iva);
    }

    function billAmount($dealUser, $deal) {
        $iva = $this->AccountingMath->iva();
        $amount = $dealUser[0]['total_quantity'] * $deal['Deal']['discounted_price'] * ($deal['Deal']['commission_percentage'] / 100) * (1 + $iva);
        return $this->AccountingMath->round($amount);
    }

}