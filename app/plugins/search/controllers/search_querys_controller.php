<?php

App::import('Component', 'web.WebViewDataCreator');
App::import('Model', 'web.WebCity');
App::import('Model', 'Deal');
App::import('Model', 'DealCategory');

class SearchQuerysController extends SearchAppController {

    public $name = 'SearchQuerys';
    private $cityName = '';
    private $message = '';
    private $scV;
    public $components = array(
        'RequestHandler',
        'Auth',
        'Cookie',
        'search.SearchService',
        'search.SearchUrlObjectBuilder',
        'search.SearchParamBuilder',
        'search.SearchTermService'
    );

    public function makesearch() {
       try {
            if ($this->isScv) {
                $this->scV = $this->SearchUrlObjectBuilder->recoverUrlObject($_GET['scV']);
            } else {
                $this->SearchTermService->searchDataSave($_GET['q'], $this->Auth->user('id'));
            }
            $this->evalReset();
            $this->evalAddFacet();
            $this->eval_SubtractFacet_Page_Sort();
            $this->evalSetQuery();
            $this->evalCurrentCity();
            $this->evalSetFilterUserCity();
            $resultsSolr = $this->getSolrResult();
            $data = $this->getDataObjectForView($resultsSolr);
            $dataObj = json_decode($data);
            $this->set('city_name', $this->cityName);
            $this->set('city_s_slug', $this->scV['User']['CitySlug']);
            $this->set('search', $this->scV);
            $this->set('resultsSolr', $resultsSolr);
            $this->set('scvToQString', $this->SearchUrlObjectBuilder->getUrlObject($this->scV));
            $this->set('data', $data);
            $this->set('isFirstPahLevelIDCategoryNow', Deal::isCategoryNow($dataObj->vData->hFacetFieldFirstPathLevelID));
            $this->render('/search_querys/mainview');
        } catch (Exception $e) {
            $this->ApiLogger->notice("Error no clasificado en buscador.", array(
                'params' => $this->params,
                'error' => $e->getMessage()
            ));
            $this->ApiLogger->error("Error no clasificado en buscador.", array(
                'params' => $this->params,
                'error' => $e->getMessage()
            ));
        }
    }

    private function getSolrResult() {
        $showAllDeals = false;
        $disableLocation = false;
        $out = false;
        do {
            $serviceRespObj = $this->executeSearch();
            if (is_object($serviceRespObj)) {
                $resultsSolr = json_decode($serviceRespObj->getRawResponse());
                $data = array(
                    'numFound' => $resultsSolr->response->numFound,
                    'status' => $resultsSolr->responseHeader->status,
                    'QTime' => $resultsSolr->responseHeader->QTime
                );
                $this->ApiLogger->info('Solr response:', $data);
            } else {
                $this->ApiLogger->error('Ping a Solr Service no responde.');
            }
            list($showAllDeals, $disableLocation, $out) = $this->evalSolrResult($resultsSolr, $showAllDeals, $disableLocation, $out);
        } while (!$out);
        return $resultsSolr;
    }

    private function isSetAnyHFacetField($fq) {
        $resp = false;
        $cont = 0;
        while ($cont < count($fq) && !$resp) {
            $pos = strpos($fq[$cont], 'deal_category_path_l');
            if (!($pos === false)) {
                $resp = true;
            }
            $cont++;
        }
        return $resp;
    }

    private function getDataObjectForView($resultsSolr) {
        $fq = $resultsSolr->responseHeader->params->fq;
        $isSetAnyHFacetField = $this->isSetAnyHFacetField($fq);
        if ($isSetAnyHFacetField && $resultsSolr->response->numFound > 0) {
            $doc = $resultsSolr->response->docs[0];
            $firstPathLevelID = $doc->deal_category_l1_id;
        }
        $params = array(
            'viewType' => WebViewDataCreatorComponent::SOLR_RESULTS,
            'citySlug' => $this->scV['User']['CitySlug'],
            'lastGeoCitySlug' => $this->Cookie->read('geo_city_slug'),
            'actualPage' => $this->scV['Page']['Actual'],
            'userEmail' => $this->Auth->user('email'),
            'userType' => $this->Auth->user('user_type_id'),
            'userID' => $this->Auth->user('id'),
            'message' => null,
            'resultsSolr' => $resultsSolr,
            'url' => $this->params['url']['url'],
            'isSetHFacetField' => $isSetAnyHFacetField,
            'hFacetFieldFirstPathLevelID' => $firstPathLevelID
        );
        $dataCreator = new WebViewDataCreatorComponent();
        $data = $dataCreator->createData($params);
        return json_encode($data);
    }

    private function evalSolrResult($resultsSolr, $showAllDeals, $disableLocation, $out) {
        if ($resultsSolr->response->numFound == 0) {
            if (isset($resultsSolr->spellcheck->suggestions->correctlySpelled)) {
                if (!$resultsSolr->spellcheck->suggestions->correctlySpelled) {
                    $this->set('suggestion', $resultsSolr->spellcheck->suggestions->collation);
                    $this->set('searched', $resultsSolr->responseHeader->params->q);
                }
            }
            $this->ApiLogger->info('--- Sin resultados en la búsqueda ---');
            $this->evalReset(true);
            $this->scV['Query']['Q'] = '*:*';
            $this->set('message', $GLOBALS['messages']['No Result']);
            $this->message = $GLOBALS['messages']['No Result'];
            if (!$showAllDeals) {
                $cityfiltered = $this->evalSetFilterUserCity();
                if ($cityfiltered) {
                    $this->message.= $GLOBALS['messages']['ShowResultsFor'] . $cityfiltered . ':';
                } else {
                    $this->message.= $GLOBALS['messages']['ShowAllResults'] . ':';
                }
                $showAllDeals = true;
            } else {
                $this->message.= $GLOBALS['messages']['ShowAllResults'] . ':';
                if (!$disableLocation) {
                    $disableLocation = true;
                } else {
                    $out = true;
                }
            }
        } else {
            $out = true;
        }
        if ($this->message != '') {
            $this->Session->setFlash($this->message);
        }
        return array(
            $showAllDeals,
            $disableLocation,
            $out
        );
    }

    private function evalCurrentCity() {
        if (isset($_GET['currentCitySlug'])) {
            $slugCity = $_GET['currentCitySlug'];
            $this->scV['User']['CitySlug'] = $slugCity;
        } else {
            if (isset($this->scV['User']['CitySlug'])) {
                $slugCity = $this->scV['User']['CitySlug'];
            } else {
                $slugCity = 'ciudad-de-buenos-aires';
                $this->scV['User']['CitySlug'] = $slugCity;
            }
        }
        $cityObj = new WebCity();
        $city = $cityObj->findBySlug($slugCity);
        $this->cityName = $city['WebCity']['name'];
    }

    private function evalSetFilterUserCity() {
        if ($_GET['fromSearchPg'] == 'no' || (!$this->isQ && !$this->isScv)) {
            $cityObj = new WebCity();
            $city = $cityObj->findBySlug($this->scV['User']['CitySlug']);
            if (!$city['WebCity']['is_group']) {
                $fieldCity = 'city_name_no_group';
            } else {
                return null;
            }
            $this->scV['Flts']['FctsSel'][$fieldCity] = $city['WebCity']['name'];
            return $city['WebCity']['name'];
        } else {
            return null;
        }
    }

    private function executeSearch() {
        $obj = new stdClass;
        $obj->query = $this->scV['Query']['Q'];
        $obj->firstItem = ($this->scV['Page']['Actual'] - 1) * $GLOBALS['dealsQuantityPerPage'];
        $obj->firstItem+= (($obj->firstItem == 0) ? 0 : $GLOBALS['extraDealsFirstPage']);
        $obj->quantityItems = $GLOBALS['dealsQuantityPerPage'];
        $obj->quantityItems+= (($obj->firstItem == 0) ? $GLOBALS['extraDealsFirstPage'] : 0);
        $obj->allParams = $this->SearchParamBuilder->getParams($this->scV);
        $this->ApiLogger->info('Solr query:', $obj);
        return $this->SearchService->getServiceResponse($obj);
    }

    private function evalSetQuery() {
        $isPage = (isset($this->scV['Page']['Actual']));
        if ($this->isQ) {
            $this->scV['Query']['Q'] = $this->SearchParamBuilder->sanitizeQueryQParam($_GET['q']);
            $this->scV['Page']['Actual'] = 1;
            $this->scV['Sort']['Option'] = reset($GLOBALS['searchOrderOptions']);
        }
        if ((!$this->isQ && !$this->isScv) || !$isPage) {
            $this->scV['Page']['Actual'] = 1;
            $this->scV['Sort']['Option'] = reset($GLOBALS['searchOrderOptions']);
        }
        if (!$this->isQ && !$this->isScv) {
            $this->scV['Query']['Q'] = '*:*';
        }
    }

    private function evalReset($forceReset = false) {
        if ($this->isQ || $forceReset) {
            unset($this->scV['Query']['Q']);
            unset($this->scV['Flts']['FctsSel']);
            unset($this->scV['Flts']['FctsHierSel']);
            unset($this->scV['Flts']['FacetsRSel']);
            unset($this->scV['Flts']['FctsCRMin']);
            unset($this->scV['Flts']['FctsCRMax']);
            $this->scV['Page']['Actual'] = 1;
            $this->scV['Sort']['Option'] = reset($GLOBALS['searchOrderOptions']);
        }
        if ($this->isResetHierField) {
            $this->scV['Query']['Q'] = '*:*';
            unset($this->scV['Flts']);
            $field = $_GET['resetHierField'];
            $facet = $_GET['resetHierFacet'];
            $this->scV['Flts']['FctsHierSel'][$field] = $facet;
            $this->scV['Page']['Actual'] = 1;
            $this->scV['Sort']['Option'] = reset($GLOBALS['searchOrderOptions']);
        }
    }

    private function evalAddFacet() {
        if ($this->isSetFacetField) {
            $field = $_GET['setFacetField'];
            $this->scV['Flts']['FctsSel'][$field] = $_GET['setFacet'];
        }
        if ($this->isSetRFacetField) {
            $field = $_GET['setRFacetField'];
            $this->scV['Flts']['FacetsRSel'][$field] = $_GET['setRBase'];
        }
        if ($this->isSetCRFacet) {
            $this->scV['Flts']['FctsCRMin'] = $_GET['CRangeMin'];
            $this->scV['Flts']['FctsCRMax'] = $_GET['CRangeMax'];
        }
        if ($this->isSetHFacetField) {
            $field = $_GET['setHFacetField'];
            $this->scV['Flts']['FctsHierSel'][$field] = $_GET['setFacet'];
        }
    }

    private function evalSubFctItem($fctAction, $fctType = '') {
        if (isset($this->params['url'][$fctAction])) {
            if ($fctAction == 'subFctRangeCustom') {
                unset($this->scV['Flts']['FctsCRMin']);
                unset($this->scV['Flts']['FctsCRMax']);
            } else {
                $urlFctAction = $this->params['url'][$fctAction];
                unset($this->scV['Flts'][$fctType][$urlFctAction]);
                if (count($this->scV['Flts'][$fctType]) == 0) {
                    unset($this->scV['Flts'][$fctType]);
                }
            }
            $this->scV['Page']['Actual'] = 1;
        }
    }

    private function eval_SubtractFacet_Page_Sort() {
        $this->evalSubFctItem('subFct', 'FctsSel');
        $this->evalSubFctItem('subFctRange', 'FacetsRSel');
        $this->evalSubFctItem('subFctRangeCustom');
        $this->evalSubFctItem('subFctHier', 'FctsHierSel');
        if (isset($this->scV['Flts']) && count($this->scV['Flts']) == 0) {
            unset($this->scV['Flts']);
        }
        if ($this->isPage) {
            $this->scV['Page']['Actual'] = $_GET['page'];
        }
        if ($this->isSetAnyFacetField) {
            $this->scV['Page']['Actual'] = 1;
        }
        if ($this->isQSort) {
            $this->scV['Sort']['Option'] = $_GET['qSort'];
            $this->scV['Page']['Actual'] = 1;
        }
    }

}
