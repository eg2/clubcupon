<?php

require_once realpath(dirname(__FILE__) . '/../..') . ConstSolrSearch::SLRPC_PATH;

class SearchServiceComponent {

    private $solrClient;

    function __construct() {
        $this->solrClient = new Apache_Solr_Service(ConstSolrSearch::HOST, ConstSolrSearch::PORT, ConstSolrSearch::PATH);
    }

    public function getServiceResponse($params) {
        if ($this->solrClient->ping()) {
            $response = $this->solrClient->search($params->query, $params->firstItem, $params->quantityItems, $params->allParams, Apache_Solr_Service::METHOD_POST);
            return $response;
        } else {
            return false;
        }
    }

}
