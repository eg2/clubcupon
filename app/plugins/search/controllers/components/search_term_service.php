<?php

App::import('Model', 'search.SearchTerm');
App::import('Sanitize');

class SearchTermServiceComponent {

    public function __construct() {
        $this->SearchTerm = & new SearchTerm();
    }

    public function searchDataSave($dataText, $idUser = NULL) {
        $dataSearch = $this->SearchTerm->newSearchTerm();
        $dataSearch["SearchTerm"]["terms"] = Sanitize::html($dataText, array(
                    'remove' => true
        ));
        $dataSearch["SearchTerm"]["user_id"] = $idUser;
        $this->SearchTerm->save($dataSearch);
    }

}
