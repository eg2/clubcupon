<?php

class SearchUrlObjectBuilderComponent {

    private $scV;
    private $recParams = array(
        'city' => array(
            'User',
            'CitySlug',
            true,
            false,
            false
        ),
        'page' => array(
            'Page',
            'Actual',
            false,
            false,
            false
        ),
        'query' => array(
            'Query',
            'Q',
            true,
            false,
            false
        ),
        'sort' => array(
            'Sort',
            'Option',
            true,
            false,
            false
        ),
        'min' => array(
            'Flts',
            'FctsCRMin',
            false,
            false,
            false
        ),
        'max' => array(
            'Flts',
            'FctsCRMax',
            false,
            false,
            false
        ),
        'facet' => array(
            'Flts',
            'FctsSel',
            false,
            true,
            false
        ),
        'cat' => array(
            'Flts',
            'FctsHierSel',
            false,
            false,
            true
        )
    );

    function __construct() {
        
    }

    private function evalKey($key, $key2, $conv, $isString = false, $testEmpty = false) {
        $resp = '';
        if (isset($this->scV[$key][$key2])) {
            if (!$isString) {
                $resp.= $conv . $this->scV[$key][$key2];
            } else {
                if ($testEmpty && $this->scV[$key][$key2] == '') {
                    return;
                }
                $resp.= $conv . $this->rmCh($this->scV[$key][$key2]);
            }
        }
        return $resp;
    }

    public function getUrlObject($scv) {
        $this->scV = $scv;
        $strUrl = '';
        $strUrl.= $this->evalKey('User', 'CitySlug', '-city--', true);
        $strUrl.= $this->evalKey('Page', 'Actual', '-page--');
        $strUrl.= $this->evalKey('Query', 'Q', '-query--', true);
        $strUrl.= $this->evalKey('Sort', 'Option', '-sort--', true, true);
        $strUrl.= $this->evalKey('Flts', 'FctsCRMin', '-min--');
        $strUrl.= $this->evalKey('Flts', 'FctsCRMax', '-max--');
        if (isset($this->scV['Flts']['FctsSel'])) {
            foreach ($this->scV['Flts']['FctsSel'] as $field => $facet) {
                $strUrl.= '-facet--' . $this->rmCh($field) . "." . $this->rmCh($facet);
            }
        }
        if (isset($this->scV['Flts']['FctsHierSel'])) {
            foreach ($this->scV['Flts']['FctsHierSel'] as $field => $facet) {
                $strUrl.= '-cat--' . substr($field, -1) . "." . $this->rmCh($facet);
            }
        }
        $enc = urlencode($strUrl);
        return $enc;
    }

    private function evalRecKey($key, $value) {
        $r = $this->recParams;
        $hfNoLevel = $GLOBALS['HierFacetsFieldNoLevel'];
        if ($r[$key][3]) {
            $sv = explode(".", $value);
            $this->scV[$r[$key][0]][$r[$key][1]][$this->rstCh($sv[0])] = $this->rstCh($sv[1]);
            return;
        }
        if ($r[$key][4]) {
            $sv = explode(".", $value);
            $this->scV[$r[$key][0]][$r[$key][1]][$hfNoLevel . $sv[0]] = $this->rstCh($sv[1]);
            return;
        }
        if (!$r[$key][2]) {
            $this->scV[$r[$key][0]][$r[$key][1]] = $value;
        } else {
            $this->scV[$r[$key][0]][$r[$key][1]] = $this->rstCh($value);
        }
    }

    public function recoverUrlObject($urlScv) {
        $strUrl = urldecode($urlScv);
        $i = 1;
        $end = false;
        do {
            list($i, $key, $value, $end) = $this->getUrlNextKeyValue($strUrl, $i);
            $this->evalRecKey($key, $value);
        } while (!$end);
        return $this->scV;
    }

    private function getUrlNextKeyValue($strUrl, $i) {
        $end = false;
        $onKey = true;
        $key = '';
        $value = '';
        $i--;
        do {
            $i++;
            $char = substr($strUrl, $i, 1);
            if ($char != '-') {
                if ($onKey) {
                    $key.= $char;
                } else {
                    $value.= $char;
                }
            } else if (substr($strUrl, ($i + 1), 1) == '-') {
                $onKey = false;
            }
            $ev1 = ($char != '-');
            $ev2 = (substr($strUrl, ($i + 1), 1) == '-');
            $ev3 = (substr($strUrl, ($i - 1), 1) == '-');
            $ev4 = ($i < strlen($strUrl));
        } while (($ev1 || $ev2 || $ev3) && $ev4);
        if ($i >= strlen($strUrl)) {
            $end = true;
        }
        $i++;
        return array(
            $i,
            $key,
            $value,
            $end
        );
    }

    private function rstCh($str) {
        $str = str_replace('~', '-', $str);
        $str = str_replace('Âº', '.', $str);
        $str = str_replace('oooo', '*:*', $str);
        return $str;
    }

    private function rmCh($str) {
        $str = str_replace('-', '~', $str);
        $str = str_replace('.', 'Âº', $str);
        $str = str_replace('*:*', 'oooo', $str);
        return $str;
    }

}
