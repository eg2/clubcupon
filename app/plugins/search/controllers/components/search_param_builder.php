<?php

class SearchParamBuilderComponent {

    private $scV;

    function __construct() {
        
    }

    public function getParams($scv) {
        $this->scV = $scv;
        $params = $this->getDynamicFacetsParams();
        $params = array_merge($params, $GLOBALS['fctsRParams']);
        $params = array_merge($params, array(
            'sort' => $this->scV['Sort']['Option']
        ));
        $params = array_merge($params, $GLOBALS['facetsParams']);
        if ($GLOBALS['paramsDebug']) {
            echo "<pre>";
            print_r($params);
            echo "</pre>";
        }
        return $params;
    }

    private function getDynamicFacetsParams() {
        $fFacets = $this->getFacetsFilters('FctsSel');
        $fFacetsHier = $this->getFacetsFilters('FctsHierSel');
        $fFacetsRange = $this->getFacetsFilters('FacetsRSel');
        $fFacetsCRange = $this->getFacetsFilters('FacetsRangeCustom');
        $fFacetsResult = array();
        $fFacetsResult = array_merge($fFacetsResult, $fFacets);
        $fFacetsResult = array_merge($fFacetsResult, $fFacetsHier);
        $fFacetsResult = array_merge($fFacetsResult, $fFacetsRange);
        $fFacetsResult = array_merge($fFacetsResult, $fFacetsCRange);
        $fFacetsResult = array_merge($fFacetsResult, $GLOBALS['extraFilters']);
        return array(
            'fq' => $fFacetsResult
        );
    }

    private function getFacetsFilters($facetType) {
        $facets = array();
        if ($facetType == 'FacetsRangeCustom' && isset($this->scV['Flts']['FctsCRMin'])) {
            $min = $this->scV['Flts']['FctsCRMin'];
            $max = $this->scV['Flts']['FctsCRMax'];
            $facets[] = $GLOBALS['facetsCustomRangeField'] . ':[' . $min . ' TO ' . $max . ']';
        }
        if (isset($this->scV['Flts'][$facetType])) {
            foreach ($this->scV['Flts'][$facetType] as $field => $facet) {
                switch ($facetType) {
                    case 'FctsHierSel':
                        $facets[] = $field . ':"' . $facet . '"';
                        break;

                    case 'FctsSel':
                        if ($field == 'city_name_no_group' && ($facet != 'Ciudad de Buenos Aires')) {
                            $facets[] = '(' . $field . ':"' . $facet . '" OR  city_is_group:"true")';
                        } else {
                            $facets[] = $field . ':"' . $facet . '"';
                        }
                        break;

                    case 'FacetsRSel':
                        $aux = $facet + $GLOBALS['fctsRParams']['facet.range.gap'] - 1;
                        $facets[] = $field . ':[' . $facet . ' TO ' . $aux . ']';
                        break;

                    default:
                        break;
                }
            }
        }
        return $facets;
    }

    public function sanitizeQueryQParam($string) {
        if ($string != '*:*') {
            $match = array(
                '\\',
                '+',
                '-',
                '&',
                '|',
                '!',
                '(',
                ')',
                '{',
                '}',
                '[',
                ']',
                '^',
                '~',
                '*',
                '?',
                ':',
                '"',
                ';',
                '/'
            );
            $string = str_replace($match, '', $string);
        }
        return $string;
    }

}
