<?php

class SearchBridgesController extends SearchAppController {

    public $name = 'SearchBridges';
    public $components = array(
        'RequestHandler',
        'Security',
        'Auth',
        'XAjax',
        'DebugKit.Toolbar',
        'Cookie',
        'UserMenu',
        'Pdf',
        'search.SearchService'
    );

    public function makesearch() {
        try {
            $cResp = $this->executeAsynchSearch($this->params['form']['term']);
            $result = $this->getTerms(json_decode($cResp->getRawResponse()));
            $this->layout = 'ajax';
            $this->autoLayout = false;
            $this->autoRender = false;
            $this->header('Content-Type: application/json');
            echo json_encode($result);
        } catch (Exception $e) {
            Debugger::log(sprintf($e->getMessage()), LOG_ERR);
        }
    }

    private function executeAsynchSearch($term) {
        $obj = new stdClass;
        $obj->query = '*.*';
        $obj->firstItem = 0;
        $obj->quantityItems = 0;
        $obj->allParams = array(
            'indent' => 'on',
            'facet' => 'true',
            'facet.field' => $GLOBALS['autocomplete']['FacetField'],
            'facet.prefix' => $term,
            'fq' => $GLOBALS['extraFilters'][0],
            'wt' => 'json'
        );
        return $this->SearchService->getServiceResponse($obj);
    }

    private function getTerms($data) {
        $facetF = $data->facet_counts->facet_fields;
        $arr = $facetF->deal_name_autocomplete_terms;
        $resp = array();
        foreach (array_keys(get_object_vars($arr)) as $key) {
            $resp[] = $key;
        }
        return $resp;
    }

}
