<?php

class SearchTerm extends AppModel {

    public $name = 'SearchTerm';
    public $recursive = -1;

    function newSearchTerm() {
        return array(
            'SearchTerm' => array(
                'created' => date("c"),
                'modified' => date("c"),
                'created_by' => 0,
                'modified_by' => null,
                'deleted' => 0,
                'user_id' => null,
                'terms' => null
            )
        );
    }

}
