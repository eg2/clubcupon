<?php

    $auxVarsEnc = urlencode($scvToQString);
    $actualPage = $search['Page']['Actual'];
    $total = (int) $resultsSolr->response->numFound;

    if (isset($suggestion)) {
        $link = $html->link(
            $suggestion,
            array(
                'plugin'=>'search',
                'controller' => 'search_querys',
                'action' => 'makesearch',
                '?' => array(
                    'q' => $suggestion,
                    'qr' => 'reset',
                    'scV' => $scvToQString
                )
            ),
            array(
                'title' => 'Inicio',
                'class' => ''
            )
        );
        echo '<div class="maybe">' . $GLOBALS['messages']['Maybe1'] . '<b>' . $searched . '</b>' . $GLOBALS['messages']['Maybe2'] . $link . '?</div>';
    }
	
	$destacadowidth  = Configure::read('thumb_size.search_plugin_main_deal.width');
	$destacadoheight = Configure::read('thumb_size.search_plugin_main_deal.height');
	$listDealwidth = Configure::read('thumb_size.searchdeals_big_thumb.width');
	$listDealheight = Configure::read('thumb_size.searchdeals_big_thumb.height');
?>



<?php
	$firstElem = true;
    foreach ($dealList as $deal) {
		if (($actualPage == 1) && $firstElem){
			echo $this->element(
				'dealItemDestacado',
				array(
					'slug' => $slug,
					'deal' => $deal,
					'width'=>$destacadowidth,
					'height'=>$destacadoheight
				)
			);
			echo '<section id="cupones" class="categorias">'; 
		$firstElem = false;
		}else {
			if($firstElem):
				echo '<section id="cupones" class="categorias">';
				$firstElem = false;
			endif;
	        echo $this->element(
	            'dealItem', 
	            array(
					'slug' => $slug,
	                'deal' => $deal,
	            	'width'=>$listDealwidth,
	            	'height'=>$listDealheight
	            )
	        );
		}
    }
    if(!$firstElem){
		echo '</section>';
	}
?>	
<div class="pagination-holder clearfix">
   	<div class="pagination light-pagination"></div>
</div>

<?php 
    $bool = 'false';
    if ($total && $total > $GLOBALS['dealsQuantityPerPage'] + $GLOBALS['extraDealsFirstPage']) {
        $bool = 'true';
    }
?>
<script type="text/javascript">
if (<?php echo $bool; ?>) {
    $(function() {
        $('.light-pagination').pagination({
            items: <?php echo $total - $GLOBALS['extraDealsFirstPage']; ?>,
            itemsOnPage: <?php echo $GLOBALS['dealsQuantityPerPage']; ?>,
            currentPage: <?php echo $actualPage; ?>,
            cssStyle: 'light-theme',
            hrefTextPrefix: 'makesearch?scV=<?php echo $auxVarsEnc; ?>&page=',
            prevText: 'Anterior',
			nextText: 'Siguiente',
            edges: 1,
            displayedPages: 3
        });
    });
}
/*
$(document).ready(function(){
    $( "#search-plugin .imagen-oferta" ).bind({
        mouseenter: function() {
            $(this).find(".over-info").show();
        },
        mouseleave: function() {
            $(this).find(".over-info").hide();
        }
    });
});
*/
</script>
