<?php
    $actualPage = $search['Page']['Actual'];
    $total = (int) $resultsSolr->response->numFound;

    $firstItemActualPage = ($actualPage - 1) * $GLOBALS['dealsQuantityPerPage'] + 1;
    $firstItemActualPage += (($actualPage == 1) ? 0 : $GLOBALS['extraDealsFirstPage']);

    $lastItemActualPage = $firstItemActualPage + $GLOBALS['dealsQuantityPerPage'] - 1;
    $lastItemActualPage += (($actualPage == 1) ? $GLOBALS['extraDealsFirstPage'] : 0);
    if ($lastItemActualPage > $total) {
        $lastItemActualPage = $total;
    }

    if ($resultsSolr->response->numFound > 1 ) {    	
?>


    <form id="orderForm" accept-charset="utf-8" method="get">
        <select id="qSort" name="qSort">
            <?php 
            foreach ($GLOBALS['searchOrderOptions'] as $key => $value) {
                $selected = '';
                if ($value == $search[Sort][Option]) {
                    $selected = 'selected';
                }
                echo "<option value='$value' $selected>$key</option>";
            }
            ?>
        </select>
        <input name="scV" type="hidden" value="<?php echo $scvToQString; ?>">
    </form>

<script type="text/javascript">
    $('#qSort').change(function() { $('#orderForm').submit(); });
</script>
<?php
    }
?>
<?php 
    if ($total>0 && !isset($message)) {
        if ($firstItemActualPage != $lastItemActualPage) {
            echo "<p><span>$firstItemActualPage - $lastItemActualPage</span> de <span>$total</span> ofertas disponibles </p>"; 
        } else {
            echo "<p><span>$firstItemActualPage</span> de <span>$total</span> ofertas disponibles</p>"; 
        }
    }
?>
