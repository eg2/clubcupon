<?php 
    if (isset($search['Flts'])){ 
        if (count($search['Flts']) > 0) {
?>
    <div class="block-title">Filtrado por</div>
<?php
        }
    }
?>
<ul>
<?php
    $selectedFound = false;

    foreach ($GLOBALS['HierFacetsCats'] as $cat) {
        if (isset($search['Flts']['FctsHierSel'][$cat])) {
            $selectedFound = true;
            echo '<li class="selected"><span>' . $search['Flts']['FctsHierSel'][$cat] . '</span>';
            $linkUnset = $html->link(
                'x',
                array(
                    'plugin'=>'search',
                    'controller' => 'search_querys',
                    'action' => 'makesearch',
                    '?' => array(
                        'subFctHier' => $cat,
                        'scV' => $scvToQString
                    )
                ),
                array(
                    'title' => 'title',
                    'class' => 'itemU-class'
                )
            );
            echo ' ' . $linkUnset;
            echo '</li>';
        }
    }

    foreach ($resultsSolr->facet_counts->facet_ranges as $fieldname => $fieldvalue) {
        $fieldN = htmlspecialchars($fieldname, ENT_NOQUOTES, 'utf-8');
        if (isset($search['Flts']['FacetsRSel'])) {
            if (isset($search['Flts']['FacetsRSel'][$fieldN])) {
                $selectedFound = true;
                $auxStart = $search['Flts']['FacetsRSel'][$fieldN];
                $auxEnd = $auxStart + $GLOBALS['fctsRParams']['facet.range.gap'] - 1;
                echo '<li class="selected"><span>' . $auxStart . ' - ' . $auxEnd . '</span>';
                $linkUnset = $html->link(
                    'x', 
                    array(
                        'plugin'=>'search',
                        'controller' => 'search_querys', 
                        'action' => 'makesearch',
                        '?' => array(
                            'subFctRange' => $fieldN,
                            'scV' => $scvToQString
                        )
                    ), 
                    array(
                        'title' => 'title', 
                        'class' => 'itemU-class'
                    )
                );
                echo ' ' . $linkUnset;
                echo '</li>';
            }
        }
    }
    if (isset($search['Flts']['FctsCRMin'])) {
        $auxStart = $search['Flts']['FctsCRMin'];
        $auxEnd = $search['Flts']['FctsCRMax'];
        $selectedFound = true;
        echo '<li class="selected"><span>' . $auxStart . ' - ' . $auxEnd . '</span>';
        $linkUnset = $html->link(
            'x',
            array(
                'plugin'=>'search',
                'controller' => 'search_querys',
                'action' => 'makesearch',
                '?' => array(
                    'subFctRangeCustom' => $GLOBALS['facetsCustomRangeField'],
                    'scV' => $scvToQString
                )
            ),
            array(
                'title' => 'title',
                'class' => 'itemU-class'
            )
        );
        echo ' ' . $linkUnset;
        echo '</li>';
    }
    
    foreach ($resultsSolr->facet_counts->facet_fields as $fieldname => $fieldvalue) {
        $fieldN = htmlspecialchars($fieldname, ENT_NOQUOTES, 'utf-8');
        if (isset($search['Flts']['FctsSel'])) {
            if (isset($search['Flts']['FctsSel'][$fieldN])) {
                $selectedFound = true;
                $label = $search['Flts']['FctsSel'][$fieldN];
                if (isset($GLOBALS['facetsTranslations'][$fieldN][$label])) {
                    $label = $GLOBALS['facetsTranslations'][$fieldN][$label];
                }
                echo '<li class="selected"><span>' . $label . '</span>';
                $linkUnset = $html->link(
                    'x',
                    array(
                        'plugin'=>'search',
                        'controller' => 'search_querys',
                        'action' => 'makesearch',
                        '?' => array(
                            'subFct' => $fieldN,
                            'scV' => $scvToQString
                        )
                    ),
                    array(
                        'title' => 'title',
                        'class' => 'itemU-class'
                    )
                );
                echo ' ' . $linkUnset;
                echo '</li>';
            }
        }
    }
?>
</ul>
