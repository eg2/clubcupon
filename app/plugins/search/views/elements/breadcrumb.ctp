<ul>
<?php
    $link = $html->link(
        'Inicio',
        array(
            'plugin'=>'search',
            'controller' => 'search_querys',
            'action' => 'makesearch',
            '?' => array(
                'q' => '*:*',
                'qr' => 'reset',
                'scV' => $scvToQString
            )
        ),
        array(
            'title' => 'Inicio',
            
        )
    );
    echo '<li>'.$link.'</li>';

    $maxCat = 0;
    if (isset($search['Flts']['FctsHierSel'])) {
        foreach ($search['Flts']['FctsHierSel'] as $cat => $val) {
            $catLevel = array_search($cat, $GLOBALS['HierFacetsCats']) + 1;
            if ($catLevel > $maxCat) {
                $maxCat = $catLevel;
            }
        }
    }

    function recursiveS($obj, $level, $maxCat, $search, $scvToQString, $html){
        foreach($obj as $key=>$value){
            if (is_array($value)){
                recursiveS($value, $level+1, $maxCat, $search, $scvToQString, $html);
            }else{
                if (is_object($value)) {
                    if (isset($search['Flts']['FctsHierSel']))  {
                        $link = $html->link(
                            $value->value,
                            array(
                                'plugin'=>'search',
                                'controller' => 'search_querys',
                                'action' => 'makesearch',
                                '?' => array(
                                    'resetHierField' => $value->field,
                                    'resetHierFacet' => $value->value,
                                    'scV' => $scvToQString
                                )
                            ),
                            array(
                                'title' => $value->value,
                                
                            )
                        );
                        echo '<li>&nbsp;>&nbsp;</li><li>' . $link.'</li>';
                    }
                    if (isset($value->pivot) && $maxCat > $level) {
                        if (count($value->pivot)>0) {
                            recursiveS($value->pivot, $level+1, $maxCat, $search, $scvToQString, $html);
                        }
                    }
                }
            }
        }
    }
    recursiveS($resultsSolr->facet_counts->facet_pivot, 0, $maxCat, $search, $scvToQString, $html);

    if (isset($search['Query']['Q'])) {
        if ($search['Query']['Q'] != null && $search['Query']['Q'] != '*:*') {
            $link = $html->link(
                $search['Query']['Q'],
                array(
                    'plugin'=>'search',
                    'controller' => 'search_querys',
                    'action' => 'makesearch',
                    '?' => array(
                        'q' => $search['Query']['Q'],
                        'scV' => $scvToQString
                    )
                ),
                array(
                    'title' => $search['Query']['Q'],
                    
                )
            );
            echo '<li>&nbsp;>&nbsp;</li><li>' . $link.'</li>';
        }
    }

?>
</ul>