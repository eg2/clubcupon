<?php
/*
 * @author: pward
 * params: deal - slug
 */

$title = $deal->title;
$description = $deal->description;

$titleShort = cutTextSP($title, 50);
$descShort = cutHtml($description, 145, 50);

$title = htmlspecialchars($title);

$companyName = $deal->company->name;
$companyCity = $deal->company->city;

if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    $srcImg = $GLOBALS['dummySearchFirstElementUrlImage'];
} else {
    $srcImg = $deal->imageURL;
}


?>
		<article id="destacado-categoria">
			<img src="<?php echo $srcImg; ?>" alt="cupon" width="<?php echo $width;?>" height="<?php echo $height;?>" >
			<div>
				<h3><?php echo $titleShort; ?></h3>
				<p><?php echo $descShort; ?></p>
				<div class="ubicacion">
					<p><?php echo $companyName; ?>
						<br>
						<?php if (!empty($companyCity)): ?>
						<img src="/img/web/clubcupon/pick.png" />&nbsp;<?php echo $companyCity; ?>
						<?php endif; ?>
					</p>
				</div>
				<?php if (!$deal->onlyPrice): ?>
					<?php if (!$deal->hidePrice): ?>			
					<p class="precio"><span>$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></p>
					<?php else: ?>
					<p class="precio"><?php echo $deal->discountedPercent; ?>%</p>
					<?php endif; ?>
				<?php else: ?>
					<p class="precio">$<?php echo $deal->discountedPrice; ?></p>
				<?php endif; ?>
				<a href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>" title="<?php echo $title; ?>">Ver oferta</a>
			</div>
		</article>
		
