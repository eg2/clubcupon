<?php
/*
 * modif: pward
 * pararms: deal - slug 
 */


$title = $deal->title;

$titleShort = cutText($title, 70);

$title = htmlspecialchars($title);

$companyName = $deal->company->name;
$companyCity = $deal->company->city;

if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    $srcImg = $GLOBALS['dummySearchElementUrlImage'];
} else {
    $srcImg = $deal->imageURL;
}

?>

<article id="item_categorias">
	<a href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>" title="<?php echo $title; ?>">
		<img src="<?php echo $srcImg; ?>" alt="cupon">
		<h3><?php echo $titleShort; ?></h3>
		<div class="ubicacion">
			<p><?php echo $companyName; ?>
				<br>
				<?php if (!empty($companyCity)):?>
				<img src="/img/web/clubcupon/pick.png" />&nbsp;<?php echo $companyCity; ?>
				<?php endif;?>
			</p>
		</div>
	<?php if (!$deal->onlyPrice): ?>
		<?php if (!$deal->hidePrice): ?>			
		<p class="precio"><span>$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></p>
		<?php else: ?>
		<p class="precio"><?php echo $deal->discountedPercent; ?>%</p>
		<?php endif; ?>
	<?php else: ?>
		<p class="precio">$<?php echo $deal->discountedPrice; ?></p>
	<?php endif; ?>
	</a>
</article>
