<?php
    if ($GLOBALS['showFixedRFacets']) {
        foreach ($resultsSolr->facet_counts->facet_ranges as $fieldname => $fieldvalue) {
            $fieldN = htmlspecialchars($fieldname, ENT_NOQUOTES, 'utf-8');
            $fieldSelected = false;
            if (isset($search['Flts']['FacetsRSel'])) {
                if (isset($search['Flts']['FacetsRSel'][$fieldN])) {
                    $fieldSelected = true;
                }
            }
            if (!$fieldSelected) {
                if (get_object_vars($fieldvalue->counts)) {
                    echo 'Field: ' . $fieldN . '<br>';
                    foreach ($fieldvalue->counts as $key => $value) {
                        $keyAux = $key.' - '.($key+$GLOBALS['fctsRParams']['facet.range.gap']-1);
                        $keyN = htmlspecialchars($keyAux, ENT_NOQUOTES, 'utf-8');
                        $link = $html->link(
                            $keyN, 
                            array(
                                'plugin'=>'search',
                                'controller' => 'search_querys', 
                                'action' => 'makesearch',
                                '?' => array(
                                    'setRFacetField' => $fieldN, 
                                    'setRBase' => $key,
                                    'scV' => $scvToQString
                                )
                            ), 
                            array(
                                'title' => 'title', 
                                'class' => 'item-class'
                            )
                        );
                        echo $link . ': ';
                        echo htmlspecialchars($value, ENT_NOQUOTES, 'utf-8') . '<br>';
                    }
                }
            }
        }
    }
    $customRangeSelected = false;
    if (isset($search['Flts']['FctsCRMin']) || isset($search['Flts']['FctsCRMax'])) {
        $customRangeSelected = true;
    }     
?>   

<?php if (!$customRangeSelected && $resultsSolr->response->numFound > 1) { ?>
    <div class="block-title">Rango de precios</div>
    <div class="input-group">
        <form action="/search/search_querys/makesearch" method="get" class="range-form">
            <input name="CRangeMin" type="text" data-validate="number" data-required data-describedby="messages1" data-description="test" placeholder="Mín" style="width:40px">
            <input name="CRangeMax" type="text" data-validate="number" data-required data-describedby="messages2" data-description="test" placeholder="Máx" style="width:40px">
            <input name="setCRFacet" type="hidden" value="<?php echo $GLOBALS['facetsCustomRangeField']; ?>">
            <input name="scV" type="hidden" value="<?php echo $scvToQString; ?>">
            <button type="submit" class="btn">Filtrar</button>
            <div class="contValidateError">
                <span id="messages1" class="validateError"></span>
                <span id="messages2" class="validateError"></span>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $('.range-form').validate({
            description : {
                test : {
                    required : '<div class="error">Valores requeridos</div>',
                    pattern : '<div class="error">Por favor ingrese solo números</div>',
                    conditional : '<div class="error"></div>',
                    valid : '<div class="success"></div>'
                }
            }
        });
        jQuery.validateExtend({
            number : {
                required : true,
                pattern : /^(0|[1-9][0-9]*)$/,
                conditional : function(value) {
                    if (value!=0) {
                        return Number(value);
                    } else {
                        return true;
                    }
                }
            }
        });
    </script>
<?php } ?>