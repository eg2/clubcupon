<?php
    $maxCat = 0;
    $allSelectedCats = array();
    if (isset($search['Flts']['FctsHierSel'])) {
        foreach ($search['Flts']['FctsHierSel'] as $cat => $val) {
            $allSelectedCats[] = $val;
            $catLevel = array_search($cat, $GLOBALS['HierFacetsCats']) + 1;
            if ($catLevel > $maxCat) {
                $maxCat = $catLevel;
            }
        }
    }

    $resultHtml = '';
    function recFind($obj, $level, $html, $scvToQString, $allSelectedCats, $maxCat, $resultHtml, $lShow){
        foreach($obj as $key=>$value){
            if (is_array($value)){
                $resultHtml = recFind($value, $level+1, $html, $scvToQString, $allSelectedCats, $maxCat, $resultHtml, $lShow);
            }else{
                if (is_object($value)) {
                    if (!in_array($value->value, $allSelectedCats) && $maxCat<$level && !in_array($value->value, $GLOBALS['CatBlackList'])) {
                        if (!$lShow['enable'] || $lShow['level'] == $level) {
                            $link = '/search/search_querys/makesearch?setHFacetField='.$value->field.'&setFacet='.$value->value.'&scV='.$scvToQString;
                            $GLOBALS['isLink'] = true;
                            $tagA = '<a href="'.$link.'" class="item-class" title="'.$value->value.'">'.$value->value.'</a>';
                            $resultHtml .= '<li>' . $tagA . ' <span class="cant">(';
                            $resultHtml .= htmlspecialchars($value->count, ENT_NOQUOTES, 'utf-8') . ')</span></li>';
                            if (!$lShow['enable']) {
                                $lShow['level'] = $level;
                                $lShow['enable'] = true;
                            }
                        }
                    }
                    if (isset($value->pivot) && !in_array($value->value, $GLOBALS['CatBlackList'])) {
                        if (count($value->pivot)>0) {
                            $resultHtml = recFind($value->pivot, $level+1, $html, $scvToQString, $allSelectedCats, $maxCat, $resultHtml, $lShow);
                        }
                    }
                }
            }
        }
        return $resultHtml;
    }
    
    $lShow = array(
        'enable' => false ,
        'level'  => 0
    );
    $resultHtml = recFind($resultsSolr->facet_counts->facet_pivot, 0, $html, $scvToQString, $allSelectedCats, $maxCat, $resultHtml, $lShow);

    if ($resultHtml!='') {
        echo '<div class="block-title first">Categorías</div>';
        echo '<ul>';
        echo $resultHtml;
        echo '</ul>';
    }

?>
