<?php

    //$title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $browserIsLinuxHost = (strpos($browser, 'Linux') !== false);
    $browserIsIE7 = (strpos($browser, 'MSIE 7.0') !== false);
    $browserIsIE8 = (strpos($browser, 'MSIE 8.0') !== false);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml"<?php if (isset ($og_data)) echo 'xmlns:og = "http://ogp.me/ns#"  xmlns:fb = "http://www.facebook.com/2008/fbml"'; ?>>
    <head>
        <?php echo $html->charset(), "\n"; ?>

        <!--[if IE 7]>
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

        <!--[if IE 8]>
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

    <title>
        <?php
            if(preg_match('/'.Configure::read('site.name').'/', $metaData->title)) {
                $title = Configure::read('site.name') . ' | ' .$html->cText($metaData->title, false);
            } else {
                $title = Configure::read('site.name') . ' | ' . $html->cText($metaData->title, false);
            }
            echo $title;
        ?>
    </title>
        
        <?php

              echo $html->meta('keywords', $metaData->keywords), "\n";

              if (isset ($og_data)) {
                // var_dump ($og_data);
                foreach ($og_data as $key => $value) {
                  if (is_array ($value)) {
                    foreach ($value as $val) {
                      echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                    }
                  } else {
                    echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                  }
                }
              }
             
              ?>
              <meta name="description" content="<?php echo $metaData->description; ?>"/>
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
             
              <?php
              //CSSs
            echo $html->css(Configure::read('theme.asset_version') . 'reset');      //unifica los estados de los navegadores
            //
            //Analizamos que hoja de estilo vamos a usar...

            switch (@$css_layout)
            {
                case 'popup':
                    $css_grilla = 'popup';
                    break;
                case 'wide':
                    //estamos en un layout del tipo WIDE
                    $css_grilla = 'custom-grid';
                    break;
                default:
                    //Cualquier tipo de layout
                    $css_grilla = '960_16_col';
            }

            //CSSs
            echo $html->css(Configure::read('search.asset_version') . 'simplePagination');   //pagination
            echo $html->css(Configure::read('search.asset_version') . 'jquery-ui-1.10.3');   //autocomplete
            echo $html->css(Configure::read('search.asset_version') . 'general');   //search
            //echo $html->css(Configure::read('bootstrap.asset_version') . 'bootstrap-2.3.2.min');   //bootstrap

            echo $html->css(Configure::read('web.asset_version').'clubcupon/new/style.css'); //web plugin

            //JSs
            echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true));
            echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.7.1.min');
			echo $javascript->link(Configure::read('theme.asset_version') . 'jquery.colorbox');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.cookie');//lo usa funciones
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.form');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.countdown');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.livequery');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.ui.datepicker');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/date.format');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.metadata');
            echo $javascript->link(Configure::read('theme.asset_version') . '/libs/BigDecimal-all-last.min');
            echo $javascript->link(Configure::read('search.asset_version') . 'jquery.simplePagination');
            echo $javascript->link(Configure::read('search.asset_version') . 'jquery-ui-1.8.18.min');
            echo $javascript->link(Configure::read('search.asset_version') . 'search-Plugin-1.0');
            echo $javascript->link(Configure::read('search.asset_version') . 'jquery-validate.min');
            echo $javascript->link(Configure::read('bootstrap.asset_version') . 'bootstrap-2.3.2.min');
            echo $javascript->link(Configure::read('theme.asset_version') . 'funciones');
        ?>
        
        <!-- web plugin -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300|Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
        
        <!-- facebook -->
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />

        <!--[IF IE]>
            <script type="text/javascript" src="/js/theme_clean//libs/jq-corners.js"></script>
        <![endif]-->

        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />

        <!-- Bootstrap
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script> -->

         	<!--[if lt IE 9]>
	<script type="text/javascript">
		document.createElement("nav");
		document.createElement("header");
		document.createElement("footer");
		document.createElement("section");
		document.createElement("article");
		document.createElement("aside");
		document.createElement("hgroup");
	</script>
<![endif]-->
         <meta name="cXenseParse:pageclass" content="frontpage"> 
    </head>
