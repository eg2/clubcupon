<?php
    // iterate result facets
    foreach ($resultsSolr->facet_counts->facet_fields as $fieldname => $fieldvalue) {
        $fieldN = htmlspecialchars($fieldname, ENT_NOQUOTES, 'utf-8');
        $fieldSelected = false;
        if (isset($search['Flts']['FctsSel'])) {
            if (isset($search['Flts']['FctsSel'][$fieldN])) {
                $fieldSelected = true;
            }
        }
        if (!$fieldSelected) {
            if (get_object_vars($fieldvalue)) {
                $resultHtml = '';
                foreach ($fieldvalue as $key => $value) {
                    $keyN = htmlspecialchars($key, ENT_NOQUOTES, 'utf-8');
                    if (!in_array($keyN, $GLOBALS['FctsBlackList'])) {
                        $keyLab = $keyN;
                        if (isset($GLOBALS['facetsTranslations'][$fieldN][$keyN])) {
                            $keyLab = $GLOBALS['facetsTranslations'][$fieldN][$keyN];
                        }
                        $link = '/search/search_querys/makesearch?setFacetField='.$fieldN.'&setFacet='.$keyN.'&scV='.$scvToQString;
                        $tagA = '<a href="'.$link.'" class="item-class" title="'.$keyLab.'">'.$keyLab.'</a>';
                        $resultHtml .= '<li>' . $tagA . ' <span class="cant">(';
                        $resultHtml .= htmlspecialchars($value, ENT_NOQUOTES, 'utf-8') . ')</span></li>';
                    }
                }
                if ($resultHtml!='') {
                    echo '<div class="block-title">' . $GLOBALS['facetsLabels'][$fieldN] . '</div><ul>';
                    echo $resultHtml;
                    echo "</ul>";
                }
            }
        }
    }
?>