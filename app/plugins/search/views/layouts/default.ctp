<?php 
    $title_for_layout = 'Title Search';
    
    $data = $this->viewVars['data'];
    $dataObj = json_decode($data);
    
    $user = $dataObj->user;
    $city = $dataObj->city;
    $slug = $city->slug;
    $imageURL = $city->imageURL;
    $citiesLinkList = $dataObj->vData->citiesLinkList;
    $citiesLinks = $citiesLinkList->citiesLinks;
    $specialLinks = $dataObj->vData->specialLinkList;
    $extraLinks = $citiesLinkList->extraLinks;
    $selectedTab = $dataObj->vData->selectedTab;
    $metaData = $dataObj->vData->metaData;

    echo $this->element(
        'head',
        array (
            'metaData' => $metaData
        )
    );
    
    if (Configure::read('app.rDebug.enable')) {
        rdebug('$dataObj', $dataObj);
        rdebug('$resultsSolr', $resultsSolr);
        rdebug('$search', $search);
    }

?>
    
    <body>
        <?php
        	if (Configure::read('app.tracking.enable')) {
        		echo $this->element('trackingGoogle');
        			echo $this->element('trackingWeb',array('view'=>$dataObj->vData->viewType,'citiSlug'=>$slug));
        	}
             $dummy = Configure::read('theme.theme_name');
             if (Configure::read('app.tracking.enable')) {
             	echo $this->element('tracking-dataexpand');
             }
                    
            //Flash alerts
             if ($session->check('Message.error'))  {$session->flash('error');}
             if ($session->check('Message.success')){$session->flash('success');}
             //if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
             echo $this->element(
             		'clubcupon/headerClubcupon',
             		array (
    				        'user' => $user,
     		                'city' => $city,
             				'imageURL' => $imageURL,
             				'isBusinessUnit' => $isBusinessUnit,
             				'citiesLinks' => $citiesLinks,
             				'specialLinks' => $specialLinks,
             				'extraLinks' => $extraLinks,
             				'selectedTab'=>$selectedTab
             		)
             );
             echo $content_for_layout;
             
             //echo $this->element('clubcupon/tagEplanningProduct5');
             
             echo $this->element('clubcupon/jsEplanningHome');//banners 4 215x100
             
             echo $this->element(
             		'clubcupon/footerClubcupon',
             		array (
             				'slug' => $slug,
             		)
             );
            
            //Tracker
            
        ?>
    </body>
    <script id="nvg_rt" type="text/javascript" src="//navdmp.com/req?acc=23111&cus=101911"></script>
</html>