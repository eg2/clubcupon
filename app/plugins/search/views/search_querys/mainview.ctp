<?php
$dataObj = json_decode($data);
$dealList = $dataObj->vData->dealList->dealList;
$city = $dataObj->city;
$slug = $city->slug;

$dataVars = array(
    'plugin' => 'search',
    'resultsSolr' => $resultsSolr,
    'search' => $search,
    'suggestion' => $suggestion,
    'searched' => $searched,
    'varsToQueryString' => $scvToQString,
    'dealList' => $dealList,
    'slug' => $slug
);
if (isset($message)) {
    $dataVars['message'] = $message;
}
$adsSearch = Configure::read('search.eplanningIds');
$hFacetFieldFirstPathLevelID = $dataObj->vData->hFacetFieldFirstPathLevelID;
if ($dealList[0]->isTourism) {
    $isTourism = 'si';
} else {
    $isTourism = 'no';
}
?>
<div id="content">
    <div class="homeBanners homeBanners1">
        <div class="banner">
            <?php echo $this->element('resultadoDeBusquedaBusqueda950x50'); ?>
        </div>
    </div>
    <h1><?php
        $findme = '*:*';
        $pos = strpos($search["Query"]["Q"], $findme);
        if (!isset($message) && !empty($search["Query"]["Q"]) && ($pos === false)):
            echo($search["Query"]["Q"]);
        endif;
        ?>
    </h1>
    <div class="messages">
        <?php
        if (isset($message)) {
            if ($session->check('Message.flash')) {
                $session->flash();
            }
        }
        ?>
    </div>
    <div id="ruta_filtro">
        <?php echo $this->element('breadcrumb', $dataVars); ?>
        <div><?php echo $this->element('searchform', $dataVars); ?></div>
    </div>
    <div id="resultado_busqueda" class="span3 filters">
        <div class="filter-selected"><?php echo $this->element('sidebarselectedfilters', $dataVars); ?></div>
        <div class="filter-categories"><?php echo $this->element('sidebarhierarchicalfacet', $dataVars); ?></div>
        <div class="filter-range"><?php echo $this->element('sidebarrangefacet', $dataVars); ?></div>
        <div class="filter-custom-range"><?php echo $this->element('sidebarfacet', $dataVars); ?></div>
    </div>
    <div id="banners_search" class="span3 filters">
        <?php
        echo $this->element(
                'banners_search'
        );
        ?>
    </div>
    <?php if ($hFacetFieldFirstPathLevelID == Configure::read('search.specialCategoryIDWithEplanning')) { ?>
        <div>
            <?php
            if (Configure::read('web.ad_server') == 'eplanning') {
                echo $this->element(
                        'adNormal', array(
                    'idAd' => $adsSearch['homeMiddle1x462'],
                    'isTourism' => $isTourism,
                    'idAviso' => $dealList[0]->id,
                    'slug' => $slug,
                    'width' => '728',
                    'height' => '90'
                        )
                );
            }
            ?>
        </div>
    <?php } ?>
    <?php echo $this->element('gallery', $dataVars); ?>
</div>

