<?php

function cutTextSP($text, $pos) {
    while (substr($text, $pos, 1) != ' ' && strlen($text) > $pos) {
        $pos++;
    }
    if (strlen($text) > $pos) {
        $text = substr($text, 0, $pos);
        $text = $text . " ...";
    }
    return $text;
}

function cutText($text, $pos) {
    if (strlen($text) > $pos) {
        $text = substr($text, 0, $pos);
        $text = $text . "...";
    }
    return $text;
}

function cutHtml($html, $pos, $spaceLenght) {
    $html = html_entity_decode($html);
    $html = strip_tags($html);
    $html = preg_replace('/(?:<|&lt;).*?(?:>|&gt;)/', '', $html);
    if (strlen($html) > $pos) {
        $descLenght = strlen($html);
        $html = substr($html, 0, $pos) . '*.*.*' . substr($html, ($pos + 1), $descLenght - $pos);
        $html = htmlentities($html);
        $html = str_replace('*.*.*', "... " . str_repeat('&nbsp;', $spaceLenght), $html);
    } else {
        $html = htmlentities($html);
    }
    return $html;
}

?>