<?php

if (Configure::read('app.rDebug.enable')) {
    App::import('Vendor', 'web.ref');
}
App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));
App::import('Vendor', 'search.commons');

class SearchAppController extends AppController {

    public $logFileNamePrefix = "search";
    public $ApiLogger;
    protected $isQ;
    protected $isScv;
    protected $isSubFct;
    protected $isSetFacetField;
    protected $isSetHFacetField;
    protected $isSetRFacetField;
    protected $isSetCRFacet;
    protected $isQSort;
    protected $isPage;
    protected $isSubFctCity;
    protected $isSetAnyFacetField;

    function __construct() {
        $this->ApiLogger = new ApiLogger($this->name, $this->logFileNamePrefix);
        $this->ApiLogger->putAll(array(
            'pid' => getmypid(),
            'IP' => $_SERVER[REMOTE_ADDR]
        ));
        $this->isQ = (isset($_GET['q']));
        $this->isScv = (isset($_GET['scV']));
        $this->isSubFct = (isset($_GET['subFct']));
        $this->isSetFacetField = (isset($_GET['setFacetField']));
        $this->isSetHFacetField = (isset($_GET['setHFacetField']));
        $this->isSetRFacetField = (isset($_GET['setRFacetField']));
        $this->isSetCRFacet = (isset($_GET['setCRFacet']));
        $this->isQSort = (isset($_GET['qSort']));
        $this->isResetHierField = (isset($_GET['resetHierField']));
        $this->isPage = (isset($_GET['page']));
        $this->isSetAnyFacetField = ($this->isSetFacetField || $this->isSetHFacetField || $this->isSetRFacetField || $this->isSetCRFacet);
        $this->isSubFctCity = ($_GET['subFct'] == 'city_name_no_group' || $_GET['subFct'] == 'city_name_group');
        parent::__construct();
    }

    function __mergeVars() {
        parent::__mergeVars();
        unset($this->components['UserMenu']);
        unset($this->components['Pdf']);
        $this->uses = null;
    }

    public function index() {
        
    }

    function _checkAuth() {
        return true;
    }

    function beforeFilter() {
        
    }

    function beforeRender() {
        
    }

}
