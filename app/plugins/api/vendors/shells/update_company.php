<?php
App::import('Component', 'api.ApiBacService');
App::import('Model', 'api.ApiCompany');

class UpdateCompanyShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiBacService = new ApiBacServiceComponent();
        $this->ApiCompany = ClassRegistry::init('api.ApiCompany');
    }

    function main() {
        $items[] = 
             array('AccountingItem' => array(
                  'id' => '28339',
                  'created'=>'2013-05-07 00:02:10',
				          'modified'=>'2013-05-07 00:02:10',
				          "created_by"=>"0",
                  "modified_by"=>null,
				          "deleted"=>"0",
                  "accounting_calendar_id"=>"11",
                  "accounting_item_id"=>null,
                  "accounting_type"=>"BILL_BY_SOLD_FOR_COMPANY",
                  "model"=>"Company",
                  "model_id"=>"1480",
                  "deal_id"=>"79788",
                  "total_quantity"=>"1",
                  "total_amount"=>"68.06",
                  "bac_sent"=>null,
                  "bac_response_id"=>null,
                  "discounted_price"=>"375.00",
                  "billing_commission_percentage"=>"15.00",
                  "billing_iva_percentage"=>"21",
                  "liquidating_is_first"=>"0",
                  "liquidating_guarantee_fund_percentage"=>"0",
                  "liquidating_agreed_percentage"=>"0",
                  "liquidating_invoiced_amount"=>"0.00",
                  "liquidating_iibb_percentage"=>"0"
                ));
           
       
        echo "[" . __METHOD__ . "] Begin.\n";
        echo "[" . __METHOD__ . "] Getting Companies.\n";
        $companies = $this->ApiCompany->find('all');
        $portalId = 18;
        $bacIdFieldName = 'bac_id';
        $i  = 0 ;
        foreach ($companies as $company) {
            echo "[" . __METHOD__ . "] Crating/Updating Companies.\n";
            try {
            $this->ApiBacService->createBacCompanyUser($company, $portalId=null, $bacIdFieldName=null);
            } catch (Exception $e) {
                $i = $i + 1;
                echo $e->getMessage() . 'Fallaron: '. $i;
            }
        }
        echo "[" . __METHOD__ . "] End.\n";
    }

}

?>
