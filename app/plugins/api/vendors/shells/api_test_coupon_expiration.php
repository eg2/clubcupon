<?php

App::import('Component', 'api.ApiDealService');

class ApiTestCouponExpirationShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiDealService = null;
        $this->deal = null;
        $this->dealUser = null;
    }

    function setUp() {
        echo __METHOD__ . "\n";
        //error_reporting(E_ERROR | E_WARNING | E_PARSE);
        //ini_set('display_errors', 1);
        $this->ApiDealService = new ApiDealServiceComponent();
        $this->deal = array(
            'Deal' => array(
                'is_variable_expiration' => 1,
                'end_date' => '2015-06-25 23:59:00',
                'coupon_duration' => 10,
                'coupon_expiry_date' => '2015-06-29 23:59:00',
                'coupon_start_date' => '2015-06-15 00:00:00'
            )
        );
        $this->dealUser = array(
            'DealUser' => array(
                'created' => '2015-06-20 15:41:21'
            )
        );
    }

    function tearDown() {
        echo __METHOD__ . "\n";
    }

    function testCouponExpirationShouldBeLowerThanCouponExpiry() {
        echo __METHOD__ . "\n";
        $couponExpirationDate = $this->ApiDealService->couponExpirationDate($this->deal, $this->dealUser);
        echo $couponExpirationDate . "\n";
        if ($couponExpirationDate <= $this->deal['Deal']['coupon_expiry_date']) {
            throw new DomainException();
        }
    }

    function testCouponExpirationShouldBeFirstDayPlusCouponDuration() {
        echo __METHOD__ . "\n";
        $couponExpirationDate = $this->ApiDealService->couponExpirationDate($this->deal, $this->dealUser);
        if ($couponExpirationDate != '2015-06-25 23:59:00') {
            throw new DomainException();
        }
    }

    function main() {
        echo __METHOD__ . "\n";
        $this->setUp();
        $this->testCouponExpirationShouldBeLowerThanCouponExpiry();
        $this->testCouponExpirationShouldBeFirstDayPlusCouponDuration();
        $this->tearDown();
    }

}
