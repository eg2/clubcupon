<?php

class SolrImportShell extends Shell {
  const SLEEP_TIME_IN_SECONDS = 30;
  const MAXIMUM_TIME_WAITTING_END_IMPORT = 500;

  public function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->importStartUrl = "http://" . ConstSolrSearch::HOST . ':' . ConstSolrSearch::PORT .
      ConstSolrSearch::IMPORT_START_PATH;
    $this->importStatusUrl = "http://" . ConstSolrSearch::HOST . ':' . ConstSolrSearch::PORT .
      ConstSolrSearch::IMPORT_STATUS_PATH;
  }


  public function  main() {
        echo __METHOD__."\n";
        echo "La Url para Iniciar la Importacion Completa de SOLR es: ". $this->importStartUrl . "\n";
        echo "La Url para Consultar el Estado de la Importacion Completa de SOLR es: ". $this->importStatusUrl . "\n";


        echo "Iniciando Importacion Completa de Solr \n";
        $canStartImport = $this->startImport();
        if (!$canStartImport) {
          echo "ERROR al Intentar Iniciar la Importacion Completa de Solr.\n";
          return false;
        }

        echo "Importacion Iniciada.\n";
        echo "Consultando el Estado de la Importacion Completa de Solr.\n";
        $statusImport = $this->checkImportStatus();
        $waitingTime = 0;
        while(  !$statusImport['has_error']
          && !$statusImport['is_completed']
          && $number_of_attempts < self::MAXIMUM_NUMBER_OF_ATTEMPTS) {

          sleep(self::SLEEP_TIME_IN_SECONDS);
          $importComplete = $this->isImportComplete();
          $waitingTime = $waitingTime  + self::SLEEP_TIME_IN_SECONDS;
        }

        if ($statusImport['is_completed']) {
          echo "Importacion Completa.\n";
          echo "Reporte de Importacion:\n";
          echo "--------------------------------------------------\n";
          echo $statusImport['status_info'] . "\n";
          echo "--------------------------------------------------\n";
        } else {
          echo "Se supero el tiempo maximo de espera a que la importacion termine o ocurrio un error al intentar consultar. \n";
          return false;
        }

        echo "Proceso Terminado.\n";
  }

  public function startImport() {
        $ch = curl_init($this->importStartUrl);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $chResult = curl_exec($ch);
        if (!curl_errno($ch)) {
          curl_close($ch);
          return true;
        } else {
          return false;
        }
  }

  public function checkImportStatus() {
        $ch = curl_init($this->importStatusUrl);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $chResult = curl_exec($ch);
        if (!curl_errno($ch)) {
          curl_close($ch);
          return array(
            'has_error' => false,
            'is_completed' => $this->extractIsCompletedAttr($chResult),
            'status_info' => $chResult
          );
        } else {
          return array(
            'has_error' => true ,
          );
        }
  }

  private function extractIsCompletedAttr($xmlString) {
    $pos = strpos($xmlString, 'Indexing completed');
    return $pos !== false;
  }

  private function extractTotalImportedDocumentsAttr($xmlText) {
    return $xmlText; 
  }



}
