<?php

App::import('Component', 'api.ApiSameIpAttemptValidator');

class ApiTestSubscriptionsShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
    }

    public function testValidatorRsponseShouldBeFalse() {
        echo __METHOD__ . "\n";
        $validator = new ApiSameIpAttemptValidatorComponent(10, 200000, array());
        $valid = $validator->validate('127.0.0.1');
        if ($valid) {
            throw new DomainException();
        }
    }

    public function testValidatorRsponseShouldBeTrue() {
        echo __METHOD__ . "\n";
        $validator = new ApiSameIpAttemptValidatorComponent(10, 30, array());
        $valid = $validator->validate('127.0.0.1');
        if (!$valid) {
            throw new DomainException();
        }
    }

    public function testValidatorShouldUseWhiteList() {
        echo __METHOD__ . "\n";
        $validator = new ApiSameIpAttemptValidatorComponent(10, 200000, array(
            '127.0.0.1'
        ));
        $valid = $validator->validate('127.0.0.1');
        if (!$valid) {
            throw new DomainException();
        }
    }

    function setUp() {
        echo __METHOD__ . "\n";
    }

    function tearDown() {
        echo __METHOD__ . "\n";
    }

    function main() {
        $this->setUp();
        $this->testValidatorRsponseShouldBeFalse();
        $this->testValidatorRsponseShouldBeTrue();
        $this->testValidatorShouldUseWhiteList();
        $this->tearDown();
    }

}
