class Clubcupon.Models.User extends Backbone.Model
  defaults:
    token: ''
    walletAmount: 0

  validate:
    token:
      required: true
    walletAmount:
      required: true
      type: 'number'
      min: 0
