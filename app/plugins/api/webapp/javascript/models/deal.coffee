class Clubcupon.Models.Deal extends Backbone.Model
  defaults:
    title: ''
    imgUrl: ''
    price: 0

  validate:
    title:
      required: true
    imgUrl:
      required: true
    price:
      required: true
      type: 'number'
      min: 1
