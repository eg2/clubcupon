class Clubcupon.Models.Buy extends Backbone.Model
  defaults:
    user_id: null
    deal_id: null
    quantity: 1
    price: null
    payment_option_id: null
    is_combined: 0
  validate:
    quantity:
      required: true
      type: 'number'
      min: 1
