describe "Deal model", ->
  goDisneyData =
    title: 'Viaje a Disney a 1500 dolares, 50% de descuento'
    price: 300
    imgUrl: 'img/disney110x110.jpg'

  it "should exist", ->
    expect(Clubcupon.Models.Deal).toBeDefined()

  describe "Attributes", ->
    aDeal = new Clubcupon.Models.Deal

    it "should have default attributes", ->
      expect(aDeal.attributes.title).toBeDefined()
      expect(aDeal.attributes.price).toBeDefined()
      expect(aDeal.attributes.imgUrl).toBeDefined()

    it "should valid", ->
      goDisneyDeal = new Clubcupon.Models.Deal goDisneyData
      expect(goDisneyDeal.isValid()).toBe(true)

  describe "Validations", ->
    invalidData = null

    beforeEach ->
      invalidData = $.extend(true, {}, goDisneyData)

    afterEach ->
      invalidDeal = new Clubcupon.Models.Deal invalidData
      expect(invalidDeal.isValid()).toBeFalsy()

    it "should validate the presence of title", ->
      invalidData['title'] = null

    it "should validate the presence of imgUrl", ->
      invalidData['imgUrl'] = null

    it "should validate the presence of price", ->
      invalidData['price'] = null

    it "should no accept a price < 1", ->
        invalidData['price'] = 0
