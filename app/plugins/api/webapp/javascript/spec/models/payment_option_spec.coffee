describe "PaymentOption model", ->
  validData =
    id: 123
    title: "American Express"
    payment_type_id: 10

  it "should exist", ->
    expect(Clubcupon.Models.PaymentOption).toBeDefined()

  describe "Attributes", ->
    aOption = new Clubcupon.Models.PaymentOption

    it "should have default attributes", ->
      expect(aOption.attributes.id).toBeDefined()
      expect(aOption.attributes.title).toBeDefined()
      expect(aOption.attributes.payment_type_id).toBeDefined()

  describe "Validations", ->
    invalidData = null

    beforeEach ->
      invalidData = $.extend(true, {}, validData)

    afterEach ->
      invalidOption = new Clubcupon.Models.PaymentOption invalidData
      expect(invalidOption.isValid()).toBeFalsy()

    it "should validate the presence of title", ->
      invalidData['title'] = null

    it "should validate the presence of payment_type_id", ->
      invalidData['payment_type_id'] = null

