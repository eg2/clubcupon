describe "Buy model", ->
  validData =
    user_id: 123456
    deal_id: 123456
    quantity: 5
    price: 10
    payment_option_id: 7
    isCombined: 1

  it "should exist", ->
    expect(Clubcupon.Models.Buy).toBeDefined()

  describe "Attributes", ->
    aBuy = new Clubcupon.Models.Buy

    it "should have default attributes", ->
      expect(aBuy.attributes.quantity).toBeDefined()
      expect(aBuy.attributes.price).toBeDefined()

  describe "Validations", ->
    invalidData = null

    beforeEach ->
      invalidData = $.extend(true, {}, validData)

    afterEach ->
      invalidBuy = new Clubcupon.Models.Buy invalidData
      expect(invalidBuy.isValid()).toBeFalsy()

    it "should validate the presence of quantity", ->
      invalidData['quantity'] = null

    it "should no accept a quantity < 1", ->
      invalidData['quantity'] = 0
