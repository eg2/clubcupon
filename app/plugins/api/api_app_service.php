<?php
App::import('vendor', 'Utility', array('file' => 'Utility/ApiLogger.php'));
class ApiAppService extends Object {
  public $ApiLogger = null;

  function __construct($fileLogPrefix=null) {
    $this->ApiLogger = new ApiLogger(get_class($this), $fileLogPrefix);
  }
}
?>
