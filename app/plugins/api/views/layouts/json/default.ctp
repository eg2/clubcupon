<?php
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('status: 200');
if(hasMessage($session)) {
  addMessages($api, $session);
};

echo $content_for_layout;

if(!empty($debug)) {
  $api->set('debug', $debug);
}

echo json_encode($api->getResult());

function hasMessage($session) {
  return $session->read('Message');
}

function addMessages($api, $session) {
  if($session->read('Message.flash.layout') == 'failure') {
    $messageSeverity = 'error';
  } else {
    $messageSeverity = 'warning';
  }

  $api->add(
    'messages',
    $session->read('Message.flash.message'),
    $messageSeverity
  );
}

?>
