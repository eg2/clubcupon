<?php
  header('Content-type: application/xml');
  if($session->read('Message')) {
    $api->add('messages', array(
        'message' => array(
          $session->read('Message.flash.message')
        )
    ));
  };

  echo $content_for_layout;

  $xml = ApiXml::fromArray(array('result' => $api->getResult()));
  echo $xml->asXML();

  if(!is_null($api->extraText)) {
    echo $api->extraText;
  }


?>
