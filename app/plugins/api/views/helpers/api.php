<?php
/**
 * Este helper se utiliza para permitir que las vistas y el layout tengan
 * un array de objectos comun.
 *
 * @TODO evaluar otra solucion para reutilizar objetos entre vistas.
 */
class ApiHelper extends AppHelper {

  protected $result = null;
  public $extraText = null;


  function __construct() {
    $this->result = array();
  }

  public function add($parentKey, $value, $childrenKey = null) {

    if(!isset($this->result[$parentKey])) {
       $this->result[$parentKey] = array();
    }

    if(is_null($childrenKey)) {
      $this->result[$parentKey][] = $value;
    } else {
      $this->result[$parentKey][$childrenKey] = $value;
    }
  }

  public function set($key, $value) {
      $this->result[$key] = $value;
  }

  public function get($key) {
    return $this->result[$key];
  }

  public function getResult() {
    return $this->result;
  }

}
?>
