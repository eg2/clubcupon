<?php
  // http://clubcupon.local/api/api_deals/buy?deal_id=1977&token=1034220cf79a1442b9c4cbde2532c7a4b754b6b5
  $paymentOptionsArr = array();
  foreach($paymentOptions as $paymentOption) {
    if($paymentOption['PaymentOption']['payment_type_id']!=5000) {
      $paymentOption['PaymentOption']['title'] = $paymentOption['PaymentOption']['description'];
      $paymentOption['PaymentOption']['PaymentPlanItem'] = $paymentOption['PaymentPlanItem'];
      $i=0;
      foreach ($paymentOption['PaymentOption']['PaymentPlanItem'] as $plan) {
          $paymentOption['PaymentOption']['PaymentPlanItem'][$i]['id'] = 
            $paymentOption['PaymentOption']['PaymentPlanItem'][$i]['ApiPaymentPlanItem']['id'];
          $paymentOption['PaymentOption']['PaymentPlanItem'][$i]['name'] = 
            $paymentOption['PaymentOption']['PaymentPlanItem'][$i]['ApiPaymentPlanItem']['name'];
          $paymentOption['PaymentOption']['PaymentPlanItem'][$i]['PaymentOptionId'] = $paymentOption['PaymentOption']['id'];
          unset($paymentOption['PaymentOption']['PaymentPlanItem'][$i]['ApiPaymentPlanItem']);
          $i++;
      }
      $paymentOptionsArr[] = $paymentOption['PaymentOption'];
    } else {
      $walletPaymentOptionId = $paymentOption['PaymentOption']['id'];
    }
  }
?>

<div id="buy-application">
</div>
<script type="text/javascript">
  $(document).ready(function() {
    window.deal = <?php echo json_encode($deal['Deal']) ?>,
    window.user = <?php echo json_encode($user['User']) ?>,
    window.buyOptions = {
      el: "#buy-application",
      deal: { id: deal.id, title: deal.name, price: deal.discounted_price, buy_max_quantity_per_user: deal.buy_max_quantity_per_user, buy_min_quantity_per_user: deal.buy_min_quantity_per_user, category_name: deal.category_name},
      user: { id: user.id, email: user.email, wallet: user.available_balance_amount, token: user.token, quantity_buyed_per_user: user.quantity_buyed_per_user, qty_user_can_buy: user.qty_user_can_buy},
      paymentOptions: <?php echo json_encode($paymentOptionsArr) ?>,
      serverParams: <?php echo json_encode($params) ?>,
      <?php if (!empty($dealExternal)) { ?>
        dealExternal: <?php echo json_encode($dealExternal) ?>,
      <?php } ?>
      bacCrearPagoSeguroUri: "<?php echo Configure::read('BAC.crearPagoSeguro'); ?>",
      bacCrearTransactionUri: "<?php echo Configure::read('BAC.crearTx'); ?>",
      <?php if (isset($walletPaymentOptionId)) { ?>
      walletPaymentOptionId: "<?php echo $walletPaymentOptionId; ?>",
      <?php } ?>
    };
    window.router = new Buy.Routers.BuyAppRouter(buyOptions);

    Backbone.history.start({pushState: true, root: "/api/api_deals/buy/"});
    <?php if (!empty($dealExternal)) { ?>
      <?php if(!empty($payStatus) && $payStatus==='aceptado') { ?>
        window.router.navigate('successBuy', true);
      <?php } elseif( $payStatus==='cancelado') { ?>
        window.router.navigate('errorBuy', true);
      <?php } ?>
    <?php } else { ?>
        window.router.navigate('', true);
    <?php } ?>
  });
</script>