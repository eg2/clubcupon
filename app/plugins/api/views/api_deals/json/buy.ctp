<?php $api->set('params', $params); ?>
<?php $api->set('deal', $deal['Deal']); ?>
<?php
  $paymentOptionsArr = array();
  foreach($paymentOptions as $paymentOption) {
    if($paymentOption['PaymentOption']['payment_type_id']!=5000) {
      $paymentOptionsArr[] = $paymentOption['PaymentOption'];
    }
  }
  $api->set('paymentOptions', $paymentOptionsArr); ?>
<?php $api->set('paymentOptionsWithType', $paymentOptions); ?>
<?php $api->set('user', $user['User']); ?>
<?php $api->set('dealExternal', $dealExternal['DealExternal']); ?>
