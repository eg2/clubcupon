<?php
class ApiAppModel extends AppModel {

  const FIRST_PAGE = 1;

  private $limit = null;
  private $page = null;
  public $orders = null;
  
  public $recursive = -1;

  public $actsAs = array(
    'Containable',
    'RowObject',
  );

  function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
    $this->page = self::FIRST_PAGE;
    $this->limit = Configure::read('ApiModel.find.pagelimit');
  }

  public function getLastQuery() {
    if(Configure::read('debug')==2) {
      $dbo = $this->getDatasource();
      $logs = $dbo->_queriesLog;
      return end($logs);
    } else {
      return false;
    }
  }

  public function getQueryLog() {
    if(Configure::read('debug')==2) {
      $dbo = $this->getDatasource();
      $logs = $dbo->_queriesLog;
      return $logs;
    } else {
      return false;
    }
  }

  protected function getPageLimit($limit=null) {
    return !is_null($limit) ? $limit : $this->limit;
  }

  protected function getPage($page=null) {
    return !is_null($page) ? $page : $this->page;
  }

  public function getOrder($order=null) {
    if (empty($order)) {
      throw new Exception ('Parámetro $order no debe ser null en getOrder');
    }
    return $this->orders[$order];
  }
  
  public function logModel($message, $data=null, $level=null) {
    ApiLogger::getInstance()->log($message, $data, $level, $this->alias);
  }

  public function notice($message, $data=null) {
    $this->logModel($message, $data, LOG_NOTICE);
  }

  public function info($message, $data=null) {
    $this->logModel($message, $data, LOG_INFO);
  }

  public function debug($message, $data=null) {
    $this->logModel($message, $data, LOG_DEBUG);
  }

  public function trace($message, $data=null) {
    $this->logModel($message, $data, LOG_DEBUG);
  }
}
?>
