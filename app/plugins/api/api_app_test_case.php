<?php
App::import('vendor', 'Utility', array('file' => 'Utility/ApiLogger.php'));
class ApiAppTestCase extends CakeTestCase {

  function __construct() {
    $this->ApiLogger = new ApiLogger(get_class($this), 'test');
    parent::__construct();
  }
  function startCase() {
    echo get_class($this).":\n";
    $this->ApiLogger->notice('Iniciando case.');
    parent::startCase();
  }

  function startTest($method) {
    echo '..'.$method."\n";
    $this->ApiLogger->put('test', $method);
    $this->ApiLogger->notice('Iniciando test.');
    parent::startTest($method);
  }

  function endTest($method) {
    $this->ApiLogger->notice('Test terminado.');
    $this->ApiLogger->reset();
    parent::endTest($method);
  }

  /**
   * Overrides SimpleTestCase::assert to enable calling of skipIf() from within tests
   */
  function assert(&$expectation, $compare, $message = '%s') {
    $result = parent::assert($expectation, $compare, $message);
    $assertArr = array(
        'expectation' => $expectation,
        'compare' => $compare,
        'message' => $message,
        'result' => $result
      );
    if($result) {
      $this->ApiLogger->debug('assert cumplido', $assertArr);
    } else {
      $this->ApiLogger->debug('assert no cumplido', $assertArr);
    }
    return $result;
  }


  function endCase() {
    $this->ApiLogger->notice('Case terminado.');
    parent::endCase();
  }

  function before($method) {
    // $this->ApiLogger->trace('Before method.', array('method' => $method));
    parent::before($method);
  }

  function start() {
//    $this->ApiLogger->trace('Start.');
    parent::start();
  }

  function end() {
//    $this->ApiLogger->trace('End.');
    parent::end();
  }

  function after($method) {
    // $this->ApiLogger->trace('After method.', array('method' => $method));
    parent::after($method);
  }

}
?>
