<?php
class ApiPreredemption extends ApiAppModel {
  public $name       = 'ApiPreredemption';
  public $alias      = 'Preredemption';
  public $useTable   = 'preredemptions';
  public $actsAs = array('SoftDeletable' => array('find' => true));
  public $recursive = -1;

  public function getUnassignedPreredemptionSlot($deal_id) {

    $res = $this->find('first', array(
        'fields' => array('id', 'posnet_code'),
        'conditions' => array(
          'deals_id' => $deal_id,
          'is_assigned' =>0,
        ),
        'recursive' => -1
      ));

    return $res;
  }

  public function assignPreredemptionSlot($preredemption_id) {
    return  $this->save(array(
        'Preredemption' => array(
          'id' => $preredemption_id,
          'is_assigned' => true,
        )
      ));
  }


}


?>
