<?php
class ApiPaymentOption extends ApiAppModel{
  public $name = 'ApiPaymentOption';
  public $alias = 'PaymentOption';
  public $useTable = 'payment_options';
  public $recursive = -1;
  public $belongsTo = array(
      'PaymentMethod' => array(
        'className' => 'api.ApiPaymentMethod',
        'foreignKey' => 'payment_method_id',
      ),
      'PaymentSetting' => array(
        'className' => 'api.ApiPaymentSetting',
        'foreignKey' => 'payment_setting_id',
      ),
      'PaymentType' => array(
        'className' => 'api.ApiPaymentType',
        'foreignKey' => 'payment_type_id',
      ),
  );
  
  public $PaymentOptionsfields = array (
      'PaymentOption.id',
      'PaymentOption.payment_method_id',
      'PaymentOption.payment_setting_id',
      'PaymentOption.payment_type_id',
      'PaymentOption.name',
      'PaymentOption.description',
      'PaymentOption.is_offline',
  );
  
  public function findAllForIds($ids) {
       
    return $this->find('all', array (
                'fields' => $this->PaymentOptionsfields,
                'conditions' => 
                  array ('id' => $ids,),
                'recursive' => -1,
            )
          );
  }
  public function isPaymentOptionOffline($paymentOptionId) {
    $paymentOption = $this->findById($paymentOptionId);
    return $paymentOption['PaymentOption']['is_offline'];
  }
}


?>
