<?php

class ApiState extends ApiAppModel {

    const ARGENTINA_COUNTRY_ID = 284;

    public $name = 'ApiState';
    public $useTable = 'states';
    public $displayField = 'name';
    public $recursive = - 1;

    public function findAllForApi() {
        $fields = array(
            "ApiState.id",
            "ApiState.name"
        );
        $conditions = array(
            'ApiState.is_approved' => 1
        );
        return $this->find('list', array(
                    'ApiState.fields' => $fields,
                    'ApiState.conditions' => $conditions
        ));
    }

    public function findByCountryForApi($countryId) {
        $conditions = array(
            'ApiState.country_id' => $countryId,
            'ApiState.is_selectable' => 1,
            'ApiState.is_approved' => 1
        );
        $order = array(
            'ApiState.name'
        );
        return $this->find('list', array(
                    'ApiState.conditions' => $conditions,
                    'ApiState.order' => $order
        ));
    }

    public function findAllArgentineStates() {
        $conditions = array(
            'ApiState.country_id' => self::ARGENTINA_COUNTRY_ID,
            'ApiState.is_selectable' => 1,
            'ApiState.is_approved' => 1
        );
        $order = array(
            'ApiState.name'
        );
        return $this->find('list', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

}
