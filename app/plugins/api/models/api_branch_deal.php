<?php

class ApiBranchDeal extends ApiAppModel {

  public $name = 'ApiBranchDeal';
  public $alias = 'BranchDeal';
  public $useTable = 'branches_deals';
  public $actsAs = array('SoftDeletable' => array('find' => true));
  public $belongsTo = array(
    'Branch' => array (
      'className' => 'api.ApiBranch',
      'foreignKey' => 'now_branch_id',
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'counterCache' => true
    ),
    'Deal' => array (
      'className' => 'api.ApiDeal',
      'foreignKey' => 'now_deal_id',
      'conditions' => '',
      'counterCache' => true
    ),
  );
  public $recursive = -1;
  private $activeConditions = array(
    'BranchDeal.now_deal_status_id' =>
    array(
      ApiDeal::STATUS_OPEN,
      ApiDeal::STATUS_TIPPED
    ),
    'BranchDeal.deleted' => 0,
  );

  public function findByDeal($deal) {
    
    return $this->find('all', array(
      'fields' => array(
        'BranchDeal.*',
        'Branch.street',
        'Branch.number'
      ),
      'contain'=> array(
         'Branch',
      ),
      'conditions' => array(
        'now_deal_id' => $deal['Deal']['id']
      ),
      'recursive' => 1
    ));
  }

  private function findForApi( $findMethod, $extraConditions=null,
    $fields=null, $limit=null, $page=null) {

    if (is_null(($fields))) {
      $fields = array('BranchDeal.*');
    }

    return  $this->find($findMethod, array(
      'fields' => $fields,
      'contain' => array ('Deal'),
      'conditions' => $this->getConditionsForActive($extraConditions),
      'recursive' => 1,
      'limit' => $this->getPageLimit($limit),
      'page' => $this->getPage($page),
    ));
  }

  private function getConditionsForActive($extraConditions) {
    if(!empty($extraConditions)) {
      return array_merge($this->activeConditions, $extraConditions);
    } else {
      return $this->activeConditions;
    }
  }

  public function findDistinctDealIdsByLatitudeAndLongitude(
    $lat=null, $long=null, $cityId=null, $limit=null, $page=null) {

    $geoDelta = Configure::read('API.geo.delta');
    $extra_conditions['BranchDeal.latitud  <= '] = $lat  + $geoDelta;
    $extra_conditions['BranchDeal.latitud  >= '] = $lat  - $geoDelta;
    $extra_conditions['BranchDeal.longitud <= '] = $long + $geoDelta;
    $extra_conditions['BranchDeal.longitud >= '] = $long - $geoDelta;

    $fields = array('DISTINCT (BranchDeal.now_deal_id) as deal_id');

    if (!is_null($cityId)) {
      $extra_conditions['Deal.city_id']= $cityId;
    }

    return $this->findForApi(
      'all', $extra_conditions, $fields, $limit, $page
    );
  }

}
