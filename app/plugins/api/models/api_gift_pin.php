<?php
class ApiGiftPin extends ApiAppModel {
  public $name       = 'ApiGiftPin';
  public $alias      = 'GiftPin';
  public $useTable   = 'gift_pins';
  public $displayField = 'code';
  public $recursive = -1;

  public function isBlocked($giftPin) {
      return $giftPin['GiftPin']['blocked'] == 1;
  }

  public function block($giftPin) {
    $giftPin['GiftPin']['blocked'] = 1;
    if(!$this->save($giftPin)) {
      throw new Exception('Error al Blockear el GifPin '.$giftPin["GiftPin"]["id"]);
    }
    return $giftPin;
  }

  public function unBlock($giftPin) {
    $giftPin['GiftPin']['blocked'] = 0;
    if(!$this->save($giftPin)) {
      throw new Exception('Error al Desblockear el el GifPin '.$giftPin["GiftPin"]["id"]);
    }
    return $giftPin;
  }

}
