<?php
class ApiUser extends ApiAppModel {

  const USER_ACTIVE = 1;
  
  const USER_TYPE_ADMIN = 1;
  const USER_TYPE_USER = 2;
  const USER_TYPE_COMPANY = 3;
  const USER_TYPE_AGENCY = 4;
  const USER_TYPE_PARTNER = 5;
  const USER_TYPE_SUPER_ADMIN = 6;
  const USER_TYPE_SELLER = 7;
  
  public $name = 'ApiUser';
  public $alias = 'User';
  public $useTable = 'users';
  public $recursive = -1;
  public $validate = array(
    'email' => array(
      'rule3' => array(
        'rule' => 'isUnique',
        'message' => 'La dirección de mail ya existe'
      ) ,
      'rule2' => array(
        'rule' => 'email',
        'message' => 'Debe ser un mail válido'
      ) ,
      'rule1' => array(
        'rule' => 'notempty',
        'message' => 'Ingresá un mail'
      )
    ),
    'password' => array(
      'rule2' => array(
        'rule' => array('minLength', 6) ,
        'message' => 'Mínimo 6 caracteres'
      ),
      'rule1' => array(
        'rule' => 'notempty',
        'message' => 'Ingresá una contraseña'
      ),
    ),
    'username' => array(
        'rule6' => array(
            'rule' => array(
                'minLength',
                '3'
                ) ,
        'message' => 'Debe ser mínimo de 3 caracteres'
        ) ,
        'rule5' => array(
            'rule' => array(
                'maxLength',
                '20'
            ) ,
            'message' => 'Máximo 20 caracteres'
        ) ,
        'rule4' => array(
            'rule' => 'alphaNumeric',
            'message' => 'Sólo letras o números, sin espacios u otros signos'
        ) ,
        'rule3' => array(
            'rule' => 'isUnique',
            'message' => 'El nombre de usuario ya existe'
        ) ,
        'rule1' => array(
            'rule' => 'notempty',
            'message' => 'Ingresá un nombre de usuario'
        )
      ) 
  );

  private $expiryTime = null;
  private $checkExpirity = null;
  private $timeThreshold = null;
  private $maxCallForTimeThreshold = null;
  private $checkNumberOfCalls = null;
  private $securitySalt = null;
  
  public $hasOne = array(
  		'UserProfile' => array(
  				'className' => 'api.UserProfile',
  				'foreignKey' => 'user_id',
  		),
  );

  function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
    $this->expiryTime = Configure::read('API.user.token_expiry_time');
    $this->checkExpirity = Configure::read('API.user.token_check_expirity');
    $this->timeThreshold = Configure::read('API.time');
    $this->maxCallForTimeThreshold = Configure::read('API.maximum');
    $this->checkNumberOfCalls = Configure::read('API.check_number_of_calls');
    $this->securitySalt = Configure::read('Security.salt');
  }

  public $activeUser = array (
     'User.is_active' => self::USER_ACTIVE,
  );

  public $apiUserFields = array (
      'username',
      'email',
      'id',
  );
  /**
   * Valida si el token ingresado es valido
   * y si no se a superado el limite de llamadas.
   * Registra la cantidad de llamadas en el umbral de tiempo actual.
   *
   * el limite de llamdas.
   *
   * @param unknown $user (optional)
   * @return user_id o false si no es valido o si a superado
   */
  public function useToken($user=null) {
    if ($this->isTokenUsable($user)) {
      $this->updateNumberOfTokenCalls($user);
    }

    return $user;
  }

  public function isTokenUsable($user=null) {
    if (empty($user['User']['token'])) {
      $this->debug("El token es vacio. El token no es usable.");
      return false;
    }

    if ($this->checkExpirity
      && $this->hasExpired($user)) {
      $this->debug("El token a expirado. El token no es usable.");
      return false;
    }

    if ($this->checkNumberOfCalls
      && $this->hasExceededAllowedNumberOfTokenCalls($user)) {
      $this->debug("El usuario a superado el maximo de llamadas. El token no es usable.", $user);
      return false;
    }

    return true;
  }

  private function hasExpired($user) {
    if (empty($user)) {
      throw new Exception('User must not be empty');
    }

    if (!isset($user['User']['token_used'])) {
      return false;
    }

    $now = time();
    $tokenExpirityTime = $this->getTokenExpiryTime($user);
    $hasExpired = $now >= $tokenExpirityTime;
    if($hasExpired) {
      $this->debug(
        'Evaluando expiracion de token de usuario, el token a expirado',
        array(
          'now' => date('Y-m-d H:i:s', $now),
          'expirityTime' => date('Y-m-d H:i:s', $tokenExpirityTime),
          'tokenUsed' => date('Y-m-d H:i:s', $user['User']['token_used'])
        )
      );
    }
    return $hasExpired;
  }

  private function getTokenExpiryTime($user) {
    if (isset($user['User']['token_used']) && !empty($user['User']['token_used'])) {
      return strtotime('+'.$this->expiryTime, strtotime($user['User']['token_used']));
    }
    return false;
  }

  /**
   * Retorna el momento donde termina el umbral de tiempo actual.
   *
   * @param unknown $user
   * @param unknown $waitingTime (optional)
   * @return unknown
   */
  private function getTokenCurrentTimeThreshold($user, $waitingTime=null) {
    if (isset($user['User']['token_used']) && !empty($user['User']['token_used'])) {
      return strtotime('+'.$this->timeThreshold, strtotime($user['User']['token_used']));
    }
    return false;
  }

  /**
   * Retorna el momento donde inicio el umbral de tiempo actual.
   *
   * @param unknown $user
   * @return unknown
   */
  private function getTokenUsed($user) {
    return $user[$this->alias]['token_used'];
  }

  /**
   * Retorna la cantidad de llamadas realizadas en el umbral de tiempo actual.
   *
   * @param unknown $user
   * @return unknown
   */
  private function getTokenUses($user) {
    return $user[$this->alias]['token_uses'];
  }

  /**
   * Retorna si a superado el limite de llamadas a la api.
   *
   * @param unknown $user
   * @return unknown
   */
  private function hasExceededAllowedNumberOfTokenCalls($user) {
    if (empty($user)) {
      throw new Exception('User must not be empty');
    }

    $tokenUsed = $this->getTokenUsed($user);
    $tokenUses = $this->getTokenUses($user);
    $tokenCurrentTimeThreshold = $this->getTokenCurrentTimeThreshold($user);
    $now = time();


    if (!empty($tokenUsed)
      && $now <= $tokenCurrentTimeThreshold
      && $tokenUses >= $this->maxCallForTimeThreshold) {

      return true;
    }

    return false;
  }

  /**
   * Retorna si ya se encuentra en un umbral de tiempo a verificar
   * la cantidad de llamadas.
   *
   * @param unknown $user
   * @return unknown
   */
  private function isInTokenTimeThreshold($user) {
    $tokenCurrentTimeThreshold = $this->getTokenCurrentTimeThreshold($user);
    $now = time();
    return $now <= $tokenCurrentTimeThreshold; // si no supero el umbral
  }

  /**
   * Registra la cantidad de llamadas en el umbral de tiempo.
   *
   * @param unknown $user
   */
  private function updateNumberOfTokenCalls($user) {
    $this->id = $user['User']['id'];
    if ($this->isInTokenTimeThreshold($user)) {
      $this->saveField('token_uses', $user['User']['token_uses'] + 1);
    } else {
      $this->save(
        array(
          'token_used' => date('Y-m-d H:i:s'),
          'token_uses' => 1
        ),
        false,
        array(
          'token_used',
          'token_uses'
        )
      );
    }
  }

  public function token($user) {
    $this->id = $user['User']['id'];
    $user['User']['token_used'] = date('Y-m-d H:i:s');
    $user['User']['token_uses'] = 0;
    $user['User']['token'] = sha1(String::uuid());
    return $this->save($user);
  }

  public function isUserWalletBlocked($data) {

    if ($data ['User']['wallet_blocked']) {
      return true;
    }
    return false;
  }

  public function blockWallet($user_id) {
    $this->id = $user_id;
    return $this->saveField ('wallet_blocked',
      true,
      false);
  }

  public function unBlockWallet($user_id) {
    $this->id = $user_id;
    return $this->saveField ('wallet_blocked',
      false,
      false);
  }
  
  public function findActiveByEmail($extraConditions){
     
    $conditions = array_merge($this->activeUser, $extraConditions);
    
    return $this->find('first', array(
        'fields' => $this->apiUserFields,
        'conditions' =>   $conditions
      ));
  }
  
  public function getResetPasswordHash($userId) {
    return md5($userId .
            '-' .
            date('y-m-d') .
            $this->securitySalt);
  }

  public function isBacCharged($user) {
    return empty($user['User']['bac_id']);
  }
  
  public function canBuy($user) {
    return (!$this->isCompany($user));
  }
  
  public function isCompany($user){
    return ($user['User']['user_type_id']==self::USER_TYPE_COMPANY);
  }
  
  public function isWalletEnabled($user) {
        return $user['User']['available_balance_amount'] > 0 && !$this->isUserWalletBlocked($user);
  }
  
  public function findByUserNameWithProfile($user_name) {
  	$extraConditions= array('User.username' => $user_name);
  	$conditions = array_merge($this->activeUser, $extraConditions);
  	
  	$user = $this->find(
  			'first',
  			array(
  					
  					'conditions' =>   $conditions,
  					'contain' => array(
  							'UserProfile' => array(
  									'fields' => array(
  											'UserProfile.zip_code',
  											'UserProfile.gender',
  											'UserProfile.dob',
  									)
  							)
  					),
  					'recursive' => 1
  			)
  	);
  	return $user;
  }
  public function findByEmailWithProfile($email) {
  	$extraConditions= array('User.email' => $email);
  	$conditions = array_merge($this->activeUser, $extraConditions);
  	 
  	$user = $this->find(
  			'first',
  			array(
  					'conditions' =>   $conditions,
  					'contain' => array(
  							'UserProfile' => array(
  									'fields' => array(
  											'UserProfile.zip_code',
  											'UserProfile.gender',
  											'UserProfile.dob',
  									)
  								)
  					),
  					
  					'recursive' => 1
  			)
  	);
  	return $user;
  }
}
