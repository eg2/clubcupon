<?php
class ApiPaymentMethod extends ApiAppModel {
  public $name = 'ApiPaymentMethod';
  public $alias = 'PaymentMethod';
  public $useTable = 'payment_methods';
  public $recursive = -1;
}

