<?php
class ApiTransaction extends ApiAppModel {
  public $name        = 'ApiTransaction';
  public $alias       = 'Transaction';
  public $useTable    = 'transactions';
  public $recursive   = -1;
  
  public function settingValues($dealUser, $last_inserted_id, $price, $transaction_type_id) {

    $transaction['Transaction']['user_id'] = $dealUser['DealUser']['user_id'];
    $transaction['Transaction']['foreign_id'] = $last_inserted_id;
    $transaction['Transaction']['class'] = 'DealUser';
    $transaction['Transaction']['amount'] = $price;
    $transaction['Transaction']['transaction_type_id'] = $transaction_type_id;
      
    return $transaction;
  }

  public function log($data) {
    if (empty($data)) {
      throw new Exception('Data not be empty');
    }

    $this->create();
    $this->save($data);
    return $this->getLastInsertId();
  }


}


?>
