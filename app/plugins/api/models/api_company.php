<?php

class ApiCompany extends ApiAppModel {

    public $name = 'ApiCompany';
    public $alias = 'Company';
    public $useTable = 'companies';
    public $recursive = -1;
    public $displayField = 'name';
    var $belongsTo = array(
        'City' => array(
            'className' => 'api.ApiCity',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
    );

    function findByIdForApi($company_id) {
        return $this->find('first', array('conditions' => array('Company.id' => $company_id),
                    'fields' => array('Company.id', 'Company.name', 'City.name'),
                    'recursive' => 1)
        );
    }

    function findBySlugForApi($company_slug) {
        return $this->find('first', array('conditions' => array('Company.slug' => $company_slug),
                    'fields' => array('Company.id', 'Company.name', 'City.name'),
                    'recursive' => 1)
        );
    }

    public function findByUserId($userId) {
        $conditions = array(
            'Company.user_id' => $userId
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

}
