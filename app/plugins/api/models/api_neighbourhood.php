<?php
class ApiNeighbourhood extends AppModel {

  public $name = 'ApiNeighbourhood';
  public $alias = 'Neighbourhood';
  public $useTable = 'neighbourhoods';
  public $recursive = -1;
  public $displayField = 'name';

    public $belongsTo = array(
        'City' => array(
            'className' => 'api.ApiCity',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
      );

  public function findAllForApi($extraConditions) {
    $neighbourhoods = $this->find('all', array(
      'conditions' => $extraConditions
    ));
    return $neighbourhoods;
  }
  
  public function findByCityForApi($parent_id){
  	return $this->find('list',
				array ('conditions' =>
				array ('city_id' => $parent_id,
				'is_selectable' => true,
				),
				'order' => 'name'));
  }
  
}
