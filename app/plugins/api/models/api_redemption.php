<?php

App::import("Model", 'api.ApiDealUser');
App::import("Model", 'api.ApiDeal');

class ApiRedemption extends ApiAppModel {

    const POSNET_CODE_IVR_PREFIX = "62797603";
    const WAY_POSNET_WEB = "'posnet_web'";
    const WAY_IVR = "'ivr'";
    const WAY_BAC = "'bac'";
    const BAC_SERVICE = "'bac'";

    public $name = 'ApiRedemption';
    public $alias = 'Redemption';
    public $useTable = 'redemptions';
    public $recursive = - 1;
    public $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function updateAsRedeemed($redemption, $discountedAmount, $way) {
        $ahora = date('Y-m-d H:i:s');
        return $this->updateAll(array(
                    'Redemption.way' => $way,
                    'Redemption.discounted_price' => $discountedAmount,
                    'Redemption.redeemed' => "'$ahora'"
                        ), array(
                    'Redemption.id' => $redemption['Redemption']['id'],
        ));
    }

    function updateAsNotRedeemed($redemption) {
        return $this->updateAll(array(
                    'Redemption.way' => null,
                    'Redemption.discounted_price' => 0,
                    'Redemption.redeemed' => null
                        ), array(
                    'Redemption.id' => $redemption['Redemption']['id'],
        ));
    }

    function updateAsRedeemedByIvr($redemption, $discountedAmount, $modified_by = null) {
        return $this->updateAsRedeemed($redemption, $discountedAmount, self::WAY_IVR);
    }

    public function isRedeemed($redemption) {
        return !is_null($redemption['Redemption']['redeemed']);
    }

    public function getCompanyNumbers($page, $pagesize) {
        $query = "
      SELECT  distinct Company.id
      FROM redemptions Redemption
      INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
      INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
      INNER JOIN users User ON User.id = DealUser.user_id
      INNER JOIN companies Company ON Company.id = Deal.company_id
      where Redemption.posnet_code  > 627976030000000000
      AND Redemption.way is null
      ORDER BY Company.id  asc limit " . $page . "," . $pagesize;
        $results = $this->query($query);
        if (empty($results)) {
            return false;
        }
        return $results;
    }

    public function getPosnetCodes($page, $pagesize) {
        $query = "
      SELECT  Company.id, Redemption.posnet_code
      FROM redemptions Redemption
      INNER JOIN deal_users DealUser ON Redemption.deal_user_id = DealUser.id
      INNER JOIN deals Deal ON Deal.id = DealUser.deal_id
      INNER JOIN users User ON User.id = DealUser.user_id
      INNER JOIN companies Company ON Company.id = Deal.company_id
      where Redemption.posnet_code  > 627976030000000000
      AND Redemption.way is null
      ORDER BY Company.id  asc limit " . $page . "," . $pagesize;
        $results = $this->query($query);
        if (empty($results)) {
            return false;
        }
        return $results;
    }

    public function findFirstByPosnetCode($posnetCode) {
        return $this->find('first', array(
                    'conditions' => array(
                        'Redemption.posnet_code ' => $posnetCode
                    )
        ));
    }

}
