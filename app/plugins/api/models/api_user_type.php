<?php
class ApiUserType extends ApiAppModel {
  public $name = 'ApiUserType';
  public $alias = 'UserType';
  public $useTable = 'user_types';
  public $recursive = -1;
  
  const Admin = 1;
  const User = 2;
  const Company = 3;
  const Agency = 4;
  const Partner = 5;
  const SuperAdmin = 6;
  const Seller = 7;
}

?>
