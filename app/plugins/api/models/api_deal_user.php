<?php

class ApiDealUser extends ApiAppModel {

    public $name = 'ApiDealUser';
    public $alias = 'DealUser';
    public $useTable = 'deal_users';
    public $recursive = - 1;
    public $actsAs = array(
        'SoftDeletable' => array(
            'find' => true
        )
    );
    public $belongsTo = array(
        'Deal' => array(
            'className' => 'api.ApiDeal',
            'foreignKey' => 'deal_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'DealExternal' => array(
            'className' => 'api.ApiDealExternal',
            'foreignKey' => 'deal_external_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasOne = array(
        'Redemption' => array(
            'className' => 'api.ApiRedemption',
            'foreignKey' => 'deal_user_id'
        )
    );
    public $dealFields = array(
        'Deal.company_id'
    );
    public $dealUserFields = array(
        'DealUser.id'
    );

    public function findForApiByUserId($userId) {
        return $this->find('all', array(
                    'fields' => array(
                        'Deal.coupon_condition',
                        'Deal.coupon_expiry_date',
                        'Deal.coupon_highlights',
                        'Deal.custom_company_address1',
                        'Deal.custom_company_address2',
                        'Deal.custom_company_city',
                        'Deal.custom_company_contact_phone',
                        'Deal.custom_company_country',
                        'Deal.custom_company_name',
                        'Deal.custom_company_state',
                        'Deal.custom_company_zip',
                        'Deal.descriptive_text',
                        'Deal.id',
                        'Deal.is_now',
                        'Deal.name',
                        'Deal.slug',
                        'Deal.subtitle',
                        'Deal.original_price',
                        'Deal.discount_percentage',
                        'Deal.discounted_price',
                        'DealUser.coupon_code',
                        'DealUser.deal_id',
                        'DealUser.discount_amount',
                        'DealUser.id',
                        'DealUser.is_paid',
                        'DealUser.is_returned',
                        'DealUser.is_used',
                        'DealUser.pin_code',
                        'DealUser.quantity',
                        'DealExternal.external_status',
                        'Redemption.posnet_code'
                    ),
                    'conditions' => $this->getConditionsForAvailableOrPending(array(
                        'DealUser.user_id' => $userId
                    )),
                    'contain' => array(
                        'Redemption',
                        'Deal',
                        'DealExternal'
                    ),
                    'order' => array(
                        'DealUser.id' => 'ASC'
                    ),
                    'recursive' => 1
        ));
    }

    private function getConditionsForAvailable($extraConditions) {
        $availableConditions = array();
        $availableConditions['DealUser.emailed'] = 1;
        $availableConditions['DealUser.is_used'] = 0;
        $availableConditions['DealUser.is_gift'] = 0;
        $availableConditions['Deal.deal_status_id'] = array(
            ApiDeal::STATUS_CLOSED,
            ApiDeal::STATUS_TIPPED,
            ApiDeal::STATUS_PAIDTOCOMPANY
        );
        $availableConditions['DATE(Deal.coupon_expiry_date) >='] = date('Y-m-d');
        if (!empty($extraConditions)) {
            return array_merge($availableConditions, $extraConditions);
        }
        return $availableConditions;
    }

    private function getConditionsForPending($extraConditions) {
        $pendingConditions['Deal.deal_status_id'] = array(
            ApiDeal::STATUS_CLOSED,
            ApiDeal::STATUS_OPEN,
            ApiDeal::STATUS_TIPPED,
            ApiDeal::STATUS_PAIDTOCOMPANY
        );
        $pendingConditions['DealUser.emailed'] = 0;
        $pendingConditions['DealUser.is_used'] = 0;
        $pendingConditions['DealUser.is_gift'] = 0;
        $pendingConditions['DATE(Deal.coupon_expiry_date) >='] = date('Y-m-d');
        if (!empty($extraConditions)) {
            return array_merge($pendingConditions, $extraConditions);
        }
        return $pendingConditions;
    }

    private function getConditionsForAvailableOrPending($extraConditions = null) {
        $availableOrPendingConditions = $this->getConditionsForAvailable($this->getConditionsForPending());
        unset($availableOrPendingConditions["DealUser.emailed"]);
        if (!empty($extraConditions)) {
            return array_merge($availableOrPendingConditions, $extraConditions);
        }
        return $availableOrPendingConditions;
    }

    public function countQuantityForDealId($dealId) {
        $extraConditions = array(
            'DealUser.deal_id' => $dealId
        );
        $groupFields = array(
            'DealUser.deal_id'
        );
        return $this->countQuantity($extraConditions, $groupFields);
    }

    public function countQuantityForDealIdAndUserID($dealId, $userId) {
        $extraConditions = array(
            'DealUser.deal_id' => $dealId,
            'DealUser.user_id' => $userId
        );
        $groupFields = array(
            'DealUser.deal_id',
            'DealUser.user_id'
        );
        return $this->countQuantity($extraConditions, $groupFields);
    }

    private function countQuantity($extraConditions, $groupFields) {
        $count = $this->find('first', array(
            'conditions' => $extraConditions,
            'fields' => array(
                'SUM(DealUser.quantity) as total_count'
            ),
            'group' => $groupFields,
            'recursive' => - 1
        ));
        return $count['0']['total_count'];
    }

    public function updateAsUsed($dealUser) {
        $belongsTo = $this->belongsTo;
        $hasOne = $this->hasOne;
        $this->unbindModel(array(
            'belongsTo' => array_keys($belongsTo),
            'hasOne' => array_keys($hasOne)
                ), true);
        return $this->updateAll(array(
                    'DealUser.is_used' => 1
                        ), array(
                    'DealUser.id' => $dealUser['DealUser']['id']
        ));
    }

    public function updateAsNotUsed($dealUser) {
        return $this->updateAll(array(
                    'DealUser.is_used' => 0
                        ), array(
                    'DealUser.id' => $dealUser['DealUser']['id']
        ));
    }

    public function findAllPagedByCompanyId($companyId, $start_date, $end_date, $page, $size) {
        $fields = array(
            'Deal.descriptive_text',
            'Redemption.posnet_code',
            'Deal.name',
            'DealUser.coupon_code',
            'User.username',
            'Company.name',
            'Company.address1',
            'Company.address2',
            'Deal.custom_company_contact_phone',
            'Deal.custom_company_city',
            'Deal.coupon_expiry_date',
            'DealUser.created',
            'Deal.coupon_condition',
            'User.id',
            'DealUser.gift_dni',
            'DealUser.is_used'
        );
        $conditions = array(
            'Company.id' => $companyId,
            'DealUser.created >=' => $start_date,
            'DealUser.created <=' => $end_date
        );
        $joins = array(
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'conditions' => array(
                    'DealUser.deal_id = Deal.id'
                )
            ),
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'inner',
                'conditions' => array(
                    'Deal.company_id = Company.id'
                )
            ),
            array(
                'table' => 'users',
                'alias' => 'User',
                'type' => 'inner',
                'conditions' => array(
                    'User.id = DealUser.user_id'
                )
            ),
            array(
                'table' => 'redemptions',
                'alias' => 'Redemption',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'DealUser.id = Redemption.deal_user_id'
                )
            )
        );
        $return = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'joins' => $joins,
            'limit' => $page . ', ' . $size
        ));
        return $return;
    }

    public function findByCompanyIdAndPosnetCode($companyId, $posnetCode) {
        $fields = array(
            'Deal.descriptive_text',
            'Redemption.posnet_code',
            'Deal.name',
            'DealUser.coupon_code',
            'User.username',
            'Company.name',
            'Company.address1',
            'Company.address2',
            'Deal.custom_company_contact_phone',
            'Deal.custom_company_city',
            'Deal.coupon_expiry_date',
            'DealUser.created',
            'Deal.coupon_condition',
            'User.id',
            'DealUser.gift_dni',
            'DealUser.is_used'
        );
        $conditions = array(
            'Redemption.posnet_code' => $posnetCode,
            'Company.id' => $companyId
        );
        $joins = array(
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'DealUser.deal_id = Deal.id'
                )
            ),
            array(
                'table' => 'companies',
                'alias' => 'Company',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'Deal.company_id = Company.id'
                )
            ),
            array(
                'table' => 'users',
                'alias' => 'User',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'User.id = DealUser.user_id'
                )
            ),
            array(
                'table' => 'redemptions',
                'alias' => 'Redemption',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'DealUser.id = Redemption.deal_user_id'
                )
            )
        );
        $return = $this->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'joins' => $joins
        ));
        return $return;
    }

    public function findFirstByPinCode($pinCode, $extraConditions) {
        $conditions = array(
            'pin_code' => $pinCode
        );
        if (!empty($extraConditions)) {
            $conditions = array_merge($conditions, $extraConditions);
        }
        $fields = array_merge($this->dealFields, $this->dealUserFields);
        $dealUser = $this->find('first', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'contain' => array(
                'Deal'
            )
        ));
        return $dealUser;
    }

    public function findAllByDealExternalId($deal_external_id) {
        $fields = array(
            'Redemption.posnet_code',
            'Redemption.redeemed'
        );
        $conditions = array(
            'DealUser.deal_external_id' => $deal_external_id,
            'DealUser.deleted' => 0
        );
        $joins = array(
            array(
                'table' => 'redemptions',
                'alias' => 'Redemption',
                'type' => 'inner',
                'foreignKey' => false,
                'conditions' => array(
                    'DealUser.id = Redemption.deal_user_id'
                )
            )
        );
        $return = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'joins' => $joins
        ));
        return $return;
    }

    public function findFirstByPosnetCode($posnet_code) {
        $fields = array(
            'DealUser.id',
            'Redemption.posnet_code',
            'DealExternal.bac_id',
            'DealExternal.id'
        );
        $conditions = array(
            'Redemption.posnet_code' => $posnet_code,
            'DealUser.deleted' => 0
        );
        $contain = array(
            'Redemption',
            'DealExternal'
        );
        $return = $this->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'contain' => $contain,
            'recursive' => 1
        ));
        return $return;
    }

}
