<?php

class ApiCity extends ApiAppModel {

  
  public $name = 'ApiCity';
  public $alias = 'City';
  public $useTable = 'cities';
  public $displayField = 'name';
  public $recursive = -1;
  private $activeConditions = array(
        'City.is_approved' => 1,
        'City.is_hidden' => false,
        'City.active_deal_count !=' => 0,
      );

  private $excludeSlugs = null;
  private $externalHiddenCities = null;
  
  public function __construct() {
        parent::__construct();
        $this->excludeSlugs = 
                Configure::read('corporate.city_name');
        $this->externalHiddenCities = 
                Configure::read('corporate.external_hidden_cities');
  }
  public function findAllForApi() {
    return $this->find('all', array(
      'fields' => array(
        "City.id",
        "City.name",
        "City.slug",
        "City.latitude",
        "City.longitude",
        "City.is_group",
        "City.order",
        "City.is_solapa",
        "City.is_business_unit",
        "City.is_corporate",
      ),
      'conditions' => $this->getConditionsForActive(),
      'order' => array(
        'City.is_group' => 'ASC',
        'City.name' => 'ASC',
      ),
      'recursive' => -1
    ));
  }
  public function findAllBySlugs($slugs=array()){
  	
  	$slugsCondition=
            array( 'City.slug' =>  $slugs);
  	
  	$conditions=array_merge($this->getConditionsForActive(),$slugsCondition);

  	return $this->find('all', array(
  			'fields' => array(
  					"City.id",
  					"City.name",
  					"City.slug",
  					"City.latitude",
  					"City.longitude",
  					"City.is_group",
  					"City.order",
  					"City.is_solapa",
  					"City.is_business_unit",
  					"City.is_corporate",
  			),
  			'conditions' => $conditions,
  			'order' => array(
  					'City.is_group' => 'ASC',
  					'City.name' => 'ASC',
  			),
  			'recursive' => -1
  	));
  	
  }
  public function findByStateForApi($parent_id){
  	return $this->find ('list',
				array ('conditions' =>
				array ('state_id' => $parent_id,
				'is_selectable' => true,
				'is_group'=> 0,
				'is_approved' => true),
				'order' => 'name'));
  }
  
  public function findIdBySlug($slugcity){
      return $this->find(
              'first',
              array(
                  'fields' => array('City.id'),
                  'conditions' => array(
                      'City.slug' => $slugcity,
                  ),
                  'recursive' => -1,
              ));
  }

  private function getConditionsForActive() {
    $conditions = 
       array_merge($this->activeConditions,
               $this->getConditionForExcludedCities());
    return $conditions;
    
  }
  
  private function getConditionForExcludedCities() {
    $corporates = 
        array_merge($this->excludeSlugs, 
                $this->externalHiddenCities);
    $conditions = 
        array('NOT' => 
            array( 'City.slug' => $corporates));
    
    return $conditions;
  }
  public function isSubscriptionEnabledForCityId($cityId) {
      $city = $this->findById($cityId);
      return $city['City']['is_user_subscription_allowed'];
  }
}

