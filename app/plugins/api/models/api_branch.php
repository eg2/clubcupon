<?php

class ApiBranch extends ApiAppModel {

  public $name = 'ApiBranch';
  public $alias = 'Branch';
  public $useTable = 'branches';
  public $recursive = -1;
  public $actsAs = array('SoftDeletable' => array('find' => true));

  public function findByCompanyId($company_id) {
    return $this->find('all', array(
      'conditions' => array(
        'company_id' => $company_id
      ),
      'recursive' => - 1
    ));
  }

}
