<?php
class ApiEmailTemplate extends ApiAppModel {

  public $name  = 'ApiEmailTemplate';
  public $alias = 'EmailTemplate';
  public $useTable = 'email_templates';
  public $recursive = -1;

  public function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
    $this->validate = array(
      'from' => array(
        'rule' => 'notempty',
        'allowEmpty' => false,
        'message' => __l('Required')
      ) ,
      'subject' => array(
        'rule' => 'notempty',
        'allowEmpty' => false,
        'message' => __l('Required')
      ) ,
      'email_content' => array(
        'rule' => 'notempty',
        'allowEmpty' => false,
        'message' => __l('Required')
      )
    );
  }

  public function selectTemplate($tempName) {
    $emailTemplate = $this->find('first', array(
        'conditions' => array(
          'EmailTemplate.name' => $tempName
        ) ,
        'fields' => array(
          'EmailTemplate.email_content',
          'EmailTemplate.subject',
          'EmailTemplate.from',
          'EmailTemplate.reply_to',
          'EmailTemplate.is_html'
        ) ,
        'recursive' => -1
      ));
    $resultArray = array();
    foreach ($emailTemplate as $singleArrayEmailTemplate) {
      foreach ($singleArrayEmailTemplate as $key => $value) {
        $resultArray[$key] = $value;
      }
    }
    return $resultArray;
  }

  public function getContentDynamicTemplate($tempName) {
    $path = ROOT . DS . 'email_templates/' . $tempName . '.ctp';
    if (!file_exists($path)) {
      return false;
    }
    ob_start();
    include $path;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }


}


?>
