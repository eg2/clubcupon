<?php

class ApiUserProfile extends ApiAppModel {

  public $name = 'ApiUserProfile';
  public $alias = 'UserProfile';
  public $useTable = 'user_profiles';
  public $recursive = -1;
  public $validate = array(
    'dni' => array(
      'rule1' => array(
        'rule' => 'notempty',
        'message' => 'Ingresá un dni'
      ),
      'rule2' => array(
        'rule' => 'numeric',
        'message' => 'Ingresá sólo números.'
      ),
       
    ),
    'first_name' => array(
        'rule1' => array(
          'rule' => 'notempty',
          'message' => 'Ingresá tu nombre'
        )
    ),
    'last_name' => array(
        'rule1' => array(
          'rule' => 'notempty',
          'message' => 'Ingresá tu apellido'
        )
    ),
  );

  public function getUserProfile($userId) {
    return $this->find ('first', 
                        array (
                          'conditions' => array (
                            'UserProfile.user_id' => $userId,),
                        'recursive' => -1,
                      ));
  }
}
