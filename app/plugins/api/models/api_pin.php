<?php
class ApiPin extends ApiAppModel {
  public $name       = 'ApiPin';
  public $alias      = 'Pin';
  public $useTable   = 'pins';
  public $displayField = 'name';
  public $recursive = -1;
  var $actsAs = array('Containable');
  public $belongsTo = array(
    'Deal' => array (
      'className' => 'api.ApiDeal',
      'foreignKey' => 'deal_id',
    ),
    'DealUser' => array (
      'className'  => 'api.ApiDealUser',
      'foreignKey' => 'deal_user_id',
    ),
  );

  public function next($deal_id, $qty = 1) {
    $pins = $this->find ('all', array (
        'conditions' => array (
          'Pin.deal_id' => $deal_id,
          'Pin.is_used' => false,
        ),
        'fields' => array (
          'Pin.id',
          'Pin.code',
        ),
        'order' => array (
          'Pin.created'
        ),
        'limit' => $qty,
      ));

    if (count($pins) == $qty) {
      $pin_ids = array ();
      $codes = '';
      foreach ($pins as $pin) {
        $pin_ids [] = $pin ['Pin']['id'];
        $codes = $pin ['Pin']['code'] . "\n" . $codes;
      }
      $codes = substr($codes, 0, -1);
      $this->updateAll (array ('Pin.is_used' => true),
        array ('Pin.id' => $pin_ids));
      return array ('ids' => $pin_ids, 'code' => $codes);
    }

    return array ();
  }
  
  public function nextByProduct($product_id, $qty = 1) {
  	$pins = $this->find ('all', array (
  			'conditions' => array (
  					'Pin.product_id' => $product_id,
  					'Pin.is_used' => false,
  			),
  			'fields' => array (
  					'Pin.id',
  					'Pin.code',
  			),
  			'order' => array (
  					'Pin.created'
  			),
  			'limit' => $qty,
  	));
  
  	if (count($pins) == $qty) {
  		$pin_ids = array ();
  		$codes = '';
  		foreach ($pins as $pin) {
  			$pin_ids [] = $pin ['Pin']['id'];
  			$codes = $pin ['Pin']['code'] . "\n" . $codes;
  		}
  		$codes = substr($codes, 0, -1);
  		$this->updateAll (array ('Pin.is_used' => true),
  				array ('Pin.id' => $pin_ids));
  		return array ('ids' => $pin_ids, 'code' => $codes);
  	}
  
  	return array ();
  }

  function countPinsByDeal($deal_id) {
    $result = $this->find('count',
      array('conditions' => array('Pin.deal_id'=>$deal_id)));
    return !empty($result)?$result:0;
  }
  
  function findFirstByPinCode($code, $extraConditions) {
      $conditions = array('code' => $code,);
      if (!empty($extraConditions)) {
          $conditions = array_merge($conditions, $extraConditions);
      }

      $pin = $this->find('first', 
              array(
                  'conditions' => $conditions,
                  'contain' => array(
                      'DealUser', 
                      'Deal',
                      )
                  )
              );
      return $pin;
  }
}
?>
