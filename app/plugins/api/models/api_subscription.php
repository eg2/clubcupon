<?php

class ApiSubscription extends ApiAppModel {

    public $name = 'ApiSubscription';
    public $alias = 'Subscription';
    public $useTable = 'subscriptions';
    public $recursive = - 1;
    public $emailActiveConditions = array(
        'is_subscribed' => 1
    );
    public $subscriptionsFields = array(
        'Subscription.*'
    );

    public function findByCityIdAndUserId($city_id, $user_id) {
        $subscription = $this->find('first', array(
            'conditions' => array(
                'user_id' => $user_id,
                'city_id' => $city_id
            )
        ));
        return $subscription;
    }

    public function isEmailAndCitySuscribeAndActive($email, $city_id) {
        $extraConditions = array_merge(array(
            'email' => $email,
            'city_id' => $city_id
                ), $this->emailActiveConditions);
        $isSuscribe = $this->findForApi('first', $extraConditions);
        if (!empty($isSuscribe)) {
            return true;
        }
    }

    public function isEmailSuscribe($email, $city_id) {
        $extraConditions = array(
            'email' => $email,
            'city_id' => $city_id
        );
        $isSuscribe = $this->findForApi('first', $extraConditions);
        return $isSuscribe;
    }

    private function findForApi($findMethod, $extraConditions = null, $limit = null, $page = null) {
        return $this->find($findMethod, array(
                    'fields' => $this->subscriptionsFields,
                    'conditions' => $extraConditions,
                    'order' => array(
                        'Subscription.id' => 'desc'
                    ),
                    'recursive' => 1,
                    'limit' => $this->getPageLimit($limit),
                    'page' => $this->getPage($page)
        ));
    }

    public function getSubscribeByEmailToSave($email, $city_id, $ip) {
        $subscription['city_id'] = $city_id;
        $subscription['email'] = $email;
        $subscription['subscription_ip'] = $ip;
        return $subscription;
    }

}
