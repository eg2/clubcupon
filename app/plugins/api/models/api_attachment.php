<?php

class ApiAttachment extends ApiAppModel {

    const CLASS_DEAL = 'Deal';
    const CLASS_DEAL_MULTIPLE = 'DealMultiple';
    const CLASS_DEAL_NEWSLETTER = 'DealNewsLetter';
    const CLASS_NOW_DEAL = 'NowDeal';
    const CLASS_USER_AVATAR = 'UserAvatar';

    public $name = 'ApiAttachment';
    public $alias = 'Attachment';
    public $useTable = 'attachments';
    public $recursive = - 1;

    public function findFirstAttachementForDeal($deal) {
        if ($deal['Deal']['is_now']) {
            return $this->findAttachmentForForeingId($deal['Deal']['id'], self::CLASS_NOW_DEAL);
        } else {
            return $this->findAttachmentForForeingId($deal['Deal']['id'], self::CLASS_DEAL);
        }
    }

    private function findAttachmentForForeingId($foreingId, $foreingClass, $findMethod = 'first') {
        return $this->find($findMethod, array(
                    'conditions' => array(
                        'Attachment.foreign_id = ' => $foreingId,
                        'Attachment.class = ' => $foreingClass,
                    ),
                    'recursive' => - 1,
        ));
    }

    public function newAttachment() {
        return array(
            'Attachment' => array(
                'id' => null,
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'class' => null,
                'foreign_id' => null,
            )
        );
    }

}
