<?php
class ApiPaymentOptionDeal extends ApiAppModel {
  public $name      = 'ApiPaymentOptionDeal';
  public $alias     = 'PaymentOptionDeal';
  public $useTable  = 'payment_option_deals';
  public $recursive = -1;
  public $belongsTo = array(
      'PaymentOption' => array(
        'className' => 'api.ApiPaymentOption',
        'foreignKey' => 'payment_option_id',
      ),
      'PaymentPlan' => array(
        'classname' => 'api.ApiPaymentPlan',
        'foreignKey' => 'payment_plan_id',
      )
  );
  public $fields = array(
      'PaymentOption.*', 
      'PaymentOptionDeal.*',
  	  'PaymentPlan.*'
  );
  public function findPaymentOptionIdByDeal($dealId) {
  
    $paymentOptions = $this->find('list',
              array(
              'fields' => 'payment_option_id',
              'conditions'=> array('deal_id' =>  $dealId),
              'recursive' => -1,
              )
            );
    if (!empty($paymentOptions)) {
      foreach ($paymentOptions as $value) {
        $optionIds[] = $value;
      }
    }
    return $optionIds;
  }

  public function findPaymentOptionsByDeal($dealId){
      
      $paymentOptions = $this->find('all',
              array(
              'fields' => $this->fields,
              'conditions'=> array('deal_id' =>  $dealId),
              'recursive' => 1,
              )
            );
      
      return $paymentOptions;
  }

  public function newPaymentOptionDeal() {
    return array(
      'PaymentOptionDeal' => array('id' => null)
    );
  }
  
  public function saveMultipleRowsByDeal($paymentOptions,$deal){
  	$rows = array();
  	foreach ($paymentOptions as $paymentOption) {
  		$row = array();
  		$row['PaymentOptionDeal']['payment_option_id'] = $paymentOption['PaymentOption']['id'];
  		$row['PaymentOptionDeal']['payment_plan_id'] = $paymentOption['PaymentPlan']['id'];
  		$row['PaymentOptionDeal']['deal_id'] = $deal['Deal']['id'];
  		$rows[] = $row;
  	}
  	
  	$this->saveAll($rows);
  	
  }
  
}

?>
