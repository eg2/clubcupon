<?php
class ApiPaymentType extends ApiAppModel{
  public $name       = 'ApiPaymentType';
  public $alias      = 'PaymentType';
  public $useTable   = 'payment_types';
  public $recursive  = -1;
  const WALLET  = 'Cuenta Club Cupon';
  const ID_WALLET = 5000;
  public function findBacPaymentTypeId($paymentTypeId) {
    $bacPaymentType = $this->find('first',
      array(
        'fields'  => array('bac_payment_type'),
        'conditions'  => array('id'=>$paymentTypeId),
        'recursive'=> -1));
    return $bacPaymentType['PaymentType']['bac_payment_type'];
  }

  public function getGatewayIdByPaymentTypeId($paymentTypeId) {
    $gatewayId = $this->find('first',
      array(
        'fields'  => array('gateway_id'),
        'conditions'  => array('id'=>$paymentTypeId),
        'recursive'=> -1)
    );
    return $gatewayId['PaymentType']['gateway_id'];
  }

  public function isPaymentTypeMP($payment_type_id) {
    
    $data = $this->findById($payment_type_id);
    return in_array($data['PaymentType']['gateway_id'], array(11, 5, 23, 24));
  }

  public function findPaymentTypeIdForWallet() {
    return $this->find('list',
      array(
        'fields' => array('id'),
        'conditions'=>
        array('name'=>WALLET)));
  }

  public function isWallet($payment_type_id) {
      return self::ID_WALLET == $payment_type_id;
  }

}
?>
