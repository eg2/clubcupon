<?php

class ApiPaymentPlanItem extends ApiAppModel {

    public $name = 'ApiPaymentPlanItem';
    public $useTable = 'payment_plan_items';
    public $recursive = - 1;

    public function getPaymentInstallments($paymentPlanId) {
        return $this->find('all', array(
                    'fields' => array(
                        'id',
                        'name'
                    ),
                    'conditions' => array(
                        'payment_plan_id' => $paymentPlanId
                    )
        ));
    }

    function getNumInstallments($payment_plan_option_id) {
        $num_quota = $this->read('payment_installments', $payment_plan_option_id);
        return $num_quota['PaymentPlanItem']['payment_installments'];
    }

}
