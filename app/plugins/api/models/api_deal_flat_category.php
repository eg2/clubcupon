<?php

class ApiDealFlatCategory extends ApiAppModel {

    public $name = 'ApiDealFlatCategory';
    public $alias = 'DealFlatCategory';
    public $useTable = 'deal_flat_categories';
    public $recursive = - 1;

}