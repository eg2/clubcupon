<?php
class ApiCountry extends ApiAppModel
{
    public $name = 'ApiCountry';
    public $alias = 'Country';
    public $useTable = 'countries';
    
    public $displayField = 'name';
    
    public function findAllForApi() {
    	return $this->find('list',array(
				'order' => array(
						'Country.name' => 'asc'
				) ,
    	));
    }

}

