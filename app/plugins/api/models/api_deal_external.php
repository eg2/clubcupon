<?php

class ApiDealExternal extends ApiAppModel {

    public $name = 'ApiDealExternal';
    public $alias = 'DealExternal';
    public $useTable = 'deal_externals';
    public $recursive = - 1;
    public $defaults = array(
        'quantity' => 1,
        'buy_ip' => '127.0.0.1'
    );
    public $validate = array(
        'deal_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'el id de la oferta debe ser un valor numerico'
            )
        ),
        'user_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'el id del usuario debe ser un valor numerico'
            )
        ),
        'payment_type_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'el tipo de pago debe ser un valor numerico'
            )
        ),
        'final_amount' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'final_amount debe ser un valor numerico'
            )
        ),
        'quantity' => array(
            'rule3' => array(
                'rule' => array(
                    'comparison',
                    '>',
                    0
                ),
                'allowEmpty' => false,
                'message' => 'cantidad debe ser mayor a cero'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'cantidad debe ser un valor numerico'
            ),
            'rule1' => array(
                'rule' => 'notempty',
                'message' => 'cantidad no puede estar vacio'
            )
        ),
        'payment_option_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => 'payment_option_id debe ser un valor numerico'
            )
        ),
        'gift_pin_code' => array(
            'rule2' => array(
                'rule' => array(
                    'asertMaxLength',
                    20
                ),
                'message' => 'gift_pin_code debe tener una longitud de hasta 20 caracteres'
            ),
            'rule1' => array(
                'rule' => 'alphaNumeric',
                'allowEmpty' => true,
                'message' => 'gift_pin_code debe ser un valor alfanumerico'
            )
        ),
        'gift_pin_id' => array(
            'rule1' => array(
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'gift_pin_id debe ser un valor numerico'
            )
        )
    );

    const MOBILE = 2;
    const BUY_CHANNEL_TYPE_MOBILE = 2;
    const BUY_CHANNEL_TYPE_SITE = 1;

    public $fields = array(
        'DealExternal.*'
    );

    public function insertDealExternal($deal_external) {
        $deal_external['DealExternal'] = $this->fillDefaults($deal_external['DealExternal']);
        $deal_external = $this->save($deal_external);
        $deal_external_id = $this->getInsertID();
        $deal_external['DealExternal']['id'] = $deal_external_id;
        return $deal_external;
    }

    public function setDatesToNow($deal_external) {
        $now = date('Y-m-d H:i:s');
        $deal_external['DealExternal']['created'] = $now;
        $deal_external['DealExternal']['updated'] = $now;
        return $deal_external;
    }

    private function fillDefaults($deal_external) {
        return array_merge($this->defaults, $deal_external);
    }

    public function isMobile($dealExternal) {
        return $dealExternal['DealExternal']['buy_channel_type_id'];
    }

    public function countQuantityForDealId($dealId) {
        $extraConditions = array(
            'DealExternal.deal_id' => $dealId,
            'DealExternal.external_status' => array(
                'A',
                'P'
            )
        );
        $groupFields = array(
            'DealExternal.deal_id'
        );
        return $this->countQuantity($extraConditions, $groupFields);
    }

    private function countQuantity($extraConditions, $groupFields) {
        $count = $this->find('first', array(
            'conditions' => $extraConditions,
            'fields' => array(
                'SUM(DealExternal.quantity) as total_count'
            ),
            'group' => $groupFields,
            'recursive' => - 1
        ));
        return $count['0']['total_count'];
    }

    public function countQuantityForDealIdAndUserId($dealId, $userId) {
        $extraConditions = array(
            'DealExternal.deal_id' => $dealId,
            'DealExternal.user_id' => $userId,
            'DealExternal.external_status' => array(
                'A',
                'P'
            )
        );
        $groupFields = array(
            'DealExternal.deal_id'
        );
        return $this->countQuantity($extraConditions, $groupFields);
    }

    public function findbyIdAndUser($userId, $dealExternalId) {
        $extraConditions = array(
            'DealExternal.user_id' => $userId,
            'DealExternal.id' => $dealExternalId
        );
        $dealExternal = $this->findForApi('first', $extraConditions);
        return $dealExternal;
    }

    public function findLastDealExternalByUserId($user_id) {
        $fields = array(
            'DealExternal.id',
            'DealExternal.created',
            'DealFlatCategory.id',
            'DealFlatCategory.path',
        );
        $conditions = array(
            'DealExternal.user_id' => $user_id
        );
        $joins = array(
            array(
                'table' => 'deals',
                'alias' => 'Deal',
                'type' => 'inner',
                'conditions' => array(
                    'Deal.id = DealExternal.deal_id'
                )
            ),
            array(
                'table' => 'deal_flat_categories',
                'alias' => 'DealFlatCategory',
                'type' => 'inner',
                'conditions' => array(
                    'Deal.deal_category_id = DealFlatCategory.id'
                )
            )
        );
        $order = array(
            'DealExternal.id' => 'desc'
        );
        return $this->find('first', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'order' => $order,
                    'recursive' => - 1
        ));
    }

    private function findForApi($findMethod, $extraConditions = null, $limit = null, $page = null) {
        return $this->find($findMethod, array(
                    'fields' => $this->fields,
                    'conditions' => $extraConditions,
                    'order' => array(
                        'DealExternal.id' => 'desc'
                    ),
                    'limit' => $this->getPageLimit($limit),
                    'page' => $this->getPage($page)
        ));
    }

    function paymentTypeIdCreditCard() {
        return array(
            1,
            2,
            7,
            20,
            22,
            23,
            24,
            25,
            28,
            32,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            83,
            85,
            86,
            89,
            90,
            92,
            94,
            95,
            96,
            97,
            98,
            99,
            100,
            101,
            102,
            103,
            104,
            105,
            106
        );
    }

    function findFirstCreditedCreditCardPaymentByUserId($userId) {
        $conditions = array(
            'DealExternal.user_id' => $userId,
            'DealExternal.external_status' => 'A',
            'DealExternal.payment_type_id' => $this->paymentTypeIdCreditCard()
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

}
