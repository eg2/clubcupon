<?php
class ApiPaymentOptionsController extends ApiAppController {
  public $name = 'ApiPaymentOptions';
  
  public $uses = array(
      'api.ApiPaymentOptionDeal',
      'api.ApiPaymentOption',
      'api.ApiPaymentPlanItem'
  );
  
  public function options() {
   // die("hehehehehe");
    $dealId = $this->getDealIdParam();
    try {
      /*$optionIds =
        $this->ApiPaymentOptionDeal->findPaymentOptionIdByDeal($dealId);
      $ret =
        $this->ApiPaymentOption->findAllForIds($optionIds);*/
      
     $paymentOptions =
         $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($dealId);
    
     foreach ($paymentOptions as $key => $value){
         
       $paymentOptions[$key]['PaymentPlanItem'] = 
             $this->ApiPaymentPlanItem->
                 getPaymentInstallments(
                  $value['PaymentOptionDeal']['payment_plan_id']
                 );
       unset($paymentOptions[$key]['PaymentOptionDeal']);
     }
     $this->set('payment_option', $paymentOptions);
    }
    catch (Exception $e) {
       $this->Session->setFlash($e->getMessage(), 'failure');
       $this->debug($e, 'exception');
    }
  }

  public function getDealIdParam(){
    return $this->getUrlParam('deal_id');
  }
}

?>
