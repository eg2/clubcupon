<?php

class ApiNeighbourhoodsController extends ApiAppController {

    public $name = 'ApiNeighbourhoods';
    public $uses = array(
        'api.ApiNeighbourhood'
    );

    public function neighbourhoods() {
        $this->ApiLogger->info('Parametros', $this->params);
        $extraConditions = array();
        $phase = $this->getNamedParam("phase");
        if ($phase) {
            $extraConditions["Neighbourhood.name LIKE"] = "%" . $phase . "%";
        }
        $cityId = $this->getNamedParam("city_id");
        if ($cityId) {
            $extraConditions["Neighbourhood.city_id"] = $cityId;
        }
        $neighbourhoods = $this->ApiNeighbourhood->findAllForApi($extraConditions);
        $this->set('neighbourhoods', $neighbourhoods);
    }

}
