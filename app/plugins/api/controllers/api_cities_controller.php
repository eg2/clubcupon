<?php

class ApiCitiesController extends ApiAppController {

    public $name = 'ApiCities';
    public $uses = array(
        'api.ApiCity',
        'api.ApiSubscription'
    );

    public function cities() {
        $cities = $this->ApiCity->findAllForApi();
        if ($this->isLoggedUser()) {
            $cities = $this->appendIsUserSubscribed($cities, $this->getUser());
        }
        $this->set('cities', $cities);
    }

    private function appendIsUserSubscribed($cities, $user) {
        $subscriptions = $this->ApiSubscription->findAllByUserId($user['User']['id']);
        foreach ($subscriptions as $subscription) {
            $index = $this->getIndexByCityId($subscription['Subscription']['city_id'], $cities);
            if ($index >= 0) {
                $cities[$index]['City']['is_subscribed'] = "1";
            }
        }
        return $cities;
    }

    private function getIndexByCityId($city_id, $cities) {
        $i = 0;
        foreach ($cities as $city) {
            if ($city['City']['id'] == $city_id) {
                return $i;
            }
            $i+= 1;
        }
        return -1;
    }

}
