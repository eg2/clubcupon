<?php

class ApiDealExternalsController extends ApiAppController {

    public $name = 'ApiDealExternals';
    public $uses = array(
        'api.ApiUser',
        'api.ApiDealExternal'
    );
    public $components = array(
        'ApiBacService',
        'ApiPaymentService'
    );

    public function bac_payment_info() {
        try {
            $dealExternalId = $this->getDealExternalIdParam();
            list($bac_info, $use_litebox) = $this->ApiBacService->getBacPaymentInfo($dealExternalId, $ipClient = null);
            $this->set('bac_info', $bac_info);
            $this->set('use_litebox', $use_litebox);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    public function deal_external() {
        try {
            $dealExternalId = $this->getDealExternalIdParam();
            $userId = $this->Auth->user('id');
            if ($userId) {
                $dealExternal = $this->getDealExternalByIdAndUser($userId, $dealExternalId);
            }
            $this->set('deal_external', $dealExternal);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    private function getDealExternalIdParam() {
        return $this->getUrlParam('deal_external_id');
    }

    private function getDealExternalByIdAndUser($userId, $dealExternalId) {
        return $this->ApiDealExternal->findbyIdAndUser($userId, $dealExternalId);
    }

}
