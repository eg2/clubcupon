<?php

class ApiDealUsersController extends ApiAppController {

    public $name = 'ApiDealUsers';
    public $uses = array(
        'api.ApiDealUser',
        'api.ApiDeal',
        'api.ApiCompany',
    );
    public $actionsForUserLoggedIn = array(
        'api_deal_users/cupons',
        'api_deal_users/index',
        'api_deal_users/view'
    );

    /**
     * Retorna el Listado de Cupones para un Usuario:
     * El listado puede ser xml o json.
     *
     * Ejemplo de Llamadas:
     * api/api_deal_users/cupons.xml
     * api/api_deal_users/cupons.json
     *
     */
    public function cupons() {
        try {
            $user = $this->getUser();
            $cupons = $this->ApiDealUser->findForApiByUserId($user["User"]["id"]);
            $qr_img = $this->getQrUrl();

            $cuponsSize = count($cupons);
            for ($i = 0; $i < $cuponsSize; $i++) {
                $cupons[$i]['DealUser']['qr_img'] = $qr_img;
                $cupons[$i]['Deal']['web_url'] = $this->getDealUrl($cupons[$i]);
                // clean null values
                if (is_null($cupons[$i]['Deal']['custom_company_address1'])) {
                    $cupons[$i]['Deal']['custom_company_address1'] = '';
                }
                if (is_null($cupons[$i]['Deal']['custom_company_name'])) {
                    $cupons[$i]['Deal']['custom_company_name'] = '';
                }
            }

            $this->set('cupons', $cupons);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    public function index() {
        $result = array();
        $page = $this->getUrlParam('page');
        $size = $this->getUrlParam('size');
        $start_date = $this->getUrlParam('start_date');
        $end_date = $this->getUrlParam('end_date');
        if (is_null($size)) {
            $size = 30;
        }
        if (is_null($page)) {
            $page = 1;
        }
        if ($this->Auth->user()) {
            $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
            if (!empty($company)) {
                $result = $this->ApiDealUser->findAllPagedByCompanyId($company['Company']['id'], $start_date, $end_date, $page, $size);
            }            
        }
        $this->set('deal_users', $result);
    }

    public function view() {
        $result = array();
        $posnetCode = $this->getUrlParam('posnetCode');
        if (!is_null($posnetCode) && $this->Auth->user()) {
            $company = $this->ApiCompany->findByUserId($this->Auth->user('id'));
            if (!empty($company)) {
                $result = $this->ApiDealUser->findByCompanyIdAndPosnetCode($company['Company']['id'], $posnetCode);
            }
        }
        $this->set('deal_users', $result);
    }

    private function getQrUrl() {
        return Router::url('/img/mobile/theme_clean/qr_cc.png', true);
    }

}

?>
