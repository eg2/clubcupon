<?php

class ApiUsersController extends ApiAppController {

  public $name = 'ApiUsers';
  public $helpers = array(
    'Api'
  );
  public $components = array(
    'ApiUserLoginService',
    'ApiUserRegisterService',
    );

  public $actionsForUserLoggedIn = array(
    'api_users/view',
  );

  public $actionsForAppSecure = array(
    'api_users/login_facebook',
  );

  /**
   * Genera un nuevo token para el usuario logueado.
   */
  public function token() {
    $user = $this->Auth->user();
    try {
      $user = $this->ApiUser->token($user);
      $this->set('token', $user['User']['token']);

    } catch (Exception $e) {
      $this->set('token', null);
      $this->Session->setFlash('There was an error generating this token');
    }
  }

  public function login_facebook() {
    $this->ApiLogger->notice('Iniciando login por facebook', $this->params);
    try {
      $data = $this->getLoginFacebookData();
      $subscribe = $this->getFormParam('no_subscribe')=="1"?false:true;
      $this->ApiLogger->notice('Con auto-subscripcion ?', $subscribe);

      $user = $this->ApiUser->findByEmail($data['User']['email']);
      if(empty($user)) {
        $user = $this->ApiUserRegisterService->registerAndActive($data, $subscribe);
      } else {
        $user = $this->ApiUserLoginService->updateUserFacebookId(
          $user,
          $data['User']['fb_user_id']);
      }
      
      $user = $this->ApiUserLoginService->loginByUser($user);
      $this->ApiLogger->notice('Iniciando login por facebook', $this->params);

      $this->set('user', $user);

    } catch (Exception $e) {
      $this->Session->setFlash($e->getMessage(), 'failure');
      $this->ApiLogger->notice('Error al intentar loguearse con facebook.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
      $this->ApiLogger->error('Error al intentar loguearse con facebook.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
    }
  }

  public function login() {
    $this->ApiLogger->notice('Iniciando login', $this->params);
    try {
      $user = $this->_login(
        $this->getFormParam('username'),
        $this->getFormParam('email'),
        $this->getFormParam('password')
      );

      $this->set('user', $user);
    } catch (Exception $e) {
      $this->Session->setFlash($e->getMessage(), 'failure');
      $this->ApiLogger->notice('Error al intentar loguearse.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
      $this->ApiLogger->error('Error al intentar loguearse.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
    }
  }

  public function view() {
    $this->set('user', $this->ApiUser->findById($this->Auth->user('id')));
  }


  private function _login($username, $email, $password) {
    $user = $this->ApiUserLoginService->login(
      $username,
      $email,
      $password
    );

    if (!empty($user)) {
      $user = $this->ApiUser->token($user);
    }

    return $user;
  }

  public function register() {
    $this->ApiLogger->notice('Iniciando registracion', $this->params);
    try {
      $data = $this->getRegisterData();
      $data['User']['username'] = $this->getusername($data);
      $validate = $this->ApiUserRegisterService->validate($data);
      
      if($validate["is_valid"]) {
        $user = $this->ApiUserRegisterService->registerAndActive($data);
      } else {
        $this->Session->setFlash($validate["errors"], 'failure');
      }
      if ($data['Login']) {
        $userToken = $this->ApiUserLoginService->login(
          $data['User']['username'],
          $data['User']['email'],
          $data['User']['password']
        );
        $user['User']['token'] = $this->ApiUser->token($user);
      }
      
      $this->set('user', $user);
    } catch (Exception $e) {
      $this->Session->setFlash($e->getMessage(), 'failure');
      $this->ApiLogger->notice('Error al intentar registrarse.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
      $this->ApiLogger->error('Error al intentar registrarse.', array('error' => $e->getMessage(), 'trace' => $e->getTraceAsString()));
    }
  }

  public function getusername($data) {
    if(empty($data['User']['username']) || 
       $this->usernameAlreadyExist($data['User']['username'])){
       
       $username =
          $this->generateUsernameByEmail($data['User']['email']);
    } else {
       $username = $data['User']['username'];
    }
    return $username;
  }

  public function usernameAlreadyExist($username) {
    return $this->ApiUserRegisterService->usernameExist($username);
  }

  public function forgot_password() {
    try {
    $email = $this->getFormParam('email');
    
    if (empty($email)) {
      throw new Exception ('Email está vacío. Ingresá algún valor');
    }
    
    $ok = $this->ApiUserLoginService->forgotPassword($email);
    $this->set('ok', $ok);
    
    
    } catch (Exception $e) {
      $this->set('ok', false);
      $this->Session->setFlash($e->getMessage(), 'failure');
      $this->debug($e, 'exception');
    }
  }

  private function generateUsernameByEmail($email) {
    return $this->ApiUserRegisterService->generateUsernameByEmail($email);
  }

  private function getRegisterData() {
    $data = array(
      'User' =>  array(
        'email' => $this->getFormParam('email'),
        'password' => $this->getFormParam('password'),
        'username' => $this->getFormParam('username'),
      ),
      'UserProfile' => array(
        'dni' => $this->getFormParam('dni'),
        'first_name' => $this->getFormParam('first_name'),
        'last_name' => $this->getFormParam('last_name'),
      ),
      'Login' => $this->getFormParam('login'),
    );

    $middleName = $this->getFormParam('middle_name');
    if(!empty($middleName)) {
      $data['UserProfile']['middle_name'] = $this->getFormParam('middle_name');
    }
    $username = $this->getFormParam('username');
    if(!empty($username)) {
      $data['User']['username'] = $this->getFormParam('username');
    }
    return $data;
  }

  private function getLoginFacebookData() {
    $data = array(
      'User' =>  array(
        'email' => $this->getFormParam('email'),
        'username' => $this->getFormParam('username'),
        'fb_user_id' => $this->getFormParam('fb_user_id'),
        'password' => $this->getFormParam('fb_user_id'),
      ),
      'UserProfile' => array(
        'first_name' => $this->getFormParam('first_name'),
        'last_name' => $this->getFormParam('last_name'),
        'date_of_birth' => $this->getFormParam('date_of_birth'),
        'dni' => $this->getFormParam('dni'),
      ),
    );

    list($data['option_id'], 
         $data['payment_id'], 
         $data['gateway_id']) = 
            $this->getFormParam('strpayment_options');
    
    $genderFB = $this->getFormParam('gender');
    if(!empty($genderFB)) {
      $data["UserProfile"]["gender"] = substr($genderFB, 0, 1);
    }

    return $data;
  }

  public function token_valid() {
    try {
    $token = $this->getTokenParam();
    if (empty($token)) {
      throw new Exception ('Token vacío.');
    }
    $user = $this->ApiUser->findByToken($token);
    $this->set('usable', $this->ApiUser->isTokenUsable($user));
    } catch (Exception $e) {
      $this->set('usable', false);
      $this->Session->setFlash($e->getMessage(), 'failure');
      $this->debug($e, 'exception');
    }
  }
  
  public function token_error() {
      $this->autoRender = false;
      $this->header('Content-type: application/json'); 
      $msg = array('ok'=> false, 'msg'=>'Token Inválido.');
      $this->set('error', $msg);
      echo json_encode($msg);
  }
}
