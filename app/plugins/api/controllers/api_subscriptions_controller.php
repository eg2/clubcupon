<?php
class ApiSubscriptionsController extends ApiAppController {
  public $name = 'ApiSubscriptions';
  
  public $components = array(
    'ApiUserLoginService',
    'ApiSubscriptionsService',
    );
  
  public $actionsForUserLoggedIn = array(
    'api_subscriptions/suscribe',
    'api_subscriptions/unsuscribe'
  );
  
  public $uses = array(
      'api.ApiSubscription',
  );
  
  public function suscribe(){
    try {
      $city = $this->getParamsOfSubscription();
      $user = $this->getCurrentUser();
     
      if (empty($city['city_id']) && empty($city['city_slug'])){
        throw new Exception("Faltan Parámetros, envíe el id de la ciudad o slug");
      }
      $ip = $this->RequestHandler->getClientIP();
      $campaignInfo = $this->_getCampaignInfoParams();
      $this->ApiLogger->trace("campaignInfo", $campaignInfo);
      
      $idSubscription =
        $this->ApiSubscriptionsService->suscribe($city, $user, $ip, $campaignInfo);
      
      $this->set('idSuscripcion', $idSubscription);
      $this->set('message', '¡Te Subscribiste correctamente!.');
    }
    catch (Exception $e){
      $this->set('message', $e->getMessage());
    }
  }
  
  public function suscribe_by_email(){
    try {
      $email 		= $this->getUrlParam('mail');
      $city_id 	= $this->getUrlParam('city_id');
      $ip = $this->RequestHandler->getClientIP();
      if (empty($email)&&empty($city_id)){
        echo "Faltan Parámetro, Email o id del City";
      }else{
        $idSubscription =
          $this->ApiSubscriptionsService->subscribeByEmail($email, $city_id, $ip);
      }
      $this->set('idSuscripcion', $idSubscription);
      $this->set('message', '¡Te Subscribiste correctamente!.');
    }
    catch (Exception $e){
      $this->set('message', $e->getMessage());
    }

  } 


  private function _getCampaignInfoParams() {
    $campaignInfo = array();

    if (!is_null($this->getFormParam('campaign'))) {
      $campaignInfo['campaign'] = $this->getFormParam('campaign');
    }
    if (!is_null($this->getFormParam('utm_source'))) {
      $campaignInfo['utm_source'] = $this->getFormParam('utm_source');
    }
    if (!is_null($this->getFormParam('utm_mediun'))) {
      $campaignInfo['utm_mediun'] = $this->getFormParam('utm_medium');
    }
    if (!is_null($this->getFormParam('utm_term'))) {
      $campaignInfo['utm_term'] = $this->getFormParam('utm_term');
    }
    if (!is_null($this->getFormParam('utm_content'))) {
      $campaignInfo['utm_content'] = $this->getFormParam('utm_content');
    }
    if (!is_null($this->getFormParam('utm_campaign'))) {
      $campaignInfo['utm_campaign'] = $this->getFormParam('utm_campaign');
    }
    return $campaignInfo;
  }

  public function unsuscribe(){
    try {
    $city = $this->getParamsOfSubscription();
    $user = $this->getCurrentUser();
    
    if ((empty($city['city_id']) && empty($city['city_slug']))){
      throw new Exception("Faltan Parámetros, envíe el id de la ciudad o slug");
    }
    
    $idSubscription = 
      $this->ApiSubscriptionsService->unsuscribe($city, $user);
    
    $this->set('idSuscripcion', $idSubscription);
    $this->set('message', '¡Te Desuscribirte correctamente!.');
    }
    catch (Exception $e){
      $this->set('message', $e->getMessage());
    }
  }

  public function getParamsOfSubscription() {
    return array(
    'city_id' => $this->getFormParam('city_id'),
    'city_slug' => $this->getFormParam('city_slug')
    ); 
  }
  
}
?>
