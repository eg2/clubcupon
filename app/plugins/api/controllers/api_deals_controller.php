<?php

class ApiDealsController extends ApiAppController {

    public $name = 'ApiDeals';
    public $uses = array(
        'api.ApiDeal',
        'api.ApiUser',
        'api.ApiBranchDeal',
        'api.ApiAttachment',
        'api.ApiCity',
        'api.ApiDealExternal',
        'api.ApiDealUser',
        'api.ApiPaymentOption',
        'api.ApiPaymentType',
        'api.ApiPaymentOptionDeal',
        'api.ApiPaymentPlanItem'
    );
    public $helpers = array(
        'Html'
    );
    public $components = array(
        'ApiBuyService',
        'ApiBacService'
    );
    public $actionsForUserLoggedIn = array(
        'api_deals/buy',
        'api_deals/do_buy'
    );

    const ALL_DEALS = 2;

    public function company_deals() {
        $this->layout = false;
        $this->set('parametros', $this->params);
        $findConditions = array();
        $companyId = $this->params['url']['id_compania'];
        if (is_null($companyId)) {
            $companyId = $this->params['named']['id_compania'];
        }
        $findConditions['Company.id'] = $companyId;
        $deals = $this->ApiDeal->findAllForCompany($findConditions, $this->getLimitParam(), $this->getPageParam());
        $dealsSize = count($deals);
        for ($i = 0; $i < $dealsSize; $i++) {
            $deals[$i] = $this->appendExtraInfo($deals[$i]);
        }
        if ($this->_isJSON()) {
            echo json_encode(array(
                'params' => $this->params,
                'deals' => $deals
            ));
        } else {
            if (!empty($deals)) {
                $this->redirect(Router::url(array(
                            'plugin' => '',
                            'controller' => 'deals',
                            'action' => 'buy',
                            $deals[0]['Deal']['id']
                                ), true));
            } else {
                $this->redirect(Router::url(array(
                            'plugin' => '',
                            'controller' => 'deals',
                            'action' => 'index'
                                ), true));
            }
        }
    }

    public function deals() {
        try {
            $findConditions = array();
            $idCiudad = $this->getUrlParam('id_ciudad');
            $isNow = $this->getUrlParam('is_now');
            $latitud = $this->getUrlParam('latitud');
            $longitud = $this->getUrlParam('longitud');
            $slugCiudad = $this->getUrlParam('slug');
            $companyId = $this->getUrlParam('id_compania');
            if (!empty($idCiudad)) {
                $findConditions['City.id'] = $idCiudad;
            }
            if (!empty($slugCiudad)) {
                $findConditions['City.slug'] = $slugCiudad;
            }
            if (!empty($companyId)) {
                $findConditions['Deal.company_id'] = $companyId;
            }
            if ($this->hasToFilterForNow($isNow)) {
                $findConditions['Deal.is_now'] = $isNow;
            }
            if (!empty($latitud) || !empty($longitud)) {
                $city = $this->ApiCity->findIdBySlug($slugCiudad);
                $dealIds = $this->ApiBranchDeal->findDistinctDealIdsByLatitudeAndLongitude($latitud, $longitud, $city['City']['id']);
                $findConditions['Deal.id'] = $this->extractDealIds($dealIds);
            }
            $deals = $this->ApiDeal->findAllForApi($findConditions, $this->getLimitParam(), $this->getPageParam());
            $dealsSize = count($deals);
            for ($i = 0; $i < $dealsSize; $i++) {
                $deals[$i] = $this->appendExtraInfo($deals[$i]);
                if (is_null($cupons[$i]['Deal']['custom_company_address1'])) {
                    $cupons[$i]['Deal']['custom_company_address1'] = '';
                }
                if (is_null($cupons[$i]['Deal']['custom_company_name'])) {
                    $cupons[$i]['Deal']['custom_company_name'] = '';
                }
            }
            $this->set('deals', $deals);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    private function appendExtraInfo($deal) {
        if ($this->ApiBuyService->isPurchasable($deal)) {
            $deal['Deal']['is_purchasable'] = '1';
        } else {
            $deal['Deal']['is_purchasable'] = '0';
        }
        $deal['Deal']['web_url'] = $this->getDealUrl($deal);
        $deal['Deal']['buy_url'] = $this->getBuyUrl($deal);
        $deal['Deal']['coupon_condition_mobile'] = $this->getStrCouponConditionMobile(null, $deal);
        $subDeals = $this->ApiDeal->findSubDealsForApi($deal);
        if (!empty($subDeals)) {
            $subDeals = $this->addSubCouponConditionMobile($subDeals);
            $deal['SubDeals'] = $subDeals;
        }
        if ($this->hasToFilterForNow($this->ApiDeal->isNow($deal))) {
            $branches = $this->ApiBranchDeal->findByDeal($deal);
            if (!empty($branches)) {
                $deal['Deal']['Branches'] = $branches;
                $deal = $this->addSubtitleToBranches($branches, $deal);
                $deal = $this->addCouponConditionMobile($branches, $deal);
            }
        }
        return $deal;
    }

    public function solr_data_import() {
        $findConditions = array(
            'Deal.is_now' => 0
        );
        $deals = null;
        $hasMore = null;
        $countAllSolrDataImport = null;
        $currentPage = $this->getPageParam();
        $currentLimit = $this->getLimitParam();
        $deals = $this->ApiDeal->findAllForSolrDataImport($findConditions, $this->getLimitParam(), $this->getPageParam());
        $count = $this->ApiDeal->countAllForSolrDataImport($findConditions);
        if ($count > ($currentPage * $currentLimit)) {
            $hasMore = 'true';
        } else {
            $hasMore = 'false';
        }
        $dealsSize = count($deals);
        for ($i = 0; $i < $dealsSize; $i++) {
            if ($this->ApiBuyService->isPurchasable($deals[$i])) {
                $deals[$i]['Deal']['is_purchasable'] = '1';
            } else {
                $deals[$i]['Deal']['is_purchasable'] = '0';
            }
            if ($i == ($dealsSize - 1)) {
                $deals[$i]['has_more'] = $hasMore;
                $deals[$i]['next_url'] = $this->getApiSolrDataImportUrl($currentPage + 1, $currentLimit);
            }
            $pathExplode = explode('|', $deals[$i]['DealFlatCategory']['path']);
            $deals[$i]['DealFlatCategory']['l1_name'] = (isset($pathExplode[0]) ? $pathExplode[0] : '');
            $deals[$i]['DealFlatCategory']['l2_name'] = (isset($pathExplode[1]) ? $pathExplode[1] : '');
            $deals[$i]['DealFlatCategory']['l3_name'] = (isset($pathExplode[2]) ? $pathExplode[2] : '');
            $deals[$i]['Deal']['discounted_price'] = intval($deals[$i]['Deal']['discounted_price']);
            $deals[$i]['Deal']['original_price'] = intval($deals[$i]['Deal']['original_price']);
            if ($deals[$i]['City']['is_group'] == 1) {
                $deals[$i]['City']['nameIfGroup'] = $deals[$i]['City']['name'];
                $deals[$i]['City']['nameIfNoGroup'] = '';
            } else {
                $deals[$i]['City']['nameIfGroup'] = '';
                $deals[$i]['City']['nameIfNoGroup'] = $deals[$i]['City']['name'];
            }
            $attachmentObj = new Attachment();
            $myAttachment = $attachmentObj->find('first', array(
                'conditions' => array(
                    'class' => 'Deal',
                    'foreign_id' => $deals[$i]['Deal']['id']
                )
            ));
            $myUrl = $this->getImageUrl('Deal', $myAttachment, null, false);
            $deals[$i]['Deal']['image_path'] = $myUrl;
        }
        $this->set('deals', $deals);
    }

    protected function getApiSolrDataImportUrl($page = null, $limit = null) {
        $this->ApiLogger->notice('Iniciando solr data import', $this->params);
        $strUrl = Router::url(array(
                    'plugin' => 'api',
                    'controller' => 'api_deals',
                    'action' => 'solr_data_import'
        ));
        if (!empty($page)) {
            $strUrl.= '.xml?page=' . $page;
        }
        return 'http://' . $_SERVER['HTTP_HOST'] . $strUrl;
    }

    private function addSubCouponConditionMobile($subDeals) {
        $i = 0;
        foreach ($subDeals as $deal) {
            $subDeals[$i]['Deal']['coupon_condition_mobile'] = $this->getStrCouponConditionMobile(null, $deal);
            $i++;
        }
        return $subDeals;
    }

    private function addCouponConditionMobile($branches = null, $deal) {
        $size = count($branches);
        for ($i = 0; $i < $size; $i++) {
            $deal['Deal']['Branches'][$i]['coupon_condition_mobile'] = $this->getStrCouponConditionMobile($branch, $deal);
            $i++;
        }
        return $deal;
    }

    private function addSubtitleToBranches($branches, $deal) {
        $i = 0;
        foreach ($branches as $val) {
            $deal['Deal']['Branches'][$i]['subtitulo'] = $this->getStrDealDate($val);
            $i++;
        }
        return $deal;
    }

    private function getStrCouponConditionMobile($branch, $deal) {
        $discountPercentage = $deal['Deal']['discount_percentage'];
        $precioOriginal = $deal['Deal']['original_price'];
        $precioConDescuento = $deal['Deal']['discounted_price'];
        $str = '';
        if ($deal['Deal']['only_price'] == 1) {
            $str = '$' . ((int) $precioConDescuento);
        } elseif ($deal['Deal']['hide_price'] == 1) {
            $str = $discountPercentage . '%';
        } else {
            $precioOriginal = $deal['Deal']['original_price'];
            $precioConDescuento = $deal['Deal']['discounted_price'];
            $str = '$' . ((int) $precioConDescuento) . ' en vez de $' . ((int) $precioOriginal);
        }
        if (!$branch == null) {
            $str.= ' en ' . $branch['Branch']['name'] . '.';
        }
        return $str;
    }

    private function getStrDealDate($branches) {
        $inicio = date("H", strtotime($branches['BranchDeal']['ts_inicio']));
        $fin = date("H", strtotime($branches['BranchDeal']['ts_fin']));
        $street = $branches['Branch']['street'] . ' ' . $branches['Branch']['number'];
        $str = "¡Hoy de $inicio hs a $fin hs! En $street";
        return $str;
    }

    public function view() {
        try {
            $slug = $this->getSlugParam();
            $dealId = $this->getDealIdParam();
            if ((empty($slug) && empty($dealId))) {
                throw new Exception("Slug or Deal ID must not be empty");
            }
            $deal = $this->getDealBySlugOrDealId($slug, $dealId);
            $deal = $this->appendExtraInfo($deal);
            $this->set('deal', $deal);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    public function img() {
        try {
            $this->autoRender = false;
            $size = $this->getNamedParam('size');
            list($dealId, $type) = explode('.', $this->params['pass'][1]);
            $deal = $this->ApiDeal->findById($dealId);
            $imageUrl = $this->getDealImageUrl($deal, array(
                'dimension' => $size,
                'type' => $type
            ));
            $this->redirect($imageUrl);
        } catch (Exception $e) {
            $this->header("HTTP/1.0 404 Not Found");
        }
    }

    public function img_uri() {
        try {
            $size = $this->getUrlParam('size');
            $dealId = $this->getDealIdParam();
            $type = 'jpg';
            if (empty($size) || empty($dealId)) {
                throw new Exception("Size or Deal ID must not be empty");
            }
            $deal = $this->ApiDeal->findById($dealId);
            $imageUrl = $this->getDealImageUrl($deal, array(
                'dimension' => $size,
                'type' => $type
            ));
            $this->set('img_uri', $imageUrl);
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    private function getDealBySlugOrDealId($slug = null, $dealId = null) {
        if ($dealId) {
            $deal = $this->ApiDeal->findForApiById($dealId);
        } else {
            $deal = $this->ApiDeal->findForApiBySlug($slug);
        }
        return $deal;
    }

    private function findPaymentOptionByDeal($dealId) {
        $paymentOptions = $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($dealId);
        foreach ($paymentOptions as $key => $value) {
            $paymentOptions[$key]['PaymentPlanItem'] = $this->ApiPaymentPlanItem->getPaymentInstallments($value['PaymentOptionDeal']['payment_plan_id']);
            unset($paymentOptions[$key]['PaymentOptionDeal']);
        }
        return $paymentOptions;
    }

    public function buy() {
        try {
            $this->set('params', $this->params);
            $this->ApiLogger->notice('Iniciando pantalla de compra', $this->params['url']);
            $dealId = $this->getDealIdParam();
            $dealExternalId = $this->getUrlParam('deal_external_id');
            $payStatus = $this->getUrlParam('paystatus');
            $userId = $this->Auth->user('id');
            Debugger::log('Mobile - Buy, dealExternalId: ' . $dealExternalId, LOG_DEBUG);
            Debugger::log('Mobile - Buy, payStatus:' . $payStatus, LOG_DEBUG);
            Debugger::log('Mobile - Buy, userId: ' . $userId, LOG_DEBUG);
            if (empty($dealId)) {
                throw new Exception("Deal Id must not be empty.");
            }
           
            $deal = $this->ApiDeal->findById($dealId, array(
                'id',
                'name',
                'discounted_price',
                'buy_max_quantity_per_user',
                'buy_min_quantity_per_user',
            	'deal_category_id'
            ));
            $dealCategory = $this->ApiDeal->DealCategory->find('first', array(
            		'conditions' => array(
            				'DealCategory.id' => $deal['Deal']['deal_category_id']
            		),
            		'fields' => array(
            				'DealCategory.name'
            		),
            		'recursive' => - 1
            ));
            $deal['Deal']['category_name']=$dealCategory['DealCategory']['name'];
  
            if ($this->ApiBuyService->isPurchasableByUser($deal, $user, $quantity = 1)) {
                throw new Exception("User must can buy the deal.");
            }
            $paymentOptions = $this->findPaymentOptionByDeal($dealId);
            $optionsSize = count($paymentOptions);
            for ($i = 0; $i < $optionsSize; $i++) {
                $type = $this->ApiPaymentType->findById($paymentOptions[$i]['PaymentOption']['payment_type_id']);
                $paymentOptions[$i]['PaymentType'] = $type['PaymentType'];
            }
            $user = $this->ApiUser->findById($userId, array(
                'id',
                'email',
                'token',
                'available_balance_amount'
            ));
            $user['User']['quantity_buyed_per_user'] = $this->ApiDealExternal->countQuantityForDealIdAndUserId($dealId, $userId);
            $user['User']['qty_user_can_buy'] = $deal['Deal']['buy_max_quantity_per_user'] - $user['User']['quantity_buyed_per_user'];
            if (!empty($dealExternalId)) {
                $dealExternal = $this->ApiDealExternal->findById($dealExternalId);
                
                if ($dealExternal['DealExternal']['user_id'] != $userId) {
                    throw new Exception('Must be owner of the deal external.');
                }
            }
            $this->set('deal', $deal);
            $this->set('paymentOptions', $paymentOptions);
            $this->set('user', $user);
            if (!empty($dealExternal)) {
                $this->set('dealExternal', $dealExternal);
            }
            if (!empty($payStatus)) {
                $this->set('payStatus', $payStatus);
            }
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->debug($e, 'exception');
        }
    }

    public function do_buy() {
        try {
            $this->ApiLogger->notice('Iniciando compra', $this->params);
            $this->set('params', $this->params);
            $userId = $this->Auth->user('id');
            $dealId = $this->getFormParam('deal_id');
            $quantity = $this->getFormParam('quantity');
            $paymentOptionId = $this->getFormParam('payment_option_id');
            $paymentPlanItemId = $this->getFormParam('payment_plan_item_id');
            $giftOptions = $this->getFormParam('gift_options');
            $discountPin = $this->getFormParam('discount_pin');
            $isCombined = $this->getFormParam('is_combined');
            $buyChannel = ApiDealExternal::BUY_CHANNEL_TYPE_MOBILE;
            $ipClient = RequestHandlerComponent::getClientIP();
            $dealExternal = $this->ApiBuyService->buy($userId, $dealId, $quantity, $paymentOptionId, $paymentPlanItemId, $isCombined, $buyChannel, $giftOptions, $discountPin);
            if (!isset($dealExternal['DealExternal'])) {
                throw new Exception("DealExternal must not be created.");
            }
            if (!$this->ApiBuyService->isBuyOnlyWithWallet($dealExternal['DealExternal']['payment_type_id'])) {
                list($bac_info, $use_litebox) = $this->ApiBacService->getBacPaymentInfo($dealExternal['DealExternal']['id'], $ipClient);
                $this->ApiLogger->notice("Compra exitosa.", array(
                    "dealExternalId" => $dealExternal['DealExternal']['id'],
                    "bacInfo" => $bac_info,
                    "useLitebox" => $use_litebox
                ));
            }
            $this->set('bac_info', $bac_info);
            $this->set('use_litebox', $use_litebox);
            $this->set('dealExternal', $dealExternal);
        } catch (DomainException $e) {
            $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
            $this->Session->setFlash($e->getMessage(), 'default', null, 'error');
            $this->ApiLogger->error('Error al intentar comprar por api.', array(
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
        } catch (Exception $e) {
            $this->Session->setFlash($e->getMessage(), 'failure');
            $this->ApiLogger->notice('Error al intentar comprar por api.', array(
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
            $this->ApiLogger->error('Error al intentar comprar por api.', array(
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
        }
    }

    private function getDealImageUrl($deal, $options = false) {
        if (empty($deal)) {
            throw new Exception('No existe esa Oferta.');
        }
        $attach = $this->ApiAttachment->findFirstAttachementForDeal($deal);
        if (empty($attach)) {
            throw new Exception('No se existe archivo adjunto');
        }
        if ($this->ApiDeal->isNow($deal)) {
            $modelForImage = 'NowDeal';
        } else {
            $modelForImage = 'Deal';
        }
        return $this->getImageUrl($modelForImage, $attach, $options);
    }

    private function getSlugParam() {
        return $this->getUrlParam('slug');
    }

    private function getDealIdParam() {
        return $this->getUrlParam('deal_id');
    }

    private function hasToFilterForNow($isNow) {
        return isset($isNow) && $isNow != self::ALL_DEALS;
    }

    private function extractDealIds($branchDeals) {
        $dealIds = array();
        foreach ($branchDeals as $branchDeal) {
            $dealIds[] = $branchDeal['BranchDeal']['deal_id'];
        }
        return $dealIds;
    }

    private function getBuyUrl($deal) {
        return Router::url(array(
                    'plugin' => '',
                    'controller' => 'deals',
                    'action' => 'buy',
                    $deal['Deal']['id']
                        ), true);
    }

    private function getDealByDealExternalId($dealExternalId) {
        $dealId = $this->ApiDealExternal->find('first', array(
            array(
                'conditions' => array(
                    'id' => $idPagoPortal
                ),
                'fields' => array(
                    'deal_id'
                )
            )
        ));
        $deal = $this->ApiDeal->find('first', array(
            'conditions' => array(
                'id' => $dealId
            )
        ));
        return $deal;
    }

    public function initialize_payment() {
        $idUsuarioPortal = $this->getFormParam('idUsuarioPortal');
        $idPortal = $this->getFormParam('idPortal');
        $productos = $this->getFormParam('productos');
        $idPagoPortal = $this->getFormParam('idPagoPortal');
        $idMedioPago = $this->getFormParam('idMedioPago');
        $idGateway = $this->getFormParam('idGateway');
        $cantidadCuotas = $this->getFormParam('cantidadCuotas');
        $descripcion = $this->getFormParam('descripcion');
        $mountDiscountMP = $this->getFormParam('tmount_mp_combined');
        $products = json_decode($productos);
        $isMPPayment = $this->ApiPaymentType->isPaymentTypeMP($idMedioPago);
        $idMedioPago = $this->ApiPaymentType->findBacPaymentTypeId($idMedioPago);
        $deal = $this->getDealByDealExternalId($idPagoPortal);
        $isTourism = $deal['Deal']['is_tourism'];
        $isWholesaler = $deal['Deal']['is_wholesaler'];
        if (!empty($mountDiscountMP) && isset($mountDiscountMP) && $isMPPayment && $isTourism && $isWholesaler) {
            $products[4]->monto = $products[4]->monto + (-$mountDiscountMP);
        } else {
            if (!empty($mountDiscountMP) && isset($mountDiscountMP) && $isMPPayment) {
                $products[1]->monto = $products[1]->monto + (-$mountDiscountMP);
            }
        }
        $secure_code = Configure::read('cc.bac.secure_code');
        $params = new stdClass();
        $params->idUsuarioPortal = $idUsuarioPortal;
        $params->idPortal = $idPortal;
        $params->productos = $products;
        $params->idPagoPortal = $idPagoPortal;
        $params->idMedioPago = $idMedioPago;
        $params->idGateway = $idGateway;
        $params->cantidadCuotas = $cantidadCuotas;
        $params->descripcion = $descripcion;
        $params->ipUsuario = RequestHandlerComponent::getClientIP();
        $this->ApiLogger->notice('Ip Usuario: ' . $params->ipUsuario);
        try {
            $idPago = $this->ApiBacService->iniciarPagoSeguro($params);
            $ret['status'] = True;
            $ret['idPago'] = $idPago;
            $ret['secureHash'] = md5($idGateway . $ret['idPago'] . $idPortal . $secure_code);
            $this->set('bac_info', $ret);
        } catch (Exception $e) {
            $ret['status'] = False;
            $ret['idPago'] = null;
            $this->set('bac_info', $ret);
            $this->ApiLogger->error('API deals Controller - initialize_payment', array(
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
            $this->ApiLogger->error('Error al iniciar comprar por api.', array(
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
            $this->debug($e, 'exception');
        }
    }
    public function company_deals_beacon(){
    	$this->layout = false;
    	$uuid = $this->getUrlParam('uuid');
    	$major = $this->getUrlParam('major');
    	$mac = $this->getUrlParam('mac');
    	 
   		$deals1= array();
    	$deals2= array();
    	 
    	for ($x = 2; $x <= 4; $x++) {
    		$deal = array();
    		if(($x % 2) > 0){
    			$deal['Deal']['id'] = 291472;
    			$deal['Deal']['name'] = '$229  Cena o almuerzo para dos en El Armenio: entrada + plato principal + postre.';
    			$deal['Deal']['subtitle'] = '¡45% menos para degustar nuevos sabores!';
    			$deals1[]=$deal;
    		}else{
    			
    			 $deal['Deal']['id'] = 292199;
    			$deal['Deal']['name'] = '30 piezas de sushi de salmón en Sama-Sushi.';
    			$deal['Deal']['subtitle'] = 'Imperdible por $145.';
    			$deals2[]=$deal;
    			
    		}
    		
    
    	}
    	 
    	$info_company1=array('id_company'=>1233,
    			'name_company'=>'El Armenio');
    	$beacon_data1=array('uuid'=>'2f234454-cf6d-4a0f-adf2-f4911ba9ffa6',
    		'mac'=>'D5:2E:7D:9C:23:B0'
    	);
    	$info_company2=array('id_company'=>1234,
    			'name_company'=>'El acople');
    	$beacon_data2=array('uuid'=>'b9407f30-f5f8-466e-aff9-25556b57fe6d',
    			'mac'=>'DF:73:95:B6:2F:95'
    	);
    	
    	 
    	if ($this->_isJSON()) {
    		echo json_encode(array(
    				array(
    						'info_company' => $info_company1,
    						'beacon_data'=>$beacon_data1,
    						'deals' => $deals1),
    				array(
    						'info_company' => $info_company2,
    						'beacon_data'=>$beacon_data2,
    						'deals' => $deals2),
    		)
    		);
    	}
    }
}
