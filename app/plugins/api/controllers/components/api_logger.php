<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('vendor', 'Utility', array('file' => 'Utility/ApiLogger.php'));
class ApiLoggerComponent extends ApiBaseComponent {

  private $logger = null;

  public function initialize(&$controller) {
    $logNamePrefix = null;
    if (isset($controller->logFileNamePrefix)) {
      $logNamePrefix = $controller->logFileNamePrefix;
    }
    $this->logger = new ApiLogger(get_class($controller).'['.$controller->getRequestedAction().']', $logNamePrefix);
  }

  public function info($msg, $data=null) {
    $this->logger->info($msg, $data);
  }

  public function notice($msg, $data=null) {
    $this->logger->notice($msg, $data);
  }

  public function error($msg, $data=null) {
    $this->logger->debug("ERROR|".$msg, $data);
    $this->logger->error($msg, $data);
  }

  public function warning($msg, $data=null) {
    $this->logger->warning($msg, $data);
  }

  public function debug($msg, $data=null) {
    $this->logger->debug($msg, $data);
  }

  public function trace($msg, $data=null) {
    $this->logger->trace($msg, $data);
  }

  public function log($msg, $data=null, $severity=null, $name=null, $dmc=null) {
    $this->logger->log($msg, $data, $severity, $name, $dmc);
  }

  public function putMDCFieldsByController($controller) {
    return $this->putMDCFields($controller->mdcFields, $controller->params);
  }

  public function reset() {
    $this->logger->reset();
  }

  public function get($key) {
    return $this->logger->get($key);
  }

  public function put($key, $val) {
    $this->logger->put($key, $val);
  }

  public function putAll($items) {
    foreach ($items as $key => $val) {
      $this->logger->put($key, $val);
    }
  }

  public function startup(&$controller) {
    if ($this->isUserLoggedIn($controller)) {
      $this->put('user', $controller->Auth->user('email'));
    }
  }

  private function isUserLoggedIn($controller) {
    $user = $controller->Auth->user();
    return !empty($user);
  }

  public function logRedirectToHomeForError($e) {
    $this->error('Error , Redireccionando a la Home.', array('error' => $e->getMessage()));
    $this->logRedirectToHome();
  }

  public function logRedirectToHome() {
    $this->notice('Redireccionando a la Home.');
  }

  public function logRedirectToActualPath() {
    $this->notice('Redireccionando al path actual.');
  }

  public function logRedirectTo404() {
    $this->notice('Redireccionando a la pagina de error 404.');
  }
}
