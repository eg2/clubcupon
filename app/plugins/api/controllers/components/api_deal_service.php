<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealExternal');
App::import('Model', 'api.ApiBranchDeal');
App::import('Model', 'api.ApiPaymentOptionDeal');

class ApiDealServiceComponent extends ApiBaseComponent {

    public $name = 'ApiDealService';
    public $components = array(
        'api.ApiDebug'
    );

    public function __construct() {
        $this->ApiDeal = & new ApiDeal();
        $this->ApiDealExternal = & new ApiDealExternal();
        $this->ApiBranchDeal = & new ApiBranchDeal();
        $this->ApiPaymentOptionDeal = & new ApiPaymentOptionDeal();
        parent::__construct();
    }

    public function setStatusClosed($deal) {
        if ($this->isNow($deal)) {
            $ret = $this->updateStatusToClosedDealNow($deal);
        } else {
            $ret = $this->updateStatusToClosedDealCC($deal);
        }
        return $ret;
    }

    public function setStatusTipped($deal) {
        if ($this->ApiDeal->isNow($deal)) {
            $ret = $this->updateStatusToTippedDealNow($deal);
        } else {
            $ret = $this->updateStatustoTippedDealCC($deal);
        }
        return $ret;
    }

    protected function updateStatusToTippedDealNow($deal) {
        $db = $this->ApiDeal->getDataSource();
        $this->ApiBranchDeal->updateAll(array(
            'NowBranchesDeals.now_deal_status_id' => ApiDeal::STATUS_TIPPED
                ), array(
            'NowBranchesDeals.now_deal_id' => $deal['Deal']['id']
        ));
        return $this->ApiDeal->updateAll(array(
                    'Deal.deal_status_id' => ApiDeal::STATUS_TIPPED,
                    'Deal.deal_tipped_time' => '\'' . date('Y-m-d H:i:s') . '\''
                        ), array(
                    'Deal.deal_status_id' => ApiDeal::STATUS_OPEN,
                    'Deal.deal_user_count >=' => $db->expression('Deal.min_limit'),
                    'Deal.id' => $deal['Deal']['id']
        ));
    }

    protected function updateStatusToTippedDealCC($deal) {
        $db = $this->ApiDeal->getDataSource();
        $ret = $this->ApiDeal->updateAll(array(
            'Deal.deal_status_id' => ApiDeal::STATUS_TIPPED,
            'Deal.deal_tipped_time' => '\'' . date('Y-m-d H:i:s') . '\''
                ), array(
            'Deal.deal_status_id' => ApiDeal::STATUS_OPEN,
            'Deal.deal_user_count >=' => $db->expression('Deal.min_limit'),
            'Deal.id' => $deal['Deal']['id']
        ));
        return $ret;
    }

    protected function updateStatusToClosedDealNow($deal) {
        $conditions['NowBranchesDeals.now_deal_id'] = $deal['Deal']['id'];
        return $this->ApiBranchDeal->updateAll(array(
                    'NowBranchesDeals.now_deal_status_id' => ApiDeal::STATUS_CLOSED
                        ), $conditions);
    }

    protected function updateStatusToClosedDealCC($deal, $date = false) {
        $when = date('Y-m-d H:i:s');
        if ($date != false) {
            $when = date('Y-m-d H:i:s', strtotime($date));
        }
        $conditions['Deal.id'] = $deal['Deal']['id'];
        return $this->updateAll(array(
                    'Deal.total_purchased_amount' => '(Deal.discounted_price * Deal.deal_user_count)',
                    'Deal.total_commission_amount' => '(Deal.bonus_amount + ( Deal.total_purchased_amount *
           ( Deal.commission_percentage / 100 )))',
                    'Deal.end_date' => '"' . $when . '"',
                    'Deal.deal_status_id' => ApiDeal::STATUS_CLOSED
                        ), $conditions);
    }

    public function updateStatusDeal($deal) {
        if ($deal['Deal']['deal_status_id'] == ApiDeal::STATUS_OPEN) {
            $this->setStatusTipped($deal);
        } elseif ($this->hasExceedMaxLimit($deal)) {
            $this->setStatusClosed($deal);
        }
        return $this;
    }

    public function hasExceedMaxLimit($deal) {
        return (!empty($deal['Deal']['max_limit']) && $deal['Deal']['deal_user_count'] >= $deal['Deal']['max_limit']);
    }

    public function couponStartDate($deal, $dealUser) {
        $couponStartDate = $deal['Deal']['coupon_start_date'];
        if ($this->ApiDeal->isVariableExpiration($deal)) {
            $couponStartDate = array_key_exists('DealUser', $dealUser) ? $dealUser['DealUser']['created'] : $dealUser['created'];
        }
        return $couponStartDate;
    }

    public function couponExpirationDate($deal, $dealUser) {
        $couponExpirationDate = $deal['Deal']['coupon_expiry_date'];
        if ($this->ApiDeal->isVariableExpiration($deal)) {
            $since = max($deal['Deal']['start_date'], $deal['Deal']['coupon_start_date'], $dealUser['DealUser']['created']);
            $duration = max($deal['Deal']['coupon_duration'] - 1, 0);
            $couponExpirationDate = date('Y-m-d 23:59:00', strtotime($since . " +{$duration} day"));
        } else {
            $date = strtotime($deal['Deal']['coupon_expiry_date']);
            $hours = date('H', $date);
            $minutes = date('I', $date);
            if ($hours == 0 && $minutes == 0) {
                $couponExpirationDate = date('Y-m-d 23:59:00', strtotime($deal['Deal']['coupon_expiry_date'] . " - 1 day"));
            }
        }
        return $couponExpirationDate;
    }

    public function couponLastExpirationDate($deal) {
        return $this->ApiDeal->couponLastExpirationDate($deal);
    }

    public function couponExpirationDateFromNow($deal) {
        $couponExpirationDate = $deal['Deal']['coupon_expiry_date'];
        if ($this->ApiDeal->isVariableExpiration($deal)) {
            $since = max($deal['Deal']['coupon_start_date'], time());
            $duration = max($deal['Deal']['coupon_duration'] - 1, 0);
            $couponExpirationDate = date('Y-m-d 23:59:00', strtotime($since . " +{$duration} day"));
        }
        return $couponExpirationDate;
    }

    public function isCouponExpired($deal, $dealUser) {
        $today = strtotime(date("Y-m-d"));
        $couponExpirationDate = strtotime($this->couponExpirationDate($deal, $dealUser));
        return $couponExpirationDate > $today;
    }

    public function propagatePaymentOptions($dealId) {
        $this->ApiLogger->debug(__FUNCTION__, array(
            'dealId' => $dealId
        ));
        $deal = $this->ApiDeal->findForApiById($dealId);
        if (isset($deal)) {
            $allSubdeals = $this->ApiDeal->findAllSubDealsForApi($deal);
            if (isset($allSubdeals) && !empty($allSubdeals)) {
                $idAllSubdeals = $this->getIdAllSubdeals($allSubdeals);
                $list = implode(',', $idAllSubdeals);
                $query = " DELETE FROM `payment_option_deals` WHERE `deal_id` IN ({$list}) ";
                $deleted = $this->ApiPaymentOptionDeal->query($query);
                $allPaymentOptions = $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($deal['Deal']['id']);
                foreach ($allSubdeals as $subdeal) {
                    $this->ApiPaymentOptionDeal->saveMultipleRowsByDeal($allPaymentOptions, $subdeal);
                }
                $this->ApiLogger->debug(__FUNCTION__, array(
                    '#allSubdeals' => count($allSubdeals),
                    'deleted' => $deleted,
                    '#allPaymentOptions' => count($allPaymentOptions)
                ));
            }
        }
    }

    private function getIdAllSubdeals($allSubdeals) {
        $idAllSubdeals = array();
        foreach ($allSubdeals as $deal) {
            $idAllSubdeals[] = $deal['Deal']['id'];
        }
        return $idAllSubdeals;
    }

}
