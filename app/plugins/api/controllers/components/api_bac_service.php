<?php

App::import('Component', 'api.ApiBac');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiPaymentService');
App::import('Component', 'product.ProductInventoryService');
App::import('Component', 'shipping.ShippingAddressService');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealExternal');
App::import('Model', 'api.ApiPaymentType');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiUserProfile');
App::import('Model', 'api.ApiState');
App::import('Model', 'api.ApiPaymentPlanItem');
App::import('Model', 'product.ProductProduct');
App::import('Model', 'shipping.ShippingAddress');

class ApiBacServiceComponent extends ApiBaseComponent {

    const BAC_IVA_CONDITION_CODE_EXCENTO = 7;
    const BAC_IVA_CONDITION_CODE_MONOTRIBUTO = 5;
    const BAC_IVA_CONDITION_CODE_RI = 1;
    const BAC_TOURISM_PRODUCTID_IVA_FULL = 103033;
    const BAC_TOURISM_PRODUCTID_IVA_HALF = 103032;
    const BAC_TOURISM_PRODUCTID_EXEMPT = 103029;
    const BAC_TOURISM_PRODUCTID_RETAIL = 103034;
    const BAC_TOURISM_PRODUCTID_RESOLUCION_3450 = 104403;

    public $name = 'ApiBacService';
    public $components = array(
        'api.ApiDebug',
        'api.ApiBac',
        'api.ApiPaymentService'
    );
    private $id_portal = null;
    private $bac_tourism_id_old = null;
    private $CapitalFederal = null;
    private $Argentina = null;
    private $moneda_pesos = null;
    private $id_producto = null;
    private $id_producto_final = null;
    private $id_producto_final_old = null;
    private $id_producto_debito = null;
    private $cc_bac_secure_code = null;
    private $cc_bac_secure_code_old = null;
    private $cctur_bac_secure_code_old = null;
    private $id_tipo_usuario_usuario = null;
    private $id_tipo_usuario_empresa = null;
    private $bac_tourism_id_producto_old = null;
    private $bac_tourism_id_producto_debito_old = null;
    private $id_portal_old = null;
    private $id_tipo_documento_empresa = null;
    private $idProvinceBac = null;
    private $threatMetrix = null;
    private $isActiveBacShippingInfo = null;
    private $isActiveSendProductIdInventory = null;
    private $bacOrigenPortal = null;

    public function __construct() {
        parent::__construct();
        $this->ApiBac = ClassRegistry::init('ApiBacComponent');
        $this->ApiPaymentService = ClassRegistry::init('ApiPaymentServiceComponent');
        $this->ApiCompany = ClassRegistry::init('ApiCompany');
        $this->ApiDeal = ClassRegistry::init('ApiDeal');
        $this->ApiDealExternal = ClassRegistry::init('ApiDealExternal');
        $this->ApiPaymentType = ClassRegistry::init('ApiPaymentType');
        $this->ApiUser = ClassRegistry::init('ApiUser');
        $this->ApiUserProfile = ClassRegistry::init('ApiUserProfile');
        $this->ApiState = ClassRegistry::init('ApiState');
        $this->ApiPaymentPlanItem = ClassRegistry::init('ApiPaymentPlanItem');
        $this->ProductInventoryService = ClassRegistry::init('ProductInventoryServiceComponent');
        $this->ShippingAddressService = ClassRegistry::init('ShippingAddressServiceComponent');
        $this->ShippingAddress = ClassRegistry::init('ShippingAddress');
        $this->bacOrigenPortal = Configure::read('BAC.bac_origen_portal');
        $this->isActiveBacShippingInfo = Configure::read('BAC.sendShippingInfo');
        $this->isActiveSendProductIdInventory = Configure::read('BAC.sendProductIdInventory');
        $this->CapitalFederal = Configure::read('BAC.CapitalFederal');
        $this->id_tipo_usuario_usuario = Configure::read('BAC.id_tipo_usuario_usuario');
        $this->id_tipo_usuario_empresa = Configure::read('BAC.id_tipo_usuario_empresa');
        $this->Argentina = Configure::read('BAC.Argentina');
        $this->cctur_bac_secure_code_old = Configure::read('cctur.bac.secure_code_old');
        $this->cc_bac_secure_code_old = Configure::read('cc.bac.secure_code_old');
        $this->cc_bac_secure_code = Configure::read('cc.bac.secure_code');
        $this->moneda_pesos = Configure::read('BAC.pesos');
        $this->id_producto_final = Configure::read('BAC.id_producto_final');
        $this->id_portal = Configure::read('BAC.id_portal');
        $this->id_producto = Configure::read('BAC.id_producto');
        $this->id_producto_debito = Configure::read('BAC.id_producto_debito');
        $this->bac_tourism_id_producto_debito_old = Configure::read('BAC.bac_tourism_id_producto_debito_old');
        $this->bac_tourism_id_old = Configure::read('BAC.bac_tourism_id_old');
        $this->bac_tourism_id_producto_old = Configure::read('BAC.bac_tourism_id_producto_old');
        $this->id_portal_old = Configure::read('BAC.id_portal_old');
        $this->id_producto_old = Configure::read('BAC.id_producto_old');
        $this->id_producto_debito_old = Configure::read('BAC.id_producto_debito_old');
        $this->id_producto_final_old = Configure::read('BAC.id_producto_final_old');
        $this->id_tipo_documento_empresa = Configure::read('BAC.id_tipo_documento_empresa');
        $this->idProvinceBac = Configure::read('BAC.id_provincia_id_state');
        $this->threatMetrix = Configure::read('order.ThreatMetrix');
        $this->ProductProduct = new ProductProduct();
    }

    public function createBacUserByDeal($user = null, $deal = null) {
        return $this->createBacUser($user, $this->getBacIdPortalByDeal($deal), $this->getUserBacIdFieldByDeal($deal));
    }

    public function createBacUser($user, $portalId = null, $bacIdFieldName = null) {
        $this->ApiLogger->trace(__FUNCTION__, array(
            'portalId' => $portalId,
            'bacIdFieldName' => $bacIdFieldName
        ));
        if (!is_array($user)) {
            throw new Exception("User param is not valid, must be array.");
        }
        if (is_null($portalId)) {
            $portalId = $this->id_portal;
        }
        if (is_null($bacIdFieldName)) {
            $bacIdFieldName = 'bac_id';
        }
        $userProfile = $this->ApiUserProfile->findByUserId($user['User']['id']);
        $params = $this->buildBacParamForCreateUser($user, $userProfile, $portalId, $bacIdFieldName);
        try {
            $bac_user_id = $this->ApiBac->createOrUpdateUser($params);
        } catch (Exception $e) {
            $bac_user_id = 0;
            $this->ApiLogger->error('createBacUser', array(
                'exception' => $e->getMessage()
            ));
            throw new Exception('no se pudo crear el usuario en BAC');
        }
        if ($bac_user_id != null || is_numeric($bac_user_id)) {
            $this->ApiUser->updateAll(array(
                'User.' . $bacIdFieldName => $bac_user_id
                    ), array(
                'User.id' => $user['User']['id']
            ));
        }
        return $bac_user_id;
    }

    public function createBacCompanyUserByDeal($company, $deal) {
        return $this->createBacCompanyUser($company, $this->getBacIdPortalByDeal($deal), $this->getUserBacIdFieldByDeal($deal));
    }

    public function createBacCompanyUser($company, $portalId = null, $bacIdFieldName = null) {
        $this->ApiLogger->trace(__FUNCTION__, array(
            'portalId' => $portalId,
            'bacIdFieldName' => $bacIdFieldName
        ));
        if (!is_array($company)) {
            throw new Exception("Company param is not valid, must be array.");
        }
        if (is_null($portalId)) {
            $portalId = $this->id_portal;
        }
        if (is_null($bacIdFieldName)) {
            $bacIdFieldName = 'bac_id';
        }
        $user = $this->ApiUser->findById($company['Company']['user_id']);
        $params = $this->buildBacParamForCreateCompanyUser($company, $user, $portalId, $bacIdFieldName);
        try {
            $bac_user_id = $this->ApiBac->createOrUpdateUser($params);
        } catch (Exception $e) {
            $bac_user_id = 0;
            throw new Exception('no se pudo crear el usuario en BAC');
        }
        if ($bac_user_id != null || is_numeric($bac_user_id)) {
            $this->ApiLogger->trace('update Company bac_id', array(
                'bacIdFieldName' => $bacIdFieldName,
                'value' => $bac_user_id
            ));
            $this->ApiCompany->updateAll(array(
                'Company.' . $bacIdFieldName => $bac_user_id
                    ), array(
                'Company.id' => $company['Company']['id']
            ));
        }
        return $bac_user_id;
    }

    private function buildBacParamForCreateCompanyUser($company, $companyUser, $portalId, $bacIdFieldName) {
        $this->ApiLogger->trace(__FUNCTION__, array(
            'company' => $company,
            'companyUser' => $companyUser,
            'portalId' => $portalId,
            'bacIdFieldName' => $bacIdFieldName
        ));
        $params = array(
            'idTipoUsuario' => $this->id_tipo_usuario_empresa,
            'idPortal' => $portalId,
            'idPortal' => 18,
            'idUsuarioPortal' => $company['Company']['user_id'],
            'login' => $company['Company']['name'],
            'email' => $companyUser['User']['email'],
            'razonSocial' => $company['Company']['fiscal_name'],
            'domicilioComercial' => $company['Company']['fiscal_address'],
            'localidadComercial' => $this->getNameStateById($company['Company']['fiscal_state_id']),
            'codigoPostalComercial' => $company['Company']['zip'],
            'telefonoComercial' => $company['Company']['fiscal_phone'],
            'idCondicionIva' => $this->getBacIvaCondition($company['Company']['fiscal_cond_iva']),
            'idTipoDocumento' => $this->id_tipo_documento_empresa,
            'nroDocumento' => $company['Company']['fiscal_cuit'],
            'nroInscriptoIIBB' => $company['Company']['fiscal_iibb'],
            'idProvincia' => $this->getIdProvinceBac($company),
            'idPais' => $this->Argentina
        );
        $this->ApiLogger->trace(__FUNCTION__, array(
            'field' => $bacIdFieldName,
            'value' => $company['Company'][$bacIdFieldName]
        ));
        if ($company['Company'][$bacIdFieldName] != 0) {
            $params['idUsuario'] = $company['Company'][$bacIdFieldName];
        } else {
            $params['idUsuario'] = null;
        }
        return $params;
    }

    public function getBacPaymentInfo($dealExternalId, $ipClient) {
        $dealExternal = $this->ApiDealExternal->findById($dealExternalId);
        $use_litebox = $this->ApiPaymentService->useLiteBox($dealExternal['DealExternal']['payment_type_id']);
        $bac_info = $this->getBacInfoForSecurePayment($dealExternal, $ipClient);
        $this->ApiLogger->put('DealExternalId', $dealExternalId);
        $this->ApiLogger->notice('Obteniendo Parametro a enviar a BAC.', $bac_info);
        return array(
            $bac_info,
            $use_litebox
        );
    }

    private function getBacIvaCondition($ivaCondition) {
        switch ($ivaCondition) {
            case 'ri':
                return self::BAC_IVA_CONDITION_CODE_RI;
            case 'exento':
                return self::BAC_IVA_CONDITION_CODE_EXCENTO;
            case 'monotributo':
                return self::BAC_IVA_CONDITION_CODE_MONOTRIBUTO;
            default:
                throw new Exception('La condicion de iva no tiene codigo bac definido.' . json_encode(array(
                    'condicion de iva en clubcupon' => $ivaCondition
                )));
        }
    }

    public function iniciarPagoSeguro($params) {
        try {
            $token = $this->ApiBac->initializePaymente($params);
        } catch (Exception $e) {
            $this->ApiLogger->error("error al iniciar el pago en Bac", array(
                'exception' => $e->getMessage(),
                'params' => $params
            ));
            $this->ApiLogger->notice("error al iniciar el pago en Bac", array(
                'exception' => $e->getMessage(),
                'params' => $params
            ));
            throw new Exception("No se pudo iniciar el pago en Bac");
        }
        return $token;
    }

    public function getBacIdPortalByDeal($deal) {
        $idPortal = $this->id_portal;
        $this->ApiLogger->trace(__FUNCTION__, array(
            'deal' => $deal,
            'isTecDia' => $this->ApiDeal->isTecDia($deal),
            'bac_tourism_id_old' => $this->bac_tourism_id_old,
            'id_portal_old' => $this->id_portal_old,
            'id_portal' => $this->id_portal,
            'return' => $idPortal
        ));
        return $idPortal;
    }

    private function getUserBacIdFieldByDeal($deal) {
        $bacIdFieldName = null;
        if ($this->ApiDeal->isTecDia($deal)) {
            $bacIdFieldName = 'bac_id';
        } else {
            if ($this->ApiDeal->isTourism($deal)) {
                $bacIdFieldName = 'bac_tourism_id';
            } else {
                $bacIdFieldName = 'bac_user_id';
            }
        }
        return $bacIdFieldName;
    }

    private function buildBacParamForCreateUser($user, $userProfile, $portalId, $bacIdFieldName) {
        if (is_null($user['User']['username'])) {
            $loginUserNameForBAC = $user['User']['id'];
        } else {
            $loginUserNameForBAC = $user['User']['username'];
        }
        $params = array(
            'idTipoUsuario' => $this->id_tipo_usuario_usuario,
            'idPortal' => $portalId,
            'idUsuarioPortal' => $user['User']['id'],
            'login' => $loginUserNameForBAC,
            'email' => $user['User']['email'],
            'nombre' => $userProfile['UserProfile']['first_name'],
            'apellido' => $userProfile['UserProfile']['last_name'],
            'idProvincia' => $this->CapitalFederal,
            'idPais' => $this->Argentina
        );
        if ($user['User'][$bacIdFieldName] != 0) {
            $params['idUsuario'] = $user['User'][$bacIdFieldName];
        } else {
            $params['idUsuario'] = null;
        }
        return $params;
    }

    private function getBacInfoForSecurePayment($dealExternal, $ipClient) {
        $deal = $this->ApiDeal->findById($dealExternal['DealExternal']['deal_id']);
        $params = $this->getParamsToBac($dealExternal, $deal, $ipClient);
        $this->ApiLogger->notice('getBacInfoForSecurePayment::Parametros enviados a BAC', $params);
        $token = $this->iniciarPagoSeguro($params);
        $idGateway = $this->getIdGateway($dealExternal);
        $secureCode = $this->getSecureCode($deal);
        $idPortal = $this->getBacIdPortalByDeal($deal);
        $secureHash = md5($idGateway . $token . $idPortal . $secureCode);
        $bacInfo = array(
            'id_gateway' => $idGateway,
            'token' => $token,
            'id_portal' => $idPortal,
            'secure_hash' => $secureHash
        );
        return $bacInfo;
    }

    private function getSecureCode($deal) {
        return $this->cc_bac_secure_code;
    }

    private function isTarjetaNaranjaEnTresPagos($dealExternal) {
        //payment_plan_option_id -> payment_plan_items.id
        return in_array($dealExternal['DealExternal']['payment_plan_option_id'], array(17));
    }

    public function getInstallmentsbyDealExternal($dealExternal) {
        $installments = 1;
        if (!empty($dealExternal['DealExternal']['payment_plan_option_id'])) {
            if ($this->isTarjetaNaranjaEnTresPagos($dealExternal)) {
                $installments = 11;
            } else {
                $paymentPlanItem = $this->ApiPaymentPlanItem->find('first', array(
                    'fields' => 'payment_installments',
                    'conditions' => array(
                        'id' => $dealExternal['DealExternal']['payment_plan_option_id']
                    )
                ));
                if (!empty($paymentPlanItem)) {
                    $installments = $paymentPlanItem ['ApiPaymentPlanItem']['payment_installments'];
                }
            }
        }
        return $installments;
    }

    private function getParamsToBac($dealExternal, $deal, $ipClient) {
        $params = new stdClass();
        $productProduct = array();
        $sendShippingInfo = ($this->isEnableToSendCustomProductId($deal) && $this->isEnableToSendBacStorehouseId($dealExternal, $deal));
        if ($sendShippingInfo) {
            $this->ApiLogger->notice('Condiciones para enviar datos de modalidad de envío', array(
                '$sendShippingInfo' => $sendShippingInfo
            ));
            $params->datosEnvio = $this->getShippingData($dealExternal);
            $params->idOrigen = $this->bacOrigenPortal;
            $productProduct = $this->ProductProduct->findById($deal['Deal']['product_product_id']);
        }
        $instalments = $this->getInstallmentsbyDealExternal($dealExternal);
        $productos = $this->getParamsToProducts($dealExternal, $deal, $productProduct);
        $params->idUsuarioPortal = $dealExternal['DealExternal']['user_id'];
        $params->idPortal = $this->getBacIdPortalByDeal($deal);
        $params->productos = $productos;
        $params->idPagoPortal = $dealExternal['DealExternal']['id'];
        $params->idMedioPago = $this->getBacPaymentId($dealExternal);
        $params->idGateway = $this->getIdGateway($dealExternal);
        $params->cantidadCuotas = $instalments;
        $params->descripcion = $this->getDealDescription($deal);
        $params->ipUsuario = $ipClient;
        if ($this->isSendingSessionUserId()) {
            $params->idSesionUsuario = session_id();
        }
        return $params;
    }

    private function getShippingData($dealExternal) {
        $userProfile = $this->ApiUserProfile->findByUserId($dealExternal['DealExternal']['user_id']);
        $shippingAddress = $this->ShippingAddress->findById($dealExternal['DealExternal']['shipping_address_id']);
        $shippingInfo = array(
            'nombre' => $userProfile['UserProfile']['first_name'],
            'apellido' => $userProfile['UserProfile']['last_name'],
            'idCondicionEnvio' => $shippingAddress['ShippingAddress']['libertya_delivery_mode_id'],
            'idAlmacenEntrega' => $shippingAddress['ShippingAddress']['bac_storehouse_id'],
            'contrareembolso' => 0
        );
        $this->ApiLogger->trace('Datos de modalidad de envío', $shippingInfo);
        return $shippingInfo;
    }

    private function isSendingSessionUserId() {
        return $this->threatMetrix;
    }

    private function getParamsToProducts($dealExternal, $deal, $productProduct = array()) {
        if ($this->ApiDeal->isTourism($deal) && $this->ApiDeal->isWholesaler($deal)) {
            $productos = $this->getProductToTourism($dealExternal, $deal);
        } elseif ($this->ApiPaymentService->isCombined($dealExternal)) {
            $productos = $this->getProductToPaymentCombined($dealExternal, $deal);
        } else {
            $productos = $this->getProductToPaymentDirect($dealExternal, $deal, $productProduct);
        }
        $this->ApiLogger->trace(__FUNCTION__, array(
            'productos' => $productos
        ));
        return $productos;
    }

    private function getAmountRetail($deal) {
        $price = $deal['Deal']['discounted_price'];
        $amount_exempt = $deal['Deal']['amount_exempt'];
        $amount_full_iva = $deal['Deal']['amount_full_iva'];
        $amount_half_iva = $deal['Deal']['amount_half_iva'];
        $amount_resolution_3450 = $deal['Deal']['amount_resolution_3450'];
        return ($price - ($amount_exempt + $amount_full_iva + $amount_half_iva + $amount_resolution_3450));
    }

    private function getProductToTourism($dealExternal, $deal) {
        $isdebito = false;
        $quantity = $dealExternal['DealExternal']['quantity'];
        $moneda = $this->moneda_pesos;
        $amount_exempt = $deal['Deal']['amount_exempt'];
        $amount_full_iva = $deal['Deal']['amount_full_iva'];
        $amount_half_iva = $deal['Deal']['amount_half_iva'];
        $amount_retail = $this->getAmountRetail($deal);
        $amount_resolucion3450 = $deal['Deal']['amount_resolution_3450'];
        $items = array();
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_exempt, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_EXEMPT);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_full_iva, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_IVA_FULL);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_half_iva, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_IVA_HALF);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_retail, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_RETAIL);
        $items[] = $this->getProductsForBac($deal, $quantity, $amount_resolucion3450, $moneda, $isdebito, self::BAC_TOURISM_PRODUCTID_RESOLUCION_3450);
        if ($this->ApiPaymentService->isCombined($dealExternal) || $this->hasDiscount($dealExternal)) {
            $items[] = $this->addDiscount($dealExternal, $deal);
        }
        return $items;
    }

    private function getQuantity($dealExternal, $productProduct) {
        $quantity = $dealExternal['DealExternal']['quantity'];
        if (!empty($productProduct)) {
            $quantity = $dealExternal['DealExternal']['quantity'] * $productProduct['ProductProduct']['decremented_units'];
        }
        return $quantity;
    }

    private function getAmount($deal, $productProduct) {
        $mount = $deal['Deal']['discounted_price'];
        if (!empty($productProduct)) {
            $mount = round($deal['Deal']['discounted_price'] / $productProduct['ProductProduct']['decremented_units'], 2);
        }
        return $mount;
    }

    private function getProductToPaymentCombined($dealExternal, $deal, $productProduct = array()) {
        $quantity = $this->getQuantity($dealExternal, $productProduct);
        $mount = $this->getAmount($deal, $productProduct);
        $moneda = $this->moneda_pesos;
        $isdebito = false;
        $idProduct = $this->getIdProduct($deal, $dealExternal);
        $prod = $this->getProductsForBac($deal, $quantity, $mount, $moneda, $isdebito, $idProduct);
        $prodDebito = $this->addDiscount($dealExternal, $deal);
        $productos = array(
            $prod,
            $prodDebito
        );
        return $productos;
    }

    private function getProductToPaymentDirect($dealExternal, $deal, $productProduct = array()) {
        $quantity = $this->getQuantity($dealExternal, $productProduct);
        $mount = $this->getAmount($deal, $productProduct);
        $moneda = $this->moneda_pesos;
        $isdebito = false;
        $idProduct = $this->getIdProduct($deal, $dealExternal);
        $prod = $this->getProductsForBac($deal, $quantity, $mount, $moneda, $isdebito, $idProduct);
        $items[] = $prod;
        if ($this->hasDiscount($dealExternal)) {
            $discount = $this->addDiscount($dealExternal, $deal);
            $items[] = $discount;
        }
        return $items;
    }

    protected function addDiscount($dealExternal, $deal) {
        $isdebito = true;
        $quantity = 1;
        $moneda = $this->moneda_pesos;
        $mount = Precision::sub($dealExternal['DealExternal']['final_amount'], $dealExternal['DealExternal']['parcial_amount']);
        $idProduct = $this->getIdProductDebito($deal);
        $discount = $this->getProductsForBac($deal, $quantity, $mount, $moneda, $isdebito, $idProduct);
        return $discount;
    }

    protected function hasDiscount($dealExternal) {
        return $dealExternal['DealExternal']['final_amount'] != $dealExternal['DealExternal']['parcial_amount'] && $dealExternal['DealExternal']['parcial_amount'] > 0;
    }

    protected function getBacPaymentId($dealExternal) {
        return $this->ApiPaymentType->findBacPaymentTypeId($dealExternal['DealExternal']['payment_type_id']);
    }

    protected function isEnableToSendBacStorehouseId($dealExternal, $deal) {
        $isEnable = false;
        $hasProductStragegy = $this->dealHasProduct($deal);
        $isProductInventoryEnabledToBeSentToBac = $this->ProductInventoryService->isProductInventoryEnabledToBeSentToBacByProductId($deal['Deal']['product_product_id']);
        if (!empty($dealExternal['DealExternal']['shipping_address_id'])) {
            $shippingAddress = $this->ShippingAddress->findById($dealExternal['DealExternal']['shipping_address_id']);
            if (empty($shippingAddress)) {
                $this->ApiLogger->notice('Tiene Punto de Retiro Asociado y el' . ' tipo de Punto de Retiro NO esta definido', array(
                    'shipping_address_id' => $dealExternal['DealExternal']['shipping_address_id'],
                    'dealExternal' => $dealExternal
                ));
                $isEnable = false;
            } else {
                $hasLogistic = $this->ShippingAddress->isManagedLogistic($shippingAddress);
                $hasStorehouseId = $this->ShippingAddress->hasStorehouseId($shippingAddress);
                $isEndUser = $this->ApiDeal->isEndUser($deal);
                $isEnable = ($hasProductStragegy && $isProductInventoryEnabledToBeSentToBac && $hasLogistic && $hasStorehouseId && $isEndUser && $this->isActiveBacShippingInfo);
            }
        }
        $this->ApiLogger->notice('isEnableToSendBacStorehouseId', array(
            'Es Oferta de Precompra' => $isEndUser,
            'isEnableToSendBacStorehouseId' => $isEnable,
            'Administra Stock' => $hasProductStragegy,
            'Tiene Producto de Inventario' => $isProductInventoryEnabledToBeSentToBac,
            'El punto de Retiro es de CMD' => $hasLogistic,
            'Tiene ID de almacen asociado' => $hasStorehouseId
        ));
        return $isEnable;
    }

    protected function getBacStorehouseId($dealExternal) {
        $shippingAddress = $this->ShippingAddress->findById($dealExternal['DealExternal']['shipping_address_id']);
        return $shippingAddress['ShippingAddress']['bac_storehouse_id'];
    }

    protected function getDealDescription($deal) {
        $description = mb_substr($deal['Deal']['name'], 0, 94);
        $description = ApiStrUtils::cleanChars($description);
        $this->ApiLogger->notice('descripcion para BAC', array(
            'description' => $description
        ));
        return $description;
    }

    protected function getIdGateway($dealExternal) {
        $paymentType = $this->ApiPaymentType->findById($dealExternal['DealExternal']['payment_type_id']);
        return $paymentType['PaymentType']['gateway_id'];
    }

    protected function getProductsForBac($deal, $quantity, $mount, $moneda, $isDebito, $idProduct) {
        $prod = new stdClass();
        if ($isDebito) {
            $prod->idProducto = $idProduct;
            $prod->monto = (-1) * $mount;
        } else {
            $prod->idProducto = $idProduct;
            $prod->monto = $mount;
        }
        $prod->cantidad = (int) $quantity;
        $prod->moneda = $moneda;
        return $prod;
    }

    protected function generateProductString($productos) {
        foreach ($productos as $val) {
            $producto = (array) $val;
            foreach ($producto as $nameattr => $valattr) {
                $stringProducts = $stringProducts . "$nameattr=$valattr,";
            }
            $stringProducts = substr($stringProducts, 0, -1);
            $stringProducts = $stringProducts . ';';
        }
        $stringProducts = substr($stringProducts, 0, -1);
        return $stringProducts;
    }

    public function isEnableToSendCustomProductId($deal) {
        $a = $this->dealHasProduct($deal);
        $b = $this->isActiveSendProductIdInventory;
        $c = $this->ApiDeal->isEndUser($deal);
        $d = $this->ProductInventoryService->isProductInventoryEnabledToBeSentToBacByProductId($deal['Deal']['product_product_id']);
        $this->ApiLogger->notice('isEnableToSendCustomProductId: Se debe ' . 'cumplir A, B, C y D', array(
            'tiene Producto Clubcupon' => $a,
            'isActiveSendProductIdInventory' => $b,
            'es Oferta Precompra' => $c,
            'Tiene un producto de Inventario' => $d
        ));
        $isEnabled = $a && $b && $c && $d;
        return $isEnabled;
    }

    protected function getIdProduct($deal, $dealExternal) {
        if ($this->isEnableToSendCustomProductId($deal) && $this->isEnableToSendBacStorehouseId($dealExternal, $deal)) {
            $idProducto = $this->getBacProductInventoryIdByDeal($deal);
            $this->ApiLogger->notice('Está activo el envío de productos' . ' Imagena/Clubcupon a Bac', array(
                'idProducto' => $idProducto
            ));
        } else {
            if ($this->ApiDeal->isEndUser($deal)) {
                $idProducto = $this->id_producto_final;
            } else {
                $idProducto = $this->id_producto;
            }
        }
        return $idProducto;
    }

    protected function dealHasProduct($deal) {
        return !empty($deal['Deal']['product_product_id']);
    }

    protected function getBacProductInventoryIdByDeal($deal) {
        $productInventory = $this->ProductInventoryService->findProductInventoryByProducProductId($deal['Deal']['product_product_id']);
        return $productInventory['ProductInventory']['bac_product_id'];
    }

    protected function getIdProductDebito($deal) {
        $idProducto = $this->id_producto_debito;
        return $idProducto;
    }

    protected function getNameStateById($stateId) {
        $exist = array_key_exists($stateId, $this->idProvinceBac);
        if ($exist) {
            $state = $this->ApiState->read('name', $stateId);
            $state = $state['ApiState']['name'];
        } else {
            $state = Configure::read('BAC.localidadComercial');
        }
        return $state;
    }

    protected function getIdProvinceBac($company) {
        $idProvince = array();
        $exist = array_key_exists($company['Company']['fiscal_state_id'], $this->idProvinceBac);
        if ($exist) {
            $idProvince = $this->idProvinceBac[$company['Company']['fiscal_state_id']];
        } else {
            $idProvince = $this->CapitalFederal;
        }
        return $idProvince;
    }

}
