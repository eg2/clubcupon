<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiDealExternal');

class ApiCxsenseServiceComponent extends ApiBaseComponent {

    const URL_DATE = 'https://api.cxense.com/public/date';
    const URL_UPDATE = 'https://api.cxense.com/profile/user/external/update';
    const URL_READ = 'https://api.cxense.com/profile/user/external/read';
    const API_KEY = 'api&user&EXHmfJ3Xs6TzhhV/nut/9g==';
    const USERNAME = 'jmordcovich@cmd.com.ar';
    const PREFIX_GCC = 'gcc';
    const PREFIX_GCZ = 'gcz';

    function __construct() {
        $this->ApiDealExternal = new ApiDealExternal();
        parent::__construct();
    }

    function serverDate() {
        $return = '';
        $ch = curl_init(self::URL_DATE);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $chResult = curl_exec($ch);
        if (!curl_errno($ch)) {
            curl_close($ch);
            $obj = json_decode($chResult);
            $date = $obj->{'date'};
            $return = strftime("%Y-%m-%dT%H:%M:%S", strtotime($date));
        }
        return $return;
    }

    function ageCluster($yearOfBirth) {
        $age = date("Y") - $yearOfBirth;
        switch ($age) {
            case ($age <= 11):
                return "Niños";
            case ($age > 11 && $age < 20):
                return "Adolescentes";
            case ($age >= 20 && $age < 26):
                return "Jóvenes";
            case ($age >= 26 && $age < 41):
                return "Adulto Temprano";
            case ($age >= 41 && $age < 65):
                return "Adulto";
            case ($age >= 65):
                return "Adulto mayor";
        }
    }

    function gender($user) {
        $gender = "unknown";
        if (!empty($user["UserProfile"]["gender"])) {
            $gender = $user['UserProfile']['gender'] == 'm' ? 'male' : 'female';
        }
        return $gender;
    }

    function yearOfBirth($user) {
        $yearOfBirth = '';
        if (!empty($user['UserProfile']['dob'])) {
            $yearOfBirth = strftime("%Y", strtotime($user['UserProfile']['dob']));
        }
        return $yearOfBirth;
    }

    function payload($prefix, $id, $gender, $yearOfBirth, $ageCluster, $hasAtLeastOneCreditCardPayment) {
        $payload = '{"id":"' . $id . '", "type":"' . $prefix . '", "profile":[{"group":"' . $prefix . '-gender","item":"' . $gender . '"}';
        if (!empty($yearOfBirth)) {
            $payload.= ',{"group":"' . $prefix . '-byear","item":"' . $yearOfBirth . '"},{"group":"' . $prefix . '-agecluster","item":"' . $ageCluster . '"},{"group":"' . $prefix . '-consumer","item":"' . $hasAtLeastOneCreditCardPayment . '"}]}';
        } else {
            $payload.= ']}';
        }
        return $payload;
    }

    function dateFormated() {
        return date("Y-m-d\TH:i:s.000O");
    }

    function hash($mail) {
        return sha1($mail . strlen($mail));
    }

    function id($prefix, $user) {
        switch ($prefix) {
            case self::PREFIX_GCC:
                return $user["User"]["id"];
            case self::PREFIX_GCZ:
                return $this->hash($user["User"]["email"]);
            default:
                return null;
        }
    }

    function request($url, $payload) {
        $username = self::USERNAME;
        $date = $this->dateFormated();
        $signature = hash_hmac("sha256", $date, self::API_KEY);
        $options = array(
            'http' => array(
                'timeout' => 3,
                'header' => "Content-Type: application/json; charset=UTF-8\r\n" . "X-cXense-Authentication: username=$username date=$date hmac-sha256-hex=$signature\r\n",
                'method' => 'POST',
                'content' => $payload
            )
        );
        Debugger::log(__METHOD__ . ' ' . $options, LOG_DEBUG);
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        Debugger::log(__METHOD__ . ' ' . $result, LOG_DEBUG);
        return json_decode($result, true);
    }

    function update($payload) {
        return $this->request(self::URL_UPDATE, $payload);
    }

    function read($payload) {
        return $this->request(self::URL_READ, $payload);
    }

    function hasAtLeastOneCreditCardPayment($user) {
        $dealExternal = $this->ApiDealExternal->findFirstCreditedCreditCardPaymentByUserId($user["User"]["id"]);
        return !empty($dealExternal) ? 'Si' : 'No';
    }

    function sendUserData($prefix, $user) {
        $id = $this->id($prefix, $user);
        $profile = $this->read(json_encode(array(
            'id' => $id,
            'type' => $prefix
        )));
        $gender = $this->gender($user);
        $yearOfBirth = $this->yearOfBirth($user);
        $ageCluster = $this->ageCluster($yearOfBirth);
        $hasAtLeastOneCreditCardPayment = $this->hasAtLeastOneCreditCardPayment($user);
        if (!empty($profile)) {
            foreach ($profile['data'][0]['profile'] as $data) {
                if ($data[0]['group'] == $prefix . '-agecluster') {
                    if (!$ageCluster) {
                        $ageCluster = $data[0]['item'];
                    }
                } elseif ($data[0]['group'] == $prefix . '-byear') {
                    if ($yearOfBirth == '') {
                        $yearOfBirth = $data[0]['item'];
                    }
                } elseif ($data[0]['group'] == $prefix . '-gender') {
                    if ($gender == "unknown") {
                        $gender = $data[0]['item'];
                    }
                } elseif ($data[0]['group'] == $prefix . '-consumer') {
                    if (!$hasAtLeastOneCreditCardPayment) {
                        $hasAtLeastOneCreditCardPayment = $data[0]['item'];
                    }
                }
            }
        }
        $payload = $this->payload($prefix, $id, $gender, $yearOfBirth, $ageCluster, $hasAtLeastOneCreditCardPayment);
        $this->update($payload);
    }

    function sendGccUserData($user) {
        $this->sendUserData(self::PREFIX_GCC, $user);
    }

    function sendGczUserData($user) {
        $this->sendUserData(self::PREFIX_GCZ, $user);
    }

}
