<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiLogger');
App::import('Component', 'AccountingEventService');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealUser');
App::import('Model', 'api.ApiRedemption');

class ApiIvrServiceComponent extends ApiBaseComponent {

    const ERROR_CODE_COMPANY_INVALID = 1;
    const ERROR_CODE_POSNET_INVALID = 2;
    const ERROR_CODE_POSNET_EXPIRED = 3;
    const ERROR_CODE_POSNET_BURNED = 4;
    const ERROR_CODE_UPDATE_FAILED = 5;
    const ERROR_CODE_UNKOWN_ERROR = 999;

    public $components = array(
        'Auth',
        'api.ApiDebug'
    );

    public function __construct() {
        parent::__construct();
        $this->ApiCompany = new ApiCompany();
        $this->ApiDeal = new ApiDeal();
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiRedemption = new ApiRedemption();
        $this->AccountingEventService = new AccountingEventServiceComponent();
    }

    public function validateForRedeem($posnetCode, $companyId) {
        $this->ApiLogger->put('posnetCode', $posnetCode);
        $this->ApiLogger->put('companyId', $companyId);
        $company = $this->ApiCompany->findById($companyId);
        if (is_null($company)) {
            $errorCode = self::ERROR_CODE_COMPANY_INVALID;
            $this->ApiLogger->notice("El id de compania no es valido. No existe una compania con ese id. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
        if (is_null($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_INVALID;
            $this->ApiLogger->notice("El codigo posnet no es valido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
        $companyIdByRedemption = $this->findCompanyIdByRedemption($redemption);
        if ($companyIdByRedemption <> $companyId) {
            $errorCode = self::ERROR_CODE_POSNET_INVALID;
            $this->ApiLogger->notice("La id de compania no coincide con el id de compania asociado al cupon. Validacion para redimir por ivr no satisfactoria.", array(
                'companyIdRight' => $companyIdByRedemption,
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
        $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
        if ($this->isCouponExpired($dealUser)) {
            $errorCode = self::ERROR_CODE_POSNET_EXPIRED;
            $this->ApiLogger->notice("El cupon a exipirado. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        if ($this->ApiRedemption->isRedeemed($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_BURNED;
            $this->ApiLogger->notice("El cupon ya fue redimido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        return array(
            'isValid' => true
        );
    }

    public function validateAndRedeem($posnetCode, $companyId) {
        try {
            $this->ApiLogger->put('posnetCode', $posnetCode);
            $validForRedeemResult = $this->validateForRedeem($posnetCode, $companyId);
            if (!$validForRedeemResult['isValid']) {
                $this->ApiLogger->notice("Validacion para redimir por ivr no satisfactoria. No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $validForRedeemResult['errorCode']
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $validForRedeemResult['errorCode']
                );
            }
            $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
            $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
            $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
            $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
            $updateRedemption = $this->ApiRedemption->updateAsRedeemedByIvr($redemption, $dealUser['DealUser']['discount_amount']);
            if (!$updateRedemption) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado la redencion (Redemption). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            $updateDealUser = $this->ApiDealUser->updateAsUsed($dealUser);
            if (!$updateDealUser) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado el cupon (DealUser). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            try {
                $dealForEvent = array(
                    'Deal' => array(
                        'id' => $dealUser['DealUser']['deal_id'],
                        'company_id' => $companyId
                    )
                );
                $this->AccountingEventService->registerAccountingEvent($dealForEvent);
            } catch (Exception $e) {
                $this->ApiLogger->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                    'errorCode' => $e->getMessage()
                ));
            }
            return array(
                'redeemed' => true
            );
        } catch (Exception $e) {
            $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
            $this->ApiLogger->error("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage()
            ));
            $this->ApiLogger->notice("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage(),
                'errorCode' => $errorCode
            ));
            return array(
                'redeemed' => false,
                'errorCode' => $errorCode
            );
        }
    }

    private function findCompanyIdByRedemption($redemption) {
        $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
        $deal = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
        return $deal['Deal']['company_id'];
    }

    public function isCouponExpired($dealUser) {
        App::import('Component', 'api.ApiDealService');
        $apiDealService = & new ApiDealServiceComponent();        
        $now = date('Y-m-d H:i:s');
        $deal = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
        if (is_null($deal)) {
            $this->ApiLogger->debug('No se encontro una oferta asociada al codigo posnet.');
            return false;
        }
        $this->ApiLogger->put('dealId', $deal['Deal']['id']);
        $couponExpiryDate = $apiDealService->couponExpirationDate($deal, $dealUser);
        $isExpired = strtotime(date("Y-m-d 00:00:00", strtotime($couponExpiryDate))) < strtotime(date("Y-m-d 00:00:00", strtotime($now)));
        if ($isExpired) {
            $this->ApiLogger->debug('Evaluando la vigencia del cupon. El cupon esta Expirado.', array(
                'couponExpiryDate' => $couponExpiryDate,
                'nowDate' => $now
            ));
        }
        return $isExpired;
    }

}
