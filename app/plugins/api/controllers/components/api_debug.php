<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
class ApiDebugComponent extends ApiBaseComponent {

  const ENABLE_API_DEBUG = 1;
  const SHOW_ERROR = 1;

  private $debug = array();
  private $enable = false;

  public function initialize(&$controller) {
    if($this->isRequestedWithDebug($controller)) {
      $this->enable = true;
    }
    if($this->isRequestedWithShowError($controller)) {
      Configure::write('debug', 1);
    }
  }

  public function beforeRender(&$controller) {
    $controller->set('debug', $this->debug);
  }

  public function debug($data, $key=null) {
    if($this->enable) {
      if(empty($key)) {
        $this->debug[] = $data;
      } else {
        $this->debug[$key] = $data;
      }
    }
    Debugger::log($key . $data);
  }

  private function isRequestedWithDebug($controller) {
    if(array_key_exists('enable_debug', $controller->params['url'])){
    return $controller->params['url']['enable_debug'] == self::ENABLE_API_DEBUG;
    }
  }

  private function isRequestedWithShowError($controller) {
    if(array_key_exists('show_error', $controller->params['url'])){
    return $controller->params['url'] == self::SHOW_ERROR;
    }
  }

}
