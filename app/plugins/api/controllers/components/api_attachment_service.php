<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiAttachment');

class ApiAttachmentServiceComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct();
        $this->ApiAttachment = new ApiAttachment();
    }

    static function instance() {
        if (self::$instance == null) {
            self::$instance = new AttachmentServiceComponent();
        }
        return self::$instance;
    }

    public function replicate($originalAttachment, $replicaClass, $replicaForeignId) {
        $replicaAttachment = $this->ApiAttachment->newAttachment();
        $this->copyReplicableFields($originalAttachment, $replicaAttachment);
        $replicaAttachment['Attachment']['class'] = $replicaClass;
        $replicaAttachment['Attachment']['foreign_id'] = $replicaForeignId;
        $replicaAttachment['Attachment']['filename'] = $originalAttachment['Attachment']['filename'];
        $replicaAttachment['Attachment']['dir'] = $this->makeDirName($replicaClass, $replicaForeignId);
        if (!$this->ApiAttachment->validates()) {
            $this->ApiLogger->error("Error al intentar Replicar el Attachment, el nuevo registro attachment no es valido", array(
                'originalAttachment' => $originalAttachment,
                'replicateAttachment' => $replicaAttachment
            ));
            return false;
        }
        $saveOk = $this->ApiAttachment->save($replicaAttachment['Attachment']);
        $replicaAttachment['Attachment']['id'] = $this->ApiAttachment->getLastInsertId();
        if (!$saveOk) {
            $this->ApiLogger->error("Error al intentar Replicar el Attachment, no se pudo guardar  el registro en la base de datos", array(
                'originalAttachment' => $originalAttachment,
                'replicateAttachment' => $replicaAttachment
            ));
            return false;
        }
        $copyFileOk = $this->createDirAndCopyFile($originalAttachment['Attachment']['dir'], $originalAttachment['Attachment']['filename'], $replicaAttachment['Attachment']['dir'], $replicaAttachment['Attachment']['filename']);
        if (!$copyFileOk) {
            $this->ApiLogger->error("Error al intentar Replicar el Attachment, no se pudo copiar el archivo fisico", array(
                'originalAttachment' => $originalAttachment,
                'replicateAttachment' => $replicaAttachment
            ));
        }
        return $replicaAttachment;
    }

    private function makeDirName($class, $foreignId) {
        return $class . DIRECTORY_SEPARATOR . $foreignId;
    }

    private function createDirAndCopyFile($fromDirName, $fromFilename, $toDirName, $toFilename) {
        $toDirFullPath = APP . 'media' . DIRECTORY_SEPARATOR . $toDirName;
        $toFileFullPath = APP . 'media' . DIRECTORY_SEPARATOR . $toDirName . DIRECTORY_SEPARATOR . $toFilename;
        $fromFileFullPath = APP . 'media' . DIRECTORY_SEPARATOR . $fromDirName . DIRECTORY_SEPARATOR . $fromFilename;
        if (!is_dir($toDirFullPath)) {
            $this->ApiLogger->info("El Directorio no existe", array(
                'toDir' => $toDirFullPath
            ));
            $makeDirOk = mkdir($toDirFullPath);
            if (!$makeDirOk) {
                $this->ApiLogger->error("No se pudo crear el directorio", array(
                    'toDir' => $toDirFullPath,
                    'result' => $makeDirOk
                ));
                return false;
            }
        }
        $copyOk = copy($fromFileFullPath, $toFileFullPath);
        if (!$copyOk) {
            $this->ApiLogger->error("No se pudo copiar el archivo", array(
                'fromFile' => $fromFileFullPath,
                'toFile' => $toFileFullPath,
                'result' => $copyOk
            ));
            return false;
        }
        return true;
    }

    private function copyReplicableFields($from, &$to) {
        $replicateFields = array(
            'class',
            'mimetype',
            'filesize',
            'height',
            'width',
            'thumb',
            'description'
        );
        foreach ($replicateFields as $keyField) {
            $to['Attachment'][$keyField] = $from['Attachment'][$keyField];
        }
        return $to;
    }

}
