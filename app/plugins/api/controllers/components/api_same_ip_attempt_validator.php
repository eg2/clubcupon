<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiSubscription');

class ApiSameIpAttemptValidatorComponent extends ApiBaseComponent {

    public function __construct($attempts, $minutes, $whiteList) {
        parent::__construct();
        $this->attempts = $attempts;
        $this->minutes = $minutes;
        $this->whiteList = $whiteList;
        $this->ApiSubscription = new ApiSubscription();
    }

    public function validate($ip) {
        $valid = true;
        if (!in_array($ip, $this->whiteList)) {
            $count = $this->findCountByIp($ip);
            $valid = $count <= $this->attempts;
        }
        return $valid;
    }

    private function extraConditionsSql() {
        return DboSource::expression("Subscription.created > NOW() - INTERVAL {$this->minutes} MINUTE");
    }

    private function findCountByIp($ip) {
        $fields = array(
            'COUNT(1) as quantity'
        );
        $conditions = array(
            'Subscription.subscription_ip' => $ip,
            $this->extraConditionsSql()
        );
        $return = $this->ApiSubscription->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));
        return $return[0]['quantity'];
    }

}
