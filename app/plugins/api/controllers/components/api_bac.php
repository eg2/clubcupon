<?php

App::import('Component', 'api.ApiBase');
App::import('Core', 'HttpSocket');

class ApiBacComponent extends ApiBaseComponent {

    public $name = 'ApiBac';
    public $components = array(
        'api.ApiDebug'
    );
    private $createOrUpdateUserUrl = null;
    private $checkPaymentUrl = null;
    private $liquidateCompanyUrl = null;
    private $initializePayment = null;
    private $obtenerPinUrl = null;
    private $consumirPinUrl = null;
    private $crearCargosUrl = null;
    private $createPaymentUrl = null;

    public function __construct() {
        parent::__construct();
        $this->createOrUpdateUserUrl = Configure::read('BAC.wsdl.altaModificacionClienteEmpresa');
        $this->checkPaymentUrl = Configure::read('BAC.wsdl.consultarPago');
        $this->liquidateCompanyUrl = Configure::read('BAC.wsdl.liquidarEmpresa');
        $this->initializePayment = Configure::read('BAC.wsdl.consultarPago');
        $this->obtenerPinUrl = Configure::read('BAC.wsdl.obtenerPin');
        $this->consumirPinUrl = Configure::read('BAC.wsdl.consumirPin');
        $this->crearCargosUrl = Configure::read('BAC.wsdl.crearCargos');
        $this->createPaymentUrl = Configure::read('BAC.wsdl.crearPagos');
    }

    private function newSoapClient($url) {
        $soapClient = new SoapClient($url);
        if (!isset($soapClient)) {
            Debugger::log(__CLASS__ . '::' . __METHOD__ . ' cannot connect to ' . $url, LOG_DEBUG);
        }
        return $soapClient;
    }

    private function invokeSoapClientMethod($url, $method, $parameters) {
        $soapClient = $this->newSoapClient($url);
        try {
            $result = $soapClient->$method($parameters);
        } catch (Exception $e) {
            $this->ApiLogger->error('error en la llamada SOAP', array(
                'exception' => $e->getMessage(),
                'params' => array(
                    $url,
                    $method,
                    $parameters
                )
            ));
            throw $e;
        }
        return $result;
    }

    public function createOrUpdateUser($parameters) {
        $this->ApiLogger->putAll($parameters);
        $this->ApiLogger->debug('creando o actualizando usuario bac', array(
            'parametros' => $parameters,
            'uri' => $this->createOrUpdateUserUrl
        ));
        try {
            $result = $this->invokeSoapClientMethod($this->createOrUpdateUserUrl, 'altaModificacionClienteEmpresa', $parameters);
        } catch (Exception $e) {
            $this->ApiLogger->error('Error: No se Actualizó/creó el usuario');
            throw $e;
        }
        $this->ApiLogger->debug('creacion o actualizacion de usuario bac realizada', array(
            'result' => $result
        ));
        return $result;
    }

    public function checkPayment($parameters) {
        return $this->invokeSoapClientMethod($this->checkPaymentUrl, 'consultarPago', $parameters);
    }

    public function liquidateCompany($parameters) {
        return $this->invokeSoapClientMethod($this->liquidateCompanyUrl, 'consultarPago', $parameters);
    }

    public function canGetWSDL($wsdl) {
        return file_get_contents($wsdl);
    }

    public function initializePaymente($parameters) {
        return $this->invokeSoapClientMethod($this->initializePayment, 'iniciarPagoSeguro', $parameters);
    }

    public function obtenerPin($parameters) {
        return $this->invokeSoapClientMethod($this->obtenerPinUrl, 'obtenerPin', $parameters);
    }

    public function consumirPin($parameters) {
        return $this->invokeSoapClientMethod($this->consumirPinUrl, 'consumirPin', $parameters);
    }

    public function crearCargos($params) {
        $this->ApiLogger->putAll($params);
        $this->ApiLogger->debug('creando cargos en bac', array(
            'parametros' => $params,
            'uri' => $this->crearCargosUrl
        ));
        $result = $this->invokeSoapClientMethod($this->crearCargosUrl, 'crearCargos', $params);
        $this->ApiLogger->debug('creacion de cargos en bac realizada.', array(
            'result' => $result
        ));
        return $result;
    }

    public function createPayment($params) {
        $this->ApiLogger->putAll($params);
        $this->ApiLogger->debug('creando pago en bac', array(
            'parametros' => $params,
            'uri' => $this->createPaymentUrl
        ));
        $result = $this->invokeSoapClientMethod($this->createPaymentUrl, 'crearPagos', $params);
        $this->ApiLogger->debug('creacion de pago en bac realizada.', array(
            'result' => $result
        ));
        return $result;
    }

}
