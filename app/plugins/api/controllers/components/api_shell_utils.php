<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class ApiShellUtilsComponent extends ApiBaseComponent {

  public function getOptions($args) {
    $params = array();
    foreach ($args as $arg) {
      if(substr($arg, 0, 2) != "+D") {
        $argByKey = split(":", $arg);
        $params[$argByKey[0]] = $this->remplaceEqualToColon($argByKey[1]);
      }
      if(substr($arg, 0, 3) == "--h") {
        $params['h'] = 1;
      }
    }
    return $params;
  }

  public function remplaceEqualToColon($argByKey) {
    return str_replace("=", ":", $argByKey);
  }

  public function overrideConfigurationsByParams($args, $write=true) {
    $overrideConfigurations = array();
    foreach ($args as $arg) {
      if(substr($arg, 0, 2) == "+D") {
        $argByKey = split(":", substr($arg, 2));
        $overrideConfigurations[$argByKey[0]] = $argByKey[1];
      }
    }

    if ($write) {
      foreach ($overrideConfigurations as $key => $value) {
        if(strpos($value, ',')) {
          $value = split(',', $value);
        }
        Configure::write($key, $value);
      }
    }

    return $overrideConfigurations;
  }

  public function loadConfigurationsValues($keys) {
    $values = array();
    foreach($keys as $key) {
      $values[$key] = Configure::read($key);
    }
    return $values;
  }

}
