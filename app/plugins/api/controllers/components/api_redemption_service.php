<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
//App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiDealService');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealUser');
App::import('Model', 'api.ApiRedemption');
App::import('Model', 'api.ApiPin');
App::import('Model', 'api.ApiDealExternal');

class ApiRedemptionServiceComponent extends ApiBaseComponent {

    const ERROR_CODE_COMPANY_INVALID = 1;
    const ERROR_CODE_POSNET_INVALID = 2;
    const ERROR_CODE_POSNET_EXPIRED = 3;
    const ERROR_CODE_POSNET_BURNED = 4;
    const ERROR_CODE_UPDATE_FAILED = 5;
    const ERROR_DEAL_EXTERNAL_NOT_CREDITED = 6;
    const ERROR_DEAL_EXTERNAL_NOT_FOUND = 7;
    const ERROR_DEAL_USERS_NOT_FOUND = 8;
    const ERROR_CODE_UNKOWN_ERROR = 999;

    public $components = array(
        'Auth',
        'api.ApiDebug'
    );

    public function __construct() {
        parent::__construct();
        $this->ApiCompany = new ApiCompany();
        $this->ApiDeal = new ApiDeal();
        $this->ApiDealUser = new ApiDealUser();
        $this->ApiRedemption = new ApiRedemption();
        $this->ApiPin = new ApiPin();
        $this->ApiDealExternal = new ApiDealExternal();
        $this->ApiDealService = new ApiDealServiceComponent();
    }

    public function validateForRedeem($posnetCode, $companyId) {
        $this->ApiLogger->put('posnetCode', $posnetCode);
        $this->ApiLogger->put('companyId', $companyId);
        $company = $this->ApiCompany->findById($companyId);
        if (is_null($company)) {
            $errorCode = self::ERROR_CODE_COMPANY_INVALID;
            $this->ApiLogger->notice("El id de compania no es valido. No existe una compania con ese id. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
        if (is_null($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_INVALID;
            $this->ApiLogger->notice("El codigo posnet no es valido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
        $companyIdByRedemption = $this->findCompanyIdByRedemption($redemption);
        if ($companyIdByRedemption <> $companyId) {
            $errorCode = self::ERROR_CODE_POSNET_INVALID;
            $this->ApiLogger->notice("La id de compania no coincide con el id de compania asociado al cupon. Validacion para redimir por ivr no satisfactoria.", array(
                'companyIdRight' => $companyIdByRedemption,
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
        $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
        if ($this->isCouponExpired($dealUser)) {
            $errorCode = self::ERROR_CODE_POSNET_EXPIRED;
            $this->ApiLogger->notice("El cupon ha expirado. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        if ($this->ApiRedemption->isRedeemed($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_BURNED;
            $this->ApiLogger->notice("El cupon ya fue redimido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        return array(
            'isValid' => true
        );
    }

    public function validateForRedeemWithoutCompany($posnetCode) {
        $this->ApiLogger->put('posnetCode', $posnetCode);
        $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
        if (is_null($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_INVALID;
            $this->ApiLogger->notice("El codigo posnet no es valido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
        $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
        $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
        if ($this->isCouponExpired($dealUser)) {
            $errorCode = self::ERROR_CODE_POSNET_EXPIRED;
            $this->ApiLogger->notice("El cupon ha expirado. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        if ($this->ApiRedemption->isRedeemed($redemption)) {
            $errorCode = self::ERROR_CODE_POSNET_BURNED;
            $this->ApiLogger->notice("El cupon ya fue redimido. Validacion para redimir por ivr no satisfactoria.", array(
                'errorCode' => $errorCode
            ));
            return array(
                'isValid' => false,
                'errorCode' => $errorCode
            );
        }
        return array(
            'isValid' => true
        );
    }

    public function validateAndRedeemForPosnetWeb($posnetCode, $companyId) {
        return $this->validateForRedeem($posnetCode, $companyId, ApiRedemption::WAY_POSNET_WEB);
    }

    public function validateAndRedeemForIvr($posnetCode, $companyId) {
        return $this->validateForRedeem($posnetCode, $companyId, ApiRedemption::WAY_IVR);
    }

    public function validateAndRedeemForBac($pincode) {
        $dealUser = $this->ApiDealUser->findFirstByPinCode($pincode);
        $redemption = $this->ApiRedemption->findByDealUserId($dealUser['DealUser']['id']);
        return $this->validateAndRedeem($redemption['Redemption']['posnet_code'], $dealUser['Deal']['company_id'], ApiRedemption::BAC_SERVICE);
    }

    public function reverseRedeem($posnetCode) {
        $redemption = $this->ApiRedemption->findFirstByPosnetCode($posnetCode);
        if (!empty($redemption)) {
            $success = $this->ApiRedemption->updateAsNotRedeemed($redemption);
            if ($success) {
                $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
                if (!empty($dealUser)) {
                    $this->ApiDealUser->updateAsNotUsed($dealUser);
                }
            }
        }
    }

    public function validateAndRedeemWithoutCompany($posnetCode, $way) {
        try {
            $this->ApiLogger->put('posnetCode', $posnetCode);
            $validForRedeemResult = $this->validateForRedeemWithoutCompany($posnetCode);
            if (!$validForRedeemResult['isValid']) {
                $this->ApiLogger->notice("Validacion para redimir por ivr no satisfactoria. No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $validForRedeemResult['errorCode']
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $validForRedeemResult['errorCode']
                );
            }
            $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
            $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
            $this->ApiLogger->debug("(DealUser)", $dealUser);
            $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
            $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
            $updateRedemption = $this->ApiRedemption->updateAsRedeemed($redemption, $dealUser['DealUser']['discount_amount'], $way);
            if (!$updateRedemption) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado la redencion (Redemption). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            $updateDealUser = $this->ApiDealUser->updateAsUsed($dealUser);
            if (!$updateDealUser) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado el cupon (DealUser). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            try {
                $dealForEvent = array(
                    'Deal' => array(
                        'id' => $dealUser['DealUser']['deal_id'],
                        'company_id' => $companyId
                    )
                );
            } catch (Exception $e) {
                $this->ApiLogger->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                    'errorCode' => $e->getMessage()
                ));
            }
            return array(
                'redeemed' => true
            );
        } catch (Exception $e) {
            $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
            $this->ApiLogger->error("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage()
            ));
            $this->ApiLogger->notice("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage(),
                'errorCode' => $errorCode
            ));
            return array(
                'redeemed' => false,
                'errorCode' => $errorCode
            );
        }
    }

    public function validateAndRedeem($posnetCode, $companyId, $way) {
        try {
            $this->ApiLogger->put('posnetCode', $posnetCode);
            $validForRedeemResult = $this->validateForRedeem($posnetCode, $companyId);
            if (!$validForRedeemResult['isValid']) {
                $this->ApiLogger->notice("Validacion para redimir por ivr no satisfactoria. No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $validForRedeemResult['errorCode']
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $validForRedeemResult['errorCode']
                );
            }
            $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
            $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
            $this->ApiLogger->put('redemptionId', $redemption['Redemption']['id']);
            $this->ApiLogger->put('dealUserId', $dealUser['DealUser']['id']);
            $updateRedemption = $this->ApiRedemption->updateAsRedeemed($redemption, $dealUser['DealUser']['discount_amount'], $way);
            if (!$updateRedemption) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado la redencion (Redemption). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            $updateDealUser = $this->ApiDealUser->updateAsUsed($dealUser);
            if (!$updateDealUser) {
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("No se a actualizado el cupon (DealUser). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                return array(
                    'redeemed' => false,
                    'errorCode' => $errorCode
                );
            }
            try {
                $dealForEvent = array(
                    'Deal' => array(
                        'id' => $dealUser['DealUser']['deal_id'],
                        'company_id' => $companyId
                    )
                );
            } catch (Exception $e) {
                $this->ApiLogger->error("Hubo una falla al reportar el evento contable de cambio de estado de los cupones", array(
                    'errorCode' => $e->getMessage()
                ));
            }
            return array(
                'redeemed' => true
            );
        } catch (Exception $e) {
            $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
            $this->ApiLogger->error("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage()
            ));
            $this->ApiLogger->notice("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage(),
                'errorCode' => $errorCode
            ));
            return array(
                'redeemed' => false,
                'errorCode' => $errorCode
            );
        }
    }

    private function findCompanyIdByRedemption($redemption) {
        $dealUser = $this->ApiDealUser->findById($redemption['Redemption']['deal_user_id']);
        $deal = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
        return $deal['Deal']['company_id'];
    }

    public function isCouponExpired($dealUser) {
        $now = date('Y-m-d H:i:s');
        $deal = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
        $couponExpiryDate = $this->ApiDealService->couponExpirationDate($deal, $dealUser);
        if (is_null($deal)) {
            $this->ApiLogger->debug('No se encontro una oferta asociada al codigo posnet.');
            return false;
        }
        $extendedHours = $this->ApiDeal->getExtendedHoursToRedeem($deal);
        if ($this->ApiDeal->isNow($deal) || $this->ApiDeal->isImagena($deal)) {
            $couponExpiryDate = date("Y-m-d H:i:s", strtotime($deal['Deal']['coupon_expiry_date'] . " + $extendedHours hours"));
        }
        $this->ApiLogger->put('couponExpiryDate', $couponExpiryDate);
        $this->ApiLogger->put('dealId', $deal['Deal']['id']);
        $this->ApiLogger->put('extendedHours', $extendedHours);
        $isExpired = strtotime($couponExpiryDate) < strtotime($now);
        if ($isExpired) {
            $this->ApiLogger->debug('Evaluando la vigencia del cupon. El cupon esta Expirado.', array(
                'coupon_expiry_date' => $deal['Deal']['coupon_expiry_date'],
                'nowDate' => $now,
                'couponExpiryDate' => $couponExpiryDate
            ));
        }
        return $isExpired;
    }

    public function getSimulateErrorsByPosnetCode($posnetCode) {
        switch ($posnetCode) {
            case 1:
                $errorCode = self::ERROR_CODE_POSNET_INVALID;
                $this->ApiLogger->notice("SIMULANDO ERROR: El codigo posnet no es valido. Validacion para redimir por ivr no satisfactoria.", array(
                    'errorCode' => $errorCode
                ));
                $result = array(
                    'status' => false,
                    'errorCode' => $errorCode
                );
                break;

            case 2:
                $errorCode = self::ERROR_CODE_POSNET_EXPIRED;
                $this->ApiLogger->notice("SIMULANDO ERROR: El cupon a exipirado. Validacion para redimir por ivr no satisfactoria.", array(
                    'errorCode' => $errorCode
                ));
                $result = array(
                    'status' => false,
                    'errorCode' => $errorCode
                );
                break;

            case 3:
                $errorCode = self::ERROR_CODE_POSNET_BURNED;
                $this->ApiLogger->notice("SIMULANDO ERROR: El cupon ya fue redimido. Validacion para redimir por ivr no satisfactoria.", array(
                    'errorCode' => $errorCode
                ));
                $result = array(
                    'status' => false,
                    'errorCode' => $errorCode
                );
                break;

            case 4:
                $errorCode = self::ERROR_CODE_UPDATE_FAILED;
                $this->ApiLogger->notice("SIMULANDO ERROR: No se a actualizado la redencion (Redemption). No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                $result = array(
                    'status' => false,
                    'errorCode' => $errorCode
                );
                break;

            case 5:
                $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
                $this->ApiLogger->notice("SIMULNADO ERROR: Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                    'errorCode' => $errorCode
                ));
                $result = array(
                    'status' => false,
                    'errorCode' => $errorCode
                );
                break;
        }
        return $result;
    }

    public function validateAndRedeemByPosnetCode($posnetCode) {
        try {
            $this->ApiLogger->debug("validateAndRedeemByPosnetCode:", array(
                'posnetCode' => $posnetCode
            ));
            $dealUserToRedeem = $this->ApiDealUser->findFirstByPosnetCode($posnetCode);
            if (!empty($dealUserToRedeem)) {
                $redeemedResult = $this->validateAndRedeemWithoutCompany($posnetCode, ApiRedemption::WAY_BAC);
                $dealUsers = $this->ApiDealUser->findAllByDealExternalId($dealUserToRedeem['DealExternal']['id']);
                $is_totally_redeemed = true;
                foreach ($dealUsers as $dealUser) {
                    if (is_null($dealUser['Redemption']['redeemed'])) {
                        $is_totally_redeemed = false;
                    }
                }
                $redeemedResult['is_totally_redeemed'] = $is_totally_redeemed;
                $redeemedResult['bac_id'] = $dealUserToRedeem['DealExternal']['bac_id'];
                return $redeemedResult;
            } else {
                return array(
                    'redeemed' => false,
                    'errorCode' => self::ERROR_CODE_POSNET_INVALID
                );
            }
        } catch (Exception $e) {
            $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
            $this->ApiLogger->error("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage()
            ));
            $this->ApiLogger->notice("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage(),
                'errorCode' => $errorCode
            ));
            return array(
                'redeemed' => false,
                'errorCode' => $errorCode
            );
        }
    }

    public function validateAndRedeemByDealExternalId($dealExternalId) {
        try {
            $dealExternal = $this->ApiDealExternal->findById($dealExternalId);
            if (!empty($dealExternal)) {
                if ($dealExternal['DealExternal']['external_status'] == 'A') {
                    $dealUsers = $this->ApiDealUser->findAllByDealExternalId($dealExternalId);
                    if (!empty($dealUsers)) {
                        $results = array();
                        foreach ($dealUsers as $dealUser) {
                            $posnetCode = $dealUser['Redemption']['posnet_code'];
                            $redeemedResult = $this->validateAndRedeemWithoutCompany($posnetCode, ApiRedemption::WAY_BAC);
                            $results[] = $redeemedResult;
                        }
                        return $results;
                    } else {
                        return array(
                            'redeemed' => false,
                            'errorCode' => self::ERROR_DEAL_USERS_NOT_FOUND
                        );
                    }
                } else {
                    return array(
                        'redeemed' => false,
                        'errorCode' => self::ERROR_DEAL_EXTERNAL_NOT_CREDITED
                    );
                }
            } else {
                return array(
                    'redeemed' => false,
                    'errorCode' => self::ERROR_DEAL_EXTERNAL_NOT_FOUND
                );
            }
        } catch (Exception $e) {
            $errorCode = self::ERROR_CODE_UNKOWN_ERROR;
            $this->ApiLogger->error("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage()
            ));
            $this->ApiLogger->notice("Error no clasificado al intentar redimir. No se puede seguir con la redencion por ivr.", array(
                'exception' => $e->getMessage(),
                'errorCode' => $errorCode
            ));
            return array(
                'redeemed' => false,
                'errorCode' => $errorCode
            );
        }
    }

}
