<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiPaymentOption');
App::import('Model', 'api.ApiDealExternal');
App::import('Model', 'api.ApiPaymentType');
App::import('Model', 'shipping.ShippingAddressUser');
App::import('Model', 'product.ProductInventoryStrategy');
App::import('Component', 'product.ProductInventoryService');

class ApiBuyServiceComponent extends ApiBaseComponent {

    public $name = 'ApiBuyService';
    public $components = array(
        'api.ApiDebug',
        'api.ApiCouponService',
        'api.ApiPaymentService',
        'api.ApiEmailService',
        'api.ApiBacService',
        'api.ApiGiftPinService'
    );

    public function __construct() {
        $this->ApiUser = & new ApiUser();
        $this->ApiDeal = & new ApiDeal();
        $this->ApiPaymentOption = & new ApiPaymentOption();
        $this->ApiDealExternal = & new ApiDealExternal();
        $this->ApiPaymentType = & new ApiPaymentType();
        $this->ProductInventoryService = & new ProductInventoryServiceComponent();
        $this->ShippingAddressUser = & new ShippingAddressUser();
        parent::__construct();
    }

    private function hasExceededMaxQuantityForUser($deal, $user, $quantity) {
        return $this->ApiCouponService->hasExceededMaxQuantityForUser($deal, $user, $quantity);
    }

    private function hasStock($deal) {
        $availableQuantity = $this->ProductInventoryService->availableQuantity($deal['Deal']['id']);
        return ($availableQuantity > 0);
    }

    public function buy($userId, $dealId, $quantity, $paymentOptionId, $paymentPlanItemId = null, $isCombined = 0, $buy_channel = 0, $giftOptions = null, $discountPin = null, $shippingAddressId = null, $request = null, $shippingAddressUser = null) {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $user = $this->ApiUser->findById($userId);
        $deal = $this->ApiDeal->findForApiById($dealId);
        $this->ApiLogger->notice('Metodo de Pago (paymentOptionId):', $paymentOptionId);
        $this->ApiLogger->notice('Es una Compra en cuotas (paymentPlanItemId):', $paymentPlanItemId);
        $this->ApiLogger->notice('Es pago combinado', array(
            'isCombined' => $isCombined
        ));
        $this->ApiLogger->notice('Es compra con cuenta Clubcupon', $this->isBuyWithWalletByPaymentOptionId($paymentOptionId));
        if (empty($giftOptions['email'])) {
            $giftOptions['email'] = $user['User']['email'];
        }
        if ($this->hasDealShippingAddresses($shippingAddressId) && !$this->hasSelectedShippingAddress($shippingAddressId)) {
            throw new DomainException('Debe seleccionar un Punto de Retiro.');
        }
        if (!$this->isPurchasableByUser($deal, $user, $quantity)) {
            throw new DomainException('Se alcanzo la cantidad maxima de cupones por oferta.');
        }
        if ($this->hasExceededMaxQuantityForDeal($deal, $quantity) || $this->hasExceededMaxQuantityForUser($deal, $user, $quantity) || $this->ApiDeal->hasExpired($deal) || !$this->hasStock($deal)) {
            throw new DomainException("Alcanzaste la cantidad maxima de cupones por oferta que puedes comprar.");
        }
        if ($this->isQuantityLessThanMinPerUSer($deal, $quantity)) {
            throw new DomainException("La cantidad ingresada es menor a la minima permitida.");
        }
        if ($this->hasExceededAvailableQuantityOfPins($deal, $quantity)) {
            throw new DomainException('La cantidad ingresada excede los cupones disponibles para esta oferta.');
        }
        if ($this->canBuyWithOnlyGiftPin($discountPin, $deal, $quantity)) {
            $dealExternal = $this->buyDealOnlyWithGiftPin($user, $deal, $quantity, $paymentOptionId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        } elseif ($this->isBuyWithWalletByPaymentOptionId($paymentOptionId)) {
            $dealExternal = $this->buyDealForWallet($user, $deal, $quantity, $paymentOptionId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        } elseif ($isCombined) {
            if ($this->hasEnoughMoney($user, $deal, $quantity)) {
                $dealExternal = $this->buyDealForWallet($user, $deal, $quantity, $paymentOptionId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
            } else {
                $dealExternal = $this->buyDealForCombined($user, $deal, $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
            }
        } else {
            $dealExternal = $this->buyDealForBac($user, $deal, $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        }
        $paymentType = $this->ApiPaymentType->findById($dealExternal['DealExternal']['payment_type_id']);
        $paymentOption = $this->ApiPaymentOption->findById($paymentOptionId);
        $this->ApiEmailService->sendEmailForBuyDeal($user, $deal, $paymentType, $paymentOption, $dealExternal, $quantity);
        if ($this->ApiDeal->isShippingAddressUser($deal)) {
            $this->registerShippingAddressUser($dealExternal, $shippingAddressUser);
        }
        return $dealExternal;
    }

    private function registerShippingAddressUser($dealExternal, $shippingAddressUser) {
        $shippingAddressUser['ShippingAddressUser']['deal_external_id'] = $dealExternal['DealExternal']['id'];
        $shippingAddressUser['ShippingAddressUser']['user_id'] = $dealExternal['DealExternal']['user_id'];
        if (!$this->ShippingAddressUser->save($shippingAddressUser)) {
            $this->ApiLogger->error('Imposible guardar:', $shippingAddressUser);
        }
    }

    protected function isBuyWithWalletByPaymentOptionId($paymentOptionId) {
        $paymentType = $this->ApiPaymentOption->findById($paymentOptionId);
        $paymentTypeId = $paymentType['PaymentOption']['payment_type_id'];
        return $this->isBuyWithWallet($paymentTypeId);
    }

    public function hasEnoughCreditGiftPinCode($giftPinCode, $deal, $quantity) {
        $result = false;
        if (!empty($giftPinCode)) {
            $bacPin = $this->ApiGiftPinService->isUsable($giftPinCode);
            $total_amount = $deal['Deal']['discounted_price'] * $quantity;
            if (empty($bacPin)) {
                $bacPin['monto'] = 0;
            }
            $result = ($bacPin['monto'] >= $total_amount);
        }
        return $result;
    }

    public function canBuyWithOnlyGiftPin($giftPinCode, $deal, $quantity) {
        return $this->hasEnoughCreditGiftPinCode($giftPinCode, $deal, $quantity);
    }

    protected function buyDealOnlyWithGiftPin($user, $deal, $quantity, $paymentOptionId, $buy_channel = null, $giftOptions = null, $discountPin = null, $shippingAddressId = null, $request = null) {
        $paymentPlanItemId = null;
        $internalAmount = 0;
        $parcialAmount = 0;
        $external_status = 'A';
        $paymentOptionId = $this->ApiPaymentOption->find('first', array(
            'order' => 'PaymentOption.id DESC',
            'conditions' => array(
                'payment_type_id' => ApiPaymentType::ID_WALLET
            )
        ));
        $paymentOptionId = $paymentOptionId['PaymentOption']['id'];
        $dealExternal = $this->ApiPaymentService->create($user['User']['id'], $deal['Deal']['id'], $quantity, $paymentOptionId, $paymentPlanItemId, $internalAmount, $parcialAmount, $external_status, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        $paymentTypeId = ApiPaymentType::ID_WALLET;
        $this->ApiCouponService->generateCoupons($user, $deal, $quantity, $paymentTypeId, $dealExternal);
        return $dealExternal;
    }

    protected function buyDealForWallet($user, $deal, $quantity, $paymentOptionId, $buy_channel = null, $giftOptions = null, $discountPin = null, $shippingAddressId = null, $request = null) {
        $paymentType = $this->ApiPaymentOption->findById($paymentOptionId);
        $paymentTypeId = $paymentType['PaymentOption']['payment_type_id'];
        if (!empty($discountPin)) {
            $pin = $this->ApiGiftPinService->isUsable($discountPin);
        } else {
            $pin = null;
        }
        $hasEnoughMoney = $this->isCreditEnough($user['User']['id'], $deal['Deal']['discounted_price'], $quantity, $paymentOptionId, $pin);
        if (!$hasEnoughMoney && !$user['User']['wallet_blocked']) {
            throw new DomainException('No tienes suficiente dinero o Cuenta ClubCupon Bloqueada');
        }
        $this->ApiUser->blockWallet($user['User']['id']);
        $internalAmount = Precision::mul($quantity, $deal['Deal']['discounted_price']);
        $internalAmount = $internalAmount - $pin['monto'];
        $parcialAmount = 0;
        $external_status = 'A';
        $paymentPlanItemId = null;
        $dealExternal = $this->ApiPaymentService->create($user['User']['id'], $deal['Deal']['id'], $quantity, $paymentOptionId, $paymentPlanItemId, $internalAmount, $parcialAmount, $external_status, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        $dealExternalId = $dealExternal['DealExternal']['id'];
        $this->ApiCouponService->generateCoupons($user, $deal, $quantity, $paymentTypeId, $dealExternal);
        $this->ApiUser->unBlockWallet($user['User']['id']);
        $this->ProductInventoryService->decrease($deal['Deal']['id'], $quantity, $user['User']['id']);
        return $dealExternal;
    }

    protected function buyDealForCombined($user, $deal, $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel = null, $giftOptions = null, $discountPin = null, $shippingAddressId = null, $request = null) {
        if ($user['User']['bac_user_id'] == 0) {
            $bacUserId = $this->ApiBacService->createBacUserByDeal($user, $deal);
        } else {
            $bacUserId = $user['User']['bac_user_id'];
        }
        if (empty($bacUserId) || !is_numeric($bacUserId)) {
            throw new Exception('No se pudo crear Usuario en BAC
        para realizar la compra. ' . $bacUserId);
        }
        $this->ApiUser->blockWallet($user['User']['id']);
        $dealExternal = $this->ApiPaymentService->createForCombined($user['User']['id'], $deal['Deal']['id'], $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        return $dealExternal;
    }

    protected function buyDealForBac($user, $deal, $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel = null, $giftOptions = null, $discountPin = null, $shippingAddressId = null, $request = null) {
        $this->ApiLogger->notice(__method__);
        $internalAmount = 0;
        $external_status = 'P';
        $parcialAmount = $this->getAmountTotal($deal['Deal']['discounted_price'], $quantity);
        if ($user['User']['bac_user_id'] == 0) {
            $bacUserId = $this->ApiBacService->createBacUserByDeal($user, $deal);
        } else {
            $bacUserId = $user['User']['bac_user_id'];
        }
        if (empty($bacUserId) || !is_numeric($bacUserId)) {
            throw new Exception('No se pudo crear Usuario en BAC para realizar la compra. ' . $bacUserId);
        }
        $dealExternal = $this->ApiPaymentService->create($user['User']['id'], $deal['Deal']['id'], $quantity, $paymentOptionId, $paymentPlanItemId, $internalAmount, $parcialAmount, $external_status, $buy_channel, $giftOptions, $discountPin, $shippingAddressId, $request);
        return $dealExternal;
    }

    public function isBuyOnlyWithWallet($paymentTypeId) {
        return $this->isBuyWithWallet($paymentTypeId);
    }

    public function isBuyWithWallet($paymentTypeId) {
        return $this->ApiPaymentType->isWallet($paymentTypeId);
    }

    private function getAmountTotal($deal_discount_price, $quantity) {
        return Precision::mul($deal_discount_price, $quantity);
    }

    public function hasEnoughMoney($user, $deal, $quantity = 1) {
        $user_available = $user['User']['available_balance_amount'];
        $total_deal = $this->getAmountTotal($deal['Deal']['discounted_price'], $quantity);
        return $user_available >= $total_deal;
    }

    public function isPurchasableByUser($deal, $user, $quantity = 1) {
        return $this->isPurchasable($deal, $quantity) && $this->ApiUser->canBuy($user);
    }

    public function isPurchasable($deal, $quantity = 1) {
        $hasExceeded = $this->hasExceededMaxQuantityForDeal($deal, $quantity);
        $hasExpired = $this->ApiDeal->hasExpired($deal);
        $isActive = $this->ApiDeal->isActive($deal);
        $this->ApiLogger->debug('Oferta es Comprable', array(
            'isPurchasable' => !($hasExceeded || $hasExpired || (!($isActive))),
            'dealId' => $deal['Deal']['id']
        ));
        if ($hasExceeded || $hasExpired || (!($isActive))) {
            return false;
        } else {
            return true;
        }
    }

    private function hasExceededAvailableQuantityOfPins($deal, $quantity = 1) {
        $this->ApiLogger->debug(__METHOD__, $deal['Deal']['name']);
        if (isset($deal['Deal']['product_product_id']) && ($deal['ProductProduct']['has_pins'] > 0)) {
            $availableQuantity = $this->ProductInventoryService->availableQuantity($deal['Deal']['id']);
            $this->ApiLogger->debug(__METHOD__, '$availableQuantity:' . $availableQuantity);
            $result = ($quantity > $availableQuantity);
            $this->ApiLogger->debug(__METHOD__, $result);
            return ($quantity > $availableQuantity);
        }
    }

    private function hasExceededMaxQuantityForDeal($deal, $quantity = 1) {
        if (empty($deal['Deal']['max_limit'])) {
            return false;
        }
        $quantityBought = $this->ApiDealExternal->countQuantityForDealId($deal['Deal']['id']);
        $result = ($quantity + $quantityBought) > $deal['Deal']['max_limit'];
        $this->ApiLogger->debug('Ha excedido la maxima cantidad por Oferta?', array(
            'result' => $result,
            'dealId' => $deal['DealId'],
            'Comprados' => $quantityBought,
            'AComprar' => $quantity,
            'maxLimit' => $deal['Deal']['max_limit']
        ));
        return $result;
    }

    private function isQuantityLessThanMinPerUser($deal, $quantity = 1) {
        $this->ApiLogger->debug(__METHOD__, $deal['Deal']['name']);
        if (empty($deal['Deal']['buy_min_quantity_per_user'])) {
            return false;
        }
        $result = $quantity < $deal['Deal']['buy_min_quantity_per_user'];
        $this->ApiLogger->debug('La cantidad ingresada es menor que la cantidad minima de compra por Oferta?', array(
            'result' => $result,
            'dealId' => $deal['DealId'],
            'AComprar' => $quantity,
            'buy_min_quantity_per_user' => $deal['Deal']['buy_min_quantity_per_user']
        ));
        return $result;
    }

    public function hasUpdateBacUser($user, $dealStartDate) {
        if ($user['User']['bac_id'] == 0 && strtotime($dealStartDate) >= Configure::read('tec_dia.start_date')) {
            $ret = true;
        } else {
            $ret = false;
        }
        return $ret;
    }

    public function isCreditEnough($userId, $discountedPrice, $quantity, $paymentOptionId, $pin) {
        $isCreditEnough = false;
        $user = $this->ApiUser->findById($userId);
        if (!empty($user)) {
            $credit = 0;
            if (!empty($pin)) {
                $credit = $pin['monto'];
            }
            if ($this->isBuyWithWalletByPaymentOptionId($paymentOptionId)) {
                $credit = $credit + $user['User']['available_balance_amount'];
            }
            $totalAmount = round($discountedPrice * $quantity, 2);
            $isCreditEnough = $credit >= $totalAmount;
        }
        return $isCreditEnough;
    }

    public function hasDealShippingAddresses($shippingAddressId) {
        return isset($shippingAddressId);
    }

    public function hasSelectedShippingAddress($shippingAddressId) {
        return !empty($shippingAddressId);
    }

}
