<?php
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiEmailTemplate');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiPaymentType');
App::import('Model', 'api.ApiPaymentPlanItem');
App::import('Model', 'api.ApiDeal');

class ApiEmailServiceComponent extends ApiBaseComponent {
  public $name = 'ApiEmailService';

  protected $smtpOptions = array ();
  protected $delivery = '';

  public $components = array (
    'Email'
  );
  public $emailElements = array();

  public function __construct() {
    $this->ApiEmailTemplate = &new ApiEmailTemplate();
    $this->ApiUser = &new ApiUser();
    $this->ApiPaymentType = &new ApiPaymentType();
    $this->ApiDeal = &new ApiDeal();
    $this->ApiPaymentPlanItem = &new ApiPaymentPlanItem();
  }

  public function sendEmailForBuySubdeal($user, $deal, $paymentType, $dealExternal, $quantity) {
    $finalAmount = $dealExternal['DealExternal']['final_amount'];
    $parcialAmount = $dealExternal['DealExternal']['parcial_amount'];  
    $company_address = $deal['Campany']['custom_company_address1']. ' ' . $deal['Campany']['custom_company_address2'];
    $paymentTypeName = $paymentType['PaymentType']['name'];
      
    $email_custom =  $this->setElements($user, $deal, $paymentTypeName,$quantity, $finalAmount,$parcialAmount, $company_address);
    $email_template = $this->ApiEmailTemplate->selectTemplate('Subdeal Bought');

    return $this->sendMail($email_custom, $email_template, $user['User']['email']);
  }

  public function sendEmailForBuyNow($user, $deal, $paymentType, $dealExternal, $quantity) {  
    $finalAmount = $dealExternal['DealExternal']['final_amount'];
    $parcialAmount = $dealExternal['DealExternal']['parcial_amount'];      
    $paymentTypeName = $paymentType['PaymentType']['name'];
    $company_address = $deal['Campany']['custom_company_address1']. ' ' .$deal['Campany']['custom_company_address2'];                                       
    $email_custom =  $this->setElements($user, $deal, $paymentTypeName, $quantity, $finalAmount, $parcialAmount, $company_address);
    $email_template = $this->EmailTemplate->selectTemplate('Deal Bought Ya');
    return $this->sendMail($email_custom, $email_template, $user['User']['email']);
  }
  private function getInstallmentsText($dealExternal) {
    $parcialAmount = $dealExternal['DealExternal']['parcial_amount'];
    $installments = $this->ApiPaymentPlanItem->getNumInstallments($dealExternal['DealExternal']['payment_plan_option_id']);
    $installmentsValue = round(($parcialAmount/$installments), 2);
    $installmentsText = ' (en '.$installments.' cuotas de '. 
                        Configure::read('site.currency') .
                        number_format($installmentsValue, 2, ',', '.').
                        ')';
    return $installmentsText;
  }
  private function getPaymentName($paymentType) {
    
      if($paymentType['PaymentType']['name'] == 'TarjetaDeCreditoTurismo') {
          $paymentType['PaymentType']['name'] =  'Tarjeta De Cr&eacute;dito';
      }
      if($paymentType['PaymentType']['name'] == 'PagoOfflineTurismo ') {
          $paymentType['PaymentType']['name'] =  'Pago Offline';
      }
      if($paymentType['PaymentType']['name'] == 'Tarjeta') {
          $paymentType['PaymentType']['name'] =  'Tarjeta De Cr&eacute;dito';
      }
      if($paymentType['PaymentType']['name']=='NPS Turismo'){
          $paymentType['PaymentType']['name'] =  'Tarjeta De Cr&eacute;dito';
      }
      return $paymentType['PaymentType']['name'];
  }
  private function getConnectorText($paymentType) {
    
    if($paymentType['PaymentType']['name']=='NPS Turismo'){
        $connectorText = ' con ';
    } else {
        $connectorText = ' por ';
    }
    return $connectorText;
  }
  private function getTemplateByDeal($deal) {
    if ($this->ApiDeal->isNow($deal)) {
      $template = $this->ApiEmailTemplate->selectTemplate('Deal Bought Ya');
    }
    elseif ($this->ApiDeal->isSubDeal($deal)) {
      $template = $this->ApiEmailTemplate->selectTemplate('Subdeal Bought'); 
    }
    else {
    	if ($deal['City']['is_business_unit'] == 1) {
    		$template = $this->ApiEmailTemplate->selectTemplate('Deal Bought Beneficios');
    	}else{
    		$template = $this->ApiEmailTemplate->selectTemplate('Deal Bought');
    	}
       
    }
   
    return $template;
  }
  private function getDealNameForDeal($deal) {
       if ($this->ApiDeal->isSubDeal($deal) && !empty($deal ['Deal']['descriptive_text']) && (!$this->ApiDeal->isNow($deal))) {
         $dealName = substr ($deal ['Deal']['name'], 0, - (strlen ($deal ['Deal']['descriptive_text']) + 3));
         //$dealName = $deal['Deal']['name'];
       } else {
         $dealName = $deal['Deal']['name'];
       }
       return $dealName;
  }
  private function getDescriptiveTextForDeal($deal) {
       $dealDescriptiveText = '';
       if ($this->ApiDeal->isSubDeal($deal) && !empty($deal ['Deal']['descriptive_text'])) {
           $dealDescriptiveText = $deal ['Deal']['descriptive_text'];
       } 
       return $dealDescriptiveText;
  }
  public function sendEmailForBuyDeal($user, $deal, $paymentType, $paymentOption, $dealExternal, $quantity) {
   
    $this->setOptions();
    $installmentsText = '';
    $finalAmount = $dealExternal['DealExternal']['final_amount'];
    $parcialAmount = $dealExternal['DealExternal']['parcial_amount'];
    
    if($paymentType['PaymentType']['name']=='NPS Turismo') { 
      $installmentsText = $this->getInstallmentsText($dealExternal); 
    }
    $company_address = $deal['Campany']['custom_company_address1']. ' ' . $deal['Campany']['custom_company_address2'];
    $deal['Deal']['name'] = $this->getDealNameForDeal($deal);
    $deal['Deal']['descriptive_text'] = $this->getDescriptiveTextForDeal($deal);
    $paymentTypeName = $this->getPaymentName($paymentType);
    $connectorText = $this->getConnectorText($paymentType);
    
    if (!($paymentType['PaymentType']['id']==ApiPaymentType::ID_WALLET)) {
        $parcialAmount = $this->formatAmount($deal, $parcialAmount);
        $externalPaidHtml = 'Precio pagado'.$connectorText.$paymentTypeName.': '.' <strong> ' . $parcialAmount.$installmentsText.'</strong><br/>';
        $creditUsed = $this->formatAmount($deal, ($finalAmount-$parcialAmount));
        $creditUsedHtml = $deal['Deal']['is_discount_mp'] ? ' Monto de descuento: ' : ' Precio pagado por Cuenta ClubCupon: <strong>' . $creditUsed . '</strong><br/>' ;

    } else {

       $externalPaidHtml = '';
       $creditUsed = $this->formatAmount($deal, ($finalAmount-$parcialAmount));
       $creditUsedHtml =  'Precio pagado por Cuenta ClubCupon: <strong>' . $creditUsed . '</strong><br/>' ;

    }
    $finalAmount = $this->formatAmount($deal, ($finalAmount));
    $email_custom =  $this->setElements( $user, $deal, $paymentTypeName, $quantity, $finalAmount, $parcialAmount, $company_address, $creditUsedHtml,$externalPaidHtml, $creditUsed);
    $email_template = $this->getTemplateByDeal($deal);
    
    
    if ($deal['City']['is_business_unit'] == 1) {
    	$email_template['from'] ='info@nuestrosbeneficios.com';
    }
    
    if ($deal['City']['is_business_unit'] == 0 && $paymentOption['PaymentOption']['is_offline'] == 1) {
        $return = $this->sendMail($email_custom, $email_template, $user['User']['email']);
    }
    return $return;    
  }
  protected function formatAmount($deal, $amount) {
    if ($deal['Deal']['is_discount_mp']) {
          $amount = '$' . number_format($amount, 2, ',', '.');
    } else {
          $amount = '$' . sprintf('%d', $amount);
    }
    return $amount;
  }
  protected function setOptions() {
    $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
    $this->Email->delivery = Configure::read('site.email.delivery');
  }

  protected function setElements($user, $deal, $paymentTypeName, $quantity, $total_deal_amount, $parcialAmount, $company_address, $creditUsedHtml,$externalPaidHtml, $creditUsed) {
    return array (
      '##SITE_NAME##' => Configure::read('site.name'),
      '##USERNAME##' => $user['User']['username'],
      '##DEAL_NAME##' => $deal['Deal']['name'],
      '##DEAL_TITLE##' => $deal['Deal']['name'],
      '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
      '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') .
      '/img/email/',
      '##DEAL_AMOUNT##' => $total_deal_amount,
      '##DEAL_EXTERNAL_PAID##' => Configure::read('site.currency') .$parcialAmount,
      '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
      '##QUANTITY##' => $quantity,
      '##PURCHASE_ON##' => htmlentities(strftime("%d/%m/%Y")),
      '##DEAL_STATUS##' => $this->getFriendly($deal['Deal']['deal_status_id']),
      '##COMPANY_NAME##' => $deal['Company']['name'],
      '##COMPANY_ADDRESS##' => $company_address,
      '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') .
      '/img/email/',
      '##PAYMENT_TYPE##' => $paymentTypeName,
      '##CREDIT_USED_DESCRIPTION_HTML##' => $creditUsedHtml,
      '##CREDIT_USED_DESCRIPTION##' => $creditUsedHtml,
      '##CREDIT_USED##' => $creditUsed,
      '##EXTERNAL_PAID_HTML##' => $externalPaidHtml,
      '##DESCRIPTIVE_TEXT##' => $deal ['Deal']['descriptive_text'],
      '##COUPON_URL_IMG_FOOTER##'=>'coupon_web/operadora.jpg',
    );
  }

  protected function getDataForDeal($userId, $dealId, $paymentTypeId) {
    $user = $this->ApiUser->findbyId($userId);
    $deal = $this->ApiDeal->findForApiById($dealId);
    $paymentType = $this->ApiPaymentType->findbyId($paymentTypeId);

    return array($user, $deal, $paymentType);
  }

  protected function sendMail($email_custom, $template, $to) {

    $this->Email->from = ($template['from'] == '##FROM_EMAIL##')
      ? Configure::read('EmailTemplate.from_email') : $template['from'];

    $this->Email->replyTo = ($template['reply_to'] == '##REPLY_TO_EMAIL##')
      ? Configure::read('EmailTemplate.reply_to_email') : $template['reply_to'];
    $this->Email->to = $to;
    $this->Email->subject =
      strtr($template['subject'], $email_custom);
    $this->Email->content =
      strtr($template['email_content'], $email_custom);
    $this->Email->sendAs =
      ($template['is_html']) ? 'html' : 'text';
    return $this->Email->send($this->Email->content);
  }

  protected function getFriendly($key) {
      $translations = array(
          ApiDeal::STATUS_UPCOMMING => 'Proxima',
          ApiDeal::STATUS_OPEN => 'Abierta',
          ApiDeal::STATUS_CANCELED => 'Cancelada',
          ApiDeal::STATUS_TIPPED => 'En Marcha',
          ApiDeal::STATUS_CLOSED => 'Cerrada',
          ApiDeal::STATUS_REFUNDED => ' Reintegrada ',
          ApiDeal::STATUS_PAIDTOCOMPANY => 'Pagada a empresa',
          ApiDeal::STATUS_PENDINGAPPROVAL => 'Pendiente de aprobacion',
          ApiDeal::STATUS_REJECTED => 'Rechazada',
          ApiDeal::STATUS_DRAFT => 'Borrador',
          ApiDeal::STATUS_DELETE => 'Eliminada',
          ApiDeal::STATUS_TOTALLYPAID => 'Totalmente pagada',
          ApiDeal::STATUS_FINALIZED => 'Finalizada',
          ApiDeal::STATUS_CICLEENDED => 'Fuera de ciclo',
      );
      return $translations[$key];
  }

  public function sendForgotPassword($user) {
    $this->setOptions();
    
    $contactUsURL = $this->getContactUsURL();
     
    $email_custom = 
      $this->setElementsForForgotPassword($user, $contactUsURL);
    $email_template =  
      $this->ApiEmailTemplate->selectTemplate('Forgot Password');
    
    return $this->sendMail(
            $email_custom,
            $email_template,  
            $user['User']['email']);
  }
  
  private function setElementsForForgotPassword($user, $contactUsURL) {
  
    $fields = array (
      '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
      '##USERNAME##' => (isset($user['User']['username'])) ? $user['User']['username'] : '',
      '##SITE_NAME##' => Configure::read('site.name'),
      '##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
      '##CONTACT_US##' => $contactUsURL,
      '##RESET_URL##' => Router::url(array(
        'plugin' => '',
        'controller' => 'users',
        'action' => 'reset',
        $user['User']['id'],
        $this->ApiUser->getResetPasswordHash($user['User']['id'])
        ), true) . '?from=mailing',
      '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
    );
    return $fields;
  }
  
  protected function getContactUsURL() {
     $contactUsURL = Configure::read('static_domain_for_mails') . 
      Router::url(array(
      'controller' => 'contacts',
      'action' => 'add',
      'admin' => false
      ), false) . '?from=mailing';
     return  $contactUsURL;
  }
}
?>
