<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
class ApiSecurityComponent extends ApiBaseComponent {

  public $components = array('RequestHandler', 'Session', 'Auth');
  private $fieldNameAppKey  = 'app_security_key';
  
  public function initialize(&$controller, $setting=array()) {
    if (!empty($setting['fieldNameAppKey'])) { $this->fieldNameAppKey = $setting['fieldNameAppKey']; }
    $controller->Security->enable = false;
  }

  public function startup(&$controller) {
    $controller->Security->enable = false;

    if ($this->isActionForUserLoggedIn($controller)
      && !$this->isUserLoggedIn()) {

       $this->ApiLogger->notice("Accion c/ seguridad por token de usuario, el token de usuario no es valida. Redireccionando a pagina de error por token de seguridad.");
       $controller->redirectToErrorToken();
    }

    if ($this->isActionForSecureApp($controller)
      && !$this->isSecureApp($controller)) {

       $this->ApiLogger->notice("Accion c/ seguridad por clave de aplicacion, la clave de aplicacion no es valida. Redireccionando a pagina de error por token de seguridad.");
       $controller->redirectToErrorToken();
    }
  }

  private function isUserLoggedIn() {
    $user = $this->Auth->user();
    return !empty($user);
  }

  private function isSecureApp($controller) {
    $appSecurityHashKey = $controller->getFormParam($this->fieldNameAppKey);
    if ($appSecurityHashKey == $this->generateAppHashKey()) {
      return true;
    } else {
      $this->ApiLogger->notice(
        "La clave de aplicacion no es correcta",
        array(
          'app_security_key' => $appSecurityHashKey,
          'system_key' => $this->generateAppHashKey()
        )
      );
      return false;
    }
  }

  private function generateAppHashKey() {
    return md5(Configure::read('Security.salt'). (date('z')+1));
  }

  private function getRequestedAction($controller) {
    return $controller->params['controller'].'/'.$controller->params['action'];
  }

  private function isActionForUserLoggedIn($controller) {
    return in_array(
      $this->getRequestedAction($controller),
      $controller->actionsForUserLoggedIn
    );
  }

  private function isActionForSecureApp($controller) {
    return in_array(
      $this->getRequestedAction($controller),
      $controller->actionsForAppSecure
    );
  }

}
