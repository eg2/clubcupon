<?php
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiDealUser');
App::import('Model', 'api.ApiRedemption');

App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiDeal');
App::import("Model", 'api.ApiPreredemption');
App::import("Model", 'api.ApiPin');

class ApiCouponServiceComponent extends ApiBaseComponent {
  public $name = 'ApiCouponService';

  public $components = array(
    'api.ApiDebug',
    'api.ApiBuyService',
    'api.ApiDealService',
    'api.ApiLogTransactionService'
  );

  public function __construct() {
    parent::__construct();
    $this->ApiDealUser = &new ApiDealUser();
    $this->ApiRedemption = &new ApiRedemption();
    $this->ApiUser = &new ApiUser();
    $this->ApiDeal = &new ApiDeal();
    $this->ApiPreredemption = &new ApiPreredemption();
    $this->ApiPin = new ApiPin();
  }

  public function generateCoupons($user,
    $deal,
    $quantity,
    $paymentTypeId,
    $dealExternal) {
    $dealExternalId     = $dealExternal['DealExternal']['id'];
    $price              = $deal['Deal']['discounted_price'];
    $quantity           = $quantity;
    $deal_user_existing = 
      $this->getCountDealUserbyDealExternal($dealExternalId);
    $q_create_dealuser  = $quantity - $deal_user_existing;
    
    $deal_user = array ();
    $i=0;
    $hasEnoughMoney = true;
    
    if (!empty($dealExternal['DealExternal']['gifted_amount'])) {
      $pinAmount = $dealExternal['DealExternal']['gifted_amount'];
      $userBalance = $user['User']['available_balance_amount'];
      $user['User']['available_balance_amount'] = $userBalance + $pinAmount;
    }
    while ($hasEnoughMoney && ($i < $q_create_dealuser)) {
      
      $deal_user =
        $this->setDealUserValues($user, $deal, $dealExternal, $paymentTypeId);

      if ($this->ApiDeal->hasPins($deal) || $this->ApiDeal->hasProductAndPins($deal)) {

      	if($this->ApiDeal->hasPins($deal)){
      		$pin = $this->ApiPin->next(
      				$deal ['Deal']['id'],
      				$deal ['Deal']['has_pins']
      		);
      	}elseif ($this->ApiDeal->hasProductAndPins($deal)){
      		$pin = $this->ApiPin->nextByProduct(
      				$deal ['ProductProduct']['id'],
      				$deal ['ProductProduct']['has_pins']
      		);
      	}
        

        if (!empty($pin)) {
          $deal_user ['DealUser']['pin_code'] = $pin ['code'];
        } else {
          unset ($deal_user ['DealUser']['pin_code']);
        }
      }
      $this->ApiDealUser->create();
      $this->ApiDealUser->set($deal_user);
      
      $hasEnoughMoney =
        $this->ApiBuyService->hasEnoughMoney($user, $deal, 1);

      if ($this->ApiDealUser->save($deal_user)) {
        $last_inserted_id = $this->ApiDealUser->getLastInsertId();

        $this->createRedemption($last_inserted_id);
        $this->ApiLogTransactionService->logTransaction(
                $deal_user, $last_inserted_id, $deal
        );
        $this->incrementDealUserCount($deal);
        if(($this->ApiDeal->hasPins($deal) || $this->ApiDeal->hasProductAndPins($deal) )&& !empty($pin)) {
          $this->asociatePinsWithDealUser($last_inserted_id, $pin);
        }
        
        $user['User']['available_balance_amount'] = Precision::sub(
          $user['User']['available_balance_amount'],
          $price
        );
        $this->updateUser($user);
        $this->ApiDealService->updateStatusDeal($deal);
      } 
      $i++;
    }
    return $this;
  }

  protected function incrementDealUserCount($deal) {
    $isOk = $this->ApiDeal->updateAll(
        array('Deal.deal_user_count' => 'Deal.deal_user_count + 1 ', ),
        array('Deal.id' => $deal['Deal']['id']));

    if(!$isOk || $this->ApiDeal->getAffectedRows()==0) {
      throw new Exception("Can't not imcrement deal user count");
    }
    return $this;
  }

  protected function updateUser($user) {

    return $this->ApiUser->updateAll(
      array('User.available_balance_amount' =>
        $user['User']['available_balance_amount']),
      array('User.id' => $user['User']['id'])
    );
  }

  protected function getCountDealUserbyDealExternal($dealExternalId) {
    $count = $this->ApiDealUser->find ('count',
      array('conditions' =>
        array ('DealUser.deal_external_id' =>
          $dealExternalId)));

    if (is_null($count)) { $count = 0; }
    return $count;
  }

  protected function setDealUserValues($user,
    $deal,
    $dealExternal,
    $paymentTypeId) {

    $deal_user ['DealUser']['deal_external_id'] =
      $dealExternal['DealExternal']['id'];
    $deal_user ['DealUser']['discount_amount'] =
      $deal['Deal']['discounted_price'];
    $deal_user ['DealUser']['payment_type_id'] =
      $paymentTypeId;
    $deal_user ['DealUser']['deal_id'] =
      $deal ['Deal']['id'];
    $deal_user ['DealUser']['is_gift'] = $dealExternal['DealExternal']['is_gift'];
    $deal_user ['DealUser']['user_id'] =
      $user ['User']['id'];
    $deal_user['DealUser']['quantity'] = 1;
    $deal_user['DealUser']['coupon_code'] =
      $this->generateUUID();

    if ($dealExternal['DealExternal']['is_gift']) {
      $giftOptions = (array) json_decode($dealExternal['DealExternal']['gift_data']);
      $deal_user ['DealUser']['gift_to'] = $giftOptions['to'];
      $deal_user ['DealUser']['gift_from'] = $user['User']['username'];
      $deal_user ['DealUser']['gift_email'] = $giftOptions['email'];
      $deal_user ['DealUser']['gift_dni'] =  $giftOptions['dni'];
      $deal_user ['DealUser']['gift_dob'] = $giftOptions['dob'];
      $deal_user ['DealUser']['message'] = $giftOptions['message'];
    }
    return $deal_user;
  }

  public function generateUUID() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
  }

  protected function createRedemption($dealUserId, $created_by=0) {
    $this->ApiRedemption = &new ApiRedemption();
    $saved = $this->ApiRedemption->save(array(
      'Redemption' => array(
        'deal_user_id' => $dealUserId,
        'created_by' => $created_by,
        'posnet_code' => $this->fetchPostnetCodeByDealUser($dealUserId)
      )
    ));
    return $saved;
  }

  private function fetchPostnetCodeByDealUser($dealUserId) {

    $dealUser = $this->ApiDealUser->findById($dealUserId);
    $deal   = $this->ApiDeal->findById($dealUser['DealUser']['deal_id']);
    $result = '';

    if ($this->ApiDeal->isNow($deal)==1) {
      $result = $this->generatePosnetCodeForNow($deal['Deal']['id']);
    }
    else {
      $result = $this->generatePosnetCodeForCC();
    }
    return $result;
  }

  private function generatePosnetCodeForNow($dealId) {
    $preredemption_res =
      $this->ApiPreredemption->getUnassignedPreredemptionSlot($dealId);

    if ($preredemption_res) {
      $posnet_code = $preredemption_res['Preredemption']['posnet_code'];
      $this->ApiPreredemption->assignPreredemptionSlot(
        $preredemption_res['Preredemption']['id']);
      $this->ApiLogger->debug(
      "El posnet code para now obtenido de una predemption disponible.", array(
        'predemption_res' => $preredemption_res,
      ));
    }
    return $posnet_code;
  }

  private function generatePosnetCodeForCC() {
    $posnet_code = $this->generatePosnetCode('62797603');
    $this->ApiLogger->debug(
      "El posnet code para clubcupon generado.", array(
        'posnet_code' => $posnet_code,
      ));
    return $posnet_code;
  }

  private function generatePosnetCode($preffix=null, $codeSize=18) {
    if (strlen($preffix) >= $codeSize) {
      $this->ApiLogger->info(
        "El tamano del prefijo es mayor o igual que el tamano del codigo a generar.", array(
          'preffix' => $preffix,
          'codeSize' => $codeSize
        ));
      return $preffix;
    }

    $randSize = $codeSize - strlen($preffix);
    return $preffix .  sprintf('%0'.$randSize.'d', mt_rand(1, pow(10, $randSize)-1));
  }

  public function asociatePinsWithDealUser($dealUserId, $pinIds) {

    if (empty($pinIds)) {
      throw new Exception("PinIds not be empty.");
    }
    $isOk = $this->ApiPin->updateAll(
      array('deal_user_id' => $dealUserId),
      array('id' => $pinIds["ids"])
    );
    if(!$isOk || $this->ApiPin->getAffectedRows()==0) {
      throw new Exception("Can't not asociate pins with deal user.");
    }
    return $this;
  }

  public function hasExceededMaxQuantityForUser($deal, $user, $quantity) {
    if (empty($deal['Deal']['buy_max_quantity_per_user'])) {
      return false;
    }

    $quantityBought = $this->ApiDealUser->countQuantityForDealIdAndUserId(
      $deal['Deal']['id'], $user['User']['id']
    );

    $maxPerUser = $deal['Deal']['buy_max_quantity_per_user'];

    return ($quantity + $quantityBought) > $maxPerUser;
  }
}
?>
