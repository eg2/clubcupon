<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiSameIpAttemptValidator');
App::import('Model', 'api.ApiSubscription');
App::import('Model', 'api.ApiCity');
App::import('Model', 'api.ApiUser');

class ApiSubscriptionsServiceComponent extends ApiBaseComponent {

    public $components = array(
        'Auth',
        'api.ApiDebug',
        'api.ApiEmailService'
    );

    public function __construct() {
        parent::__construct();
        $this->ApiSubscription = new ApiSubscription();
        $this->ApiCity = new ApiCity();
        $this->ApiUser = new ApiUser();
        $this->ApiSameIpAttemptValidator = new ApiSameIpAttemptValidatorComponent(3, 5, array(
            '127.0.0.1'
        ));
    }

    public function suscribe($city, $user, $ip, $campaignInfo = null) {
        $this->ApiLogger->put('userId', $user['User']['id']);
        $this->ApiLogger->put('cityId', $city['City']['id']);
        $this->ApiLogger->notice('Iniciando subscripcion de usuario a una ciudad', array(
            'city' => $city,
            'user' => $user
        ));
        $subscription = $this->getRegisterToSave($city, $user, $ip, $campaignInfo);
        $updateId = $this->isEmailForUpdate($user, $subscription);
        if ($updateId) {
            $this->ApiSubscription->id = $updateId;
        }
        if (!$this->ApiSubscription->save($subscription)) {
            $this->ApiLogger->notice('Occurrio un error al intentar guardar la subscripcion del usuario.', array(
                'subscription' => $subscription,
                'errors' => $this->getErrorFromModel('ApiSubscription')
            ));
            throw new Exception('No se guardó la subscripción.');
        }
        $idSubscription = $this->getLastIdInSubscription();
        $this->ApiLogger->put('subscriptionId', $idSubscription);
        $this->ApiLogger->notice('Subscripcion creada.', $idSubscription);
        return $idSubscription;
    }

    public function subscribeByEmail($email, $city_id, $ip) {
        $this->ApiLogger->notice('Iniciando subscripcion de email una ciudad', array(
            'city' => $city,
            'email' => $email
        ));
        if ($this->verificaremail($email) == TRUE) {
            $subscription = $this->ApiSubscription->getSubscribeByEmailToSave($email, $city_id, $ip);
            $dataUserActive = $this->ApiUser->findActiveUserByEmail($email);
            if ($dataUserActive['User']['id'] == NULL) {
                $subscription['user_id'] = NULL;
            } else {
                $subscription['user_id'] = $dataUserActive['User']['id'];
            }
            if ($this->ApiSubscription->isEmailAndCitySuscribeAndActive($email, $city_id)) {
                throw new Exception("El email " . $email . " ya esta subscripto para la ciudad " . $city_id);
            } else {
                if (!$this->ApiSubscription->save($subscription)) {
                    $this->ApiLogger->notice('Occurrio un error al intentar guardar la subscripcion del email.', array(
                        'subscription' => $subscription,
                        'errors' => $this->getErrorFromModel('ApiSubscription')
                    ));
                    throw new Exception('No se guardó la subscripción.');
                }
            }
        } else {
            throw new Exception('Email esta escrito incorecto');
        }
        $idSubscription = $this->getLastIdInSubscription();
        $this->ApiLogger->put('subscriptionId', $idSubscription);
        $this->ApiLogger->notice('Subscripcion creada.', $idSubscription);
        return $idSubscription;
    }

    private function getLastIdInSubscription() {
        if (empty($this->ApiSubscription->id)) {
            $idSubscription = $this->ApiSubscription->getLastInsertId();
        } else {
            $idSubscription = $this->ApiSubscription->id;
        }
        return $idSubscription;
    }

    public function unsuscribe($city, $user) {
        $city_id = $this->getCityIdFromParams($city);
        $subscription = $this->ApiSubscription->findByCityIdAndUserId($city_id, $user['User']['id']);
        if (empty($subscription)) {
            throw new Exception('Email No se encuentra suscrito a esa ciudad');
        }
        $subscription['Subscription']['is_subscribed'] = 0;
        $this->ApiSubscription->id = $subscription['Subscription']['id'];
        if (!$this->ApiSubscription->save($subscription)) {
            $subscription = false;
        }
        return $subscription['Subscription']['id'];
    }

    protected function getRegisterToSave($city, $user, $ip, $campaignInfo = null) {
        $subscription['user_id'] = $user['User']['id'];
        $subscription['email'] = $user['User']['email'];
        $subscription['is_subscribed'] = 1;
        $subscription['subscription_ip'] = $ip;
        $subscription['available_balance_amount'] = $user['User']['available_balance_amount'];
        $subscription['city_id'] = $this->getCityIdFromParams($city);
        if (!is_null($campaignInfo) && count($campaignInfo) > 0) {
            $this->ApiLogger->trace("campaignInfo", $campaignInfo);
            $subscription = array_merge($campaignInfo, $subscription);
        }
        return $subscription;
    }

    public function isEmailForUpdate($user, $subscription) {
        $ret = False;
        if ($this->ApiSubscription->isEmailAndCitySuscribeAndActive($user['User']['email'], $subscription['city_id'])) {
            throw new Exception('Email ya está suscrito a la ciudad.');
        }
        $usersuscribed = $this->ApiSubscription->isEmailSuscribe($user['User']['email'], $subscription['city_id']);
        if (!empty($usersuscribed['Subscription']['id'])) {
            $ret = $usersuscribed['Subscription']['id'];
        }
        return $ret;
    }

    public function getCityIdFromParams($city) {
        if (!empty($city['city_id'])) {
            $city_id = $city['city_id'];
        } else {
            $city = $this->ApiCity->findIdBySlug($city['city_slug']);
            $city_id = $city['City']['id'];
        }
        return $city_id;
    }

    private function verificaremail($email) {
        return ereg("^([a-zA-Z0-9._]+)@([a-zA-Z0-9.-]+).([a-zA-Z]{2,4})$", $email);
    }

    public function isCaptchaNecessary($ip) {
        return !$this->ApiSameIpAttemptValidator->validate($ip);
    }

}
