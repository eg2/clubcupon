<?php


App::import('Component', 'api.ApiLogger');

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiUserType');
App::import('Model', 'api.ApiUserProfile');
class ApiUserRegisterServiceComponent extends ApiBaseComponent {

  public $components = array(
    'Auth',
    'api.ApiDebug',
    'api.ApiSubscriptionsService',
    'RequestHandler'
  );

  function __construct() {
    parent::__construct();
    $this->ApiUser = new ApiUser();
    $this->ApiUserProfile = new ApiUserProfile();
  }

  public function registerAndActive($data, $subscribe=true, $userType = null, $city_id = null) {
    $this->ApiLogger->notice('Iniciando a registrar, confirmar y activar a un usuario.', array(
      'data' => $data,
      'subscribe?' => $subscribe));

    $user = $this->register($data, $subscribe, $userType, $city_id);

    if(!empty($user)) {
      $user = $this->confirmAndActive($user);
    }

    $this->ApiLogger->notice('Usuario registrado, confirmado y activo.', $user);
    return $user;
  }

  private function confirmAndActive($user) {
    $this->ApiLogger->notice('Iniciando a confirmar y activar usuario', $data);
    $this->ApiUser->id = $user["User"]["id"];
    $this->ApiUser->set(
      array(
        'is_agree_terms_conditions' => 1,
        'is_email_confirmed' => 1,
        'is_active' => 1,
      )
    );
    if(!$this->ApiUser->save()) {
      $error = $this->getErrorFromModel('ApiUser');
      $this->ApiLogger->notice('Occurrio un error al intentar guardar el usuario ya confirmado y activo.', array(
        'user' => $this->ApiUser->data,
        'errors' => $this->getErrorFromModel('ApiUser')
      ));
      throw new Exception("User Can't update.");
    }

    $this->ApiUser->read();
    return $this->ApiUser->data;
  }

  public function register($data, $subscribe=true, $userType = null,  $city_id = null) {
    $this->ApiLogger->notice('Iniciando a registrar usuario', array(
      'data' => $data,
      'subscribe' => $subscribe));
    $user = $data['User'];
    $userProfile = $data['UserProfile'];
    $this->ApiLogger->put('email', $user['email']);
    
    $user['user_type_id'] = ApiUserType::User;
    if($userType == 3){
    	$user['user_type_id'] = ApiUserType::Company;
    }
    $user['password'] = $this->Auth->password($user['password']);
    $user['signup_ip'] = $this->RequestHandler->getClientIP();
    $this->ApiUser->set($user);
    
    if(!$this->ApiUser->validates()) {
      $error = $this->getErrorFromModel('ApiUser');
      $this->ApiLogger->notice('Datos para registro de usuario no validos', array(
        'user' => $user,
        'validation_errors' => $error ));
      throw New Exception($error);
    }
    if(!$this->ApiUser->save($user)) {
      $this->ApiLogger->notice('Occurrio un error al intentar guardar el usuario.', array('user' => $user));
      throw New Exception("Tu registro no pudo completarse. Por favor, intentá más tarde.");
    }

    $userProfile["user_id"] = $this->ApiUser->id;
    if(empty($data['UserProfile']['dni'])) {
      $userProfile['dni'] = 0;
    }
    $this->ApiUserProfile->set($userProfile);
    
    if(!$this->ApiUserProfile->validates($userProfile)) {
      $error = $this->getErrorFromModel('ApiUserProfile');
      $this->ApiLogger->notice('Datos para registro de usuario no validos, validacion del perfil de usuario no satisfactoria.', array(
        'userProfile' => $userProfile,
        'validation_errors' => $error ));
      throw New Exception($error);
    }
    if(!$this->ApiUserProfile->save($userProfile)) {
      $this->ApiLogger->notice('Occurrio un error al intentar guardar el perfil del usuario.', array('userProfile' => $userProfile));
      throw New Exception("UserProfile can't create.");
    }

    $this->ApiUser->read();
    $this->ApiUserProfile->read();
    $userId = $this->ApiUser->data['User']['id'];
    $this->ApiLogger->put('userId', $userId);
    $this->ApiLogger->notice('Usuario y Perfil creados', array(
      'user' => $this->ApiUser->data,
      'userProfile' => $this->ApiUserProfile->data));

    if ($subscribe) {
      $this->ApiLogger->notice('Auto-Subscribiendo al Usuario', array(
        'user' => $this->ApiUser->data,
        'userProfile' => $this->ApiUserProfile->data));

      $this->suscribeByDefault($userId,$city_id);
    }
    return $this->ApiUser->data;
  }

  public function usernameExist($username){
    return $this->ApiUser->find('first',
                         array('conditions'=> array('username' => $username),
                               'recursive' => -1));
  }

  public function generateUsernameByEmail($email) {
    return substr(
      preg_replace(
        array("/@/","/[^0-9A-Za-z]/"),
        array("AT",""),
        $email
      ),
      0,
      15
    ).substr(uniqid(),-5);
  }

  public function validate($data) {
    $validate = array('is_valid' => true, 'errors' => array());
    $user = $data['User'];
    $userProfile = $data['UserProfile'];


    if(!$this->ApiUser->validates($user)) {
      $validate["is_valid"] = false;
      $validate["errors"]["User"] = $this->ApiUser->validationErrors;
    }

    if(!$this->ApiUserProfile->validates($userProfile)) {
      $validate["is_valid"] = false;
      $validate["errors"]["UserProfile"] = $this->ApiUser->validationErrors;
    }

    return $validate;
  }
  
  public function suscribeByDefault($userId,$city_id = null) {
    $this->ApiLogger->put('userId', $userId);
    if($city_id==null){
    	$city['city_id'] = Configure::read('CityIdDefault');
    }else{
    	$city['city_id'] = $city_id;
    } 
    $user = $this->ApiUser->findbyId($userId);
    $this->ApiSubscriptionsService->suscribe($city, $user, $ip='127.0.0.1');
  }
  
}
