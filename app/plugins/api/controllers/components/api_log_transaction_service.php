<?php
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiTransaction');
class ApiLogTransactionServiceComponent  extends ApiBaseComponent {
  public $name = 'ApiLogTransactionService';
  
  public $components = array(
    'api.ApiDebug',
  );
  public function __construct() {
    $this->ApiTransaction = &new ApiTransaction();
  }

  public function logTransaction($deal_user, $last_inserted_id, $deal) {
    $transaction_type_id = (!empty($deal['Deal']['is_gift'])) ?
      ConstTransactionTypes::DealGift :
      ConstTransactionTypes::BuyDeal;
    $transaction =
      $this->ApiTransaction->settingValues($deal_user,
      $last_inserted_id,
      $deal['Deal']['discounted_price'],
      $transaction_type_id);

    return $this->ApiTransaction->log($transaction);
  }

}

?>
