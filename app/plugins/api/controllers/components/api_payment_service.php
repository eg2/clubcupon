<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiDealExternal');
App::import('Model', 'api.ApiPaymentOption');
App::import('Model', 'api.ApiPaymentType');
App::import('Model', 'api.ApiUser');

class ApiPaymentServiceComponent extends ApiBaseComponent {

    public $name = 'ApiPaymentService';
    public $components = array(
        'api.ApiDebug',
        'api.ApiGiftPinService'
    );

    public function __construct() {
        $this->ApiDeal = & new ApiDeal();
        $this->ApiDealExternal = & new ApiDealExternal();
        $this->ApiPaymentOption = & new ApiPaymentOption();
        $this->ApiUser = & new ApiUser();
        $this->ApiPaymentType = & new ApiPaymentType();
        parent::__construct();
    }

    public function createForCombined($userId, $dealId, $quantity, $paymentOptionId, $paymentPlanItemId, $buy_channel, $giftOptions, $discountPinCode, $shippingAddressId, $source = null) {
        $user = $this->ApiUser->findbyId($userId);
        $deal = $this->ApiDeal->findbyId($dealId);
        $external_status = 'P';
        $internalAmount = $user['User']['available_balance_amount'];
        $finalAmount = $this->getFinalAmount($deal['Deal']['discounted_price'], $quantity);
        $parcialAmount = $finalAmount;
        $dealExternal = $this->create($user['User']['id'], $deal['Deal']['id'], $quantity, $paymentOptionId, $paymentPlanItemId, $internalAmount, $parcialAmount, $external_status, $buy_channel, $giftOptions, $discountPinCode, $shippingAddressId, $source);
        return $dealExternal;
    }

    public function addGiftOptions($dealExternal, $giftOptions) {
        if ($giftOptions['is_gift']) {
            $dealExternal['DealExternal']['is_gift'] = 1;
            $giftOptions['is_gift'] = (bool) $giftOptions['is_gift'];
            $dealExternal['DealExternal']['gift_data'] = json_encode($giftOptions);
        } else {
            $dealExternal['DealExternal']['gift_data'] = json_encode(array(
                'is_gift' => false
            ));
            $dealExternal['DealExternal']['is_gift'] = 0;
        }
        return $dealExternal;
    }

    public function addDiscountMP($dealExternal) {
        $parcialAmount = $dealExternal['DealExternal']['parcial_amount'];
        $discountMPAmount = $this->calculateDiscountMPAmount($parcialAmount);
        $dealExternal['DealExternal']['discounted_amount'] = $discountMPAmount;
        $dealExternal['DealExternal']['parcial_amount'] = Precision::sub($parcialAmount, $discountMPAmount);
        return $dealExternal;
    }

    public function addDiscountPin($dealExternal, $discountPinCode) {
        $discountPin = $this->ApiGiftPinService->saveBacPinInGiftPinByCode($discountPinCode);
        $this->ApiGiftPinService->block($discountPinCode);
        $dealExternal['DealExternal']['gift_pin_id'] = $discountPin['GiftPin']['id'];
        $dealExternal['DealExternal']['gift_pin_code'] = $discountPin['GiftPin']['code'];
        $giftedAmount = $this->ApiGiftPinService->calculateGiftUsedAmount($discountPin['GiftPin']['discount'], $dealExternal['DealExternal']['final_amount']);
        $this->ApiLogger->trace("AMOUNTS", array(
            'discountPinAmount' => $discountPin['GiftPin']['discount'],
            'finalAmount' => $dealExternal['DealExternal']['final_amount'],
            'giftedAmountREsult' => $giftedAmount,
            'dealExternal' => $dealExternal
        ));
        $dealExternal['DealExternal']['gifted_amount'] = $giftedAmount;
        if ($dealExternal['DealExternal']['payment_type_id'] != ApiPaymentType::ID_WALLET) {
            $dealExternal['DealExternal']['parcial_amount'] = $dealExternal['DealExternal']['parcial_amount'] - $dealExternal['DealExternal']['gifted_amount'];
        }
        return $dealExternal;
    }

    public function create($userId, $dealId, $quantity, $paymentOptionId, $paymentPlanItemId, $internalAmount = 0, $parcialAmount = 0, $external_status = 'P', $buy_channel = false, $giftOptions = null, $discountPinCode = null, $shippingAddressId = null, $source = null) {
        $deal = $this->ApiDeal->findById($dealId);
        if (empty($buy_channel)) {
            $buy_channel = ApiDealExternal::BUY_CHANNEL_TYPE_MOBILE;
        }
        $dealExternal = array(
            'DealExternal' => array(
                'user_id' => $userId,
                'deal_id' => $dealId,
                'quantity' => $quantity,
                'payment_option_id' => $paymentOptionId,
                'payment_plan_option_id' => $paymentPlanItemId,
                'internal_amount' => $internalAmount,
                'external_status' => $external_status,
                'buy_channel_type_id' => $buy_channel,
                'created_by' => $userId,
                'modified_by' => $userId,
                'shipping_address_id' => $shippingAddressId,
                'source' => $source
            )
        );
        $paymentOption = $this->ApiPaymentOption->findById($paymentOptionId);
        $dealExternal['DealExternal']['payment_type_id'] = $paymentOption['PaymentOption']['payment_type_id'];
        $dealExternal = $this->ApiDealExternal->setDatesToNow($dealExternal);
        $dealExternal['DealExternal']['final_amount'] = $this->getFinalAmount($deal['Deal']['discounted_price'], $quantity);
        $dealExternal = $this->addGiftOptions($dealExternal, $giftOptions);
        $dealExternal['DealExternal']['parcial_amount'] = $parcialAmount;
        if (!is_null($discountPinCode) && $this->ApiGiftPinService->isUsable($discountPinCode)) {
            $dealExternal = $this->addDiscountPin($dealExternal, $discountPinCode);
        }
        if ($dealExternal['DealExternal']['payment_type_id'] != ApiPaymentType::ID_WALLET) {
            $dealExternal['DealExternal']['parcial_amount'] = $dealExternal['DealExternal']['parcial_amount'] - $internalAmount;
        }
        if ($deal['Deal']['is_discount_mp'] && $this->ApiPaymentType->isPaymentTypeMP($dealExternal['DealExternal']['payment_type_id'])) {
            $dealExternal = $this->addDiscountMP($dealExternal);
        }
        $dealExternal = $this->ApiDealExternal->insertDealExternal($dealExternal);
        if (is_null($dealExternal) || is_null($dealExternal['DealExternal']['id'])) {
            $error = $this->getErrorFromModel('ApiDealExternal');
            throw new Exception('No se pudo crear el Pago. - La compra no fue realizada, ' . $error);
        }
        $this->ApiDeal->updateDealExternalCount($deal['Deal']['id'], $quantity);
        return $dealExternal;
    }

    function calculateDiscountMPAmount($paymentAmount) {
        $discount = Precision::mul($paymentAmount, ((float) Configure::read('deal.discount_mp')));
        if ($discount > ((float) Configure::read('deal.limit_discount_mp'))) {
            $discount = Configure::read('deal.limit_discount_mp');
        }
        return $discount;
    }

    private function getFinalAmount($deal_discount_price, $quantity) {
        $finalAmount = Precision::mul($deal_discount_price, $quantity);
        return $finalAmount;
    }

    public function isLiteBoxEnable() {
        return Configure::read('isLiteBoxEnabled');
    }

    public function useLiteBox($payment_type_id) {
        return ($this->isLiteBoxEnable() && $this->ApiPaymentType->isPaymentTypeMP($payment_type_id));
    }

    public function isCombined($dealExternal) {
        if (($dealExternal['DealExternal']['internal_amount'] > 0) && !empty($dealExternal['DealExternal']['parcial_amount'])) {
            $isCombined = true;
        } else {
            $isCombined = false;
        }
        return $isCombined;
    }

    public function isWallet($dealExternal) {
        $idPaymentTypeWallet = $this->ApiPaymentType->findPaymentTypeIdForWallet();
        if (!empty($dealExternal->internal_amount) && $dealExternal->payment_type_id == $idPaymentTypeWallet) {
            $isWallet = true;
        } else {
            $isWallet = false;
        }
        return $isWallet;
    }

    public function isDirect($dealExternal) {
        $idPaymentTypeWallet = $this->ApiPaymentType->findPaymentTypeIdForWallet();
        if ($dealExternal->internal_amount == 0 && $dealExternal->payment_type_id != $idPaymentTypeWallet) {
            $isDirect = true;
        } else {
            $isDirect = false;
        }
        return $isDirect;
    }

    public function addAmountToWalletIfHasGiftPin($dealExternal, $user) {
        $this->ApiLogger->notice('Depositando monto del codigo de pin en Monedero');
        $giftPinCode = $dealExternal['DealExternal']['gift_pin_code'];
        $this->ApiGiftPinService->unblock($giftPinCode);
        if ($giftPinCode) {
            $bacPin = $this->ApiGiftPinService->isUsable($giftPinCode);
            $this->ApiLogger->trace('bacPin - addAmountToWalletIfHasGiftPin', $bacPin);
        }
        if (!$bacPin) {
            $this->ApiLogger->error('No es usable, pincode: ' . $dealExternal['DealExternal']['gift_pin_code'], $dealExternal);
        } else {
            $user = $this->addAmountToWallet($dealExternal['DealExternal']['gifted_amount'], $user, 'add amount to wallet from GiftPin');
        }
        return $user;
    }

    public function addAmountToWallet($amount, $user, $message) {
        $this->ApiLogger->trace('addAmountToWallet - Entrada', array(
            $amount,
            $user,
            $message
        ));
        $data = array(
            'user_id' => $user['User']['id'],
            'foreign_id' => ConstUserIds::Admin,
            'class' => 'SecondUser',
            'amount' => $amount,
            'description' => $message ? $message : 'added amount to wallet ' . __METHOD__,
            'gateway_fees' => 0,
            'transaction_type_id' => ConstTransactionTypes::AddedToWallet
        );
        $user['User']['available_balance_amount'] = $user['User']['available_balance_amount'] + $amount;
        $this->ApiUser->updateall(array(
            'User.available_balance_amount' => $user['User']['available_balance_amount']
                ), array(
            'User.id' => $user['User']['id']
        ));
        $this->ApiLogger->trace('Esto es lo que guarda para el usuario', $user);
        return $user;
    }

}
