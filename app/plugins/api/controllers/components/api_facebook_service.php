<?php
App::import('Component', 'api.ApiBase');
App::import('Vendor', Configure::read('facebook_lib.path')); //facebook_desa.php

class ApiFacebookServiceComponent extends ApiBaseComponent {
  public $name = 'ApiFacebookService';
  
  public function facebook_login($appId, $appSecret) {
    try {
       $this->facebook = new Facebook(array(
        'appId' => $appId,
        'secret' => $appSecret,
        'cookie' => true,
       ));
       return $this->facebook->api('/me?fields=id,email,first_name,middle_name,last_name,about');
    }
    catch (Exception $e) {
      die($e);
    }
  }
}

?>
