<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiCxsenseService');
App::import('Model', 'api.ApiUser');

class ApiUserLoginServiceComponent extends ApiBaseComponent {

    const emptyFields = 1;
    const badEmail = 2;
    const badPassword = 3;

    public $components = array(
        'Auth',
        'api.ApiDebug',
        'api.ApiEmailService'
    );

    function __construct() {
        $this->ApiUser = new ApiUser();
        $this->ApiCxsenseService = new ApiCxsenseServiceComponent();
        parent::__construct();
    }

    public function login($username = null, $email = null, $password = null) {
        if ((empty($username) || empty($email)) && empty($password)) {
            throw new Exception("Por favor, completá todos los campos.", self::emptyFields);
        }
        if (!empty($username)) {
            $user = $this->ApiUser->findByUserNameWithProfile($username);
        } else {
            $user = $this->ApiUser->findByEmailWithProfile($email);
        }
        if (empty($user)) {
            throw new Exception("Ingresá un mail válido.", self::badEmail);
        }
        $user['User']['password'] = $this->Auth->password($password);
        if ($this->Auth->login($user['User'])) {
            $this->ApiCxsenseService->sendGccUserData($user);
            $this->ApiCxsenseService->sendGczUserData($user);
            return $user;
        } else {
            throw new Exception("Login inválido.", self::badPassword);
        }
    }

    public function loginByToken($token) {
        $user = $this->ApiUser->findByToken($token);
        if (empty($user)) {
            throw new Exception('Token is not valid.');
        }
        if (!$this->ApiUser->isTokenUsable($user)) {
            throw new Exception('Token is not usable currently.');
        }
        if (!$this->Auth->login($user)) {
            throw new Exception('Failed to login the user.');
        }
        return $user;
    }

    public function forgotPassword($email) {
        $conditions = array(
            'User.email' => $email
        );
        $user = $this->ApiUser->findActiveByEmail($conditions);
        if (empty($user)) {
            throw new Exception("No se encontrá el usuario.");
        } else {
            $this->ApiEmailService->sendForgotPassword($user);
        }
        return true;
    }

    public function loginByUser($user) {
        if (empty($user)) {
            throw new Exception('Token is not valid.');
        }
        if (!$this->Auth->login($user)) {
            throw new Exception('Failed to login the user.');
        }
        return $this->ApiUser->token($user);
    }

    public function updateUserFacebookId($user, $facebookId) {
        if (!empty($user['User']['fb_user_id'])) {
            if ($user['User']['fb_user_id'] != $facebookId) {
                $this->ApiLogger->info("El usuario ya posee un facebook Id y es distinto al ingresado", array(
                    "User" => $user,
                    "nuevo_facebook_id" => $facebookId
                ));
            } else {
                $this->ApiLogger->info("El usuario ya tiene ese facebook id", array(
                    "User" => $user,
                    "nuevo_facebook_id" => $facebookId
                ));
            }
            return $user;
        }
        $user['User']['fb_user_id'] = $facebookId;
        $this->ApiUser->save($user);
    }

}
