<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiBac');
App::import('Model', 'api.ApiGiftPin');

class ApiGiftPinServiceComponent extends ApiBaseComponent {

    const BAC_STATUS_AVAILABLE = 1;
    const BAC_STATUS_USED = 2;
    const BAC_STATUS_CANCELED = 3;
    const BAC_STATUS_EXPIRED = 4;
    const BAC_STATUS_DISABLED = 5;
    const BAC_STATUS_CANCELED_MANUALLY = 6;

    private $bac_id_portal = null;
    public $name = 'ApiGiftPinService';
    public $components = array(
        'api.ApiDebug',
        'api.ApiBac'
    );

    public function __construct() {
        $this->ApiGiftPin = & new ApiGiftPin();
        $this->bac_id_portal = Configure::read('BAC.id_portal');
        if (!isset($this->ApiBac)) {
            $this->ApiBac = & new ApiBacComponent();
        }
    }

    public function isUsable($code) {
        if (empty($code)) {
            return false;
        }
        if (strlen($code) > 10) {
            return false;
        }
        $bacPin = $this->obtenerBacPin($code);
        if ($this->isAvaliableForBac($bacPin) && !$this->isBlocked($code)) {
            return $bacPin;
        } else {
            return false;
        }
    }

    private function isAvaliableForBac($bacPin) {
        if (!empty($bacPin)) {
            return $bacPin['idEstadoPin'] == self::BAC_STATUS_AVAILABLE;
        }
        return false;
    }

    private function isBlocked($code) {
        $giftPin = $this->ApiGiftPin->findByCode($code);
        if (!empty($giftPin)) {
            return $this->ApiGiftPin->isBlocked($giftPin);
        }
        return false;
    }

    public function saveBacPinInGiftPinByCode($code) {
        $giftPin = $this->ApiGiftPin->findByCode($code);
        if (empty($giftPin)) {
            $bacPin = $this->obtenerBacPin($code);
            $giftPin = array(
                'GiftPin' => array()
            );
            $giftPin['GiftPin']['code'] = $code;
            $giftPin['GiftPin']['discount'] = $bacPin['monto'];
            $giftPin['GiftPin']['status'] = $bacPin['idEstadoPin'];
            $giftPin = $this->ApiGiftPin->save($giftPin);
            $giftPin['GiftPin']['id'] = $this->ApiGiftPin->id;
            Debugger::log(__METHOD__ . __LINE__ . print_r($giftPin, 1));
        }
        return $giftPin;
    }

    public function block($code) {
        $giftPin = $this->ApiGiftPin->findByCode($code);
        if (empty($giftPin)) {
            $giftPin = $this->saveBacPinInGiftPinByCode($code);
        }
        $giftPin = $this->ApiGiftPin->block($giftPin);
        return $giftPin;
    }

    public function unBlock($code) {
        $giftPin = $this->ApiGiftPin->findByCode($code);
        return $this->ApiGiftPin->unBlock($giftPin);
    }

    public function unBlockAndBurnGiftPin($code, $userId) {
        $this->unBlock($code);
        return $this->burnGiftPin($code, $userId);
    }

    public function burnGiftPin($code, $userId) {
        $giftPin = $this->saveBacPinInGiftPinByCode($code);
        $consumirPinParams = array(
            'idUsuarioPortal' => $userId,
            'idPortal' => $this->bac_id_portal,
            'nroPin' => $code,
            'retailer' => 'Compra ClubCupon'
        );
        try {
            $bacResponse = $this->ApiBac->consumirPin($consumirPinParams);
            if (is_string($bacResponse)) {
                Debugger::log(__CLASS__ . '::' . __METHOD__ . __LINE__ . ' throws ' . $bacResponse, LOG_DEBUG);
            }
        } catch (Exception $e) {
            Debugger::log(__CLASS__ . '::' . __METHOD__ . __LINE__ . ' throws ' . "No se pudo quemar el Pin en BAC " . $code, LOG_DEBUG);
        }
        $giftPin['GiftPin']['status'] = self::BAC_STATUS_USED;
        Debugger::log("QUEMANDO PIN START");
        Debugger::log(print_r($giftPin, 1));
        Debugger::log("QUEMANDO PIN END");
        $this->ApiGiftPin->save($giftPin);
        return $giftPin;
    }

    private function obtenerBacPin($code) {
        $params = array(
            'idPortal' => $this->bac_id_portal,
            'codigoPin' => $code
        );
        $bacPin = $this->ApiBac->obtenerPin($params);
        if (is_string($bacPin)) {
            throw new Exception("$bacPin");
        }
        return get_object_vars($bacPin);
    }

    public function calculateGiftUsedAmount($giftAvailableAmount, $totalGrossAmount) {
        $giftUsedAmount = 0;
        if ($totalGrossAmount <= $giftAvailableAmount) {
            $giftUsedAmount = $totalGrossAmount;
        } else {
            $giftUsedAmount = $giftAvailableAmount;
        }
        return $giftUsedAmount;
    }

}
