<?php

class ApiIvrsController extends ApiAppController
{
  public $name = 'ApiIvrs';

  var $uses = array(
    'api.ApiRedemption',
    'api.ApiCompany',
  );

  public $components = array(
    'api.ApiRedemptionService'
  );

  public $actionsForAppSecure = array(
    'api_ivrs/validatecompanynumber',
    'api_ivrs/validateposnetcode',
    'api_ivrs/getcompanynumbers',
    'api_ivrs/getposnetcodes',
    'api_ivrs/test',
  );

  public function validatecompanynumber() {
    $this->layout= false;
    $companyId = $this->params['form']['companyNumber'];
    $this->ApiLogger->put('companyId', $companyId);
    $this->ApiLogger->notice("Iniciando validacion de nro de compania por ivr", $this->params['form']);

    $company = $this->ApiCompany->findById($companyId);
    if (!is_null($company)) {
      $result = array('status' => 1);
      $this->set('json', $result);
      $this->ApiLogger->notice(
        "El id de compania es valido. La validacion fue satisfactoria.",
        $result
      );

    } else {
      $result = array(
        'status' => 0,
        'error_code' => ApiRedemptionServiceComponent::ERROR_CODE_COMPANY_INVALID
      );
      $this->set('json', $result);
      $this->ApiLogger->notice(
        "El id de compania no es valido. La validacion no fue satisfactoria.",
        $result
      );
    }

    $this->render('ivr_output');
  }

    public function test() {
        $this->layout= false;
        $json = array('method' => __METHOD__);
        $this->set('json', $this->params);
        $this->render('ivr_output');
    }


  public function validateposnetcode() {
    try {
      $this->layout= false;

      $companyId = $this->params['form']['companyNumber'];
      $posnetCode = $this->params['form']['posnetCode'];
      if (strlen($posnetCode)==10) {
        $posnetCode = ApiRedemption::POSNET_CODE_IVR_PREFIX.$posnetCode;
      }
      $this->ApiLogger->put('postnetCode', $posnetCode);
      $this->ApiLogger->notice("Iniciando redencion de cupon por ivr", $this->params['form']);

      $redeemedResult = $this->ApiRedemptionService->validateAndRedeem($posnetCode, $companyId, ApiRedemption::WAY_IVR);
      if ($redeemedResult['redeemed']) {
        $redemption = $this->ApiRedemption->findByPosnetCode($posnetCode);
        $this->ApiLogger->notice("La redencion por ivr fue exitosa");
        $result =  array('status' => 1, 'deal_user_id' => $redemption['Redemption']['deal_user_id'],);

      } else {
        $this->ApiLogger->notice("La redencion por ivr no fue exitosa.", array('errorCode' => $redeemedResult["errorCode"]));
        $result = array('status' => 0, 'error_code' => $redeemedResult['errorCode']);
      }

    } catch (Exception $e) {
      $this->ApiLogger->error('Error al intentar redimir por ivr un cupon.', array('error' => $e->getMessage()));
      $result = array('status' => 0, 'error_code' => ApiRedemptionServiceComponent::ERROR_CODE_UNKOWN_ERROR);
    }

    $this->ApiLogger->notice('Finalizando redencion de cupon por ivr.', $result);
    $this->set('json', $result);
    $this->render('ivr_output');
  }

    public function getcompanynumbers() {
        $this->layout= false;


        $page = $this->params['form']['page'];
        $pagesize = $this->params['form']['pageSize'];

        $json = array('CompanyNumbers' => $this->ApiRedemption->getCompanyNumbers($page,$pagesize));
        $this->set('json', $json);
        $this->render('ivr_output');
    }

    public function getposnetcodes() {
        $this->layout= false;
        $page = $this->params['form']['page'];
        $pagesize = $this->params['form']['pageSize'];
       ;
        $results= $this->ApiRedemption->getPosnetCodes($page,$pagesize);
        $json = array();
        foreach($results as $result){
            array_push($json, array('Companynumber'=> $result['Company']['id'],
                              'Posnetcode'=>substr($result['Redemption']['posnet_code'],8,10)
                ));
        }
        $this->set('json', array('PosnetCodes'=>$json));
        $this->render('ivr_output');
    }

}
