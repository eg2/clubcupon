<?php

class ApiLogsController extends ApiAppController {

  public $name = 'ApiLogs';
  public $uses = array(
  );

  public function save() {
    $logParam = $this->getFormParam('log');
    if (is_null($logParam)) {
      throw new Exception("El parametro log no debe ser vacio.");
    }
    $this->ApiLogger->log(
      $logParam['message'],
      $logParam['extraInfo'],
      $logParam['severity'],
      'AjaxLogger::'.$logParam['name'],
      $logParam['mdc']
    );
  }

}
?>
