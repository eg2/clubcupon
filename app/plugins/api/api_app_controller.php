<?php
class ApiAppController extends AppController {

  public $logFileNamePrefix = "api";

  public $layout = 'api.default';

  public $helpers = array(
    'api.Api',
    'Html'
  );

  public $components = array(
    'Auth' => array(
      'authorize' => 'controller'
    ),
    'Session',
    'api.ApiDebug',
    'api.ApiUserLoginService',
    'api.ApiSecurity',
    'api.ApiLogger'
  );

  public $uses = array(
    'api.ApiUser'
  );

  public $actionsForUserLoggedIn = array();

  public $actionsForAppSecure = array();

  private $staticDomain;

  private $imageDefaultOptions = array(
    'dimension' => 'medium_big_thumb',
    'class' => '',
    'alt' => 'alt',
    'title' => 'title',
    'type' => 'jpg'
  );

  function __construct() {
    parent::__construct();
    App::import('vendor', 'Utility', array('file' => 'Utility/ApiLogger.php'));
    App::import('vendor', 'Utility', array('file' => 'Utility/ApiXml.php'));
    $this->staticDomain = Configure::read('static_domain_for_api');
    $this->defaultLimit = Configure::read('ApiModel.find.pagelimit');
  }

  public function beforeFilter() {
    try {
      $this->Security = false;
      $this->Session->delete('Message.flash');

      if ($this->_isJSON() && !$this->RequestHandler->isGet()) {
        if (empty($this->data) && !empty($_POST)) {
          $this->data[$this->modelClass] = $_POST;
        }
        if (isset($_POST['data'])) {
            $this->params['post'] = json_decode($_POST['data']);
        }
      }

      $token = $this->getTokenParam();
      try {
        if(!empty($token)) {
          $user = $this->ApiUserLoginService->loginByToken($token);
        }
      } catch (Exception $e) {
         $this->ApiLogger->notice("Error al intentar loguear el usuario por token.", array('token' => $token, 'error' => $e->getMessage()));
         $this->ApiLogger->error("Error al intentar loguear el usuario por token.", array('token' => $token, 'error' => $e->getMessage()));
         $this->redirectToErrorToken();
      }

      if (!empty($user)) {
        $this->ApiUser->useToken($user);
      }

      parent::beforeFilter();
    } catch (Exception $e) {
      $this->autoRender = false;
      $this->ApiLogger->notice("Error no clasificado en el beforeFilter.", array('params' => $this->params, 'error' => $e->getMessage()));
      $this->ApiLogger->error("Error no clasificado en el beforeFilter.", array('params' => $this->params, 'error' => $e->getMessage()));
      echo json_enconde(array('error'=> $e->getMessage()));
    }
  }

  public function getRequestedAction() {
    return $this->params['controller'].'/'.$this->params['action'];
  }

  public function beforeRender() {
    parent::beforeRender();
    if ($this->_isJSON() || $this->_isXML()) {
      Configure::write('debug', 0);
      $this->disableCache();
    }
  }

  protected function _isJSON() {
    return $this->RequestHandler->ext == 'json';
  }

  protected function _isXML() {
    return $this->RequestHandler->ext == 'xml';
  }

  protected function getUser() {
    return $this->Auth->user();
  }
  public function getCurrentUser() {
     $userId = $this->Auth->user('id');
     $user = $this->ApiUser->findById(
        $userId,
        array('id', 'email', 'username', 
            'fb_user_id', 'gift_user_id',
            'bac_user_id', 'token_used', 'token_uses',
            'token', 'available_balance_amount')
      );
     return $user;
  }
  protected function isLoggedUser() {
    $user = $this->Auth->user();
    return !empty($user);
  }

  protected function getTokenParam() {
    if (!is_null($this->getFormParam('token'))) {
      $token = $this->getFormParam('token');
    } else {
      $token = $this->getUrlParam('token');
    }
    return $token;
  }

  public function getFormParam($key) {
    return $this->getParam($key, 'form');
  }

  public function getUrlParam($key) {
    return $this->getParam($key, 'url');
  }

  public function getNamedParam($key) {
    return $this->getParam($key, 'named');
  }

  public function getParam($key, $parentKey) {
    if(isset($this->params[$parentKey][$key])) {
      return $this->params[$parentKey][$key];
    }
    return null;
  }

  protected function getLimitParam() {
    $limit = $this->getUrlParam('limit');
    if (empty($limit)) {
      $limit = $this->defaultLimit;
    }
    return $limit;
  }

  protected function getPageParam() {
    $page = $this->getUrlParam('page');
    if (empty($page)) {
      $page = 1;
    }
    return $page;
  }

  /**
   * @param options
   */
  protected function getImageUrl($model, $attachment, $options=null, $domain=true) {
    if (!empty($options)) {
      $options = array_merge($this->imageDefaultOptions, $options);
    } else {
      $options = $this->imageDefaultOptions;
    }

    $toEncript = Configure::read('Security.salt');
    $toEncript .= $model . $attachment['Attachment']['id'];
    $toEncript .= $options['type'] . $options['dimension'];
    $toEncript .= Configure::read('site.name');

    $imageHash = $options['dimension'] . '/';
    $imageHash .= $model . '/';
    $imageHash .= $attachment['Attachment']['id'];
    $imageHash .= '.' . md5($toEncript);
    $imageHash .= '.' . $options['type'];

    if ($domain) {
        return $this->staticDomain . 'img/' . $imageHash;
    } else {
        return '/img/' . $imageHash;
    }
  }

  protected function getDealUrl($deal) {
    $dealUrl = null;
    if($this->ApiDeal->isNow($deal)) {
      $dealUrl = $this->getNowUrl();
    } else {
      $dealUrl = $this->getDealWebUrl($deal);
    }
    return $dealUrl;
  }

  protected function getDealWebUrl($deal) {
    return Router::url(
      array(
        'plugin'    => '',
        "controller" => "deal",
        "action" => "",
        "id"=>$deal["Deal"]["slug"]
      ),
      true
    );
  }



  protected function getNowUrl() {
    return Router::url(
      array(
        'plugin'    => 'now',
        "controller" => "now_deals",
        "action" => "index",
      ),
      true
    );
  }

  public function redirectToErrorToken() {
    $this->redirect(array(
      'plugin' => 'api',
      'controller' => 'api_users',
      'action' =>'token_error'
    ));
  }

  public function debug($e, $severity) {
    $this->ApiLogger->error("Ocurrio una Exception", array('error' => $e->getMessage()));
    $this->ApiLogger->notice("Ocurrio una Exception", array('error' => $e->getMessage()));
  }
}
?>
