<?php

App::import('vendor', 'Utility', array('file' => 'Utility/ApiStrUtils.php'));

class AccountingAppTestCase extends CakeTestCase {
  public $autoFixtures = false;

  function startCase() {
    parent::startCase();
  }

  function testShouldDefined() {
    $this->assertTrue(class_exists("ApiStrUtils"));
  }
}
?>
