<?php
App::import('Component', 'api.ApiBac');
App::import('', 'api.ApiAppTestCase');

class ApiBacTestCase extends ApiAppTestCase {

  public $autoFixtures = false;
  public $fixtures = false;

  function startCase() {
    parent::startCase();
    $this->ApiBac = ClassRegistry::init('ApiBacComponent');
  }

  function testShouldDefined() {
    $this->assertTrue(
      class_exists("ApiBacComponent"),
      "La clase ApiBac debe existir.");
    $this->assertTrue(
      is_object($this->ApiBac),
      "Se debe poder instanciar un ApiBac object.");
  }

  function testCrearUsuario($params) {
  }

  function testModificarUsuario($params) {
  }

  function testLiquidarEmpresa($params) {
  }

}
?>
