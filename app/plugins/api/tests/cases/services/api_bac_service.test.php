<?php
App::import('', 'api.ApiAppTestCase');
App::import('Component', 'api.ApiBac');
App::import('Component', 'api.ApiBacService');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiUser');

class ApiBacServiceTestCase extends ApiAppTestCase {

  var $fixtures = array(
    'plugin.api.api_attachment',
    'plugin.api.api_branch_deal',
    'plugin.api.api_city',
    'plugin.api.api_company',
    'plugin.api.api_deal',
    'plugin.api.api_deal_external',
    'plugin.api.api_payment_method',
    'plugin.api.api_payment_setting',
    'plugin.api.api_payment_type',
    'plugin.api.api_user',
    'plugin.api.api_user_profile',
  );

  function startCase() {
    parent::startCase();
    $this->ApiCompany = ClassRegistry::init('ApiCompany');
    $this->ApiDeal = ClassRegistry::init('ApiDeal');
    $this->ApiUser = ClassRegistry::init('ApiUser');
    $this->ApiUserProfile = ClassRegistry::init('ApiUserProfile');
    $this->ApiBacService = ClassRegistry::init('ApiBacServiceComponent');
  }

  function testShouldDefined() {
    $this->assertTrue(
      class_exists("ApiBacServiceComponent"),
      "La clase ApiBacService debe existir.");
    $this->assertTrue(
      is_object($this->ApiBacService),
      "Se debe poder instanciar un ApiBacService object.");
  }

  function testUpdateUserInBac() {
    $userId = 1;
    $user = $this->ApiUser->findById($userId);
    $userProfile = $this->ApiUserProfile->findByUserId($user['User']['id']);

    $this->ApiLogger->trace('params' , array(
      'user' => $user,
      'userProfile' => $userProfile,
    ));

    $expectedBacId = $user['User']['bac_id'];
    $expectedBacParams = array(
      'idUsuario'             => $user['User']['bac_id'],
      'idPortal'              => 18,
      'idUsuarioPortal'       => $user['User']['id'],
      'login'                 => $user['User']['username'],
      'email'                 => $user['User']['email'],
      'apellido'              => $userProfile['UserProfile']['first_name'],
      'nombre'                => $userProfile['UserProfile']['last_name'],
      'idPais'                => Configure::read('BAC.Argentina'),
      'idProvincia'           => Configure::read('BAC.CapitalFederal'),
      'idTipoUsuario'         => Configure::read('BAC.id_tipo_usuario_usuario'),
    );

    $apiBacMock = ClassRegistry::init('ApiBacMock');
    $apiBacMock->mockReturn = $expectedBacId;
    $this->ApiBacService->ApiBac = $apiBacMock;
    $this->ApiBacService->createBacUser($user);


    $this->assertEqual(
      1, count($apiBacMock->createOrUpdateUserCalls),
      "Debe llamar al crearOrUpdateUser una vez.".json_encode(
        array('llamadas' => count($apiBacMock->createOrUpdateUserCalls)))
      );

    ksort($expectedBacParams);
    ksort($apiBacMock->createOrUpdateUserCalls[0]);
    $this->assertEqual(
      $expectedBacParams,
      $apiBacMock->createOrUpdateUserCalls[0],
      "Debe llamar crearOrUpdateUser con los parametros esperados y solo ellos.".json_encode(
        array(
          'Esperados' => $expectedBacParams,
          'Reales' => $apiBacMock->createOrUpdateUserCalls[0],
          'Parametros que estan en esperados y son distintos o no estan en los reales' =>  array_diff_assoc(
            $expectedBacParams, $apiBacMock->createOrUpdateUserCalls[0]),
          'Parametros que estan en reales y son distintos o no estan en los esperados' =>  array_diff_assoc(
            $apiBacMock->createOrUpdateUserCalls[0], $expectedBacParams),
        )
      )
    );

    $userAfter = $this->ApiUser->findById($userId);
    $this->assertEqual(
      $expectedBacId,
      $userAfter['User']['bac_id'],
      "El bac id debe seguir sindo el mismo.".json_encode(array(
        'bac_id esperado' => $expectedBacId,
        'bac_id real' => $userAfter['User']['bac_id'],))
      );

  }

  function testCreateUserInBac() {
    $userId = 3;
    $user = $this->ApiUser->findById($userId);
    $userProfile = $this->ApiUserProfile->findByUserId($user['User']['id']);

    $this->ApiLogger->trace('params' , array(
      'user' => $user,
      'userProfile' => $userProfile,
      'last_name' => $userProfile['UserProfile']['last_name'],
      'first_name' => $userProfile['UserProfile']['first_name'],
    ));

    $expectedBacId = 9999;
    $expectedBacParams = array(
      'idUsuario'             => null,
      'idPortal'              => 18,
      'idUsuarioPortal'       => $user['User']['id'],
      'login'                 => $user['User']['username'],
      'email'                 => $user['User']['email'],
      'nombre'                => $userProfile['UserProfile']['first_name'],
      'apellido'              => $userProfile['UserProfile']['last_name'],
      'idPais'                => Configure::read('BAC.Argentina'),
      'idProvincia'           => Configure::read('BAC.CapitalFederal'),
      'idTipoUsuario'         => Configure::read('BAC.id_tipo_usuario_usuario'),
    );

    $apiBacMock = ClassRegistry::init('ApiBacMock');
    $apiBacMock->mockReturn = $expectedBacId;
    $this->ApiBacService->ApiBac = $apiBacMock;
    $this->ApiBacService->createBacUser($user);


    $this->assertEqual(
      1, count($apiBacMock->createOrUpdateUserCalls),
      "Debe llamar al crearOrUpdateUser una vez.".json_encode(
        array('llamadas' => count($apiBacMock->createOrUpdateUserCalls)))
      );

    ksort($expectedBacParams);
    ksort($apiBacMock->createOrUpdateUserCalls[0]);
    $this->assertEqual(
      $expectedBacParams,
      $apiBacMock->createOrUpdateUserCalls[0],
      "Debe llamar crearOrUpdateUser con los parametros esperados y solo ellos.".json_encode(
        array(
          'Esperados' => $expectedBacParams,
          'Reales' => $apiBacMock->createOrUpdateUserCalls[0],
          'Parametros que estan en esperados y son distintos o no estan en los reales' =>  array_diff_assoc(
            $expectedBacParams, $apiBacMock->createOrUpdateUserCalls[0]),
          'Parametros que estan en reales y son distintos o no estan en los esperados' =>  array_diff_assoc(
            $apiBacMock->createOrUpdateUserCalls[0], $expectedBacParams),
        )
      )
    );

    $userAfter = $this->ApiUser->findById($userId);
    $this->assertEqual(
      $expectedBacId,
      $userAfter['User']['bac_id'],
      "El bac id debe guardarse en el campo User.bac_id.".json_encode(array(
        'bac_id esperado' => $expectedBacId,
        'bac_id real' => $userAfter['User']['bac_id'],))
      );

  }

  function testUpdateCompanyInBac() {
    $company = $this->ApiCompany->findById(2);
    $companyUser = $this->ApiUser->findById($company['Company']['user_id']);

    $this->ApiLogger->trace('params' , array(
      'company' => $company,
      'companyUser' => $companyUser,
    ));


    $expectedBacId = $company['Company']['bac_id'];
    $expectedBacParams = array(
      'idUsuario'             => $company['Company']['bac_id'],
      'idPortal'              => 18,                                             // $idPortal,
      'idUsuarioPortal'       => $company['Company']['user_id'],                 // $idUsuario,
      'login'                 => $company['Company']['name'],                    // $company['Company']['name'],
      'email'                 => $companyUser['User']['email'],                  // $email,
      'idPais'                => Configure::read('BAC.Argentina'),
      'idProvincia'           => Configure::read('BAC.CapitalFederal'),
      'idTipoUsuario'         => Configure::read('BAC.id_tipo_usuario_empresa'),
      'razonSocial'           => $company['Company']['fiscal_name'],
      'domicilioComercial'    => $company['Company']['fiscal_address'],
      'localidadComercial'    => $company['Company']['fiscal_state_id'],
      'codigoPostalComercial' => $company['Company']['zip'],
      'telefonoComercial'     => $company['Company']['fiscal_phone'],
      'idCondicionIva'        => 1,
      'idTipoDocumento'       => 80,
      'nroDocumento'          => $company['Company']['fiscal_cuit'],
      'nroInscriptoIIBB'      => $company['Company']['fiscal_iibb'],
    );

    $apiBacMock = ClassRegistry::init('ApiBacMock');
    $apiBacMock->mockReturn = $expectedBacId;
    $this->ApiBacService->ApiBac = $apiBacMock;
    $this->ApiBacService->createBacCompanyUser($company);

    $this->assertEqual(
      1, count($apiBacMock->createOrUpdateUserCalls),
      "Debe llamar al crearOrUpdateUser una vez.".json_encode(
        array('llamadas' => count($apiBacMock->createOrUpdateUserCalls)))
      );

    ksort($expectedBacParams);
    ksort($apiBacMock->createOrUpdateUserCalls[0]);
    $this->assertEqual(
      $expectedBacParams,
      $apiBacMock->createOrUpdateUserCalls[0],
      "Debe llamar crearOrUpdateUser con los parametros esperados y solo ellos.".json_encode(
        array(
          'Esperados' => $expectedBacParams,
          'Reales' => $apiBacMock->createOrUpdateUserCalls[0],
          'Parametros que estan en esperados y son distintos o no estan en los reales' =>  array_diff_assoc(
            $expectedBacParams, $apiBacMock->createOrUpdateUserCalls[0]),
          'Parametros que estan en reales y son distintos o no estan en los esperados' =>  array_diff_assoc(
            $apiBacMock->createOrUpdateUserCalls[0], $expectedBacParams),
        )
      )
    );

    $companyAfter = $this->ApiCompany->findById('2');
    $this->assertEqual(
      $expectedBacId,
      $companyAfter['Company']['bac_id'],
      "El bac id debe seguir siendo el mismo.".json_encode(array(
        'bac_id esperado' => $expectedBacId,
        'bac_id real' => $companyAfter['Company']['bac_id'],))
      );

    $this->ApiLogger->trace('company', $company);
  }

  function testCreateCompanyInBac() {
    $companyId = 3;
    $company = $this->ApiCompany->findById($companyId);
    $companyUser = $this->ApiUser->findById($company['Company']['user_id']);

    $this->ApiLogger->trace('params' , array(
      'company' => $company,
      'companyUser' => $companyUser,
    ));

    $expectedBacId = 9999;
    $expectedBacParams = array(
      'idUsuario'             => null,
      'idPortal'              => 18,                                             // $idPortal,
      'idUsuarioPortal'       => $company['Company']['user_id'],                 // $idUsuario,
      'login'                 => $company['Company']['name'],                    // $company['Company']['name'],
      'email'                 => $companyUser['User']['email'],                  // $email,
      'idPais'                => Configure::read('BAC.Argentina'),
      'idProvincia'           => Configure::read('BAC.CapitalFederal'),
      'idTipoUsuario'         => Configure::read('BAC.id_tipo_usuario_empresa'),
      'razonSocial'           => $company['Company']['fiscal_name'],
      'domicilioComercial'    => $company['Company']['fiscal_address'],
      'localidadComercial'    => $company['Company']['fiscal_state_id'],
      'codigoPostalComercial' => $company['Company']['zip'],
      'telefonoComercial'     => $company['Company']['fiscal_phone'],
      'idCondicionIva'        => 1,
      'idTipoDocumento'       => 80,
      'nroDocumento'          => $company['Company']['fiscal_cuit'],
      'nroInscriptoIIBB'      => $company['Company']['fiscal_iibb'],
    );

    $apiBacMock = ClassRegistry::init('ApiBacMock');
    $apiBacMock->mockReturn = $expectedBacId;
    $this->ApiBacService->ApiBac = $apiBacMock;
    $this->ApiBacService->createBacCompanyUser($company);

    $this->assertEqual(
      1, count($apiBacMock->createOrUpdateUserCalls),
      "Debe llamar al crearOrUpdateUser una vez.".json_encode(
        array('llamadas' => count($apiBacMock->createOrUpdateUserCalls)))
      );

    ksort($expectedBacParams);
    ksort($apiBacMock->createOrUpdateUserCalls[0]);
    $this->assertEqual(
      $expectedBacParams,
      $apiBacMock->createOrUpdateUserCalls[0],
      "Debe llamar crearOrUpdateUser con los parametros esperados y solo ellos.".json_encode(
        array(
          'Esperados' => $expectedBacParams,
          'Reales' => $apiBacMock->createOrUpdateUserCalls[0],
          'Parametros que estan en esperados y son distintos o no estan en los reales' =>  array_diff_assoc(
            $expectedBacParams, $apiBacMock->createOrUpdateUserCalls[0]),
          'Parametros que estan en reales y son distintos o no estan en los esperados' =>  array_diff_assoc(
            $apiBacMock->createOrUpdateUserCalls[0], $expectedBacParams),
        )
      )
    );

    $companyAfter = $this->ApiCompany->findById($companyId);
    $this->assertEqual(
      $expectedBacId,
      $companyAfter['Company']['bac_id'],
      "El bac id debe guardarse en el campo Company.bac_id.".json_encode(array(
        'bac_id esperado' => $expectedBacId,
        'bac_id real' => $companyAfter['Company']['bac_id'],))
      );

    $this->ApiLogger->trace('company', $company);
  }

}

class ApiBacMock extends ApiBacComponent {

  public $createOrUpdateUserCalls = array();
  public $mockReturn = null;

  public function createOrUpdateUser($parameters) {
    $this->createOrUpdateUserCalls[] = $parameters;
    return $this->mockReturn;
  }
}
?>
