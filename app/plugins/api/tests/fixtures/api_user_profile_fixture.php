<?php

class ApiUserProfileFixture extends CakeTestFixture {
  var $name = 'UserProfile';

  var $fields = array(
    'id' => array('type' => 'integer', 'key' => 'primary'),
    'created' => 'datetime',
    'updated' => 'datetime',
    'user_id' => 'integer',
    'first_name' => 'string',
    'last_name' => 'string',
  );

  var $records = array(
    array(
      'id' => 1,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'user_id' => 3,
      'first_name' => 'Matilda',
      'last_name' => 'Compradora',
    ),
    array(
      'id' => 2,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
      'user_id' => 4,
      'first_name' => 'Matilda',
      'last_name' => 'CompradoraConBacId',
    ),
  );
}
?>
