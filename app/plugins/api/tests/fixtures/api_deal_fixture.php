<?php

class ApiDealFixture extends CakeTestFixture {
  var $name = 'Deal';

  var $fields = array(
    'id' => array('type' => 'integer', 'key' => 'primary'),
    'created' => 'datetime',
    'updated' => 'datetime',
    'start_date' => 'datetime',
    'is_tourism' => 'string',
  );

  var $records = array(
    array(
      'id' => 1,
      'created' => '2013-03-10 10:45:31',
      'updated' => '2013-03-10 10:45:31',
      'start_date' => '2013-03-10 10:45:31',
      'is_tourism' => '0',
    ),
  );

}
?>
