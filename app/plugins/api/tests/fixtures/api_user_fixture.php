<?php

class ApiUserFixture extends CakeTestFixture {
  var $name = 'User';

  var $fields = array(
    'id' => array('type' => 'integer', 'key' => 'primary'),
    'created' => 'datetime',
    'updated' => 'datetime',
    'email' => 'string',
    'username' => 'string',
    'bac_id' => 'integer',
    'bac_tourism_id' => 'integer',
    'bac_user_id' => 'integer',
  );

  var $records = array(
    array(
      'id' => 1,
      'email' => 'virginia_prueba@mailinator.com',
      'username' => 'virginia_prueba_test',
      'bac_user_id' => null,
      'bac_id' => null,
      'bac_tourism_id' => null,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
    array(
      'id'=> '2',
      'email' => 'matilda_company@mailinator.com',
      'username' => 'matilda_company',
      'bac_id'=> '0',
      'bac_tourism_id'=> '0',
      'bac_user_id'=> '0',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
    array(
      'id'=> '3',
      'email' => 'matilda_compradora@mailinator.com',
      'username' => 'matilda_compradora',
      'bac_id'=> '0',
      'bac_tourism_id'=> '0',
      'bac_user_id'=> '0',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
    array(
      'id'=> '4',
      'email' => 'matilda_compradoraConBacId@mailinator.com',
      'username' => 'matilda_compradora_con_bac_id',
      'bac_id'=> '88888',
      'bac_tourism_id'=> '0',
      'bac_user_id'=> '0',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
    array(
      'id'=> '381',
      'email'=> 'fernanda.cazala@47-street.com.ar',
      'username'=> 'Cuarentaysiete',
      'bac_id'=> '0',
      'bac_tourism_id'=> '0',
      'bac_user_id'=> '0',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
  );
}
?>
