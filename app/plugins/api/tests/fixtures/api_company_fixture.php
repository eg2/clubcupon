<?php

class ApiCompanyFixture extends CakeTestFixture {
  var $name = 'Company';

//  var $import = array(
//    'model' => 'api.ApiCompany',
//  );

  var $fields = array(
    'id'              => array('type' => 'integer', 'key' => 'primary'),
    'name'            => 'string',
    'zip'             => 'string',
    'fiscal_address'  => 'string',
    'fiscal_cond_iva' => 'string',
    'fiscal_cuit'     => 'string',
    'fiscal_iibb'     => 'string',
    'fiscal_name'     => 'string',
    'fiscal_phone'    => 'string',
    'fiscal_state_id' => 'string',
    'user_id'         => 'integer',
    'bac_user_id'     => 'integer',
    'bac_id'     => 'integer',
    'created'         => 'datetime',
    'updated'         => 'datetime',
  );

  var $records = array(
//    array(
//      'id' => 1,
//      'name'            => 'Compania Demo',
//      'zip'             => '1111',
//      'fiscal_address'  => 'Demo 111',
//      'fiscal_cond_iva' => 'ri',
//      'fiscal_cuit'     => '11111111111',
//      'fiscal_iibb'     => '111111111111',
//      'fiscal_name'     => 'Demo S.A',
//      'fiscal_phone'    => '11111111',
//      'fiscal_state_id' => 'string',
//      'user_id'         => '2',
//      'bac_user_id'     => null,
//      'created' => '2007-03-18 10:43:23',
//      'updated' => '2007-03-18 10:45:31',
//    ),
    array(
      'id'              => '2',
      'name'            => '47 Street',
      'zip'             => '1425',
      'fiscal_address'  => 'Julian Alvarez 761',
      'fiscal_cond_iva' => 'ri',
      'fiscal_cuit'     => '30707528458',
      'fiscal_iibb'     => '009010504072',
      'fiscal_name'     => 'Kemini S.A',
      'fiscal_phone'    => '48584747',
      'fiscal_state_id' => '5431',
      'user_id'         => '381',
      'bac_user_id'     => '110659',
      'bac_id'          => '1253531',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
    array(
      'id'              => '3',
      'name'            => 'Matilda Company',
      'zip'             => '1425',
      'fiscal_address'  => 'Matilda Calle 761',
      'fiscal_cond_iva' => 'ri',
      'fiscal_cuit'     => '30707528458',
      'fiscal_iibb'     => '009010504072',
      'fiscal_name'     => 'Matilda S.A',
      'fiscal_phone'    => '48584747',
      'fiscal_state_id' => '5431',
      'user_id'         => '2',
      'bac_user_id'     => '0',
      'bac_id'          => '0',
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),

  );

}
?>
