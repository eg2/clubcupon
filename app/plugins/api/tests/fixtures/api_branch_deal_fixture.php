<?php

class ApiBranchDealFixture extends CakeTestFixture {
  var $name = 'BranchesDeal';

  var $fields = array(
    'id' => array('type' => 'integer', 'key' => 'primary'),
    'created' => 'datetime',
    'updated' => 'datetime',
  );

  var $records = array(
    array(
      'id' => 1,
      'created' => '2007-03-18 10:43:23',
      'updated' => '2007-03-18 10:45:31',
    ),
  );

}
?>
