<?php

class BeaconDeviceDeal extends BeaconAppModel {
	public $name = 'BeaconDeviceDeal';
	public $alias = 'BeaconDeviceDeal';
	public $useTable = 'beacon_device_deals';
	
	public $belongsTo = array(
			'BeaconDevice' => array(
					'className' => 'beacon.BeaconDevice',
					'foreignKey' => 'beacon_device_id',
			),
			'ApiDeal' => array(
					'className' => 'api.ApiDeal',
					'foreignKey' => 'deal_id',
			),
	);
	public $validate = array(
			'since' => array (
					'rule2' => array (
							'rule' => '_isValidTimeRange',
							'message' => 'La Hora de Inicio debe ser menor que la Hora de Fin',
							'allowEmpty' => false
					),
			),
			
			'until' => array (
					'rule2' => array (
							'rule' => '_isValidTimeRange',
							'message' => 'La Hora de Fin debe ser mayor que la Hora de Inicio',
							'allowEmpty' => false
					),
			),
			'title' => array (
					'rule1' => array(
                    	'rule' => 'notempty',
                    	'message' => 'Debe ingresar un texto para la notificación' ,
                    	'allowEmpty' => false
                	)
			),
			
	);
	function _isValidTimeRange(){
		$t1=strtotime($this->data[$this->name]['since']);
		$t2=strtotime($this->data[$this->name]['until']);
		
		return ($t1<$t2);
	}
	public function __construct() {
		parent::__construct();
	}
	public function findAllByMac($mac,$conditions=null){
		
		$condition = array(
				'BeaconDevice.mac' => $mac,
				'ApiDeal.deal_status_id' => array(ConstDealStatus::Open,ConstDealStatus::Tipped),
				
		);
		if(isset($conditions) && !empty($conditions)){
			$condition= array_merge($condition,$conditions);
		}
		$contain = array(
				'BeaconDevice',
				'ApiDeal'
		);
		return $this->find('all', array(
				'fields' => array(
						'BeaconDeviceDeal.id',
						'BeaconDeviceDeal.title',
						'BeaconDeviceDeal.subtitle',
						'BeaconDeviceDeal.beacon_device_id',
						'BeaconDeviceDeal.deal_id',
						'BeaconDeviceDeal.since',
						'BeaconDeviceDeal.until',
						'BeaconDevice.uuid',
						'BeaconDevice.mac',
						'ApiDeal.name',
						'ApiDeal.company_id',
						'ApiDeal.only_price',
						'ApiDeal.discounted_price',
						'ApiDeal.discount_percentage',
						'ApiDeal.original_price',
				),
				'contain' => $contain,
				'conditions' => $condition,
				'order' => array(
						'ApiDeal.company_id' => 'DESC',
				),
				'recursive' => 0
		));
	}
	
	public function findAllForIos($beacon,$conditions=null){
	
		$condition = array(
				'BeaconDevice.uuid' => $beacon->uuid,
				'BeaconDevice.major' => $beacon->major,
				'BeaconDevice.minor' => $beacon->minor,
				'ApiDeal.deal_status_id' => array(ConstDealStatus::Open,ConstDealStatus::Tipped),
	
		);
		if(isset($conditions) && !empty($conditions)){
			$condition= array_merge($condition,$conditions);
		}
		$contain = array(
				'BeaconDevice',
				'ApiDeal'
		);
		return $this->find('all', array(
				'fields' => array(
						'BeaconDeviceDeal.id',
						'BeaconDeviceDeal.title',
						'BeaconDeviceDeal.subtitle',
						'BeaconDeviceDeal.beacon_device_id',
						'BeaconDeviceDeal.deal_id',
						'BeaconDeviceDeal.since',
						'BeaconDeviceDeal.until',
						'BeaconDevice.uuid',
						'BeaconDevice.mac',
						'ApiDeal.name',
						'ApiDeal.company_id',
						'ApiDeal.only_price',
						'ApiDeal.discounted_price',
						'ApiDeal.discount_percentage',
						'ApiDeal.original_price',
				),
				'contain' => $contain,
				'conditions' => $condition,
				'order' => array(
						'ApiDeal.company_id' => 'DESC',
				),
				'recursive' => 0
		));
	}
	
	
	function beforeSave() {
		 
		$this->data['BeaconDeviceDeal']['since']="2015-01-01 ".$this->data['BeaconDeviceDeal']['since'];
		$this->data['BeaconDeviceDeal']['until']="2015-01-01 ".$this->data['BeaconDeviceDeal']['until'];
		
		return true;
	}
}
