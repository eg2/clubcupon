<?php

class BeaconDevice extends BeaconAppModel {
public $name = 'BeaconDevice';
	public $alias = 'BeaconDevice';
	public $useTable = 'beacon_devices';
	//public $displayField = 'name';
	
	
	public $belongsTo = array(
			'Company' => array(
					'className' => 'api.ApiCompany',
					'foreignKey' => 'company_id',
			),
	);
	public $validate = array(
			'name' => array (
					'rule1' => array (
							'rule' => 'notempty',
							'message' => 'Requerido',
							'allowEmpty' => false
					),
			),
			'mac' => array (
					'rule1' => array (
							'rule' => 'notempty',
							'message' => 'Requerido',
							'allowEmpty' => false 
					),
					'rule2' => array (
							'rule' => '_isValidMacFormat',
							'message' => 'La MAC debe tener el sgt formato XX:XX:XX:XX:XX:XX',
							'allowEmpty' => false 
					),
					'rule3' => array (
							'rule' => '_isHexaMac',
							'message' => 'La MAC debe contener valores Hexadecimales.',
							'allowEmpty' => false 
					),
       		 ),
			'minor' => array (
					'rule1' => array (
							'rule' => 'notempty',
							'message' => 'Requerido',
							'allowEmpty' => false
					),
					'rule2' => array (
							'rule' => '_isValidMinorValue',
							'message' => 'El Minor es un valor entre 0 y 65,535.',
							'allowEmpty' => false
					),
			),
			'major' => array (
					'rule1' => array (
							'rule' => 'notempty',
							'message' => 'Requerido',
							'allowEmpty' => false
					),
					'rule2' => array (
							'rule' => '_isValidMajorValue',
							'message' => 'El Major es un valor entre 0 y 65,535.',
							'allowEmpty' => false
					),
			),
			'uuid' => array (
					'rule1' => array (
							'rule' => 'notempty',
							'message' => 'Requerido',
							'allowEmpty' => false
					),
					'rule2' => array (
							'rule' => '_isValidUuidFormat',
							'message' => 'El UUID debe tener el sgt formato: XX-XX-XX-XX-XX',
							'allowEmpty' => false
					),
					'rule3' => array (
							'rule' => '_isHexaUuid',
							'message' => 'El UUID debe contener valores hexadecimales',
							'allowEmpty' => false
					),
	
			),
		);
	function _isValidMacFormat(){
		$c = substr_count($this->data[$this->name]['mac'], ':');
		return ($c==5);
	}
	function _isHexaMac() {
		$new_mac = str_replace(':', '', $this->data[$this->name]['mac']);
		return ctype_xdigit($new_mac); 
	}
	function _isValidMinorValue() {
		return ($this->data[$this->name]['minor']>=0 && $this->data[$this->name]['minor']<=65535);
	}
	function _isValidMajorValue() {
		return ($this->data[$this->name]['major']>=0 && $this->data[$this->name]['major']<=65535);
	}
	function _isValidUuidFormat(){
		$c = substr_count($this->data[$this->name]['uuid'], '-');
		return ($c==4);
	}
	function _isHexaUuid() {
		$new_mac = str_replace('-', '', $this->data[$this->name]['uuid']);
		return ctype_xdigit($new_mac);
	}
	
	
	public function __construct() {
		
		parent::__construct();
	}
	
	
	
}
