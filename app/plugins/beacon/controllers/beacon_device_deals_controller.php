<?php

class BeaconDeviceDealsController extends BeaconAppController {

	var $name = 'BeaconDeviceDeals';
	/*
	public $actionsForAppSecure = array(
			'beacon_device_deals/getDealsByBeacons',
	);
	*/
	
	public $components = array(
		'BeaconService'
	);
	
	public function lastversionapp(){
		$this->layout = false;
		$versions = Configure::read('web.beacon.versions');
		$lastVersion=array();
		
		$lastKey = count($versions)-1;
				
		$lastVersion['version']=$versions[$lastKey]['version'];
		$lastVersion['isBlocker']=$versions[$lastKey]['isBlocker'];
		if ($this->_isJSON()) {
    		echo json_encode($lastVersion);
    	}
	}
	public function beacondeals() {
		$this->layout = false;
    	$data= '{"beacons":[
  		  {"uuid":"b9407f30-f5f8-466e-aff9-25556b57fe6d", "mac":"DF:73:95:B6:2F:95", "minor":"11", "major":"8555"},
    	  {"uuid":"2f234454-cf6d-4a0f-adf2-f4911ba9ffa6", "mac":"D5:2E:7D:9C:23:B0", "minor":"4", "major":"1114"}
    
		]}';
	 	$data=$this->params['form']['beacons'];
    	
    	$obj = json_decode($data);
    	$deals = $this->BeaconService->getDealsByBeacons($obj->beacons);
    	
    	if ($this->_isJSON()) {
    		echo json_encode($deals);
    	}
   }
   public function beacondeals_new() {
   	$this->layout = false;
   	$data= '{"beacons":[
  		  {"uuid":"b9407f30-f5f8-466e-aff9-25556b57fe6d", "mac":"DF:73:95:B6:2F:95", "minor":"11", "major":"8555"},
    	  {"uuid":"2f234454-cf6d-4a0f-adf2-f4911ba9ffa6", "mac":"D5:2E:7D:9C:23:B0", "minor":"4", "major":"1114"}
   
		]}';
   	$data=$this->params['form']['beacons'];
   	 
   	$obj = json_decode($data);
   	$isNew=true;
   	$deals = $this->BeaconService->getDealsByBeacons($obj->beacons,$isNew);
   	 
   	if ($this->_isJSON()) {
   		echo json_encode($deals);
   	}
   }
    public function beacondeals_ios() {
    	$this->layout = false;
    	
    	$data= '{"beacons":[
  		  {"uuid":"b9407f30-f5f8-466e-aff9-25556b57fe6d", "mac":"DF:73:95:B6:2F:95", "minor":"11", "major":"8555"},
    	  {"uuid":"2f234454-cf6d-4a0f-adf2-f4911ba9ffa6", "mac":"D5:2E:7D:9C:23:B0", "minor":"4", "major":"1114"}
    	
		]}';
    	$data=$this->params['form']['beacons'];
    	
    	$obj = json_decode($data);
    	$deals = $this->BeaconService->getDealsByBeaconsIos($obj->beacons);
    	if ($this->_isJSON()) {
    		echo json_encode($deals);
    	}
    }
    
	
}