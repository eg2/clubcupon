<?php
//App::import('Core', 'Component');
App::import('Component', 'beacon.BeaconBase');
App::import('Model', 'beacon.BeaconDeviceDeal');
App::import('Model', 'beacon.BeaconDevice');
App::import('Model', 'beacon.BeaconLog');

class BeaconServiceComponent extends BeaconBaseComponent {
	
	public $name = 'BeaconService';
	
	function __construct() {
		
		$this->BeaconDevice = & new BeaconDevice();
		
		$this->BeaconDeviceDeal = & new BeaconDeviceDeal();
		
		$this->BeaconLog = & new BeaconLog();
		
		parent::__construct();
	}
	public function getDealsByBeacons($beacons,$isNew=false) {
		$deals=array();
		
		$conditions=array();
		if(!$isNew){
			$conditions=array('TIME_TO_SEC(DATE_FORMAT(now(),"%H:%i:%s")) >= TIME_TO_SEC(DATE_FORMAT(BeaconDeviceDeal.since,"%H:%i:%s")) AND TIME_TO_SEC(DATE_FORMAT(now(),"%H:%i:%s")) <= TIME_TO_SEC(DATE_FORMAT(BeaconDeviceDeal.until,"%H:%i:%s"))');
		}
		foreach($beacons as $beacon){
			$dealsByMac=array();
			$dealsByMac = $this->BeaconDeviceDeal->findAllByMac($beacon->mac,$conditions);
			if(isset($dealsByMac) && !empty($dealsByMac)){
				$deals[]=$this->getFormattedArray($dealsByMac,$isNew);
				$this->registerLog($beacon,$dealsByMac);
			}else{
				$this->registerLog($beacon,null);
			}
		}
		
		return $deals;
	}
	
	private function getFormattedArray($deals,$isNew=false){
		
		if(!$isNew){
			return $deals;
		}
		
		$result=array();
		$parent=array();
		$dealsByBeacon=array();
		
		foreach ($deals as $deal){
			if(empty($dealsByBeacon)){
				$parent=$deal['BeaconDevice'];
			}
			$dealsByBeacon[]=$this->getDealWithExtras($deal);
		}
		$result["BeaconDevice"]=$parent;
		$result["Deals"]=$dealsByBeacon;
		
		return $result;
	}
	
	private function getDealWithExtras($deal){
		$dealWithExtras=$deal['ApiDeal'];
		$dealWithExtras['title']=$deal['BeaconDeviceDeal']['title'];
		$dealWithExtras['since']=$deal['BeaconDeviceDeal']['since'];
		$dealWithExtras['until']=$deal['BeaconDeviceDeal']['until'];
		return $dealWithExtras;
	}
	
	private function registerLog($beacon,$dealsByMac){
		$deals=array();
		$this->BeaconLog = new BeaconLog();
		$this->BeaconLog->set('mac', $beacon->mac);
		$this->BeaconLog->set('token', $beacon->token);
		$this->BeaconLog->set('so', 1);
		
		foreach($dealsByMac as $deal){
			$deals[]=$deal["BeaconDeviceDeal"];
		}
		$this->BeaconLog->set('response', json_encode($deals));
		$this->BeaconLog->save();
	}
	public function getDealsByBeaconsIos($beacons) {
		$deals=array();
		$conditions=array('TIME_TO_SEC(DATE_FORMAT(now(),"%H:%i:%s")) >= TIME_TO_SEC(DATE_FORMAT(BeaconDeviceDeal.since,"%H:%i:%s")) AND TIME_TO_SEC(DATE_FORMAT(now(),"%H:%i:%s")) <= TIME_TO_SEC(DATE_FORMAT(BeaconDeviceDeal.until,"%H:%i:%s"))');
		foreach($beacons as $beacon){
			$dealsForIos=array();
				
			$dealsForIos = $this->BeaconDeviceDeal->findAllForIos($beacon,null);
				
			if(isset($dealsForIos) && !empty($dealsForIos)){
				$deals[]=$dealsForIos;
				$this->registerLogIos($beacon,$dealsForIos);
			}else{
				$this->registerLogIos($beacon,null);
			}
		}
	
		return $deals;
	}
	private function registerLogIos($beacon,$dealsForIos){
		$deals=array();
		$this->BeaconLog = new BeaconLog();
		$this->BeaconLog->set('mac', $beacon->mac);
		$this->BeaconLog->set('token', $beacon->token);
		$this->BeaconLog->set('so', 0);
	
		$this->BeaconLog->set('uuid', $beacon->uuid);  //para Ios
		$this->BeaconLog->set('minor', $beacon->minor);//para Ios
		$this->BeaconLog->set('major', $beacon->major);//para Ios
	
		foreach($dealsForIos as $deal){
			$deals[]=$deal["BeaconDeviceDeal"];
		}
		$this->BeaconLog->set('response', json_encode($deals));
		$this->BeaconLog->save();
	}
	
	
	
	
	
}