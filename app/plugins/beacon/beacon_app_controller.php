<?php

class BeaconAppController extends AppController {
	function __construct() {
		parent::__construct();
	}
	public function beforeRender() {
		
		parent::beforeRender();
		if ($this->_isJSON() || $this->_isXML()) {
			Configure::write('debug', 0);
			$this->disableCache();
		}
	}
	protected function _isJSON() {
		return $this->RequestHandler->ext == 'json';
	}
	
	protected function _isXML() {
		return $this->RequestHandler->ext == 'xml';
	}
	public function beforeFilter() {
		/*
		if ($this->_isJSON() && !$this->RequestHandler->isGet()) {
        if (empty($this->data) && !empty($_POST)) {
          $this->data[$this->modelClass] = $_POST;
        }
        if (isset($_POST['data'])) {
            $this->params['post'] = json_decode($_POST['data']);
        }
      }
		*/
		 parent::beforeFilter();
	}
}
