<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'payment.Assertion');


abstract class PaymentAppShell extends Shell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->ApiLogger = new ApiLogger(get_class($this), 'payment');
    $this->ApiShellUtils = new ApiShellUtilsComponent();
  }

  function setUp() {
  }


  function setUpMain() {
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
    $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
    $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
    $this->params = $this->ApiShellUtils->getOptions($this->args);
    $this->ApiLogger->echoConsoleWithLog(
      "Iniciando Proceso ".__class__, array(
        'args' => $this->args,
        'params' => $this->params,
        'configurationsOverride' => array_keys($this->configurationsOverride),
        'configurationsOverrideValues' => $this->configurationsOverride,
        'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
        'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues,
      ));

    $this->command = null;
	if(!empty($this->params['c'])){
		$this->command = $this->params['c'];
	}
    
    $this->populate = (isset($this->params['populate'])?true:false);
    if($this->populate) {
      $this->setUpDb($this->params);
    }

    $this->ApiLogger->notice(
      'Run Command.', array(
        'command' => $this->command
      ));

  }

  public function tearDownMain() {

    if($this->populate) {
      $this->tearDownDb($this->params);
    }

  }


  public function setUpDb($params) {
    $this->ApiLogger->echoConsoleWithLog(__METHOD__);
    return true;
  }

  public function tearDownDb($params) {
    $this->ApiLogger->echoConsoleWithLog(__METHOD__);
    return true;
  }

}
