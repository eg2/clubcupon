<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('', 'payment.PaymentTestShell');
App::import('Component', 'payment.PaymentPaymentService');
App::import('Model', 'payment.PaymentDealUser');
App::import('Model', 'payment.PaymentDealExternal');
App::import('Model', 'payment.PaymentUser');
App::import('Model', 'payment.PaymentDeal');
App::import('Component', 'payment.PaymentBacMock');
App::import('Component', 'api.ApiGiftPinService');
App::import('Model', 'api.ApiGiftPin');
App::import('Component', 'payment.ApiGiftPinServiceMock');

class PaymentPaymentServiceTestShell extends PaymentTestShell {
 
  function __construct(&$dispatch) {
    parent::__construct($dispatch);
  }
   
  public function setUp($params) {
    parent::setUp();
    $this->PaymentPaymentService = new PaymentPaymentServiceComponent();
    $this->PaymentDealExternal = new PaymentDealExternal();
    $this->PaymentDealUser = new PaymentDealUser();
    $this->PaymentDeal = new PaymentDeal();
    $this->PaymentUser = new PaymentUser();
    $this->PaymentBacMock = new PaymentBacMockComponent();
    $this->ApiGiftPin = new ApiGiftPinServiceComponent();
    $this->GiftPin = new ApiGiftPin();
    $this->ApiGiftPinMock = new ApiGiftPinServiceMockComponent();
  }
 
  public function testBuyAccredited() {
    $testCode = 'matildaSimpleBuyAccredited'; 
    $matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
    $dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
	$userBefore = $this->PaymentUser->findById(
		$dealExternalBefore['PaymentDealExternal']['user_id']
	);
   
    $paymentUpdateDetail = json_decode(
      '{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
      , true);

    $this->dataPaymentBacMock($dealExternalBefore, $testCode);
   
    $this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
	 
    $userAfter = $this->PaymentUser->findById(
		$dealExternalBefore['PaymentDealExternal']['user_id']
	);
    
    $userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
    $userExpected['available_balance_amount'] = 
    		$userBefore['PaymentUser']['available_balance_amount'] 
    		+ $userExpected['_delta_available_balance_amount'];
     
    $this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');

    $dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
    $this->assertDealUserByDealExternal($dealExternalAfter, $testCode);
    
    $expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
    $this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');

  }

  public function testBuyCancel() {
  	$testCode = 'matildaSimpleBuyCancel';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
  	
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	 
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	 
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  	 
  }
 
  public function testBuyWithStockAccredited() {  
	  	$testCode = 'matildaBuyWithStockAccredited';
	  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
	  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
	  	$userBefore = $this->PaymentUser->findById(
	  			$dealExternalBefore['PaymentDealExternal']['user_id']
	  	);
	  	
	    $productBefore = $this->PaymentDeal->getProductFromDeal($dealExternalBefore['PaymentDealExternal']['deal_id']);
	    
	  	$paymentUpdateDetail = json_decode(
	  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
	      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
	  			, true);
	  	
	  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
	  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
	  	
	  	$userAfter = $this->PaymentUser->findById(
	  			$dealExternalBefore['PaymentDealExternal']['user_id']
	  	);
	  	
	  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
	  	$userExpected['available_balance_amount'] =
	  	$userBefore['PaymentUser']['available_balance_amount']
	  	+ $userExpected['_delta_available_balance_amount'];
	  	 
	  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
	  	
	  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
	  	$this->assertDealUserByDealExternal($dealExternalAfter, $testCode);
	  	 
	  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
	  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
 
	  	$productAfter = $this->PaymentDeal->getProductFromDeal($dealExternalAfter['PaymentDealExternal']['deal_id']);
	  	$this->assertProductsDecresStock($productBefore, $productAfter,$dealExternalAfter['PaymentDealExternal']['quantity']);
  }
  //*******************
  public function testBuyWithStockCancel() {
  	$testCode = 'matildaBuyWithStockCancel';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	 
  	$productBefore = $this->PaymentDeal->getProductFromDeal($dealExternalBefore['PaymentDealExternal']['deal_id']);
  	
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	 
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
  	 
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	 
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	 
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	 
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  	
  	$productAfter = $this->PaymentDeal->getProductFromDeal($dealExternalAfter['PaymentDealExternal']['deal_id']);
  	$this->assertProductsDecresStock($productBefore, $productAfter, 0);
  }
  
  
  
  public function testBuyCancelForException() {
  	$testCode = 'matildaSimpleBuyCancelForException';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	 
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	 
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode,true);
  	 
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	 
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	 
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	 
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  	 
  }
  
  public function testBuyWithStockCancelForException() {
  	
  	$testCode = 'matildaBuyWithStockCancelForException';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$productBefore = $this->PaymentDeal->getProductFromDeal($dealExternalBefore['PaymentDealExternal']['deal_id']);
  	 
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode, true);
  	
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	 
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  	 
  	$productAfter = $this->PaymentDeal->getProductFromDeal($dealExternalAfter['PaymentDealExternal']['deal_id']);
  	$this->assertProductsDecresStock($productBefore, $productAfter, 0);
  	
  }
  
  
  public function testBuyWithDiscountMercadoPagoAccredited() {
  	$testCode = 'matildaBuyWithDiscountMercadoPagoAccredited';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	 
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$this->assertDealUserByDealExternal($dealExternalAfter, $testCode);
  	
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  	
  }
  
  public function testBuyConbinedWithNpsAccredited() {
  	$testCode = 'matildaBuyConbinedWithNpsAccredited';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	 
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$this->assertDealUserByDealExternal($dealExternalAfter, $testCode);
  	
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  }
  
  
  public function testBuyGiftCardWithWalletAndGatewayAccredited() {
  	$testCode = 'matildaBuyGiftCardWithWalletAndGatewayAccredited';
  	$matildaSimpleBuyDealExternalId = $this->data['PaymentDealExternal'][$testCode.'DealExternal']['id'];
  	
  	$dealExternalBefore = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$userBefore = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$valor =$this->ApiGiftPin->block($dealExternalBefore['PaymentDealExternal']['gift_pin_code']);
  	
  	$dealExternalBefore = $this->PaymentDealExternal->updateGiftPinId($dealExternalBefore,$valor);
  
  	$paymentUpdateDetail = json_decode(
  			'{"ID_PAGO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['id'].  '",
      "ID_USUARIO_PORTAL":"'.$dealExternalBefore['PaymentDealExternal']['user_id'].'"}'
  			, true);
  	 
  	$this->dataPaymentBacMock($dealExternalBefore, $testCode);
  	$this->dataApiGiftPinMock($testCode);
  	
  	$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
  	
  	$userAfter = $this->PaymentUser->findById(
  			$dealExternalBefore['PaymentDealExternal']['user_id']
  	);
  	
  	$userExpected = $this->expectedData['PaymentUser'][$testCode.'UserExpected'];
  	$userExpected['available_balance_amount'] =
  	$userBefore['PaymentUser']['available_balance_amount']
  	+ $userExpected['_delta_available_balance_amount'];
  	 
  	$this->assertEntityWithExpected($userExpected,$userAfter['PaymentUser'],'User');
  	
  	$dealExternalAfter = $this->PaymentDealExternal->findById($matildaSimpleBuyDealExternalId);
  	$this->assertDealUserByDealExternal($dealExternalAfter, $testCode);
  	
  	$expectedDealExternal = $this->expectedData['PaymentDealExternal'][$testCode.'DealExternalExpected'];
  	$this->assertEntityWithExpected($expectedDealExternal, $dealExternalAfter['PaymentDealExternal'], 'DealExternal');
  }
  
  public function xtestBuyWithDiscountPinAccredited() {
  	 
  }
 
  public function xtestBuyWithStockPinAccredited() {
  }
    
  public function xtestBuyAccreditedWithLimitOfDealCoupons() {
  }
 /* 
  public function xtestBuyWithStockPinCancel() {
  }
  
  public function xtestBuyWithDiscountMercadoPagoCancel() {
  }
  
  public function xtestBuyWithWalletCancelForException() {
  }
  
  public function xtestBuyWithWalletCancel() {
  }
  
  public function xtestBuyWithDiscountPinCancel() {
  }*/
  
  //**********************************************************METHODS FOR TESTS ***********************************
  public function assertDealUserByDealExternal($dealExternal, $testCode){
  	$dealUsers = $this->PaymentDealUser->findByDealExternal($dealExternal);
  	$sucess = $this->Assert->assertEquals(
  			count($dealUsers),
  			$dealExternal['PaymentDealExternal']['quantity'],
  			'cantidad de cupones'
  	);
  	$i=0;
  	foreach ($dealUsers as $dealUser){
  		$i++;
  		$expectedFirstDealUser = $this->expectedData['PaymentDealUser'][$testCode.'DealUserExpected'];
  		$sucess = $sucess && $this->assertEntityWithExpected(
  				$expectedFirstDealUser, 
  				$dealUser['PaymentDealUser'], 
  				'DealUserID:'.$i.'['.$dealUser['PaymentDealUser']['id'].']'
		);
  	}
  	return $sucess;
  }
  
  public function dataPaymentBacMock($dealExternal, $testCode, $exception = false){
  	if(!$exception){
  		$this->PaymentBacMock->responseException = false;
  		$PaymentBacMockResponse = $this->sourceData['_'.$testCode.'PaymentBacMockResponse'];
  		$this->PaymentBacMock->dataBacMockResponse = json_encode($PaymentBacMockResponse);
  	}else {
  		$this->PaymentBacMock->responseException = true;
  	}
  	$this->PaymentPaymentService->PaymentBac = $this->PaymentBacMock;
  }
  

  public function assertProductsDecresStock($productBefore, $productAfter, $deltaQuantity){
  	$before = $productBefore['ProductProduct']['stock'] - $deltaQuantity;
  	if($before<0){
  		$before = 0;
  	}
  	return $this->Assert->assertEquals(
  			$before,
  			$productAfter['ProductProduct']['stock'],
  			'stock'
  	 );
  }
  function dataApiGiftPinMock($testCode){
  	$this->ApiGiftPinMock->bacPin = $this->sourceData['_'.$testCode.'ApiGiftPinMock'];
  	$this->PaymentPaymentService->ApiGiftPin = $this->ApiGiftPinMock; 
  	
  }
/*
 function main() {
    $this->setUpMain();
    $this->setUp($this->params);
    switch ($this->command) {
    case "test":
      $this->invokeAll();
      break;

    default:
      $this->ApiLogger->echoConsoleWithLog("ERROR: Debe especificar un comando");
      $this->ApiLogger->error("Debe especificar un comando.");
    }
  } 
*/
 /* public function invokeAll() {
    $class_methods = get_class_methods($this);
    $this->summaryErrorsByTest = array();
    $this->successTests = array();
    $this->failTests = array();
    $this->checkedTests = array();
    $this->ignoreTests = array(); 

    $this->currentTest = 'Buy Accredited';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyAccredited();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'Buy Cancel';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyCancel();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }



    $this->currentTest = 'Buy Cancel For Exception';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyCancelForException();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'Buy Conbined With Nps Accredited';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyConbinedWithNpsAccredited();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'BuyGiftCardAccredited';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyGiftCardAccredited();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }



    $this->currentTest = 'BuyWithDiscountMercadoPagoAccredited';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyWithDiscountMercadoPagoAccredited();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'BuyWithStockAccredited';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyWithStockAccredited();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'BuyWithStockCancel';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyWithStockCancel();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }


    $this->currentTest = 'BuyWithStockCancelForException';
    $this->ApiLogger->echoConsole("%K## %wRunning test%n %W". ucwords($this->currentTest) . "%w ...begin...  %n");
    $this->testBuyWithStockCancelForException();
    $this->summaryErrorsByTest[$this->currentTest] = array();
    $this->ApiLogger->echoConsole("%K## %wTest%n %W". ucwords($this->currentTest) . "%w ...end... %n");
    if(count($this->summaryErrorsByTest[$this->currentTest])==0) {
      $this->successTests[] = $this->currentTest;
    } else {
      $this->failTests[] = $this->currentTest;
    }

    $this->printSummary();
  }*/

}
