<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('', 'payment.PaymentAppShell');
App::import('Component', 'payment.PaymentPaymentService');

class PaymentProcessShell extends PaymentAppShell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->PaymentPaymentService = new PaymentPaymentServiceComponent();
  }

 
  function main() {
    	$this->setUpMain();

    	$this->ApiLogger->notice(
      			'Run Command.', array(
        		'command' => $this->command
      	));
		if(!empty($this->params['data'])){
			$data = $this->params['data'];
		}
    	switch ($this->command) {
    		case "update_payment":
      				$paymentUpdateDetail = json_decode($data, true);
      				$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
      		break;
      		case "update_payment_by_id":
      			$paymentUpdateDetail = array();//json_decode($data, true);
      			if(!empty($this->params['id'])){
      				$paymentUpdateDetail['ID_PAGO_PORTAL'] = $this->params['id'];
      				$paymentUpdateDetail['ID_USUARIO_PORTAL'] = $this->params['user_id'];
      				$this->PaymentPaymentService->updatePayment($paymentUpdateDetail);
      			}else{
      				$this->man();
      			}
       		break;
			default:
     				$this->man();
    	}
    	
    	$this->tearDownMain();
  }
  
  function man(){
  	$this->ApiLogger->echoConsoleWithLog("ERROR: Debe especificar un comando:  c:update_payment, c:update_payment_by_id id:xxxxxx ");
  	$this->ApiLogger->error("Debe especificar un comando.");
  }

}
