<?php
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('', 'payment.PaymentTestShell');
App::import('Component', 'payment.FixtureManager');
App::import('Model', 'payment.PaymentDealExternal');
App::import('Component', 'payment.PaymentPaymentService');


class PaymentDealExternalTestShell extends PaymentTestShell {

  function __construct(&$dispatch) {
    parent::__construct($dispatch);
    $this->PaymentPaymentService = new PaymentPaymentServiceComponent();
    $this->PaymentDealExternal = new PaymentDealExternal();
  }

  public function setUp($params) {
    parent::setUp();
    $this->data = $this->FixtureManager->initializeData(dirname(realpath(__FILE__)).'/../../fixtures/data.yaml');
    $this->expectedData = $this->FixtureManager->expectedEntities(dirname(realpath(__FILE__)).'/../../fixtures/data.yaml', $this->data);
    $this->sourceData = $this->FixtureManager->readData(dirname(realpath(__FILE__)).'/../../fixtures/data.yaml');
  }
  
  
  public function testFindById() { 
    $firstMatildaBuyDealExternalId = $this->data['PaymentDealExternal']['matildaFirstBuyDealExternal']['id'];
    $dealExternal = $this->PaymentDealExternal->findById($firstMatildaBuyDealExternalId);
    // $this->echoConsole("Deal External Encontrado:");
    // $this->ApiLogger->printTableForEntity($dealExternal['PaymentDealExternal'], null, null, '%w', '%w');

    
    $dealExternal = $this->PaymentDealExternal->findById($firstMatildaBuyDealExternalId);
    
    $expectedDealExternal = $this->expectedData['PaymentDealExternal']['matildaFirstBuyDealExternalExpected'];
//     $this->echoConsole("Deal External Esperado:");
//     $this->ApiLogger->printTableForEntity($expectedDealExternal, null, null, '%y', '%y');
    $entityWithExpectedValues = $this->Assert->mergeWithExpected($dealExternal['PaymentDealExternal'], $expectedDealExternal);
    $diffs = $this->Assert->compare($expectedDealExternal, $dealExternal['PaymentDealExternal']);
    if(empty($diffs)) {
      $this->echoOkConsole('%g Se encontro el deal external esperado.%n');
      $this->ApiLogger->printTableForEntityWithExpected($entityWithExpectedValues, null, null, '%w', '%R');
    } else {
      $this->echoErrorConsole('Se encontraron el deal external esperado diferente.');
      $this->ApiLogger->printTableForEntityWithExpected($entityWithExpectedValues, null, null, '%w', '%R');
    }
  }
 /* 
  public function testUserStatus() {
  	$user = array(
  			'id' => $this->sourceData['_matilda_id']['id'],
  			'available_balance_amount' => 60, 
  			'wallet_blocked' => 1,
  	);
  	$userExpected = $this->expectedData['PaymentUser']['matildaUserExpected'];
  	$entityWithExpectedValues = $this->Assert->mergeWithExpected($user, $userExpected);
  	$this->ApiLogger->printTableForEntityWithExpected($entityWithExpectedValues, null, null, '%w', '%R');
  	 
  	 
  	
  	$this->echoOKConsole("Calculo Correcto");
  }*/


}
