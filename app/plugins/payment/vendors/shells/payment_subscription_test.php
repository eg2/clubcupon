<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL&~E_DEPRECATED&~E_STRICT&~E_WARNING);
App::import('', 'payment.PaymentAppShell');
App::import('Component', 'web.WebSubscriptionMailValidatorService');

class PaymentSubscriptionTestShell extends PaymentAppShell {

	private $WebSubscriptionMailValidatorService;
	
  	function __construct(&$dispatch) {
    	parent::__construct($dispatch);
    	$this->WebSubscriptionMailValidatorService = new WebSubscriptionMailValidatorServiceComponent();
   		
  	}

 
  function main() {
    	$this->setUpMain();

    	$this->ApiLogger->notice(
      			'Run Command.', array(
        		'command' => $this->command
      	));

		switch ($this->command) {
    		case "test_domain_mail":
      			if(!empty($this->params['mail'])){
      				$email = $this->params['mail'];
      				if($this->WebSubscriptionMailValidatorService->verifyMailDomain($email)){
      					$this->ApiLogger->echoConsole('El dominio perteneciente al mail: '.$email." es valido\n");	
      				}else{
      					$this->ApiLogger->echoConsole('El dominio perteneciente al mail: '.$email." No es valido\n");
      				}
      			}else{
      				$this->man();
      			}
       		break;
			default:
     				$this->man();
    	}
    	
    	$this->tearDownMain();
  }
  
  function man(){
  	$this->ApiLogger->echoConsoleWithLog("ERROR: Debe especificar un comando: c:test_domain_mail mail:xxxx@xxxx.xxx ");
  	$this->ApiLogger->error("Debe especificar un comando.");
  }

}
