<?php

App::import('Component', 'api.ApiLogger');
App::import('Component', 'payment.PaymentPaymentService');

class PaymentPaymentsController extends AppController {

    public $name = 'PaymentPaymentsController';
    private $ApiLogger;
    private $PaymentService;
    public $components = array(
    );
    public $actionsForAppSecure = array(
    );

    function __construct() {
        parent::__construct();
        $this->ApiLogger = new ApiLogger(get_class($this), 'payment');
        $this->PaymentService = new PaymentPaymentServiceComponent();
    }

    function beforeFilter() {
        $this->Auth->allow('updatePayment', 'testPost');
    }

    function getParamsFromBac() {
        $detail = array();
        $detail['ID_PAGO_PORTAL'] = isset($_POST['ID_PAGO_PORTAL']) ? $_POST['ID_PAGO_PORTAL'] : null;
        $detail['ID_USUARIO_PORTAL'] = isset($_POST['ID_USUARIO_PORTAL']) ? $_POST['ID_USUARIO_PORTAL'] : null;
        return $detail;
    }

    function updatePayment() {
        $this->ApiLogger->notice("TEST Data From Bac this->params:", $this->params);
        $this->ApiLogger->notice("TEST Data From Bac POST:", $_POST);
        $detail = $this->getParamsFromBac();
        $this->layout = false;
        $this->autoRender = false;
        if (!is_null($detail['ID_PAGO_PORTAL'])) {
            $this->ApiLogger->notice("Iniciando proceso update Payment", $detail);
            $this->PaymentService->updatePayment($detail);
            $this->ApiLogger->notice("Proceso Finalizado");
        } else {
            $this->ApiLogger->error("No se recibieron datos de Bac");
        }
    }

    function testPost() {
        $this->layout = false;
        $this->autoRender = false;
        $this->ApiLogger->notice("TEST Data From Bac this->params:", $this->params);
        $this->ApiLogger->notice("TEST Data From Bac POST:", $_POST);
    }

}
