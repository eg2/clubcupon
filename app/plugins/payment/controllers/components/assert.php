<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class AssertComponent extends ApiBaseComponent {

  public $name = 'Assert';

  function __construct($test) {
    parent::__construct();
    $this->test = $test;
  }

  public function mergeWithExpected($real, $expected) {
    $result = array();
    foreach($expected as $key => $expectedValue) {
        $result[$key] = array();
        $result[$key]['expected'] = isset($expected[$key])?$expected[$key]:''; 
        $result[$key]['real'] = isset($real[$key])?$real[$key]:''; 
    }
    return $result;
  }

  public function compare($expected, $real) {
    $diffs = array();
    foreach($expected as $key => $expectedValue) {
      if($this->isComparableField($key) && $real[$key] != $expectedValue) {
        $difss[$key] = array();
        $diffs[$key]['expected'] = $expectedValue;
        $diffs[$key]['real'] = $real[$key];
      }
    } 
    return $diffs;
  }
  
  
  public function isComparableField($key) {
  	return !($this->startWith($key, '_'));	
  }
  
  public function startWith($haystack, $needle) {
  	return $needle === "" || strpos($haystack, $needle) === 0;
  }
  
  public function assertEquals($expected, $real, $message) {
  	if ($expected == $real) {
  		$this->test->echoOkConsole("$message es igual al esperado [real:$real, esperada:$expected]");
  		return true;
  	} else {
  		$this->test->echoErrorConsole("$message es distinto al esperado [real:$real, esperada:$expected]");
  		return false;
  	}
  }



}


