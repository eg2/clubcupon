<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Vendor', 'spyc' . DS . 'Spyc');
require('Spyc.php');

class FixtureManagerComponent extends ApiBaseComponent {

  public $name = 'FixtureManager';

  function __construct() {
    parent::__construct();
  }
  
  public function readDataString($fixtureFileName, $realEntitiesByType) {
  	$data = Spyc::YAMLLoadString($fixtureFileName);
  	return $data;
  }
  
  public function readData($fixtureFileName, $realEntitiesByType) {
  	$data = Spyc::YAMLLoad($fixtureFileName);
  	return $data;
  }

  public function expectedEntitiesString($fixtureFileName, $realEntitiesByType) {
    $data = Spyc::YAMLLoadString($fixtureFileName);
   // $this->ApiLogger->echoConsoleWithLog(__METHOD__, array(
   //   'fixtureFileName' => $fixtureFileName,
   //   'data' =>  $data
   // ));
    $entitiesByType = array();
    $entitiesByCode = array();

    foreach ($data as $entityKey => $entityValue) {
      if ($this->isExpectedEntity($entityKey)) {
        $entityType = $this->getEntityType($entityKey);
        $entityCode = $this->getEntityCode($entityKey);
        if(!empty($realEntitiesByType[$entityType][str_replace('Expected', '', $entityCode)])) {
        $entityValue['id'] = $realEntitiesByType[$entityType][str_replace('Expected', '', $entityCode)]['id'];
        }
        $entityValue = $this->replaceEntityByIds($entityValue, $this->compactMultilevelEntityList($realEntitiesByType));
        $entitiesByCode[$entityCode] = $entityValue;
        $entitiesByType[$entityType][$entityCode] = $entityValue;
      }
    }

    $this->ApiLogger->printTablesForEntityLists($entitiesByType);

    return $entitiesByType;


  }

  public function expectedEntities($fixtureFileName, $realEntitiesByType) {
    $data = Spyc::YAMLLoad($fixtureFileName);
   // $this->ApiLogger->echoConsoleWithLog(__METHOD__, array(
   //   'fixtureFileName' => $fixtureFileName,
   //   'data' =>  $data
   // ));
    $entitiesByType = array();
    $entitiesByCode = array();

    foreach ($data as $entityKey => $entityValue) {
      if ($this->isExpectedEntity($entityKey)) {
        $entityType = $this->getEntityType($entityKey);
        $entityCode = $this->getEntityCode($entityKey);
        if(!empty($realEntitiesByType[$entityType][str_replace('Expected', '', $entityCode)])) {
        $entityValue['id'] = $realEntitiesByType[$entityType][str_replace('Expected', '', $entityCode)]['id'];
        }
        $entityValue = $this->replaceEntityByIds($entityValue, $this->compactMultilevelEntityList($realEntitiesByType));
        $entitiesByCode[$entityCode] = $entityValue;
        $entitiesByType[$entityType][$entityCode] = $entityValue;
      }
    }

    $this->ApiLogger->printTablesForEntityLists($entitiesByType);

    return $entitiesByType;


  }

  public function compactMultilevelEntityList($realEntitiesByType){
  	$compactList = array();
  	foreach ($realEntitiesByType as $entities){
  	 	$compactList = array_merge($compactList,$entities);
  	 }
  	 return $compactList;
  }
  
  public function initializeDataString($content) {
    $data = Spyc::YAMLLoadString($content);
  //  $this->ApiLogger->echoConsoleWithLog(__METHOD__, array(
  //    'fixtureFileName' => $fixtureFileName,
  //    'data' =>  $data
  //  ));
    $entitiesByType = array();
    $entitiesByCode = array();

    $this->importModels($data);

    foreach ($data as $entityKey => $entityValue) {
      if ($this->isEntity($entityKey)) {
        $entityType = $this->getEntityType($entityKey);
        $entityCode = $this->getEntityCode($entityKey);
        $entity = $this->saveEntity($entityType, $entityCode, $entityValue, $entitiesByCode);
        $entitiesByCode[$entityCode] = $entity;
        $entitiesByType[$entityType][$entityCode] = $entity;
      }
    }

    $this->ApiLogger->printTablesForEntityLists($entitiesByType);

    return $entitiesByType;
  }


  public function initializeData($fixtureFileName) {
    $data = Spyc::YAMLLoad($fixtureFileName);
  //  $this->ApiLogger->echoConsoleWithLog(__METHOD__, array(
  //    'fixtureFileName' => $fixtureFileName,
  //    'data' =>  $data
  //  ));
    $entitiesByType = array();
    $entitiesByCode = array();

    $this->importModels($data);

    foreach ($data as $entityKey => $entityValue) {
      if ($this->isEntity($entityKey)) {
        $entityType = $this->getEntityType($entityKey);
        $entityCode = $this->getEntityCode($entityKey);
        $entity = $this->saveEntity($entityType, $entityCode, $entityValue, $entitiesByCode);
        $entitiesByCode[$entityCode] = $entity;
        $entitiesByType[$entityType][$entityCode] = $entity;
      }
    }

    $this->ApiLogger->printTablesForEntityLists($entitiesByType);

    return $entitiesByType;
  }

  public function color($text) {
    return $this->ConsoleColor->convert($text);
  }

  private function saveEntity($entityType, $entityCode, $entityValue, $entitiesByCode) {
    $entityValueWithIds = $this->replaceEntityByIds($entityValue, $entitiesByCode);

    if ($entityType == 'PaymentDealExternal') {
      $entityValueWithIds['id'] = null;
      if(!$this->PaymentDealExternal->save($entityValueWithIds)){
      	$this->ApiLogger->error("ERROR AL INSERTAR",$entityValueWithIds);
      	$this->ApiLogger->echoConsole("ERROR AL INSERTAR".json_encode($entityValueWithIds));
      	exit;
      } 
      $entityValueWithIds['id'] = $this->PaymentDealExternal->getLastInsertId();

    } else {
      $entityValueWithIds['id'] = rand();
    }
    return $entityValueWithIds;
  }

  public function replaceEntityByIds($entityValue, $entitiesByCode) {
    $entityValueWithIds = array();
    foreach ($entityValue as $key => $value) {
      if(array_key_exists($value, $entitiesByCode)) {
        $entityValueWithIds[$key] = $entitiesByCode[$value]['id'];
      } else {
        $entityValueWithIds[$key] = $value;
      }
    }
    return $entityValueWithIds;
  }


  public function startsWith($haystack, $needle) {
    return $needle === "" || strpos($haystack, $needle) === 0;
  }


  private function isExpectedEntity($text) {
    return $this->startsWith($text, '_x');
  }

  private function isEntity($text) {
    return $text[0] != '_';
  }

  private function getEntityType($entityKey) {
    $startIn = strpos($entityKey, '.')+1;
    $endIn = strpos($entityKey, '(');
    return  substr($entityKey, $startIn, $endIn-$startIn);
  }

  private function getEntityCode($entityKey) {
    $startIn = strpos($entityKey, '(')+1;
    $endIn = strpos($entityKey, ')');
    return  substr($entityKey, $startIn, $endIn-$startIn);
  }

  public function importModels($data) {
    App::import('Model', 'payment.PaymentDealExternal');
    $this->PaymentDealExternal = new PaymentDealExternal();
    return true;
  }
}
