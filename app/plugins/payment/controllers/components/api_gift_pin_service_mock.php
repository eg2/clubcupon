<?php
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiBac');
App::import('Model', 'api.ApiGiftPin');
class ApiGiftPinServiceMockComponent extends ApiBaseComponent {

  const BAC_STATUS_AVAILABLE = 1; // El pin se encuntra listo para ser usado
  const BAC_STATUS_USED = 2; // El pin ha sido utilizado
  const BAC_STATUS_CANCELED = 3; // El pin se encuentra inhabilitado
  const BAC_STATUS_EXPIRED = 4; // El pin pertenece a un paquete que ha caducado
  const BAC_STATUS_DISABLED = 5; // El pin ha sido deshabilitado
  const BAC_STATUS_CANCELED_MANUALLY = 6; // El pin se anula manualmente

  public $pinIsAvaiableForBac = true;
  public $bacPin;
  
  private $bac_id_portal = null;

  public $name = 'ApiGiftPinService';
  public $components = array(
    'api.ApiDebug',
    'api.ApiBac',
  ); 

  public function __construct() {
    $this->ApiGiftPin = &new ApiGiftPin();
    $this->bac_id_portal = Configure::read('BAC.id_portal');
    if (!isset($this->ApiBac)) {
      $this->ApiBac = &new ApiBacComponent();
    }
  }

  public function isUsable($code) {
    if (empty($code)){
        return false;
    }      
      
    if(strlen($code)> 10) {
      return false;
    }
  
    $bacPin = $this->obtenerBacPin($code);
    if( true && !$this->isBlocked($code)) {
      return $bacPin;
    } else {
      return false;
    }
  }

  private function isAvaliableForBac($bacPin) {
    if(!empty($bacPin)) {
      return $this->pinIsAvaiableForBac;
    }
    return false;
  }

  private function isBlocked($code) {
    $giftPin = $this->ApiGiftPin->findByCode($code);
    if(!empty($giftPin)) {
      return $this->ApiGiftPin->isBlocked($giftPin);
    }
    return false;
  }

  public function saveBacPinInGiftPinByCode($code) {
    $giftPin = $this->ApiGiftPin->findByCode($code);
    if(empty($giftPin)) {
      $bacPin = $this->obtenerBacPin($code);
      $giftPin = array('GiftPin' => array());
      $giftPin['GiftPin']['code'] = $code;
      $giftPin['GiftPin']['discount'] = $bacPin['monto'];
      $giftPin['GiftPin']['status'] = $bacPin['idEstadoPin'];
      $giftPin = $this->ApiGiftPin->save($giftPin);
      $giftPin['GiftPin']['id'] = $this->ApiGiftPin->id;
      Debugger::log(__METHOD__.__LINE__. print_r($giftPin,1));
    }
    return $giftPin;
  }

  public function block($code) {
    $giftPin = $this->ApiGiftPin->findByCode($code);
    if(empty($giftPin)) {
      $giftPin = $this->saveBacPinInGiftPinByCode($code);
    }
    
    $giftPin = $this->ApiGiftPin->block($giftPin);
    return $giftPin;
  }

  public function unBlock($code) {
    $giftPin = $this->ApiGiftPin->findByCode($code);
  
    return $this->ApiGiftPin->unBlock($giftPin);
  }

  public function unBlockAndBurnGiftPin($code, $userId) {
      $this->unBlock($code);
      return $this->burnGiftPin($code, $userId);
  }

  public function burnGiftPin($code, $userId) {
      $giftPin = $this->saveBacPinInGiftPinByCode($code);
      $consumirPinParams = array(
        'idUsuarioPortal' => $userId,
        'idPortal' => $this->bac_id_portal,
        'nroPin' => $code,
        'retailer' => 'Compra ClubCupon',
      );

      $giftPin['GiftPin']['status'] = self::BAC_STATUS_USED;
      Debugger::log("QUEMANDO PIN START");
      Debugger::log(print_r($giftPin,1));
      Debugger::log("QUEMANDO PIN END");
      $this->ApiGiftPin->save($giftPin);
      return $giftPin;
  }

  /**
   *  [codigo] => epqfxwnoz8
   *  [descripcion] => Código de descuento
   *  [idEstadoPaquete] => 1
   *  [idEstadoPin] => 1
   *  [idMoneda] => 1
   *  [idPaquete] => 10125
   *  [idPin] => 14091758
   *  [idProducto] => 102334
   *  [medida] => 0
   *  [monto] => 25
   */
   private function obtenerBacPin($code){
    return $this->bacPin;
  }

  public function calculateGiftUsedAmount($giftAvailableAmount, $totalGrossAmount) {
      $giftUsedAmount = 0;
      if ($totalGrossAmount <= $giftAvailableAmount) {
        $giftUsedAmount = $totalGrossAmount;
      } else {
        $giftUsedAmount = $giftAvailableAmount;
      }
      return $giftUsedAmount;
  }

}
