<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiBac');
App::import('Model', 'payment.PaymentDeal');

class PaymentBacMockComponent extends ApiBaseComponent {
 
	//******************************************************************************
	//******************************************************************************
	const ID_USUARIO_PORTAL = 'ID_USUARIO_PORTAL';
	const ID_PAGO_PORTAL = 'ID_PAGO_PORTAL';
	const ID_PAGO = 'ID_PAGO';
	const ID_MEDIO_PAGO = 'ID_MEDIO_PAGO';
	const ID_GATEWAY = 'ID_GATEWAY';
	const ESTADO_DEL_PAGO = 'ESTADO_DEL_PAGO';
	const ESTADO_DEL_PAGO_EN_GATEWAY = 'ESTADO_DEL_PAGO_EN_GATEWAY';
	
	const MESSAGE_BAC_EXCEPTION_NO_USER = 'TransaccionService.usuarioInexistente'; // nuevo UsuarioService.usuarioInexistente
	const MESSAGE_BAC_EXCEPTION_NO_PORTAL = 'TransaccionService.portalClienteInexistente';
	const MESSAGE_BAC_EXCEPTION_NEW_NO_PAYMENT = 'PagoService.pagoNuevoPortalInexistente';
	const MESSAGE_BAC_EXCEPTION_NO_PAYMENT = 'TransaccionService.pagoInexistente';
	
	const DATA_BAC_ESTADO_ACREDITADO = 'ACREDITADO';
	const DATA_BAC_ESTADO_PENDIENTE = 'PENDIENTE';
	const DATA_BAC_ESTADO_CANCELADO = 'CANCELADO';
	const DATA_BAC_ESTADO_ANULADO = 'ANULADO';
	const DATA_BAC_ESTADO_EXPIRADO = 'EXPIRADO';
	const DATA_BAC_ESTADO_ANULADO_INICIADO = 'ANULADO_INICIADO';
	
	//******************************************************************************
	//******************************************************************************
  
	private $bacPortalId;
    private $ApiBac;
    private $enableQueryBac;//deshabilita bac, habilita simulacion para test
	private $PaymentDeal;
	
	public $dataBacMockResponse;
	public $responseException;
	
  	function __construct() {
    	parent::__construct();
    	$this->bacPortalId = Configure::read('BAC.id_portal');
    	$this->ApiBac = new ApiBacComponent();
		$this->enableQueryBac = false; //deshabilito consulta a bac para trabajar local
		$this->PaymentDeal = new PaymentDeal();
		$this->responseException = false;
  	}
  	
  	public function isKnowBacException($messageBacException){
  		if((stripos ($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_USER) !== false)
  			|| (stripos ($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_PORTAL) !== false)
  			|| (stripos ($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_PAYMENT) !== false)
  			|| (stripos ($messageBacException, self::MESSAGE_BAC_EXCEPTION_NEW_NO_PAYMENT) !== false)){
  				return true;
  		}
  		return false;
  	}
  	
  	public function checkPayment($userId,$dealExternal, $portal = 0) {
  		if(!$this->responseException){
  			return json_decode($this->dataBacMockResponse);
  		}else{
  			throw new Exception(self::MESSAGE_BAC_EXCEPTION_NO_PAYMENT);
  		}
  	}
  	
  	public function isReputableState($dataBac){
  		if( ( $dataBac->estado == self::DATA_BAC_ESTADO_ACREDITADO ) ){
  			return true;
  		}
  		return false;
  	}
  
  	public function isSetGateway($dataBac){
  		if(isset($dataBac->estadoDescripcionGateway)){
  			return true;
  		}
  		return false;
  	}
  	public function getStateDescriptionGateway($dataBac){
  		return substr ($dataBac->estadoDescripcionGateway, 0, 63);
  	}
  	public function isStatePending($dataBac){
  		if($dataBac->estado == self::DATA_BAC_ESTADO_PENDIENTE){
  			return true;
  		}
  		return false;
  	}
  	public function isKowBacState($dataBac){
  		if(($dataBac->estado == self::DATA_BAC_ESTADO_ACREDITADO)
  			//||($dataBac->estado == self::DATA_BAC_ESTADO_PENDIENTE)	
  			||($dataBac->estado == self::DATA_BAC_ESTADO_CANCELADO)
  			||($dataBac->estado == self::DATA_BAC_ESTADO_ANULADO)
  			||($dataBac->estado == self::DATA_BAC_ESTADO_EXPIRADO)
  			||($dataBac->estado == self::DATA_BAC_ESTADO_ANULADO_INICIADO)){
 					return true; 			
  		}
  		return false;
  	}
  	public function getIdPago($dataBac){
  		return $dataBac->idPago;
  	}
  	
  	public function isCancellationState($dataBac){
  		if(($dataBac->estado == self::DATA_BAC_ESTADO_CANCELADO)
  		||($dataBac->estado == self::DATA_BAC_ESTADO_ANULADO)
  		||($dataBac->estado == self::DATA_BAC_ESTADO_EXPIRADO)
  		||($dataBac->estado == self::DATA_BAC_ESTADO_ANULADO_INICIADO)){
  			return true;
  		}
  		return false;
  	}
}

