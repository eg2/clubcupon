<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class AssertComponent extends ApiBaseComponent {

  public $name = 'Assert';

  function __construct() {
    parent::__construct();
  }

  public function compare($expected, $real) {
    $diffs = array();
    foreach($expected as $key => $expectedValue) {
      if($real[$key] != $expectedValue) {
        $diffs[$key]['expected'] = $expectedValue;
        $diffs[$key]['real'] = $real[$key];
      }
    }
    return $diffs;
  }

}


