<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiBac');
App::import('Model', 'payment.PaymentDealExternal');
App::import('Model', 'payment.PaymentDeal');
App::import('Model', 'payment.PaymentDealUser');
App::import('Model', 'payment.PaymentUser');
App::import('Component', 'payment.PaymentBac');
App::import('Component', 'payment.PaymentBacMock');
App::import('Component', 'product.ProductInventoryService');
App::import('Component', 'api.ApiGiftPinService');
App::import('Core', 'Controller');
App::import('Controller', 'Deals');
App::import('Core', 'Router');
App::import('Core', 'View');
App::import('Component', 'Email');
App::import('Component', 'Session');
App::import('Model', 'Deal');
App::import('Model', 'EmailTemplate');

class PaymentPaymentServiceComponent extends ApiBaseComponent {

    public $name = 'PaymentPaymentService';

    const DEAL_EXTERNAL_PENDING = 'P';
    const DEAL_EXTERNAL_CANCELLED = 'C';

    private $PaymentDealExternal;
    private $dataSource;
    private $PaymentDeal;
    private $PaymentUser;
    private $Email;
    private $Deal;
    private $PaymentDealUser;
    public $PaymentBac;
    public $ApiGiftPin;
    private $processDealStatus;
    private $processDealStatusEpsilon;

    function _initEmailAndDealsController() {
        $this->Email = new EmailComponent();
        $this->Email->Controller = new DealsController();
        $this->Email->Controller->Session = new SessionComponent();
        $view = new View($this->Email->Controller);
        $this->Email->Controller->Deal = $this->Deal;
        $this->Email->Controller->EmailTemplate = new EmailTemplate();
        $this->Email->Controller->Email = $this->Email;
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $this->Email->sendAs = 'html';
    }

    function __construct() {
        parent::__construct();
        $this->bacPortalId = Configure::read('BAC.id_portal');
        $this->PaymentDealExternal = new PaymentDealExternal();
        $this->dataSource = $this->PaymentDealExternal->getDataSource();
        $this->PaymentDeal = new PaymentDeal();
        $this->PaymentBac = new PaymentBacComponent();
        $this->PaymentUser = new PaymentUser();
        $this->ApiGiftPin = new ApiGiftPinServiceComponent();
        $this->Deal = new Deal();
        $this->PaymentDealUser = new PaymentDealUser();
        $this->_initEmailAndDealsController();
        $this->processDealStatus = Configure::read('ProcessDealStatus.types');
        $this->processDealStatusEpsilon = Configure::read('ProcessDealStatus.epsilon');
    }

    public function updatePayment($detail) {
        $this->ApiLogger->notice('Actualizando Pago', array(
            'detail' => $detail
        ));
        $dealExternal = $this->PaymentDealExternal->findById($detail[PaymentBacComponent::ID_PAGO_PORTAL]);
        if (!is_null($dealExternal)) {
            if ($this->isCreationTimeWithinRangeForUpdate($dealExternal)) {
                $User = $this->PaymentUser->findById($dealExternal['PaymentDealExternal']['user_id']);
                $this->ApiLogger->notice('DealExternal a Procesar', array(
                    'id' => $dealExternal['PaymentDealExternal']['id']
                ));
                $this->ApiLogger->notice('Usuario Comprador', array(
                    'user_id' => $User['PaymentUser']['id']
                ));
                if ($this->isDealExternalUpdeteable($dealExternal)) {
                    $this->updateDealExternal($dealExternal, $detail, $User);
                } else {
                    $this->ApiLogger->notice('EL pago no se encuentra pendiente');
                }
            } else {
                $this->ApiLogger->notice('El DealExternal no se procesara por limite de tiempo', array(
                    'deal_external_id' => $dealExternal['PaymentDealExternal']['id'],
                    'proces_deal_status_time' => $this->processDealStatus['new']['minAge']
                ));
            }
        } else {
            $this->ApiLogger->notice('No se encontro el pago');
        }
    }

    private function isDealExternalCancelled($dealExternal) {
        return $dealExternal['PaymentDealExternal']['external_status'] == self::DEAL_EXTERNAL_CANCELLED;
    }

    function isCreationTimeWithinRangeForUpdate($dealExternal) {
        $return = true;
        if (!$this->isDealExternalCancelled($dealExternal)) {
            $created = date('Y-m-d H:i:s', strtotime($dealExternal['PaymentDealExternal']['created']));
            $limite = date('Y-m-d H:i:s', strtotime("-" . $this->processDealStatus['new']['minAge'], strtotime(date('Y-m-d H:i'))));
            $limite = date('Y-m-d H:i:s', strtotime("+" . $this->processDealStatusEpsilon, strtotime($limite)));
            $this->ApiLogger->notice('comparando fecha de creacion con processDealStatus_new_minAge', array(
                'deal_external_created' => $dealExternal['PaymentDealExternal']['created'],
                'processDealStatus_new_minAge' => $limite,
                'epsilon' => $this->processDealStatusEpsilon
            ));
            $return = $created > $limite;
        }
        return $return;
    }

    public function updateDealExternal($dealExternal, $detail, $User) {
        $userId = $detail[PaymentBacComponent::ID_USUARIO_PORTAL];
        try {
            $this->ApiLogger->notice('Consultando Bac deal_external_id:' . $dealExternal['PaymentDealExternal']['id']);
            $dataBac = $this->PaymentBac->checkPayment($userId, $dealExternal);
            $this->ApiLogger->notice('Response from Bac:', $dataBac);
            $this->processDealExternal($dealExternal, $dataBac, $User);
        } catch (Exception $e) {
            $this->ApiLogger->notice('Bac respondio con una excepcion:' . $e->getMessage(), $e);
            $this->cancelDealExternalByException($dealExternal, $User, $e->getMessage());
        }
    }

    private function processDealExternal($dealExternal, $dataBac, $User) {
        $oldDealExternal = $dealExternal;
        $this->ApiLogger->notice('Procesando Deal External', array(
            'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
        ));
        $this->ApiLogger->notice('Starting Transaction');
        $this->dataSource->begin();
        $this->ApiLogger->notice('deal External antes de UpdateBacIdAndStock', $dealExternal);
        $dealExternal = $this->updateBacIdAndStock($dealExternal, $dataBac);
        $this->ApiLogger->notice('deal External despues de updateBacIdAndStock', $dealExternal);
        $dealExternal = $this->updateBacSubstate($dealExternal, $dataBac);
        $this->ApiLogger->notice('deal External despues de updateBacSubstate', $dealExternal);
        $dealExternal = $this->updateDealExternalStatus($dealExternal, $dataBac, $User);
        if (is_null($dealExternal)) {
            $this->ApiLogger->error('Error al Actualizar DealExternal ROLLBAK de cambios', $oldDealExternal);
            $this->dataSource->rollback();
        }
        $this->ApiLogger->notice('deal External despues de updateDealExternalStatus', $dealExternal);
        $this->ApiLogger->notice('Deal External Procesado');
        if ($this->isAccreditedDealExternal($dealExternal) && !is_null($dealExternal)) {
            $this->ApiLogger->notice('Generando Cupon');
            if (!$this->generateCupon($dealExternal)) {
                $this->ApiLogger->error('Error al Generar Cupon, ROLLBAK de cambios', $oldDealExternal);
                $this->dataSource->rollback();
            }
            $this->ApiLogger->notice('Cupon Generado');
            if (!$this->sendCupon($dealExternal)) {
                $this->ApiLogger->error('Error al Enviar Cupon');
            }
        }
        $this->ApiLogger->notice('Ending Transaction');
        $this->dataSource->commit();
    }

    function isAccreditedDealExternal($dealExternal) {
        return ($dealExternal['PaymentDealExternal']['external_status'] == 'A');
    }

    function updateDealExternalStatus($dealExternal, $dataBac, $User) {
        if ($this->PaymentBac->isKowBacState($dataBac)) {
            $this->ApiLogger->notice('Actualizando Estado DealExternal', array(
                'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
            ));
            $this->unlockWallet($dataBac, $User);
            if ($this->PaymentBac->isCancellationState($dataBac)) {
                $this->ApiLogger->notice('El pago se encuentra en estado Cancelatorio');
                $this->ApiLogger->notice('Cancelando DealExternal', array(
                    'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                ));
                $dealExternal = $this->cancelDealExternal($dealExternal);
                if (!is_null($dealExternal)) {
                    if ($this->PaymentDealExternal->hasGiftPin($dealExternal)) {
                        $this->ApiLogger->notice('unblock gift pin.');
                        $this->unBlockGiftPin($dealExternal);
                    }
                    $this->ApiLogger->notice('DealExternal Cancelado', array(
                        'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                    ));
                } else {
                    $this->ApiLogger->error('Error al cancelar DealExternal', array(
                        'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                    ));
                    return null;
                }
            }
            if ($this->PaymentBac->isReputableState($dataBac)) {
                $this->ApiLogger->notice('El pago se encuentra en estado ACREDITADO', array(
                    'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                ));
                $dealExternal = $this->estabishPayment($dealExternal, $dataBac, $User);
                if (is_null($dealExternal)) {
                    return null;
                }
            }
            return $dealExternal;
        }
    }

    function estabishPayment($dealExternal, $dataBac, $user) {
        $dealExternal = $this->updateAmounts($dealExternal, $dataBac, $user);
        if (is_null($dealExternal)) {
            return null;
        }
        $this->updateAmountsIfGiftPin($dealExternal, $user);
        $dealExternal = $this->accreditDealExternal($dealExternal);
        if (is_null($dealExternal)) {
            return null;
        }
        return $dealExternal;
    }

    function accreditDealExternal($dealExternal) {
        $dealExternal['PaymentDealExternal']['external_status'] = 'A';
        $dealExternal['PaymentDealExternal']['updated'] = date('Y-m-d H:i:s');
        $this->ApiLogger->notice('Acreditando Pago', array(
            'dealExternal' => array(
                'id' => $dealExternal['PaymentDealExternal']['id'],
                'external_status' => $dealExternal['PaymentDealExternal']['external_status'],
                'updated' => $dealExternal['PaymentDealExternal']['updated']
            )
        ));
        if ($this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
            $this->ApiLogger->notice('Pago Acreditado', array(
                'dealExternal' => array(
                    'id' => $dealExternal['PaymentDealExternal']['id']
                )
            ));
            return $dealExternal;
        } else {
            $this->ApiLogger->error('Error al intentar actualiza el campo bac_id del deal_external', $dealExternal['PaymentDealExternal']);
            return null;
        }
    }

    function unlockWallet($dataBac, $User) {
        $this->ApiLogger->notice('Desbloqueando monedero del usuario.', array(
            'user_id' => $User['PaymentUser']['id']
        ));
        if ($this->PaymentUser->unlockWallet($User)) {
            $this->ApiLogger->notice('Monedero del usuario desbloqueado.');
        } else {
            $this->ApiLogger->error('No se pudo desbloquear el monedero del usuario.', array(
                'user_id' => $User['PaymentUser']['id']
            ));
        }
    }

    function updateBacSubstate($dealExternal, $dataBac) {
        $backDealExternal = $dealExternal;
        if ($this->PaymentBac->isSetGateway($dataBac)) {
            $dealExternal['PaymentDealExternal']['bac_substate'] = $this->PaymentBac->getStateDescriptionGateway($dataBac);
            $this->ApiLogger->notice('Actualizando el campo bac_substate del deal_external', array(
                'id' => $dealExternal['PaymentDealExternal']['id'],
                'bac_substate' => $dealExternal['PaymentDealExternal']['bac_substate']
            ));
            if ($this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
                $this->ApiLogger->notice('Campo bac_substate del deal_external actualizado exitosamente');
                return $dealExternal;
            } else {
                $this->ApiLogger->error('Error al intentar actualiza el campo bac_substate del deal_external', $dealExternal['PaymentDealExternal']);
                return $backDealExternal;
            }
        }
        return $dealExternal;
    }

    function updateBacIdAndStock($dealExternal, $dataBac) {
        $backDealExternal = $dealExternal;
        $bacId = $this->PaymentBac->getIdPago($dataBac);
        $dealExternal['PaymentDealExternal']['bac_id'] = $bacId;
        $this->ApiLogger->notice('Actualizando BacID del DealExternal', array(
            'DealExternalId' => $dealExternal['PaymentDealExternal']['id'],
            'bac_id' => $bacId
        ));
        if ($this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
            $this->ApiLogger->notice('Campo bac_id del deal_external actualizado exitosamente');
            if ($this->PaymentBac->isReputableState($dataBac)) {
                $this->ApiLogger->notice('Actualizando Stock', array(
                    'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                ));
                $dealExternal = $this->stockDecrease($dealExternal);
                $this->ApiLogger->notice('Se actualizo el stock', array(
                    'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
                ));
            }
        } else {
            $this->ApiLogger->error('Error al intentar actualiza el campo bac_id del deal_external', array(
                'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
            ));
            return $backDealExternal;
        }
        return $dealExternal;
    }

    function stockDecrease($dealExternal) {
        if (!$this->PaymentDealExternal->isStockDecrease($dealExternal)) {
            $inventoryService = ProductInventoryServiceComponent::instance();
            $inventoryService->decrease($dealExternal['PaymentDealExternal']['deal_id'], $dealExternal['PaymentDealExternal']['quantity'], $dealExternal['PaymentDealExternal']['user_id']);
            $this->PaymentDealExternal->updateStockDecrease($dealExternal);
            $dealExternal['PaymentDealExternal']['is_stock_decresed'] = 1;
        }
        return $dealExternal;
    }

    private function cancelDealExternalByException($dealExternal, $User, $messageBacException) {
        if ($this->PaymentBac->isKnowBacException($messageBacException)) {
            $this->ApiLogger->notice('Cancelando deal External por Excepcion Conocida');
            $dealExternal['PaymentDealExternal']['external_status'] = 'C';
            $dealExternal['PaymentDealExternal']['cancellation_reason'] = $messageBacException;
            $dealExternal['PaymentDealExternal']['updated'] = date('Y-m-d H:i:s.u');
            $this->dataSource->begin();
            if (!$this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
                $this->ApiLogger->error('No se pudo cancelar el Deal External. ROLLBAC de cambios');
                $this->dataSource->rollback();
            } else {
                $this->ApiLogger->notice('Deal External Cancelado');
            }
            if ($dealExternal['PaymentDealExternal']['internal_amount']) {
                $this->ApiLogger->notice('Desbloqueando Monedero. user_id:' . $User['PaymentUser']['id']);
                if (!$this->PaymentUser->unlockWallet($User)) {
                    $this->ApiLogger->error('No se pudo desbloquear el monedero. ROLLBAC de los cambios.');
                    $this->dataSource->rollback();
                } else {
                    $this->ApiLogger->notice('Monedero del usuario desbloqueado.');
                }
            }
            $this->ApiLogger->notice('Commit de los cambios');
            $this->dataSource->commit();
        } else {
            $this->ApiLogger->error('Excepcion Desconocida deal External no procesado', array(
                'ExceptionMessage' => $messageBacException,
                'dealExternalId' => $dealExternal['PaymentDealExternal']['id']
            ));
        }
    }

    private function cancelDealExternal($dealExternal) {
        $backDealExternal = $dealExternal;
        $this->ApiLogger->notice('Cancelando DealExternal');
        $dealExternal['PaymentDealExternal']['external_status'] = 'C';
        $dealExternal['PaymentDealExternal']['updated'] = date('Y-m-d H:i:s.u');
        if (!$this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
            $this->ApiLogger->error('No se pudo cancelar el Deal External.');
            return null;
        }
        return $dealExternal;
    }

    public function isDealExternalUpdeteable($dealExternal) {
        return in_array($dealExternal['PaymentDealExternal']['external_status'], array(
            self::DEAL_EXTERNAL_PENDING,
            self::DEAL_EXTERNAL_CANCELLED
        ));
    }

    public function UpdateBacId($bacId, $dealExternal) {
        $dealExternal['PaymentDealExternal']['bac_id'] = $bacId;
        if ($this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
            return true;
        } else {
            return false;
        }
    }

    private function unBlockGiftPin($dealExternal) {
        return $this->ApiGiftPin->unBlock($dealExternal['PaymentDealExternal']['gift_pin_code']);
    }

    function updateAmounts($dealExternal, $dataBac, $User) {
        $this->ApiLogger->notice('Actualizando Montos', array(
            'DealExternalId' => $dealExternal['PaymentDealExternal']['id']
        ));
        $dealExternal = $this->updatePartialAndFinalAmounts($dealExternal, $dataBac);
        if (is_null($dealExternal)) {
            $this->ApiLogger->error('Error al actualizar montos');
            return null;
        }
        $this->ApiLogger->notice('Actualizando actualizando monto de monedero');
        $amount = $dataBac->montoFinal - $dataBac->interes;
        $this->ApiLogger->notice('Actualizando monto del monedero del usuario', array(
            'user_id' => $User['PaymentUser']['id'],
            'amount' => $amount
        ));
        if (!$this->PaymentUser->addAmountToWallet($amount, $User)) {
            $this->ApiLogger->error('Error al actualizar montos', array(
                'user_id' => $User['PaymentUser']['id'],
                'amount' => $amount
            ));
            return null;
        }
        $this->ApiLogger->notice('Monto del monedero actualizado');
        $this->addAmountToWalletIfDealHasDiscount($dealExternal, $User);
        return $dealExternal;
    }

    function updatePartialAndFinalAmounts($dealExternal, $dataBac) {
        $dealExternal['PaymentDealExternal']['parcial_amount'] = $dataBac->montoParcial;
        $dealExternal['PaymentDealExternal']['final_amount'] = $dealExternal['PaymentDealExternal']['parcial_amount'] + $dealExternal['PaymentDealExternal']['internal_amount'] + $dealExternal['PaymentDealExternal']['discounted_amount'] + $dealExternal['PaymentDealExternal']['gifted_amount'];
        $this->ApiLogger->notice('Actualizando monto parcial y nonto final del DealExternal', array(
            'id' => $dealExternal['PaymentDealExternal']['id'],
            'final_amount' => $dealExternal['PaymentDealExternal']['final_amount'],
            'parcial_amount' => $dealExternal['PaymentDealExternal']['parcial_amount'],
            'internal_amount' => $dealExternal['PaymentDealExternal']['internal_amount'],
            'discounted_amount' => $dealExternal['PaymentDealExternal']['discounted_amount'],
            'gifted_amount' => $dealExternal['PaymentDealExternal']['gifted_amount']
        ));
        if (!$this->PaymentDealExternal->save($dealExternal['PaymentDealExternal'])) {
            $this->ApiLogger->error('Error al Actualizar montos parcial y final');
            return null;
        }
        return $dealExternal;
    }

    private function addAmountToWalletIfHasGiftPin($dealExternal, $user) {
        $code = $dealExternal['PaymentDealExternal']['gift_pin_code'];
        $this->ApiGiftPin->unBlock($code);
        if ($code) {
            $bacPin = $this->ApiGiftPin->isUsable($code);
        }
        if (!$bacPin) {
            $this->ApiLogger->error('No es usable, pincode: ' . $code, $dealExternal);
        } else {
            $this->ApiLogger->error('Es usable, pincode: ', array(
                'deal_external' => $dealExternal,
                'bac_pin' => $bacPin
            ));
            $this->ApiLogger->notice('Depositando monto del codigo de pin en Monedero', array(
                'user_id' => $user['PaymentUser']['id'],
                'monto' => $bacPin['monto']
            ));
            $amount = $bacPin['monto'];
            $this->PaymentUser->addAmountToWallet($amount, $user);
        }
    }

    private function updateAmountsIfGiftPin($dealExternal, $user) {
        if ($this->PaymentDealExternal->hasGiftPin($dealExternal)) {
            $this->ApiLogger->notice('La Compra tiene Pin de Descuento: ' . $dealExternal['PaymentDealExternal']['gift_pin_code'] . ', depositando saldo al monedero del usuario.');
            $this->ApiLogger->notice("Actualizando monto del Monedero");
            $this->addAmountToWalletIfHasGiftPin($dealExternal, $user);
            $this->ApiLogger->notice("Quemando Pin", array(
                'gift_pin_code' => $dealExternal['PaymentDealExternal']['gift_pin_code']
            ));
            $this->ApiGiftPin->burnGiftPin($dealExternal['PaymentDealExternal']['gift_pin_code'], $user['PaymentUser']['id']);
            $this->ApiLogger->notice('Saldo depositado');
        }
    }

    function sendCupon($dealExternal) {
        $return = true;
        $this->ApiLogger->notice('Buscando DealUsers para el envio de cupon');
        $dealUsers = $this->PaymentDealUser->findByDealExternal($dealExternal);
        $this->ApiLogger->notice('Se enviaran ' . count($dealUsers) . ' cupones');
        foreach ($dealUsers as $dealUser) {
            $this->ApiLogger->notice('Enviando Cupon', $dealUser);
            $this->ApiLogger->notice('Buscando Oferta');
            $dealToSendCupon = $this->Deal->findDealForSendCupon($dealExternal['PaymentDealExternal']['deal_id'], $dealUser['PaymentDealUser']['id']);
            if (!is_null($dealToSendCupon)) {
                $this->ApiLogger->notice('Oferta a enviar', array(
                    'DealToSendID' => $dealToSendCupon['Deal']['id']
                ));
                if ($this->Deal->_sendCouponMail($dealToSendCupon)) {
                    if ($this->PaymentDealUser->updateEmailSend($dealUser)) {
                        $this->ApiLogger->notice('Cupon enviado');
                        $return = true && $return;
                    } else {
                        $this->ApiLogger->error('Error al enviar el cupon');
                        $return = false;
                    }
                }
            }
        }
        return $return;
    }

    function generateCupon($dealExternal) {
        $data['Deal']['deal_id'] = $dealExternal['PaymentDealExternal']['deal_id'];
        $data['Deal']['quantity'] = $dealExternal['PaymentDealExternal']['quantity'];
        $data['Deal']['is_gift'] = $dealExternal['PaymentDealExternal']['is_gift'];
        $data['Deal']['gift_data'] = json_decode($dealExternal['PaymentDealExternal']['gift_data'], true);
        $data['Deal']['payment_type_id'] = $dealExternal['PaymentDealExternal']['payment_type_id'];
        $data['Deal']['user_id'] = $dealExternal['PaymentDealExternal']['user_id'];
        $data['DealExternal'] = $dealExternal['PaymentDealExternal'];
        $this->ApiLogger->notice('Data generacion de cupon:', $data);
        if (!$this->Email->Controller->_buyDeal($data, false)) {
            return false;
        }
        return true;
    }

    function addAmountToWalletIfDealHasDiscount($dealExternal, $User) {
        $deal = $this->PaymentDeal->findById($dealExternal['PaymentDealExternal']['deal_id']);
        if ($this->PaymentDeal->isDiscountMP($deal)) {
            $this->ApiLogger->notice("Tiene Descuento MP");
            $totalToApplyDiscount = ($deal['PaymentDeal']['discounted_price'] * $dealExternal['PaymentDealExternal']['quantity']) - $dealExternal['PaymentDealExternal']['gifted_amount'] - $dealExternal['PaymentDealExternal']['internal_amount'];
            $discountedAmount = $this->getDiscountMP($totalToApplyDiscount, 1);
            $this->ApiLogger->notice("Actualizando monto descuento al monedero", array(
                'user_id' => $User['PaymentUser']['id'],
                'amount' => $discountedAmount
            ));
            $this->PaymentUser->addAmountToWallet($discountedAmount, $User, 'add amount to wallet from discount');
        }
    }

    function getDiscountMP($discount_price, $quantity) {
        $total = Precision::mul($discount_price, $quantity);
        $discount = Precision::mul($total, ((float) Configure::read('deal.discount_mp')));
        if ($discount > ((float) Configure::read('deal.limit_discount_mp'))) {
            $discount = Configure::read('deal.limit_discount_mp');
        }
        return ((float) $discount);
    }

}
