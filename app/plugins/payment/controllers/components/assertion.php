<?php
App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');

class AssertionComponent extends ApiBaseComponent {

  public $name = 'Assertion';

  function __construct() {
    parent::__construct();
  }

}
