<?php 

App::import('', 'api.ApiAppModel');
App::import('Component', 'api.ApiBase');


class PaymentAppModel extends ApiAppModel {

  public $recursive = -1;

  function __construct() {
    parent::__construct();
    $this->ApiLogger = new ApiLogger(get_class($this), 'PaymentAppModel');
  }

  public function getLastQuery() {
    if(Configure::read('debug')==2) {
      $dbo = $this->getDatasource();
      $logs = $dbo->_queriesLog;
      return end($logs);
    } else {
      return false;
    }
  }

  public function getQueryLog() {
    if(Configure::read('debug')==2) {
      $dbo = $this->getDatasource();
      $logs = $dbo->_queriesLog;
      return $logs;
    } else {
      return false;
    }
  }

}

