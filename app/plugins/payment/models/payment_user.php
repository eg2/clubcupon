<?php

App::import('Model', 'User');

class PaymentUser extends PaymentAppModel {

    var $useTable = 'users';
    var $name = 'PaymentUser';
    var $alias = 'PaymentUser';
    public $recursive = - 1;

    public function unlockWallet($User) {
        if (!$this->updateAll(array(
                    'PaymentUser.wallet_blocked' => 0
                        ), array(
                    'PaymentUser.id' => $User['PaymentUser']['id']
                ))) {
            return false;
        }
        return true;
    }

    function addAmountToWallet($amount, $User, $description = null) {
        $UserOld = new User();
        $data = array(
            'user_id' => $User['PaymentUser']['id'],
            'foreign_id' => ConstUserIds::Admin,
            'class' => 'SecondUser',
            'amount' => $amount,
            'description' => $description ? $description : 'added amount to wallet ' . __METHOD__,
            'gateway_fees' => 0,
            'transaction_type_id' => ConstTransactionTypes::AddedToWallet
        );
        $transaction_id = $UserOld->Transaction->log($data);
        if (!$this->updateAll(array(
                    'PaymentUser.available_balance_amount' => 'PaymentUser.available_balance_amount + ' . $amount
                        ), array(
                    'PaymentUser.id' => $User['PaymentUser']['id']
                ))) {
            return false;
        }
        return true;
    }

}
