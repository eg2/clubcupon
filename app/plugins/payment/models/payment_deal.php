<?php

class PaymentDeal extends PaymentAppModel {

  var $useTable = 'deals';
  var $name = 'PaymentDeal';
  var $alias = 'PaymentDeal';
  

  public $recursive = -1;

  
  var $belongsTo = array (
  		'ProductProduct' => array (
  				'className' => 'ProductProduct',
  				'foreignKey' => 'product_product_id',
  				'conditions' => '',
  				'dependent' => true
  		),
  );
  
  function findDealByDealId($dealId){
  	return $this->find('first', array(
  			'conditions' => array(
  					'PaymentDeal.id' => $dealId,
  			),
  			'contain' => array(
  					'ProductProduct'
  			),
  	));
  }
  
  function getProductFromDeal($dealId){
  	$deal = $this->findDealByDealId($dealId);
  	return array( 'ProductProduct'=> $deal['ProductProduct']);
  }
   
 function isDiscountMp($deal){
 	return ($deal['PaymentDeal']['is_discount_mp'] == 1);
 }

}
