<?php

class PaymentDealUser extends PaymentAppModel {

  var $useTable = 'deal_users';
  var $name = 'PaymentDealUser';
  var $alias = 'PaymentDealUser';
  

  public $recursive = -1;


  function findByDealExternal($dealExternal){
  		return $this->find(
  				'all',
  				array(
  						'conditions' => array(
  								'PaymentDealUser.deal_external_id' => $dealExternal['PaymentDealExternal']['id'],
  						),
  						'recursive' => -1,
  				));
  }

  function updateEmailSend($dealUser){
  	if (!$this->updateAll (
  							array ('PaymentDealUser.emailed' => 1), 
  							array ('PaymentDealUser.id' => $dealUser['PaymentDealUser']['id'])
  				)) {
  					return false;
  	}
  	return true;
  }
}
