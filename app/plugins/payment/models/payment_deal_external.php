<?php

class PaymentDealExternal extends PaymentAppModel {

    var $useTable = 'deal_externals';
    var $name = 'PaymentDealExternal';
    var $alias = 'PaymentDealExternal';
    public $recursive = - 1;

    public function updateBacID($dealExternal) {
        return $this->updateAll(array(
                    'PaymentDealExternal.bac_id' => $dealExternal['PaymentDealExternal']['bac_id']
                        ), array(
                    'PaymentDealExternal.id' => $dealExternal['PaymentDealExternal']['id']
        ));
    }

    function hasGiftPin($dealExternal) {
        return !empty($dealExternal['PaymentDealExternal']['gift_pin_code']);
    }

    function isStockDecrease($dealExternal) {
        return ($dealExternal['PaymentDealExternal']['is_stock_decresed'] == 1);
    }

    function updateStockDecrease($dealExternal) {
        return $this->updateAll(array(
                    'PaymentDealExternal.is_stock_decresed' => 1
                        ), array(
                    'PaymentDealExternal.id' => $dealExternal['PaymentDealExternal']['id']
        ));
    }

    function updateGiftPinId($dealExternal, $giftPin) {
        if (!$this->updateAll(array(
                    'PaymentDealExternal.gift_pin_id' => $giftPin['GiftPin']['id']
                        ), array(
                    'PaymentDealExternal.id' => $dealExternal['PaymentDealExternal']['id']
                ))) {
            return false;
        }
        return $this->findById($dealExternal['PaymentDealExternal']['id']);
    }

    function findById($id) {
        $dealExternal = $this->find('first', array(
            'conditions' => array(
                'id' => $id
            ),
            'recursive' => - 1
        ));
        return $dealExternal;
    }

}
