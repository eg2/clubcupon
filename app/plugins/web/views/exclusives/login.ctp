<?php
/*
 * view: loginv.ctp
 * @author: EParisi - Pward 
 * 
 */
?>

<?php
$dataObj = json_decode($data);
$slug = $dataObj->city->slug;

if (!$dataObj->city->isBusinessUnit == 1 && $slug != 'mcdonalds') {
    $defValue = 'Ingresá tu PIN';
} else {
    $defValue = 'Ingresá tu DNI';
}
?>

<div id="login" <?php if ($slug == 'mcdonalds') {
    echo 'class="arcosLogin"';
} ?>>

    <?php if ($dataObj->city->isBusinessUnit == 1) { ?>
        <img src="/img/web/exclusives/nuestrosBeneficios/logo_login_nb.png" alt="logo" />
    <?php } else { ?>
        <?php if ($slug == 'mcdonalds') { ?>
                <!--  <img src="" alt="logo" /> -->
        <?php } else { ?>
            <img src="/img/web/exclusives/exclusive/logo_login_exclusive.png" alt="logo" />
        <?php } ?>
<?php } ?>

    <form id="formLogin" action="<?php echo Configure::read('web.firstLevelURLPath'); ?>login<?php if ($dataObj->city->isBusinessUnit == 1) {
            echo '/1';
        } ?>" method="post">
<?php if ($dataObj->city->isBusinessUnit == 1) { ?>
            <p>Bienvenido al portal de Ofertas y Beneficios Exclusivos<br>para empleados del Grupo Clarín</p>
<?php } ?>
        <div class="input-wrapper">
            <input type="text" name="pin" data-validetta="required" value="<?php echo $defValue ?>" onclick="this.value == '<?php echo $defValue ?>' ? this.value = '' : ''" onblur="this.value == '' ? this.value = '<?php echo $defValue ?>' : ''"  />
            <span class="validetta-error" style="display: none;"><span class="message"><?php echo $dataObj->message->text; ?></span><span class="validetta-errorClose">x</span></span>
            <input class="botonExclusive" type="submit" value="Ingresar"/>
        </div>
    </form>
</div>

<script type="text/javascript">

    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    }
    String.prototype.ltrim = function () {
        return this.replace(/^\s+/, "");
    }
    String.prototype.rtrim = function () {
        return this.replace(/\s+$/, "");
    }

    $.fn.validettaLanguage = function () {
    };
    $.validettaLanguage = {
        init: function () {
            $.validettaLanguage.messages = {
                empty: 'Campo obligatorio.',
                email: 'Mail no válido.',
                number: 'Este campo sólo acepta valores numéricos.',
                maxLength: 'Este campo acepta como máximo {count} caracteres.',
                minLength: 'Este campo requiere como mínimo {count} caracteres.',
                checkbox: 'Es necesario marcar este campo para continuar.',
                maxChecked: 'Sólo se puede marcar {count} opciones como máximo.',
                minChecked: 'Es necesario marcar como mínimo {count} opciones.',
                selectbox: 'Es necesario seleccionar un elemento de la lista.',
                maxSelected: 'Sólo se puede marcar {count} opciones como máximo.',
                minSelected: 'Es necesario marcar como mínimo {count} opciones.',
                notEqual: 'Los campos no coinciden.',
                creditCard: 'Tarjeta de crédito no válida.'
            };
        }
    };
    $.validettaLanguage.init();

    $(document).ready(function () {
        $('#formLogin').validetta({
            realTime: true
        });

        if ($('.validetta-error .message').text().trim().length) {
            $('.validetta-error').show();
        }
    });
</script>
