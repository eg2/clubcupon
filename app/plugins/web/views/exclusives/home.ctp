<?php
$dataObj = json_decode($data);
$user = $dataObj->user;
$slug = $dataObj->city->slug;
$isBusinessUnit = $dataObj->city->isBusinessUnit;
$dealList = $dataObj->vData->dealList;
$dealA = $dataObj->vData->dealA;
$dealB = $dataObj->vData->dealB;
$bannerList = $dataObj->vData->bannerList;
$showTiempoLibre = Configure::read('web.tuTiempoLibre.enable');
if ($dealA->isTourism) {
    $isTourism = 'si';
} else {
    $isTourism = 'no';
}
echo $this->element('adBackground', array(
    'slug' => $slug,
    'isCorporate' => true,
    'isTourism' => $isTourism
));
$MainDealwidth = Configure::read('thumb_size.outstandingourbenefits_big_thumb.width');
$MainDealheight = Configure::read('thumb_size.outstandingourbenefits_big_thumb.height');
$dealListwidth = Configure::read('thumb_size.listdeals_big_thumb.width');
$dealListheight = Configure::read('thumb_size.listdeals_big_thumb.height');
?>

<div class="maincontent centerdiv">
    <?php if (!empty($dealA)): ?>
        <?php
        echo $this->element('exclusives/homeMainDeals', array(
            'slug' => $slug,
            'deal1' => $dealA,
            'deal2' => $dealB,
            'isBusinessUnit' => $isBusinessUnit,
            'whidth' => $MainDealwidth,
            'height' => $MainDealheight
        ));
        ?>
    <?php endif; ?>
    <?php if ((!empty($dealList)) || (!empty($bannerList->dealList))): ?>	
        <a name="nuestrosCupones">&nbsp;</a>
        <?php
        echo $this->element('exclusives/dealList', array(
            'slug' => $slug,
            'dealList' => $dealList,
            'title' => "Nuestros Cupones",
            'url' => 'todos',
            'urlText' => 'Ver Todos',
            'isBusinessUnit' => $isBusinessUnit,
            'width' => $dealListwidth,
            'height' => $dealListheight,
            'bannerList' => $bannerList->dealList
        ));
        ?>
    <?php else: ?>
        <div class="noDeals">Sin Ofertas Activas.</div>	
    <?php endif; ?>	
    <?php if ($isBusinessUnit): ?>
        <?php
        if (Configure::read('web.ad_server') == 'eplanning') {
            echo $this->element('exclusives/adsList', array(
                'id' => "nuestros_convenios",
                'title' => "Nuestros Convenios",
                'pos' => 0,
                'isBusinessUnit' => $isBusinessUnit,
                'isTourism' => $isTourism,
                'idAviso' => $dealA->id,
                'slug' => $slug
            ));
        }
        ?>
        <?php if (Configure::read('web.ad_server') == 'dfp') : ?>
            <section id="cupones" class="ads-cupons">
                <h2>Nuestros Convenios&nbsp;</h2>
                <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_1x215x100', [215, 100]).display();
                        </script>
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_2x215x100', [215, 100]).display();
                        </script>
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_3x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_4x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                </div>
                <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_5x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_6x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_7x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                    <div style="display:inline-block; margin: 10px;">
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_NB_8x215x100', [215, 100]).display();
                        </script>                    
                    </div>
                </div>            
            </section>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'agea'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Agea_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'cmd'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CMD_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'cablevision'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_CableVision_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'radio-mitre'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Mitre_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'gestion-compartida'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Gestion_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'tyc-sports'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_TYC_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'diario-los-andes'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LosAndes_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'impripost-tecnologias'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Impripost_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'grupo-clarin-nb'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarinNB_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'cuspide'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_cuspide_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'grupo-clarin'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_GrupoClarin_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'la-voz-del-interior'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_LVI_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'pol-ka-producciones'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_POL_K_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
            <?php if ($slug == 'artear'): ?>
                <section id="cupones" class="ads-cupons">
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_1x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_2x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_3x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_4x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                    <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_5x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_6x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_7x215x100', [215, 100]).display();
                            </script>
                        </div>
                        <div style="display:inline-block; margin: 10px;">
                            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/CC_NB_Artear_8x215x100', [215, 100]).display();
                            </script>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <!-- ------------------------------------------------------------------------------------------- -->
        <?php endif; ?>
        <?php
        if ($showTiempoLibre) {
            echo $this->element('exclusives/adsList', array(
                'id' => "tiempoLibre",
                'title' => "Tu Tiempo Libre",
                'keywordEplanning' => $keywordEplanning,
                'pos' => 1,
                'isBusinessUnit' => $isBusinessUnit,
                'isTourism' => $isTourism,
                'deal' => $dealA
            ));
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                slider($('#nuestros_convenios_0'), 0);
    <?php if ($showTiempoLibre) { ?>
                    slider($('#tiempoLibre_1'), 1);
    <?php } ?>
            });
        </script>
    <?php endif; ?>
</div>