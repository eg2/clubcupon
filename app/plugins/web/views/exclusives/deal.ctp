<?php
/*
 * view: deal.ctp
 * @author: EParisi - Pward 
 * params: data
 */
?>

<?php
$dataObj = json_decode($data);

$user = $dataObj->user;
$slug = $dataObj->city->slug;
$isCorporate = $dataObj->city->isCorporate;
$isBusinessUnit = $dataObj->city->isBusinessUnit;

$dealList = $dataObj->vData->dealList;
$deal = $dataObj->vData->deal;

$fechaFinalizacion = $deal->endDate;
$coupon_start_date = $deal->couponStartDate;
$coupon_expiry_date = $deal->couponExpiryDate;

$isTourism = 'no';
if ($deal->isTourism) {
    $isTourism = 'si';
}

if ($isBusinessUnit) {
    $howBuyEmail = 'tucupon@nuestrosbeneficios.com';
} else {
    $howBuyEmail = 'tucupon@clubcupon.com.ar';
}

if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    if ($isBusinessUnit) {
        $srcImg = Configure::read('web.pluginDebugImages.dummyUDNDealUrlImage');
    } else {
        $srcImg = Configure::read('web.pluginDebugImages.dummyUDNDealUrlImageCC');
    }
} else {
    $srcImg = $deal->imageURL;
}

$ads = Configure::read('web.eplanningIds');

echo $this->element(
        'adBackground', array(
    'slug' => $slug,
    'isCorporate' => true,
    'isTourism' => $isTourism
        )
);
?>

<div class="maincontent centerdiv">

    <section id="detalle_fichas">
        <h1><?php echo $deal->title; ?></h1>
        <div id="comprar">
            <h3><?php echo $deal->subtitle; ?></h3>

            <?php if (!($deal->onlyPrice) && ($deal->hidePrice)): ?>
                <div class="precio">descuento <span><?php echo $deal->discountedPercent; ?>%</span></div>
            <?php else: ?>
                <div class="precio">precio <span>$<?php echo $deal->discountedPrice; ?></span></div>
            <?php endif; ?>

            <!-- boton comprar TODO: oferta finalizada y accion boton -->
            <?php if ($deal->isPurchasable == 1 && $user->userType != ConstUserTypes::Company): ?>
                <a class="botonExclusive" href="/<?php echo $slug; ?>/deals/buy/<?php echo $deal->id; ?>">Comprar</a>
            <?php endif; ?>

            <div class="descuento_valor">


                <?php if (!$deal->onlyPrice): ?>
                    <?php if (!$deal->hidePrice): ?>
                        <p>Descuento<span><?php echo $deal->discountedPercent; ?>%</span></p>
                        <p>Valor real<span>$<?php echo $deal->price; ?></span></p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <?php
            echo $this->element(
                    'dealCountdown', array(
                'fechaFinalizacion' => $fechaFinalizacion,
                'isCorporate' => $isCorporate
                    )
            );
            ?>

            <?php
            echo $this->element(
                    'dealPlace', array(
                'deal' => $deal
                    )
            );
            ?>	

            <?php
            echo $this->element(
                    'comoComprar', array(
                'email' => $howBuyEmail,
                    )
            );
            ?>
        </div>

        <div id="ficha">
            <!-- imagen de la oferta -->
            <?php if ($deal->isPurchasable == 0): ?>
                <div class="mensaje">
                    <h2>Chequeá si está vigente!</h2>
                    <h3>Usá el Buscador y conocé todas nuestras Ofertas</h3>
                    <?php /* <a href="#">Ayuda</a> */ ?>
                </div>
            <?php endif; ?>

            <img src="<?php echo $srcImg; ?>" alt="imagen cupon">

            <?php //echo $this->element('exclusives/formasPago'); ?>
            <?php
            //eplanning top1
            if (Configure::read('web.ad_server') == 'eplanning') {
                echo $this->element(
                        'ad', array(
                    'slug' => $slug,
                    'isTourism' => $isTourism,
                    'idAd' => $ads['top1'],
                    'idAviso' => $deal->id,
                        )
                );
            }
            ?>

            <!-- Coupon conditions -->
            <?php if (!empty($deal->condition)): ?>
                <h3>Condiciones</h3>
                <div><?php echo $deal->condition; ?></div>
            <?php endif; ?>
            <?php
            $vigencia = "";
            if ($deal->isVariableExpiration) {
                $vigencia = "Válido durante " . $deal->couponDuration . " días desde el momento de la compra";
            } else {
                $vigencia = "Válido para canjear desde el ";
                $coupon_start_date_cleaned = htmlentities(strftime("%d/%m/%Y", strtotime($coupon_start_date)), ENT_QUOTES, 'UTF-8');
                $coupon_expiry_date_cleaned = htmlentities(strftime("%d/%m/%Y", strtotime($coupon_expiry_date)), ENT_QUOTES, 'UTF-8');
                $vigencia.=$coupon_start_date_cleaned . ' hasta el ' . $coupon_expiry_date_cleaned;
            }
            ?>
            <p class="vigencia"><?php echo $vigencia; ?></p>

            <?php if (!empty($deal->description)): ?>
                <!--  descripcion -->
                <h3>Más información</h3>
                <div><?php echo $deal->description; ?></div>
            <?php endif; ?>

            <?php
            //eplanning bottom1
            if (Configure::read('web.ad_server') == 'eplanning') {
                echo $this->element(
                        'ad', array(
                    'slug' => $slug,
                    'isTourism' => $isTourism,
                    'idAd' => $ads['bottom1'],
                    'idAviso' => $deal->id
                        )
                );
            }
            ?>
        </div>
    </section>

    <?php
    echo $this->element(
            'sidebar', array(
        'slug' => $slug,
        'isTourism' => $isTourism,
        'isCorporate' => $isCorporate,
        'idAviso' => $deal->id,
        'isBusinessUnit' => $isBusinessUnit
            )
    );
    ?>	
    <hr>

    <section id="cupones">
        <?php
        echo $this->element(
                'exclusives/dealList', array(
            'slug' => $slug,
            'dealList' => $dealList,
            'title' => "M&aacute;s Ofertas",
            'isBusinessUnit' => $isBusinessUnit,
                )
        );
        ?>

    </section>

</div>  
<?php
echo $this->element('tracking_visitas');
?>  


