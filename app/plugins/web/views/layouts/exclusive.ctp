<?php 
/*
 * layout: exclusive.ctp
 * @author: EParisi - Pward 
 * params: -
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<?php 
    $title_for_layout = 'Exclusive';
    
    $data = $this->viewVars['data'];
    $dataObj = json_decode($data);

    $user = $dataObj->user;
    $city = $dataObj->city;
    $slug = $city->slug;
    $imageURL = $city->imageURL;
    $isBusinessUnit = $city->isBusinessUnit;
    
    echo $this->element('exclusives/headExclusive',
    		array(
				'isBusinessUnit' => $isBusinessUnit
		 ));
?>
    <body>
    	<?php 
        	if (Configure::read('app.rDebug.enable')) {
        	    rdebug('$dataObj', $dataObj);
        	}
            if (Configure::read('app.tracking.enable')) {
            	echo $this->element('trackingGoogle');
        		echo $this->element('tracking-dataexpand');
        	}
        	    	
    		echo $this->element(
    			'exclusives/headerExclusive',
    			array (
    				'slug' => $slug,
					'imageURL' => $imageURL,
					'isBusinessUnit' => $isBusinessUnit,
					'user' => $user
	    		)
    		); 
		?>
    	
    	<div id="contentBackground">
        <?php echo $content_for_layout; ?>
    	</div>
        
        <?php 
            echo $this->element(
                'exclusives/footerExclusive',
                array (
                    'slug' => $slug,
					'isBusinessUnit' => $isBusinessUnit,
                )
            ); 
        ?>
        
        <?php 
            //Tracker
            if (Configure::read('app.tracking.enable')) {
                echo '<div style="display:none">';
                echo $this->element('trackingWeb');
                echo '</div>';
            }
        ?>
    </body>
</html>