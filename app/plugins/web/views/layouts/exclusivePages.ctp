<?php 
/*
 * layout: exclusive.ctp
 * @author: EParisi - Pward 
 * params: -
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<?php 
    $title_for_layout = 'Exclusive';
    
    $slug = $this->getVar('citySlug');
    $imageURL = $this->getVar('imageURL');
    $isBusinessUnit = $this->getVar('isBusinessUnit');
    
    echo $this->element('exclusives/headExclusive',
    		array(
				'isBusinessUnit' => $isBusinessUnit
		 ));
?>
    <body>
    	<?php 
    		if (Configure::read('app.tracking.enable')) {
    			echo $this->element('trackingGoogle');
    			echo $this->element('tracking-dataexpand');
    		}
    		echo $this->element(
    			'exclusives/headerExclusive',
    			array (
    				'slug' => $slug,
					'imageURL' => $imageURL,
					'isBusinessUnit' => $isBusinessUnit,
                    'user' => $user
	    		)
    		); 
		?>
    	
        <?php echo $content_for_layout; ?>
        
        <?php 
            echo $this->element(
                'exclusives/footerExclusive',
                array (
                    'slug' => $slug
                )
            ); 
        ?>
        
        <?php 

                  
        ?>
    </body>
</html>

<script type="text/javascript">
<!--
$(document).ready(function() {
	$('.promo-precio').each(function(){
		var parent = $(this).parent();

		$(this).detach().appendTo(parent);

	});
});
//-->
</script>
<?php 
if (Configure::read('app.tracking.enable')) {
	echo $this->element('trackingWeb');
}
?>


