<?php 
/*
 * layout: clubCupon.ctp
 * @author: EParisi - Pward 
 * params: -
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<?php 
    $title_for_layout = 'Club Cupon';
    
    $data = $this->viewVars['data'];
    $dataObj = json_decode($data);
    
    $bannerMobile = $dataObj->vData->bannerMobile;
    
    $user = $dataObj->user;
    $city = $dataObj->city;
    $slug = $city->slug;
    $imageURL = $city->imageURL;
    $isBusinessUnit = $city->isBusinessUnit;
    
    $citiesLinkList = $dataObj->vData->citiesLinkList;
    $citiesLinks = $citiesLinkList->citiesLinks;
    $specialLinks = $dataObj->vData->specialLinkList;
    $extraLinks = $citiesLinkList->extraLinks;
    $enablePopup = $dataObj->vData->enablePopup;
    $selectedTab = $dataObj->vData->selectedTab;
    $session->flash();
    $dealA = $dataObj->vData->dealA;
    $og_data = $dataObj->vData->deal->ogData;
    $metaData = $dataObj->vData->metaData;
    
    echo $this->element(
        'clubcupon/headClubcupon',
        array (
            'metaData' => $metaData,
            'og_data' => $og_data
        )
    );
    
    //para ccya vista detalle deal
    $deal = $dataObj->vData->deal;
    
    $user_mail = $dataObj->user->userEmail;
    
    
?>
    <body>
    	<?php 
    	    //si viene de una subscripcion desde landingpre
    	    if(isset($this->params ['url']['landingpre']) && $this->params ['url']['landingpre'] == 'si'){
    	    	if ($session->check('Message.error'))  {$session->flash('error');}
				if ($session->check('Message.success')){$session->flash('success');}
				if ($session->check('Message.flash'))  {$session->flash();}	
    	    }
    		
    	
    	
    	
        	if (Configure::read('app.rDebug.enable')) {
        	    rdebug('dataObj', $dataObj);
        	}
            if (Configure::read('app.tracking.enable')) {
            	echo $this->element('trackingGoogle');
        		echo $this->element('tracking-dataexpand');
        	}
        	
        	$hideSearchBar = false;
        	if($deal->city->slug == Configure::read('City.clubcuponya')){
				$hideSearchBar = true;
			}
    	
    		echo $this->element(
    			'clubcupon/headerClubcupon',
    			array (
    				'user' => $user,
    				'city' => $city,
					'imageURL' => $imageURL,
					'isBusinessUnit' => $isBusinessUnit,
					'citiesLinks' => $citiesLinks,
					'specialLinks' => $specialLinks,
    				'extraLinks' => $extraLinks,	
    				'dealA' => $dealA,
    				'selectedTab'=>$selectedTab,
					'hideSearchBar' => $hideSearchBar,
	    		)
    		);
    	
    		echo $this->element(
    			'adBackground',
    			array (
    				'slug' => $slug,
    				'isCorporate' => true,
    				'isTourism' => ''
	    		)
    		); 
		?>
    	
    	<div id="contentBackground">
        	<?php echo $content_for_layout; ?>
    	</div>
        	<?php echo $this->element('clubcupon/jsEplanningHome');?> 
        <?php if(Configure::read('web.bannermobileHtml5.enable')):?>
         	<?php
         		echo $this->element('clubcupon/bannermobile',array('bannerMobile'=>$bannerMobile));
         	?>
        <?php else:?>
         	<?php
         		//echo $this->element('clubcupon/tagEplanningProduct5');
         	?>
        <?php endif;?>
         
	
				
		
        <?php 
            echo $this->element(
                'clubcupon/footerClubcupon',
                array (
    				'slug' => $slug,
                )
            ); 
        ?>
        
         
       <?php echo $this->element('clubcupon/tagCxense',
        array (
            'user_mail' => $user_mail,
        ));?>
       
       
    </body>
    
<?php
    if ($enablePopup) {
?>
<script>
    $(document).ready(function(){
					 <?php
        					$popup_w = 662; // ancho default
        					$popup_h = 650; // alto default
        					$popup = array ();
        					$popup ['controller'] = 'firsts';
        					$popup ['action']     = 'index';
        					$popup ['city']     = Configure::read ('Actual.city_slug');
        					switch (Configure::read ('Actual.city_slug')) {
				            // popup para circa
            					case 'circa':
                					$popup = null; break;
            					default: break;
        					}

        					$parametros='';
        					if(isset($_GET['utm_source']) && $_GET['utm_source']!=''){
            					$parametros ='utm_source='.$_GET['utm_source'];
        					}
        					if(isset($_GET['utm_medium']) && $_GET['utm_medium']!=''){
            					$parametros .='&utm_medium='.$_GET['utm_medium'];
        					}
        					if(isset($_GET['utm_term']) && $_GET['utm_term']!=''){
            					$parametros .='&utm_term='.$_GET['utm_term'];
        					}
        					if(isset($_GET['utm_content']) && $_GET['utm_content']!=''){
            					$parametros .='&utm_content='.$_GET['utm_content'];
        					}
        					if(isset($_GET['utm_campaign']) && $_GET['utm_campaign']!=''){
            					$parametros .='&utm_campaign='.$_GET['utm_campaign'];
        					}

        					if(count ($popup) != 0) {
            					echo 'popupWelcome(\'/' . $slug . '/popup' .'?'.$parametros.'\', ' . $popup_w . ', ' . $popup_h . ', function () { })';
        					}
        			?>
  });
</script>
<?php } ?>
<script id="nvg_rt" type="text/javascript" src="//navdmp.com/req?acc=23111&cus=101911"></script>
</html>

<script type="text/javascript">
<!--
$(document).ready(function() {
	$('.promo-precio').each(function(){
		var parent = $(this).parent();

		$(this).detach().appendTo(parent);

	});
});
//-->
</script>

<?php 
 if (Configure::read('app.tracking.enable')) {
 	echo '<div style="display:none">';
     echo $this->element('trackingWeb',array('view'=>$dataObj->vData->viewType,'citiSlug'=>$slug));
    echo '</div>';
 }
 ?>
 

