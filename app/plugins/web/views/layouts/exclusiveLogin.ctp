<?php 
/*
 * layout: exclusiveLogin.ctp
 * @author: EParisi - Pward 
 * params: -
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<?php 
    $title_for_layout = 'Exclusive Login';

    $data = $this->viewVars['data'];
    $dataObj = json_decode($data);
    $slug = $dataObj->city->slug;

    echo $this->element(
        'exclusives/headLogin',
        array(
            'isBusinessUnit' => $dataObj->city->isBusinessUnit,
        )
    );
    if ($dataObj->city->isBusinessUnit == 1) {
        $bodyClass = '';
    } else { 
        if ($slug == 'mcdonalds') {
            $bodyClass = 'arcosCorp';
        } else {
            $bodyClass = 'notUDN';
        }
    }
?>
    <body id="content_login" <?php echo 'class="'.$bodyClass.'"'; ?>>
    	<?php 
        	if (Configure::read('app.rDebug.enable')) {
        	    rdebug('$dataObj', $dataObj);
        	}
            if (Configure::read('app.tracking.enable')) {
            	echo $this->element('trackingGoogle');
        		echo $this->element('tracking-dataexpand');
        	}
	    ?>
    
        <?php echo $content_for_layout; ?>
        
        <?php 
            //Tracker
            if (Configure::read('app.tracking.enable')) {
                echo '<div style="display:none">';
                echo $this->element('trackingWeb');
                echo '</div>';
            }
        ?>
    </body>
</html>
