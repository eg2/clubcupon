<?php
	 //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $head_parameters = array ('css_layout' => 'popup'); //para determinar que hoja de estilos levantamos
    echo $this->element('clubcupon/headpopup',$head_parameters ); // nuevo

    if ($session->check('Message.error')):
        $session->flash('error');
    endif;

    if ($session->check('Message.success')):
        $session->flash('success');
    endif;

    if ($session->check('Message.flash')):
        $session->flash();
    endif;
    ?>
<body>
	<?php 
	if (Configure::read('app.tracking.enable')) {
		echo $this->element('trackingGoogle');
	}
    echo $content_for_layout;
    echo $cakeDebug;
    ?>
</body>
</html>
<?php 
if (Configure::read('app.tracking.enable')) {
	echo $this->element('trackingWeb');
}
?>