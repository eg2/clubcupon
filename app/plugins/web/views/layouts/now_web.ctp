<?php 
/*
 * layout: now_web.ctp
 * @author: Pward 
 * params: -
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<?php 
    $title_for_layout = 'Club Cupon Ya';
    
    $data = $this->viewVars['data'];
    $dataObj = json_decode($data);

    $user = $dataObj->user;
    $city = $dataObj->city;
    $slug = $city->slug;
    $imageURL = $city->imageURL;
    $isBusinessUnit = $city->isBusinessUnit;
    
    $citiesLinkList = $dataObj->vData->citiesLinkList;
    $citiesLinks = $citiesLinkList->citiesLinks;
    $specialLinks = $dataObj->vData->specialLinkList;
    $extraLinks = $citiesLinkList->extraLinks;
    $enablePopup = $dataObj->vData->enablePopup;
    $categories = $dataObj->vData->categories;
    $selectedTab = $dataObj->vData->selectedTab;
    $session->flash();
    $metaData = $dataObj->vData->metaData;

    echo $this->element(
        'now/head_web',
        array (
            'metaData' => $metaData
        )
    );
?>
    <body>
    	<?php 
        	if (Configure::read('app.rDebug.enable')) {
        	    rdebug('$dataObj', $dataObj);
        	}
            if (Configure::read('app.tracking.enable')) {
            	echo $this->element('trackingGoogle');
        		echo $this->element('tracking-dataexpand');
        	}
    	
    		echo $this->element(
    			'clubcupon/headerClubcupon',
    			array (
    				'user' => $user,
    				'city' => $city,
					'imageURL' => $imageURL,
					'isBusinessUnit' => $isBusinessUnit,
					'citiesLinks' => $citiesLinks,
					'specialLinks' => $specialLinks,
    				'extraLinks' => $extraLinks,
					'hideSearchBar' => true,
    				'selectedTab' =>$selectedTab
	    		)
    		);
    	
//     		echo $this->element(
//     			'adBackground',
//     			array (
//     				'slug' => $slug,
//     				'isCorporate' => true,
//     				'isTourism' => ''
// 	    		)
//     		); 
		?>
		<?php if ($this->action == 'index') { ?>
    		<div id=now-container>
    		<?php // banner con link?>
    	    <section id="banner">
            <img src="/img/web/clubcupon/banner_ccya.jpg">
            <div id="con_banner_link">
                <a id="banner_link" class="banner_link" href="/now/now_registers/landingus">Conocé más sobre Club Cupón Ya! <em style=" color: #148caf; font-size:14px; font-family: times, serif; font-weight: normal;">  &#9658; </em></a>
            </div>
    			</section>
    			
    			<?php echo $this->element('now/searchbox_web',
    					array(
    							'categories'=>$categories
    							)); ?>
    			
    	    	<?php // echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'searchbox'); ?>
    			<?php // echo $this->element(Configure::read('NowPlugin.theme.theme_name') . 'categoriesbar'); ?>
            </div>
        <?php } ?>
		<div id="content" class="now-content">	    	
	        <?php echo $content_for_layout; ?>
	    </div>    
        
        <?php 
            echo $this->element(
                'clubcupon/footerClubcupon',
                array (
    				'slug' => $slug,
                )
            ); 
        ?>
        
        
    </body>
    
<?php
    if ($enablePopup) {
?>
<script>
    $(document).ready(function(){

    <?php
        $popup_w = 662; // ancho default
        $popup_h = 512; // alto default
        $popup = array ();
        $popup ['controller'] = 'firsts';
        $popup ['action']     = 'index';
        $popup ['city']     = Configure::read ('Actual.city_slug');

        switch (Configure::read ('Actual.city_slug')) {

            // popup para circa
            case 'circa':
                $popup = null; break;
            default: break;
        }

        $parametros='';
        if(isset($_GET['utm_source']) && $_GET['utm_source']!=''){
            $parametros ='utm_source='.$_GET['utm_source'];
        }
        if(isset($_GET['utm_medium']) && $_GET['utm_medium']!=''){
            $parametros .='&utm_medium='.$_GET['utm_medium'];
        }
        if(isset($_GET['utm_term']) && $_GET['utm_term']!=''){
            $parametros .='&utm_term='.$_GET['utm_term'];
        }
        if(isset($_GET['utm_content']) && $_GET['utm_content']!=''){
            $parametros .='&utm_content='.$_GET['utm_content'];
        }
        if(isset($_GET['utm_campaign']) && $_GET['utm_campaign']!=''){
            $parametros .='&utm_campaign='.$_GET['utm_campaign'];
        }

        if(count ($popup) != 0) {
            echo 'popupWelcome(\'/' . $slug . '/popup' .'?'.$parametros.'\', ' . $popup_w . ', ' . $popup_h . ', function () { })';
        }
        ?>
  });
</script>
<?php } ?>

</html>

<?php 
 if (Configure::read('app.tracking.enable')) {
 		echo $this->element('trackingWeb');
 }
?>