<script>
    $(document).ready(function() {
	
        window.fbAsyncInit = function() {
            var api_key=__cfg['api_key'];
            var channel='<?php echo STATIC_DOMAIN_FOR_CLUBCUPON; ?>';

            FB.init({
                appId: api_key, // App ID
                channelUrl: channel, // Channel File
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true, // parse XFBML
                version    : 'v2.6'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));

        function doFacebookLogin() {
            fbAsyncInit();
            FB.login(function(response) {
                 if (response.authResponse) {
                	<?php if ( !empty($subscribe)){?>
                			var city = document.getElementById('SubscriptionCitySlug').value;
                			
                			parent.window.location.href = '/<?php echo Configure::read('web.default.citySlug'); ?>/addsubscription?isFacebookLogin=true&selectedCity='+city;
                	<?php }else {?>
                    		window.location.href = '/users/login?isFacebookLogin=true';
                    <?php }?>
                }
            }, {
                scope: 'email'
            });
        }

        $('#facebookLoginButton').click(function() {
            doFacebookLogin();
        });

    });
</script>