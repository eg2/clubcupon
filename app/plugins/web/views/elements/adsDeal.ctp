<?php
/*
 * element: adsDeal.ctp
 * @author: Pward 
 */
if (Configure::read('web.ad_server') == 'eplanning'){
    $ads = Configure::read('web.eplanningIds');
    $dealAds = $ads['dealAds'];
    $max = count($dealAds);
}
?>
<div class="tiempo_libre">
    <ul>
        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
        <li>
            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                googletag.pubads().definePassback('/261718750/ClubCupon_Ficha_Right', [218, 179]).display();
            </script>
        </li>
        <li>
            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                googletag.pubads().definePassback('/261718750/ClubCupon_Ficha_R1ofertax218', [220, 400]).display();
            </script>
        </li>
        <?php endif; ?>
        <?php
        if (Configure::read('web.ad_server') == 'eplanning'){
		if ($isCorporate) {
            $mx = $maxCorp;
            $array = $dealAdsCorp;
		}else {
            $mx = $max;
            $array = $dealAds;
		}
		for ($i=0;$i<$mx;$i++){
			echo "<li>";
			echo $this->element(
			'ad',
				array (
					'slug' => $slug,
					'isTourism' => $isTourism,
					'idAd' => $array[$i],
					'idAviso' => $idAviso,
				)
			);
			echo "</li>";
			echo "</li>";
		}
        }
        ?>
    </ul>
</div>


