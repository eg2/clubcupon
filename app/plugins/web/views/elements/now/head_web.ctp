<?php 
/*
 * element: head_web.ctp
 * @author: Pward 
 * 
 */
?>

<head>
    <title>
        <?php
            if(preg_match('/'.Configure::read('site.name').'/', $metaData->title)) {
                $title = Configure::read('site.name') . ' | ' .$html->cText($metaData->title, false);
            } else {
                $title = Configure::read('site.name') . ' | ' . $html->cText($metaData->title, false);
            }
            echo $title;
        ?>
    </title>

	<meta charset="utf-8" />
    <meta name="description" content="<?php echo $metaData->description; ?>"/>

    <?php echo $html->meta('keywords', $metaData->keywords), "\n"; ?>

	<link rel="shortcut icon" href="/favicon.png" />
	<link rel="alternate" title="RSS" type="application/rss+xml" href="/feed.rss" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
	
	<?php echo $html->css(Configure::read('web.asset_version').'clubcupon/style.css'); ?>
	<?php echo $html->css(Configure::read('web.asset_version').'/now/global.css'); //parte de global.css de plugin now antiguo puesto por compatibilidad ?>
	<?php echo $html->css(Configure::read('web.clubcupon.asset_version').'colorbox'); ?>
	<?php echo $html->css(Configure::read('search.asset_version').'jquery-ui-1.10.3');   //autocomplete Search ?>
	
	<?php 
		echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true));
		echo $javascript->codeBlock('var NowPlugin_calendar_minium_date = ' .Configure::read('NowPlugin.calendar_minium_date'));
	?>
	
	<?php echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.7.1.min'); ?>
	<?php echo $javascript->link(Configure::read('search.asset_version').'jquery-ui-1.8.18.min'); ?>	
	<?php echo $javascript->link(Configure::read('now.asset_version').'jquery.colorbox.js'); ?>
	<?php echo $javascript->link(Configure::read('libs.asset_version').'jquery.livequery'); ?>
	<?php echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.cookie'); ?>
 	<?php echo $javascript->link(Configure::read('web.asset_version').'custom.js'); ?>
	
	<?php 
		echo $javascript->link('/now/js/common.js?v=7');
	 	echo $javascript->link('http://maps.google.com/maps/api/js?sensor=false&libraries=geometry');
	 	
		echo $javascript->link(Configure::read('web.now.asset_version') . 'city-search.js');
		echo $javascript->link(Configure::read('now.asset_version').'funciones.js');
	?>
 	
 	
 	
 	
 	<!--[if lt IE 9]>
	<script type="text/javascript">
		document.createElement("nav");
		document.createElement("header");
		document.createElement("footer");
		document.createElement("section");
		document.createElement("article");
		document.createElement("aside");
		document.createElement("hgroup");
	</script>
<![endif]-->		
</head>
