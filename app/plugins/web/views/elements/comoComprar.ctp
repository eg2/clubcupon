<?php 
/*
 * element: comoComprar.ctp
 * @author: Pward 
 * params: -
 */
?>
<div id="pasos_compra">
	<h3>Cómo comprar el cupón</h3>
	<p><span>1</span> Hacé click en el botón “comprar”</p>
	<p><span>2</span> Completá el registro y elegí el medio de pago que quieras</p>
	<p><span>3</span> Acreditado el pago, recibirás por mail el cupón que confirma tu compra.</p>
	<p><span>4</span> Cuando tengas tu cupón lee las condiciones para el canje del mismo.</p>
	<p>Agregá a tu lista de contactos <?php echo $email; ?> para que tu cupón no llegue a correo no deseado.</p>
</div>

