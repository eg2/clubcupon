<?php if(Configure::read('web.ad_server') == 'eplanning'): ?>
<script>
    $(document).ready(function() {
    	var randNumber = (new String(Math.random())).substring(2,8) + (((new Date()).getTime()) & 262143);
    <?php 
        $adID = Configure::read('web.eplanningIds');
        if ($isCorporate) { ?>
        var imageUrl = 'http://ads.e-planning.net/eb/3/9b30/<?php echo $adID['backgroundCorporate']; ?>?o=i&rnd='+randNumber+'&kw_ciudades=<?php echo $slug; ?>&kw_turismo=<?php echo $isTourism; ?>';
    <?php }else{ ?>
        var imageUrl = 'http://ads.e-planning.net/eb/3/9b30/<?php echo $adID['backgroundNotCorporate']; ?>?o=i&rnd='+randNumber+'&kw_ciudades=<?php echo $slug; ?>&kw_turismo=<?php echo $isTourism; ?>';
    <?php } ?>
    $("#contentBackground").css('background-image', 'url(' + imageUrl + ')');
    });
</script>
<?php endif; ?>