<?php
$comScorePath = 'clubcupon';

if (isset($view) && !empty($view)) {
    switch ($view) {
        case WebViewDataCreatorComponent::DEAL_DETAIL_CC:
            $comScorePath.='.' . $citiSlug . '.oferta';
            break;

        case WebViewDataCreatorComponent::CITY_HOME_CC:
            $comScorePath.='.' . $citiSlug;
            break;

        case WebViewDataCreatorComponent::FORM_USER_REGISTER:
            $comScorePath.='.registracion';
            break;
        case WebViewDataCreatorComponent::SOLR_RESULTS:
            $comScorePath.='.resultado-de-busqueda';
            break;
        case WebViewDataCreatorComponent::USER_LOGIN:
            $comScorePath.='.login';
            break;

        default: $comScorePath.='.otros';
            break;
    }
} else {
    if (isset($isOrder) && $isOrder) {
        $comScorePath.='.compra';
    } else {
        $comScorePath.='.otros';
    }
}
?>
<!-- ABR14 -->
<!-- tracking comscore -->
<script language="JavaScript1.3" src="http://b.scorecardresearch.com/c2/6906409/ct.js"></script>


<!-- Tracking de Google Analytics (General) [BEGIN] -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-4436008-37', 'clubcupon.com.ar');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');

</script>
<!-- Tracking de Google Analytics (General) [END] -->

<!-- Google Code for Remarketing VIa Resto Remarketing List -->
<script type = "text/javascript"  src = "http://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style = "display: inline;">
    <img height = "1"  width = "1"  style = "border-style: none;"  alt = ""  src = "http://www.googleadservices.com/pagead/conversion/981091258/?label=GCQ5CJ6HoQIQuofp0wM&amp;guid=ON&amp;script=0" />
</div>
</noscript>
<!-- Tracking de Google Analytics (General - Bis) [END] -->

<!-- Begin comScore UDM code 1.1104.26 -->

<script type="text/javascript">

// <![CDATA[

    function comScore(t) {
        var b = "comScore", o = document, f = o.location, a = "", e = "undefined", g = 2048, s, k, p, h, r = "characterSet", n = "defaultCharset", m = (typeof encodeURIComponent != e ? encodeURIComponent : escape);
        if (o.cookie.indexOf(b + "=") != -1) {
            p = o.cookie.split(";");
            for (h = 0, f = p.length; h < f; h++) {
                var q = p[h].indexOf(b + "=");
                if (q != -1) {
                    a = "&" + unescape(p[h].substring(q + b.length + 1))
                }
            }
        }
        t = t + "&ns__t=" + (new Date().getTime());
        t = t + "&ns_c=" + (o[r] ? o[r] : (o[n] ? o[n] : "")) + "&c8=" + m(o.title) + a + "&c7=" + m(f && f.href ? f.href : o.URL) + "&c9=" + m(o.referrer);
        if (t.length > g && t.indexOf("&") > 0) {
            s = t.substr(0, g - 8).lastIndexOf("&");
            t = (t.substring(0, s) + "&ns_cut=" + m(t.substring(s + 1))).substr(0, g)
        }
        if (o.images) {
            k = new Image();
            if (typeof ns_p == e) {
                ns_p = k
            }
            k.src = t
        } else {
            o.write('<p><' + 'img src="' + t + '" height="1" width="1" alt="*"></p>')
        }
    }
    ;

    comScore('http' + (document.location.href.charAt(4) == 's' ? 's://sb' : '://b') + '.scorecardresearch.com/p?c1=2&c2=6906409&amp;ns_site=club-cupon&name=<?php echo $comScorePath; ?>');
// ]]>

</script>

<noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906409&amp;

                  ns_site=club-cupon&amp;name="<?php echo $comScorePath ?>" height="1" width="1" alt="*"></p></noscript>

</script>

<!-- End comScore UDM code -->

<?php if (preg_match('/turismo/s', $this->params['url']['url'])): ?>
    <!-- Google Code for Viajes y Turismo Remarketing List -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 981091258;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "tbWDCParpAMQuofp0wM";
        var google_conversion_value = 0;
        /* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/981091258/?label=tbWDCParpAMQuofp0wM&guid=ON&script=0"/>
    </div>
    </noscript>
    <!-- Google Code for Viajes y Turismo Remarketing List -->
<?php endif; ?>
<!-- Google Code for Smart Pixel - Gen&eacute;rico -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 981091258;
    var google_conversion_label = "GTQ4CI6niAcQuofp0wM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<div style="display:none">
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
</div>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/981091258/?value=0&amp;label=GTQ4CI6niAcQuofp0wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
