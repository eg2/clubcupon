<?php
/*
 * element: sidebar.ctp
 * @author: Pward 
 * params: - dealListSearched - slug - isCorporate - isTourism - deal - isBusinessUnit
 */
?>
<aside id="sidebar">
    <?php
    echo $this->element(
            'adsDeal', array(
        'slug' => $slug,
        'isCorporate' => $isCorporate,
        'isTourism' => $isTourism,
        'idAviso' => $idAviso
            )
    );
    ?> 
    <?php
    $isWidgetEnable = Configure::read('CXENSE.is_widget_enable');


    if ($isWidgetEnable == 1) {

        if (!$isBusinessUnit) {
            echo $this->element(
                    'clubcupon/mostViewedCxense', array(
                'slug' => $slug,
                    )
            );
        }
    } else {

        if (!$isBusinessUnit) {
            echo $this->element(
                    'clubcupon/mostViewed', array(
                'slug' => $slug,
                'dealListSearched' => $dealListSearched,
                'width' => $width,
                'height' => $height,
                    )
            );
        }
    }
    ?> 
</aside>
