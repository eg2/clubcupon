<?php 
/*
 * element: exclusives/deal.ctp
 * @author: Pward 
 * paramos: deal - slug - 
 */

// $title = $banner->title;
$title = $banner->title;
$titleShort = cutText($title, 70);
$title = htmlspecialchars($title);
$cSlug = $banner->city->slug;

if ($banner->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    $srcImg = Configure::read('web.pluginDebugImages.dummyListElementUrlImage');
} else {
    $srcImg = $banner->imageURL;
}

?>
<style>
.image-banner
{    
    position:absolute;
    left:0;
    top:0;
}
.title-banner
{
    z-index:100;
    position:absolute;    
    color: #454545;
    font-size: 15px;
    left:10px;
    top:148px;
}
</style>
<script>
			$(document).ready(function(){
				/*
				$(".banner-deal").click (function (e) {
					window.scrollTo(0,0);
					e.preventDefault();    
					goTo = $(this).attr("href");
		        	popupBanner (goTo, '100%', '85%','Cerrar');
		        	return false;
		        });
		        */
			});
			var popupBanner = function(url, width, height, onClose='Cerrar') {
				
			 	if(!isMobile.any()){

				  if(typeof(width) == "undefined" || width == null) {
				    width = _configuration.popupsDefault.width;
				  }

				  if(typeof(height) == "undefined" || height == null) {
				    height = _configuration.popupsDefault.height;
				  }

				  var configColorbox = {
				    escKey: true,
				    /*overlayClose: false,*/
				    close: onClose,
				    href: url,
				    iframe: true,
				    width: width,
				    height: height,
				    /*closeButton:true,
				    onClosed: function () {$.cookie('fullscreenClose', 1);}
				    */
				  };

				  $.fn.colorbox(configColorbox);
			 	}
				return false;
			};
		</script>
<article>
	<a href="<?php echo $banner->slug; ?>" title="<?php echo $title; ?>" class="banner-deal" target="_blank">
		<img src="<?php echo $srcImg; ?>" alt="cupon" width="215px" height="275px" class="image-banner">
		<span class="title-banner"><?php echo $titleShort; ?></span>
	</a>
</article>


