<?php 
/*
 * element: mainDeal.ctp
 * @author: Pward 
 * params: deal - slug 
 */
?>
<?php 
	$title = $deal->title;
// 	$description = $deal->description;
	$titleShort = cutText($title, 85);
	$title = htmlspecialchars($title);
// 	$description = cutHtml($description, 38, 60);

    if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
	    $srcImg = Configure::read('web.pluginDebugImages.dummyFirstElementUrlImage');
	} else {
	    $srcImg = $deal->imageURL;
	}
?>
<?php if (!empty($deal)): ?>
	<article id="destacado">
		<a href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>"><img src="<?php echo $srcImg; ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>"></a>
		<div class="content">
			<div style="width:540px;height:60px;overflow:hidden" title="<?php echo $title; ?>">
			<h3><?php echo $titleShort; ?></h3>
			</div>
			<?php /*
			<p><?php echo $description; ?></p>
			*/?>
		<?php if (!$deal->onlyPrice): ?>
			<?php if (!$deal->hidePrice): ?>			
			<p class="precio"><span>$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></p>
			<?php else: ?>
			<p class="precio"><?php echo $deal->discountedPercent; ?>%</p>
			<?php endif; ?>
		<?php else: ?>
			<p class="precio">$<?php echo $deal->discountedPrice; ?></p>
		<?php endif; ?>
			<a class="btn-naranja" href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>">Ver oferta</a>
		</div>
		
		<div class="banner">
                    <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
                    <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                        googletag.pubads().definePassback('/261718750/ClubCupon_Home_PromoTop2', [700, 29]).display();
                    </script>
                    <?php endif; ?>
			<?php 
                        if (Configure::read('web.ad_server') == 'eplanning'){
	    		echo $this->element(
	    			'adNormal',
	    			array (
	    				'idAd' => $ads['homePromoTop2'],
	    				'isTourism' => $isTourism,
	    				'idAviso' => $deal->id,
	    				'slug' => $slug,
						'width' => '700',
						'height' => '73',
	    			)
	    		);
                        }
		    ?>
	    </div>
	</article>
<?php endif;?>



