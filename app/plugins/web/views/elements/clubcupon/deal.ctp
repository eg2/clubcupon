<?php 
/*
 * element: exclusives/deal.ctp
 * @author: Pward 
 * paramos: deal - slug - 
 */

// $title = $deal->title;
$title = $deal->title;
$titleShort = cutText($title, 70);
$title = htmlspecialchars($title);
$cSlug = $deal->city->slug;

if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    $srcImg = Configure::read('web.pluginDebugImages.dummyListElementUrlImage');
} else {
    $srcImg = $deal->imageURL;
}

?>

<article>
	<a href="/<?php echo $cSlug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>" title="<?php echo $title; ?>">
		<img src="<?php echo $srcImg; ?>" alt="cupon" width="<?php echo $width; ?>" height="<?php echo $height; ?>" >
		<span class="title"><?php echo $titleShort; ?></span>
		<div class="ubicacion">
		<?php if(($deal->tsInicio != null)&&($deal->tsFin != null)){
				$tsInicio = date("H", strtotime($deal->tsInicio));
				$tsFin = date("H", strtotime($deal->tsFin));
				echo '<span>'.$deal->company->name.'<br><img src="/img/web/clubcupon/reloj_2.png"/> de '.$tsInicio.'Hs a '.$tsFin.'Hs</span>';
		}else{?>
			<span><?php echo $deal->company->name; ?>
				<?php if (!empty($deal->customCompanyAddress2)): ?>
				<br>
				<img src="/img/web/clubcupon/pick.png" />&nbsp;<?php echo $deal->customCompanyAddress2; ?>
				<?php endif;?>
			</span>
		<?php }?>
		</div>
	<?php if (!$deal->onlyPrice): ?>
		<?php if (!$deal->hidePrice): ?>			
		<span class="price"><span class="oldPrice">$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></span>
		<?php else: ?>
		<span class="price"><?php echo $deal->discountedPercent; ?>%</span>
		<?php endif; ?>
	<?php else: ?>
		<span class="price">$<?php echo $deal->discountedPrice; ?></span>
	<?php endif; ?>
	</a>
</article>


