<?php ?>
<script type="text/javascript">
    <?php echo 'var __cfg = ' . $javascript->object($js_vars_facebook['cfg']) . ';'; ?>
</script>
<script>
    $(document).ready(function() {
        var submitObj = document.getElementById('suscript');
        verifyEmail();
        $("#SubscriptionEmail").bind({
            'click': function(){
                if ($(this).val()=='Ingresá tu mail') {
                    $(this).val('');
                }
            },
            'blur': function(){
                if($(this).val()==''){
                    $(this).val('Ingresá tu mail');
                }
            }
        });
        function verifyEmail()
        {
            var status = false;
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if ($('#SubscriptionEmail').val().search(emailRegEx) == -1)
            {
				$("#suscript").addClass('compraroff');
                submitObj.disabled = true;
            }
            else
            {
                $("#suscript").removeClass('compraroff');
				submitObj.disabled = false;
                status = true;
            }
            return status;
        }
        $("#SubscriptionEmail").keyup(function() {;
            verifyEmail();
        });

    });  //fin doc ready

</script>
<section id="suscripcion" class="detailFBSuscrip">
	<div style="float:left;width: 100%;">
			<h3>No te pierdas otra oferta<span>Recibí alertas por Mail</span></h3>
			<form class="normal" enctype="multipart/form-data" id="SubscriptionAddForm" method="post" action="/ciudad-de-buenos-aires/addsubscription" target="_top">
			<fieldset style="display:none;">
				<input type="hidden">
				<input type="hidden">
			</fieldset>	        
			
			<input name="SubscriptionEmail" type="text" placeholder="Ingresá tu mail" id="SubscriptionEmail">
			<select name="SubscriptionCitySlug" id="SubscriptionCitySlug">
					<?php 
					foreach ($citiesLinks as $cityLink){
						echo '<option value="'.substr($cityLink->url, 1).'"';
						if (substr($cityLink->url, 1) == $slug) {
						    echo ' selected="selected"';
						}
						echo '>'.$cityLink->label.'</option>';
					}
					?>
			</select>
			
			<?php
			 
			if($isCaptchaNecesary){
				echo $this->element('clubcupon/captcha'); 
			}
			?>
			
			<input id="suscript" class="suscription compraroff" type="submit" value="Suscribirme!">
			<fieldset style="display:none;">
				<input type="hidden">
			</fieldset>
			</form>
		</div>
		<div style="border:1px solid #ededed;float:left">
			<div id="labelFBAcount">
				<p>¿Tenés cuenta de Facebook? Usala para suscribirte en ClubCupón</p>
			</div>
			<div id="fbBtnDealDetailCont">
	    		<a id="facebookLoginButton" title="fconnect" rel="nofollow" class="suscribirme fbBtnDealDetail">Suscribirme con Facebook</a>
		    </div>
		</div>
		
	</section>
