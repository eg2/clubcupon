<?php
/*
 * element: clubcupon/footerClubcupon.ctp
 * @author: Pward 
 * 
 */
?>
<footer>
    <div id="gris" style="height: 100px;">
        <div>
            <img src="/img/web/clubcupon/logo_footer.png" alt="logo ClubCupón" style="padding-top:20px;">
            <ul class="menu_footer">
                <li><a href="/<?php echo $slug; ?>/pages/who">Quiénes Somos</a></li>
                <li>|</li>
                <li><a href="http://soporte.clubcupon.com.ar/">Centro de Atención al Cliente</a></li>
                <li>|</li>
                <li><a href="/exclusive">Exclusive</a></li>
                <li>|</li>
                <li><a href="/<?php echo $slug; ?>/pages/concurso">Concursos y Promociones</a></li>
            </ul>
            <ul style="padding:0px !important;margin-left:10px;">
                <li style="vertical-align: middle;">
                    <?php echo $this->element('clubcupon/homeLogoFooter'); ?>
                </li>
            </ul>
            <ul class="redes">
                <li><a href="http://www.facebook.com/ClubCupon" class="icon_facebook" target="_blank" rel="nofollow">Facebook</a></li>
                <li><a href="/xmldeals/rss/<?php echo $slug; ?>" class="icon_rss" target="_blank" rel="nofollow">rss</a></li>
            </ul>
        </div>
    </div>
    <div id="negro">
        <ul>
            <li><a href="/<?php echo $slug; ?>/pages/terms">Términos y Condiciones</a></li>
            <li>|</li>
            <li><a href="/<?php echo $slug; ?>/pages/policy">Políticas de Privacidad</a></li>
            <li>|</li>
            <li><a href="/<?php echo $slug; ?>/pages/protection">Protección de Datos Personales</a></li>
            <li>|</li>
            <li><a href="http://www.buenosaires.gob.ar/defensaconsumidor">Dirección General de Defensa y Protección al Consumidor</a></li>
        </ul>

        <div>
            <p>Copyright © <?php echo date("Y"); ?> Derechos Reservados.</p>
            <img src="/img/web/clubcupon/logo_clarin.png" alt="logo Grupo Clarin"> 
            <a target="_F960AFIPInfo" href="http://qr.afip.gob.ar/?qr=aaBUq_tGRAX3UzK3EQuMmg,,">
                <img width="62" border="0" src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg">
            </a>
        </div>
    </div>

</footer>

<?php
//FACEBOOK  
if (
        $this->params['plugin'] == 'order' &&
        $this->params['controller'] == 'order_processings' &&
        $this->params['action'] == 'buy'
) {
    ?>
    <div id="fb-root"></div>

    <?php echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true)); ?>
    <script>/* la usa facebook*/
        function __cfg(c) {
            return(cfg && cfg.cfg && cfg.cfg[c]) ? cfg.cfg[c] : false;
        }
    </script>
    <?php echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection'); ?>
    <?php
}
?>

