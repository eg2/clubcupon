<?php
$img_url_background='';
$url_background='';
if(isset($bannerMobile)){
	$img_url_background=$bannerMobile->SearchLinks->image;
	$url_background=$bannerMobile->SearchLinks->url;
}

?>
<script type="text/javascript">
$(document).ready(function() {

	$("#contenedor").attr("src","<?php echo $img_url_background;?>");
	$("#linkContenedor").attr("href","<?php echo $url_background;?>"); 
	
	if(jQuery.browser.mobile){

		if( $("#contenedor").attr("src")!="") {
			$('#flotante').css('display','block');
		}
		$('#close-flotante').on('click', function(e) { 
        	$('#flotante').remove(); 
    	});
		
	}

		
});

</script>
<div id="flotante" style="position:fixed;bottom:0;width:100%;text-align:center;background:#f6f6f6;z-index:999;display:none">
	<span id='close-flotante' style="float:right;display:inline-block;"><img src="/img/colorbox/cross.png" width="40" /></span>
	<a target="_blank" id="linkContenedor"><img alt="" src="" id="contenedor"></a>
</div>       
