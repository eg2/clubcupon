<?php 
/*
 * element: clubcupon/mostViewed.ctp
 * @author: Pward 
 * params: - dealListSearched 
 */

$dealList = $dealListSearched->dealList;
$title = $dealListSearched->title;

?>
<ul class="mas_vistas">
	<h2><?php echo $title; ?></h2>
	<?php 
      // A character is ~ 6 px. with font-size: 13px, font-weight:bold
      $one_row   = ($width/7);
      $two_row   = $one_row*2;
      
	    foreach ($dealList as $deal) { 
          if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
	            $srcImg = Configure::read('web.pluginDebugImages.dummyListMostViewedElementUrlImage');
	        } else {
	            $srcImg = $deal->imageURL;
	        }
          // A character is ~ 6 px. with font-size: 13px, font-weight:bold
          $msgDiscount = '$'.$deal->discountedPrice.' en vez de '.'$'.$deal->price;
          if (strlen($msgDiscount) > $one_row) { $msgDefault = $msgDiscount; } 
          else { $msgDefault = $msgDiscount .' '.$deal->title; }
           
          if (!$deal->onlyPrice) {
            if (!$deal->hidePrice) {             
              $msgToolTips = $msgDiscount .' '.$deal->title;
              $msgShort = substr($msgDefault, 0, $two_row);
            } else {
              $msgShort    = substr($deal->discountedPercent. ' % de descuento por '. $deal->title, 0, $two_row);
              $msgToolTips = $deal->discountedPercent. ' % de descuento por '. $deal->title;
            }
          } else {
            $msgToolTips = $deal->discountedPrice.' '.$deal->title;
            $msgShort    = substr($deal->discountedPrice.' '.$deal->title, 0, $two_row);
          }          
	?>
	<li>
		<a rel="tooltip" title="<?php echo $msgToolTips; ?>" href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>">
			<img src="<?php echo $srcImg; ?>" alt="<?php echo $deal->title; ?>" width="<?php echo $width;?>" height="<?php echo $height;?>">
      <span class="content"><span><?php echo $msgShort; ?> </span> </span>      
		</a>
	</li>
	<?php } ?>

</ul>


