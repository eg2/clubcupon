<?php 
/*
 * element: clubcupon/desplegableTop.ctp
 * @author: Pward 
 * params: groupsLinks - slug - istourism
 */
?>

<?php 

    $ads = Configure::read('web.eplanningIds');
    if($dealA->isTourism) {
    	$isTourism = 'si';
    } else {
    	$isTourism = 'no';
    }

    $items = count($specialLinks);
    $itemsPerColumn = Configure::read('web.specialDesplegableItemsPerColumn');
    $cols = floor(($items-1)/$itemsPerColumn)+1;
    switch ($cols) {
        case 1: $qtCols = 'UnaColumnaSP'; break;
        case 2: $qtCols = 'DosColumnasSP'; break;
        case 3: $qtCols = 'TresColumnasSP'; break;
        case 4: $qtCols = 'CuatroColumnasSP'; break;
        default:  break;
    }
?>

<img src="/img/web/clubcupon/flecha_top.png" class="flecha menu" width="47" height="24" >

<div class="desplegable menu <?php echo $qtCols ?>">
	<?php if (Configure::read('web.specials.enableAds')) { ?>
    	<div class="ad right">
            <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
            <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                googletag.pubads().definePassback('/261718750/ClubCupon_Menu_R2ofertax212', [207, 207]).display();
            </script>
            <?php endif; ?>
    		<?php
                if (Configure::read('web.ad_server') == 'eplanning'){
    	 		echo $this->element(
    				'adIframe',
    				array (
    					'idAd' => $ads['especiales1'],
    					'isTourism' => $isTourism,
    					'idAviso' => $dealA->id,
    					'slug' => $slug,
    					'width' => '200',
    					'height' => '207',
    				    'idIframe'=>'adIframeDesplegableMenu1',
    					'typeAd' => ConstElementsEPlanning::AD_NORMAL	
    				)
    			);
                }        
    	 	?>
    	</div>
    	<div class="ad left">
        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>    
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
            googletag.pubads().definePassback('/261718750/ClubCupon_Menu_R1ofertax212', [207, 207]).display();
        </script>
        <?php endif; ?>
    		<?php
                if (Configure::read('web.ad_server') == 'eplanning'){
    		echo $this->element(
    				'adIframe',
    				array (
    						'idAd' => $ads['especiales2'],
    						'isTourism' => $isTourism,
    						'idAviso' => $dealA->id,
    						'slug' => $slug,
    						'width' => '200',
    						'height' => '207',
    						'idIframe'=>'adIframeDesplegableMenu1',
    						'typeAd' => ConstElementsEPlanning::AD_NORMAL
    				)
    		);
                }
    	 	?>	
    	</div>
    <?php } else { ?>
    	<div class="ad">
            <img src="<?php echo Configure::read('web.desplegableImg') ?>">
    	</div>
    	<div class="ad">
            <img src="<?php echo Configure::read('web.desplegableImg') ?>">
    	</div>
    <?php } ?>

	<?php if (!empty($specialLinks)): ?>

		<div>
    		<ol
    		<?php 
    	        $cont = 0;
    		    foreach ($specialLinks as $specialLink): 
    		        $col = floor($cont/$itemsPerColumn)+1;
    		        $reset = ($col == ($cont/$itemsPerColumn+1) && $col != 1) ? 'reset' : '';
    		        $classColumn = 'column' . $col;
    	    ?>
    			><li class='childCombo <?php echo $classColumn.' '.$reset ?>'><a href="<?php echo $specialLink->url; ?>"><?php echo $specialLink->label; ?></a></li
    		<?php
    		        $cont++; 
    		    endforeach; 
    	    ?>
    		></ol>
		</div>

	<?php endif; ?>
</div>


