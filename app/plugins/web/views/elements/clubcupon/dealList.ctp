<?php 
/*
 * element: clubcupon/dealList.ctp
 * @author: Pward 
 * params: dealList - title - slug
 */
?>

<?php if (!empty($dealList) || !empty($bannerList)): ?>
	<section id="cupones">
		<?php if (!empty($title)): ?>
		<h2> <?php if (!empty($url)): ?><?php echo $title; ?><?php endif; ?></h2>
		<?php endif; ?>
		
	<?php
	if(isset($showMediumsDeals) && $showMediumsDeals):
		/*inicio-mostrar 2 primeras ofertas como destacadas en la primera fila*/
	    $dealFirstList = array_slice($dealList, 0, 2);
	  	foreach ($dealFirstList as $deal) {
	        echo $this->element(
	        		'clubcupon/dealDestacado',
	        		array(
	        				'deal' => $deal,
	        		)
	        );
		}
	    /*fin*/
	
		$dealList = array_slice($dealList, 2);//sub lista sin las primeras 2 ofertas destacadas
	endif;
	    $i=0;
		$dealsByRow=4;
		$showBanners=true;
	    foreach ($dealList as $deal) {
            if($i<$dealsByRow){
   	        	echo $this->element(
	            	'clubcupon/deal', 
	            	array(
	                	'deal' => $deal,
						'width' => $width,
						'height' => $height,
	            	)
	        	);
	        	
				$i++;
			}else{
                if($showBanners){
				  foreach ($bannerList as $banner) {
					echo $this->element(
					    'clubcupon/banner',
						array(
							'banner' => $banner,
							'width' => $width,
							'height' => $height,
					    )
				        );
                  }
                  $showBanners=false;
                  
                }  
                $i=0;
                $dealsByRow=99999;
                echo $this->element(
						'clubcupon/deal',
						array(
								'deal' => $deal,
								'width' => $width,
								'height' => $height,
						)
				);
				
			}
	    }//fin del for
	    
	    if($showBanners){//quiere decir que aun no se mostraron los banners
	    
	    	foreach ($bannerList as $banner) {
	    		echo $this->element(
	    				'clubcupon/banner',
	    				array(
	    						'banner' => $banner,
	    						'width' => $width,
	    						'height' => $height,
	    				)
	    		);
	    
	    	}
	    	$showBanners=false;
	    }
	    
	?>			
	
	</section>
<?php endif; ?>

