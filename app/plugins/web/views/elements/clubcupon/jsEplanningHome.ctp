<div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
<?php if(Configure::read('web.ad_server') == 'eplanning') : ?>    
    <script language="JavaScript" type="text/javascript">
        var eplDoc = document;
        var eplLL = false;
        var eS1 = 'clarin.img.e-planning.net';
        var eplArgs = '';
        if (typeof $.cookie("cxSegmentos") == "undefined") {
            eplArgs = {
                iIF: 1,
                sV: schemeLocal() + "://ads.e-planning.net/",
                vV: "4",
                sI: "9b30",
                sec: "Home",
                eIs: ["Middle1x462", "PromoTop2", "Middle2x462", "Top_940", "Middle3x728", "3x215x100", "4x215x100", "2x215x100", "Bottom950x50", "1x215x100"]
            };
        } else {
            eplArgs = {
                iIF: 1,
                sV: "http://ads.e-planning.net/",
                vV: "4",
                sI: "9b30",
                sec: "Home",
                eIs: ["Middle1x462", "PromoTop2", "Middle2x462", "Top_940", "Middle3x728", "3x215x100", "4x215x100", "2x215x100", "Bottom950x50", "1x215x100"],
                kVs: {
                    segmentos: $.cookie("cxSegmentos")
                }
            };
        }



        function eplCheckStart() {
            if (document.epl) {
                var e = document.epl;
                if (e.eplReady()) {
                    return true;
                } else {
                    e.eplInit(eplArgs);
                    if (eplArgs.custom) {
                        for (var s in eplArgs.custom) {
                            document.epl.setCustomAdShow(s, eplArgs.custom[s]);
                        }
                    }
                    return e.eplReady();
                }
            } else {
                if (eplLL)
                    return false;
                if (!document.body)
                    return false;
                var eS2;
                var dc = document.cookie;
                var cookieName = ('https' === schemeLocal() ? 'EPLSERVER_S' : 'EPLSERVER') + '=';
                var ci = dc.indexOf(cookieName);
                if (ci != -1) {
                    ci += cookieName.length;
                    var ce = dc.indexOf(';', ci);
                    if (ce == -1)
                        ce = dc.length;
                    eS2 = dc.substring(ci, ce);
                }
                var eIF = document.createElement('IFRAME');
                eIF.src = 'about:blank';
                eIF.id = 'epl4iframe';
                eIF.name = 'epl4iframe';
                eIF.width = 0;
                eIF.height = 0;
                eIF.style.width = '0px';
                eIF.style.height = '0px';
                eIF.style.display = 'none';
                document.body.appendChild(eIF);

                var eIFD = eIF.contentDocument ? eIF.contentDocument : eIF.document;
                eIFD.open();
                eIFD.write('<html><head><title>e-planning</title></head><bo' + 'dy></bo' + 'dy></html>');
                eIFD.close();
                var s = eIFD.createElement('SCRIPT');
                s.src = schemeLocal() + '://' + (eS2 ? eS2 : eS1) + '/layers/epl-41.js';
                eIFD.body.appendChild(s);
                if (!eS2) {
                    var ss = eIFD.createElement('SCRIPT');
                    ss.src = schemeLocal() + '://ads.e-planning.net/egc/4/41a3';
                    eIFD.body.appendChild(ss);
                }
                eplLL = true;
                return false;
            }
        }
        eplCheckStart();

        function eplSetAdM(eID, custF) {
            if (eplCheckStart()) {
                if (custF) {
                    document.epl.setCustomAdShow(eID, eplArgs.custom[eID]);
                }
                document.epl.showSpace(eID);
            } else {
                var efu = 'eplSetAdM("' + eID + '", ' + (custF ? 'true' : 'false') + ');';
                setTimeout(efu, 250);
            }
        }

        function eplAD4M(eID, custF) {
            document.write('<div style="display:inline;padding:10px;" id="eplAdDiv' + eID + '"></div>');
            if (custF) {
                if (!eplArgs.custom) {
                    eplArgs.custom = {};
                }
                eplArgs.custom[eID] = custF;
            }
            eplSetAdM(eID, custF ? true : false);
        }

        function schemeLocal() {
            if (document.location.protocol) {
                protocol = document.location.protocol;
            } else {
                protocol = window.top.location.protocol;
            }
            if (protocol) {
                if (protocol.indexOf('https') !== -1) {
                    return 'https';
                } else {
                    return 'http';
                }
            }
        }

        eplAD4M("1x215x100");
        eplAD4M("2x215x100");
        eplAD4M("3x215x100");
        eplAD4M("4x215x100");
    </script>
    <?php endif; ?>
    <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
    <div style="display:inline-block; margin: 10px;">
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
        googletag.pubads().definePassback('/261718750/ClubCupon_Home_1x215x100', [215, 100]).display();
        </script>
    </div>
    <div style="display:inline-block; margin: 10px;">
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
        googletag.pubads().definePassback('/261718750/ClubCupon_Home_2x215x100', [215, 100]).display();
        </script>
    </div>

    <div style="display:inline-block; margin: 10px;">
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
        googletag.pubads().definePassback('/261718750/ClubCupon_Home_3x215x100', [215, 100]).display();
        </script>
    </div>

    <div style="display:inline-block; margin: 10px;">
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
        googletag.pubads().definePassback('/261718750/ClubCupon_Home_4x215x100', [215, 100]).display();
        </script> 
    </div>
    <?php endif; ?>
</div>
