<?php 
/*
 * element: headClubcupon.ctp
 * @author: EParisi - Pward 
 * 
 */
?>

<head>

    <title>
        <?php
            if(preg_match('/'.Configure::read('site.name').'/', $metaData->title)) {
                $title = Configure::read('site.name') . ' | ' .$html->cText($metaData->title, false);
            } else {
                $title = Configure::read('site.name') . ' | ' . $html->cText($metaData->title, false);
            }
            echo $title;
        ?>
    </title>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta charset="utf-8" />
    <meta name="description" content="<?php echo $metaData->description; ?>"/>

    <?php echo $html->meta('keywords', $metaData->keywords), "\n"; ?>
	
	<?php if(isset($metaData->category)):?>
		<meta name="cXenseParse:pageclass" content="article"> 
		<meta name="cXenseParse:recs:category"   content="<?php echo ApiStrUtils::cleanChars($metaData->category);?>"/>
   		<meta name="cXenseParse:gcc-categories" content="<?php echo ApiStrUtils::cleanChars($metaData->category);?>"/>
    <?php else:?>
    	<meta name="cXenseParse:pageclass" content="frontpage"> 
    <?php endif;?>
    <?php 
        if (isset ($og_data)) {
            foreach ($og_data as $key => $value) {
                $pos = stripos($key, 'cXenseParse');
                if ($pos !== false) {
					echo $html->meta (array ('name' => $key, 'content' => $value)), "\n";
				}else{
					echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
				}
                
            }
        }
     ?>

	<link rel="shortcut icon" href="/favicon.png" />
	<link rel="alternate" title="RSS" type="application/rss+xml" href="/feed.rss" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300|Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>

	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $html->css(Configure::read('web.asset_version').'clubcupon/reset.css'); } ?>
	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $html->css(Configure::read('web.asset_version').'clubcupon/galeria.css'); } ?>
	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $html->css(Configure::read('web.asset_version').'clubcupon/nivo-slider.css'); } ?>
	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $html->css(Configure::read('web.asset_version').'clubcupon/default.css'); } ?>
	
	<?php echo $html->css(Configure::read('web.asset_version').'clubcupon/new/style.css'); ?>
	<?php echo $html->css(Configure::read('web.clubcupon.asset_version').'colorbox'); ?>
	<?php echo $html->css(Configure::read('search.asset_version').'jquery-ui-1.10.3'); //autocomplete Search ?>

	<?php echo $javascript->link(Configure::read('theme.asset_version') . 'jquery-1.7.1.min'); ?>
	<?php echo $javascript->link(Configure::read('search.asset_version').'jquery-ui-1.8.18.min'); ?>
	<?php echo $javascript->link(Configure::read('search.asset_version').'search-Plugin-1.0'); ?>
	<?php echo $javascript->link(Configure::read('now.asset_version').'jquery.colorbox.js'); ?>
	<?php echo $javascript->link(Configure::read('libs.asset_version').'jquery.livequery'); ?>
	<?php echo $javascript->link(Configure::read('theme.asset_version') . '/libs/jquery.cookie'); ?>
 	<?php echo $javascript->link(Configure::read('web.asset_version').'custom.js'); ?>
 	
 	<?php  echo $javascript->link(Configure::read('web.asset_version').'jquery.countdown.js'); ?>
 	<?php  echo $javascript->link(Configure::read('web.asset_version').'jquery.scrollpanel-0.5.0.min.js'); ?>
 	<?php  echo $javascript->link(Configure::read('web.asset_version').'detectmobilebrowser.js'); ?>
 	
 	 	
 	
	<?php if ($this->params['url']['url'] == 'cel/landing') { /*echo $javascript->link(Configure::read('web.asset_version').'jquery.tools.min');*/ } ?>
	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $javascript->link(Configure::read('web.asset_version').'jquery.nivo.slider'); } ?>
 	<?php if ($this->params['url']['url'] == 'cel/landing') { echo $javascript->link(Configure::read('web.asset_version').'celTabs'); } ?>
 	<!--[if lt IE 9]>
		<script type="text/javascript">
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>
	<![endif]-->
 	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->
 	<meta name="apple-itunes-app" content="app-id=603096819">		
 	<script>
		var _prum = [['id', '55c8c4a7abe53dd11766d513'],
             ['mark', 'firstbyte', (new Date()).getTime()]];
		(function() {
    	var s = document.getElementsByTagName('script')[0], p = document.createElement('script');
    	p.async = 'async';
    	p.src = '//rum-static.pingdom.net/prum.min.js';
    	s.parentNode.insertBefore(p, s);
		})();
	</script>
 	
</head>
