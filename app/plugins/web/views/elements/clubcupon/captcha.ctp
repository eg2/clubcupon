<style>
.modulo_captcha{
	/*border: 1px solid red;*/
	float: left;
    width: 440px;
    /*padding-top: 5px;*/
}
.modulo_captcha .captcha-container {
	/*border: 1px solid blue;*/
}

.modulo_captcha form {
	color: #626262;
	font-size: 13px;
	padding: 0 30px;
}

.modulo_captcha .input {
	display: block;
	float: left;
	width: 50%;
	margin-bottom: 10px;
}

.modulo_captcha .input label {
	display: block;
	margin: 2px 0 6px;
}

.modulo_captcha .input input {
	border: 1px solid #D3D3D3;
	display: block;
	height: 34px;
	width: 93%;
}

.modulo_captcha .email-container {
	clear: both;
	overflow: hidden;
}

.modulo_captcha .email-container .input {
	width: 100%;
}

.modulo_captcha .email-container .input input {
	width: 96.5%;
}
/* 	.modulo_captcha form .email{ */
/* 		width: 100%; */
/* 		} */
/* 	.modulo_captcha form .email input{ */
/* 		width: 394px; */
/* 		} */
.modulo_captcha input[type="submit"] {
	font-size: 18px;
	height: 48px;
	padding: 0 15px;
	float: right;
	margin-top: 30px;
	margin-right: 13px;
}

.modulo_captcha .captcha-container {
	overflow: hidden;
	clear: both;
	margin-bottom: 5px;
}

.modulo_captcha .captcha-container label {
	display: block;
	font-size: 12px;
}

.modulo_captcha  .captcha-container .input #UserCaptcha {
	margin: 10px 0 0 0;
	height: 43px;
}

.modulo_captcha .captcha {
	/* 	display: inline-block; */
	float: left;
	height: 45px;
	margin: 5px 10px 0 0;
	border: 1px solid #D3D3D3;
}

.modulo_captcha .captcha img {
	float: left;
	display: inline-block;
}

.modulo_captcha .captcha label {
	clear: both;
	display: block;
	float: left;
	margin: 8px 0 6px;
	width: 100%;
}

.modulo_captcha .captcha input {
	border: 1px solid #D3D3D3;
	float: right;
	height: 41px;
	/*margin-bottom: 10px;*/
	margin-right: 13px;
	width: 43%;
}

.modulo_captcha .captcha-right {
	display: inline-block;
}

.modulo_captcha .captcha .captcha-right .captcha-reload {
	display: inline-block;
	/* 	margin-bottom: 4px; */
}

.modulo_captcha .captcha .captcha-right img {
	padding: 4px 0;
}


#cc-popup form {
    height: 75px;
}

.subs_box .modulo_captcha .captcha {
margin-top:-4px !important;
}

#suscripcion {
    height: 200px !important;
}

.preBox .subs_box{
    height: 103px !important;
}
</style>
<div class="modulo_captcha">
	<div class="captcha-container">
		<!-- label>Ingresá el código de seguridad</label-->
		<div class="captcha captcha-block js-captcha-container">
	    			<?php echo $html->image(Router::url( array('plugin' => '', 'controller' => 'users', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('title' => 'CAPTCHA', 'class' => 'floatLeft captcha-img'));?>
	    			<div class="captcha-right">
	    				<?php echo $html->link($html->image('captcha_reload.png'), '#', array('class' => 'js-captcha-reload captcha-reload', 'title' => 'Actualizar CAPTCHA', 'escape' => false));?>
	    				<br />
	    				<?php echo $html->link($html->image('captcha_audio.png'), array('plugin' => '', 'controller' => 'users', 'action' => 'captcha_play'), array('title' => 'Version Audio', 'rel' => 'nofollow', 'escape' => false));?>
	    			</div>
		</div>
	    		<?php
	    		if(isset($landingpre) && $landingpre){
					echo $form->input('captcha', array('div' => false,'label' => 'Ingresá el código de seguridad','class' => 'js-captcha-input'));
				}else{
					echo $form->input('captcha', array('div' => false,'label' => 'Ingresá el código de seguridad','class' => 'js-captcha-input','id'=>'SubscriptionCaptcha','name'=>'SubscriptionCaptcha'));
				}
 
	    		
	    		?>
	</div>
</div>