<?php
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $browserIsLinuxHost = (strpos($browser, 'Linux') !== false);
    $browserIsIE7 = (strpos($browser, 'MSIE 7.0') !== false);
    $browserIsIE8 = (strpos($browser, 'MSIE 8.0') !== false);
?>
   <head>
        <?php echo $html->charset(), "\n"; ?>

        <!--[if IE 7]>
            <meta http-equiv="X-UA-Compatible" content="IE=7" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

        <!--[if IE 8]>
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <style>#footer{float: none !important;}</style>
        <![endif]-->

        <title>
        <?php
            if(preg_match('/'.Configure::read('site.name').'/', $title_for_layout)) {
                $title = Configure::read('site.name') . ' | ' .$html->cText($title_for_layout, false);
            } else {
                $title = Configure::read('site.name') . ' | ' . $html->cText($title_for_layout, false);
            }
            echo $title;
        ?>
        </title>

        <?php
		      echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";
              if (isset ($og_data)) {
                // var_dump ($og_data);
                foreach ($og_data as $key => $value) {
                  if (is_array ($value)) {
                    foreach ($value as $val) {
                      echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                    }
                  } else {
                    echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                  }
                }
              }
              $this->log('head:description->'.Configure::read('web.meta.description'));
        ?>
              <meta name="description" content="<?php echo Configure::read('web.meta.description')?>"/>
        <?php
              //CSSs
            echo $html->css(Configure::read('theme_clean.asset_version').'reset');      //unifica los estados de los navegadores
            echo $html->css(Configure::read('theme_clean.asset_version').'popup'); //fw de css
            //Jquery
            echo $javascript->link(Configure::read('theme_clean.asset_version').'jquery-1.7.1.min');
		    echo $javascript->link(Configure::read('theme_clean.asset_version').'libs/jquery.form');
        ?>
         <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />

        <!--[IF IE]>
            <script type="text/javascript" src="/js/theme_clean//libs/jq-corners.js"></script>
        <![endif]-->
        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />
    </head>
