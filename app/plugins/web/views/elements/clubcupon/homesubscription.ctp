
<script type="text/javascript">
$(document).ready(function() {
	$(".closeSubscribeNewsletter").click(function(){
	  $("#jHeaderMsgLayer").hide();
	});
	$(".subscribeLink").click(function(){
	  $("#jHeaderMsgLayer").toggle();
	});
	$("#jHeaderNlRegisterForm").submit(function() {
		 if (!verifyEmailSubscription()) {
		 	alert("Ingrese un e-mail válido");
		 	return false;
		 }else{
			 return true;
		 }
		 
	});
	function verifyEmailSubscription()
    {
        var status = false;
        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if ($('#emailaddress').val().search(emailRegEx) == -1)
        {
        }
        else
        {
           status = true;
        }
        return status;
    }	

	$("#recibir_ofertas").click(function(event) {
		event.preventDefault();
			$(".pagewrap").slideToggle();
		});

		$(".closeSubscribeNewsletter").click(function(event) {
		event.preventDefault();
			$(".pagewrap").slideUp();
		});
	
});
</script>


<div data-bhw-path="Header|PushdownSubscriptionForm" class="boxSubscribeNewsletter pagewrap cp" data-bhw="PushdownSubscriptionForm" id="jBoxSubscribeNewsletter" style="display: none;">
	<form id="jHeaderNlRegisterForm" action="/ciudad-de-buenos-aires/addsubscription" method="post">

		<span class="boxSubscribeNewsletterDesc">Recibí las Ofertas de :</span>

	  	<select name="SubscriptionCitySlug" id="SubscriptionCitySlug-header">
		<?php 
		foreach ($citiesLinks as $cityLink){
			echo '<option value="'.substr($cityLink->url, 1).'"';
			if (substr($cityLink->url, 1) == $slug) {
				echo ' selected="selected"';
			}
			echo '>'.$cityLink->label.'</option>';
			}
		?>
	  	</select>
	  <span class="boxSubscribeNewsletterInputWrapper">
		<input data-bhw-path="Header|PushdownSubscriptionForm|PushdownSubscriptionFormEmailInputField" data-bhw="PushdownSubscriptionFormEmailInputField" id="emailaddress" name="SubscriptionEmail" class="nlHeaderInput predefinedInput borderRadius2px cp" placeholder="Ingresá tu email">
	  </span>

	  <button data-bhw-path="Header|PushdownSubscriptionForm|PushdownSubscriptionFormSubmitSubscriptionButton" data-bhw="PushdownSubscriptionFormSubmitSubscriptionButton" class="btn_gen btn_red btn_25 cp" name="subscribeNewsletter" id="newsletterSubmitBtn" type="submit" value="">Suscribirme!</button>

	  <button class="closeSubscribeNewsletter" type="button">×</button>

	</form>
  </div>