<?php 
/*
 * element: clubcupon/desplegableTop.ctp
 * @author: Pward 
 * params: citiesLinks
 */
?>

<?php 
	$ads = Configure::read('web.eplanningIds');
	if($dealA->isTourism) {
		$isTourism = 'si';
	} else {
		$isTourism = 'no';
	}
	$items = count($citiesLinks);
	$itemsPerColumn = Configure::read('web.topDesplegableItemsPerColumn');
	$cols = floor(($items-1)/$itemsPerColumn)+1;
    switch ($cols) {
        case 1: $qtCols = 'UnaColumna'; break;
        case 2: $qtCols = 'DosColumnas'; break;
        case 3: $qtCols = 'TresColumnas'; break;
        case 4: $qtCols = 'CuatroColumnas'; break;
        default:  break;
    }
	//'extraLinks' => $extraLinks,
?>
<img src="/img/web/clubcupon/flecha_top.png" class="flecha top" width="47" height="24" >
<div class="desplegable top <?php echo $qtCols ?>">
	<div class="banner">
        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>    
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
            googletag.pubads().definePassback('/261718750/ClubCupon_Menu_R3especialx200', [207, 207]).display();
        </script>
        <?php endif; ?>
		<?php 
                if (Configure::read('web.ad_server') == 'eplanning'){
    	 		echo $this->element(
    				'adIframe',
    				array (
    					'idAd' => $ads['menuR3Especialx200'],
    					'isTourism' => $isTourism,
    					'idAviso' => $dealA->id,
    					'slug' => $slug,
    					'width' => '200',
    					'height' => '207',
    					'idIframe'=>'adIframeDesplegableTop1',
    					'typeAd' => ConstElementsEPlanning::AD_NORMAL
	    			)
    			);
                }
    	 	?>	
	</div>

	<?php if (!empty($citiesLinks)): ?>
	    <div>
    		<ol
    		<?php 
    	        $cont = 0;
    		    foreach ($citiesLinks as $cityLink): 
    		        $col = floor($cont/$itemsPerColumn)+1;
    		        $reset = ($col == ($cont/$itemsPerColumn+1) && $col != 1) ? 'reset' : '';
    		        $classColumn = 'column' . $col;
    	    ?>
    			><li class='<?php echo $classColumn.' '.$reset ?>'><a href="<?php echo $cityLink->url; ?>"><?php echo $cityLink->label; ?></a></li
    		<?php
    		        $cont++; 
    		    endforeach; 
    	    ?>
    		></ol>
		</div>
	<?php endif; ?>

	<?php if (!empty($extraLinks)): ?>
		<br><p>Extra</p>
		<ul>
		<?php foreach ($extraLinks as $extraLink): ?>
			<li><a href="<?php echo $extraLink->url; ?>"><?php echo $extraLink->label; ?></a></li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>


