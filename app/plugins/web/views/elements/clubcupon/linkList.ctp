<?php 
/*
 * element: clubcupon/linkList.ctp
 * @author: Pward 
 * params: linkList - title - slug
 */
?>

<?php if (!empty($linkList)): ?>
	<nav id="menu_vertical">
		<div class="scrollpanel no2">
			<ul>
			<?php foreach ($linkList as $link): ?>
				<li>
					<a href="<?php echo $link->url; ?>" class="<?php echo (is_null($link->classStyle)?'ccya_search_link':$link->classStyle); ?>">
					<?php if(!empty($link->logoLeft)): ?>
					<img class="ccya_search_link_image" src="/img/web/clubcupon/links/<?php echo $link->logoLeft; ?>"/>
					<?php endif;?>
					<div>
					  <h2 class="title_02"><?php echo $link->label; ?></h2>
					  <p><?php echo $link->description; ?></p>
					</div>
					<?php if(!empty($link->logoRight)): ?>
					<img class="ccya_search_link_image" src="/img/web/clubcupon/links/<?php echo $link->logoRight; ?>"/>
					<?php endif;?>
					</a>
				</li>
				<li><img src="/img/web/clubcupon/new/images/linea_divisora.png"></li>
			<?php endforeach; ?>
			</ul>
		</div>
		<?php echo $this->element(
				'clubcupon/descargarApp'
				);
		?>
	</nav>
<?php endif; ?>



