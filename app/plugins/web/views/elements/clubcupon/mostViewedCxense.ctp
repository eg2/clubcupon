<?php 
/*
 * element: clubcupon/mostViewedCxense.ctp
 * @author: Pward 
 * params: - slug 
 */
$widgetId="";
switch ($slug) {
	case (strcasecmp ( $slug, "ciudad-de-buenos-aires" ) == 0) :
		$widgetId= "8e8853e28cf70588ea7461c08c969358f4eed3fb";
		break;
	case (strcasecmp ( $slug, "gran-buenos-aires-1" ) == 0) :
		$widgetId= "18528fb91e9d49cc6b748d5ee22dde31cab35471";
		break;
	case (strcasecmp ( $slug, "ofertas-nacionales" ) == 0) :
		$widgetId= "6d5c63674833a50ab36fc42a9dd8dc0cb9ec5bd7";
		break;
	case (strcasecmp ( $slug, "turismo" ) == 0) :
		$widgetId= "296f46684fadc93c4c18c883a7b64c6d31c250a4";
		break;
	case (strcasecmp ( $slug, "rosario" ) == 0) :
		$widgetId= "dd282b301f8e3904ef18a394d6b6e4f68b27f1b0";
		break;
	case (strcasecmp ( $slug, "mendoza" ) == 0) :
		$widgetId= "3f3031d1babd90ef6b4f2f0d217d9d41fb31c157";
		break;
	case (strcasecmp ( $slug, "la-plata" ) == 0) :
		$widgetId= "2b44b9c3cf68d847a82fd947f33599dbdd63e129";
		break;
	case (strcasecmp ( $slug, "cordoba" ) == 0) :
		$widgetId= "72d398ba95229e0935eef7e6d7072a4a8a68a877";
		break;
	default :
		$widgetId= "6d5c63674833a50ab36fc42a9dd8dc0cb9ec5bd7";
}
$quantityDeals=Configure::read('CXENSE.quantity_of_items_in_widget');
$heightTitle=Configure::read('CXENSE.height_title_items_in_widget'); // 
$heightItem=Configure::read('CXENSE.height_of_item_in_widget'); // 
$heightTotal=$heightTitle+$quantityDeals*$heightItem;
?>

<ul class="mas_vistas">
	<!-- h2><?php echo $quantityDeals.' '.$heightItem?></h2-->
	<li>
		<!-- Cxense content widget: ClubCupon - Recomendacion en Oferta --> <!-- Cxense content widget: ClubCupon - Recomendacion en Oferta -->
		<div id="cx_<?php echo $widgetId?>"
			style="display: none"></div> <script type="text/javascript">
    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
    cX.callQueue.push(['insertWidget', {
        widgetId: '<?php echo $widgetId?>',
        insertBeforeElementId: 'cx_<?php echo $widgetId?>',
        width: 230, height: <?php echo $heightTotal?>, renderTemplateUrl: 'auto',  resizeToContentSize: true
    }]);

    // Async load of cx.js
    (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
    e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
    t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
</script> <!-- End of Cxense content widget -->

	</li>
</ul>


