<?php
/*
 * element: clubcupon/menuTop.ctp
 * @author: Pward 
 * params: slug - groupsLinks
 * 
 */
?>
<?php //echo $selectedTab;     ?>
<nav id="menu_top">
    <ul>
        <?php
        $menuItems = Configure::read('web.plugin.tabsMenuTop');
        foreach ($menuItems as $key => $item):
            //echo $key;
            ?>		
            <?php if ($selectedTab == $key): ?>
                <li class="menu_top_selected li_top">
                    <?php if ($key == 1): ?>
                        <a title="<?php echo $item['alt']; ?>" href="<?php echo $item['path']; ?>" class="btn-naranja_menu_top_selected"><?php echo $item['label']; ?></a>
                    <?php else: ?>
                        <a title="<?php echo $item['alt']; ?>" href="<?php echo $item['path']; ?>" class="btn-naranja_menu_top_selected"><?php echo $item['label']; ?></a>
                    <?php endif; ?>
                </li>
                <?php if ($key == 1): ?>
                    <li class="especiales parent last_item"><?php // $li_class = $slug == 'especial-belleza' ? 'select' : '';      ?>
                        <a id="specials" title="Categorias" href="#" class="btn-naranja_menu_top">CATEGORIAS</a>

                        <?php
                        echo $this->element(
                                'clubcupon/desplegableMenu', array(
                            'specialLinks' => $specialLinks,
                            'slug' => $slug,
                            'dealA' => $dealA
                                )
                        );
                        ?>
                    </li>
                <?php endif; ?>
            <?php else: ?>
                <li class="li_top"><?php $path = $bodytag = str_replace(Configure::read('web.plugin.tabsMenuTopPlaceHolderCitySlug'), $slug, $item['path']); ?>
                    <a title="<?php echo $item['alt']; ?>" href="<?php echo $path; ?>" class="btn-naranja_menu_top" escape="false"><?php echo $item['label']; ?></a>
                </li>
                <?php if ($key == 1): ?>
                    <li class="especiales parent last_item"><?php // $li_class = $slug == 'especial-belleza' ? 'select' : '';       ?>
                        <a id="specials" title="Especiales" href="/especiales" class="btn-naranja_menu_top">ESPECIALES</a>

                        <?php
                        echo $this->element(
                                'clubcupon/desplegableMenu', array(
                            'specialLinks' => $specialLinks,
                            'slug' => $slug,
                            'dealA' => $dealA
                                )
                        );
                        ?>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php /*
          <li><?php $li_class = $slug == 'productos' ? 'select' : ''; ?>
          <a title="Productos" href="/productos" class="<?php echo $li_class; ?>">Productos</a>
          </li>
         */ ?>

        <?php /*
          <li><?php $li_class = $slug == 'exclusive' ? 'select' : '';  ?>
          <a title="Exclusive" href="/exclusive" class="<?php echo $li_class; ?>">Exclusive</a>
          </li>
          <li class="last_item">
          <?php
          if(Configure::read('now.is_menu_enabled') &&  !$is_corporate_city) {
          echo '<a title="Club Cup&oacute;n YA" href="'.FULL_BASE_URL.'/ya" >Club Cup&oacute;n Ya</a>';
          }
          ?>
          </li>
         */ ?>
    </ul>
</nav>
<script>

    $(document).ready(function () {

        /*
         $("li[class*='li_top']").click(function(){
         window.location=$(this).find("a").attr("href"); 
         return false;
         });
         */
        /*
         $("li[class*='li_top']").mousedown(function(e){
         switch(e.which)
         {
         case 1:
         window.location=$(this).find("a").attr("href");
         break;
         case 2:
         var newWindow = window.open('','_blank');
         newWindow.location = $(this).find("a").attr("href");
         
         break;
         case 3:
         //right Click
         break;
         }
         
         
         });
         */
    });

</script>