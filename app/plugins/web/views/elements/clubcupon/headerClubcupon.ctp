<?php 
/*
 * element: clubcupon/headerClubcupon.ctp
 * @author: Pward 
 * params: city - citiesLinks - groupsLinks - user - isBusinessUnit - imageURL - hideSearchBar
 * 
 */
?>

<?php 
	$slug = $city->slug;
	$geoCityName = $city->lastGeoCityName;
?>

<header>
	
	<div>
		<a class="logo" href="/<?php echo $slug; ?>" title="ClubCupón"><h1>ClubCupón</h1></a>
		
		<!-- Buscador -->	
		<div id="buscador">
			<?php if (!$hideSearchBar): ?>
	 			<?php 
	        	    $fromSearchPg = 'no';
	                if ($this->action == 'makesearch') {
	                	$fromSearchPg = 'yes';
	                }
	                $query = '';
	                if (isset($search)) {
	                  	if ($search['Query']['Q'] != '*:*') {
	                  		$query = $search['Query']['Q'];
	                  	}
	                }
	           	?>
				<form class="headerInputQuery" action='/search/search_querys/makesearch' id="searchFormRight" accept-charset="utf-8" method="get">
	            	<input class="qSearch" id="q" name="q" type="text" value="<?php echo $query; ?>"  placeholder="Buscar ofertas en clubcupon.com.ar"/>
	            	<input type="hidden" id="qr" name="qr" value="reset"/>
	                <input type="hidden" id="currentCitySlug" name="currentCitySlug" value="<?php echo $slug; ?>"/>
	               	<input type="hidden" id="fromSearchPg" name="fromSearchPg" value="<?php echo $fromSearchPg; ?>"/>
	            </form>
	         	<script type="text/javascript">
	            	var autocompFField = '<?php echo  $GLOBALS['autocomplete']['FacetField']?>';
	                var autocompItems = <?php echo  $GLOBALS['autocomplete']['Items']?>;
	                var extraFilter = '<?php echo  $GLOBALS['extraFilters'][0]?>';
	                var autocompSolrAsyncUrlBridge =  '<?php echo  $GLOBALS['autocomplete']['SolrAsyncUrlBridge']?>';
	                var autocompSolrAsyncUrl = '<?php echo  $GLOBALS['autocomplete']['SolrAsyncUrl']?>';
	
	                $(document).ready(function() {
	                	initAutocomplete();
	                });
	                
				</script>
			
				<div id="btn_desplegar" class="btn_desplegar parent">
					<!-- img class="pin" src="/img/web/clubcupon/pick2.png"-->
					<?php echo $geoCityName; ?>
					<?php 
				        echo $this->element(
					    	'clubcupon/desplegableTop', 
					       	array(
								'citiesLinks' => $citiesLinks,
					       		'extraLinks' => $extraLinks,
					       		'dealA' =>$dealA,
					       	    'slug' => $slug
					        )
					    );
					?>	
				</div>
			    
			
			<?php endif; ?>
		</div>
		
		<?php if (!$user->userEmail) { ?>
    		<ul id="login">
				<li><a class="item-salir" href="/<?php echo $slug ?>/users/login" title="Ingresar" rel="nofollow" >INGRESAR</a></li>
    			<li>|</li>
				<li><a class="item-salir" href="/<?php echo $slug ?>/users/register" title="Registrarse" rel="nofollow" >REGISTRARSE</a></li>
	   		</ul>
    		<div id="recibir_ofertas" style="display: block;" class="subscribeLink"><a href="#">Recibir <strong>OFERTAS</strong> <img src="/img/web/clubcupon/new/images/pick4.png"></a></div>
		<?php } else { ?>
			<?php $emailShort = cutText($user->userEmail, 20); ?>
			<ul id="login" class="logged opciones_login parent">
				<li><span class="loginEmail"><?php echo $emailShort; ?></span></li>
		    </ul>
		    
		    <div id="desplegable_login">
				<img src="/img/web/clubcupon/flecha_top.png" class="flecha" alt="flecha">
				<div class="menu_final">
					<ul>
						<?php 
						foreach ($user->menuElements as $item){
							if($item->type == 'string'){
								if($item->value != 0.00){
									echo "<li><div class='str'>".$item->string."</div></li>";
								}
							}elseif($item->type == 'link'){
								$link="/";
								if($item->label!='Mi Perfil'){
									if( !empty($item->admin) && ($item->admin==true) ){
										$link = $link.$slug."/";
										if (!empty($item->action)){
											$link = $link.$item->action;
										}
										echo "<li><a href='$link'>".$item->label."</a></li>";
									}else{
										if (!empty($item->plugin)){
											$link = $link.$item->plugin."/";
										}
										if (!empty($item->controller)){
											$link = $link.$item->controller."/";
										}
										if (!empty($item->action)){
											$link = $link.$item->action;
										}
										if (!empty($item->parameters)){
											$link = $link."/".$item->parameters;
										}
										echo "<li><a href='$link'>".$item->label."</a></li>";
									}
								}else {
									echo "<li>".$html->link (
                                		$item->label,
                                    	array (
                                    		'plugin'        => $item->plugin,
                                    		'controller'    => $item->controller,
                                    		'action'        => $item->action,
                                    		'admin'         => $item->admin,
                                    		$item->parameters,
                                    	)
                                	)."</li>";
								}
							}elseif ($item->type == 'externalLink'){
								echo "<li><a target='$item->target' href='$item->url'>".$item->label."</a></li>";
							}
						} 
						?>
						<li><a class="udLink" href="/<?php echo $slug ?>/users/logout" title="Salir" class="item-salir">Salir</a></li>
					</ul>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php
    	echo $this->element('clubcupon/homesubscription',array('citiesLinks'=>$citiesLinks,'slug'=>$slug));
	?>	
	<?php 
       	echo $this->element(
	       'clubcupon/menuTop', 
	       array(
				'slug' => $slug,
				'specialLinks' => $specialLinks,
	       		'dealA' =>$dealA,
	       		'selectedTab'=>$selectedTab,
	       		'slug' => $slug
	       )
	    );
	?>
	<div id="mobileAd">
	    <div class="ad">
	        <div class="close">
	            <a href="#"><img src="/img/colorbox/cross.png" width="40" /></a>
	        </div>
	        
	        <div class="logoIcon">
	            <img />
	        </div>
	        
	        <div class="downloadLink">
	            <a>Descargar</a>
	        </div>
	        
	        <div class="text">
	            Descarg&aacute; la aplicaci&oacute;n de <strong>Club Cup&oacute;n</strong> para tu celular y disfrut&aacute; de las mejores ofertas.
	        </div>
    	</div>
	</div>
	
</header>

<script type="text/javascript">
	$(document).ready(function() {
		$('#specials').click(function(event){
			event.preventDefault();
		});

		$('html').click(function(e){
            e.stopPropagation();
            if($(e.target).parents('.desplegable').length==0) {
            	if($('.desplegable:visible').length>0) $('.desplegable').slideUp(200);
            	$('.parent').removeClass('open');
            	$('.parent .flecha').hide();
            }
            if($(e.target).parents('.#desplegable_login').length==0) {
            	if($('#desplegable_login').length>0) $('#desplegable_login').slideUp(200);
            	$('.parent').removeClass('open');
            }

        });

		$(".especiales").click(function(event) {
			event.stopPropagation();
			$('#btn_desplegar .desplegable').slideUp();
			$('#btn_desplegar .flecha').hide();
			$('#desplegable_login ').slideUp();

			$(this).children('.desplegable').slideToggle();
			$(this).children('.flecha').toggle();
		});
		
		$("#btn_desplegar").click(function(event) {
			event.stopPropagation();
			$(this).toggleClass('open');
			
			$('.especiales .desplegable').slideUp();
			$('.especiales .flecha').hide();
			$('#desplegable_login ').slideUp();

			$(this).children('.desplegable').slideToggle();
			$(this).children('.flecha').toggle();
		});		
		
		$(".opciones_login").click(function(event) {
			event.stopPropagation();
			event.preventDefault();
			$(this).toggleClass('open');
			
			$('.especiales .desplegable').slideUp();
			$('.especiales .flecha').hide();
			$('#btn_desplegar .desplegable').slideUp();
			$('#btn_desplegar .flecha').hide();
			
			$("#desplegable_login").slideToggle();

			$(this).children('.desplegable').slideToggle();
			$(this).children('.flecha').toggle();
		});


		
		$('#mobileAd .ad .close a').click(function(){
	        $('#mobileAd').toggle();
	    })

	    settingsForMobileAd = returnSettingsForMobileAd();
	    if(settingsForMobileAd) {
	        renderMobileAd(settingsForMobileAd);
	    }
		
	});
	
</script>



