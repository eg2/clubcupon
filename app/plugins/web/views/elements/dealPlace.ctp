<?php 
/*
 * element: dealPlace.ctp
 * @author: Pward 
 * params: -
 */

$companyName = $deal->company->name;
$companyCity = $deal->company->city;
$companyURL = $deal->company->url;
$companyAddress = $deal->company->address1;

?>


<h3><?php echo $companyName; ?> <?php if (!empty($companyURL)): ?><a href="<?php echo $companyURL; ?>">Ver Web</a><?php endif; ?></h3>
<div id="ubicacion">
	<span class="pin"></span>
	<span><?php echo $companyCity; ?></span>
	<p><?php echo $companyAddress; ?></p>
</div>

