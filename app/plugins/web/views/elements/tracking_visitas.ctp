<script type="text/javascript">
    var emtBaseURL = (("https:" == document.location.protocol) ? "https://emt.cmd.com.ar/" : "http://emt.cmd.com.ar/");
    document.write(unescape("%3Cscript src='" + emtBaseURL + "emt-min_v2.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var tracker = new EmtTracker(emtBaseURL);
        tracker.setSite('CC');
        tracker.setCampaignByUrlParam('emt_campaign');
        tracker.setGoalByUrlParam('emt_goal');
        tracker.setDateByUrlParam('emt_date');
        tracker.setUserByUrlParam('emt_user');
        tracker.setSegmentByUrlParam('emt_segment');
        tracker.trackPage();
    } catch (err) {
    }
</script>