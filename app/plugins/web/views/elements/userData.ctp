<?php
/*
 * element: userData.ctp
 * @author: Parisi
 * params: user
 */
?>
<div id="uData">
    <?php
    if ($user->userEmail) {
        echo $user->userEmail . ' | ';
        ?>
        <a class="udLink" href="/<?php echo $slug ?>/userLogout" title="Salir" class="item-salir">Salir</a>
        <?php
    }
    ?>
</div>
