<?php 
/*
 * element: headExclusive.ctp
 * @author: EParisi - Pward 
 * 
 */
?>

<head>
	<title><?php if ($isBusinessUnit): ?>Nuestros Beneficios<?php else: ?>Exclusive<?php endif; ?></title>
	<meta charset="utf-8" />
	<link rel="shortcut icon" href="<?php if ($isBusinessUnit): ?>/img/web/exclusives/nuestrosBeneficios/favicon_exclusive.ico<?php else: ?>/favicon.png<?php endif; ?>" />
	<link rel="alternate" title="RSS" type="application/rss+xml" href="/feed.rss" />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
	
	<?php echo $html->css(Configure::read('web.asset_version').'exclusives/nuestrosBeneficios/style.css'); ?>	
	
	<!-- jquery 1.8.2 -->
	<?php echo $javascript->link(Configure::read('web.asset_version').'jquery.min.js'); ?>
	 
	<?php echo $javascript->link(Configure::read('web.asset_version').'modernizr.custom.17475.js'); ?>
	<?php echo $javascript->link(Configure::read('web.asset_version').'jquery.countdown.js'); ?>
 	<?php echo $javascript->link(Configure::read('web.asset_version').'validetta-min.js'); ?>
 	<?php echo $javascript->link(Configure::read('web.asset_version').'custom.js'); ?>
 	
 	<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->		
</head>