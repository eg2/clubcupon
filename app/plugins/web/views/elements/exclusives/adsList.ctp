<?php
/*
 * element: adsList.ctp
 * @author: Pward 
 * params: title, id, slug, deal, isTourism, pos
 */

$ads = Configure::read('web.eplanningIds');


$max = count($ads[$id]);
?>
<section id="cupones" class="ads-cupons">
    <a name="<?php echo $id; ?>">&nbsp;</a>
    <h2><?php echo $title; ?>&nbsp;</h2>

    <div id="<?php echo $id . '_' . $pos; ?>" class="slider">
        <div class="nav">
            <div class="prev">prev</div>
            <div class="next">next</div>
        </div>
        <div class="content">
            <ul  class="">
                <?php for ($i = 0; $i < $max; $i++): ?>
                    <li>
                        <?php
                        if (Configure::read('web.ad_server') == 'eplanning') {
                            echo $this->element(
                                    'ad', array(
                                'slug' => $slug,
                                'isTourism' => $isTourism,
                                'idAd' => $ads[$id][$i],
                                'idAviso' => $idAviso,
                                    )
                            );
                        }
                        ?>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>		
</section>



