<?php
/*
 * element: exclusives/dealList.ctp
 * @author: Pward 
 * params: dealList - title - slug
 */
?>
<?php $showBanners = true; ?>
<?php if (!empty($dealList)): ?>
    <section id="cupones">
        <?php if (!empty($title)): ?>
            <h2><?php echo $title; ?> <?php if (!empty($url)): ?><a href="/<?php echo $slug . '/' . $url; ?>"><?php echo $urlText ?></a><?php endif; ?></h2>
        <?php endif; ?>

        <?php
        $i = 0;
        $dealsByRow = 4;

        foreach ($dealList as $deal) {
            if ($i < $dealsByRow) {
                echo $this->element(
                        'exclusives/deal', array(
                    'slug' => $slug,
                    'deal' => $deal,
                    'isBusinessUnit' => $isBusinessUnit,
                    'width' => $width,
                    'height' => $height
                        )
                );
                $i++;
            } else {
                if ($showBanners) {
                    foreach ($bannerList as $banner) {
                        echo $this->element(
                                'clubcupon/banner', array(
                            'banner' => $banner,
                            'width' => $width,
                            'height' => $height,
                                )
                        );
                    }
                    $showBanners = false;
                }
                $i = 0;
                $dealsByRow = 99999;
                echo $this->element(
                        'exclusives/deal', array(
                    'slug' => $slug,
                    'deal' => $deal,
                    'isBusinessUnit' => $isBusinessUnit,
                    'width' => $width,
                    'height' => $height
                        )
                );
            }
        }
        if ($showBanners) {//quiere decir que aun no se mostraron los banners
            foreach ($bannerList as $banner) {
                echo $this->element(
                        'exclusives/banner', array(
                    'banner' => $banner,
                    'width' => $width,
                    'height' => $height,
                        )
                );
            }
            $showBanners = false;
        }
        ?>			

    </section>
    <?php endif; ?>

    <?php
    if ($showBanners) {//quiere decir que aun no se mostraron los banners
        echo '<section id="cupones">';
        foreach ($bannerList as $banner) {
            echo $this->element(
                    'exclusives/banner', array(
                'banner' => $banner,
                'width' => $width,
                'height' => $height,
                    )
            );
        }
        $showBanners = false;
    } echo '</section>';
    ?>

