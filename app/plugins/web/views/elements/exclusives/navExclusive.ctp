<?php
/*
 * element: navExclusive.ctp
 * @author: Pward 
 * params: slug- isBussinessUnit
 * 
 */
?>

<nav id="menu">
    <ul>
        <li class="first">
            <a href="/<?php echo $slug; ?>/#nuestrosCupones" >Nuestros Cupones</a>
        </li>
        <?php if ($isBusinessUnit && Configure::read('web.tuTiempoLibre.enable')): ?>
            <li>
                <a href="/<?php echo $slug; ?>/#tiempoLibre" >Tu tiempo libre</a>
            </li>
        <?php endif; ?>
        <?php
        echo $this->element(
                'userData', array(
            'user' => $user,
            'slug' => $slug
                )
        );
        ?>
    </ul>
</nav>




