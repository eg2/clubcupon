<?php
/*
 * element: exclusives/headerExclusive.ctp
 * @author: Pward 
 * params: slug - isBusinessUnit - imageURL
 * 
 */
?>

<header>
    <div class="headcontainer centerdiv clearfix">

        <div class="mainLogo" >
            <?php if ($isBusinessUnit): ?>
                <img src="/img/web/exclusives/nuestrosBeneficios/logo.png" alt="Club Cupon" width="359" height="67" >
            <?php else: ?>
                <img src="/img/theme_clean/exclusive/logo.png" alt="Exclusive" width="160" height="63" >
            <?php endif; ?>	
        </div>
        <?php
        echo $this->element(
                'exclusives/logoUDN', array(
            'slug' => $slug,
            'imageURL' => $imageURL,
            'isBusinessUnit' => $isBusinessUnit
                )
        );
        ?>
    </div>
</header>

<?php
echo $this->element('exclusives/navExclusive', array(
    'slug' => $slug,
    'isBusinessUnit' => $isBusinessUnit,
    'user' => $user,
    'slug' => $slug
        )
);
?>


