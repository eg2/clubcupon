<?php 
/*
 * element: exclusives/deal.ctp
 * @author: Pward 
 * paramos: deal - slug - empresa
 */

    $title = $deal->title;
    $titleShort = cutText($title, 70);
    $title = htmlspecialchars($title);

    if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
        if ($isBusinessUnit) {
            $srcImg = Configure::read('web.pluginDebugImages.dummyUDNListElementUrlImage');
        } else {
            $srcImg = Configure::read('web.pluginDebugImages.dummyUDNListElementUrlImageCC');
        }
    } else {
        $srcImg = $deal->imageURL;
    }

    $title = htmlspecialchars($title);
?>

<article>
	<a href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>" title="<?php echo $title; ?>">
		<img src="<?php echo $srcImg; ?>" alt="cupon" width="<?php echo $width;?>" height="<?php echo $height;?>" >
		<span class="title"><?php echo $titleShort; ?></span>
	<?php if (!$deal->onlyPrice): ?>
		<?php if (!$deal->hidePrice): ?>			
		<span class="price"><span class="oldPrice">$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></span>
		<?php else: ?>
		<span class="price"><?php echo $deal->discountedPercent; ?>%</span>
		<?php endif; ?>
	<?php else: ?>
		<span class="price">$<?php echo $deal->discountedPrice; ?></span>
	<?php endif; ?>
	</a>
</article>


