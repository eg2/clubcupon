<?php
/*
 * element: mainDeal.ctp
 * @author: Pward 
 * params: deal - slug 
 */
?>
<?php
$title = $deal->title;
$description = $deal->description;

$titleShort = cutText($title, 28);
$title = htmlspecialchars($title);
$description = cutHtml($description, 38, 60);

if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
    if ($isBusinessUnit) {
        $srcImg = Configure::read('web.pluginDebugImages.dummyUDNFirstUrlImage');
    } else {
        $srcImg = Configure::read('web.pluginDebugImages.dummyUDNFirstUrlImageCC');
    }
} else {
    $srcImg = $deal->imageURL;
}
?>
<?php if (!empty($deal)): ?>
    <article>
        <a href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>"><img src="<?php echo $srcImg; ?>" width="460" height="326" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" ></a>
        <div>
            <h3><?php echo $titleShort; ?></h3>
            <p><?php echo $description; ?></p>
            <?php if (!$deal->onlyPrice): ?>
                <?php if (!$deal->hidePrice): ?>			
                    <p class="precio"><span>$<?php echo $deal->price; ?></span> $<?php echo $deal->discountedPrice; ?></p>
                <?php else: ?>
                    <p class="precio"><?php echo $deal->discountedPercent; ?>%</p>
                <?php endif; ?>
            <?php else: ?>
                <p class="precio">$<?php echo $deal->discountedPrice; ?></p>
            <?php endif; ?>
            <a class="botonExclusive" href="/<?php echo $slug . Configure::read('web.extraDealURLPath') . $deal->slug; ?>">Ver oferta</a>
        </div>
    </article>
<?php endif; ?>
