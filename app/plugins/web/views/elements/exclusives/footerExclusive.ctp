<?php 
/*
 * element: footerExclusive.ctp
 * @author: Pward 
 * @params: slug - isBusinessUnit
 */
?>

<footer>
	<ul>
    <?php if(!$isBusinessUnit) {?>
		<li><a href="http://soporte.clubcupon.com.ar/" class="first">Atención al cliente</a></li>
		<li>|</li>
    <?php } ?>
		<li><a href="/<?php echo $slug; ?>/pages/protection">Protección de Datos Personales</a></li>
		<li>|</li>
		
		<?php if($isBusinessUnit): ?>
		<li><a href="/<?php echo $slug; ?>/pages/terms-nb">Términos y Condiciones</a></li>
		<li>|</li>
		<li><a href="/<?php echo $slug; ?>/pages/pol&iacute;ticas-nb">Políticas de Privacidad</a></li>
		<li>|</li>
		<li><a href="/<?php echo $slug; ?>/pages/FAQ-nb">Preguntas Frecuentes</a></li>
		<li>|</li>
		<li><a href="http://www.buenosaires.gob.ar/defensaconsumidor">Defensa al Consumidor</a></li>		
		<?php else:?>
		
		<li><a href="/<?php echo $slug; ?>/pages/terms">Términos y Condiciones</a></li>
		<li>|</li>
		<li><a href="/<?php echo $slug; ?>/pages/policy">Políticas de Privacidad</a></li>
		<li>|</li>
		<li><a href="http://www.buenosaires.gob.ar/defensaconsumidor">Dirección General de Defensa y Protección al Consumidor</a></li>
		<?php endif; ?>
	</ul>
	<p>Copyright © 2012 Derechos Reservados.</p>
</footer>