<?php
/*
 * element: exclusives/logoUDN.ctp
 * @author: Pward 
 * params: imageURL - isBusinessUnit
 */
?>

<div class="contLogo">
    <img class="logo" src="<?php echo $imageURL; ?>" alt="">
</div>