<?php
/*
 * element: homeMainDeals.ctp
 * @author: Pward 
 * params: deal1, deal2
 */
?>


<section id="destacados">
    <?php
    echo $this->element(
            'exclusives/mainDeal', array(
        'slug' => $slug,
        'deal' => $deal1,
        'isBusinessUnit' => $isBusinessUnit,
        'whidth' => $width,
        'height' => $height
            )
    );

    if (!empty($deal2)) {
        echo $this->element(
                'exclusives/mainDeal', array(
            'slug' => $slug,
            'deal' => $deal2,
            'isBusinessUnit' => $isBusinessUnit,
            'whidth' => $width,
            'height' => $height
                )
        );
    }
    ?>
</section>
