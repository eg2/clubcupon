<?php 
/*
 * element: dealCountdown.ctp
 * @author: Pward 
 * params: date
 */
?>
<div id="finaliza">
	<div id="deal_countdown_amout" style="display: none;"><?php echo intval(strtotime($fechaFinalizacion) - time()); ?></div>
	<p>Este cupón termina en:</p>
	<img class="clock" src="<?php if ($isCorporate): ?>/img/web/exclusives/nuestrosBeneficios/reloj.jpg<?php else: ?>/img/web/clubcupon/reloj.jpg<?php endif; ?>">
	<span class="horas-num"><?php echo intval(strtotime($fechaFinalizacion) - time()); ?></span>
</div>

