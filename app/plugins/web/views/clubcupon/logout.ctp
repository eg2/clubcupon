<?php
$title_for_layout = 'Club Cupon';
$data = $this->viewVars['data'];
$dataObj = json_decode($data);
$slug = $dataObj->city->slug;
?>
<section id="content">
    <h1 class="registro"><span>Has cerrado tu sesi&oacute;n</span></h1>
    <div class="logout-links">
        <a href="/users/login"> Ingresar</a> o <a href="/users/register">Registrate</a>
    </div>
    <section id="content">
        <?php
        if(Configure::read('web.ad_server') == 'eplanning'){
            echo $this->element('eplLogOut');
        }
        ?>
        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
        <div  style="width: 100%; margin: 10px auto; position: relative; text-align:center;">
            <div style="display:inline-block; margin: 10px;">
                <!-- <img src="http://dummyimage.com/940x520/000/fff" alt="Dummy image"> -->
                <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                    googletag.pubads().definePassback('/261718750/ClubCupon_Logoutx940x520', [940, 520]).display();
                </script>                
            </div>
        </div>
        <?php endif; ?>
    </section>
</section>
