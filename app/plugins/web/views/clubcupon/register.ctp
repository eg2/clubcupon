<?php
$dataObj = json_decode($data);

$slug = $dataObj->city->slug;
$errors = $dataObj->vData->errors;

echo $this->element('facebookConnect');

if ($session->check('Message.flash')) {
    $session->flash();
}
?> 
<script type="text/javascript">
<?php echo 'var __cfg = ' . $javascript->object($js_vars_facebook['cfg']) . ';'; ?>
</script>

<section id="content">

    <h1 class="registro"><span>Registrate</span> si ya tenés una cuenta <a href="/<?php echo $slug; ?>/users/login">Ingresá</a></h1>

    <article class="modulo_registro">
        <?php
        echo $form->create('User', array('url' => "/$slug/users/register", 'class' => ''));

        echo $form->input('username', array('info' => '', 'label' => 'Nombre de usuario'));

        echo $form->input('UserProfile.dni', array('label' => 'DNI'));

        echo '<div class="email-container">';
        echo $form->input('email', array('label' => 'Mail'));
        echo $form->input('confirm_email', array('label' => 'Confirmar mail', 'onpaste' => 'return false;'));
        echo '</div>';

        if (empty($this->data['User']['fb_user_id'])) {
            echo $form->input('passwd', array('label' => 'Contraseña'));
            echo $form->input('confirm_password', array('type' => 'password', 'label' => 'Repetir contraseña', 'onpaste' => 'return false;'));
        }

        if (!empty($this->data['User']['fb_user_id'])) {
            echo $form->input('fb_user_id', array('type' => 'hidden', 'value' => $this->data['User']['fb_user_id']));
        }
        echo'<div class="captcha-container">';
        echo'<label>Ingresá el código de seguridad</label>';
        echo'<div class="captcha captcha-block js-captcha-container">';
        echo $html->image(Router::url(array('plugin' => '', 'controller' => 'users', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('title' => 'CAPTCHA', 'class' => 'floatLeft captcha-img'));
        echo '<div class="captcha-right">';
        echo $html->link($html->image('captcha_reload.png'), '#', array('class' => 'js-captcha-reload captcha-reload', 'title' => 'Actualizar CAPTCHA', 'escape' => false));
        echo '<br />';
        echo $html->link($html->image('captcha_audio.png'), array('plugin' => '', 'controller' => 'users', 'action' => 'captcha_play'), array('title' => 'Version Audio', 'rel' => 'nofollow', 'escape' => false));
        echo '</div>';
        echo '</div>';
        echo $form->input('captcha', array('label' => '', 'class' => 'js-captcha-input'));
        echo '</div>';

        echo '<div class="terms">';
        echo $form->input(
                'is_agree_terms_conditions', array(
            'type' => 'checkbox',
            'label' => sprintf(
                    'He le&iacute;do y acepto los %s del servicio', $html->link(
                            'T&eacute;rminos y Condiciones', array(
                        'plugin' => '',
                        'controller' => 'pages',
                        'action' =>
                        'terms',
                        'admin' => false
                            ), array(
                        'target' =>
                        '_blank',
                        'escape' =>
                        false
                            )
                    )
            ),
            'class' => 'chk'
                )
        );
        echo $form->input('is_agree_daily_subscriptions', array('type' => 'checkbox', 'checked' => 'checked', 'label' => 'S&iacute;, quiero recibir mails con las ofertas diarias de Club Cup&oacute;n', 'class' => 'chk'));
        if (Configure::read('now.is_enabled')) {
            echo $form->input('wants_to_be_a_company', array('type' => 'hidden', 'value' => '0'));
        }
        echo '</div>';
        echo $form->submit('Ingresar', array('id' => 'bt_enviar', 'class' => 'buttonRegistro btn-naranja'));
        echo '</ul>';
        echo $form->end();
        ?>
        <div style="float: left">
            <p style="color:#999999;font-size: xx-small; padding: 10px; line-height: 20px">
                Recabamos sus datos con la finalidad de administrar y gestionar la actividad de acceso a nuestros servicios que lo vinculan a nuestra Empresa, conforme lo establecido por el art. 6 de Ley 25323 Usted es responsable por la inexactitud o negativa de proporcionar sus datos.<br /><br />
                .El titular de datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter&eacute;s leg&iacute;timo al efecto, conforme lo establecido en el art&iacute;culo 14, inciso 3 de la Ley n&ordm; 25.326".<br />
                La direcci&oacute;n Nacional de Datos Personales, &Oacute;rgano de Control de la Ley n&ordm; 25.326, tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales."<br />
                Usted podr&aacute; ejercer el derecho de acceso, rectificaci&oacute;n, y supresi&oacute;n de sus datos conforme lo establecido en el art. 6 de la Ley 25326, enviando un email a datospersonales@clubcupon.com.ar o comunicandose telefonicamente al &nbsp;+5411 49438700<br /><br />
                <span style="font-weight: bold">Para contactar a la Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales:<br /></span>
                Sarmiento 1118, 5&ordm; piso (C1041AAX)<br />
                Tel. 4383-8510/12/13/15<br />
                <a href="http://www.jus.gov.ar/datospersonales">www.jus.gov.ar/datospersonales</a><br />
                <a href="mailto:infodnpdp@jus.gov.ar">infodnpdp@jus.gov.ar</a>
            </p>
        </div>
    </article>

    <article class="modulo_registro last">
        <p>o utilizá tu cuenta de Facebook</p>
        <a id="facebookLoginButton" title="fconnect" rel="nofollow" href="#">Ingresar</a>
    </article>

</section>