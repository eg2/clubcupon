<?php 
    $dataObj = json_decode($data);

    $user = $dataObj->user;
    $slug = $dataObj->city->slug;
    $message = $dataObj->message->text;
    $codeMessage = $dataObj->message->code;
    
    echo $this->element('facebookConnect');
    
    if ($session->check('Message.success')){$session->flash('success');}
    if ($session->check('Message.flash'))  {$session->flash();}
	if($dataObj->vData->redirectUrl!=null){
		$redirect='?f='.$dataObj->vData->redirectUrl;
	}else{
		$redirect='';
	}
?> 
<script type="text/javascript">
    <?php echo 'var __cfg = ' . $javascript->object($js_vars_facebook['cfg']) . ';'; ?>
</script>

<div id="container">

    <section id="content">

        <h1 class="registro"><span>Ingresá</span> o <a href="/<?php echo $slug; ?>/users/register">Registrate</a></h1>

        <article class="modulo_login">
            <form action="/<?php echo $slug; ?>/users/login<?php echo $redirect; ?>" method="post">

                <div class="email">
	                <label>Mail</label>
	                <input name="email" type="email" value="" placeholder>
	                <?php if ($codeMessage == 2): ?>
	                <p class="errorMessage"><?php echo $message; ?></p>
	                <?php endif; ?>
                </div>
                
                <div class="email">
                	<label>Contraseña</label>
                	<input name="password" type="password" value="" placeholder>
                </div>

                <a href="/<?php echo $slug; ?>/users/forgot_password" class="login">¿Olvidaste tu contraseña?</a>
                
                <!-- falta funcionalidad? -->
                <div class="sessionClose">
                	<input name="sessionClose" type="checkbox">
                	<label class="check">No cerrar sesión</label>
                </div>

                <input type="submit" value="Ingresar" class="gradient">
                
                <?php if ($codeMessage != 2): ?>
                <p class="errorMessage"><?php echo $message; ?></p>
                <?php endif; ?>
            </form>
        </article>
        <article class="modulo_login last">
            <p>o utilizá tu cuenta de Facebook</p>
            <a id="facebookLoginButton" title="fconnect" rel="nofollow" href="#">Ingresar</a>
        </article>

    </section>

</div>

