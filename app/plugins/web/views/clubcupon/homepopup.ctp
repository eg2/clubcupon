<script type="text/javascript">
    <?php echo 'var __cfg = ' . $javascript->object($js_vars_facebook['cfg']) . ';'; ?>
</script>
<?php
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    $head_parameters = array ('css_layout' => 'popup'); //para determinar que hoja de estilos levantamos
    echo $this->element('clubcupon/headpopup',$head_parameters ); // nuevo 
	$data = $this->viewVars['data'];
	$dataObj = json_decode($data);
	$citiesLinkList = $dataObj->vData->citiesLinkList;
	$citiesLinks = $citiesLinkList->citiesLinks;
	$slug = $dataObj->city->slug;
	echo $this->element('facebookConnect',array('subscribe'=>'si'));
    if (Configure::read('app.rDebug.enable')) { rdebug('dataObj', $dataObj); }
	?>
<body>
    
<script>
    $(document).ready(function() {
    	$("#SubscriptionEmail").val('Ingresá tu mail');
        var submitObj = document.getElementById('suscript');
        verifyEmail();
        $("#SubscriptionEmail").bind({
            'click': function(){
                if ($(this).val()=='Ingresá tu mail') {
                    $(this).val('');
                }
            },
            'blur': function(){
                if($(this).val()==''){
                    $(this).val('Ingresá tu mail');
                }
            }
        });
        function verifyEmail()
        {
            var status = false;
            var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if ($('.mail').val().search(emailRegEx) == -1)
            {
				$("input.suscription").addClass('compraroff');
                submitObj.disabled = true;
            }
            else
            {
                $("input.suscription").removeClass('compraroff');
				submitObj.disabled = false;
                status = true;
            }
            return status;
        }
        $(".mail").keyup(function() {;
            verifyEmail();
        });
    });  //fin doc ready

</script>

<div class="bloque-pop clearfix">
	<div id="cc-popup">     
        
		<img src="/img/web/clubcupon/banner_popup.jpg" alt="alter" width="620" height="313"/>
	    <form class="normal" enctype="multipart/form-data" id="SubscriptionAddForm" method="post" action="/ciudad-de-buenos-aires/addsubscription" target="_top">
			<fieldset style="display:none;">
				<input type="hidden">
				<input type="hidden">
			</fieldset>	        
			<div class="input text required">
				<input name="SubscriptionEmail" type="text" class="mail" placeholder="Ingresá tu mail" id="SubscriptionEmail">
			</div>
			<div class="input select required">
				<select name="SubscriptionCitySlug" id="SubscriptionCitySlug">
					<?php 
					foreach ($citiesLinks as $cityLink){
						echo '<option value="'.substr($cityLink->url, 1).'"';
						if (substr($cityLink->url, 1) == $slug) {
						    echo ' selected="selected"';
						}
						echo '>'.$cityLink->label.'</option>';
					}
					?>
					</select>
			</div>                
			<input id="suscript" class="suscription compraroff" type="submit" value="Suscribirme!">
			<fieldset style="display:none;">
				<input type="hidden">
			</fieldset>
			<?php if($isCaptchaNecesary){?>
			<?php echo $this->element('clubcupon/captcha'); ?>
			
			<?php }?>
		</form>
   
		<article>
			<a id="facebookLoginButton" title="fconnect" rel="nofollow" class="suscribirme" style="cursor:pointer;">Suscribirme con Facebook</a>
			<p id="follow">Seguí nuestros descuentos en</p>
			<ul class="redes">
				<li><a href="#" onclick="parent.window.location.replace('http://www.facebook.com/ClubCupon'); return false;" class="icon_facebook" rel="nofollow">Facebook</a></li>
			</ul>
		</article>
                <div style="float: left">
                    <p style="color:#999999;font-size: xx-small; padding: 10px; line-height: 10px; margin-left: 0px;">
                        Recabamos sus datos con la finalidad de administrar y gestionar la actividad de acceso a nuestros servicios que lo vinculan a nuestra Empresa, conforme lo establecido por el art. 6 de Ley 25323 Usted es responsable por la inexactitud o negativa de proporcionar sus datos.<br /><br />
                        .El titular de datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter&eacute;s leg&iacute;timo al efecto, conforme lo establecido en el art&iacute;culo 14, inciso 3 de la Ley n&ordm; 25.326".<br />
                        La direcci&oacute;n Nacional de Datos Personales, &Oacute;rgano de Control de la Ley n&ordm; 25.326, tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales."<br />
                        Usted podr&aacute; ejercer el derecho de acceso, rectificaci&oacute;n, y supresi&oacute;n de sus datos conforme lo establecido en el art. 6 de la Ley 25326, enviando un email a datospersonales@clubcupon.com.ar o comunicandose telefonicamente al &nbsp;+5411 49438700<br /><br />
                        <span style="font-weight: bold">Para contactar a la Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales:<br /></span>
                        Sarmiento 1118, 5&ordm; piso (C1041AAX)<br />
                        Tel. 4383-8510/12/13/15<br />
                        <span >
                            <a href="http://www.jus.gov.ar/datospersonales" style="font-size: xx-small; padding: 10px 10px 10px 0px; line-height: 10px; margin-left: 0px;">
                                www.jus.gov.ar/datospersonales
                            </a>
                            <br />
                            <a href="mailto:infodnpdp@jus.gov.ar"style="font-size: xx-small; padding: 10px 10px 10px 0px; line-height: 10px; margin-left: 0px;">
                                infodnpdp@jus.gov.ar
                            </a>
                        </span>
                    </p>
                </div>
	</div>

	<!-- div class="pie">
		<?php echo $html->link ('Pol&iacute;ticas de Privacidad', array ('controller' => 'pages', 'action' => 'policy'), array ('title' => 'Pol&iacute;ticas de Privacidad', 'target' => '_top', 'escape'=>true)) ?>

		<?php if (isset ($redirect)) { ?>
			<a href="#" onclick="parent.window.location.replace('<?php echo $redirect; ?>'); return false;">Ya estoy suscripto</a>
		<?php } else { ?>
			<a href="#" onclick="parent.$.fn.colorbox.close(); return false;">Ya estoy registrado</a>
		<?php } ?>
    </div -->
</div>
   <?php 
   	  if (Configure::read('app.tracking.enable')){
   		echo $this->element('trackingWeb');
   	  }
    ?>
</body>
