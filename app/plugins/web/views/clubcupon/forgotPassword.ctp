<?php 
    $dataObj = json_decode($data);

    $slug = $dataObj->city->slug;
    $errors = $dataObj->vData->errors;

?> 

<div id="container">
    <section id="content">
        <h1 class="registro"><span>¿Olvidaste tu contraseña?</span></h1>
        <article class="modulo_registro forgot-pass">
			<p class="subtitle">Ingresá tu mail y te enviaremos instrucciones para restablecer tu contraseña.</p>

            <form action="/<?php echo $slug; ?>/users/forgot_password" method="post">
              <?php
                echo $form->create('User', array('action' => '/'.$slug.'/users/forgot_password', 'class' => 'normal'));
                echo $form->input('email', array('label' => 'Mail'));
                echo '<span class="error-message">'.$errors->email.'</span>';
                echo $form->submit('Recuperar contraseña',    array('id'     => 'bt_enviar', 'class'=>'btn-verde'));
                echo $form->end();
              ?>
        </article>
    </section>
</div>

