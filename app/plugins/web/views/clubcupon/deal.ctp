<?php 
/*
 * view: deal.ctp
 * @author: EParisi - Pward 
 * params: data
 */
?>

<?php 
    $dataObj = json_decode($data);
    
    $user = $dataObj->user;
    $slug = $dataObj->city->slug;
    $isCorporate = $dataObj->city->isCorporate;
    $isBusinessUnit = $dataObj->city->isBusinessUnit;

    $citiesLinkList = $dataObj->vData->citiesLinkList;
    $citiesLinks = $citiesLinkList->citiesLinks;
    
    $dealList = $dataObj->vData->dealList;
    $dealListTitle = $dealList->title;
    $urlList = $dealList->basePathUrl . '/todos';
    $urlText = 'Ver todas las ofertas';
    
    $dealListSearched = $dataObj->vData->dealListSearched;
    $deal = $dataObj->vData->deal;
	
	$fechaFinalizacion = $deal->endDate;
	$coupon_start_date = $deal->couponStartDate;
	$coupon_expiry_date = $deal->couponExpiryDate;
	
    if($deal->isTourism) {
        $isTourism = 'si';
    } else {
        $isTourism = 'no';
    }
    
    if ($deal->imageURL == NO_ORIGINAL_IMAGE && Configure::read('app.replaceDealImages.enable')) {
        $srcImg = Configure::read('web.pluginDebugImages.dummyDealUrlImage');
    } else {
        $srcImg = $deal->imageURL;
    }

    $ads = Configure::read('web.eplanningIds');

	$dealwidth  = Configure::read('thumb_size.maindeal_big_thumb.width');	
	$dealheight = Configure::read('thumb_size.maindeal_big_thumb.height');
	$width = Configure::read('thumb_size.listdeals_big_thumb.width');
	$height = Configure::read('thumb_size.listdeals_big_thumb.height');
	$searchedwidth  = Configure::read('thumb_size.searchedlist_big_thumb.width');
	$searchedheight = Configure::read('thumb_size.searchedlist_big_thumb.height');
?>  

<script type="text/javascript">
    <?php echo 'var __cfg = ' . $javascript->object($js_vars_facebook['cfg']) . ';'; ?>
</script>

    <?php
       if ($deal->isPurchasable == 0){
           echo $this->element('facebookConnect',array('subscribe'=>'si'));
       } else {
           echo $this->element('facebookConnect');
       }
	?>

   <?php
		if($deal->city->slug == Configure::read('City.clubcuponya')){
	?>
			<section id="banner2"><img src="/img/web/clubcupon/header_ya.png"></section>
	<?php
		} ?> 
<div id="content" class="noTop">
	<div class="adsGeneral">
        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>    
        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
            googletag.pubads().definePassback('/261718750/ClubCupon_Ficha_Topx940', [950, 50]).display();
        </script>
        <?php endif; ?>
		<?php 
              if (Configure::read('web.ad_server') == 'eplanning'){
		echo $this->element(
			'ad',
				array (
					'slug' => $slug,
					'isTourism' => $isTourism,
					'idAd' => $ads['dealTop'],
					'idAviso' => $deal->id
				)
			);
                }
		?>
	</div>

	<?php 
		if ($deal->isPurchasable == 0){
			echo $this->element(
				'clubcupon/formsubscription',
				array (
					'citiesLinks' => $citiesLinks,
					'slug'	=>$slug
					)
			);
		}
	?>
	<section id="detalle_fichas">
		<h1><?php echo $deal->title; ?>
		<?php if(($deal->tsInicio != null)&&($deal->tsFin != null)){
				$tsInicio = date("H", strtotime($deal->tsInicio));
				$tsFin = date("H", strtotime($deal->tsFin));
				echo '<div id="subtitulonow"><img src="/img/web/clubcupon/reloj_2.png"/>Para usar hoy de '.$tsInicio.'Hs a '.$tsFin.'Hs</div>';
		}?>
		</h1>
		<?php if(isset($deal->subtitle) && !empty($deal->subtitle)):?>
			<h3 class="subtitle"><?php echo $deal->subtitle; ?></h3>
		<?php endif;?>
		
		<div id="comprar">
			
			<?php if (!($deal->onlyPrice) && ($deal->hidePrice)): ?>
			<div class="precio">descuento <span><?php echo $deal->discountedPercent; ?>%</span></div>
			<?php else: ?>
			<div class="precio">precio <span>$<?php echo $deal->discountedPrice; ?></span></div>
			<?php endif; ?>
			
			<?php if ($deal->isPurchasable == 1): ?>
                            <?php if ($deal->availableQuantity > 0 || $deal->hasSubdeals ): ?>
                                <a href="/<?php echo $slug; ?>/deals/buy/<?php echo $deal->id; ?><?php echo ($isDescuentoCityRequest && $descuentoCityUtmRef) ?'/source:descuentocity.com/ref:' . $descuentoCityUtmRef:''; ?>">Comprar</a>
                            <?php else: ?>
                                <a href="javascript:void(0);" style="pointer-events: none; background: grey; text-shadow: 1px 1px #000;">Actualmente sin stock</a>
                            <?php endif; ?>
			<?php endif; ?>
			
			<div class="descuento_valor">			
				<?php if (!$deal->onlyPrice): ?>
					<?php if (!$deal->hidePrice): ?>
						<p class="discount">Descuento<span><?php echo $deal->discountedPercent; ?>%</span></p>
						<p class="oldPrice">Valor real<span>$<?php echo $deal->price; ?></span></p>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			
			<?php echo $this->element(
				'dealCountdown',
				array (
					'fechaFinalizacion' => $fechaFinalizacion,
					'isCorporate' => $isCorporate
					)
			); ?>
			
			<?php echo $this->element(
				'socialIcons',
				array (
					'shareDeal' => true,
					'dealName' => $deal->title,
					'shortUrl' => $deal->shortUrl,
					'percent' => $deal->discountedPercent,
					)
			); ?>
			
			<?php echo $this->element(
				'dealPlace',
				array (
					'deal' => $deal
					)
			); ?>	
			
			<?php 
			    echo $this->element(
                    'comoComprar',
			        array (
    					'email' => 'tucupon@clubcupon.com.ar',
					)
                ); 
		    ?>
		</div>
		
		<div id="ficha">
			<!-- imagen de la oferta -->
			<?php if ($deal->isPurchasable == 0): ?>
				<div class="mensaje">
				<?php if($deal->city->slug == Configure::read('City.clubcuponya')): ?>
					<h2>Oferta Agotada</h2>
				<?php else:?>
					<h2>Chequeá si está vigente!</h2>
					<h3>Usá el Buscador y conocé todas nuestras Ofertas</h3>
					<?php /* <a href="#">Ayuda</a> */ ?>
				<?php endif; ?>
				</div>
			<?php endif;?>
			
			<img id="deal_image" width="<?php echo $dealwidth;?>" height="<?php echo $dealheight;?>" src="<?php echo $srcImg; ?>" alt="imagen cupon"/>
                        <?php if(Configure::read('web.ad_server') == 'dfp') : ?>
			<script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_Ficha_PromoTop1', [461, 27]).display();
                        </script>
                        <?php endif; ?>
			<?php //echo $this->element('exclusives/formasPago'); ?>
			<?php //eplanning top1    
                        if (Configure::read('web.ad_server') == 'eplanning'){
			echo $this->element(
				'ad',
					array (
						'slug' => $slug,
						'isTourism' => $isTourism,
						'idAd' => $ads['top1'],
						'idAviso' => $deal->id
					)
				);
                        }
			?>
		<!-- Coupon conditions -->
		<?php if(!empty($deal->condition)): ?>
			<h3>Condiciones</h3>
			<div class="containerWrapper">
				<div id='conditions' class="container" >
					<?php echo $deal->condition; ?>										
					<div class="adsGeneral" style="display: none;">
						<?php //eplanning bottom1
                                                if (Configure::read('web.ad_server') == 'eplanning'){
						echo $this->element(
							'ad',
								array (
									'slug' => $slug,
									'isTourism' => $isTourism,
									'idAd' => $ads['bottom1'],
			                        'idAviso' => $deal->id
								)
							);
                                                }
						?>
					</div>			
				</div>
				<a class="verMas" style="display: none;" >Ver Mas</a>
				<a class="verMenos" style="display: none;" >Ver Menos</a>
			</div>
		<?php endif;?>
        <?php
        	$vigencia = "";
                        if ($deal->isVariableExpiration) {
                            $vigencia = "Válido durante " . $deal->couponDuration . " días desde el momento de la compra";
                        } else {
                            $vigencia = "Válido para canjear desde el ";
                            $coupon_start_date_cleaned = htmlentities(strftime("%d/%m/%Y", strtotime($coupon_start_date)), ENT_QUOTES, 'UTF-8');
                            $coupon_expiry_date_cleaned = htmlentities(strftime("%d/%m/%Y", strtotime($coupon_expiry_date)), ENT_QUOTES, 'UTF-8');
                            $vigencia.=$coupon_start_date_cleaned . ' hasta el ' . $coupon_expiry_date_cleaned;
                        }
        ?>
            <p class="vigencia"><?php echo $vigencia; ?></p>
            
		<?php if(!empty($deal->description)): ?>
			<!--  descripcion -->
			<h3>Más información</h3>
			<div class="containerWrapper">
				<div id='description' class="container">
					<?php echo $deal->description; ?>					
				</div>
				<a class="verMas" style="display: none;" >Ver Mas</a>
				<a class="verMenos" style="display: none;" >Ver Menos</a>
			</div>
		<?php endif;?>
		</div>
		<?php
		if($deal->city->slug == Configure::read('City.clubcuponya')){
		?>
		<div id='dealnowmap'>
			<p><?php echo $deal->company->address1;?></p>
			<img id="gmap" width="460" height="312" alt="" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $deal->latitude;?>,<?php echo $deal->longitude;?>&amp;zoom=15&amp;size=460x312&amp;markers=icon:http://i44.tinypic.com/2ij1729.png%26chld=cafe%257C996600|<?php echo $deal->latitude;?>,<?php echo $deal->longitude;?>&amp;sensor=false"> </br>
		</div>
		<?php }?>
		
	</section>
	
	<?php echo $this->element(
			'sidebar',
			array(
				'slug' => $slug,
				'isTourism' => $isTourism,
				'isCorporate' => $isCorporate,
				'dealListSearched' => $dealListSearched,
				'isBusinessUnit' => $isBusinessUnit,
				'idAviso' => $deal->id,
				'width' => $searchedwidth,
				'height' => $searchedheight,
			)
			
			); ?>	
	<hr class="divisor">
		
	<section id="cupones">
		<?php echo $this->element(
			'clubcupon/dealList',
			array (
				'dealList' => $dealList->dealList,
				'title' => $dealListTitle,
				'url' => $urlList,
				'urlText' => $urlText,
				'width' => $width,
				'height' => $height,
				)
		); ?>
		
	</section>
		
</div>  
<?php
    echo $this->element('tracking_visitas');
?>  
  <script type="text/javascript">
	$(function(){

		$("#despliega_ver_mas").click(function(event) {
		event.preventDefault();

		$("#txt_ver_mas").slideToggle();
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$('.containerWrapper').each(function (){
			if ($(this).children('.container').prop('scrollHeight') > 100){
				$(this).children().children('.adsGeneral').show();
				$(this).children('.verMas').show();
			}
		});
	
		$('.verMas').click(function () {
			$(this).siblings('.container').css('height',$(this).siblings('.container').prop('scrollHeight'));
			$(this).siblings('.verMenos').show();
			$(this).hide();
		});
		$('.verMenos').click(function () {
			$(this).siblings('.container').css('height','99');
			$(this).siblings('.verMas').show();
			$(this).hide();
		});
	});
</script>	
  
	
