<?php 

    $dataObj = json_decode($data);
    $slug = $dataObj->city->slug;

?> 

<div id="container">
    <section id="content">
        <h1 class="registro"><span>Restablecer tu contrase&ntilde;a</span></h1>
         <article class="modulo_registro forgot-pass">
            <form action="/web/users/reset/<?php echo $form->data['User']['user_id'].'/'.$form->data['User']['hash'] ?>" method="post">
              <?php
                echo $form->create ('User',             array('action' => 'reset' ,'class' => 'normal'));
                echo $form->input  ('user_id',          array('type' => 'hidden'));
                echo $form->input  ('hash',             array('type' => 'hidden'));
                echo $form->input  ('passwd',           array('type' => 'password','label' => 'Introducir una nueva contrase&ntilde;a' ,'id' => 'password'));
                echo $form->input  ('confirm_password', array('type' => 'password','label' => 'Confirmar contrase&ntilde;a'));
                echo $form->submit ('Cambiar contrase&ntilde;a',    array('id'     => 'bt_enviar', 'class'=>'btn-verde', 'escape'=>false));
                echo $form->end    ();
              ?>
        </article>
    </section>
</div>

