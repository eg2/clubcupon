<?php 
/*
 * view: clubcupon/all.ctp
 * @author: EParisi - Pward 
 * params: data
 */
?>

<?php 
	$width = Configure::read('thumb_size.listdeals_big_thumb.width');
	$height = Configure::read('thumb_size.listdeals_big_thumb.height');
?>  

<?php
	$dataObj = json_decode($data);
	
	$user = $dataObj->user;
	$slug = $dataObj->city->slug;
	
	$dealList = $dataObj->vData->dealList;
	$bannerList = $dataObj->vData->bannerList;
	
	
?>
<div id="content">
	<?php if (!empty($dealList)): ?>
		<?php echo $this->element(
			'clubcupon/dealList',
			array (
				'dealList' => $dealList,
				'title' => "Nuestros Cupones",
				'width' => $width,
				'height' => $height,
				'bannerList' => $bannerList->dealList,
			)
		); ?>
	<?php else: ?>
	<div class="noDeals">Sin Ofertas Activas.</div>
	
	<?php endif; ?>
</div>
