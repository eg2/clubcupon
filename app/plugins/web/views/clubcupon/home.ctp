<?php
$dataObj = json_decode($data);

$user = $dataObj->user;
$slug = $dataObj->city->slug;
$cityName = $dataObj->city->name;

$linkList = $dataObj->vData->categoryLinkList;
$dealA = $dataObj->vData->dealA;
$dealListA1 = $dataObj->vData->dealListA1;
$dealListA2 = $dataObj->vData->dealListA2;
$dealListA3 = $dataObj->vData->dealListA3;
$dealListB = $dataObj->vData->dealListB;

$bannerList = $dataObj->vData->bannerList;

$dealListA1Title = $dealListA1->title;
$dealListBTitle = $dealListB->title;
$urlListA1 = $dealListA1->basePathUrl . '/todos';
$urlListB = $dealListB->basePathUrl . '/todos';
$urlText = 'Ver todas las ofertas';
$subscriptionOk = $dataObj->trackingEvent->subscriptionOk;
$registerOk = $dataObj->trackingEvent->registerOk;
$MainDealwidth = Configure::read('thumb_size.outstanding_big_thumb.width');
$MainDealheight = Configure::read('thumb_size.outstanding_big_thumb.height');
$listDealwidth = Configure::read('thumb_size.listdeals_big_thumb.width');
$listDealheight = Configure::read('thumb_size.listdeals_big_thumb.height');

if ($dealA->isTourism) {
    $isTourism = 'si';
} else {
    $isTourism = 'no';
}

$ads = Configure::read('web.eplanningIds');
?>  
<div class="bg_destacado">
    <div class="cont_destacado">
        <?php
        echo $this->element(
                'clubcupon/linkList', array(
            'slug' => $slug,
            'linkList' => $linkList
                )
        );
        ?>

        <?php
        echo $this->element(
                'clubcupon/mainDeal', array(
            'slug' => $slug,
            'deal' => $dealA,
            'width' => $MainDealwidth,
            'height' => $MainDealheight,
            'ads' => $ads,
            'isTourism' => $isTourism
                )
        );
        ?>
    </div>
</div>
<section id="content">

    <div class="homeBanners homeBanners1">
        <div class="banner" style="padding-bottom: 10px">
            <?php echo $this->element('clubcupon/homeHome950x50'); ?>
        </div>

        <div class="banner">
            <?php if (Configure::read('web.ad_server') == 'dfp') : ?>
                <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                    googletag.pubads().definePassback('/261718750/ClubCupon_Home_Top_940', [930, 300]).display();
                </script>                    
            <?php endif; ?>
            <?php
            if (Configure::read('web.ad_server') == 'eplanning') {
                echo $this->element(
                        'adNormal', array(
                    'idAd' => $ads['homeTop940'],
                    'isTourism' => $isTourism,
                    'idAviso' => $dealA->id,
                    'slug' => $slug,
                    'width' => '940',
                    'height' => '50',
                        )
                );
            }
            ?>
        </div>
    </div>
    <?php
    if (!empty($dealA)):

        $showMediumsDeals = true;
        $repeatTitle = false;

        if (!empty($dealListA1->dealList) || !empty($bannerList->dealList)):
            ?>	
            <?php
            echo $this->element(
                    'clubcupon/dealList', array(
                'dealList' => $dealListA1->dealList,
                'bannerList' => $bannerList->dealList,
                'title' => $dealListA1Title,
                'url' => $urlListA1,
                'urlText' => $urlText,
                'width' => $listDealwidth,
                'height' => $listDealheight,
                'showMediumsDeals' => $showMediumsDeals,
                    )
            );
            $showMediumsDeals = false;
            ?>

            <div class="homeBanners homeBanners1">
                <?php /*
                  <img alt="EJEMPLO" src="/img/web/clubcupon/index_banner_small.jpg">
                  <img alt="EJEMPLO2" src="/img/web/clubcupon/index_banner_small2.jpg">
                 */ ?>

                <div class="banner">
                    <?php if (Configure::read('web.ad_server') == 'dfp') : ?>
                        <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_Home_Middle1x462', [950, 50]).display();
                        </script>
                    <?php endif; ?>
                    <?php
                    if (Configure::read('web.ad_server') == 'eplanning') {
                        echo $this->element(
                                'adNormal', array(
                            'idAd' => $ads['homeMiddle1x462'],
                            'isTourism' => $isTourism,
                            'idAviso' => $dealA->id,
                            'slug' => $slug,
                            'width' => '462',
                            'height' => '139'
                                )
                        );
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($dealListA2->dealList)): ?>	
            <?php
            echo $this->element(
                    'clubcupon/dealList', array(
                'dealList' => $dealListA2->dealList,
                'title' => null,
                'url' => null,
                'urlText' => null,
                'width' => $listDealwidth,
                'height' => $listDealheight,
                'showMediumsDeals' => $showMediumsDeals,
                    )
            );
            $showMediumsDeals = false;
            ?>

            <div class="homeBanners homeBanners2">
                <?php if (Configure::read('web.ad_server') == 'dfp') : ?>
                    <script type='text/javascript' src='https://www.googletagservices.com/tag/js/gpt.js'>
                            googletag.pubads().definePassback('/261718750/ClubCupon_Home_Middle3x728', [728, 90]).display();
                    </script>
                <?php endif; ?>
                <?php /* 					
                  <img alt="EJEMPLO" src="/img/web/clubcupon/index_banner_large.jpg">
                 */ ?>

                <?php
                if (Configure::read('web.ad_server') == 'eplanning') {
                    echo $this->element(
                            'adNormal', array(
                        'idAd' => $ads['homeMiddle3x728'],
                        'isTourism' => $isTourism,
                        'idAviso' => $dealA->id,
                        'slug' => $slug,
                        'width' => '728',
                        'height' => '90',
                            )
                    );
                }
                ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($dealListA3->dealList)): ?>	
            <?php
            echo $this->element(
                    'clubcupon/dealList', array(
                'dealList' => $dealListA3->dealList,
                'title' => null,
                'url' => null,
                'urlText' => null,
                'width' => $listDealwidth,
                'height' => $listDealheight,
                'showMediumsDeals' => $showMediumsDeals,
                    )
            );
            $showMediumsDeals = false;
            ?>
        <?php endif; ?>
        <?php if (!empty($dealListA1Title)): ?>
            <!-- repetir el titulo -->
            <section id="cupones">
                <h2 class="ver-todo"><?php if (!empty($urlListA1)): ?><a href="<?php echo $urlListA1; ?>">Ver todas las ofertas</a><?php endif; ?></h2>
            </section>
        <?php endif; ?>

        <?php if (!empty($dealListB->dealList)): ?>	
            <?php
            echo $this->element(
                    'clubcupon/dealList', array(
                'dealList' => $dealListB->dealList,
                'title' => $dealListBTitle,
                'url' => $urlListB,
                'urlText' => $urlText,
                'width' => $listDealwidth,
                'height' => $listDealheight,
                'showMediumsDeals' => $showMediumsDeals,
                    )
            );
            $showMediumsDeals = false;
            ?>
            <?php if (!empty($dealListBTitle)): ?>
                <!-- repetir el titulo -->
                <section id="cupones">
                    <h2 class="ver-todo"> <?php if (!empty($urlListB)): ?><a href="<?php echo $urlListB; ?>">Ver todas las ofertas</a><?php endif; ?></h2>
                </section>
            <?php endif; ?>
        <?php endif; ?>


        <div class="homeBottom">
            <?php echo $this->element('clubcupon/social-gadgets'); ?>
        </div>

    <?php else: ?>
        <div class="noDeals">Sin Ofertas Activas.</div>	
    <?php endif; ?>

</section>

<?php
if (Configure::read('app.tracking.enable')) {
    if ($subscriptionOk) {
        echo $this->element('clubcupon/trackingSubscriptionOk');
    }
    if ($registerOk) {
        echo $this->element('clubcupon/trackingRegisterOk');
    }
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.scrollpanel').scrollpanel();
    });

</script>
