<?php
$data = $this->viewVars['data'];
$dataObj = json_decode($data);
$slug = $dataObj->city->slug;
$idAviso = $dataObj->vData->ePlanning->idAviso;
$idAd = $dataObj->vData->ePlanning->idAd;
$width = $dataObj->vData->ePlanning->width;
$height = $dataObj->vData->ePlanning->height;
$idIframe = $dataObj->vData->ePlanning->idIframe;
$typeAd = $dataObj->vData->ePlanning->typeAd;
if (Configure::read('app.rDebug.enable')) {
    rdebug('dataObj', $dataObj);
}
if ($dealA->isTourism) {
    $isTourism = 'si';
} else {
    $isTourism = 'no';
}
?>
<script>
    function resizeIframe() {
        parent.document.getElementById('<?php echo $idIframe; ?>').width = document.getElementById('divIframe').scrollWidth;
        parent.document.getElementById('<?php echo $idIframe; ?>').height = document.getElementById('divIframe').scrollHeight;
    }
</script>
<body onLoad="setInterval('resizeIframe()', 200);">
    <div id="divIframe">
        <?php
        switch ($typeAd) {
            case ConstElementsEPlanning::AD:
                if (Configure::read('web.ad_server') == 'eplanning') {
                    echo $this->element(
                            'ad', array(
                        'slug' => $slug,
                        'isTourism' => $isTourism,
                        'idAd' => $ads['dealTop'],
                        'idAviso' => $deal->id
                            )
                    );
                }
                break;
            case ConstElementsEPlanning::AD_NORMAL:
                if (Configure::read('web.ad_server') == 'eplanning') {
                    echo $this->element(
                            'adNormal', array(
                        'idAd' => $idAd,
                        'isTourism' => $isTourism,
                        'idAviso' => $idAviso,
                        'slug' => $slug,
                        'width' => $width,
                        'height' => $height,
                            )
                    );
                }
                break;
        }
        ?>
    </div>
</body>



