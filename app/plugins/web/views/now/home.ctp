<?php 
// 	$data = $this->viewVars['data'];
	$dataObj = json_decode($data);
	
	$city = $dataObj->city;
	$slug = $city->slug;
	
	
	$branchesListDeals = $dataObj->vData->branchesListDeals;
	$dealList = $branchesListDeals->dealList;
	
	$width = Configure::read('thumb_size.now_listdeals_thumb.width');
	$height = Configure::read('thumb_size.now_listdeals_thumb.height');
?>  
<div class="inner-content">
	<?php if (!empty($dealList)): ?>
		<section id="cupones">
			
		<?php
		    foreach ($dealList as $deal) {
		        echo $this->element(
		            'clubcupon/deal', 
		            array(
		                'deal' => $deal,
						'width' => $width,
						'height' => $height,
		            )
		        );
		    }
		?>			
					
		</section>
	<?php else: ?>
	<div class="noDealsResult">Sin resultados para tu búsqueda</div>
	<div class="noDealsTryDeals">
	    Intentalo nuevamente o mir&aacute; todas nuestras ofertas de Club Cup&oacute;n Ya!
	    <a href="/">Ver Ofertas</a>
	</div>
	<?php endif; ?>
</div>

