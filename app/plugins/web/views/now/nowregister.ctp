<?php 
    $dataObj = json_decode($data);

    $slug = $dataObj->city->slug;
    $errors = $dataObj->vData->errors;
    if ($session->check('Message.flash'))  {$session->flash();}

?> 
<div style="display:none">
	<div id="re-ccya-pop-up" class="contenedor-popup-recib">
		<form>
		<div class="bloque-ccya-pop2">
			<!--a href="">CERRAR <span>X</span></a-->
			<img src="/img/now/logoYApop.png">
			<p class="title">Bienvenido a <span>Club Cup&oacute;n Ya!</span></p>
			<p>Para utilizar este nuevo servicio deb&eacute;s continuar con los siguientes pasos:</p>
			<div class="texto">
			<center><strong>Condiciones Generales de Contrataci&oacute;n (CGC) de CLUB CUPON YA</strong></center><br>
		<br>
		El uso del Servicio denominado "CLUB CUPON YA" brindado por el Sitio Web denominado "CLUB CUPON" de propiedad de Compa&ntilde;&iacute;a de Medios Digitales (CMD) S.A. ("CMD") alojado en la URL www.clubcupon.com/ccya (el "SITIO") se encuentra sujeto a las siguientes Condiciones Generales de Contrataci&oacute;n ("CGC"). El ingreso y el uso de la informaci&oacute;n contenida en el SITIO constituyen acuerdo expreso de los usuarios propietarios de locales comerciales con las condiciones y t&eacute;rminos aqu&iacute; expresados.
		<br>
		Dado que CMD gestiona Sitios Web con un gran volumen de visitas en Argentina y los usuarios propietarios de locales comerciales (el/los "COMERCIO/S") se encuentran interesados en participar en las oportunidades de promoci&oacute;n que ofrece CMD a trav&eacute;s del SITIO, con el fin de distribuir cupones de descuento (los "CUPONES") sobre los productos y/o servicios de su propiedad a clientes finales interesados, CMD y el/los COMERCIO/S acuerdan las siguientes cl&aacute;usulas:
		<br><br>

		<strong>1.  &Aacute;mbito de aplicaci&oacute;n:</strong><br>
		Las presentes CGC se aplican exclusivamente a personas f&iacute;sicas y/o jur&iacute;dicas que act&uacute;en en el marco de su actividad empresarial y/o profesional.
		<br><br>

		<strong>2.  Prestaciones de CLUB CUPON YA</strong>
		<br>
		2.1. El SITIO pondr&aacute; a disposici&oacute;n de los clientes finales interesados (los "CLIENTES") los CUPONES de propiedad de/l COMERCIO/S adherido/s en virtud de las presentes CGC. A tal fin, el SITIO ofrecer&aacute; los CUPONES del/los COMERCIO/S entre las diversas ofertas publicitarias publicadas simult&aacute;neamente y distribuidas de acuerdo a la Ciudad correspondiente; ello de conformidad con lo establecido en la cl&aacute;usula 5. Los CUPONES con las diversas ofertas permanecer&aacute;n vigentes entre seis (6) y veinticuatro (24) horas.
		<br>
		2.2 La solicitud de/l COMERCIOS de conformidad con lo dispuesto en la cl&aacute;usula 2.1 deber&aacute; ser realizada por &eacute;stos en forma on line, debiendo completar un formulario de registro con la totalidad de los datos solicitados en el mismo. Una vez completo, los datos incorporados son validados y aprobados por CMD. Una vez aprobados, el/los COMERCIO/S deber&aacute;n cargar la/s sucursales de/l COMERCIOS y la/s oferta/s a publicar de los CUPONES de descuento correspondientes. Esta/s oferta/s ser&aacute;n validadas por un moderador de CMD, quien, en caso de considerar que la/s misma/s no cumplen con la totalidad de los requerimientos exigidos, le solicitar&aacute; al/los COMERCIO/S, la modificaci&oacute;n de la/s oferta/s. CMD se reserva el derecho de rechazar una/s oferta/s en el supuesto en que, entre otros, el/los COMERCIO/S no cumpliere con las presentes CGC como as&iacute; tambi&eacute;n con los requisitos exigidos en la legislaci&oacute;n vigente.
		<br>
		2.3 En caso de ser aceptadas, la/s oferta/s ser&aacute; publicada/s en el SITIO a partir de las 00.00 hs. de la fecha de publicaci&oacute;n determinada por el/los COMERCIO/S. 
		<br>
		2.4 CMD no ser&aacute; responsable frente a los CLIENTES por el contenido de la/s oferta/s ni por el material gr&aacute;fico y/o comercial incluido en la/s misma/s as&iacute; como tampoco por el servicio brindado. 
		<br><br>

		<strong>3. Retribuci&oacute;n y Pago:</strong><br>
		3.1 De los importes percibidos de los CLIENTES como pago del precio de los CUPONES adquiridos por &eacute;stos, CMD retendr&aacute; para s&iacute; un porcentaje que depender&aacute; de lo acordado con el/los COMERCIOS al momento de la carga de la/s oferta/s; ello m&aacute;s el IVA y los impuestos sobre las ventas aplicables. 
		<br>
		3.2 CMD ABONAR&Aacute; AL/LOS COMERCIO/S EL PORCENTAJE ACORDADO ÚNICAMENTE SOBRE LOS CUPONES REDIMIDOS O EFECTIVAMENTE CONSUMIDOS. CASO CONTRARIO, CMD NO TENDR&Aacute; LA OBLIGACIÓN DE ABONAR LOS MISMOS.
		<br>
		3.3 Los CUPONES efectivamente consumidos deber&aacute;n ser redimidos en la WEB o a trav&eacute;s de Posnet dentro de las 48 horas h&aacute;biles posteriores a la publicaci&oacute;n. 
		<br>
		3.4 CMD se compromete a abonarle al/los COMERCIOS el porcentaje de conformidad con lo dispuesto en  los apartados precedentes, dentro de los diez (10) d&iacute;as h&aacute;biles posteriores a la publicaci&oacute;n de la/s oferta/s. 
		<br>
		3.5 El COMERCIO deber&aacute; emitir y entregar al CLIENTE la respectiva boleta o factura del producto y/o servicio adquirido.
		<br>
		3.6 CMD efectuar&aacute; el pago al/los COMERCIO/S a trav&eacute;s de transferencia bancaria a la cuenta informada por el/los COMERCIO/S en el formulario de registro.
		<br><br>

		<strong>4. Ofertas. Derecho de determinaci&oacute;n del COMERCIO.</strong><br>
		4.1 Las ofertas que el/los COMERCIO/S le efectuar&aacute;n a los CLIENTES adquirentes de los CUPONES se encontrar&aacute;n publicadas en el SITIO.
		<br>
		4.2 El/los COMERCIO/S garantizar&aacute;n a CMD y a los CLIENTES adquirentes de los CUPONES que las ofertas contenidas en los CUPONES ser&aacute;n puestas a disposici&oacute;n de &eacute;stos &uacute;ltimos en cualquier momento, siempre que se canjeen durante el per&iacute;odo de vigencia de dicho cup&oacute;n.
		<br>
		4.3 De la totalidad de las ofertas contenidas en los CUPONES ser&aacute;n responsables &uacute;nica y exclusivamente el/los COMERCIO/S. Los CLIENTES adquirentes de los CUPONES tendr&aacute;n el derecho de exigir las ofertas contenidas en los mismos &uacute;nica y exclusivamente al/los COMERCIO/S. El/los COMERCIO/S mantendr&aacute;n indemne a CMD de cualquier reclamo de CLIENTES adquirentes de los CUPONES proveniente de las ofertas contenidas en los mismos. El/los COMERCIO/S deber&aacute;n proporcionar, a su costo, toda la infraestructura y personal necesario para responder las dudas de los CLIENTES adquirentes de los CUPONES a trav&eacute;s de su propio servicio de atenci&oacute;n al cliente.
		<br>
		4.4 En caso de que se produzcan cambios en el/los COMERCIO/S, que sean relevantes para el cumplimiento de las presentes CGC, &eacute;ste debe comunicarlos inmediatamente a CMD mediante el env&iacute;o de un mail a la casilla empresas@clubcupon.com.ar.
		<br>
		4.5 La/s oferta/s publicadas por el/los COMERCIO/S podr&aacute;n finalizar por las siguientes causas:
		<br>
		(i) finalizaci&oacute;n del per&iacute;odo de vigencia establecido en la/s oferta/s y (ii) se hubiere alcanzado el l&iacute;mite m&aacute;ximo de CUPONES fijados para esa/s oferta/s.
		<br>
		4.6 CMD se reserva el derecho de finalizar una oferta/s en el supuesto en que la misma no se ajustara a los standares establecidos por &eacute;sta.   
		<br><br>

		<strong>5. Exclusividad</strong><br>
		5.1 CMD gozar&aacute; de exclusividad en la medida en que el/los COMERCIO/S se obligan a no otorgar cualquier descuento de similares caracter&iacute;sticas al/los ofrecidos a trav&eacute;s del SITIO mientras &eacute;stos se encuentren publicados en el mismo, oblig&aacute;ndose a no ofrecer por ninguna v&iacute;a (propia o de terceros) a ning&uacute;n CLIENTE mejores o iguales condiciones que las ofrecidas a trav&eacute;s del SITIO; durante ese per&iacute;odo. El/los COMERCIO/S quedan autorizados para adoptar cualquier otra medida para la promoci&oacute;n de su negocio y/o de sus servicios.
		<br><br>

		<strong>6. Utilizaci&oacute;n de datos personales.</strong><br>
		6.1 El/los COMERCIO/S prestan su consentimiento expreso para que el SITIO pueda utilizar sus datos personales, especialmente su nombre y apellido, su direcci&oacute;n, su n&uacute;mero de tel&eacute;fono y su mail, con el fin de mantener el contacto.
		<br>
		6.2 En cualquier momento el COMERCIO tiene la posibilidad de revocar su consentimiento sin necesidad de indicar ning&uacute;n motivo para ello. La revocaci&oacute;n deber&aacute; realizarse por mail a empresas@clubcupon.com.ar.
		<br><br>

		<strong>7. Propiedad intelectual y uso de la marca.</strong><br>
		7.1 Se deja expresa constancia que la propiedad intelectual, comercial e industrial del SITIO, marcas, dominios, logos, sistemas, incluidos hardware y software, etc. objeto de estas CGC, son de exclusiva propiedad de CMD. Las presentes CGC no otorgan licencia alguna al COMERCIO, excepto el derecho de uso del SITIO para el normal desarrollo de los servicios ofrecidos. Cualquier marca comercial, nombre, logo o producto asociado con el SITIO y/o sus proveedores, no podr&aacute;n ser utilizados sin la autorizaci&oacute;n por escrito de CMD. El COMERCIO otorga a CMD una licencia para utilizar sus marcas o derechos de propiedad industrial o comercial que sean necesarias para la promoci&oacute;n de sus CUPONES. 
		<br>
		7.2 El COMERCIO no obtendr&aacute; ning&uacute;n derecho sobre avisos publicitarios efectuados por CMD ni podr&aacute; registrar ninguna marca o usar nombre comercial que sea confusamente similar a las marcas comerciales de CMD.
		<br>
		7.3 El COMERCIO deber&aacute; comunicar a CMD respecto de cualquier uso indebido de sus marcas comerciales que llegue a su conocimiento y deber&aacute; asistir a CMD en todos los tr&aacute;mites y medidas que sean necesarias para defender los derechos de propiedad respecto de sus marcas comerciales.
		<br><br>

		<strong>8. Publicaci&oacute;n de ofertas</strong><br>
		8.1 El/los COMERCIO/S deber&aacute;n ofrecer sus productos y/o servicios conforme los par&aacute;metros del SITIO. 
		<br>
		Tales ofertas podr&aacute;n incluir fotograf&iacute;as, descripciones, gr&aacute;ficos, etc., siempre que en los mismos no se atente contra las Condiciones Generales y/u otras disposiciones emanadas del SITIO. Dichas descripciones, gr&aacute;ficos, fotograf&iacute;as, etc., deber&aacute;n ser fieles al producto y/o servicio de la/s oferta/s. 
		<br>
		S&oacute;lo se encuentra habilitada la carga de ofertas cuyos productos y/o servicios no se encuentre prohibida conforme estas Condiciones Generales, las dem&aacute;s pol&iacute;ticas del SITIO  y/o la legislaci&oacute;n vigente.
		<br>
		8.2 Al publicar una oferta en el SITIO, el/los COMERCIO/S otorgan a &eacute;ste una licencia para usar, modificar, ejecutar y exhibir p&uacute;blicamente, reproducir y distribuir dicha/s oferta/s en el SITIO y/o en la aplicaci&oacute;n m&oacute;vil del mismo como as&iacute; tambi&eacute;n a trav&eacute;s de distintas se&ntilde;ales televisivas (como ser Canal 13, Todo Noticias, TyC Sports, Quiero M&uacute;sica y/o Magazine, entre otros), se&ntilde;ales radiales (como ser Radio Mitre y/o FM100, entre otros) y/o medios gr&aacute;ficos (como ser Diario Clar&iacute;n, Diario Ol&eacute;, Revista Viva de Clar&iacute;n, Revista Pymes de Clar&iacute;n y/o Diario La Raz&oacute;n, entre otros), tanto en el Territorio de la Rep&uacute;blica Argentina como en los pa&iacute;ses del resto del mundo.
		<br><br>

		<strong>9. Derecho Aplicable y Jurisdicci&oacute;n competente.</strong><br>
		9.1 Esta PROPUESTA est&aacute; sujeto a las leyes de la Rep&uacute;blica de Argentina.
		<br>
		9.2 Para todos los efectos de las presentes CGC, las partes fijan su domicilio en la Ciudad de Buenos Aires, Argentina y se someten a la jurisdicci&oacute;n de los Tribunales Ordinarios en lo Comercial, renunciando a cualquier otro fuero o jurisdicci&oacute;n.
		<br><br>
			</div>
		</div>
		<!--div class="conte-check">
		<input type="checkbox" /><label>He leido y acepto los t&eacute;rminos y condiciones del servicio</label>
		<input class="continuar btn-celeste" type="submit" value="Continuar" />
		</div-->
		</form>
	</div>
</div>
<section id="content">
	<div id="ccnow">
		<div class="grid_16">
			<div class="logonewccnow clearfix">
				<img src="/img/theme_clean/logoYA.png" class="logoya">
				<h2 class="potencialesc">La nueva forma de publicar ofertas instant&#225;neas  y ganar nuevos clientes.</h2>
			</div>
		</div>	
	</div>
	<h1 class="registro"><span>Registrate</span> si ya tenés una cuenta <a href="/ciudad-de-buenos-aires/users/login">Ingresá</a></h1>

	<article class="modulo_registro">
	    <?php 
	    
        echo $form->create('User', array('url' => "/$slug/users/registernow", 'class' => ''));
	    echo $form->input('username', array('type' => 'hidden', 'value' => 'empresa'));
	    echo $form->input('UserProfile.dni', array('type' => 'hidden','value' => '99999999'));
	    echo '<div class="email-container">';
	    echo $form->input('email', array('label' => 'Mail'));
	    echo $form->input('confirm_email', array('label' => 'Confirmar mail','onpaste' => 'return false;'));
		echo '</div>';

		echo $form->input('passwd', array('label' => 'Contraseña'));

	    echo $form->input('confirm_password', array('type' => 'password', 'label' => 'Confirmar contraseña', 'onpaste' => 'return false;'));
	    
	    echo'<div class="captcha-container">';
	    echo'<label>Código de Seguridad</label>';
	    echo'<div class="captcha captcha-block js-captcha-container">';
	    echo $html->image(Router::url( array('plugin' => '', 'controller' => 'users', 'action' => 'show_captcha', md5(uniqid(time()))), true), array('title' => 'CAPTCHA', 'class' => 'floatLeft captcha-img'));
	    echo '<div class="captcha-right">';
	    echo $html->link($html->image('captcha_reload.png'), '#', array('class' => 'js-captcha-reload captcha-reload', 'title' => 'Actualizar CAPTCHA', 'escape' => false));
	    echo '<br />';
	    echo $html->link($html->image('captcha_audio.png'), array('plugin' => '', 'controller' => 'users', 'action' => 'captcha_play'), array('title' => 'Version Audio', 'rel' => 'nofollow', 'escape' => false));
	    echo '</div>';
	    echo '</div>';
	    echo $form->input('captcha', array('label' => '','class' => 'js-captcha-input'));
	    echo '</div>';
	    
	    echo '<div class="terms">';?>
	    <input type="hidden" name="data[User][is_agree_terms_conditions]" id="UserIsAgreeTermsConditions_" value="0"><input type="checkbox" name="data[User][is_agree_terms_conditions]" href="#re-ccya-pop-up" class="chk" value="1" id="UserIsAgreeTermsConditions">
	    <label for="UserIsAgreeTermsConditions"> He leído y acepto los 
	    <a class="ver-tyc cboxElement" href="#re-ccya-pop-up">Términos y Condiciones</a> 
	    del servicio</label>
	    <?php 
	    echo '</div>';
	    echo $form->submit('Ingresar', array('id' => 'bt_enviar','class'=>'buttonRegistro'));
	    echo '</ul>';
	    echo $form->end();

	    ?>
	<div class="grid_7 omega">
			<div class="modulo-colder blq-conoce clearfix">
				<p class="conocermas">&#191;Quer&#233;s conocer m&#225;s de <strong>Club Cup&#243;n <span>Ya</span></strong>?</p>
				<!--input type="button" value="CONOC&#201; M&#193;S" class="btn-naranja2"-->
				<?php echo $html->link ('CONOC&#201; M&#193;S', array ('controller' => 'now_registers', 'plugin'=>'now','action' => 'landing' ), array('escape' => false,'class'=>'btn-naranja2','style'=>'width:50%;margin-left:70px')); ?>
				
			</div>
		</div>
	</article>

	<article class="modulo_registro">
	<div id="ccnow">	
	<div class="grid_9 ">
			
			<div class="modulo-colizq clearfix">
				<img alt="" src="/img/theme_clean/modulo0.jpg">
				<div class="textouno h100">
					<h4>Cre&#225; tu Cuenta como Empresa</h4>
					<p>Registrate como empresa en Club Cup&#243;n Ya! y comenz&#225; a publicar ofertas instant&#225;neas</p>
				</div>				
			</div>	
			
			<div class="modulo-colizq clearfix">
				<img alt="" src="/img/theme_clean/modulo1.jpg">
				<div class="texto h150">
					<h4>Agreg&#225; tus Sucursales</h4>
					<p>Agreg&#225; todas las sucursales con las que 
						cuentes. Ten&#233; en cuenta que, cuantas m&#225;s 
						sucursales habilites para el canje, mayor 
						ser&#225; el &#233;xito de tu oferta.</p>
				</div>				
			</div>
			
			<div class="modulo-colizq clearfix">
				<img alt="" src="/img/theme_clean/modulo2.jpg">
				<div class="texto h150">
					<h4>Public&#225; tu Oferta</h4>
					<p>Eleg&#237; la oferta que quer&#233;s publicar, el 
						descuento que dese&#225;s ofrecer, cu&#225;ndo
						y durante cu&#225;nto tiempo publicarlo.</p>
				</div>				
			</div>

			<div class="modulo-colizq clearfix">
				<img alt="" src="/img/theme_clean/modulo3.jpg">
				<div class="texto h150">
					<h4>Canje&#225; tu Cupones vendidos</h4>
					<p>Los usuarios podr&#225;n encontrar tu oferta, 
						comprarla v&#237;a web o celular y recibir el c&#243;digo 
						de descuento para concretar el canje en el 
						momento. Se acercar&#225;n a la sucursal m&#225;s 
						cercana a su ubicaci&#243;n en el horario estipulado 
						para disfrutar de tu oferta.</p>
				</div>				
			</div>
		</div>
       </div>     
	</article>

</section>

<script type = "text/javascript">
  $(document).ready (function () {
	$('.ver-tyc').colorbox({
		inline:true,
		onLoad:function(){
			$('#cboxClose').html('X').addClass('cerrarPopupTyC');
		}
	});
  });
</script>