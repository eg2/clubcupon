<?php

class WebUser extends WebAppModel {

    public $name = 'WebUser';
    public $useTable = 'users';

    function findByUserId($id) {

        if(empty($id)) { return false; }

        $conditions = array ();
        $conditions ['WebUser.id'] = $id;

        $user = $this->find ('all', array (
                        'conditions' => array (
                                        $conditions,
                        ),
                        'recursive' => 0,
        ));
        return $user;
    }

    public function findByCookieHash($cookie_hash) {
        $user = $this->find(
            'first',
            array(
                'conditions' => array(
                    'User.cookie_hash =' => $cookie_hash,
                    'User.is_active' => 1
                ),
                'fields' => array(
                    'User.id', 
                    'User.email',
                    'User.username',
                    'User.password'
                ),
                'recursive' => -1
            )
        );
        return $user;
    }
  
    public function findByUserName($user_name) {
    	$user = $this->find(
    			'first',
    			array(
    					'conditions' => array(
    							'username' => $user_name
    					),
    					'recursive' => -1
    			)
    	);
    	return $user;
    }

    function isRegularUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::User;
    }

    function isAgencyUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::Agency;
    }

    function isPartnerUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::Partner;
    }

    function isSellerUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::Seller;
    }

    function isAdminUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::Admin;
    }

    function isSuperAdminUser($data) {
        return $data['WebUser']['user_type_id'] == ConstUserTypes::SuperAdmin;
    }
    
}

