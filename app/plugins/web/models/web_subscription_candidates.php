<?php

class WebSubscriptionCandidates extends WebAppModel{

	public $name = 'WebSubscriptionCandidates';
	public $useTable = 'subscription_candidates';
	
	public $dataWebSubscriptionCandidates;
	var $belongsTo = array(
			'User' => array(
					'className' => 'User',
					'foreignKey' => 'user_id',
					'conditions' => '',
					'fields' => '',
					'order' => '',
			),
			'City' => array(
					'className' => 'City',
					'foreignKey' => 'city_id',
					'conditions' => '',
					'fields' => '',
					'order' => '',
			)
	);
	
	function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
       	$this->validate = array(
            'city_id' => array(
                'rule' => 'numeric',
                'allowEmpty' => false,
                'message' => __l('Required')
            ),
            'email' => array(
                'rule2' => array(
                    'rule' => 'email',
                    'allowEmpty' => false,
                    'message' => __l('Please enter valid email address')
                ),
                'rule1' => array(
                    'rule' => 'notempty',
                    'allowEmpty' => false,
                    'message' => __l('Required')
                )
            ),
            'utm_source' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_medium' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_term' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_content' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            ),
            'utm_campaign' => array(
                'rule' => array(
                    'custom',
                    '/^[a-z0-9_-]{1,}$/i'
                )
            )
        );
        $this->moreActions = array(
            ConstMoreAction::Delete => __l('Delete'),
            ConstMoreAction::UnSubscripe => __l('Unsubscribe'),
        );
    }
	
	function findById($id) {
		$subscription = $this->find(
				'first',
				array(
						'conditions' => array(
								'id' => $id
						),
						'recursive' => -1
				)
		);
		return $subscription;
	}

  public function findByEmailAndCityId($email, $cityId) {
		return $this->find(
				'first',
				array(
						'conditions' => array(
								'email' => $email,
								'city_id'=>$cityId
						),
						'recursive' => -1
          ));
  }
	
	public function verifiSubscription($email,$city_id){
		$subscription = $this->find(
				'first',
				array(
						'conditions' => array(
								'email' => $email,
								'city_id'=>$city_id
						),
						'recursive' => -1
				)
		);
		if (!empty($subscription)) {
			return true;
		}else {
			return false;
		}
	}
	
	public function subscribe($email, $city, $ip, $user, $campaignInfo = null){
		if($this->verifiSubscription($email, $city['WebCity']['id'])){
			return false;
		}else{
			if($user!=null){
				$user_id = $user['WebUser']['id'];
			}else{
				$user_id=null;
			}
			$data =  array('WebSubscriptionCandidates' => Array(
					'email' => $email,
					'city_id'=>$city['WebCity']['id'],
					'user_id'=>$user_id,
					'subscription_ip'=>$ip,
					'is_suscribe'=>1
				)
			);
			if ($campaignInfo != null){
				$data['WebSubscriptionCandidates'] = array_merge(
						$data['WebSubscriptionCandidates'],
						$campaignInfo);
			}
			$this->dataWebSubscriptionCandidates = $data;
			$this->create();
			if($this->save($this->dataWebSubscriptionCandidates)){
				return true;
			}else{
				throw new Exception('Hubo un problema al guardar.');
			}
		}		
	}
}
