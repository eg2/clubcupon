<?php

class WebDeal extends WebAppModel {

    public $name = 'WebDeal';
    public $useTable = 'deals';

    public function findBySlug($slug) {
        return $this->find('first', array(
                    'conditions' => array(
                        'WebDeal.slug' => $slug,
                    ),
                    'recursive' => - 1,
        ));
    }

    public function isValidDeal($deal) {
        if (!$deal) {
            return false;
        }
        $is_subdeal = isset($deal['WebDeal']['id']) && isset($deal['WebDeal']['parent_deal_id']) && ($deal['WebDeal']['parent_deal_id'] != $deal['WebDeal']['id']);
        if ($is_subdeal) {
            return false;
        }
        return true;
    }

}
