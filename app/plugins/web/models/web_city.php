<?php

class WebCity extends WebAppModel {

    public $name = 'WebCity';
    public $useTable = 'cities';

    function findById($id) {
        $conditions = array(
            'id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    public function findBySlug($slug) {
        $conditions = array(
            'slug' => $slug
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    public function findAllApprovedAndHasNewsletter() {
        $conditions = array(
            'is_approved' => 1,
            'has_newsletter' => 1
        );
        $order = array(
            'name' => 'asc'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'recursive' => - 1
        ));
    }

    public function getActiveGroupsWithDeals() {
        return $this->_getActive(true, true, true);
    }

    public function getActiveGeoCities() {
        $conditions = array(
            'is_approved' => 1,
            'is_group' => 0,
            'is_corporate' => 0,
            'is_business_unit' => 0
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'order ASC'
        ));
    }

    public function getActiveCitiesWithDeals() {
        return $this->_getActive(true, false, true);
    }

    private function _getActive($withDeals = false, $groups = false, $selectable = false) {
        $conditions = array(
            'is_approved' => 1,
            'is_group' => $groups,
            'is_hidden' => false,
            'is_corporate' => false,
            'is_business_unit' => false
        );
        if ($withDeals) {
            $conditions['deal_count >='] = '1';
        }
        if ($selectable) {
            $conditions['is_selectable'] = 1;
        }
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'order ASC'
        ));
    }

}
