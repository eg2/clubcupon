<?php

class WebSearchLinksLogos extends WebAppModel {

    public $name = 'WebSearchLinksLogos';
    public $alias = 'WebSearchLinksLogos';
    public $useTable = 'search_link_logos';

    function findByName($name) {
        return $this->find('first', array(
                    'conditions' => array(
                        'name' => $name
                    ),
                    'recursive' => - 1
        ));
    }

    function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

}
