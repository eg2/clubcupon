<?php

class WebCorporateGuest extends WebAppModel {

    public $name = 'WebCorporateGuest';
    public $validate = array(
        'pin' => array(
            'required' => true
        )
    );
    public $useTable = 'corporate_guests';

    public function isValidCorporatePin($corporate_pin = null) {
        if (empty($corporate_pin)) {
            return false;
        }
        $count = $this->find('count', array(
            'conditions' => array(
                'pin' => $corporate_pin
            )
        ));
        return ($count >= 1);
    }

    public function findByPin($pin) {
        $corpGuest = $this->find('first', array(
            'conditions' => array(
                'pin' => $pin
            )
        ));
        return $corpGuest;
    }

}
