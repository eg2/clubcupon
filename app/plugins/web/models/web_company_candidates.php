<?php
//company_candidates
class WebCompanyCandidates extends WebAppModel {

	public $name = 'WebCompanyCandidates';
	public $useTable = 'company_candidates';

	function findByUserId($userId) {
		return $this->find('first', array(
				'conditions' => array(
						'WebCompanyCandidates.user_id' => $userId
				),
				'recursive' => -1
		));
	}
}