<?php

class WebSearchLinks extends WebAppModel {

    public $name = 'SearchLinks';
    public $alias = 'SearchLinks';
    public $useTable = 'search_links';
    public $recursive = - 1;

    public function deleteAllByCityIdAndTypeId($cityId = null, $type_id = 1) {
        if (empty($cityId)) {
            return null;
        }
        return $this->deleteAll(array(
                    'city_Id' => $cityId,
                    'search_link_type_id' => $type_id
        ));
    }

    public function findAllByCityIdAndTypeId($cityId = null, $type_id = 1, $withBlanks = false) {
        if (empty($cityId)) {
            return null;
        }
        $conditions = array(
            'city_Id' => $cityId,
            'search_link_type_id' => $type_id
        );
        if (!$withBlanks) {
            $conditions['label !='] = '';
        }
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => array(
                        'priority' => 'ASC'
                    )
        ));
    }

}
