<?php

class WebBranchesDeals extends WebAppModel {

    public $name = 'WebBranchesDeals';
    public $useTable = 'branches_deals';

    function findByDealId($dealId) {
        return $this->find('first', array(
            'conditions' => array(
                'WebBranchesDeals.now_deal_id' => $dealId
            ),
            'recursive' => -1
        ));
    }
}

