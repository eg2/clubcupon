<?php

class WebSearchLinksStyles extends WebAppModel {

    public $name = 'WebSearchLinksStyles';
    public $alias = 'WebSearchLinksStyles';
    public $useTable = 'search_link_styles';
    public $recursive = - 1;

    function findByName($name) {
        return $this->find('first', array(
                    'conditions' => array(
                        'name' => $name,
                    ),
                    'recursive' => - 1,
        ));
    }

    function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

}
