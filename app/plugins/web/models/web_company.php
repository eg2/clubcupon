<?php

class WebCompany extends WebAppModel {

    public $name = 'WebCompany';
    public $useTable = 'companies';

    function findByUserId($userId) {
        return $this->find('first', array(
                    'conditions' => array(
                        'WebCompany.user_id' => $userId
                    ),
                    'recursive' => -1
        ));
    }

    function findAll() {
        $order = array(
            'name' => 'asc'
        );
        return $this->find('all', array(
                    'order' => $order,
                    'recursive' => - 1
        ));
    }

}
