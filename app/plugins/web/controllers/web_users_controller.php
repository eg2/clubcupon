<?php

App::import('Component', 'api.ApiUserRegisterService');
App::import('Component', 'api.ApiSubscriptionsService');
App::import('Component', 'api.ApiUserLoginService');
App::import('Component', 'web.WebUserFacebookLogin');
App::import('Component', 'web.WebUserRegistration');
App::import('Component', 'web.WebUserRecoverPassword');
App::import('Component', 'web.WebUserLoginRemember');
App::import('Model', 'api.ApiUserType');
App::import('Model', 'User');
App::import('Model', 'web.WebCompany');
App::import('Model', 'web.WebCompanyCandidates');
App::import('Model', 'web.WebCity');

class WebUsersController extends WebAppController {

    public $name = 'WebUsersController';
    public $layout = 'clubcupon';
    private $apiLogin;
    private $apiRegister;
    private $apiSubscription;
    private $facebookLogin;
    private $userRegistration;
    private $userRecPassword;
    public $User;
    private $UserType = 2;
    private $webCompany;
    private $webCompanyCandidates;

    function __construct() {
        parent::__construct();
        $this->apiRegister = new ApiUserRegisterServiceComponent();
        $this->apiSubscription = new ApiSubscriptionsServiceComponent();
        $this->apiLogin = new ApiUserLoginServiceComponent();
        $this->facebookLogin = new WebUserFacebookLoginComponent();
        $this->userRegistration = new WebUserRegistrationComponent();
        $this->userRecPassword = new WebUserRecoverPasswordComponent();
        $this->userLoginRemember = new WebUserLoginRememberComponent();
        $this->webCompany = new WebCompany();
        $this->webCompanyCandidates = new WebCompanyCandidates();
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'register', 'forgotPassword', 'reset', 'registernow');
        $this->Security->validatePost = false;
    }

    public function logout() {
        if ($this->Auth->user()) {
            $this->Auth->logout();
            $this->Cookie->del('User');
            $this->Cookie->del('user_language');
        }
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
        $this->render('/clubcupon/logout');
    }

    public function login() {
        $this->viewUrlRedirect();
        if (!$this->Auth->User()) {
            $isFacebookLogin = $this->detectFacebookLogin();
            if (sizeof($this->params['form']) > 0 || $isFacebookLogin) {
                try {
                    $user = $this->Auth->User();
                    if (empty($user)) {
                        if (!$isFacebookLogin) {
                            $this->apiLogin->Auth = $this->Auth;
                            $user = $this->apiLogin->login(null, $this->params['form']['email'], $this->params['form']['password']);
                        } else {
                            $appId = Configure::read('facebook.fb_api_key');
                            $secret = Configure::read('facebook.fb_secrect_key');
                            $user = $this->facebookLogin->login($appId, $secret, $this->Auth, $this->RequestHandler, $this->Session, $this->Cookie);
                        }
                        $this->userLoginRemember->setCookieHash($user, $this->params['form'], $this->Cookie);
                    }
                    $this->fRedir();
                    $this->redirectOnLogued($user);
                } catch (Exception $e) {
                    $this->viewMessage = $e->getMessage();
                    $this->codeMessage = $e->getCode();
                }
            }
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::USER_LOGIN;
            $this->render('/clubcupon/login');
        } else {
            $this->fRedir();
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_HOME_CC;
            $this->render('/clubcupon/home');
        }
    }

    private function detectFacebookLogin() {
        $resp = false;
        if ($this->params['url']['isFacebookLogin']) {
            $resp = true;
        }
        if ($this->params['url']['required_facebook_login']) {
            $resp = true;
        }
        return $resp;
    }

    private function fRedir() {
        $forward = $this->params['url']['f'];
        if ($forward) {
            if (substr($forward, 0, 4) != 'http') {
                $forward = '/' . $forward;
            }
            $this->redirect($forward);
        }
    }

    private function viewUrlRedirect() {
        $this->viewParams['redirectUrl'] = $this->params['url']['f'];
    }

    private function redirectCompanyLogued($user) {
        if ($this->webCompany->findByUserId($user['User']['id']) == null) {
            $candidate = $this->webCompanyCandidates->findByUserId($user['User']['id']);
            if ($candidate == null) {
                $this->goRedirect('registerCompany');
            } else {
                if ($candidate['WebCompanyCandidates']['state'] == ConstCandidateStatuses::Reject) {
                    $this->Session->setFlash(Configure::read('web.plugin.messagePendingReject'));
                } else {
                    $this->Session->setFlash(Configure::read('web.plugin.messagePendingApproval'));
                }
                $this->goRedirect('home');
            }
        } else {
            $this->goRedirect('companyPanel');
        }
    }

    private function redirectOnLogued($user) {
        switch ($user['User']['user_type_id']) {
            case ApiUserType::Admin:
                $this->goRedirect('admin');
                break;

            case ApiUserType::SuperAdmin:
                $this->goRedirect('admin');
                break;

            case ApiUserType::Company:
                $this->redirectCompanyLogued($user);
                break;

            default:
                $this->goRedirect('home');
                break;
        }
    }

    private function urlParams() {
        $url = '';
        if (isset($this->params['urlParams'])) {
            $url = '?';
            foreach ($this->params['urlParams'] as $key => $value) {
                if ($url == '?') {
                    $url = $url . $key . "=" . $value;
                } else {
                    $url = '&' . $url . $key . "=" . $value;
                }
            }
        }
        return $url;
    }

    private function goRedirect($redirPage) {
        switch ($redirPage) {
            case 'admin':
                $url = '/' . $this->params['named']['city_slug'] . '/admin/deals';
                break;

            case 'companyPanel':
                $url = '/now/now_deals/my_cupons/city:' . $this->params['named']['city_slug'];
                break;

            case 'home':
                $url = '/' . $this->params['named']['city_slug'] . $this->urlParams();
                break;

            case 'registerCompany':
                $url = '/now/now_registers/register_company';
                break;
        }
        $this->redirect($url);
    }

    private function formDataCompanyUser() {
        $this->UserType = 3;
        $this->data['User']['username'] = substr(preg_replace(array(
                    "/@/",
                    "/[^0-9A-Za-z]/"
                                ), array(
                    "AT",
                    ""
                                ), $this->data['User']['email']), 0, 15) . substr(uniqid(), -5);
        $this->data['UserProfile']['dni'] = '99999999';
    }

    public function registernow() {
        $this->layout = 'now_web';
        try {
            $this->User = new User();
            if ($this->userRegistration->validateFormData($this->User, $this->data)) {
                $this->formDataCompanyUser();
                $this->registerUser();
                $this->goRedirect('registerCompany');
            }
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::FORM;
            $this->viewParams['formMessages'] = $this->User->validationErrors;
            $this->render('/now/nowregister');
        } catch (Exception $e) {
            $dum = $e;
        }
    }

    public function register() {
        try {
            $this->User = new User();
            if ($this->userRegistration->validateFormData($this->User, $this->data)) {
                $this->registerUser();
                $this->params['urlParams']['registerOk'] = true;
                $this->redirectOnLogued($dataResponse['User']);
            }
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::FORM_USER_REGISTER;
            $this->viewParams['formMessages'] = $this->User->validationErrors;
            $this->render('/clubcupon/register');
        } catch (Exception $e) {
            $dum = $e;
        }
    }

    private function registerUser() {
        $this->data['User']['password'] = $this->data['User']['passwd'];
        $this->apiRegister->Auth = $this->Auth;
        $this->apiRegister->RequestHandler = $this->RequestHandler;
        Configure::write('CityIdDefault', Configure::read('web.default.cityId'));
        $this->apiRegister->ApiSubscriptionsService = $this->apiSubscription;
        $suscript = $this->data['User']['is_agree_daily_subscriptions'];
        $city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
        $dataResponse = $this->apiRegister->registerAndActive($this->data, $suscript, $this->UserType, $city['WebCity']['id']);
        $this->Auth->login($dataResponse);
    }

    public function forgotPassword() {
        if (!empty($this->data)) {
            $this->User = new User();
            $this->User->set($this->data);
            unset($this->User->validate['email']['rule3']);
            if ($this->User->validates()) {
                $user = $this->User->findByEmail($this->data['User']['email']);
                if (!empty($user['User']['email'])) {
                    $this->userRecPassword->recover($user);
                    $this->Session->setFlash(__l('An email has been sent with a link where you can change your password'), 'default', null, 'success');
                    $url = '/' . $this->params['named']['city_slug'] . '/users/login';
                    $this->redirect($url);
                } else {
                    $this->viewParams['formMessages']['email'] = Configure::read('web.recoverPassword.noUserMail') . $this->data['User']['email'] . '.';
                }
            }
        }
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::FORM;
        $this->render('/clubcupon/forgotPassword');
    }

    function reset() {
        if (!$this->params['named']['city_slug']) {
            $this->params['named']['city_slug'] = Configure::read('web.default.citySlug');
        }
        $this->User = new User();
        if (!empty($this->data)) {
            if ($this->User->isValidResetPasswordHash($this->data['User']['user_id'], $this->data['User']['hash'])) {
                $this->User->set($this->data);
                if ($this->User->validates()) {
                    $this->User->updateAll(array(
                        'User.password' => '\'' . $this->Auth->password($this->data['User']['passwd']) . '\''
                            ), array(
                        'User.id' => $this->data['User']['user_id']
                    ));
                    $this->Session->setFlash(__l('Your password changed successfully, Please login now'), 'default', null, 'success');
                    $this->redirect(array(
                        'plugin' => '',
                        'controller' => 'users',
                        'action' => 'login'
                    ));
                }
                $this->Session->setFlash(__l('Could not update your password, please enter password.'), 'default', null, 'error');
                $this->data['User']['passwd'] = '';
                $this->data['User']['confirm_password'] = '';
            } else {
                $this->Session->setFlash(__l('Invalid change password request'));
                $this->redirect(array(
                    'plugin' => 'users',
                    'controller' => 'users',
                    'action' => 'login'
                ));
            }
        } else {
            if (is_null($this->params['named']['user_id']) || is_null($this->params['named']['hash'])) {
                $this->cakeError('error404');
            }
            $user = $this->User->findActiveUserById($this->params['named']['user_id']);
            if (empty($user)) {
                $this->Session->setFlash(__l('User cannot be found in server or admin deactivated your account, please register again'));
                $this->redirect(array(
                    'plugin' => '',
                    'controller' => 'users',
                    'action' => 'register'
                ));
            }
            if (!$this->User->isValidResetPasswordHash($this->params['named']['user_id'], $this->params['named']['hash'])) {
                $this->Session->setFlash(__l('Invalid change password request'));
                $this->redirect(array(
                    'plugin' => '',
                    'controller' => 'users',
                    'action' => 'login'
                ));
            }
            $this->data['User']['user_id'] = $this->params['named']['user_id'];
            $this->data['User']['hash'] = $this->params['named']['hash'];
        }
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::FORM;
        $this->render('/clubcupon/resetPassword');
    }

}
