<?php

class WebErrorsController extends WebAppController {

    public $name = 'WebErrorsController';
    public $layout = 'clubcupon';

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('error404');
    }

    public function error404() {
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
        $this->render('/clubcupon/error404');
    }

}
