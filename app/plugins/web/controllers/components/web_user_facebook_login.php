<?php 

App::import('Vendor','facebook-php-sdk-v5/autoload');
App::import('Model', 'User');
App::import('Model', 'Subscription');

class WebUserFacebookLoginComponent {
    
    private $User;
    private $Auth;

    const noConnectionFB = 4;

    function __construct() {
        $this->User = new User(); 
        $this->Subscription = new Subscription(); 
    }

    function fbConnect($appId, $appSecret) {
        session_start();         
        try {
            $this->facebook = new Facebook\Facebook([
              'app_id' => $appId,
              'app_secret' => $appSecret,
              'default_graph_version' => 'v2.9',
              'cookie' => true                
              ]);            

            $helper = $this->facebook->getJavaScriptHelper();

            $accessToken = $helper->getAccessToken();
            $this->facebook->setDefaultAccessToken($accessToken);
            
            $response = $this->facebook->get('/me?fields=id,email,first_name,middle_name,last_name,about');
            
            $fb_user = $response->getGraphUser();          
            
          //echo 'Facebook getGraphUser: ' . $fb_user;
          //exit;

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          //exit;
            $fb_user = null;          
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          //exit;
            $fb_user = null; 
        } catch (Exception $e) {
          echo 'Facebook SDK returned an error Exception: ' . $e->getMessage();            
          //exit;          
            $fb_user = null;
        }
        return $fb_user;
    }

    public function login ($appId, $appSecret, $auth, $requestHandler, &$session, $cookie) {

        $this->Auth = $auth;
        $fb_user = $this->fbConnect($appId, $appSecret);
        if ($fb_user) {
            $user = $this->getUser($fb_user);
            $this->Auth->fields['username'] = 'username';
            if (empty($user)) {
                $this->createAndLoginUser($user, $fb_user, $requestHandler, $session, $cookie);
            } else {
                $this->updateAndLoginUser($user);
            }
        } else {
            throw new Exception("No es posible conectar con los servidores de Facebook.", self::noConnectionFB);
        }

        return $user;
    }

    private function updateAndLoginUser($user) {

        if(empty($user["User"]["fb_user_id"])) {
            $user["User"]["fb_user_id"] = $fb_user["id"];
            $this->User->save($user, false);
        }

        $this->data['User']['email'] = $user['User']['email'];
        $this->data['User']['username'] = $user['User']['username'];
        $this->data['User']['password'] = $user['User']['password'];

        $this->Auth->login($this->data);
    }

    private function createDataUser($user, $fb_user, $requestHandler) {
        
        $this->User->create();
        $this->data['UserProfile']['first_name'] = $fb_user['first_name'];
        $this->data['UserProfile']['middle_name'] = isset($fb_user['middle_name']) ? $fb_user['middle_name'] : "";
        $this->data['UserProfile']['last_name'] = isset($fb_user['last_name']) ? $fb_user['last_name'] : "";
        $this->data['UserProfile']['about_me'] = isset($fb_user['about']) ? $fb_user['about'] : "";
        $this->data['User']['email'] = $fb_user['email'];

        if (strlen($fb_user['first_name']) > 2) {
            $this->data['User']['username'] = $this->User->checkUsernameAvailable(strtolower($fb_user['first_name']));
        }
        if (empty($this->data['User']['username'])) {
            $this->data['User']['username'] = $this->User->checkUsernameAvailable(strtolower($fb_user['first_name'] . $fb_user['last_name']));
        }
        $this->data['User']['username'] = str_replace(' ', '', $this->data['User']['username']);
        $this->data['User']['username'] = str_replace('.', '_', $this->data['User']['username']);
        if (strlen($this->data['User']['username']) <= 2) {
            $this->data['User']['username'] = !empty($fb_user['first_name']) ? str_replace(' ', '', strtolower($fb_user['first_name'])) : 'fbuser';
            $i = 1;
            $created_user_name = $this->data['User']['username'] . $i;
            while (!$this->User->checkUsernameAvailable($created_user_name)) {
                $created_user_name = $this->data['User']['username'] . $i++;
            }
            $this->data['User']['username'] = $created_user_name;
        }
        $this->data['User']['fb_user_id'] = $fb_user['id'];
        $this->data['User']['password'] = $this->Auth->password($fb_user['id'] . Configure::read('Security.salt'));
        
    }

    private function creatAditionalDataUser(&$session, $requestHandler, $cookie) {

        $this->data['User']['is_agree_terms_conditions'] = '1';
        $this->data['User']['is_email_confirmed'] = 1;
        $this->data['User']['is_active'] = 1;
        $this->data['User']['user_type_id'] = ConstUserTypes::User;
        $this->data['User']['signup_ip'] = $requestHandler->getClientIP();
        
        if ($session->read('gift_user_id')) {
            $this->data['User']['gift_user_id'] = $session->read('gift_user_id');
            $session->del('gift_user_id');
        }
        if (Configure::read('user.is_referral_system_enabled')) {
            $cookie_value = $cookie->read('referrer');
            if (!empty($cookie_value)) {
                $this->data['User']['referred_by_user_id'] = $cookie_value;
                $this->data['User']['referred_date'] = date ('Y-m-d H:i:s');
            }
        }

    }

    private function creatSuscriptionDataUser() {

        $subscriptionTotalAmount = $this->Subscription->findTotalAvailableBalanceAmountByEmail($this->data['User']['email']);
        if(!is_null($subscriptionTotalAmount[0]['total_amount'])) {
            $this->data ['User']['available_balance_amount'] = $subscriptionTotalAmount[0]['total_amount'];
        }

    }

    private function saveDataUser(&$session) {

        $this->User->save($this->data, false);
        $session->write('is_facebook_session', true);
        $this->data['UserProfile']['user_id'] = $this->User->id;
        $this->User->UserProfile->save($this->data);

    }

    private function getFriendsData() {
/*
        try{
            //$my_access_token = $this->facebook->getAccessToken();
            //$friends = $this->facebook->api('/me/friends?fields=id,email,first_name&limit=1000',array('access_token'=>$my_access_token));            
            $requestFriends = $this->facebook->get('/me/taggable_friends?fields=id,email,name&limit=1000');
            $friends = $requestFriends->getGraphEdge();            
            echo 'Friends:' . $friends;
            
            // if have more friends than 100 as we defined the limit above on line no. 68
            if ($this->facebook->next($friends)) {
                    $allFriends = array();
                    $friendsArray = $friends->asArray();
                    $allFriends = array_merge($friendsArray, $allFriends);
                    while ($friends = $fb->next($friends)) {
                            $friendsArray = $friends->asArray();
                            $allFriends = array_merge($friendsArray, $allFriends);
                    }
                    foreach ($allFriends as $key) {
                            echo $key['name'] . "<br>";
                    }
                    echo count($allFriends);
            } else {
                    $allFriends = $friends->asArray();
                    $totalFriends = count($allFriends);
                    foreach ($allFriends as $key) {
                            echo $key['name'] . "<br>";
                    }
            }
            exit;
            
            
            $aux = array();
            $fb_friends = array();
            $fbFriends = array();
            $count = Configure::read('web.FbFriends.count');
            if(count($friends['data'])<$count){
            		foreach ($friends['data'] as $friend){
                		$fb_friends['FbFriend']['fb_user_id']    = $friend['id'];
                		$fb_friends['FbFriend']['friend_user_id']= $this->User->id;
                		$fb_friends['FbFriend']['username']      = $friend['first_name'];
                		$fb_friends['FbFriend']['email']         = $friend['email'];
                		$fbFriends[]=$fb_friends;
		    		}
		    		$this->User->FbFriend->create();
		    		$this->User->FbFriend->saveAllFriends($fbFriends);
            }else{
            		$aux = array_chunk($friends['data'], $count);
		    		foreach ($aux as $Groupfriends) {
		    			$fbFriends = array();
		    			foreach ($Groupfriends as $friend){
                			$fb_friends['FbFriend']['fb_user_id']    = $friend['id'];
                			$fb_friends['FbFriend']['friend_user_id']= $this->User->id;
                			$fb_friends['FbFriend']['username']      = $friend['first_name'];
                			$fb_friends['FbFriend']['email']         = $friend['email'];
                			$fbFriends[]=$fb_friends;
		    			}
		    			$this->User->FbFriend->create();
		    			$this->User->FbFriend->saveAllFriends($fbFriends);
		    			$fbFriends = null;
        			}
            }
        } catch (FacebookApiException $e) {
            // Log
        }
        */
    }
	
    private function createAndLoginUser($user, $fb_user, $requestHandler, &$session, $cookie) {
        
        $this->createDataUser($user, $fb_user, $requestHandler);
        $this->creatAditionalDataUser($session, $requestHandler, $cookie);
        $this->creatSuscriptionDataUser();
        $this->saveDataUser($session);
        $this->getFriendsData();
        
        $this->Auth->login($this->data);
    }

    public function getUser($fb_user) {
        $user = $this->User->find(
            'first', 
            array(
                'conditions' => array(
                    'OR' => array(
                        'User.fb_user_id' => $fb_user["id"],
                        'User.email' => $fb_user["email"],
                    )
                ),
                'fields' => array(
                    'User.id',
                    'User.fb_user_id',
                    'User.password',
                    'User.username',
                    'User.email'
                )
            )
        );
        return $user;
    }
}
?>