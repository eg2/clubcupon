<?php 

App::import('Model', 'web.WebUser');

class WebUserLoginRememberComponent {

    private $WebUser;
    
    function __construct() {
        $this->WebUser = new WebUser();
    }

    public function setCookieHash($user, $form, &$gCookie) {
        if (isset($form['sessionClose'])) {
            $gCookie->del('User');
            $cookie = array();
            $remember_hash = md5($user['User'][Configure::read('user.using_to_login')] . $user['User']['password'] . Configure::read('Security.salt'));
            $cookie['cookie_hash'] = $remember_hash;
            $gCookie->write('User', $cookie, true, Configure::read('web.rememberLogin.cookieTime'));
            $this->WebUser->updateAll(
                array(
                    'WebUser.cookie_hash' => '\'' . md5($remember_hash) . '\'',
                    'WebUser.cookie_time_modified' => '\'' . date('Y-m-d H:i:s') . '\'',
                ), 
                array(
                    'WebUser.id' => $user['User']['id']
                )
            );
        }
    }

    public function existCookieHash($gCookie) {
        return $gCookie->read('User');
    }

    public function autoLogin(&$Auth, $gCookie) {

        $user = $Auth->User();
        if (empty($user)) {

            $userCookie = $gCookie->read('User');
            if ($userCookie && $userCookie['cookie_hash']) {

                $cryptedCookieHash =  md5($userCookie['cookie_hash']);
                $user = $this->WebUser->findByCookieHash($cryptedCookieHash);
                if (!empty($user)) {

                    $data['User']['email'] = $user['WebUser']['email'];
                    $data['User']['username'] = $user['WebUser']['username'];
                    $data['User']['password'] = $user['WebUser']['password'];
    
                    $Auth->login($data);
                }
            }
        }
    }
}

?>