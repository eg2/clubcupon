<?php

App::import('Model', 'web.WebUser');
App::import('Model', 'web.WebCompany');

class WebUserMenuComponent {

    public $name = 'WebUserMenu';

    function __construct($userId) {
        $this->WebUser = new WebUser();
        $this->WebCompany = new WebCompany();
    }

    public function returnMenuOptionsForUser($userId) {
        $user = $this->WebUser->findByUserId($userId);
        $company = $this->WebCompany->findByUserId($userId);
        $user = $user[0];
        $companyIsNowDealsLink = array(
            'type' => 'link',
            'label' => 'Mis Ofertas',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_deals',
            'admin' => false
        );
        $companyIsNowBranchesLink = array(
            'type' => 'link',
            'label' => 'Mis Sucursales',
            'plugin' => 'now',
            'controller' => 'now_branches',
            'action' => 'index',
            'admin' => false
        );
        $companyIsNowCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_cupons',
            'admin' => false
        );
        $companyIsNowProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Empresa',
            'plugin' => 'now',
            'controller' => 'now_registers',
            'action' => 'my_company',
            'admin' => false
        );
        $companyIsRegularProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Empresa',
            'plugin' => '',
            'controller' => 'users',
            'action' => 'change_password',
            'admin' => false
        );
        $companyIsRegularDealsLink = array(
            'type' => 'link',
            'label' => 'Mis Ofertas',
            'plugin' => '',
            'controller' => 'deals',
            'action' => 'company',
            'admin' => false,
            'parameters' => $company['WebCompany']['slug']
        );
        $companyIsRegularCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_cupons',
            'admin' => false
        );
        $companyCampaign = array(
            'type' => 'link',
            'label' => 'CampaÃ±a',
            'plugin' => '',
            'controller' => 'dashboards',
            'action' => 'company_campaign',
            'admin' => false
        );
        $companyLiquidations = array(
            'type' => 'link',
            'label' => 'Liquidaciones',
            'plugin' => 'now',
            'controller' => 'now_liquidations',
            'action' => 'liquidations',
            'admin' => false
        );
        $userSubscriptionsLink = array(
            'type' => 'link',
            'label' => 'Suscripciones',
            'plugin' => '',
            'controller' => 'subscriptions',
            'action' => 'manage_subscriptions',
            'admin' => false
        );
        $userProfileLink = array(
            'type' => 'link',
            'label' => 'Mi Perfil',
            'plugin' => '',
            'controller' => 'user_profiles',
            'action' => 'edit',
            'admin' => false,
            'parameters' => $user['WebUser']['id']
        );
        $userChangePasswordLink = array(
            'type' => 'link',
            'label' => 'Cambio de contraseÃ±a',
            'plugin' => '',
            'controller' => 'users',
            'action' => 'change_password',
            'admin' => false
        );
        $userCuponsLink = array(
            'type' => 'link',
            'label' => 'Mis Cupones',
            'plugin' => '',
            'controller' => 'deal_users',
            'action' => 'index',
            'admin' => false,
            'parameters' => 'type:available'
        );
        $userAdminLink = array(
            'type' => 'link',
            'label' => 'Admin',
            'plugin' => '',
            'action' => 'admin/deals',
            'admin' => true
        );
        $userBalanceString = array(
            'type' => 'string',
            'string' => 'Ten&eacute;s $' . $user['WebUser']['available_balance_amount'] . ' en tu cuenta.',
            'value' => $user['WebUser']['available_balance_amount']
        );
        $userHelpLink = array(
            'type' => 'externalLink',
            'label' => 'Ayuda',
            'url' => 'http://soporte.clubcupon.com.ar/',
            'target' => '_blank',
            'format' => array(
                'title' => 'Centro de atenci&oacute;n al cliente'
            )
        );
        $companyHelpLink = array(
            'type' => 'externalLink',
            'label' => 'Ayuda',
            'url' => Configure::read('ayuda.comercios'),
            'target' => '_blank',
            'format' => array(
                'title' => 'Centro de atenci&oacute;n al comercio'
            )
        );
        $userIsCompanyLinks = array(
            $companyIsNowProfileLink,
            $companyIsNowDealsLink,
            $companyIsNowCuponsLink,
            $companyLiquidations,
            $companyHelpLink
        );
        $isUserCompany = ($user['WebUser']['user_type_id'] == ConstUserTypes::Company);
        if ($isUserCompany) {
            $userCompany = $this->WebCompany->findByUserId($user['WebUser']['id']);
            if ($userCompany) {
                $isCompanyNow = $userCompany['WebCompany']['now_eula_ok'];
                if ($isCompanyNow) {
                    $userIsCompanyLinks[] = $companyIsNowBranchesLink;
                }
            }
        }
        $userIsRegularLinks = array(
            $userBalanceString,
            $userProfileLink,
            $userCuponsLink,
            $userSubscriptionsLink,
            $userHelpLink
        );
        $userIsAdminLinks = array(
            $userBalanceString,
            $userProfileLink,
            $userCuponsLink,
            $userSubscriptionsLink,
            $userAdminLink,
            $userHelpLink
        );
        $userIsAgencyLinks = array(
            $userSubscriptionsLink,
            $userAdminLink,
            $companyHelpLink
        );
        $userIsSellerLinks = array(
            $userSubscriptionsLink,
            $companyHelpLink
        );
        if ($isUserCompany && $userCompany) {
            return $userIsCompanyLinks;
        }
        if ($this->WebUser->isRegularUser($user)) {
            return $userIsRegularLinks;
        }
        if ($this->WebUser->isAgencyUser($user) || $this->WebUser->isPartnerUser($user)) {
            return $userIsAgencyLinks;
        }
        if ($this->WebUser->isSellerUser($user)) {
            return $userIsSellerLinks;
        }
        if ($this->WebUser->isAdminUser($user) || $this->WebUser->isSuperAdminUser($user)) {
            return $userIsAdminLinks;
        }
        return null;
    }

}
