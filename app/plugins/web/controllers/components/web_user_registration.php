<?php 

// App::import('Model', 'User');

class WebUserRegistrationComponent {

    public function register() {
        
    }

    public function validateFormData(&$User, $data) {

        $response = false;

        if (!empty($data)) {

            $User->set($data);
            $User->UserProfile->set($data);
            $User->Company->set($data);
            
            if ($User->validates() & 
                $User->UserProfile->validates() & 
                $User->Company->validates() & 
                $User->Company->City->validates() & 
                $User->Company->State->validates()) {
                    $response = true;
            }
        }

        return $response;
    }

}

?>