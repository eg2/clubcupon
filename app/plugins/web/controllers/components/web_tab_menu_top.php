<?php

class WebTabMenuTopComponent {
	
	protected $search = array("/", "?", ".", '*');
	protected $remplace = array("\/", "\?", "\.", '.*');
	
	private function replaceEspecialChars($value){
		return str_replace($this->search, $this->remplace,$value);
	}
	
	private function compareStringWithPattern($string,$pattern){
		return preg_match("/".$this->replaceEspecialChars($pattern)."/",$string,$con,PREG_OFFSET_CAPTURE);
	}
	public function getSelectedTab($url){
		$url='/'.$url;
		$priori = Configure::read('web.plugin.tabsMenuMinPriority');
		$valor = 0;
		$menuTabs = Configure::read('web.plugin.tabsMenuTop');
		foreach ($menuTabs as $key => $value){
			if($this->compareStringWithPattern($url, $value['pattern'])){
				if($value['priority']<$priori){
					$priori = $value['priority'];
					$valor = $key;
				}
			}
		}
		return $valor;
	}
}