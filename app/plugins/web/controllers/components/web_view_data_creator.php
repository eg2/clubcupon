<?php

App::import('Model', 'web.WebCity');

App::import('Component', 'web.WebUserMenu');

App::import('Component', 'web.view_data_creator/view_objects/WebDataObject');
App::import('Component', 'web.view_data_creator/view_objects/WebMessageObject');
App::import('Component', 'web.view_data_creator/view_objects/WebUserObject');
App::import('Component', 'web.view_data_creator/view_objects/WebCityObject');
App::import('Component', 'web.view_data_creator/view_objects/WebTrackingObject');

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');

App::import('Component', 'web.view_data_creator/view_structures/WebCityHomeStructure');
App::import('Component', 'web.view_data_creator/view_structures/WebCityAllStructure');
App::import('Component', 'web.view_data_creator/view_structures/WebDealDetailStructure');

App::import('Component', 'web.view_data_creator/view_structures/WebCityHomeClubcuponStructure');
App::import('Component', 'web.view_data_creator/view_structures/WebDealDetailClubcuponStructure');
App::import('Component', 'web.view_data_creator/view_structures/WebDealNowClubcuponStructure');
App::import('Component', 'web.view_data_creator/view_structures/WebAdsAllStructure');


App::import('Component', 'web.view_data_creator/view_structures/WebSolrResultsStructure');

App::import('Component', 'web.view_data_creator/view_structures/WebFormsStructure');


class WebViewDataCreatorComponent{
    
    // ---------------------------------- Exclusive Views ----------------------------------------
    const BASE_VIEW    = 'Base_View';    // ---
    const CITY_HOME    = 'City_Home';    // 2 Ofertas + Lista de Ofertas (para city)
    const CITY_ALL     = 'City_All';     // Lista de Ofertas (para city)
    const DEAL_DETAIL  = 'Deal_Detail';  // 1 Oferta + Lista de Ofertas (para city)
    const PAGE_WHO     = 'Page_Who';     // Quienes Somos
    const PAGE_TERMS   = 'Page_Terms';     // Términos y condiciones
    const PAGE_POLICY   = 'Page_Policy';     // Términos y condiciones
    const PAGE_PROTECTION   = 'Page_Protection';     // Términos y condiciones
    const USER_LOGIN   = 'User_Login';     // Términos y condiciones
      
    // ---------------------------------- Clubcupon Views ----------------------------------------
    const CITY_HOME_CC    = 'City_Home_CC';    // 1 Oferta + Lista Categorías + 2 Lista de Ofertas (para city)
    const DEAL_DETAIL_CC  = 'Deal_Detail_CC';  // 1 Oferta + Lista de Ofertas + Lista más buscadas
    const CITY_ALL_CC     = 'City_All_CC';     // Lista de Ofertas (para city)
    const CITY_NOW_CC     = 'City_Now_CC';    //
    const ADS_ALL_CC      = 'Ads_All_CC';    //
    const CITY_NOW_LANDING     = 'City_Now_Landing';
    const CITY_NOW_FAQ     = 'City_Now_Faq';
    
    // ---------------------------------- Solr Results -------------------------------------------
    const SOLR_RESULTS    = 'Solr_Results';    // Lista de ofertas para mostrar resultados de Solr

    // ---------------------------------- Formularios -------------------------------------------
    const FORM            = 'Form';        // Formulario
    const FORM_USER_REGISTER   = 'Form_Register';        // Formulario
    
    function __construct() {
        $this->WebCity = new WebCity();
    }
    
    /** Método que crea un objeto, normalizado, con datos que serán enviados a la vista (WebDataObject)
     *  
     *  Recibe un array de parámetros con los siguientes valores:
     *  $params['viewType'] ------> Constante de esta clase que define la estructura y contenido de los datos a ser 
     *                              enviados a la vista (Obligatorio)
     *  $params['citySlug'] ------> Slug de la ciudad/grupo/Empresa/UDN actual (Optativo)
     *  $params['lastGeoCitySlug']  Slug de la última ciudad geográfica visitada (Optativo)
     *  $params['dealSlug'] ------> Slug de la oferta actual (Optativo)
     *  $params['userPIN'] -------> PIN de logueo del usuario actual (Optativo)
     *  $params['userType'] ------> ID de tipo de usuario actual si está logueado (Optativo)
     *  $params['userEmail'] -----> Email de usuario actual si está logueado (Optativo)
     *  $params['message'] -------> Mensaje a ser mostrado al usuario por la vista (Optativo)
     *  $params['codeMessage'] ---> Código de Mensaje a ser mostrado al usuario por la vista (Optativo)
     *  $params['formMessages'] --> Mensajes a ser mostrados por formularios (Optativo)
     *  $params['EnablePopup']----> Habilita POPUP de subscripcion (first user)
     *  $params['redirectUrl']----> URL de redireccion para parametro "f" por get
     *  $params['url'] -----------> URL de la pagina solicitada
     *  $params['id-ad'] ---------> id ad e-planing
     *  $params['width-ad'] ------> width image ad e-planing
     *  $params['height-ad'] -----> height ad e-planing
     *  $params['isSetHFacetField'] Si en los resultados del buscador se filtra por categoría (Optativo)
     *
     *  El objecto devuelto (WebDataObject) tiene los siguientes atributos:
     *  user ---------> Objeto con datos del usuario
     *  city ---------> Objeto con datos de la ciudad actual
     *  message ------> Objeto con mensaje a ser presentado por la vista
     *  vData --------> Objeto con datos específicos para la vista
     *  
     *  Los objetos y subobjetos que pueden conformar WebDataObject se encuentran en 
     *  /plugins/web/controllers/components/view_data_creator/view_objects
     */
    private function getDataTrackingEvent($params){
    	$subscriptionOk = false;
    	$registerOk = false;
    	$buyOk = false;
    	if($params['subscriptionOk'] === true){
    		$subscriptionOk = true;
    	}
    	if($params['registerOk'] === true){
    		$registerOk = true;
    	}
    	if($params['buyOk'] === true){
    		$buyOk = true;
    	}
    	return new WebTrackingObjectComponent($subscriptionOk, $registerOk, $buyOk);
    }
    public function createData($params) {
        
        if (!$params['citySlug'] && !$params['lastGeoCitySlug']) {
            $params['citySlug'] = Configure::read('web.default.citySlug');
        }

        $cityData = $this->getCityObject($params['citySlug'], $params['lastGeoCitySlug']);

        $menuElements = null;
        if ($params['userID']) {
            $wUserMenu = new WebUserMenuComponent();
            $menuElements = $wUserMenu->returnMenuOptionsForUser($params['userID']);
        }
        
        $userData = new WebUserObjectComponent(
            $params['userPIN'], 
            $params['userType'],
            $params['userEmail'],
            $menuElements
        );

        $trackingEvent = $this->getDataTrackingEvent($params);
        
        $messageData = new WebMessageObjectComponent($params['message'], $params['codeMessage']);
        $params['cityData'] = $cityData;

        $viewData = $this->getViewDataObject($params);
        
        if (empty($viewData)) {
            $response = null;
        } else {        
            $response = new WebDataObjectComponent($userData, $cityData, $messageData, $viewData, $trackingEvent);
        }
        
        return $response;
    }

    private function getViewDataObject($params) {
        $userType = $params['userType'];
        switch ($params['viewType']) {
            // Exclusive
        	case self::USER_LOGIN:
            case self::BASE_VIEW: $viewDataCreator = new WebBaseStructureComponent($userType); 
                break;
            case self::CITY_HOME: $viewDataCreator = new WebCityHomeStructureComponent($userType); 
                break;
            case self::CITY_ALL: $viewDataCreator = new WebCityAllStructureComponent($userType); 
                break;
            case self::DEAL_DETAIL: $viewDataCreator = new WebDealDetailStructureComponent($userType); 
                break;
            // ClubCupon
            case self::CITY_HOME_CC: $viewDataCreator = new WebCityHomeClubcuponStructureComponent($userType); 
                break;
            case self::DEAL_DETAIL_CC: $viewDataCreator = new WebDealDetailClubcuponStructureComponent($userType); 
                break;
            case self::CITY_ALL_CC: $viewDataCreator = new WebCityAllStructureComponent($userType); 
                break;
            case self::SOLR_RESULTS: $viewDataCreator = new WebSolrResultsStructureComponent($userType); 
                break;
            case self::FORM_USER_REGISTER:
            case self::FORM: $viewDataCreator = new WebFormsStructureComponent($userType); 
                break;
            case self::CITY_NOW_LANDING:
            case self::CITY_NOW_FAQ:
            case self::CITY_NOW_CC: $viewDataCreator = new WebDealNowClubcuponStructureComponent($userType);
            	break;
            case self::ADS_ALL_CC: $viewDataCreator = new WebAdsAllStructureComponent($userType);
            	break;
            case self::PAGE_WHO: $viewDataCreator = new WebBaseStructureComponent($userType);
            	break;
            case self::PAGE_TERMS: $viewDataCreator = new WebBaseStructureComponent($userType);
            	break;
            case self::PAGE_POLICY: $viewDataCreator = new WebBaseStructureComponent($userType);
            	break;
            case self::PAGE_PROTECTION: $viewDataCreator = new WebBaseStructureComponent($userType);
            	break;
        }

        $vData =  $viewDataCreator->create($params);
        $vData->viewType =  $params['viewType'];
        return $viewDataCreator->postCreate($vData, $params);

    }

    private function getCityObject($citySlug, $lastGeoCitySlug) {

        do {
            $city = $this->WebCity->findBySlug($citySlug);
            if (!$city) { $citySlug = $lastGeoCitySlug; }
        } while (!$city);
        
        $lastCity = $this->WebCity->findBySlug($lastGeoCitySlug);
        $lastGeoCityName = $lastCity['WebCity']['name'];

        $cityData = new WebCityObjectComponent(
            $city['WebCity']['id'],
            $city['WebCity']['name'],
            $city['WebCity']['slug'],
            $city['WebCity']['is_corporate'],
            $city['WebCity']['is_business_unit'],
            $city['WebCity']['is_group'],
            $city['WebCity']['image'],
            $lastGeoCityName
        );
        return $cityData;
    }
}

?>
