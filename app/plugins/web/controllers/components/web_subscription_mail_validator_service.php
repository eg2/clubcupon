<?php
App::import('Component', 'api.ApiLogger');
App::import('Model','web.WebFakePatterns');

class WebSubscriptionMailValidatorServiceComponent {
	
	protected $ApiLogger;
	protected $WebFakePatterns;
	
	protected $search = array('%');
	protected $remplace = array('.*');
	
	function __construct() {
		$this->ApiLogger = new ApiLogger(get_class($this), 'subscription');
		$this->SubscriptionEmailErrorLogger = new ApiLogger(get_class($this), 'SubscriptionEmailError');
		$this->WebFakePatterns = new WebFakePatterns();
	}
	
	private function replaceEspecialChars($value){
		return str_replace($this->search, $this->remplace,$value);
	}
	
	private function compareStringWithPattern($email,$pattern){
		return preg_match("/^".$this->replaceEspecialChars($pattern)."$/",$email,$con,PREG_OFFSET_CAPTURE);
	}
	
	

	function verifyMailDomain($email){
		if($this->validateMailWithBlackListPatterns($email)){
			$domain = substr($email, strpos($email, '@') + 1);
#		$this->SubscriptionEmailErrorLogger->notice('Comprobando dominio: '.$domain.' para subscripcion');
#		$this->ApiLogger->notice('Comprobando dominio: '.$domain.' para subscripcion');
			$status = checkdnsrr( $domain, 'MX');
			if($status){
				$this->ApiLogger->notice('El dominio: '.$domain.' es valido para subscripcion');
				return true;
			}else{
				$this->ApiLogger->error('El dominio: '.$domain.' no es valido para subscripcion');
				return false;
			}	
		}
	}
	
	function  validateMailWithBlackListPatterns($email){
#	$this->ApiLogger->notice('Validando mail: '.$email.' mediante Black List Patterns');
		$fakePatterns = $this->WebFakePatterns->findAll();
		$valido = true;
		foreach ($fakePatterns as $pattern){
			if($this->compareStringWithPattern($email, $pattern['WebFakePatterns']['pattern'])){
				$this->ApiLogger->error('El mail: '.$email.' no es valido. Black List Pattern match: '.$pattern['WebFakePatterns']['pattern']);
				$valido = false;
			}
		}
		if($valido){
			$this->ApiLogger->notice('El mail: '.$email.' es valido');
		}
		return $valido;
	}
	
}

?>
