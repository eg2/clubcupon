<?php 

App::import('Component', 'Email');
App::import('Model', 'EmailTemplate');
App::import('Model', 'User');

class WebUserRecoverPasswordComponent {

    function __construct() {
        $this->EmailTemplate = new EmailTemplate();
        $this->Email = new EmailComponent();
        $this->User = new User();
    }

    public function recover($user) {

        $contact_us_url = Configure::read('static_domain_for_mails') . Router::url(array(
            'controller' => 'contacts',
            'action' => 'add',
            'admin' => false
        ), false) . '?from=mailing';

        $emailFindReplace = array(
            '##SITE_LINK##' => Router::url('/', true) . '?from=mailing',
            '##USERNAME##' => (isset($user['User']['username'])) ? $user['User']['username'] : '',
            '##SITE_NAME##' => Configure::read('site.name'),
            '##SUPPORT_EMAIL##' => Configure::read('site.contact_email'),
            '##CONTACT_US##' => $contact_us_url,
            '##RESET_URL##' => Router::url(array(
                'plugin' => 'web',
                'controller' => 'users',
                'action' => 'reset',
                $user['User']['id'],
                $this->User->getResetPasswordHash($user['User']['id'])
            ), true) . '?from=mailing',
            '##ABSOLUTE_IMG_PATH##' => Configure::read('static_domain_for_mails') . '/img/email/',
        );

        $email = $this->EmailTemplate->selectTemplate('Forgot Password');
        $this->Email->from = ($email['from'] == '##FROM_EMAIL##') ? Configure::read('EmailTemplate.from_email') : $email['from'];
        $this->Email->replyTo = ($email['reply_to'] == '##REPLY_TO_EMAIL##') ? Configure::read('EmailTemplate.reply_to_email') : $email['reply_to'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = strtr($email['subject'], $emailFindReplace);
        $this->Email->sendAs = ($email['is_html']) ? 'html' : 'text';
        $this->Email->send(strtr($email['email_content'], $emailFindReplace));
    }
}

?>