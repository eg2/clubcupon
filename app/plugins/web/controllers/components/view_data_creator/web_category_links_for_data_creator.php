<?php

App::import('Component', 'web.view_data_creator/view_objects/WebLinkObject');
App::import('Model', 'web.WebSearchLinks');
App::import('Model', 'web.WebSearchLinksLogos');
App::import('Model', 'web.WebSearchLinksStyles');

class WebCategoryLinksForDataCreatorComponent {

    public $WebSearchLinks;
    public $WebSearchLinksLogos;
    public $WebSearchLinksStyles;

    function __construct() {
        $this->WebSearchLinks = new WebSearchLinks();
        $this->WebSearchLinksLogos = new WebSearchLinksLogos();
        $this->WebSearchLinksStyles = new WebSearchLinksStyles();
    }

    public function getCategoryLinks($qt, $cityID, $linkType) {
        $links = $this->WebSearchLinks->findAllByCityIdAndTypeId($cityID, $linkType);
        
        if ($links) {
            $linksSize = count($links);
        } else {
            $defaultLinks = Configure::read('web.defaultSlotCategories');
            $linksSize = count($defaultLinks[$linkType]);
        }
        $catLinkList = array();
        $i = 0;
        while ($i < $linksSize && $i < $qt) {
            if ($links) {
                $logoRight = $this->WebSearchLinksLogos->findById($links[$i]['SearchLinks']['search_link_logo_right_id']);
                $logoLeft = $this->WebSearchLinksLogos->findById($links[$i]['SearchLinks']['search_link_logo_left_id']);
                $style = $this->WebSearchLinksStyles->findById($links[$i]['SearchLinks']['search_link_style_id']);
                $catLinkList[] = new WebLinkObjectComponent($links[$i]['SearchLinks']['url'], $links[$i]['SearchLinks']['label'], $logoRight['WebSearchLinksLogos']['file_name'], $logoLeft['WebSearchLinksLogos']['file_name'], $style['WebSearchLinksStyles']['css_class'],$links[$i]['SearchLinks']['description']);
            } else {
                $catLinkList[] = new WebLinkObjectComponent($defaultLinks[$linkType][$i]['path'], $defaultLinks[$linkType][$i]['label'], $defaultLinks[$linkType][$i]['logo'], $defaultLinks[$linkType][$i]['styleClass'],$links[$i]['SearchLinks']['description']);
            }
            $i++;
        }
        
        return $catLinkList;
    }

}
