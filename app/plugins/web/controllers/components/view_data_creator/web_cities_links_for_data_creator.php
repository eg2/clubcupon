<?php

App::import('Component', 'web.view_data_creator/view_objects/WebLinkObject');
App::import('Component', 'web.view_data_creator/view_objects/WebCitiesListsObject');
App::import('Model', 'web.WebCity');

class WebCitiesLinksForDataCreatorComponent {

    function __construct() {
        $this->WebCity = new WebCity();
    }

    public function getCitiesLinks() {

        $activeCitiesWithDeals = $this->WebCity->getActiveCitiesWithDeals();

        $citiesLinks = $this->getLinksArrayFromCities($activeCitiesWithDeals);
        $extraLinks = $this->getExtraLinksArray();

        return new WebCitiesListsObjectComponent($citiesLinks, $extraLinks);
    }

    private function getExtraLinksArray(){
    	$extraLinks = array();
    	$extra = Configure::read('web.plugin.extraLinks');
    	foreach ($extra as $elink){
    		$extraLinks[] = new WebLinkObjectComponent(
    				$elink['url'],
    				$elink['label']
    		);
    	}
    	return $extraLinks;
    }
    private function getLinksArrayFromCities($cities) {
        $resp = array();
        foreach ($cities as $city) {
            $url = '/'.$city['WebCity']['slug'];
            $resp[] = new WebLinkObjectComponent(
                $url,
                $city['WebCity']['name']
            );
        }
        return $resp;
    }
}
