<?php

class WebDealListObjectComponent {
    
    public $title;
    public $basePathUrl;
    public $dealList;
    
    function __construct($title, $basePathUrl, $dealList) {
        $this->title = $title;
        $this->basePathUrl = $basePathUrl;
        $this->dealList = $dealList;
    }
}
?>