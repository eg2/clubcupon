<?php

class WebCompanyObjectComponent {
    
    public $name;
    public $city;
    public $address1;
    public $state;
    public $zip;
    public $url;
    public $logo;
    public $showMap;
    
    function __construct(
        $name,
        $city,
        $address1,
        $state,
        $zip,
        $url,
        $logo,
        $showMap
    ) {
        $this->name = $name;
        $this->city = $city;
        $this->address1 = $address1;
        $this->state = $state;
        $this->zip = $zip;
        $this->url = $url;
        $this->logo = $logo;
        $this->showMap = $showMap;
    }
}
?>