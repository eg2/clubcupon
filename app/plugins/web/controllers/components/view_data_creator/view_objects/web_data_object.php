<?php

class WebDataObjectComponent {
    
    public $user;
    public $city;
    public $message;
    public $vData;
    public $trackingEvent;
    
    function __construct($user, $city,$message, $vData, $trackingEvent) {
        
        $this->user = $user;
        $this->city = $city;
        $this->message = $message;
        $this->vData = $vData;
        $this->trackingEvent = $trackingEvent;
    }
}
?>