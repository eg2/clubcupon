<?php
class WebEplaningObjectComponent {

	public $idAd;
	public $width;
	public $height;
	public $idAviso;
	public $isTourism;
	public $idIframe;
	public $typeAd;
	
	function __construct($idAd, $width, $height, $idAviso, $isTourism, $idIframe, $typeAd) {
		$this->idAd = $idAd;
		$this->width = $width;
		$this->height = $height;
		$this->idAviso = $idAviso;
		$this->isTourism = $isTourism;
		$this->idIframe = $idIframe;
		$this->typeAd = $typeAd;
	}
}