<?php

class WebMessageObjectComponent {
    
    public $text;
    public $code;
    
    function __construct($text, $code) {
        $this->text = $text;
        $this->code = $code;
        
    }
}
?>