<?php

class WebTrackingObjectComponent {
    
    public $subscriptionOk;
    public $registerOk;
    public $buyOk;
    
    function __construct($subscriptionOk, $registerOk, $buyOk) {
        $this->subscriptionOk = $subscriptionOk;
   		$this->registerOk = $registerOk;
    	$this->buyOk = $buyOk;
    }
}
?>