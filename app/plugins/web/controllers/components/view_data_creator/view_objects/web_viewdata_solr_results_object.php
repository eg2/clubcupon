<?php

class WebViewdataSolrResultsObjectComponent {
    
    public $dealList;
    public $citiesLinkList;
    public $actualPage;
    public $isSetHFacetField;
    public $hFacetFieldFirstPathLevelID;
    
    function __construct($delList, $citiesLinkList, $actualPage, $isSetHFacetField, $hFacetFieldFirstPathLevelID) {
        $this->dealList = $delList;
        $this->citiesLinkList = $citiesLinkList;
        $this->actualPage = $actualPage;
        $this->isSetHFacetField = $isSetHFacetField;
        $this->hFacetFieldFirstPathLevelID = $hFacetFieldFirstPathLevelID;
    }
}
?>