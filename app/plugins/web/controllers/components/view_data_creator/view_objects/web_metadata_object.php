<?php

class WebMetadataComponent {

    public $keywords;
    public $description;
    public $title;
    public $category;
    

    function __construct($keywords, $description, $title) {
        $this->keywords = $keywords;
        $this->description = $description;
        $this->title = $title;
    }
}
?>