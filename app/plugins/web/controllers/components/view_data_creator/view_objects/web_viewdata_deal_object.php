<?php

class WebViewdataDealObjectComponent {
    
    public $deal;
    public $dealList;
    public $dealListSearched;
    
    function __construct($deal, $dealList, $dealListSearched = null) {
        
        $this->deal = $deal;
        $this->dealList = $dealList;
        $this->dealListSearched = $dealListSearched;
    }
}
?>