<?php

class WebUserObjectComponent {
    
    public $pin;
    public $userType;
    public $userEmail;
    public $menuElements;
    
    function __construct($pin, $userType, $userEmail, $menuElements) {
        $this->pin = $pin;
        $this->userType = $userType;
        $this->userEmail = $userEmail;
        $this->menuElements = $menuElements;
    }
}
?>