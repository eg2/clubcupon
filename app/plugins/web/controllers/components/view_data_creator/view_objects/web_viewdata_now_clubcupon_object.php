<?php

class WebViewdataNowClubcuponObjectComponent {
	
	public $category;
	public $categories; 
    public $branchesListDeals;
    
    function __construct($category,$categories, $branchesListDeals) 
    {
		$this->category = $category;
    	$this->categories = $categories;
        $this->branchesListDeals = $branchesListDeals;
    }
}
?>