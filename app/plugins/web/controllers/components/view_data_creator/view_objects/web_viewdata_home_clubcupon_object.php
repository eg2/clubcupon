<?php

class WebViewdataHomeClubcuponObjectComponent {

    public $dealA;
    public $dealListA1;
    public $dealListA2;
    public $dealListA3;
    public $dealListB;
    public $categoryLinkList;
    public $bannerList;
    public $bannerMobile;

    function __construct($dealA, $dealListA1, $dealListA2, $dealListA3, $dealListB, $categoryLinkList,$bannerList=null,$bannerMobile=null) {

        $this->dealA = $dealA;
        $this->dealListA1 = $dealListA1;
        $this->dealListA2 = $dealListA2;
        $this->dealListA3 = $dealListA3;
        $this->dealListB = $dealListB;
        $this->categoryLinkList = $categoryLinkList;
        $this->bannerList = $bannerList;
        $this->bannerMobile = $bannerMobile;
    }
}
?>