<?php

class WebLinkObjectComponent {

    public $url;
    public $label;
    public $logoRight;
    public $logoLeft;
    public $classStyle;
    public $description;

    function __construct($url, $label, $logoRight = null, $logoLeft = null, $classStyle = null,$description) {
        $this->url = $url;
        $this->label = $label;
        $this->logoRight = $logoRight;
        $this->logoLeft = $logoLeft;
        $this->classStyle = $classStyle;
        $this->description = $description;
        
    }

}
