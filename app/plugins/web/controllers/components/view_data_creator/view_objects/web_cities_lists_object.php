<?php

class WebCitiesListsObjectComponent {
    
    public $citiesLinks;
    public $extraLinks;
    
    function __construct($citiesLinks, $extraLinks) {
        $this->citiesLinks = $citiesLinks;
        $this->extraLinks = $extraLinks;
    }
}
?>