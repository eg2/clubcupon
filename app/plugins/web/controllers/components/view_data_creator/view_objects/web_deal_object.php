<?php

class WebDealObjectComponent {

    public $id;
    public $title;
    public $subtitle;
    public $description;
    public $price;
    public $discountedPrice;
    public $discountedPercent;
    public $condition;
    public $endDate;
    public $imageURL;
    public $onlyPrice;
    public $hidePrice;
    public $isPurchasable;
    public $tsInicio;
    public $tsFin;
    public $slug;
    public $isTourism;
    public $couponStartDate;
    public $couponExpiryDate;
    public $company;
    public $city;
    public $customCompanyAddress2;
    public $latitude;
    public $longitude;
    public $metaKeywords;
    public $metaDescription;
    public $shortUrl;
    public $ogData;
    public $category;
    public $isBanner;
    public $isVariableExpiration;
    public $couponDuration;

}
