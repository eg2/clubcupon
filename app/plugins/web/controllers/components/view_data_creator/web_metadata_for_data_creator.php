<?php

App::import('Component', 'web.view_data_creator/view_objects/WebMetadataObject');

class WebMetadataForDataCreatorComponent {

    private $metaData;
    
    function __construct() {
        $defaultMetaData = Configure::read('web.meta');
        $keywords = $defaultMetaData['keywords'];
        $description = $defaultMetaData['description'];
        //$title = Configure::read('site.name');
        $title = $defaultMetaData['title'];
        $this->metaData = new WebMetadataComponent($keywords, $description, $title);
    }

    public function getMetaData($vData) {
        
        switch ($vData->viewType) {
            case WebViewDataCreatorComponent::DEAL_DETAIL_CC:
                $this->setMetaDataForDeal($vData->deal);
                break;
            case WebViewDataCreatorComponent::DEAL_DETAIL:
                $this->setMetaDataForDeal($vData->deal);
                break;
            case WebViewDataCreatorComponent::CITY_HOME_CC:
                $this->setMetaDataForHome($vData->dealA);
                break;
            case WebViewDataCreatorComponent::PAGE_WHO:
                $this->setMetaDataForWho($vData->deal);
                break;
            case WebViewDataCreatorComponent::PAGE_TERMS:
                $this->setMetaDataForTerms($vData->deal);
                break;
            case WebViewDataCreatorComponent::PAGE_POLICY:
                $this->setMetaDataForPolicy($vData->deal);
                break;
            case WebViewDataCreatorComponent::PAGE_PROTECTION:
                $this->setMetaDataForProtection($vData->deal);
                break;
            case WebViewDataCreatorComponent::FORM_USER_REGISTER:
                $this->setMetaDataForUserRegister($vData->deal);
                break;
            case WebViewDataCreatorComponent::USER_LOGIN:
                $this->setMetaDataForUserLogin($vData->deal);
                break;
            case WebViewDataCreatorComponent::CITY_NOW_LANDING:
                $this->setMetaDataForNowLanding($vData->deal);
                break;
            case WebViewDataCreatorComponent::CITY_NOW_FAQ:
                $this->setMetaDataForNowFaq($vData->deal);
                break;
            default: break;
        }
        return $this->metaData;
    }

	private function setMetaDataForNowLanding($deal) {
    	$this->metaData->description = 'Como funciona Club Cupón Ya - '. $this->metaData->description;
   		$this->metaData->title = 'Como funciona Club Cupón Ya - '. $this->metaData->title;
    }
	private function setMetaDataForNowFaq($deal) {
    	$this->metaData->description = 'Preguntas frecuentes Club Cupón Ya - '. $this->metaData->description;
    	$this->metaData->title = 'Preguntas frecuentes Club Cupón Ya - '. $this->metaData->title;
    }
	private function setMetaDataForUserLogin($deal) {
    	$this->metaData->description = 'Ingresar a Club Cupón - '. $this->metaData->description;
    }
	private function setMetaDataForUserRegister($deal) {
    	$this->metaData->description = 'Registrarse en Club Cupón - '. $this->metaData->description;
    	$this->metaData->title = 'Registro Club Cupón - '. $this->metaData->title;
    }
    private function setMetaDataForWho($deal) {
    	$this->metaData->description = 'Quienes somos - Club Cupón - '. $this->metaData->description;
    	$this->metaData->title = 'Quienes somos Club Cupón - '. $this->metaData->title;
    }
    
 	private function setMetaDataForTerms($deal) {
    	$this->metaData->description = 'Términos y condiciones - Club Cupón - '. $this->metaData->description;
    	$this->metaData->title = 'Términos y condiciones Club Cupón - '. $this->metaData->title;
    }
    
 	private function setMetaDataForPolicy($deal) {
   		$this->metaData->description = 'Politicas de privacidad - Club Cupón - '. $this->metaData->description;
   		$this->metaData->title = 'Politicas de privacidad Club Cupón - '. $this->metaData->title;
   		
    }
    
    private function setMetaDataForProtection($deal) {
    	$this->metaData->description = 'Protección de datos personales - Club Cupón - '. $this->metaData->description;
    	$this->metaData->title = 'Protección de datos personales Club Cupón - '. $this->metaData->title;
    }
       
    private function setMetaDataForHome($deal) {
    	if ($deal) {
    		$this->metaData->description = 'Club Cupón - '. $deal->title. ' - '. $this->metaData->description;
    	}
        $this->metaData->title = 'ClubCupon.com.ar - ' . $this->metaData->title;
    }

    private function setMetaDataForDeal($deal) {
    	
    	if ($deal->metaKeywords) {
            $this->metaData->keywords .= ', '.$deal->metaKeywords;
        }
        if ($deal->metaDescription) {
            $this->metaData->description = 'Club Cupón - '.$deal->company->name. ' - ' .$deal->title. ' - '. $deal->metaDescription;
        }else{
        	$this->metaData->description = 'Club Cupón - '.$deal->company->name. ' - ' .$deal->title. ' - '. $this->metaData->description;
        }
        $this->metaData->title = 'Club Cupón - Oferta ' . $deal->company->name . ' ' . $deal->title;
        $this->metaData->category = str_replace(" | ", "/", $deal->category); 
        
    }
}
