<?php

App::import('Model', 'web.WebCity');

App::import('Component', 'web.view_data_creator/WebMetadataForDataCreator');
App::import('Component', 'web.view_data_creator/WebDealsForDataCreator');
App::import('Component', 'web.view_data_creator/WebCategoryLinksForDataCreator');
App::import('Component', 'web.view_data_creator/WebCitiesLinksForDataCreator');
App::import('Component', 'web.WebTabMenuTop');

class WebBaseStructureComponent {

    protected $metadataForData;
    protected $dealsForData;
    protected $hasHeaderCitiesLinks = true;
    protected $WebTabMenuTop;

    function __construct($userType = ConstUserTypes::Guest) {
        $this->metadataForData = new WebMetadataForDataCreatorComponent();
        $this->dealsForData = new WebDealsForDataCreatorComponent($userType);
        $this->categoryLinksForData = new WebCategoryLinksForDataCreatorComponent();
        $this->citiesLinksForData = new WebCitiesLinksForDataCreatorComponent();
        $this->WebTabMenuTop = new WebTabMenuTopComponent();
        $this->WebCity = new WebCity();
    }

    public function create($params) {
        return new stdClass();
    }

    public function postCreate($vData, $params) {
        if ($this->hasHeaderCitiesLinks) {
            $vData->citiesLinkList = $this->citiesLinksForData->getCitiesLinks();
        }

        $qt = Configure::read('web.maxQuantitySpecialSearchLinks');
        $cSlug = ($params['citySlug']) ? $params['citySlug'] : $params['lastGeoCitySlug'];
        $city = $this->WebCity->findBySlug($cSlug);
        $vData->specialLinkList = $this->categoryLinksForData->getCategoryLinks(
            $qt,
            $city['WebCity']['id'],
            ConstSearchLinksTypes::Especial
        );

        $vData->enablePopup = $params['EnablePopup'];
        $vData->redirectUrl = $params['redirectUrl'];
        $vData->selectedTab = $this->WebTabMenuTop->getSelectedTab($params['url']);
        $vData->metaData = $this->metadataForData->getMetaData($vData);
        return $vData;
    }

    protected function getIdsFromDealsArray($dealArrayList) {
        $resp = array();
        foreach ($dealArrayList as $element) {
            $resp[] = $element->id;
        }
        return $resp;
    }

}
?>