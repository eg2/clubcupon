<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataDealObject');

class WebDealDetailStructureComponent extends WebBaseStructureComponent {
    
    protected $hasHeaderCitiesLinks = false;
    
    public function create($params) {
        $dealSlug = $params['dealSlug'];
        $cityID = $params['cityData']->id;
        $this->dealsForData->dimensionImage='maindeal_big_thumb';
        $deal = $this->dealsForData->getDealBySlug($dealSlug);
        if (empty($deal->id)) {
            return null;
        }
        
        $blackListDeals = array($deal->id);
        $qtDealDetailList = Configure::read('web.quantityDealDetailList');
        $this->dealsForData->dimensionImage='listdeals_big_thumb';
        $dealList = $this->dealsForData->getListDealForCity($qtDealDetailList, $cityID, $blackListDeals);
        $vData = new WebViewdataDealObjectComponent($deal, $dealList);
        return $vData;
    }
}
?>