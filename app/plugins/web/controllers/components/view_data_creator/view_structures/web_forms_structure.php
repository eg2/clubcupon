<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataFormObject');

class WebFormsStructureComponent extends WebBaseStructureComponent {

    protected $hasHeaderCitiesLinks = true;

    public function __construct() {
        parent::__construct();
    }

    public function create($params) {

        $vData = new WebViewdataFormObjectComponent($params['formMessages']);
        return $vData;
    }
}
?>