<?php

App::import('Model', 'api.ApiCity');

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataNowClubcuponObject');

class WebDealNowClubcuponStructureComponent extends WebBaseStructureComponent {

	private 	$ApiCity;
	protected 	$hasHeaderCitiesLinks = true;

	public function __construct() {
		parent::__construct();
		$this->ApiCity = new ApiCity();
	}

	public function create($params) {
		$this->dealsForData->dimensionImage = 'now_listdeals_thumb';
	    $dealArrayForList = $this->dealsForData->getListDealForCCNow($params['branchesDeals']);
	    $title = $params['cityData']->name;
	    $basePathUrl = '/'.$params['cityData']->slug;
	    $dealListA = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayForList);

		$category = $params['category'];
		$categories = $params['categories'];

		return new WebViewdataNowClubcuponObjectComponent($category, $categories, $dealListA);
	}

	
}
?>