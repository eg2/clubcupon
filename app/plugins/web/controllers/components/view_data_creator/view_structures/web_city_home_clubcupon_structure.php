<?php

App::import('Model', 'api.ApiCity');
App::import('Model', 'web.SearchLinks');

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataHomeClubcuponObject');
App::import('Component', 'web.view_data_creator/view_objects/WebDealListObject');

class WebCityHomeClubcuponStructureComponent extends WebBaseStructureComponent {

    private $ApiCity;
    protected $hasHeaderCitiesLinks = true;

    public function __construct() {
        parent::__construct();
        $this->ApiCity = new ApiCity();
        $this->WebSearchLinks = new WebSearchLinks();
    }

    public function create($params) {
        $cityID = $params['cityData']->id;
        $this->dealsForData->dimensionImage = 'outstanding_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $dealA = $this->dealsForData->getPriorityDealForCity(1, $cityID);
        
        $this->dealsForData->dimensionImage = 'listdeals_outstanding_thumb';
        
        $dealB = $this->dealsForData->getPriorityDealForCity(2, $cityID);
        $dealC = $this->dealsForData->getPriorityDealForCity(3, $cityID);
        
        $this->dealsForData->dimensionImage = 'medium_big_thumb';

        
        // LIST BANNERS
        $bannerArrayForListA1 = $this->dealsForData->getListBannerForCity($cityID);
        
        $elementsByRow=4;
        $quantityMediumsDeals=2;//ofertas que aparecen en la primera fila del listado
 
        // LIST A1 ------------------------------------------------------------------------------------------------
        $blackListDeals = array($dealA->id, $dealB->id, $dealC->id);
        $this->dealsForData->dimensionImage='listdeals_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $quantityClubCuponHomeListA1=count($bannerArrayForListA1)>0?Configure::read('web.quantityClubCuponHomeListA1')+(Configure::read('web.quantityClubCuponHomeListA1')-count($bannerArrayForListA1)):Configure::read('web.quantityClubCuponHomeListA1');
        $dealArrayForListA1 = $this->dealsForData->getListDealForCity(
            $quantityClubCuponHomeListA1,
            $cityID,
            $blackListDeals 
        );
        array_unshift($dealArrayForListA1,$dealC);
        array_unshift($dealArrayForListA1,$dealB);
        $title = $params['cityData']->name;
        $basePathUrl = '/'.$params['cityData']->slug;
        
        //$dealListA1 = new WebDealListObjectComponent($title, $basePathUrl,array_merge( $dealArrayForListA1, $bannerArrayForListA1) );
        $dealListA1 = new WebDealListObjectComponent($title, $basePathUrl,$dealArrayForListA1 );
        
        $bannerList = new WebDealListObjectComponent($title, $basePathUrl,$bannerArrayForListA1 );
         
        //var_dump($dealListA1);echo '<br>**************************<br>';die;
        

        // LIST A2 ------------------------------------------------------------------------------------------------
        $dealsIds = $this->getIdsFromDealsArray($dealArrayForListA1);
        $blackListDeals = array_merge($blackListDeals, $dealsIds);
        $this->dealsForData->dimensionImage='listdeals_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $dealArrayForListA2 = $this->dealsForData->getListDealForCity(
            Configure::read('web.quantityClubCuponHomeListA2'),
            $cityID,
            $blackListDeals
        );
        $title = $params['cityData']->name;
        $basePathUrl = '/'.$params['cityData']->slug;
        $dealListA2 = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayForListA2);

        // LIST A3 ------------------------------------------------------------------------------------------------
        $dealsIds = $this->getIdsFromDealsArray($dealArrayForListA2);
        $blackListDeals = array_merge($blackListDeals, $dealsIds);
        $this->dealsForData->dimensionImage='listdeals_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $dealArrayForListA3 = $this->dealsForData->getListDealForCity(
            Configure::read('web.quantityClubCuponHomeListA3'),
            $cityID,
            $blackListDeals
        );
        $title = $params['cityData']->name;
        $basePathUrl = '/'.$params['cityData']->slug;
        $dealListA3 = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayForListA3);
        
        
        
        
        // LIST B ------------------------------------------------------------------------------------------------
        
        $randomCity = $this->getValidSpecialsRandomCity($params['cityData']);
        $randomCityId = $randomCity['City']['id'];
        $dealArrayForListB = $this->dealsForData->getListDealForCity(
            Configure::read('web.quantityClubCuponHomeListB'),
            $randomCityId,
            array()
        );
        $title = $randomCity['City']['name'];
        $basePathUrl = '/'.$randomCity['City']['slug'];
        $dealListB = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayForListB);

        $categoryLinkList = $this->categoryLinksForData->getCategoryLinks(
            Configure::read('web.maxQuantityHomeCategoryLinks'), 
            $cityID,
            ConstSearchLinksTypes::Destacado
        );

        $bannerMobile=$this->getBannerMobile($cityID);
        
        
        $vData = new WebViewdataHomeClubcuponObjectComponent(
            $dealA, 
            $dealListA1, 
            $dealListA2, 
            $dealListA3, 
            $dealListB, 
            $categoryLinkList,
        	$bannerList,
        	$bannerMobile
        );
        return $vData;
    }

    private function getBannerMobile($cityID){
    	$links = $this->WebSearchLinks->findAllByCityIdAndTypeId($cityID, ConstSearchLinksTypes::Banner_mobile, true);
    	$link=null;
    	if(!empty($links)){
    		$n=count($links);
    		$msec=$this->millitime();
    		
    		$index_selected=fmod($msec,$n);
    		
    		//echo 'n:'.$n.'- msec:'.$msec.'- index:'.$index_selected.'<br>';
    		
    		$link=$links[$index_selected];
    	}
    	
    	return $link;
    }
    private function millitime() {
    	$microtime = microtime();
    	$comps = explode(' ', $microtime);
    	return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    }
    private function getValidRandomCity($excludeCityId) {

        $cities = $this->ApiCity->findAllForApi();

        foreach ($cities as $city) {
            if ($city['City']['id'] == $excludeCityId || $city['City']['is_corporate'] || $city['City']['is_business_unit']) {
                $key = array_search($city, $cities, TRUE);
                unset($cities[$key]);
            }
        }

        $citiesDef = array();
        foreach ($cities as $city) { $citiesDef[] = $city; }

        $citiesLength = sizeof($citiesDef)-1;
        srand((double) microtime() * 1000000);
        $randNum = rand(0,$citiesLength);
        return $citiesDef[$randNum];
    }
    private function getValidSpecialsRandomCity($cityData) {
    	$excludeCityId=$cityData->id;
    	$specialCitiesByCity=Configure::read('web.home.specialCitiesByCity');
    	$cities=array();
    	$specialCities=array();
    	if (array_key_exists($cityData->slug, $specialCitiesByCity)) {
    		$specialCities=$specialCitiesByCity[$cityData->slug];
    	}
    	
    	if(empty($specialCities)){
    		
    		$cities = $this->ApiCity->findAllForApi();
    		foreach ($cities as $city) {
    			if (!$city['City']['is_group'] || $city['City']['id'] == $excludeCityId ) {
    				$key = array_search($city, $cities, TRUE);
    				unset($cities[$key]);
    			}
    		}	
    	}else{
    	
    		$cities = $this->ApiCity->findAllBySlugs($specialCities);
    	}
    

    
    	$citiesDef = array();
    	foreach ($cities as $city) { $citiesDef[] = $city; }
    
    	$citiesLength = count($citiesDef);
    	$mc=microtime()* 1000000;
    	$randNum = ($mc % $citiesLength);
		
    	return $citiesDef[$randNum];
    }
}
?>