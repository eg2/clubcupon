<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataDealObject');
App::import('Component', 'web.view_data_creator/view_objects/WebDealListObject');

class WebDealDetailClubcuponStructureComponent extends WebBaseStructureComponent {
    
    protected $hasHeaderCitiesLinks = true;
    
    public function create($params) {
        $dealSlug = $params['dealSlug'];
        $cityID = $params['cityData']->id;
        $this->dealsForData->dimensionImage='maindeal_big_thumb';//cambia tamaño de la imagen de las Ofertas
        $deal = $this->dealsForData->getDealBySlug($dealSlug, true);
        if (empty($deal->id)) {
            return null;
        }
        
        $blackListDeals = array($deal->id);
        $this->dealsForData->dimensionImage='searchedlist_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $dealArrayListSearched = $this->dealsForData->getListDealForCity(
            Configure::read('web.quantityClubCuponDealDetailSearchedList'), 
            $cityID, 
            $blackListDeals
        );
        $title = Configure::read('web.titleClubCuponDealDetailSearchedList');
        $basePathUrl = '';
        $dealListSearched = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayListSearched);
        
        $dealsIds = $this->getIdsFromDealsArray($dealArrayListSearched);
        $blackListDeals = array_merge($blackListDeals, $dealsIds);
        $this->dealsForData->dimensionImage='listdeals_big_thumb'; //cambia tamaño de las imagenes de las Ofertas
        $dealArrayList = $this->dealsForData->getListDealForCity(
            Configure::read('web.quantityClubCuponDealDetailList'), 
            $cityID, 
            $blackListDeals
        );
        $title = Configure::read('web.titleClubCuponDealDetailList', 'Más ofertas');
        $basePathUrl = '/'.$params['cityData']->slug;
        $dealList = new WebDealListObjectComponent($title, $basePathUrl, $dealArrayList);

        $vData = new WebViewdataDealObjectComponent($deal, $dealList, $dealListSearched);
        return $vData;
    }

}
?>