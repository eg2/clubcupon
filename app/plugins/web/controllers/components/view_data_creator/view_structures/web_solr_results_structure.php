<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataSolrResultsObject');
App::import('Component', 'web.view_data_creator/view_objects/WebDealListObject');

class WebSolrResultsStructureComponent extends WebBaseStructureComponent {

    protected $hasHeaderCitiesLinks = true;

    public function __construct() {
        parent::__construct();
    }

    public function create($params) {

        $actualPage = $params['actualPage'];
        $citiesLinkList = $this->citiesLinksForData->getCitiesLinks();

        $dealsArray = $this->dealsForData->getListDealsFromSolr($params['resultsSolr'], $actualPage);
        $dealList = new WebDealListObjectComponent(null, null, $dealsArray);

        $vData = new WebViewdataSolrResultsObjectComponent(
            $dealList, 
            $citiesLinkList, 
            $actualPage,
            $params['isSetHFacetField'],
            $params['hFacetFieldFirstPathLevelID']
        );
        return $vData;
    }
}
?>