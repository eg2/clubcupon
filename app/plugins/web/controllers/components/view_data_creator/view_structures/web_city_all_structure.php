<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataHomeallObject');

class WebCityAllStructureComponent extends WebBaseStructureComponent {

    protected $hasHeaderCitiesLinks = true;

    public function create($params) {
    	
        $cityID = $params['cityData']->id;
        
      
         
        
        
        $blackListDeals = array();
        $maxAllDeals = Configure::read('web.maxAllDeals');
        $this->dealsForData->dimensionImage='listdeals_big_thumb';
        
        // LIST BANNERS
        $bannerArrayForListA1 = $this->dealsForData->getListBannerForCity($cityID);
        $title = $params['cityData']->name;
        $basePathUrl = '/'.$params['cityData']->slug;
        $bannerList = new WebDealListObjectComponent($title, $basePathUrl,$bannerArrayForListA1 );
        
        
        $dealList = $this->dealsForData->getListDealForCity($maxAllDeals, $cityID, $blackListDeals);
        $vData = new WebViewdataHomeallObjectComponent($dealList,$bannerList);
        return $vData;
    }
}
?>