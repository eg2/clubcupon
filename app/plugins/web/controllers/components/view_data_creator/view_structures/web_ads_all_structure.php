<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataHomeObject');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataAdsAllObject');
App::import('Component', 'web.view_data_creator/view_objects/WebEplaningObject');

class WebAdsAllStructureComponent extends WebBaseStructureComponent {

    private $ApiCity;
    protected $hasHeaderCitiesLinks = true;

    public function __construct() {
        parent::__construct();
    }

    public function create($params) {
        $idAd = $params['id-ad'];
        $width = $params['width-ad'];
        $height = $params['height-ad'];
        $idAviso = $params['idAviso'];
        $isTourism = $params['isTourism'];
        $idIframe = $params['idIframe'];
        $typeAd = $params['typeAd'];
        
        $ePlanning = new WebEplaningObjectComponent($idAd, $width, $height, $idAviso, $isTourism, $idIframe, $typeAd);
        $vData = new WebViewdataAdsAllObjectComponent($ePlanning);
        return $vData;
    }
}
?>