<?php

App::import('Component', 'web.view_data_creator/view_structures/WebBaseStructure');
App::import('Component', 'web.view_data_creator/view_objects/WebViewdataHomeObject');

class WebCityHomeStructureComponent extends WebBaseStructureComponent {

    protected $hasHeaderCitiesLinks = false;

    public function create($params) {
        $cityID = $params['cityData']->id;
        $bannerArrayForListA1 = $this->dealsForData->getListBannerForCity($cityID);
        $title = $params['cityData']->name;
        $basePathUrl = '/' . $params['cityData']->slug;
        $bannerList = new WebDealListObjectComponent($title, $basePathUrl, $bannerArrayForListA1);
        $this->dealsForData->dimensionImage = 'outstandingourbenefits_big_thumb';
        $dealA = $this->dealsForData->getPriorityDealForCity(1, $cityID);
        $dealB = $this->dealsForData->getPriorityDealForCity(2, $cityID);
        $blackListDeals = array(
            $dealA->id,
            $dealB->id
        );
        $qtDealsHomeList = Configure::read('web.quantityDealsHomeList');
        $this->dealsForData->dimensionImage = 'listdeals_big_thumb';
        $dealList = $this->dealsForData->getListDealForCity($qtDealsHomeList, $cityID, $blackListDeals);
        $vData = new WebViewdataHomeObjectComponent($dealA, $dealB, $dealList, $bannerList);
        return $vData;
    }

}
