<?php

App::import('Component', 'web.view_data_creator/view_objects/WebDealObject');
App::import('Component', 'web.view_data_creator/view_objects/WebCompanyObject');
App::import('Component', 'web.view_data_creator/view_objects/WebCityObject');
App::import('Component', 'api.ApiBuyService');
App::import('Component', 'product.ProductInventoryService');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'web.WebAttachment');
App::import('Model', 'web.WebCity');
App::import('Model', 'web.WebBranchesDeals');
App::import('Model', 'web.WebSearchLinks');

class WebDealsForDataCreatorComponent {

    public $viewFormatResponse = true;
    public $dimensionImage = 'medium_big_thumb';
    private $imageDefaultOptions = array(
        'class' => '',
        'alt' => 'alt',
        'title' => 'title',
        'type' => 'jpg'
    );
    private $actualUserType;

    function __construct($userType = ConstUserTypes::Guest) {
        $this->ApiBuyService = new ApiBuyServiceComponent();
        $this->ApiDeal = new ApiDeal();
        $this->WebAttachment = new WebAttachment();
        $this->WebCity = new WebCity();
        $this->WebBranchesDeals = new WebBranchesDeals();
        $this->actualUserType = $userType;
        $this->WebSearchLinks = new WebSearchLinks();
        $this->ProductInventoryService = new ProductInventoryServiceComponent();
    }

    public function getDealBySlug($dealSlug, $isDetail = false) {
        $deal = $this->ApiDeal->findForApiBySlug($dealSlug);
        $deal = $this->addDealsExtraAttributes($deal);
        $deal = $this->addDealsExtraAttributesNow($deal);
        $isNow = $deal['Deal']['is_now'];
        $dealForView = $this->getDealForView($deal, 'Deal', true, $isNow, $isDetail);
        return $dealForView;
    }

    private function addDealsExtraAttributes($deal) {
        if ($this->ApiBuyService->isPurchasable($deal) && $this->actualUserType != ConstUserTypes::Company) {
            $deal['Deal']['is_purchasable'] = '1';
            $deal['Deal']['availableQuantity'] = $this->ProductInventoryService->availableQuantity($deal['Deal']['id']);
        } else {
            $deal['Deal']['is_purchasable'] = '0';
        }
        $deal['Deal']['has_subdeals'] = $this->ApiDeal->hasSubdeals($deal);
        return $deal;
    }

    private function addDealsExtraAttributesNow($deal) {
        $branchDeal = $this->WebBranchesDeals->findByDealId($deal['Deal']['id']);
        if ($branchDeal != null) {
            $deal['Deal']['ts_inicio'] = $branchDeal['WebBranchesDeals']['ts_inicio'];
            $deal['Deal']['ts_fin'] = $branchDeal['WebBranchesDeals']['ts_fin'];
            $deal['Deal']['latitude'] = $branchDeal['WebBranchesDeals']['latitud'];
            $deal['Deal']['longitude'] = $branchDeal['WebBranchesDeals']['longitud'];
        }
        return $deal;
    }

    private function makeImageUrl($model, $attachment, $options = null) {
        $this->imageDefaultOptions = array_merge($this->imageDefaultOptions, array(
            'dimension' => $this->dimensionImage
        ));
        if (!empty($options)) {
            $options = array_merge($this->imageDefaultOptions, $options);
        } else {
            $options = $this->imageDefaultOptions;
        }
        $toEncript = Configure::read('Security.salt');
        $toEncript.= $model . $attachment['Attachment']['id'];
        $toEncript.= $options['type'] . $options['dimension'];
        $toEncript.= Configure::read('site.name');
        $imageHash = $options['dimension'] . '/';
        $imageHash.= $model . '/';
        $imageHash.= $attachment['Attachment']['id'];
        $imageHash.= '.' . md5($toEncript);
        $imageHash.= '.' . $options['type'];
        return '/img/' . $imageHash;
    }

    private function isOriginalImage($dealId, $dealType = 'Deal', $bySlug, $isbySlugAndNow) {
        if ($bySlug && $isbySlugAndNow) {
            $dealType = 'NowDeal';
        }
        $ds = DIRECTORY_SEPARATOR;
        $pathToDirImage = APP . 'media' . $ds . $dealType . $ds . $dealId;

        $resp = is_dir($pathToDirImage);

        return $resp;
    }

    private function getImageUrl($deal, $dealType = 'Deal', $bySlug = false, $isbySlugAndNow = false) {
        if ($this->isOriginalImage($deal[$dealType]['id'], $dealType, $bySlug, $isbySlugAndNow)) {
            $class = $dealType;
            if ($bySlug && $isbySlugAndNow) {
                $class = 'NowDeal';
            }
            $myAttachment = $this->WebAttachment->find('first', array(
                'conditions' => array(
                    'class' => $class,
                    'foreign_id' => $deal[$dealType]['id']
                )
            ));
            if ($bySlug) {
                $dealType = 'NowDeal';
            }
            $url = $this->makeImageUrl($dealType, $myAttachment, null);
        } else {
            $url = NO_ORIGINAL_IMAGE;
        }
        return $url;
    }

    private function getDealForView($deal, $dealType = 'Deal', $bySlug = false, $isbySlugAndNow = false, $isDetail = false) {
        $dealForView = new WebDealObjectComponent();
        $dealForView->id = $deal[$dealType]['id'];
        $dealForView->title = $deal[$dealType]['name'];
        $dealForView->subtitle = $deal[$dealType]['subtitle'];
        $dealForView->description = $deal[$dealType]['description'];
        $dealForView->price = intval($deal[$dealType]['original_price']);
        $dealForView->discountedPrice = intval($deal[$dealType]['discounted_price']);
        $dealForView->discountedPercent = $deal[$dealType]['discount_percentage'];
        $dealForView->condition = $deal[$dealType]['coupon_condition'];
        $dealForView->endDate = $deal[$dealType]['end_date'];
        $dealForView->imageURL = $this->getImageUrl($deal, $dealType, $bySlug, $isbySlugAndNow);
        $dealForView->onlyPrice = $deal[$dealType]['only_price'];
        $dealForView->hidePrice = $deal[$dealType]['hide_price'];
        $dealForView->isPurchasable = $deal[$dealType]['is_purchasable'];
        $dealForView->slug = $deal[$dealType]['slug'];
        $dealForView->isTourism = $deal[$dealType]['is_tourism'];
        $dealForView->couponStartDate = $deal[$dealType]['coupon_start_date'];
        $dealForView->couponExpiryDate = $deal[$dealType]['coupon_expiry_date'];
        $dealForView->company = $this->getCompanyForView($deal, $dealType);
        $dealForView->city = $this->getCityForView($deal, $dealType);
        $dealForView->customCompanyAddress2 = $deal[$dealType]['custom_company_address2'];
        $dealForView->latitude = $deal[$dealType]['latitude'];
        $dealForView->longitude = $deal[$dealType]['longitude'];
        $dealForView->metaKeywords = $deal[$dealType]['meta_keywords'];
        $dealForView->metaDescription = $deal[$dealType]['meta_description'];
        $dealForView->shortUrl = $deal[$dealType]['bitly_short_url_prefix'];
        if ($dealType == 'NowDeal') {
            $dealForView->tsInicio = $deal['NowBranchesDeals']['ts_inicio'];
            $dealForView->tsFin = $deal['NowBranchesDeals']['ts_fin'];
        } else {
            $dealForView->tsInicio = $deal[$dealType]['ts_inicio'];
            $dealForView->tsFin = $deal[$dealType]['ts_fin'];
            $dealForView->availableQuantity = $deal[$dealType]['availableQuantity'];
        }
        if ($isDetail) {
            $dealForView->ogData = $this->getOgDataForDeal($deal, $dealType, $bySlug, $isbySlugAndNow);
            $dealForView->category = $deal['DealFlatCategory']['path'];
        }
        $dealForView->isBanner = false;
        $dealForView->hasSubdeals = $deal[$dealType]['has_subdeals'];
        $dealForView->isVariableExpiration = $deal[$dealType]['is_variable_expiration'];
        $dealForView->couponDuration = $deal[$dealType]['coupon_duration'];
        return $dealForView;
    }

    private function getBannerForView($deal, $dealType = 'SearchLinks') {
        $bannerForView = new WebDealObjectComponent();
        $bannerForView->id = $deal[$dealType]['id'];
        $bannerForView->slug = $deal[$dealType]['url'];
        $bannerForView->title = $deal[$dealType]['label'];
        $bannerForView->imageURL = $deal[$dealType]['image'];
        $bannerForView->isBanner = true;
        return $bannerForView;
    }

    private function getOgDataForDeal($deal, $dealType, $bySlug, $isbySlugAndNow) {
        $og_data = array();
        $og_data['og:title'] = $deal[$dealType]['name'];
        $og_data['og:type'] = 'product';
        $og_data['og:image'] = STATIC_DOMAIN_FOR_CLUBCUPON . $this->getImageUrl($deal, $dealType, $bySlug, $isbySlugAndNow);
        $og_data['og:url'] = STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $deal['City']['slug'] . '/deal/' . $deal[$dealType]['slug'];
        $og_data['og:site_name'] = Configure::read('site.name');
        if (!empty($deal['City']['fb_api_key'])) {
            $og_data['fb:app_id'] = $deal['City']['fb_api_key'];
        } else {
            $og_data['fb:app_id'] = Configure::read('facebook.fb_api_key');
        }
        $og_data['og:description'] = empty($deal[$dealType]['subtitle']) ? " " : ApiStrUtils::cleanChars($deal[$dealType]['subtitle']);
        $this->setMetaPropertyCxenseParse($og_data, $deal, $dealType);
        return $og_data;
    }

    private function setMetaPropertyCxenseParse(&$og_data, $deal, $dealType) {


        $og_data['cXenseParse:recs:city'] = $deal['City']['name'];
        $og_data['cXenseParse:recs:price'] = $deal[$dealType]['original_price'];
        $og_data['cXenseParse:recs:discountedPrice'] = $deal[$dealType]['discounted_price'];
        $og_data['cXenseParse:recs:discountPercentage'] = 100 * ($deal[$dealType]['original_price'] - $deal[$dealType]['discounted_price']) / $deal[$dealType]['original_price'];
        $og_data['cXenseParse:recs:productCompanyName'] = $deal[$dealType]['custom_company_name'];

        $startDate = $deal[$dealType]['start_date'];
        $endDate = $deal[$dealType]['end_date'];
        $timeZone = substr(strftime(date("c")), -6);

        $og_data['cXenseParse:recs:publishtime'] = str_replace(" ", "T", strftime(date("Y-m-d H:i:s.000", strtotime($startDate)))) . $timeZone;
        $og_data['cXenseParse:recs:expirationtime'] = str_replace(" ", "T", strftime(date("Y-m-d H:i:s.000", strtotime($endDate)))) . $timeZone;

        $og_data['cXenseParse:articleid'] = $deal[$dealType]['id'];
        $og_data['cXenseParse:recs:articleType'] = "oferta";
    }

    private function getCityForView($deal, $dealType = 'Deal') {
        if ($dealType == 'Deal') {
            return new WebCityObjectComponent($deal['City']['id'], $deal['City']['name'], $deal['City']['slug']);
        } else {
            $dealCompany = $deal['NowDeal']['Company'];
            $city = $this->WebCity->findById($dealCompany['city_id']);
            return new WebCityObjectComponent($city['WebCity']['id'], $city['WebCity']['name'], $city['WebCity']['slug']);
        }
    }

    private function getCompanyForView($deal, $dealType = 'Deal') {
        if ($dealType == 'Deal') {
            $dealCompany = $deal['Company'];
        } else {
            $dealCompany = $deal['NowDeal']['Company'];
        }
        $cityOfCompany = $this->WebCity->findById($dealCompany['city_id']);
        $name = $dealCompany['name'];
        $city = $cityOfCompany['WebCity']['name'];
        $address1 = !empty($deal['Deal']['custom_company_address1']) ? $deal['Deal']['custom_company_address1'] : $dealCompany['address1'];
        $state = $deal['Deal']['custom_company_state'];
        $zip = !empty($deal['Deal']['custom_company_zip']) ? $deal['Deal']['custom_company_zip'] : $dealCompany['zip'];
        $url = $dealCompany['url'];
        $logo = $dealCompany['logo_image'];
        $showMap = $deal['Deal']['show_map'];
        return new WebCompanyObjectComponent($name, $city, $address1, $state, $zip, $url, $logo, $showMap);
    }

    private function getOrderedDeals($conditions, $limit) {
        $deals = $this->ApiDeal->findAllForApi($conditions, $limit, 1);
        $dealsSize = count($deals);
        for ($i = 0; $i < $dealsSize; $i++) {
            $deals[$i] = $this->addDealsExtraAttributes($deals[$i]);
        }
        return $deals;
    }

    private function getOrderedBanners($cityId = null, $type_id = 3) {
        return $this->WebSearchLinks->findAllByCityIdAndTypeId($cityId, $type_id);
    }

    public function getPriorityDealForCity($priority, $cityID) {
        $conditions = array();
        $conditions['City.id'] = $cityID;
        $deals = $this->getOrderedDeals($conditions, $priority);
        $dealForView = null;
        if ($deals[$priority - 1]) {
            $dealForView = $this->getDealForView($deals[$priority - 1]);
        }
        return $dealForView;
    }

    public function getListDealForCity($quantity, $cityID, $blackListIDs = array(
    )) {
        $conditions = array();
        $conditions['City.id'] = $cityID;
        $qt = $quantity + count($blackListIDs);
        $deals = $this->getOrderedDeals($conditions, $qt);
        $dealList = null;
        if ($deals) {
            $dealList = array();
            $dealsAdded = 0;
            $i = 0;
            while ($dealsAdded < $quantity && $deals[$i] != null) {
                $dealforView = $this->getDealForView($deals[$i]);
                $i++;
                if (!in_array($dealforView->id, $blackListIDs)) {
                    $dealList[] = $dealforView;
                    $dealsAdded++;
                }
            }
        }
        return $dealList;
    }

    public function getListBannerForCity($cityID) {
        $banners = $this->getOrderedBanners($cityID);
        $bannerList = null;
        if ($banners) {
            $bannerList = array();
            $i = 0;
            while ($banners[$i] != null) {
                $bannnerforView = $this->getBannerForView($banners[$i]);
                $bannerList[] = $bannnerforView;
                $i++;
            }
        }
        return $bannerList;
    }

    public function getListDealForCCNow($nowDeals) {
        $dealList = array();
        if ($nowDeals) {
            foreach ($nowDeals as $nowDeal) {
                $dealforView = $this->getDealForView($nowDeal, 'NowDeal');
                $dealList[] = $dealforView;
            }
        }
        return $dealList;
    }

    public function getListDealsFromSolr($resultsSolr, $actualPage) {
        $respArray = array();
        $cont = 1;
        foreach ($resultsSolr->response->docs as $doc) {
            if ($actualPage == 1 && $cont == 1) {
                $this->dimensionImage = 'search_first_element';
            } else {
                $this->dimensionImage = 'searchdeals_big_thumb';
            }
            $deal = array();
            $deal['Deal']['id'] = $doc->deal_id;
            $deal['Deal']['name'] = $doc->deal_name;
            $deal['Deal']['subtitle'] = $doc->deal_subtitle;
            $deal['Deal']['description'] = $doc->deal_description;
            $deal['Deal']['original_price'] = $doc->deal_original_price;
            $deal['Deal']['discounted_price'] = $doc->deal_discounted_price;
            $deal['Deal']['discount_percentage'] = $doc->deal_discount_percentage;
            $deal['Deal']['end_date'] = $doc->deal_end_date;
            $deal['Deal']['only_price'] = $doc->deal_only_price;
            $deal['Deal']['hide_price'] = $doc->deal_hide_price;
            $deal['Deal']['is_purchasable'] = $doc->deal_is_purchasable;
            $deal['Deal']['slug'] = $doc->deal_slug;
            $deal['Deal']['is_tourism'] = $doc->deal_is_tourism;
            $deal['Company']['city_id'] = $doc->company_city_id;
            $deal['Company']['name'] = $doc->company_name;
            $respArray[] = $this->getDealForView($deal);
            $cont++;
        }
        return $respArray;
    }

}
