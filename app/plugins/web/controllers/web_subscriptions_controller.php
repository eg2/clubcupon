<?php

App::import('Vendor', Configure::read('facebook_lib.path'));
App::import('Component', 'web.WebUserFacebookLogin');
App::import('Model', 'Deal');
App::import('Component', 'api.ApiSubscriptionsService');
App::import('Component', 'web.WebSubscriptionMailConfirmation');
App::import('Component', 'web.WebSubscriptionMailValidatorService');
App::import('Component', 'event.EventImportEmtEventService');

class WebSubscriptionsController extends WebAppController {

    public $name = 'WebCitiesController';
    private $ipClient;
    private $email;
    private $user;
    private $city;
    private $message;
    private $facebookLogin;
    private $sourceEmail;
    private $campaignInfo;
    private $webSubscriptionMail;
    private $forwardDeal;
    private $WebSubscriptionMailValidatorService;
    private $enableValidationMailDns;
    private $captcha;
    public $uses = array(
        'web.WebSubscriptions',
        'web.WebUser',
        'web.WebSubscriptionCandidates',
        'web.WebDeal'
    );

    function __construct() {
        parent::__construct();
        $this->facebookLogin = new WebUserFacebookLoginComponent();
        $this->webSubscriptionMail = new WebSubscriptionMailConfirmationComponent();
        $this->WebSubscriptionMailValidatorService = new WebSubscriptionMailValidatorServiceComponent();
        $this->enableValidationMailDns = Configure::read('web.enableSubscriptionValidationMailDns');
        $this->ApiSubscriptionsService = new ApiSubscriptionsServiceComponent();
    }

    private function loadUserData() {
        if ($this->Auth->user()) {
            $us = $this->Auth->user();
            $this->user['WebUser']['id'] = $us['User']['id'];
        } else {
            $this->user = null;
        }
        $this->ipClient = $this->RequestHandler->getClientIP();
    }

    private function getFormUrlParam($key) {
        if (!is_null($this->getFormParam($key))) {
            return $this->getFormParam($key);
        }
        if (!is_null($this->getUrlParam($key))) {
            return $this->getUrlParam($key);
        }
        return null;
    }

    function beforeFilter() {
        $this->Auth->allow('addsubscriptions');
        $this->loadSubscriptionData();
        $this->loadUserData();
        $this->getCampaignInfoParams();
        $this->getForwardDeal();
    }

    private function loadSubscriptionData() {
        if (!is_null($this->getFormUrlParam('SubscriptionEmail'))) {
            $this->email = $this->getFormUrlParam('SubscriptionEmail');
        }
        if (!is_null($this->getFormUrlParam('SubscriptionCitySlug'))) {
            $this->city = $this->WebCity->findBySlug($this->getFormUrlParam('SubscriptionCitySlug'));
        } else {
            $this->city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
        }
        if (!empty($_GET['selectedCity'])) {
            $this->city = $this->WebCity->findBySlug($_GET['selectedCity']);
        }
        if (!is_null($this->getUrlParam('sourceEmail')) && ($this->getUrlParam('sourceEmail') == 'true')) {
            $this->sourceEmail = true;
        } else {
            $this->sourceEmail = false;
        }
        if (!is_null($this->getFormUrlParam('SubscriptionCaptcha'))) {
            $this->captcha = $this->getFormUrlParam('SubscriptionCaptcha');
        }
    }

    private function getForwardDeal() {
        if (!is_null($this->getFormUrlParam('forward_deal_id'))) {
            $this->forwardDeal = $this->WebDeal->findById($this->getFormUrlParam('forward_deal_id'));
        }
    }

    private function getCampaignInfoParams() {
        $campaignInfo = array();
        if (!is_null($this->getFormUrlParam('campaign'))) {
            $campaignInfo['campaign'] = $this->getFormUrlParam('campaign');
        }
        if (!is_null($this->getFormUrlParam('utm_source'))) {
            $campaignInfo['utm_source'] = $this->getFormUrlParam('utm_source');
        }
        if (!is_null($this->getFormUrlParam('utm_mediun'))) {
            $campaignInfo['utm_mediun'] = $this->getFormUrlParam('utm_medium');
        }
        if (!is_null($this->getFormUrlParam('utm_term'))) {
            $campaignInfo['utm_term'] = $this->getFormUrlParam('utm_term');
        }
        if (!is_null($this->getFormUrlParam('utm_content'))) {
            $campaignInfo['utm_content'] = $this->getFormUrlParam('utm_content');
        }
        if (!is_null($this->getFormUrlParam('utm_campaign'))) {
            $campaignInfo['utm_campaign'] = $this->getFormUrlParam('utm_campaign');
        }
        if (count($campaignInfo) == 0) {
            $this->campaignInfo = null;
        } else {
            $this->campaignInfo = $campaignInfo;
        }
    }

    protected function setEventSubscription($email) {
        $eventService = new EventImportEmtEventServiceComponent();
        $eventService->eventSubscriptionService($email);
    }

    protected function subscribeConfirm() {
        $actualSubscription = $this->WebSubscriptions->findByEmailAndCityId($this->email, $this->city['WebCity']['id']);
        if ($actualSubscription == null) {
            if ($this->WebSubscriptions->subscribe($this->email, $this->city, $this->ipClient, $this->user, $this->campaignInfo)) {
                $this->message = Configure::read('web.message.subscriptionOK');
                $this->setEventSubscription($this->email);
            } else {
                $this->message = Configure::read('web.message.subscriptionExist');
            }
        } else {
            $actualSubscription['WebSubscriptions']['is_subscribed'] = 1;
            if ($this->WebSubscriptions->update($actualSubscription)) {
                $this->message = Configure::read('web.message.subscriptionOK');
                $this->setEventSubscription($this->email);
            } else {
                $this->message = 'Hubo un problema al activar la suscripcion.';
            }
        }
    }

    protected function subscribeCandidate() {
        $actualSubscription = $this->WebSubscriptions->findByEmailAndCityId($this->email, $this->city['WebCity']['id']);
        $actualSubscriptionCandidate = $this->WebSubscriptionCandidates->findByEmailAndCityId($this->email, $this->city['WebCity']['id']);
        if ($actualSubscription == null || $actualSubscription['WebSubscription']['is_subscribed'] == 0) {
            if ($actualSubscriptionCandidate == null) {
                if (!$this->WebSubscriptionCandidates->subscribe($this->email, $this->city, $this->ipClient, $this->user, $this->campaignInfo)) {
                    $this->message = Configure::read('web.message.subscriptionExist');
                    return false;
                }
            }
            $this->message = Configure::read('web.message.subscriptionMail');
            $this->webSubscriptionMail->sendConfirmationMail($this->email, $this->city, $this->user, $this->campaignInfo, $this->forwardDeal);
        } else {
            $this->message = Configure::read('web.message.subscriptionExist');
            return false;
        }
    }

    function validateDomainMailDns($email) {
        if ($this->enableValidationMailDns) {
            return $this->WebSubscriptionMailValidatorService->verifyMailDomain($email);
        } else {
            return true;
        }
    }

    protected function subscribe() {
        if ($this->validateCaptcha()) {
            if ($this->validateDomainMailDns($this->email)) {
                try {
                    if (Configure::read('web.subscriptionMail.enable') == true) {
                        if (!$this->sourceEmail) {
                            $this->subscribeCandidate();
                        } else {
                            $this->subscribeConfirm();
                        }
                    } else {
                        $this->subscribeConfirm();
                    }
                } catch (Exception $e) {
                    $this->message = $e->getMessage();
                }
            } else {
                $this->message = Configure::read('web.message.subscriptionFailValidationMail');
            }
        } else {
            $this->message = Configure::read('web.message.subscriptionFailValidationCaptcha');
        }
    }

    private function enableTrackerSubscription() {
        if ($this->message == 'Te suscribiste con éxito a Club Cupón.') {
            $this->viewParams['subscriptionOk'] = true;
        }
    }

    private function redirectBeforeSubscribe() {
        if (!is_null($this->forwardDeal) && $this->sourceEmail) {
            $this->redirect('/' . $this->params['named']['city_slug'] . Configure::read('web.extraDealURLPath') . $this->forwardDeal['WebDeal']['slug']);
        } else {
            $this->redirect('/' . $this->city['WebCity']['slug']);
        }
    }

    function addsubscriptions() {
        if (!$this->detectFacebookLogin()) {
            $this->subscribe();
        } else {
            $this->facebookSubscription();
        }
        $this->enableTrackerSubscription();
        $this->Session->setFlash($this->message);
        $this->redirectBeforeSubscribe();
    }

    function validateCaptcha() {
        $valid = false;
        if (isset($this->captcha)) {
            $valid = $this->isValidCaptcha();
        } else {
            $valid = !$this->ApiSubscriptionsService->isCaptchaNecessary($this->ipClient);
        }
        return $valid;
    }

    private function isValidCaptcha() {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';

        $img = new Securimage();
        return $img->check($this->captcha);
    }

    protected function facebookSubscription() {
        if ($this->detectFacebookLogin()) {
            $fb_user = $this->facebookUser();
            if ($fb_user) {
                $User = $this->facebookLogin->getUser($fb_user);
                if ($User) {
                    $this->user['WebUser']['id'] = $User['User']['id'];
                } else {
                    $this->user = null;
                }
                $this->email = $fb_user['email'];
                $this->subscribe();
            } else {
                $this->message = 'Hubo un problema al conectarse a Facebook.';
            }
        }
    }

    protected function facebookUser() {
        $appId = Configure::read('facebook.fb_api_key');
        $appSecret = Configure::read('facebook.fb_secrect_key');
        try {
            $this->facebook = new Facebook(array(
                'appId' => $appId,
                'secret' => $appSecret,
                'cookie' => true
            ));
            $fb_user = $this->facebook->api('/me?fields=id,email,first_name,middle_name,last_name,about');
            return $fb_user;
        } catch (Exception $e) {
            return null;
        }
    }

    protected function detectFacebookLogin() {
        $resp = false;
        if ($this->params['url']['isFacebookLogin']) {
            $resp = true;
        }
        if ($this->params['url']['required_facebook_login']) {
            $resp = true;
        }
        return $resp;
    }

}
