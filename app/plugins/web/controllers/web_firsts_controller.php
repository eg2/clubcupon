<?php

App::import('Component', 'api.ApiSubscriptionsService');

class WebFirstsController extends WebAppController {

    private $isCaptchaNecesary;

    function beforeFilter() {
        $this->Auth->allow('homepopup', 'ads');
    }

    function __construct() {
        parent::__construct();
        $this->apiSubscription = new ApiSubscriptionsServiceComponent();
    }

    function homepopup() {
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
        $this->layout = null;
        $this->isCaptchaNecesary = $this->apiSubscription->isCaptchaNecessary($this->RequestHandler->getClientIP());
        $this->set('isCaptchaNecesary', $this->isCaptchaNecesary);
        $this->render('/clubcupon/homepopup');
    }

    private function getAdsData() {
        $this->viewParams['id-ad'] = $this->getUrlParam('id-ad');
        $this->viewParams['width-ad'] = $this->getUrlParam('width-ad');
        $this->viewParams['height-ad'] = $this->getUrlParam('height-ad');
        $this->viewParams['idAviso'] = $this->getUrlParam('idAviso');
        $this->viewParams['isTourism'] = $this->getUrlParam('isTourism');
        $this->viewParams['idIframe'] = $this->getUrlParam('idIframe');
        $this->viewParams['typeAd'] = $this->getUrlParam('typeAd');
    }

    function ads() {
        $this->getAdsData();
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::ADS_ALL_CC;
        $this->layout = null;
        $this->render('/clubcupon/ads');
    }

}
