<?php

App::import('Model', 'web.WebDeal');
App::import('Component', 'api.ApiBuyService');
App::import('Component', 'api.ApiSubscriptionsService');

class WebDealsController extends WebAppController {

    const SOURCE_DESCUENTO_CITY = 'descuentocity.com';

    public $name = 'WebDealsController';
    private $WebDeal;
    private $ApiBuyService;
    private $isCaptchaNecesary;

    function __construct() {
        parent::__construct();
        $this->WebDeal = new WebDeal();
        $this->ApiBuyService = new ApiBuyServiceComponent();
        $this->apiSubscription = new ApiSubscriptionsServiceComponent();
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('deal');
    }

    public function deal() {
        $deal = $this->WebDeal->findBySlug($this->params['named']['deal_Slug']);
        if ($this->WebDeal->isValidDeal($deal)) {
            $this->isCaptchaNecesary = $this->apiSubscription->isCaptchaNecessary($this->RequestHandler->getClientIP());
            $this->set('isDescuentoCityRequest', $this->isDescuentoCityRequest());
            $this->set('descuentoCityUtmRef', $this->utmRef());
            $this->set('isCaptchaNecesary', $this->isCaptchaNecesary);
            if ($this->isCorporateCity()) {
                if ($this->isPinValid()) {
                    $this->viewParams['dealSlug'] = $this->params['named']['deal_Slug'];
                    $this->viewParams['viewType'] = WebViewDataCreatorComponent::DEAL_DETAIL;
                    $this->layout = 'exclusive';
                    $this->render('/exclusives/deal');
                } else {
                    $this->redirect(Configure::read('web.firstLevelURLPath') . 'login/' . $this->isCityUDN());
                }
            } else {
                if ($this->isNotPurchasableAndHasRedirect($deal)) {
                    $this->redirect($deal['WebDeal']['redirect_url']);
                } else {
                    $this->viewParams['dealSlug'] = $this->params['named']['deal_Slug'];
                    $this->viewParams['viewType'] = WebViewDataCreatorComponent::DEAL_DETAIL_CC;
                    $this->layout = 'clubcupon';
                    $this->render('/clubcupon/deal');
                }
            }
        } else {
            $this->cakeError('error404');
        }
    }

    protected function isNotPurchasableAndHasRedirect($deal) {
        return !$this->ApiBuyService->isPurchasable(array(
                    'Deal' => $deal['WebDeal']
                )) && $this->Auth->user('user_type_id') != ConstUserTypes::Company && !empty($deal['WebDeal']['redirect_url']);
    }

    private function isDescuentoCityRequest() {
        return $this->params['url']['utm_source'] == self::SOURCE_DESCUENTO_CITY;
    }

    function utmRef() {
        $ref = false;
        if (isset($this->params['url']['utm_ref'])) {
            $ref = $this->params['url']['utm_ref'];
        }
        return $ref;
    }

}
