<?php

class WebCitiesController extends WebAppController {

    public $name = 'WebCitiesController';

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('home', 'todos');
    }

    public function home() {
        if ($this->isCorporateCity()) {
            if ($this->isPinValid()) {
                $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_HOME;
                $this->layout = 'exclusive';
                $this->render('/exclusives/home');
            } else {
                $this->redirect(Configure::read('web.firstLevelURLPath') . 'login/' . $this->isCityUDN());
            }
        } else {
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_HOME_CC;
            $this->layout = 'clubcupon';
            $this->render('/clubcupon/home');
        }
    }

    public function todos() {
        if ($this->isCorporateCity()) {
            if ($this->isPinValid()) {
                $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_ALL;
                $this->layout = 'exclusive';
                $this->render('/exclusives/all');
            } else {
                $this->redirect(Configure::read('web.firstLevelURLPath') . 'login/' . $this->isCityUDN());
            }
        } else {
            $this->viewParams['viewType'] = WebViewDataCreatorComponent::CITY_ALL_CC;
            $this->layout = 'clubcupon';
            $this->render('/clubcupon/all');
        }
    }

}
