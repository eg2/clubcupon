<?php

class WebExclusiveLoginsController extends WebAppController {

    public $name = 'WebExclusiveLoginsController';
    public $layout = 'exclusiveLogin';
    private $isUDN = 0;

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login');
    }

    public function login($isUDN = 0) {
        $this->isUDN = $isUDN;
        $pin = $this->params['form']['pin'];
        if ($pin) {
            if ($this->WebCorporateGuest->isValidCorporatePin($pin)) {
                $cityObj = $this->getCityToAccess($pin, $isUDN);
                if ($cityObj) {
                    if ($this->isValidPinForCity($pin, $cityObj)) {
                        if ($this->Cookie->read('fromURL')) {
                            $path = '/' . $this->Cookie->read('fromURL');
                        } else {
                            $path = '/' . $cityObj['WebCity']['slug'];
                        }
                        $this->cleanCookies();
                        $this->Cookie->write('PIN', $pin, true, Configure::read('web.cookie.time'));
                        $this->Cookie->write('CORP_city_slug', $cityObj['WebCity']['slug'], false, Configure::read('web.cookie.time'));
                        $this->redirect($path);
                    }
                }
                $mesg = Configure::read('web.messages.PIN_Not_Valid');
                $this->viewMessage = $mesg[$isUDN];
            } else {
                $mesg = Configure::read('web.messages.PIN_Not_Found');
                $this->viewMessage = $mesg[$isUDN];
            }
        } else if ($this->Cookie->read('Message')) {
            $this->viewMessage = $this->Cookie->read('Message');
        }
        $this->detectFromCitySlug();
        $this->viewParams['viewType'] = WebViewDataCreatorComponent::BASE_VIEW;
        $this->render('/exclusives/login');
    }

    private function detectFromCitySlug() {
        if ($this->Cookie->read('fromCitySlug')) {
            $this->params['named']['city_slug'] = $this->Cookie->read('fromCitySlug');
        }
    }

    private function getCityToAccess($pin, $isUDN) {
        $cityObj = null;
        if ($this->Cookie->read('fromCitySlug')) {
            $cSlug = $this->Cookie->read('fromCitySlug');
            $cityObj = $this->WebCity->findBySlug($cSlug);
        } else {
            $corpGuest = $this->WebCorporateGuest->findByPin($pin);
            $pinID = $corpGuest['WebCorporateGuest']['id'];
            $corpGC = $this->WebCorporateGuestCity->findByPinIdAndUDN($pinID, $isUDN);
            if ($corpGC) {
                $cityID = $corpGC['WebCorporateGuestCity']['city_id'];
                $cityObj = $this->WebCity->findById($cityID);
            }
        }
        return $cityObj;
    }

    function beforeRender() {
        parent::beforeRender();
        $dataObj = json_decode($this->viewVars['data']);
        if ($this->isUDN) {
            $dataObj->city->isBusinessUnit = 1;
        } else {
            $dataObj->city->isBusinessUnit = 0;
        }
        $this->viewVars['data'] = json_encode($dataObj);
    }

}
