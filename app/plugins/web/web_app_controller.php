<?php

if (Configure::read('app.rDebug.enable')) {
    App::import('Vendor', 'web.ref');
}
App::import('Vendor', 'web.commons');
App::import('Component', 'web.WebUserLoginRemember');

class WebAppController extends AppController {

    public $logFileNamePrefix = "web";
    protected $viewParams = array();
    protected $viewMessage = '';
    protected $codeMessage;
    protected $userLoginRemember;
    public $uses = array(
        'web.WebCity',
        'web.WebCorporateGuest',
        'web.WebCorporateGuestCity'
    );
    public $components = array(
        'Auth',
        'Cookie',
        'WebViewDataCreator'
    );

    function __construct() {
        parent::__construct();
        $this->userLoginRemember = new WebUserLoginRememberComponent();
    }

    function __mergeVars() {
        parent::__mergeVars();
        unset($this->components['UserMenu']);
        unset($this->components['Pdf']);
    }

    protected function isCityUDN() {
        $city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
        return $city['WebCity']['is_business_unit'];
    }

    protected function isCorporateCity() {
        $city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
        return $city['WebCity']['is_corporate'];
    }

    private function isSlugValid($slug) {
        $resp = true;
        if ($this->Cookie->read('CORP_city_slug')) {
            if ($slug != $this->Cookie->read('CORP_city_slug')) {
                $resp = false;
            }
        }
        return $resp;
    }

    protected function isPinValid() {
        $pin = $this->Cookie->read('PIN');
        if ($pin && $this->isSlugValid($this->params['named']['city_slug'])) {
            if ($this->WebCorporateGuest->isValidCorporatePin($pin)) {
                $city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
                if ($this->isValidPinForCity($pin, $city)) {
                    $this->cleanCookies();
                    return true;
                }
                $mesg = Configure::read('web.messages.PIN_Not_Valid');
                $this->Cookie->write('Message', $mesg[$city[WebCity][is_business_unit]]);
            } else {
                $city = $this->WebCity->findBySlug($this->params['named']['city_slug']);
                $mesg = Configure::read('web.messages.PIN_Not_Found');
                $this->Cookie->write('Message', $mesg[$city[WebCity][is_business_unit]]);
            }
        } else {
            $this->Cookie->write('Message', '');
        }
        $this->Cookie->write('fromURL', $this->params['url']['url']);
        $this->Cookie->write('fromCitySlug', $this->params['named']['city_slug']);
        return false;
    }

    protected function isValidPinForCity($pin, $city) {
        $corpGuest = $this->WebCorporateGuest->findByPin($pin);
        $pinID = $corpGuest['WebCorporateGuest']['id'];
        $cityID = $city['WebCity']['id'];
        $isValid = $this->WebCorporateGuestCity->isValidPinForCity($pinID, $cityID);
        return ($isValid && $city['WebCity']['is_corporate']);
    }

    protected function cleanCookies() {
        $this->Cookie->del('fromURL');
        $this->Cookie->del('fromCitySlug');
        $this->Cookie->del('Message');
    }

    protected function updateGralCookie() {
        if ($this->params['named']['city_slug']) {
            $this->Cookie->write('city_slug', $this->params['named']['city_slug'], false);
            $wCity = $this->WebCity->findBySlug($this->params['named']['city_slug']);
            if ($wCity && !$wCity['WebCity']['is_group']) {
                $this->Cookie->write('geo_city_slug', $this->params['named']['city_slug'], false);
            }
        }
    }

    function beforeFilter() {
        $this->userLoginRemember->autoLogin($this->Auth, $this->Cookie);
        $this->setFirstUserAndShowPopUp();
        $this->setTrackingRegisterOk();
    }

    private function setFirstUserAndShowPopUp() {
        $this->viewParams['EnablePopup'] = false;
        if (isset($_GET['popup'])) {
            if ($_GET['popup'] == 'si') {
                $this->viewParams['EnablePopup'] = true;
                $this->setFirstUser(true);
            }
            if ($_GET['popup'] == 'no') {
                $this->viewParams['EnablePopup'] = false;
                $this->setFirstUser(false);
            }
        } else {
            $this->setFirstUser(true);
        }
    }

    private function setTrackingRegisterOk() {
        $this->viewParams['registerOk'] = false;
        if (isset($_GET['popup'])) {
            if ($_GET['registerOk'] == '1') {
                $this->viewParams['registerOk'] = true;
            }
            if ($_GET['registerOk'] == '0') {
                $this->viewParams['registerOk'] = false;
            }
        }
    }

    private function setFirstUser($boolPopup) {
        if (!($this->Cookie->read('first_time_user'))) {
            $this->Cookie->write('first_time_user', 1, true, Configure::read('web.firstUser.cookieTime'));
            $this->viewParams['EnablePopup'] = $boolPopup;
        }
    }

    function addJavascriptVarsForFacebook() {
        if ($this->Auth->user('fb_user_id') || (!$this->Auth->user()) && Configure::read('facebook.is_enabled_facebook_connect')) {
            $this->js_vars['cfg']['api_key'] = Configure::read('facebook.fb_api_key');
            $this->js_vars['cfg']['path_absolute'] = Router::url('/', true);
        }
        $this->set('js_vars_facebook', (isset($this->js_vars)) ? $this->js_vars : '');
    }

    function beforeRender() {
        $this->addJavascriptVarsForFacebook();
        $this->updateGralCookie();
        $this->viewParams['citySlug'] = $this->params['named']['city_slug'];
        $this->viewParams['lastGeoCitySlug'] = $this->Cookie->read('geo_city_slug');
        $this->viewParams['userPIN'] = $this->Cookie->read('PIN');
        $this->viewParams['userType'] = $this->Auth->user('user_type_id');
        $this->viewParams['userID'] = $this->Auth->user('id');
        $this->viewParams['userEmail'] = $this->Auth->user('email');
        $this->viewParams['message'] = $this->viewMessage;
        $this->viewParams['codeMessage'] = $this->codeMessage;
        $this->viewParams['url'] = $this->params['url']['url'];
        $data = $this->WebViewDataCreator->createData($this->viewParams);
        if (empty($data)) {
            $this->cakeError('error404');
        }
        $this->set('data', json_encode($data));
        parent::beforeRender();
    }

    public function getFormParam($key) {
        return $this->getParam($key, 'form');
    }

    public function getUrlParam($key) {
        if (isset($_GET[$key])) {
            return $_GET[$key];
        }
        return null;
    }

    public function getParam($key, $parentKey) {
        if (isset($this->params[$parentKey][$key])) {
            return $this->params[$parentKey][$key];
        }
        return null;
    }

}
