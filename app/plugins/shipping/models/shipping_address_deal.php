<?php

App::import('Model', 'api.ApiCompany');

class ShippingAddressDeal extends ShippingAppModel {

    public $name = 'ShippingAddressDeal';
    public $alias = 'ShippingAddressDeal';
    public $useTable = 'shipping_address_deals';
    public $actsAs = array(
        'WhoDidIt' => array()
    );
    public $belongsTo = array(
        'ShippingAddress' => array(
            'className' => 'shipping.ShippingAddress',
            'foreignKey' => 'shipping_address_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Deal' => array(
            'className' => 'api.ApiDeal',
            'foreignKey' => 'deal_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    private $fields = array(
        'ShippingAddress.id',
        'ShippingAddress.street',
        'ShippingAddress.number',
        'ShippingAddress.floor_number',
        'ShippingAddress.apartment',
        'ShippingAddress.details'
    );

    function findAddressesListByDealId($dealId) {
        $shippingAddresses = $this->find('all', array(
            'conditions' => array(
                'ShippingAddressDeal.deal_id =' => $dealId,
                'ShippingAddressDeal.deleted =' => 0
            ),
            'contain' => array(
                'ShippingAddress' => array(
                    'City',
                    'Neighbourhood'
                ),
                'ShippingAddressDeal'
            ),
            'order' => 'ShippingAddress.custom_address',
            'recursive' => 2
        ));
        $addressesList = array();
        foreach ($shippingAddresses as $shippingAddress) {
            $addressesList[$shippingAddress['ShippingAddress']['id']] = ShippingAddress::getLabelAddressToShow($shippingAddress);
        }
        return $addressesList;
    }

    function findAddressesByDealId($dealId, $includeCc = false) {
        $conditions = array();
        if (!$includeCc) {
            $company = new ApiCompany();
            $company = $company->findBySlugForApi('club-cupon');
            $conditions = array(
                'ShippingAddressDeal.deal_id =' => $dealId,
                'ShippingAddress.company_id <>' => $company['Company']['id']
            );
        } else {
            $conditions = array(
                'ShippingAddressDeal.deal_id =' => $dealId
            );
        }
        return $this->find('list', array(
                    'conditions' => $conditions,
                    'fields' => array(
                        'ShippingAddress.id'
                    ),
                    'recursive' => 1
        ));
    }

    function findAddressesCcByDealId($dealId) {
        $conditions = array();
        $company = new ApiCompany();
        $company = $company->findBySlugForApi('club-cupon');
        $conditions = array(
            'ShippingAddressDeal.deal_id =' => $dealId,
            'ShippingAddress.company_id =' => $company['Company']['id']
        );
        return $this->find('list', array(
                    'conditions' => $conditions,
                    'fields' => array(
                        'ShippingAddress.id'
                    ),
                    'recursive' => 1
        ));
    }

    function delMany($dealId) {
        $conditions = array(
            'ShippingAddressDeal.deal_id =' => $dealId
        );
        $shippingAddressDeals = $this->find('list', array(
            'conditions' => $conditions,
            'fields' => array(
                'ShippingAddressDeal.id'
            ),
            'recursive' => 0
        ));
        foreach ($shippingAddressDeals as $id) {
            $this->del($id);
        }
    }

    function delManyByShippingAddress($shippingAddressId) {
        $conditions = array(
            'ShippingAddressDeal.shipping_address_id =' => $shippingAddressId
        );
        $shippingAddressDeals = $this->find('list', array(
            'conditions' => $conditions,
            'fields' => array(
                'ShippingAddressDeal.id'
            ),
            'recursive' => 0
        ));
        foreach ($shippingAddressDeals as $id) {
            $this->del($id);
        }
    }

    function delManyFromSubdeals($dealId) {
        App::import('Model', 'api.ApiDeal');
        $this->ApiDeal = & new ApiDeal();
        $deal = array();
        $deal['Deal']['id'] = $dealId;
        $subDeals = $this->ApiDeal->findAllSubDealsForApi($deal);
        foreach ($subDeals as $subDeal) {
            $this->delMany($subDeal['Deal']['id']);
        }
    }

    function associateMany($dealId, $data) {
        $result = true;
        $shipping_addresses_id = $data['ShippingAddressDeal']['shipping_address_id'];
        $shipping_addressescc_id = $data['ShippingAddressDeal']['shipping_addresscc_id'];
        $dataAll = array();
        foreach ($shipping_addresses_id as $id) {
            $data1 = array();
            $data1['ShippingAddressDeal']['shipping_address_id'] = $id;
            $data1['ShippingAddressDeal']['deal_id'] = $dealId;
            $dataAll[] = $data1;
        }
        foreach ($shipping_addressescc_id as $cc_id) {
            $data2 = array();
            $data2['ShippingAddressDeal']['shipping_address_id'] = $cc_id;
            $data2['ShippingAddressDeal']['deal_id'] = $dealId;
            $dataAll[] = $data2;
        }
        $this->saveAll($dataAll);
        return $result;
    }

    function saveMany($data) {
        $result = true;
        $shipping_addresses_id = $data['ShippingAddressDeal']['shipping_address_id'];
        foreach ($shipping_addresses_id as $shipping_address_id) {
            $data['ShippingAddressDeal']['shipping_address_id'] = $shipping_address_id;
            $this->create();
            if (!$this->save($data)) {
                $result = false;
                break;
            }
        }
        return $result;
    }

    function hasDealShippingAddressesByDealId($dealId) {
        $shippingAddressDeal = $this->findAllByDealId($dealId);
        return !empty($shippingAddressDeal);
    }

    function findAllByDealId($dealId) {
        $conditions = array(
            'ShippingAddressDeal.deal_id =' => $dealId,
            'ShippingAddressDeal.deleted =' => 0
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
