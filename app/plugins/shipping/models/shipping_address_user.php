<?php

class ShippingAddressUser extends ShippingAppModel {

    const VALIDATE_CITY_NAME = '/(\w|\s)+/i';
    const VALIDATE_STREET = '/(\w|\s)+/i';
    const VALIDATE_NUMBER = '/[0-9]+/';
    const VALIDATE_FLOOR_NUMBER = '/([a-z]|[0-9])*/i';
    const VALIDATE_APARTMENT = '/([a-z]|[0-9])*/i';
    const VALIDATE_POSTAL_CODE = '/([a-z][0-9]{4}[a-z]{3}|[0-9]{4})/i';
    const VALIDATE_TELEPHONE = '/[0-9]{7,13}/';
    const VALIDATE_DETAILS = '/(\w|\s)*/i';

    public $name = 'ShippingAddressUser';
    public $alias = 'ShippingAddressUser';
    public $useTable = 'shipping_address_users';
    public $actsAs = array(
        'api.SoftDeletable'
    );
    public $validate = array(
        'city_name' => array(
            'cityNameRule1' => array(
                'rule' => self::VALIDATE_CITY_NAME,
                'allowEmpty' => false,
                'message' => 'El campo solo puede contener letras, numeros o espacios'
            )
        ),
        'street' => array(
            'streetRule1' => array(
                'rule' => self::VALIDATE_STREET,
                'allowEmpty' => false,
                'message' => 'El campo solo puede contener letras, numeros o espacios'
            )
        ),
        'number' => array(
            'numberRule1' => array(
                'rule' => self::VALIDATE_NUMBER,
                'allowEmpty' => false,
                'message' => 'El campo solo puede contener numeros'
            )
        ),
        'floor_number' => array(
            'floorNumberRule1' => array(
                'rule' => self::VALIDATE_FLOOR_NUMBER,
                'allowEmpty' => true,
                'message' => 'El campo solo puede contener letras y numeros'
            )
        ),
        'apartment' => array(
            'apartmentRule1' => array(
                'rule' => self::VALIDATE_APARTMENT,
                'allowEmpty' => true,
                'message' => 'El campo solo puede contener letras y numeros'
            )
        ),
        'postal_code' => array(
            'postalCodeRule1' => array(
                'rule' => self::VALIDATE_POSTAL_CODE,
                'allowEmpty' => false,
                'message' => 'El campo solo admite un CPA valido o un codigo de 4 numeros'
            )
        ),
        'telephone' => array(
            'postalCodeRule1' => array(
                'rule' => self::VALIDATE_TELEPHONE,
                'allowEmpty' => true,
                'message' => 'El campo solo admite numeros de entre 7 y 13 digitos'
            )
        ),
        'details' => array(
            'detailsRule1' => array(
                'rule' => self::VALIDATE_DETAILS,
                'allowEmpty' => true,
                'message' => 'El campo solo puede contener letras, numeros o espacios'
            )
        )
    );

    function findById($id) {
        $conditions = array(
            'ShippingAddressUser.id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    private function dealExternalInnerJoin() {
        return array(
            array(
                'table' => 'deal_externals',
                'alias' => 'DealExternal',
                'type' => 'inner',
                'conditions' => array(
                    'DealExternal.id = ShippingAddressUser.deal_external_id'
                )
            )
        );
    }

    function findLastByUserId($userId) {
        $conditions = array(
            'DealExternal.user_id' => $userId
        );
        $joins = $this->dealExternalInnerJoin();
        $order = array(
            'ShippingAddressUser.id DESC'
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'order' => $order,
                    'recursive' => - 1
        ));
    }

}
