<?php

class ShippingAddress extends ShippingAppModel {

    var $name = 'ShippingAddress';
    public $alias = 'ShippingAddress';
    public $useTable = 'shipping_addresses';
    public $shippingManagedLogisticsName = 'managed_logistics';
    var $actsAs = array(
        'WhoDidIt' => array(),
        'api.SoftDeletable',
    );
    var $belongsTo = array(
        'City' => array(
            'className' => 'api.ApiCity',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Neighbourhood' => array(
            'className' => 'api.ApiNeighbourhood',
            'foreignKey' => 'neighbourhood_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'api.ApiCompany',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ShippingAddressType' => array(
            'className' => 'shipping.ShippingAddressType',
            'foreignKey' => 'shipping_address_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    var $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Campo Obligatorio'
        ),
        'country_id' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Campo Obligatorio'
        ),
        'state_id' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Campo Obligatorio'
        ),
        'street' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Campo Obligatorio'
        ),
        'number' => array(
            'rule' => 'numeric',
            'required' => true,
            'message' => 'Campo Obligatorio. Sólo se admiten números.'
        ),
        'postal_code' => array(
            'rule' => 'alphaNumeric',
            'required' => true,
            'message' => 'Campo Obligatorio. Sólo se admiten letras y números.'
        ),
        'fax_number' => array(
            'rule1' => array(
                'rule' => '_checkifIsNumeric',
                'allowEmpty' => true,
                'message' => 'Sólo se admiten números.'
            ),
        ),
        'custom_address' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Campo Obligatorio.'
        )
    );
    private $fields = array(
        'ShippingAddress.id',
        'ShippingAddress.street',
        'ShippingAddress.number',
        'ShippingAddress.floor_number',
        'ShippingAddress.apartment',
        'Neighbourhood.name',
        'City.name',
        'ShippingAddress.details',
        'ShippingAddress.custom_address',
    );

    function _checkifIsNumeric() {
        if (!empty($this->data['ShippingAddress']['fax_number'])) {
            return (is_numeric($this->data['ShippingAddress']['fax_number']));
        }
        return true;
    }

    public static function getLabelAddressToShow($shippingAddress) {
        $strShippingAddress = $shippingAddress['ShippingAddress']['custom_address'];
        $shippingAddress['ShippingAddress']['details'] = trim($shippingAddress['ShippingAddress']['details']);
        if (isset($shippingAddress['ShippingAddress']['details']) && !empty($shippingAddress['ShippingAddress']['details'])) {
            $strShippingAddress.= ' - ' . $shippingAddress['ShippingAddress']['details'] . '';
        }
        return $strShippingAddress;
    }

    function findByCompanyId($companyId) {
        $shippingAddresses = $this->find('all', array(
            'fields' => $this->fields,
            'conditions' => array(
                'ShippingAddress.company_id' => $companyId
            ),
            'contain' => array(
                'City',
                'Neighbourhood',
            ),
            'order' => 'ShippingAddress.custom_address',
            'recursive' => 1,
        ));
        $shpAddresses = Array();
        foreach ($shippingAddresses as $shippingAddress) {
            $shpAddresses[$shippingAddress['ShippingAddress']['id']] = ShippingAddress::getLabelAddressToShow($shippingAddress);
        }
        return $shpAddresses;
    }

    function findByCompanySlug($companySlug) {
        $shippingAddresses = $this->find('all', array(
            'conditions' => array(
                'Company.slug' => $companySlug
            ),
            'fields' => $this->fields,
            'order' => 'ShippingAddress.custom_address',
            'recursive' => 1
        ));
        $shpAddresses = Array();
        foreach ($shippingAddresses as $shippingAddress) {
            $shpAddresses[$shippingAddress['ShippingAddress']['id']] = ShippingAddress::getLabelAddressToShow($shippingAddress);
        }
        return $shpAddresses;
    }

    function getAddressToShow($id) {
        $shippingAddress = $this->find('first', array(
            'conditions' => array(
                'id' => $id
            ),
            'recursive' => 0
        ));
        return ShippingAddress::getLabelAddressToShow($shippingAddress);
    }

    function getAddressToShowInCoupon($id) {
        $shippingAddress = $this->find('first', array(
            'conditions' => array(
                'id' => $id
            ),
            'recursive' => 0
        ));
        $strShippingAddress = '';
        $shippingAddress['ShippingAddress']['link_bukeala'] = trim($shippingAddress['ShippingAddress']['link_bukeala']);
        if (isset($shippingAddress['ShippingAddress']['link_bukeala']) && !empty($shippingAddress['ShippingAddress']['link_bukeala'])) {
            $strShippingAddress.= $shippingAddress['ShippingAddress']['link_bukeala'] . '<br />';
            $strShippingAddress.= $shippingAddress['ShippingAddress']['custom_address'];
        } else {
            $strShippingAddress = $shippingAddress['ShippingAddress']['custom_address'];
        }
        $shippingAddress['ShippingAddress']['details'] = trim($shippingAddress['ShippingAddress']['details']);
        if (isset($shippingAddress['ShippingAddress']['details']) && !empty($shippingAddress['ShippingAddress']['details'])) {
            $strShippingAddress.= '<br />';
            $strShippingAddress.= $shippingAddress['ShippingAddress']['details'];
        }
        return $strShippingAddress;
    }

    function findById($id) {
        $conditions = array(
            'ShippingAddress.id' => $id
        );
        return $this->find('first', array(
                    'fields' => array(
                        'ShippingAddress.*',
                        'ShippingAddressType.name'
                    ),
                    'conditions' => $conditions,
                    'contain' => array(
                        'ShippingAddressType'
                    )
        ));
    }

    function hasStorehouseId($shippingAddress) {
        return $shippingAddress['ShippingAddress']['bac_storehouse_id'];
    }

    function isManagedLogistic($shippingAddress) {
        return $shippingAddress['ShippingAddressType']['name'] == $this->shippingManagedLogisticsName;
    }

}
