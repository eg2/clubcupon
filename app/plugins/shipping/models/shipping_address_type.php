<?php

class ShippingAddressType extends ShippingAppModel {

    public $name = 'ShippingAddressType';
    public $alias = 'ShippingAddressType';
    public $useTable = 'shipping_address_types';
    public $fields = array(
        'id',
        'label',
    );

    public function findAllForForm() {
        $shippingAddresses = $this->find('all', array(
            'fields' => $this->fields,
        ));
        $types = array();
        foreach ($shippingAddresses as $address) {
            $types[$address['ShippingAddressType']['id']] = $address['ShippingAddressType']['label'];
        }
        return $types;
    }

}
