<?php

App::import('Model', 'shipping.ShippingAddressUser');

class ShippingAddressUserTestShell extends Shell {

    const TEST_USER_ID = 839;
    const TEST_SHIPPING_ADDRESS_USER_ID = 25;

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ShippingAddressUser = new ShippingAddressUser();
        $this->data = array(
            'ShippingAddressUser' => array(
                'user_id' => 839,
                'country_id' => 284,
                'state_id' => 5439,
                'created_by' => 0,
                'city_name' => 'Ciudad Autónoma de Buenos Aires',
                'street' => 'Colonia',
                'number' => '170',
                'floor_number' => '',
                'apartment' => '',
                'postal_code' => 'C1437JND',
                'telephone' => '541149438700',
                'details' => '',
                'deal_external_id' => 653034
            )
        );
    }

    private function debug() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    private function setUp() {
        $this->debug();
        echo __METHOD__ . "\n";
    }

    private function tearDown() {
        echo __METHOD__ . "\n";
    }

    private function testDataShouldBeValidated() {
        echo __METHOD__ . "\n";
        $this->ShippingAddressUser->set($this->data);
        $errors = $this->ShippingAddressUser->invalidFields();
        if (!empty($errors)) {
            $show = json_encode($errors);
            echo "errors {$show}\n";
            throw new DomainException();
        }
    }

    private function testDataShouldBeSaved() {
        echo __METHOD__ . "\n";
        $result = $this->ShippingAddressUser->save($this->data);
        if (!$result) {
            throw new DomainException();
        }
    }

    private function testFieldShouldBeValidated() {
        echo __METHOD__ . "\n";
        if (!preg_match(ShippingAddressUser::VALIDATE_CITY_NAME, $this->data['ShippingAddressUser']['city_name'])) {
            throw new DomainException();
        }
    }

    private function testEmptyFieldShouldBeValidated() {
        echo __METHOD__ . "\n";
        if (preg_match(ShippingAddressUser::VALIDATE_CITY_NAME, "")) {
            throw new DomainException();
        }
    }

    private function testDataShouldBeFoundByUserId() {
        echo __METHOD__ . "\n";
        $data = $this->ShippingAddressUser->findLastByUserId(self::TEST_USER_ID);
        if (empty($data)) {
            throw new DomainException();
        }
        if (count($data) > 1) {
            throw new DomainException();
        }
    }

    public function main() {
        echo __METHOD__ . "\n";
        $this->setUp();
        $this->testFieldShouldBeValidated();
        $this->testEmptyFieldShouldBeValidated();
        $this->testDataShouldBeValidated();
        $this->testDataShouldBeSaved();
        $this->testDataShouldBeFoundByUserId();
        $this->tearDown();
    }

}
