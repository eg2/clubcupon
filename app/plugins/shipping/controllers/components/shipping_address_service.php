<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'shipping.ShippingAddress');
App::import('Model', 'shipping.ShippingAddressTypes');

class ShippingAddressServiceComponent extends ApiBaseComponent {

    public $shippingManagedLogisticsName = 'managed_logistics';

    public function __construct() {
        $this->ShippingAddress = ClassRegistry::init('ShippingAddress');
        $this->ShippingAddressType = ClassRegistry::init('ShippingAddressType');
    }

    public function isShippingAddressManagedLogistic($shippingAddress) {
        $shippingAddressType = 
                $this->ShippingAddressType->findById(
                        $shippingAddress['ShippingAddress']['shipping_address_type_id']);
        $shippingAddress = array_merge($shippingAddress, $shippingAddressType);        
        return $this->ShippingAddress->isManagedLogistic($shippingAddress);       
    }
    public function prepareData($data) {
      if (!$this->isShippingAddressManagedLogistic($data)) {        
            $data['ShippingAddress']['libertya_delivery_mode_id'] = null;
            $data['ShippingAddress']['bac_storehouse_id'] = null;            
      }
      return $data;
    }

}