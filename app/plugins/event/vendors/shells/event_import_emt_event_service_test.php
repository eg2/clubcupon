<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'event.EventImportEmtEventService');

class EventImportEmtEventServiceTestShell extends Shell {

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        Configure::load('email_files_generator_config');
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
        $this->ApiShellUtils = new ApiShellUtilsComponent();
    }

    function main() {
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
        $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
        $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->params = $this->ApiShellUtils->getOptions($this->args);
        $this->ApiLogger->debug("Iniciando Proceso " . __class__, array(
            'args' => $this->args,
            'params' => $this->params,
            'configurationsOverride' => array_keys($this->configurationsOverride),
            'configurationsOverrideValues' => $this->configurationsOverride,
            'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
            'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues
        ));
        $command = $this->params['c'];
        $this->setUp($this->params);
        $this->ApiLogger->notice('Run Command.', array(
            'command' => $command
        ));
        switch ($command) {
            case "test":
                $this->test();
                break;

            case "test_event_subscription_service":
                $email = $this->params['email'];
                $this->testEventSubscriptionService($email);
                break;

            default:
                echo "ERROR: Debe especificar un comando.\n";
                $this->printMethods();
                $this->ApiLogger->error("Debe especificar un comando.");
        }
    }

    public function printMethods() {
        $class_methods = get_class_methods(new EventImportEmtEventServiceTestShell());
        $this->ApiLogger->notice('Debe Especificar un Metodod', $class_methods);
        foreach ($class_methods as $method_name) {
            echo "\n $method_name \n";
        }
    }

    public function setUp($params) {
        $this->EventImportEmtEventService = new EventImportEmtEventServiceComponent();
        $this->EventHit = new EventHit();
    }

    public function test() {
        $this->ApiLogger->trace(__METHOD__ . "::BEGIN", array(
            'params' => $this->params
        ));
        $this->ApiLogger->trace(__METHOD__ . "::END", array(
            'params' => $this->params
        ));
    }

    public function testEventSubscriptionService($email) {
        $this->ApiLogger->trace(__METHOD__ . "::BEGIN", array(
            'params' => $this->params
        ));
        $result = $this->EventImportEmtEventService->eventSubscriptionService($email);
        $this->ApiLogger->trace(__METHOD__ . "::END", array(
            'params' => $this->params
        ));
    }

}
