<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'Event.EventImportEmtEventService');

class ImportEmtEventsShell extends Shell {

    const DEFAULT_TIME = 24;

    private $time = null;
    private $startDate = null;
    private $endDate = null;

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
        $this->ApiShellUtils = new ApiShellUtilsComponent();
        $this->EventImportEmtEventsService = new EventImportEmtEventServiceComponent();
    }

    private function initProcess() {
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
        $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
        $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->params = $this->ApiShellUtils->getOptions($this->args);
        $this->ApiLogger->debug("Iniciando Proceso " . __class__, array(
            'args' => $this->args,
            'params' => $this->params,
            'configurationsOverride' => array_keys($this->configurationsOverride),
            'configurationsOverrideValues' => $this->configurationsOverride,
            'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
            'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues
        ));
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function main() {
        $this->initProcess();
        if (!empty($this->params['c'])) {
            $command = $this->params['c'];
        } else {
            $command = null;
        }
        switch ($command) {
            case "import_for_time_hours":
                $this->ApiLogger->notice("Comando: import_for_time_hours");
                $this->time = $this->params['t'];
                if (is_null($this->time) || !is_numeric($this->time)) {
                    $this->startDate = null;
                    $this->endDate = null;
                    $this->ApiLogger->echoConsole('Error: tiempo (t) ingresado');
                    $this->ApiLogger->notice("Error tiempo (t) ingresado");
                } else {
                    $this->ApiLogger->notice("Tiempo en Hs a procesar:", $this->time);
                    $this->startDate = date('Y-m-d H:00:00');
                    $this->endDate = date('Y-m-d H:00:00', strtotime("-" . $this->time . " hour"));
                }
                break;

            case "import_for_start_end_date":
                $this->startDate = $this->params['s'];
                $this->endDate = $this->params['e'];
                if (is_null($this->startDate)) {
                    $this->startDate = null;
                    $this->ApiLogger->echoConsole('Error: fecha (s)');
                    $this->ApiLogger->notice("Error en fecha (s)");
                }
                if (is_null($this->endDate)) {
                    $this->endDate = null;
                    $this->ApiLogger->echoConsole('Error: fecha (e)');
                    $this->ApiLogger->notice("Error de fecha (e)");
                }
                break;

            case "import_for_default_options":
                $this->time = self::DEFAULT_TIME;
                $this->startDate = date('Y-m-d H:00:00');
                $this->endDate = date('Y-m-d H:00:00', strtotime("-" . $this->time . " hour"));
                break;

            case "create_events_for_subscriptions":
                $this->startDate = null;
                $this->endDate = null;
                $fromId = $this->params['from_id'];
                if (is_null($fromId)) {
                    $fromId = 1;
                }
                $pageSize = $this->params['page_size'];
                if (is_null($pageSize)) {
                    $pageSize = 50000;
                }
                $dropBeforeEvents = $this->params['drop_before_events'];
                $dropBeforeProfiles = $this->params['drop_before_profiles'];
                $this->EventImportEmtEventsService->createEventsForSubscriptions($fromId, $dropBeforeProfiles, $dropBeforeEvents, $pageSize);
                break;

            default:
                $this->help();
                break;
        }
        if (!is_null($this->startDate) && !is_null($this->endDate)) {
            $this->ApiLogger->echoConsoleWithTime('Iniciando servicio');
            $this->ApiLogger->notice("Iniciando Servicio", array(
                'Hora' => date('Y-m-d H:i:s')
            ));
            try {
                $result = $this->EventImportEmtEventsService->importData($this->startDate, $this->endDate);
                $this->ApiLogger->echoConsoleWithTime('Proceso finalizado Hora');
                $this->ApiLogger->notice("Proceso Finalizado");
            } catch (Exception $e) {
                $this->ApiLogger->notice("Exception", array(
                    'error' => $e
                ));
            }
        }
    }

    function help() {
        echo "\n\n";
        echo 'NAME:' . "\t" . 'import_emt_events' . "\n To imports emt events on clubcupon";
        echo "\n\n" . 'SYNOPSIS:' . "\n\n";
        echo "import_emt_events \n\n c:[import_for_default_options | \n" . " import_for_time_hours | \n import_for_start_end_date]\n\n";
        echo "OPTIONS:\n\n";
        echo 'c:import_for_default_options - import all events 24 Hours ago' . "\n";
        echo 'c:import_for_time_hours t:[n] - import all events t Hours ago ([n] unsigned int)' . "\n";
        echo 'c:import_for_start_end_date s:[START_DATE] e:[END_DATE] ' . "\n";
        echo "\n\nDEBUG: \n\n";
        echo "+DLog.Verbose:1 +Ddebug:2 +DLog.PrefixFileName:file_name.log\n";
        echo "\n\nEXAMPLES:\n\n";
        echo 'import_emt_events c:import_for_default_options ' . "\n";
        echo 'import_emt_events c:import_for_time_hours t:20 ' . "\n";
        echo 'import_emt_events c:import_for_start_end_date s:"2014-6-3 17:00:00" e:"2014-6-2 17:00:00"' . "\n\n";
    }

}
