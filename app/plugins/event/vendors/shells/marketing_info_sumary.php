<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING);
App::import('Component', 'api.ApiLogger');
App::import('Component', 'api.ApiShellUtils');
App::import('Component', 'event.MarketingInfoSumaryService');

class MarketingInfoSumaryShell extends Shell {

    private $MarketingInfoSumaryService;

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
        $this->ApiShellUtils = new ApiShellUtilsComponent();
        $this->MarketingInfoSumaryService = new MarketingInfoSumaryServiceComponent();
    }

    private function initProcess() {
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args, false);
        $this->configurationsOverrideBeforeValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->configurationsOverride = $this->ApiShellUtils->overrideConfigurationsByParams($this->args);
        $this->configurationsOverrideAfterValues = $this->ApiShellUtils->loadConfigurationsValues(array_keys($this->configurationsOverride));
        $this->params = $this->ApiShellUtils->getOptions($this->args);
        $this->ApiLogger->echoConsoleWithTime("Iniciando proceso");
        $this->ApiLogger->notice("Iniciando Proceso " . __class__, array(
            'args' => $this->args,
            'params' => $this->params,
            'configurationsOverride' => array_keys($this->configurationsOverride),
            'configurationsOverrideValues' => $this->configurationsOverride,
            'configurationsToOverrideBeforeValues' => $this->configurationsOverrideBeforeValues,
            'configurationsToOverrideAfterValues' => $this->configurationsOverrideAfterValues
        ));
    }

    function main() {
        $this->initProcess();
        if (!empty($this->params['c'])) {
            $command = $this->params['c'];
        } else {
            $command = null;
        }
        if (!empty($this->params['page_size_enable'])) {
            $pageSizeEnable = $this->params['page_size_enable'];
        }
        switch ($command) {
            case "create_files_for_all":
                $this->ApiLogger->echoConsoleWithTime("Iniciando servicio");
                $this->ApiLogger->notice("Iniciando Servicio", array(
                    'Hora' => date('Y-m-d H:i:s')
                ));
                try {
                    if (!empty($pageSizeEnable)) {
                        if ($pageSizeEnable == 'true') {
                            $this->MarketingInfoSumaryService->generateFileForAllFrequencyEventClass(true);
                        } else {
                            $this->MarketingInfoSumaryService->generateFileForAllFrequencyEventClass(false);
                        }
                    } else {
                        $this->MarketingInfoSumaryService->generateFileForAllFrequencyEventClass(false);
                    }
                    $this->ApiLogger->echoConsoleWithTime("Proceso Finalizado");
                    $this->ApiLogger->notice("Proceso Finalizado");
                } catch (Exception $e) {
                    $this->ApiLogger->notice("Exception", array(
                        'error' => $e
                    ));
                }
                break;

            default:
                $this->help();
                break;
        }
    }

    function help() {
        echo "\n\n";
        echo 'NAME:' . "\t" . 'marketing_info_sumary' . "\n To export email profiles on csv file";
        echo "\n\n" . 'SYNOPSIS:' . "\n\n";
        echo "[ create_files_for_all ]\n\n";
        echo "OPTIONS:\n\n";
        echo 'c:create_files_for_all ' . "\n";
        echo "\n\nDEBUG: \n\n";
        echo "+DLog.Verbose:1 +Ddebug:2 +DLog.PrefixFileName:file_name.log\n";
        echo "\n\nEXAMPLES:\n\n";
        echo 'marketing_info_sumary c:create_files_for_all ' . "\n\n";
    }

}
