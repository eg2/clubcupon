<?php

class EventEmailProfile extends EventAppModel {

    var $useTable = 'email_profiles';
    var $name = 'EventEmailProfile';
    var $alias = 'EventEmailProfile';
    public $recursive = - 1;

    function getAll() {
        return $this->find('all', array(
                    'recursive' => - 1
        ));
    }

    public function findByFrecuencyEventClassId($frecuencyEventClassId) {
        $conditions = array(
            'frecuency_event_class_id' => $frecuencyEventClassId
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
