<?php

App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));

class EventEvent extends EventAppModel {

    var $useTable = 'events';
    var $name = 'EventEvent';
    var $alias = 'EventEvent';
    public $recursive = - 1;
    private $eventEventData;
    private $fieldsEvents = array(
        'EventEvent.id',
        'EventEvent.email',
        'EventEvent.last_event_date',
        'EventEvent.event_type_id'
    );
    private $fieldsToUpdateEmailProfile = array(
        'EventEvent.email',
        'MAX(EventEvent.last_event_date) as last_event_date'
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
    }

    public function findEventsVIOMAILByEmailAndEventTypeIds($email, $eventTypeIds) {
        $events = $this->find('all', array(
            'fields' => $this->fieldsEvents,
            'conditions' => array(
                'email' => $email,
                'event_type_id' => $eventTypeIds
            )
        ));
        return $events;
    }

    public function getByEmail($email) {
        return $this->find('first', array(
                    'conditions' => array(
                        'email' => $email
                    ),
                    'recursive' => - 1
        ));
    }

    public function emailExists($email) {
        return $this->getByEmail($email);
    }

    private function whereDateConditionSQL($min, $max, $today) {
        $conditions = array();
        if (!empty($max) && $max > 0) {
            $maxDate = date('Y-m-d 00:00:00', strtotime($today . " -$max day"));
            $conditions = array(
                "$this->alias.last_event_date" . ' >=' => $maxDate
            );
        }
        if (!empty($min) && $min > 0) {
            $minDate = date('Y-m-d 00:00:00', strtotime($today . " -$min day"));
            $conditionsMin = array(
                "$this->alias.last_event_date" . ' <=' => $minDate
            );
            $conditions = array_merge($conditions, $conditionsMin);
        }
        return $conditions;
    }

    private function whereBetweenDateORSQL($betweenDatesByEventType, $today) {
        $conditions = null;
        $quantityFrecuencies = count($betweenDatesByEventType['frecuency']);
        foreach ($betweenDatesByEventType['frecuency'] as $idEventType => $betweenDateRange) {
            if (!empty($betweenDateRange[0]) || !empty($betweenDateRange[1])) {
                $conditionEventTypeId = array(
                    'EventEvent.event_type_id' => $idEventType
                );
                $conditionsDate = $this->whereDateConditionSQL($betweenDateRange[0], $betweenDateRange[1], $today);
                $conditionsBetweenDate = array_merge($conditionEventTypeId, $conditionsDate);
                if ($quantityFrecuencies > 1) {
                    $conditions['OR'][] = $conditionsBetweenDate;
                } else {
                    $conditions = $conditionsBetweenDate;
                }
            }
        }
        return $conditions;
    }

    public function findLastEventBetweenDates($limit, $page, $betweenDatesByEventType, $extraConditions, $onlyFrecuencyClassId, $today) {
        $conditions = $this->whereBetweenDateORSQL($betweenDatesByEventType, $today);
        if (!empty($extraConditions)) {
            $conditions = array_merge($conditions, $extraConditions);
        }
        $emailProfilesHasClassified = " SELECT 1 " . " FROM email_profiles as EmailProfile " . " WHERE EmailProfile.frecuency_event_class_id = " . $onlyFrecuencyClassId . " AND EmailProfile.email = EventEvent.email ";
        $conditions[] = DboSource::expression(" EXISTS ({$emailProfilesHasClassified}) ");
        $this->ApiLogger->trace('whereBetweenDateORSQL', array(
            'betweenDates' => $conditions
        ));
        $result = $this->find('all', array(
            'conditions' => $conditions,
            'limit' => $limit,
            'page' => $page,
            'group' => array(
                $this->alias . '.email'
            ),
            'fields' => $this->fieldsToUpdateEmailProfile
        ));
        return $result;
    }

    public function findByEmailEvent($emailEvent) {
        return $this->getByEmail($emailEvent['EventEvent']['email']);
    }

    public function findByEventTypeEmail($idEventType, $email) {
        return $this->find('first', array(
                    'conditions' => array(
                        'email' => $email,
                        'event_type_id' => $idEventType
                    ),
                    'recursive' => - 1
        ));
    }

    public function updateEvent($hit, $event) {
        $eventId = $event['EventEvent']['id'];
        $modified = "'" . date('Y-m-d H:i:s') . "'";
        $last_event_date = "'" . $hit[0]['event_date'] . "'";
        if ($this->updateAll(array(
                    'EventEvent.last_event_date' => $last_event_date,
                    'EventEvent.modified' => $modified
                        ), array(
                    'EventEvent.id' => $eventId
                ))) {
            return true;
        } else {
            throw new Exception('Hubo un problema al guardar.');
        }
    }

    public function insertEvent($hit, $email, $event_type_id) {
        $created = date('Y-m-d H:i:s');
        $modified = date('Y-m-d H:i:s');
        $last_event_date = $hit[0]['event_date'];
        $this->eventEventData = array(
            'EventEvent' => Array(
                'created' => $created,
                'modified' => $modified,
                'email' => $email,
                'last_event_date' => $last_event_date,
                'event_type_id' => $event_type_id,
                'deleted' => 0
            )
        );
        $this->create();
        if ($this->save($this->eventEventData)) {
            return true;
        } else {
            return false;
        }
    }

    function transaction() {
        $this->actsAs = array(
            'Transactional'
        );
        return $this->begin();
    }

    function insertAll($sql) {
        try {
            $hola = $this->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}
