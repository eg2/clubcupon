<?php

class EventEventType extends AppModel {

    public $name = 'EventEventType';
    public $useTable = 'event_types';
    public $alias = 'EventEventType';

    function findByCode($code) {
        $conditions = array(
            'code' => $code
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
