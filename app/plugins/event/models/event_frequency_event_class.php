<?php

class EventFrequencyEventClass extends EventAppModel {

    var $useTable = 'frecuency_event_classes';
    var $name = 'EventFrequencyEventClass';
    var $alias = 'EventFrequencyEventClass';

    function findById($id) {
        $conditions = array(
            'id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function findByCode($code) {
        $conditions = array(
            'code' => $code
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

}
