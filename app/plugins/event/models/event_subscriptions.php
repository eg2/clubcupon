<?php

class EventSubscriptions extends AppModel {

    public $name = 'EventSubscriptions';
    public $useTable = 'subscriptions';

    function findById($id) {
        $conditions = array(
            'id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    function findByEmail($email) {
        $conditions = array(
            'email' => $email
        );
        $order = array(
            'id DESC'
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'order' => $order,
                    'recursive' => - 1
        ));
    }

    function getAllActiveSubscriptions($desde, $hasta) {
        try {
            $sql = "select distinct(email), created,modified from subscriptions s where id >=" . $desde . " and id <= " . $hasta . " and is_subscribed = 1;";
            return $this->query($sql);
        } catch (Exception $e) {
            return null;
        }
    }

}
