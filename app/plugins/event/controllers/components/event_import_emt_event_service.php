<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiLogger');
App::import('Model', 'Event.EventEventType');
App::import('Model', 'Event.EventEvent');
App::import('Model', 'Event.EventHit');
App::import('Model', 'Event.EventSubscriptions');
App::import('Component', 'newsletter.NewsletterEmailProfileService');
App::import('Model', 'Event.EventEmailProfile');
App::import('Model', 'Event.EventFrequencyEventClass');
App::import('Model', 'newsletter.NewsletterNewsletterProfile');

class EventImportEmtEventServiceComponent extends Component {

    public $name = 'EventImportEmtEventService';
    public $eventTypeByEmt = null;
    private $NewsletterEmailProfile;
    private $hits;
    private $subscriptions;
    private $event;
    private $eventType;
    private $countInsert = 0;
    private $countUpdate = 0;
    private $countInactive = 0;
    private $countSubscriptions = 0;
    private $countErrorSubscriptions = 0;
    private $countTypeEventUnknow = 0;

    const NO_CLASS_CODE = 'NC';
    const NA_NEWSLETTER_PROFILE_CODE = 'NA';
    const CODE_EVENT_TYPE_SUBSCRIBED = 'SUBSCRIBED';

    function __construct() {
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
        $this->subscriptions = new EventSubscriptions();
        $this->hits = new EventHit();
        $this->event = new EventEvent();
        $this->eventType = new EventEventType();
        $this->NewsletterNewsletterProfile = new NewsletterNewsletterProfile();
        $this->EventFrequencyEventClass = new EventFrequencyEventClass();
        $this->NewsletterEmailProfileService = new NewsletterEmailProfileServiceComponent();
        $this->NewsletterEmailProfile = new NewsletterEmailProfile();
        $this->noClassFrecuencyEventClass = $this->EventFrequencyEventClass->findByCode(self::NO_CLASS_CODE);
        $this->naNewsletterProfile = $this->NewsletterNewsletterProfile->findByCode(self::NA_NEWSLETTER_PROFILE_CODE);
        $this->noClassFrecuencyEventClassId = $this->noClassFrecuencyEventClass['EventFrequencyEventClass']['id'];
        $this->naNewsletterProfileId = $this->naNewsletterProfile['NewsletterNewsletterProfile']['id'];
        $this->ApiLogger->debug("Obteniendo Ids por Nulos de newsletter_profile y frecuency_event_class", array(
            'Frecuency Class NO_CLASS' => $this->noClassFrecuencyEventClassId,
            'Newsletter Profile na' => $this->naNewsletterProfileId
        ));
    }

    private function insertEventType($hit, $subscription) {
        $codeEvents = Configure::read('event.emtConversionEventsCode');
        if (!empty($codeEvents[$hit['hits']['event_type']])) {
            $eType = $this->eventType->findByCode($codeEvents[$hit['hits']['event_type']]);
        } else {
            $eType = null;
        }
        if (!is_null($eType)) {
            $event = $this->event->findByEventTypeEmail($eType['EventEventType']['id'], $subscription['EventSubscriptions']['email']);
            if (count($event) > 0) {
                if ($this->event->UpdateEvent($hit, $event)) {
                    $this->ApiLogger->trace("Se actualizo el evento correctamente");
                    $this->countUpdate++;
                } else {
                    $this->ApiLogger->echoConsole("error al insertar");
                }
            } else {
                if ($this->event->insertEvent($hit, $subscription['EventSubscriptions']['email'], $eType['EventEventType']['id'])) {
                    $this->ApiLogger->trace("Se inserto el evento correctamente");
                    $this->countInsert++;
                } else {
                    $this->ApiLogger->echoConsole("error al insertar");
                }
            }
        } else {
            $this->ApiLogger->notice("El evento de EMT no se guardara por tipo de evento no contemplado", array(
                'Hit' => $hit['hits']
            ));
            $this->countTypeEventUnknow++;
        }
    }

    private function procesarData($hitsData) {
        $this->ApiLogger->echoConsoleWithTime("\nProcesando eventos ");
        $this->ApiLogger->notice("Procesando eventos" . date('Y-m-d H:i:s'));
        $index = 1;
        $countImport = count($hitsData);
        foreach ($hitsData as $hit) {
            $subscription = $this->subscriptions->findById($hit[0]['subscription_id']);
            if (is_null($subscription)) {
                $this->ApiLogger->trace("No contiene un id de suscripcion valida", $hit);
                $this->countInactive++;
            } else {
                $this->insertEventType($hit, $subscription);
            }
            if (fmod($index, 250) == 0) {
                $this->ApiLogger->echoConsole(".", false);
            }
            if (fmod($index, 1000) == 0) {
                $this->ApiLogger->notice("Eventos procesados " . $index . "/" . $countImport);
            }
            $index++;
        }
        $this->ApiLogger->notice("Eventos procesados " . $index . "/" . $countImport);
        $this->ApiLogger->echoConsoleWithTime("\nEventos procesados");
        $this->ApiLogger->notice("Eventos procesados" . date('Y-m-d H:i:s'));
    }

    public function importData($startDate, $endDate) {
        $this->importEvents($startDate, $endDate);
    }

    public function importEvents($startDate, $endDate) {
        $this->ApiLogger->echoConsole("Iniciando importacion de Eventos Emt StartDate: " . $startDate . " EndDate: " . $endDate);
        $this->ApiLogger->notice("Iniciando importacion de Eventos Emt", array(
            'StartDate' => $startDate,
            'EndDate' => $endDate
        ));
        $hitsData = $this->hits->getLastEvents($startDate, $endDate);
        $countImport = count($hitsData);
        if ($countImport < 0) {
            return false;
        } else {
            $this->ApiLogger->notice("Procesando eventos ", array(
                'startDate' => $startDate,
                'endDate' => $endDate
            ));
            $this->ApiLogger->echoConsole("\nse procesaran " . $countImport . " eventos");
            $this->ApiLogger->echoConsole("_____________________________________________________________________");
            $this->ApiLogger->notice("Se procesaran ", array(
                'eventos' => $countImport
            ));
            $this->procesarData($hitsData);
            $this->ApiLogger->echoConsole("\n___________________________________________________________________");
        }
        $this->ApiLogger->echoConsole("\nEventos Insertados " . $this->countInsert);
        $this->ApiLogger->notice("Eventos Insertados " . $this->countInsert);
        $this->ApiLogger->echoConsole("Eventos Actualizados " . $this->countUpdate . " registros");
        $this->ApiLogger->notice("Eventos Actualizados " . $this->countUpdate);
        $this->ApiLogger->echoConsole("Suscripciones no validas " . $this->countInactive);
        $this->ApiLogger->notice("Suscripciones no validas " . $this->countInactive);
        $this->ApiLogger->echoConsole("Eventos desconocidos " . $this->countTypeEventUnknow);
        $this->ApiLogger->notice("Eventos desconocidos " . $this->countTypeEventUnknow);
        $this->ApiLogger->echoConsole("\n___________________________________________________________________");
        return true;
    }

    private function generateEventSQLInsert($email, $idEventType, $lastEventDate, $frecuency_event_class_id) {
        return "INSERT INTO events (created,modified,deleted,email,last_event_date,event_type_id)
			VALUES ('" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "',0,'" . $email . "','" . $lastEventDate . "',1)
			ON DUPLICATE KEY UPDATE last_event_date ='" . $lastEventDate . "', modified = '" . date('Y-m-d H:i:s') . "';";
    }

    private function generateEmailProfileSQLInsert($date, $email, $lastEventDate, $frecuency_event_class_id, $newslleter_profile_id) {
        return "INSERT INTO email_profiles (created,modified,deleted,email,last_event_date, frecuency_event_class_id, newsletter_profile_id_for_cities,newsletter_profile_id_for_groups)
					VALUES ('" . $date . "','" . $date . "',0,'" . $email . "','" . $lastEventDate . "'," . $frecuency_event_class_id . "," . $newslleter_profile_id . "," . $newslleter_profile_id . ")
    				ON DUPLICATE KEY UPDATE created = created;";
    }

    public function processSubscriptionsSQL($subscriptionsData) {
        $eType = $this->eventType->findByCode(self::CODE_EVENT_TYPE_SUBSCRIBED);
        $index = 0;
        $max = 10;
        try {
            $this->event->transaction();
            foreach ($subscriptionsData as $subscription) {
                $email = $subscription['s']['email'];
                $lastEventDate = $subscription['s']['modified'];
                if (!is_null($email)) {
                    if (!$this->event->query($this->generateEventSQLInsert($email, $eType['EventEventType']['id'], $lastEventDate))) {
                        $this->countSubscriptions++;
                    }
                    if (!$this->event->query($this->generateEmailProfileSQLInsert(date('Y-m-d H:i:s'), $email, $lastEventDate, $this->noClassFrecuencyEventClassId, $this->naNewsletterProfileId))) {
                        $this->countEmailProfiles++;
                    }
                    if (fmod($index, 500) == 0) {
                        $this->ApiLogger->echoConsole(".", false);
                    }
                    $index++;
                }
            }
            $return = $this->event->commit();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function processSubscriptions($subscriptionsData) {
        $eType = $this->eventType->findByCode(self::CODE_EVENT_TYPE_SUBSCRIBED);
        foreach ($subscriptionsData as $subscription) {
            $email = $subscription['s']['email'];
            if ($this->createEventSubscription($email)) {
                $this->countSubscriptions++;
            }
        }
    }

    public function createEventsForSubscriptions($fromId = 1, $dropBeforeProfiles = false, $dropBeforeEvents = false, $pageSize = 50000) {
        $this->ApiLogger->echoConsoleWithTime("Iniciando Generacion de Eventos para las suscripciones activas ");
        $this->ApiLogger->debug("Iniciando Generacion de Eventos para las suscripciones activas", array(
            'date' => date('Y-m-d H:i:s'),
            'drop_before_events' => $dropBeforeEvents,
            'drop_before_profiles' => $dropBeforeProfiles
        ));
        if ($dropBeforeEvents) {
            $this->event->query('truncate events;');
        }
        if ($dropBeforeProfiles) {
            $this->event->query('truncate email_profiles;');
        }
        $delta = $pageSize;
        $lotNumber = 1;
        $inicio = $fromId;
        $fin = $inicio + $delta;
        $this->ApiLogger->echoConsoleWithTime("Obteniendo subscripciones  [dde:" . $inicio . " | hta:" . $fin . " | reintentos:" . $reintentos . "]");
        $this->ApiLogger->debug("Obteniendo subscripciones");
        $subscriptions = $this->subscriptions->getAllActiveSubscriptions($inicio, $fin);
        $count = count($subscriptions);
        while ($count > 0 || $reintentos < 100) {
            if ($count > 0) {
                $this->ApiLogger->echoConsoleWithTime("Procesando lote de #" . $lotNumber . "(" . $count . " suscripciones [dde:" . $inicio . "|hta:" . $fin . "]) ");
                $reintentos = 0;
                $this->processSubscriptionsSQL($subscriptions);
                $this->ApiLogger->echoConsoleWithTime("Lote procesado");
            } else {
                $reintentos = $reintentos + 1;
                $this->ApiLogger->echoConsole("No hay suscripciones [dde:" . $inicio . " | hta:" . $fin . " | reintentos:" . $reintentos . "]");
                $this->ApiLogger->debug("No hay suscripciones");
            }
            $inicio = $inicio + $delta;
            $fin = $fin + $delta;
            $lotNumber = $lotNumber + 1;
            $this->ApiLogger->echoConsoleWithTime("Obteniendo subscripciones [dde:" . $inicio . " | hta:" . $fin . " | reintentos:" . $reintentos . "]");
            $subscriptions = $this->subscriptions->getAllActiveSubscriptions($inicio, $fin);
            $count = count($subscriptions);
        }
        $this->ApiLogger->echoConsoleWithTime("Se procesaron " . $this->countSubscriptions . " suscripciones ");
        $this->ApiLogger->debug("Se procesaron " . $this->countSubscriptions . " suscripciones");
    }

    private function createEventSubscription($email) {
        $this->ApiLogger->notice(__METHOD__, array(
            'email' => $email
        ));
        if (!is_null($email)) {
            $eType = $this->eventType->findByCode(self::CODE_EVENT_TYPE_SUBSCRIBED);
            $event = $this->event->findByEventTypeEmail($eType['EventEventType']['id'], $email);
            $hit[0]['event_date'] = date('Y-m-d H:00:00');
            if (count($event) > 0) {
                if ($this->event->UpdateEvent($hit, $event)) {
                    return true;
                }
            } else {
                if ($this->event->insertEvent($hit, $email, $eType['EventEventType']['id'])) {
                    return true;
                }
            }
        }
        return false;
    }

    public function eventSubscriptionService($subscription_email) {
        $this->ApiLogger->notice(__METHOD__, array(
            'email' => $subscription_email
        ));
        if (!is_null($subscription_email)) {
            if ($this->createEventSubscription($subscription_email)) {
                if ($this->NewsletterEmailProfileService->insertAndUpdateEmailProfileByEmail($subscription_email)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function showEvents($startDate = null, $email = null, $eventTypeIds = array()) {
        
    }

}
