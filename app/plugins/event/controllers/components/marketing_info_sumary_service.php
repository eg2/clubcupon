<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiLogger');
App::import('Model', 'Event.EventSubscriptions');
App::import('Model', 'Event.EventEmailProfile');
App::import('Model', 'Event.EventFrequencyEventClass');
App::import('Model', 'api.ApiDealExternal');
App::import('Model', 'User');

class MarketingInfoSumaryServiceComponent extends Component {

    private $eventSubscriptions;
    private $eventEmailProfile;
    private $eventFrequencyEventClass;
    private $frequencyEventClassToExport;
    private $dealExternal;
    private $headerFile;
    private $filePath;
    private $pageSize;
    private $user;
    private $pageSizeEnable;
    private $pageFull;

    function __construct() {
        parent::__construct();
        Configure::load('marketing_info_sumary_configuration');
        $this->ApiLogger = new ApiLogger(get_class($this), 'event');
        $this->eventSubscriptions = new EventSubscriptions();
        $this->eventEmailProfile = new EventEmailProfile();
        $this->eventFrequencyEventClass = new EventFrequencyEventClass();
        $this->frequencyEventClassToExport = Configure::read('event.marketingSumary.FrequencyEventToExport');
        $this->dealExternal = new ApiDealExternal();
        $this->headerFile = Configure::read('event.marketingSumary.FileHeader');
        $this->filePath = Configure::read('event.marketingSumary.FilePath');
        $this->pageSize = Configure::read('event.marketingSumary.pageSize');
        $this->user = new User();
    }

    private function getFileNamePage($date, $code, $pageNumber) {
        $fecha = str_replace("-", "", $date);
        return $this->filePath . $fecha . "-" . $code . "-" . $pageNumber . ".csv";
    }

    private function getFileName($date, $code) {
        $fecha = str_replace("-", "", $date);
        return $this->filePath . $fecha . "-" . $code . ".csv";
    }

    private function createFile($fecha, $code, $pageNumber, $content) {
        if ($this->pageFull) {
            $fullFileName = $this->getFileName($fecha, $code);
            $handle = fopen($fullFileName, 'w');
            fclose($handle);
            $handle = fopen($fullFileName, 'a+');
            $this->pageFull = false;
            $this->ApiLogger->echoConsole("\nCreando archivo " . $fullFileName);
            $this->ApiLogger->notice("Creando Archivo", array(
                'nameFile' => $fullFileName
            ));
        } else {
            if ($this->pageSizeEnable) {
                $fullFileName = $this->getFileNamePage($fecha, $code, $pageNumber);
                $handle = fopen($fullFileName, 'w');
                $this->ApiLogger->echoConsole("\nCreando archivo " . $fullFileName);
                $this->ApiLogger->notice("Creando Archivo", array(
                    'nameFile' => $fullFileName
                ));
            } else {
                $fullFileName = $this->getFileName($fecha, $code);
                $handle = fopen($fullFileName, 'a+');
                $this->ApiLogger->echoConsole("\nEscribiendo archivo " . $fullFileName);
                $this->ApiLogger->notice("Escribiendo Archivo", array(
                    'nameFile' => $fullFileName
                ));
            }
        }
        if ($handle) {
            $index = 0;
            $this->ApiLogger->echoConsole("\nActualizando archivo");
            foreach ($content as $fields) {
                fputcsv($handle, $fields);
                if (fmod($index, 500) == 0) {
                    $this->ApiLogger->echoConsole(".", false);
                }
                $index++;
            }
        } else {
            $this->ApiLogger->echoConsole("ERROR AL CREAR ARCHIVO " . $fullFileName);
            $this->ApiLogger->error("Creando Archivo", array(
                'nameFile' => $fullFileName,
                'data' => $content
            ));
        }
        fclose($handle);
    }

    private function getLineForFile(&$data) {
        return array(
            $data['email'],
            $data['segmento'],
            $data['subscription_id'],
            $data['compro'],
            $data['fecha_ultima_compra'],
            $data['fecha_ultima_apertura'],
            $data['category_id'],
            $data['category_path']
        );
    }

    private function getArrayData($frequencyEventClass, $subscription, $profile, $compro, $fecha_ultima_compra, $category_id, $category_path) {
        return array(
            'email' => $profile['EventEmailProfile']['email'],
            'segmento' => $frequencyEventClass['EventFrequencyEventClass']['code'],
            'subscription_id' => $subscription['EventSubscriptions']['id'],
            'compro' => $compro,
            'fecha_ultima_compra' => $fecha_ultima_compra,
            'fecha_ultima_apertura' => $profile['EventEmailProfile']['last_event_date'],
            'category_id' => $category_id,
            'category_path' => $category_path
        );
    }

    private function processDataToFile(&$processData, $frequencyEventClass, $pageNumber) {
        $content = array();
        if ($this->pageFull) {
            $content[] = $this->headerFile;
        } else {
            if ($this->pageSizeEnable) {
                $content[] = $this->headerFile;
            }
        }
        foreach ($processData as $data) {
            $content[] = $this->getLineForFile($data);
        }
        $this->createFile(date('Y-m-d'), $frequencyEventClass['EventFrequencyEventClass']['code'], $pageNumber, $content);
    }

    private function processEmailProfiles($frequencyEventClass, &$emailProfiles) {
        if (!is_null($emailProfiles)) {
            $index = 1;
            $processData = array();
            $this->ApiLogger->echoConsole("\n_________________________________________");
            $this->ApiLogger->echoConsoleWithTime("\nProcesando email profiles ");
            $this->ApiLogger->notice("Procesando email profiles");
            $pageNumber = 1;
            $countEmailProfiles = count($emailProfiles);
            if ($countEmailProfiles < $this->pageSize) {
                $this->pageSize = $countEmailProfiles;
            }
            foreach ($emailProfiles as $profile) {
                $subscription = $this->eventSubscriptions->findByEmail($profile['EventEmailProfile']['email']);
                $compro = 0;
                $fecha_ultima_compra = null;
                $category_id = null;
                $category_path = null;
                if (!is_null($subscription['EventSubscriptions']['user_id'])) {
                    $dealExt = $this->dealExternal->findLastDealExternalByUserId($subscription['EventSubscriptions']['user_id']);
                    if (!is_null($dealExt)) {
                        $compro = 1;
                        $fecha_ultima_compra = $dealExt['DealExternal']['created'];
                        $category_id = $dealExt['DealFlatCategory']['id'];
                        $category_path = $dealExt['DealFlatCategory']['path'];
                    }
                }
                $processData[] = $this->getArrayData($frequencyEventClass, $subscription, $profile, $compro, $fecha_ultima_compra, $category_id, $category_path);
                if (fmod($index, $this->pageSize) == 0) {
                    $this->processDataToFile($processData, $frequencyEventClass, $pageNumber);
                    $pageNumber++;
                    $processData = array();
                    $this->ApiLogger->echoConsole("\n\nEmail profiles procesados " . $index . "/" . $countEmailProfiles);
                    $this->ApiLogger->echoConsole("\n_________________________________________");
                    $this->ApiLogger->echoConsoleWithTime("\nProcesando email profiles ");
                    $this->ApiLogger->notice("Procesando email profiles");
                }
                if (fmod($index, 500) == 0) {
                    $this->ApiLogger->echoConsole(".", false);
                }
                if (fmod($index, 1000) == 0) {
                    $this->ApiLogger->notice("Email profiles procesados " . $index . "/" . $countEmailProfiles);
                }
                $index++;
            }
            if (count($processData) > 0) {
                $this->processDataToFile($processData, $frequencyEventClass, $pageNumber);
                $this->ApiLogger->echoConsole("\n\nEmail profiles procesados " . $index . "/" . $countEmailProfiles);
                $this->ApiLogger->echoConsole("\n_________________________________________");
                $this->ApiLogger->echoConsoleWithTime("\nProcesando email profiles ");
                $this->ApiLogger->notice("Procesando email profiles");
            }
            $this->pageSize = Configure::read('event.marketingSumary.pageSize');
        }
    }

    public function generateFileForAllFrequencyEventClass($pageSizeEnable = false) {
        $this->pageSizeEnable = $pageSizeEnable;
        foreach ($this->frequencyEventClassToExport as $frecuencyEventClassCode => $hasExport) {
            if ($hasExport) {
                if (!$this->pageSizeEnable) {
                    $this->pageFull = true;
                } else {
                    $this->pageFull = false;
                }
                $frequencyEventClass = $this->eventFrequencyEventClass->findByCode($frecuencyEventClassCode);
                $this->generateFileForFrequencyEventClass($frequencyEventClass);
            } else {
                $this->ApiLogger->notice("Se ignora FrequencyEventClass.", array(
                    'frecuencyEventClassCode' => $frecuencyEventClassCode
                ));
            }
        }
    }

    public function generateFileForFrequencyEventClass($frequencyEventClass) {
        $this->ApiLogger->echoConsole("\n_________________________________________________________________________");
        $this->ApiLogger->echoConsole("obteniendo email profiles por la frecuencia " . $frequencyEventClass['EventFrequencyEventClass']['code']);
        $this->ApiLogger->notice("obteniendo email profiles por la frecuencia", array(
            'EventFrequencyEventClass' => $frequencyEventClass['EventFrequencyEventClass']['code']
        ));
        $emailProfiles = $this->eventEmailProfile->findByFrecuencyEventClassId($frequencyEventClass['EventFrequencyEventClass']['id']);
        $count = count($emailProfiles);
        $this->ApiLogger->echoConsole("\n emailProfiles found: " . $emailProfiles);
        if (!is_null($emailProfiles)) {
            if ($this->pageSizeEnable) {
                $n = ($count / $this->pageSize);
                $nFiles = round($n);
                if (($n - $nFiles) > 0) {
                    $nFiles++;
                }
            } else {
                $nFiles = 1;
            }
            $this->ApiLogger->echoConsole("Se procesaran " . $count . " email profiles en " . $nFiles . " Archivos");
            $this->ApiLogger->notice("Se procesaran " . $count . " email profiles  en " . $nFiles . " Archivos");
            $processDataToExport = $this->processEmailProfiles($frequencyEventClass, $emailProfiles);
        }
        $this->ApiLogger->echoConsole("\n_________________________________________________________________________");
        return true;
    }

}
