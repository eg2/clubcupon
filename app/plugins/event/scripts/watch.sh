#! /bin/bash
while getopts f:c:p opts; do
  case ${opts} in
    f) shellFileName=${OPTARG} ;;
    c) commandMethod=${OPTARG} ;;
  esac
done
while inotifywait -re close_write app; do ./do.sh -f $shellFileName -c $commandMethod; done

