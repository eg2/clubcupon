<?php

class EventAppModel extends AppModel {

    public $recursive = - 1;

    public function getLastQuery() {
        if (Configure::read('debug') == 2) {
            $dbo = $this->getDatasource();
            $logs = $dbo->_queriesLog;
            return end($logs);
        } else {
            return false;
        }
    }

    public function getQueryLog() {
        if (Configure::read('debug') == 2) {
            $dbo = $this->getDatasource();
            $logs = $dbo->_queriesLog;
            return $logs;
        } else {
            return false;
        }
    }

}
