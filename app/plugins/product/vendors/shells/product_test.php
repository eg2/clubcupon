<?php

App::import('Component', 'product.ProductInventoryService');
App::import('Model', 'product.ProductProduct');
App::import('Model', 'product.ProductDeal');

class ProductTestShell extends Shell {

    public function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ProductInventoryService = new ProductInventoryServiceComponent();
        $this->ProductProduct = new ProductProduct();
        $this->ProductDeal = new ProductDeal();
        $this->dealIdList = array(
            41502,
            41501,
            41500,
        );
    }

    function testStockShouldBeDisplayed() {
        echo __METHOD__ . "\n";
        foreach ($this->dealIdList as $dealId) {
            $stock = $this->ProductInventoryService->stock($dealId);
            echo 'Stock: ' . $stock . "\n";
        }
    }

    function testStockShouldBeIncremented() {
        echo __METHOD__ . "\n";
        foreach ($this->dealIdList as $dealId) {
            $this->ProductInventoryService->increment($dealId, 5);
        }
    }

    function testStockShouldBeDecremented() {
        echo __METHOD__ . "\n";
        foreach ($this->dealIdList as $dealId) {
            $this->ProductInventoryService->decrease($dealId, 5);
        }
    }

    function clearStock() {
        echo __METHOD__ . "\n";
        $fields = array(
            'ProductProduct.stock' => 1000
        );
        $conditions = array(
            'ProductProduct.id' => 1
        );
        $this->ProductProduct->updateAll($fields, $conditions);
    }

    function testAvailableQuantityShouldBeDisplayed() {
        echo __METHOD__ . "\n";
        foreach ($this->dealIdList as $dealId) {
            $availableQuantity = $this->ProductInventoryService->availableQuantity($dealId);
            echo 'AvailableQuantity: ' . $availableQuantity . "\n";
        }
    }

    function setUp() {
        echo __METHOD__ . "\n";
        $this->clearStock();
    }

    function tearDown() {
        echo __METHOD__ . "\n";
        $this->clearStock();
    }

    function main() {
        $this->setUp();
        $this->testAvailableQuantityShouldBeDisplayed();
        $this->testStockShouldBeDecremented();
        $this->tearDown();
    }

}