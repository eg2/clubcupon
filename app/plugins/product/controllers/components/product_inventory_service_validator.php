<?php

App::import('Component', 'api.ApiBase');

class ProductInventoryServiceValidatorComponent extends ApiBaseComponent {

    public function __construct() {
        parent::__construct('product');
    }

    public function validate($product) {
        $isMissingParams = false;
        $requiered = array(
            'bac_product_id',
            'name',
            'bac_invoice_id',
            'bac_libertya_id',
            'bac_portal_id'
        );
        foreach ($requiered as $key) {
            if (empty($product['ProductInventory'][$key])) {
                $isMissingParams = true;
                break;
            }
        }
        return !$isMissingParams;
    }

}
