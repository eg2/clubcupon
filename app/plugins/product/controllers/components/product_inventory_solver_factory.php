<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'product.ProductInventorySolverUnmanagedStock');
App::import('Component', 'product.ProductInventorySolverManagedStock');
App::import('Component', 'product.ProductInventorySolverManagedWithPinsStock');
App::import('Component', 'product.ProductInventorySolverManagedWithBacStock');
App::import('Model', 'product.ProductInventoryStrategy');

class ProductInventorySolverFactoryComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct();
        $this->ProductInventorySolverUnmanagedStock = new ProductInventorySolverUnmanagedStockComponent();
        $this->ProductInventorySolverManagedStock = new ProductInventorySolverManagedStockComponent();
        $this->ProductInventorySolverManagedWithPinsStock = new ProductInventorySolverManagedWithPinsStockComponent();
        $this->ProductInventorySolverManagedWithBacStock = new ProductInventorySolverManagedWithBacStockComponent();
    }

    function solver($productInventoryStrategyId) {
        switch ($productInventoryStrategyId) {
            case ProductInventoryStrategy::ID_UNMANAGED_STOCK:
                return $this->ProductInventorySolverUnmanagedStock;
            case ProductInventoryStrategy::ID_MANAGED_STOCK:
                return $this->ProductInventorySolverManagedStock;
            case ProductInventoryStrategy::ID_MANAGED_WITH_PINS_STOCK:
                return $this->ProductInventorySolverManagedWithPinsStock;
            case ProductInventoryStrategy::ID_MANAGED_WITH_PINS_AND_BAC_STOCK:
                return $this->ProductInventorySolverManagedWithBacStock;
            default:
                return $this->ProductInventorySolverUnmanagedStock;
        }
    }

}
