<?php

App::import('Component', 'product.ProductInventorySolver');

class ProductInventorySolverUnmanagedStockComponent extends ProductInventorySolverComponent {

    function __construct() {
        parent::__construct();
    }

    public function availableQuantity($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $maxLimit = $this->ProductDeal->maxLimit($deal);
        $quantitySold = $this->quantitySold($deal);
        return $maxLimit - $quantitySold;
    }

    public function decrease($deal, $quantity, $userId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $quantity,
            $userId
        ));
    }

}
