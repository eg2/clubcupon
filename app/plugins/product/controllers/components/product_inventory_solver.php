<?php

App::import('Component', 'api.ApiBase');
App::import('Model', 'product.ProductDeal');
App::import('Model', 'product.ProductDealExternal');
App::import('Model', 'product.ProductMovement');

abstract class ProductInventorySolverComponent extends ApiBaseComponent {

    function __construct() {
        parent::__construct('product');
        $this->ProductDeal = new ProductDeal();
        $this->ProductMovement = new ProductMovement();
        $this->ProductDealExternal = new ProductDealExternal();
    }

    protected function quantitySold($deal) {
        $quantitySold = null;
        $productDealExternal = $this->ProductDealExternal->findQuantityGoupByDealId($deal['ProductDeal']['id']);
        if (!empty($productDealExternal)) {
            $quantitySold = $productDealExternal[0][0]['quantity'];
        }
        return $quantitySold;
    }

    abstract function availableQuantity($deal);

    abstract function decrease($deal, $quantity, $userId);

    protected function trackingStockMovements($productId, $action, $oldStock, $newStock, $userId) {
        return $this->ProductMovement->trackingMovements($productId, 'stock', $action, $oldStock, $newStock, $userId);
    }

}
