<?php

App::import('Component', 'product.ProductInventorySolver');
App::import('Model', 'product.ProductProduct');
App::import('Model', 'product.ProductInventory');
App::import('Model', 'product.ProductPin');

class ProductInventorySolverManagedWithBacStockComponent extends ProductInventorySolverComponent {

    function __construct() {
        parent::__construct('product');
        $this->ProductProduct = new ProductProduct();
        $this->ProductInventory = new ProductInventory();
        $this->ProductPin = new ProductPin();
    }

    private function pinStock($deal) {
        return $this->ProductPin->availableQuantity($deal['ProductDeal']['product_product_id']);
    }

    private function inventoryStock($deal) {
        $stock = 0;
        $product = $this->ProductProduct->findById($deal['ProductDeal']['product_product_id']);
        if (!empty($product)) {
            $productInventory = $this->ProductInventory->findById($product['ProductProduct']['product_inventory_id']);
            if (!empty($productInventory)) {
                $stock = floor($productInventory['ProductInventory']['stock'] / $product['ProductProduct']['decremented_units']);
            }
        }
        return $stock;
    }

    public function availableQuantity($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $stocks = array();
        $stocks[] = $this->ProductDeal->maxLimit($deal);
        $stocks[] = $this->inventoryStock($deal);
        if ($deal['ProductProduct']['has_pins']) {
            $stocks[] = $this->pinStock($deal);
        }
        return min($stocks);
    }

    public function decrease($deal, $quantity, $userId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $quantity,
            $userId
        ));
    }

}
