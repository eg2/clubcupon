<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'product.ProductDeal');
App::import('Model', 'product.ProductMovement');
App::import('Model', 'product.ProductCampaign');
App::import('Model', 'product.ProductProduct');
App::import('Model', 'api.ApiCompany');

class ProductServiceComponent extends ApiBaseComponent {

    private static $instance = null;

    function __construct() {
        parent::__construct('product');
        $this->ProductDeal = new ProductDeal();
        $this->ProductMovement = new ProductMovement();
        $this->ProductCampaign = new ProductCampaign();
        $this->ProductProduct = new ProductProduct();
        $this->ApiCompany = new ApiCompany();
    }

    function createProduct($name, $campaignId, $stock, $decrementedUnits, $hasPins, $optional) {
        if (empty($name) || empty($campaignId)) {
            throw new Exception('Nombre de producto o Id de Campaña vacíos');
        }
        $product['ProductProduct']['name'] = $name;
        $product['ProductProduct']['product_campaign_id'] = $campaignId;
        $product['ProductProduct']['stock'] = $stock;
        $product['ProductProduct']['decremented_units'] = $decrementedUnits;
        $product['ProductProduct']['has_pins'] = $hasPins;
        $product['ProductProduct']['product_inventory_id'] = $optional['ProductProduct']['product_inventory_id'];
        if (!empty($optional['ProductProduct'])) {
            $product = array_merge($optional['ProductProduct'], $product['ProductProduct']);
        }
        if (!($this->ProductProduct->save($product))) {
            throw new Exception('No se pudo guardar el producto.');
        }
        return $product;
    }

    function createProductCampaign($companyId, $nameCampaign) {
        $campaing['company_id'] = $companyId;
        $campaing['name'] = $nameCampaign;
        $this->ProductCampaign->save($campaing);
        return $campaing;
    }

    static function instance() {
        if (self::$instance == null) {
            self::$instance = new ProductServiceComponent();
        }
        return self::$instance;
    }

}
