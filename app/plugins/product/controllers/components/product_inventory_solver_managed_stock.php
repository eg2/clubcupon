<?php

App::import('Component', 'product.ProductInventorySolver');
App::import('Model', 'product.ProductProduct');

class ProductInventorySolverManagedStockComponent extends ProductInventorySolverComponent {

    function __construct() {
        parent::__construct();
        $this->ProductProduct = new ProductProduct();
    }

    private function stock($deal) {
        $product = $this->ProductProduct->findById($deal['ProductDeal']['product_product_id']);
        $stock = null;
        if (!empty($product)) {
            $stock = $product['ProductProduct']['stock'];
        }
        return $stock;
    }

    public function availableQuantity($deal) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal
        ));
        $maxLimit = $this->ProductDeal->maxLimit($deal);
        $stock = $this->stock($deal);
        $quantitySold = $this->quantitySold($deal);
        return min($maxLimit - $quantitySold, $stock);
    }

    public function decrease($deal, $quantity, $userId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $quantity
        ));
        $product = $this->ProductProduct->findById($deal['ProductDeal']['product_product_id']);
        $oldStock = $product['ProductProduct']['stock'];
        if (!empty($product)) {
            $product['ProductProduct']['stock'] = $product['ProductProduct']['stock'] - $quantity;
            if ($product['ProductProduct']['stock'] < 0) {
                $product['ProductProduct']['stock'] = 0;
            }
            $this->ProductProduct->save($product);
        }
        $this->ApiLogger->info(__FUNCTION__, array(
            $deal,
            $quantity,
            $product['ProductProduct']['id'],
            $oldStock,
            $product['ProductProduct']['stock'],
            $userId
        ));
        $this->trackingStockMovements($product['ProductProduct']['id'], __METHOD__ . ' - Decrementando Stock', $oldStock, $product['ProductProduct']['stock'], $userId);
    }

}
