<?php

/* SVN FILE: $Id$ */
/* Product Test cases generated on: 2013-09-12 13:09:53 : 1379002913 */
App::import('Model', 'Product.Product');

class ProductTestCase extends CakeTestCase {

    var $Product = null;
    var $fixtures = array(
        'plugin.product.product'
    );

    function startTest() {
        $this->Product = & ClassRegistry::init('Product');
    }

    function testProductInstance() {
        $this->assertTrue(is_a($this->Product, 'Product'));
    }

    function testProductFind() {
        $this->Product->recursive = - 1;
        $results = $this->Product->find('first');
        $this->assertTrue(!empty($results));
        $expected = array(
            'Product' => array(
                'id' => 1,
                'created' => 1,
                'modified' => 1,
                'created_by' => 1,
                'modified_by' => 1,
                'deleted' => 1,
                'company_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'stock' => 1
            )
        );
        $this->assertEqual($results, $expected);
    }

}
