<?php

/* SVN FILE: $Id$ */
/* Product Fixture generated on: 2013-09-12 13:09:53 : 1379002913 */

class ProductFixture extends CakeTestFixture {

    var $name = 'Product';
    var $fields = array(
        'id' => array(
            'type' => 'integer',
            'null' => false,
            'default' => NULL,
            'length' => 20,
            'key' => 'primary'
        ),
        'created' => array(
            'type' => 'timestamp',
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP'
        ),
        'modified' => array(
            'type' => 'timestamp',
            'null' => false,
            'default' => '0000-00-00 00:00:00'
        ),
        'created_by' => array(
            'type' => 'integer',
            'null' => false,
            'default' => NULL,
            'length' => 20
        ),
        'modified_by' => array(
            'type' => 'integer',
            'null' => true,
            'default' => NULL,
            'length' => 20
        ),
        'deleted' => array(
            'type' => 'boolean',
            'null' => false,
            'default' => '0'
        ),
        'company_id' => array(
            'type' => 'integer',
            'null' => false,
            'default' => NULL,
            'length' => 20,
            'key' => 'index'
        ),
        'name' => array(
            'type' => 'string',
            'null' => false,
            'default' => NULL
        ),
        'stock' => array(
            'type' => 'integer',
            'null' => false,
            'default' => '0'
        ),
        'indexes' => array(
            'PRIMARY' => array(
                'column' => 'id',
                'unique' => 1
            ),
            'company_id' => array(
                'column' => 'company_id',
                'unique' => 0
            )
        )
    );
    var $records = array(
        array(
            'id' => 1,
            'created' => 1,
            'modified' => 1,
            'created_by' => 1,
            'modified_by' => 1,
            'deleted' => 1,
            'company_id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'stock' => 1
        )
    );

}
