<?php

class ProductCampaign extends ProductAppModel {

    public $name = 'ProductCampaign';
    public $alias = 'ProductCampaign';
    public $useTable = 'product_campaigns';
    public $actsAs = array(
        'api.SoftDeletable'
    );
    public $belongsTo = array(
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id'
        )
    );
    public $hasMany = array(
        'ProductProduct' => array(
            'className' => 'Product.ProductProduct',
            'foreignKey' => 'product_campaign_id',
            'conditions' => array(
                'deleted' => 0
            )
        )
    );
    var $validate = array(
        'name' => array(
            'rule1' => array(
                'rule' => 'notempty',
                'message' => 'Por favor, ingrese un nombre para la campaña'
            ),
            'rule2' => array(
                'rule' => 'isUnique',
                'message' => 'Ese nombre de campaña ya existe, por favor, ingresa otro'
            )
        ),
        'company_id' => array(
            'rule' => 'notempty',
            'message' => 'Por favor, seleccione una empresa'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        $conditions = array(
            'ProductCampaign.id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

    function findAllByCompanyId($id) {
        $conditions = array(
            'ProductCampaign.company_id' => $id
        );
        return $this->find('all', array(
                    'conditions' => $conditions
        ));
    }

    private function existsAtLeastOneProductForCampaignSql() {
        $atLeastOneProductForCampaignSql = " SELECT 1 FROM product_products AS ProductProduct WHERE ProductProduct.product_campaign_id = ProductCampaign.id ";
        $sql = " (EXISTS ({$atLeastOneProductForCampaignSql}) ) ";
        return DboSource::expression($sql);
    }

    function findAllWithProductsByCompanyId($id) {
        $conditions = array(
            'ProductCampaign.company_id' => $id,
            $this->existsAtLeastOneProductForCampaignSql()
        );
        $order = array(
            'ProductCampaign.name'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

}
