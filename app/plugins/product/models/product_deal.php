<?php

class ProductDeal extends ProductAppModel {

    const DEAL_TRADE_AGREEMENT_ID_PREPURCHASE_WITH_LOGISTICS = 6;

    public $name = 'ProductDeal';
    public $alias = 'ProductDeal';
    public $useTable = 'deals';
    public $belongsTo = array(
        'ProductProduct' => array(
            'className' => 'ProductProduct',
            'foreignKey' => 'product_product_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function maxLimit($deal) {
        $maxLimit = PHP_INT_MAX;
        if ($deal['ProductDeal']['max_limit'] != null) {
            $maxLimit = $deal['ProductDeal']['max_limit'];
        }
        return $maxLimit;
    }

    function findById($id) {
        $fields = array(
            'ProductDeal.id',
            'ProductDeal.max_limit',
            'ProductDeal.product_product_id',
            'ProductDeal.product_inventory_strategy_id',
            'ProductDeal.is_end_user',
            'ProductDeal.deal_trade_agreement_id'
        );
        $conditions = array(
            'ProductDeal.id' => $id
        );
        $contain = array(
            'ProductProduct' => array(
                'fields' => array(
                    'ProductProduct.has_pins',
                    'ProductProduct.stock'
                )
            )
        );
        return $this->find('first', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'contain' => $contain
        ));
    }

    function isEndUser($deal) {
        return $deal['ProductDeal']['is_end_user'];
    }

    function hasAssociatedProduct($deal) {
        return $deal['ProductDeal']['product_product_id'];
    }

    function isPrepurchaseWithOwnLogistics($deal) {
        return $deal['ProductDeal']['deal_trade_agreement_id'] == self::DEAL_TRADE_AGREEMENT_ID_PREPURCHASE_WITH_LOGISTICS;
    }

}
