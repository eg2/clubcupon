<?php

class ProductProduct extends ProductAppModel {

    public $name = 'ProductProduct';
    public $alias = 'ProductProduct';
    public $useTable = 'product_products';
    public $actsAs = array(
        'api.SoftDeletable'
    );
    public $belongsTo = array(
        'ProductCampaign' => array(
            'className' => 'ProductCampaign',
            'foreignKey' => 'product_campaign_id'
        )
    );
    public $hasMany = array(
        'ProductPin' => array(
            'className' => 'Product.ProductPin',
            'foreignKey' => 'product_id',
            'dependent' => true
        )
    );
    public $validate = array(
        'name' => array(
            'rule1' => array(
                'rule' => 'notempty',
                'message' => 'Por favor, ingrese un nombre para el producto'
            ),
            'rule2' => array(
                'rule' => 'isUnique',
                'message' => 'Ese nombre de producto ya existe, por favor, ingresa otro'
            )
        ),
        'stock' => array(
            'rule1' => array(
                'rule' => 'notempty',
                'message' => 'Por favor, ingrese un stock'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'El stock a ingresar, debe ser un valor numerico.'
            ),
            'largo' => array(
                'rule' => array(
                    'maxLength',
                    9
                ),
                'message' => 'El stock solo permite ingresar hasta 9 digitos.'
            )
        ),
        'decremented_units' => array(
            'rule1' => array(
                'rule' => 'notempty',
                'message' => 'Por favor, ingrese una cantidad de unidades.'
            ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'La cantidad de unidades por cupón debe ser un valor numérico.'
            ),
            'rule3' => array(
                'rule' => array(
                    'comparison',
                    '>=',
                    1
                ),
                'message' => 'La cantidad de unidades debe mayor o igual a 1.'
            ),
            'rule4' => array(
                'rule' => '/^[0-9]*$/',
                'message' => 'La cantidad de unidades debe ser un número natural.'
            ),
            'rule5' => array(
                'rule' => array(
                    'between',
                    1,
                    7
                ),
                'message' => 'La cantidad de unidades debe ser un número de hasta 7 dígitos.'
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        $conditions = array(
            'ProductProduct.id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

    function findAllByCampaignId($id) {
        $conditions = array(
            'ProductProduct.product_campaign_id' => $id
        );
        return $this->find('all', array(
                    'conditions' => $conditions
        ));
    }

    function findAllSelectablesByCampaignId($id) {
        $products_with_pins = "select distinct Pins.product_id from pins as Pins where Pins.is_used = 0 and Pins.product_id is not null";
        $query = "select ProductProduct.* from product_products as ProductProduct where ProductProduct.deleted=0 and ProductProduct.product_campaign_id={$id} and (ProductProduct.has_pins = 0 or (ProductProduct.has_pins = 1 and ProductProduct.id in ({$products_with_pins})))";
        return $this->query($query);
    }

    function deleteAllByCampaignId($campaignId, $userId) {
        $fields = array(
            'ProductProduct.deleted' => 1,
            'ProductProduct.modified_by' => $userId
        );
        $conditions = array(
            'ProductProduct.product_campaign_id' => $campaignId
        );
        return $this->updateAll($fields, $conditions);
    }

    function _isInteger() {
        return (bool) preg_match("/^d+(.d{1,6})?$/'", $this->data[$this->name]['stock']);
    }

    function hasProductInventory($productProduct) {
        return $productProduct['ProductProduct']['product_inventory_id'];
    }

}
