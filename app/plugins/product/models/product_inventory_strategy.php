<?php

class ProductInventoryStrategy extends ProductAppModel {

    const ID_UNMANAGED_STOCK = 1;
    const ID_MANAGED_STOCK = 2;
    const ID_MANAGED_WITH_PINS_STOCK = 3;
    const ID_MANAGED_WITH_PINS_AND_BAC_STOCK = 4;

    public $name = 'ProductInventoryStrategy';
    public $alias = 'ProductInventoryStrategy';
    public $useTable = 'product_inventory_strategies';
    public $actsAs = array(
        'api.SoftDeletable',
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        $conditions = array(
            'ProductInventoryStrategy.id' => $id,
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

}
