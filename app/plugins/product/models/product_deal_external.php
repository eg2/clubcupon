<?php

class ProductDealExternal extends ProductAppModel {

    public $name = 'ProductDealExternal';
    public $alias = 'ProductDealExternal';
    public $useTable = 'deal_externals';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findQuantityGoupByDealId($dealId) {
        $fields = array(
            'SUM(quantity) as quantity'
        );
        $conditions = array(
            'ProductDealExternal.deal_id' => $dealId,
            'ProductDealExternal.external_status' => array(
                'P',
                'A'
            )
        );
        $group = array(
            'ProductDealExternal.deal_id'
        );
        return $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $conditions,
                    'group' => $group
        ));
    }

}
