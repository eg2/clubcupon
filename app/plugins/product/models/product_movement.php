<?php

class ProductMovement extends ProductAppModel {

    public $name = 'ProductMovement';
    public $alias = 'ProductMovement';
    public $useTable = 'product_movements';
    public $actsAs = array(
        'api.SoftDeletable'
    );
    public $belongsTo = array(
        'Products' => array(
            'className' => 'ProductProduct',
            'foreignKey' => 'product_id'
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function trackingMovements($productId, $field, $action, $oldStock, $newStock, $userId) {
        $movement['ProductMovement'] = array(
            'product_id' => $productId,
            'action' => $action,
            'field' => $field,
            'old_value' => $oldStock,
            'new_value' => $newStock,
            'user_id' => $userId
        );
        if ($this->save($movement)) {
            $movement['ProductMovements']['id'] = $this->getInsertID();
        } else {
            $movement = null;
        }
        return $movement;
    }

}
