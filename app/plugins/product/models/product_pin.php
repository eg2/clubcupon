<?php

class ProductPin extends ProductAppModel {

    public $name = 'ProductPin';
    public $alias = 'ProductPin';
    public $useTable = 'pins';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function findById($id) {
        $fields = array(
            'ProductPin.id',
            'ProductPin.stock',
            'ProductPin.has_pins'
        );
        $conditions = array(
            'ProductPin.id' => $id
        );
        return $this->find('first', array(
                    'fields' => $fields,
                    'conditions' => $conditions
        ));
    }

    function availableQuantity($product_id) {
        $conditions = array(
            'ProductPin.product_id' => $product_id,
            'ProductPin.is_used' => 0
        );
        $count_availables_pins = $this->find('count', array(
            'conditions' => $conditions
        ));
        if (!isset($count_availables_pins)) {
            $count_availables_pins = 0;
        }
        return $count_availables_pins;
    }

    public static function getTotalPinsByProductId($product_id) {
        $conditions = array(
            'ProductPin.product_id' => $product_id
        );
        $product_pin = & new ProductPin();
        return $product_pin->find('count', array(
                    'conditions' => $conditions
        ));
    }

}
