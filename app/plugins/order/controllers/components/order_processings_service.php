<?php

App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));
App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));
App::import('Component', 'api.ApiBase');
App::import('Model', 'api.ApiAttachment');
App::import('Model', 'api.ApiDeal');
App::import('Model', 'api.ApiCity');
App::import('Model', 'api.ApiPaymentOption');
App::import('Model', 'api.ApiPaymentOptionDeal');
App::import('Model', 'api.ApiPaymentPlanItem');
App::import('Model', 'api.ApiPaymentType');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiUserProfile');
App::import('Model', 'api.ApiCompany');
App::import('Model', 'api.ApiDealExternal');

class OrderProcessingsServiceComponent extends ApiBaseComponent {

    public $components = array(
        'product.ProductInventoryService',
        'api.ApiBuyService',
        'api.ApiSubscriptionsService'
    );

    function __construct() {
        parent::__construct('order');
        $this->ApiAttachment = new ApiAttachment();
        $this->ApiDeal = new ApiDeal();
        $this->ApiCity = new ApiCity();
        $this->ApiPaymentOption = new ApiPaymentOption();
        $this->ApiPaymentOptionDeal = new ApiPaymentOptionDeal();
        $this->ApiPaymentPlanItem = new ApiPaymentPlanItem();
        $this->ApiPaymentType = new ApiPaymentType();
        $this->ApiUser = new ApiUser();
        $this->ApiUserProfile = new ApiUserProfile();
        $this->ApiCompany = new ApiCompany();
        $this->ApiDealExternal = new ApiDealExternal();
        $fileLogPrefix = 'api';
        $this->ApiLogger = new ApiLogger(get_class($this), $fileLogPrefix);
    }

    private function addHasStockSubDeal($deals) {
        foreach ($deals as $i => $subdeal) {
            $deals[$i] = $this->addHasStock($subdeal);
        }
        return $deals;
    }

    private function addHasStock($deal) {
        $availableQuantity = $this->ProductInventoryService->availableQuantity($deal['Deal']['id']);
        $deal['has_stock'] = ($availableQuantity > 0);
        return $deal;
    }

    public function findAllSubdealByParentDealId($parentDealId) {
        $deal = array(
            'Deal' => array(
                'id' => $parentDealId
            )
        );
        $subdeals = $this->ApiDeal->findSubDealsForApi($deal);
        $subdeals = $this->addHasStockSubDeal($subdeals);
        return $subdeals;
    }

    public function findDealById($id) {
        $conditions = array(
            'Deal.id' => $id,
            'Deal.deal_status_id' => array(
                ApiDeal::STATUS_OPEN,
                ApiDeal::STATUS_TIPPED
            ),
            'Deal.publication_channel_type_id' => ApiDeal::PUBLICATION_CHANNEL_TYPE_WEB
        );
        $deal = $this->ApiDeal->find('first', array(
            'conditions' => $conditions
        ));
        $deal['Attachment'] = $this->ApiAttachment->findFirstAttachementForDeal($deal);
        $deal = $this->addHasStock($deal);
        if (empty($deal['Attachment'])) {
            unset($deal['Attachment']);
        }
        return $deal;
    }

    private function generateLogoFileNameForEachPaymentMethod($paymentOptions) {
        foreach ($paymentOptions as $key => $value) {
            $logoFileName = strtolower(str_replace(' ', '_', $paymentOptions[$key]['PaymentOption']['name']));
            $paymentOptions[$key]['PaymentOption']['logo'] = $logoFileName . '.gif';
        }
        return $paymentOptions;
    }

    public function findAllPaymentOptionByDealId($dealId) {
        $paymentOptions = $this->ApiPaymentOptionDeal->findPaymentOptionsByDeal($dealId);
        $paymentOptions = $this->generateLogoFileNameForEachPaymentMethod($paymentOptions);
        if (!empty($paymentOptions)) {
            foreach ($paymentOptions as $key => $value) {
                $paymentOptions[$key]['PaymentPlanItem'] = $this->ApiPaymentPlanItem->getPaymentInstallments($value['PaymentOptionDeal']['payment_plan_id']);
                unset($paymentOptions[$key]['PaymentOptionDeal']);
            }
        }
        return $paymentOptions;
    }

    public function findUserByEmail($email) {
        $conditions = array(
            'User.email' => $email
        );
        return $this->ApiUser->findByEmail($conditions);
    }

    public function isRegisteredUser($email) {
        $user = $this->findUserByEmail($email);
        return !empty($user);
    }

    public function updateUserProfileDniByUserId($dni, $userId) {
        $fields = array(
            'UserProfile.dni' => $dni
        );
        $conditions = array(
            'UserProfile.user_id' => $userId
        );
        $this->ApiUserProfile->updateAll($fields, $conditions);
    }

    public function isBuyWithWallet($paymentOptionId) {
        $isBuyWithWallet = false;
        $paymentOption = $this->ApiPaymentOption->findById($paymentOptionId);
        if (!empty($paymentOption)) {
            $isBuyWithWallet = $this->ApiPaymentType->isWallet($paymentOption['PaymentOption']['payment_type_id']);
        }
        return $isBuyWithWallet;
    }

    public function isGatewayPayment($paymentOptionId) {
        return !$this->isBuyWithWallet($paymentOptionId) && !empty($paymentOptionId);
    }

    public function isEnabledToBuy($userId, $discountedPrice, $quantity, $paymentOptionId, $pin) {
        $isCreditEnough = $this->ApiBuyService->isCreditEnough($userId, $discountedPrice, $quantity, $paymentOptionId, $pin);
        $isGatewayPayment = $this->isGatewayPayment($paymentOptionId);
        return $isCreditEnough || $isGatewayPayment;
    }

    private function isWalletRemovable($dealId, $userId, $selectedPaymentOptionId) {
        $isWalletRemovable = false;
        $deal = $this->ApiDeal->findRowById($dealId);
        if (!empty($deal)) {
            if ($this->ApiDeal->isPrepurchaseWithOwnLogistics($deal)) {
                $isWalletRemovable = true;
            } else {
                $user = $this->ApiUser->findById($userId);
                if (!empty($user)) {
                    $isWalletRemovable = !$this->ApiUser->isWalletEnabled($user);
                    if (!empty($selectedPaymentOptionId)) {
                        $isWalletRemovable = $isWalletRemovable || $this->isBuyWithWallet($selectedPaymentOptionId);
                    }
                }
            }
        }
        return $isWalletRemovable;
    }

    private function removeWallet($paymentOptions) {
        foreach ($paymentOptions as $i => $paymentOption) {
            if ($this->ApiPaymentType->isWallet($paymentOption['PaymentOption']['payment_type_id'])) {
                unset($paymentOptions[$i]);
            }
        }
        return $paymentOptions;
    }

    public function findAllPaymentOption($dealId, $userId, $selectedPaymentOptionId) {
        $paymentOptions = $this->findAllPaymentOptionByDealId($dealId);
        if (!empty($paymentOptions)) {
            if ($this->isWalletRemovable($dealId, $userId, $selectedPaymentOptionId)) {
                $paymentOptions = $this->removeWallet($paymentOptions);
            }
        }
        return $paymentOptions;
    }

    public function subscribeUserToCity($userId, $cityId, $ip) {
        $user = $this->ApiUser->findById($userId);
        if (!empty($user)) {
            $city = $this->ApiCity->findById($cityId);
            if (!empty($city)) {
                $parameters['city_id'] = $city['City']['id'];
                $this->ApiSubscriptionsService->suscribe($parameters, $user, $ip);
            }
        }
    }

    public function findDealByIdForBuyPending($dealId) {
        $deal = $this->findDealById($dealId);
        $deal['Company'] = $this->ApiCompany->read(array(
            'Company.name'
                ), $deal['Deal']['company_id']);
        $deal['City'] = $this->ApiCity->findById($deal['Deal']['city_id']);
        return $deal;
    }

    public function findDealExternalByIdAndUserId($userId, $dealExternalId) {
        $dealExternal = $this->ApiDealExternal->findbyIdAndUser($userId, $dealExternalId);
        if (!$userId || $dealExternalId === NULL || empty($dealExternal)) {
            $this->cakeError('error404');
        }
        return $dealExternal;
    }

    public function findSlugByDeal($deal) {
        if ($this->ApiDeal->isSubDeal($deal)) {
            $deal = $this->ApiDeal->findById($deal['Deal']['parent_deal_id']);
            $slug = $deal['Deal']['slug'];
        } else {
            $slug = $deal['Deal']['slug'];
        }
        return $slug;
    }

    public function notifyClarin365($deal, $dealExternal, $data) {
        if ($deal['Deal']['is_clarin365_deal']) {
            $importe = $deal['Deal']['original_price'] * $dealExternal['DealExternal']['quantity'];
            $importeDscto = ($importe - $deal['Deal']['discounted_price'] * $dealExternal['DealExternal']['quantity']);
            $date = date_create($dealExternal['DealExternal']['created']);
            $fechaHora = date_format($date, 'YmdHis');
            $importe = floatval($importe);
            $importeDscto = floatval($importeDscto);
            $data = array(
                "idSucursal" => 1,
                "idBeneficio" => 1,
                "fechaHora" => $fechaHora,
                "nroCredencial" => $data['Deal']['clarin_affiliate_number'],
                "tipoTransaccion" => "V",
                "importe" => $importe,
                "importeDescuento" => $importeDscto,
                "verificarCredencial " => 0
            );
            $dataObj = json_encode($data);
            $url = Configure::read('clarin365.tracking_url');
            $this->ApiLogger->debug(__METHOD__, 'url:' . $url);
            $url.= "?q=" . $dataObj;
            $this->ApiLogger->debug(__METHOD__, 'url:' . $url);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FAILONERROR, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            if (!curl_errno($ch)) {
                curl_close($ch);
            } else {
                $this->ApiLogger->debug(__METHOD__, 'error:' . curl_error($ch));
            }
            $this->ApiLogger->debug(__METHOD__, 'result:' . $result);
        }
    }

}
