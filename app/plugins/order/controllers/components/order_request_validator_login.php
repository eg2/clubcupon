<?php

App::import('Component', 'order.OrderRequestValidator');
App::import('Model', 'api.ApiUser');

class OrderRequestValidatorLoginComponent extends OrderRequestValidatorComponent {

    function __construct($data, $userId) {
        parent::__construct($data, $userId);
        $this->ApiUser = new ApiUser();
    }

    function validate() {
        $errors = array();
        $this->data['User']['password'] = $this->data['User']['passwd'];
        $this->ApiUser->set($this->data['User']);
        unset($this->ApiUser->validate['email']['rule3']);
        if (!$this->ApiUser->validates()) {
            $errors = $this->ApiUser->validationErrors;
        }
        return $errors;
    }

}
