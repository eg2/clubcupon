<?php

App::import('Component', 'order.OrderRequestValidator');
App::import('Model', 'api.ApiUser');
App::import('Model', 'api.ApiUserProfile');

class OrderRequestValidatorRegisterComponent extends OrderRequestValidatorComponent {

    function __construct($data, $userId) {
        parent::__construct($data, $userId);
        $this->ApiUser = new ApiUser();
        $this->ApiUserProfile = new ApiUserProfile();
    }

    private function checkConfirmPassword() {
        $errors = array();
        if ($this->data['User']['passwd'] != $this->data['User']['confirm_password']) {
            $errors = array(
                'confirm_password' => 'Las contraseñas no coinciden, por favor, volvé a escribirlas'
            );
        }
        return $errors;
    }

    private function checkConfirmEmail() {
        $errors = array();
        if ($this->data['User']['confirm_email'] != $this->data['User']['email']) {
            $errors = array(
                'confirm_email' => 'El Email no coincide, por favor, volvé a escribirlo'
            );
        }
        if (empty($this->data['User']['confirm_email'])) {
            $errors = array(
                'confirm_email' => 'Ingresá la confirmación del mail.'
            );
        }
        return $errors;
    }

    private function isValidPasswordLength($password) {
        return strlen($password) > 5;
    }

    function validate() {
        $errors = array();
        $confirmPasswordError = $this->checkConfirmPassword();
        $confirmEmailError = $this->checkConfirmEmail();
        $this->data['User']['password'] = $this->data['User']['passwd'];
        $this->ApiUser->set($this->data['User']);
        $this->ApiUserProfile->set($this->data['UserProfile']);
        $registerValidateFieldList = array(
            'fieldList' => array(
                'username',
                'email',
                'password'
            )
        );
        $isOkUserProfile = $this->ApiUserProfile->validates();
        $isOkUser = $this->ApiUser->validates($registerValidateFieldList);
        if (!($isOkUserProfile && $isOkUser)) {
            $errors = array_merge($this->ApiUser->validationErrors, $this->ApiUserProfile->validationErrors);
        }
        if (!$this->isValidPasswordLength($this->data['User']['passwd'])) {
            $errors = array_merge(array(
                'password' => 'Mínimo 6 caracteres'
                    ), $errors);
        }
        if (empty($this->data['User']['confirm_password'])) {
            $errors = array_merge(array(
                'confirm_password' => 'Reingresá una contraseña'
                    ), $errors);
        }
        if (!$this->isValidPasswordLength($this->data['User']['confirm_password'])) {
            $errors = array_merge(array(
                'confirm_password' => 'Mínimo 6 caracteres'
                    ), $errors);
        }
        if ($confirmPasswordError) {
            $errors = array_merge($confirmPasswordError, $errors);
        }
        if ($confirmEmailError) {
            $errors = array_merge($confirmEmailError, $errors);
        }
        if (empty($this->data['User']['is_agree_terms_conditions'])) {
            $errors = array_merge(array(
                'is_agree_terms_conditions' => 'Debes aceptar los t&eacute;rminos y condiciones.'
                    ), $errors);
        }
        return $errors;
    }

}
