<?php

App::import('Component', 'api.ApiGiftPinService');
App::import('Component', 'order.OrderRequestValidator');
App::import('Component', 'product.ProductInventoryService');
App::import('Model', 'api.ApiUser');
App::import('Model', 'shipping.ShippingAddressUser');

class OrderRequestValidatorSelectPaymentOptionComponent extends OrderRequestValidatorComponent {

    function __construct($data, $userId, $deal) {
        parent::__construct($data, $userId);
        $this->ApiUser = new ApiUser();
        $this->ApiGiftPinService = new ApiGiftPinServiceComponent();
        $this->ProductInventoryService = new ProductInventoryServiceComponent();
        $this->deal = $deal;
    }

    private function isStockAvailable() {
        $availableQuantity = $this->ProductInventoryService->availableQuantity($this->deal['Deal']['id']);
        return $availableQuantity >= $this->data['quantity'];
    }

    private function isDiscountCodeSubmitted() {
        return !empty($this->data['Deal']['discount_code']);
    }

    private function validateDiscountCode($discountCode) {
        $errors = array();
        try {
            $pin = $this->ApiGiftPinService->isUsable($discountCode);
        } catch (Exception $e) {
            $errors = array(
                'discount_code' => 'Error en Consulta del código Pin.'
            );
        }
        if (!$pin) {
            $errors = array(
                'discount_code' => 'El código no existe o ya fue utilizado.'
            );
        }
        return $errors;
    }

    private function isClarin365Required() {
        return $this->deal['Deal']['is_clarin365_deal'];
    }

    private function isValidClarinAffiliateNumber($number) {
        $url = Configure::read('clarin365.verification_url');
        $url.= "?q={\"nroCredencial\":\"{$number}\"}";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        if (!curl_errno($ch)) {
            curl_close($ch);
        } else {
            $result = "ERROR";
        }
        return substr($result, 0, 2) == 'OK';
    }

    private function isShippingAddressRequired() {
        return $this->deal['Deal']['is_shipping_address'];
    }

    private function isShippingAddressSubmitted($shippingAddressId) {
        return !empty($shippingAddressId);
    }

    private function isQuantitySubmitted() {
        return !empty($this->data['quantity']);
    }

    private function isGiftSubmitted() {
        !empty($this->data['Deal']['gift_options']['is_gift']);
    }

    private function validateGift() {
        $errors = array();
        if (empty($this->data['Deal']['gift_options']['to'])) {
            $errors = array_merge(array(
                'to' => 'Debés completar el nombre de tu amigo'
                    ), $errors);
        }
        if (empty($this->data['Deal']['gift_options']['message'])) {
            $errors = array_merge(array(
                'message' => 'Debés colocar un mensaje'
                    ), $errors);
        }
        if (!empty($this->data['Deal']['gift_options']['dni']) && !is_numeric($this->data['Deal']['gift_options']['dni'])) {
            $errors = array_merge(array(
                'dni' => 'Sólo admite números'
                    ), $errors);
        }
        return $errors;
    }

    private function isShippingAddressUserRequired() {
        return $this->deal['Deal']['is_shipping_adress_user'];
    }

    private function validateShippingAddressUser() {
        $errors = array();
        if (empty($this->data['Deal']['adress_state_id'])) {
            $errors = array_merge(array(
                'adress_state_id' => 'Seleccioná una provincia'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_CITY_NAME, $this->data['Deal']['adress_city_name'])) {
            $errors = array_merge(array(
                'adress_city_name' => 'Sólo admite letras, números o espacios'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_STREET, $this->data['Deal']['adress_street'])) {
            $errors = array_merge(array(
                'adress_street' => 'Sólo admite letras, números o espacios'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_NUMBER, $this->data['Deal']['adress_number'])) {
            $errors = array_merge(array(
                'adress_number' => 'Sólo admite números'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_FLOOR_NUMBER, $this->data['Deal']['adress_floor_number'])) {
            $errors = array_merge(array(
                'adress_floor_number' => 'Sólo admite letras y números'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_APARTMENT, $this->data['Deal']['adress_apartment'])) {
            $errors = array_merge(array(
                'adress_apartment' => 'Sólo admite letras y números'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_POSTAL_CODE, $this->data['Deal']['adress_postal_code'])) {
            $errors = array_merge(array(
                'adress_postal_code' => 'Sólo admite un CPA válido o un código de 4 numeros'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_TELEPHONE, $this->data['Deal']['adress_telephone'])) {
            $errors = array_merge(array(
                'adress_telephone' => 'Sólo admite entre 7 y 13 dígitos'
                    ), $errors);
        }
        if (!preg_match(ShippingAddressUser::VALIDATE_DETAILS, $this->data['Deal']['adress_details'])) {
            $errors = array_merge(array(
                'adress_details' => 'Sólo admite letras, números o espacios'
                    ), $errors);
        }
        return $errors;
    }

    function validate() {
        $errors = array();
        if ($this->isDiscountCodeSubmitted()) {
            $discountCodeErrors = $this->validateDiscountCode($this->data['Deal']['discount_code']);
            $errors = array_merge($discountCodeErrors, $errors);
        }
        if (!$this->isQuantitySubmitted()) {
            $errors = array_merge(array(
                'quantity' => 'Cantidad a comprar debe ser mayor a 0'
                    ), $errors);
        }
        if ($this->isGiftSubmitted()) {
            $giftErrors = $this->validateGift();
            $errors = array_merge($giftErrors, $errors);
        }
        if ($this->isClarin365Required()) {
            if (!$this->isValidClarinAffiliateNumber($this->data['Deal']['clarin_affiliate_number'])) {
                $errors = array_merge(array(
                    'clarin_affiliate_number' => 'El número de tarjeta Clarín 365 es inválido'
                        ), $errors);
            }
        }
        if ($this->isShippingAddressRequired()) {
            if (!$this->isShippingAddressSubmitted($this->data['Deal']['shipping_address_id'])) {
                $errors = array_merge(array(
                    'select_shipping_address' => 'Seleccioná un Punto de Retiro.'
                        ), $errors);
            }
        }
        if (!$this->isStockAvailable()) {
            $errors = array_merge(array(
                'quantity' => 'Momentáneamente sin stock'
                    ), $errors);
        }
        //
        if ($this->isShippingAddressUserRequired()) {
            $shippingAddressUserErrors = $this->validateShippingAddressUser();
            $errors = array_merge($shippingAddressUserErrors, $errors);
        }
        //
        return $errors;
    }

}
