<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'order.OrderRequestValidatorLogin');
App::import('Component', 'order.OrderRequestValidatorRegister');
App::import('Component', 'order.OrderRequestValidatorSelectPaymentOption');

class OrderRequestValidatorFactoryComponent extends ApiBaseComponent {

    function loginValidator($data, $userId) {
        return new OrderRequestValidatorLoginComponent($data, $userId);
    }

    function registerValidator($data, $userId) {
        return new OrderRequestValidatorRegisterComponent($data, $userId);
    }

    function selectPaymentOptionValidator($data, $userId, $deal) {
        return new OrderRequestValidatorSelectPaymentOptionComponent($data, $userId, $deal);
    }

}
