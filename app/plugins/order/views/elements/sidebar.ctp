<?php if (count($side_deals)) { ?>
    <div class="pull-right sidebar">
        <h4>
            M&aacute;s Ofertas
        </h4>
        <?php

            foreach($side_deals as $idx => $side_deal) {

                $name = (strlen($side_deal['Deal']['name']) > 14) ? substr($side_deal['Deal']['name'],0,14).'...<br />' : $side_deal['Deal']['name'];
                $slugUrl = '/' . $side_deal['City']['slug'] . '/deal/' . $side_deal['Deal']['slug'];

                if($side_deal['Deal']['only_price']){

                   $title = '<span>$'.$side_deal['Deal']['discounted_price'].'</span>';
                   $description = 'Pag&aacute; s&oacute;lo $'.$side_deal['Deal']['discounted_price'];

                } elseif($side_deal['Deal']['hide_price']){

                   $title = '<span>'.$side_deal['Deal']['discount_percentage'].'%</span> de descuento';
                   $description = $side_deal['Deal']['discount_percentage'] . '% de descuento ';

                } else {

                    $title = '<span>$'.$side_deal['Deal']['discounted_price'].'</span> en vez de $'.$side_deal['Deal']['original_price'];
                    $description = 'Pag&aacute; s&oacute;lo $'.$side_deal['Deal']['discounted_price'] . ' y obten&eacute; un descuento de ' . $side_deal['Deal']['discount_amount'] ;

                }
                $linkConfiguration = array('controller' => 'deals', 'action' => 'view', $side_deal['Deal']['slug'], 'plugin' =>'');
                $linkFormatOptions = array('title' => $side_deal['Deal']['name']);
                $imageLinkFormatOptions = array('title' => $side_deal['Deal']['name'], 'class' =>'imageLink' );
                $imageFormatOptions = array('dimension' => 'small_big_thumb', 'alt' => $side_deal['Company']['name'], 'title' => $side_deal['Company']['name'] );
                
        ?>
        
        <div class="dealBlock">

            <!-- Titulo -->
            <p class="title">
                <?php echo $html->link($title, $linkConfiguration, $linkFormatOptions, null, false); ?>
            </p>

            <!-- Imagen -->
            <div class="image">
                <?php echo $html->link($html->showImage('Deal', $side_deal['Attachment'], $imageFormatOptions), $linkConfiguration, $imageLinkFormatOptions, null, false); ?>
<!--                <a href="#" class=""><img src="#" width="209" height="139"></a>-->
                <div class="veil">
                    <?php echo $html->link('Ver m&aacute;s', $linkConfiguration, $linkFormatOptions, null, false); ?>
                </div>
            </div>

            <!-- Descripcion -->
            <div class="description">
                <?php echo $html->link($description, $linkConfiguration, $linkFormatOptions, null, false); ?>
                <span class="nombre_oferta"><?php echo $name; ?></span>
            </div>

        </div>
    <?php } ?>

    </div>


    <script>
        $('.image a.imageLink').hover(function(){
            $(this).parent().find('.veil').show();
        })
        $('.veil').mouseout(function(){
            $(this).hide();
        })
        $('.veil a').hover(function(){
            $(this).parent().show();
        })
    </script>
    
<?php } ?>