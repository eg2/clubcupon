<?php

    $totalPurchaseAmount = $this->data['quantity']*$deal['Deal']['discounted_price'];
    $userAvailableBalanceAmount = $user['User']['available_balance_amount'];
    $purchaseDifference = $totalPurchaseAmount - $userAvailableBalanceAmount - $pin['monto'];
    
?>

<div class="SELECT_PAYMENT_OPTION bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
            <span>Abonar diferencia</span>
        </div>
    </div>
    <div class="combined-payment payment-group">
        <h2>
            El monto a abonar supera su cr&eacute;dito en Cuenta Club Cup&oacute;n,
            debe pagar la diferencia mediante uno de los medios de pago alternativos<br />
        </h2>
        <p>
            <strong>Monto a abonar:</strong> $<?php echo $totalPurchaseAmount; ?>
            <br />
            <?php if(!empty($pin['monto'])) { ?>
                <strong>Monto a pagar por C&oacute;digo de Descuento:</strong> $<?php echo $pin['monto']; ?>
                <br />
            <?php } ?>
            <strong>Monto a pagar con Cuenta Club Cup&oacute;n:</strong> $<?php echo $userAvailableBalanceAmount; ?>
            <br />
            <strong>Diferencia a pagar por medio de pago:</strong> $<?php echo $purchaseDifference; ?>
        </p>
        
        <h2>Forma de pago con la cual desea abonar la diferencia:</h2>
        
        <div class="payment-options">
            <input type="hidden" name="data[Deal][is_combined]"  value="<?php echo $is_combined;?>">
            <input type="hidden" name="data[Deal][discount_code]"  value="<?php echo $discount_code;?>">
            <input type="hidden" name="data[quantity]" id="quantity" value="<?php echo $this->data['quantity'];?>">
            <ul>
                <?php foreach($paymentOptions as $paymentOption) { ?>
                    <li>
                        <div>
                            <input type="hidden" name="data[Deal][payment_type_id]" id="DealPaymentTypeId_" value="" checked="checked">
                            <input type="radio" name="data[Deal][payment_option_id]" id="DealPaymentTypeId220120" class="js-payment-type cardLogo" value="<?php echo $paymentOption['PaymentOption']['id']; ?>" >
                            <?php echo $html->image ('/img/card_logos/'.$paymentOption['PaymentOption']['logo'], array ('alt' => $paymentOption['PaymentOption']['name'])); ?>
                            <?php echo $paymentOption['PaymentOption']['name']; ?>
                            <?php if(count ($paymentOption['PaymentPlanItem'])) {?>
                                <select class="paymentPlans" name="data[Deal][payment_plan_item_id]">
                                    <?php foreach($paymentOption['PaymentPlanItem'] as $paymentPlans) { ?>
                                        <option  id="payment_plan_item_id_<?php echo $paymentOption['PaymentOption']['id'] ?>" value="<?php echo $paymentPlans['ApiPaymentPlanItem']['id']; ?>"><?php echo $paymentPlans['ApiPaymentPlanItem']['name']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </div>
                    </li>
                <?php } ?>

            </ul>
            <div class="bottom">
                <div class="pull-right">
                    <input id="btnPagar" type="submit" class="subm" value="Pagar">
                </div>
            </div>
            
        </div>
    
    </div>
    
</div>
<script type="text/javascript">
  $(document).ready(function() {
    //check almost a payment method
    var primerMedioDePagoSeleccionado = $(".SELECT_PAYMENT_OPTION .payment-group input[type='radio']:first");
    $(primerMedioDePagoSeleccionado).attr("checked", "checked");
    $(primerMedioDePagoSeleccionado).parent().find('select').toggle();
    
    //Manejo de selects en las opciones de pago 
    $(".js-payment-type").change(function(){ 
        hideSelects();
        var parentTag = $(this).parent().get(0);
        $(parentTag).find('select').toggle();
    });
        
    function hideSelects() {
        $('.paymentPlans').hide();
    }   
    // disable button submit
    $('#Buy').submit(function() {
        $('#btnPagar').attr('disabled', 'disabled');
        $('#btnPagar').css('font-size', '10px');
        $('#btnPagar').css('padding-left','17px');
        $('#btnPagar').attr('value','Enviando');
    });
  });
</script>