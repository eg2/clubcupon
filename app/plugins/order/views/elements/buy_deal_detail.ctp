<div class="BUY_DEAL_DETAIL bloque-modelo ">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner tucompra">
            <span>Tu compra</span>
        </div>
    </div>
    <div class="detalle pull-left">
        <table>
            <thead>
                <tr>
                    <th class="col1">
                        <span>Descripci&oacute;n</span>
                    </th>
                    <th class="col2">
                        <span>Cantidad</span>
                    </th>
                    <th class="col3">
                        <span>Precio por cup&oacute;n</span>
                    </th>
                    <th class="col4">
                        <span>Precio Total</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col1">
                        <div class="pull-left">
                            <?php echo $html->showImage('Deal', $deal['Attachment']['Attachment'], array('id' => 'deal_image', 'dimension' => 'medium_thumb', 'alt' => $html->cText($deal['Company']['name'] . ' ' . $deal['Deal']['name'], false), 'title' => $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false))); ?>
                        </div>
                        <div class="pull-left dealName">
                            <?php echo $orderFormatStrings->shortenStringsWithHtmlEntities($deal['Deal']['name'], 60); ?>
                        </div>
                    </td>
                    <td class="col2">
                        <?php echo $form->input ('quantity', array ('value'=>$this->data['quantity'],'label' => false, 'class' => 'dealQuantity', 'name'=>'data[quantity]','maxlength'=>4)); ?>
                        <?php if($errors['quantity']){ ?>
                          <div class="error-message" >
            	            <?php echo $errors['quantity']; ?>
        	          </div>
		        <?php } ?>
                        <?php echo $form->hidden ('original_quantity', array ('value'=>$deal['Deal']['min_limit'])); ?>
                        <p class="errors"></p>
                        <?php echo '<p class="minmax">M&iacute;nimo: ' . $min_quantity . '</p>'; ?>
                        <?php if (!empty ($max_quantity)){ echo '<p class="minmax">M&aacute;ximo: ' . $max_quantity . '</p>'; } ?>
                    </td>
                    <td class="col3">
                        $<?php echo intval($deal['Deal']['discounted_price']); ?>
                    </td>
                    <td class="col4">
                        <span>$</span>
                        <span class="js-deal-total"><?php echo $deal['Deal']['discounted_price']; ?></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="cocarda pull-right">
        <p>Ahorraste</p>
        <p class="precionum"><?php echo $deal['Deal']['discount_percentage'];?></p>
        <span>%</span>
    </div>
    
    <?php if($isLoggedUser) { ?>
        <div class="couponCodePin" style="display: block;">
			<label for="discount_code" >Tengo un c&oacute;digo de descuento</label>
			<?php echo $form->input('discount_code', array('type'=> 'text', 'class'=>'discount_code', 'id'=>'discount_code', 'label' =>false)); ?>
        <?php if($errors['discount_code']){ ?>
            <div class="error-message" >
                El c&oacute;digo de descuento ingresado no es v&aacute;lido
            	<?php echo $errors['discount_code']; ?>
        	</div>
		<?php } ?>
        </div>
    <?php } ?>
    
    <?php if($display_365_dialog) { ?>
        <div class="clarin365">
            <?php echo $html->image ('/img/chapa_clarin365.png', array('alt'=>'Oferta exclusiva Clarin 365', 'name'=>'oferta exclusiva Clarin 365', 'class'=> 'logo pull-right')); ?>
            <table>
                <tr>
                    <td colspan="2">
                        <strong>Esta oferta es exclusiva para socios de Clar&iacute;n 365</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="discount_code" >N&uacute;mero de socio</label>
                    </td>
                    <td>
                        <?php echo $form->input('clarin_affiliate_number', array('type'=> 'text', 'class'=>'clarin_365_account_code', 'label' =>false)); ?>
                    </td>
                </tr>
                <tr class="clarin365Error">
                    <td colspan="2">
                        <strong>
                            El n&uacute;mero de socio ingresado no es v&aacute;lido
                        </strong>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
    <div class="clearFloats"></div>
</div>

<script>
    
    /* $('.togglePinDialogBox').click(function(){
        disablePaymentOptions();
        toggleCouponCodeBoxes();
    });
    $('.couponCodePin .cancel').click(function(){
        enablePaymentOptions();
        toggleCouponCodeBoxes();
    });*/
    
    function toggleCouponCodeBoxes(){
        $('#discount_code').val('');
        $('.couponCodePin').toggle();
        $('.togglePinDialogBox').toggle();
    }
    
    var dealOriginalQuantity = <?php echo $deal['Deal']['min_limit']; ?>;
    var dealOriginalPrice = <?php echo $deal['Deal']['discounted_price']; ?>;
    
    $('.dealQuantity').keyup(function () { 
        if(validateIfDealQuantityIsNumber() && (!isZero())){
            updatePurchasTotalAmount();
            $('.errors').html('');
        } else {
            $('.errors').html('Cantidad debe ser mayor que 0');
            //resetDealQuantity();
        }
    });
    function isZero() {
      quantity = $('.dealQuantity').val();
      return (quantity==0);
    }
    function fillQuantity() {
      quantity = $('.dealQuantity').val();
      if (!validateIfDealQuantityIsNumber()) {
        $('.dealQuantity').val(1);
      }
    }
    fillQuantity();
    function calculatePurchaseTotalAmount() {
        return $('.dealQuantity').val() * dealOriginalPrice;
    }
    
    function validateIfDealQuantityIsNumber(){
        return /^[0-9]+$/.test($('.dealQuantity').val());
    }
    
    function updatePurchasTotalAmount(){
    	$('.js-deal-total').text(calculatePurchaseTotalAmount());
    }
    
    function resetDealQuantity(){
        $('.dealQuantity').val(dealOriginalQuantity);
        updatePurchasTotalAmount();
    }
    
    updatePurchasTotalAmount();
    
    function validateIfClarin365FieldIsComplete(){
        return $('.clarin_365_account_code').val().length == 19;
    }
    function validateIfClarin365FieldIsOnlyNumbers(){
        return /^[0-9]+$/.test($('.clarin_365_account_code').val());
    }
    
    function showClarin365Error(){
        $('.clarin365Error').show();
    }
    
    
    
    <?php if($display_365_dialog) { ?>
        $( document ).ready(function() {
            $('.subm').click(function(){
                if(!validateIfClarin365FieldIsComplete() || !validateIfClarin365FieldIsOnlyNumbers()){
                    showClarin365Error();
                    $('html, body').animate({scrollTop:0}, 'slow');
                    $('.clarin_365_account_code').focus();
                    return false;
                }
            });
        });
    <?php } ?>
    
    <?php if($errors['clarin_affiliate_number']){ ?>
        showClarin365Error();
    <?php } ?>
</script>