<div class="REGISTER bloque-modelo">    
    <h2>Direcci&oacute;n de env&iacute;o</h2>       
    <div class="fields">
        <div class="field">
            <p>Provincia</p>
            <?php
            echo $form->input('adress_state_id', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'options' => $argentineStatesList,
                'empty' => 'Seleccioná una provincia'
                    )
            );
            ?>
            <?php if ($errors['adress_state_id']) { ?>
                <div class="error-message" ><?php echo $errors['adress_state_id']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Ciudad</p>
            <?php echo $form->input('adress_city_name', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_city_name']) { ?>
                <div class="error-message" ><?php echo $errors['adress_city_name']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Calle</p>
            <?php echo $form->input('adress_street', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_street']) { ?>
                <div class="error-message" ><?php echo $errors['adress_street']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>N&uacute;mero</p>
            <?php echo $form->input('adress_number', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_number']) { ?>
                <div class="error-message" ><?php echo $errors['adress_number']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Piso</p>
            <?php echo $form->input('adress_floor_number', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_floor_number']) { ?>
                <div class="error-message" ><?php echo $errors['adress_floor_number']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Departamento</p>
            <?php echo $form->input('adress_apartment', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_apartment']) { ?>
                <div class="error-message" ><?php echo $errors['adress_apartment']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>C&oacute;digo postal</p>
            <?php echo $form->input('adress_postal_code', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_postal_code']) { ?>
                <div class="error-message" ><?php echo $errors['adress_postal_code']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Tel&eacute;fono</p>
            <?php echo $form->input('adress_telephone', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_telephone']) { ?>
                <div class="error-message" ><?php echo $errors['adress_telephone']; ?></div>
            <?php } ?>
        </div>
        <div class="field">
            <p>Detalles complementarios</p>
            <?php echo $form->input('adress_details', array('label' => false, 'div' => false)); ?>
            <?php if ($errors['adress_details']) { ?>
                <div class="error-message" ><?php echo $errors['adress_details']; ?></div>
            <?php } ?>
        </div>		
    </div>
</div>
<?php if (!empty($errors)) { ?>
    <br>
    <br>
    <br>
<?php } ?>