<div class="SELECT_DEAL_DETAIL bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
            <span>Tu compra</span>
        </div>
    </div>
    
    <div class="dealImage pull-left">
		<?php echo $html->showImage ('Deal', $deal['Attachment']['Attachment'], array ( 'dimension' => 'small_big_thumb', 'alt' => sprintf ('[Imagen: %s]', $html->cText ($deal ['Deal']['name'], false)), 'title' => $html->cText ($deal ['Deal']['name'], false))); ?>
    </div>
        
    <div class="dealDescription pull-left">
    	<?php echo $deal['Deal']['name']; ?>
	</div>
        
    <h2 class="subTitle">Seleccion&aacute; una opci&oacute;n</h2>
</div>