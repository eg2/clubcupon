<?php

	echo $html->css(Configure::read('plugin-order.asset_version').'datepicker');
	echo $html->css(Configure::read('plugin-order.asset_version').'datepicker.less');
    echo $javascript->link(Configure::read('plugin-order.asset_version').'bootstrap-datepicker');
?>

<div class="SELECT_PAYMENT_OPTION bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
        <?php if ($deal['Deal']['is_shipping_address'] == 1 && $deal['Deal']['is_shipping_adress_user'] == 0) { ?>
            <span>Seleccion&aacute; el <strong>punto de retiro</strong> y el <strong>medio de pago</strong> y continu&aacute; con la compra</span>	
        <?php } elseif ($deal['Deal']['is_shipping_address'] == 1 && $deal['Deal']['is_shipping_adress_user'] == 1) { ?>
            <span>Registr&aacute; la <strong>direcci&oacute;n de env&iacute;o</strong> o eleg&iacute; el <strong>punto de retiro</strong>, seleccion&aacute; el <strong>medio de pago</strong> y continu&aacute; con la compra</span>
        <?php } elseif ($deal['Deal']['is_shipping_address'] == 0 && $deal['Deal']['is_shipping_adress_user'] == 1) { ?>
            <span>Registr&aacute; la <strong>direcci&oacute;n de env&iacute;o</strong>, seleccion&aacute; el <strong>medio de pago</strong> y continu&aacute; con la compra</span>
        <?php } else { ?>            
            <span>Seleccion&aacute; el <strong>medio de pago</strong> y continu&aacute; con la compra</span>
        <?php } ?>
        </div>
    </div>
    <div class="payment-group">
    <?php
    if ($deal['Deal']['is_shipping_address'] == 1 && $hasShippingAddresses) {
        echo $this->element('select_shipping_address');
    }
    ?>                    
        
    <?php
    if ($deal['Deal']['is_shipping_adress_user'] == 1) {
        echo $this->element('select_shipping_address_user');
    }
    ?>
        
        <?php if($deal['Deal']['is_discount_mp']){ ?>
            <img src="/img/mp_discount.jpg" />
        <?php }else{ ?>
        <h2>Medios de pago</h2>
        <?php } ?>
        
        <div class="payment-options">
            <input type="hidden" name="data[Deal][is_combined]" id="DealPaymentTypeId_" value="<?php echo $is_combined;?>">
            <ul>
                <?php foreach($paymentOptions as $paymentOption) { ?>
                    <li>
                        <div>
                            <input type="hidden" name="data[Deal][payment_type_id]" id="DealPaymentTypeId_" value="" checked="checked">
                            <input type="radio" name="data[Deal][payment_option_id]" id="DealPaymentTypeId220120" class="js-payment-type cardLogo" value="<?php echo $paymentOption['PaymentOption']['id']; ?>" >
                            <?php echo $html->image ('/img/card_logos/'.$paymentOption['PaymentOption']['logo'], array ('alt' => $paymentOption['PaymentOption']['name'])); ?>
                            <?php echo $paymentOption['PaymentOption']['name']; ?>
                            <?php if(count ($paymentOption['PaymentPlanItem'])) {?>
                                <select class="paymentPlans" name="data[Deal][payment_plan_item_id]">
                                    <?php foreach($paymentOption['PaymentPlanItem'] as $paymentPlans) { ?>
                                        <option  id="payment_plan_item_id_<?php echo $paymentOption['PaymentOption']['id'] ?>" value="<?php echo $paymentPlans['ApiPaymentPlanItem']['id']; ?>"><?php echo $paymentPlans['ApiPaymentPlanItem']['name']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </div>
                    </li>
                <?php } ?>

            </ul>
        
        </div>
 
        <div class="bottom">
            <div class="pull-left">
                <a class="regala cboxElement" href="#buyGift" data-toggle="modal"> Regal&aacute; este Cup&oacute;n a un amigo</a>
            </div>
            <div class="pull-right">
                <input id="btnPagar" type="submit" class="subm" value="Pagar">
            </div>
        </div>
    
        <div id="buyGift" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cancelBuyGift">&times;</button>
                <h3>Regaláselo a un amigo</h3>
            </div>
             <div class="alert alert-error" id="listOfErrors" style="display:none">  
  				<!-- a class="close" data-dismiss="alert">×</a-->  
  				<!-- strong>Se han encontrado los siguientes errores:</strong><br-->
  				<ul></ul>
			</div>  
            <div class="modal-body gift-options">
                <h4>Datos de tu amigo</h4>
                
                <table>
                    <tr>
                        <td>
                            Nombre
                        </td>
                        <td>
                            <input type="text" name="data[Deal][gift_options][to]"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DNI
                        </td>
                        <td>
                            <input type="text" name="data[Deal][gift_options][dni]"/>
                            <?php echo $form->input('is_gift', array ('value'=>$this->data['Deal']['gift_options']['is_gift'], 'name'=>'data[Deal][gift_options][is_gift]', 'type'=>'hidden')); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fecha de nacimiento
                        </td>
                        <td>
                            <div class="input-append date" id="friendBirthday" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd">
                                <input class="span2" size="16" type="text" readonly name="data[Deal][gift_options][dob]">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab" class="customGift">Enviar por mail</a></li>
                        <li><a href="#tab2" data-toggle="tab" class="customGift">Para imprimir</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <table>
                                <tr>
                                    <td class="col1">Mail</td>
                                    <td class="col2">
                                        <?php echo $form->input ('email', array ('label' => false, 'name' => 'data[Deal][gift_options][email]')); ?>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Repet&iacute; el mail</td>
                                    <td><?php echo $form->input ('confirm_gift_email', array ('label' => false)); ?></td>
                                </tr>
                                <tr>
                                    <td>Mensaje</td>
                                    <td><?php echo $form->textarea ('message', array ('label' => false, 'name' => 'data[Deal][gift_options][message]')); ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <table class="send-options">
                                <tr>
                                    <td colspan="2"><strong>&iexcl;Va a llegar un mail a tu casilla para que imprimas y regales!</strong></td>
                                </tr>
                                <tr>
                                    <td>Mensaje</td>
                                    <td><?php echo $form->textarea ('message2', array ('label' => false, 'name' => 'data[Deal][gift_options][message]')); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                
            </div>
            
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal" id="giftCboxSave">Guardar</a>
            </div>
        </div>
    
    </div>
    
</div>

<script>
    
     var primerMedioDePagoSeleccionado = $(".SELECT_PAYMENT_OPTION .payment-group input[type='radio']:first");
    $(primerMedioDePagoSeleccionado).attr("checked", "checked");
    $(primerMedioDePagoSeleccionado).parent().find('select').toggle();
      
    //Manejo de selects en las opciones de pago 
    $(".js-payment-type").change(function(){ 
        hideSelects();
        var parentTag = $(this).parent().get(0);
        $(parentTag).find('select').toggle();
    });
        
    function hideSelects() {
        $('.paymentPlans').hide();
    }    


    $('#friendBirthday').datepicker({viewMode:2, autoclose:true });
    $(document).ready( function(){
      $('#Buy').submit(function(event) {
        $('#btnPagar').attr('disabled', 'disabled');
        $('#btnPagar').css('font-size', '12px');
        $('#btnPagar').css('padding-left','17px');
        $('#btnPagar').attr('value','Enviando');
        
        paymentPlanOptionSelected = $(document).find("[name*='data[Deal][payment_option_id]']:checked");
        $(paymentPlanOptionSelected).parent().parent().siblings().find("[name*='data[Deal][payment_option_id]']").attr('disabled','disabled');
        
        paymentPlanOptionSelected = $(document).find("input[name*='data[Deal][payment_option_id]']:checked");
        paymentPlanOptionSelectedValue = $(paymentPlanOptionSelected).val();

        $("[id*=payment_plan_item_id]").each(function(d, i) {
            paymentoptionid = this.id.split('_');
            if (paymentPlanOptionSelectedValue!= paymentoptionid[4]) {
                $(this).attr('disabled', 'disabled');
            }
        });
      });
    });

</script>
