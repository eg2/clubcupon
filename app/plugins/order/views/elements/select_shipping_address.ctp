<div style="width:100%;float:left">
    <h2>Puntos de Retiro</h2>       
    <?php
    $inputDefaults = array(
        'label' => false
    );
    ?>
    <div class="control-group">
        <div class="controls" style="width:35%;float:left">       
            <?php
            echo $form->input('shipping_address_id', array_merge($inputDefaults, array('empty' => __l('Please Select'))));
            ?>
        </div>
        <?php if ($errors['select_shipping_address']) { ?>
            <div class="error-message" style="float:left">
                <?php echo $errors['select_shipping_address']; ?>
            </div>
        <?php } ?>
    </div>
</div>
