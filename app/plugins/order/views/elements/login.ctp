<div class="LOGIN bloque-password bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
            <span>Ingres&aacute; tu <strong>contrase&ntilde;a</strong> para avanzar en tu compra</span>
        </div>
    </div>
    
    <div class="user-login pull-left">
			<div class="email">     	
				<?php echo $form->input ('email', array ('label' => false, 'div' => false, 'readonly'=>'readonly', 'class'=> 'whiteField')); ?>
				<?php echo $html->link('cambiar', array( 'plugin' => 'order', 'controller' => 'order_processings', 'action' => 'buy', 'deal_id:' . $deal_id . '/step:0'.$parameters), array('id'=>'cambiarmailok')); ?>
            </div>
            <div class="pass">
            	<p>Contrase&ntilde;a</p>
                	<?php echo $form->input ('passwd', array ('label' => false, 'div' => false)); ?>
                    <input type="submit" class="subm pull-right" value="Continuar">
            </div>
            <?php if($errors['password']){  ?>
            	<div class="error-message" ><?php echo $errors['password']; ?></div>
            <?php } ?>
        <div class="error-message" style="display:none;float:left" id="msgerroremail">Debe ser un mail v&aacute;lido</div>
    </div>
    
</div>
