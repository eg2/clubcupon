<div class="UPDATE_USER_PROFILE bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
            <span>Ingres&aacute; tu <strong>DNI</strong> para avanzar en tu compra</span>
        </div>
    </div>
    
    <div class="user-login pull-left">
        
        <p class="leyenda">Ingres&aacute; tu <span>DNI</span></p>
        
        <div class="correo">
            
            <table>
                <tr>
                    <td class="col1">N&uacute;mero de DNI:</td>
                    <td class="col2">
                        <?php echo $form->input ('dni', array ('label' => false, 'div' => false)); ?>
                        <input type="submit" class="subm pull-right" value="Continuar">
                    </td>
                </tr>
            </table>
            
        </div>
        <div class="error-message" style="display:none;float:left" id="msgerroremail">Debe ser un mail v&aacute;lido</div>
    </div>
    
</div>
