<div id="footer" style="width:100%;">
<div class="footer-bg">
    <div class="container">
        <div class="row">
    
            <div class="logo pull-left">
                <?php
                    $img_url =  $is_corporate_city && $is_deal_view ? 'theme_clean/exclusive/':'/img/';
                    echo $html->link($html->image($img_url . 'logo_footer.png', array('alt' => 'CLUB CUP&Oacute;N.')), array('controller' => 'deals', 'action' => 'index', 'admin' => false, 'plugin' => ''), array('class' => 'floatLeft', 'escape' => false));
                ?>
            </div>
            
            <div class="logo-gc pull-left">
                <?php echo $html->image($img_url . 'logo_gc.png', array('alt' => 'Grupo Clarin')); ?>
            </div>
            
            <div class="menu pull-left bottom_nav">
                <ul>
                    <li><?php echo $html->link('Qui&eacute;nes Somos', array('controller' => 'pages', 'action' => 'who', 'admin' => false, 'plugin' => ''), array('escape' => false)); ?></li>
                    <?php if($city['City']['is_business_unit']!=='1') {?>
                    <li><a title="Centro de Atenci&oacute;n al Cliente" href="http://soporte.clubcupon.com.ar/" target="_blank">Centro de Atenci&oacute;n al Cliente</a></li>
                    <?php } ?>
                    <li><a href="<?php echo FULL_BASE_URL.'/exclusive';?>">Exclusive</a></li>
                    <li><?php echo $html->link('Concursos', array('controller'=>'pages','action'=>'concurso', 'admin'=> false, 'plugin' => ''), array('escape'=>false)); ?> </li>
                    <li><?php echo $html->link('T&eacute;rminos y Condiciones', array('controller' => 'pages', 'action' => 'terms', 'admin' => false, 'plugin' => ''), array('escape' => false)); ?></li>
                    <li><?php echo $html->link('Pol&iacute;ticas de Privacidad', array('controller' => 'pages', 'action' => 'policy', 'admin' => false, 'plugin' => ''), array('escape' => false)); ?> </li>
                    <li><?php echo $html->link('Protecci&oacute;n de Datos Personales', array('controller' => 'pages', 'action' => 'protection', 'plugin' => ''), array('escape' => false, 'class'=>'email')); ?></li>
                    <li><a target="_blank" rel="nofollow" href="http://www.buenosaires.gob.ar/" title="Dirección General de Defensa y Protección al Consumidor - Consultas y/o denuncias" class="txtl">Direcci&oacute;n General de Defensa y Protección al Consumidor</a></li>
                    <li class="last">Copyright &copy; 2012 Derechos Reservados.</li>

                </ul>
            </div>
            
            
            <div class="afip pull-left">
                <?php echo '<a href="http://qr.afip.gob.ar/?qr=aaBUq_tGRAX3UzK3EQuMmg,," target="_F960AFIPInfo"><img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" width="62"></a>'; ?>
            </div>
            
            <div class="social pull-right">
                <ul>
                    <li><a style="padding: 0px;" target="_blank" href="/xmldeals/rss/ciudad-de-buenos-aires"><img alt="" src="/img/social_actions/rss.png"></a></li>
                    <li><a target="_blank" href="http://www.facebook.com/ClubCupon"><img alt="" src="/img/social_actions/facebook.png"></a></li>
                    <li><a target="_blank" href="http://twitter.com/ClubCupon"><img alt="" src="/img/social_actions/twitter.png"></a></li>
                </ul>
            </div>
            
            
        </div>
    </div>
</div>
</div>
<div id="fb-root"></div>


<?php echo $javascript->codeBlock('var cfg = ' . $javascript->object($js_vars_for_layout), array('inline' => true)); ?>
<script>/* la usa facebook*/
    function __cfg(c) { return(cfg && cfg.cfg && cfg.cfg[c]) ? cfg.cfg[c]: false;}
</script>
<?php echo $this->element(Configure::read('theme.theme_name') . 'facebookConnection'); ?>