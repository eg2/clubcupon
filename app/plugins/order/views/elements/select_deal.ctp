<?php
    $dealDescription = !empty($deal['Deal']['descriptive_text']) ?  $deal['Deal']['descriptive_text'] : $deal['Deal']['name'];
?>

<div class="SELECT_DEAL bloque-modelo">
    <table>
        <tr>
            <th class="col1">Descripci&oacute;n</th>
            <th class="col2">Precio</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td class="col1"><?php echo !empty($deal['Deal']['descriptive_text']) ?  $deal['Deal']['descriptive_text'] : $deal['Deal']['name']; ?></td>
            <td class="col2"><?php echo $deal['Deal']['discounted_price']; ?></td>
            <td class="col3">
                <?php 
                    if ($deal['has_stock']) { 
                        $parameters = array ( 'deal_id'=>$deal ['Deal']['id'], 'step'=> 1);
                        if ($isDescuentoCityRequest && $descuentoCityUtmRef) {
                            $parameters['source'] = 'descuentocity.com';
                            $parameters['ref'] = $descuentoCityUtmRef;
                        }                        
                        echo $html->link ('Comprar', $parameters, array ('title' => $deal ['Deal']['name'], 'class' => 'btn-compra_azul')); 
                    } else {
                        echo '<span class="btn-gris-centrado btn-agotado">Agotada</span>';
                    }
                    if ($deal['Deal']['is_clarin365_deal']) {
                        echo $html->image ('/img/chapa_clarin365.png', array('alt'=>'Oferta exclusiva Clarin 365', 'name'=>'oferta exclusiva Clarin 365', 'class'=> 'logo pull-right'));
                    }
                    
                    ?>
            </td>
        </tr>
        <?php foreach($subdeals as $subdeal){?>
            <tr>
                <td class="col1"><?php echo !empty($subdeal['Deal']['descriptive_text']) ?  $subdeal['Deal']['descriptive_text'] : $subdeal['Deal']['name']; ?></td>
                <td class="col2"><?php echo $subdeal['Deal']['discounted_price']; ?></td>
                <td class="col3">
                    <?php
                        if ($subdeal['has_stock']) {
                            $parameters = array ( 'deal_id'=>$subdeal ['Deal']['id'], 'step'=> 1);
                            if ($isDescuentoCityRequest && $descuentoCityUtmRef) {
                                $parameters['source'] = 'descuentocity.com';
                                $parameters['ref'] = $descuentoCityUtmRef;
                            }                              
                            echo $html->link ('Comprar', $parameters, array ('title' => $subdeal ['Deal']['name'], 'class' => 'btn-compra_azul'));
                        } else {
                            echo '<div class="btn-gris-centrado pull-right">Agotada</div>';
                        }
                    
                        if ($subdeal['Deal']['is_clarin365_deal']) {
                            echo $html->image ('/img/chapa_clarin365.png', array('alt'=>'Oferta exclusiva Clarin 365', 'name'=>'oferta exclusiva Clarin 365', 'class'=> 'logo pull-right'));
                        }
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
