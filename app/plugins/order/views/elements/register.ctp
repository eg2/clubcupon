<style>
  .userEmailcell {
    pointer-events: none;
  }
  
  *.unselectable {
   -moz-user-select: none;
   -khtml-user-select: none;
   -webkit-user-select: none;
   /*      Introduced in IE 10.
     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/    */
   -ms-user-select: none;
 }
</style>
<script>
  $(document).ready(function(){
     $('input.disablecopypaste').bind('copy paste', function (e) {
        e.preventDefault();
     });
  });
  </script>
<div class="REGISTER bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner">
            <span>Complet&aacute; tus <strong>Datos de Registro</strong> para avanzar con tu compra</span>
        </div>
    </div>
    
    <div class="user-register pull-left">
    	<div class="fields">
	    	<div class="email">
	    		<p>Mail</p>
				<?php echo $form->input('email', array ('label' => false, 'div' => false, 'readonly'=>'readonly', 'class'=> 'whiteField', 'disabled'=>'disabled')); ?>
	            <?php echo $form->input('email', array ('label' => false, 'div' => false, 'readonly'=>'readonly', 'type'=>'hidden')); ?>
	            
	            <?php echo $html->link('cambiar', array( 'plugin' => 'order', 'controller' => 'order_processings', 'action' => 'buy', 'deal_id:' . $deal_id . '/step:0'.$parameters), array('id'=>'cambiarmailok')); ?>
		        <?php if($errors['email']){ ?>
		           	<div class="error-message" ><?php echo $errors['email']; ?></div>
		        <?php } ?>
	    		
	    		<p>Confirmar mail</p>
				<?php echo $form->input ('confirm_email', array ('label' => false, 'div' => false, 'onpaste' => 'return false;', 'style'=>'width:94%')); ?>
				<?php if($errors['confirm_email']){ ?>
					<div class="error-message" ><?php echo $errors['confirm_email']; ?></div>
          <div style="clear:both;"></div>
				<?php } ?>
	    		
	    	</div>
	    	<div class="field">
	    		<p>Nombre de usuario</p>
				<?php echo $form->input ('username', array ('label' => false, 'div' => false)); ?>
				<?php if($errors['username']){ ?>
					<div class="error-message" ><?php echo $errors['username']; ?></div>
				<?php } ?>
	    	</div>
	    	<div class="field">
	    		<p>DNI</p>
				<?php echo $form->input ('UserProfile.dni', array ('label' => false, 'div' => false)); ?>
				<?php if($errors['dni']){ ?>
					<div class="error-message" ><?php echo $errors['dni']; ?></div>
				<?php } ?>
	    	</div>
	    	<div class="field">
	    		<p>Contrase&ntilde;a</p>
				<?php echo $form->input ('passwd', array ('label' => false, 'div' => false)); ?>
	            <?php if($errors['password']){ ?>
	            	<div class="error-message" ><?php echo $errors['password']; ?></div>
				<?php } ?>
	    	</div>
	    	<div class="field">
	    		<p>Confirmar contrase&ntilde;a</p>
				<?php echo $form->input ('confirm_password', array ('label' => false, 'div' => false, 'type'=>'password')); ?>
				<?php if($errors['confirm_password']){ ?>
					<div class="error-message" ><?php echo $errors['confirm_password']; ?></div>
				<?php } ?>
	    	</div>
    	</div>
    	<div style="clear:both;"></div>
        <div class="terms">
        	<div>
	            <?php echo $form->checkbox('is_agree_terms_conditions'); ?>
	            <label>
	                He le&iacute;do y acepto los<span> <a href="/ciudad-de-buenos-aires/pages/terms" target="_blank">T&eacute;rminos y Condiciones</a></span> del servicio
	            </label>
                <?php if($errors['is_agree_terms_conditions']){ ?>
                    <div class="error-message" ><?php echo $errors['is_agree_terms_conditions']; ?></div>
                <?php } ?>
          </div>
          <div style="clear:both;"></div>
            <div>
	            <?php if ($show_subscription) { ?>
	            <?php echo $form->checkbox('Subscription.notifications', array('checked' => 'checked','class' => 'check-st')); ?>
	            <label>Si, quiero recibir mails con las ofertas diarias de Club Cup&oacute;n</label>
	            <?php }?>
            </div>
        </div>
                
        <div style="float: left">
            <p style="color:#999999;font-size: xx-small; padding: 10px; line-height: 20px">
                Recabamos sus datos con la finalidad de administrar y gestionar la actividad de acceso a nuestros servicios que lo vinculan a nuestra Empresa, conforme lo establecido por el art. 6 de Ley 25323 Usted es responsable por la inexactitud o negativa de proporcionar sus datos.<br /><br />
                .El titular de datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter&eacute;s leg&iacute;timo al efecto, conforme lo establecido en el art&iacute;culo 14, inciso 3 de la Ley n&ordm; 25.326".<br />
                La direcci&oacute;n Nacional de Datos Personales, &Oacute;rgano de Control de la Ley n&ordm; 25.326, tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales."<br />
                Usted podr&aacute; ejercer el derecho de acceso, rectificaci&oacute;n, y supresi&oacute;n de sus datos conforme lo establecido en el art. 6 de la Ley 25326, enviando un email a datospersonales@clubcupon.com.ar o comunicandose telefonicamente al &nbsp;+5411 49438700<br /><br />
                <span style="font-weight: bold">Para contactar a la Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales:<br /></span>
                Sarmiento 1118, 5&ordm; piso (C1041AAX)<br />
                Tel. 4383-8510/12/13/15<br />
                <a href="http://www.jus.gov.ar/datospersonales">www.jus.gov.ar/datospersonales</a><br />
                <a href="mailto:infodnpdp@jus.gov.ar">infodnpdp@jus.gov.ar</a>
            </p>
        </div>
        <input type="submit" class="subm" value="Continuar">
    </div>
    
    <div class="condiciones pull-right">
    	<p><strong>Completá tus datos de Registro para avanzar con tu compra.</strong>
        <div class="disclaimer">
            Puede que ya estes suscripto a nuestras newsletters pero no tengas una cuenta.
            <strong>Registrate Gratis</strong> completando tus datos y empez&aacute; a disfrutar de todos los beneficios.
        </div>
    </div>
    <div style="clear:both;"></div>
</div>
<script>

    $( document ).ready(function() {
        $('.subm').click(function(){

            if(isAgreeWithTerms()){
                console.log('Enviando formulario al siguiente paso');

            } else {
                console.log('No se aceptaron los terminos y condiciones');
                displayTermsAndConditiosError();
                return false;
            }

        });


    });

    function isAgreeWithTerms(){
        //return $('#UserIsAgreeTermsConditions').is(':checked');
        return true;
    }
    
    function displayTermsAndConditiosError(){
        alert('Debes aceptar los términos y condiciones para continuar');
    }

</script>
