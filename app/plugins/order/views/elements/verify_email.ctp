<div class="VERIFY_EMAIL login bloque-modelo">
    <div class="navbar navbar-inverse ">
        <div class="navbar-inner tucompra">
            <span>Ingres&aacute; tu mail para avanzar en tu compra</span>
        </div>
    </div>
    
    <div class="user-login pull-left">
        <?php /*<p class="ingresacorreo">Ingres&aacute; tu <span>mail</span></p>*/ ?>
        <div class="correo">
            <label>Mail</label>
            <?php echo $form->input(Configure::read('user.using_to_login'), array('label'=>false,'div' => false, 'oncopy'=>'return false;', 'id'=>'userEmail')); ?>            
            <input type="submit" class="subm" value="Continuar">
        </div>
        <div class="error-message" style="display:none;" id="msgerroremail">Debe ser un mail v&aacute;lido</div>
    </div>
    
    <div class="facebook pull-right">
        <p>O utiliz&aacute; tu cuenta de Facebook</p>
        <p>para comprar en <span>Club Cup&oacute;n</span></p>
        <?php /* <p><a href="#" class="fb-link" rel="nofollow" title="fconnect" id="facebookLoginButton"><img src="/img/theme_clean/dynamic/fb-ing.jpg" alt=""></a></p>*/?>
        <p><a href="#" class="fb-link" rel="nofollow" title="fconnect" id="facebookLoginButton">Ingresar</a></p>
    </div>
    
</div>

<script>


$( document ).ready(function() {
    
    $('.subm').click(function(){
        
        email = $('#userEmail').val();
        if(validateEmailFormat(email)){
            console.log('Enviando formulario al siguiente paso');
            
        } else {
            console.log('Error en el formato del email ingresado, o email vacio');
            displayEmailError();
            return false;
        }
        
    });
    
    
});



function displayEmailError(){
    $('.error-message').show();
}

$(document).ready(function () {
    $('#Selector').bind('copy paste', function (e) {
       e.preventDefault();
    });
  });
</script>
   
    