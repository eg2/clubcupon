<!DOCTYPE html>
<html lang="es">
    
    <head>
        <meta name="description" content="<?php echo Configure::read('web.meta.description')?>"/>
        <?php
            echo $html->charset(), "\n";
            $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
            $title_for_layout = Configure::read('site.name') . ' | ' . $html->cText($title_for_layout, false);

            $title_for_layout = 'Club Cupón - Comprar '.$deal['Deal']['name'];
            
            echo '<title>' . $title_for_layout . '</title>';

            echo $html->meta('keywords', $meta_for_layout['keywords']), "\n";

            if (isset ($og_data)) {
                foreach ($og_data as $key => $value) {
                    if (is_array ($value)) {
                        foreach ($value as $val) {
                            echo $html->meta (array ('property' => $key, 'content' => $val)), "\n";
                        }
                    } else {
                        echo $html->meta (array ('property' => $key, 'content' => $value)), "\n";
                    }
                }
            }
            
            echo $html->css(Configure::read('bootstrap.asset_version').'bootstrap.min');
            echo $html->css(Configure::read('plugin-order.asset_version').'order-buy');
            echo $html->css(Configure::read('web.clubcupon.asset_version').'style.css');

            echo $javascript->link(Configure::read('libs.asset_version').'jquery-1.9.1.min');
            echo $javascript->link(Configure::read('bootstrap.asset_version').'bootstrap.min');
            echo $javascript->link(Configure::read('plugin-order.asset_version').'order');

            echo $javascript->link(Configure::read('libs.asset_version').'ecommerce');

        ?>
        <!-- facebook -->
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <link rel="alternate" type="application/rss+xml" title="RSS-News" href="<?php echo $rss_feed_url_for_city ?>" />

        <!-- favicon -->
        <link href="/favicon.png" type="image/x-icon" rel="icon" />
        <link href="/favicon.png" type="image/x-icon" rel="shortcut icon" />
        
        <!-- web plugin restyling-->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,500italic,700,700italic,300italic' rel='stylesheet' type='text/css'>
        
         <!--[if lt IE 9]>
        	<script type="text/javascript">
        		document.createElement("nav");
        		document.createElement("header");
        		document.createElement("footer");
        		document.createElement("section");
        		document.createElement("article");
        		document.createElement("aside");
        		document.createElement("hgroup");
        	</script>
        <![endif]-->
        
        <meta name="cXenseParse:pageclass" content="frontpage"> 
    </head>