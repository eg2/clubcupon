<?php 
    //Title
    $title_for_layout = isset($title_for_layout) ? $title_for_layout : '';
    echo $this->element('order_head');
?>
    
    <body>
        <?php
        	if (Configure::read('app.tracking.enable')) {
        		echo $this->element('trackingGoogle');
        		echo $this->element('trackingWeb',array('isOrder'=>true));
        	}
            //Flash alerts
            if ($session->check('Message.error'))  {$session->flash('error');}
            if ($session->check('Message.success')){$session->flash('success');}
            if ($session->check('Message.flash'))  {$session->flash();}
        
            //layout
            echo $this->element('order_header');
            echo '<div class="container cuerpo">';
            echo $content_for_layout;
            echo '</div>';
            echo $this->element('clubcupon/jsEplanningHome');//banners 4 215x100
			echo $this->element('clubcupon/footerClubcupon', array('slug'=>$this->params['named']['city']));
			
			echo $this->element('clubcupon/tagCxense',array('user_mail'=>$auth->user('email')));
			echo $this->element('clubcupon/tagEplanningProduct5');
			
			
        ?>
        <?php
    		if(Configure::read('order.ThreatMetrix')==TRUE){
    			echo $this->element('threat_metrix');
    		}
    	?>
    </body>
</html>