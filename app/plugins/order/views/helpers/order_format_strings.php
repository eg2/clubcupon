<?php
class OrderFormatStringsHelper extends AppHelper {
    
    function shortenStringsWithHtmlEntities($s, $l = 25) {

        // En el caso de que nos pasen algo no utf-8

        $s = utf8_decode($s);

        // Quitamos las entidades html

        $s = html_entity_decode($s);

        // Corto en función del largo

        $sub = (strlen($s) > $l) ? substr($s, 0, $l) . '...' : $s;

        // Devolvemos el string, con entidades html

        return htmlentities($sub);
    }
}

?>