<style>
  .btn-gris {
    background: none repeat scroll 0 0 #F3F3F3;
    border-color: #D2D2D2;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 2px;
    color: #676767;
    cursor: pointer;
    display: block;
    font: bold 12px arial;
    padding: 8px 10px;
    text-align: center;
    text-shadow: none;
    margin-left: 20px;
    margin-right: 20px;
    
}
p {
    margin-left: 20px;
    margin-right: 20px;
}
div.title {
    margin-left: 20px;
    margin-right: 20px;
}
.footer-bg {
    background: none repeat scroll 0 0 #D9D9D9;
    border-top: 3px solid #A4A5A7;
    padding: 20px 0 0;
    position: absolute;
    bottom: 0;
    width:  100%;
}
</style>
<div id="main" class="container_16 main_frt">
    <div class="grid_12">
        <div class="bloque-home clearfix bloque-modelo">
        <div class="title">  
        <h3 class="fmt-h2">Tu selección.</h2>
        </div>    
            <!-- Contenidos -->
                <p>
                    Lo sentimos, se produjo un error.
                    <br />
                    Revis&aacute; los datos ingresados e intentalo nuevamente.
                </p>
                
                <div class="links">
                    <?php
                            
                    if($is_hidden_city)
                    {
                        echo $html->link ('Volver', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>'ciudad-de-buenos-aires'), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> 'btn-gris', 'escape' => false));
                    }
                    else
                    {
                        echo '<a href="/' . $citySlug . '" class="btn-gris">Volver</a>';
                       //echo '<a href="/" class="btn-gris">Volver</a>';
                    }

                    ?>
                </div>   

            
            <!-- / Contenidos -->

        </div>
    </div>
    <div class="grid_4 col_derecha" id ="pages">
      <?php// echo $this->element('theme_clean/sidebar_wide'); ?>
    </div>
</div>

