<div class="BUY_DEAL_OK bloque-modelo ">  
    <h1 class="title_compra">
      <span>
      Tu selección
      </span>
    </h1>
    <?php
        $current_user_details = array('username' => $auth->user('username'), 'user_type_id' => $auth->user('user_type_id'), 'id' => $auth->user('id'));
        $cantidad        = $dealExternal['DealExternal']['quantity'];
        $precio          = $deal['Deal']['discounted_price'];
        $total           = sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$dealExternal['DealExternal']['quantity']));
        $pagoTerceros    = sprintf('%.2f',Precision::mul($deal['Deal']['discounted_price'],$dealExternal['DealExternal']['quantity']))-$dealExternal['DealExternal']['internal_amount'];
        $creditousado = array_sum(array($dealExternal['DealExternal']['internal_amount'], $dealExternal['DealExternal']['gifted_amount'], $dealExternal['DealExternal']['discounted_amount']));
        // formating
        $precio = sprintf('%d',$precio);
        $total = sprintf('%d',$total);
        $creditousado = sprintf('%d',$creditousado);
        $pagoTerceros = sprintf('%d', $pagoTerceros);
    ?>
        <div class="gracias">
          &iexcl;Muchas gracias por elegirnos, <?php echo $html->getUserLink($current_user_details); ?>!
          <?php if($deal['Deal']['is_now']==0){ ?>
              <p>
                Recibirás el cup&oacute;n para concretar el canje cuando tu pago se acredite.
                Recuerda que si seleccionaste Pago F&aacute;cil, Rapipago, BaproExpress, Banelco o Link la acreditaci&oacute;n demorar&aacute; hasta 72 hs h&aacute;biles.
                Si no completaste los datos para el pago, la compra no se concretar&aacute;.
              </p>
          <?php } else{
            $cuando   = $time->isToday($deal['Deal']['start_date']) ? "Hoy" : 'Ma&ntilde;ana';
            $h_inicio = substr($deal['Deal']['start_date'], 11, -3);
            $h_fin    = substr($deal['Deal']['end_date'],   11, -3);
            if($h_fin=='00:00') $h_fin='24'; ?>
            <h3 class="leyenda-oferta">Para utilizar <?php echo $cuando; ?> desde las <?php echo $h_inicio; ?>hs hasta las <?php echo $h_fin; ?>hs. Disfrut&aacute; de tu descuento <span>Club Cup&oacute;n YA</span></h3>
        <?php  } ?>
        </div>
        <div class="dealInfo">
          <?php  echo $html->showImage('Deal', $deal['Attachment']['Attachment'], array('class' => 'bigImage', 'dimension' => 'small_big_thumb', 'alt' => sprintf('[Foto: %s]', $html->cText($deal['Company']['name'] . ' ' .$deal['Deal']['name'], false)), 'title' => $html->cText($deal['Deal']['name'], false))); ?>
          <!-- <img width="209" height="139" title="Oferta común 1404" alt="[Foto: Oferta común 1404]" class="bigImage" src="http://cc.local/img/web/clubcupon/img_destacado.jpg"> -->
          <p>
            <?php 
            if ($deal['Deal']['id'] != $deal['Deal']['parent_deal_id'] && $deal['Deal']['is_now']==0) {
              echo $deal['Deal']['name'] .  ' - '. $deal['Deal']['descriptive_text'];
            } else {
              echo $deal['Deal']['name'];
            }?>
          </p>
        </div>
        <div class="detail">
            <h2 class="title_compra">Resumen</h2>
            <table class="fmt-tabla tabla-compras">
                    <tr>
                        <td>Su Pedido</td>
                        <td><?php echo "$cantidad x \${$precio}" ;?></td>
                    </tr>
                   
                    <tr>
                        <td>Crédito Usado</td>
                        <td><?php echo "\${$creditousado}";?></td>
                    </tr>
                    <tr>
                        <td>Pagado por medios de pago de terceros</td>
                        <td><?php if($pagoTerceros <=0){ echo '$0';} else {  echo "&nbsp;&nbsp;\${$pagoTerceros}";} ?></td>
                    </tr>
                    <tr class="total">
                        <td>Total Pagado</td>
                        <td><?php echo "\${$total}";?></td>
                    </tr>
            </table>
            <div class="btn_options">
                       <?php echo $html->link('Ir a mis cupones', array('controller'=>'deal_users', 'action' => 'index', 'plugin'=>'', 'type' => 'available'), array('class' => 'btn-gris')); ?>
                       <?php echo $html->link('Volver', array ('plugin' => '','controller' => 'deals', 'action' => 'index', 'admin' => false, 'city'=>$city['City']['slug'], 'plugin'=>''), array ('title' => 'Ofertas del Día ' . Configure::read ('site.name'), 'class'=> 'btn-gris', 'escape' => false)); ?>
                       <a target="_blank" id="fbShare">
                       <?php $image_dir = '/img/theme_clean/';?>
                       <img src="<?php echo $image_dir; ?>facebook_compartir.gif" alt="club cupon" title="Club Cupon" border="0" height ="15px" />
                       </a>
            </div>
        </div>
        <div>
		</div>
		
    </div>
    <!-- / Contenidos -->
<script>
    $(document).ready(function() {
     	var urlShare='<?php echo $dealUrl?>';
    	$('#fbShare').attr('href','https://www.facebook.com/sharer/sharer.php?u='+urlShare);
    });
</script>

<?php
	echo $this->element('dax',array("name_page" => "clubcupon.deal.buyOk"));
    echo $this->element('tracking-compra-ok');
    echo '<!-- EmtTracker -->';
    echo $this->element('theme_clean/emt/tracking_conversion');
    echo '<!-- / EmtTracker -->';
?>