<?php

$parameters = '';
if ($isDescuentoCityRequest && $descuentoCityUtmRef) {
    $parameters = '/source:descuentocity.com' . '/ref:' . $descuentoCityUtmRef;
}
if (in_array($step, array(
            1,
            6
        ))) {
    echo $form->create('Deal', array(
        'type' => 'post',
        'url' => array(
            'plugin' => 'order',
            'controller' => 'order_processings',
            'action' => 'buy',
            'deal_id:' . $deal_id . '/step:' . $step . $parameters
        ),
        'autocomplete' => 'off',
        'id' => 'Buy'
    ));
} elseif (in_array($step, array(
            0,
            2,
            3,
            4
        ))) {
    echo $form->create('User', array(
        'type' => 'post',
        'url' => array(
            'plugin' => 'order',
            'controller' => 'order_processings',
            'action' => 'buy',
            'deal_id:' . $deal_id . '/step:' . $step . $parameters
        ),
        'autocomplete' => 'off',
        'id' => 'login'
    ));
} else {
    echo $form->create('UserProfile', array(
        'type' => 'post',
        'url' => array(
            'plugin' => 'order',
            'controller' => 'order_processings',
            'action' => 'buy',
            'deal_id:' . $deal_id . '/step:' . $step . $parameters
        ),
        'autocomplete' => 'off',
        'id' => 'UserProfile'
    ));
}
if ($is_combined) {
    echo $this->element('combined_payment');
} else {
    if ($step == 1) {
        echo $this->element('select_deal_detail');
    } else {
        echo $this->element('buy_deal_detail');
    }
    switch ($step) {
        case 0:
            echo $this->element('verify_email');
            break;

        case 1:
            echo $this->element('select_deal');
            break;

        case 2:
            echo $this->element('verify_email');
            echo $form->end();
            break;

        case 3:
            echo $this->element('login', array('parameters'=>$parameters));
            break;

        case 4:
            echo $this->element('register', array('parameters'=>$parameters));
            break;

        case 5:
            echo $this->element('update_user_profile');
            break;

        case 6:
            echo $this->element('select_payment_option');
            break;
    }
}
echo $form->end();
?>
