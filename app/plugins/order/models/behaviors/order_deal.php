
<?php

App::import('Core', 'Behavior');
App::import('Model', 'api.ApiDeal');

class OrderDealBehavior extends ModelBehavior {

    function setup(&$Model, $settings) {
        $this->settings = $settings;
        $this->model = $Model;
    }

    function findDealsPurchasableByCityId(&$Model, $cityId, $excludeId) {
        $conditions = array();
        $conditions['Deal.is_side_deal'] = 1;
        $conditions['Deal.city_id'] = $cityId;
        $conditions['Deal.deal_status_id'] = array(
            ConstDealStatus::Open,
            ConstDealStatus::Tipped
        );
        $conditions[] = 'Deal.parent_deal_id = Deal.id';
        $conditions['Not']['Deal.id'] = $excludeId;
        $conditions['Not']['Deal.deal_status_id'] = array(
            ConstDealStatus::PendingApproval,
            ConstDealStatus::Upcoming
        );
        $order = array(
            'Deal.priority' => 'DESC',
            'Deal.deal_status_id' => 'ASC',
            'Deal.start_date' => 'ASC',
            'Deal.id' => 'ASC'
        );
        return $this->model->find('all', array(
                    'conditions' => $conditions,
                    'recursive' => 1,
                    'order' => $order
        ));
    }

}
