<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'integration.IntegrationRequestValidator');

class IntegrationRequestValidatorSaveOrUpdateProductInventoryComponent extends IntegrationRequestValidatorComponent {

    public $requiered = array(
        'app_security_key',
        'idProducto',
        'nombre',
        'codigoFacturacion',
        'valueLy',
        'portalId'
    );

    public function __construct($data, $userId) {
        parent::__construct($data, $userId);
    }

    public function validate() {
        $errors = array();
        $errors = array_merge($this->checkFieldsRequiered($this->data), $errors);
        $errors = array_merge($this->checkTypeIdProducto($this->data), $errors);
        return $errors;
    }

    public function checkFieldsRequiered($data) {
        $errors = array();
        foreach ($this->requiered as $field) {
            if (empty($data[$field])) {
                $errors[$field] = 'Campo requerido';
            }
        }
        return $errors;
    }

    public function checkTypeIdProducto($data) {
        $errors = array();
        if (((int) $data['idProducto']) == 0) {
            $errors['idProducto'] = 'Debe ser numerico';
        }
        return $errors;
    }

}