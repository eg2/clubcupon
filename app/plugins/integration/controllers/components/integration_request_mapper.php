<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'integration.IntegrationMapper');

class IntegrationRequestMapperComponent extends IntegrationMapperComponent {

    function __construct() {
        parent::__construct('integration');
    }

    public function map($data) {
        return array(
            'name' => $data['nombre'],
            'bac_product_id' => $data['idProducto'],
            'description' => $data['descripcion'],
            'bac_invoice_id' => $data['codigoFacturacion'],
            'bac_libertya_id' => $data['valueLy'],
            'bac_portal_id' => $data['portalId'],
            'id' => $data['idProductoPortal']
        );
    }

}
