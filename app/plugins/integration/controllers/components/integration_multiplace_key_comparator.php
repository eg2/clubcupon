<?php

class IntegrationMultiplaceComparatorComponent extends Object {

    protected $model;
    protected $field;

    function __construct($model, $key) {
        parent::__construct();
        $this->model = $model;
        $this->key = $key;
    }

    function compare($a, $b) {
        return strcmp($a[$this->model][$this->key], $b[$this->model][$this->key]);
    }

}
