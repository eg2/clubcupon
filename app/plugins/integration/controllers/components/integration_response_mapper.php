<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'integration.IntegrationMapper');

class IntegrationResponseMapperComponent extends IntegrationMapperComponent {

    function __construct() {
        parent::__construct('integration');
    }

    public function map($data) {
        return array(
            'nombre' => $data['name'],
            'idProduct' => $data['bac_product_id'],
            'descripcion' => $data['description'],
            'codigoFacturacion' => $data['bac_invoice_id'],
            'valueLy' => $data['bac_libertya_id'],
            'portalId' => $data['bac_portal_id'],
            'idProductoPortal' => $data['id'],
            'stock' => $data['stock']
        );
    }

}
