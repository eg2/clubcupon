<?php

App::import('Component', 'api.ApiBase');

abstract class IntegrationRequestValidatorComponent extends ApiBaseComponent {

    protected $data = array();
    protected $userId = null;

    function __construct($data, $userId) {
        parent::__construct('integration');
        $this->data = $data;
        $this->userId = $userId;
    }

    abstract function validate();
}
