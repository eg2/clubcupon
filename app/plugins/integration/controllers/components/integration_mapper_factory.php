<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'integration.IntegrationRequestMapper');
App::import('Component', 'integration.IntegrationResponseMapper');

class IntegrationMapperFactoryComponent extends ApiBaseComponent {

    public function __construct() {
        parent::__construct('integration');
    }

    public function getMapper($type) {
        switch ($type) {
            case 'request':
                return new IntegrationRequestMapperComponent();
            case 'response':
                return new IntegrationResponseMapperComponent();
            default:
                return new IntegrationRequestMapperComponent();
        }
    }

}
