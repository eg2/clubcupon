<?php

App::import('Component', 'api.ApiBase');

abstract class IntegrationMapperComponent extends ApiBaseComponent {

    function __construct($prefix) {
        parent::__construct($prefix);
    }

    abstract function map($data);
}
