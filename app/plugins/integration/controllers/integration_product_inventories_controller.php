<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'integration.IntegrationMapperFactory');

class IntegrationProductInventoriesController extends IntegrationAppController {

    public $components = array(
        'integration.IntegrationRequestValidatorFactory',
        'product.ProductInventoryService',
    );
    public $uses = array(
        'ProductInventory'
    );
    public $actionsForAppSecure = array(
        'integration_product_inventories/save_or_update_product',
        'integration_product_inventories/update_list_products'
    );
    public $layout = 'json/default';

    public function __construct() {
        parent::__construct();
        $this->IntegrationMapperFactory = new IntegrationMapperFactoryComponent();
        $this->RequestMapper = $this->IntegrationMapperFactory->getMapper('request');
        $this->ResponseMapper = $this->IntegrationMapperFactory->getMapper('response');
    }

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function beforeRender() {
        $responseRequest = json_encode($this->viewVars['responseRequest']);
        $this->set('responseRequest', $responseRequest);
    }

    public function save_or_update_product() {
        $this->layout = 'json/default';
        $this->ApiLogger->notice('Request: ', $this->params['form']);
        try {
            $validator = $this->IntegrationRequestValidatorFactory->getSaveOrUpdateValidator($this->params['form'], $this->Auth->user('id'));
            $errors = $validator->validate();
            if (empty($errors)) {
                $productStock = $this->getFormParam('stock');
                $stock = (empty($productStock) ? 0 : $productStock);
                $request = $this->RequestMapper->map($this->params['form']);
                $product = $this->ProductInventoryService->createOrEditProductInventory($request['name'], $request['bac_product_id'], $request['description'], $request['bac_invoice_id'], $request['bac_libertya_id'], $request['bac_portal_id'], $stock, $request['id']);
                $created = $this->ResponseMapper->map($product['ProductInventory']);
            } else {
                throw new DomainException('Parametros faltantes');
            }
            $response['ProductInventory'] = $created;
        } catch (Exception $e) {
            $response['ProductInventory'] = '';
            $response['error'] = array(
                'exception' => $e->getMessage(),
                'fields' => $errors,
            );
        }
        $this->ApiLogger->notice('Response: ', $response);
        $this->set('responseRequest', $response);
    }

    public function update_list_products() {
        $this->layout = 'json/default';
        $this->ApiLogger->notice('Request: ', $this->data);
        $list = json_decode($this->data);
        $errors = array();
        foreach ($list as $products) {
            foreach ($products as $product) {
                $updated = $this->ProductInventory->updateStockByBacLibertyaId($product->productCode, $product->totalAvailableStock);
                if (!$updated) {
                    $errors[] = $product->productCode;
                }
            }
        }
        $response = array(
            "erroneos" => $errors
        );
        $this->ApiLogger->notice('Response: ', $response);
        $this->set('responseRequest', $response);
    }

}
