<?php

App::import('Component', 'integration.IntegrationMultiplaceKeyComparator');
App::import('Model', 'product.ProductInventory');

class IntegrationMultiplaceSynchronizeShell extends Shell {

    const NOT_FOUND_KEY = 'ZZZZZZZZZ';
    const SHOW_PAD = 13;
    const LINE_LENGTH = 1024;
    const FIELD_SEPARATOR = ";";
    const FIELD_SELLER_SKU = 0;
    const FIELD_NAME = 1;
    const FIELD_QUANTITY = 2;
    const FIELD_PRICE = 3;

    function __construct(&$dispatch) {
        parent::__construct($dispatch);
        $this->ProductInventory = new ProductInventory();
        $this->comparator = new IntegrationMultiplaceComparatorComponent('ProductInventory', 'source_identifier');
        $this->filename = 'https://www.dropbox.com/sh/4z15r869d8pt3d1/AADqKOnvf_2hMJXMwffadgola/reportCupon.csv?dl=1';
    }

    function findLocalRecords() {
        $records = array();
        $products = $this->ProductInventory->findAllMultiplace();
        if (!empty($products)) {
            $records = $products;
        }
        return $records;
    }

    function recordName($product) {
        return substr($product[self::FIELD_SELLER_SKU] . ' - ' . '($' . $product[self::FIELD_PRICE] . ') ' . $product[self::FIELD_NAME], 0, 255);
    }

    function recordStock($product) {
        return (int) filter_var($product[self::FIELD_QUANTITY], FILTER_SANITIZE_NUMBER_INT);
    }

    function recordSourceIdentifier($product) {
        return substr($product[self::FIELD_SELLER_SKU], 0, 255);
    }

    function recordPrice($product) {
        return (float) filter_var($product[self::FIELD_PRICE], FILTER_SANITIZE_NUMBER_FLOAT);
    }

    function record($product) {
        return array(
            'ProductInventory' => array(
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'deleted' => 0,
                'name' => $this->recordName($product),
                'stock' => $this->recordStock($product),
                'description' => null,
                'bac_product_id' => 0,
                'bac_libertya_id' => null,
                'bac_invoice_id' => null,
                'bac_portal_id' => 18,
                'source' => ProductInventory::SOURCE_MULTIPLACE,
                'source_identifier' => $this->recordSourceIdentifier($product),
                'price' => $this->recordPrice($product),
                'last_price_update' => "'" . date("Y-m-d H:i:s") . "'"
            )
        );
    }

    function findRemoteRecords() {
        $handle = fopen($this->filename, 'r');
        $records = array();
        if ($handle) {
            $i = 0;
            while ($product = fgetcsv($handle, self::LINE_LENGTH, self::FIELD_SEPARATOR)) {
                if ($i > 0) {
                    $records[$i] = $this->record($product);
                }
                $i++;
            }
            fclose($handle);
        } else {
            die("File not found: " . $this->filename);
        }
        usort($records, array(
            $this->comparator,
            'compare'
        ));
        return $records;
    }

    function lessThan($x, $y) {
        return $x['ProductInventory']['source_identifier'] < $y['ProductInventory']['source_identifier'];
    }

    function equalTo($x, $y) {
        return $x['ProductInventory']['source_identifier'] == $y['ProductInventory']['source_identifier'];
    }

    function greaterThan($x, $y) {
        return $x['ProductInventory']['source_identifier'] > $y['ProductInventory']['source_identifier'];
    }

    function isNotFound($record) {
        return $record['ProductInventory']['source_identifier'] == self::NOT_FOUND_KEY;
    }

    function updateRecord($sourceIdentifier, $record) {
        if (isset($record['ProductInventory']['stock'])) {
            $fields ['ProductInventory.stock'] = $record['ProductInventory']['stock'];
        }
        if (isset($record['ProductInventory']['price'])) {
            $fields ['ProductInventory.price'] = $record['ProductInventory']['price'];
        }
        if (isset($record['ProductInventory']['last_price_update'])) {
            $fields ['ProductInventory.last_price_update'] = $record['ProductInventory']['last_price_update'];
        }
        $conditions = array(
            'ProductInventory.source' => ProductInventory::SOURCE_MULTIPLACE,
            'ProductInventory.source_identifier' => $sourceIdentifier
        );
        return $this->ProductInventory->updateAll($fields, $conditions);
    }

    function create($record) {
        $this->ProductInventory->create();
        return $this->ProductInventory->save($record);
    }

    function showRecord($label, $record) {
        $text = $record['ProductInventory']['source_identifier'];
        if ($this->isNotFound($record)) {
            $text = "not found";
        }
        echo str_pad($label, self::SHOW_PAD) . ": {$text}\n";
    }

    function showAction($label) {
        echo str_pad('Action', self::SHOW_PAD) . ": {$label}\n";
    }

    function showResult($label) {
        $value = $label ? 'ok' : 'error';
        echo str_pad('Result', self::SHOW_PAD) . ": {$value}\n";
    }

    function showCount($label, $count) {
        echo str_pad($label, self::SHOW_PAD) . ": {$count}\n";
    }

    function showNewLine() {
        echo "\n";
    }

    function priceUpdated($localRecord, $remoteRecord) {
        return $localRecord['ProductInventory']['price'] !== $remoteRecord['ProductInventory']['price'];
    }

    function main() {
        $localRecords = $this->findLocalRecords();
        $remoteRecords = $this->findRemoteRecords();
        $localCount = count($localRecords);
        $remoteCount = count($remoteRecords);
        $l = 0;
        $r = 0;
        //
        $this->showCount('Local count', $localCount);
        $this->showCount('Remote count', $remoteCount);
        $this->showNewLine();
        //
        while ($l < $localCount || $r < $remoteCount) {
            if ($l == $localCount) {
                $localRecords[$l]['ProductInventory']['source_identifier'] = self::NOT_FOUND_KEY;
            }
            if ($r == $remoteCount) {
                $remoteRecords[$r]['ProductInventory']['source_identifier'] = self::NOT_FOUND_KEY;
            }
            $this->showRecord('Local record', $localRecords[$l]);
            $this->showRecord('Remote record', $remoteRecords[$r]);
            if ($this->lessThan($localRecords[$l], $remoteRecords[$r])) {
                //
                $this->showAction('local record update stock to zero');
                $record = array('ProductInventory' => array(
                        'stock' => 0
                ));
                $result = $this->updateRecord(
                        $localRecords[$l]['ProductInventory']['source_identifier'], $record
                );
                $this->showResult($result);
                $l++;
                //
            } elseif ($this->equalTo($localRecords[$l], $remoteRecords[$r])) {
                //
                $this->showAction('local record update stock to remote record stock');
                $record = array('ProductInventory' => array(
                        'stock' => $remoteRecords[$r]['ProductInventory']['stock']
                ));
                if ($this->priceUpdated($localRecords[$l], $remoteRecords[$r])) {
                    $this->showAction('local record update price to remote record price');
                    $record['ProductInventory']['price'] = $remoteRecords[$r]['ProductInventory']['price'];
                    $record['ProductInventory']['last_price_update'] = $remoteRecords[$r]['ProductInventory']['last_price_update'];
                }
                $result = $this->updateRecord(
                        $localRecords[$l]['ProductInventory']['source_identifier'], $record
                );
                $this->showResult($result);
                $l++;
                $r++;
                //
            } elseif ($this->greaterThan($localRecords[$l], $remoteRecords[$r])) {
                //
                $this->showAction('remote record create');
                $result = $this->create(
                        $remoteRecords[$r]
                );
                $this->showResult($result);
                $r++;
                //
            }
            $this->showNewLine();
        }
    }

}
