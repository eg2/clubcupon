<?php
header("Pragma: no-cache");
header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate");
header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('status: 200');

echo $content_for_layout;
?>
