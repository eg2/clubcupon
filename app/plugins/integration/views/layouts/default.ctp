<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Compra Clubcupon</title>
        <?php
            echo $javascript->link('/js/assets/jquery.js');
            echo $javascript->link('/js/assets/jquery_ujs.js');
            echo $javascript->link('/js/assets/underscore.js');
            echo $javascript->link('/js/assets/backbone.js');
            echo $javascript->link('/js/assets/backbone_rails_sync.js');
            echo $javascript->link('/js/assets/backbone_datalink.js');
            echo $javascript->link('/js/assets/vendor/backbone-localstorage.js');
            echo $javascript->link('/js/assets/vendor/jsDump.js');
            echo $javascript->link('/js/assets/backbone/buy.js.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/buy_form.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/buy_form_select.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/deal_detail.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/init_buy.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/pay_combined_summary.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/payment_option.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/payment_options_list.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/select_payment.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/select_payment_plan_item.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/wallet_payment_option.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/success_buy.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/payment_plan_item.js');
            echo $javascript->link('/js/assets/backbone/templates/buy/payment_plan_items_list.js');
            echo $javascript->link('/js/assets/backbone/models/deal.js.js');
            echo $javascript->link('/js/assets/backbone/models/deal_external.js.js');
            echo $javascript->link('/js/assets/backbone/models/payment_option.js.js');
            echo $javascript->link('/js/assets/backbone/models/payment_plan_item.js.js');
            echo $javascript->link('/js/assets/backbone/models/user.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/buy_form_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/buy_form_select_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/deal_detail_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/init_buy_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/pay_combined_summary.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/payment_option_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/payment_options_list_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/payment_plan_item_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/payment_plan_items_list_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/select_payment_plan_item_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/select_payment_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/wallet_payment_option_view.js.js');
            echo $javascript->link('/js/assets/backbone/views/buy/success_buy_view.js.js');
            echo $javascript->link('/js/assets/backbone/routers/buy_app_router.js.js');
            echo $javascript->link('/js/assets/backbone/utils/ajax_logger.js.js');

            //tw-bootstrap
            echo $javascript->link('/js/bootstrap/bootstrap.min.js');
            echo $html->css('/css/bootstrap/bootstrap.min.css');
            echo $html->css('/css/bootstrap/bootstrap-responsive.min.css');
            
            //mobile
            //echo $html->css('/css/mobile/api.css');
            //echo $html->css('/css/mobile/tema.css');
            echo $html->css('/css/mobile/mobile.bare.css');
            //echo $javascript->link('/js/mobile/funciones.js');
            
            //
        ?>
        
        <script type="text/javascript" src="<?php echo Configure::read('bac.static_content_url');?>/backend_web/WEB-JS/crearPagoLiteBox.js"></script>
        <script type="text/javascript" src="http://mp-tools.mlstatic.com/buttons/render.js"></script>
        <script type="text/javascript" src="<?php echo Configure::read('bac.static_content_url');?>/backend_web/WEB-JS/json2.js"></script>
        
    </head>
    <body>
        <div id="main">
            <?php
                echo $content_for_layout;
            ?>
        </div>
        <div id="divBAC"></div>
    </body>
</html>