<?php

App::import('Controller', 'api.ApiApp');

class IntegrationAppController extends ApiAppController {

    public $logFileNamePrefix = "integration";
    public $layout = 'integration.default';

    public function beforeFilter() {
        parent::beforeFilter();
    }

}
