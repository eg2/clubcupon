<?php
/**
 * Custom configurations
 */
$config['site']['name'] = 'altodot-framework.com';
$config['site']['version'] = 'v1.0a1';

$config['meta']['keywords'] = 'altodot, dev1';
$config['meta']['description'] = 'dev1 framework';

// CDN...
$config['cdn']['images'] = null; // 'http://images.localhost/';
$config['cdn']['css'] = null; // 'http://static.localhost/';

/*
date_default_timezone_set('Asia/Calcutta');

Configure::write('Config.language', 'spa');
setlocale (LC_TIME, 'es');
*/
?>