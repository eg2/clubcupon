<?php
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7820 $
 * @modifiedby    $LastChangedBy: renan.saddam $
 * @lastmodified  $Date: 2008-11-03 23:57:56 +0530 (Mon, 03 Nov 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

Router::parseExtensions('rss', 'csv', 'json', 'txt', 'pdf', 'kml', 'xml', 'mobile');
// REST support controllers
Router::mapResources(array('deals'));
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
$controllers = Cache::read('controllers_list', 'default');
if ($controllers === false) {
    $controllers = Configure::listObjects('controller');
    foreach($controllers as &$value) {
        $value = Inflector::underscore($value);
    }

array_push($controllers,'subscribe','company', 'company_candidates', 'deal', 'page', 'user', 'admin', 'deal_user','contactus', 'sitemap', 'robots', 'sitemap.xml', 'robots.txt', 'first', 'google9ce53ccf7b48e821.html','now.now_deals');

    $controllers = implode('|', $controllers);
    Cache::write('controllers_list', $controllers);
}

Router::connect('/now/',
        array('plugin' => 'now',
              'controller' => 'now_deals',
              'action'=> 'index'),
        array('city'=>'[a-zA-Z0-9\-]+',
            )
        );

Router::connect('/now/:controller/:action/*',
        array('plugin' => 'now',
              'controller' => 'now_deals',
              'action'=> 'index'),
        array('city'=>'[a-zA-Z0-9\-]+',
            )
        );

Router::connect('/', array(
    'controller' => 'deals',
    'action' => 'index'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));

Router::connect('/fibertel/:action', array(
    'controller' => 'fibertels',
) , array());

Router::connect('/xmldeals/:action', array(
    'controller' => 'xmldeals',
) , array());

Router::connect('/xmldeals/rss/:city_slug', array(
    'controller' => 'xmldeals',
) , array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+',
));

Router::connect('/xmldeals/rss/:city_slug/recent', array(
    'controller' => 'xmldeals',
) , array(
    'city_slug' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+',
    'options' => 'recent'
));

Router::connect('/:city/', array(
    'controller' => 'deals',
    'action' => 'index'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));

Router::connect('/:city/users/twitter/login/', array(
			'controller' => 'users',
			'action' => 'login',
			'type' => 'twitter'
		) ,array(
		'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
	)
);
//usado para los previews de las paginas...
Router::connect('/:city/pages/preview', array(
    'controller' => 'pages',
    'action' => 'preview'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/pages/*', array(
    'controller' => 'pages',
    'action' => 'view'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/pages/*', array(
    'controller' => 'pages',
    'action' => 'view'
));
Router::connect('/:city/company/user/register/*', array(
    'controller' => 'users',
    'action' => 'company_register'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));

Router::connect('/:city/contactus/', array(
    'controller' => 'contacts',
    'action' => 'add'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/deals/recent', array(
    'controller' => 'deals',
    'action' => 'index',
    'type' => 'recent'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/deals/company/:company', array(
    'controller' => 'deals',
    'action' => 'index',
) , array(
    'company' => '[a-zA-Z0-9\-]+',
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/city_suggestions/new', array(
    'controller' => 'city_suggestions',
    'action' => 'add',
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/subscribe', array(
    'controller' => 'subscriptions',
    'action' => 'add',
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/subscribeadd', array(
    'controller' => 'subscriptions',
    'action' => 'add',
) , array());

Router::connect('/:city/' . Configure::read('Routing.admin') , array(
    'controller' => 'users',
    'action' => 'stats',
    'prefix' => Configure::read('Routing.admin') ,
    'admin' => 1
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/robots', array(
    'controller' => 'devs',
    'action' => 'robots',
'ext' => 'txt'
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/sitemap', array('controller' => 'xmldeals', 'action' => 'sitemap', 'ext' => 'xml'), array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/img/:size/*', array(
    'controller' => 'images',
    'action' => 'view'
) , array(
    'size' => '(?:[a-zA-Z_]*)*'
));
Router::connect('/files/*', array(
    'controller' => 'images',
    'action' => 'view',
    'size' => 'original'
));
Router::connect('/img/*', array(
    'controller' => 'images',
    'action' => 'view',
    'size' => 'original'
));
Router::connect('/' . Configure::read('Routing.admin') . '/:controller/:action/*', array(
    'admin' => true
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/' . Configure::read('Routing.admin') . '/:controller/:action/*', array(
    'admin' => true
) , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));
Router::connect('/:city/:controller/:action/*', array() , array(
    'city' => '(?!' . $controllers . ')[a-zA-Z0-9\-]+'
));

?>