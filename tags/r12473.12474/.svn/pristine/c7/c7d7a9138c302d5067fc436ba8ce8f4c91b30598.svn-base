<?php

App::import('Core', 'Component');
App::import('Component', 'api.ApiBase');
App::import('Component', 'api.ApiBac');
App::import('Model', 'payment.PaymentDeal');

class PaymentBacComponent extends ApiBaseComponent {

    const ID_USUARIO_PORTAL = 'ID_USUARIO_PORTAL';
    const ID_PAGO_PORTAL = 'ID_PAGO_PORTAL';
    const ID_PAGO = 'ID_PAGO';
    const ID_MEDIO_PAGO = 'ID_MEDIO_PAGO';
    const ID_GATEWAY = 'ID_GATEWAY';
    const ESTADO_DEL_PAGO = 'ESTADO_DEL_PAGO';
    const ESTADO_DEL_PAGO_EN_GATEWAY = 'ESTADO_DEL_PAGO_EN_GATEWAY';
    const MESSAGE_BAC_EXCEPTION_NO_USER = 'TransaccionService.usuarioInexistente';
    const MESSAGE_BAC_EXCEPTION_NO_PORTAL = 'TransaccionService.portalClienteInexistente';
    const MESSAGE_BAC_EXCEPTION_NEW_NO_PAYMENT = 'PagoService.pagoNuevoPortalInexistente';
    const MESSAGE_BAC_EXCEPTION_NO_PAYMENT = 'TransaccionService.pagoInexistente';
    const DATA_BAC_ESTADO_ACREDITADO = 'ACREDITADO';
    const DATA_BAC_ESTADO_PENDIENTE = 'PENDIENTE';
    const DATA_BAC_ESTADO_CANCELADO = 'CANCELADO';
    const DATA_BAC_ESTADO_ANULADO = 'ANULADO';
    const DATA_BAC_ESTADO_EXPIRADO = 'EXPIRADO';
    const DATA_BAC_ESTADO_ANULADO_INICIADO = 'ANULADO_INICIADO';

    private $bacPortalId;
    private $ApiBac;
    private $enableQueryBac;
    private $PaymentDeal;

    function __construct() {
        parent::__construct();
        $this->bacPortalId = Configure::read('BAC.id_portal');
        $this->ApiBac = new ApiBacComponent();
        $this->enableQueryBac = false;
        $this->PaymentDeal = new PaymentDeal();
    }

    public function isKnowBacException($messageBacException) {
        return ((stripos($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_USER) !== false) || (stripos($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_PORTAL) !== false) || (stripos($messageBacException, self::MESSAGE_BAC_EXCEPTION_NO_PAYMENT) !== false) || (stripos($messageBacException, self::MESSAGE_BAC_EXCEPTION_NEW_NO_PAYMENT) !== false));
    }

    public function checkPayment($userId, $dealExternal, $portal = 0) {
        try {
            $deal = $this->PaymentDeal->findById($dealExternal['PaymentDealExternal']['deal_id']);
            $parameters = array(
                'idPortal' => $this->bacPortalId,
                'idUsuarioPortal' => $userId,
                'idPagoPortal' => $dealExternal['PaymentDealExternal']['id']
            );
            $this->ApiLogger->notice('Consultando a bac deal_external_id :' . $dealExternal['PaymentDealExternal']['deal_id'] . 'parametros:', $parameters);
            return $this->ApiBac->checkPayment($parameters);
        } catch (Exception $e) {
            $this->ApiLogger->error('Error al consultar pago en Bac', array(
                'parametros' => $parameters,
                'exception' => $e
            ));
            throw $e;
        }
    }

    public function isReputableState($dataBac) {
        return $dataBac->estado == self::DATA_BAC_ESTADO_ACREDITADO;
    }

    public function isSetGateway($dataBac) {
        return isset($dataBac->estadoDescripcionGateway);
    }

    public function getStateDescriptionGateway($dataBac) {
        return substr($dataBac->estadoDescripcionGateway, 0, 63);
    }

    public function isStatePending($dataBac) {
        return $dataBac->estado == self::DATA_BAC_ESTADO_PENDIENTE;
    }

    public function isKowBacState($dataBac) {
        return in_array($dataBac->estado, array(
            self::DATA_BAC_ESTADO_ACREDITADO,
            self::DATA_BAC_ESTADO_CANCELADO,
            self::DATA_BAC_ESTADO_ANULADO,
            self::DATA_BAC_ESTADO_EXPIRADO,
            self::DATA_BAC_ESTADO_ANULADO_INICIADO
        ));
    }

    public function getIdPago($dataBac) {
        return $dataBac->idPago;
    }

    public function isCancellationState($dataBac) {
        return in_array($dataBac->estado, array(
            self::DATA_BAC_ESTADO_CANCELADO,
            self::DATA_BAC_ESTADO_ANULADO,
            self::DATA_BAC_ESTADO_EXPIRADO,
            self::DATA_BAC_ESTADO_ANULADO_INICIADO
        ));
    }

}
