<?php

App::import('Model', 'web.WebCity');
include_once dirname(__FILE__) . '/app_cached_controller.php';

class AppController extends Controller {

    var $components = array(
        'RequestHandler',
        'Security',
        'Auth',
        'XAjax',
        'DebugKit.Toolbar',
        'Cookie',
        'UserMenu',
        'Pdf'
    );
    var $helpers = array(
        'Html',
        'Javascript',
        'AutoLoadPageSpecific',
        'Form',
        'Asset',
        'Auth',
        'Time',
        'RestXml',
        'RestJson',
        'clubcupon'
    );
    var $uses = array(
        'Company',
        'User',
        'UserLogin'
    );
    var $pluginExceptions = array(
        'now',
        'efg',
        'api',
        'order',
        'integration',
        'promotion',
        'beacon'
    );
    var $cookieTerm = '+4 weeks';
    protected $WebCity;

    function beforeRender() {
        $this->Cookie->write('user_language', 'es', false);
        $this->set('meta_for_layout', Configure::read('web.meta'));
        $this->set('js_vars_for_layout', (isset($this->js_vars)) ? $this->js_vars : '');
        if (Configure::read('site.is_api_enabled')) {
            if (!empty($this->params['url']['api_key']) && !empty($this->params['url']['api_token'])) {
                if (!empty($this->viewVars['api_response'])) {
                    $this->set('response', $this->viewVars['api_response']);
                } else {
                    $this->set('response', $this->viewVars);
                }
            }
        }
        if ($this->_isLoggedIn($this->Auth->user())) {
            $this->_setMenuLinksForCompany($this->Auth->user(), $this->Company->findByUser($this->Auth->user()));
            if ($this->User->isCompanyWithNow($this->Auth->user()) || $this->User->isCompanyCandidate($this->Auth->user())) {
                $this->set('username', $this->_getMailAccount($this->Auth->user('email')));
            } else {
                $this->set('username', $this->Auth->user('username'));
            }
        }
        if ($this->UserMenu) {
            $this->set('menuOptions', $this->UserMenu->returnMenuOptionsForUser($this->Auth->user('id')));
        }
        parent::beforeRender();
    }

    function _getMailAccount($email) {
        $email = explode('@', $email);
        return $email[0];
    }

    function __construct() {
        parent::__construct();
        $this->WebCity = new WebCity();
        App::import('vendor', 'Precision', array(
            'file' => 'Precision/Precision.php'
        ));
        App::import('Model', 'Setting');
        App::import('Debugger');
        $setting_model_obj = new Setting();
        $databaseSettings = Cache::read('databaseSettings', 'very_short');
        if ($databaseSettings !== false) {
            $settings = $databaseSettings;
        } else {
            $settings = $setting_model_obj->getKeyValuePairs();
        }
        Cache::write('databaseSettings', $settings, 'very_short');
        Configure::write($settings);
        $current_city_slug = '';
        if (!empty($_GET['url'])) {
            $city_slug = explode('/', $_GET['url']);
            $current_city_slug = (!empty($city_slug[0])) ? $city_slug[0] : Configure::read('site.city');
        } else {
            $current_city_slug = Configure::read('site.city');
        }
        $lang_code = Configure::read('site.language');
        if (!empty($_COOKIE['CakeCookie']['user_language'])) {
            $lang_code = $_COOKIE['CakeCookie']['user_language'];
        } else if (!empty($current_city_slug)) {
            $cookie_city_slug = !empty($_COOKIE['CakeCookie']['city_slug']) ? $_COOKIE['CakeCookie']['city_slug'] : '';
            if (empty($cookie_city_slug) || ($current_city_slug != $cookie_city_slug)) {
                App::import('Model', 'City');
                $city_model_obj = new City();
                $city = $city_model_obj->find('first', array(
                    'conditions' => array(
                        'City.slug' => $current_city_slug,
                        'City.is_approved' => 1
                    ),
                    'contain' => array(
                        'Language' => array(
                            'fields' => array(
                                'Language.iso2'
                            )
                        )
                    ),
                    'fields' => array(
                        'City.language_id'
                    ),
                    'recursive' => 1
                ));
                if (!empty($city['Language']['iso2'])) {
                    setcookie('CakeCookie[city_language]', $city['Language']['iso2']);
                    $lang_code = $city['Language']['iso2'];
                } else {
                    setcookie('CakeCookie[city_language]', $lang_code);
                }
            } else {
                $lang_code = isset($_COOKIE['CakeCookie']) && isset($_COOKIE['CakeCookie']['city_language']) ? $_COOKIE['CakeCookie']['city_language'] : '';
            }
        }
        $lang_code = 'es';
        Configure::write('lang_code', $lang_code);
        App::import('Model', 'Translation');
        $translation_model_obj = new Translation();
        Cache::set(array(
            'duration' => '+100 days'
        ));
        $translations = Cache::read($lang_code . '_translations');
        if (empty($translations) and $translations === false) {
            $translations = $translation_model_obj->find('all', array(
                'conditions' => array(
                    'Language.iso2' => $lang_code
                ),
                'fields' => array(
                    'Translation.key',
                    'Translation.lang_text'
                ),
                'contain' => array(
                    'Language' => array(
                        'fields' => array(
                            'Language.iso2'
                        )
                    )
                ),
                'recursive' => 0
            ));
            Cache::set(array(
                'duration' => '+100 days'
            ));
            Cache::write($lang_code . '_translations', $translations);
        }
        if (!empty($translations)) {
            foreach ($translations as $translation) {
                $GLOBALS['_langs'][$translation['Language']['iso2']][$translation['Translation']['key']] = $translation['Translation']['lang_text'];
            }
        }
    }

    function _hasAdwordDiff($data) {
        if (isset($this->params["url"]["utm_source"])) {
            foreach (Configure::read('google_adwords.fields') as $key) {
                if ($this->Session->read($key) != urldecode($this->params['url'][$key])) {
                    return true;
                }
            }
        }
        return false;
    }

    function _getAdwordFields($data) {
        $adwordFields = array();
        foreach (Configure::read('google_adwords.fields') as $key) {
            if (isset($data[$key])) {
                $adwordFields[$key] = urldecode($data[$key]);
            }
        }
        return $adwordFields;
    }

    function _getAdwordFieldsInSession() {
        $adwordFields = array();
        foreach (Configure::read('google_adwords.fields') as $key) {
            if (!is_null($this->Session->read($key))) {
                $adwordFields[$key] = $this->Session->read($key);
            }
        }
        return $adwordFields;
    }

    function _setAdwordFieldsInSession($data) {
        foreach (Configure::read('google_adwords.fields') as $key) {
            if (isset($data[$key])) {
                $this->Session->write($key, $data[$key]);
            } else {
                $this->Session->delete($key);
            }
        }
    }

    function beforeFilter() {
        if ($this->_hasAdwordDiff($this->params['url'])) {
            $this->_setAdwordFieldsInSession($this->_getAdwordFields($this->params['url']));
        }
        $this->set('homeForCompany', false);
        $this->Email->delivery = Configure::read('site.email.delivery');
        $this->Email->smtpOptions = Configure::read('site.email.smtp_options');
        $cur_page = $this->params['controller'] . '/' . $this->params['action'];
        if (!empty($this->params['named']['city_name'])) {
            App::import('Model', 'City');
            $this->City = new City();
            $city = $this->City->find('first', array(
                'conditions' => array(
                    'City.name' => $this->params['named']['city_name'],
                    'City.is_approved' => 1
                ),
                'contain' => array(
                    'Language' => array(
                        'fields' => array(
                            'Language.iso2'
                        )
                    )
                ),
                'fields' => array(
                    'City.language_id',
                    'City.slug'
                ),
                'recursive' => 1
            ));
            if (!empty($city)) {
                $this->redirect(array(
                    'controller' => 'deals',
                    'action' => 'index',
                    'city' => $city['City']['slug']
                ));
            } else {
                $this->redirect(Router::url('/', true));
            }
        }
        if (Configure::read('site.is_ssl_for_deal_buy_enabled')) {
            $secure_array = array(
                'deals/buy'
            );
            $cur_page = $this->params['controller'] . '/' . $this->params['action'];
            if (in_array($cur_page, $secure_array) && $this->params['action'] != 'flashupload') {
                $this->Security->blackHoleCallback = 'forceSSL';
                $this->Security->requireSecure($this->params['action']);
            } else if (env('HTTPS') && !$this->RequestHandler->isAjax()) {
                $this->_unforceSSL();
            }
        }
        if ($this->params['controller'] != 'images') {
            App::import('Model', 'City');
            $citySlugFinder = !empty($this->params['named']['city']) ? $this->params['named']['city'] : Configure::read('site.city');
            $cacheKeyCitySlug = 'cache_finder_' . $citySlugFinder;
            $city = Cache::read($cacheKeyCitySlug, 'very_short');
            $this->City = new City();
            if ($city === false) {
                $city = $this->City->find('first', array(
                    'conditions' => array(
                        'City.slug' => $citySlugFinder,
                        'City.is_approved' => 1
                    ),
                    'fields' => array(
                        'City.id',
                        'City.name',
                        'City.slug',
                        'City.language_id',
                        'City.twitter_url',
                        'City.twitter_username',
                        'City.facebook_url',
                        'City.is_group',
                        'City.is_business_unit'
                    ),
                    'recursive' => - 1
                ));
                Cache::write($cacheKeyCitySlug, $city, 'very_short');
            }
            if (!empty($this->params['named']['city']) and empty($city)) {
                $this->cakeError('error404');
            }
            $city['City']['twitter_url'] = empty($city['City']['twitter_url']) ? Configure::read('twitter.site_twitter_url') : $city['City']['twitter_url'];
            $city['City']['twitter_username'] = empty($city['City']['twitter_username']) ? Configure::read('twitter.username') : $city['City']['twitter_username'];
            $city['City']['facebook_url'] = empty($city['City']['facebook_url']) ? Configure::read('facebook.site_facebook_url') : $city['City']['facebook_url'];
            $this->set('city_is_business_unit', $city['City']['is_business_unit']);
            $this->set('city_id', $city['City']['id']);
            $this->set('city_name', $city['City']['name']);
            $this->set('city_is_group', $city['City']['is_group']);
            $this->set('city_slug', $city['City']['slug']);
            $this->set('city_twitter_url', $city['City']['twitter_url']);
            $this->set('city_twitter_username', $city['City']['twitter_username']);
            $this->set('rss_feed_url', Configure::read('static_domain_for_mails') . '/xmldeals/rss/');
            $this->set('rss_feed_url_for_city', Configure::read('static_domain_for_mails') . '/xmldeals/rss/' . $city['City']['slug']);
            $this->set('city_facebook_url', $city['City']['facebook_url']);
            $this->set('city_is_group', $city['City']['is_group']);
            Configure::write('Actual.city_id', $city['City']['id']);
            Configure::write('Actual.city_name', $city['City']['name']);
            Configure::write('Actual.city_slug', $city['City']['slug']);
            Configure::write('Actual.city_twitter_url', $city['City']['twitter_url']);
            Configure::write('Actual.city_facebook_url', $city['City']['facebook_url']);
            Configure::write('Actual.city_is_group', $city['City']['is_group']);
            App::import('Model', 'ProcessRun');
            $processRun = new ProcessRun();
            $this->set('crons_locked', $processRun->isRunningByName('LOCK'));
            if (!empty($this->params['named']['banner']))
                $this->set('from_banner', $this->params['named']['banner']);
            else
                $this->set('from_banner', false);
            if ($this->Auth->user('id')) {
                App::import('Model', 'User');
                $user_model_obj = new User();
                $this->set('user_available_balance', $user_model_obj->checkUserBalance($this->Auth->user('id')));
            }
        }
        if (!empty($this->params['named']['city'])) {
            $myCity = new City();
            $cityObj = $myCity->findBySlug($this->params['named']['city']);
            if ($cityObj && !$cityObj['City']['is_corporate']) {
                $this->Cookie->write('city_slug', $this->params['named']['city'], false);
                $wCity = $this->WebCity->findBySlug($this->params['named']['city']);
                if ($wCity && !$wCity['WebCity']['is_group']) {
                    $this->Cookie->write('geo_city_slug', $this->params['named']['city'], false);
                }
            }
        }
        $default_city = Cache::read('site.default_city', 'long');
        if (($default_city = Cache::read('site.default_city', 'long')) === false) {
            Cache::write('site.default_city', Configure::read('site.city'), array(
                'config' => 'long'
            ));
        }
        if (($city_url = Cache::read('site.city_url', 'long')) === false) {
            Cache::write('site.city_url', Configure::read('site.city_url'), array(
                'config' => 'long'
            ));
        }
        $this->loadModel('BannedIp');
        $bannedIp = $this->BannedIp->checkIsIpBanned($this->RequestHandler->getClientIP());
        if (empty($bannedIp)) {
            $bannedIp = $this->BannedIp->checkRefererBlocked(env('HTTP_REFERER'));
        }
        if (!empty($bannedIp)) {
            if (!empty($bannedIp['BannedIp']['redirect'])) {
                header('location: ' . $bannedIp['BannedIp']['redirect']);
            } else {
                $this->cakeError('error403');
            }
        }
        $cur_page = $this->params['controller'] . '/' . $this->params['action'];
        if (empty($this->params['requested']) and $cur_page != 'images/view' and $cur_page != 'devs/robots' and Configure::read('site.maintenance_mode') && (($this->Auth->user('user_type_id') && ConstUserTypes::isNotLikeAdmin($this->Auth->user('user_type_id'))) or ( empty($this->params['prefix']) or ( $this->params['prefix'] != 'admin' and $cur_page != 'users/admin_login')))) {
            $this->cakeError('error500');
        }
        if ((isset($_SERVER['HTTP_USER_AGENT']) and ( (strtolower($_SERVER['HTTP_USER_AGENT']) == 'shockwave flash') or ( strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'adobe flash player') !== false))) and isset($this->params['pass'][0]) and ( $this->action == 'flashupload')) {
            session_id($this->params['pass'][0]);
            session_start();
        }
        if ($this->Auth->user('fb_user_id') || (!$this->Auth->user() && Configure::read('facebook.is_enabled_facebook_connect'))) {
            App::import('Vendor','facebook-php-sdk-v5/autoload');            
            $this->facebook = new Facebook\Facebook([
              'app_id' => Configure::read('facebook.fb_api_key'),
              'app_secret' => Configure::read('facebook.fb_secrect_key'),                
              'default_graph_version' => 'v2.9',
              'cookie' => true                
              ]);  
        }
        if (strpos($this->here, '/view/') !== false) {
            trigger_error('*** dev1framework: Do not view page through /view/; use singular/slug', E_USER_ERROR);
        }
        $methods = array_flip($this->methods);
        if (!isset($methods[strtolower($this->params['action'])])) {
            return $this->cakeError('missingAction', array(
                        array(
                            'className' => Inflector::camelize($this->params['controller'] . "Controller"),
                            'action' => $this->params['action'],
                            'webroot' => $this->webroot,
                            'url' => $this->here,
                            'base' => $this->base
                        )
            ));
        }
        if (Configure::read('site.is_api_enabled')) {
            $this->_handleRest();
        }
        $this->_checkAuth();
        $admin_layout_exceptional_array = array(
            'deals/index'
        );
        if (in_array($cur_page, $admin_layout_exceptional_array) && isset($this->params['named']['company'])) {
            $this->layout = 'admin_no_sidebar';
        }
        $geo_code_exceptional_array = array(
            'users/login',
            'users/register',
            'users/refer',
            'deal/buy',
            'gift_users/view_gift_card'
        );
        $cur_page = $this->params['controller'] . '/' . $this->params['action'];
        $this->js_vars['cfg']['is_geo_redirect_enabled'] = 0;
        if (!in_array($cur_page, $geo_code_exceptional_array))
            $this->js_vars['cfg']['is_geo_redirect_enabled'] = 1;
        $this->js_vars['cfg']['path_relative'] = Router::url('/');
        $this->js_vars['cfg']['path_absolute'] = Router::url('/', true);
        if ($this->Auth->user('fb_user_id') || (!$this->Auth->user()) && Configure::read('facebook.is_enabled_facebook_connect')) {
            $this->js_vars['cfg']['api_key'] = Configure::read('facebook.fb_api_key');
        }
        $this->js_vars['cfg']['date_format'] = Configure::read('site.date.format');
        $this->js_vars['cfg']['today_date'] = date('Y-m-d');
        parent::beforeFilter();
    }

    function _checkAuth() {
        $this->Auth->loginAction = 'users/login';
        $this->Auth->fields = array(
            'username' => Configure::read('user.using_to_login'),
            'password' => 'password'
        );
        $exception_array = array(
            'firsts/register',
            'deals/bac_transaction',
            'deals/autocomplete',
            'deals/buyOk',
            'deals/buyCanceled',
            'deals/buy_without_subdeal',
            'deals/buyPending',
            'deals/landingofertas',
            'deals/check_clarin365_card_number',
            'deals/check_gift_code',
            'firsts/circa',
            'firsts/index',
            'firsts/index_cordoba',
            'firsts/promo',
            'firsts/beta',
            'firsts/recommend',
            'firsts/option1',
            'firsts/option2',
            'firsts/selcontacts',
            'pages/view',
            'pages/links',
            'pages/estopa',
            'pages/display',
            'pages/home',
            'pages/landingmobile',
            'deals/index',
            'deals/view',
            'deals/display_secondary_deals',
            'subscriptions/add',
            'cities/index',
            'companies/view',
            'contacts/show_captcha',
            'users/register',
            'users/registerok',
            'users/registernow',
            'users/registernowok',
            'users/company_register',
            'users/register_company',
            'users/login',
            'users/logout',
            'users/reset',
            'users/forgot_password',
            'users/openid',
            'users/activation',
            'users/resend_activation',
            'users/view',
            'users/show_captcha',
            'users/show_now_captcha',
            'users/captcha_play',
            'users/buy_prelogin',
            'images/view',
            'devs/robots',
            'contacts/add',
            'contacts/show_captcha',
            'contacts/captcha_play',
            'images/view',
            'cities/autocomplete',
            'states/autocomplete',
            'users/admin_login',
            'users/admin_logout',
            'languages/change_language',
            'subscriptions/add',
            'subscriptions/index',
            'subscriptions/unsubscribe',
            'subscriptions/unsubscribe_all',
            'subscriptions/find_total_available_balance_by_email',
            'users/referred_users',
            'users/resend_activemail',
            'subscriptions/home',
            'subscriptions/city_suggestions',
            'subscriptions/facebook',
            'pages/refer_a_friends',
            'users/refer',
            'user_cash_withdrawals/process_masspay_ipn',
            'deals/barcode',
            'city_suggestions/add',
            'cities/twitter_facebook',
            'user_comments/index',
            'deals/buy',
            'deals/buy_as_gift',
            'deals/process_user',
            'deals/_buyDeal',
            'deals/payment_success',
            'deals/payment_cancel',
            'deals/admin_search',
            'deals/cronjob_update_status',
            'deals/cronjob_send_deals_mailing',
            'deals/cronjob_process_deal_status',
            'deals/cronjob_process_send_coupons',
            'companies/view',
            'page/learn',
            'deals/company_deals',
            'gift_users/view_gift_card',
            'xmldeals/sitemap',
            'devs/robotos',
            'deals/landingpre',
            'xmldeals/dealandia',
            'xmldeals/rss',
            'xmldeals/csv',
            'xmldeals/descuentocity',
            'widgets/wizzard',
            'widgets/widget',
            'widgets/clarin',
            'widgets/clarin_2',
            'widgets/img',
            'errors/error404',
            'banners/front',
            'banners/logout',
            'pages/preview',
            'corporate_guests/check_corporate_pin',
            'corporate_guests/email_signin',
            'deals/register_user_to_buy',
            'alldeals/show',
            'passbooks/generate_passbook_file',
            'dashboards/company_campaign_liquidations',
            'dashboards/company_deals_by_campaign_code',
            'dashboards/company_liquidation_details',
            'deal_users/send_cupon',
            'promotions/christmas'
        );
        $cur_page = $this->params['controller'] . '/' . $this->params['action'];
        if (!in_array($cur_page, $exception_array) && $this->params['action'] != 'flashupload' && !in_array($this->params['plugin'], $this->pluginExceptions)) {
            if (!$this->Auth->user('id')) {
                $cookie_hash = $this->Cookie->read('User.cookie_hash');
                if (!empty($cookie_hash)) {
                    if (is_integer($this->cookieTerm) || is_numeric($this->cookieTerm)) {
                        $expires = time() + intval($this->cookieTerm);
                    } else {
                        $expires = strtotime($this->cookieTerm, time());
                    }
                    App::import('Model', 'User');
                    $user_model_obj = new User();
                    $this->data = $user_model_obj->find('first', array(
                        'conditions' => array(
                            'User.cookie_hash =' => md5($cookie_hash),
                            'User.cookie_time_modified <= ' => date('Y-m-d H:i:s', $expires)
                        ),
                        'fields' => array(
                            'User.' . Configure::read('user.using_to_login'),
                            'User.password'
                        ),
                        'recursive' => - 1
                    ));
                    if ($this->Auth->login($this->data)) {
                        $user_model_obj->UserLogin = new UserLogin();
                        $user_model_obj->UserLogin->insertUserLogin($this->Auth->user('id'));
                    }
                }
                $this->Session->setFlash(__l('Authorization Required'));
                $is_admin = false;
                if (isset($this->params['prefix']) and $this->params['prefix'] == 'admin') {
                    $is_admin = true;
                }
                $this->redirect(array(
                    'plugin' => '',
                    'controller' => 'users',
                    'action' => 'login',
                    'admin' => $is_admin,
                    '?f=' . $this->params['url']['url']
                ));
            }
            if (isset($this->params['prefix']) and $this->params['prefix'] == 'admin' and ConstUserTypes::isNotPrivilegedUserOrAdmin($this->Auth->user('user_type_id'))) {
                $this->redirect('/');
            }
            if (ConstUserTypes::isPrivilegedUser($this->Auth->user('user_type_id'))) {
                $allowedActions = array(
                    'deals/admin_index',
                    'deals/admin_search',
                    'subscriptions/admin_export',
                    'users/admin_index',
                    'users/admin_export',
                    'users/admin_change_password',
                    'deals',
                    'deals/admin_add',
                    'deals/admin_edit',
                    'deals/coupons_export',
                    'deals/admin_send_upcoming_deals',
                    'deals/admin_add_pins',
                    'companies/admin_edit',
                    'companies/admin_index',
                    'subscriptions/admin_index',
                    'companies/admin_edit',
                    'companies/admin_add',
                    'deals/admin_update',
                    'deal_users/admin_index',
                    'deal_users/admin_update',
                    'deal_users/view',
                    'transactions/admin_index',
                    'clusters/admin_index',
                    'clusters/admin_add'
                );
                $this->layout = 'admin';
                $requestedAction = $this->params['controller'] . '/' . $this->params['action'];
                if (!in_array($requestedAction, $allowedActions)) {
                    die('Su perfil de usuario no le permite el acceso a esta pÃ¡gina.');
                }
            }
        } else {
            $this->Auth->allow('*');
        }
        $this->Auth->autoRedirect = false;
        $this->Auth->userScope = array(
            'User.is_email_confirmed' => 1
        );
        if (isset($this->Auth)) {
            Debugger::log(sprintf('APP_CONTROLLER::_checkAuth - isset(this->Auth) '), LOG_DEBUG);
            $this->Auth->loginError = sprintf('No estÃ¡s registrado con esos datos Ã‚Â¿estÃ¡s intentando loguearte con Facebook?');
        }
        $this->layout = 'default';
        if (ConstUserTypes::isPrivilegedUserOrAdmin($this->Auth->user('user_type_id')) && (isset($this->params['prefix']) and $this->params['prefix'] == 'admin')) {
            $this->layout = 'admin';
        }
        if (!empty($this->params['url']['api_key']) && !empty($this->params['url']['api_token'])) {
            $this->layout = false;
            $this->viewPath = 'api';
        }
        if (Configure::read('site.is_mobile_app') and stripos(getenv('HTTP_HOST'), 'm.') === 0) {
            $this->layoutPath = 'mobile';
            if (file_exists(VIEWS . $this->viewPath . DS . 'mobile' . DS . $this->params['action'] . $this->ext)) {
                $this->viewPath .= DS . 'mobile';
            }
        }
        if (Configure::read('site.is_api_enabled')) {
            if (!empty($this->params['url']['api_key']) && !empty($this->params['url']['api_token'])) {
                $this->layout = false;
                $this->viewPath = 'api';
            }
        }
    }

    function autocomplete($param_encode = null, $param_hash = null) {
        $modelClass = Inflector::singularize($this->name);
        $conditions = false;
        if (isset($this->{$modelClass}->_schema['is_approved'])) {
            $conditions['is_approved = '] = '1';
        }
        $this->XAjax->autocomplete($param_encode, $param_hash, $conditions);
        if (is_null($this->viewVars['json'])) {
            $this->viewVars['json'] = array();
        }
    }

    function show_captcha() {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';

        $img = new securimage();
        $img->show();
        $this->autoRender = false;
    }

    function show_now_captcha() {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';

        $img = new securimage();
        $img->image_width = 240;
        $img->image_height = 50;
        $img->font_size = 32;
        $img->text_x_start = 50;
        $img->show();
        $this->autoRender = false;
    }

    function captcha_play() {
        include_once VENDORS . DS . 'securimage' . DS . 'securimage.php';

        $img = new Securimage();
        $this->disableCache();
        $this->RequestHandler->respondAs('wav', array(
            'attachment' => 'captcha.wav'
        ));
        echo $img->getAudibleCode();
    }

    function _uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    function _redirectGET2Named($whitelist_param_names = null) {
        $query_strings = array();
        $ajax_query_strings = '';
        if (is_array($whitelist_param_names)) {
            foreach ($whitelist_param_names as $param_name) {
                if (!empty($this->params['url'][$param_name])) {
                    if ($this->params['isAjax']) {
                        $ajax_query_strings .= $param_name . ':' . $this->params['url'][$param_name] . '/';
                    } else {
                        $query_strings[$param_name] = $this->params['url'][$param_name];
                    }
                }
            }
        } else {
            $query_strings = $this->params['url'];
            unset($query_strings['url']);
        }
        if (!empty($query_strings) || !empty($ajax_query_strings)) {
            if ($this->params['isAjax']) {
                $this->redirect(array(
                    'controller' => $this->params['controller'],
                    'action' => $this->params['action'],
                    $ajax_query_strings
                        ), null, true);
            } else {
                $query_strings = array_merge($this->params['named'], $query_strings);
                $this->redirect($query_strings, null, true);
            }
        }
    }

    public function redirect($url, $status = null, $exit = true) {
        if (Cache::read('site.city_url', 'long') == 'prefix') {
            parent::redirect(router_url_city($url, $this->params['named']), $status, $exit);
        }
        parent::redirect($url, $status, $exit);
    }

    public function flash($message, $url, $pause = 1) {
        if (Cache::read('site.city_url', 'long') == 'prefix') {
            parent::flash($message, router_url_city($url, $this->params['named']), $pause);
        }
        parent::redirect($message, $url, $pause);
    }

    function forceSSL() {
        $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    }

    function _unforceSSL() {
        if (empty($this->params['requested']))
            $this->redirect('http://' . $_SERVER['SERVER_NAME'] . $this->here);
    }

    function _handleRest() {
        if (!empty($this->params['url']['api_key']) && !empty($this->params['url']['api_token'])) {
            $this->Security->enabled = false;
            $this->loadModel('User');
            $this->data = $this->User->find('first', array(
                'conditions' => array(
                    'User.api_key' => $this->params['url']['api_key'],
                    'User.api_token' => $this->params['url']['api_token']
                ),
                'fields' => array(
                    'User.' . Configure::read('user.using_to_login'),
                    'User.password'
                ),
                'recursive' => - 1
            ));
            if (!$this->Auth->login($this->data)) {
                $this->Session->setFlash(__l('Your API authorization request failed. Please try again'));
                $this->cakeError('error404');
            }
        }
    }

    function objectSpec2Object($object) {
        if (is_array($object)) {
            $object = null;
        }
        if (is_string($object)) {
            $assoc = null;
            if (strpos($object, '.') !== false) {
                list($object, $assoc) = explode('.', $object);
            }
            if (isset($assoc, $this->{$object}->{$assoc})) {
                $object = $this->{$object}->{$assoc};
            } elseif (isset($assoc, $this->{$this->modelClass}, $this->{$this->modelClass}->{$assoc})) {
                $object = $this->{$this->modelClass}->{$assoc};
            } elseif (isset($this->{$object})) {
                $object = $this->{$object};
            } elseif (isset($this->{$this->modelClass}, $this->{$this->modelClass}->{$object})) {
                $object = $this->{$this->modelClass}->{$object};
            } else {
                $object = null;
            }
        } elseif (empty($object)) {
            if (isset($this->{$this->modelClass})) {
                $object = $this->{$this->modelClass};
            } else {
                $className = null;
                $name = $this->uses[0];
                if (strpos($this->uses[0], '.') !== false) {
                    list($name, $className) = explode('.', $this->uses[0]);
                }
                if ($className) {
                    $object = $this->{$className};
                } else {
                    $object = $this->{$name};
                }
            }
        }
        return $object;
    }

    function paginate($type = 'all', $scope = array(), $whitelist = array()) {
        $object = $this->objectSpec2Object(null);
        if (isset($this->paginate[$object->alias])) {
            array_unshift($this->paginate[$object->alias], $type);
        } else {
            array_unshift($this->paginate, $type);
        }
        return parent::paginate($object, $scope, $whitelist);
    }

    function setMenuActionLinksForCompany($user) {
        
    }

    function _isLoggedIn($user) {
        return $user;
    }

    function _setMenuLinksForCompany($user, $company = null) {
        if ($this->User->isCompanyCandidate($user)) {
            $this->set('my_company_url', $this->_myCompanyCandidateUrl($user, $company));
            $this->set('my_deals_url', false);
            $this->set('my_cupons_url', false);
        } else {
            $this->set('my_company_url', $this->_myCompanyWithNowUrl($user, $company));
            $this->set('my_deals_url', $this->_myDealsWithNowUrl($user, $company));
            $this->set('my_cupons_url', $this->_myCuponsWithNowUrl($user, $company));
        }
    }

    function _myCompanyWithNowUrl($user, $company = null) {
        return array(
            'plugin' => 'now',
            'controller' => 'now_registers',
            'action' => 'my_company'
        );
    }

    function _myDealsWithNowUrl($user, $company = null) {
        return array(
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_deals'
        );
    }

    function _myCuponsWithNowUrl($user, $company = null) {
        return array(
            'plugin' => 'now',
            'controller' => 'now_deals',
            'action' => 'my_cupons'
        );
    }

    function _myCompanyWithoutNowUrl($user, $company = null) {
        return $this->_myCompanyWithNowUrl($user, $company);
    }

    function _myDealsWithoutNowUrl($user, $company = null) {
        return array(
            'plugin' => '',
            'controller' => 'deals',
            'action' => 'company',
            $company['Company']['slug']
        );
    }

    function _myCuponsWithoutNowUrl($user, $company = null) {
        return $this->_myCompanyWithNowUrl($user, $company);
    }

    function _myCompanyCandidateUrl($user, $company = null) {
        return array(
            'plugin' => 'now',
            'controller' => 'now_registers',
            'action' => 'my_company'
        );
    }

    public function getRequestedAction() {
        return $this->params['controller'] . '/' . $this->params['action'];
    }

}
