<?php
/**
 * RowObject Behavior is an implementation of a very simple Row Data Gateway
 * @link http://www.martinfowler.com/eaaCatalog/rowDataGateway.html
 * This behavior operates at row level, not like model which operates on a table level
 * By including this behavior, it will transform all the results fetched with find ()
 * into objects, that will have some basic crud functions (for now save (), to update
 * row data, and remove (), to remove the row).
 * To have your own object representing a row, set the rowClass option for the bahavior.
 * @example $actsAc = array ('RowObject' => array ('rowClass' => 'MyRow'));
 * MyRow must be placed into the models folder in my_row.php file.
 * MyRow class must extend AppRow class.
 *
 * You can also create your own AppRow class by creating app_row.php file in the
 * app folder, just like AppModel or AppController, methods in this class will
 * be shared among all the Row classes.
 * AppRow class must extend Row class.
 *
 * @example
 * $row = $this->findById (1);
 * $row->name = 'Another Name';
 * $row->save ();  // saved
 * $row->remove ();  // removed
 *
 * $rows = $this->findByActive (0);  // find all the inactive users
 * foreach ($rows as $row) {
 *   $row->remove ();
 * }
 *
 * This becomes much more useful when you create your own row classes and put
 * row manipulation logic.
 *
 * @author Anton Galitch (freenity)
 * @version 0.2 2008.12.30
 */


/**
 * This is the class that contains basic methods that will operate on row level
 * AppRow should extend this class.
 */
class Row implements ArrayAccess  {
  public  static $model  = null;  /** @var       - Model used to operate on the model */
  private        $fields = null;  /** @var array - Table columns                      */


  /**
   * In order to be more "integration friendly" we must implement ArrayAccess' methods
   */
  public function offsetSet   ($offset, $value)  { $this->{$offset} = $value; }
  public function offsetUnset ($offset        )  { $this->{$offset} = null;   }

  public function offsetExists ($offset) {
    return array_key_exists ($offset, get_object_vars ($this));
  }

  public function offsetGet ($offset) {
    return $this->offsetExists ($offset) ? $this->{$offset} : null;
  }


  /**
   * Saves the changes made to the row.
   * IMPORTANT: the primary key shouldn't be changed at all.
   *
   * @return boolean true on success, false on fail
   */
  public function save () {
    $record = array (
        self::$model->name => array (),
    );

    foreach ($this->getFields () as $field) {
      $record [self::$model->name][$field] = $this->{$field};
    }

    return self::$model->save ($record);
  }

  /**
   * Removes current row.
   *
   * @return boolean True on success, false on fail
   */
  public function remove () {
    return self::$model->remove ($this->{self::$model->primaryKey});
  }

  /**
   * This method is the inverse to toArray (), it will receive an array, and
   * set all the needed fields with it's values.
   *
   * @param array Data that will populate the object
   * @return boolean true on success, false on fail
   */
  public function setFromArray ($data) {
    foreach ($this->getFields () as $field) {
      if (isset ($data [$field])) {
        $this->$field = $data [$field];
      }
    }
    return true;
  }

  /**
   * Converts all the fields contained in this object into an array, so Model
   * can use it.
   */
  public function toArray () {
    $res = array ();
    foreach ($this->getFields () as $field) {
      $res [$field] = $this->$field;
    }
    return $res;
  }


  /**
   * @param Model model that is used.
   *          This is set automatically, so it shouldn't be used at all.
   */
  public static function setModel (Model $model) {
    self::$model = $model;
  }

  private function getFields () {
    if ($this->fields === null) {
      $this->fields = array_keys (self::$model->schema ());
    }
    return $this->fields;
  }
}


/**
 * Used as app_model.php /doesn't exist/ app_controller.php
 */
if (file_exists (APP . 'app_row.php')) {
  include_once (APP . 'app_row.php');
}


/**
 * Used in case app_row.php doesn't exist
 */
if (!class_exists ('AppRow')) {
  class AppRow extends Row {}
}

/**
 * Path to the application's rows directory.
 */
if (!defined ('ROWS')) {
	define('ROWS', APP . 'rows' . DS);
}



class RowObjectBehavior extends ModelBehavior {
  private $rowModel = null;

  /**
   * Initializes the whole behavior. If rowClass option is not defined,
   * it will use app_row.php located in the app folder, if this file doesn't
   * exist it will finally use the Row class defined above.
   */
  public function setUp ($model, $config = array ()) {
    if (isset ($config ['rowClass'])) {
      $this->rowModel = $config ['rowClass'];
      $filename = ROWS . Inflector::underscore ($this->rowModel) . '.php';
    } else if (file_exists (APP . 'app_row.php')) {
      $this->rowModel = 'AppRow';
      $filename = APP . 'app_row.php';
    } else {
      $this->rowModel = 'AppRow';
      $filename = 'null';
    }

    if ($filename !== null && file_exists ($filename)) {
      include_once ($filename);
    }
    AppRow::setModel ($model);
  }

  /**
   * This method returns an instance of a RowObject to be inserted as a new row
   * when save () is called a new row will be inserted.
   *
   * @return RowObject - returns an instance of the RowObject, AppRow or Row.
   */
  public function createRow ($model) {
    $initialization = array ();
    foreach (array_keys ($model->schema ()) as $field) {
      $initialization [$field] = null;
    }
    return Set::map ($initialization, $this->rowModel);
  }

  /**
   * Just takes the results and converts them into a user defined object.
   */
  public function afterFind ($model, $results, $primary = false) {
    if (strtolower(substr ($model->findQueryType, -3)) == 'row') {
      return Set::map ($results, $this->rowModel);
    } else {
      return $results;
    }
  }
}
?>