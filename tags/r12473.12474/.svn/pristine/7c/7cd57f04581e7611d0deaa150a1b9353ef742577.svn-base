<?php

App::import('Component', 'api.ApiBase');
App::import('Component', 'product.ProductInventorySolverFactory');
App::import('Component', 'product.ProductInventoryServiceValidator');
App::import('Component', 'product.ProductDeal');
App::import('Model', 'product.ProductInventory');
App::import('Model', 'product.ProductMovement');
App::import('Model', 'product.ProductProduct');

class ProductInventoryServiceComponent extends ApiBaseComponent {

    private static $instance = null;

    public function __construct() {
        parent::__construct('product');
        $this->ProductInventory = new ProductInventory();
        $this->ProductProduct = new ProductProduct();
        $this->ProductDeal = new ProductDeal();
        $this->ProductMovement = new ProductMovement();
        $this->ProductInventorySolverFactory = new ProductInventorySolverFactoryComponent();
        $this->ProductInventoryServiceValidator = new ProductInventoryServiceValidatorComponent();
    }

    static function instance() {
        if (self::$instance == null) {
            self::$instance = new ProductInventoryServiceComponent();
        }
        return self::$instance;
    }

    private function solver($deal) {
        return $this->ProductInventorySolverFactory->solver($this->getInventoryStrategyId($deal));
    }

    private function getInventoryStrategyId($deal) {
        if ($this->isAInventoryProductDeal($deal)) {
            return ProductInventoryStrategy::ID_MANAGED_WITH_PINS_AND_BAC_STOCK;
        } elseif ($this->isAPinDeal($deal)) {
            return ProductInventoryStrategy::ID_MANAGED_WITH_PINS_STOCK;
        } else {
            return $deal['ProductDeal']['product_inventory_strategy_id'];
        }
    }

    public function isAInventoryProductDeal($deal) {
        $isAExternalProductDeal = false;
        if (!empty($deal['ProductDeal']['product_product_id'])) {
            $product = $this->ProductProduct->findById($deal['ProductDeal']['product_product_id']);
            if (!empty($product['ProductProduct']['product_inventory_id'])) {
                $isAExternalProductDeal = true;
            }
        }
        return $isAExternalProductDeal;
    }

    function isProductInventoryEnabledToBeSentToBacByProductId($productId) {
        $isEnabled = false;
        if (!empty($productId)) {
            $product = $this->ProductProduct->findById($productId);
            if (!empty($product['ProductProduct']['product_inventory_id'])) {
                $productInventory = $this->ProductInventory->findById($product['ProductProduct']['product_inventory_id']);
                if (!empty($productInventory)) {
                    $isEnabled = $this->ProductInventory->isEnabledToBeSentToBac($productInventory);
                }
            }
        }
        return $isEnabled;
    }

    private function isAPinDeal($deal) {
        return (isset($deal['ProductProduct']['has_pins']) && $deal['ProductProduct']['has_pins'] > 0);
    }

    function getProductInventoryStrategyId($dealId) {
        $deal = $this->ProductDeal->findById($dealId);
        $productInventoryStrategyId = null;
        if (!empty($deal)) {
            $productInventoryStrategyId = $this->getInventoryStrategyId($deal);
        }
        return $productInventoryStrategyId;
    }

    function availableQuantityByProduct($product) {
        $stock = $product['ProductProduct']['stock'];
        if (!empty($product['ProductProduct']['product_inventory_id'])) {
            $inventory = $this->ProductInventory->findById($product['ProductProduct']['product_inventory_id']);
            $stock = $inventory['ProductInventory']['stock'];
        }
        return $stock;
    }

    function availableQuantity($dealId) {
        $deal = $this->ProductDeal->findById($dealId);
        $available = null;
        if (!empty($deal)) {
            $solver = $this->solver($deal);
            $available = $solver->availableQuantity($deal);
        }
        return $available;
    }

    function decrease($dealId, $quantity, $userId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            $dealId,
            $quantity
        ));
        $deal = $this->ProductDeal->findById($dealId);
        if (!empty($deal)) {
            $solver = $this->solver($deal);
            $solver->decrease($deal, $quantity, $userId);
        }
    }

    function trackingStockMovements($productId, $action, $oldStock, $newStock, $userId) {
        $this->ApiLogger->info(__FUNCTION__, array(
            array(
                'action' => $action,
                'productId' => $productId,
                'newStock' => $newStock,
                'oldStock' => $oldStock,
                'userId' => $userId
            )
        ));
        return $this->ProductMovement->trackingMovements($productId, 'stock', $action, $oldStock, $newStock, $userId);
    }

    public function createOrEditProductInventory($name, $idProduct, $description, $InvoiceCode, $LibertyaCode, $portalId, $stock, $idProductoPortal) {
        $product['ProductInventory'] = array(
            'name' => $name,
            'bac_product_id' => $idProduct,
            'description' => $description,
            'bac_invoice_id' => $InvoiceCode,
            'bac_libertya_id' => $LibertyaCode,
            'bac_portal_id' => $portalId,
            'id' => $idProductoPortal,
            'stock' => $stock,
            'source' => 'LIBERTYA',
            'source_identifier' => $LibertyaCode
        );
        if (empty($product['ProductInventory']['stock'])) {
            unset($product['ProductInventory']['stock']);
        }
        if (!$this->ProductInventoryServiceValidator->validate($product)) {
            $this->ApiLogger->error('Parametros Faltantes/Error en invocacion del servicio.', $product);
            throw new DomainException('Parametros Faltantes.');
        }
        $this->ApiLogger->notice('Creacion/Edicion de ProductInventory', $product);
        if (empty($idProductoPortal)) {
            $product = $this->createProductInventory($product);
        } else {
            $product = $this->editProductInventory($product);
        }
        $this->ApiLogger->trace('Finalizo Creacion/Edicion de ProductInventory', $product);
        return $product;
    }

    public function prepareProductData($product) {
        $product['ProductInventory']['name'] = Sanitize::clean($product['ProductInventory']['name'], array(
                    'encode' => false
        ));
        $product['ProductInventory']['bac_invoice_id'] = Sanitize::clean($product['ProductInventory']['bac_invoice_id'], array(
                    'encode' => false
        ));
        $product['ProductInventory']['bac_libertya_id'] = Sanitize::clean($product['ProductInventory']['bac_libertya_id'], array(
                    'encode' => false
        ));
        $product['ProductInventory']['bac_portal_id'] = Sanitize::clean($product['ProductInventory']['bac_portal_id'], array(
                    'encode' => false
        ));
        $product['ProductInventory']['description'] = Sanitize::clean($product['ProductInventory']['description'], array(
                    'encode' => false
        ));
        $product['ProductInventory']['source_identifier'] = Sanitize::clean($product['ProductInventory']['source_identifier'], array(
                    'encode' => false
        ));
        return $product;
    }

    public function addQuotesForEditProduct($product) {
        $product['ProductInventory']['name'] = "'" . $product['ProductInventory']['name'] . "'";
        $product['ProductInventory']['bac_invoice_id'] = "'" . $product['ProductInventory']['bac_invoice_id'] . "'";
        $product['ProductInventory']['bac_libertya_id'] = "'" . $product['ProductInventory']['bac_libertya_id'] . "'";
        $product['ProductInventory']['bac_portal_id'] = "'" . $product['ProductInventory']['bac_portal_id'] . "'";
        $product['ProductInventory']['description'] = "'" . $product['ProductInventory']['description'] . "'";
        $product['ProductInventory']['source_identifier'] = "'" . $product['ProductInventory']['source_identifier'] . "'";
        return $product;
    }

    private function editProductInventory($product) {
        $fields = $this->prepareProductData($product);
        $fields = $this->addQuotesForEditProduct($fields);
        $foundById = $this->ProductInventory->findById($product['ProductInventory']['id']);
        if (!empty($foundById)) {
            $foundByBacLibertyaId = $this->ProductInventory->findByBacLibertyaId($product['ProductInventory']['bac_libertya_id']);
            if (!empty($foundByBacLibertyaId)) {
                if ($foundById['ProductInventory']['id'] != $foundByBacLibertyaId['ProductInventory']['id']) {
                    $message = 'El bac_libertya_id:' . $product['ProductInventory']['bac_libertya_id'] . ' del producto:' . $product['ProductInventory']['id'] . ' que intenta editarse coincide con el de otro producto:' . $foundByBacLibertyaId['ProductInventory']['id'];
                    $this->ApiLogger->error($message, $product);
                    throw new DomainException($message);
                }
            }
            $this->ProductInventory->updateById($fields['ProductInventory'], $product['ProductInventory']['id']);
        } else {
            $message = 'No es posible encontrar el id_producto_portal:' . $product['ProductInventory']['id'];
            $this->ApiLogger->error($message, $product);
            throw new DomainException($message);
        }
        return $product;
    }

    private function createProductInventory($product) {
        $product = $this->prepareProductData($product);
        if ($this->existsProductInventory($product)) {
            $this->ApiLogger->error('Producto ya se encuentra en existencia:', $product);
            throw new DomainException('Producto ya se encuentra en existencia: ' . $product['ProductInventory']['bac_libertya_id']);
        }
        if ($this->ProductInventory->save($product)) {
            $product['ProductInventory']['id'] = $this->ProductInventory->getLastInsertID();
        } else {
            $product = false;
        }
        return $product;
    }

    public function existsProductInventory($product) {
        $inventory = $this->ProductInventory->findByBacLibertyaId($product['ProductInventory']['bac_libertya_id']);
        return !empty($inventory);
    }

    public function findProductInventoryByProducProductId($productId) {
        $product = $this->ProductProduct->findById($productId);
        $inventory = $this->ProductInventory->findById($product['ProductProduct']['product_inventory_id']);
        return $inventory;
    }

    public function addAvailableStockForProducts($products) {
        foreach ($products as $i => $product) {
            $products[$i]['ProductProduct']['stock'] = $this->availableQuantityByProduct($product);
        }
        return $products;
    }

}
