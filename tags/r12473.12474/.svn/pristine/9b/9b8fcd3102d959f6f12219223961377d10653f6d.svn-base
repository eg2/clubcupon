<?php

class ApiDeal extends ApiAppModel {

    const STATUS_UPCOMMING = 1;
    const STATUS_OPEN = 2;
    const STATUS_CANCELED = 3;
    const STATUS_TIPPED = 5;
    const STATUS_CLOSED = 6;
    const STATUS_REFUNDED = 7;
    const STATUS_PAIDTOCOMPANY = 8;
    const STATUS_PENDINGAPPROVAL = 9;
    const STATUS_REJECTED = 10;
    const STATUS_DRAFT = 11;
    const STATUS_DELETE = 12;
    const STATUS_TOTALLYPAID = 13;
    const STATUS_FINALIZED = 14;
    const STATUS_CICLEENDED = 15;
    const PUBLICATION_CHANNEL_TYPE_WEB = 1;
    const NOT_IS_FIBERTEL_DEAL = 0;
    const NOT_IS_TICKET_PORTAL_DEAL = 0;
    const NOT_IS_CLARIN365 = 0;
    const MOBILECHANNEL = 2;
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';
    const CITY_ID_DEFAULT = 42550;
    const PAYMENT_TERM_DEFAULT = 30;
    const SEGMENTATION_PROFILE_ID_DEFAULT = 3;
    const CREATED_BY_DEFAULT = 0;
    const MODIFIED_BY_DEFAULT = 0;
    const CREATED_BY_IP_DEFAULT = '127.0.0.1';
    const MODIFIED_BY_IP_DEFAULT = '127.0.0.1';
    const SLUG_LO_MEJOR_DE_LA_SEMANA = 'lo-mejor-del-semana';
    const SLUG_FIDELIZACION = 'fidelizacion';
    const SLUG_CIUDAD_DE_BUENOS_AIRES = 'ciudad-de-buenos-aires';
    const SLUG_FESTIVAL_GASTRONICO = 'festival-gastronomico';
    const SLUG_PRODUCTOS = 'productos';
    const COMPANY_ID_IMAGENA = 18;
    const DEAL_TRADE_AGREEMENT_ID_PREPURCHASE_WITH_LOGISTICS = 6;

    public $name = 'ApiDeal';
    public $alias = 'Deal';
    public $useTable = 'deals';
    public $orders = array(
        'isSideDealASCPriorityDESC' => array(
            'is_side_deal' => 'ASC',
            'priority' => 'DESC'
        ),
        'isSideDealDESCPriorityASC' => array(
            'is_side_deal' => 'DESC',
            'priority' => 'ASC'
        ),
        'comercialValueDESC' => array(
            "if (City.slug = 'lo-mejor-del-mes', 1, 0)" => 'DESC',
            "if (City.slug = 'fidelizacion', 1, 0)" => 'DESC',
            "if (City.slug = 'ciudad-de-buenos-aires', 1, 0)" => 'DESC',
            "if (City.slug = 'festival-gastronomico', 1, 0)" => 'DESC',
            "if (City.slug = 'productos', 1, 0)" => 'DESC',
            'commission_percentage * sales_forecast' => 'DESC',
            'priority' => 'DESC',
            'is_side_deal' => 'ASC'
        )
    );
    public $recursive = - 1;
    public $belongsTo = array(
        'City' => array(
            'className' => 'api.ApiCity',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'Company' => array(
            'className' => 'api.ApiCompany',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealFlatCategory' => array(
            'className' => 'api.ApiDealFlatCategory',
            'foreignKey' => 'deal_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'DealCategory' => array(
            'className' => 'api.ApiDealCategory',
            'foreignKey' => 'deal_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        ),
        'ProductProduct' => array(
            'className' => 'product.ProductProduct',
            'foreignKey' => 'product_product_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'counterCache' => true
        )
    );
    public $hasOne = array(
        'Attachment' => array(
            'className' => 'api.ApiAttachment',
            'foreignKey' => 'foreign_id',
            'conditions' => array(
                'Attachment.class =' => 'Deal'
            ),
            'dependent' => true
        )
    );
    public $hasMany = array(
        'BranchesDeal' => array(
            'classname' => 'api.ApiBranchDeal',
            'foreignKey' => 'now_deal_id'
        )
    );
    private $activeConditions = array(
        'Deal.deal_status_id' => array(
            self::STATUS_OPEN,
            self::STATUS_TIPPED
        ),
        'Deal.publication_channel_type_id' => self::PUBLICATION_CHANNEL_TYPE_WEB
    );
    private $mobileConditions = array(
        'Deal.is_fibertel_deal' => self::NOT_IS_FIBERTEL_DEAL,
        'Deal.is_ticketportal_deal' => self::NOT_IS_TICKET_PORTAL_DEAL
    );
    private $parentConditions = array(
        'Deal.parent_deal_id = Deal.id'
    );
    private $nowConditions = array(
        'Deal.is_now' => 1
    );
    private $subDealConditions = array(
        'Deal.parent_deal_id != Deal.id'
    );
    private $apiSubDealFields = array(
        'Deal.buy_max_quantity_per_user',
        'Deal.coupon_condition',
        'Deal.descriptive_text',
        'Deal.discount_percentage',
        'Deal.discounted_price',
        'Deal.id',
        'Deal.max_limit',
        'Deal.min_limit',
        'Deal.original_price',
        'Deal.parent_deal_id',
        'Deal.subtitle',
        'Deal.name',
        'Deal.is_clarin365_deal'
    );
    private $solrDataImportFields = array(
        'Deal.descriptive_text',
        'Deal.description',
        'Deal.discount_percentage',
        'Deal.discounted_price',
        'Deal.deal_status_id',
        'Deal.end_date',
        'Deal.id',
        'Deal.name',
        'Deal.gender',
        'Deal.is_now',
        'Deal.max_limit',
        'Deal.min_limit',
        'Deal.original_price',
        'Deal.parent_deal_id',
        'Deal.slug',
        'Deal.start_date',
        'Deal.subtitle',
        'Deal.only_price',
        'Deal.hide_price',
        'Deal.priority',
        'Deal.is_side_deal',
        'Deal.is_tourism',
        'Deal.meta_keywords',
        'Company.id',
        'Company.name',
        'Company.city_id',
        'City.id',
        'City.name',
        'City.slug',
        'City.is_group',
        'City.is_corporate',
        'City.is_hidden',
        'City.is_approved',
        'City.is_searchable',
        'DealCategory.id',
        'DealCategory.name',
        'DealFlatCategory.l1_id',
        'DealFlatCategory.parent_id',
        'DealFlatCategory.path'
    );
    private $apiDealFields = array(
        'Deal.buy_max_quantity_per_user',
        'Deal.buy_min_quantity_per_user',
        'Deal.coupon_condition',
        'Deal.custom_company_address1',
        'Deal.custom_company_address2',
        'Deal.custom_company_city',
        'Deal.custom_company_contact_phone',
        'Deal.custom_company_country',
        'Deal.custom_company_name',
        'Deal.custom_company_state',
        'Deal.custom_company_zip',
        'Deal.descriptive_text',
        'Deal.discount_percentage',
        'Deal.discounted_price',
        'Deal.deal_status_id',
        'Deal.end_date',
        'Deal.id',
        'Deal.is_now',
        'Deal.max_limit',
        'Deal.min_limit',
        'Deal.name',
        'Deal.original_price',
        'Deal.parent_deal_id',
        'Deal.slug',
        'Deal.start_date',
        'Deal.subtitle',
        'Deal.only_price',
        'Deal.hide_price',
        'Deal.priority',
        'Deal.is_side_deal',
        'Deal.coupon_highlights',
        'Deal.description',
        'Deal.bitly_short_url_prefix',
        'Deal.bitly_short_url_subdomain',
        'Deal.is_clarin365_deal',
        'Deal.is_discount_mp',
        'Deal.has_pins',
        'Company.logo_image',
        'Company.name',
        'Company.phone',
        'Company.url',
        'Company.latitude',
        'Company.longitude',
        'Company.zip',
        'Company.address1',
        'Company.city_id',
        'DealFlatCategory.l1',
        'DealFlatCategory.path',
        'City.id',
        'City.slug',
        'City.name',
        'City.is_business_unit',
        'Deal.city_id',
        'Deal.is_tourism',
        'Deal.coupon_start_date',
        'Deal.coupon_expiry_date',
        'Deal.is_shipping_address',
        'Deal.product_product_id',
        'Deal.meta_keywords',
        'Deal.meta_description',
        'Deal.is_variable_expiration',
        'Deal.coupon_duration',
        'Deal.is_shipping_adress_user'
    );
    private $tecDiaStartDate = null;

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->tecDiaStartDate = Configure::read('tec_dia.start_date');
    }

    public function findSubDealsForApi($deal) {
        $subdeals = $this->find('all', array(
            'fields' => $this->apiSubDealFields,
            'conditions' => $this->getConditionsForActiveSubDeal(array(
                'Deal.parent_deal_id' => $deal['Deal']['id']
            )),
            'order' => array(
                'Deal.id' => 'desc'
            ),
            'recursive' => - 1
        ));
        return $subdeals;
    }

    public function findAllSubDealsForApi($deal) {
        $subdeals = $this->find('all', array(
            'fields' => $this->apiSubDealFields,
            'conditions' => array(
                'Deal.parent_deal_id' => $deal['Deal']['id'],
                'Deal.parent_deal_id != Deal.id'
            ),
            'order' => array(
                'Deal.id' => 'desc'
            ),
            'recursive' => - 1
        ));
        return $subdeals;
    }

    public function findForApiBySlug($slug) {
        $extra_conditions = array(
            'Deal.slug' => $slug
        );
        return $this->findForApi('first', $extra_conditions);
    }

    public function findForApiById($id) {
        $extra_conditions = array(
            'Deal.id' => $id
        );
        return $this->findForApi('first', $extra_conditions);
    }

    public function findAllForApi($extraConditions, $limit = null, $page = null) {
        $extraConditions = $this->getConditionsForMobile($this->getconditionsForWebActiveParentsOrNowActive($extraConditions));
        return $this->findForApi('all', $extraConditions, $limit, $page);
    }

    public function findAllForCompany($extraConditions, $limit = null, $page = null) {
        $extraConditions = $this->getConditionsForMobile($this->getconditionsForWebActiveParentsOrNowActive($extraConditions));
        $query = $this->find('all', array(
            'fields' => $this->apiDealFields,
            'conditions' => $extraConditions,
            'contain' => array(
                'City',
                'Company',
                'DealFlatCategory'
            ),
            'order' => $this->getOrder('comercialValueDESC'),
            'recursive' => 1,
            'limit' => $this->getPageLimit($limit),
            'page' => $this->getPage($page)
        ));
        return $query;
    }

    private function getConditionsForMobile($extraConditions = null) {
        if (!empty($extraConditions)) {
            return array_merge($this->mobileConditions, $extraConditions);
        }
        return $this->mobileConditions;
    }

    private function getConditionsForActiveSubDeal($extraConditions = null) {
        $activeSubDealConditions = $this->getConditionsForActive($this->subDealConditions);
        if (!empty($extraConditions)) {
            return array_merge($activeSubDealConditions, $extraConditions);
        }
        return $activeSubDealConditions;
    }

    private function getConditionsForActive($extraConditions = null) {
        if (!empty($extraConditions)) {
            return array_merge($this->activeConditions, $extraConditions);
        }
        return $this->activeConditions;
    }

    private function getconditionsForWebActiveParentsOrNowActive($extraConditions = null) {
        $alldeals = array(
            'OR' => array(
                $this->parentConditions,
                $this->nowConditions
            )
        );
        $extraConditions = array_merge($alldeals, $extraConditions);
        $activeParentsCondition = $this->getConditionsForActive($extraConditions);
        return $activeParentsCondition;
    }

    public function findAllForSolrDataImport($extraConditions, $limit = null, $page = null) {
        $extraConditions = $this->getConditionsForMobile($this->getconditionsForWebActiveParentsOrNowActive($extraConditions));
        $extraConditions['City.is_hidden'] = 0;
        $extraConditions['City.is_approved'] = 1;
        $extraConditions['City.is_searchable'] = 1;
        return $this->findForSolrDataimport('all', $extraConditions, $limit, $page);
    }

    public function countAllForSolrDataImport($extraConditions) {
        $extraConditions = $this->getConditionsForMobile($this->getconditionsForWebActiveParentsOrNowActive($extraConditions));
        $query = $this->find('count', array(
            'conditions' => $extraConditions
        ));
        return $query;
    }

    private function findForSolrDataimport($findMethod, $extraConditions = null, $limit = null, $page = null) {
        $query = $this->find($findMethod, array(
            'fields' => $this->solrDataImportFields,
            'conditions' => $extraConditions,
            'contain' => array(
                'City',
                'Company',
                'DealFlatCategory',
                'DealCategory'
            ),
            'order' => $this->getOrder('isSideDealASCPriorityDESC'),
            'recursive' => 1,
            'limit' => $this->getPageLimit($limit),
            'page' => $this->getPage($page)
        ));
        return $query;
    }

    private function findForApi($findMethod, $extraConditions = null, $limit = null, $page = null) {
        $query = $this->find($findMethod, array(
            'fields' => $this->apiDealFields,
            'conditions' => $extraConditions,
            'contain' => array(
                'City',
                'Company',
                'DealFlatCategory',
                'ProductProduct' => array(
                    'fields' => array(
                        'ProductProduct.has_pins'
                    )
                )
            ),
            'order' => $this->getOrder('isSideDealASCPriorityDESC'),
            'recursive' => 1,
            'limit' => $this->getPageLimit($limit),
            'page' => $this->getPage($page)
        ));
        return $query;
    }

    public function isNow($deal) {
        return $deal['Deal']['is_now'];
    }

    public function isImagena($deal) {
        return $deal['Deal']['company_id'] == self::COMPANY_ID_IMAGENA;
    }

    public function isSubDeal($deal) {
        return $deal['Deal']['id'] <> $deal['Deal']['parent_deal_id'];
    }

    public function hasPins($deal) {
        return $deal['Deal']['has_pins'] > 0;
    }

    public function hasProductAndPins($deal) {
        return isset($deal['ProductProduct']) && $deal['ProductProduct']['has_pins'] > 0;
    }

    public function updateDealExternalCount($deal_id, $quantity) {
        return $this->updateAll(array(
                    'deal_external_count' => 'deal_external_count + ' . $quantity
                        ), array(
                    'Deal.id' => $deal_id
        ));
    }

    public function isTourism($deal) {
        return $deal['Deal']['is_tourism'];
    }

    public function isWholesaler($deal) {
        return $deal['Deal']['is_wholesaler'];
    }

    public function isTecDia($deal) {
        return strtotime($deal['Deal']['start_date']) >= $this->tecDiaStartDate;
    }

    public function isActive($deal) {
        return in_array($deal['Deal']['deal_status_id'], array(
            self::STATUS_OPEN,
            self::STATUS_TIPPED
        ));
    }

    public function hasExpired($deal) {
        $now = strtotime(date("Y-m-d"));
        $dateDealExpired = strtotime($deal['Deal']['end_date']);
        return $dateDealExpired < $now;
    }

    public function getStartDate($deal) {
        return $deal['Deal']['start_date'];
    }

    public function isEndUser($deal) {
        return $deal['Deal']['is_end_user'];
    }

    public function hasSubdeals($deal) {
        $deals = $this->findSubDealsForApi($deal);
        return (count($deals) > 0);
    }

    public function getExtendedHoursToRedeem($deal) {
        $extendedHours = 0;
        if ($this->isNow($deal)) {
            $extendedHours = Configure::read('deal.extended_hours_to_redeem_now');
        }
        if ($this->isImagena($deal)) {
            $extendedHours = Configure::read('deal.extended_hours_to_redeem_imagena');
        }
        return $extendedHours;
    }

    public function findRowById($id) {
        $conditions = array(
            'Deal.id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'recursive' => - 1
        ));
    }

    public function isPrepurchaseWithOwnLogistics($deal) {
        return $deal['Deal']['deal_trade_agreement_id'] == self::DEAL_TRADE_AGREEMENT_ID_PREPURCHASE_WITH_LOGISTICS;
    }

    public function isVariableExpiration($deal) {
        return $deal['Deal']['is_variable_expiration'] == 1;
    }

    public function couponLastExpirationDate($deal) {
        $couponExpirationDate = $deal['Deal']['coupon_expiry_date'];
        if ($this->isVariableExpiration($deal)) {
            $since = max($deal['Deal']['end_date'], $deal['Deal']['coupon_start_date']);
            $duration = max($deal['Deal']['coupon_duration'], 0);
            $couponExpirationDate = date('Y-m-d 23:59:00', strtotime($since . " +{$duration} day"));
        }
        return $couponExpirationDate;
    }

    public function isShippingAddressUser($deal) {
        return $deal['Deal']['is_shipping_adress_user'] == 1;
    }

}
