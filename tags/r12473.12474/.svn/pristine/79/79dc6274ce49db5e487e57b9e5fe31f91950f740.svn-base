-- http://tracker.int.clarin.com/browse/CC-3495

CREATE OR REPLACE VIEW accounting_calendars_grouped AS
SELECT `deal`.`company_id` AS `deal_company_id`,`liquidate`.`accounting_calendar_id` AS `liquidate_accounting_calendar_id`, MAX(`calendar`.`since`) AS `calendar_since`, MAX(`calendar`.`until`) AS `calendar_until`, SUM(`liquidate`.`total_quantity`) AS `liquidate_total_quantity`, SUM(IFNULL(`bill`.`total_amount`,0)) AS `bill_total_amount`, SUM(`liquidate`.`total_amount`) AS `liquidate_total_amount`, MAX(IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY))) AS `max_payment_date`
FROM ((((`accounting_calendars` `calendar`
JOIN `accounting_items` `liquidate` ON((`calendar`.`id` = `liquidate`.`accounting_calendar_id`)))
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `companies` `company` ON((`liquidate`.`model_id` = `company`.`id`)))
WHERE ((`liquidate`.`deleted` = 0 AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND (`company`.`validated` = 1) AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)))
GROUP BY `deal`.`company_id`,`liquidate`.`accounting_calendar_id`;

CREATE OR REPLACE VIEW accounting_calendars_grouped_items AS
SELECT `deal`.`company_id` AS `deal_company_id`,`liquidate`.`id` AS `liquidate_id`,`liquidate`.`accounting_calendar_id` AS `liquidate_accounting_calendar_id`,`deal`.`start_date` AS `deal_start_date`, IFNULL(`deal`.`campaign_code`,'0') AS `deal_campaign_code`,`deal`.`id` AS `deal_id`, CONCAT(`deal`.`name`, IF((`deal`.`descriptive_text` IS NOT NULL),' - ',''), IFNULL(`deal`.`descriptive_text`,'')) AS `deal_name`,`city`.`name` AS `city_name`,`liquidate`.`total_quantity` AS `liquidate_total_quantity`, IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`,`liquidate`.`total_amount` AS `liquidate_total_amount`, IFNULL(`bill`.`bac_sent`,`liquidate`.`created`) AS `liquidate_bac_sent`, IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS `max_payment_date`
FROM (((`accounting_items` `liquidate`
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0 AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)));

CREATE OR REPLACE VIEW accounting_campaigns AS
SELECT `deal`.`company_id` AS `deal_company_id`, IFNULL(`deal`.`campaign_code`,'0') AS `deal_campaign_code`, MAX(`deal`.`name`) AS `deal_name`, SUM(`liquidate`.`total_quantity`) AS `liquidate_total_quantity`, SUM(IFNULL(`bill`.`total_amount`,0)) AS `bill_total_amount`, SUM(`liquidate`.`total_amount`) AS `liquidate_total_amount`, MAX(IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY))) AS `max_payment_date`
FROM (((`accounting_items` `liquidate`
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `companies` `company` ON((`liquidate`.`model_id` = `company`.`id`)))
WHERE ((`liquidate`.`deleted` = 0 AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND (`company`.`validated` = 1) AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)))
GROUP BY `deal`.`company_id`, IFNULL(`deal`.`campaign_code`,'0');

CREATE OR REPLACE VIEW accounting_campaign_items AS
SELECT `deal`.`company_id` AS `deal_company_id`,`liquidate`.`id` AS `liquidate_id`,`deal`.`start_date` AS `deal_start_date`, IFNULL(`deal`.`campaign_code`,'0') AS `deal_campaign_code`,`deal`.`id` AS `deal_id`, CONCAT(`deal`.`name`, IF((`deal`.`descriptive_text` IS NOT NULL),' - ',''), IFNULL(`deal`.`descriptive_text`,'')) AS `deal_name`,`city`.`name` AS `city_name`,`liquidate`.`total_quantity` AS `liquidate_total_quantity`, IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`,`liquidate`.`total_amount` AS `liquidate_total_amount`, IFNULL(`bill`.`bac_sent`,`liquidate`.`created`) AS `liquidate_bac_sent`, IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS `max_payment_date`
FROM (((`accounting_items` `liquidate`
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0 AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)));

CREATE OR REPLACE VIEW accounting_campaign_item_details AS
SELECT `details`.`accounting_item_id` AS `accounting_item_id`,`deal_user`.`coupon_code` AS `deal_user_coupon_code`,`redemption`.`posnet_code` AS `redemption_posnet_code`,`deal_external`.`created` AS `deal_external_created`,`redemption`.`redeemed` AS `redemption_redeemed`,`redemption`.`way` AS `redemption_way`,`deal`.`pay_by_redeemed` AS `deal_pay_by_redeemed`
FROM ((((`accounting_item_details` `details`
JOIN `deal_users` `deal_user` ON((`details`.`deal_user_id` = `deal_user`.`id`)))
JOIN `deals` `deal` ON((`deal_user`.`deal_id` = `deal`.`id`)))
JOIN `deal_externals` `deal_external` ON((`deal_external`.`id` = `deal_user`.`deal_external_id`)))
LEFT JOIN `redemptions` `redemption` ON((`redemption`.`deal_user_id` = `deal_user`.`id`)));

CREATE OR REPLACE VIEW accounting_campaign_item_extras AS
SELECT `deal`.`company_id` AS `deal_company_id`,`liquidate`.`id` AS `liquidate_id`, IFNULL(`bill`.`id`,0) AS `bill_id`,`deal`.`id` AS `deal_id`,`deal`.`start_date` AS `deal_start_date`,`deal`.`name` AS `deal_name`,`city`.`name` AS `city_name`,`liquidate`.`total_quantity` AS `liquidate_total_quantity`,`calendar`.`since` AS `calendar_since`,`calendar`.`until` AS `calendar_until`, IFNULL(`bill`.`bac_sent`,`liquidate`.`created`) AS `liquidate_bac_sent`, IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS `max_payment_date`,`deal`.`discounted_price` AS `deal_discounted_price`,(IFNULL(`bill`.`total_quantity`,0) * `deal`.`discounted_price`) AS `bill_gross_amount`, IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`, IFNULL(`bill`.`billing_commission_percentage`,0) AS `bill_billing_commission_percentage`, ROUND((((IFNULL(`bill`.`discounted_price`,0) * (IFNULL(`bill`.`billing_commission_percentage`,0) / 100)) * IFNULL(`bill`.`total_quantity`,0)) * (IFNULL(`bill`.`billing_iva_percentage`,0) / 100)),2) AS `bill_iva_amount`, IFNULL(`bill`.`billing_iva_percentage`,0) AS `bill_iva_percentage`,0 AS `liquidate_iibb_amount`,`liquidate`.`liquidating_iibb_percentage` AS `liquidate_iibb_percentage`,`liquidate`.`total_amount` AS `liquidate_total_amount`,(((`liquidate`.`total_quantity` * `liquidate`.`discounted_price`) - `liquidate`.`liquidating_invoiced_amount`) - `liquidate`.`total_amount`) AS `liquidate_guarantee_funds`
FROM ((((`accounting_items` `liquidate`
JOIN `accounting_calendars` `calendar` ON((`liquidate`.`accounting_calendar_id` = `calendar`.`id`)))
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0 AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)));
