<?php

class City extends AppModel {

    var $name = 'City';
    var $displayField = 'name';
    var $actsAs = array(
        'Sluggable' => array(
            'label' => array(
                'name'
            ),
            'translation' => 'utf-8',
            'overwrite' => false
        ),
        'WhoDidIt' => array(),
        'Logable' => array(
            'userModel' => 'User',
            'userKey' => 'user_id',
            'change' => 'full',
            'description_ids' => TRUE,
            'noLogUserTypes' => array(
                ConstUserTypes::User,
                ConstUserTypes::Company,
                ConstUserTypes::Agency,
                ConstUserTypes::Partner
            )
        )
    );
    var $belongsTo = array(
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Language' => array(
            'className' => 'Language',
            'foreignKey' => 'language_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'CityPerms' => array(
            'className' => 'CityPerms',
            'foreignKey' => 'city_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Subscription' => array(
            'className' => 'Subscription',
            'foreignKey' => 'city_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Banner' => array(
            'className' => 'Banner',
            'foreignKey' => 'city_id',
            'dependent' => true
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->isFilterOptions = array(
            ConstMoreAction::Inactive => __l('Unapproved'),
            ConstMoreAction::Active => __l('Approved')
        );
        $this->moreActions = array(
            ConstMoreAction::Inactive => __l('Unapproved'),
            ConstMoreAction::Active => __l('Approved'),
            ConstMoreAction::Delete => __l('Delete')
        );
        $this->validate = array(
            'name' => array(
                'rule' => 'notempty',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'state_id' => array(
                'rule' => 'numeric',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'country_id' => array(
                'rule' => 'numeric',
                'message' => __l('Required'),
                'allowEmpty' => false
            ),
            'email_from' => array(
                'rule1' => array(
                    'rule' => 'email',
                    'allowEmpty' => true,
                    'message' => __l('Must be a valid email')
                )
            ),
            'order' => array(
                'rule' => 'numeric',
                'message' => 'Ingresar un valor numerico',
                'allowEmpty' => false
            ),
            'email_from' => array(
                'rule' => array(
                    'email',
                    true
                ),
                'allowEmpty' => true,
                'message' => 'Ingresar un email valido, o dejar el campo vacio'
            ),
            'image' => array(
                'rule' => 'notempty',
                'message' => 'Ingresar una imagen',
                'allowEmpty' => true
            ),
            'week_days' => array(
                'rule' => array('custom', '/[1-7]{1,7}/'),
                'message' => 'Sólo dígitos entre 1 y 7, mínimo 1 carácter, máximo 7',
                'allowEmpty' => true
            )
        );
    }

    function beforeValidate($options = array()) {
        if ($this->data['City']['is_business_unit']) {
            $this->data['City']['is_corporate'] = 1;
        }
        parent::beforeValidate($options);
    }

    function findOrSaveCityAndGetId($name, $state_id, $country_id) {
        $findExist = $this->find('first', array(
            'conditions' => array(
                'name' => $name
            ),
            'fields' => array(
                'id'
            ),
            'recursive' => - 1
        ));
        if (!empty($findExist)) {
            return $findExist[$this->name]['id'];
        } else {
            $data['City']['name'] = $name;
            $data['City']['state_id'] = $state_id;
            $data['City']['country_id'] = $country_id;
            $this->create();
            $this->set($data['City']);
            $this->save($data['City']);
            return $this->getLastInsertId();
        }
    }

    function getActiveCities() {
        return $this->_getActive(false, false);
    }

    function getActiveCitiesWithDeals() {
        return $this->_getActive(true, false);
    }

    function getActiveCitiesForBanners() {
        return $this->_getActiveForBanners();
    }

    function getActiveGroups() {
        return $this->_getActive(false, true);
    }

    function getActiveGroupsWithDeals($excludedSlugs = null) {
        return $this->_getCityWithActiveDealCount(true, true, null, $excludedSlugs);
    }

    function getActiveCitiesWithActiveDeals($excludedSlugs = null) {
        return $this->_getCityWithActiveDealCount(true, false, null, $excludedSlugs);
    }

    function getActiveAllowedCities($userid) {
        return $this->_getActiveAllowed(false, false, $userid);
    }

    function getActiveAllowedCitiesWithDeals($userid) {
        return $this->_getActive(true, false, $userid);
    }

    function getActiveAllowedGroups($userid) {
        return $this->_getActive(false, true, $userid);
    }

    function getActiveAllowedGroupsWithDeals($userid) {
        return $this->_getActive(true, true, $userid);
    }

    function _getActiveForBanners($withDeals = false, $is_group = false, $is_hidden = true, $userid = null, $excludedSlugs = null) {
        $conditions = array(
            'is_approved' => 1
        );
        if ($withDeals) {
            $conditions['deal_count >='] = '1';
        }
        if (!empty($userid)) {
            $cp = $this->CityPerms->find('list', array(
                'conditions' => array(
                    'user_id' => $userid
                ),
                'fields' => array(
                    'city_id'
                )
            ));
            if (!empty($cp)) {
                $conditions['id'] = array_values($cp);
            }
        }
        if (sizeof($excludedSlugs)) {
            $conditions['NOT']['City.slug'] = $excludedSlugs;
        }
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'order DESC',
                    'contain' => array(
                        'CityPerms'
                    )
        ));
    }

    function _getActive($withDeals = false, $groups = false, $userid = null, $excludedSlugs = null) {
        $conditions = array(
            'is_approved' => 1,
            'is_selectable' => 1,
            'is_group' => $groups,
            'is_hidden' => false
        );
        if ($withDeals) {
            $conditions['deal_count >='] = '1';
        }
        if (!empty($userid)) {
            $cp = $this->CityPerms->find('list', array(
                'conditions' => array(
                    'user_id' => $userid
                ),
                'fields' => array(
                    'city_id'
                )
            ));
            if (!empty($cp)) {
                $conditions['id'] = array_values($cp);
            }
        }
        if (sizeof($excludedSlugs)) {
            $conditions['NOT']['City.slug'] = $excludedSlugs;
        }
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'order DESC',
                    'contain' => array(
                        'CityPerms'
                    )
        ));
    }

    function getAllowedCitiesAndGroupsForSecondaryDeals() {
        $conditions = array(
            'is_approved' => 1,
            'is_corporate' => false,
            'is_hidden' => false,
            'active_deal_count >=' => 1
        );
        $conditions['NOT']['City.slug'] = Configure::read('secondaryDeals.excludedCitySlugs');
        return $this->find('list', array(
                    'conditions' => $conditions,
                    'order' => 'order DESC',
                    'fields' => array(
                        'id',
                        'slug'
                    ),
                    'recursive' => - 1
        ));
    }

    function _getCityWithActiveDealCount($withDeals = false, $groups = false, $userid = null, $excludedSlugs = null) {
        $conditions = array(
            'is_approved' => 1,
            'is_group' => $groups,
            'is_hidden' => false
        );
        if ($withDeals) {
            $conditions['active_deal_count >='] = '1';
        }
        if (!empty($userid)) {
            $cp = $this->CityPerms->find('list', array(
                'conditions' => array(
                    'user_id' => $userid
                ),
                'fields' => array(
                    'city_id'
                )
            ));
            if (!empty($cp)) {
                $conditions['id'] = array_values($cp);
            }
        }
        if (sizeof($excludedSlugs)) {
            $conditions['NOT']['City.slug'] = $excludedSlugs;
        }
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'order DESC',
                    'contain' => array(
                        'CityPerms'
                    )
        ));
    }

    function findIsAprovedById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'City.id' => $id,
                        'City.is_approved' => 1
                    ),
                    'recursive' => - 1
        ));
    }

    function findIfIsHiddenCityBySlug($slug) {
        $city = $this->findBySlug($slug);
        return $city['City']['is_hidden'];
    }

    function findById($id) {
        return $this->find('first', array(
                    'conditions' => array(
                        'City.id' => $id
                    ),
                    'recursive' => - 1
        ));
    }

    function findBySlug($slug) {
        return $this->find('first', array(
                    'conditions' => array(
                        'City.slug' => $slug
                    ),
                    'recursive' => - 1
        ));
    }

    function findListIsAproved() {
        return $this->find('list', array(
                    'conditions' => array(
                        'City.is_approved =' => 1
                    ),
                    'order' => array(
                        'City.name' => 'asc'
                    )
        ));
    }

    function findActiveForSubscription($isGroup) {
        return $this->find('all', array(
                    'conditions' => array(
                        'City.is_approved' => 1,
                        'City.is_group' => $isGroup,
                        'City.is_hidden' => 0,
                        'City.has_newsletter' => 1,
                        'City.deal_count >=' => 1,
                        'City.is_user_subscription_allowed' => 1
                    ),
                    'order' => 'City.order DESC',
                    'recursive' => - 1
        ));
    }

    function findActiveCitiesForSubscription() {
        return $this->findActiveForSubscription(0);
    }

    function findActiveGroupsForSubscription() {
        return $this->findActiveForSubscription(1);
    }

    function findApprovedCitiesAndApprovedGroupsList() {
        return $this->find('list', array(
                    'conditions' => array(
                        'City.is_approved =' => 1
                    ),
                    'order' => array(
                        'City.name' => 'asc'
                    )
        ));
    }

    function isCorporate($slug) {
        Debugger::log('isCorporate, slug: ' . $slug);
        if (!($cities_slug_corporate = Cache::read('cities_slug_corporate'))) {
            $cities_slug_corporate = $this->findCitiesSlugsCorporate();
            Cache::write('cities_slug_corporate', $cities_slug_corporate);
        }
        $isCorporate = false;
        foreach ($cities_slug_corporate as $key => $value) {
            $slug_corporate = $value['City']['slug'];
            $isCorporate = $slug_corporate == $slug;
            if ($isCorporate) {
                break;
            }
        }
        return $isCorporate;
    }

    function isUserSubscriptionEnabled($slug) {
        $city = $this->findBySlug($slug);
        return $city['City']['is_user_subscription_allowed'];
    }

    function findCitiesSlugsCorporate() {
        return $this->find('all', array(
                    'conditions' => array(
                        'is_corporate' => 1
                    ),
                    'recursive' => - 1,
                    'fields' => array(
                        'slug'
                    )
        ));
    }

}
