<?php

App::import('vendor', 'Utility', array(
    'file' => 'Utility/ApiLogger.php'
));
App::import('Behavior', 'order.OrderDeal');

class OrderProcessingsController extends OrderAppController {

    const START_BUY = 0;
    const SELECT_DEAL = 1;
    const VERIFY_EMAIL = 2;
    const LOGIN = 3;
    const REGISTER = 4;
    const UPDATE_USER_PROFILE = 5;
    const SELECT_PAYMENT_OPTION = 6;
    const SOURCE_DESCUENTO_CITY = 'descuentocity.com';

    private $deal = array();
    public $name = 'OrderProcessings';
    public $helpers = array(
        'Html',
        'OrderFormatStrings'
    );
    public $components = array(
        'RequestHandler',
        'api.ApiUserLoginService',
        'api.ApiUserRegisterService',
        'api.ApiBuyService',
        'api.ApiBacService',
        'api.ApiGiftPinService',
        'order.OrderProcessingsService',
        'order.OrderRequestValidatorFactory'
    );
    public $uses = array(
        'api.ApiDeal',
        'api.ApiDealCategory',
        'api.ApiCompany',
        'api.ApiUser',
        'api.ApiUserProfile',
        'api.ApiCity',
        'api.ApiDealExternal',
        'api.ApiPaymentType',
        'api.ApiPaymentOption',
        'shipping.ShippingAddress',
        'shipping.ShippingAddressDeal',
        'api.ApiState',
        'shipping.ShippingAddressUser'
    );

    private function initializeEnvironment() {
        $this->Security->enabled = false;
        $this->set('is_corporate_city', 0);
        $fileLogPrefix = 'api';
        $this->ApiLogger = new ApiLogger(get_class($this), $fileLogPrefix);
    }

    private function initializeDependencies() {
        $this->ApiDeal->Behaviors->attach('OrderDeal');
    }

    public function beforeFilter() {
        $this->initializeEnvironment();
        $this->initializeDependencies();
        parent::beforeFilter();
    }

    public function beforeRender() {
        $this->layout = 'order_layout';
        parent::beforeRender();
    }

    public function buy() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->deal = $this->OrderProcessingsService->findDealById($this->params['named']['deal_id']);
        $this->set('isLoggedUser', $this->isLoggedUser());
        $this->set('isDescuentoCityRequest', $this->isDescuentoCityRequest());
        $this->set('descuentoCityUtmRef', $this->utmRef());
        if (!empty($this->deal)) {
            $this->deal['DealCategory'] = $this->ApiDealCategory->findById($this->deal['Deal']['deal_category_id']);
            $this->deal['Company'] = $this->ApiCompany->findByIdForApi($this->deal['Deal']['company_id']);
            $this->set('deal', $this->deal);
            $this->set('quantity', $this->data['quantity']);
            $this->set('deal_id', $this->deal['Deal']['id']);
            $this->set('city', $this->ApiCity->findById($this->deal['Deal']['city_id']));
            if ($this->deal['has_stock'] || (!$this->ApiDeal->isSubdeal($this->deal))) {
                $this->step();
            } else {
                $this->Session->setFlash('Ya alcanzaste el límite máximo de compra para esta oferta.', 'default', null, 'error');
                $this->cakeError('error404');
            }
        } else {
            $this->Session->setFlash('Oferta no encontrada.', 'default', null, 'error');
            $this->cakeError('error404');
        }
    }

    private function step() {
        switch ($this->params['named']['step']) {
            case self::START_BUY:
                $this->startBuy();
                break;

            case self::SELECT_DEAL:
                $this->selectDeal();
                break;

            case self::VERIFY_EMAIL:
                $this->verifyEmail();
                break;

            case self::LOGIN:
                $this->login();
                break;

            case self::REGISTER:
                $this->register();
                break;

            case self::UPDATE_USER_PROFILE:
                $this->updateUserProfile();
                break;

            case self::SELECT_PAYMENT_OPTION:
                $this->selectPaymentOption();
                break;

            default:
                $this->startBuy();
                break;
        }
    }

    private function startBuy() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->set('min_quantity', $this->minQuantityForBuy());
        if (!empty($this->deal['Deal']['max_limit'])) {
            $this->set('max_quantity', $this->maxQuantityForBuy());
        }
        if ($this->ApiDeal->hasSubDeals($this->deal)) {
            $subdeals = $this->OrderProcessingsService->findAllSubdealByParentDealId($this->deal['Deal']['id']);
            $this->set('subdeals', $subdeals);
            $this->set('step', self::SELECT_DEAL);
            $sideDeals = $this->ApiDeal->findDealsPurchasableByCityId(Configure::read('Actual.city_id'), $this->deal['Deal']['id']);
            $this->set('side_deals', $sideDeals);
        } elseif ($this->isLoggedUser()) {
            $this->verifyUserDni();
        } else {
            $this->set('step', self::VERIFY_EMAIL);
        }
    }

    private function selectDeal() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        if ($this->isLoggedUser()) {
            $this->verifyUserDni();
        } else {
            $this->set('step', self::VERIFY_EMAIL);
        }
    }

    private function verifyEmail() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->set('min_quantity', $this->minQuantityForBuy());
        if ($this->OrderProcessingsService->isRegisteredUser($this->data['User']['email'])) {
            $this->set('step', self::LOGIN);
        } else {
            $this->set('show_subscription', $this->isSubscriptionEnabledForCity($this->deal['Deal']['city_id']));
            $this->set('step', self::REGISTER);
        }
    }

    private function isSubscriptionEnabledForCity($cityId) {
        return $this->ApiCity->isSubscriptionEnabledForCityId($cityId);
    }

    private function setDealPromotionFlags() {
        $this->set('display_365_dialog', $this->deal['Deal']['is_clarin365_deal']);
    }

    private function login() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $validator = $this->OrderRequestValidatorFactory->loginValidator($this->data, null);
        $errors = $validator->validate();
        if (empty($errors)) {
            $user = $this->OrderProcessingsService->findUserByEmail($this->data['User']['email']);
            if (!empty($user)) {
                try {
                    $user = $this->ApiUserLoginService->login($user['User']['username'], $user['User']['email'], $this->data['User']['passwd']);
                    $this->verifyUserDni();
                } catch (Exception $e) {
                    $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
                    $this->Session->setFlash('No se pudo completar el logueo. Verifique los datos ingresados.', 'default', null, 'error');
                    $this->set('step', self::LOGIN);
                }
            } else {
                $this->Session->setFlash('Usuario no se encuentra registrado.', 'default', null, 'error');
                $this->set('step', self::LOGIN);
            }
        } else {
            $this->set('errors', $errors);
            $this->set('step', self::LOGIN);
        }
    }

    private function isSubscriptionRequired() {
        return $this->data['Subscription']['notifications'];
    }

    private function subscribeUserToCityOfDeal() {
        try {
            $this->OrderProcessingsService->subscribeUserToCity($this->Auth->user('id'), $this->deal['Deal']['city_id'], $this->RequestHandler->getClientIP());
        } catch (Exception $e) {
            $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
        }
    }

    private function register() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $validator = $this->OrderRequestValidatorFactory->registerValidator($this->data, null);
        $errors = $validator->validate();
        if (empty($errors)) {
            try {
                $this->data['User']['password'] = $this->data['User']['passwd'];
                $user = $this->ApiUserRegisterService->registerAndActive($this->data, $this->data['Subscription']['notifications']);
                $this->Auth->login($user['User']);
                if ($this->isSubscriptionRequired()) {
                    $this->subscribeUserToCityOfDeal();
                }
                $this->verifyUserDni();
            } catch (Exception $e) {
                $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
                $this->Session->setFlash('Ocurrió un error en el proceso de Registro.', 'default', null, 'error');
                $this->set('step', self::REGISTER);
            }
        } else {
            $this->set('show_subscription', $this->isSubscriptionEnabledForCity($this->deal['Deal']['city_id']));
            $this->set('errors', $errors);
            $this->set('step', self::REGISTER);
        }
    }

    private function isLoggedUser() {
        $user = $this->Auth->user();
        return !empty($user);
    }

    private function prepareSelectPaymentOption() {
        $this->set('isLoggedUser', $this->isLoggedUser());
        $this->set('isDescuentoCityRequest', $this->isDescuentoCityRequest());
        if (!empty($this->deal['Deal']['max_limit'])) {
            $this->set('max_quantity', $this->maxQuantityForBuy());
        }
        $this->set('min_quantity', $this->minQuantityForBuy());
        $paymentOptions = $this->OrderProcessingsService->findAllPaymentOption($this->params['named']['deal_id'], $this->Auth->user('id'), $this->data['Deal']['payment_option_id']);
        $this->set('paymentOptions', $paymentOptions);
        $this->set('step', self::SELECT_PAYMENT_OPTION);
    }

    private function prepareSelectShippingAddresses() {
        $addressesList = $this->ShippingAddressDeal->findAddressesListByDealId($this->params['named']['deal_id']);
        $this->set('hasShippingAddresses', !empty($addressesList));
        $this->set('shippingAddresses', $addressesList);
    }

    private function prepareSelectShippingAddressUser() {
        if ($this->ApiDeal->isShippingAddressUser($this->deal)) {
            $argentineStatesList = $this->ApiState->findAllArgentineStates();
            $this->set('argentineStatesList', $argentineStatesList);
            $shippingAddressUser = $this->ShippingAddressUser->findLastByUserId($this->Auth->user('id'));
            if (!empty($shippingAddressUser)) {                
                $this->data['Deal']['adress_state_id'] = $shippingAddressUser['ShippingAddressUser']['state_id'];
                $this->data['Deal']['adress_city_name'] = $shippingAddressUser['ShippingAddressUser']['city_name'];
                $this->data['Deal']['adress_street'] = $shippingAddressUser['ShippingAddressUser']['street'];
                $this->data['Deal']['adress_number'] = $shippingAddressUser['ShippingAddressUser']['number'];
                $this->data['Deal']['adress_floor_number'] = $shippingAddressUser['ShippingAddressUser']['floor_number'];
                $this->data['Deal']['adress_apartment'] = $shippingAddressUser['ShippingAddressUser']['apartment'];
                $this->data['Deal']['adress_postal_code'] = $shippingAddressUser['ShippingAddressUser']['postal_code'];
                $this->data['Deal']['adress_telephone'] = $shippingAddressUser['ShippingAddressUser']['telephone'];
                $this->data['Deal']['adress_details'] = $shippingAddressUser['ShippingAddressUser']['details'];
            }
        }
    }

    private function verifyUserDni() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->setDealPromotionFlags();
        $user = $this->ApiUserProfile->findByUserId($this->Auth->user('id'));
        if (empty($user['UserProfile']['dni'])) {
            $this->set('step', self::UPDATE_USER_PROFILE);
        } else {
            $this->set('user', $user);
            $this->prepareSelectShippingAddresses();
            $this->prepareSelectPaymentOption();
            $this->prepareSelectShippingAddressUser();
        }
    }

    private function minQuantityForBuy() {
        $minQuantity = $this->deal['Deal']['buy_min_quantity_per_user'];
        return $minQuantity;
    }

    private function maxQuantityForBuy() {
        $maxQuantity = array();
        if ((!empty($this->deal['Deal']['available_qty'])) && ($this->deal['Deal']['available_qty'] < $this->deal['Deal']['buy_max_quantity_per_user'])) {
            $maxQuantity = $this->deal['Deal']['available_qty'];
        } else {
            $maxQuantity = $this->deal['Deal']['buy_max_quantity_per_user'];
        }
        return $maxQuantity;
    }

    private function isUserProfileDniUpdateable() {
        return !empty($this->data['UserProfile']['dni']);
    }

    private function updateUserProfileDni() {
        $this->OrderProcessingsService->updateUserProfileDniByUserId($this->data['UserProfile']['dni'], $this->Auth->user('id'));
    }

    private function updateUserProfile() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->setDealPromotionFlags();
        if ($this->isUserProfileDniUpdateable() && $this->isLoggedUser()) {
            $this->updateUserProfileDni();
            $this->prepareSelectShippingAddresses();
            $this->prepareSelectPaymentOption();
            $this->prepareSelectShippingAddressUser();
        } else {
            $this->Session->setFlash('Acción no permitida, usuario no logueado/Registrado.', 'default', null, 'error');
            $this->set('step', self::UPDATE_USER_PROFILE);
        }
    }

    private function discountCodeData() {
        return !empty($this->data['Deal']['discount_code']) ? $this->data['Deal']['discount_code'] : null;
    }

    private function shippingAddressData() {
        return isset($this->data['Deal']['shipping_address_id']) ? $this->data['Deal']['shipping_address_id'] : null;
    }

    private function shippinAdressUserData() {
        $data = null;
        if ($this->ApiDeal->isShippingAddressUser($this->deal)) {
            $data = array(
                'ShippingAddressUser' => array(
                    'country_id' => 284,
                    'state_id' => $this->data['Deal']['adress_state_id'],
                    'city_name' => $this->data['Deal']['adress_city_name'],
                    'street' => $this->data['Deal']['adress_street'],
                    'number' => $this->data['Deal']['adress_number'],
                    'floor_number' => $this->data['Deal']['adress_floor_number'],
                    'apartment' => $this->data['Deal']['adress_apartment'],
                    'postal_code' => $this->data['Deal']['adress_postal_code'],
                    'telephone' => $this->data['Deal']['adress_telephone'],
                    'details' => $this->data['Deal']['adress_details']
                )
            );
        }
        return $data;
    }

    private function placeOrder() {
        return $this->ApiBuyService->buy(
                        $this->Auth->user('id'), $this->params['named']['deal_id'], $this->data['quantity'], $this->data['Deal']['payment_option_id'], $this->data['Deal']['payment_plan_item_id'], $this->data['Deal']['is_combined'], ApiDealExternal::BUY_CHANNEL_TYPE_SITE, $this->data['Deal']['gift_options'], $this->discountCodeData(), $this->shippingAddressData(), $this->requestData(), $this->shippinAdressUserData()
        );
    }

    private function isGatewayPayment($pin) {
        $isGatewayPayment = $this->OrderProcessingsService->isGatewayPayment($this->data['Deal']['payment_option_id']);
        $isCreditEnough = $this->ApiBuyService->isCreditEnough($this->Auth->user('id'), $this->deal['Deal']['discounted_price'], $this->data['quantity'], $this->data['Deal']['payment_option_id'], $pin);
        return ($isGatewayPayment && !$isCreditEnough);
    }

    private function gatewayPaymentInfo($dealExternal) {
        try {
            return $this->ApiBacService->getBacPaymentInfo($dealExternal['DealExternal']['id'], $this->RequestHandler->getClientIP());
        } catch (Exception $e) {
            $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
            return null;
        }
    }

    private function isEnabledToBuy($pin) {
        return $this->OrderProcessingsService->isEnabledToBuy($this->Auth->user('id'), $this->deal['Deal']['discounted_price'], $this->data['quantity'], $this->data['Deal']['payment_option_id'], $pin);
    }

    private function prepareSelectPaymentOptionWithErrors($errors) {
        $user = $this->ApiUser->findById($this->Auth->user('id'));
        $this->set('errors', $errors);
        $this->set('user', $user);
        $this->set('discount_code', $this->data['Deal']['discount_code']);
        $this->prepareSelectShippingAddresses();
        $this->prepareSelectPaymentOption();
        $this->prepareSelectShippingAddressUser();
    }

    private function checkout($pin) {
        try {
            $dealExternal = $this->placeOrder();
            $this->ApiLogger->notice('Order Placed', $dealExternal);
            if ($this->isGatewayPayment($pin)) {
                $bacInfo = $this->gatewayPaymentInfo($dealExternal);
                $this->ApiLogger->notice(__METHOD__, $bacInfo);
                if (!empty($bacInfo)) {
                    $this->set('bacInfo', $bacInfo);
                    $this->render('pay');
                } else {
                    $this->Session->setFlash('Ocurrió un error en el proceso de Selección.', 'default', null, 'error');
                    $this->prepareSelectShippingAddresses();
                    $this->prepareSelectPaymentOption();
                    $this->prepareSelectShippingAddressUser();
                }
            } else {
                $slug = $this->OrderProcessingsService->findSlugByDeal($deal);
                $this->OrderProcessingsService->notifyClarin365($deal, $dealExternal, $this->data);
                $this->set('dealUrl', $this->dealUrl($slug, $this->params['named']['city']));
                $this->set('dealExternal', $dealExternal);
                $this->Session->setFlash('Seleccionaste una oferta con éxito.', 'default', null, 'success');
                $this->render("buyOk");
            }
        } catch (DomainException $e) {
            $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
            $this->Session->setFlash($e->getMessage(), 'default', null, 'error');
            unset($this->data['Deal']['shipping_address_id']);
            $this->prepareSelectShippingAddresses();
            unset($this->data['Deal']['payment_option_id']);
            $this->prepareSelectPaymentOption();
            unset($this->data['Deal']['adress_state_id']);
            $this->prepareSelectShippingAddressUser();
        } catch (Exception $e) {
            $this->ApiLogger->error(__METHOD__ . ' ' . $e->getMessage());
            $this->Session->setFlash('Ocurrió un error en el proceso de Selección.', 'default', null, 'error');
            $this->prepareSelectShippingAddresses();
            $this->prepareSelectPaymentOption();
            $this->prepareSelectShippingAddressUser();
        }
    }

    private function selectPaymentOption() {
        $this->ApiLogger->debug(__METHOD__, $this->data);
        $this->setDealPromotionFlags();
        if ($this->isLoggedUser()) {
            $validator = $this->OrderRequestValidatorFactory->selectPaymentOptionValidator($this->data, $this->Auth->user('id'), $this->deal);
            $errors = $validator->validate();
            if (empty($errors)) {
                $pin = $this->ApiGiftPinService->isUsable($this->data['Deal']['discount_code']);
                $this->set('pin', $pin);
                if ($this->isEnabledToBuy($pin)) {
                    $this->checkout($pin);
                } else {
                    $this->set('is_combined', 1);
                    $this->prepareSelectPaymentOptionWithErrors($errors);
                }
            } else {
                unset($this->data['Deal']['payment_option_id']);
                $this->prepareSelectPaymentOptionWithErrors($errors);
            }
        } else {
            $this->Session->setFlash('Debes iniciar sesión para comprar.', 'default', null, 'error');
            $this->cakeError('error404');
        }
    }

    private function dealUrl($dealSlug, $citySlug) {
        return STATIC_DOMAIN_FOR_CLUBCUPON . '/' . $citySlug . '/deal/' . $dealSlug;
    }

    function buyPending($dealExternalId) {
        $dealExternal = $this->OrderProcessingsService->findDealExternalByIdAndUserId($this->Auth->user('id'), $dealExternalId);
        $deal = $this->OrderProcessingsService->findDealByIdForBuyPending($dealExternal['DealExternal']['deal_id']);
        $deal['DealCategory'] = $this->ApiDealCategory->findById($deal['Deal']['deal_category_id']);
        $deal['Company'] = $this->ApiCompany->findByIdForApi($deal['Deal']['company_id']);
        $is_payment_type_mp = $this->ApiPaymentType->isPaymentTypeMP($dealExternal['DealExternal']['payment_type_id']);
        $is_payment_offline = $this->ApiPaymentOption->isPaymentOptionOffline($dealExternal['DealExternal']['payment_option_id']);
        $slug = $this->OrderProcessingsService->findSlugByDeal($deal);
        $this->Session->setFlash('Seleccionaste una oferta con éxito.', 'default', null, 'success');
        $this->set('is_payment_offline', $is_payment_offline);
        $this->set('dealUrl', $this->dealUrl($slug, $deal['City']['City']['slug']));
        $this->set('city', $deal['City']);
        $this->set('is_payment_type_mp', $is_payment_type_mp);
        $this->set('dealExternal', $dealExternal);
        $this->set('deal', $deal);
        $this->set('internalAmount', $dealExternal['DealExternal']['internal_amount']);
        $this->set('isDescuentoCityBuy', $this->isDescuentoCityBuy($dealExternal));
    }

    function isDescuentoCityBuy($dealExternal) {
        $request = array('utm_source' => null);
        if (!empty($dealExternal['DealExternal']['source'])) {
            $request = json_decode($dealExternal['DealExternal']['source'], true);
        }
        return $request['utm_source'] == self::SOURCE_DESCUENTO_CITY;
    }

    function buyCanceled($dealExternalId) {
        $dealExternal = $this->OrderProcessingsService->findDealExternalByIdAndUserId($this->Auth->user('id'), $dealExternalId);
        $deal = $this->OrderProcessingsService->findDealByIdForBuyPending($dealExternal['DealExternal']['deal_id']);
        $slug = $this->OrderProcessingsService->findSlugByDeal($deal);
        if ($dealExternal['DealExternal']['user_id'] != $this->Auth->user('id')) {
            $this->Session->setFlash('Pago inexistente para el usuario.', 'default', null, 'error');
        }
        $this->set('city', $deal['City']);
        $this->set('dealUrl', $this->dealUrl($slug, $deal['City']['City']['slug']));
        $this->set('dealExternal', $dealExternal);
        $this->set('citySlug', $deal['City']['City']['slug']);
    }

    private function isDescuentoCityRequest() {
        $isDescuentoCityRequest = false;
        if (Configure::read('descuentocity.is_tracking_enabled')) {
            $isDescuentoCityRequest = $this->params['url']['utm_source'] == self::SOURCE_DESCUENTO_CITY || $this->params['named']['source'] == self::SOURCE_DESCUENTO_CITY;
        }
        return $isDescuentoCityRequest;
    }

    private function utmRef() {
        $ref = false;
        if (isset($this->params['url']['utm_ref'])) {
            $ref = $this->params['url']['utm_ref'];
        } elseif (isset($this->params['named']['ref'])) {
            $ref = $this->params['named']['ref'];
        }
        return $ref;
    }

    function requestData() {
        $requestData['clientIp'] = $this->RequestHandler->getClientIP();
        if ($this->isDescuentoCityRequest()) {
            $requestData['utm_source'] = self::SOURCE_DESCUENTO_CITY;
            $requestData['utmRef'] = $this->utmRef();
        }
        return json_encode($requestData);
    }

}
