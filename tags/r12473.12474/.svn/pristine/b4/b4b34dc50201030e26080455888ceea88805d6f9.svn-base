<?php

class Banner extends AppModel {

    var $name = 'Banner';
    var $belongsTo = array(
        'BannerType',
        'City'
    );
    var $actsAs = array(
        'Sluggable',
        'RowObject' => array(
            'rowClass' => 'BannerRow'
        )
    );
    var $validate = array(
        'name' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        ),
        'status' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        ),
        'order' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        ),
        'image' => array(
            'rule' => '_isValidSize',
            'message' => 'Revise que haya cargado una imagen, y que tenga las medidas adecuadas',
            'allowEmpty' => false
        ),
        'start_date' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        ),
        'end_date' => array(
            'rule1' => array(
                'rule' => '_isValidEndDate',
                'message' => 'Fecha de finalización debe ser mayor que la fecha de inicio',
                'allowEmpty' => false
            ),
            'rule2' => array(
                'rule' => 'notempty',
                'message' => 'Requerido',
                'allowEmpty' => false
            )
        ),
        'link' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        ),
        'alt' => array(
            'rule' => 'notempty',
            'message' => 'Requerido',
            'allowEmpty' => false
        )
    );

    function _isValidEndDate() {
        return strtotime($this->data[$this->name]['end_date']) > strtotime($this->data[$this->name]['start_date']);
    }

    function _isValidSize($data, $required = false) {
        if ($this->data['Banner']['tmp_image_name'] != '') {
            list($width, $height) = getimagesize($this->data['Banner']['tmp_image_name']);
            if (!empty($this->data['Banner']['banner_type_id'])) {
                $sizes = $this->BannerType->retrieveBannerSizes($this->data['Banner']['banner_type_id']);
            } else {
                return false;
            }
            return ($width == $sizes['maxWidth'] && $height == $sizes['maxHeight']);
        } else {
            return false;
        }
    }

    function __findActiveBannersForCurrentCity($options = array()) {
        $today = date('Y-m-d H:i:s');
        $currentCity = Configure::read('Actual.city_id');
        $the_options = array(
            'conditions' => array(
                'status =' => '1',
                'OR' => array(
                    array(
                        'city_id = ' => ''
                    ),
                    array(
                        'city_id = ' => $currentCity
                    )
                ),
                'AND' => array(
                    array(
                        'start_date <=' => $today
                    ),
                    array(
                        'end_date   >=' => $today
                    ),
                    array(
                        'banner_type_id   =' => $this->BannerType->field('id', array(
                            'label' => 'Front'
                        ))
                    )
                )
            ),
            'order' => 'Banner.order ASC',
            'limit' => 1
        );
        $banners = $this->find('all', array_merge($the_options, $options));
        if (!$banners) {
            $banner->style = 'box_banner';
            $banner->link = 'http://www.clubcupon.com.ar/especial-belleza';
            $banner->image = '/img/Banner/banner_club_cupon.jpg';
            $banner->alt = 'Club Cup&oacute;n';
            $banners[0] = $banner;
        }
        return $banners;
    }

    function __getNewsletterBanner($city_id = null) {
        $banner = array();
        if (empty($city_id)) {
            $banner['Banner']['image'] = '/img/email/banner_nl_diario_viajes.jpg';
            $banner['Banner']['link'] = 'http://www.clubcupon.com.ar/turismo?utm_source=NL&utm_medium=BANNER&utm_content=Turismo&utm_campaign=Default&popup=no';
            $banner['Banner']['tracker_url'] = 'http://www.clubcupon.com.ar/img/spacer.png';
        } else {
            $banner = $this->__getNewsletterBannerByCity($city_id);
            if (!$banner) {
                $banner = $this->__getNewsletterBannerForAllCities();
            }
        }
        return $banner;
    }

    function __getNewsletterBannerByCity($city_id) {
        $today = date('Y-m-d H:i:s');
        $the_options = array(
            'conditions' => array(
                'status' => '1',
                'banner_type_id' => '2',
                'city_id' => $city_id,
                'AND' => array(
                    array(
                        'start_date <=' => $today
                    ),
                    array(
                        'end_date   >=' => $today
                    )
                )
            ),
            'order' => 'Banner.order DESC',
            'recursive' => - 1
        );
        $banner = $this->find('first', $the_options);
        return $banner;
    }

    function __getNewsletterBannerForAllCities() {
        $today = date('Y-m-d H:i:s');
        $the_options = array(
            'conditions' => array(
                'status' => '1',
                'banner_type_id' => '2',
                'city_id' => '',
                'AND' => array(
                    array(
                        'start_date <=' => $today
                    ),
                    array(
                        'end_date   >=' => $today
                    )
                )
            ),
            'order' => 'Banner.order DESC',
            'recursive' => - 1
        );
        $banner = array();
        $banner = $this->find('first', $the_options);
        if (!$banner) {
            $banner['Banner']['image'] = '/img/email/banner_nl_diario_viajes.jpg';
            $banner['Banner']['link'] = 'http://www.clubcupon.com.ar/turismo?utm_source=NL&utm_medium=BANNER&utm_content=Turismo&utm_campaign=Default&popup=no';
            $banner['Banner']['tracker_url'] = 'http://www.clubcupon.com.ar/img/spacer.png';
        }
        return $banner;
    }

    function findCityNewsletterTopBanner($cityId) {
        $today = date('Y-m-d H:i:s');
        $conditions = array(
            'status' => 1,
            'banner_type_id' => 4,
            'city_id' => $cityId,
            'AND' => array(
                array(
                    'start_date <=' => $today
                ),
                array(
                    'end_date >=' => $today
                )
            )
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'order' => 'Banner.order DESC',
                    'recursive' => -1
        ));
    }

    function findGlobalNewsletterTopBanner() {
        $today = date('Y-m-d H:i:s');
        $conditions = array(
            'status' => 1,
            'banner_type_id' => 4,
            'city_id' => '',
            'AND' => array(
                array(
                    'start_date <=' => $today
                ),
                array(
                    'end_date >=' => $today
                )
            )
        );
        return $this->find('first', array(
                    'conditions' => $conditions,
                    'order' => 'Banner.order DESC',
                    'recursive' => -1
        ));
    }

    function findNewsletterTopBanner($cityId) {
        $banner = $this->findCityNewsletterTopBanner($cityId);
        if (empty($banner)) {
            $banner = $this->findGlobalNewsletterTopBanner();
        }
        return $banner;
    }

}
