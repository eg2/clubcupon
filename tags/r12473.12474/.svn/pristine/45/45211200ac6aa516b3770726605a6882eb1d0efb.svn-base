<?php

/**
 * Short description for file.
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7904 $
 * @modifiedby    $LastChangedBy: mark_story $
 * @lastmodified  $Date: 2008-12-05 22:19:43 +0530 (Fri, 05 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
App::import('Core', 'Helper');

/**
 * This is a placeholder class.
 * Create the same file in app/app_helper.php
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake
 */
class AppHelper extends Helper {

  function getUserAvatar($user_id) {
    App::import('Model', 'User');
    $this->User = new User();
    $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $user_id,
                ),
                'fields' => array(
                    'UserAvatar.id',
                    'UserAvatar.dir',
                    'UserAvatar.filename'
                ),
                'recursive' => 0
            ));
    return $user['UserAvatar'];
  }

  function checkForPrivacy($type = null, $field_to_check = null, $logged_in_user = null, $user_id = null, $is_boolean = false) {
    App::import('Model', 'UserPermissionPreference');
    $this->UserPermissionPreference = new UserPermissionPreference();
    $privacy = $this->UserPermissionPreference->getUserPrivacySettings($user_id);
    $is_show = true;
    if (Configure::read($type . '-' . $field_to_check)) {
      if ($privacy['UserPermissionPreference'][$type . '-' . $field_to_check] == ConstPrivacySetting::Users && !$logged_in_user) {
        $is_show = false;
      } else if ($privacy['UserPermissionPreference'][$type . '-' . $field_to_check] == ConstPrivacySetting::Nobody) {
        $is_show = false;
      } else if ($privacy['UserPermissionPreference'][$type . '-' . $field_to_check] == ConstPrivacySetting::Friends) {
        // To write user friends lists in config
        App::import('Model', 'UserFriend');
        $this->UserFriend = new UserFriend();
        $is_show = $this->UserFriend->checkIsFriend($logged_in_user, $user_id);
      } else if ($is_boolean) {
        $is_show = $privacy['UserPermissionPreference'][$type . '-' . $field_to_check];
      }
    }
    return $is_show;
  }

  function getLanguage() {
    App::import('Model', 'Translation');
    $this->Translation = new Translation();
    $languages = $this->Translation->find('all', array(
                'conditions' => array(
                    'Language.id !=' => 0
                ),
                'fields' => array(
                    'DISTINCT(Translation.language_id)',
                    'Language.name',
                    'Language.iso2'
                ),
                'order' => array(
                    'Language.name' => 'ASC'
                )
            ));
    $languageList = array();
    if (!empty($languages)) {
      foreach ($languages as $language) {
        $languageList[$language['Language']['iso2']] = $language['Language']['name'];
      }
    }
    return $languageList;
  }

  function getCompany($user_id = null) {
    App::import('Model', 'Company');
    $this->Company = new Company();
    $company = $this->Company->find('first', array(
                'conditions' => array(
                    'Company.user_id' => $user_id,
                ),
                'fields' => array(
                    'Company.id',
                    'Company.name',
                    'Company.slug',
                    'Company.is_company_profile_enabled'
                ),
                'recursive' => -1
            ));
    return $company;
  }

  function siteLogo() {
    App::import('Model', 'Attachment');
    $this->Attachment = new Attachment();
    $attachment = $this->Attachment->find('first', array(
                'conditions' => array(
                    'Attachment.class' => 'SiteLogo'
                ),
                'fields' => array(
                    'Attachment.id',
                    'Attachment.dir',
                    'Attachment.filename',
                    'Attachment.width',
                    'Attachment.height'
                ),
                'recursive' => -1
            ));
    return $attachment;
  }

  function isAllowed($user_type = null) {
    if ($user_type == ConstUserTypes::Company && !Configure::read('user.is_company_actas_normal_user')) {
      return false;
    }
    return true;
  }

  function getCityTwitterFacebookURL($slug = null) {
    App::import('Model', 'City');
    $this->City = new City();
    $city = $this->City->find('first', array(
                'conditions' => array(
                    'City.slug' => $slug
                ),
                'fields' => array(
                    'City.twitter_url',
                    'City.facebook_url'
                ),
                'recursive' => -1
            ));
    return $city;
  }

  public function url($url = null, $full = false) {

     $is_plugin_now = (  isset($url['plugin']) && $url['plugin'] =='now');
    if (Cache::read('site.city_url', 'long') == 'prefix' && !$is_plugin_now) {
      return Router::url(router_url_city($url, $this->params['named']), $full);
    }
    if (is_array($url)) {
        $url['foo']=$full;
    }
    return Router::url($url, $full);
  }

  function total_saved() {
    App::import('Model', 'DealUser');
    $this->DealUser = new DealUser();
    $total_saved = $this->DealUser->Deal->find('first', array(
                'conditions' => array(
                    'Deal.deal_status_id' => ConstDealStatus::PaidToCompany
                ),
                'fields' => array(
                    'SUM(Deal.savings * Deal.deal_user_count) as total_saved'
                ),
                'recursive' => -1
            ));
    $total_bought = $this->DealUser->find('first', array(
                'fields' => array(
                    'SUM(DealUser.quantity) as total_bought'
                ),
                'recursive' => -1
            ));

    $total_array = array(
        'total_saved' => (!empty($total_saved[0]['total_saved'])) ? $total_saved[0]['total_saved'] : 0,
        'total_bought' => (!empty($total_bought[0]['total_bought'])) ? $total_bought[0]['total_bought'] : 0,
    );
    return $total_array;
  }

  function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false) {
    return $this->Text->truncate($this->cText($text, false), $length, $ending, $exact, $considerHtml);
  }

  function cCurrency($str, $wrap = 'span', $title = false) {
    $_precision = 2;
    $changed = (($r = floatval($str)) != $str);
    $rounded = (($rt = round($r, $_precision)) != $r);
    $r = $rt;
    if ($wrap) {
      if (!$title) {
        $title = ucwords(Numbers_Words::toCurrency($r, 'es_AR', 'ARS'));
      }
      $number_format = number_format($r, $_precision, ',', '.');
      $number_format = explode(",", $number_format);
      if ($number_format[1] == '00') {
        $number_format = $number_format[0];
      } else {
        $number_format = implode(",", $number_format);
      }
      $r = '<' . $wrap . ' title="' . $title . '">' . $number_format . '</' . $wrap . '>';
    }
    return $r;
  }

  function getUserLink($user_details,$start=0,$length=255) {
    if($user_details['user_type_id'] == ConstUserTypes::Company) {
      $companyDetails = $this->getCompany ($user_details ['id']);
      if (!$companyDetails ['Company']['is_company_profile_enabled']) {
        //return $this->cText ($companyDetails ['Company']['name']);
		return $this->cText(substr($user_details ['username'],$start,$length));
      } else {
        return $this->cText(substr($companyDetails ['Company']['name'],$start,$length), "false");
      }
    }else {
      return $this->cText(substr($user_details ['username'],$start,$length));
    }
  }
  function getUserTypeFriendly($typeId) {
    if(ConstUserTypes::User == $typeId)
      return 'Usuario';
    if(ConstUserTypes::Company == $typeId)
      return 'Empresa';
    if(ConstUserTypes::Agency == $typeId)
      return 'Agencia';
    if(ConstUserTypes::Partner == $typeId)
      return 'Partner';
    if(ConstUserTypes::isLikeAdmin($typeId))
      return 'Admin';
  }

  function getUserAvatarLink($user_details, $dimension = 'medium_thumb', $is_link = true) {
    if (isset($user_details['user_type_id']) && (in_array($user_details['user_type_id'],array(ConstUserTypes::SuperAdmin,ConstUserTypes::Admin,ConstUserTypes::Agency,ConstUserTypes::Partner,ConstUserTypes::User)))) {
      //get user image
      $user_image = $this->showImage('UserAvatar', (!empty($user_details['UserAvatar'])) ? $user_details['UserAvatar'] : '', array('dimension' => $dimension, 'alt' => sprintf('[Image: %s]', $user_details['username']), 'title' => $user_details['username']));
      //return image to user
      return (!$is_link) ? $user_image : $this->link($user_image, array('controller' => 'users', 'action' => 'view', $user_details['username'], 'admin' => false), array('title' => $this->cText($user_details['username'], false), 'escape' => false));
    }
    //for company
    if (isset($user_details['user_type_id']) && ($user_details['user_type_id'] == ConstUserTypes::Company)) {
      $companyDetails = $this->getCompany($user_details['id']);
      //get user image
      $user_image = $this->showImage('UserAvatar', $user_details['UserAvatar'], array('dimension' => $dimension, 'alt' => sprintf('[Image: %s]', $this->cText($companyDetails['Company']['name'], false)), 'title' => $this->cText($companyDetails['Company']['name'], false)));
      //return image to user
      return (!$companyDetails['Company']['is_company_profile_enabled'] || !$is_link) ? $user_image : $this->link($user_image, array('controller' => 'companies', 'action' => 'view', $companyDetails['Company']['slug'], 'admin' => false), array('title' => $this->cText($companyDetails['Company']['name'], false), 'escape' => false));
    }
  }

  function transactionDescription($transaction) {

    $deal_name = (!empty($transaction['DealUser']['Deal']['name'])) ? $transaction['DealUser']['Deal']['name'] : ((!empty($transaction['Deal']['name'])) ? $transaction['Deal']['name'] : '');
    $deal_slug = (!empty($transaction['DealUser']['Deal']['slug'])) ? $transaction['DealUser']['Deal']['slug'] : ((!empty($transaction['Deal']['slug'])) ? $transaction['Deal']['slug'] : '');
    $friend_link = $user_link = '';
    if ($transaction['Transaction']['class'] == 'GiftUser') {
      if ($transaction['Transaction']['transaction_type_id'] == ConstTransactionTypes::GiftSent)
        $friend_link = $this->getUserLink($transaction['GiftUser']['GiftedToUser']);
      else
        $friend_link = $this->getUserLink($transaction['GiftUser']['User']);
    }
    if ($transaction['Transaction']['class'] == 'SecondUser') {
      $user_link = $this->getUserLink($transaction['SecondUser']);
    }
    $transactionReplace = array(
        '##DEAL_LINK##' => (!empty($deal_slug) && ($transaction['Transaction']['class'] == 'DealUser' || $transaction['Transaction']['class'] == 'Deal')) ? $this->link($this->cText($deal_name), array('controller' => 'deals', 'action' => 'view', $deal_slug, 'admin' => false), array('escape' => false, 'title' => $this->cText($deal_name, false))) : '',
        '##FRIEND_LINK##' => $friend_link,
        '##USER_LINK##' => $user_link
    );
    $transaction['TransactionType']['name'] = isset($transaction['TransactionType']['name']) ? $transaction['TransactionType']['name'] : '';
    return strtr($transaction['TransactionType']['name'], $transactionReplace);
  }


  function getCategoryName($category_id) {
    App::import('Model', 'DealCategory');
    $this->DealCategory = new DealCategory;
    $category = $this->DealCategory->find('first', array(
                'conditions' => array(
                    'id' => $category_id,
                ),
                'fields' => array(
                    'DealCategory.name',
                ),
                'recursive' => -1
            ));
    return $category['DealCategory']['name'];
  }

}

?>
