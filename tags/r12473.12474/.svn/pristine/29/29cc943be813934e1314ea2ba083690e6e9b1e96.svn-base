<?php

App::import('Helper', 'Text');

class ProductInventory extends ProductAppModel {

    const SOURCE_MULTIPLACE = 'MULTIPLACE';
    const SOURCE_LIBERTYA = 'LIBERTYA';

    public $name = 'ProductInventory';
    public $alias = 'ProductInventory';
    public $useTable = 'product_inventories';
    public $actsAs = array(
        'api.SoftDeletable'
    );
    public $fields = array(
        'id',
        'name',
        'description',
        'bac_libertya_id',
        'stock',
        'source',
        'source_identifier'
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->Text = new TextHelper();
    }

    function findById($id) {
        $conditions = array(
            'ProductInventory.id' => $id
        );
        return $this->find('first', array(
                    'conditions' => $conditions
        ));
    }

    function selectOptionText($product) {
        $source = $this->Text->truncate($product['ProductInventory']['source'], 16);
        $sourceIdentifier = $this->Text->truncate($product['ProductInventory']['source_identifier'], 16);
        $name = $this->Text->truncate($product['ProductInventory']['name'], 128);
        $stock = (int) $product['ProductInventory']['stock'];
        return $source . ':' . $sourceIdentifier . ' | ' . 'stock:' . $stock . ' | ' . $name;
    }

    function findAllForSelectForm() {
        $products = $this->find('all', array(
            'fields' => $this->fields,
            'order' => array(
                'ProductInventory.source',
                'ProductInventory.source_identifier'
            )
        ));
        $productOptions = array();
        foreach ($products as $product) {
            $productOptions[$product['ProductInventory']['id']] = $this->selectOptionText($product);
        }
        return $productOptions;
    }

    function updateStockByBacLibertyaId($bacLivertyaId, $stock) {
        $fields = array(
            'stock' => $stock
        );
        $conditions = array(
            'bac_libertya_id' => $bacLivertyaId
        );
        $productInventory = $this->findByBacLibertyaId($bacLivertyaId);
        if ($productInventory['ProductInventory']['stock'] == $stock && !empty($productInventory['ProductInventory']['stock'])) {
            return true;
        }
        $this->updateAll($fields, $conditions);
        return $this->getAffectedRows();
    }

    function updateById($fields, $id) {
        $conditions = array(
            'id' => $id
        );
        return $this->updateAll($fields, $conditions);
    }

    function findAllMultiplace() {
        $conditions = array(
            'ProductInventory.source' => self::SOURCE_MULTIPLACE
        );
        $order = array(
            'ProductInventory.source_identifier'
        );
        return $this->find('all', array(
                    'conditions' => $conditions,
                    'order' => $order
        ));
    }

    function isEnabledToBeSentToBac($productInventory) {
        return $productInventory['ProductInventory']['source'] == self::SOURCE_LIBERTYA;
    }

}
