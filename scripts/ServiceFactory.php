<?php

/**
 * Description of ServiceFactory
 *
 * @author msaladino
 */
$path = '';
if (defined('ROOT')) {
    $path = ROOT . DS . 'scripts/';
}
require_once $path . 'AbstractGateway.php';
require_once $path . 'UsersGateway.php';
require_once $path . 'TransactionsGateway.php';
require_once $path . 'DealExternalsGateway.php';
require_once $path . 'DealUsersGateway.php';
require_once $path . 'DealsGateway.php';
require_once $path . 'AnulledCouponsGateway.php';
require_once $path . 'RedemptionsGateway.php';
require_once $path . 'OperationsService.php';

if (!defined('ROOT')) {
    require_once '../core/cake/basics.php';
    require_once '../app/config/database.php';
    require_once '../app/vendors/Precision/Precision.php';
}

class ServiceFactory {

    /**
     *
     * @return OperationsService 
     */
    public static function getOpetarionsService($for = 'console') {
        $db = new DATABASE_CONFIG();
        $mysqli = new mysqli($db->default['host'], $db->default['login'], $db->default['password'], $db->default['database']);
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        $usersGateway = new UsersGateway($mysqli);
        $transactionsGateway = new TransactionsGateway($mysqli);
        $dealExternalsGateway = new DealExternalsGateway($mysqli);
        $dealUsersGateway = new DealUsersGateway($mysqli);
        $dealsGateway = new DealsGateway($mysqli);
        $anulledCouponsGateway = new AnulledCouponsGateway($mysqli);
        $redeptionsGateway = new RedemptionsGateway($mysqli);
        if ($for === 'console') {
            $operationsService = new OperationsService($usersGateway, $transactionsGateway, $dealExternalsGateway, $dealUsersGateway, $dealsGateway, $anulledCouponsGateway, $redeptionsGateway);
        } else if ($for === 'httpRequestCake') {
            $operationsService = new OperationsServiceWithCakeLogger($usersGateway, $transactionsGateway, $dealExternalsGateway, $dealUsersGateway, $dealsGateway, $anulledCouponsGateway, $redeptionsGateway);
        } else {
            throw new Exception('OperationService for: ' . $for . ' not implemented.');
        }
        return $operationsService;
    }

}