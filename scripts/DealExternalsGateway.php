<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DealExternalsGateway
 *
 * @author msaladino
 */
class DealExternalsGateway extends AbstractGateway {

    protected $table = "deal_externals";

    public function sqlUpdateColumn($table, $id, $column, $value) {
        $sql = "update " . $table . " set " . $column . " = " . $this->toNull($this->mysqli->real_escape_string($value));
        if ($this->httpUser) {
            $sql .= " , modified_by = " . $this->httpUser . " ";
        }
        $sql .= " where id = " . $id . " limit 1";
        return $sql;
    }

    public function updateAsCredited($dealExternalId, $amount) {
        $sql = "update " . $this->table . " set updated = now(), final_amount = " . $amount . ", parcial_amount = " . $amount . ", external_status = 'A'";
        if ($this->httpUser) {
            $sql .= " , modified_by = " . $this->httpUser . " ";
        }
        $sql .= " where id = " . $dealExternalId . " limit 1";
        $this->mysqli->query($sql);
        return $this->mysqli->affected_rows;
    }
    
    public function getTotalPurchasedFromWalletDateRange($userId,$dateFrom,$dateTo){
    	/*obtiene el total gastado del monedero en un determinado periodo de tiempo*/
    	/* formateamos las fechas ingresadas */
    	$dateFrom=date("Y-m-d", strtotime($dateFrom));
    	$dateTo=date("Y-m-d", strtotime($dateTo));
    	
    	
    	$sql="SELECT IF( SUM( internal_amount ) IS NULL , 0, SUM( internal_amount ) )
			  FROM " . $this->table . "
			  WHERE user_id =" . $userId . "
			  AND created >= '" . $dateFrom . "'
			  AND created <= '" . $dateTo . "'";
        $resultado = $this->mysqli->query($sql);
        while ($fila = mysqli_fetch_row($resultado)) {
            $totalPurchased = $fila[0];
        }
        return $totalPurchased;
    }

    function copyDealExternalWithWallet($dealExternal) {
        return array('bac_id' => null,
            'deal_id' => $dealExternal['deal_id'],
            'user_id' => $dealExternal['user_id'],
            'payment_type_id' => 5000,
            'quantity' => $dealExternal['quantity'],
            'internal_amount' => $dealExternal['final_amount'],
            'final_amount' => $dealExternal['final_amount'],
            'parcial_amount' => 0,
            'external_status' => 'P',
            'bac_substate' => '',
            'used_points' => $dealExternal['used_points'],
            'created' => date('Y-m-d H:i:s'),
            'updated' => date('Y-m-d H:i:s'),
            'is_gift' => $dealExternal['is_gift'],
            'gift_data' => $dealExternal['gift_data'],
            'buy_ip' => $dealExternal['buy_ip'],
            'created_by' => 0,
            'modified_by' => 0,
            'cancellation_reason' => '',
            'payment_option_id' => $dealExternal['payment_option_id'],
            'is_send_email' => array(1,2));
    }

}

?>
