<?php
# Scritp para automatizar la ejecución del store promo_duplicate()
# Nicolás Demarchi 2/03/2012
# ver http://jira.int.clarin.com/browse/SOPOP-13849
require_once 'ServiceFactory.php';
$fecha = time (); 

echo "Ejecutando Proceso eliminar Fakes \n";
echo date ( "Y-m-d h:i:s \n" , $fecha ); 
$operationsService = ServiceFactory::getOpetarionsService();
$db = new DATABASE_CONFIG();
$mysqli = new mysqli($db->default['host'], $db->default['login'], $db->default['password'], $db->default['database']);
if ($mysqli->connect_errno) {
    printf("Error al conectar: %s\n", $mysqli->connect_error);
    exit();
}
$rs = $mysqli->query("CALL promo_duplicate()");
while($row = $rs->fetch_array())
{
$list[] = $row;
}
if (count($list) > 0) {
	foreach ($list as $dealExternalId) {
    		$operationsService->creditDealExternal($dealExternalId['id']);
	}	
} else {
  echo "No hay cupones para duplicar";
}
$rs->close();
$mysqli->close();
?>