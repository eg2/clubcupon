<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DealUsersGateway
 *
 * @author msaladino
 */
class DealUsersGateway extends AbstractGateway {

    protected $table = "deal_users";

    public function findByDealExternalId($dealExternalId) {
        return $this->select("deleted = 0 and deal_external_id = " . $dealExternalId);
    }

    public function newDealUser($dealExternal, $deal) {
        return array('created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'user_id' => $dealExternal['user_id'],
            'deal_id' => $dealExternal['deal_id'],
            'deal_external_id' => $dealExternal['id'],
            'coupon_code' => $this->uuid(),
            'pin_code' => null,
            'quantity' => 1,
            'discount_amount' => $deal['discounted_price'],
            'payment_type_id' => $dealExternal['payment_type_id'],
            'is_paid' => 1,
            'is_repaid' => 0,
            'is_gift' => 0,
            'gift_to' => null,
            'gift_from' => null,
            'gift_email' => null,
            'message' => null,
            'is_used' => 0,
            'is_used_user' => 0,
            'emailed' => 0,
            'email_to_company' => 0);
    }
    
    public function delete($id) {
        $this->updateColumn($id, 'annulled', date('Y-m-d H:i:s'));
        return $this->updateColumn($id, 'deleted', 1);
    }
}

