<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DealsGateway
 *
 * @author msaladino
 */
class RedemptionsGateway extends AbstractGateway {

    protected $table = 'redemptions';

    public function generatePosnetCode() {
        $length = 10;
        usleep(3);
        list($usec, $sec) = explode(' ', microtime());
        mt_srand(($sec + $usec) * 1000000);
        $xrand = mt_rand();
        if (strlen($xrand) < $length) {
            $xrand = $this->generatePosnetCode($length - strlen($xrand)) . $xrand;
        }
        return '62797603' . substr($xrand, -$length);
    }

    function newRedemption($dealUserId) {
        return array(
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'created_by' => 0,
            'modified_by' => 0,
            'deal_user_id' => $dealUserId,
            'posnet_code' => $this->generatePosnetCode());
    }

    public function deleteByDealUserId($dealUserId) {
        $sql = "update " . $this->table . " set deleted=1 where deal_user_id = " . $dealUserId . " limit 1";
        $this->mysqli->query($sql);
        return $this->mysqli->affected_rows;
    }

    public function delete($id) {
        return $this->updateColumn($id, 'deleted', 1);
    }

}

?>
