<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OperationsService
 *
 * @author msaladino
 */
class OperationsService {

    private $usersGateway;
    private $transactionsGateway;
    private $dealExternalsGateway;
    private $dealUsersGateway;
    private $dealsGateway;
    private $anulledCouponsGateway;
    private $redemptionsGateway;
    protected $httpUser = false;

    protected function log($msg) {
        echo $msg . "\n";
    }

    public function __construct(UsersGateway $usersGateway, TransactionsGateway $transactionsGateway, DealExternalsGateway $dealExternalsGateway, DealUsersGateway $dealUsersGateway, DealsGateway $dealsGateway, AnulledCouponsGateway $anulledCouponsGateway, RedemptionsGateway $redemptionsGateway) {
        $this->usersGateway = $usersGateway;
        $this->transactionsGateway = $transactionsGateway;
        $this->dealExternalsGateway = $dealExternalsGateway;
        $this->dealUsersGateway = $dealUsersGateway;
        $this->dealsGateway = $dealsGateway;
        $this->anulledCouponsGateway = $anulledCouponsGateway;
        $this->redemptionsGateway = $redemptionsGateway;
    }

    public function setHttpUser($userId) {
        $this->httpUser = $userId;
        $this->dealExternalsGateway->setHttpUser($userId);
    }

    public function addFundsToWallet($userId, $amount) {
        $user = $this->usersGateway->find($userId);
        if (!empty($user)) {
            $user['available_balance_amount'] = Precision::add($user['available_balance_amount'], $amount);
            if ($user['available_balance_amount'] < 0) {
                $user['available_balance_amount'] = 0;
            }
            $this->usersGateway->updateColumn($userId, 'available_balance_amount', $user['available_balance_amount']);
            $transaction = $this->transactionsGateway->newAddFunds($userId, $amount, $this->httpUser);
            $this->transactionsGateway->insert($transaction);
            $this->log("[addFundsToWallet] " . "userId: " . $userId . ", amount: " . $amount . ", available_balance_amount: " . $user['available_balance_amount']);
        } else {
            $this->log("[addFundsToWallet] " . "unknown userId: " . $userId);
            throw new Exception("[addFundsToWallet] " . "unknown userId: " . $userId);
        }
    }

    public function addFundsToWalletBetweenDates($userId, $amount, $dateFrom, $dateTo) {
        if ($amount > 0) {
            throw new Exception("[addFundsToWallet] the amount must be lower than 0 ");
        }
        $totalPurchased = $this->dealExternalsGateway->getTotalPurchasedFromWalletDateRange($userId, $dateFrom, $dateTo);
        if ($totalPurchased < abs($amount)) {
            $amount = $amount + $totalPurchased;
            $this->addFundsToWallet($userId, $amount);
        }
    }

    public function creditDealExternal($dealExternalId) {
        $dealExternal = $this->dealExternalsGateway->find($dealExternalId);
        if (!empty($dealExternal)) {
            $dealUsers = $this->dealUsersGateway->findByDealExternalId($dealExternalId);
            $quantity = $dealExternal['quantity'] - count($dealUsers);
            for ($i = 1; $i <= $quantity; $i++) {
                $deal = $this->dealsGateway->find($dealExternal['deal_id']);
                if ($i == 1) {
                    $amount = $dealExternal['quantity'] * $deal['discounted_price'];
                    $this->dealExternalsGateway->updateAsCredited($dealExternalId, $amount);
                    if (!$dealExternal['external_status'] == 'A') {
                        $addFundsTransaction = $this->transactionsGateway->newAddFunds($dealExternal['user_id'], $amount);
                        $this->transactionsGateway->insert($addFundsTransaction);
                    }
                    $this->log('[creditDealExternal] dealExternalId: ' . $dealExternalId . " credited");
                }
                $newDealUser = $this->dealUsersGateway->newDealUser($dealExternal, $deal);
                $newDealUser['id'] = $this->dealUsersGateway->insert($newDealUser);
                $newRedemption = $this->redemptionsGateway->newRedemption($newDealUser['id']);
                $this->redemptionsGateway->insert($newRedemption);
                $dealUserTransaction = $this->transactionsGateway->newDealUser($newDealUser, $deal);
                $this->transactionsGateway->insert($dealUserTransaction);
                $this->log('[creditDealExternal] dealUserId: ' . $newDealUser['id'] . " inserted");
            }
        } else {
            $this->log('[creditDealExternal] unknown dealExternalId: ' . $dealExternalId);
            throw new Exception('[creditDealExternal] unknown dealExternalId: ' . $dealExternalId);
        }
    }

    public function cancelDealUser($dealUser) {
        $anulledCoupon = $this->anulledCouponsGateway->find($dealUser['id']);
        if (empty($anulledCoupon)) {
            $dealUser['motive'] = 'cancel deal user from process';
            $dealUser['anulled_email_sent'] = 0;
            $this->anulledCouponsGateway->insert($dealUser);
            $this->redemptionsGateway->deleteByDealUserId($dealUser['id']);
            $this->dealUsersGateway->delete($dealUser['id']);
            $transactions = $this->transactionsGateway->findByClassAndForeignId("'DealUser'", $dealUser['id']);
            foreach ($transactions as $transaction) {
                $this->transactionsGateway->delete($transaction['id']);
            }
            $this->log('[cancelDealExternal] dealUserId: ' . $dealUser['id'] . " cancelled");
        }
    }

    public function cancelDealExternal($dealExternalId, $cancelQuantity = NULL) {
        $dealExternal = $this->dealExternalsGateway->find($dealExternalId);
        if (!empty($dealExternal)) {
            if (is_null($cancelQuantity)) {
                $skipQuantity = 0;
            } elseif (intval($cancelQuantity) < 0) {
                $skipQuantity = $dealExternal['quantity'];
            } else {
                $skipQuantity = $dealExternal['quantity'] - $cancelQuantity;
            }
            $i = 1;
            if ($skipQuantity <= 0) {
                $this->dealExternalsGateway->updateColumn($dealExternalId, 'external_status', 'C');
                $this->dealExternalsGateway->updateColumn($dealExternalId, 'cancelled', date('Y-m-d H:i:s'));
            } else {
                $this->dealExternalsGateway->updateColumn($dealExternalId, 'quantity', $skipQuantity);
            }
            $this->log('[cancelDealExternal] dealExternalId: ' . $dealExternalId . " cancelled");
            $dealUsers = $this->dealUsersGateway->findByDealExternalId($dealExternalId);
            foreach ($dealUsers as $dealUser) {
                if ($i > $skipQuantity) {
                    $this->cancelDealUser($dealUser);
                }
                $i++;
            }
        } else {
            $this->log('[cancelDealExternal] unknown dealExternalId: ' . $dealExternalId);
            throw new Exception('[cancelDealExternal] unknown dealExternalId: ' . $dealExternalId);
        }
    }

}
