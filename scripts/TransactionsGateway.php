<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransactionsGateway
 *
 * @author msaladino
 */
class TransactionsGateway extends AbstractGateway {

    protected $table = "transactions";

    public function findByClassAndForeignId($class, $foreignId) {
        return $this->select("class = " . $class . " and foreign_id = " . $foreignId);
    }

    public function newAddFunds($userId, $amount, $loggedUserId) {
        return array('created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'user_id' => $userId,
            'foreign_id' => $loggedUserId,
            'class' => 'SecondUser',
            'transaction_type_id' => 1,
            'amount' => $amount,
            'description' => 'add funds to user wallet ('.$userId.')  by admin user ' . $loggedUserId,
            'payment_gateway_id' => '',
            'gateway_fees' => 0);
    }

    public function newDealUser($dealUser, $deal) {
        return array('created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'user_id' => $dealUser['user_id'],
            'foreign_id' => $dealUser['id'],
            'class' => 'DealUser',
            'transaction_type_id' => 2,
            'amount' => $deal['discounted_price'],
            'description' => 'create deal user from process',
            'payment_gateway_id' => '',
            'gateway_fees' => 0);
    }

}

?>
