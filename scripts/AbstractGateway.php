<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractGateway
 *
 * @author msaladino
 */
abstract class AbstractGateway {

    protected $mysqli;
    protected $table = false;
    protected $httpUser = false;

    public function __construct($mysqli) {
        // Properties cannot be declared abstract 
        // manual check
        if (empty($this->table)) {
            throw new Exception('Se debe implementar en la clase la propiedad table con el nombre de tabla correspondiente.');
        }
        $this->mysqli = $mysqli;
    }

    public function setHttpUser($userId) {
        $this->httpUser = $userId;
    }

    protected function uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    }

    public function toNull($value) {
        if (chop($value) != "") {
            if (strtolower(chop($value)) == "null") {
                return "NULL";
            } else {
                return "'" . $value . "'";
            }
        } else {
            return "NULL";
        }
    }

    public function sqlSelect($table, $conditions) {
        $sql = "select * from " . $table . " where " . $conditions;
        return $sql;
    }

    public function sqlUpdateColumn($table, $id, $column, $value) {
        $sql = "update " . $table . " set " . $column . " = " . $this->toNull($this->mysqli->real_escape_string($value)) . " where id = " . $id . " limit 1";
        return $sql;
    }

    public function sqlInsert($table, $row) {
        $keys = array_keys($row);
        $values = array_values($row);
        $array = array();
        foreach ($values as $value) {
            $array[] = $this->toNull($value);
        }
        $sql = "insert into " . $table . " (" . implode(",", $keys) . ") values (" . implode(",", $array) . ") ";
        return $sql;
    }

    public function sqlDelete($table, $id) {
        $sql = "delete from " . $table . " where id = " . $id . " limit 1";
        return $sql;
    }

    public function insert($row) {
        $sql = $this->sqlInsert($this->table, $row);
        $this->mysqli->query($sql);
        return $this->mysqli->insert_id;
    }

    public function updateColumn($id, $column, $value) {
        $sql = $this->sqlUpdateColumn($this->table, $id, $column, $value);
        $this->mysqli->query($sql);
        return $this->mysqli->affected_rows;
    }

    protected function select($conditions) {
        $rows = array();
        $sql = $this->sqlSelect($this->table, $conditions);
        $result = $this->mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
            $result->close();
        }
        return $rows;
    }

    public function find($id) {
        $row = array();
        $sql = $this->sqlSelect($this->table, "id = " . $id);
        $result = $this->mysqli->query($sql);
        if ($result) {
            $row = $result->fetch_assoc();
            $result->close();
        }
        return $row;
    }

    public function delete($id) {
        $sql = $this->sqlDelete($this->table, $id);
        $this->mysqli->query($sql);
        return $this->mysqli->affected_rows;
    }

}

?>
