<?php 
  // Tell me the root folder path.
  // You can also try this one
  // $HOME = $_SERVER["DOCUMENT_ROOT"];
  // Or this
  // dirname(__FILE__)
  $HOME = $_SERVER ["DOCUMENT_ROOT"];
  // That's all I need
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv = "Content-Type"  content = "text/html; charset=utf-8" />
    <title>UTF8 BOM FINDER and REMOVER</title>
    <style>
      body { font-size: 10px; font-family: Arial, Helvetica, sans-serif; background: #FFF; color: #000; }
      .FOUND { color: #F30; font-size: 14px; font-weight: bold; }
    </style>
  </head>
  <body>
<?php
  $BOMBED = array ();
  recursiveFolder ($HOME);
  echo '    <h2>These files had UTF8 BOM, but i cleaned them:</h2>' . "\n";
  echo '      <p class = "FOUND">'                                  . "\n";
  foreach ($BOMBED as $utf) {
    echo '        ' . $utf . '<br />' . "\n";
  }
  echo '      </p>' . "\n";

  // Recursive finder
  function recursiveFolder ($home) {
    global $BOMBED;

    if (PHP_OS == 'WINNT') {
      $ds = "\\";
    } else {
      $ds = "/";
    }

    $folder = dir ($home);

    $foundfolders = array ();
    while ($file = $folder->read ()) {
      if (substr ($file, 0, 1) != '.') {
        if (filetype ($home . $ds . $file) == "dir") {
          $foundfolders [count ($foundfolders)] = $home . $ds . $file;
        } else {
          $content = file_get_contents ($home . $ds . $file);
          if (searchBOM ($content)) {
            $BOMBED [] = $home . $ds . $file;
            // Remove first three chars from the file
            $content = substr ($content, 3);
            // Write to file 
            file_put_contents ($home . $ds . $file, $content);
          }
        }
      }
    }
    $folder->close ();

    if (count ($foundfolders) > 0) {
      foreach ($foundfolders as $folder) {
        recursiveFolder ($folder, $ds);
      }
    }
  }

  // Searching for BOM in files
  function searchBOM ($string) { 
    return (substr ($string, 0, 3) == pack ("CCC", 0xef, 0xbb, 0xbf));
  }
?>
  </body>
</html>