<?php

App::uses('AppController', 'Controller');

/**
 * DeliveriesController
 *
 */
class DeliveriesController extends AppController {

    var $uses = array(
        'Delivery'
    );

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array(
            'view',
            'add'
        );
        $this->layout = false;
        $this->response->type('json');
    }

    public function view($external_id, $send_date) {
        $delivery = $this->Delivery->findByExternalIdAndSendDate($external_id, $send_date);
        $response = array(
            'status' => true,
            'message' => $delivery
        );
        if (!$delivery) {
            $response = array(
                'status' => false,
                'message' => 'Invalid Delivery'
            );
        }
        $this->set(compact('response'));
        $this->render("json/view");
    }

    public function add() {
        if ($this->request->isPost()) {
            $response = array(
                'status' => false,
                'message' => 'The Delivery could not be saved. Please, try again.'
            );
            if (!empty($this->request->data)) {
                $this->Delivery->create();
                if ($this->Delivery->save($this->request->data)) {
                    $response = array(
                        'status' => true,
                        'message' => 'The Delivery has been saved'
                    );
                }
            }
            $this->set(compact('response'));
            $this->render("json/add");
        }
    }

}