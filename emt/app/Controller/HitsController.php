<?php

/**
 * Hits Controller
 *
 * @property Hit $Hit
 */
class HitsController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array('hit');
    }

    private function _getClientIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];
    }

    private function _getEventType($shortEventType) {
        
        //echo 'sd';
        switch ($shortEventType) {
            case 'v':
                return self::VISIT_EVENT_TYPE;
            case 'c':
                return self::CONVERSION_EVENT_TYPE;
            case 'm':
                return self::MAILVIEW_EVENT_TYPE;
            default:
                return null;
        }
    }

    public function _getSiteParam($request) {
        $siteExternalId = array_key_exists('site', $request) ? $request['site'] : null;

        if (empty($siteExternalId))
            throw new Exception('No se encuentra el parametro website');

        return $siteExternalId;
    }

    public function _getCampaignParam($request) {
        $campaignExternalId = array_key_exists('campaign', $request) ? $request['campaign'] : null;

        if (empty($campaignExternalId))
            throw new Exception('No se encuentra el parametro campaña');

        return $campaignExternalId;
    }

    public function _getGoalParam($request) {
        return array_key_exists('goal', $request) ? $request['goal'] : null;
    }

    public function _getUserParam($request) {
        return array_key_exists('user', $request) ? $request['user'] : null;
    }
    
    public function _getSegmentParam($request) {
    	return array_key_exists('segment', $request) ? $request['segment'] : null;
    }
    public function _getSegmentParamCC($request) {
    	return array_key_exists('emt_segment', $request) ? $request['emt_segment'] : null;
    }
    public function getSegmentParam($request) {
    	if($this->_getSegmentParam($request)!=null){
    		return $this->_getSegmentParam($request);
    	}else{
    		return $this->_getSegmentParamCC($request);
    	}
    }
    
    public function _getDateParam($request) {
        return array_key_exists('date', $request) ? $request['date'] : null;
    }

    public function _getUrlParam($request) {
        return array_key_exists('url', $request) ? urldecode($request['url']) : null;
    }

    public function _getEventParam($request) {
        $shortEventType = array_key_exists('event', $request) ? $request['event'] : null;

        if (empty($shortEventType))
            throw new Exception('No se encuentra el parametro evento');

        $eventType = $this->_getEventType($shortEventType);

        if (empty($eventType))
            throw new Exception('Evento inválido: ' . $shortEventType);

        return $eventType;
    }
    
    public function _getUserAgent(){
    	return $_SERVER['HTTP_USER_AGENT'];
    }

    // Servicio de trackeo de clicks.
    // Devuelve una imagen gif transaparente de 1 x 1, incluso si los parametros son incorrectos.
    // Este mismo servicio es solicitado desde distintos lugares, a saber:
    // - Es llamado por el script emt.js que se incluye en las paginas de apuertura y de conversion.
    // - Es el 'source' de las imagenes transparentes que se incluyen en los emails utilizadas para trackear aperturas de mail.
    public function hit() {

        try {
            $params = $this->request->query;
            $siteExternalId = $this->_getSiteParam($params);
            $campaignExternalId = $this->_getCampaignParam($params);
            $goalExternalId = $this->_getGoalParam($params);
            $userExternalId = $this->_getUserParam($params);
            $sendDate = $this->_getDateParam($params);
            $eventType = $this->_getEventParam($params);
            $url = $this->_getUrlParam($params);
            $segmentCode = $this->getSegmentParam($params);
            $userAgent = $this->_getUserAgent();
            
            $now = date('Y-m-d H:i:s', strtotime('now'));
            $ip = $this->_getClientIp();
            $goalId = null;

            //Obtengo el Id del WebSite
            $webSiteId = $this->requestAction(
                    array('controller' => 'Websites', 'action' => 'getByExternalId'),
                    array('pass' => array($siteExternalId)));

            //Obtengo el id de la campaña
            $campaignId = $this->requestAction(
                    array('controller' => 'Campaigns', 'action' => 'getOrCreateCampaign'),
                    array('pass' => array($webSiteId, $campaignExternalId, $sendDate))
            );

            //solo si existe el parametro Goal busco su Id de referencia.
            if (!empty($goalExternalId)) {
                //Obtengo el id de la oferta
                $goalId = $this->requestAction(
                        array('controller' => 'Goals', 'action' => 'getOrCreateGoal'),
                        array('pass' => array($campaignId, $goalExternalId)));
            }

            $this->Hit->create();
            $this->Hit->save(array(
                'opening_date' => $now,
                'ip' => $ip,
                'event_type' => $eventType,
                'user_id' => $userExternalId,
                'campaign_id' => $campaignId,
                'goal_id' => $goalId,
                'url_referral' => $url,
            	'segment_code' => $segmentCode,
            	'user_agent' => $userAgent
            ));
        }
        catch (Exception $exc) {
            //No hago nada, siempre devuelvo la imagen
            CakeLog::write('hit_errors', 'Ocurrio un error (HitController/hit): ' . $exc->getMessage());
        }

        $this->layout = false;
        $this->autoRender = false;
        $file = WWW_ROOT . 'img' . DS . 'pixel.gif';
        $this->response->disableCache();

        header('Content-Type: image/gif');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit(200);
    }

}

