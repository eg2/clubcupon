<?php

App::uses('AppController', 'Controller');

/**
 * Goals Controller
 *
 * @property Goal $Goal
 */
class GoalsController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array('getOrCreateGoal');
    }

    ///Chequea que el formato de la fecha y sus valores sean validos
    public function _checkDate($crudDate) {
        if (strlen($crudDate) != 12)
            return false;

        $dateTime = strtotime($crudDate);

        if (empty($dateTime))
            return false;

        return true;
    }

    public function _formatDate($crudDate) {
        if (!$this->_checkDate($crudDate))
            $date = strtotime('now');
        else
            $date = $dateTime = strtotime($crudDate);

        return date('Y-m-d H:i:s', $date);
    }

    public function getOrCreateGoal($campaignId = null, $goalExternalId = null) {

        if (empty($this->request->params['requested']))
            throw new Exception("GoalController::getOrCreateGoal: No se puede acceder a este servicio en forma directa.");

        return $this->Goal->getOrCreateGoal($campaignId, $goalExternalId);
    }

}
