<?php

App::uses('AppController', 'Controller');

/**
 * Bounces Controller
 *
 * @property Bounce $Bounce
 */
class UsersController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array());
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Usuario creado.', 'default', array(), 'auth');
                return $this->redirect(array(
                            'controller' => 'users',
                            'action' => 'index',
                            'admin' => true
                        ));
            }
        }
    }

    public function admin_edit($id = null) {
        $this->User->id = $id;

        if (!$this->User->exists()) {
            throw new NotFoundException('Usuario Invalido');
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Se ha modificado el usuario.');
                $this->redirect(array('action' => 'admin_index'));
            }
            else {
                $this->Session->setFlash('El usuario no pudo ser modificado. Intente nuevamente.');
            }
        }
        else {
            $this->request->data = $this->User->read(null, $id);
        }
    }

    public function admin_login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                //if ($this->Auth->user('is_admin')) {
                return $this->redirect($this->Auth->loginRedirect);
//                }
//                else {
//                    return $this->redirect('/');
//                }
            }
            else {
                $this->Session->setFlash('Usuario o password incorrecto', 'default', array(), 'auth');
            }
        }
    }

    public function admin_logout() {
        $this->redirect($this->Auth->logout());
    }

    public function admin_index() {
        $users = $this->paginate($this->User);
        $this->set(compact('users'));
    }

    public function admin_delete($id = null) {

        $this->User->id = $id;

        if (!$this->User->exists()) {
            $this->Session->setFlash('Usuario inválido');
        }

        if ($this->request->is('post')) {
            if ($this->User->delete()) {
                $this->Session->setFlash('Usuario eliminado.');   
            }
        }
        
        $this->redirect(array('action' => 'admin_index'));
    }

}
