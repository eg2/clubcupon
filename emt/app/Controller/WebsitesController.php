<?php

App::uses('AppController', 'Controller');

/**
 * Websites Controller
 *
 * @property Website $Website
 */
class WebsitesController extends AppController {

    function beforeFilter() {
        $this->Auth->allowedActions = array('getByExternalId');
        parent::beforeFilter();
    }

    public function admin_index() {
        $this->Website->recursive = 0;
        $this->set('websites', $this->paginate());
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Website->create();
            if ($this->Website->save($this->request->data)) {
                $this->Session->setFlash('Se ha creado el nuevo WebSite.');
                $this->redirect(array('action' => 'admin_index'));
            }
            else {
                $this->Session->setFlash('El nuevo WebSite no pudo ser dado de alta. Intente nuevamente.');
            }
        }
    }

    public function admin_edit($id = null) {
        $this->Website->id = $id;
        if (!$this->Website->exists()) {
            throw new NotFoundException(__('Invalid website'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Website->save($this->request->data)) {
                $this->Session->setFlash('Se ha modificado el WebSite correctamente.');
                $this->redirect(array('action' => 'admin_index'));
            }
            else {
                $this->Session->setFlash('El WebSite no pudo ser modificado. Intente nuevamente.');
            }
        }
        else {
            $this->request->data = $this->Website->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
//    public function admin_delete($id = null) {
//
//        $this->Website->id = $id;
//
//        if (!$this->Website->exists()) {
//            $this->Session->setFlash('Website inválido');
//            $this->redirect(array('action' => 'admin_index'));
//        }
//
//        if ($this->request->is('post')) {
//
//            if (!empty($this->request->data['Website']['timestamp'])) {
//                if ($this->Website->delete()) {
//                    $this->Session->setFlash('Website eliminado.');
//                    $this->redirect(array('action' => 'admin_index'));
//                }
//            }
//            $this->Session->setFlash('El Website no pudo ser eliminado.');
//            //$this->redirect(array('action' => 'index'));
//        }
//
//        $this->set('timestamp', time());
//    }

    public function getByExternalId($externalId) {

        if (empty($this->request->params['requested']))
            throw new Exception("Website::getByExternalId: No se puede acceder a este servicio en forma directa.");

        return $this->Website->getByExternalId($externalId);
    }

}
