<?php

App::uses('AppController', 'Controller');

/**
 * Campaigns Controller
 *
 * @property Campaign $Campaign
 */
class CampaignsController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array('getOrCreateCampaign');
    }

    public function getOrCreateCampaign($websiteId = null, $campaign = null, $date = null) {

        if (empty($this->request->params['requested']))
            throw new Exception("CampaignsController::getOrCreateCampaign: No se puede acceder a este servicio en forma directa.");

        return $this->Campaign->getOrCreateCampaign($websiteId, $campaign, $date);
    }

}