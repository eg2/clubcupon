<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    const VISIT_EVENT_TYPE = 'VISITA';
    const CONVERSION_EVENT_TYPE = 'CONVERSION';
    const MAILVIEW_EVENT_TYPE = 'VIOMAIL';

    //public $components = array('Auth', 'Session');
    public $components = array(
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'users',
                'action' => 'login',
                'admin' => true
            ),
            'authError' => 'Debe auntenticarse primero',
            'loginRedirect' => array(
                'controller' => 'websites',
                'action' => 'index',
                'admin' => true
            ),
            'authorize' => array('Controller'),
            'authenticate' => array(
                'all' => array(
//                    'scope' => array('User.is_active' => 1)
                ),
                'Form'),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login',
                'admin' => true
            ),
        ),
        'Session');

    public function beforeFilter() {
        $this->_setErrorLayout();
    }

    function _setErrorLayout() {
        if ($this->name == 'CakeError') {
            $this->layout = 'error';
        }
    }

    public function isAuthorized($user) {
        if ($this->params['prefix'] === 'admin' && empty($user)) {
//        if ($this->params['prefix'] === 'admin' && ($user['is_admin'] != 1)) {
            return false;
        }
        return true;
    }

}