<?php

App::uses('AppController', 'Controller');

/**
 * Bounces Controller
 *
 * @property Bounce $Bounce
 */
class BouncesController extends AppController {

    var $uses = array('Website', 'Bounce');
    
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array('jsonList');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Bounce->recursive = 0;
        $this->set('bounces', $this->paginate());
    }

    function _isDateString($dateString, &$errors) {
        if (strlen($dateString) != 8 || !checkdate(substr($dateString, 4, 2), substr($dateString, 6, 2), substr($dateString, 0, 4))) {
            $errors[] = "la fecha debe seguir el formato YYYYMMDD";
            return false;
        }
        return true;
    }

    /**
     * 
     * @return un json con una lista de mails con el status code 200 
     * un json con una lista de errores con el status code 400 si los parametros no son correctos 
     */
    public function jsonList() {
        try {
            $this->layout = null;
            $errors = array();
            $website = null;
            // Control de los queryParams
            if (array_key_exists('idapp', $this->request->query)) {
                $website = $this->Website->find('first', array(
                    'conditions' => array('Website.external_id' => $this->request->query['idapp'])));
                if (empty($website)) {
                    $errors[] = 'El campo idapp no es valido';
                }
            } else {
                $errors[] = 'Falta el campo idapp';
            }

            if (!array_key_exists('desde', $this->request->query) || !$this->_isDateString($this->request->query['desde'], $errors)) {
                $errors[] = 'Falta la fecha desde';
            }
            if (!array_key_exists('hasta', $this->request->query) || !$this->_isDateString($this->request->query['hasta'], $errors)) {
                $errors[] = 'Falta la fecha hasta';
            }
            if (!array_key_exists('cantidad', $this->request->query) || !is_numeric($this->request->query['cantidad'])) {
                $errors[] = 'Falta la cantidad de registros';
            }
            if (!array_key_exists('pagina', $this->request->query) || !is_numeric($this->request->query['pagina'])) {
                $errors[] = 'Falta el numero de Pagina';
            }
            // FIN Control de los queryParams
            if (!empty($errors)) {
                $result = json_encode($errors);
                $this->response->statusCode("400");
            } else {
                $start = date('Y-m-d H:i:s', strtotime($this->request->query['desde']));
                $end = date('Y-m-d H:i:s', strtotime($this->request->query['hasta']));
                $cantidad = $this->request->query['cantidad'];
                $page = $this->request->query['pagina'];
                $maxSoftBounce = empty($website['Website']['soft_bounce_max']) ? 5 : $website['Website']['soft_bounce_max'];

                $limiteInferior = $cantidad * ($page - 1);

                $db = $this->Bounce->getDataSource();
                $mailsBounced = $db->fetchAll(
                        "(SELECT email FROM bounces AS Bounce 
                          WHERE hard=1 AND website_id = :websiteId 
                          AND send_date >= :start 
                          AND send_date <= :end)
                         UNION 
                         (SELECT email 
                          FROM bounces AS Bounce1 
                          WHERE Bounce1.hard=0 
                          AND Bounce1.website_id = :websiteId 
                          AND Bounce1.send_date >= :start 
                          AND Bounce1.send_date <= :end
                          GROUP BY email
                          HAVING COUNT(*) >= :maxSoftBounce)
                LIMIT $limiteInferior, $cantidad;"
                        , array('websiteId' => $website['Website']['id'],
                    'start' => $start, 'end' => $end, 'maxSoftBounce' => $maxSoftBounce));
                $bounces = array();
                foreach ($mailsBounced as $mailBounced) {
                    $bounces[] = $mailBounced[0]['email'];
                }
                $result = json_encode($bounces);
            }
        } catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
            $errors[] = 'No funciona el servicio, por favor comunicarse con el administrador para que controle el log de la aplicacion';
            $result = json_encode($errors);
            $this->response->statusCode("400");
        }
        $this->response->type('json');
        $this->set(compact('result'));
        $this->render("json/index");
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->Bounce->id = $id;
        if (!$this->Bounce->exists()) {
            throw new NotFoundException(__('Invalid bounce'));
        }
        $this->set('bounce', $this->Bounce->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Bounce->create();
            if ($this->Bounce->save($this->request->data)) {
                $this->Session->setFlash(__('The bounce has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The bounce could not be saved. Please, try again.'));
            }
        }
        $websites = $this->Bounce->Website->find('list');
        $this->set(compact('websites'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->Bounce->id = $id;
        if (!$this->Bounce->exists()) {
            throw new NotFoundException(__('Invalid bounce'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Bounce->save($this->request->data)) {
                $this->Session->setFlash(__('The bounce has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The bounce could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Bounce->read(null, $id);
        }
        $websites = $this->Bounce->Website->find('list');
        $this->set(compact('websites'));
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Bounce->id = $id;
        if (!$this->Bounce->exists()) {
            throw new NotFoundException(__('Invalid bounce'));
        }
        if ($this->Bounce->delete()) {
            $this->Session->setFlash(__('Bounce deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Bounce was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
