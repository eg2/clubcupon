/**
Uso normal del script en una pagina que se intenta trackear:

1 - Crear el Objeto EmtTracker:
    var tracker = new EmtTracker();

2.1 - Setear parametros por url:
    tracker.setSiteByUrlParam('utm_src');
    tracker.setCampaignByUrlParam('utm_campaign');
    tracker.setGoalByUrlParam('utm_goal');
    tracker.setDateByUrlParam('utm_date');
    tracker.setUserByUrlParam('utm_user');

Cada una de estas funciones puede recibir un segundo parametro para indicar si el parametro que se estÃ¡ seteando
es indispensable para el request al server (el valor default es false), por ejemplo: 

    tracker.setUserByUrlParam('utm_user', true);

Si no se encuentra el parametro 'utm_user' en la url de la pagina que se esta cargando, no se hara el request al 
server de tracking. De lo contrario se hara un request al server con los parametros que el script haya podido recuperar.

2.2 - Setear parametros fijos:
    tracker.setSite('sitio_cc');
    tracker.setCampaign('campania_1234');
    tracker.setGoal('oferat_1234');
    tracker.setDate('20120304');
    tracker.setUser('user_1234');

3.1 - Trackear una visita a un pagina:
    tracker.trackPage();

3.1 - Trackear una Conversion
    tracker.trackConversion();
 
 
Para generar la version mini: http://jscompress.com/

 **/
function EmtParam(paramName, value) {
    this.paramName = paramName;
    this.value = value;
}

function EmtTracker(emtBaseURL) {
    
    var params = new Array(),
    siteParam = "site",
    campaignParam = "campaign",
    goalParam = "goal",
    userParam = "user",
    segmentParam = "segment",
    dateParam = "date",
    eventTypeParam = 'event',
    visitEventConst = 'v',
    conversionEventConst = 'c',
    trackerServiceName = 'hit'
    urlTracker = emtBaseURL + trackerServiceName,
    status = true,
    currentUrl = document.location.href;
    
    function setParam(param, val, requerido) {  
        
        val = val === undefined ? '' : val;
        val = val === null ? '' : val;

        if (requerido && (val == '' || val == null))
            status = false;
        else if (param != null)
            params[param] = val;
    }
    
    function setUrlParam(urlParam, param, requerido) {
        val = getParameter(currentUrl, urlParam);
        setParam(param, val, requerido);
    }
    
    function getParameter(url, name) {
        // scheme : // [username [: password] @] hostame [: port] [/ [path] [? query] [# fragment]]
        var e = new RegExp('^(?:https?|ftp)(?::/*(?:[^?]+)[?])([^#]+)'),
        matches = e.exec(url),
        f = new RegExp('(?:^|&)' + name + '=([^&]*)'),
        result = matches ? f.exec(matches[1]) : 0;

        return result ? window.decodeURIComponent(result[1]) : '';
    }
    
    /*
     * Envia un request de una imagen al server donde se aloja la EMT por medio de un GET.
     */
    function getImage(request) {
        
        if (status) {          
            var image = new Image(1, 1);
            image.onload = function () { };
            image.src = urlTracker + (urlTracker.indexOf('?') < 0 ? '?' : '&') + request;
        }
    }
    
    function getRequest() {
        request = '';

        for(var item in params)
            request += item + "=" + params[item] + "&";
            //request += params[item].paramName + "=" + params[item].value + "&";
            
        return request.substr(0, request.length -1);
    }
    
    function getDefaultBoolValue(boolValue) {
        return typeof boolValue !== 'undefined' ? boolValue : false;
    }
    
    return {
        setSiteByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, siteParam, getDefaultBoolValue(requerido));
        },
    
        setCampaignByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, campaignParam, getDefaultBoolValue(requerido));
        },
    
        setGoalByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, goalParam, getDefaultBoolValue(requerido));
        },

        setDateByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, dateParam, getDefaultBoolValue(requerido));
        },
    
        setUserByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, userParam, getDefaultBoolValue(requerido));
        },
        
        setSegmentByUrlParam : function(urlParam, requerido){
            setUrlParam(urlParam, segmentParam, getDefaultBoolValue(requerido));
        },

        setSite : function(value){
            setParam(siteParam, value);
        },
    
        setCampaign : function(value){
            setParam(campaignParam, value);
        },
    
        setGoal : function(value){
            setParam(goalParam, value);
        },

        setDate : function(value){
            setParam(dateParam, value);
        },
    
        setUser : function(value){
            setParam(userParam, value);
        },

        setSegment : function(value){
            setParam(segmentParam, value);
        },

        
        //Trackea apertura de paginas.
        trackPage : function() {
            setParam('url', window.encodeURIComponent(currentUrl), false);
            setParam(eventTypeParam, visitEventConst);
            getImage(getRequest());
        },
        
        //Trackea conversiones.
        trackConversion : function() {
            setParam('url', window.encodeURIComponent(currentUrl), false);
            setParam(eventTypeParam, conversionEventConst);
            getImage(getRequest());
        }
    }
    
    return true;
}
