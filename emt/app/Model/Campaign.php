<?php

App::uses('AppModel', 'Model');

/**
 * Campaign Model
 *
 * @property Website $Website
 * @property Click $Click
 * @property Goal $Goal
 */
class Campaign extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'external_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'website_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Website' => array(
            'className' => 'Website',
            'foreignKey' => 'website_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Hit' => array(
            'className' => 'Hit',
            'foreignKey' => 'campaign_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Goal' => array(
            'className' => 'Goal',
            'foreignKey' => 'campaign_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function _formatDate($crudDate) {
        $dateTime = strtotime($crudDate);
        
        if (empty($dateTime))
            $dateTime = strtotime('now');

        return date('Y-m-d', $dateTime);
    }

    public function getOrCreateCampaign($websiteId = null, $campaign = null, $date = null) {

        if (empty($websiteId) || !is_numeric($websiteId) || empty($campaign) || empty($date))
            throw new Exception('Campaign: Parametros incorrectos.');

        try {

            $campaignId = 0;
            $dateTime = $this->_formatDate($date);
            $cachedKey = $this->name . '_' . $websiteId . '_' . $campaign . '_' . $date;

            $cachedValue = Cache::read($cachedKey);

            if (!empty($cachedValue)) {
                $campaignId = $cachedValue;
            }
            else {
                $result = $this->find('first',
                        array(
                    'conditions' => array(
                        'website_id' => $websiteId,
                        'external_id' => $campaign,
                        'send_date' => $dateTime),
                    'recursive' => -1));

                if (empty($result)) {
                    //Si no existe creo la campaña on demand
                    $this->create();

                    $this->save(array(
                        'external_id' => $campaign,
                        'send_date' => $dateTime,
                        'website_id' => $websiteId
                    ));

                    $campaignId = $this->id;
                }
                else
                    $campaignId = $result['Campaign']['id'];

                //Si no estaba en cache porque expiro o porque es nuevo lo agrego
                Cache::write($cachedKey, $campaignId);
            }

            return $campaignId;
        }
        catch (Exception $exc) {
            throw $exc;
        }
        return null;
    }

}
