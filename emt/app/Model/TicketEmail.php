<?php

App::uses('ConnectionManager', 'Model');

class TicketEmail extends AppModel {

    // Important:
    public $useDbConfig = 'emailTicket';
    public $useTable = false;

    function setPopAccount($name, $account, $pass, $url) {
        ConnectionManager::create($name, array(
                    'datasource' => 'ImapSource',
                    'server' => $url,
                    'connect' => 'pop3/ssl/novalidate-cert',
                    'username' => $account,
                    'password' => $pass,
                    'port' => '995',
                    'ssl' => true,
                    'encoding' => 'UTF-8',
                    'error_handler' => '',
                    'auto_mark_as' => array(
                        'Deleted')));
        $this->useDbConfig = $name;

    }

    // Semi-important:
    // You want to use the datasource schema, and still be able to set
    // $useTable to false. So we override Cake's schema with that exception:
    function schema($field = false) {
        if (!is_array($this->_schema) || $field === true) {
            $db = & ConnectionManager::getDataSource($this->useDbConfig);
            $db->cacheSources = ($this->cacheSources && $db->cacheSources);
            $this->_schema = $db->describe($this, $field);
        }
        if (is_string($field)) {
            if (isset($this->_schema[$field])) {
                return $this->_schema[$field];
            } else {
                return null;
            }
        }
        return $this->_schema;
    }

}