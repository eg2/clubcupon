<?php

App::uses('AppModel', 'Model');

/**
 * Website Model
 *
 * @property Campaign $Campaign
 */
class Website extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe seleccionar un nombre para el WebSite.',
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 30),
            //'message' => 'Your custom message here',
            ),
        ),
        'external_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => array('Debe seleccionar un código de aplicación externo.'),
            ),
            'maxlength' => array(
                'rule' => array('maxlength', 10),
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'El codigo de aplicacion externo no está disponible.'
            )
        ),
        'imap_account' => array(
            'maxlength' => array(
                'rule' => array('maxlength', 50),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'imap_password' => array(
            'maxlength' => array(
                'rule' => array('maxlength', 30),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'imap_url' => array(
            'maxlength' => array(
                'rule' => array('maxlength', 50),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'soft_bounce_max' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Debe ingresar un entero válido',
                'allowEmpty' => false,
                'required' => true,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'website_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function getByExternalId($externalId) {
        $webSiteId = 0;
        $cachedKey = $this->name . '_' . $externalId;

        $cachedValue = Cache::read($cachedKey);

        if (!empty($cachedValue)) {
            $webSiteId = $cachedValue;
        }
        else {
           
            $webSite = $this->find('first',
                    array(
                'fields' => array('id'),
                'conditions' => array('external_id' => $externalId),
                'recursive' => -1));
                        
            if (empty($webSite))
                throw new Exception('Website Invalido: ' . $externalId);
            else
                $webSiteId = $webSite['Website']['id'];

            //Si no estaba en cache porque expiro o porque es nuevo lo agrego
            Cache::write($cachedKey, $webSiteId);
        }

        return $webSiteId;
    }

}
