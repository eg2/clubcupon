<?php

App::uses('AppModel', 'Model');

/**
 * Goal Model
 *
 * @property Campaign $Campaign
 * @property Click $Click
 */
class Goal extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'external_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'campaign_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Hit' => array(
            'className' => 'Hit',
            'foreignKey' => 'goal_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function getOrCreateGoal($campaignId = null, $goalExternalId = null) {

        if (empty($campaignId) || !is_numeric($campaignId) || empty($goalExternalId))
            throw new Exception('Goal: Parametros incorrectos');

        try {
            $goalId = 0;
            $cachedKey = $this->name . '_' . $campaignId . '_' . $goalExternalId;
            
            $cachedValue = Cache::read($cachedKey);

            if (!empty($cachedValue)) {
                $goalId = $cachedValue;
            }
            else {
                $result = $this->find('first',
                        array(
                    'fields' => array('id'),
                    'conditions' => array(
                        'external_id' => $goalExternalId,
                        'campaign_id' => $campaignId
                    ),
                    'recursive' => -1));

                if (empty($result)) {
                    //Si no existe creo la campaña on demand
                    $this->create();

                    $this->save(array(
                        'campaign_id' => $campaignId,
                        'external_id' => $goalExternalId
                    ));

                    $goalId = $this->id;
                }
                else
                    $goalId = $result['Goal']['id'];
                
                //Si no estaba en cache porque expiro o porque es nuevo lo agrego
                Cache::write($cachedKey, $goalId);
            }

            return $goalId;
        }
        catch (Exception $exc) {
            throw $exc;
        }
    }

}
