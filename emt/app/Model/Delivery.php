<?php

App::uses('AppModel', 'Model');

/**
 * Bounce Model
 *
 */
class Delivery extends AppModel {

    public function findByExternalIdAndSendDate($external_id, $send_date) {
        return $this->find('all', array(
                    'conditions' => array(
                        'Delivery.external_id' => $external_id,
                        'Delivery.send_date' => $send_date
                    )
                ));
    }

}