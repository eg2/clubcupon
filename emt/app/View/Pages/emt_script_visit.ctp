<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml">
    <head>
        <title>EMT - Test del script de visita </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        
        <script type="text/javascript">
            var emtBaseURL = (("https:" == document.location.protocol) ? "https://localhost/emt/" : "http://localhost/emt/");
            document.write(unescape("%3Cscript src='" + emtBaseURL + "emt.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var tracker = new EmtTracker(emtBaseURL);
                tracker.setSiteByUrlParam('utm_source');
                tracker.setCampaignByUrlParam('utm_campaign');
                tracker.setGoalByUrlParam('utm_goal');
                tracker.setDateByUrlParam('utm_date');
                tracker.setUserByUrlParam('utm_user');
                tracker.trackPage();
            } catch( err ) {}
        </script>
        
        
    </head>
    <body>
        <center><h2>Test del script de visita</h2></center>
    </body>
</html>
