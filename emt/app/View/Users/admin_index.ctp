<div class="websites index">
    <h2>Usuarios</h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('username'); ?></th>
            <th class="actions">Acciones</th>
        </tr>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                <td class="actions">
                    <?php
                    echo $this->Html->link('Editar',
                            array(
                        'controller' => 'users',
                        'action' => 'edit',
                        'admin' => true,
                        $user['User']['id']
                    ));
                    ?>
                    <?php
                    echo $this->Form->postLink('Borrar',
                            array('controller' => 'users',
                        'action' => 'delete',
                        'admin' => true,
                        
                        $user['User']['id']),array(), 'Está seguro de borrar el usuario?'
                    );
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => 'Página {:page} de {:pages}'
        ));
        ?>	</p>

    <div class="paging">
        <?php
        echo $this->Paginator->prev('< Anterior', array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next('Siguiente >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3>Acciones</h3>
    <ul>
        <li><?php
        echo $this->Html->link('WebSites',
                array('controller' => 'websites', 'action' => 'index',
            'admin' => true,));
        ?></li>
        <li><?php
            echo $this->Html->link('Nuevo Usuario',
                    array('controller' => 'users', 'action' => 'add',
                'admin' => true,));
        ?></li>
    </ul>
</div>
