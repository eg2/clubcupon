<div class="websites form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>Nuevo Usuario</legend>
        <?php
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('password_confirm', array('type' => 'password'));
        ?>
    </fieldset>
    <?php echo $this->Form->end('Guardar'); ?>
</div>
<div class="actions">
    <h3>Acciones</h3>
    <ul>
        <li><?php echo $this->Html->link('WebSites',
            array('controller' => 'websites', 'action' => 'admin_index')); ?></li>
        <li><?php echo $this->Html->link('Usuarios',
                    array('action' => 'admin_index')); ?></li>
    </ul>
</div>
