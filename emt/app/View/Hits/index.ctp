<div class="hits index">
	<h2><?php echo __('Hits');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('opening_date');?></th>
			<th><?php echo $this->Paginator->sort('ip');?></th>
			<th><?php echo $this->Paginator->sort('event_type');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('goal_id');?></th>
			<th><?php echo $this->Paginator->sort('campaign_id');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('updated');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($hits as $hit): ?>
	<tr>
		<td><?php echo h($hit['Hit']['id']); ?>&nbsp;</td>
		<td><?php echo h($hit['Hit']['opening_date']); ?>&nbsp;</td>
		<td><?php echo h($hit['Hit']['ip']); ?>&nbsp;</td>
		<td><?php echo h($hit['Hit']['event_type']); ?>&nbsp;</td>
		<td><?php echo h($hit['Hit']['user_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($hit['Goal']['id'], array('controller' => 'goals', 'action' => 'view', $hit['Goal']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($hit['Campaign']['id'], array('controller' => 'campaigns', 'action' => 'view', $hit['Campaign']['id'])); ?>
		</td>
		<td><?php echo h($hit['Hit']['created']); ?>&nbsp;</td>
		<td><?php echo h($hit['Hit']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $hit['Hit']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $hit['Hit']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $hit['Hit']['id']), null, __('Are you sure you want to delete # %s?', $hit['Hit']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Hit'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Goals'), array('controller' => 'goals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Goal'), array('controller' => 'goals', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Campaigns'), array('controller' => 'campaigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Campaign'), array('controller' => 'campaigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
