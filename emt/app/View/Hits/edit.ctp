<div class="hits form">
<?php echo $this->Form->create('Hit');?>
	<fieldset>
		<legend><?php echo __('Edit Hit'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('opening_date');
		echo $this->Form->input('ip');
		echo $this->Form->input('event_type');
		echo $this->Form->input('user_id');
		echo $this->Form->input('goal_id');
		echo $this->Form->input('campaign_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Hit.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Hit.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Hits'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Goals'), array('controller' => 'goals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Goal'), array('controller' => 'goals', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Campaigns'), array('controller' => 'campaigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Campaign'), array('controller' => 'campaigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
