<div class="websites index">
    <h2><?php echo __('Websites'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('name',
        'Nombre'); ?></th>
            <th><?php echo $this->Paginator->sort('external_id', 'Clave Externa'); ?></th>
            <th><?php echo $this->Paginator->sort('imap_account',
                        'Cuenta Imap'); ?></th>
            <th><?php echo $this->Paginator->sort('imap_url', 'Url Imap'); ?></th>
            <th><?php echo $this->Paginator->sort('soft_bounce_max',
                        'Max Soft Bounce'); ?></th>
            <th><?php echo $this->Paginator->sort('created',
                        'Creado'); ?></th>
            <th><?php echo $this->Paginator->sort('updated', 'Modificado'); ?></th>
            <th class="actions">Acciones</th>
        </tr>
<?php foreach ($websites as $website): ?>
            <tr>
                <td><?php echo h($website['Website']['id']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['name']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['external_id']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['imap_account']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['imap_url']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['soft_bounce_max']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['created']); ?>&nbsp;</td>
                <td><?php echo h($website['Website']['updated']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link('Editar',
                            array('action' => 'admin_edit', $website['Website']['id'])); ?>
                    <?php
                    //echo $this->Html->link('Borrar', array('action' => 'admin_delete', $website['Website']['id']));
                    ?>
                </td>
            </tr>
<?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => 'Página {:page} de {:pages}'
        ));
        ?>	</p>

    <div class="paging">
        <?php
        echo $this->Paginator->prev('< Anterior', array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next('Siguiente >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3>Acciones</h3>
    <ul>
        <li><?php echo $this->Html->link('Nuevo WebSite',
                array('action' => 'admin_add')); ?></li>
        <li><?php echo $this->Html->link('Usuarios',
                array('controller' => 'users', 'action' => 'admin_index')); ?></li>
    </ul>
</div>
