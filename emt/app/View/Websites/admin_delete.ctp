<div class="websites form">
    <?php echo $this->Form->create('Website'); ?>
    <fieldset>
        <h3>¿Está seguro de borrar el WebSite?</h3>
        <b>Si borra el Website se perderá toda la informacion recolectada en el tiempo en que se usó.</b>
        <?php
        echo $this->Form->hidden('website_id');
        echo $this->Form->hidden('timestamp', array('value' => $timestamp));
        ?>
    </fieldset>
    <?php echo $this->Form->submit('Borrar todo',
            array('onclick' => 'return confirm("Confirma el borrado del sitio?");')); ?>
</div>
<div class="actions">
    <h3>Acciones</h3>
    <ul>
        <li><?php echo $this->Html->link('WebSites',
            array('controller' => 'websites', 'action' => 'admin_index')); ?></li>
        <li><?php echo $this->Html->link('Usuarios',
                    array('action' => 'admin_index')); ?></li>
    </ul>
</div>
