<div class="websites form">
    <?php echo $this->Form->create('Website'); ?>
    <fieldset>
        <legend>Nuevo Website</legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->input('external_id', array('type' => 'text', 'maxlength'=>10));
        echo $this->Form->input('imap_account');
        echo $this->Form->input('imap_password', array('type'=>'password'));
        echo $this->Form->input('imap_url');
        echo $this->Form->input('soft_bounce_max');
        ?>
    </fieldset>
    <?php echo $this->Form->end('Guardar'); ?>
</div>
<div class="actions">
    <h3>Acciones</h3>
    <ul>
        <li><?php echo $this->Html->link('WebSites',
            array('controller' => 'websites', 'action' => 'admin_index')); ?></li>
        <li><?php echo $this->Html->link('Usuarios',
                    array('controller' => 'users', 'action' => 'admin_index')); ?></li>
    </ul>
</div>
