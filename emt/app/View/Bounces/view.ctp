<div class="bounces view">
<h2><?php  echo __('Bounce');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Send Date'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['send_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Opening Date'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['opening_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hard'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['hard']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Website'); ?></dt>
		<dd>
			<?php echo $this->Html->link($bounce['Website']['name'], array('controller' => 'websites', 'action' => 'view', $bounce['Website']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Update'); ?></dt>
		<dd>
			<?php echo h($bounce['Bounce']['update']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bounce'), array('action' => 'edit', $bounce['Bounce']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bounce'), array('action' => 'delete', $bounce['Bounce']['id']), null, __('Are you sure you want to delete # %s?', $bounce['Bounce']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bounces'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bounce'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Websites'), array('controller' => 'websites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Website'), array('controller' => 'websites', 'action' => 'add')); ?> </li>
	</ul>
</div>
