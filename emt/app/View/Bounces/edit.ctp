<div class="bounces form">
<?php echo $this->Form->create('Bounce');?>
	<fieldset>
		<legend><?php echo __('Edit Bounce'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('email');
		echo $this->Form->input('send_date');
		echo $this->Form->input('opening_date');
		echo $this->Form->input('body');
		echo $this->Form->input('hard');
		echo $this->Form->input('website_id');
		echo $this->Form->input('update');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Bounce.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Bounce.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bounces'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Websites'), array('controller' => 'websites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Website'), array('controller' => 'websites', 'action' => 'add')); ?> </li>
	</ul>
</div>
