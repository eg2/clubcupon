<div class="bounces index">
	<h2><?php echo __('Bounces');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('email');?></th>
			<th><?php echo $this->Paginator->sort('send_date');?></th>
			<th><?php echo $this->Paginator->sort('opening_date');?></th>
			<th><?php echo $this->Paginator->sort('body');?></th>
			<th><?php echo $this->Paginator->sort('hard');?></th>
			<th><?php echo $this->Paginator->sort('website_id');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('update');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($bounces as $bounce): ?>
	<tr>
		<td><?php echo h($bounce['Bounce']['id']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['email']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['send_date']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['opening_date']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['body']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['hard']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bounce['Website']['name'], array('controller' => 'websites', 'action' => 'view', $bounce['Website']['id'])); ?>
		</td>
		<td><?php echo h($bounce['Bounce']['created']); ?>&nbsp;</td>
		<td><?php echo h($bounce['Bounce']['update']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $bounce['Bounce']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $bounce['Bounce']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $bounce['Bounce']['id']), null, __('Are you sure you want to delete # %s?', $bounce['Bounce']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bounce'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Websites'), array('controller' => 'websites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Website'), array('controller' => 'websites', 'action' => 'add')); ?> </li>
	</ul>
</div>
