<?php
/**
 * BounceFixture
 *
 */
class BounceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'email' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'send_date' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'opening_date' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'body' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 1000, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'hard' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'website_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'update' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'website_id' => array('column' => 'website_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'email' => 'Lorem ipsum dolor sit amet',
			'send_date' => '2012-05-30 22:07:45',
			'opening_date' => '2012-05-30 22:07:45',
			'body' => 'Lorem ipsum dolor sit amet',
			'hard' => 1,
			'website_id' => 1,
			'created' => '2012-05-30 22:07:45',
			'update' => '2012-05-30 22:07:45'
		),
	);
}
