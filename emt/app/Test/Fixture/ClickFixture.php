<?php
/**
 * ClickFixture
 *
 */
class ClickFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
		'opening_date' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'ip' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'event_type' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'user_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'goal_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'campaign_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'goal_id' => array('column' => 'goal_id', 'unique' => 0), 'campaign_id' => array('column' => 'campaign_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'opening_date' => '2012-05-30 17:15:01',
			'ip' => 'Lorem ipsum d',
			'event_type' => 'Lorem ipsum dolor ',
			'user_id' => 'Lorem ipsum dolor ',
			'goal_id' => 1,
			'campaign_id' => 1,
			'created' => '2012-05-30 17:15:01',
			'updated' => '2012-05-30 17:15:01'
		),
	);
}
