<?php
App::uses('Click', 'Model');

/**
 * Click Test Case
 *
 */
class ClickTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.click', 'app.goal', 'app.campaign', 'app.website');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Click = ClassRegistry::init('Click');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Click);

		parent::tearDown();
	}

}
