<?php
App::uses('Hit', 'Model');

/**
 * Hit Test Case
 *
 */
class HitTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.hit', 'app.goal', 'app.campaign', 'app.website', 'app.click');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Hit = ClassRegistry::init('Hit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Hit);

		parent::tearDown();
	}

}
