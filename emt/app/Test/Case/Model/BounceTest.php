<?php
App::uses('Bounce', 'Model');

/**
 * Bounce Test Case
 *
 */
class BounceTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.bounce', 'app.website', 'app.campaign', 'app.click', 'app.goal');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bounce = ClassRegistry::init('Bounce');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bounce);

		parent::tearDown();
	}

}
