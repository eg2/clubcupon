<?php
App::uses('Website', 'Model');

/**
 * Website Test Case
 *
 */
class WebsiteTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.website', 'app.campaign');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Website = ClassRegistry::init('Website');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Website);

		parent::tearDown();
	}

}
