<?php
App::uses('GoalsController', 'Controller');

/**
 * TestGoalsController *
 */
class TestGoalsController extends GoalsController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * GoalsController Test Case
 *
 */
class GoalsControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.goal', 'app.campaign', 'app.website', 'app.hit');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Goals = new TestGoalsController();
		$this->Goals->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Goals);

		parent::tearDown();
	}

}
