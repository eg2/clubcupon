<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('AppController', 'Controller');

/**
 * como correrlo: 
 * * cd C:\wamp\www\emtclubcupon\app
 * * Console\cake bounce 
 * 
 */
class BounceShell extends AppShell {
	var $uses = array('Bounce', 'Website', 'Campaign');
    var $hardBounceText = array();
    
    private $errorParameters;
    
    function main() {
        //Se verifican los mails a todas las casillas registradas en la base.
    	$websites = $this->Website->find('all');
        foreach ($websites as $website) {
        CakeLog::write('debug', "Bajando los rebotes del sitio: ".$website['Website']['name']);
        try {
            if (empty($website['Website']['imap_account']) || empty($website['Website']['imap_password']) || empty($website['Website']['imap_url'])) {
                 	$descripcionError = 'Falta un parametro en la configuracion del website '. $website['Website']['name'] . " : ".
                        	(empty($website['Website']['imap_account'])?" IMAP_ACCOUNT ":"").
                        	(empty($website['Website']['imap_url'])?" IMAP_URL ":"").
                        	(empty($website['Website']['imap_password'])?" IMAP_PASSWORD ":"");
                    	CakeLog::write('debug', "No se pudo realizar la bajada de los rebotes del sitio: ".$website['Website']['name']);                    
                    	CakeLog::write('debug', $descripcionError);                    
                    	trigger_error($descripcionError, E_USER_NOTICE);
                    	continue;
       		}
            $hostname = '{'. $website["Website"]["imap_url"] . ':995/pop3/ssl/novalidate-cert}INBOX';
            $username = $website['Website']['imap_account'];
            $password = $website['Website']['imap_password'];
                
            /* try to connect */
            $inbox = imap_open($hostname, $username, $password, NULL, 1);
            if(!$inbox){
                    trigger_error("Imposible de conectartse al site " . $website['Website']['name'] . "\n" . imap_last_error(), E_USER_NOTICE);
                    continue;
            }
            CakeLog::write('debug', "Me bajo los ids ");
            $emails = imap_search($inbox, 'ALL', SE_UID);
            $emails = imap_search($inbox, 'ALL');
			CakeLog::write('debug', $emails." \n");
            CakeLog::write('debug', "Termino de bajarme los  ids ");

            if ($emails) {
                    foreach ($emails as $email_number) {
                        /* get information specific to this email */
                    	CakeLog::write('debug', "Bajando un mail ".$email_number);
                        $overview = imap_fetch_overview($inbox, $email_number, FT_UID);
                        $body = imap_fetchbody($inbox, $email_number, "", FT_UID);
                        //CakeLog::write('debug', "Obteniendo el body ".$body);
                        if(count($overview) == 0){
                            $header = imap_fetchheader($inbox, $email_number, FT_UID);
                        }
                        //imap_delete($inbox, $email_number, FT_UID);
                        imap_setflag_full($inbox,$email_number, '\\Deleted',ST_UID); 
                        imap_expunge($inbox);
                        
                        if(count($overview) == 0){
                            CakeLog::write('error_headers', "El mail con el encabezado siguiente no se pudo parsear: " . $header);
                            if(!empty($body)){
                                CakeLog::write('error_headers', "El body tiene " . strlen($body) . " caracteres");
                            }
                        }else{
                            $to = $overview[0]->to;
                            $posTag = strrpos($to, "<");
                            if($posTag){
                                $to = substr($to, $posTag + 1, strrpos($to, ">") - $posTag - 1);
                            }
                            //1 - recupero los parametros
			    			echo "\nParseando el return-path: ".$to."\n";
                            $toEnDosPartes = explode("@", $to);
                            $parametros = explode("+", $toEnDosPartes[0]);
                            $campaignExternalId = null;
                            $aplicacionExternalId = null;
			    			$segmentCode = null;
                            // Si vino con el formato de VERP que definimos, tiene que tener 4 parametros
			    			$this->errorParameters = false;
                            if (sizeof($parametros) == 4) {
									echo "\nentre por =4 \n";
                                	$campaignExternalId = $parametros[0];
                                	$sendDate = $parametros[1];
                                	$aplicacionExternalId = $parametros[2];
                                	$mailRebotado = str_replace("=", "@", $parametros[3]);
                             }else if (sizeof($parametros) == 5){
									echo "\nentre por =5 ". json_encode($parametros)."\n";
                            		$campaignExternalId = $parametros[0];
                            		$sendDate = $parametros[1];
                            		$aplicacionExternalId = $parametros[2];
                            		$segmentCode = $parametros[3];
                            		$mailRebotado = str_replace("=", "@", $parametros[4]);
                            }else if (sizeof($parametros) >= 5) {
									echo "\nentre por >5 ". json_encode($parametros) ."\n";
                                	$campaignExternalId = $parametros[0];
                                	$sendDate = $parametros[1];
                                	$aplicacionExternalId = $parametros[2];
                                	if(strpos($parametros[3], "=")) {
                                		// es el email y no tiene segmento
										echo "\nes el mail pero no tiene segmento". json_encode($parametros) ."\n";
                                		$segmentCode = null;
                                		$mailRebotado = $parametros[3];
                                		$firtsIndex = 4;
                                	} else {
										echo "\nelse general ". json_encode($parametros);
                                		$segmentCode = $parametros[3];
                                		$mailRebotado = $parametros[4];
                                		$firtsIndex = 5;
                                	}

                                	for ($index = $firstIndex; $index < count($parametros); $index++) {
                                    	$mailRebotado .= "+".$parametros[$index];
                                	}
                                	$mailRebotado = str_replace("=", "@", $mailRebotado);        
                            } else {
                                	trigger_error("Los Parametros que vinieron en la dirección VERP no corresponden a un formato conocido $to  ", E_NOTICE);
                                	$mailRebotado = $to;
                                	$this->errorParameters = true;
                            }
                            //2 - busco el website
                            if(!$this->errorParameters){
                            		$websiteDelRebote = $this->Website->findByExternalId($aplicacionExternalId);
                            		$websiteId = "";
                            		if ($websiteDelRebote != null) {
                            				$websiteId = $websiteDelRebote['Website']['id'];
                            		} else {
                            				trigger_error("No existe el website correspondiente a la aplicacionId: " . $aplicacionExternalId . "(quizas el verp está mal configurado)", E_NOTICE);
                            				continue;
                            		}
                            		$campaignId = "";
                            		if (!empty($campaignExternalId)) {
                            				$campaignId = $this->Campaign->getOrCreateCampaign($websiteId, $campaignExternalId, $sendDate);
                            		}
                            	
                            		if (empty($sendDate)) {
                            				$sendDate = date('Y-m-d', strtotime("now"));
                            		}
                            		CakeLog::write('debug', "chequeando si es hard ");
                            		//4 - busco si se trata de un hard bounce
                            		$hardbounce = $this->_isHardBounce($body);
                            		CakeLog::write('debug', "email ".$mailRebotado);
                            		//            CakeLog::write('debug', "body ".$body);
                            		CakeLog::write('debug', "send_date ".$sendDate);
                            		CakeLog::write('debug', "hard  ".$hardbounce);
                            		CakeLog::write('debug', "website_id ".$website['Website']['id']);
                            		CakeLog::write('debug', "campaign_id  ".$campaignId);
                            		//5 - busco si se trata de un hard bounce
                            		imap_expunge($inbox);
                            		$this->Bounce->create();
                            		echo "\nGuardando Bounce ".json_encode(
                            				array(
                            						'email' => $mailRebotado,
                            						'send_date' => $sendDate,
                            						'opening_date' => date('Y-m-d h:i:s', strtotime("now")),
                            						'hard' => $hardbounce ? 1 : 0,
                            						'website_id' => $website['Website']['id'],
                            						'campaign_id' => $campaignId,
                            						'segment_code'	=> $segmentCode
                            				));
                            		$this->Bounce->save(array(
                            				'email' => $mailRebotado,
                            				'body' => $body,
                            				'send_date' => $sendDate,
                            				'opening_date' => date('Y-m-d H:i:s', strtotime("now")),
                            				'hard' => $hardbounce ? 1 : 0,
                            				'website_id' => $website['Website']['id'],
                            				'campaign_id' => $campaignId,
                            				'segment_code'	=> $segmentCode
                            		));
                            }else{
                            	CakeLog::write('debug', "Los Parametros que vinieron en la dirección VERP no corresponden a un formato conocido");
                            }
                        }
                    }//endforeach
                }
            } catch (Exception $e) {
                	trigger_error("Se produjo un error con el site " . $website['Website']['name'] . "\n" . $e->getMessage(), E_ERROR);
					continue;
            }
			echo "\nSe finalizo la bajada de los rebotes del sitio\n";
            //CakeLog::write('debug', "Se finalizo la bajada de los rebotes del sitio : ".$website['Website']['name']);
        }
    }
    
    function _isHardBounce($body) {
        //Si no están en memoria los textos de hardbounce, se los carga.
        if (empty($this->hardBounceText)) {
            $path = App::path('Console');
            $lines = file($path[0] . "/Command/bounce.properties");
            foreach ($lines as $line) {
                $this->hardBounceText[] = trim($line);
            }
        }
        //Se verifica si se encuentra una palabra clave
        foreach ($this->hardBounceText as $text) {
            if (strpos($body, $text) !== false) {
                return 'true';
            }
        }
        return false;
    }

}

?>