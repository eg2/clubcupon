SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `emtdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bounces`
--

DROP TABLE IF EXISTS `bounces`;
CREATE TABLE IF NOT EXISTS `bounces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `send_date` datetime NOT NULL,
  `opening_date` datetime NOT NULL,
  `body` varchar(1000) DEFAULT NULL,
  `hard` tinyint(4) NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `website_id` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(20) NOT NULL,
  `send_date` date NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `web_sites_fk` (`website_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22023 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goals`
--

DROP TABLE IF EXISTS `goals`;
CREATE TABLE IF NOT EXISTS `goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(20) NOT NULL,
  `campaign_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `campaign_fk` (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96293 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hits`
--

DROP TABLE IF EXISTS `hits`;
CREATE TABLE IF NOT EXISTS `hits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `opening_date` datetime NOT NULL,
  `ip` varchar(15) NOT NULL,
  `event_type` varchar(20) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `goal_id` int(11) unsigned DEFAULT NULL,
  `campaign_id` int(11) unsigned NOT NULL,
  `url_referral` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `goal_id` (`goal_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=180423 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `websites`
--

DROP TABLE IF EXISTS `websites`;
CREATE TABLE IF NOT EXISTS `websites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `imap_account` varchar(50) DEFAULT NULL,
  `imap_password` varchar(30) DEFAULT NULL,
  `imap_url` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `soft_bounce_max` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `external_id` (`external_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `bounces`
--
ALTER TABLE `bounces`
  ADD CONSTRAINT `bounces_ibfk_1` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`);

--
-- Filtros para la tabla `campaigns`
--
ALTER TABLE `campaigns`
  ADD CONSTRAINT `campaigns_ibfk_1` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`);

--
-- Filtros para la tabla `hits`
--
ALTER TABLE `hits`
  ADD CONSTRAINT `hits_ibfk_1` FOREIGN KEY (`goal_id`) REFERENCES `goals` (`id`),
  ADD CONSTRAINT `hits_ibfk_2` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);
