DROP TABLE IF EXISTS `deliveries`;
CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `send_date` datetime NOT NULL,
  `sent_quantity` int(11) unsigned NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  `external_id` varchar(20) NOT NULL,
  `created` datetime,
  PRIMARY KEY (`id`),
  KEY `website_id` (`website_id`),
  KEY `campaign_id` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;