//Importamos la librería del componente Alert
import mx.controls.Alert;

//seteamos los estilos genericos
_global.style.setStyle("themeColor", "haloOrange") 

//evitamos problemas de encoding
System.useCodepage = true;

initForm();

function initForm()
{
	for (var i = 1; i<10; i++) {
		this["msj"+i]._visible = false;
	};
};

email_txt.onSetFocus = function()
{
	email_txt.text = ""
}

function validaForma()
{
	if (email_txt.text == "" || !esCorreo(email_txt.text))
	{
		msj5._visible = true;
		email_txt.setFocus();
	}
	else
	{
		enviarCorreo();
	};
};

function esCorreo(email:String):Boolean
{
	var res:Boolean = false;
	if (email.indexOf("@") > 0 && email.indexOf("@") == email.lastIndexOf("@"))
	{
		if (email.lastIndexOf(".") > email.indexOf("@") && email.lastIndexOf(".") < email.length - 1)
		{
			res = true;
		};

	};
	return res;
};

enviar_btn.onRelease = function()
{
	validaForma();
};

function enviarCorreo()
{
	//recuperamos las vars...
	_root.subscription__email    = email_txt.text;
	_root.subscription__from     = _root.from;
	_root.subscription__city_id  = _root.city_id;
	_root.subscription__campaign = _root.campaign;

	getURL(_root.url, "_self", "POST");
	enviar_btn.enabled = false;
	enviar_lv.email = email_txt.text;
	
};
