-- https://artear.atlassian.net/browse/CC-5534
ALTER TABLE beacon_device_deals ADD column since TIMESTAMP NOT NULL DEFAULT '2015-01-01 00:00:00';
ALTER TABLE beacon_device_deals ADD column until TIMESTAMP NOT NULL DEFAULT '2015-01-01 24:00:00';
	