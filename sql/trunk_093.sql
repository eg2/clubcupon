-- Agregar pregunta "en que otros sitios estas registrado?"
insert into `questions`
(`created`, `modified`, `name`,           `content`,                                      `multiple`)
values
(now(),     now(),      'otrossitios',    'Otros sitios web en los que estoy registrado', true      );

-- Agregar respuestas a la pregunta "en que otros sitios estas registrado"
insert into `answers`
(`created`, `modified`, `content`,                `question_id`,                                         `creator`)
values
(now(),     now(),      'Facebook',               (select id from questions where name = 'otrossitios'), 'backend'),
(now(),     now(),      'Twitter',                (select id from questions where name = 'otrossitios'), 'backend'),
(now(),     now(),      'Sitios de Cupones',      (select id from questions where name = 'otrossitios'), 'backend'),
(now(),     now(),      'Agrupadores de cupones', (select id from questions where name = 'otrossitios'), 'backend');

-- Agregar la user_answer para la pregunta "otrossitios" en base a los checks anteriores
insert into user_answers (created, modified, user_id, answer_id, question_id)
select now(), now(), p.user_id, a.id, q.id
from user_profiles p, answers a, questions q
where 0 = 0
and q.name = 'otrossitios'
and ((has_facebook_registration        = 1 and a.content = 'Facebook'              ) or
     (has_twitter_registration         = 1 and a.content = 'Twitter'               ) or
     (has_coupon_sites_registration    = 1 and a.content = 'Sitios de cupones'     ) or
     (has_coupon_grouping_registration = 1 and a.content = 'Agrupadores de cupones'));

-- Eliminar checks obsoletos
alter table user_profiles
drop column has_facebook_registration,
drop column has_twitter_registration,
drop column has_coupon_sites_registration,
drop column has_coupon_grouping_registration;