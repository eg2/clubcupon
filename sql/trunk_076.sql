-- Agrega el campo "ocultar el precio de esta oferta" a las ofertas
alter table `deals` add `hide_price` TINYINT(1) default FALSE after `accepts_points`;