-- -------------------------------------------------------------------------------------------------
-- -- Acciones (los eventos que generan puntos) ----------------------------------------------------
-- -------------------------------------------------------------------------------------------------

create table `actions`
  (
    `id`          BIGINT(20)        not null  unique  AUTO_INCREMENT,
    `created`     DATETIME      default null,
    `modified`    DATETIME      default null,
    `name`        VARCHAR(64)       not null,
    `description` VARCHAR(255)      not null,
    --
    primary key (`id`)
  ) ENGINE = INNODB  DEFAULT CHARSET = utf8  COLLATE = utf8_unicode_ci  COMMENT = 'Actions';
--
create index `action__id` on `actions`(`id`);
--
insert into `actions` (created, modified, name, description) values ('0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Débito', 'Esta acción representa una extracción de puntos, por ende, ha de ser siempre NEGATIVA la cantidad asociada de los mismos.');
insert into `actions` (created, modified, name, description) values ('0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Antigüedad', 'Esta acción representa la asignación de puntos por antigüedad.');


-- -------------------------------------------------------------------------------------------------
-- -- Puntos de acciones (cuantos puntos esta generando un evento dado) ----------------------------
-- -------------------------------------------------------------------------------------------------

create table `action_points`
  (
    `id`        BIGINT(20)      not null  unique  AUTO_INCREMENT,
    `created`   DATETIME    default null,
    `action_id` BIGINT(20)      not null,
    `points`    INT(11)         not null,
    --
    primary key (`id`),
    --
    foreign key (`action_id`) references `actions`(`id`)
  ) ENGINE = INNODB  DEFAULT CHARSET = utf8  COLLATE = utf8_unicode_ci  COMMENT = 'Action points';
--
create index `action_points__id`        on `action_points`(`id`);
create index `action_points__action_id` on `action_points`(`action_id`);
create index `action_points__created`   on `action_points`(`created`);
--
insert into `action_points` (created, action_id, points) values ('0000-00-00 00:00:00', 1, 0);
insert into `action_points` (created, action_id, points) values ('0000-00-00 00:00:00', 2, 0);


-- -------------------------------------------------------------------------------------------------
-- -- Relacion entre un usuario y los puntos que le son atribuidos ---------------------------------
-- -------------------------------------------------------------------------------------------------

create table `action_point_users`
  (
    `id`               BIGINT(20)      not null  unique  AUTO_INCREMENT,
    `created`          DATETIME    default null,
    `modified`         DATETIME    default null,
    `action_point_id`  BIGINT(20)      not null,
    `user_id`          BIGINT(20)      not null,
    `deal_external_id` BIGINT(20)  default null  unique,
    `points`           INT(11)         not null,
    `status`           INT(2)          not null,
    --
    primary key (`id`),
    --
    foreign key (`action_point_id`) references `action_points`(`id`),
    foreign key (`user_id`)         references `users`(`id`)
  ) ENGINE = INNODB  DEFAULT CHARSET = utf8  COLLATE = utf8_unicode_ci  COMMENT = 'Action points <--> Users';
--
create index `action_point_users__id`               on `action_point_users`(`id`);
create index `action_point_users__action_point_id`  on `action_point_users`(`action_point_id`);
create index `action_point_users__user_id`          on `action_point_users`(`user_id`);
create index `action_point_users__deal_external_id` on `action_point_users`(`deal_external_id`);
create index `action_point_users__status`           on `action_point_users`(`status`);


-- -------------------------------------------------------------------------------------------------
-- -- Razon de conversion entre puntos y pesos -----------------------------------------------------
-- -------------------------------------------------------------------------------------------------

create table `rates`
  (
    `id`      BIGINT(20)      not null  unique  AUTO_INCREMENT,
    `created` DATETIME    default null,
    `rate`    FLOAT           not null,
    --
    primary key (`id`)
  ) ENGINE = INNODB  DEFAULT CHARSET = utf8  COLLATE = utf8_unicode_ci  COMMENT = 'Action points <--> Users';
--
create index `rates__id`       on `rates`(`id`);
create index `rates__created`  on `rates`(`created`);
--
insert into `rates` (created, rate) values ('0000-00-00 00:00:00', 1.0);


-- -------------------------------------------------------------------------------------------------
-- -- Cache de puntos disponibles y booleano para ver si ya se compro o no -------------------------
-- -------------------------------------------------------------------------------------------------

alter table `users` add `available_points` INT(11)     not null  after `available_balance_amount`;
alter table `users` add `has_buyed`        TINYINT(1)  not null  after `available_points`;


-- -------------------------------------------------------------------------------------------------
-- -- Booleano que determina si la oferta acepta puntos o no ---------------------------------------
-- -------------------------------------------------------------------------------------------------

alter table `deals` add `accepts_points` TINYINT(1)  not null  after `name`;


-- -------------------------------------------------------------------------------------------------
-- -- Hacer definitivos los cambios restantes ------------------------------------------------------
-- -------------------------------------------------------------------------------------------------
commit;
