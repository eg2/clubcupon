-- se saca un "not null" de la tabla "companies" y se actualizan los campos en 0 a null.
alter table companies modify cheque_noorden int(11);
update companies set cheque_noorden = null where cheque_noorden = 0;