-- Se crea variable de configuración desde admin para setear el mínimo.
INSERT INTO `settings`
(id,setting_category_id,name,value,`description`,`type`,`label`,`order`)
VALUES (NULL,11,'deal.global_minimal_amount_financial',
                '0.00',
                'El monto global predeterminado por el cual se permite el financiamento de una transacción',
                'text',
                'Monto Global a Financiar',9);

-- Se crea la configuración por oferta.
ALTER TABLE  `deals` ADD  `minimal_amount_financial` decimal(10,2) DEFAULT NULL AFTER  `discounted_price`;
