-- Agrega el campo "esta acción asigna puntos (esta activa) o no"

alter table `actions` add `is_active` TINYINT(1) default true after `modified`;