-- la movida de who did it de deals.
alter table `deals` add `created_by`  BIGINT(20) not null;
alter table `deals` add `modified_by` BIGINT(20) not null;
alter table `deals` add `created_ip`  VARCHAR(15) not null;
alter table `deals` add `modified_ip` VARCHAR(15) not null;