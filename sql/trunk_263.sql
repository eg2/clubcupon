alter table payment_option_deals
add column  payment_plan_id bigint(20) unsigned;

alter table payment_option_deals add constraint 
FOREIGN KEY payment_option_deals_payment_plans_fk (payment_plan_id)
REFERENCES payment_plans (id);

alter table deal_externals
add column  payment_plan_option_id bigint(20) unsigned;

alter table deal_externals add constraint 
FOREIGN KEY deal_externals_payment_plan_options_fk (payment_plan_option_id)
REFERENCES payment_plan_options (id);

alter table payment_plan_options 
add column payment_installments bigint(20) unsigned;