-- tracker.int.clarin.com/browse/CC-3387

CREATE OR REPLACE VIEW accounting_grouped_by_calendars AS
SELECT  deal.company_id AS deal_company_id,
        IFNULL(bill.accounting_calendar_id, '0') AS accounting_calendar_id,
        AVG(deal.discounted_price) AS deal_average_price,
        SUM(deal.deal_external_count) AS deal_external_total,
        SUM(deal.deal_external_count * deal.discounted_price) AS deal_total_raised,
        AVG(deal.commission_percentage) AS deal_average_commision,
        0  AS deal_net_commission_amount,
        SUM( (deal.commission_percentage / 100) * (deal.discounted_price * deal.deal_external_count) ) AS deal_gross_commission_amount,
        AVG (deal.downpayment_percentage) AS deal_downpayment_average,
        0 AS deal_transfer_amount,
        MIN(deal.start_date) AS deal_min_start_date,
        DATE_ADD(deal.start_date, INTERVAL MIN(deal.payment_term) DAY) AS deal_min_due_date,
        DATE_ADD(deal.start_date, INTERVAL MAX(deal.payment_term) DAY) AS deal_max_due_date
FROM accounting_items AS liquidate
    LEFT OUTER JOIN accounting_items AS bill ON liquidate.accounting_item_id = bill.id
    INNER JOIN deals AS deal ON liquidate.deal_id = deal.id
    INNER JOIN companies AS company ON liquidate.model_id = company.id
WHERE liquidate.accounting_type IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED') 
    AND liquidate.model = 'Company' 
    AND company.validated = 1
    AND ((bill.accounting_type IN ('BILL_BY_SOLD_FOR_COMPANY', 'BILL_BY_REDEEMED_FOR_COMPANY')) OR (bill.accounting_type IS NULL))
    AND ((bill.model = 'Company') OR (bill.model IS NULL))
GROUP BY liquidate.accounting_calendar_id, IFNULL(liquidate.accounting_calendar_id, '0');

-- 

CREATE OR REPLACE VIEW accounting_grouped_by_calendar_items AS
SELECT deal.company_id as deal_company_id,
       IFNULL(bill.accounting_calendar_id, '0') AS accounting_calendar_id,
       liquidate.id AS liquidate_id, 
       deal.start_date AS deal_start_date,
       IFNULL(deal.campaign_code, '0') AS deal_campaign_code, 
       deal.id AS deal_id, 
       CONCAT(deal.name, IF(deal.descriptive_text IS NOT NULL, ' - ', ''), IFNULL(deal.descriptive_text,'')) AS deal_name, 
       city.name AS city_name, 
       liquidate.total_quantity AS liquidate_total_quantity, 
       IFNULL(bill.total_amount,0) AS bill_total_amount, 
       liquidate.total_amount AS liquidate_total_amount, 
       IFNULL(bill.bac_sent, liquidate.created) AS liquidate_bac_sent, 
       if(deal.pay_by_redeemed = 0, if(DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY) >= now(),DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY), DATE_ADD(liquidate.created, INTERVAL 7 DAY)), DATE_ADD(liquidate.created, INTERVAL deal.payment_term DAY)) max_payment_date
FROM accounting_items AS liquidate
LEFT OUTER JOIN accounting_items AS bill ON liquidate.accounting_item_id = bill.id
INNER JOIN deals AS deal ON liquidate.deal_id = deal.id
INNER JOIN cities AS city ON deal.city_id = city.id
WHERE liquidate.accounting_type IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED') 
AND liquidate.model = 'Company' 
AND ((bill.accounting_type IN ('BILL_BY_SOLD_FOR_COMPANY', 'BILL_BY_REDEEMED_FOR_COMPANY')) OR (bill.accounting_type IS NULL))
AND ((bill.model = 'Company') OR (bill.model IS NULL));