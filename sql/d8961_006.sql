DELETE FROM search_links where city_id = 42550;
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Gastronomía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad de Buenos Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo');
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Productos', '/productos');
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Belleza', '/especial-belleza');
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Indumentaria', '/especial-indumentaria');
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Turismo', '/turismo');
INSERT INTO `search_links` (`city_id`, `label`, `url`) VALUES (42550, 'Especiales', '/lo-mejor-de-la-semana');

