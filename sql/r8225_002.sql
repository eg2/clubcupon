-- http://tracker.int.clarin.com/browse/CC-4113
alter table `deal_externals` add `shipping_address_id` BIGINT(20) UNSIGNED NULL;
alter table `deal_externals` add foreign key (`shipping_address_id`) references `shipping_addresses`(`id`);