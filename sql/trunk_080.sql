ALTER TABLE deal_users
 DROP FOREIGN KEY fk_deal_users_deals1,
 DROP FOREIGN KEY fk_deal_users_payment_types1,
 DROP FOREIGN KEY fk_deal_users_users1,
 CHANGE gift_dob gift_dob DATETIME;
ALTER TABLE deal_users
 ADD CONSTRAINT fk_deal_users_deals1 FOREIGN KEY (deal_id) REFERENCES deals (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
 ADD CONSTRAINT fk_deal_users_payment_types1 FOREIGN KEY (payment_type_id) REFERENCES payment_types (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
 ADD CONSTRAINT fk_deal_users_users1 FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anulled_coupons CHANGE gift_dob gift_dob DATETIME;