ALTER TABLE cities ADD COLUMN is_solapa TINYINT(1) DEFAULT 0;

UPDATE cities SET is_solapa = 1
WHERE slug IN ('turismo', 'especial-belleza', 'festival-gastronomico', 'productos');