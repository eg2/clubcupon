CREATE OR REPLACE VIEW `deal_external_cupon_summary_view` AS
SELECT `deal`.`id` AS `deal_id`,
        SUM(1) AS `coupon_count`,
        de.quantity - SUM(1)  AS `coupon_returned_count_cc`,
        de.quantity - SUM(1)  AS `coupon_returned_count_ccnow`,
        SUM(IF((`du`.`is_used` = 1), 1, 0)) AS `coupon_redemed_count`,
        SUM(IF((`deal`.`pay_by_redeemed` = 1 AND `du`.`is_used` = 1 AND `du`.`is_billed` = 1) OR (`deal`.`pay_by_redeemed` = 0 AND `du`.`is_billed` = 1), 1, 0)) AS `coupon_billed`,
        SUM(IF(((UNIX_TIMESTAMP(`deal`.`coupon_expiry_date`) < UNIX_TIMESTAMP(NOW())) ),1,0)) AS `coupon_expired_count`
FROM (`deals` `deal` LEFT JOIN `deal_users` `du` ON(`deal`.`id` = `du`.`deal_id` 
                                                    AND du.deleted = 0 
                                                    AND (deal.is_now = 0 || du.is_returned = 0)) 
                     LEFT JOIN deal_externals de ON((du.deal_external_id = de.id)))
GROUP BY deal.id, de.id;


CREATE OR REPLACE VIEW `deal_view` AS
SELECT `deal`.`id` AS `deal_id`,`deal`.`company_id` AS `company_id`,`deal`.`start_date` AS `deal_start_date`,`deal`.`end_date` AS `deal_end_date`,`deal`.`name` AS `deal_name`,`deal`.`deal_status_id` AS `deal_status_id`,`deal`.`original_price` AS `deal_original_price`,`deal`.`discount_percentage` AS `deal_discount_percentage`,`deal`.`discounted_price` AS `deal_discounted_price`,`deal`.`commission_percentage` AS `deal_commission_percentage`,`deal`.`is_now` AS `deal_is_now`,`deal`.`coupon_start_date` AS `coupon_start_date`,`deal`.`coupon_expiry_date` AS `coupon_expiry_date`,`deal`.`subtitle` AS `deal_subtitle`,`deal`.`parent_deal_id` AS `deal_parent_deal_id`,`city`.`id` AS `city_id`,`city`.`name` AS `city_name`,`deal`.`pay_by_redeemed` AS `deal_is_pay_by_redeemed`,`deal`.`is_end_user` AS `deal_is_precompra`,`schedule`.`id` AS `deal_schedule_id`,
sum(`cupons`.`coupon_redemed_count`) AS `coupon_redemed_count`, sum(`cupons`.`coupon_expired_count`) AS `coupon_expired_count`, sum(`cupons`.`coupon_returned_count_cc`) AS `coupon_returned_count_cc`, sum(`cupons`.`coupon_returned_count_ccnow`) AS `coupon_returned_count_ccnow`, sum(`cupons`.`coupon_count`) AS `coupon_count`, sum(`cupons`.`coupon_billed`) AS `coupon_billed`
FROM ((`cities` `city`
JOIN `deal_external_cupon_summary_view` `cupons`)
JOIN (`deals` `deal`
LEFT JOIN `scheduled_deals` `schedule` ON((`schedule`.`deal_id` = `deal`.`id`))))
WHERE ((`deal`.`city_id` = `city`.`id`) AND (`cupons`.`deal_id` = `deal`.`id`))
group by deal.id;
