-- http://tracker.int.clarin.com/browse/CC-3440
CREATE TABLE `newsletter_subjects` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `deleted` TINYINT(1) NOT NULL DEFAULT '0',
    `city_id` BIGINT(20) UNSIGNED NOT NULL,
    `scheduled` DATETIME NOT NULL,
    `male_text` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
    `female_text` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
    `unisex_text` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', 
PRIMARY KEY (`id`), 
INDEX `city_id` (`city_id`), 
INDEX `scheduled` (`scheduled`), 
CONSTRAINT `newsletter_subjects_cities_fk` 
    FOREIGN KEY (`city_id`) 
    REFERENCES `cities` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;