--
-- Estructura de tabla para la tabla `emails_files_process`
--
CREATE TABLE IF NOT EXISTS `emails_files_process` (
 `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
 `created` DATETIME NOT NULL,
 `modified` DATETIME NOT NULL,
 `is_stopped` TINYINT(4) NOT NULL,
 `city_id` INT(10) UNSIGNED DEFAULT NULL,
 `page_number` INT(10) UNSIGNED DEFAULT NULL,
 `server_instance` INT(10) UNSIGNED NOT NULL, PRIMARY KEY (`id`)
) ENGINE= INnoDB  DEFAULT CHARSET=latin1;