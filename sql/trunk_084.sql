-- payment_settings
delete from `payment_settings` where `name` in ('Por MercadoPago', 'Todos');
insert into `payment_settings` (`id`, `name`) values
(7, 'Por MercadoPago'),
(6, 'Todos'          );

-- payment_types
insert into `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`)
values (60, NOW(), NOW(), 'MercadoPago', 5, 'dim', 1);

-- payment_options
-- MercadoPago
insert into `payment_options`(`payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`) values
( 1, 7, 60, 'Hasta en 12 cuotas con American Express',                  'American Express por MercadoPago'),
( 6, 7, 60, 'Hasta en 12 cuotas con Mastercard',                        'Mastercard por MercadoPago'      ),
( 9, 7, 60, 'Hasta en 12 cuotas con Visa',                              'Visa por MercadoPago'            ),
(12, 7, 60, 'En 1 pago por PagoFacil, Rapipago y otros medios de pago', 'Otros medios por MercadoPago'    ),
(12, 7, 60, 'Hasta en 12 cuotas con otras tarjetas',                    'Otras tarjetas por MercadoPago'  );
-- Todos
insert into `payment_options`(`payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`) values
( 1, 6,   20, 'American Express',                                         'American Express por NPS'        ),
( 1, 6,   60, 'Hasta en 12 cuotas con American Express',                  'American Express por MercadoPago'),
( 3, 6,   23, 'Cabal',                                                    'Cabal por NPS'                   ),
( 5, 6,   28, 'Italcred',                                                 'Italcred por NPS'                ),
( 6, 6,   22, 'Mastercard',                                               'Mastercard por NPS'              ),
( 6, 6,   60, 'Hasta en 12 cuotas con Mastercard',                        'Mastercard por MercadoPago'      ),
( 7, 6,   24, 'Naranja',                                                  'Naranja por NPS'                 ),
( 9, 6,   25, 'Visa',                                                     'Visa por NPS'                    ),
( 9, 6,   60, 'Hasta en 12 cuotas con Visa',                              'Visa por MercadoPago'            ),
(12, 6,   60, 'En 1 pago por PagoFacil, Rapipago y otros medios de pago', 'Otros medios por MercadoPago'    ),
(12, 6,   60, 'Hasta en 12 cuotas con otras tarjetas',                    'Otras tarjetas por MercadoPago'  ),
(12, 6, 5000, 'Monedero',                                                 'Monedero'                        );

--
commit;