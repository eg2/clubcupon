
ALTER TABLE redemptions ADD discounted_price decimal(10,2) default 0.00 AFTER deal_user_id;