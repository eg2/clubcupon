-- http://jira.int.clarin.com/browse/CC-5362

UPDATE search_links SET description = 'Kits escolares, Útiles, Electrónica, Indumentaria' WHERE label LIKE '%Vuelta al cole%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Restaurantes, Regalos, Indumentaria, Gastronomía' WHERE label LIKE '%San Valentín%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Lavado, Servicios, Limpieza, Seguridad, Confort' WHERE label LIKE '%Especial Autos%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Tecnología, Informática, Hogar' WHERE label LIKE '%Electro%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Objetos, Decoración, Diseño' WHERE label LIKE '%Muebles%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Tecnología, Informática, Hogar' WHERE label LIKE '%Electro y Tecno%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Paquetes, Hoteles, Pasajes, Estadías, Días de Campo, Entretenimientos' WHERE label LIKE '%Viajes%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Entradas, Espectáculos, Shows' WHERE label LIKE '%Cine%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Restaurante, Delivery, Catering' WHERE label LIKE '%Sushi%' AND search_link_type_id = 2;
UPDATE search_links SET description = '' WHERE label LIKE '%Año Nuevo%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Electro, Servicios, Decoración, Informática, Productos' WHERE label LIKE '%Especial Hogar%' AND search_link_type_id = 2;
UPDATE search_links SET description = '' WHERE label LIKE '%Club Cupón%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Revelado, Servicios, Foto Regalos, Productos' WHERE label LIKE '%Fotografía%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Vinos, Bebidas Alcoholicas, Productos' WHERE label LIKE '%Vinos y Más%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Restautantes, Pizzas, Empanadas, Helados, Delivery, Sushi, Reposteria, Bebidas' WHERE label LIKE '%Gastronomía%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Peluqueria, Depilación, Estética, Tratamiento Corporal, Salud' WHERE label LIKE '%Salud y Belleza%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Viajes, Vacaciones, Productos' WHERE label LIKE '%Especial Verano%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Cine, Teatro, Shows, Recitales' WHERE label LIKE '%Entretenimiento%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Electro, Hogar, Informática, Indumentaria' WHERE label LIKE '%Productos%' AND search_link_type_id = 2;
UPDATE search_links SET description = 'Paquetes, Hoteles, Pasajes, Estadías, Días de Campo, Entretenimientos' WHERE label LIKE '%Vacaciones%' AND search_link_type_id = 2;

UPDATE search_link_logos SET file_name = 'ccya_search_link.png' WHERE file_name = 'ccya_search_link.jpg';