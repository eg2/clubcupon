INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.listdeals_big_thumb.width', '217', 'text', 'List deals big thumb', '15');
INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.listdeals_big_thumb.height', '138', 'text', '', '15');

INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.maindeal_big_thumb.width', '460', 'text', 'Main deal big thumb', '15');
INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.maindeal_big_thumb.height', '312', 'text', '', '15');

INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.searchedlist_big_thumb.width', '95', 'text', 'Searched list big thumb', '15');
INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.searchedlist_big_thumb.height', '61', 'text', '', '15');

INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.outstandingourbenefits_big_thumb.width', '460', 'text', 'Outstanding our benefits big thumb', '15');
INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.outstandingourbenefits_big_thumb.height', '326', 'text', '', '15');

INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.searchdeals_big_thumb.width', '338', 'text', 'Search deals big thumb', '15');
INSERT INTO `settings` (`setting_category_id`, `name`, `value`, `type`, `label`, `order`) VALUES ('7', 'thumb_size.searchdeals_big_thumb.height', '216', 'text', '', '15');

