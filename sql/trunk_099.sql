-- Alta de tabla para formatos de banners

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `clubcupon`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner_types`
--

CREATE TABLE IF NOT EXISTS `banner_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(4) NOT NULL,
  `height` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `banner_types`
--

INSERT INTO `banner_types` (`id`, `label`, `width`, `height`) VALUES
(1, 'Front', 231, 100),
(2, 'Newsletter', 500, 50);
