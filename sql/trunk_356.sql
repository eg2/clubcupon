-- http://jira.int.clarin.com/browse/CC-5040
INSERT INTO `payment_methods`
	(`id`, `name`, `logo`)
VALUES
	(20, 'Nativa', 'nativa.gif');

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(108, 20, 11, 83, 'Nativa', 'Nativa por Mercado Pago Checkout', 0);

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(109, 4, 11, 83, 'Diners', 'Diners por Mercado Pago Checkout', 0);

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(110, 20, 13, 85, 'Nativa', 'Nativa por Mercado Pago Checkout', 0);

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(111, 4, 13, 85, 'Diners', 'Diners por Mercado Pago Checkout', 0);

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(112, 20, 14, 89, 'Nativa', 'Nativa por Mercado Pago Checkout', 0);

INSERT INTO `payment_options`
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
	(113, 4, 14, 89, 'Diners', 'Diners por Mercado Pago Checkout', 0);