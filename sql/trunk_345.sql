
-- LIST DESTACADOS - Ofertas Nacionales (ciudad ofertas-nacionales)

	-- Viajes y Escapadas (Especial : turismo)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--ofertas~nacionales-page--1-query--oooo-facet--city_name_no_group.Ofertas+Nacionales', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Productos con envío (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos con envío', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--envio+a+domicilio', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Celulares (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Celulares', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--celulares', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Sommier (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Sommier', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--sommier', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--cine', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Hoteles (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Hoteles', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--hoteles', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- TV Led (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'TV Led', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--TV+Led', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

-- TAB ESPECIALES - Ofertas Nacionales (ciudad ofertas-nacionales)

	-- Viajes y Escapadas (Especial : turismo)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--ofertas~nacionales-page--1-query--oooo-facet--city_name_no_group.Ofertas+Nacionales', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Productos con envío (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos con envío', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--envio+a+domicilio', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Celulares (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Celulares', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--celulares', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Sommier (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Sommier', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--sommier', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--cine', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- Hoteles (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Hoteles', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--hoteles', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);

	-- TV Led (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'TV Led', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ofertas+Nacionales&scV=-city--ofertas~nacionales-page--1-query--TV+Led', 
		(SELECT id FROM cities WHERE slug = 'ofertas-nacionales')
	);


