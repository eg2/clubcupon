-- Agrega el valor por defecto 0 a deal_external_count:
ALTER TABLE `deals` ALTER `deal_external_count` SET DEFAULT 0;

-- Inicializa los campos modificados en el paso anterior
UPDATE `deals` SET `deal_external_count` = (SELECT SUM(`deal_externals`.`quantity`) FROM `deal_externals` WHERE `deal_externals`.`deal_id` = `deals`.`id`);