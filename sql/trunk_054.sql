CREATE TABLE  `payment_methods` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 255 ) NOT NULL ,
`logo` VARCHAR( 255 ) NOT NULL
) ENGINE = INNODB AUTO_INCREMENT=13 CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE  `payment_options` (
`id` BIGINT UNSIGNED NOT NULL ,
`payment_method_id` BIGINT UNSIGNED NOT NULL ,
`payment_setting_id` BIGINT UNSIGNED NOT NULL ,
`payment_type_id` BIGINT UNSIGNED NOT NULL ,
`name` VARCHAR( 255 ) NOT NULL ,
`description` TEXT NOT NULL ,
PRIMARY KEY (  `id` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE  `payment_settings` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 255 ) NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE  `payment_options` DROP PRIMARY KEY ,
ADD PRIMARY KEY (  `id` ) ;

ALTER TABLE  `payment_settings` DROP PRIMARY KEY ,
ADD PRIMARY KEY (  `id` ) ;

ALTER TABLE  `payment_options` CHANGE  `id`  `id` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE  `payment_settings` CHANGE  `id`  `id` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT;


CREATE TABLE `payment_option_deals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `deal_id` bigint(20) unsigned NOT NULL,
  `payment_option_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE  `payment_types` ADD  `gateway_id` BIGINT UNSIGNED NOT NULL AFTER  `name`;
ALTER TABLE  `payment_types` ADD  `gateway_key` ENUM(  'nps',  'dim',  'wallet' ) NOT NULL AFTER  `gateway_id`;
--
update `payment_types` set gateway_id = 2, gateway_key = 'nps' where id in(20,25,22,23,28,24,5001,5002);
update `payment_types` set gateway_id = 1, gateway_key = 'dim' where id in(1,2);
update `payment_types` set gateway_id = 5000 , gateway_key = 'wallet' where id in(5000) limit 1;
--
-- payment_methods
INSERT INTO payment_methods (id, name, logo) VALUES (1,'American Express','american_express.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (2,'Argencard','argencard.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (3,'Cabal','cabal.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (4,'Diners','diners.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (5,'Italcred','italcred.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (6,'Mastercard','master_card.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (7,'Tarjeta Naranja','naranja.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (8,'Tarjeta Shopping','shopping.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (9,'Visa','visa.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (10,'Monedero','sin_logo.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (11,'Puntos','sin_logo.gif');
INSERT INTO payment_methods (id, name, logo) VALUES (12,'Sin logo','sin_logo.gif');

-- payment_settings
INSERT INTO payment_settings (id, name) VALUES (1, 'Por NPS');
INSERT INTO payment_settings (id, name) VALUES (2, 'Por MercadoPago');
INSERT INTO payment_settings (id, name) VALUES (3, 'Por Puntos');
INSERT INTO payment_settings (id, name) VALUES (4, 'Por Monedero');

-- payment_options
-- por NPS
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (1, 1, 20, 'American Express', 'American Express por NPS');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (9, 1, 25, 'Visa', 'Visa por NPS');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (6, 1, 22, 'Mastercard', 'Mastercard por NPS');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (3, 1, 23, 'Cabal', 'Cabal por NPS');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (5, 1, 28, 'Italcred', 'Italcred por NPS');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (7, 1, 24, 'Naranja', 'Naranja por NPS');
-- por MercadoPago
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (9, 2, 7, 'Hasta en 12 cuotas con Visa', 'Visa por MercadoPago');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (1, 2, 7, 'Hasta en 12 cuotas con American Express', 'American Express por MercadoPago');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (6, 2, 7, 'Hasta en 12 cuotas con Mastercard', 'Mastercard por MercadoPago');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (12, 2, 7, 'Hasta en 12 cuotas con otras tarjetas', 'Otras tarjetas por MercadoPago');
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (12, 2, 8, 'En 1 pago por PagoFacil, Rapipago y otros medios de pago', 'Otros medios por MercadoPago'); 
-- por Monedero
INSERT INTO payment_options
    (payment_method_id, payment_setting_id, payment_type_id, name, description)
VALUES
    (12, 4, 5000, 'Monedero', 'Monedero'); 
--
COMMIT;

