-- http://tracker.int.clarin.com/browse/CC-3495

CREATE TABLE last_deal_tmp AS
SELECT d.company_id, MAX(d.id) AS deal_id
FROM deals AS d
WHERE d.id = d.parent_deal_id 
AND d.company_id IS NOT NULL 
AND d.seller_id IS NOT NULL
GROUP BY d.company_id;

CREATE TABLE last_seller_tmp AS
SELECT l.company_id, d.seller_id
FROM deals AS d INNER JOIN last_deal_tmp AS l ON d.id = l.deal_id;

UPDATE companies AS c SET 
    c.seller_id = (SELECT l.seller_id FROM last_seller_tmp AS l WHERE l.company_id = c.id);

DROP TABLE last_deal_tmp;
DROP TABLE last_seller_tmp;