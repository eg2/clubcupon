DROP TABLE IF EXISTS scheduled_deals ;
CREATE TABLE IF NOT EXISTS `scheduled_deals` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `deal_id` bigint(20) unsigned DEFAULT NULL,
    `start_date` date,
    `end_date` date,
    `Monday` tinyint(1) DEFAULT '0',
    `Tuesday` tinyint(1) DEFAULT '0',
    `Wednesday` tinyint(1) DEFAULT '0',
    `Thursday` tinyint(1) DEFAULT '0',
    `Friday` tinyint(1) DEFAULT '0',
    `Saturday` tinyint(1) DEFAULT '0',
    `Sunday` tinyint(1) DEFAULT '0',
    `created` datetime DEFAULT NULL,
    `modified` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `deal_id_uk` (`deal_id`),
    UNIQUE KEY `id` (`id`,`deal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;
