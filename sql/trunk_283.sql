-- http://tracker.int.clarin.com/browse/CC-3408

INSERT INTO `payment_types`
	(`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`)
VALUES
	(93, now(), now(), 'PagoMisCuentas', 2, 'nps', 1, 90);

INSERT INTO `payment_settings`
	(`id`, `name`)
VALUES
	(16, 'Por NPS PagoMisCuentas');

INSERT INTO `payment_methods`
	(`id`, `name`, `logo`)
VALUES
	(19, 'PagoMisCuentas', 'pagomiscuentas.gif');

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(103, 19, 16, 93, 'PagoMisCuentas', 'PagoMisCuentas por NPS', 1);
