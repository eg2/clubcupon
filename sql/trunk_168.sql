--
-- Nueva columna behavior SoftDeletable en redemptions
--
alter table redemptions add column deleted TINYINT(1) DEFAULT 0;