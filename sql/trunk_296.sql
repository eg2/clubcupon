-- http://tracker.int.clarin.com/browse/CC-3804

CREATE TABLE IF NOT EXISTS `deal_trade_agreement` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) unsigned NOT NULL,
  `modified_by` bigint(20) unsigned DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_end_user_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_end_user_enabled` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `deal_trade_agreement` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `is_end_user_default`, `is_end_user_enabled`) VALUES
(1, '2013-11-07 21:30:39', '2013-11-07 21:30:39', 0, 0, 0, 'Tradicional', 0, 0),
(2, '2013-11-07 21:30:39', '2013-11-07 21:30:39', 0, 0, 0, 'Precompra', 1, 0),
(3, '2013-11-07 21:30:39', '2013-11-07 21:30:39', 0, 0, 0, 'Pago a proveedores', 1, 0),
(4, '2013-11-07 21:30:39', '2013-11-07 21:30:39', 0, 0, 0, 'Anticipo', 0, 0),
(5, '2013-11-07 21:30:39', '2013-11-07 21:30:39', 0, 0, 0, 'Canje', 1, 0);


ALTER TABLE deals ADD COLUMN deal_trade_agreement_id BIGINT(20) UNSIGNED DEFAULT NULL;


ALTER TABLE deals ADD CONSTRAINT `deals_deal_trade_agreement_fk` FOREIGN KEY (`deal_trade_agreement_id`)
 REFERENCES `deal_trade_agreement` (`id`);
