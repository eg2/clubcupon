-- Agrega un campo con la cantidad de saldo disponible por subscripcion.
alter table subscriptions add column available_balance_amount decimal(10,2) not null default 0;
