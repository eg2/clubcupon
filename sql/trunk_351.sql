alter table newsletter_profiles modify column code varchar(255) NOT NULL;
alter table frecuency_event_classes modify column code varchar(255) NOT NULL;
alter table newsletter_profiles add unique index(code);
alter table frecuency_event_classes add unique index(code);
alter table events modify column deleted tinyint(1) DEFAULT 0;
