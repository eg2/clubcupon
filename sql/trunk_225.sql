create table gift_pins (
  id bigint(20) unsigned not null auto_increment,
  code varchar(10) not null,
  blocked tinyint(1) not null,
  discount decimal(10,2) not null,
  created timestamp not null default current_timestamp,
  updated timestamp null default null,
  primary key (id),
  unique key code (code)
);

alter table deal_externals add column gift_pin_code varchar(20);
alter table deal_externals add column gift_pin_id bigint(20);
alter table deal_externals add column gift_amount decimal(10,2);

create index gift_pin_code_idx on deal_externals(gift_pin_code);
create index gift_pin_id_idx on deal_externals(gift_pin_id);
