-- http://jira.int.clarin.com/browse/CC-5166
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (95, now(), now(), 'American Express', 34, 'nps', 1, 20);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (96, now(), now(), 'Master Card', 34, 'nps', 1, 22);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (97, now(), now(), 'Cabal', 34, 'nps', 1, 23);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (98, now(), now(), 'Naranja', 34, 'nps', 1, 24);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (99, now(), now(), 'Visa', 34, 'nps', 1, 25);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (100, now(), now(), 'Italcred', 34, 'nps', 1, 28);

INSERT INTO `payment_settings` (`id`, `name`) VALUES (18, 'Por NPS NuestrosBeneficios');

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (124, 1, 18, 95, 'American Express', 'American Express por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (125, 9, 18, 99, 'Visa', 'Visa por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (126, 6, 18, 96, 'MasterCard', 'MasterCard por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (127, 3, 18, 97, 'Cabal', 'Cabal por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (128, 5, 18, 100, 'Italcred', 'Italcred por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (129, 7, 18, 98, 'Naranja', 'Naranja por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (130, 15, 18, 99, 'Visa ICBC', 'Visa ICBC por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (131, 16, 18, 96, 'MasterCard ICBC', 'MasterCard ICBC por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (132, 9, 18, 99, 'Visa Santander Rio', 'Visa Santander Rio por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (133, 1, 18, 95, 'AMEX Santander Rio', 'AMEX Santander Rio por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (134, 6, 18, 96, 'Bancor Cordobesa', 'MasterCard por NPS NuestrosBeneficios', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (135, 6, 18, 96, 'Bancor Fan', 'MasterCard por NPS NuestrosBeneficios', 0);
