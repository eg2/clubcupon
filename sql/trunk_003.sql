-- Gran reestructuracion de medios de pago

ALTER TABLE `clubcupon`.`deal_externals` DROP COLUMN `seller_op_id` , ADD COLUMN `is_gift` TINYINT(1) NULL DEFAULT 0  AFTER `updated` ;
ALTER TABLE `clubcupon`.`deals` DROP COLUMN `is_coupon_mail_sent` ;
