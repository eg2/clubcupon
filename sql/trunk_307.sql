DROP VIEW IF EXISTS deal_view;
CREATE OR REPLACE VIEW deal_view AS
SELECT
       deal.id                    AS 'deal_id',
       deal.company_id            AS 'company_id',
       deal.start_date            AS 'deal_start_date',
       deal.end_date              AS 'deal_end_date',
       deal.name                  AS 'deal_name',
       deal.deal_status_id        AS 'deal_status_id',
       deal.original_price        AS 'deal_original_price',
       deal.discount_percentage   AS 'deal_discount_percentage',
       deal.discounted_price      AS 'deal_discounted_price',
       deal.commission_percentage AS 'deal_commission_percentage',
       deal.is_now             AS 'deal_is_now',
       deal.coupon_start_date  AS 'coupon_start_date',
       deal.coupon_expiry_date AS 'coupon_expiry_date',
       deal.subtitle           AS 'deal_subtitle',
       deal.parent_deal_id     AS 'deal_parent_deal_id',
       city.id                 AS 'city_id',
       city.name               AS 'city_name',
       deal.pay_by_redeemed    AS 'deal_is_pay_by_redeemed',
       deal.is_end_user        AS 'deal_is_precompra',
       schedule.id             AS 'deal_schedule_id',
       cupons.coupon_redemed_count,
       cupons.coupon_expired_count,
       cupons.coupon_returned_count_cc,
       cupons.coupon_returned_count_ccnow,
       cupons.coupon_count,
       cupons.coupon_billed
FROM cities city, deal_cupon_summary_view cupons, deals deal
LEFT OUTER JOIN scheduled_deals schedule on  schedule.deal_id = deal.id
WHERE deal.city_id =  city.id
  and cupons.deal_id = deal.id;




