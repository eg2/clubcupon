ALTER TABLE `product_inventories`
	ADD COLUMN `source` VARCHAR(255);

ALTER TABLE `product_inventories`
	ADD COLUMN `source_identifier` VARCHAR(255);

UPDATE `product_inventories`
	SET `source` = 'LIBERTYA';

UPDATE `product_inventories`
	SET `source_identifier` = `bac_libertya_id`;

ALTER TABLE `product_inventories`
    DROP INDEX `bac_libertya_id_uk`;

ALTER TABLE `product_inventories`
    ADD UNIQUE INDEX `product_inventories_uk` (`source`, `source_identifier`);