-- Agrega la plantilla del modelo de mail sin precio

INSERT INTO `email_templates`
(`id`, `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES
(NULL, NOW(), NOW(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Deal of the day hide price', 'This email will be sent to all the subscribers of a particular city, when there is a deal available for that particular day.', '##DEAL_NAME##', '', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_LINK, DEAL_NAME, BUY_PRICE, ORIGINAL_PRICE, DISCOUNT, SAVINGS, COMPANY_NAME, COMPANY_SITE, COMPANY_ADDRESS, CITY_NAME, RATING, DEAL_NAME, DESCRIPTION, UNSUBSCRIBE_LINK ', '1');
commit;