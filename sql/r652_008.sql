create table google_adwords_conversions ( 
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `utm_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utm_medium` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utm_term` varchar(255) COLLATE utf8_unicode_ci,
  `utm_content` varchar(255) COLLATE utf8_unicode_ci,
  `utm_campaign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entity_type_id` tinyint(1) NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Google Adwords Conversion'

