-- Remover las `user_answers` que referencien preguntas de 'deportes' o 'tiempolibre',
-- remover las `answers` que referencien preguntas de 'deportes' o 'tiempolibre'
-- remover las `questions` de 'deportes' o 'tiempolibre'
delete from `user_answers` where `question_id` in (select `id` from `questions` where `name` in ('deportes', 'tiempolibre'));
delete from `answers`      where `question_id` in (select `id` from `questions` where `name` in ('deportes', 'tiempolibre'));
delete                                                         from `questions` where `name` in ('deportes', 'tiempolibre') ;

-- Insertar una nueva `question` para 'Hijos'
insert into `questions` (`created`, `modified`, `name`, `content`) values (NOW(), NOW(), 'hijos', 'Hijos');
-- Insertar las `answers` para la pregunta de 'Hijos'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Sí', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'hijos';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'No', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'hijos';

-- Insertar una nueva `question` para 'Edad de los hijos'
insert into `questions` (`created`, `modified`, `name`, `content`) values (NOW(), NOW(), 'edaddehijos', 'Edad de los hijos');
-- Insertar las `answers` para la pregunta de 'Edad de los hijos'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Menores de 12', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'edaddehijos';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'De 12 a 17',    `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'edaddehijos';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'De 18 o más',   `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'edaddehijos';

/*
-- Insertar una nueva `question` para '¿Qué tipo de ofertas de turismo preferís?'
insert into `questions` (`created`, `modified`, `name`, `content`) values (NOW(), NOW(), 'turismo', '¿Qué tipo de ofertas de turismo preferís?');
-- Insertar las `answers` para la pregunta de '¿Qué tipo de ofertas de turismo preferís?'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Para pasar el día',      `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'turismo';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'De una/dos noches',      `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'turismo';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'De una semana completa', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'turismo';
*/

-- Cambiar el contenido de la respuesta 'Solo' a 'Soltero/a' para la pregunta de 'estadocivil'
update `answers`
set `content` = 'Soltero/a'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `creator` = 'backend'
and `content` = 'Solo';
-- Cambiar el contenido de la respuesta 'En pareja sin hijos / sin hijos en casa' a 'Casado / En pareja' para la pregunta de 'estadocivil'
update `answers`
set `content` = 'Casado / En pareja'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `creator` = 'backend'
and `content` = 'En pareja sin hijos / sin hijos en casa';
-- Hacer que todas las `user_answers` que antes respondían con 'En pareja con hijos en casa'
-- ahora lo hagan con 'Casado / En pareja' para la pregunta 'estadocivil'
update `user_answers`
set `answer_id` =
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
    and `creator` = 'backend'
    and `content` = 'Casado / En pareja'
  )
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
    and `creator` = 'backend'
    and `content` = 'En pareja con hijos en casa'
  );
-- Eliminar la respuesta 'En pareja con hijos en casa' para la pregunta 'estadocivil'
delete from `answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `creator` = 'backend'
and `content` = 'En pareja con hijos en casa';
-- Eliminar las `user_answers` que respondían con una opción seteada por usuarios para la pregunta
-- 'estadocivil'
delete from `user_answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
    and `creator` = 'user'
  );
-- Eliminar las respuestas seteadas por usuarios para la pregunta 'estadocivil'
delete from `answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'estadocivil')
and `creator` = 'user';
-- Insertar las `answers` para la pregunta de 'estadocivil'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Separado / Divorciado', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'estadocivil';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Viudo',                 `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'estadocivil';

-- Cambiar el contenido de la respuesta 'Ir al cine ' a 'Ir al cine / teatro' para la pregunta de 'salidas'
update `answers`
set `content` = 'Ir al cine / teatro'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'salidas')
and `creator` = 'backend'
and `content` = 'Ir al cine ';
-- Hacer que todas las `user_answers` que antes respondían con 'Ir al teatro'
-- ahora lo hagan con 'Ir al cine / teatro' para la pregunta 'salidas'
update `user_answers`
set `answer_id` =
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'salidas')
    and `creator` = 'backend'
    and `content` = 'Ir al cine / teatro'
  )
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'salidas')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'salidas')
    and `creator` = 'backend'
    and `content` = 'Ir al teatro'
  );
-- Eliminar las `user_answers` que respondían con una opción seteada por usuarios para la pregunta
-- 'salidas'
delete from `user_answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'salidas')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'salidas')
    and `creator` = 'user'
  );
-- Eliminar las respuestas seteadas por usuarios para la pregunta 'salidas'
delete from `answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'salidas')
and `creator` = 'user';
-- Insertar las `answers` para la pregunta de 'salidas'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Hacer compras / ir de shopping', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'salidas';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Hacer deportes',                 `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'salidas';

-- Cambiar el contenido de la respuesta 'Pizza' a 'Pizza / Empanadas' para la pregunta de 'comidafavorita'
update `answers`
set `content` = 'Pizza / Empanadas'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
and `creator` = 'backend'
and `content` = 'Pizza';
-- Eliminar las `user_answers` que respondían con 'Española' o 'Italiana' para la pregunta
-- 'comidafavorita'
delete from `user_answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
    and `content` in ('Española', 'Italiana')
  );
-- Eliminar las `user_answers` que respondían con una opción seteada por usuarios para la pregunta
-- 'comidafavorita'
delete from `user_answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
    and `creator` = 'user'
  );
-- Eliminar las respuestas 'Española' o 'Italiana' para la pregunta 'comidafavorita'
delete from `answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
and `content` in ('Española', 'Italiana');
-- Eliminar las respuestas seteadas por usuarios para la pregunta 'comidafavorita'
delete from `answers`
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'comidafavorita')
and `creator` = 'user';

/*
-- Cambiar el contenido de la respuesta 'Aire Libre' a 'Turismo' para la pregunta de 'ofertasinteres'
update `answers`
set `content` = 'Turismo'
where 0 = 0
and `question_id` = (select `id` from `questions` where `name` = 'ofertasinteres')
and `creator` = 'backend'
and `content` = 'Aire Libre';
*/
-- Cambiar el contenido de la respuesta 'Restaurantes' a 'Restaurantes, Deliverys' para la pregunta de 'ofertasinteres'
update `answers`
set `content` = 'Restaurantes, Deliverys'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
and `creator` = 'backend'
and `content` = 'Restaurantes';
-- Cambiar el contenido de la respuesta 'Belleza y Salud' a 'SPA, Belleza y Peluquería' para la pregunta de 'ofertasinteres'
update `answers`
set `content` = 'SPA, Belleza y Peluquería'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
and `creator` = 'backend'
and `content` = 'Belleza y Salud';
-- Cambiar el contenido de la respuesta 'Indumentaria ' a 'Indumentaria y Calzado' para la pregunta de 'ofertasinteres'
update `answers`
set `content` = 'Indumentaria y Calzado'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
and `creator` = 'backend'
and `content` = 'Indumentaria ';
-- Cambiar el contenido de la respuesta 'Electrodomésticos' a 'Hogar (bazar, decoración)' para la pregunta de 'ofertasinteres'
update `answers`
set `content` = 'Hogar (bazar, decoración)'
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
and `creator` = 'backend'
and `content` = 'Electrodomésticos';
-- Hacer que todas las `user_answers` que antes respondían con 'Calzado'
-- ahora lo hagan con 'Indumentaria y Calzado' para la pregunta 'ofertasinteres'
update `user_answers`
set `answer_id` =
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
    and `creator` = 'backend'
    and `content` = 'Indumentaria y Calzado'
  )
where 0 = 0
and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
and `answer_id` in
  (
    select `id`
    from `answers`
    where 0 = 0
    and `question_id` in (select `id` from `questions` where `name` = 'ofertasinteres')
    and `creator` = 'backend'
    and `content` = 'Calzado'
  );
-- Insertar las `answers` para la pregunta de 'ofertasinteres'
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Fotografía',            `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'ofertasinteres';
insert into `answers` (`created`, `modified`, `content`, `question_id`, `creator`) select NOW(), NOW(), 'Clases y capacitación', `q`.`id`, 'backend' from `questions` `q` where `q`.`name` = 'ofertasinteres';

--
commit;


-- Eliminar los campos `address_street` y `address_number`
alter table `user_profiles` drop column `address_street`;
alter table `user_profiles` drop column `address_number`;

-- Eliminar el campo `i_love`
alter table `user_profiles` drop column `i_love`;

-- Agregar "checkboxes" para la pregunta "Otros sitios web en los que estoy registrado"
alter table `user_profiles` add `has_coupon_grouping_registration` TINYINT(1) default null after `gender`;
alter table `user_profiles` add `has_coupon_sites_registration`    TINYINT(1) default null after `gender`;
alter table `user_profiles` add `has_twitter_registration`         TINYINT(1) default null after `gender`;
alter table `user_profiles` add `has_facebook_registration`        TINYINT(1) default null after `gender`;
