------------------------------------------------------------------------------
-- Se actualiza el valor de la nueva columna. 
------------------------------------------------------------------------------
update deal_externals set 
    bac_substate = 'ACREDITADO'
where external_status = 'A';

update deal_externals set 
    bac_substate = 'CANCELADO'
where external_status = 'C';

commit;