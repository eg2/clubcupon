-- http://jira.int.clarin.com/browse/CC-5518

ALTER TABLE cities ADD column is_searchable TINYINT(1) NOT NULL DEFAULT 1 AFTER is_business_unit;
