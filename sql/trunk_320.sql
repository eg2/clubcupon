-- http://jira.int.clarin.com/browse/CC-4241
create or replace view deal_view as
select `deal`.`id` AS `deal_id`,`deal`.`company_id` AS `company_id`,`deal`.`start_date` AS `deal_start_date`,`deal`.`end_date` AS `deal_end_date`,`deal`.`name` AS `deal_name`,`deal`.`deal_status_id` AS `deal_status_id`,`deal`.`original_price` AS `deal_original_price`,`deal`.`discount_percentage` AS `deal_discount_percentage`,`deal`.`discounted_price` AS `deal_discounted_price`,`deal`.`commission_percentage` AS `deal_commission_percentage`,`deal`.`is_now` AS `deal_is_now`,`deal`.`coupon_start_date` AS `coupon_start_date`,`deal`.`coupon_expiry_date` AS `coupon_expiry_date`,`deal`.`subtitle` AS `deal_subtitle`,`deal`.`parent_deal_id` AS `deal_parent_deal_id`,`city`.`id` AS `city_id`,`city`.`name` AS `city_name`,`deal`.`pay_by_redeemed` AS `deal_is_pay_by_redeemed`,`deal`.`is_end_user` AS `deal_is_precompra`,
sum(if(((`du`.`is_used` = 1) and (`du`.`deleted` = 0)),1,0)) AS `coupon_redemed_count`,
sum(if((unix_timestamp(`deal`.`coupon_expiry_date`) < unix_timestamp(now())),1,0)) AS `coupon_expired_count`,
abs((sum(1) - ifnull(`de`.`quantity`,0))) AS `coupon_returned_count_cc`,
abs((sum(1) - ifnull(`de`.`quantity`,0))) AS `coupon_returned_count_ccnow`,
sum(if(((`du`.`deleted` = 0) and (`du`.`is_returned` = 0)),1,0)) AS `coupon_count`,
sum(if((((`deal`.`pay_by_redeemed` = 1) and (`du`.`is_used` = 1) and (`du`.`is_billed` = 1)) or ((`deal`.`pay_by_redeemed` = 0) and (`du`.`is_billed` = 1))),1,0)) AS `coupon_billed`,
null AS `deal_schedule_id`
from (((`deals` `deal` join cities city on deal.city_id = city.id)  left join `deal_users` `du` on(((`deal`.`id` = `du`.`deal_id`) and ((`deal`.`is_now` = 0) or (`du`.`is_returned` = 0))))) left join `deal_externals` `de` on(((`du`.`deal_external_id` = `de`.`id`) and (`de`.`external_status` = 'A'))))
group by deal.id;
