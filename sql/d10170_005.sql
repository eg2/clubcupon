-- update en shipping_address con el id de modo de envío, libertya_delivery_mode_id
-- config enviada por correo 

SET @Managedid = (select id from shipping_address_types where name='managed_logistics');
SET @NOManagedid = (select id from shipping_address_types where name='unmanaged_logistics');

UPDATE shipping_addresses SET libertya_delivery_mode_id=17, shipping_address_type_id=@Managedid
WHERE company_id IN ( 
    SELECT c.id AS company_id 
    FROM  companies c     
    WHERE slug='club-cupon');

UPDATE shipping_addresses SET  shipping_address_type_id=@NOManagedid
WHERE company_id IN ( 
    SELECT c.id AS company_id 
    FROM  companies c     
    WHERE slug!='club-cupon');

ALTER TABLE product_inventories MODIFY COLUMN stock int(11) DEFAULT 0;