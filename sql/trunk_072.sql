ALTER TABLE  `deal_users` ADD  `gift_dni` VARCHAR( 255 ) NOT NULL AFTER  `gift_email` ,
                          ADD  `gift_dob` DATE NOT NULL AFTER  `gift_dni`;

ALTER TABLE  `deal_users` CHANGE  `gift_dni`  `gift_dni` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `deal_users` CHANGE  `gift_dob`  `gift_dob` DATE NULL DEFAULT NULL;

