-- Agrega los campos "esta ciudad es un grupo de interes o no" y "esta ciudad emite newsletters o no"

alter table `cities` add `is_group`       TINYINT(1) default false after `facebook_url`;
alter table `cities` add `has_newsletter` TINYINT(1) default true  after `is_group`    ;