alter table accounting_items
  add bac_payment_id BIGINT( 20 ) unsigned;
alter table accounting_items
  add column bac_payment_sent datetime;
