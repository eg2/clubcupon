CREATE TABLE  `deal_categories` (
  `id` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `parent_id` BIGINT( 20 ) UNSIGNED NULL DEFAULT NULL
) ENGINE = INNODB;

ALTER TABLE  `deal_categories` ADD  `lft` BIGINT( 20 ) UNSIGNED NOT NULL ,
ADD  `rght` BIGINT( 20 ) UNSIGNED NOT NULL ,
ADD  `name` VARCHAR( 255 ) NOT NULL;


INSERT INTO `deal_categories` (`id`, `name`, `parent_id`, `lft`, `rght`) VALUES (1, 'Unknown', NULL, 0, 0);
ALTER TABLE  `deals` add  `deal_category_id`  BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '1';
