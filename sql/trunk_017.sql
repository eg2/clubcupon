-- Nuevo campo para el perfil de usuario, identifica otro barrio que no esté entre los posibles
ALTER TABLE user_profiles ADD COLUMN neighbourhood VARCHAR(255) NOT NULL DEFAULT '';
