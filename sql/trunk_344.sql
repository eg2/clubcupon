
-- LIST DESTACADOS - Viajes y Escapadas (ciudad turismo)

	-- Ofertas Nacionales (Especial : ofertas-nacionales)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--turismo-page--1-query--oooo-facet--city_name_no_group.Viajes+y+Escapadas', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Pasajes en micro (Especial : rutatlantica)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Pasajes en micro', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Rutatlantica&scV=-city--turismo-page--1-query--oooo-facet--city_name_no_group.Viajes+y+Escapadas', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Día de campo (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Mar del Plata (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Viajes a Mar del Plata', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--mar+del+plata', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Norte Argentino (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Viajes a Norte Argentino', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--norte+argentino', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Córdoba (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Viajes a Córdoba', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--cordoba+(turismo)', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Asistencia al viajero (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Asistencia al viajero', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--cardinal', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

-- TAB ESPECIALES - Viajes y Escapadas (ciudad turismo)

	-- Ofertas Nacionales (Especial : ofertas-nacionales)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--turismo-page--1-query--oooo-facet--city_name_no_group.Viajes+y+Escapadas', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Pasajes en micro (Especial : rutatlantica)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Pasajes en micro', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Rutatlantica&scV=-city--turismo-page--1-query--oooo-facet--city_name_no_group.Viajes+y+Escapadas', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Día de campo (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Mar del Plata (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Viajes a Mar del Plata', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--mar+del+plata', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Norte Argentino (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Viajes a Norte Argentino', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--norte+argentino', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Viajes a Córdoba (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Viajes a Córdoba', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--cordoba+(turismo)', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);

	-- Asistencia al viajero (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Asistencia al viajero', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Viajes+y+Escapadas&scV=-city--turismo-page--1-query--cardinal', 
		(SELECT id FROM cities WHERE slug = 'turismo')
	);


