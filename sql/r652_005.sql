INSERT INTO `email_templates` (`id`, `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`) VALUES (NULL, CURDATE(), CURDATE(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Mail no buyers', 'Mail de ofertas activas enviado a los no compradores de ofertas similares', 'ClubCupon - Esta Oferta te puede interesar', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 

Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>&iexcl;</title>
    </head>

    <body style="background: #f7f7f7;">
        <table width="593" align="center" cellpadding="0" cellspacing="0" height="300" style="background: 

#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#4d4d4d;">
            <tr>
                <td valign="top" height="93">
                    <img src="http://imagenes.clubcupon.com.ar/2011/07/20/img02/header.jpg" border="0"/>
                </td>
            </tr>
            <tr>
                  <td colspan="2" style="padding:10px; border-left:1px solid #ddd; border-right:1px solid 

#ddd;"><br />
Hola <b>##USERNAME##</b>,<br />

Intentaste comprar la oferta de ##DEAL_TITLE## y tenemos registrado que no pudiste completar el proceso.<br /><br />

No te la pierdas otra vez y complet&aacute; la compra antes que finalice el d&iacute;a de hoy <br />
haciendo click en el siguiente link <a href="##DEAL_URL##" style="border:0; text-decoration: none;">##DEAL_URL##</a> <br /><br />

El equipo de Club Cup&oacute;n.<br />
  <br /><br />
  
  <b style="font-size:16px; color:#FF6600"></b>
  
   </td>
            </tr>
            <tr>
                <td style="background: #f7f7f7;">
                    <a href="http://soporte.clubcupon.com.ar/" style="border:0; text-decoration: none;">
                        <img src="##ABSOLUTE_IMG_PATH##email/pie_mail_deal_bought.jpg" style=" 

border:none;" />
                    </a>
                </td>
            </tr>
        </table>
    </body>

</html>
', 'FROM_EMAIL, REPLY_TO_EMAIL, USERNAME, DEAL_TITLE, DEAL_URL', '1');