------------------------------------------------------------------------------
-- Se agrega utm_source para registrar desde dónde llegan las suscripciones. 
------------------------------------------------------------------------------
ALTER TABLE subscriptions ADD utm_source VARCHAR(64) AFTER is_voluntary;