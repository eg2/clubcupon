-- http://jira.cmd.com.ar/browse/CC-5552

CREATE TABLE `beacon_logs` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`beacon_device_id` BIGINT(20) UNSIGNED NOT NULL,
	`deal_id` BIGINT(20) UNSIGNED NOT NULL,
	`mac` VARCHAR(17) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE `beacon_logs`
		ADD CONSTRAINT fk_beacon_id FOREIGN KEY (`beacon_device_id`) REFERENCES `beacon_devices` (`id`),
		ADD CONSTRAINT fk_deal_id FOREIGN KEY (`deal_id`) REFERENCES `deals` (`id`);
		
ALTER TABLE `beacon_device_deals`
	ADD COLUMN address VARCHAR(100) NULL COLLATE 'utf8_unicode_ci';