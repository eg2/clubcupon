DROP VIEW IF EXISTS deal_view;
CREATE OR REPLACE VIEW deal_view AS
SELECT
       deal.id                    AS 'deal_id',
       deal.company_id            AS 'company_id',
       deal.start_date            AS 'deal_start_date',
       deal.end_date              AS 'deal_end_date',
       deal.name                  AS 'deal_name',
       deal.deal_status_id        AS 'deal_status_id',
       deal.original_price        AS 'deal_original_price',
       deal.discount_percentage   AS 'deal_discount_percentage',
       deal.discounted_price      AS 'deal_discounted_price',
       deal.commission_percentage AS 'deal_commission_percentage',
       deal.is_now             AS 'deal_is_now',
       deal.coupon_start_date  AS 'coupon_start_date',
       deal.coupon_expiry_date AS 'coupon_expiry_date',
       deal.subtitle           AS 'deal_subtitle',
       deal.parent_deal_id     AS 'deal_parent_deal_id',
       city.id                 AS 'city_id',
       city.name               AS 'city_name',
       deal.pay_by_redeemed    AS 'deal_is_pay_by_redeemed',
       deal.is_end_user        AS 'deal_is_precompra',
      (select count(1) from deal_users du where du.is_returned = 1 and du.deal_id = deal.id)                                              AS 'coupon_redemed_count',
      (select count(1) from deal_users du where du.is_used = 1 and du.deal_id = deal.id)                                                  AS 'coupon_used_count',
      (select count(1) from deal_users du where unix_timestamp(deal.coupon_expiry_date) < unix_timestamp(now()) and du.deal_id = deal.id) AS 'coupon_returned_count',
      (select count(1) from deal_users du where du.deal_id = deal.id) AS 'coupon_count',
      (select count(1) from deal_users du where du.is_billed = 1) AS 'coupon_billed'
FROM deals deal, cities city
WHERE deal.city_id =  city.id;