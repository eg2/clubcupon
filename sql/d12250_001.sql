CREATE TABLE `accounting_errors` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `deleted` TINYINT(1) NOT NULL DEFAULT '0',
    `accounting_item_id` BIGINT(20) UNSIGNED NOT NULL,
    `method` VARCHAR(255) NOT NULL,
    `message` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `accounting_item_id` (`accounting_item_id`),
    CONSTRAINT `accounting_errors_items_fk`
        FOREIGN KEY (`accounting_item_id`) 
        REFERENCES `accounting_items` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;