-- http://jira.int.clarin.com/browse/CC-5073
ALTER TABLE shipping_addresses modify created DATETIME DEFAULT NULL;

ALTER TABLE shipping_addresses modify modified DATETIME DEFAULT NULL;
