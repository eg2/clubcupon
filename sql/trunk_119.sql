------------------------------------------------------------------------------
-- Se agrega bac_substate para guardar el subestado reportado por BAC
------------------------------------------------------------------------------
alter table `deal_externals` add `bac_substate` VARCHAR(64) after `external_status`;