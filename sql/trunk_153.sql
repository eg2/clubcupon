INSERT INTO `deal_categories` ( `parent_id`, `lft`, `rght`, `name`) VALUES
(0, 1, 6, 'Now Root');

INSERT INTO `deal_categories` ( `parent_id`, `lft`, `rght`, `name`, `icon`) VALUES
(LAST_INSERT_ID(), 1, 2, 'Comidas  y Bebidas','comidas'),
(LAST_INSERT_ID(), 2, 3, 'Bares y Restaurantes','resto'),
(LAST_INSERT_ID(), 3, 4, 'Cafés','cafes'),
(LAST_INSERT_ID(), 4, 5, 'Turismo','turismo'),
(LAST_INSERT_ID(), 5, 6, 'Transporte','transporte');
(LAST_INSERT_ID(), 0, 1, 'Todas','todas');