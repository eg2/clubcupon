ALTER TABLE `branches` ADD `street` VARCHAR( 50 ) NOT NULL AFTER `city_id` ,
ADD `number` INT NOT NULL AFTER `street` ,
ADD `floor` VARCHAR( 5 ) NOT NULL AFTER `number` ,
ADD `apartament` VARCHAR( 5 ) NOT NULL AFTER `floor` 