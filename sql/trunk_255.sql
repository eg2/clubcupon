-- accounting_calendars
alter table accounting_calendars add column payment_sold_check tinyint(1) DEFAULT 0;
alter table accounting_calendars add column payment_sold_transfer tinyint(1) DEFAULT 0;
alter table accounting_calendars add column payment_redeemed_check tinyint(1) DEFAULT 0;
alter table accounting_calendars add column payment_redeemed_transfer tinyint(1) DEFAULT 0;