-- http://jira.int.clarin.com/browse/CC-3162

UPDATE email_templates SET 
	`from` = 'tucupon@clubcupon.com.ar'
WHERE name = 'Deal Coupon Generic'
LIMIT 1;