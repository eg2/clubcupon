DELETE
FROM search_index;

INSERT INTO search_index (association_key, model, DATA, created, modified)
SELECT id AS association_key, 'Company' AS model, name AS DATA, NOW() AS created, NOW() AS modified
FROM companies;

INSERT INTO search_index (association_key, model, DATA, created, modified)
SELECT id AS association_key, 'Deal' AS model, name AS DATA, NOW() AS created, NOW() AS modified
FROM deals;