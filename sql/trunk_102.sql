-- Agrega el campo "is_end_user" a deals y a companies
alter table `companies` add `is_end_user` TINYINT(1) not null default FALSE after `is_tourism`;
alter table `deals`     add `is_end_user` TINYINT(1) not null default FALSE after `is_tourism`;