UPDATE `email_templates`
SET `subject` = 'Este es tu cupón de compra de ##SITE_NAME##: ##DEAL_NAME##.'
WHERE `name` = 'Deal Coupon';
COMMIT;