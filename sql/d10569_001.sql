-- http://jira.int.clarin.com/browse/CC-5108
UPDATE shipping_addresses SET bac_storehouse_id=39 WHERE name='Parque Patricios';

UPDATE shipping_addresses SET bac_storehouse_id=38 WHERE name='Recoleta';

UPDATE shipping_addresses SET bac_storehouse_id=37 WHERE name='Obelisco';

UPDATE shipping_addresses SET bac_storehouse_id=36 WHERE name='Núñez';

UPDATE shipping_addresses SET bac_storehouse_id=35 WHERE name='La Plata';