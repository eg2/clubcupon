-- Campo de verificacion. Indica si una oferta es exclusiva de los socios de fibertel
ALTER TABLE `deals` ADD `is_fibertel_deal` TINYINT( 1 ) NOT NULL DEFAULT '0';