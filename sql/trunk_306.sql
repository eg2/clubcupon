DROP VIEW IF EXISTS deal_cupon_summary_view;
CREATE OR REPLACE VIEW deal_cupon_summary_view AS
SELECT 
    deal.id AS deal_id,
  sum(if(((du.deal_id = deal.id) AND (du.deleted = 1) AND ((deal.pay_by_redeemed=1 AND du.is_used = 1) OR (deal.pay_by_redeemed=0))),
    1,
    0)) AS coupon_returned_count_cc,
  sum(if(((du.deal_id = deal.id) AND (du.deleted= 1 or du.is_returned = 1) AND ((deal.pay_by_redeemed=1 AND du.is_used = 1) OR (deal.pay_by_redeemed=0))),
    1,
    0)) AS coupon_returned_count_ccnow,
  sum(if((du.deal_id = deal.id AND du.is_used = 1  AND ((du.is_returned=0 AND du.deleted=0 AND deal.pay_by_redeemed=1) OR (deal.pay_by_redeemed=0))),
    1,
    0)) AS coupon_redemed_count,
	sum(if(((du.deal_id = deal.id) AND ((du.is_returned=0 AND du.deleted=0 AND deal.pay_by_redeemed=1 AND du.is_used = 1) OR (deal.pay_by_redeemed=0 AND du.is_returned=0 AND du.deleted=0))),
		1, 
		0)) AS coupon_count,
	sum(if((du.deal_id = deal.id AND (du.is_billed = 1 AND ((deal.pay_by_redeemed=1 AND du.is_used = 1) OR (deal.pay_by_redeemed=0)))), 
		1, 
		0)) AS coupon_billed,
  sum(if(((unix_timestamp(deal.coupon_expiry_date) < unix_timestamp(now())) and (du.deal_id = deal.id)),
    1,
    0)) AS coupon_expired_count
FROM deals deal left outer join deal_users du on deal.id = du.deal_id
GROUP BY deal.id

