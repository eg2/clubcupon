UPDATE `email_templates`
SET `email_variables` = CONCAT(`email_variables`, ', PAYMENT_TYPE')
WHERE `name` = 'Deal Bought';
COMMIT;