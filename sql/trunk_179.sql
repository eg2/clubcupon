--http://tracker.int.clarin.com/browse/CC-1879
SET @rootid = (SELECT id FROM deal_categories WHERE name = "Now Root");
DELETE FROM deal_categories WHERE parent_id = @rootid;
INSERT INTO deal_categories (`id`,`parent_id`,`lft`,`rght`,`name`,`icon`) VALUES (NULL, @rootid, '0', '1', 'Todas', 'todas');
INSERT INTO deal_categories (`id`,`parent_id`,`lft`,`rght`,`name`,`icon`) VALUES (NULL, @rootid, '1', '2', 'Entretenimiento', 'resto');
INSERT INTO deal_categories (`id`,`parent_id`,`lft`,`rght`,`name`,`icon`) VALUES (NULL, @rootid, '2', '3', 'Salud y Belleza', 'todas');
INSERT INTO deal_categories (`id`,`parent_id`,`lft`,`rght`,`name`,`icon`) VALUES (NULL, @rootid, '3', '4', 'Gastronomía', 'comidas');