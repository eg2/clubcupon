-- Agrega campo 'parent_deal_id' que indica el deal del cual este es subdeal.
alter table `deals` add `parent_deal_id` BIGINT(20) default null after `id`;