INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`)
VALUES (83, NOW(), NOW(), 'TarjetaDeCredito', 11, 'dim', 1, 1);

INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`)
VALUES (84, NOW(), NOW(), 'PagoOffline', 11, 'dim', 1, 2);

UPDATE payment_options SET
payment_type_id = 83
WHERE payment_setting_id = 11 AND payment_type_id <> 5000
LIMIT 7;

UPDATE payment_options SET
payment_type_id = 84
WHERE payment_setting_id = 12
LIMIT 3;