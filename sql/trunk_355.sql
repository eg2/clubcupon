-- http://jira.int.clarin.com/browse/CC-5039
DELETE FROM `payment_types` WHERE `id` = 94;
DELETE FROM `payment_settings` WHERE `id` = 17;
DELETE FROM `payment_options` WHERE `id` IN (114, 115, 116, 117, 118, 119, 120, 121, 122, 123);
--
INSERT INTO `payment_types`
	(`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`)
VALUES
	(94, now(), now(), 'TarjetaDeCreditoNuestrosBeneficios', 25, 'dim', 1, 1);
--
INSERT INTO `payment_settings`
	(`id`, `name`)
VALUES
	(17, 'Por MercadoPago NuestrosBeneficios');
--	
INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(114, 1, 17, 94, 'American Express', 'American Express por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(115, 6, 17, 94, 'MasterCard', 'MasterCard por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(116, 8, 17, 94, 'Shopping', 'Shopping por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(117, 2, 17, 94, 'Argencard', 'Argencard por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(118, 7, 17, 94, 'Naranja', 'Naranja Express por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(119, 3, 17, 94, 'Cabal', 'Cabal por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(120, 9, 17, 94, 'Visa', 'Visa por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(121, 17, 17, 94, 'Banelco', 'Banelco Por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 
	(122, 20, 17, 94, 'Nativa', 'Nativa por MercadoPago NuestrosBeneficios', 0);

INSERT INTO `payment_options` 
	(`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) 
VALUES 	
	(123, 4, 17, 94, 'Diners', 'Diners por MercadoPago NuestrosBeneficios', 0);
--	