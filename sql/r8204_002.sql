ALTER TABLE cities ADD is_business_unit tinyint(1);
UPDATE cities SET is_business_unit = 0;
