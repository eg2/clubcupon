-- http://jira.int.clarin.com/browse/CC-3057
alter table deals ADD COLUMN payment_period INT( 3 ) NOT NULL;
alter table deals ADD COLUMN campaign_id BIGINT ( 20 ) NOT NULL;
