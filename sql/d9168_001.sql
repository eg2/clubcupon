-- http://jira.int.clarin.com/browse/CC-4591

alter table product_products add column `has_pins` INT(11) NULL DEFAULT '0';

alter table pins add column product_id BIGINT(20) UNSIGNED default null;//debería ser NOT NULL pero asi podemos actualizar los registros actuales
alter table pins add CONSTRAINT `pins_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product_products` (`id`) ON UPDATE NO ACTION;

ALTER TABLE `pins`
	CHANGE COLUMN `deal_id` `deal_id` BIGINT(20) UNSIGNED NULL AFTER `deal_user_id`;//para poder crear los nuevos pines sin asociarlos a ofertas

