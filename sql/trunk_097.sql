-- Carga de banners activos e inactivos,
-- que estaban codeados en elements/sidebar en forma estatica

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `clubcupon`
--

--
-- Volcar la base de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `name`, `status`, `order`, `position`, `style`, `city_id`, `start_date`, `end_date`, `image`, `link`, `alt`) VALUES
(102, 'Club cupon llego a zona norte', 1, 1, NULL, 'box_banner', '42550', '2011-08-01', '2012-08-01', '/img/Banner/banner_Club cupon llego a zona norte-ontcnqq141.png', 'buenos-aires-zona-norte', 'Buenos Aires Zona Norte'),
(103, 'El Noble', 0, 2, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_El Noble-c76cd4vjb1.png', '/deal/dulces-placeres-50-off-en-1-caja-de-9-bombones-cher-de-135-gramos-5-bocaditos-de-dulce-de-lech', 'El Noble'),
(104, 'Zelaya', 0, 3, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Zelaya-giq8i7po6w.png', '/deal/ahora-todas-nos-podemos-liberar-arma-tu-tratamiento-de-depilacion-definitiva-con-el-innovador-', 'Ahora la belleza no cuesta: arm&aacute; tu tratamiento de depilación definitiva con el innovador m&eacute;todo Soprano'),
(105, 'Bonafide-Nugaton', 0, 4, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Bonafide-Nugaton-buzyhbh0s1.png', 'ciudad-de-buenos-aires/deal/dulces-placeres-50-off-en-1-caja-de-9-bombones-cher-de-135-gramos-5-boca', 'OFERTA DEL DÍA Dulces placeres: 50% off en 1 caja de 9 bombones Cher de 135 gramos + 5 Bocaditos de dulce de leche al rhum + 3 Nugatones blancos. Por $28. '),
(106, 'Si al cupon', 0, 5, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-05', '/img/Banner/banner_Si al cupon-miaqjn0aft.png', '/pages/sialcupon?utm_source=BNHome-CC&utm_medium=BNHome-SiAlCupon&utm_term=SiAlCupon-FB&utm_campaign', 'Concurso Facebook - S&iacute; al Cup&oacute;n'),
(107, 'Circa', 0, 6, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Circa-elj6m4t1os.png', '#', 'Exclusivo para usuarios CIRCA'),
(108, 'Ganadores Calle 13', 0, 7, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_Ganadores Calle 13-qm83y4sza5.png', '/pages/bases', 'Ganadores Calle 13'),
(109, 'Ganadores Aventura', 0, 8, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_Ganadores Aventura-v9tc134z16.png', '/pages/aventura', 'Ganadores Aventura'),
(110, 'Promo mil', 0, 9, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_Promo mil-e16pw2l2mg.png', '/pages/promo_mil', 'Ganá mil pesos para seguir disfrutando'),
(111, 'Estopa', 0, 10, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Estopa-w03v4wi74m.jpg', '/pages/estopa', 'Estopa'),
(112, 'Concurso Facebook', 0, 11, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Concurso Facebook-o37m96s1fm.png', '/pages/concursofacebook', 'Concurso Facebook'),
(113, 'Concurso Facebook día del amigo', 0, 12, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Concurso Facebook dia del amigo-mra1g30o2i.png', '/pages/diadelamigo?utm_source=CC-BNHome&utm_medium=BNHome-DiaDelAmigo&utm_term=DiaDelAmigo-FB&utm_ca', 'Concurso Facebook &ldquo;D&iacute;a del Amigo&rdquo;'),
(114, 'Concurso Facebook - Dia del Padre', 0, 13, NULL, 'box_banner', '42550', '2011-08-03', '2011-08-04', '/img/Banner/banner_Concurso Facebook - Dia del Padre-k6sq27gwsb.png', '/pages/padre', 'Concurso Facebook - Dia del Padre'),
(115, 'Nuevos medios de pago', 0, 14, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_Nuevos medios de pago-8wt4dbqiqk.gif', '/pages/terms', 'Nuevos medios de pago'),
(116, 'Contacto', 0, 15, NULL, 'box_banner', '', '2011-08-03', '2011-08-04', '/img/Banner/banner_Contacto-p4r9p0aq7f.gif', '#', 'Contacto');
