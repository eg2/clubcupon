ALTER TABLE payment_types ADD COLUMN bac_payment_type BIGINT(20);

UPDATE payment_types SET bac_payment_type = id;