DROP VIEW IF EXISTS `accounting_guarantee_fund_grouped`;
CREATE OR REPLACE VIEW `accounting_guarantee_fund_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       `acg`.`pay_by_redeemed`,
       sum(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`
FROM `accounting_company_id_grouped` AS `acg`
WHERE `acg`.`pay_by_redeemed` != 1
GROUP BY `acg`.`deal_company_id`,
	 `acg`.`max_payment_date`;
