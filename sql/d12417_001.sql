-- https://artear.atlassian.net/browse/CC-5735
CREATE TABLE `shipping_address_users` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED,
    `deleted` TINYINT(1) NOT NULL DEFAULT 0,
    `user_id` BIGINT(20) NOT NULL,
    `deal_external_id` BIGINT(20) UNSIGNED NOT NULL,
    `country_id` BIGINT(20) UNSIGNED NOT NULL,
    `state_id` BIGINT(20) NOT NULL,
    `city_name` VARCHAR(128) NOT NULL,
    `street` VARCHAR(128) NOT NULL,
    `number` VARCHAR(16) NOT NULL,
    `floor_number` VARCHAR(16) DEFAULT NULL,
    `apartment` VARCHAR(16) DEFAULT NULL,
    `postal_code` VARCHAR(16) NOT NULL,
    `telephone` VARCHAR(16) DEFAULT NULL,
    `details` VARCHAR(512) DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `shipping_address_users_deal_externals_fk`
        FOREIGN KEY (`deal_external_id`) REFERENCES `deal_externals` (`id`),
    CONSTRAINT `shipping_address_users_users_fk`
        FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
    CONSTRAINT `shipping_address_users_countries_fk`
        FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
    CONSTRAINT `shipping_address_users_states_fk`
        FOREIGN KEY (`state_id`) REFERENCES `states` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

CREATE INDEX `shipping_address_users_deal_externals_fk`
    ON `shipping_address_users` (`deal_external_id`);

CREATE INDEX `shipping_address_users_users_fk`
    ON `shipping_address_users` (`user_id`);

CREATE INDEX `shipping_address_users_countries_fk`
    ON `shipping_address_users` (`country_id`);

CREATE INDEX `shipping_address_users_states_fk`
    ON `shipping_address_users` (`state_id`);
--
-- https://artear.atlassian.net/browse/CC-5738
ALTER TABLE `deals`
    ADD COLUMN `is_shipping_adress_user` TINYINT(1) NULL DEFAULT '0';
