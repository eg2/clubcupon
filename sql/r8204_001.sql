CREATE TABLE `corporate_guest_cities` (
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` BIGINT(20) UNSIGNED NOT NULL,
`modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
`deleted` TINYINT(1) NOT NULL DEFAULT '0',
`city_id` BIGINT(20) UNSIGNED NOT NULL,
`corporate_guest_id` BIGINT(20) UNSIGNED NOT NULL,
`priority` INT(11) NULL DEFAULT '0',
PRIMARY KEY (`id`),
INDEX `city_id` (`city_id`),
INDEX `corporate_guest_id` (`corporate_guest_id`),
INDEX `priority` (`priority`),
FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
FOREIGN KEY (`corporate_guest_id`) REFERENCES `corporate_guests` (`id`)
) ENGINE=InnoDB;
