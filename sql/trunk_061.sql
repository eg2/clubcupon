insert into `actions` (`id`, `created`, `modified`, `name`, `description`)
values (3, NOW(), NOW(), 'Compra', 'Esta acción representa una compra exitosa.'),
       (4, NOW(), NOW(), 'Referido', 'Esta acción representa una compra exitosa por un usuario referido.');

insert into `action_points` (`id`, `created`, `action_id`, `points`)
values (3, NOW(), 3, 5),
       (4, NOW(), 4, 15);

commit;