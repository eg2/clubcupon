--
-- Nueva columna behavior SoftDeletable en deal_user
--
alter table deal_users add column deleted TINYINT(1) DEFAULT 0;