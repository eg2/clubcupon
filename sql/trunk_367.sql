-- http://jira.int.clarin.com/browse/CC-5343

INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (27, now(), now(), 0, 0, 0, 1, 138, 40);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (28, now(), now(), 0, 0, 0, 2, 138, 10);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (29, now(), now(), 0, 0, 0, 3, 138, 30);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (30, now(), now(), 0, 0, 0, 4, 138, 20);
