UPDATE cities c SET 
c.slug = 'club-cupon-ya', 
c.has_newsletter = 0,
c.is_approved = 1, 
c.is_selectable = 0, 
c.is_group = 1,
c.is_hidden = 1
WHERE slug IN('club-cupon-ya', 'ccya');