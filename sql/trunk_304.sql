DROP VIEW IF EXISTS `accounting_company_id_grouped`;
CREATE OR REPLACE VIEW `accounting_company_id_grouped` AS
SELECT `deal`.`company_id` AS `deal_company_id`,
        `deal`.`pay_by_redeemed` AS `pay_by_redeemed`,
        date(`deal`.`start_date`) AS `deal_start_date`,
        sum(`liquidate`.`total_quantity`) AS `liquidate_total_quantity`,
        `liquidate`.`id` AS `liquidate_id`,
        `liquidate`.`discounted_price` AS `liquidate_discounted_price`,
        `liquidate`.`liquidating_guarantee_fund_percentage` AS `liquidate_guarantee_fund_percentage`,
	ROUND((`liquidate`.`discounted_price` * `liquidate`.`total_quantity` * (`liquidate`.`liquidating_guarantee_fund_percentage`/100)),2) AS `liquidate_guarantee_fund_amount`,
        sum(ifnull(`bill`.`total_amount`,0)) AS `bill_total_amount`,
        sum(`liquidate`.`total_amount`) AS `liquidate_total_amount`,
        date(max(if((`deal`.`pay_by_redeemed` = 0),if(((`deal`.`start_date` + interval `deal`.`payment_term` DAY) >= now()),(`deal`.`start_date` + interval `deal`.`payment_term` DAY),(`liquidate`.`created` + interval 7 DAY)),(`liquidate`.`created` + interval `deal`.`payment_term` DAY)))) AS `max_payment_date`,
        `deal`.`id` AS `deal_id`,
        `deal`.`name` AS `deal_name`,
        `city`.`name` AS `city_name`
FROM
	(	(	(accounting_items AS liquidate LEFT JOIN accounting_items AS bill ON (liquidate.accounting_item_id = bill.id)
			) JOIN deals AS deal ON (liquidate.deal_id = deal.id)
		) JOIN companies AS company ON (liquidate.model_id = company.id)
	) LEFT JOIN cities AS city ON deal.city_id = city.id
WHERE ((`liquidate`.`deleted` = 0)
       AND (`liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD',
                                              'LIQUIDATE_BY_SOLD_EXPIRED',
                                              'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',
                                              'LIQUIDATE_BY_REDEEMED',
                                              'LIQUIDATE_BY_REDEEMED_EXPIRED'))
       AND (`liquidate`.`model` = 'Company')
       AND (`company`.`validated` = 1)
       AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY',
                                          'BILL_BY_REDEEMED_FOR_COMPANY'))
            OR isnull(`bill`.`accounting_type`))
       AND ((`bill`.`model` = 'Company')
            OR isnull(`bill`.`model`)))
GROUP BY `deal`.`company_id`,
	`liquidate`.`id`;

-- --------------------------------------------------------------------------------------
DROP VIEW IF EXISTS `accounting_deal_id_grouped`;
CREATE OR REPLACE VIEW `accounting_deal_id_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       `acg`.`pay_by_redeemed`,
       `acg`.`deal_id`,
       `acg`.`deal_name`,
       `acg`.`deal_start_date`,
       `acg`.`city_name`,
       `acg`.`liquidate_guarantee_fund_percentage`,
       `acg`.`liquidate_id`,
       sum(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`,
       sum(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`,
       sum(`acg`.`bill_total_amount`) AS `bill_total_amount`,
       sum(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` AS `acg`
GROUP BY `acg`.`deal_company_id`,
         `acg`.`max_payment_date`,
         `acg`.`deal_id`;

-- --------------------------------------------------------------------------------------
DROP VIEW IF EXISTS `accounting_guarantee_fund_grouped`;
CREATE OR REPLACE VIEW `accounting_guarantee_fund_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       sum(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`
FROM `accounting_company_id_grouped` AS `acg`
GROUP BY `acg`.`deal_company_id`,
	 `acg`.`max_payment_date`;

-- --------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW accounting_campaign_item_extras AS
SELECT `deal`.`company_id` AS `deal_company_id`,
       `liquidate`.`id` AS `liquidate_id`,
       IFNULL(`bill`.`id`,0) AS `bill_id`,
       `deal`.`id` AS `deal_id`,
       `deal`.`start_date` AS `deal_start_date`,
       `deal`.`name` AS `deal_name`,
       `city`.`name` AS `city_name`,
       `liquidate`.`total_quantity` AS `liquidate_total_quantity`,
       `calendar`.`since` AS `calendar_since`,
       `calendar`.`until` AS `calendar_until`,
       IFNULL(`bill`.`bac_sent`,`liquidate`.`created`) AS `liquidate_bac_sent`,
       IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS `max_payment_date`,
       `deal`.`discounted_price` AS `deal_discounted_price`,
       (IFNULL(`bill`.`total_quantity`,0) * `deal`.`discounted_price`) AS `bill_gross_amount`,
       IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`,
       IFNULL(`bill`.`billing_commission_percentage`,0) AS `bill_billing_commission_percentage`,
       ROUND((((IFNULL(`bill`.`discounted_price`,0) * (IFNULL(`bill`.`billing_commission_percentage`,0) / 100)) * IFNULL(`bill`.`total_quantity`,0)) * (IFNULL(`bill`.`billing_iva_percentage`,0) / 100)),2) AS `bill_iva_amount`,
       IFNULL(`bill`.`billing_iva_percentage`,0) AS `bill_iva_percentage`,
       0 AS `liquidate_iibb_amount`,
       `liquidate`.`liquidating_iibb_percentage` AS `liquidate_iibb_percentage`,
       `liquidate`.`total_amount` AS `liquidate_total_amount`,
       ROUND((`liquidate`.`discounted_price` * `liquidate`.`total_quantity` * (`liquidate`.`liquidating_guarantee_fund_percentage`/100)),2) AS `liquidate_guarantee_funds`
FROM ((((`accounting_items` `liquidate`
         JOIN `accounting_calendars` `calendar` ON((`liquidate`.`accounting_calendar_id` = `calendar`.`id`)))
        LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
       JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
      JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0
        AND `liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD',
                                              'LIQUIDATE_BY_SOLD_EXPIRED',
                                              'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',
                                              'LIQUIDATE_BY_REDEEMED',
                                              'LIQUIDATE_BY_REDEEMED_EXPIRED'))
       AND (`liquidate`.`model` = 'Company')
       AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY',
                                          'BILL_BY_REDEEMED_FOR_COMPANY'))
            OR ISNULL(`bill`.`accounting_type`))
       AND ((`bill`.`model` = 'Company')
            OR ISNULL(`bill`.`model`)));

