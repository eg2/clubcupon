-- http://tracker.int.clarin.com/browse/CC-4110
DROP TABLE IF EXISTS `shipping_addresses`;
CREATE TABLE `shipping_addresses` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`company_id` BIGINT(20) UNSIGNED NOT NULL,
	`country_id` BIGINT(20) UNSIGNED NOT NULL,
	`state_id` BIGINT(20) NOT NULL,
	`city_id` BIGINT(20) UNSIGNED NULL,
	`street` VARCHAR(128) NOT NULL COLLATE 'utf8_unicode_ci',
	`number` INT(5) NOT NULL,
	`neighbourhood_id` BIGINT(20) NULL,
	`floor_number` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`apartment` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`postal_code` VARCHAR(16) NOT NULL COLLATE 'utf8_unicode_ci',
	`telephone` VARCHAR(16) NULL COLLATE 'utf8_unicode_ci',
	`fax_number` VARCHAR(16) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`email` VARCHAR(25) NULL COLLATE 'utf8_unicode_ci',
	`details` VARCHAR(500) NULL COLLATE 'utf8_unicode_ci',
	`custom_address` VARCHAR(200) NOT NULL COLLATE 'utf8_unicode_ci',
	`link_bukeala` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `shipping_addresses_fk1` (`company_id`),
	INDEX `shipping_addresses_fk2` (`country_id`),
	INDEX `shipping_addresses_fk3` (`state_id`),
	INDEX `shipping_addresses_fk4` (`city_id`),
	INDEX `shipping_addresses_fk5` (`neighbourhood_id`),
	CONSTRAINT `shipping_addresses_fk1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
	CONSTRAINT `shipping_addresses_fk2` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
	CONSTRAINT `shipping_addresses_fk3` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`),
	CONSTRAINT `shipping_addresses_fk4` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
	CONSTRAINT `shipping_addresses_fk5` FOREIGN KEY (`neighbourhood_id`) REFERENCES `neighbourhoods` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;

CREATE TABLE `shipping_address_deals` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`shipping_address_id` BIGINT(20) UNSIGNED NOT NULL,
	`deal_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `deals_shipping_addresses_fk1` (`deal_id`),
	INDEX `deals_shipping_addresses_fk2` (`shipping_address_id`),
	CONSTRAINT `deals_shipping_addresses_fk1` FOREIGN KEY (`deal_id`) REFERENCES `deals` (`id`),
	CONSTRAINT `deals_shipping_addresses_fk2` FOREIGN KEY (`shipping_address_id`) REFERENCES `shipping_addresses` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;