-- accounting_deal_id_grouped;
CREATE OR REPLACE VIEW `accounting_deal_id_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`,`acg`.`pay_by_redeemed` AS `pay_by_redeemed`,`acg`.`deal_id` AS `deal_id`,`acg`.`deal_name` AS `deal_name`,`acg`.`deal_start_date` AS `deal_start_date`, acg.liquidate_discounted_price AS liquidate_discounted_price, `acg`.`city_name` AS `city_name`,`acg`.`liquidate_guarantee_fund_percentage` AS `liquidate_guarantee_fund_percentage`,`acg`.`liquidate_id` AS `liquidate_id`, SUM(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`, SUM(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`, SUM(`acg`.`bill_total_amount`) AS `bill_total_amount`, SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` `acg`
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`,`acg`.`deal_id`;
