-- Agrega los campos necesarios para mandar SMSs a user_profiles
alter table `user_profiles` add `is_sms_enabled` tinyint (1)  default false after `zip_code`;
alter table `user_profiles` add `cell_number`    varchar (8)                after `zip_code`;
alter table `user_profiles` add `cell_prefix`    varchar (5)  default  '11' after `zip_code`;
alter table `user_profiles` add `cell_carrier`   bigint  (10)               after `zip_code`;