-- http://jira.int.clarin.com/browse/CC-4808
INSERT INTO `payment_option_plans` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (now(), now(), 0, 0, 0, 1, 3, 40);
INSERT INTO `payment_option_plans` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (now(), now(), 0, 0, 0, 2, 3, 10);
INSERT INTO `payment_option_plans` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (now(), now(), 0, 0, 0, 3, 3, 30);
INSERT INTO `payment_option_plans` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (now(), now(), 0, 0, 0, 4, 3, 20);
