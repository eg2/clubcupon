TRUNCATE TABLE search_links;

-- Bs As Norte, Bs As Oeste, Bs As Sur, Ciudad de Bs As

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos Aires Norte&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos Aires Oeste&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos Aires Sur&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad de Buenos Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('3', 'Salud y Belleza', '/especial-belleza', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('3', 'Salud y Belleza', '/especial-belleza', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('3', 'Salud y Belleza', '/especial-belleza', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('3', 'Salud y Belleza', '/especial-belleza', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('4', 'Ofertas gastronómicas', '/festival-gastronomico', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('4', 'Ofertas gastronómicas', '/festival-gastronomico', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('4', 'Ofertas gastronómicas', '/festival-gastronomico', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('4', 'Ofertas gastronómicas', '/festival-gastronomico', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('5', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=buenos-aires-norte&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('5', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=buenos-aires-oeste&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('5', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=buenos-aires-sur&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('5', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=ciudad-de-buenos-aires&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Ofertas seleccionadas', '/ofertas-para-vos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Ofertas seleccionadas', '/ofertas-para-vos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Ofertas seleccionadas', '/ofertas-para-vos', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Ofertas seleccionadas', '/ofertas-para-vos', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Indumentaria', '/especial-indumentaria', ( SELECT id FROM cities WHERE slug = 'buenos-aires-norte'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Indumentaria', '/especial-indumentaria', ( SELECT id FROM cities WHERE slug = 'buenos-aires-oeste'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Indumentaria', '/especial-indumentaria', ( SELECT id FROM cities WHERE slug = 'buenos-aires-sur'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('6', 'Indumentaria', '/especial-indumentaria', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

-- Córdoba, La Plata, Mendoza, Rosario

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos-para-el-interior', ( SELECT id FROM cities WHERE slug = 'cordoba'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos-para-el-interior', ( SELECT id FROM cities WHERE slug = 'la-plata'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos-para-el-interior', ( SELECT id FROM cities WHERE slug = 'mendoza'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Productos', '/productos-para-el-interior', ( SELECT id FROM cities WHERE slug = 'rosario'));

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Viajes y Escapadas', '/turismo', ( SELECT id FROM cities WHERE slug = 'cordoba'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Viajes y Escapadas', '/turismo', ( SELECT id FROM cities WHERE slug = 'la-plata'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Viajes y Escapadas', '/turismo', ( SELECT id FROM cities WHERE slug = 'mendoza'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Viajes y Escapadas', '/turismo', ( SELECT id FROM cities WHERE slug = 'rosario'));

-- Productos

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Ciudad de Buenos Aires', '/ciudad-de-buenos-aires', ( SELECT id FROM cities WHERE slug = 'productos'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=productos&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'productos'));

-- Viajes y Escapadas

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Ofertas Nacionales', '/ofertas-nacionales', ( SELECT id FROM cities WHERE slug = 'turismo'));

-- Especial Belleza

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Ciudad de Buenos Aires', '/ciudad-de-buenos-aires', ( SELECT id FROM cities WHERE slug = 'especial-belleza'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Especial Indumentaria', '/especial-indumentaria', ( SELECT id FROM cities WHERE slug = 'especial-belleza'));

-- Festival gastronómico

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Ciudad de Buenos Aires', '/ciudad-de-buenos-aires', ( SELECT id FROM cities WHERE slug = 'festival-gastronomico'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Vinos y más', '/search/search_querys/makesearch?q=vinos&qr=reset&currentCitySlug=festival-gastronomico&fromSearchPg=yes', ( SELECT id FROM cities WHERE slug = 'festival-gastronomico'));

-- Ofertas Nacionales

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Viajes y Escapadas', '/turismo', ( SELECT id FROM cities WHERE slug = 'ofertas-nacionales'));

-- Vinos y más
-- Aún no hay Slug

-- Ofertas para vos
-- Aún no hay Slug

-- Especial Indumentaria

INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('1', 'Ciudad de Buenos Aires', '/ciudad-de-buenos-aires', ( SELECT id FROM cities WHERE slug = 'especial-indumentaria'));
INSERT INTO `search_links` (`priority`, `label`, `url`,  `city_id`) VALUES ('2', 'Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'especial-indumentaria'));


