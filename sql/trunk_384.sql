-- https://artear.atlassian.net/browse/CC-5731
ALTER TABLE `product_inventories`
    ADD COLUMN `price` DECIMAL(10,2) NULL AFTER `source_identifier`,
    ADD COLUMN `last_price_update` DATETIME NULL AFTER `price`;