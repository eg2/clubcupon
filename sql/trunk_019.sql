-- update de states con las prov de argentina.
INSERT INTO `states` VALUES  (null,'Catamarca','','',1,284);
INSERT INTO `states` VALUES  (null,'Chaco','','',1,284);
INSERT INTO `states` VALUES  (null,'Chubut','','',1,284);
INSERT INTO `states` VALUES  (null,'Córdoba','','',1,284);
INSERT INTO `states` VALUES  (null,'Corrientes','','',1,284);
INSERT INTO `states` VALUES  (null,'Entre Ríos','','',1,284);
INSERT INTO `states` VALUES  (null,'Formosa','','',1,284);
INSERT INTO `states` VALUES  (null,'Jujuy','','',1,284);
INSERT INTO `states` VALUES  (null,'La Pampa','','',1,284);
INSERT INTO `states` VALUES  (null,'La Rioja','','',1,284);
INSERT INTO `states` VALUES  (null,'Mendoza','','',1,284);
INSERT INTO `states` VALUES  (null,'Misiones','','',1,284);
INSERT INTO `states` VALUES  (null,'Neuquén','','',1,284);
INSERT INTO `states` VALUES  (null,'Río Negro','','',1,284);
INSERT INTO `states` VALUES  (null,'Salta','','',1,284);
INSERT INTO `states` VALUES  (null,'San Juan','','',1,284);
INSERT INTO `states` VALUES  (null,'San Luis','','',1,284);
INSERT INTO `states` VALUES  (null,'Santa Cruz','','',1,284);
INSERT INTO `states` VALUES  (null,'Santa Fe','','',1,284);
INSERT INTO `states` VALUES  (null,'Santiago del Estero','','',1,284);
INSERT INTO `states` VALUES  (null,'Tierra del Fuego','','',1,284);
INSERT INTO `states` VALUES  (null,'Tucumán','','',1,284);

SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `city_perms` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) not null,
  `city_id` BIGINT(20) UNSIGNED not null,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_city_id_perms` (`city_id`),
  CONSTRAINT `FK_city_id_perms` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON UPDATE NO ACTION,
  KEY `FK_user_id_perms` (`user_id`),
  CONSTRAINT `FK_user_id_perms` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cities perms';
SET FOREIGN_KEY_CHECKS=1;

INSERT INTO `user_types` (`id`, `created`, `modified`, `name`) VALUES ('5', '2010-12-28 12:26:31', '2010-12-28 12:26:33', 'Partner');