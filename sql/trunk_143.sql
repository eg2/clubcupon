DROP TABLE IF EXISTS `redemptions`;
CREATE TABLE IF NOT EXISTS `redemptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `modified_by` bigint(20) unsigned DEFAULT NULL,
  `deal_user_id` bigint(20) unsigned DEFAULT NULL,
  `posnet_code` varchar(10) NOT NULL,
  `way` varchar(8) DEFAULT NULL,
  `redeemed` datetime DEFAULT NULL,
  `sent` datetime DEFAULT NULL,
  `reported_new` bool DEFAULT false,
  `reported_used` bool DEFAULT false,
  `reported_expired` bool DEFAULT false,
  `expired` bool DEFAULT false,

  PRIMARY KEY (`id`),
  UNIQUE KEY `posnet_code_uk` (`posnet_code`),
  UNIQUE KEY `deal_user_id_uk` (`deal_user_id`),
  CONSTRAINT deal_user_id_fk FOREIGN KEY (deal_user_id) REFERENCES deal_users (id) ON UPDATE RESTRICT ON DELETE RESTRICT
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



DROP TABLE IF EXISTS `redemptions_exports_history`;
CREATE TABLE IF NOT EXISTS `redemptions_exports_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp DEFAULT CURRENT_TIMESTAMP,
  `name` text NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

