INSERT INTO `email_templates` (`id`,`created`,`modified`,`from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`) VALUES (NULL,'2014-04-23 17:44:00','2014-04-23 17:44:00','##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Confirm Subscription', 'Mail de Confirmacion de Suscripcion', 'Gracias por elegirnos','
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>&iexcl;Gracias por elegirnos!</title>

</head>

<body style="background:#EEEEEE; font:12px Arial;">
<table width="593" height="300" align="center" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#4d4d4d;">
            <tbody>
            <tr>
                <td valign="top" height="93">
                  <table>
                    <tr> 
                      <td>
                        <img src="##ABSOLUTE_IMG_PATH##/header_deal_bought.jpg" alt="Gracias por tu compra" width="300" height="100" style="float:left;" />
                      </td>
                      <td style="text-align: center; background-color: #efedee !important">
                        <span style="font-family:Arial, Helvetica, sans-serif;font-size:26px;" > &iexcl;Gracias por elegirnos!</span>
                      </td>
                    </tr>
                  </table>
                </td>
            </tr>
            <tr style="background:#fff;">
                  <td style="padding:10px;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                        <br><b>Hola ##USERNAME##</b><br><br><br>
                        
                        <strong>Para Confirmar tu suscripcion has click </strong><a href="##CONFIRM_SUBSCRIPTION_URL##" style="color:#006699; text-decoration:none;">Aqui</a><br>
                        
                        <div align="left" style="margin-left:400px;">
                            <font>
                                <b>El equipo de Club Cup&oacute;n</b><br>
                                www.clubcupon.com.ar
                            </font>
                        </div>
                  </td>
            </tr>
            <tr>
            
                <td style="background:#FFF; border-top: 1px solid #ccc">
                    <table width="100%" border="0" cellpadding="15" cellspacing="0">
                      <tr>
                        <td><div align="left" style="width:300px;"> <font>ClubCup&oacute;n&reg; es un servicio de TECDIA S.A.<br />
                              <a href="http://soporte.clubcupon.com.ar" style="color:#006699; text-decoration:none;">Centro de Atenci&oacute;n al Cliente</a></font> </div>
							  <br />
							  <span style="font-size:10px">Este es un env&iacute;o autom&aacute;tico de mail. Por favor no responda el mismo. Gracias</span>
							  </td>
                        <td align="right"><img src="##ABSOLUTE_IMG_PATH##/logo_footer_deal_bought.jpg" width="130" height="43" align="right" style="display:block;" />
						</td>
                      </tr>
                    </table>
					
											
                    </td>
            </tr>
        </tbody>
      </table> 
</body>
</html>
', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_TITLE, DESCRIPTIVE_TEXT, QUANTITY, DEAL_AMOUNT , PAYMENT_TYPE ', '1');
