-- https://artear.atlassian.net/browse/CC-5548
ALTER TABLE `cities` ADD COLUMN `week_days` VARCHAR(7) NULL DEFAULT NULL;
