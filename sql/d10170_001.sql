DROP TABLE IF EXISTS `product_inventories`;
CREATE TABLE IF NOT EXISTS `product_inventories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted`  tinyint(1) DEFAULT 0,
  `name`  varchar(256) NOT NULL,
  `stock` int(11) unsigned  DEFAULT 0,
  `description`  varchar(256) DEFAULT NULL,
  `bac_product_id`  bigint(20) unsigned NOT NULL comment 'product id sent by BAC as sku, replace the id_producto in bac_service component',
  `bac_libertya_id`  varchar(256) DEFAULT NULL comment 'IdProductoLy in BAC',
  `bac_invoice_id`  varchar(256) DEFAULT NULL comment 'IdErp in BAC',
  `bac_portal_id`  varchar(256) DEFAULT NULL comment 'Portal in BAC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='product_inventories to create Bac products' AUTO_INCREMENT=0;


ALTER TABLE product_products ADD column product_inventory_id bigint(20) unsigned DEFAULT NULL;

ALTER TABLE product_inventories MODIFY column stock DECIMAL(11,2) DEFAULT 0;