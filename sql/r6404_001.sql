ALTER TABLE cities
 ADD is_corporate TINYINT(1) NOT NULL DEFAULT '0' AFTER is_solapa;
 
 UPDATE cities SET is_corporate=1 WHERE slug IN (
    'exclusive',
    'mcdonalds',
    'zurich',
    'grandt',
    'mapfre',
    'tarjeta-naranja',
    'beneficios-grupo',
    'exclusivo-cmd'
 );