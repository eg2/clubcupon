-- Añadiendo columna nuevas de deal_user a cupones eliminados.
alter table anulled_coupons add column deleted TINYINT(1) DEFAULT 0;
alter table anulled_coupons add column is_returned INT(11) DEFAULT 0;