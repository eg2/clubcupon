---Agrega campos solictados en ER_fact+liq.v11 - 2.2.3
ALTER TABLE `companies` ADD `percepcion_iibb_caba` BOOLEAN NOT NULL AFTER `fiscal_iibb` ;
ALTER TABLE `companies` ADD `declaracion_jurada_terceros` BOOLEAN NOT NULL AFTER `fiscal_bank_cbu`;
ALTER TABLE `companies` ADD `persona` BOOLEAN NOT NULL AFTER `fiscal_bank_cbu` ;
ALTER TABLE `companies` ADD `cheque_noorden` INT NOT NULL AFTER `fiscal_bank_cbu` ;
ALTER TABLE `companies` ADD `created_by` BIGINT( 20 ) NOT NULL AFTER `bac_tourism_id` ,
ADD `modified_by` BIGINT( 20 ) NOT NULL AFTER `created`;
ALTER TABLE `companies` ADD `validated` BOOLEAN NOT NULL;
