CREATE OR REPLACE VIEW `accounting_company_id_grouped` AS
SELECT liquidate.id AS liquidate_ids,
      `deal`.`company_id` AS `deal_company_id`,
      `deal`.`pay_by_redeemed` AS `pay_by_redeemed`,
      CAST(`deal`.`start_date` AS DATE) AS `deal_start_date`,
      `liquidate`.`total_quantity` AS `liquidate_total_quantity`,
      `liquidate`.`id` AS `liquidate_id`,
      `liquidate`.`discounted_price` AS `liquidate_discounted_price`,
      if(liquidate.accounting_type = 'LIQUIDATE_BY_SOLD' , `liquidate`.`liquidating_guarantee_fund_percentage`, 0) AS `liquidate_guarantee_fund_percentage`,
      if(liquidate.accounting_type = 'LIQUIDATE_BY_SOLD',  ROUND(((`liquidate`.`discounted_price` * `liquidate`.`total_quantity`) * (`liquidate`.`liquidating_guarantee_fund_percentage` / 100)),2), 0) AS `liquidate_guarantee_fund_amount`,
      if(liquidate.accounting_type = 'LIQUIDATE_BY_SOLD',  ROUND(((`liquidate`.`discounted_price` * `liquidate`.`total_quantity`) * (`liquidate`.`liquidating_guarantee_fund_percentage` / 100)),2), 0) AS `liquidate_guarantee_fund_amount_neg`,
      if(liquidate.accounting_type = 'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',  liquidate.total_amount, if(liquidate.accounting_type = 'LIQUIDATE_BY_SOLD_EXPIRED', liquidate.total_amount - ((liquidate.total_quantity * liquidate.discounted_price)-bill.total_amount), 0)) AS `liquidate_guarantee_fund_amount_pos`,
      IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`,
      `liquidate`.`total_amount` AS `liquidate_total_amount`,
      CAST(IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()), (`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY), (`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS DATE) AS `max_payment_date`,
      `deal`.`id` AS `deal_id`,
      `deal`.`name` AS `deal_name`,
      `deal`.commission_percentage AS commission_percentage,
      `city`.`name` AS `city_name`,
      `deal`.`downpayment_percentage` AS `downpayment_percentage`,
      liquidate.accounting_type AS accounting_type
FROM ((((`accounting_items` `liquidate`
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `companies` `company` ON((`liquidate`.`model_id` = `company`.`id`)))
JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0)
  AND (`liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED'))
  AND (`liquidate`.`model` = 'Company')
  AND (`company`.`validated` = 1)
  AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)));


CREATE OR REPLACE VIEW `accounting_deal_id_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,
      `acg`.`max_payment_date` AS `max_payment_date`,
      `acg`.`pay_by_redeemed` AS `pay_by_redeemed`,
      `acg`.`deal_id` AS `deal_id`,
      `acg`.`deal_name` AS `deal_name`,
      `acg`.`deal_start_date` AS `deal_start_date`,
      `acg`.`city_name` AS `city_name`,
      `acg`.`liquidate_guarantee_fund_percentage` AS `liquidate_guarantee_fund_percentage`,
      `acg`.`liquidate_id` AS `liquidate_id`,
      SUM(`acg`.`liquidate_guarantee_fund_amount_pos`) - SUM(`acg`.`liquidate_guarantee_fund_amount_neg`) AS `liquidate_guarantee_fund_amount`,
      SUM(`acg`.`liquidate_guarantee_fund_amount_neg`) AS `liquidate_guarantee_fund_amount_neg`,
      SUM(`acg`.`liquidate_guarantee_fund_amount_pos`) AS `liquidate_guarantee_fund_amount_pos`,
      SUM(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`,
      SUM(`acg`.`bill_total_amount`) AS `bill_total_amount`,
      SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`,
      `acg`.liquidate_discounted_price AS liquidate_discounted_price,
      MAX(if(acg.accounting_type = 'LIQUIDATE_BY_SOLD_EXPIRED' OR acg.accounting_type = 'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL', 1, 0)) as has_expired,
   --   group_concat(`acg`.`liquidate_id`) AS `liquidateids`,
   --   count(1),
   --   group_concat(acg.accounting_type),
      acg.accounting_type AS accounting_type
FROM `accounting_company_id_grouped` `acg`
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`,`acg`.`deal_id`;
