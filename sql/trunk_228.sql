update companies set latitude = '-34.6036', longitude = '-58.3815' where latitude = 0 or latitude is null;
alter table companies modify column latitude float default '-34.6036';
alter table companies modify column longitude float default '-58.3815';
update deals set priority = 0 where priority is null;
alter table deals modify column priority int(11) default 0;
