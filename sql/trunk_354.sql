alter table email_profiles add column previous_frecuency_event_class_id bigint(20) unsigned;
alter table email_profiles add FOREIGN KEY fk_frecuency_event_classes_id (`frecuency_event_class_id`) REFERENCES frecuency_event_classes (`id`);
alter table email_profiles add FOREIGN KEY fk_frecuency_event_classes_previus_id (`previus_frecuency_event_class_id`) REFERENCES frecuency_event_classes (`id`);
