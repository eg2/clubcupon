-- http://tracker.int.clarin.com/browse/CC-4381
INSERT INTO `email_templates` (`created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`) VALUES (NOW(), NOW(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Deal Bought Beneficios', 'Compra de Mis Beneficios', '##SUBJECT##', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>&iexcl;Gracias por tu compra!</title>
    </head>

    <body style="background: #f7f7f7;">
        <table width="593" align="center" cellpadding="0" cellspacing="0" height="300" style="background: #fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#4d4d4d;">
            <tr>
                <td valign="top" height="93">
                    <img alt="Cup&oacute;n" src="##ABSOLUTE_IMG_PATH##/coupon_web/logo.png" border="0"/>
                </td>
            </tr>
            <tr style="background: #fff;">
                  <td style="padding:10px; border-left:1px solid #ddd; border-right:1px solid #ddd;">
                        <b>Hola ##USERNAME##</b><br /><br /><br />
                        <strong>Los detalles de la compra son:</strong><br />
                        <br />
                        Nombre:   <strong>  ##DEAL_TITLE##    </strong><br />
                        Cantidad: <strong>  ##QUANTITY##      </strong><br />
                        Precio:   <strong>  ##DEAL_AMOUNT##   </strong><br />
                        ##CREDIT_USED_DESCRIPTION##   <strong>  ##CREDIT_USED##   </strong><br />
                        ##EXTERNAL_PAID_HTML##
                        Fecha de compra: <strong>##PURCHASE_ON##</strong><br /><br />
                        <b style="color:#F60">Tu pago se ha procesado exitosamente, en la pr&oacute;xima hora recibir&aacute;s el cup&oacute;n y </b> <br /> 
  						<b style="color:#F60">toda la informaci&oacute;n necesaria para que puedas usarlo. </b> <br />
				 		 
                       
                        <b>&iexcl;Gracias!</b><br /><br />

                        <div align="left" style=" width:150px; margin-left:400px;">
                            <font>
                                <b>El equipo de Nuestros Beneficios</b><br />

                                www.nuestrosbeneficios.com.ar
                            </font>
                        </div>
                  </td>
            </tr>
            <tr>
                <td style="background: #f7f7f7;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #cccccc; border-bottom:1px solid #cccccc; margin-top:20px;">
                        <tr>
                          <td valign="top" style="margin:0; height:110px;">
                          <p style="margin:0;">Ante cualquier consulta escribinos a <a href="mailto:soporte@nuestrosbeneficios.com" style="color:#2d6f95;text-decoration:none" target="_blank">soporte@nuestrosbeneficios.com</a></p></td>
                          <td valign="top"><img src="##ABSOLUTE_IMG_PATH_2####COUPON_URL_IMG_FOOTER##" alt="#" width="183" height="110" border="0" align="top"></td>
                        </tr>
                     </table>
                </td>
            </tr>
        </table>
    </body>

</html>', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_TITLE, DESCRIPTIVE_TEXT, QUANTITY, DEAL_AMOUNT , PAYMENT_TYPE', 1);
