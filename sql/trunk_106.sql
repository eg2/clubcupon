-- Agrega vendedor como tipo de usuario a la tabla 'user_types'

INSERT INTO `user_types` (
`id` ,
`created` ,
`modified` ,
`name`
)
VALUES (
NULL , '2011-08-30 10:33:20', NULL , 'Vendedor'
);

-- Agrega el campo "seller_id" a deals para identificar al vendedor de la oferta
ALTER TABLE `deals` ADD `seller_id` BIGINT( 20 ) NULL DEFAULT NULL AFTER `user_id` ;
