-- http://jira.int.clarin.com/browse/CC-5224
ALTER TABLE `product_products` ADD COLUMN `decremented_units` INT(11) UNSIGNED NOT NULL DEFAULT '1';

UPDATE `product_products` SET `decremented_units` = 1;