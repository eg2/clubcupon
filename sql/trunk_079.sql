-- Migra los datos: buscando los cupones nulados con id I que tengan un pin asociado,
--   pone el campo "id de cupon" en tales pines en I
update `pins` `p`, `anulled_coupons` `ac`
   set `p`.`deal_user_id` = `ac`.`id`
where 0 = 0
  and `ac`.`pin_id` = `p`.`id`;
commit;

-- Elimina el campo "id de pin" de los cupones anulados
alter table `anulled_coupons` drop column `pin_id`;
-- Agrega los campos faltantes
alter table `anulled_coupons` add `gift_dni` varchar (255) after `gift_email`;
alter table `anulled_coupons` add `gift_dob` date          after `gift_dni`  ;