-- http://tracker.int.clarin.com/browse/CC-4391
UPDATE payment_methods SET name='Visa ICBC'
WHERE name='Visa Standard Bank';

UPDATE payment_methods SET name='Mastercard ICBC'
WHERE name='Mastercard Standard Bank';

UPDATE payment_options SET name = 'Visa ICBC', description='Visa ICBC por NPS' 
WHERE name='Visa Standard Bank';

UPDATE payment_options SET name = 'MasterCard ICBC', description='MasterCard ICBC por NPS' 
WHERE name='MasterCard Standard Bank';

