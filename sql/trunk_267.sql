-- http://jira.int.clarin.com/browse/CC-3373
-- ATENCION: no incluir en releases posteriores a r6618.6619

UPDATE payment_option_deals SET 
payment_plan_id = NULL
WHERE payment_plan_id IS NOT NULL;

DELETE FROM payment_option_plans;