ALTER TABLE users
ADD COLUMN `token` CHAR(40) default NULL,
ADD COLUMN `token_used` DATETIME default NULL,
ADD COLUMN `token_uses` INT NOT NULL default 0,
ADD UNIQUE KEY `token`(`token`);

