CREATE TABLE IF NOT EXISTS `subscription_candidates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` bigint(20) DEFAULT '0',
  `city_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `campaign` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_subscribed` tinyint(1) DEFAULT '1',
  `is_voluntary` tinyint(1) DEFAULT '1',
  `utm_source` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_medium` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_term` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_content` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_campaign` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscription_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `available_balance_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_subscriptions_users1` (`user_id`),
  KEY `fk_subscriptions_cities1` (`city_id`),
  KEY `email` (`email`),
  KEY `created` (`created`),
  KEY `city_id_email` (`city_id`,`email`),
  KEY `city_id` (`city_id`,`email`,`is_subscribed`)
  ) ENGINE=InnoDB AUTO_INCREMENT=32215735 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `subscription_candidates` ADD UNIQUE INDEX(email, city_id);

ALTER TABLE `subscription_candidates` ADD CONSTRAINT `subscriptions_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `subscription_candidates` ADD CONSTRAINT `subscriptions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

