-- http://tracker.int.clarin.com/browse/CC-3635

INSERT INTO `payment_options`
    (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
    (104, 6, 1, 22, 'Bancor Cordobesa', 'MasterCard por NPS', 0);

INSERT INTO `payment_options`
    (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
    (105, 6, 1, 22, 'Bancor Fan', 'MasterCard por NPS', 0);
--
INSERT INTO `payment_options`
    (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
    (106, 6, 15, 92, 'Bancor Cordobesa', 'MasterCard por NPS Turismo', 0);

INSERT INTO `payment_options`
    (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES
    (107, 6, 15, 92, 'Bancor Cordobesa', 'MasterCard por NPS Turismo', 0);
--
INSERT INTO `payment_option_plans` 
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES 
    (11, now(), now(), 0, 0, 0, 1, 106, 40);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
    (12, now(), now(), 0, 0, 0, 2, 106, 10);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES 
    (13, now(), now(), 0, 0, 0, 3, 106, 30);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES (14, now(), now(), 0, 0, 0, 4, 106, 20);
--
INSERT INTO `payment_option_plans` 
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES 
    (15, now(), now(), 0, 0, 0, 1, 107, 40);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
    (16, now(), now(), 0, 0, 0, 2, 107, 10);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES 
    (17, now(), now(), 0, 0, 0, 3, 107, 30);

INSERT INTO `payment_option_plans`
    (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES 
    (18, now(), now(), 0, 0, 0, 4, 107, 20);