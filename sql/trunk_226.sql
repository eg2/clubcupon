alter table deal_externals add column discounted_amount decimal(10,2);
alter table deal_externals drop column gift_amount;
alter table deal_externals add column gifted_amount decimal(10,2) default 0;
