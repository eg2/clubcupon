-- Agrega una "setting" que penda de la "setting_category" con id = 19
insert into `settings` (`setting_category_id`, `name`, `value`, `description`, `type`, `options`, `label`, `order`) values
(19, 'EmailTemplate.from_email_sales',     'ventas@clubcupon.com.ar', 'You can make changes in your from address used for sales-related emails using this value.',     'text', null, 'Sales From Email Address',     4),
(19, 'EmailTemplate.reply_to_email_sales', 'ventas@clubcupon.com.ar', 'You can make changes in your reply to address used for sales-related emails using this value.', 'text', null, 'Sales Reply To Email Address', 5);
commit;