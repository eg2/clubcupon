-- Por ahora no cambiamos el nombre de la ciudad, por eso queda comentado:
-- UPDATE cities SET name = 'Ciudad Autónoma de Buenos Aires' WHERE name='Ciudad de Buenos Aires';

UPDATE cities SET is_approved=1,name='GBA y Pcia. de Bs.As.',state_id=(SELECT id FROM states ORDER BY id LIMIT 1) WHERE name='Buenos Aires';
INSERT INTO neighbourhoods (created,modified,city_id,name)
VALUES
(NOW(),NOW(),-1,'Acasusso'),
(NOW(),NOW(),-1,'Adrogué'),
(NOW(),NOW(),-1,'Avellaneda'),
(NOW(),NOW(),-1,'Berazategui'),
(NOW(),NOW(),-1,'Campana'),
(NOW(),NOW(),-1,'Cañuelas'),
(NOW(),NOW(),-1,'Capilla del Señor'),
(NOW(),NOW(),-1,'Cardales'),
(NOW(),NOW(),-1,'Del Viso'),
(NOW(),NOW(),-1,'Delta'),
(NOW(),NOW(),-1,'Don Torcuato'),
(NOW(),NOW(),-1,'Escobar-Mashwitz'),
(NOW(),NOW(),-1,'Ezeiza'),
(NOW(),NOW(),-1,'Guernica'),
(NOW(),NOW(),-1,'Haedo'),
(NOW(),NOW(),-1,'Hurlingham'),
(NOW(),NOW(),-1,'Ituzaingó'),
(NOW(),NOW(),-1,'José León Suarez'),
(NOW(),NOW(),-1,'La Lucila'),
(NOW(),NOW(),-1,'La Plata'),
(NOW(),NOW(),-1,'Lanús'),
(NOW(),NOW(),-1,'Lomas de Zamora'),
(NOW(),NOW(),-1,'Luján'),
(NOW(),NOW(),-1,'Martínez'),
(NOW(),NOW(),-1,'Mercedes'),
(NOW(),NOW(),-1,'Merlo'),
(NOW(),NOW(),-1,'Morón'),
(NOW(),NOW(),-1,'Olivos'),
(NOW(),NOW(),-1,'Pilar'),
(NOW(),NOW(),-1,'Quilmes'),
(NOW(),NOW(),-1,'Ramos Mejía'),
(NOW(),NOW(),-1,'Ranelah'),
(NOW(),NOW(),-1,'San Andrés de Giles'),
(NOW(),NOW(),-1,'San Fernando'),
(NOW(),NOW(),-1,'San Isidro'),
(NOW(),NOW(),-1,'San Martín'),
(NOW(),NOW(),-1,'San Miguel'),
(NOW(),NOW(),-1,'San Pedro'),
(NOW(),NOW(),-1,'Santos Lugares'),
(NOW(),NOW(),-1,'Tigre'),
(NOW(),NOW(),-1,'Vicente López'),
(NOW(),NOW(),-1,'Villa Ballester'),
(NOW(),NOW(),-1,'Villa Luzuriaga'),
(NOW(),NOW(),-1,'Zárate');

UPDATE neighbourhoods SET modified=NOW(),city_id=(SELECT id FROM cities WHERE name='GBA y Pcia. de Bs.As.') WHERE city_id=-1;
