-- Se agraga las columnas para registrar los datos de las distintas campañas
ALTER TABLE `subscriptions` ADD `utm_medium` VARCHAR( 64 ) NULL DEFAULT NULL AFTER `utm_source`;
ALTER TABLE `subscriptions` ADD `utm_term` VARCHAR( 64 ) NULL DEFAULT NULL AFTER `utm_medium`;
ALTER TABLE `subscriptions` ADD `utm_content` VARCHAR( 64 ) NULL DEFAULT NULL AFTER `utm_term`;
ALTER TABLE `subscriptions` ADD `utm_campaign` VARCHAR( 64 ) NULL DEFAULT NULL AFTER `utm_content`;