-- http://jira.int.clarin.com/browse/CC-5362

ALTER TABLE `search_links` ADD COLUMN `description` VARCHAR(150) NULL;