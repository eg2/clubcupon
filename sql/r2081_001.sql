DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512),
  `created` datetime,
  `description` varchar(512),
  `model` varchar(512),
  `model_id` bigint(20) unsigned,
  `action` varchar(512),
  `user_id` int(11),
  `change` text COLLATE utf8_unicode_ci,
  PRIMARY KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;