-- -------------------------------------------------------------------------------------------------
-- -- Pines (dados por las comapñias) --------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------

create table `pins`
  (
    `id`           BIGINT(20)    unsigned     not null  AUTO_INCREMENT,
    `created`      DATETIME               default null,
    `created_by`   BIGINT(20)             default null,
    `modified`     DATETIME               default null,
    `modified_by`  BIGINT(20)             default null,
    `code`         VARCHAR(255)               not null,
    `deal_id`      BIGINT(20)    unsigned     not null,
    `is_used`      TINYINT(1)             default FALSE,
    --
    primary key (`id`),
    --
    foreign key (`deal_id`) references `deals`(`id`)
  ) ENGINE = INNODB  DEFAULT CHARSET = utf8  COLLATE = utf8_unicode_ci  COMMENT = 'PINs';
--
create index `pins_id` on `pins`(`id`);


-- -------------------------------------------------------------------------------------------------
-- -- Agrega informacion de pines a los cupones ----------------------------------------------------
-- -------------------------------------------------------------------------------------------------

alter table `deal_users` add `pin_id`   BIGINT(20) unsigned after `coupon_code`;
alter table `deal_users` add `pin_code` VARCHAR(255)        after `pin_id`     ;
--
alter table `deal_users` add foreign key (`pin_id`) references `pins`(`id`);


-- -------------------------------------------------------------------------------------------------
-- -- Agrega informacion de pines a deals ----------------------------------------------------------
-- -------------------------------------------------------------------------------------------------

alter table `deals` add `has_pins` TINYINT(1) default FALSE after `accepts_points`;