-- se agregan defaults a varias columnas de deals que no los tenian.

alter table `deals` alter `accepts_points`           set default 0;
alter table `deals` alter `start_date`               set default '0000-00-00 00:00:00.00';
alter table `deals` alter `end_date`                 set default '0000-00-00 00:00:00.00';
alter table `deals` alter `min_limit`                set default 0;
alter table `deals` alter `deal_user_count`          set default 0;
alter table `deals` alter `closure_modality`         set default 0;
alter table `deals` alter `deferred_payment`         set default 0;
alter table `deals` alter `first_closure_percentage` set default 0;
alter table `deals` alter `payment_method`           set default 0;
alter table `deals` alter `downpayment_percentage`   set default 0;