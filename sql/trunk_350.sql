-- http://jira.int.clarin.com/browse/CC-4872
update
  search_links
set
  label = 'Entretenimiento',
  url = 'search/search_querys/makesearch?setHFacetField=deal_category_path_l1&setFacet=Entretenimiento &scV=-city--turismo-page--1-query--oooo'
where
  city_id = 43496
  and label = 'Ofertas Nacionales';