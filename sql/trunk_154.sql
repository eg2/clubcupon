-- campos de auditoria
ALTER TABLE `cities` ADD `created_by` BIGINT( 20 ) NOT NULL;
ALTER TABLE `cities` ADD `modified_by` BIGINT( 20 ) NOT NULL;
ALTER TABLE `cities` ADD `created_ip` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `cities` ADD `modified_ip` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL; 