-- https://artear.atlassian.net/browse/CC-5755
alter table `payment_settings`
    add column `account` varchar(255) not null after `name`,
    add column `gateway` varchar(255) not null after `account`;
--
update payment_settings set account = 'Normal'            , gateway = 'NPS'         where id = 1 ; -- "Por NPS"
update payment_settings set account = 'Todos'             , gateway = 'NO'          where id = 3 ; -- "Por Puntos"
update payment_settings set account = 'Todos'             , gateway = 'NO'          where id = 4 ; -- "Por Monedero"
update payment_settings set account = 'Todos'             , gateway = 'NO'          where id = 8 ; -- "Todos"
update payment_settings set account = 'Normal'            , gateway = 'NPS'         where id = 10; -- "Por Standard Bank"
update payment_settings set account = 'Normal'            , gateway = 'MercadoPago' where id = 11; -- "Por MercadoPago Checkout"
update payment_settings set account = 'Normal'            , gateway = 'MercadoPago' where id = 12; -- "Por MercadoPago Checkout Offline"
update payment_settings set account = 'Productos'         , gateway = 'MercadoPago' where id = 13; -- "Por MercadoPago Productos"
update payment_settings set account = 'Turismo'           , gateway = 'MercadoPago' where id = 14; -- "Por MercadoPago Turismo"
update payment_settings set account = 'Turismo'           , gateway = 'NPS'         where id = 15; -- "Por NPS Turismo"
update payment_settings set account = 'Normal'            , gateway = 'NPS'         where id = 16; -- "Por NPS PagoMisCuentas"
update payment_settings set account = 'NuestrosBeneficios', gateway = 'MercadoPago' where id = 17; -- "Por MercadoPago NuestrosBeneficios"
update payment_settings set account = 'NuestrosBeneficios', gateway = 'NPS'         where id = 18; -- "Por NPS NuestrosBeneficios"