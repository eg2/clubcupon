-- Agrega el campo:
--   datetime con "la fecha en la que se pagó a la compañia" o null si no
ALTER TABLE `deal_users` ADD `paid_date` datetime DEFAULT NULL AFTER `payment_type_id`;

ALTER TABLE `anulled_coupons` ADD `paid_date` datetime DEFAULT NULL AFTER `payment_type_id`;