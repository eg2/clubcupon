CREATE TABLE `payment_plans` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`name` VARCHAR(255) NOT NULL,
PRIMARY KEY (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `payment_plan_options` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`payment_plan_id` BIGINT(20) UNSIGNED NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`interest` DECIMAL(10,2) NULL DEFAULT '0.00',
	`fees` INT(10) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`id`),
INDEX `payment_plan_options_plan_id` (`payment_plan_id`),
CONSTRAINT `payment_plan_options_payment_plans_fk` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `payment_option_plans` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`payment_plan_id` BIGINT(20) UNSIGNED NOT NULL,
	`payment_option_id` BIGINT(20) UNSIGNED NOT NULL,
PRIMARY KEY (`id`),
INDEX `payment_option_plan_options_payment_plan_id` (`payment_plan_id`),
INDEX `payment_option_id` (`payment_option_id`),
CONSTRAINT `payment_option_plan_options_payment_plans_fk` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
CONSTRAINT `payment_option_plan_options_payment_options_fk` FOREIGN KEY (`payment_option_id`) REFERENCES `payment_options` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;