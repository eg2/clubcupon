-- Elimina las "settings" de emails de ventas defualt
delete from `settings` where `name` in ('EmailTemplate.from_email_sales', 'EmailTemplate.reply_to_email_sales');
commit;