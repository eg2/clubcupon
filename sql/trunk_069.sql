drop table IF  EXISTS `cluster_definitions`;
ALTER TABLE `cluster_examples` ADD `cluster_id` BIGINT(20) unsigned NOT NULL ;
ALTER TABLE `deals` ADD `cluster_id` BIGINT(20) unsigned NULL  ;
ALTER TABLE `cluster_examples` DROP COLUMN `email`;

-- Set de datos de ejemplo
INSERT INTO clusters(id, name) VALUES (1, 'Activos');

INSERT INTO cluster_examples (id, dob, gender, localidad, cluster_id, is_active) VALUES (1, null ,null, null, 1, 1);