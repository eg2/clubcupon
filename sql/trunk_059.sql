-- Agrega el campo "cuando fue referido este usuario"

alter table `users` add `referred_date` DATETIME default null after `referred_by_user_id`;