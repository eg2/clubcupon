------------------------------------------------------------------------------
-- Actualiza parent_deal_id para el nuevo significado.
------------------------------------------------------------------------------
update `deals`
set `parent_deal_id` = `id`
where `parent_deal_id` is null;
commit;