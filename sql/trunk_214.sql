-- Agrega el campo redirect_url, utilizado para determinar la url de redireccion cuando se accede directamente a una oferta agotada.
ALTER TABLE `deals` ADD `redirect_url` VARCHAR( 250 );