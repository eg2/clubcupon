CREATE OR REPLACE VIEW `accounting_guarantee_fund_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`,`acg`.`pay_by_redeemed` AS `pay_by_redeemed`, SUM(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`
FROM `accounting_company_id_grouped` `acg`, accounting_items i
WHERE acg.liquidate_id = i.id
and (`acg`.`pay_by_redeemed` <> 1)
and i.accounting_type in ('LIQUIDATE_BY_SOLD_EXPIRED' , 'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL')
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`;


