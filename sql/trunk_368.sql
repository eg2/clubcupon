-- http://jira.int.clarin.com/browse/CC-5344

INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (26, now(), now(), 0, 0, 0, 'Caballo', 'caballo.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (27, now(), now(), 0, 0, 0, 'Hotel', 'hotel.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (28, now(), now(), 0, 0, 0, 'Micro', 'micro.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (29, now(), now(), 0, 0, 0, 'Palmera', 'palmera.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (30, now(), now(), 0, 0, 0, 'Parapente', 'parapente.png');