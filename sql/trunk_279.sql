-- http://tracker.int.clarin.com/browse/CC-3450

UPDATE payment_plans SET 
name = 'MasterCard hasta en 12 cuotas'
WHERE id = 1;

INSERT INTO payment_plans VALUES (3, NOW(), NOW(), 0, 0, 0, 'MasterCard hasta en 6 cuotas');
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 3, '1 Cuota sin interes', 0, 0, 1);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 3, '3 Cuotas sin interes', 0, 0, 3);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES(NOW(), NOW(), 0, 0, 0, 3, '6 Cuotas sin interes', 0, 0, 6);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (7, NOW(), NOW(), 0, 0, 0, 3,102);

INSERT INTO payment_plans VALUES (4, NOW(), NOW(), 0, 0, 0, 'MasterCard hasta en 3 cuotas');
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 4, '1 Cuota sin interes', 0, 0, 1);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 4, '3 Cuotas sin interes', 0, 0, 3);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (10, NOW(), NOW(), 0, 0, 0, 4,102);