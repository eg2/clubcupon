ALTER TABLE `deals` ADD `downpayment_percentage` INT( 11 ) NOT NULL AFTER `commission_percentage`;
ALTER TABLE `deals` ADD `closure_modality` TINYINT( 1 ) NOT NULL AFTER `coupon_expiry_date`;
ALTER TABLE `deals` ADD `deferred_payment` INT( 3 ) NOT NULL AFTER `closure_modality`;
ALTER TABLE `deals` ADD `first_closure_percentage` INT( 3 ) NOT NULL AFTER `deferred_payment`;
ALTER TABLE `deals` ADD `payment_method` TINYINT( 1 ) NOT NULL AFTER `first_closure_percentage`;