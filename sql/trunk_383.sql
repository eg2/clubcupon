ALTER TABLE `banner_types`
	ALTER `label` DROP DEFAULT;
ALTER TABLE `banner_types`
	CHANGE COLUMN `label` `label` VARCHAR(64) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `id`;

INSERT INTO `banner_types` (`id`, `label`, `width`, `height`) VALUES (4, 'Newsletter Top', 598, 100);
