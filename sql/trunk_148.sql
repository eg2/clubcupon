-- Ajuste del tamano para la imagen ppal de un deal, en la vista view

UPDATE `settings` SET `value` = '440' WHERE `settings`.`name` = 'thumb_size.medium_big_thumb.width';
UPDATE `settings` SET `value` = '292' WHERE `settings`.`name` = 'thumb_size.medium_big_thumb.height';
UPDATE `settings` SET `value` = '219' WHERE `settings`.`name` = 'thumb_size.small_big_thumb.width';
UPDATE `settings` SET `value` = '139' WHERE `settings`.`name` = 'thumb_size.small_big_thumb.height';

UPDATE `settings` SET `value` = '<iframe src="http://www.facebook.com/plugins/likebox.php?href=###FANPAGE_URL###&amp;width=230&amp;colorscheme=light&amp;connections=6&amp;stream=false&amp;height=300&amp;locale=es_LA" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:300px;" allowTransparency="true"></iframe>' WHERE `settings`.`name` = 'facebook.feeds_code';