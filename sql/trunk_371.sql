-- http://jira.int.clarin.com/browse/CC-5363

INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (40, now(), now(), 0, 0, 0, 'Bici', 'bici.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (41, now(), now(), 0, 0, 0, 'Caja de bombones','caja-de-bombones.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (42, now(), now(), 0, 0, 0, 'Celular', 'celular.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (43, now(), now(), 0, 0, 0, 'Corazon', 'corazon.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (44, now(), now(), 0, 0, 0, 'Corona', 'corona.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (45, now(), now(), 0, 0, 0, 'Estrella', 'estrella.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (46, now(), now(), 0, 0, 0, 'Flama', 'flama.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (47, now(), now(), 0, 0, 0, 'Gota de agua', 'gota-de-agua.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (48, now(), now(), 0, 0, 0, 'Lamparita', 'lamparita.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (49, now(), now(), 0, 0, 0, 'Mochila', 'mochila.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (50, now(), now(), 0, 0, 0, 'Moto', 'moto.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (51, now(), now(), 0, 0, 0, 'Mundo', 'mundo.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (52, now(), now(), 0, 0, 0, 'Perro', 'perro.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (53, now(), now(), 0, 0, 0, 'Pileta', 'pileta.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (54, now(), now(), 0, 0, 0, 'Pizza', 'pizza.png');
INSERT INTO `search_link_logos` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (55, now(), now(), 0, 0, 0, 'Sol', 'sol.png');