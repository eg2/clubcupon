-- http://jira.int.clarin.com/browse/CC-5405

-- `payment_plans`
INSERT INTO `payment_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`) VALUES (5, now(), now(), 0, 0, 0, 'Naranja en un pago');
INSERT INTO `payment_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`) VALUES (6, now(), now(), 0, 0, 0, 'Naranja hasta en 3 cuotas #7');

-- `payment_plan_items`
INSERT INTO `payment_plan_items` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `name`, `interest`, `fees`, `payment_installments`) VALUES (15, now(), now(), 0, 0, 0, 5, '1 pago', 0.00, 0, 1);
INSERT INTO `payment_plan_items` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `name`, `interest`, `fees`, `payment_installments`) VALUES (16, now(), now(), 0, 0, 0, 6, '1 pago', 0.00, 0, 1);
INSERT INTO `payment_plan_items` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `name`, `interest`, `fees`, `payment_installments`) VALUES (17, now(), now(), 0, 0, 0, 6, '3 cuotas sin interés', 0.00, 0, 3);

-- `payment_option_plans`
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (28, now(), now(), 0, 0, 0, 5, 6, 10);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (29, now(), now(), 0, 0, 0, 5, 53, 20);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (30, now(), now(), 0, 0, 0, 6, 6, 10);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (31, now(), now(), 0, 0, 0, 6, 53, 20);