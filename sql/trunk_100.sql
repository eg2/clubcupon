---
--- Update de la tabla `banners` para que soporte la relacion con banner_types
---
ALTER TABLE `banners` ADD `banner_type_id` INT( 11 ) NOT NULL AFTER `name` ;