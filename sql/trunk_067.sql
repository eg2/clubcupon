CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW  `cluster_sources` AS
SELECT users.id, users.email, user_profiles.dob, user_profiles.gender, user_profiles.city_id AS localidad, users.is_active
FROM users
LEFT JOIN user_profiles ON user_profiles.user_id = users.id;


CREATE TABLE `cluster_examples` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) null,
  `dob` date null,
  `gender` varchar(1) null,
  `localidad` BIGINT(20) UNSIGNED null,
  `is_active` tinyint(1) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cluster examples';

CREATE TABLE  `clusters` (
  `id` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `name` VARCHAR( 255 ) NOT NULL
) ENGINE = INNODB AUTO_INCREMENT=2 CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE  `cluster_definitions` (
  `id` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `cluster_example_id` BIGINT( 20 ) UNSIGNED NOT NULL,
  `cluster_id` BIGINT( 20 ) UNSIGNED NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;


