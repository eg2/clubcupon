-- Se agregan los logos para los medios online y offline de MercadoPago
INSERT INTO payment_methods(id, name, logo)
VALUES      (13, 'MercadoPago Online', 'mercadopago_online.gif');
INSERT INTO payment_methods(id, name, logo)
VALUES      (14, 'MercadoPago Offline', 'mercadopago_offline.gif');
--
UPDATE payment_options
SET    payment_method_id = 13
WHERE  payment_setting_id = 8 AND description = 'Pago on line por Mercado';
UPDATE payment_options
SET    payment_method_id = 14
WHERE  payment_setting_id = 8 AND description = 'Pago off line por Mercado';
--
COMMIT;