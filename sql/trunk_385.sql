-- https://artear.atlassian.net/browse/CC-5746
ALTER TABLE `companies`
	ADD COLUMN `is_sales_report_enabled` TINYINT(1) NOT NULL DEFAULT '0';
