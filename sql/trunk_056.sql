-- Agrega los campos:
--   TINYINT con "la ciudad es seleccinable en el perfil de usuario"
-- a countries, states, cities y neighbourhoods.
alter table `countries` add `is_selectable` TINYINT(1) default true after `slug`;
alter table `states` add `is_selectable` TINYINT(1) default true after `is_approved`;
alter table `cities` add `is_selectable` TINYINT(1) default true after `is_approved`;
alter table `neighbourhoods` add `is_selectable` TINYINT(1) default true after `city_id`;