-- Agrega la plantilla para avisar que se compro una suboferta.

INSERT INTO `email_templates`
(`id`, `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES
(NULL, NOW(), NOW(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Subdeal Bought', 'We will send this mail, after purchased a deal', '##DEAL_NAME##', '', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_TITLE, DESCRIPTIVE_TEXT, QUANTITY, DEAL_AMOUNT , PAYMENT_TYPE ', '1');
commit;