-- http://jira.cmd.com.ar/browse/CC-5266

ALTER TABLE search_links ADD column image VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci';