-- Marca como multiples las preguntas "salidas", "comidafavorita", y "ofertasinteres"
update `questions` set `multiple` = 1 where `name` in ('salidas', 'comidafavorita', 'ofertasinteres');
commit;