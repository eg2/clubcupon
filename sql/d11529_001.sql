-- http://jira.int.clarin.com/browse/CC-5469
ALTER TABLE `deals` ADD COLUMN `is_variable_expiration` TINYINT(1) NULL DEFAULT '0';