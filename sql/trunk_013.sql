ALTER TABLE neighbourhoods DROP FOREIGN KEY fk_neighbourhoods_regions1;
ALTER TABLE neighbourhoods DROP INDEX fk_neighbourhoods_regions1;
ALTER TABLE neighbourhoods CHANGE COLUMN region_id city_id BIGINT(20)  DEFAULT NULL;
UPDATE neighbourhoods SET city_id = (SELECT id FROM cities ORDER BY id LIMIT 1);
