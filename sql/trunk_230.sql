ALTER TABLE `companies` 
	ADD COLUMN `is_pending_accounting_event` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `deals` 
	ADD COLUMN `is_pending_accounting_event` TINYINT(1) NOT NULL DEFAULT '0', 
	ADD COLUMN `liquidate_count` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	ADD COLUMN `agreed_percentage` INT(10) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `deal_users` 
	ADD COLUMN `is_billed` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `anulled_coupons` 
	ADD COLUMN `is_billed` TINYINT(1) NOT NULL DEFAULT '0';

CREATE TABLE `accounting_calendars` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`since` TIMESTAMP NOT NULL,
	`until` TIMESTAMP NOT NULL,
	`execution` TIMESTAMP NOT NULL,
	`status` VARCHAR(64),
PRIMARY KEY (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `accounting_items` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`accounting_calendar_id` BIGINT(20) UNSIGNED NOT NULL,
        `accounting_item_id` BIGINT(20) UNSIGNED,
	`accounting_type` VARCHAR(255) NOT NULL,
	`model` VARCHAR(255) NOT NULL,
	`model_id` BIGINT(20) UNSIGNED NOT NULL,
	`deal_id` BIGINT(20) UNSIGNED NOT NULL,
	`total_quantity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`total_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`bac_sent` DATETIME,
	`bac_response_id` BIGINT(20) UNSIGNED,
	`discounted_price` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`billing_commission_percentage` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`billing_iva_percentage` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`liquidating_is_first` TINYINT(1) NOT NULL DEFAULT '0',
	`liquidating_guarantee_fund_percentage` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`liquidating_agreed_percentage` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`liquidating_invoiced_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`liquidating_iibb_percentage` INT(10) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`id`),
INDEX `accounting_calendar_id` (`accounting_calendar_id`),
INDEX `accounting_item_id` (`accounting_item_id`),
INDEX `deal_id` (`deal_id`),
INDEX `model` (`model`),
INDEX `model_id` (`model_id`),
CONSTRAINT `accounting_items_calendars_fk` FOREIGN KEY (`accounting_calendar_id`) REFERENCES `accounting_calendars` (`id`),
CONSTRAINT `accounting_items_items_fk` FOREIGN KEY (`accounting_item_id`) REFERENCES `accounting_items` (`id`),
CONSTRAINT `accounting_items_deals_fk` FOREIGN KEY (`deal_id`) REFERENCES `deals` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `accounting_item_details` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
        `deleted` TINYINT(1) NOT NULL DEFAULT '0',
        `accounting_item_id` BIGINT(20) UNSIGNED NOT NULL,
        `deal_user_id` BIGINT(20) UNSIGNED NOT NULL,
PRIMARY KEY (`id`),
INDEX `accounting_item_id` (`accounting_item_id`),
INDEX `deal_user_id` (`deal_user_id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;