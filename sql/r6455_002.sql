CREATE OR
REPLACE VIEW deal_flat_categories AS
SELECT `l3`.`id` AS `id`,`l3`.`parent_id` AS `parent_id`,(SIGN(IFNULL(`l2`.`id`,0)) + SIGN(IFNULL(`l3`.`id`,0))) AS `level`, TRIM(CONCAT_WS(' | ',`l1`.`name`,`l2`.`name`,`l3`.`name`)) AS `path`, TRIM(
LEFT(CONCAT(CONCAT_WS(' | ',`l1`.`name`,`l2`.`name`,`l3`.`name`), ' | '), INSTR(CONCAT(CONCAT_WS(' | ',`l1`.`name`,`l2`.`name`,`l3`.`name`), ' | '), ' | '))) AS `l1` ,
IFNULL(`l1`.`id`, IFNULL(`l2`.`id`,`l3`.`id`)) AS `l1_id`
FROM ((`deal_categories` `l3`
LEFT JOIN `deal_categories` `l2` ON((`l3`.`parent_id` = `l2`.`id`)))
LEFT JOIN `deal_categories` `l1` ON((`l2`.`parent_id` = `l1`.`id`)));
