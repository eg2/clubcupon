-- Agrega el campo "id de cupon" a los pines
alter table `pins` add `deal_user_id` BIGINT(20) unsigned default null after `code`;
-- y lo torna una FK
alter table `pins` add foreign key (`deal_user_id`) references `deal_users` (`id`);

-- Migra los datos: buscando los cupones con id I que tengan un pin asociado,
--   pone el campo "id de cupon" en tales pines en I
update `pins` `p`, `deal_users` `du`
   set `p`.`deal_user_id` = `du`.`id`
where 0 = 0
  and `du`.`pin_id` = `p`.`id`;
commit;

-- Elimina las constraints de FK de los cupones hacia los pines
alter table `deal_users` drop foreign key `deal_users_ibfk_2`;
alter table `deal_users` drop foreign key `deal_users_ibfk_1`;
-- Elimina el campo "id de pin" de los cupones
alter table `deal_users` drop column `pin_id`;
