-- Elimina el registro "Expired", previo pasar todos los deals vencidos a cerrados
UPDATE `deals` SET `deal_status_id` = 6 WHERE `deal_status_id` = 4;
DELETE FROM `deal_statuses` WHERE `id` = 4;