-- https://artear.atlassian.net/browse/CC-5526
CREATE TABLE `beacon_devices` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`name` VARCHAR(256) NOT NULL,
	`code` VARCHAR(256) NOT NULL,
	`mac` VARCHAR(17) NOT NULL,
	`uuid` VARCHAR(36) NOT NULL,
	`minor` BIGINT(5) UNSIGNED ,
	`major` BIGINT(5) UNSIGNED,
	`company_id` BIGINT(20) UNSIGNED,
	PRIMARY KEY (`id`),
	CONSTRAINT `beacon_device_company_fk`
            FOREIGN KEY (`company_id`)
            REFERENCES `companies` (`id`),
	INDEX `beacon_device_company_id` (`company_id`)	
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;

CREATE TABLE `beacon_device_deals` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`title` VARCHAR(256) NOT NULL,
	`subtitle` VARCHAR(256) NOT NULL,
	`priority` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
	`beacon_device_id` BIGINT(20) UNSIGNED NOT NULL,
	`deal_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `beacon_device_deal_device_fk`
		FOREIGN KEY (`beacon_device_id`)
		REFERENCES `beacon_devices` (`id`),
	CONSTRAINT `beacon_device_deal_deal_fk`
		FOREIGN KEY (`deal_id`)
		REFERENCES `deals` (`id`),
	INDEX `beacon_device_deal_device_id` (`beacon_device_id`), 
	INDEX `beacon_device_deal_deal_id` (`deal_id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;