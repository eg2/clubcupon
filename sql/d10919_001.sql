-- http://jira.int.clarin.com/browse/CC-5284
ALTER TABLE `accounting_items`   ADD COLUMN `is_visible` TINYINT NOT NULL DEFAULT '0';
