insert into email_templates (created,modified,`from`,reply_to,name,description,subject,email_content,email_variables,is_html)
values(CURDATE(),CURDATE(),'##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Deal Coupon Now', 'Coupon generated for all the buyers of a deal', 'Este es tu cup�n de compra de ##SITE_NAME## Ya!: ##DEAL_NAME##.', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Club Cup&oacute;n</title>
  </head>
  <body>
    <table width="645" border="0" align="center" cellpadding="15" cellspacing="0">
      <tr>
        <td align="center" valign="middle" bgcolor="#e4e4e4">
          <table width="615" border="0" align="center" cellpadding="15" cellspacing="0">
            <tr>
              <td align="center" valign="middle" bgcolor="#FFFFFF">
                <table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="326" align="left" valign="top" bgcolor="#FFFFFF">
                    	<img src="##ABSOLUTE_IMG_PATH##coupon/header2.jpg" width="326" style="display:block;"/>
                    </td>
                    <td width="259" align="center" valign="middle" bgcolor="#ECECEC" style="font-size:14px; font-weight:bold; font-family:sans-serif;">##COUPON_CODE##<br/>Cod. Posnet: ##POSNET_CODE##</td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:14px;">
                      <!--br-->&iexcl;Felicitaciones! &iexcl;Compraste la oferta <strong style="color:#f25607; font-size:16px;">##DEAL_TITLE##</strong><strong style="color:#f25607; font-size:16px;">##DESCRIPTIVE_TEXT##</strong>, ya pod&eacute;s disfrutarla!<br><br>
                      El monto de la compra ya fue abonado. La transacci&oacute;n se confirm&oacute; con &eacute;xito.
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><table width="585" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="356" align="left" valign="top"><br><img src="##ABSOLUTE_IMG_PATH##coupon/tit_titular.gif" width="154" height="16" style="display:block;"></td>
                          <td width="17" align="left" valign="top">&nbsp;</td>
                          <td width="212" align="left" valign="top"><br><img src="##ABSOLUTE_IMG_PATH##coupon/tit_canjear.gif" width="95" height="16" style="display:block;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                            Usuario: <strong>##USER_NAME##</strong><br>
                            DNI: <strong>##USER_DNI##</strong>##COUPON_PIN##</td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                            ##COMPANY_NAME##<br>
                            ##COMPANY_PHONE##<br>
                            ##COMPANY_ADDRESS_1##<br>
                            ##COMPANY_ADDRESS_2##<br>
                            ##COMPANY_CITY##</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><!-- br-->
                            <img src="##ABSOLUTE_IMG_PATH##coupon/tit_vencimiento.gif" width="200" height="16" style="display:block;"></td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><br>
                            ##COUPON_EXPIRY_DATE##</td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td rowspan="3" align="left" valign="top"><img src="##BARCODE##" width="210" style="border:1px solid #ccc; display:none;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><br>
                            <img src="##ABSOLUTE_IMG_PATH##coupon/tit_detalles.gif" width="69" height="16" style="display:block;"></td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:12px; font-family:sans-serif;"><!-- br-->
                            ##COUPON_CONDITION##</td>
                          <td align="left" valign="top">&nbsp;</td>
                        </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##coupon/004.gif" width="585" height="287" border="0" usemap="#mapa" style="display:block;"></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:12px;">
                      <!-- br-->Ahora s&oacute;lo resta que disfrutes de este beneficio y que sigas visitando <strong>Club Cup&oacute;n</strong> para descubrir incre&iacute;bles ofertas todos los d&iacute;as.<br>
                      Te esperamos.<br><br>
                      &iexcl;Muchas Gracias por tu compra!<br><br>
                      <strong>El equipo de Club Cup&oacute;n</strong><br>
              
                      <a href="http://www.clubcupon.com.ar/?from=mailing" target="_blank" style="color:#f25607;">www.clubcupon.com.ar</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_NAME, COMPANY_NAME, COMPANY_ADDRESS_1, COMPANY_ADDRESS_2, COMPANY_CITY, COUPON_CONDITION, COUPON_EXPIRY_DATE, QUANTITY, SITE_LOGO, BARCODE, COUPON_CODE, USER_NAME, COUPON_PIN, DESCRIPTIVE_TEXT, DEAL_TITLE, COMPANY_PHONE, COUPON_PURCHASED_DATE, USER_DNI, GIFT_DNI, ABSOLUTE_IMG_PATH', 1);