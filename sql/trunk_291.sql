-- http://tracker.int.clarin.com/browse/CC-3623

CREATE TABLE `product_campaigns` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `deleted` TINYINT(1) NOT NULL DEFAULT '0',
    `company_id` BIGINT(20) UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
PRIMARY KEY (`id`),
INDEX `company_id` (`company_id`),
CONSTRAINT `product_campaigns_companies_fk`
    FOREIGN KEY (`company_id`)
    REFERENCES `companies` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `product_products` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `deleted` TINYINT(1) NOT NULL DEFAULT '0',
    `product_campaign_id` BIGINT(20) UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0',
PRIMARY KEY (`id`),
INDEX `product_campaign_id` (`product_campaign_id`),
CONSTRAINT `product_products_campaigns_fk`
    FOREIGN KEY (`product_campaign_id`)
    REFERENCES `product_campaigns` (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `product_inventory_strategies` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` BIGINT(20) UNSIGNED NOT NULL,
    `modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`code` VARCHAR(64) NOT NULL COLLATE 'utf8_unicode_ci',
    `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`description` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
PRIMARY KEY (`id`)
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

INSERT INTO `product_inventory_strategies`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `code`,`name`, `description`)
VALUES
	(1, NOW(), NULL, 839, NULL, 0,'UNMANAGED_STOCK', 'Stock no administrado', 'Se determina un límite máximo por oferta o suboferta.');
INSERT INTO `product_inventory_strategies`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `code`,`name`, `description`)
VALUES
	(2, NOW(), NULL, 839, NULL, 0,'MANAGED_STOCK', 'Stock administrado', 'El stock de una oferta o suboferta se administra por producto.');

ALTER TABLE `deals`
	ADD COLUMN `product_campaign_id`
		BIGINT(20) UNSIGNED,
	ADD COLUMN `product_product_id`
		BIGINT(20) UNSIGNED,
	ADD COLUMN `product_inventory_strategy_id`
		BIGINT(20) UNSIGNED,
	ADD CONSTRAINT `deals_product_campaigns_fk`
		FOREIGN KEY (`product_campaign_id`)
		REFERENCES `product_campaigns` (`id`),
	ADD CONSTRAINT `deals_product_products_fk`
		FOREIGN KEY (`product_product_id`)
		REFERENCES `product_products` (`id`),
	ADD CONSTRAINT `deals_product_inventory_strategies_fk`
		FOREIGN KEY (`product_inventory_strategy_id`)
		REFERENCES `product_inventory_strategies` (`id`);