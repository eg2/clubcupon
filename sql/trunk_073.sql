-- Modifica el campo "esta empresa utiliza pines" a "esta empresa utiliza X pines"
alter table `companies` modify `has_pins` INT(11) default 0;
-- Modifica el campo "esta oferta utiliza pines" a "esta oferta utiliza X pines"
alter table `deals` modify `has_pins` INT(11) default 0;