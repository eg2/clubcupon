INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) 
VALUES (87, NOW(), NOW(), 'ATM', 11, 'dim', 1, 3);

INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) 
VALUES (88, NOW(), NOW(), 'ATM Productos', 23, 'dim', 1, 3);

INSERT INTO `payment_methods` (`id`, `name`, `logo`)
VALUES (17, 'Banelco', 'banelco.gif');

INSERT INTO `payment_methods` (`id`, `name`, `logo`)
VALUES (18, 'Red Link', 'redlink.gif');

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES (87, 17, 11, 87, 'Banelco', 'Banelco por Mercado Pago Checkout', 0);

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES (88, 18, 11, 87, 'Red Link', 'Red Link por Mercado Pago Checkout', 0);

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES (89, 17, 13, 88, 'Banelco', 'Banelco Por MercadoPago Productos', 0);

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES (90, 18, 13, 88, 'Red Link', 'Red Link Por MercadoPago Productos', 0);
