-- Agrega el campo "bac_tourism_id" a users y companies
alter table `companies` add `bac_tourism_id` BIGINT(20) not null default '0' after `bac_user_id`;
alter table `users`     add `bac_tourism_id` BIGINT(20) not null default '0' after `bac_user_id`;

-- Agrega el campo "is_tourism" a deals y a companies
alter table `companies` add `is_tourism` TINYINT(1) not null default FALSE after `bac_tourism_id`;
alter table `deals`     add `is_tourism` TINYINT(1) not null default FALSE after `company_id`    ;