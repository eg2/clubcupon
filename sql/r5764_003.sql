update email_templates set email_content = 
'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Club Cup&oacute;n</title>
  </head>
  <style>
  strong{
  	font-family: sans-serif;
  }
  td{
  	padding:2px;
  }
  #cod {
    border: 5px solid #EEEEEE;
    display: block;
    font-weight: normal !important;
   /*height: 51px;*/
    margin-right: 10px;
    padding-top: 22px;
    width: 265px;
  }
  </style>
  <body>
    <table width="580" border="0" align="center" cellpadding="5" cellspacing="0">
      <tr>
        <td align="center" valign="middle" >
          <table width="575" border="0" align="center" cellpadding="15" cellspacing="0">
            <tr>
              <td align="center" valign="middle" bgcolor="#FFFFFF">
                <table width="575" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:sans-serif;">
                  <tr>
                    <td width="100%" align="left" valign="top" bgcolor="#FFFFFF">
						<img src="##ABSOLUTE_IMG_PATH####COUPON_URL_LOGO##" >
                	</td>
                    <td align="center" width="260" valign="middle"  
                    	style="font-size:14px; font-weight:bold; font-family:sans-serif;vertical-align: middle;">
                   	 <div id="cod">
                    	##COUPON_CODE##<br/>
                    	##POSNET_CODE_TEXT##
                    </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:14px;padding-top:10px;">
                      <br>
                      ##COUPON_MESSAGE##
             
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><table width="565" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="356" align="left" valign="top"><br>
                             <strong style="color:#f25607; font-size:15px;">TITULAR DEL CUP&#211;N</strong>
                          </td>
                          <td width="17" align="left" valign="top">&nbsp;</td>
                          <td width="212" align="left" valign="top"><br>
                           <strong style="color:#f25607; font-size:15px;">CANJEAR EN</strong>
                          </td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:14px; font-family:sans-serif;">
                            ##COUPON_TITULAR##
                          </td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top" style="font-size:14px; font-family:sans-serif;">
                            ##CANJEAR_EN##
                          </td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">
                            <strong style="color:#f25607; font-size:15px;">VENCIMIENTO DEL CUP&#211;N</strong>
                            <br/>
                            <div style="font-size:14px; font-family:sans-serif;padding-top:8px">
                            ##COUPON_EXPIRY_DATE##
                            </div>
                          </td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top">
                          <table>
						  <tr>
						    <td>
								<strong style="color:#f25607; font-size:15px;">FECHA DE COMPRA</strong>
							</td>
						  </tr>
						  <tr>
							<td align="left" valign="top" style="font-size:14px; font-family:sans-serif;">
								##COUPON_PURCHASED_DATE##<!--fecha de compra-->
							</td>
					      </tr>
						  <tr>
						  <td>
						  <strong style="color:#f25607; font-size:15px;">##VALOR_COUPON_TITLE##</strong>
						  </td>
						  </tr>
						  <tr>
						    <td align="left" valign="top" style="font-size:14px; font-family:sans-serif;">
								##VALOR_COUPON##<!--valor del cupon-->
							</td>
						  </tr>
						  
						  </table>
                          </td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:14px; font-family:sans-serif;">
                            </td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">
                            <strong style="color:#f25607; font-size:15px;">DETALLES</strong>
                          </td>
                          <td align="left" valign="top">&nbsp;</td>
                          <td align="left" valign="top">
							
						  </td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-size:14px; font-family:sans-serif;" colspan="3">
                            ##COUPON_CONDITION##
                          </td>
                        </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-size:14px; font-family:sans-serif;">
                        ##PASSBOOK_SECTION##
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##coupon/004.gif" width="570" height="287" border="0" usemap="#mapa" style="display:block;"></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:sans-serif; font-size:14px;">
                      Ahora s&oacute;lo resta que disfrutes de este beneficio y que sigas visitando <strong>Club Cup&oacute;n</strong> para descubrir incre&iacute;bles ofertas todos los d&iacute;as.<br>
                      Te esperamos.<br><br>
                      &iexcl;Muchas Gracias por tu compra!<br><br>
                      <strong>El equipo de Club Cup&oacute;n</strong><br>

                      <a href="http://www.clubcupon.com.ar/?from=mailing" target="_blank" style="color:#f25607;">www.clubcupon.com.ar</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>'
where name = 'Deal Coupon Generic';


