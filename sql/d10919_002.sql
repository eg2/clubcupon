-- http://jira.int.clarin.com/browse/CC-5284
UPDATE
  accounting_items AS i 
SET
  i.is_visible = 1  
WHERE
  EXISTS (
    SELECT
      c.id  
    FROM
      accounting_calendars AS c  
    WHERE
      c.id = i.accounting_calendar_id   
      AND c.status = 'ACCOUNTED'   
      AND DATE(c.execution) <= '2014-10-31'
  );
