INSERT INTO `payment_settings` (`id`, `name`) VALUES (14, 'Por MercadoPago Turismo');

INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (89, NOW(), NOW(), 'TarjetaDeCreditoTurismo', 24, 'dim', 1, 1);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (90, NOW(), NOW(), 'PagoOfflineTurismo', 24, 'dim', 1, 2);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (91, NOW(), NOW(), 'ATM Turismo', 24, 'dim', 1, 3);

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (90, 1, 14, 89, 'American Express', 'American Express por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (91, 6, 14, 89, 'Master Card', 'Master Card por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (92, 8, 14, 89, 'Shopping', 'Shopping por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (93, 2, 14, 89, 'Argencard', 'Argencard por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (94, 7, 14, 89, 'Naranja', 'Naranja Express por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (95, 3, 14, 89, 'Cabal', 'Cabal por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (96, 9, 14, 89, 'Visa', 'Visa por Mercado Pago Turismo', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (97, 10, 14, 5000, 'Cuenta Club Cupon', 'Cuenta Club Cupon', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (98, 12, 14, 90, 'BaproPagos', 'BaproPagos por Mercado Pago Turismo', 1);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (99, 12, 14, 90, 'RapiPago', 'RapiPago por Mercado Pago Turismo', 1);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (100, 12, 14, 90, 'PagoFacil', 'PagoFacil por Mercado Pago Turismo', 1);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (101, 17, 14, 91, 'Banelco', 'Banelco Por MercadoPago Turismo', 0);