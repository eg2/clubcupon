-- Agrega el campo "esta empresa utilizo pines"

alter table `companies` add `has_pins` TINYINT(1) default false after `is_company_profile_enabled`;