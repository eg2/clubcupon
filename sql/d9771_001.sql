TRUNCATE TABLE search_links;

-- LIST DESTACADOS - Bs As Norte, Bs As Oeste, Bs As Sur, Ciudad de Bs As

	-- Producto (Especial)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Entretenimiento (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Belleza (Especial)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Festival Gastronómico (Especial : festival-gastronomico)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Selección de ofertas (Especial : ofertas-para-vos)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Indumentaria (Especial : especial-indumentaria)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Fotografía (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Sushi (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

-- TAB ESPECIALES - Bs As Norte, Bs As Oeste, Bs As Sur, Ciudad de Bs As

	-- Producto (Especial)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Entretenimiento (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Entretenimiento', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--entretenimiento', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Belleza (Especial)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Salud y Belleza', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Belleza&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Festival Gastronómico (Especial : festival-gastronomico)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Festival Gastronómico', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Festival+Gastron%C3%B3mico&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Selección de ofertas (Especial : ofertas-para-vos)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '5', 'Selección de ofertas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+para+Vos&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Indumentaria (Especial : especial-indumentaria)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--ciudad~de~buenos~aires-page--1-query--oooo-facet--city_name_no_group.Ciudad+de+Buenos+Aires', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~norte-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Norte', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~oeste-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Oeste', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '6', 'Indumentaria', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Especial+Indumentaria&scV=-city--buenos~aires~sur-page--1-query--oooo-facet--city_name_no_group.Buenos+Aires+Sur', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '7', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Fotografía (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '8', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);

	-- Sushi (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Ciudad+de+Buenos+Aires&scV=-city--ciudad~de~buenos~aires-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Norte&scV=-city--buenos~aires~norte-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-norte')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Oeste&scV=-city--buenos~aires~oeste-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-oeste')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '9', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Buenos+Aires+Sur&scV=-city--buenos~aires~sur-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'buenos-aires-sur')
	);


