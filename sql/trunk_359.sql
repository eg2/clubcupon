-- http://jira.int.clarin.com/browse/CC-5068
UPDATE payment_options AS o SET 
	o.payment_type_id = 80
WHERE o.name = 'BaproPagos';

UPDATE payment_options AS o SET 
	o.payment_type_id = 81
WHERE o.name = 'RapiPago';

UPDATE payment_options AS o SET 
	o.payment_type_id = 82
WHERE o.name = 'PagoFacil';