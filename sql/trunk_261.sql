-- http://tracker.int.clarin.com/browse/CC-3195

alter table deals add column campaign_code_type ENUM ('G', 'E');

UPDATE deals SET
	campaign_code_type = 'G'
WHERE DATE(start_date)>= '2013-03-01';
