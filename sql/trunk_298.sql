CREATE TABLE `fb_friends` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`created` DATETIME NOT NULL,
	`modified` DATETIME NOT NULL,
	`username` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`fb_user_id` BIGINT(20) NULL DEFAULT NULL,
	`friend_user_id` BIGINT(20) NULL DEFAULT NULL,
	`email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`),
	INDEX `fk_fb_friends1` (`friend_user_id`),
	CONSTRAINT `fk_fb_friends1` FOREIGN KEY (`friend_user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;