-- Acentuando Activa.
UPDATE email_templates SET subject='Activá tu cuenta de ##SITE_NAME## Ya!'
WHERE name = 'Activation Request Candidate';

UPDATE email_templates SET subject='Activá tu cuenta de ##SITE_NAME##'
WHERE name = 'Activation Request';