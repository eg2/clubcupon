INSERT INTO `payment_types` 
	(`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`)
VALUES 
	(92, now(), now(), 'NPS Turismo', 30, 'dim', 1, 22);

INSERT INTO `payment_settings` 
	(`id`, `name`)
VALUES 
	(15, 'Por NPS Turismo');

INSERT INTO `payment_options` 
	(`payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`)
VALUES 
	(6, 15, 92, 'Mastercard', 'Mastercard por NPS Turismo', 0);