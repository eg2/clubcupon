-- Agrega campo 'is_voluntary' que indica si el usuario se suscribió voluntariamente.
ALTER TABLE subscriptions ADD is_voluntary TINYINT(1) DEFAULT '1' AFTER is_subscribed;
