-- http://tracker.int.clarin.com/browse/CC-3785

DELETE
FROM payment_option_plans
WHERE payment_option_id IN (106, 107)
AND payment_plan_id IN (1, 3);

INSERT INTO `payment_option_plans`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
	(19, now(), now(), 0, 0, 0, 4, 104, 20);

INSERT INTO `payment_option_plans`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
	(20, now(), now(), 0, 0, 0, 2, 104, 10);

INSERT INTO `payment_option_plans`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
	(21, now(), now(), 0, 0, 0, 4, 105, 20);

INSERT INTO `payment_option_plans`
	(`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`)
VALUES
	(22, now(), now(), 0, 0, 0, 2, 105, 10);