DROP TABLE IF EXISTS `shipping_address_types`;

CREATE TABLE IF NOT EXISTS `shipping_address_types` (
`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
`created` datetime NOT NULL,
`modified` datetime NOT NULL,
`deleted`  tinyint(1) DEFAULT 0,
`name`  varchar(256) NOT NULL,
`description`  varchar(256) DEFAULT NULL,
`label` varchar(256) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='types, managed or unmanaged shipping logistic';

INSERT INTO shipping_address_types(`created`, `modified`, `deleted`, `name`, `description`, `label`) VALUES (now(), now(), 0, 'managed_logistics', 'managed logistics for Shipping', 'Maneja Logistica de Stock');
INSERT INTO shipping_address_types(`created`, `modified`, `deleted`, `name`, `description`, `label`) VALUES (now(), now(), 0, 'unmanaged_logistics', 'do not managed logistics for Shipping', 'NO Maneja Logistica de Stock');

ALTER TABLE shipping_addresses ADD COLUMN shipping_address_type_id  bigint(20) unsigned DEFAULT NULL COMMENT 'see shipping_address_types';

ALTER TABLE shipping_addresses ADD CONSTRAINT `fk_shipping_address_type_id`
FOREIGN KEY (shipping_address_type_id) REFERENCES shipping_address_types(id) ON UPDATE NO ACTION  ON DELETE  NO ACTION;

ALTER TABLE shipping_addresses ADD COLUMN libertya_warehouse_id  varchar(255) DEFAULT NULL COMMENT 'WarehouseId From Libertya';
ALTER TABLE shipping_addresses ADD COLUMN libertya_delivery_mode_id  varchar(255) DEFAULT NULL COMMENT 'DeliveryModeId From Libertya, default (receptoria)';

ALTER TABLE product_products DROP COLUMN bac_invoice_id;
ALTER TABLE product_products DROP COLUMN bac_libertya_id;
ALTER TABLE product_products DROP COLUMN description; 
ALTER TABLE product_products DROP COLUMN bac_product_id;