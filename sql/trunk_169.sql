
CREATE TABLE IF NOT EXISTS `preredemptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `modified_by` bigint(20) unsigned DEFAULT NULL,
  `deals_id` bigint(20) unsigned DEFAULT NULL,
  `posnet_code` varchar(18) DEFAULT NULL,
  `is_assigned` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `posnet_code_uk` (`posnet_code`),
  UNIQUE KEY `id` (`id`,`posnet_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `redemptions`
--
ALTER TABLE `preredemptions`
  ADD CONSTRAINT `deals_id_fk` FOREIGN KEY (`deals_id`) REFERENCES `deals` (`id`);
