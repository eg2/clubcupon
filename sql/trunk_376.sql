-- http://jira.int.clarin.com/browse/CC-5417

-- `payment_options`
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (136, 7, 15, 92, 'Naranja', 'Naranja por NPS Turismo', 0);

INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (32, now(), now(), 0, 0, 0, 5, 136, 10);
INSERT INTO `payment_option_plans` (`id`, `created`, `modified`, `created_by`, `modified_by`, `deleted`, `payment_plan_id`, `payment_option_id`, `order`) VALUES (33, now(), now(), 0, 0, 0, 6, 136, 20);

