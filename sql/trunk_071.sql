-- Se le saca la constraint NOT NULL al campo "razon por la que se cancela este pago"

alter table deal_externals modify cancellation_reason varchar(255) collate utf8_unicode_ci;