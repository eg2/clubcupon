-- Se agregan los nuevas templates de turismo
insert into email_templates
(created, modified, `from`, reply_to, `name`, description, subject, email_content, email_variables, is_html)
values
(now(), now(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Deal Coupon Tourism',      'Coupon generated for all the buyers of a tourism deal', 'Este es tu cupón de compra de ##SITE_NAME##: ##DEAL_NAME##.', '', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_NAME, COMPANY_NAME, COMPANY_ADDRESS_1, COMPANY_ADDRESS_2, COMPANY_CITY, COUPON_CONDITION, COUPON_EXPIRY_DATE, QUANTITY, SITE_LOGO, BARCODE, COUPON_CODE, USER_NAME, COUPON_PIN', true),
(now(), now(), '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Deal Coupon Gift Tourism', 'We will send this mail, while gifting a tourism coupon to his/her friend.', 'Cupon de regalo de ##USER_NAME##', '', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, USER_NAME, MESSAGE, DEAL_NAME, QUANTITY, COMPANY_NAME, COMPANY_ADDRESS_1, COMPANY_ADDRESS_2, COMPANY_CITY, COUPON_CONDITION, COUPON_EXPIRY_DATE, BARCODE, COUPON_CODE, COUPON_PIN',          true);
commit;