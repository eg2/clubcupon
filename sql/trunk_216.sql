INSERT INTO `payment_settings` (`id`, `name`) VALUES (13, 'Por MercadoPago Productos');
--
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (76, 1, 13, 85, 'American Express', 'American Express por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (77, 6, 13, 85, 'Master Card', 'Master Card por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (78, 8, 13, 85, 'Shopping', 'Shopping por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (79, 2, 13, 85, 'Argencard', 'Argencard por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (80, 7, 13, 85, 'Naranja', 'Naranja Express por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (81, 3, 13, 85, 'Cabal', 'Cabal por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (82, 9, 13, 85, 'Visa', 'Visa por Mercado Pago Checkout', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (83, 10, 13, 5000, 'Cuenta Club Cupon', 'Cuenta Club Cupon', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (84, 12, 13, 86, 'BaproPagos', 'BaproPagos por Mercado Pago Checkout', 1);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (85, 12, 13, 86, 'RapiPago', 'RapiPago por Mercado Pago Checkout', 1);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (86, 12, 13, 86, 'PagoFacil', 'PagoFacil por Mercado Pago Checkout', 1);
--
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (85, NOW(), NOW(), 'TarjetaDeCreditoProducto', 23, 'dim', 1, 1);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (86, NOW(), NOW(), 'PagoOfflineProducto', 23, 'dim', 1, 2);