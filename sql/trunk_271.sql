-- http://tracker.int.clarin.com/browse/CC-3386

UPDATE payment_option_deals SET 
payment_plan_id = NULL
WHERE payment_plan_id IS NOT NULL;

UPDATE deal_externals SET 
	payment_plan_option_id = NULL
WHERE payment_plan_option_id IS NOT NULL;

DELETE FROM payment_option_plans;
DELETE FROM payment_plan_items;
DELETE FROM payment_plans;

INSERT INTO payment_plans VALUES (1, NOW(), NOW(), 0, 0, 0, 'MasterCard en cuotas');
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 1, '1 Cuota sin interes', 0, 0, 1);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 1, '3 Cuotas sin interes', 0, 0, 3);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES(NOW(), NOW(), 0, 0, 0, 1, '6 Cuotas sin interes', 0, 0, 6);
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES(NOW(), NOW(), 0, 0, 0, 1, '12 Cuotas sin interes', 0, 0, 12);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (1, NOW(), NOW(), 0, 0, 0, 1,3);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (2, NOW(), NOW(), 0, 0, 0, 1,51);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (3, NOW(), NOW(), 0, 0, 0, 1,102);

INSERT INTO payment_plans VALUES (2, NOW(), NOW(), 0, 0, 0, 'MasterCard en un pago');
INSERT INTO payment_plan_items (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`name`,`interest`,`fees`,`payment_installments`) VALUES (NOW(), NOW(), 0, 0, 0, 2, '1 Pago', 0, 0, 1);
INSERT INTO payment_option_plans (`id`,`created`,`modified`,`created_by`,`modified_by`,`deleted`,`payment_plan_id`,`payment_option_id`) VALUES (4, NOW(), NOW(), 0, 0, 0, 2,102);

UPDATE payment_options SET
	name = REPLACE(REPLACE(name,'Mastercard','MasterCard'),'Master Card','MasterCard'),
	description = REPLACE(REPLACE(description,'Mastercard','MasterCard'),'Master Card','MasterCard')
WHERE name LIKE '%master%';