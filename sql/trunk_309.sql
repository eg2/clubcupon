-- http://tracker.int.clarin.com/browse/CC-4140
-- Este script reemplaza 
-- r8008_001.sql
-- r8008_002.sql
-- r8008_003.sql
-- r8008_004.sql
-- r8008_005.sql
-- trunk_301.sql
-- trunk_302.sql
-- trunk_303.sql
-- trunk_304.sql
-- trunk_305.sql
-- trunk_306.sql
-- trunk_307.sql
-- trunk_308.sql

-- accounting_campaign_item_extras;
CREATE OR REPLACE VIEW `accounting_campaign_item_extras` AS
SELECT `deal`.`company_id` AS `deal_company_id`,`liquidate`.`id` AS `liquidate_id`, IFNULL(`bill`.`id`,0) AS `bill_id`,`deal`.`id` AS `deal_id`,`deal`.`start_date` AS `deal_start_date`,`deal`.`name` AS `deal_name`,`city`.`name` AS `city_name`,`liquidate`.`total_quantity` AS `liquidate_total_quantity`,`calendar`.`since` AS `calendar_since`,`calendar`.`until` AS `calendar_until`, IFNULL(`bill`.`bac_sent`,`liquidate`.`created`) AS `liquidate_bac_sent`, IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY)) AS `max_payment_date`,`deal`.`discounted_price` AS `deal_discounted_price`,(IFNULL(`bill`.`total_quantity`,0) * `deal`.`discounted_price`) AS `bill_gross_amount`, IFNULL(`bill`.`total_amount`,0) AS `bill_total_amount`, IFNULL(`bill`.`billing_commission_percentage`,0) AS `bill_billing_commission_percentage`, ROUND((((IFNULL(`bill`.`discounted_price`,0) * (IFNULL(`bill`.`billing_commission_percentage`,0) / 100)) * IFNULL(`bill`.`total_quantity`,0)) * (IFNULL(`bill`.`billing_iva_percentage`,0) / 100)),2) AS `bill_iva_amount`, IFNULL(`bill`.`billing_iva_percentage`,0) AS `bill_iva_percentage`,0 AS `liquidate_iibb_amount`,`liquidate`.`liquidating_iibb_percentage` AS `liquidate_iibb_percentage`,`liquidate`.`total_amount` AS `liquidate_total_amount`, ROUND(((`liquidate`.`discounted_price` * `liquidate`.`total_quantity`) * (`liquidate`.`liquidating_guarantee_fund_percentage` / 100)),2) AS `liquidate_guarantee_funds`
FROM ((((`accounting_items` `liquidate`
JOIN `accounting_calendars` `calendar` ON((`liquidate`.`accounting_calendar_id` = `calendar`.`id`)))
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0) AND (`liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)));

-- accounting_company_id_grouped;
CREATE OR REPLACE VIEW `accounting_company_id_grouped` AS
SELECT `deal`.`company_id` AS `deal_company_id`,`deal`.`pay_by_redeemed` AS `pay_by_redeemed`, CAST(`deal`.`start_date` AS DATE) AS `deal_start_date`, SUM(`liquidate`.`total_quantity`) AS `liquidate_total_quantity`,`liquidate`.`id` AS `liquidate_id`,`liquidate`.`discounted_price` AS `liquidate_discounted_price`,`liquidate`.`liquidating_guarantee_fund_percentage` AS `liquidate_guarantee_fund_percentage`, ROUND(((`liquidate`.`discounted_price` * `liquidate`.`total_quantity`) * (`liquidate`.`liquidating_guarantee_fund_percentage` / 100)),2) AS `liquidate_guarantee_fund_amount`, SUM(IFNULL(`bill`.`total_amount`,0)) AS `bill_total_amount`, SUM(`liquidate`.`total_amount`) AS `liquidate_total_amount`, CAST(MAX(IF((`deal`.`pay_by_redeemed` = 0), IF(((`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY) >= NOW()),(`deal`.`start_date` + INTERVAL `deal`.`payment_term` DAY),(`liquidate`.`created` + INTERVAL 7 DAY)),(`liquidate`.`created` + INTERVAL `deal`.`payment_term` DAY))) AS DATE) AS `max_payment_date`,`deal`.`id` AS `deal_id`,`deal`.`name` AS `deal_name`,`city`.`name` AS `city_name`,`deal`.`downpayment_percentage` AS `downpayment_percentage`
FROM ((((`accounting_items` `liquidate`
LEFT JOIN `accounting_items` `bill` ON((`liquidate`.`accounting_item_id` = `bill`.`id`)))
JOIN `deals` `deal` ON((`liquidate`.`deal_id` = `deal`.`id`)))
JOIN `companies` `company` ON((`liquidate`.`model_id` = `company`.`id`)))
LEFT JOIN `cities` `city` ON((`deal`.`city_id` = `city`.`id`)))
WHERE ((`liquidate`.`deleted` = 0) AND (`liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED')) AND (`liquidate`.`model` = 'Company') AND (`company`.`validated` = 1) AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY','BILL_BY_REDEEMED_FOR_COMPANY')) OR ISNULL(`bill`.`accounting_type`)) AND ((`bill`.`model` = 'Company') OR ISNULL(`bill`.`model`)))
GROUP BY `deal`.`company_id`,`liquidate`.`id`;

-- accounting_deal_id_grouped;
CREATE OR REPLACE VIEW `accounting_deal_id_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`,`acg`.`pay_by_redeemed` AS `pay_by_redeemed`,`acg`.`deal_id` AS `deal_id`,`acg`.`deal_name` AS `deal_name`,`acg`.`deal_start_date` AS `deal_start_date`,`acg`.`city_name` AS `city_name`,`acg`.`liquidate_guarantee_fund_percentage` AS `liquidate_guarantee_fund_percentage`,`acg`.`liquidate_id` AS `liquidate_id`, SUM(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`, SUM(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`, SUM(`acg`.`bill_total_amount`) AS `bill_total_amount`, SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` `acg`
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`,`acg`.`deal_id`;

-- accounting_downpayment_percentage_grouped;
CREATE OR REPLACE VIEW `accounting_downpayment_percentage_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`, SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` `acg`
WHERE (`acg`.`downpayment_percentage` > 0)
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`;

-- accounting_guarantee_fund_grouped;
CREATE OR REPLACE VIEW `accounting_guarantee_fund_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`,`acg`.`pay_by_redeemed` AS `pay_by_redeemed`, SUM(`acg`.`liquidate_guarantee_fund_amount`) AS `liquidate_guarantee_fund_amount`
FROM `accounting_company_id_grouped` `acg`
WHERE (`acg`.`pay_by_redeemed` <> 1)
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`;

-- accounting_max_payment_date_grouped;
CREATE OR REPLACE VIEW `accounting_max_payment_date_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`, SUM(IFNULL(`acg`.`bill_total_amount`,0)) AS `bill_total_amount`, SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` `acg`
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`;

-- accounting_redeemed_grouped;
CREATE OR REPLACE VIEW `accounting_redeemed_grouped` AS
SELECT `acg`.`deal_company_id` AS `deal_company_id`,`acg`.`max_payment_date` AS `max_payment_date`,`acg`.`pay_by_redeemed` AS `pay_by_redeemed`, SUM(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`, SUM(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` `acg`
GROUP BY `acg`.`deal_company_id`,`acg`.`max_payment_date`,`acg`.`pay_by_redeemed`;

-- deal_cupon_summary_view;
CREATE OR REPLACE VIEW `deal_cupon_summary_view` AS
SELECT `deal`.`id` AS `deal_id`, SUM(IF(((`du`.`deal_id` = `deal`.`id`) AND (`du`.`deleted` = 1) AND (((`deal`.`pay_by_redeemed` = 1) AND (`du`.`is_used` = 1)) OR (`deal`.`pay_by_redeemed` = 0))),1,0)) AS `coupon_returned_count_cc`, SUM(IF(((`du`.`deal_id` = `deal`.`id`) AND ((`du`.`deleted` = 1) OR (`du`.`is_returned` = 1)) AND (((`deal`.`pay_by_redeemed` = 1) AND (`du`.`is_used` = 1)) OR (`deal`.`pay_by_redeemed` = 0))),1,0)) AS `coupon_returned_count_ccnow`, SUM(IF(((`du`.`deal_id` = `deal`.`id`) AND (`du`.`is_used` = 1) AND (((`du`.`is_returned` = 0) AND (`du`.`deleted` = 0) AND (`deal`.`pay_by_redeemed` = 1)) OR (`deal`.`pay_by_redeemed` = 0))),1,0)) AS `coupon_redemed_count`, SUM(IF(((`du`.`deal_id` = `deal`.`id`) AND (((`du`.`is_returned` = 0) AND (`du`.`deleted` = 0) AND (`deal`.`pay_by_redeemed` = 1) AND (`du`.`is_used` = 1)) OR ((`deal`.`pay_by_redeemed` = 0) AND (`du`.`is_returned` = 0) AND (`du`.`deleted` = 0)))),1,0)) AS `coupon_count`, SUM(IF(((`du`.`deal_id` = `deal`.`id`) AND (`du`.`is_billed` = 1) AND (((`deal`.`pay_by_redeemed` = 1) AND (`du`.`is_used` = 1)) OR (`deal`.`pay_by_redeemed` = 0))),1,0)) AS `coupon_billed`, SUM(IF(((UNIX_TIMESTAMP(`deal`.`coupon_expiry_date`) < UNIX_TIMESTAMP(NOW())) AND (`du`.`deal_id` = `deal`.`id`)),1,0)) AS `coupon_expired_count`
FROM (`deals` `deal`
LEFT JOIN `deal_users` `du` ON((`deal`.`id` = `du`.`deal_id`)))
GROUP BY `deal`.`id`;

-- deal_view;
CREATE OR REPLACE VIEW `deal_view` AS
SELECT `deal`.`id` AS `deal_id`,`deal`.`company_id` AS `company_id`,`deal`.`start_date` AS `deal_start_date`,`deal`.`end_date` AS `deal_end_date`,`deal`.`name` AS `deal_name`,`deal`.`deal_status_id` AS `deal_status_id`,`deal`.`original_price` AS `deal_original_price`,`deal`.`discount_percentage` AS `deal_discount_percentage`,`deal`.`discounted_price` AS `deal_discounted_price`,`deal`.`commission_percentage` AS `deal_commission_percentage`,`deal`.`is_now` AS `deal_is_now`,`deal`.`coupon_start_date` AS `coupon_start_date`,`deal`.`coupon_expiry_date` AS `coupon_expiry_date`,`deal`.`subtitle` AS `deal_subtitle`,`deal`.`parent_deal_id` AS `deal_parent_deal_id`,`city`.`id` AS `city_id`,`city`.`name` AS `city_name`,`deal`.`pay_by_redeemed` AS `deal_is_pay_by_redeemed`,`deal`.`is_end_user` AS `deal_is_precompra`,`schedule`.`id` AS `deal_schedule_id`,`cupons`.`coupon_redemed_count` AS `coupon_redemed_count`,`cupons`.`coupon_expired_count` AS `coupon_expired_count`,`cupons`.`coupon_returned_count_cc` AS `coupon_returned_count_cc`,`cupons`.`coupon_returned_count_ccnow` AS `coupon_returned_count_ccnow`,`cupons`.`coupon_count` AS `coupon_count`,`cupons`.`coupon_billed` AS `coupon_billed`
FROM ((`cities` `city`
JOIN `deal_cupon_summary_view` `cupons`)
JOIN (`deals` `deal`
LEFT JOIN `scheduled_deals` `schedule` ON((`schedule`.`deal_id` = `deal`.`id`))))
WHERE ((`deal`.`city_id` = `city`.`id`) AND (`cupons`.`deal_id` = `deal`.`id`));