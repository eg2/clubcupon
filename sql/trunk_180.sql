alter table company_candidates add column company_id bigint(20) unsigned COMMENT 'Id de la compania generada';
alter table company_candidates add constraint company_candidate_companies_fk FOREIGN KEY (company_id) REFERENCES companies (id) ON UPDATE RESTRICT ON DELETE RESTRICT;
