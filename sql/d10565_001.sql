--  CREACION DE TABLA SEARCH_LINK_LOGOS
--  CREACION DE TABLA SEARCH_LINK_STYLES

create table search_link_logos (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) unsigned NOT NULL,
  `modified_by` bigint(20) unsigned DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

create table search_link_styles (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) unsigned NOT NULL,
  `modified_by` bigint(20) unsigned DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `css_style` varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci  NULL,
  `css_class` varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;	 
