UPDATE `email_templates` SET email_content='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CC recordatorio</title>
</head>
<body>
<table width="593" align="center" cellpadding="0" cellspacing="0" height="300" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#4d4d4d;"><tr><td valign="top" height="93" colspan="2"><img src="http://imagenes.clubcupon.com.ar/2011/10/14/header.jpg" alt="Club Cupon - Recordatorio de Pago" border="0"/> </td></tr>
<tr>
<td colspan="2" style="padding:10px; border-left:1px solid #ddd; border-right:1px solid #ddd;"><b style="font-size:16px">&iexcl;No te olvides!</b><br /><br /><br />
  <b>##USERNAME##</b>, te recordamos que s&oacute;lo te quedan 24hs. para hacer efectiva la compra de
  <b>##DEAL_TITLE##</b>
  Acercate con tu cup&oacute;n de pago a la opci&oacute;n que hayas elegido (RapiPago, PagoF&aacute;cil, Banelco, Link o Provincia Pagos).
  <br /><br />
  
  <b style="font-size:16px; color:#FF6600">&iexcl;Segu&iacute; disfrutando de las ofertas todos los d&iacute;as!</b>
  
   </td>
</tr>

<tr>
<td width="300px" style="border-left:1px solid #ddd; padding-bottom:10px"><b style="padding-left:10px">&iexcl;Recomend&aacute; Club Cup&oacute;n a todos tus amigos!</b></td>
<td width="293px" style="border-right:1px solid #ddd; padding-bottom:10px"> <img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/facebook.gif" alt="Facebook" style="margin-left:20px" /><img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/twitter.gif" alt="Twitter" /></td>
</tr>

</table>


<table cellpadding="0" cellspacing="0" align="center"><tr>
<td width="20"><img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/left.jpg" border="0" /></td>
<td width="450" style="border-bottom:1px solid #ddd; border-top:1px solid #ebe4d1; font-size:10px; padding-left:10px">&copy; Club Cup&oacute;n es un servicio de Compa&ntilde;&iacute;a de Medios Digitales (CMD) S.A. | <a href="#">Contacto</a></td>
<td width="113" align="right"><img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/logo.jpg" alt="Club Cupon" border="0" /></td></tr>
</table>



</body>
</html>' where name ='Reminder offline payments' and is_html = 1 ;