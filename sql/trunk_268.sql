-- http://jira.int.clarin.com/browse/CC-3373

UPDATE payment_options SET
	name = REPLACE(REPLACE(name,'Mastercard','MasterCard'),'Master Card','MasterCard'),
	description = REPLACE(REPLACE(description,'Mastercard','MasterCard'),'Master Card','MasterCard')
WHERE name LIKE '%master%';