-- Crea el grupo de afinidad corporate mapfre
INSERT INTO `cities` (`created`, `modified`, `country_id`, `state_id`, `language_id`, `name`, `slug`, `deal_count`, `is_approved`, `is_group`)
VALUES (NOW(), NOW(), 284, 5421, 40, 'Mapfre', 'mapfre', 0, 1, 1);

-- Crea el grupo de afinidad corporate Tarjeta Naranja
INSERT INTO `cities` (`created`, `modified`, `country_id`, `state_id`, `language_id`, `name`, `slug`, `deal_count`, `is_approved`, `is_group`)
VALUES (NOW(), NOW(), 284, 5421, 40, 'Tarjeta Naranja', 'tarjeta-naranja', 0, 1, 1);