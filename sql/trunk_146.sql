DROP TABLE IF EXISTS branches ;
CREATE TABLE IF NOT EXISTS branches (
  id             bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  created        datetime NOT NULL,
  modified       datetime DEFAULT NULL,
  created_by     bigint(20) UNSIGNED DEFAULT NULL,
  modified_by    bigint(20) UNSIGNED DEFAULT NULL,
  company_id     bigint(20) UNSIGNED NOT NULL,
  country_id     bigint(20) UNSIGNED NOT NULL,
  state_id       bigint(20) NOT NULL,
  city_id        bigint(20) UNSIGNED NOT NULL,
  street_address varchar(128) NOT NULL,
  postal_code    varchar(16) NOT NULL,
  latitude       float DEFAULT NULL,
  longitude      float DEFAULT NULL,
  telephone      varchar(16) NOT NULL,
  fax_number     varchar(16) DEFAULT NULL,
  email          varchar(64) NOT NULL,
  deleted        tinyint(1) DEFAULT '0',
  PRIMARY KEY (id),
  CONSTRAINT branches_companies_fk FOREIGN KEY (company_id) REFERENCES companies (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT branches_countries_fk FOREIGN KEY (country_id) REFERENCES countries (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT branches_states_fk FOREIGN KEY (state_id) REFERENCES states (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT branches_cities_fk FOREIGN KEY (city_id) REFERENCES cities (id) ON UPDATE RESTRICT ON DELETE RESTRICT
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- A esta tabla seguramente hay que agregarle redundancia para resolver las busquedas
DROP TABLE IF EXISTS branches_deals;
CREATE TABLE IF NOT EXISTS branches_deals (
  id          bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  created     datetime NOT NULL,
  modified    datetime,
  created_by  bigint(20) UNSIGNED DEFAULT NULL,
  modified_by bigint(20) UNSIGNED DEFAULT NULL,
  deal_id     bigint(20) UNSIGNED DEFAULT NULL,
  branch_id   bigint(20) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT branches_deals_fk FOREIGN KEY (deal_id) REFERENCES deals (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT branches_deals_branches_fk FOREIGN KEY (branch_id) REFERENCES branches (id) ON UPDATE RESTRICT ON DELETE RESTRICT
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;