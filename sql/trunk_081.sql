-- payment_settings por defecto
insert into payment_settings (id, name) VALUES (0, 'Todos'); 

-- payment_options por defecto
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (1,0,20,'American Express','American Express por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (9,0,25,'Visa','Visa por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (6,0,22,'Mastercard','Mastercard por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (3,0,23,'Cabal','Cabal por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (5,0,28,'Italcred','Italcred por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (7,0,24,'Naranja','Naranja por NPS');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (9,0,7,'Hasta en 12 cuotas con Visa','Visa por MercadoPago');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (1,0,7,'Hasta en 12 cuotas con American Express','American Express por MercadoPago');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (6,0,7,'Hasta en 12 cuotas con Mastercard','Mastercard por MercadoPago');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (12,0,7,'Hasta en 12 cuotas con otras tarjetas','Otras tarjetas por MercadoPago');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (12,0,8,'En 1 pago por PagoFacil, Rapipago y otros medios de pago','Otros medios por MercadoPago');
insert into `payment_options`(`payment_method_id`,`payment_setting_id`,`payment_type_id`,`name`,`description`) values (12,0,5000,'Monedero','Monedero');
commit;