CREATE TABLE `zones` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`created` DATETIME NOT NULL,
	`modified` DATETIME NOT NULL,
	`code` BIGINT(20) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

alter table neighbourhoods add column zone_id bigint(20) DEFAULT NULL;
alter table neighbourhoods add  CONSTRAINT `fk_neighbourhoods` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON UPDATE NO ACTION;

alter table branches add column neighbourhood_id bigint(20);
alter table branches add CONSTRAINT fk_branches foreign key (neighbourhood_id) references neighbourhoods(id) on update no action;
