INSERT INTO `email_templates` (`id`, `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`) VALUES (NULL, '2011-11-24 11:11:43', '2011-11-24 11:11:48', '##FROM_EMAIL##', '##REPLY_TO_EMAIL##', 'Reminder expired coupons', 'Es un recordatorio de vencimiento del cupon', 'ClubCupon Recordatorio', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CC recordatorio</title>
</head>
<body>
<table width="593" align="center" cellpadding="0" cellspacing="0" height="300" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#4d4d4d;"><tr><td valign="top" height="93"><img src="##ABSOLUTE_IMG_PATH##header_expired_coupon.jpg" border="0"/> </td></tr>
<tr><td style="padding:10px; border-left:1px solid #ddd; border-right:1px solid #ddd;"><b>Hola ##USERNAME##</b><br /><br />
        Te informamos que el cup&oacute;n de la oferta que adquiriste:&nbsp;<a href="##URL##" target="_blank" style="color:#FF6600">##DEAL_TITLE##. Por ##PRICE##.</a> vencer&aacute; <a href="##URL##" target="_blank" style="color:#FF6600">el ##EXPIRATION##</a>.<br /><br />

Si no encontr&aacute;s el cup&oacute;n que te enviamos por mail, record&aacute; que pod&eacute;s descargarlo de tu cuenta
en <a href="http://www.clubcupon.com.ar" target="_blank" style="color:#FF6600">www.clubcupon.com.ar</a>. Ingres&aacute; con tu correo electr&oacute;nico y contrase&ntilde;a, y lo tendr&aacute;s disponible en
la secci&oacute;n "Mis cupones".<br /><br />

<b>Esperamos que disfrutes tu compra.</b><br /><br />
<div align="left" style=" width:150px; margin-left:400px;"><font><b>El equipo de Club Cup&oacute;n</b><br />
        Todos los d&iacute;as, una alegr&iacute;a<br />
www.clubcupon.com.ar</font></div></td></tr>
<tr><td><table cellpadding="0" cellspacing="0"><tr>
<td width="20"><img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/left.jpg" border="0" /></td>
<td width="466" style="border-bottom:1px solid #ddd; border-top:1px solid #ebe4d1; font-size:10px; padding-left:10px">© Club Cup&oacute;n es un servicio de Compa&ntilde;&iacute;a de Medios Digitales (CMD) S.A. | <a href="#">Contacto</a></td>
<td width="113" align="right"><img src="http://imagenes.clubcupon.com.ar/2011/10/14/img/logo.jpg" border="0" /></td></tr></table></td>
</tr>
</table>
</body>
</html>
', 'FROM_EMAIL, REPLY_TO_EMAIL, SITE_LINK, SITE_NAME, USERNAME, DEAL_TITLE, DESCRIPTIVE_TEXT, QUANTITY, DEAL_AMOUNT , PAYMENT_TYPE ', '1');