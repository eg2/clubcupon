-- Actualización de medidas del banner del tipo newsletter
UPDATE `banner_types` SET `width` = '598', `height` = '100' WHERE `banner_types`.`id` =2;

-- Estado para now deal "status_id"
ALTER TABLE `branches_deals` ADD `now_deal_status_id` INT( 2 ) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `branches_deals` CHANGE `deal_categories_id` `deal_category_id` INT( 11 ) NOT NULL DEFAULT '0';
