-- http://tracker.int.clarin.com/browse/CC-3195
UPDATE deals SET 
	campaign_code = CONCAT('d', LPAD(LOWER(HEX(id)),11,'0'))
WHERE DATE(start_date)>= '2013-03-01';

CREATE INDEX deals_campaign_code ON deals (campaign_code);