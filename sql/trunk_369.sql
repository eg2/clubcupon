-- http://jira.int.clarin.com/browse/CC-5346

ALTER TABLE `accounting_items`
	CHANGE COLUMN `is_visible` `is_visible` INT NOT NULL DEFAULT '0' AFTER `bac_payment_sent`;