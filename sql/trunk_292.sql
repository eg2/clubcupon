-- http://tracker.int.clarin.com/browse/CC-3745

ALTER TABLE `deals`
    ADD COLUMN `replicated_deal_id`
        BIGINT(20) UNSIGNED,
    ADD CONSTRAINT `deals_deals_replicated_fk`
        FOREIGN KEY (`replicated_deal_id`)
        REFERENCES `deals` (`id`);