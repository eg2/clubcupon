/*agrega tilde en el subject*/

update email_templates
set
subject='Se devolvi� tu oferta - ##SITE_NAME## Ya! - ##DEAL_NAME##'
where name='now deal draft';

/*agrega tildes en el content*/

update email_templates
set
email_content='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Oferta </title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agreg&aacute; <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top" background="##ABSOLUTE_IMG_PATH##signo.jpg" bgcolor="#FFFFFF" style="background-repeat: no-repeat; background-position: right -50px; padding: 10px; font: 24px arial;">
      <p style="font:16px arial; margin-left:40px; margin-bottom:100px; margin-top:50px;"><strong>Te informamos que Club Cup&oacute;n a&uacute;n <span style="font-style:italic;">no acept&oacute;<span>.</strong><br />
Nuestro Departamento Comercial se estar&aacute; comunicando con<br />vos para brindarte m&aacute;s informaci&oacute;n.
        </p>

        <p style="font:12px arial; padding-left:40px; padding-top:10px; border-top:1px solid #aaa">&iquest;Necesit&aacute;s ayuda? <a href="mailto:empresas@clubcupon.com.ar" style="color:#1388b4; text-decoration:none;">Contactate con nosotros</a></p></td>
      </tr>

    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cup&oacute;n es un servicio de CMD S.A | contacto@clubcupon.com.ar</p>
<p>&nbsp;</p>
</body>
</html>' where name='now deal draft';