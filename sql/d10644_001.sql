-- http://jira.int.clarin.com/browse/CC-5055
alter table deals add column is_resell tinyint(1) DEFAULT 0;