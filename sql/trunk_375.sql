-- http://jira.int.clarin.com/browse/CC-5405

ALTER TABLE `deals` CHANGE COLUMN `requisition_number` `requisition_number` BIGINT(20) NULL DEFAULT NULL AFTER `is_shipping_address`;
