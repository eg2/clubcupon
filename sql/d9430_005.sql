insert into newsletter_profiles (id, created, modified, code, name, send_to_cities, send_to_groups, vmta, template_name, email_from, description)
values 
(1, now(), now(), 'NA', 'NA', 0, 0, 'na', 'na.ctp', 'na@na.com', 'Configuracion no Aplicable'),
(2, now(), now(), 'A', 'A', 1, 1, 'vmta_a', 'premium_hotmail.ctp', 'info@clubcupon.com', 'Configuracion para clase A'),
(3, now(), now(), 'B', 'B', 1, 1, 'vmta_b', 'normal.ctp', 'info@clubcupon.com', 'Configuracion para clase B'),
(4, now(), now(), 'C', 'C', 1, 1, 'vmta_c', 'premium_hotmail.ctp', 'info@clubcupon.com', 'Configuracion para clase C'),
(5, now(), now(), 'D', 'D', 1, 1, 'vmta_d', 'default.ctp', 'info@clubcupon.com', 'Configuracion para clase D'),
(6, now(), now(), 'E', 'E', 1, 1, 'vmta_e', 'default.ctp', 'info@clubcupon.com', 'Configuracion para clase E');
update email_profiles set newsletter_profile_id_for_cities = 1, newsletter_profile_id_for_groups = 1;


