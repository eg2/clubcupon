-- Correr solamente en producción
UPDATE subscriptions
SET    is_voluntary = 1;

COMMIT;

UPDATE subscriptions s
SET    is_voluntary = 0
WHERE  EXISTS
         (SELECT 1
          FROM   subscription_origin o
          WHERE  o.import_instance_id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 15, 16) AND o.subscription_id = s.id);

COMMIT;
