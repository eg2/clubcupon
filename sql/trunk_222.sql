alter table deal_externals  
drop column publication_channel_type_id;

alter table deal_externals 
add column buy_channel_type_id  int(1)
default 1;