--- tourism deals
alter table  deals add column amount_exempt decimal(10,2) DEFAULT '0.0';
alter table  deals add column amount_full_iva decimal(10,2) DEFAULT '0.0';
alter table  deals add column amount_half_iva decimal(10,2) DEFAULT '0.0';