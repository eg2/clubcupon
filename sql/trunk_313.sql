CREATE OR REPLACE VIEW `deal_external_cupon_summary_view` AS
SELECT `deal`.`id` AS `deal_id`,
        SUM(IF(`du`.`deleted`=0 AND du.is_returned=0, 1, 0)) AS `coupon_count`,
        ABS(SUM(1) - IFNULL(de.quantity, 0))   AS `coupon_returned_count_cc`,
        ABS(SUM(1) - IFNULL(de.quantity, 0)) AS `coupon_returned_count_ccnow`,
        SUM(IF((`du`.`is_used` = 1 AND `du`.`deleted`=0 ), 1, 0)) AS `coupon_redemed_count`,
        SUM(IF((`deal`.`pay_by_redeemed` = 1 AND `du`.`is_used` = 1 AND `du`.`is_billed` = 1) OR (`deal`.`pay_by_redeemed` = 0 AND `du`.`is_billed` = 1), 1, 0)) AS `coupon_billed`,
        SUM(IF(((UNIX_TIMESTAMP(`deal`.`coupon_expiry_date`) < UNIX_TIMESTAMP(NOW())) ),1,0)) AS `coupon_expired_count`
FROM (`deals` `deal` LEFT JOIN `deal_users` `du` ON(`deal`.`id` = `du`.`deal_id` 
                                                    AND (deal.is_now = 0 || du.is_returned = 0)) 
                     LEFT JOIN deal_externals de ON((du.deal_external_id = de.id AND de.external_status='A' )))   
GROUP BY deal.id, de.id;