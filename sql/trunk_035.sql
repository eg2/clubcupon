-- Agrega los campos:
--   bool que es verdadero cuando el usuario tiene la wallet bloqueada
ALTER TABLE `users` ADD `wallet_blocked` TINYINT(1) DEFAULT FALSE AFTER `available_balance_amount`;
commit;
