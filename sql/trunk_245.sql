-- http://jira.int.clarin.com/browse/CC-3104
alter table deals ADD COLUMN is_receivership_delivery TINYINT(1) NOT NULL DEFAULT '0';
alter table deals ADD COLUMN is_home_delivery TINYINT(1) NOT NULL DEFAULT '0';