UPDATE `email_templates` SET email_content='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>&iexcl;Gracias por tu compra!</title>
    </head>

    <body style="background: #f7f7f7;">
        <table width="593" align="center" cellpadding="0" cellspacing="0" height="300" style="background: #fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#4d4d4d;">
            <tr>
                <td valign="top" height="93">
                    <img src="http://imagenes.clubcupon.com.ar/2011/07/20/img02/header.jpg" border="0"/>
                </td>
            </tr>
            <tr>
                  <td colspan="2" style="padding:10px; border-left:1px solid #ddd; border-right:1px solid #ddd;"><b style="font-size:16px">&iexcl;No te olvides!</b><br /><br /><br />
  <b>##USERNAME##</b>, te recordamos que s&oacute;lo te quedan 24hs. para hacer efectiva la compra de
  <b>##DEAL_TITLE##</b>
  Acercate con tu cup&oacute;n de pago a la opci&oacute;n que hayas elegido (RapiPago, PagoF&aacute;cil, Banelco, Link o Provincia Pagos).
  <br /><br />
  
  <b style="font-size:16px; color:#FF6600">&iexcl;Segu&iacute; disfrutando de las ofertas todos los d&iacute;as!</b>
  
   </td>
            </tr>
            <tr>
                <td style="background: #f7f7f7;">
                    <a href="mailto:contacto@clubcupon.com.ar " style="border:0; text-decoration: none;">
                        <img src="##ABSOLUTE_IMG_PATH##/email/pie_mail_deal_bought.jpg" style=" border:none;" />
                    </a>
                </td>
            </tr>
        </table>
    </body>

</html>' where name ='Reminder offline payments' and is_html = 1 ;