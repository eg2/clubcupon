ALTER TABLE product_inventories MODIFY COLUMN bac_libertya_id  varchar(255) DEFAULT NULL;
ALTER TABLE product_inventories ADD CONSTRAINT bac_libertya_id_uk UNIQUE(bac_libertya_id);

ALTER TABLE product_inventories MODIFY COLUMN bac_invoice_id  varchar(255) DEFAULT NULL;
ALTER TABLE product_inventories MODIFY COLUMN bac_portal_id  varchar(255) DEFAULT NULL;
