UPDATE email_templates SET subject='Activa tu cuenta de ##SITE_NAME## Ya!' WHERE name = 'Activation Request Candidate';
UPDATE email_templates SET subject='Tus datos fueron enviados a moderar - ##SITE_NAME## Ya!'  WHERE name = 'Candidate Info Update';
