INSERT INTO `email_templates` ( `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES(
	NOW(),
	NOW(),
	'##FROM_EMAIL##',
	'##REPLY_TO_EMAIL##',
	'Deal Bought Ya',
	'',
	'Compraste una oferta en ##SITE_NAME## Ya!',
	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenido a CCnow</title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agreg&aacute; <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top"  bgcolor="#FFFFFF" style=" padding: 10px; font: 16px arial;">
        <b>Hola ##USERNAME##</b><br /><br /><br />
                        <strong>Los detalles de la compra  son:</strong><br />
                        <br />
                        Nombre:   <strong>  ##DEAL_TITLE##    </strong><br />
                        Cantidad: <strong>  ##QUANTITY##      </strong><br />
                        Precio:   <strong>  ##DEAL_AMOUNT##   </strong><br />
                        Precio pagado por   ##PAYMENT_TYPE##: <strong>##DEAL_EXTERNAL_PAID##</strong><br />
                        Fecha de compra: <strong>##PURCHASE_ON##</strong><br /><br />
                        <b style="color:#F60">Tu pago se ha procesado exitosamente.  Recibirás el cupón y toda la información necesaria para que puedas usarlo.</b><br /><br /><br />
                        <a href="http://www.clubcupon.com.ar/ciudad-de-buenos-aires/firsts/recommend?utm_source=CC-compra&utm_medium=NL&utm_campaign=gracias-compra&utm_term=recomenda" target="_blank" ><b>
                        Sum&aacute; $10 por cada amigo que traigas a ClubCupon.</b></a> <br /><br />
                        <b>&iexcl;Gracias!</b><br /><br />
        </td>
      </tr>

    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cup&oacute;n es un servicio de CMD S.A | <a href="mailto:contacto@clubcupon.com.ar" >contacto@clubcupon.com.ar</a></p>
<p>&nbsp;</p>
</body>
</html>
',
	'FROM_EMAIL, REPLY_TO_EMAIL, SITE_NAME, USERNAME, ACTIVATION_URL, SITE_LINK',
	1
);