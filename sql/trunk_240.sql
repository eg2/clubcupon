-- Crea el grupo de afinidad corporate virginia_slims
INSERT INTO `cities` (`created`, `modified`, `country_id`, `state_id`, `language_id`, `name`, `slug`, `deal_count`, `is_approved`, `is_group`, `is_hidden`)
VALUES (NOW(), NOW(), 284, 5421, 40, 'Virginia Slims', 'virginiaslims', 0, 1, 1, 1);

-- Agrega el campo para almacenar el token de virginia slims
ALTER TABLE user_profiles ADD COLUMN  virginia_slims_code varchar(64);