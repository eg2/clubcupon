-- Almacena los referidos de los usuarios
CREATE TABLE `user_referrals` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`created` DATETIME NOT NULL ,
`modified` DATETIME NOT NULL ,
`user_id` BIGINT( 20 ) NOT NULL DEFAULT '0',
`ref_email` VARCHAR( 255 ) NOT NULL,
`registered` TINYINT NOT NULL DEFAULT '0'
) ENGINE = InnoDB;