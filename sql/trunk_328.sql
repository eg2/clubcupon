-- http://jira.int.clarin.com/browse/CC-4211
CREATE  TABLE `clubcupon`.`unsuscription_details` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `subscription_id` bigint(20) unsigned NOT NULL,
  `created` TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
  `reason_id` SMALLINT NOT NULL ,
  `or_operator` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`), 
  KEY `created` (`created`) )
ENGINE = InnoDB;


CREATE  TABLE `clubcupon`.`unsuscription_reasons` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `created` TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
  `description` VARCHAR(255) NOT NULL default '',
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


CREATE TABLE `subscriptions_tracker` (
  `id` bigint(20) unsigned,
  `email` VARCHAR (255),
  `created` datetime,
  `modified` datetime,
  `user_id` bigint(20),
  `is_subscribed` tinyint(1),
  `log_date` timestamp NOT NULL DEFAULT current_timestamp,
  `log_user` varchar(50) NOT NULL DEFAULT '',
  `log_connectionid` bigint(12) NOT NULL DEFAULT '0',
  `log_event` varchar(1) NOT NULL DEFAULT '',
  KEY `idx_id` (`id`),
  KEY `log_date` (`log_date`)
);


INSERT 
	INTO 
		unsuscription_reasons (description) 
	VALUES 	('OR - Rebote duro'),
			('OR - Cuenta de correo falsa'),
			('OR - A pedido del usuario'),
			('OR - A pedido de Atencion al Cliente'),
			('OR - A pedido de Administración'),
			('OR - A pedido de Edición'),
			('OR - A pedido de la Gerencia'),
			('OR - A pedido de Ventas'),
			('OR - Usuario inexistente'),
			('OR - Usuario fraudulento'),
			('OR - Usuario banneado'),
			('OR - Por pedido Judicial'),
			('OR - Requerimiento de Sistemas'),
			('OR - Otros motivos');

USE `clubcupon`;
DROP procedure IF EXISTS `or_dessuscribir_mails`;

DELIMITER $$
USE `clubcupon`$$
CREATE DEFINER=`root`@`172.16.48.%` PROCEDURE `or_dessuscribir_mails`(
	IN OR_MAIL VARCHAR(255), 
	IN OR_CITY_ID BIGINT, 
	IN OR_OPERADOR VARCHAR(40), 
	IN OR_MOTIVO VARCHAR(255))
rutina: BEGIN 
DECLARE filas_afectadas BIGINT;
DECLARE fecha timestamp DEFAULT current_timestamp;
-- Declaro la variable con el id de razon de suscripcion

DECLARE _reasonid SMALLINT;
SELECT id INTO _reasonid FROM unsubscription_reasons WHERE description = OR_MOTIVO;

-- Valido que la razon exista en la tabla unsubscription_reasons, sino devuelvo error

      IF (_reasonid IS NULL) THEN
         SELECT 'Error: El motivo de desuscripcion es invalido. Ver tabla `unsubscription_reasons`' AS resultado;
      LEAVE rutina;
      END IF;
   
-- Hago el update y el insert dentro de una transaccion
DROP TEMPORARY TABLE IF EXISTS `ids`;
CREATE TEMPORARY TABLE ids (id BIGINT(20));

START TRANSACTION;

INSERT 
	INTO ids 
	SELECT 
			id 
		FROM 
			subscriptions 
	WHERE 		
			email = OR_MAIL COLLATE utf8_unicode_ci 
		AND 
			(OR_CITY_ID = 0 OR city_id = OR_CITY_ID)
		AND
			is_subscribed = 1;

UPDATE 
		subscriptions, ids
   	SET 
      subscriptions.is_subscribed = 0,
		subscriptions.modified = now()
WHERE subscriptions.id = ids.id;


INSERT 
	INTO 
		unsubscription_details (subscription_id, created, reason_id, or_operator)
	SELECT 
        id,
		fecha,
        _reasonid, 
        OR_OPERADOR            
        FROM 
            ids;
        


-- Guardo la cantidad de filas afectadas dentro de la variable "filas_afectadas"

SELECT row_count() INTO filas_afectadas;
IF filas_afectadas > 0 THEN
    SELECT 
			*
		FROM (
				SELECT 
						s.*, u.created AS `Fecha_desuscripcion`, u.reason_id, u.or_operator
					FROM 
						subscriptions s
				INNER JOIN ids i ON i.id = s.id
				LEFT JOIN unsubscription_details u ON u.subscription_id = s.id AND u.created = fecha
				) a;

ELSE
    SELECT 'El email no se encuentra para la ciudad especificada o no esta suscription en la ciudad indicada' AS AVISO;
END IF;

COMMIT;

END$$

DELIMITER ;


DELIMITER &&
DROP TRIGGER if exists `upd_subscriptions` &&
CREATE DEFINER=`root`@`localhost` TRIGGER `upd_subscriptions` AFTER UPDATE ON `subscriptions` 
    FOR EACH ROW BEGIN
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;

IF 
	OLD.`modified` <> NEW.modified 	OR OLD.`user_id` <> NEW.user_id OR OLD.`is_subscribed` <> NEW.is_subscribed OR OLD.`email` <> NEW.email
THEN
	INSERT INTO `clubcupon`.`subscriptions_tracker`
	( 	`id`,
		`email`,
		`modified`,
		`user_id`,
		`is_subscribed`,
		`log_user`,
		`log_connectionid`,
		`log_event`)
	VALUES
	(	OLD.`id`,
		OLD.`email`,
		OLD.`modified`,
		OLD.`user_id`,
		OLD.`is_subscribed`,
		USER(),
		connection_id(),
		'U');

END IF;
END &&
DELIMITER ;

DELIMITER $$
DROP TRIGGER /*!50032 IF EXISTS */ `clubcupon`.`uniqueActiveSubscriptionCheck`$$
CREATE /*!50017 DEFINER = 'root'@'localhost' */ TRIGGER `uniqueActiveSubscriptionCheck` BEFORE INSERT ON `subscriptions` 
FOR EACH ROW 
	BEGIN
		DECLARE message VARCHAR(255);
		IF NEW.is_subscribed = 1 THEN
			IF EXISTS (SELECT 
								1 
						FROM subscriptions s
					   WHERE 
							s.city_id = NEW.city_id 
							and s.email = NEW.email 
							and s.is_subscribed = 1
							and s.id <> NEW.id) THEN
								select concat('Suscripcion duplicada para cuenta de correo \'',NEW.email,'\' de ciudad id = ',NEW.city_id) into message;
						 SIGNAL SQLSTATE '45000'
						   SET MESSAGE_TEXT = message;
		   END IF;
		END IF;
	END;
$$
DELIMITER ;

USE clubcupon;
DELIMITER &&
DROP TRIGGER /*!50032 IF EXISTS */ `clubcupon`.`udp_uniqueActiveSubscriptionCheck`&&
DROP TRIGGER /*!50032 IF EXISTS */ `clubcupon`.`upd_uniqueActiveSubscriptionCheck`&&
CREATE DEFINER=`root`@`localhost` TRIGGER `upd_uniqueActiveSubscriptionCheck` BEFORE UPDATE ON `subscriptions` 
    FOR EACH ROW 
	BEGIN
		DECLARE message VARCHAR(255);
        IF NEW.is_subscribed = 1 THEN
		IF EXISTS(SELECT 1 FROM subscriptions s
		       WHERE 
			    s.city_id = NEW.city_id 
			    and s.email = NEW.email 
			    and s.is_subscribed = 1
			    and s.id <> NEW.id) THEN
			select concat('Suscripcion duplicada para cuenta de correo \'',NEW.email,'\' de ciudad id = ',NEW.city_id) into message; 
		    SIGNAL SQLSTATE '45001'
		    SET MESSAGE_TEXT = message;
		END IF;
        END IF;
	END;
&&
DELIMITER ;
