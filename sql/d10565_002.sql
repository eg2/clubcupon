--  ASOCIACION TABLA SEARCH_LINK_LOGOS EN SEARCH_LINKS
--  ASOCIACION TABLA SEARCH_LINK_STYLES EN SEARCH_LINKS
alter table search_links  add column search_link_style_id bigint(20) unsigned NULL;
alter table search_links  add KEY search_link_style_id (search_link_style_id);
alter table search_links  add CONSTRAINT fk_search_link_style_id FOREIGN KEY (search_link_style_id) REFERENCES search_link_styles (id);
alter table search_links  add column search_link_logo_right_id bigint(20) unsigned NULL;
alter table search_links  add column search_link_logo_left_id bigint(20) unsigned NULL;
alter table search_links  add KEY search_link_logo_right_id (search_link_logo_right_id);
alter table search_links  add KEY search_link_logo_left_id (search_link_logo_left_id);
alter table search_links  add CONSTRAINT fk_search_link_logo_right_id FOREIGN KEY (search_link_logo_right_id) REFERENCES search_link_logos (id);
alter table search_links  add CONSTRAINT fk_search_link_logo_left_id FOREIGN KEY (search_link_logo_left_id) REFERENCES search_link_logos (id);

-- INSERT VALOR IMAGEN

INSERT INTO search_link_logos(`created`,`modified`,`created_by`,`modified_by`,`deleted`,`name`,`file_name`)
VALUES(now(), now(), '0', '0', '0', 'Sin logo', NULL);

INSERT INTO clubcupon.search_link_logos (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`name`,`file_name`)
VALUES (now(), now(), '0', '0', '0', 'CCYA Logo', 'ccya_search_link.jpg');

-- INSERT VALOR ESTILO

INSERT INTO search_link_styles (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`name`,`css_style`,`css_class`)
VALUES ( now(), now(), '0', '0', '0', 'default', NULL, NULL);

INSERT INTO search_link_styles (`created`,`modified`,`created_by`,`modified_by`,`deleted`,`name`,`css_style`,`css_class`)
VALUES (now(), now(), '0', '0', '0', 'Style CCYA', NULL, 'ccya_search_link');

-- MODIFICACION ALOW NULL TABLAS SEARCH_LINKS_LOGOS SEARCH_LINKS_STYLES

alter table search_link_logos modify column file_name varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
alter table search_link_styles modify column css_style varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
alter table search_link_styles modify column css_class varchar(150)  CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
