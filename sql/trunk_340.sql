-- http://jira.cmd.com.ar/browse/CC-4794

CREATE TABLE `search_link_types` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`type_description` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `search_link_types` (`id`, `type_description`) VALUES ('1', 'Destacado');
INSERT INTO `search_link_types` (`id`, `type_description`) VALUES ('2', 'Especial');

ALTER TABLE `search_links`
	ADD COLUMN search_link_type_id BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
	ADD CONSTRAINT fk_type_id FOREIGN KEY (`search_link_type_id`) REFERENCES `search_link_types` (`id`);

INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`, `city_id`) VALUES ('2', '1', 'Link Temporal Productos', '/productos', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));
INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`, `city_id`) VALUES ('2', '2', 'Link Temporal Bs As', '/ciudad-de-buenos-aires', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));
INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`, `city_id`) VALUES ('2', '3', 'Link Temporal Turismo', '/turismo', ( SELECT id FROM cities WHERE slug = 'ciudad-de-buenos-aires'));

