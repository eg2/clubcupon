CREATE VIEW `accounting_company_id_grouped` AS
SELECT `deal`.`company_id` AS `deal_company_id`,
        `deal`.`pay_by_redeemed` AS `pay_by_redeemed`,
        `deal`.`downpayment_percentage` AS `downpayment_percentage`,
        date(`deal`.`start_date`) AS `deal_start_date`,
        sum(`liquidate`.`total_quantity`) AS `liquidate_total_quantity`,
        `liquidate`.`id` AS `liquidate_id`,
        sum(ifnull(`bill`.`total_amount`,0)) AS `bill_total_amount`,
        sum(`liquidate`.`total_amount`) AS `liquidate_total_amount`,
        date(max(if((`deal`.`pay_by_redeemed` = 0),if(((`deal`.`start_date` + interval `deal`.`payment_term` DAY) >= now()),(`deal`.`start_date` + interval `deal`.`payment_term` DAY),(`liquidate`.`created` + interval 7 DAY)),(`liquidate`.`created` + interval `deal`.`payment_term` DAY)))) AS `max_payment_date`,
        `deal`.`id` AS `deal_id`,
        `deal`.`name` AS `deal_name`,
        `city`.`name` AS `city_name`
FROM
	(	(	(accounting_items AS liquidate LEFT JOIN accounting_items AS bill ON (liquidate.accounting_item_id = bill.id)
			) JOIN deals AS deal ON (liquidate.deal_id = deal.id)
		) JOIN companies AS company ON (liquidate.model_id = company.id)
	) LEFT JOIN cities AS city ON deal.city_id = city.id
WHERE ((`liquidate`.`deleted` = 0)
       AND (`liquidate`.`accounting_type` IN ('LIQUIDATE_BY_SOLD',
                                              'LIQUIDATE_BY_SOLD_EXPIRED',
                                              'LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL',
                                              'LIQUIDATE_BY_REDEEMED',
                                              'LIQUIDATE_BY_REDEEMED_EXPIRED'))
       AND (`liquidate`.`model` = 'Company')
       AND (`company`.`validated` = 1)
       AND ((`bill`.`accounting_type` IN ('BILL_BY_SOLD_FOR_COMPANY',
                                          'BILL_BY_REDEEMED_FOR_COMPANY'))
            OR isnull(`bill`.`accounting_type`))
       AND ((`bill`.`model` = 'Company')
            OR isnull(`bill`.`model`)))
GROUP BY `deal`.`company_id`,
	`liquidate`.`id`;

CREATE VIEW `accounting_deal_id_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       `acg`.`pay_by_redeemed`,
       `acg`.`deal_id`,
       `acg`.`deal_name`,
       `acg`.`deal_start_date`,
       `acg`.`city_name`,
       `acg`.`downpayment_percentage`,
       `acg`.`liquidate_id`,
       sum(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`,
       sum(`acg`.`bill_total_amount`) AS `bill_total_amount`,
       sum(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` AS `acg`
GROUP BY `acg`.`deal_company_id`,
         `acg`.`max_payment_date`,
         `acg`.`deal_id`;

CREATE VIEW `accounting_max_payment_date_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       sum(ifnull(`acg`.`bill_total_amount`,0)) AS `bill_total_amount`,
       sum(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` AS `acg`
GROUP BY `acg`.`deal_company_id`,
	 `acg`.`max_payment_date`;

CREATE VIEW `accounting_redeemed_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       `acg`.`pay_by_redeemed`,
       sum(`acg`.`liquidate_total_quantity`) AS `liquidate_total_quantity`,
       sum(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` AS `acg`
GROUP BY `acg`.`deal_company_id`,
	 `acg`.`max_payment_date`,
	 `acg`.`pay_by_redeemed`;

CREATE VIEW `accounting_downpayment_percentage_grouped` AS
SELECT `acg`.`deal_company_id`,
       `acg`.`max_payment_date`,
       sum(`acg`.`liquidate_total_amount`) AS `liquidate_total_amount`
FROM `accounting_company_id_grouped` AS `acg`
WHERE `acg`.`downpayment_percentage` > 0
GROUP BY `acg`.`deal_company_id`,
	 `acg`.`max_payment_date`;


