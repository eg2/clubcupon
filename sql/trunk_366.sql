-- http://jira.int.clarin.com/browse/CC-5343

ALTER TABLE `payment_types` ALTER `gateway_key` DROP DEFAULT;
ALTER TABLE `payment_types` CHANGE COLUMN `gateway_key` `gateway_key` ENUM('nps','dim','wallet', 'decidir') NOT NULL AFTER `gateway_id`;

INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (101, now(), now(), 'American Express', 35, 'decidir', 1, 20);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (102, now(), now(), 'MasterCard', 35, 'decidir', 1, 22);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (103, now(), now(), 'Cabal', 35, 'decidir', 1, 23);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (104, now(), now(), 'Naranja', 35, 'decidir', 1, 24);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (105, now(), now(), 'Visa', 35, 'decidir', 1, 25);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (106, now(), now(), 'Italcred', 35, 'decidir', 1, 28);
INSERT INTO `payment_types` (`id`, `created`, `modified`, `name`, `gateway_id`, `gateway_key`, `is_active`, `bac_payment_type`) VALUES (107, now(), now(), 'PagoMisCuentas', 35, 'decidir', 1, 90);

INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (136, 1, 19, 101, 'American Express', 'American Express por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (137, 9, 19, 105, 'Visa', 'Visa por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (138, 6, 19, 102, 'MasterCard', 'MasterCard por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (139, 3, 19, 103, 'Cabal', 'Cabal por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (140, 5, 19, 106, 'Italcred', 'Italcred por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (141, 7, 19, 104, 'Naranja', 'Naranja por Decidir', 0);
INSERT INTO `payment_options` (`id`, `payment_method_id`, `payment_setting_id`, `payment_type_id`, `name`, `description`, `is_offline`) VALUES (142, 19, 19, 107, 'PagoMisCuentas', 'PagoMisCuentas por Decidir', 0);

INSERT INTO `payment_settings` (`id`, `name`) VALUES (19, 'Por Decidir');
--
UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 101
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (20, 95);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 102
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (22, 92, 96);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 103
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (23, 97);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 104
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (24, 98);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 105
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (25, 99);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 106
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (28, 100);

UPDATE deals Deal INNER JOIN payment_option_deals PaymentOptionDeal ON Deal.id = PaymentOptionDeal.deal_id SET
  PaymentOptionDeal.payment_option_id = 107
WHERE Deal.deal_status_id IN (1, 2, 5)
AND PaymentOptionDeal.payment_option_id IN (93);
--
DELETE FROM `payment_settings` WHERE `id` IN (1, 8, 10, 15, 16, 18);
