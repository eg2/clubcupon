-- http://jira.int.clarin.com/browse/CC-5448
DELIMITER //
DROP PROCEDURE IF EXISTS `update_deals_on_search_index`;
CREATE PROCEDURE `update_deals_on_search_index`()
BEGIN
  DELETE
    FROM
      search_index
    WHERE
      model = 'Deal';
  INSERT
  INTO
    search_index
    (association_key, model, `data`, created, modified)  SELECT
      id AS association_key,
      'Deal' AS model,
      name AS `data`,
      NOW() AS created,
      NOW() AS modified
    FROM
      deals
    WHERE
      created BETWEEN DATE_ADD(NOW(), INTERVAL -180 DAY) AND NOW();
END //
DELIMITER ; 