
-- LIST DESTACADOS - Córdoba, La Plata, Mendoza, Rosario

	-- Productos (Especial : productos-para-el-interior)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Viajes y Escapadas (Especial : turismo)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Ofertas Nacionales (Especial : ofertas-nacionales)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Fotografía (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Sushi (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Día de campo (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '1', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '2', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '3', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'1', '4', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

-- TAB ESPECIALES - Córdoba, La Plata, Mendoza, Rosario

	-- Productos (Especial : productos-para-el-interior)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Productos', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Productos+para+el+interior&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);


	-- Viajes y Escapadas (Especial : turismo)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Viajes y Escapadas', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Viajes+y+Escapadas&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Ofertas Nacionales (Especial : ofertas-nacionales)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--cordoba-page--1-query--oooo-facet--city_name_no_group.C%25C3%25B3rdoba', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--la~plata-page--1-query--oooo-facet--city_name_no_group.La+Plata', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--mendoza-page--1-query--oooo-facet--city_name_no_group.Mendoza', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Ofertas Nacionales', '/search/search_querys/makesearch?setFacetField=city_name_group&setFacet=Ofertas+Nacionales&scV=-city--rosario-page--1-query--oooo-facet--city_name_no_group.Rosario', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Cine (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Cine', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--Cine', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Fotografía (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Fotografía', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--fotos', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Sushi (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Sushi', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--sushi', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

	-- Día de campo (término de búsqueda)

	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '1', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Córdoba&scV=-city--cordoba-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'cordoba')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '2', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=La+Plata&scV=-city--la~plata-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'la-plata')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '3', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Mendoza&scV=-city--mendoza-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'mendoza')
	);
	INSERT INTO `search_links` (`search_link_type_id`, `priority`, `label`, `url`,  `city_id`) VALUES (
		'2', '4', 'Día de campo', '/search/search_querys/makesearch?setFacetField=city_name_no_group&setFacet=Rosario&scV=-city--rosario-page--1-query--d%25C3%25ADa%2Bde%2Bcampo', 
		(SELECT id FROM cities WHERE slug = 'rosario')
	);

