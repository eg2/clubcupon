-- Se agregan las suscripciones del prelanzamiento
insert into `subscriptions` (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
  (
    select NOW(), NOW(), null, 42550, `p`.`email`, true
    from `prelanzamiento` `p`
    where 0 = 0
      and not exists (select * from `subscriptions` `s` where `s`.`email` = `p`.`email`)
  );
commit;

-- Se agregan las suscripciones de GBA
insert into `subscriptions` (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
  (
    select NOW(), NOW(), null, 42550, `g`.`email`, true
    from `subscriptions_gba` `g`
    where 0 = 0
      and not exists (select * from `subscriptions` `s` where `s`.`email` = `g`.`email`)
  );
commit;

-- Se agregan las suscripciones temporales
insert into `subscriptions`
  (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
(select
       NOW(),      NOW(),      null,     42550, `t`.`email`,            true
from `temporal` `t`
where 0 = 0
  and not exists (select * from `subscriptions` `s` where `s`.`email` = `t`.`email`));
commit;

insert into `subscriptions`
  (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
(select
       NOW(),      NOW(),      null,     42550, `t`.`email`,            true
from `temp_emails` `t`
where 0 = 0
  and not exists (select * from `subscriptions` `s` where `s`.`email` = `t`.`email`));
commit;

insert into `subscriptions`
  (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
(select
       NOW(),      NOW(),      null,     42550, `t`.`email`,            true
from `temp_emails2` `t`
where 0 = 0
  and not exists (select * from `subscriptions` `s` where `s`.`email` = `t`.`email`));
commit;

insert into `subscriptions`
  (`created`, `modified`, `user_id`, `city_id`,     `email`, `is_subscribed`)
(select
       NOW(),      NOW(),      null,     42550, `t`.`email`,            true
from `temp_subscriptions` `t`
where 0 = 0
  and not exists (select * from `subscriptions` `s` where `s`.`email` = `t`.`email`));
commit;


-- Se eliminan las tablas "temp"
drop table if exists `temp_contacts`;
drop table if exists `temp_emails`;
drop table if exists `temp_emails2`;

-- Se eliminan tablas "temp" de produccion
drop table if exists `temp_cordoba`;
drop table if exists `temp_coupons`;
drop table if exists `temp_cordoba`;
drop table if exists `temp_subscriptions`;

-- Se eliminan las tablas "z"
drop table if exists `z_deals`;
drop table if exists `z_emilio`;
drop table if exists `z_temp_deals`;
drop table if exists `z_temp_email_templates`;
drop table if exists `z_temp_email_templates_3`;
drop table if exists `z_temp_email_templates_4`;
drop table if exists `z_temp_users`;
drop table if exists `z_tmp_email_templates_2`;
drop table if exists `z_tmp_email_templates_3`;

-- Se eliminan las tablas dudosas
drop table if exists `prelanzamiento`;
drop table if exists `subscriptions_gba`;
drop table if exists `temporal`;
