DROP TABLE IF EXISTS cat_links;
CREATE TABLE `search_links` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` BIGINT(20) UNSIGNED NOT NULL,
	`modified_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`deleted` TINYINT(1) NOT NULL DEFAULT '0',
	`city_id` BIGINT(20) UNSIGNED NOT NULL,
	`url` VARCHAR(500) NOT NULL COLLATE 'utf8_unicode_ci',
	`label` VARCHAR(150) NOT NULL COLLATE 'utf8_unicode_ci',
	`priority` INT(11) NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `city_id` (`city_id`),
	INDEX `priority` (`priority`),
	FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB;
