-- http://tracker.int.clarin.com/browse/CC-3592

ALTER TABLE cities ADD COLUMN `is_user_subscription_allowed` TINYINT(1) NOT NULL DEFAULT '1';