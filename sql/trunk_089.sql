-- Agrega el campo "esta pregunta acepta multiples respuestas" a "questions"
alter table `questions` add `multiple` tinyint (1) default false after `content`;