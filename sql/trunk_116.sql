--Tabla de referencias para el behaviour searchable
CREATE TABLE `search_index` (
        `id` int(11) NOT NULL auto_increment,
        `association_key` varchar(36) NOT NULL,
        `model` varchar(128) collate utf8_unicode_ci NOT NULL,
        `data` longtext collate utf8_unicode_ci NOT NULL,
        `created` datetime NOT NULL,
        `modified` datetime NOT NULL,
        PRIMARY KEY  (`id`),
        KEY `association_key` (`association_key`,`model`),
        FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;