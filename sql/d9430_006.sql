delete from frecuency_event_classes;
INSERT INTO `frecuency_event_classes` (id, `created`, `modified`, `deleted`, `code`, `name`, `priority`)
VALUES
(1, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'A', 'premium', '1'),
(2, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'B', 'normal', '2'),
(3, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'C', 'bajo', '3'),
(4, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'D', 'no_open_mail_subscription_menor', '4'),
(5, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'E', 'no_open_mail_subscription_mayor', '5'),
(99, '2014-05-23 12:00:00', '2014-05-23 12:00:00', '0', 'NC', 'no_class', '0');
