alter table cities add column email_from varchar(150);

ALTER TABLE `deal_categories` ADD `icon` VARCHAR( 50 ) NOT NULL DEFAULT 'todas';

-- A esta tabla seguramente hay que agregarle redundancia para resolver las busquedas
-- Efectivamente!! La anterior no respetana las naming conventions de cakephp
DROP TABLE IF EXISTS branches_deals;
CREATE TABLE IF NOT EXISTS branches_deals (
  id          bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  created     datetime NOT NULL,
  modified    datetime,
  created_by  bigint(20) UNSIGNED DEFAULT NULL,
  modified_by bigint(20) UNSIGNED DEFAULT NULL,
  now_deal_id     bigint(20) UNSIGNED DEFAULT NULL,
  now_branch_id   bigint(20) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT branches_now_deals_fk FOREIGN KEY (now_deal_id) REFERENCES deals (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT branches_now_deals_branches_fk FOREIGN KEY (now_branch_id) REFERENCES branches (id) ON UPDATE RESTRICT ON DELETE RESTRICT
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `branches_deals` ADD `ts_inicio` TIMESTAMP NOT NULL DEFAULT 0,
ADD `ts_fin` TIMESTAMP NOT NULL DEFAULT 0,
ADD `latitud` FLOAT NOT NULL ,
ADD `longitud` FLOAT NOT NULL,  
ADD `deal_categories_id` INT NOT NULL DEFAULT 0


CREATE TABLE `logs` (
`id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`title` VARCHAR( 512 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`description` VARCHAR( 512 ) NOT NULL ,
`model` VARCHAR( 512 ) NOT NULL ,
`model_id` INT NOT NULL ,
`action` VARCHAR( 512 ) NOT NULL ,
`user_id` INT NOT NULL ,
`change` VARCHAR( 1024 ) NOT NULL
) ENGINE = InnoDB;

ALTER TABLE `deal_users` ADD `is_returned` INT NOT NULL DEFAULT '0';