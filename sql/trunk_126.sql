-- agrega un campo para almacenar la IP desde la cual se suscribe un usuario
alter table `subscriptions` add `subscription_ip` VARCHAR(15);

-- agrega las variables de email-template
update `email_templates`
set `email_variables` = CONCAT(`email_variables`, ', SUBSCRIBER_EMAIL, SUBSCRIBER_IP, SUBSCRIPTION_DATE, SUBSCRIPTION_TIME')
where `name` = 'Subscription Welcome Mail';

--
commit;