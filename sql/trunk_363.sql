-- http://jira.int.clarin.com/browse/CC-5261

INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Cine', 'cine.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Electro', 'electro.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Entretenimiento', 'entretenimiento.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Fotografía', 'fotografia.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Gastronomía', 'gastronomia.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Productos', 'productos.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Salud y belleza', 'salud_y_belleza.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Sushi', 'sushi.png');
INSERT INTO `search_link_logos` (`created`, `modified`, `created_by`, `modified_by`, `deleted`, `name`, `file_name`) VALUES (now(), now(), 0, 0, 0, 'Vinos', 'vinos.png');