-- Agrega los campos:
--   string de nombre de empresa a medida
--   string de telefono de contacto de empresa a medida
--   string de direccion (1) de empresa a medida
--   string de direccion (2) de empresa a medida
--   string de nombre de ciudad de empresa a medida
--   string de nombre de provincia de empresa a medida
--   string de nombre de pais de empresa a medida
--   int de codigo postal de empresa a medida
ALTER TABLE `deals` ADD `custom_company_name`          varchar(255) DEFAULT NULL AFTER `custom_subject`;
ALTER TABLE `deals` ADD `custom_company_contact_phone` varchar(20)  DEFAULT NULL AFTER `custom_company_name`;
ALTER TABLE `deals` ADD `custom_company_address1`      varchar(50)  DEFAULT NULL AFTER `custom_company_contact_phone`;
ALTER TABLE `deals` ADD `custom_company_address2`      varchar(50)  DEFAULT NULL AFTER `custom_company_address1`;
ALTER TABLE `deals` ADD `custom_company_city`          varchar(45)  DEFAULT NULL AFTER `custom_company_address2`;
ALTER TABLE `deals` ADD `custom_company_state`         varchar(45)  DEFAULT NULL AFTER `custom_company_city`;
ALTER TABLE `deals` ADD `custom_company_country`       varchar(50)  DEFAULT NULL AFTER `custom_company_state`;
ALTER TABLE `deals` ADD `custom_company_zip`           int(11)      DEFAULT NULL AFTER `custom_company_country`;
