START TRANSACTION;
--
UPDATE
  product_products AS p 
SET
  p.has_pins = 1  
WHERE
  EXISTS (
    SELECT
      1  
    FROM
      deals AS d  
    INNER JOIN
      pins AS i 
        ON d.id = i.deal_id  
    WHERE
      d.deal_status_id IN (
        1, 2, 5  
      ) 
      AND d.product_product_id = p.id  
  );
--
UPDATE
  pins AS p
INNER JOIN
  deals AS d
    ON p.deal_id = d.id
SET
  p.product_id = d.product_product_id
WHERE
  d.deal_status_id IN (
    1, 2, 5
  );
--
COMMIT;