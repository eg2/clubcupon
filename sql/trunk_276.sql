DROP VIEW IF EXISTS accounting_grouped_by_calendars;
CREATE OR REPLACE VIEW accounting_calendars_grouped AS
SELECT deal.company_id AS deal_company_id, 
 liquidate.accounting_calendar_id AS liquidate_accounting_calendar_id, 
 MAX(calendar.since) AS calendar_since, 
 MAX(calendar.until) AS calendar_until, 
 SUM(liquidate.total_quantity) AS liquidate_total_quantity, 
 SUM(IFNULL(bill.total_amount,0)) AS bill_total_amount, SUM(liquidate.total_amount) AS liquidate_total_amount, 
 MAX(IF(deal.pay_by_redeemed = 0, IF(DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY) >= NOW(), DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY), DATE_ADD(liquidate.created, INTERVAL 7 DAY)), DATE_ADD(liquidate.created, INTERVAL deal.payment_term DAY))) AS max_payment_date
FROM accounting_calendars AS calendar
INNER JOIN accounting_items AS liquidate ON calendar.id = liquidate.accounting_calendar_id
LEFT OUTER
JOIN accounting_items AS bill ON liquidate.accounting_item_id = bill.id
INNER JOIN deals AS deal ON liquidate.deal_id = deal.id
INNER JOIN companies AS company ON liquidate.model_id = company.id
WHERE liquidate.accounting_type IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED') AND liquidate.model = 'Company' AND company.validated = 1 AND ((bill.accounting_type IN ('BILL_BY_SOLD_FOR_COMPANY', 'BILL_BY_REDEEMED_FOR_COMPANY')) OR (bill.accounting_type IS NULL)) AND ((bill.model = 'Company') OR (bill.model IS NULL))
GROUP BY deal.company_id, liquidate.accounting_calendar_id;

DROP VIEW IF EXISTS accounting_grouped_by_calendar_items;
CREATE OR REPLACE VIEW accounting_calendars_grouped_items AS
SELECT deal.company_id as deal_company_id, 
       liquidate.id AS liquidate_id, 
       liquidate.accounting_calendar_id AS liquidate_accounting_calendar_id, 
       deal.start_date AS deal_start_date,
       IFNULL(deal.campaign_code, '0') AS deal_campaign_code, 
       deal.id AS deal_id, 
       CONCAT(deal.name, IF(deal.descriptive_text IS NOT NULL, ' - ', ''), IFNULL(deal.descriptive_text,'')) AS deal_name, 
       city.name AS city_name, 
       liquidate.total_quantity AS liquidate_total_quantity, 
       IFNULL(bill.total_amount,0) AS bill_total_amount, 
       liquidate.total_amount AS liquidate_total_amount, 
       IFNULL(bill.bac_sent, liquidate.created) AS liquidate_bac_sent, 
       if(deal.pay_by_redeemed = 0, if(DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY) >= now(),DATE_ADD(deal.start_date, INTERVAL deal.payment_term DAY), DATE_ADD(liquidate.created, INTERVAL 7 DAY)), DATE_ADD(liquidate.created, INTERVAL deal.payment_term DAY)) max_payment_date
FROM accounting_items AS liquidate
LEFT OUTER JOIN accounting_items AS bill ON liquidate.accounting_item_id = bill.id
INNER JOIN deals AS deal ON liquidate.deal_id = deal.id
INNER JOIN cities AS city ON deal.city_id = city.id
WHERE liquidate.accounting_type IN ('LIQUIDATE_BY_SOLD','LIQUIDATE_BY_SOLD_EXPIRED','LIQUIDATE_BY_SOLD_EXPIRED_WITHOUT_BILL','LIQUIDATE_BY_REDEEMED','LIQUIDATE_BY_REDEEMED_EXPIRED') 
AND liquidate.model = 'Company' 
AND ((bill.accounting_type IN ('BILL_BY_SOLD_FOR_COMPANY', 'BILL_BY_REDEEMED_FOR_COMPANY')) OR (bill.accounting_type IS NULL))
AND ((bill.model = 'Company') OR (bill.model IS NULL));