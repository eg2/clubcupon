UPDATE `email_templates`
SET `email_variables` = CONCAT(`email_variables`, ', COUPON_PIN')
WHERE `name` in ('Deal Coupon', 'Deal Coupon Gift');
COMMIT;