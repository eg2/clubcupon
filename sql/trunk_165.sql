INSERT INTO `email_templates` ( `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES(
	NOW(),
	NOW(),
	'##FROM_EMAIL##',
	'##REPLY_TO_EMAIL##',
	'Activation Request Candidate',
	'We will send this mail, when user registering an account for a company he/she will get an activation request.',
	'Activa tu cuenta de ##SITE_NAME##',
	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenido a CCnow</title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agrega <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top" background="##ABSOLUTE_IMG_PATH##signo.jpg" bgcolor="#FFFFFF" style="background-repeat: no-repeat; background-position: right -50px; padding: 10px; font: 24px arial;">
        <p style="margin-top:10px; margin-left:40px; padding-bottom:0px;">Bienvenidos a <strong>Club Cup&#195;&#179;n <span style="color:#1388b4;">Ya!</span></strong>
		<br /><span style="font:16px arial; margin-left:0px;">Para continuar tu registro de empresa activ&#195;&#161; tu cuenta.</span></p>
        
        <p><a href="##ACTIVATION_URL##" style="margin-top:10px; margin-left:40px; margin-bottom:40px; display:block"><img src="##ABSOLUTE_IMG_PATH##btn.jpg" width="119" height="30" /></a></p>
        <p style="font:12px arial; padding-left:40px; padding-top:10px; border-top:1px solid #aaa">&#194;&#191;Necesit&#195;&#161;s ayuda? <a href="" style="color:#1388b4; text-decoration:none;">contactate con nosotros</a></p></td>
      </tr>
      
    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cup&#195;&#179;n es un servicio de CMD S.A | contacto@clubcupon.com.ar</p>
<p style="font:12px arial; text-align:center">Me quiero <a href="#" style="color:#0e7394">desuscribir</a> del newsletter.</p>
<p>&nbsp;</p>
</body>
</html>',
	'FROM_EMAIL, REPLY_TO_EMAIL, SITE_NAME, USERNAME, ACTIVATION_URL, SITE_LINK',
	1
);
INSERT INTO `email_templates` ( `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES(
	NOW(),
	NOW(),
	'##FROM_EMAIL##',
	'##REPLY_TO_EMAIL##',
	'Candidate Info Update',
	'We will send this mail, when user registering an account for a company he/she will get an activation request.',
	'Modificaste los datos de tu cuenta - ##SITE_NAME##',
	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenido a CCnow</title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agrega <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top" background="##ABSOLUTE_IMG_PATH##signo.jpg" bgcolor="#FFFFFF" style="background-repeat: no-repeat; background-position: right -50px; padding: 10px; font: 24px arial;">
        <p style="font:16px arial; margin-left:40px; margin-bottom:100px; margin-top:50px;">Tus datos serán revisados y te informaremos por este medio cuando sean aprobados.</p>

        <p style="font:12px arial; padding-left:40px; padding-top:10px; border-top:1px solid #aaa">¿Necesitás ayuda? <a href="" style="color:#1388b4; text-decoration:none;">contactate con nosotros</a></p></td>
      </tr>
      
    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cupón es un servicio de CMD S.A | contacto@clubcupon.com.ar</p>
<p style="font:12px arial; text-align:center">Me quiero <a href="#" style="color:#0e7394">desuscribir</a> del newsletter.</p>
<p>&nbsp;</p>
</body>
</html>
',
	'FROM_EMAIL, REPLY_TO_EMAIL, SITE_NAME, USERNAME, ACTIVATION_URL, SITE_LINK',
	1
);
INSERT INTO `email_templates` ( `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES(
	NOW(),
	NOW(),
	'##FROM_EMAIL##',
	'##REPLY_TO_EMAIL##',
	'Candidate acepted',
	'We will send this mail, when user registering an account for a company he/she will get an activation request.',
	'Tus datos fueron aceptados - ##SITE_NAME##',
	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenido a CCnow</title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agrega <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top" background="##ABSOLUTE_IMG_PATH##signo.jpg" bgcolor="#FFFFFF" style="background-repeat: no-repeat; background-position: right -50px; padding: 10px; font: 24px arial;">
        <p style="margin-top:20px; margin-left:40px; padding-bottom:0px; font:16px arial;"><span style="font:16px arial; margin-left:0px;"><strong>Tus datos han sido aprobados!</strong></span><br />
        Ya puedes empezar a publicar en <strong>Club Cup&#195;&#179;n <span style="color:#1388b4;">Ya!</span></strong>		</p>
        
        <p><a href="asd" style="margin-top:10px; margin-left:40px; margin-bottom:40px; display:block"><img src="##ABSOLUTE_IMG_PATH##btn-publicar.jpg" width="" height="30" /></a></p>
        <p style="font:12px arial; padding-left:40px; padding-top:10px; border-top:1px solid #aaa">&#194;&#191;Necesit&#195;&#161;s ayuda? <a href="" style="color:#1388b4; text-decoration:none;">contactate con nosotros</a></p></td>
      </tr>
      
    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cup&#195;&#179;n es un servicio de CMD S.A | contacto@clubcupon.com.ar</p>
<p style="font:12px arial; text-align:center">Me quiero <a href="#" style="color:#0e7394">desuscribir</a> del newsletter.</p>
<p>&nbsp;</p>
</body>
</html>',
	'FROM_EMAIL, REPLY_TO_EMAIL, SITE_NAME, USERNAME, ACTIVATION_URL, SITE_LINK',
	1
);
INSERT INTO `email_templates` ( `created`, `modified`, `from`, `reply_to`, `name`, `description`, `subject`, `email_content`, `email_variables`, `is_html`)
VALUES(
	NOW(),
	NOW(),
	'##FROM_EMAIL##',
	'##REPLY_TO_EMAIL##',
	'Candidate reject',
	'We will send this mail, when user registering an account for a company he/she will get an activation request.',
	'Tus datos fueron recahzados - ##SITE_NAME##',
	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenido a CCnow</title>
</head>

<body>
<p style="font:12px arial; text-align:center">Agrega <span style="color:#0e7394">info@clubcupon.com.ar</span> a tu lista de contactos para asegurarte de recibir nuestras comunicaciones.</p>
<table width="600" border="1" align="center" cellpadding="1" cellspacing="20" bgcolor="#4f5054">
  <tr>
    <td style="border:1px solid #000"><table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#FFFFFF"><img src="##ABSOLUTE_IMG_PATH##header.jpg" alt="" width="599" height="94" /></td>
      </tr>
      <tr>
        <td height="auto" valign="top" background="##ABSOLUTE_IMG_PATH##signo.jpg" bgcolor="#FFFFFF" style="background-repeat: no-repeat; background-position: right -50px; padding: 10px; font: 24px arial;">
        <p style="font:16px arial; margin-left:40px; margin-bottom:100px; margin-top:50px;">Te informamos que tus datos de empresa no han sido aprobados.<br><b>##MESSAGE##</b></p>

        <p style="font:12px arial; padding-left:40px; padding-top:10px; border-top:1px solid #aaa">&#194;&#191;Necesit&#195;&#161;s ayuda? <a href="" style="color:#1388b4; text-decoration:none;">contactate con nosotros</a></p></td>
      </tr>
      
    </table>
    </td>
  </tr>
</table>
<p style="font:12px arial; text-align:center">Club Cup&#195;&#179;n es un servicio de CMD S.A | contacto@clubcupon.com.ar</p>
<p style="font:12px arial; text-align:center">Me quiero <a href="#" style="color:#0e7394">desuscribir</a> del newsletter.</p>
<p>&nbsp;</p>
</body>
</html>',
	'FROM_EMAIL, REPLY_TO_EMAIL, SITE_NAME, USERNAME, ACTIVATION_URL, SITE_LINK',
	1
);
