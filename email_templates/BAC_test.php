<?php

ini_set('soap.wsdl_cache_enabled', 1);
ini_set('soap.wsdl_cache_dir', '.');
ini_set('soap.wsdl_cache_ttl', 86400);
$BAC_wsdl = 'http://nap11.int.clarin.com:1985/backend_webServices/services/BackEndWS?wsdl';
$BAC_id_portal = 4;
$pairs = array(
    198190 => 7637,
    198187 => 23398,
    144007 => 6933,
    191643 => 22435,
    198165 => 23394,
    152771 => 3748,
    198144 => 23387,
    80089 => 492,
    198148 => 23389,
    198114 => 23374,
    198126 => 23380,
    198128 => 23381,
    198110 => 21941,
    198114 => 23374,
    198114 => 23374,
    153011 => 7672,
    198115 => 23059,
    130901 => 5928,
    198111 => 23373,
    198110 => 21941,
    188454 => 21530,
    198101 => 23369,
    198094 => 23366,
    88046 => 2704,
    182068 => 16973,
    198089 => 23363,
    190547 => 22167,
    147741 => 7247,
    172768 => 9025,
    198081 => 23359,
    198080 => 23356,
    99371 => 415,
    99371 => 415,
    198073 => 23354,
    153364 => 7693,
    198071 => 23335,
    198070 => 23353,
    198068 => 1784,
    198065 => 13281,
    181934 => 16878,
    182257 => 17051,
    134766 => 6033,
    198061 => 23344,
    83736 => 1895,
    198059 => 23342,
    191934 => 22617,
    198052 => 23334,
    75644 => 932,
    198048 => 23333,
    198047 => 23332,
    198046 => 23331,
    107277 => 3208,
    188537 => 11857,
    184161 => 17797,
    190917 => 22110,
    180317 => 16190,
    75926 => 1143,
    81222 => 2042
);

function isBacOk($user_id, $deal_external_id) {
    try {
        $sopa = new SoapClient($BAC_wsdl);
        $params = array(
            'idPortal' => $BAC_id_portal,
            'idUsuarioPortal' => $user_id,
            'idPagoPortal' => $deal_external_id
        );
        $ret = array(
            'status' => TRUE,
            'ret' => $sopa->consultarPago($params)
        );
    } catch (Exception $e) {
        $ret = array(
            'status' => FALSE,
            'ret' => $e
        );
    }
    return $ret;
}

foreach ($pairs as $user_id => $deal_external_id)
    var_dump(isBacOk($user_id, $deal_external_id));
