<?php

function p2sN($a) {
    return is_null($a) ? "NULL" : $a;
}

function p2sS($a) {
    return is_null($a) ? "NULL" : "'" . $a . "'";
}

// Datos de la conexion
// $host = '10.100.1.178';
// $user = 'ccdeploy';
// $pass = 'ccd123';
// $schm = 'ccdeploy';
// $port = '3306';

$host = 'localhost';
$user = 'root';
$pass = '';
$schm = 'clubcupon';
$port = '3306';

// Conectarse y morir si falla
$conn = mysqli_connect($host, $user, $pass, $schm, $port);
if (mysqli_connect_error ())
    die("No es posible establecer la conexión: " . mysqli_connect_error () . '\n');

// Buscar primera fila de resultados
if (!($res_de = mysqli_query($conn, "SELECT * FROM `deal_externals` WHERE `external_status` = 'A'")))
    echo "No se pueden obtener deal_externals:\n" . mysqli_error($conn) . "\n";
// Por cada una, buscar cupones
while ($row_de = mysqli_fetch_assoc($res_de)) {
    // Inicializar la cantidad de cupones "buenos"
    $quant = $row_de ['quantity'];
    // Tomar filas de cupones
    if (!($res_du = mysqli_query($conn, "SELECT * FROM `deal_users` WHERE `user_id` = " . $row_de ['user_id'] .
                    " AND `deal_id` = " . $row_de ['deal_id'] .
                    " AND `deal_external_id` = " . $row_de ['id']))) {
        echo "No se puede obtener deal_user con user_id = " . $row_de ['user_id'] .
        ", deal_id = " . $row_de ['deal_id'] .
        ", y deal_external_id = " . $row_de ['deal_external_id'] . ":\n" . mysqli_error($conn) . "\n";

        continue;
    }
    // Ignorar los primeros "quant" cupones
    while (mysqli_fetch_assoc($res_du) && --$quant
        );
    // Al resto cancelarlos
    while ($row_du = mysqli_fetch_assoc($res_du)) {
        // Agregar a cupones anulados
        if (!(mysqli_query($conn, "INSERT INTO `anulled_coupons` VALUES (" . $row_du ['id'] . ", " .
                        "'" . $row_du ['created'] . "'" . ", " .
                        "'" . $row_du ['modified'] . "'" . ", " .
                        $row_du ['user_id'] . ", " .
                        $row_du ['deal_id'] . ", " .
                        $row_du ['deal_external_id'] . ", " .
                        p2sS($row_du ['coupon_code']) . ", " .
                        $row_du ['quantity'] . ", " .
                        p2sN($row_du ['discount_amount']) . ", " .
                        p2sN($row_du ['payment_type_id']) . ", " .
                        p2sN($row_du ['is_paid']) . ", " .
                        $row_du ['is_repaid'] . ", " .
                        $row_du ['is_gift'] . ", " .
                        p2sS($row_du ['gift_to']) . ", " .
                        p2sS($row_du ['gift_from']) . ", " .
                        p2sS($row_du ['gift_email']) . ", " .
                        p2sS($row_du ['message']) . ", " .
                        p2sN($row_du ['is_used']) . ", " .
                        $row_du ['emailed'] . ", " .
                        p2sN($row_du ['email_to_company']) . ", " .
                        "'Anulado por proceso.'" . ")")))
            echo "No se puede anular el deal_user con user_id = " . $row_du ['user_id'] .
            ", deal_id = " . $row_du ['deal_id'] .
            ", y deal_external_id = " . $row_du ['deal_external_id'] . ":\n" . mysqli_error($conn) . "\n";
    }
    // Cerrar resultados
    mysqli_free_result($res_du);
}
// Cerrar resultados
mysqli_free_result($res_de);

// Borrar los cupones anulados
if (!(mysqli_query($conn, "DELETE FROM `deal_users` WHERE `id` IN (SELECT `id` FROM `anulled_coupons`)")))
    echo "No se pueden borrar los deal_users de anulled_coupons.\n";

// Cerrar conexión a la base
mysqli_close($conn);
?>
