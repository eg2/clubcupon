<?php

/* Configuracion del proceso */
$PROC_timeout = 2;  // segundos que espera entre cada pago

/* Configuracion BAC */
/* esto es test */
//$BAC_wsdl_url    = 'http://nap11.int.clarin.com:43011/backend_webServices/services/BackEndWS?wsdl';
//$BAC_post_url    = 'http://nap11.int.clarin.com:43011/backend_web/pago/crearPago.htm';
/* esto es produccion */
$BAC_wsdl_url    = 'http://nap11.int.clarin.com:1985/backend_webServices/services/BackEndWS?wsdl';
$BAC_post_url    = 'https://bac.cmd.com.ar/backend_web/pago/crearPago.htm';
$BAC_id_portal   = 4;
$BAC_id_producto = 2001;
$BAC_pesos       = 1;

/* Configuracion de la DB */
/* esto es localhost */
/*
$DB_host = 'localhost';
$DB_user = 'root';
$DB_pass = 'root';
$DB_schm = 'clubcupon';
$DB_port = '3306';
*/
/* esto es test */
/*
$DB_host = '10.100.1.178';
$DB_user = 'ccdeploy';
$DB_pass = 'ccd123';
$DB_schm = 'ccdeploy';
$DB_port = 3306;
 *
 */
/* esto es produccion */

$DB_host = '10.100.1.120';
$DB_user = 'clubcupon';
$DB_pass = 'clubcup0n';
$DB_schm = 'clubcupon';
$DB_port = null;


/* Configuracion de CC */
$CC_cantidad_cuotas         = 1;
$CC_id_gateway              = 100;  // esto es el gateway de MP
$CC_payment_types           = array (7, 8);
$CC_payment_type_id_for_BAC = 100;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Muestra los pares clave/valor de un array
 *
 * Dada una array de pares clave/valor, muestra ambos, uno por linea.
 *
 * @access private
 * @param array(key=>value) $arr  array a mostrar
 * @return void
 */
function _echoArray ($arr) { foreach ($arr as $key => $value)  echo "[" . $key . "] = " . $value . "\n"; }

/**
 * Muestra los valores de un array
 *
 * Dada una array de valores, muestra uno por linea.
 *
 * @access private
 * @param array(value) $arr  array a mostrar
 * @return void
 */
function _echoArrayVals ($arr) { foreach ($arr as $value)  echo $value . "\n"; }

/**
 * Crea un string POST
 *
 * Dada una array de valores, arma el string POST (lo que va despues del '?') listo para mandar.
 *
 * @access private
 * @param array(option=>value) $args  lista de opciones a transformar
 * @return string
 */
function _makePOSTstring ($args)
  {
    $POSTstrs = array ();
    foreach ($args as $key => $value)
      $POSTstrs [] = $key . '=' . urlencode ($value);

    return implode ('&', $POSTstrs);
  }

/*
 * String de consulta a deal_externals acreditados y pagados por MP
 *
 * String SQL de consulta por los deal_externals que estan acreditados (estado 'A') y se pagaron con MP.
 *
 * @access protected
 * @return string
 */
function DB_dealExternals_credited_with_MP ()
  {
    global $CC_payment_types;

    return "SELECT *, round((`deal_externals`.`parcial_amount` / `deal_externals`.`quantity`)) `unit_amount` FROM `deal_externals` WHERE `payment_type_id` IN (" . implode (', ', $CC_payment_types) . ") AND created < STR_TO_DATE('01,6,2011','%d,%m,%Y') AND `external_status` = 'A' limit 50";
  }

/*
 * Retorna un producto BAC con los parametros dados
 *
 * Arma un string de descripcion de producto BAC para la cantidad y el precio dados.
 *
 * @access protected
 * @param int $quantity  cantidad de items en el producto
 * @param float $price  precio unitario de cada item
 * @return string
 */
function BAC_makeProduct ($quantity, $price)
  {
    global $BAC_id_producto, $BAC_pesos;

    return $BAC_id_producto . ',' . $quantity . ',' . $price . ',' . $BAC_pesos;
  }

/*
 * Verifica que un pago exista en BAC
 *
 * Retorna verdadero sii el pago en BAC existe con el user_id y deal_external_id pasados.
 *
 * @access protected
 * @param int $user_id  id de usuario para el cual consultar
 * @param int $deal_external_id  id del deal_external para el cual consultar
 * @return bool|string
 */
function BAC_paymentExists ($user_id, $deal_external_id)
  {
    global $BAC_wsdl_url, $BAC_id_portal;

    try
      {
        $sopa = new SoapClient ($BAC_wsdl_url);
        $sopa->consultarPago (array ('idPortal' => $BAC_id_portal, 'idUsuarioPortal' => $user_id, 'idPagoPortal' => $deal_external_id));
      }
    catch (Exception $e)  { return $e->getMessage (); }

    return true;
  }

/*
 * Registra un pago en BAC
 *
 * Registra un pago en BAC y retorna verdadero sii el pago existe tras haber sido creado.
 *
 * @access protected
 * @param int $user_id  id del usuario para el cual se genera el pago
 * @param int $deal_external_id  id del deal_external para el cual se genera el pago
 * @param string $productos  string de producto BAC
 * @param int $payment_type_id  id de tipo de pago que se utiliza
 * @return bool|string
 */
function BAC_createPayment ($user_id, $deal_external_id, $productos, $payment_type_id)
  {
    global $PROC_timeout, $BAC_post_url, $BAC_id_portal, $CC_id_gateway, $CC_cantidad_cuotas;

    // Inicializar la lista de errores
    $err = array ();

    // Inicializar conexion y acumular errores si hace falta
    $ch = curl_init ($BAC_post_url);
    $err [] = curl_error ($ch);
    // Setear opciones y acumular errores si hace falta
    curl_setopt_array ($ch, array (
      CURLOPT_HEADER         => false,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST           => true,
      CURLOPT_POSTFIELDS     => _makePOSTstring (array (
        'ID_USUARIO_PORTAL' => $user_id,
        'ID_PORTAL'         => $BAC_id_portal,
        'PRODUCTOS'         => $productos,
        'ID_PAGO_PORTAL'    => $deal_external_id,
        'ID_MEDIO_PAGO'     => $payment_type_id,
        'ID_GATEWAY'        => $CC_id_gateway,
        'CANTIDAD_CUOTAS'   => $CC_cantidad_cuotas,
      )),
    ));
    $err [] = curl_error ($ch);
    // Ejecutar peticion y acumular errores si hace falta
    curl_exec ($ch);
    $err [] = curl_error ($ch);
    // Cerrar la conexion y acumular errores si hace falta
    curl_close  ($ch);
    // Forzar a que esperemos, para evitarnos SDoS
    sleep ($PROC_timeout);

    // Consultar por el pago agregado
    if (($b = BAC_paymentExists ($user_id, $deal_external_id)) !== true)
      {
        // Si hubo errores, retornarlos
        $err [] = $b;
        return implode ("\n", array_filter ($err));
      }

    return true;
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

echo "\n";

// Inicializar las listas
$EX = array ();
$OK = array ();
$KO = array ();

// Conectarse y morir si falla
$DB_conn = mysqli_connect ($DB_host, $DB_user, $DB_pass, $DB_schm, $DB_port);
if (mysqli_connect_error ())  die ("No es posible conectarse a la base de datos: " . mysqli_connect_error () . "\n");

// Buscar los deal_externals que hagan falta, avisar si falla
if (!($DB_res = mysqli_query ($DB_conn, DB_dealExternals_credited_with_MP ())))  echo ("No se pueden obtener deal_externals:\n" . mysqli_error ($DB_conn) . "\n");

// Buscar la cantidad de ellos que hay, inicializar contador a 0
$n = mysqli_num_rows ($DB_res); $i = 0;

// Por cada uno, procesarlo
while ($row = mysqli_fetch_assoc ($DB_res))
// $row = mysqli_fetch_assoc ($DB_res);
  {
    // Mostrar progreso
    printf ("(% " . strlen ((string) $n) . "u / %u)\n", ++$i, $n);
    // Consultar pago en BAC
    if (BAC_paymentExists ($row ['user_id'], $row ['id']) === true)
      {
        // si ya existe agregarlo a la lista de existentes
        $EX [] = $row ['id'];
      }
    // Registrar pago en BAC
    elseif (($e = BAC_createPayment ($row ['user_id'], $row ['id'], BAC_makeProduct ($row ['quantity'], $row ['unit_amount']), $CC_payment_type_id_for_BAC)) === true)
      {
        // si se pudo crear, agregarlo a la lista de OKs
        $OK [] = $row ['id'];
      }
    else
      {
        // si no se pudo crear, agregarlo a la lista de KOs
        $KO [$row ['id']] = $e;
      }
  }

// Cerrar resultados
mysqli_free_result ($DB_res);
// Cerrar conexión a la base
mysqli_close ($DB_conn);

// Mostrar resultados
echo ("\n");
if (count ($EX) != 0) { echo ("Pagos que ya existian en BAC ("              . count ($EX) . "):\n"); _echoArrayVals ($EX); echo ("\n"); }
if (count ($KO) != 0) { echo ("Pagos que no fue posible registrar en BAC (" . count ($KO) . "):\n"); _echoArray     ($KO); echo ("\n"); }
if (count ($OK) != 0) { echo ("Pagos que se registraron en BAC ("           . count ($OK) . "):\n"); _echoArrayVals ($OK); echo ("\n"); }
?>
