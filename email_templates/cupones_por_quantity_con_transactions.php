<?php

/** Configuracion de la DB */
/** esto es localhost */
$DB_host = 'localhost';
$DB_user = 'root';
$DB_pass = 'root';
$DB_schm = 'clubcupon';
$DB_port = '3306';
/** esto es test */
/*
$DB_host = '10.100.1.178';
$DB_user = 'ccdeploy';
$DB_pass = 'ccd123';
$DB_schm = 'ccdeploy';
$DB_port = null;
*/
/** esto es produccion */
/*
$DB_host = '10.100.1.120';
$DB_user = 'clubcupon';
$DB_pass = 'clubcup0n';
$DB_schm = 'clubcupon';
$DB_port = null;
*/



////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones utilitarias ///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Genera un UUID
 *
 * Genera un UUID de la forma XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX.
 *
 * @access private
 * @return string
 */
function _uuid ()
  {
    return sprintf ('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand (0, 0xffff),
      mt_rand (0, 0xffff),
      mt_rand (0, 0xffff),
      mt_rand (0, 0x0fff) | 0x4000,
      mt_rand (0, 0x3fff) | 0x8000,
      mt_rand (0, 0xffff),
      mt_rand (0, 0xffff),
      mt_rand (0, 0xffff));
  }



/**
 * Escapea strings de tipos especificos
 *
 * En caso de pasarse un bool, retorna 'true' o 'false' ()dependiendo de su valor, en caso contrario
 * (string) $data.
 *
 * @access private
 * @param mixed $data  elemento escapear
 * @return string
 */
function _doString ($data)
  {
    return is_bool ($data) ? ($data ? "true" : "false") : (string) $data;
  }



/**
 * Escapea strings vacios (hacia null)
 *
 * En caso de pasarse un null o un string vacio, retorna 'NULL', en caso contrario el valor string
 * de $data.
 *
 * @access private
 * @param string $data  string a escapear
 * @return string
 */
function _SQLEmptyStringIsNull ($data)
  {
    return ($data === null || $data === '') ? 'NULL' : '"' . _doString ($data) . '"';
  }



/**
 * Escapea strings vacios (hacia false)
 *
 * En caso de pasarse un null, retorna 'false', en caso contrario el valor string de $data.
 *
 * @access private
 * @param string $data  string a escapear
 * @return string
 */
function _SQLNullIsFalse ($data)
  {
    return ($data === null) ? 'false' : _doString ($data);
  }



////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones de bajo nivel /////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function GetGeneric ($conn, $id, $col = 'id', $table)
  {
    // Buscamos el elemento, si hay errores retornar false
    if (($res = mysqli_query ($conn, 'SELECT * from `' . $table . '` WHERE `' . $col . '` = ' . $id . '')) === false)
      {
        mysqli_free_result ($res);
        return false;
      }

    // Metemos el elemento en una array asociativa
    $rets = array ();
    while ($row = mysqli_fetch_assoc ($res))
      $rets [] = $row;
    // Liberamos la consulta
    mysqli_free_result ($res);

    return $rets;
  }

function GetGenerics ($conn, $ids, $col = 'id', $table)
  {
    // Inicializar la lista de retorno
    $ret = array ();

    // Llamar a GetGeneric con cada id y quedarnos solo con el resultado de las que no fallaron
    foreach ($ids as $id)
      if (($res = GetGeneric ($conn, $id, $col, $table)) !== false)
        $ret [$id] = $res;

    // Solo retornar false si todas fallaron
    return (count ($ret) != 0) ? $ret : false;
  }

function SetGeneric ($conn, $fields_values, $table)
  {
    // Armamos las listas de campos y valores
    $fields = array ();
    foreach (array_keys ($fields_values) as $field)  $fields [] = '`' . $field . '`';
    $fields = implode (', ', $fields);
    $values = implode (', ', array_values ($fields_values));

    // Insertamos la fila y retornamos lo que retorne la query
    if (mysqli_query ($conn, 'INSERT INTO `' . $table . '` (' . $fields . ') VALUES (' . $values . ')') === false)
      return false;

    return mysqli_insert_id ($conn);
  }

function SetGenerics ($conn, $fields_valueses, $table)
  {
    // Inicializar la lista de retorno
    $ret = array ();

    // Llamar a GetDealUser con cada id y quedarnos con el resultado
    foreach ($fields_valueses as $key => $fields_values)
      $ret [$key] = SetGeneric ($conn, $fields_values, $table);

    return $ret;
  }

function ModGeneric ($conn, $fields_values, $table, $id, $col = 'id')
  {
    // Armamos las listas de campos y valores
    $fields = array ();
    foreach ($fields_values as $field => $value)  $fields [] = '`' . $field . '`=' . $value;
    $fields = implode (', ', $fields);

    // Updateamos la fila y retornamos lo que retorne la query
    if (mysqli_query ($conn, 'UPDATE `' . $table . '` SET ' . $fields . ' WHERE `' . $col . '`=' . $id . '') === false)
      return false;

    return mysqli_insert_id ($conn);
  }



function GetDealExternal ($conn, $id, $col = 'id')
  {
    // Buscamos el deal_external, si hay errores retornar false
    if (($rets = GetGeneric ($conn, $id, $col, 'deal_externals')) === false)  return false;
    // Decodificamos gift_data
    foreach ($rets as & $ret)
      $ret ['gift_data'] = json_decode ($ret ['gift_data'], true);

    return $rets;
  }

function GetDealExternals ($conn, $ids, $col = 'id')
  {
    // Buscamos todos los DealExternals, si hay errores retornar false
    if (($retss = GetGenerics ($conn, $ids, $col, 'deal_externals')) === false)  return false;
    // Decodificamos gift_data para todos los deal_externals
    foreach ($retss as & $rets)
      foreach ($rets as & $ret)
        $ret ['gift_data'] = json_decode ($ret ['gift_data'], true);

    return $retss;
  }

function SetDealExternal ($conn, $fields_values)
  {
    // Codificamos gift_data
    if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Guardamos el DealExternal
    return SetGeneric ($conn, $fields_values, 'deal_externals');
  }

function SetDealExternals ($conn, $fields_valueses)
  {
    // Codificamos gift_data para todos los deal_externals
    foreach ($fields_valueses as & $fields_values)
      if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Guardamos todos los DealExternals
    return SetGenerics ($conn, $fields_valueses, 'deal_externals');
  }

function ModDealExternal ($conn, $fields_values, $id, $col = 'id')
  {
    // Codificamos gift_data
    if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Modificamos el DealExternal
    return ModGeneric ($conn, $fields_values, 'deal_externals', $id, $col);
  }



function GetDealUser ($conn, $id, $col = 'id')
  {
    return GetGeneric  ($conn, $id, $col, 'deal_users');
  }

function GetDealUsers ($conn, $ids, $col = 'id')
  {
    return GetGenerics ($conn, $ids, $col, 'deal_users');
  }

function SetDealUser ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'deal_users');
  }

function SetDealUsers ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'deal_users');
  }

function ModDealUser ($conn, $fields_values, $id, $col = 'id')
  {
    return ModGeneric ($conn, $fields_values, 'deal_users', $id, $col);
  }




function GetDeal ($conn, $id,  $col = 'id')
  {
    return GetGeneric ($conn, $id, $col, 'deals');
  }

function GetDeals ($conn, $ids, $col = 'id')
  {
    return GetGenerics ($conn, $ids, $col, 'deals');
  }

function SetDeal ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'deals');
  }

function SetDeals ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'deals');
  }

function ModDeal ($conn, $fields_values, $id, $col = 'id')
  {
    return ModGeneric ($conn, $fields_values, 'deals', $id, $col);
  }



function GetTransaction ($conn, $id, $col = 'id')
  {
    return GetGeneric ($conn, $id, $col, 'transactions');
  }

function GetTransactions ($conn, $ids, $col = 'id')
  {
    return GetGenerics ($conn, $ids, $col, 'transactions');
  }

function SetTransaction ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'transactions');
  }

function SetTransactions ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'transactions');
  }

function ModTransaction ($conn, $fields_values, $id, $col = 'id')
  {
    return ModGeneric ($conn, $fields_values, 'transactions', $id, $col);
  }



////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones especificas del script ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Busca datos relevantes asociados a un DealExternal
 *
 * Dada una conexión y un id de DealExternal, busca datos relevantes asociados al Deal de ese
 * DealExternal, la cantidad de cupones que deberia haber y la cantidad que en efecto hay.
 *
 * @access protected
 * @param mysqli $conn  conexión a la base de datos
 * @param int $deal_external_id  id del DealExternal del cual se buscan datos
 * @return null|bool|array(quantity,actual_quantity,deal_data=>array(user_id,deal_id,quantity,payment_type_id,gift_data,deal_external_id,discount_amount,partial_amount,final_amount))
 */
function getRelevantDealExternalData ($conn, $deal_external_id)
  {
    // Buscamos el deal_external y lo metemos en una array asociativa
    if (($dext = GetDealExternal ($conn, $deal_external_id)) === false)
      { echo ("No se pueden obtener deal_externals para deal_external_id = " . $deal_external_id . ":\n" . mysqli_error ($conn) . "\n"); return false; }
    // En caso de que no exista retornamos null
    if (count ($dext) == 0)
      return null;
    // Si existen varios (¿WTF?) es un error
    if (count ($dext) != 1)
      { echo ("Error al obtener deal_externals: se esperaba 1 resultado y se ubtuvieron " . count ($dext) . " para deal_external_id = " . $deal_external_id . ".\n"); return false; }
    // Nos quedamos con el primero (y unico a esta altura)
    $dext = $dext [0];

    // Buscamos el deal y lo metemos en una array asociativa
    if (($deal = GetDeal ($conn, $dext ['deal_id'])) === false)
      { echo ("No se pueden obtener deals para deal_id = " . $deal_id . ":\n" . mysqli_error ($conn) . "\n"); return false; }
    // Si existen varios (¿WTF?) es un error
    if (count ($deal) != 1)
      { echo ("Error al obtener deals: se esperaba 1 resultado y se ubtuvieron " . count ($deal) . " para deal_id = " . $deal_id . ".\n"); return false; }
    // Nos quedamos con el primero (y unico a esta altura)
    $deal = $deal [0];

    // Buscar la cantidad de deal_users
    if (($actual_quantity = GetDealUser ($conn, $deal_external_id, 'deal_external_id')) === false)
      { echo ("No se pueden obtener deal_users para deal_external_id = " . $deal_external_id . ":\n" . mysqli_error ($conn) . "\n"); return false; }
    // Nos quedamos con la cantidad nada mas
    $actual_quantity = count ($actual_quantity);

    // Armar el 'ret'
    $ret = array
      (
        'user_id'          => $dext ['user_id'],
        'deal_id'          => $dext ['deal_id'],
        'quantity'         => $dext ['quantity'],
        'actual_quantity'  => $actual_quantity,
        'payment_type_id'  => $dext ['payment_type_id'],
        'gift_data'        => $dext ['gift_data'],
        'deal_external_id' => $deal_external_id,
        'discount_amount'  => $deal ['discount_amount'],
        'partial_amount'   => $deal ['discounted_price'],
        'final_amount'     => $deal ['discounted_price'] * $dext ['quantity'],
      );
    
    return $ret;
  }



/**
 * Crea una transaccion de pago
 *
 * Inserta una fila de pago en la tabla Transactions que representa un pago de un usuario.
 *
 * @access protected
 * @param mysqli $conn conexión a la base de datos
 * @param array(user_id,deal_id,quantity,payment_type_id,gift_data,deal_external_id,discount_amount,partial_amount,final_amount) $data  información del Deal
 * @return bool
 */
function createPaymentTransaction ($conn, $data)
  {
    // Intentar insertar la fila, si no fue posible retornar un error
    $fields_values = array
      (
        'created'             => 'NOW()',
        'modified'            => 'NOW()',
        'user_id'             => $data ['user_id'],
        'foreign_id'          => 1,
        'class'               => '"SecondUser"',
        'transaction_type_id' => 1,
        'amount'              => $data ['final_amount'],
        'description'         => '"add amount to wallet from cron job"',
      );
    if (SetTransaction ($conn, $fields_values) === false)
      { echo ("No se puede insertar la transaccion de pago:\n" . mysqli_error ($conn) . "\n"); return false; }

    return true;
  }



/**
 * Crea un cupon y una transaccion de cupon
 *
 * Inserta un DealUser y una fila de pago en la tabla Transactions que representa un cupon para
 * un usuario.
 *
 * @access protected
 * @param mysqli $conn conexión a la base de datos
 * @param array(user_id,deal_id,quantity,payment_type_id,gift_data,deal_external_id,discount_amount,partial_amount,final_amount) $data  información del Deal
 * @return bool
 */
function createCouponAndTransaction ($conn, $data)
  {
    // Intentar insertar el cupon, si no fue posible retornar un error
    $fields_values = array
      (
        'created'          => 'NOW()',
        'modified'         => 'NOW()',
        'user_id'          => $data ['user_id'],
        'deal_id'          => $data ['deal_id'],
        'deal_external_id' => $data ['deal_external_id'],
        'coupon_code'      => '"' . _uuid () . '"',
        'quantity'         => $data ['quantity'],
        'discount_amount'  => $data ['discount_amount'],
        'payment_type_id'  => $data ['payment_type_id'],
        'is_paid'          => 'true',
        'is_repaid'        => 'false',
        'is_gift'          => _SQLNullIsFalse ($data ['gift_data']['is_gift']),
        'gift_to'          => _SQLEmptyStringIsNull ($data ['gift_data']['to']),
        'gift_from'        => _SQLEmptyStringIsNull ($data ['gift_data']['from']),
        'gift_email'       => _SQLEmptyStringIsNull ($data ['gift_data']['email']),
        'message'          => _SQLEmptyStringIsNull ($data ['gift_data']['message']),
        'is_used'          => 'false',
        'emailed'          => 'false',
        'email_to_company' => 'false',
      );
    if (($deal_user_id = SetDealUser ($conn, $fields_values)) === false)
      { echo ("No se puede crear el cupon:\n" . mysqli_error ($conn) . "\n"); return false; }

    // Intentar insertar la transaccion, si no fue posible retornar un error
    $fields_values = array
      (
        'created'             => 'NOW()',
        'modified'            => 'NOW()',
        'user_id'             => $data ['user_id'],
        'foreign_id'          => $deal_user_id,
        'class'               => '"DealUser"',
        'transaction_type_id' => $data ['gift_data']['is_gift'] ? 3 : 2,
        'amount'              => $data ['final_amount'],
      );
    if (SetTransaction ($conn, $fields_values) === false)
      { echo ("No se puede crear la transaccion de cupon:\n" . mysqli_error ($conn) . "\n"); return false; }

    return true;
  }



/**
 * Pone un pago como acreditado
 *
 * Modifica un DealExternal de forma tal que su estado sea "A" (acreditado).
 *
 * @access protected
 * @param mysqli $conn conexión a la base de datos
 * @param int $deal_external_id  id del DealExternal al cual actualizar
 * @return bool
 */
function setDealExternalCredited ($conn, $deal_external_id)
  {
    // Intentar acreditar el DealExternal, si no fue posible retornar un error
    $fields_values = array
      (
        'external_status' => '"A"',
      );
    if (ModDealExternal ($conn, $fields_values, $deal_external_id) === false)
      { echo ("No se puede acreditar el DealExternal con deal_external_id = " . $deal_external_id . ":\n" . mysqli_error ($conn) . "\n"); return false; }

    return true;
  }



/**
 * Crear cupones faltantes dada una lista de ids de DealExternals
 *
 * Crea cupones faltantes teniendo en cuenta los ya generados y generando las transacciones
 * adecuadas para una lista de DealExternals dada.
 *
 * @access public
 * @param mysqli $conn conexión a la base de datos
 * @param array(deal_external) $arr  array de DealExternals
 * @return void
 */
function createMissingCoupons ($conn, $deal_external_ids)
  {
    // Para cada deal_external_id en el array pasado...
    foreach ($deal_external_ids as $deal_external_id)
      {
        // Buscar los datos relevantes para el DealExternal
        if (($data = getRelevantDealExternalData ($conn, $deal_external_id)) === false)
          { echo "Error al buscar la cantidad de cupones que deberia haber y la cantidad que hay en efecto.\n"; continue; }
        // Si la id no existia, ignorarla
        if ($data === null)  continue;

        // Si hay discrepancia...
        if ($data ['quantity'] - $data ['actual_quantity'] > 0)
          {
            // Si todavia no se genero ningun cupon...
            if ($data [actual_quantity] == 0)
              {
                // Crear la transaccion de pago
                if (createPaymentTransaction ($conn, $data) === false)
                  { echo "Error al insertar transaccion de pago.\n"; continue; }
              }

            // Crear los cupones y sus transacciones asociadas
            for ($i = 0; $i < $data ['quantity'] - $data ['actual_quantity']; $i++)
              {
                if (createCouponAndTransaction ($conn, $data) === false)
                  { echo "Error al insertar transaccion de cupon.\n"; continue; }
              }

            // Pone el DealExternal en estado "Acreditado"
            if (setDealExternalCredited ($conn, $deal_external_id) === false)
              { echo "Error al intentar poner como acreditado un pago.\n"; continue; }
          }
      }
  }



////////////////////////////////////////////////////////////////////////////////////////////////////
// Cuerpo del script ///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

echo "\n";

// Conectarse y morir si falla
$conn = mysqli_connect ($DB_host, $DB_user, $DB_pass, $DB_schm, $DB_port);
if (mysqli_connect_error ())
  { die ("No es posible conectarse a la base de datos: " . mysqli_connect_error () . "\n"); }


// Lista de ids a verificar
$ids = array (22932, 24182, 24184, 24186, 24187, 24189, 24192);

// Llamar al proceso para esas ids
createMissingCoupons ($conn, $ids);


// Cerrar conexión a la base
mysqli_close ($conn);
?>