<?php

  if (!function_exists ('myTruncate2')) {
    // Original PHP code by Chirp Internet: www.chirp.com.au
    // Please acknowledge use of this code by including this header.
    function myTruncate2 ($string, $limit, $break = " ", $pad = "...") {
      // return with no change if string is shorter than $limit
      if (strlen ($string) <= $limit) return $string;

      $string = substr ($string, 0, $limit);
      if (false !== ($breakpoint = strrpos ($string, $break))) {
        $string = substr ($string, 0, $breakpoint);
      }

      return $string . $pad;
    }
  }

  $main_deal = $vars ['main_deal'];

  $image_dir = $vars ['SITE_LINK'] . '/img/theme_clean/email_template/';


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv = "Content-Type"    content = "text/html; charset=utf-8" />
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge" />
    <title>Newsletter Club Cup&oacute;n</title>
</head>
<body>
<center>
<!--CONTENEDOR DEL NEWSLETTER-->
<table width="630" border="0" cellspacing="0" cellpadding="0" style="background:#ffffff">
  <tr>
    <td style="padding:10px 0;"><!--DENTRO DE ESTE TD ESTA TODO EL NEWSLETTER-->
    <table align="center" width="630" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td style="border:solid 4px #bebebe; padding:6px;">
        <table width="610" border="0" cellpadding="0" cellspacing="5">
          <tr>
            <td colspan="2" bgcolor="#1c1d1f" style="padding:10px;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0"><!--COMIENZO HEADER-->
                  <tr>
                    <td width="30%" align="left"><a target="_blank" href="<?php echo $vars ['SITE_LINK']; ?>"><img src="<?php echo $image_dir; ?>logo.png" alt="club cupon" title="Club Cupon" border="0" /></a></td>
                    <td width="70%">
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:right; padding:0; margin:0; color:#FFFFFF; line-height:20px;">
                            <?php echo date("d - m - y"); ?><br />
                            Las mejores ofertas de <span style="font-weight:bold; font-size:16px;"><?php echo $vars['CITY_NAME']; ?></span>
                        </p>
                    </td>
                  </tr>
                </table><!--FIN HEADER-->
            </td>
          </tr>
          <tr>
            <td colspan="2">









        <?php if($main_deal['newsletterimage']){?>







                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:10px;"><!--COMIENZO OFERTA PRINCIPAL-->
              <tbody><tr style="line-height: 0 !important">
              	<td style="line-height: 0 !important">
                <a target="_blank" style="line-height: 0 !important" href="<?php echo $main_deal ['DEAL_LINK']; ?>"><img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>"  width="<?php echo Configure::read('deal.NewsLetterImage.width');?>" border="0"></a>
                </td>
              </tr>
              <tr>
                <td style="border:solid 1px #cfcfcf;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                  <tbody><tr>
                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="25%">
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                    Beneficio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['DISCOUNT']; ?> </span>                    </p>                    </td>
                    <td style="border-right:1px solid #cfcfcf; padding:20px 0;" width="25%">
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                    Precio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span>                    </p>                    </td>
                    <td width="25%">
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#54b7d7; font-weight:bold;">
                    <?php echo $main_deal ['BUY_PRICE']; ?> en vez de <?php echo $main_deal ['ORIGINAL_PRICE']; ?></p>
                    </td>
                    <td align="center" width="25%"><a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>"><img src="<?php echo $image_dir; ?>buttom.jpg" alt="" border="0"></a></td>
                  </tr>
                </tbody></table>                </td>
                </tr>
                <tr>
                	<td style="background-color:#f2f2f2; border-left:solid 1px #cfcfcf; border-right:solid 1px #cfcfcf; border-bottom:solid 1px #cfcfcf; padding:5px 10px;">
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#3b3b3b; text-align:left; margin:0; padding:0;">
                    <?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>
                    </p>
                    </td>
                </tr>
            </tbody></table><!--FIN OFERTA PRINCIPAL-->



<?php } else {?>





















            <!--COMIENZO OFERTA PRINCIPAL-->

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>">
                    <img src="<?php echo $main_deal ['DEAL_IMAGE']; ?>" alt="<?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>" title="<?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>" height = "184"  border = "0" style="display:block"/>
                  </a>
                </td>
                <td rowspan="2" bgcolor="#f58229" style="padding:10px; vertical-align:top;" height="230">
                <?php if($main_deal['Deal']['only_price']==0){?>
                  <p style="font-family:Arial, Helvetica, sans-serif; font-size:22px; color:#FFFFFF; padding:0; margin:0;">
                    <span style="font-size:24px;"><?php echo $main_deal ['DISCOUNT']; ?></span> de descuento
                  </p>
                    <?php } ?>
                  <?php if($main_deal['Deal']['only_price']==0){ ?>

                        <?php if($main_deal ['ONLY_PRICE']==0){ ?>

                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                                <?php echo $main_deal ['BUY_PRICE']; ?> en vez de <?php echo $main_deal ['ORIGINAL_PRICE']; ?> en <?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>
                            </p>

                        <?php }else{ ?>

                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                                <?php echo $main_deal ['BUY_PRICE']; ?>
                            </p>

                        <?php } ?>

                  <?php }else{ ?>
                      <p style="font-family:Arial, Helvetica, sans-serif; font-size:17px; text-align:left; padding:60px 0 0 0; margin:0; color:#FFFFFF;">
                        <?php echo myTruncate2 ($main_deal ['DEAL_NAME'], 150); ?>
                      </p>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td bgcolor="#f2f2f2" style="border:solid 1px #cfcfcf; padding:10px;">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="33%" style="border-right:1px solid #cfcfcf;">

                           <?php if($main_deal['Deal']['only_price']==0){ ?>
                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                            Beneficio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['DISCOUNT']; ?></span>
                            </p>
                          <?php } ?>

                        </td>
                        <td width="33%">
                          <?php if($main_deal['Deal']['hide_price']==0){ ?>
                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                              Precio <span style="font-size:18px; font-weight:bold;"><?php echo $main_deal ['BUY_PRICE']; ?></span>
                            </p>
                          <?php } ?>
                        </td>
                        <td width="33%" align="center"><a target="_blank" href="<?php echo $main_deal ['DEAL_LINK']; ?>"><img src="<?php echo $image_dir; ?>buttom.jpg" alt="" border="0" /></a></td>
                      </tr>
                    </table>

                </td>
              </tr>
            </table>








<?php } ?>









            <!--FIN OFERTA PRINCIPAL-->


            </td>
          </tr>

          <?php
            if(!empty($vars['BANNER_IMG_PATH']))
            {
          ?>

          <!-- INICIO BANNER -->
          <tr style="line-height: 0 !important">
            <td colspan="2" align="center" style="*text-align:center; line-height: 0 !important">
              <img width = "1"  height = "1"  src = "<?php echo $vars['URL_TRACKER'];?>"  border = "0" />
              <a target="_blank" href="<?php echo $vars['BANNER_IMG_LINK']; ?>" style="border:none; line-height: 0 !important">
                <img src="<?php echo $vars ['SITE_LINK'] . $vars ['BANNER_IMG_PATH'] ; ?>" border="0"  width = "598"  height = "100"  alt = "banner"  style="display:block; border:none;margin-left: auto;margin-right:auto;width:598px; margin-bottom: 10px"/>
              </a>
            </td>
          </tr>
          <!-- FIN BANNER -->

          <?php } ?>


          <?php
            foreach ($vars ['deals'] as $deal)
            {
              if ($deal ['Deal']['id'] != $main_deal ['Deal']['id'])
              {
          ?>

                <!--COMIENZO LISTAS DE OFERTAS-->
                <tr>
                  <td colspan="2" bgcolor="#f2f2f2" style="padding:10px; border:solid 1px #cfcfcf;">
                    <!--Inicio Oferta-->
                      <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--comienzo oferta-->
                        <tr>
                          <!-- imagen de la oferta -->
                          <td width="31%">
                            <a target="_blank" href="<?php echo $deal ['DEAL_LINK']; ?>">
                              <img src = "<?php echo $deal ['DEAL_IMAGE']; ?>"  width = "201"  height = "130"  border = "0"  style="display:block"/>
                            </a>
                          </td>

                          <td width="35%" style="border-right:solid 1px #cfcfcf; padding:10px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="50%">
                                <?php if($deal['Deal']['only_price']==0){ ?>
                                  <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                    Beneficio <span style="font-size:16px; font-weight:bold; display:block"><?php echo $deal ['DISCOUNT']; ?></span>
                                  </p>
                                  <?php } ?>
                                </td>

                                <?php $cell_width = ($deal['Deal']['hide_price'] ? '100' : '50'); ?>

                                <td width="<?php echo $cell_width; ?>%" style=" border-left:solid 1px #cfcfcf;">
                                  <?php if($deal['Deal']['hide_price']==0){ ?>
                                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; padding:0; margin:0; color:#3a3a3a;">
                                      Precio <span style="font-size:16px; font-weight:bold; display:block"><?php echo $deal ['BUY_PRICE']; ?></span>
                                    </p>
                                  <?php } ?>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2" style="padding-top:10px;" align="center">
                                  <a target="_blank" href="<?php echo $deal ['DEAL_LINK']; ?>">
                                    <img src="<?php echo $image_dir; ?>buttom.jpg" alt="" border="0" />
                                  </a>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="34%" style="vertical-align:top; padding:10px 5px 0 10px;">
                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; padding:0; margin:0; color:#3a3a3a;">

                                <?php if($deal['Deal']['hide_price']==0){ ?>


                                    <?php if($deal['Deal']['only_price']==0){ ?>

                                       <span style="color:#54b7d7; display:block; font-weight:bold;"><?php echo $deal ['BUY_PRICE']; ?> en vez de <?php echo $deal ['ORIGINAL_PRICE']; ?></span>

                                    <?php }else{ ?>

                                       <span style="color:#54b7d7; display:block; font-weight:bold;"><?php echo $deal ['BUY_PRICE']; ?></span>

                                    <?php } ?>


                                <?php } ?>

                                  por <?php echo myTruncate2 ($deal ['DEAL_NAME'], 120); ?>
                            </p>
                           
                          </td>
                        </tr>
                      </table>
                    <!--Fin Oferta-->
                  </td>
                </tr>
                <!--FIN LISTAS DE OFERTAS-->

          <?php
              }
            }
          ?>

          <!--INICIO SECCION DE RECOMENDAR-->
          <tr>
          	<td width="50%" style="padding:10px;" align="center"><!-- SECCION DE RECOMENDAR-->
            <table width="85%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding:10px;"><a target="_blank" href="<?php echo $vars ['SITE_LINK']; ?>"><img src="<?php echo $image_dir; ?>recomenda.jpg" alt="" border="0" /></a></td>
                </tr>
              <tr>
                <td style="padding:15px; border:solid 1px #d9d9d9;" bgcolor="#f5f5f5">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td colspan="3" align="center" style="padding:5px 0;">
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#f6872e; padding:0; margin:0;">
                        Recomend&aacute; ahora ClubCup&oacute;n!
                    </p>
                    </td>
                    </tr>
                  <tr>
                    <td align="center"><a target="_blank" href="<?php echo $vars ['FACEBOOK_URL']; ?>"><img src="<?php echo $image_dir; ?>r_face.jpg"    alt="" border="0" /></a></td>
                    <td align="center"><a target="_blank" href="<?php echo $vars ['TWITTER_URL'];  ?>"><img src="<?php echo $image_dir; ?>r_twitter.jpg" alt="" border="0" /></a></td>
                    <td align="center"><a target="_blank" href="<?php echo $vars ['SITE_LINK'];    ?>"><img src="<?php echo $image_dir; ?>r_email.jpg"   alt="" border="0" /></a></td>
                  </tr>
                </table>
                </td>
                </tr>
            </table>
            </td>
            <!--FIN SECCION DE RECOMENDAR-->
            <td width="50%" style="border-left:solid 1px #cfcfcf; padding:10px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding:0 0 10px 0;">
                <p style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; color:#2fa9d2; padding:0; margin:0; text-align:center">
                    Seguinos en:
                </p>
                </td>
              </tr>
              <tr>
                <td align="center">
                <table width="100" border="0" cellspacing="5" cellpadding="0">
                  <tr>
                    <td><a target="_blank" href="<?php echo $vars ['FACEBOOK_URL']; ?>"><img src="<?php echo $image_dir; ?>icon_f.jpg" alt="" border="0" title="facebook" /></a></td>
                    <td><a target="_blank" href="<?php echo $vars ['TWITTER_URL'];  ?>"><img src="<?php echo $image_dir; ?>icon_t.jpg" alt="" border="0" title="twitter" /></a></td>
                    <td><a target="_blank" href="<?php echo $vars ['RSS_FEED'];     ?>"><img src="<?php echo $image_dir; ?>icon-rss.jpg" alt="" border="0" title="rss" /></a></td>
                  </tr>
                </table>
                </td>
              </tr>
              <tr>
                <td style="padding:10px 0 0 0;">
                <p style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; color:#3c3c3c; padding:0; margin:0; text-align:center">
                    Y s&eacute; el primero en enterarte de nuestras ofertas!
                </p>
                </td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td bgcolor="#d9d9d9" style="background-color:#d9d9d9; border-top:solid 3px #cfcfcf; padding:5px;"><!--DENTRO DE ESTE TD ESTA EL FOOTER-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="23%" align="right" style="padding:5px;"><img src="<?php echo $image_dir; ?>logo_club_footer.jpg" alt="" border="0" /></td>
                                <td width="62%" style="padding:5px;">
                                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:11px; text-align:center; padding:0; margin:0; color:#3b3b3b;">
                                        Recibiste este email porque te suscribiste al Newsletter <br />de ClubCup&oacute;n <?php echo $vars['CITY_NAME']; ?><br>
                                        Para no perderte ninguna oferta de ClubCupon agreg&aacute; la direcci&oacute;n <a style="color:#137ba2;" href="mailto:info@clubcupon.com.ar">info@clubcupon.com.ar</a> a tu lista de contactos,<br /> y marcanos como correo deseado.<br />
                                        <br />
                                        Si no dese&aacute;s recibir mas el newsletter de <?php echo $vars['CITY_NAME']; ?>,<br /> por favor <a target="_blank" style="color:#137ba2;" href="<?php echo $vars['UNSUBSCRIBE_LINK']; ?>">visit&aacute; este enlace.</a>
                                        <br />
                                        <br />
                                        Si quer&eacute;s darte de baja de todos nuestros newsletters,<br /> por favor <a target="_blank" style="color:#137ba2;" href="<?php echo $vars['UNSUBSCRIBE_ALL_LINK']; ?>">segu&iacute; este enlace.</a>
                                        <br />
										 Este es un env&iacute;o autom&aacute;tico de mail por favor no responda el mismo. Gracias.
		 								<br />
		 								Por cualquier consulta, ingres&aacute; ac&aacute; <a href="http://soporte.clubcupon.com.ar" >http://soporte.clubcupon.com.ar</a>
                                    </p>
                                </td>
        <td width="15%" align="left" style="padding:5px;"><img src="<?php echo $image_dir; ?>logo_clarin_footer.jpg" alt="" border="0" /></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td style="padding:10px;">
        <p style="font-family:Arial, Helvetica, sans-serif; font-size:11px; text-align:left; padding:0; margin:0; color:#3b3b3b;">
        Art.27 L.25.326 inc.3: el titular podr&aacute; en cualquier momento solicitar el retiro o bloqueo de su nombre de los bancos de datos a los que se refiere el presente art&iacute;culo. Decr. 1558/01-Art. 27 3er p&aacute;rrafo: en toda comunicaci&oacute;n con fines de publicidad que se realice por correo, tel&eacute;fono, correo electr&oacute;nico, internet, u otro medio a distancia a conocer, se deber&aacute; indicar, en forma expresa y destacada la posibilidad del titular del dato de solicitar el retiro o bloqueo, total o parcial, de su nombre de la base de datos. A pedido del interesado, se deber&aacute; informar el nombre del responsable o usuario del banco de datos que provey&oacute; la informaci&oacute;n. El titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a 6 meses, salvo que se acredite un inter&eacute;s legitimo al efecto conforme lo establecido por el art. 14, inc. 3 de la ley 25.326. La Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales, &oacute;rgano de control de la ley 25.326, tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales.
    </p>
    </td>
  </tr>
</table><!--FIN CONTENEDOR DEL NEWSLETTER-->
</center>
</body>
</html>