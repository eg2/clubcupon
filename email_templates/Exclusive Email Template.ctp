<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Club Cup&oacute;n Exclusive, ofertas exclusivas pensadas para vos</title>
    </head>

    <body>

        <center>

            <table width="620" border="0" cellpadding="0" cellspacing="0">

                <tr style="line-height:0 !important; background:#000;">
                    <td colspan="3"><img src="<?php echo $vars['ABSOLUTE_IMG_PATH']; ?>head.png" width="627" height="145" /></td>
                </tr>

                <tr style="line-height:auto !important; background:#000;">
                    <td width="23" height="62" style="color:white; font:15px arial;">&nbsp;</td>
                    <td width="570" valign="bottom" style="color:white; font:15px arial;"><div align="left">HOLA,</div></td>
                    <td width="34"  valign="bottom" style="color:white; font:15px arial;">&nbsp;</td>
                </tr>

                <tr style="line-height:auto !important; background:#000;height: 160px;">
                    <td height="82" valign="bottom" style="color:white; font:15px arial;">&nbsp;</td>
                    <td height="82" valign="middle" style="color:white; font:15px arial;">
                        <div align="left">
                            Club Cup&oacute;n Exclusive es una opci&oacute;n disponible para los empleados de las empresas que tienen acuerdos exclusivos con Club Cup&oacute;n.
                            <br />
                            <br />
                            Si pertenec&eacute;s a una compa&ntilde;&iacute;a que a&uacute;n no cuenta con acceso a nuestras ofertas exclusivas, te sugerimos nos env&iacute;es los datos del &aacute;rea de Recursos Humanos de tu empresa, para que podamos ponernos en contacto y acercarles nuestros beneficios al mail: <a href="mailto:exclusive@clubcupon.com.ar" style="color: #199FC4">exclusive@clubcupon.com.ar</a>
                        </div>
                    </td>
                    <td valign="middle" style="color:white; font:15px arial; background:#000;">&nbsp;</td>
                </tr>
                <tr style="line-height:auto !important;">
			    <td height="38" colspan="3" valign="bottom" bgcolor="#FFFFFF">
					<img src="<?php echo $vars['ABSOLUTE_IMG_PATH']; ?>bottom.png" width="627" height="38" />
                </td>
                </tr>
                <tr style="line-height:auto !important;">
                    <td height="35" colspan="3" valign="bottom" bgcolor="#FFFFFF" style="color:#6c6c6c; font:12px arial; ">
                        <div align="left" style="padding-top: 0px;">
                            &copy; Club Cup&oacute;n es un servicio de TECDIA S.A. | <a href="http://soporte.clubcupon.com.ar/" target="_blank">http://soporte.clubcupon.com.ar/</a><br />
                            Su direcci&oacute;n de mail <?php echo $vars['USER_EMAIL']; ?> fue suscripta desde la ip <?php echo $vars['USER_IP']; ?> el d&iacute;a <?php echo $vars['DATE']; ?> a las <br />
                            <?php echo $vars['TIME']; ?>. Si usted no realiz&oacute; esta suscripci&oacute;n puede desuscribirse haciendo click <a href="<?php echo $vars['UNSUBSCRIBE_LINK']; ?>">aqu&iacute;</a>.
                        </div>
                    </td>
                </tr>

            </table>

        </center>

    </body>
    
</html>