<?php
/** Configuracion de las DBs */
/** esto es localhost */
/*
$CC_DB_host = 'localhost';
$CC_DB_user = 'root';
$CC_DB_pass = 'root';
$CC_DB_schm = 'clubcupon';
$CC_DB_port = '3306';
*/
/** esto es test */
$CC_DB_host = '10.100.1.178';
$CC_DB_user = 'ccdeploy';
$CC_DB_pass = 'ccd123';
$CC_DB_schm = 'ccdeploy';
$CC_DB_port = null;
/** esto es produccion */
/*
$CC_DB_host = '10.100.1.120';
$CC_DB_user = 'clubcupon';
$CC_DB_pass = 'clubcup0n';
$CC_DB_schm = 'clubcupon';
$CC_DB_port = null;
*/

/** esto es test */
$BAC_DB_host = 'v128udesa.int.cmd.com.ar';
$BAC_DB_user = 'backend';
$BAC_DB_pass = 'backend123';
$BAC_DB_schm = 'backend_test';
$BAC_DB_port = '3306';
/** esto es produccion */
/*
$BAC_DB_host = 'v130uprod.int.cmd.com.ar';
$BAC_DB_user = 'openreports';
$BAC_DB_pass = '0p3nr1p';
$BAC_DB_schm = 'bac';
$BAC_DB_port = '3306';
*/

/** Configuracion del script */
$paid_to_company_status_id = 8;
$wallet_payment_type_id = 5000;


////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones de bajo nivel /////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function GetGeneric ($conn, $id, $col = 'id', $table, $extra = '')
  {
    // Arreglamos parámetros
    if ($extra != '')  $extra = ' ' . $extra;

    // Buscamos el elemento, si hay errores retornar false
    if (($res = mysqli_query ($conn, 'SELECT * from `' . $table . '` WHERE `' . $col . '` = ' . $id . '' . $extra . '')) === false)
      {
        mysqli_free_result ($res);
        return false;
      }

    // Metemos el elemento en una array asociativa
    $rets = array ();
    while ($row = mysqli_fetch_assoc ($res))
      $rets [] = $row;
    // Liberamos la consulta
    mysqli_free_result ($res);

    return $rets;
  }

function GetGenerics ($conn, $ids, $col = 'id', $table, $extra = '')
  {
    // Inicializar la lista de retorno
    $ret = array ();

    // Llamar a GetGeneric con cada id y quedarnos solo con el resultado de las que no fallaron
    foreach ($ids as $id)
      if (($res = GetGeneric ($conn, $id, $col, $table, $extra)) !== false)
        $ret [$id] = $res;

    // Solo retornar false si todas fallaron
    return (count ($ret) != 0) ? $ret : false;
  }

function SetGeneric ($conn, $fields_values, $table)
  {
    // Armamos las listas de campos y valores
    $fields = array ();
    foreach (array_keys ($fields_values) as $field)  $fields [] = '`' . $field . '`';
    $fields = implode (', ', $fields);
    $values = implode (', ', array_values ($fields_values));

    // Insertamos la fila y retornamos lo que retorne la query
    if (mysqli_query ($conn, 'INSERT INTO `' . $table . '` (' . $fields . ') VALUES (' . $values . ')') === false)
      return false;

    return mysqli_insert_id ($conn);
  }

function SetGenerics ($conn, $fields_valueses, $table)
  {
    // Inicializar la lista de retorno
    $ret = array ();

    // Llamar a GetDealUser con cada id y quedarnos con el resultado
    foreach ($fields_valueses as $key => $fields_values)
      $ret [$key] = SetGeneric ($conn, $fields_values, $table);

    return $ret;
  }

function ModGeneric ($conn, $fields_values, $table, $id, $col = 'id', $extra = '')
  {
    // Armamos las listas de campos y valores
    $fields = array ();
    foreach ($fields_values as $field => $value)  $fields [] = '`' . $field . '`=' . $value;
    $fields = implode (', ', $fields);

    // Arreglamos parámetros
    if ($extra != '')  $extra = ' ' . $extra;

    // Updateamos la fila y retornamos lo que retorne la query
    if (mysqli_query ($conn, 'UPDATE `' . $table . '` SET ' . $fields . ' WHERE `' . $col . '`=' . $id . '' . $extra . '') === false)
      return false;

    return mysqli_affected_rows ($conn);
  }



function GetDealExternal ($conn, $id, $col = 'id', $extra = '')
  {
    // Buscamos el deal_external, si hay errores retornar false
    if (($rets = GetGeneric ($conn, $id, $col, 'deal_externals', $extra)) === false)  return false;
    // Decodificamos gift_data
    foreach ($rets as & $ret)
      $ret ['gift_data'] = json_decode ($ret ['gift_data'], true);

    return $rets;
  }

function GetDealExternals ($conn, $ids, $col = 'id', $extra = '')
  {
    // Buscamos todos los DealExternals, si hay errores retornar false
    if (($retss = GetGenerics ($conn, $ids, $col, 'deal_externals', $extra)) === false)  return false;
    // Decodificamos gift_data para todos los deal_externals
    foreach ($retss as & $rets)
      foreach ($rets as & $ret)
        $ret ['gift_data'] = json_decode ($ret ['gift_data'], true);

    return $retss;
  }

function SetDealExternal ($conn, $fields_values)
  {
    // Codificamos gift_data
    if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Guardamos el DealExternal
    return SetGeneric ($conn, $fields_values, 'deal_externals');
  }

function SetDealExternals ($conn, $fields_valueses)
  {
    // Codificamos gift_data para todos los deal_externals
    foreach ($fields_valueses as & $fields_values)
      if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Guardamos todos los DealExternals
    return SetGenerics ($conn, $fields_valueses, 'deal_externals');
  }

function ModDealExternal ($conn, $fields_values, $id, $col = 'id', $extra = '')
  {
    // Codificamos gift_data
    if (!is_null ($fields_values ['gift_data'])) $fields_values ['gift_data'] = json_encode ($fields_values ['gift_data']);
    // Modificamos el DealExternal
    return ModGeneric ($conn, $fields_values, 'deal_externals', $id, $col, $extra);
  }



function GetDealUser ($conn, $id, $col = 'id', $extra = '')
  {
    return GetGeneric  ($conn, $id, $col, 'deal_users', $extra);
  }

function GetDealUsers ($conn, $ids, $col = 'id', $extra = '')
  {
    return GetGenerics ($conn, $ids, $col, 'deal_users', $extra);
  }

function SetDealUser ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'deal_users');
  }

function SetDealUsers ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'deal_users');
  }

function ModDealUser ($conn, $fields_values, $id, $col = 'id', $extra = '')
  {
    return ModGeneric ($conn, $fields_values, 'deal_users', $id, $col, $extra);
  }




function GetDeal ($conn, $id, $col = 'id', $extra = '')
  {
    return GetGeneric ($conn, $id, $col, 'deals', $extra);
  }

function GetDeals ($conn, $ids, $col = 'id', $extra = '')
  {
    return GetGenerics ($conn, $ids, $col, 'deals', $extra);
  }

function SetDeal ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'deals');
  }

function SetDeals ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'deals');
  }

function ModDeal ($conn, $fields_values, $id, $col = 'id', $extra = '')
  {
    return ModGeneric ($conn, $fields_values, 'deals', $id, $col, $extra);
  }



function GetTransaction ($conn, $id, $col = 'id', $extra = '')
  {
    return GetGeneric ($conn, $id, $col, 'transactions', $extra);
  }

function GetTransactions ($conn, $ids, $col = 'id', $extra = '')
  {
    return GetGenerics ($conn, $ids, $col, 'transactions', $extra);
  }

function SetTransaction ($conn, $fields_values)
  {
    return SetGeneric ($conn, $fields_values, 'transactions');
  }

function SetTransactions ($conn, $fields_valueses)
  {
    return SetGenerics ($conn, $fields_valueses, 'transactions');
  }

function ModTransaction ($conn, $fields_values, $id, $col = 'id', $extra = '')
  {
    return ModGeneric ($conn, $fields_values, 'transactions', $id, $col, $extra);
  }



////////////////////////////////////////////////////////////////////////////////////////////////////
// Funciones especificas del script ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

/*
SELECT *
FROM `deals`
WHERE $conditions
*/
function getDealsByConditions ($conn, $conditions)
  {
    if ($conditions != '')  $conditions_n = 'OR ' . $conditions; else $conditions_n = '';

    if (($ret = GetDeal ($conn, 'null', 'id', $conditions_n)) === false)
      { echo ("No se pueden obtener los Deals con condiciones = `" . $conditions . "':\n" . mysqli_error ($conn) . "\n"); return false; }

    return $ret;
  }


/*
SELECT SUM(`c`.`cantidad`) AS `quantity`, MAX(`c`.`generacion`) AS `date`
FROM `cce_cargo` `c` INNER JOIN `bac_producto` `p` ON `c`.`id_producto` = `p`.`id_producto`
WHERE `p`.`id_producto` = 2003 AND `p`.`id_portal` = 4 AND `c`.`id_comprobante_portal` = $deal_id
GROUP BY `c`.`id_comprobante_portal`
*/
function getPaymentDateAndQuantity ($conn, $deal_id)
  {
    // Buscamos los datos, si hay errores retornar false
    if (($res = mysqli_query ($conn, 'SELECT SUM(`c`.`cantidad`) AS `quantity`, MAX(`c`.`generacion`) AS `date` FROM `cce_cargo` `c` INNER JOIN `bac_producto` `p` ON `c`.`id_producto` = `p`.`id_producto` WHERE `p`.`id_producto` = 2003 AND `p`.`id_portal` = 4 AND `c`.`id_comprobante_portal` = ' . $deal_id . ' GROUP BY `c`.`id_comprobante_portal`')) === false)
      {
        mysqli_free_result ($res);
        echo ("No se pueden obtener las liquidaciones para el deal_id = " . $deal_id . ":\n" . mysqli_error ($conn) . "\n");
        return false;
      }


    // Si no hay filas...
    if (mysqli_num_rows ($res) != 1)
      {
        // El retorno es {null, null}
        $rets = array (null, null);
      }
    // Si hay una fila...
    else
      {
        // Metemos los datos en el array de retorno
        $row = mysqli_fetch_assoc ($res);
        $rets = array ($row ['date'], $row ['quantity']);
      }

    // Liberamos la consulta
    mysqli_free_result ($res);

    return $rets;
  }

/*
SELECT COUNT(*)
FROM `deal_users`
WHERE `deal_id` = 471 AND `paid_date` IS NULL AND `payment_type_id` <> 5000
*/
function getDealUserQuantity ($conn, $deal_id)
  {
    global $wallet_payment_type_id;

    // Buscamos los datos, si hay errores retornar false
    if (($res = mysqli_query ($conn, 'SELECT COUNT(*) AS `quantity` FROM `deal_users` WHERE `deal_id` = ' . $deal_id . ' AND `paid_date` IS NOT NULL AND `payment_type_id` <> ' . $wallet_payment_type_id . '')) === false)
      {
        mysqli_free_result ($res);
        echo ("No se pueden obtener los DealUsers liquidados para el deal_id = " . $deal_id . ":\n" . mysqli_error ($conn) . "\n");
        return false;
      }

    // Metemos los datos en el array de retorno
    $row = mysqli_fetch_assoc ($res);
    $rets = $row ['quantity'];
    // Liberamos la consulta
    mysqli_free_result ($res);

    return $rets;
  }


/*
UPDATE `deal_users`
SET `paid_date` = '$date'
WHERE `deal_id` = $deal_id AND `paid_date` IS NULL AND `payment_type_id` <> 5000
ORDER BY `modified`
LIMIT $quantity
*/
function setDealUserAsPaid ($conn, $deal_id, $date, $quantity = 1)
  {
    global $wallet_payment_type_id;

    // Intentar registrar los DealUsers, si no fue posible retornar un error
    $fields_values = array
      (
        'paid_date' => '"' . $date . '"',
      );

    if (mysqli_query ($conn, "UPDATE `deal_users` SET `paid_date` = '" . $date . "' WHERE `deal_id` = " . $deal_id . " AND `paid_date` IS NULL AND `payment_type_id` <> " . $wallet_payment_type_id . " ORDER BY `modified` LIMIT " . $quantity . "") === false)
      { echo ("No se pueden registrar como pagados los " . $quantity . " primeros DealUsers con deal_id = " . $deal_id . " a paid_date = " . $date . ":\n" . mysqli_error ($conn) . "\n"); return false; }

    return true;
  }



function normalizePayments ($BAC_conn, $CC_conn, $conditions)
  {
    global $paid_to_company_status_id;

    if (($deals = getDealsByConditions ($CC_conn, $conditions)) === false)
      { echo "Error al buscar los Deals para condiciones = " . $conditions . ".\n"; return; }

    $fields_values = array
      (
        'deal_status_id' => $paid_to_company_status_id,
      );

    foreach ($deals as $deal)
      {
        if ((list ($date, $BAC_quantity) = getPaymentDateAndQuantity ($BAC_conn, $deal ['id'])) === false)
          { echo "Error al buscar la cantidad y fecha de los pagos con deal_id = " . $deal ['id'] . ".\n"; continue; }
        elseif (is_null ($date) || is_null ($BAC_quantity))
          { echo ("No existen liquidaciones para el deal_id = " . $deal ['id'] . ".\n"); continue; }

        if (($CC_quantity = getDealUserQuantity ($CC_conn, $deal ['id'])) === false)
          { echo "Error al buscar la cantidad de cupones liquidados con deal_id = " . $deal ['id'] . ".\n"; continue; }

        if (($quantity = $BAC_quantity - $CC_quantity) <= 0)
          { echo "Todos los cupones con deal_id = " . $deal ['id'] . " fueron liquidados.\n"; continue; }

        if (setDealUserAsPaid ($CC_conn, $deal ['id'], $date, $quantity) === false)
          { echo "Error al registrar como pagados los DealUsers con deal_id = " . $deal ['id'] . ".\n"; continue; }

        if (ModDeal ($CC_conn, $fields_values, $deal ['id']) === false)
          { echo "Error al registrar como cerrado al Deal con deal_id = " . $deal ['id'] . ".\n"; continue; }

        echo "Se proceso con exito el Deal con deal_id = " . $deal ['id'] . ".\n";
      }
  }



////////////////////////////////////////////////////////////////////////////////////////////////////
// Cuerpo del script ///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

echo "\n";

// Conectarse a CC y morir si falla
$CC_conn = mysqli_connect ($CC_DB_host, $CC_DB_user, $CC_DB_pass, $CC_DB_schm, $CC_DB_port);
if (mysqli_connect_error ())
  { die ("No es posible conectarse a la base de datos (CC): " . mysqli_connect_error () . "\n"); }

// Conectarse a BAC y morir si falla
$BAC_conn = mysqli_connect ($BAC_DB_host, $BAC_DB_user, $BAC_DB_pass, $BAC_DB_schm, $BAC_DB_port);
if (mysqli_connect_error ())
  { die ("No es posible conectarse a la base de datos (BAC): " . mysqli_connect_error () . "\n"); }

// Condiciones para actualizar Deals
$conditions = "`deal_status_id` = " . $paid_to_company_status_id;

// Llamar al proceso para esas ids
normalizePayments ($BAC_conn, $CC_conn, $conditions);

// Cerrar conexión a las bases
mysqli_close ($BAC_conn);
mysqli_close ($CC_conn);
?>