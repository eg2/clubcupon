#! /bin/bash
while getopts f:c:k:v:p opts; do
  case ${opts} in
    f) shellFileName=${OPTARG} ;;
    c) commandMethod=${OPTARG} ;;
    k) paramKey=${OPTARG} ;;
    v) paramValue=${OPTARG} ;;
  esac
done
while inotifywait -re close_write app; do ./do.sh -f $shellFileName -c $commandMethod -k $paramKey -v $paramValue; done

